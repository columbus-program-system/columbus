

     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 This program allows the csf mixing coefficient and orbital expansion coefficient
 optimization using the graphical unitary group approach and the exponential
 operator mcscf method.
 references:  r. shepard and j. simons, ' int. j. quantum chem. symp. 14, 211 (1980).
              r. shepard, i. shavitt, and j. simons, j. chem. phys. 76, 543 (1982).
              r. shepard in "ab initio methods in quantum chemistry ii" advances in chemical
                  physics 69, edited by k. p. lawley (wiley, new york, 1987) pp. 63-200.
 Original autor: Ron Shepard, ANL
 Later revisions: Michal Dallos, University Vienna

 This Version of Program MCSCF is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.4.0.2     **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 Workspace allocation information:
        13107200 of real*8 words (  100.00 MB) of work space has been allocated.

 user input information:

 ======== echo of the mcscf input ========
 ------------------------------------------------------------------------
  &input
  NPATH =  1, 2, 3, 9,10,12,13,17,19,21,23,30
  navst(1)= 2
  wavst(1,1)=  1.000
  wavst(1,2)=  1.000
  navst(2)= 1
  wavst(2,1)=  1.000
  NSTATE=  0,
  NITER =  30,
  TOL(1)= .00000001,
  NCOUPL =   1,
  FCIORB=
          2   1 40, 2   2 40, 2   3 40, 6   1 40, 6   2 40,
  6   3 40,
 /&end
 ------------------------------------------------------------------------


 ***  Integral file informations  ***


 input integral file : /mnt/gpfs01/home/cm/cmfp2/programs/Columbus/Test/rspada/T
 EST

 Integral file header information:
 Hermit Integral Program : SIFS version  vis002.cm.cluster 11:09:11.432 16-Sep-21

 Core type energy values:
 energy( 1)=  3.329664742176E+01, ietype=   -1,    core energy of type: Nuc.Rep.
 total ao core energy =   33.296647422


   ******  Basis set information:  ******

 Number of irreps:                  8
 Total number of basis functions:  48

 irrep no.              1    2    3    4    5    6    7    8
 irrep label           Ag   B3u  B2u  B1g  B1u  B2g  B3g  Au 
 no. of bas.fcions.    11    4    7    2   11    4    7    2


 ***  MCSCF optimization procedure parmeters:  ***


 maximum number of mcscf iterations:        niter=    30

 maximum number of psci micro-iterations:   nmiter=   30
 maximum r,s subspace dimension allowed:    nvrsmx=   30

 tol(1)=  1.0000E-08. . . . delta-emc convergence criterion.
 tol(2)=  1.0000E-04. . . . wnorm convergence criterion.
 tol(3)=  1.0000E-04. . . . knorm convergence criterion.
 tol(4)=  1.0000E-08. . . . apxde convergence criterion.
 tol(5)=  1.0000E-04. . . . small diagonal matrix element tolerance.
 tol(6)=  1.0000E-06. . . . minimum ci-psci residual norm.
 tol(7)=  1.0000E-05. . . . maximum ci-psci residual norm.
 tol(8)=  1.0000E+00. . . . maximum abs(k(xy)) allowed.
 tol(9)=  1.0000E+00. . . . wnorm coupling tolerance.
 tol(10)= 0.0000E+00. . . . maximum psci emergency shift parameter.
 tol(11)= 0.0000E+00. . . . minimum psci emergency shift parameter.
 tol(12)= 0.0000E+00. . . . increment of psci emergency shift parameter.


 *** State averaging informations: ***


 MCSCF calculation performed for  2 DRTs.

 DRT  first state   no.of aver.states   weights
  1   ground state          2             0.333 0.333
  2   ground state          1             0.333

 The number of hmc(*) eigenvalues and eigenvectors calculated each iteration per DRT:
 DRT.   no.of eigenv.(=ncol)
    1        3
    2        2

 Orbitals included in invariant subspaces:
   symmetry   orbital   mask
       2       1( 12)    40
       2       2( 13)    40
       2       3( 14)    40
       6       1( 36)    40
       6       2( 37)    40
       6       3( 38)    40

 npath(*) options:
  2:  orbital-state coupling terms will be included beginning on iteration ncoupl=  1
  3:  print intermediate timing information.
  9:  suppress the drt listing.
 10:  suppress the hmc(*) eigenvector listing.
 12:  diagonalize the hmc(*) matrix iteratively.
        nunitv= 1 nciitr=25 mxvadd=20 nvcimx=20
       rtolci(*),wnorm=     1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 0.0000E+00
   noldv =   0  0
 13:  get initial orbitals from the formatted file, mocoef.
 17:  print the final natural orbitals and occupations.
 19:  transform the virtual orbitals to diagonalize qvv(*).
 21:  write out the one- and two- electron density for further use (files:mcd1fl, mcd2fl).
 23:  use the old integral transformation.
 30:  Compute mcscf (transition) density matrices


   ******  DRT info section  ******


 Informations for the DRT no.  1

 DRT file header:
 mdrt2_title                                                                     
 Molecular symmetry group:   b1u 
 Total number of electrons:   16
 Spin multiplicity:            1
 Number of active orbitals:    6
 Number of active electrons:   2
 Total number of CSFs:         9

 Informations for the DRT no.  2

 DRT file header:
 mdrt2_title                                                                     
 Molecular symmetry group:    ag 
 Total number of electrons:   16
 Spin multiplicity:            1
 Number of active orbitals:    6
 Number of active electrons:   2
 Total number of CSFs:        12
 

 faar:   0 active-active rotations allowed out of:   6 possible.


 Number of active-double rotations:         0
 Number of active-active rotations:         0
 Number of double-virtual rotations:       54
 Number of active-virtual rotations:        6
 lenbfsdef=                131071  lenbfs=                   227
  number of integrals per class 1:11 (cf adda 
 class  1 (pq|rs):         #         123
 class  2 (pq|ri):         #           0
 class  3 (pq|ia):         #        1143
 class  4 (pi|qa):         #        1962
 class  5 (pq|ra):         #         126
 class  6 (pq|ij)/(pi|qj): #         501
 class  7 (pq|ab):         #        2589
 class  8 (pa|qb):         #        4758
 class  9 p(bp,ai)         #         324
 class 10p(ai,jp):        #           0
 class 11p(ai,bj):        #        1671

 Size of orbital-Hessian matrix B:                     2016
 Size of the orbital-state Hessian matrix C:           1800
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:           3816


 Source of the initial MO coeficients:

 Input MO coefficient file: /mnt/gpfs01/home/cm/cmfp2/programs/Columbus/Test/rspada/TEST
 

               starting mcscf iteration...   1

 orbital-state coupling will be calculated this iteration.

 *** Starting integral transformation ***

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     8, naopsy(1) =    11, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 64959, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 65000

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore=  13101216

 inoutp: segmentation information:
 in-core transformation space,   avcinc =  12935361
 address segment size,           sizesg =  12783875
 number of in-core blocks,       nincbk =       106
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:      26874 transformed 1/r12    array elements were written in       5 records.


 mosort: allocated sort2 space, avc2is=    12963069 available sort2 space, avcisx=    12963321

 trial vectors are generated internally.

 trial vector  1 is unit matrix column     1
 ciiter=   5 noldhv=  3 noldv=  3

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -77.7009677595     -110.9976151812        0.0000000000        0.0000010000
    2       -77.2472911438     -110.5439385656        0.0000000000        0.0000010000
    3       -76.9092311666     -110.2058785884        0.0000000000        0.0100000000

 trial vectors are generated internally.

 trial vector  1 is unit matrix column     1
 ciiter=   6 noldhv=  9 noldv=  9

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -78.0701098997     -111.3667573215        0.0000000697        0.0000010000
    2       -77.5161086483     -110.8127560701        0.0059774936        0.0100000000
 
  tol(10)=  0.000000000000000E+000  eshsci=  1.824183244659260E-002
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 Total number of micro iterations:    7

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0=-0.99670670 pnorm= 6.2476E-02 rznorm= 3.0442E-06 rpnorm= 6.3342E-06 noldr=  7 nnewr=  7 nolds=  5 nnews=  5
 

 fdd(*) eigenvalues. symmetry block  1
   -22.524813   -2.113714   -1.212268

 qvv(*) eigenvalues. symmetry block  1
     0.392109    1.129958    1.389502    1.986319    3.151983    3.668730    4.962510    5.385463
 i,qaaresolved                     1 -0.537937738581087     
 i,qaaresolved                     2   1.30284298995033     
 i,qaaresolved                     3   2.46792649338800     

 qvv(*) eigenvalues. symmetry block  2
     3.918245

 fdd(*) eigenvalues. symmetry block  3
    -1.320776

 qvv(*) eigenvalues. symmetry block  3
     0.446307    1.190266    1.678057    3.466645    3.785468    5.581471

 qvv(*) eigenvalues. symmetry block  4
    -5.780438   -2.533152

 fdd(*) eigenvalues. symmetry block  5
   -22.522003   -1.606246

 qvv(*) eigenvalues. symmetry block  5
     0.467532    0.907332    1.519688    1.905153    2.420368    3.682212    4.456410    5.215994    6.213109
 i,qaaresolved                     1  0.189372406609963     
 i,qaaresolved                     2   1.49195190025650     
 i,qaaresolved                     3   3.60101265696969     

 qvv(*) eigenvalues. symmetry block  6
     4.392950

 fdd(*) eigenvalues. symmetry block  7
    -1.039608

 qvv(*) eigenvalues. symmetry block  7
     0.596505    1.828958    2.473164    3.432065    4.852400    6.718832

 qvv(*) eigenvalues. symmetry block  8
   -10.483556   -6.553838
 *** warning *** small active-orbital occupation. i=  1 nocc= 1.7863E-04

 restrt: restart information saved on the restart file (unit= 13).

 not all mcscf convergence criteria are satisfied.
 iter=    1 emc=    -77.6727896010 demc= 7.7673E+01 wnorm= 1.4593E-01 knorm= 8.0933E-02 apxde= 1.9608E-03    *not conv.*     

               starting mcscf iteration...   2

 orbital-state coupling will be calculated this iteration.

 *** Starting integral transformation ***

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     8, naopsy(1) =    11, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 64959, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 65000

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore=  13101216

 inoutp: segmentation information:
 in-core transformation space,   avcinc =  12935361
 address segment size,           sizesg =  12783875
 number of in-core blocks,       nincbk =       106
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:      26874 transformed 1/r12    array elements were written in       5 records.


 mosort: allocated sort2 space, avc2is=    12963069 available sort2 space, avcisx=    12963321

   4 trial vectors read from nvfile (unit= 29).
 ciiter=   3 noldhv=  3 noldv=  3

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -77.7047532251     -111.0014006469        0.0000000000        0.0000100000
    2       -77.2511221439     -110.5477695657        0.0000000000        0.0000100000
    3       -76.9127937280     -110.2094411498        0.0000000000        0.0100000000

   3 trial vectors read from nvfile (unit= 29).
 ciiter=   5 noldhv=  9 noldv=  9

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -78.0685998382     -111.3652472600        0.0000000987        0.0000100000
    2       -77.5236730801     -110.8203205019        0.0010248049        0.0100000000
 
  tol(10)=  0.000000000000000E+000  eshsci=  3.108205324194669E-004
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 Total number of micro iterations:   10

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.87057617 pnorm= 3.9944E-02 rznorm= 9.4025E-08 rpnorm= 3.4405E-07 noldr=  9 nnewr=  9 nolds=  8 nnews=  8
 

 fdd(*) eigenvalues. symmetry block  1
   -22.482788   -2.088462   -1.193365

 qvv(*) eigenvalues. symmetry block  1
     0.396515    1.137876    1.400253    1.996238    3.168411    3.681804    4.972762    5.395854
 i,qaaresolved                     1 -0.513530058312383     
 i,qaaresolved                     2   1.32302361008778     
 i,qaaresolved                     3   2.48935647203448     

 qvv(*) eigenvalues. symmetry block  2
     3.920109

 fdd(*) eigenvalues. symmetry block  3
    -1.308656

 qvv(*) eigenvalues. symmetry block  3
     0.451615    1.202182    1.693687    3.473501    3.798380    5.597780

 qvv(*) eigenvalues. symmetry block  4
    -5.771528   -2.530130

 fdd(*) eigenvalues. symmetry block  5
   -22.479978   -1.596184

 qvv(*) eigenvalues. symmetry block  5
     0.470010    0.911838    1.531308    1.919513    2.433170    3.694032    4.473031    5.225339    6.227286
 i,qaaresolved                     1  0.202515866318247     
 i,qaaresolved                     2   1.50972189996741     
 i,qaaresolved                     3   3.60801471391773     

 qvv(*) eigenvalues. symmetry block  6
     4.404827

 fdd(*) eigenvalues. symmetry block  7
    -1.029389

 qvv(*) eigenvalues. symmetry block  7
     0.598161    1.842901    2.480858    3.435073    4.865257    6.735120

 qvv(*) eigenvalues. symmetry block  8
   -10.485495   -6.552592
 *** warning *** small active-orbital occupation. i=  1 nocc= 1.8279E-04

 restrt: restart information saved on the restart file (unit= 13).

 not all mcscf convergence criteria are satisfied.
 iter=    2 emc=    -77.6748250691 demc= 2.0355E-03 wnorm= 2.4866E-03 knorm= 4.9164E-01 apxde= 2.9633E-04    *not conv.*     

               starting mcscf iteration...   3

 orbital-state coupling will be calculated this iteration.

 *** Starting integral transformation ***

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     8, naopsy(1) =    11, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 64959, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 65000

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore=  13101216

 inoutp: segmentation information:
 in-core transformation space,   avcinc =  12935361
 address segment size,           sizesg =  12783875
 number of in-core blocks,       nincbk =       106
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:      26874 transformed 1/r12    array elements were written in       5 records.


 mosort: allocated sort2 space, avc2is=    12963069 available sort2 space, avcisx=    12963321

   4 trial vectors read from nvfile (unit= 29).
 ciiter=   3 noldhv=  3 noldv=  3

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -77.7054260308     -111.0020734525        0.0000000000        0.0000010000
    2       -77.2512849145     -110.5479323363        0.0000000000        0.0000010000
    3       -76.9157954965     -110.2124429182        0.0000000000        0.0100000000

   3 trial vectors read from nvfile (unit= 29).
 ciiter=   5 noldhv=  9 noldv=  9

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -78.0688237134     -111.3654711352        0.0000001109        0.0000010000
    2       -77.5245923533     -110.8212397750        0.0030845930        0.0100000000
 
  tol(10)=  0.000000000000000E+000  eshsci=  6.142638458234449E-005
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 Total number of micro iterations:    8

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.97860459 pnorm= 5.9879E-03 rznorm= 1.5415E-07 rpnorm= 5.4015E-07 noldr=  8 nnewr=  8 nolds=  7 nnews=  7
 

 fdd(*) eigenvalues. symmetry block  1
   -22.482133   -2.088016   -1.193021

 qvv(*) eigenvalues. symmetry block  1
     0.396602    1.138036    1.400467    1.996446    3.168692    3.682056    4.972989    5.396074
 i,qaaresolved                     1 -0.513251857814924     
 i,qaaresolved                     2   1.32298077257956     
 i,qaaresolved                     3   2.55685359064236     

 qvv(*) eigenvalues. symmetry block  2
     3.853345

 fdd(*) eigenvalues. symmetry block  3
    -1.308387

 qvv(*) eigenvalues. symmetry block  3
     0.451728    1.202376    1.693961    3.473663    3.798625    5.598075

 qvv(*) eigenvalues. symmetry block  4
    -5.771429   -2.530049

 fdd(*) eigenvalues. symmetry block  5
   -22.479324   -1.595947

 qvv(*) eigenvalues. symmetry block  5
     0.470080    0.911949    1.531524    1.919791    2.433406    3.694256    4.473345    5.225555    6.227549
 i,qaaresolved                     1  0.202823776073670     
 i,qaaresolved                     2   1.50946342176024     
 i,qaaresolved                     3   3.77294765839097     

 qvv(*) eigenvalues. symmetry block  6
     4.240721

 fdd(*) eigenvalues. symmetry block  7
    -1.029177

 qvv(*) eigenvalues. symmetry block  7
     0.598223    1.843174    2.480996    3.435172    4.865506    6.735414

 qvv(*) eigenvalues. symmetry block  8
   -10.485485   -6.552562
 *** warning *** small active-orbital occupation. i=  1 nocc= 2.6120E-04

 restrt: restart information saved on the restart file (unit= 13).

 not all mcscf convergence criteria are satisfied.
 iter=    3 emc=    -77.6751782196 demc= 3.5315E-04 wnorm= 4.9141E-04 knorm= 2.0575E-01 apxde= 2.9483E-05    *not conv.*     

               starting mcscf iteration...   4

 orbital-state coupling will be calculated this iteration.

 *** Starting integral transformation ***

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     8, naopsy(1) =    11, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 64959, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 65000

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore=  13101216

 inoutp: segmentation information:
 in-core transformation space,   avcinc =  12935361
 address segment size,           sizesg =  12783875
 number of in-core blocks,       nincbk =       106
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:      26874 transformed 1/r12    array elements were written in       5 records.


 mosort: allocated sort2 space, avc2is=    12963069 available sort2 space, avcisx=    12963321

   4 trial vectors read from nvfile (unit= 29).
 ciiter=   3 noldhv=  3 noldv=  3

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -77.7055259503     -111.0021733721        0.0000000000        0.0000010000
    2       -77.2512836054     -110.5479310272        0.0000000000        0.0000010000
    3       -76.9158872681     -110.2125346899        0.0000000000        0.0100000000

   3 trial vectors read from nvfile (unit= 29).
 ciiter=   5 noldhv=  9 noldv=  9

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -78.0688197169     -111.3654671387        0.0000001807        0.0000010000
    2       -77.5246840076     -110.8213314294        0.0033550438        0.0100000000
 
  tol(10)=  0.000000000000000E+000  eshsci=  1.034905094999457E-005
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 Total number of micro iterations:    7

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0=-0.99975720 pnorm= 7.0958E-04 rznorm= 2.9114E-07 rpnorm= 3.8750E-07 noldr=  7 nnewr=  7 nolds=  6 nnews=  6
 

 fdd(*) eigenvalues. symmetry block  1
   -22.481979   -2.087928   -1.192952

 qvv(*) eigenvalues. symmetry block  1
     0.396619    1.138066    1.400504    1.996489    3.168746    3.682106    4.973031    5.396114
 i,qaaresolved                     1 -0.513171914894110     
 i,qaaresolved                     2   1.32297226448125     
 i,qaaresolved                     3   2.57681529465950     

 qvv(*) eigenvalues. symmetry block  2
     3.833541

 fdd(*) eigenvalues. symmetry block  3
    -1.308338

 qvv(*) eigenvalues. symmetry block  3
     0.451747    1.202411    1.694013    3.473691    3.798667    5.598133

 qvv(*) eigenvalues. symmetry block  4
    -5.771394   -2.530040

 fdd(*) eigenvalues. symmetry block  5
   -22.479170   -1.595898

 qvv(*) eigenvalues. symmetry block  5
     0.470092    0.911970    1.531560    1.919851    2.433454    3.694295    4.473410    5.225595    6.227600
 i,qaaresolved                     1  0.202876740171812     
 i,qaaresolved                     2   1.50961793102460     
 i,qaaresolved                     3   3.91992269393954     

 qvv(*) eigenvalues. symmetry block  6
     4.093723

 fdd(*) eigenvalues. symmetry block  7
    -1.029137

 qvv(*) eigenvalues. symmetry block  7
     0.598233    1.843226    2.481020    3.435188    4.865552    6.735471

 qvv(*) eigenvalues. symmetry block  8
   -10.485479   -6.552553
 *** warning *** small active-orbital occupation. i=  1 nocc= 2.5873E-04

 restrt: restart information saved on the restart file (unit= 13).

 not all mcscf convergence criteria are satisfied.
 iter=    4 emc=    -77.6752097576 demc= 3.1538E-05 wnorm= 8.2792E-05 knorm= 2.2035E-02 apxde= 2.7973E-07    *not conv.*     

               starting mcscf iteration...   5

 orbital-state coupling will be calculated this iteration.

 *** Starting integral transformation ***

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     8, naopsy(1) =    11, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 64959, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 65000

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore=  13101216

 inoutp: segmentation information:
 in-core transformation space,   avcinc =  12935361
 address segment size,           sizesg =  12783875
 number of in-core blocks,       nincbk =       106
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:      26874 transformed 1/r12    array elements were written in       5 records.


 mosort: allocated sort2 space, avc2is=    12963069 available sort2 space, avcisx=    12963321

   4 trial vectors read from nvfile (unit= 29).
 ciiter=   3 noldhv=  3 noldv=  3

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -77.7055318459     -111.0021792677        0.0000000000        0.0000010000
    2       -77.2512809581     -110.5479283798        0.0000000000        0.0000010000
    3       -76.9158601006     -110.2125075223        0.0000000000        0.0100000000

   3 trial vectors read from nvfile (unit= 29).
 ciiter=   5 noldhv=  9 noldv=  9

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -78.0688173185     -111.3654647402        0.0000001775        0.0000010000
    2       -77.5246846382     -110.8213320600        0.0038518000        0.0100000000
 
  tol(10)=  0.000000000000000E+000  eshsci=  1.415490118947684E-007
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 Total number of micro iterations:    4

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.99999995 pnorm= 8.4901E-06 rznorm= 6.2245E-07 rpnorm= 9.8288E-07 noldr=  4 nnewr=  4 nolds=  1 nnews=  1
 

 fdd(*) eigenvalues. symmetry block  1
   -22.481960   -2.087919   -1.192944

 qvv(*) eigenvalues. symmetry block  1
     0.396620    1.138070    1.400507    1.996494    3.168752    3.682111    4.973035    5.396117
 i,qaaresolved                     1 -0.513160554124617     
 i,qaaresolved                     2   1.32297344144230     
 i,qaaresolved                     3   2.57793254029535     

 qvv(*) eigenvalues. symmetry block  2
     3.832439

 fdd(*) eigenvalues. symmetry block  3
    -1.308334

 qvv(*) eigenvalues. symmetry block  3
     0.451748    1.202414    1.694018    3.473693    3.798671    5.598139

 qvv(*) eigenvalues. symmetry block  4
    -5.771387   -2.530039

 fdd(*) eigenvalues. symmetry block  5
   -22.479150   -1.595892

 qvv(*) eigenvalues. symmetry block  5
     0.470093    0.911973    1.531563    1.919858    2.433460    3.694299    4.473417    5.225599    6.227605
 i,qaaresolved                     1  0.202882210789531     
 i,qaaresolved                     2   1.50964984984494     
 i,qaaresolved                     3   3.93710573298991     

 qvv(*) eigenvalues. symmetry block  6
     4.076524

 fdd(*) eigenvalues. symmetry block  7
    -1.029133

 qvv(*) eigenvalues. symmetry block  7
     0.598233    1.843231    2.481022    3.435189    4.865557    6.735477

 qvv(*) eigenvalues. symmetry block  8
   -10.485478   -6.552552
 *** warning *** small active-orbital occupation. i=  1 nocc= 2.5718E-04

 restrt: restart information saved on the restart file (unit= 13).

 not all mcscf convergence criteria are satisfied.
 iter=    5 emc=    -77.6752100408 demc= 2.8326E-07 wnorm= 1.1324E-06 knorm= 3.2735E-04 apxde= 6.0009E-11    *not conv.*     

               starting mcscf iteration...   6

 orbital-state coupling will be calculated this iteration.

 *** Starting integral transformation ***

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     8, naopsy(1) =    11, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 64959, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 65000

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore=  13101216

 inoutp: segmentation information:
 in-core transformation space,   avcinc =  12935361
 address segment size,           sizesg =  12783875
 number of in-core blocks,       nincbk =       106
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:      26874 transformed 1/r12    array elements were written in       5 records.


 mosort: allocated sort2 space, avc2is=    12963069 available sort2 space, avcisx=    12963321

   4 trial vectors read from nvfile (unit= 29).
 ciiter=   3 noldhv=  3 noldv=  3

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -77.7055319237     -111.0021793454        0.0000000000        0.0000010000
    2       -77.2512809123     -110.5479283340        0.0000000000        0.0000010000
    3       -76.9158595736     -110.2125069954        0.0000000000        0.0100000000

   3 trial vectors read from nvfile (unit= 29).
 ciiter=   5 noldhv=  9 noldv=  9

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -78.0688172867     -111.3654647085        0.0000001695        0.0000010000
    2       -77.5246850840     -110.8213325057        0.0038867878        0.0100000000
 
  tol(10)=  0.000000000000000E+000  eshsci=  8.575832761875615E-008
 performing all-state projection
 Total number of micro iterations:    1

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 1.00000000 pnorm= 0.0000E+00 rznorm= 4.7171E-07 rpnorm= 4.1822E-09 noldr=  1 nnewr=  1 nolds=  0 nnews=  0
 

 fdd(*) eigenvalues. symmetry block  1
   -22.481960   -2.087918   -1.192944

 qvv(*) eigenvalues. symmetry block  1
     0.396620    1.138070    1.400507    1.996494    3.168752    3.682111    4.973035    5.396117
 i,qaaresolved                     1 -0.513160452753155     
 i,qaaresolved                     2   1.32297342690599     
 i,qaaresolved                     3   2.57793981638468     

 qvv(*) eigenvalues. symmetry block  2
     3.832432

 fdd(*) eigenvalues. symmetry block  3
    -1.308334

 qvv(*) eigenvalues. symmetry block  3
     0.451748    1.202414    1.694018    3.473693    3.798671    5.598139

 qvv(*) eigenvalues. symmetry block  4
    -5.771387   -2.530039

 fdd(*) eigenvalues. symmetry block  5
   -22.479150   -1.595892

 qvv(*) eigenvalues. symmetry block  5
     0.470093    0.911973    1.531563    1.919858    2.433460    3.694299    4.473417    5.225599    6.227605
 i,qaaresolved                     1  0.202882247130742     
 i,qaaresolved                     2   1.50965028102348     
 i,qaaresolved                     3   3.93736258157594     

 qvv(*) eigenvalues. symmetry block  6
     4.076267

 fdd(*) eigenvalues. symmetry block  7
    -1.029133

 qvv(*) eigenvalues. symmetry block  7
     0.598233    1.843231    2.481022    3.435189    4.865557    6.735477

 qvv(*) eigenvalues. symmetry block  8
   -10.485478   -6.552552
 *** warning *** small active-orbital occupation. i=  1 nocc= 2.5716E-04

 restrt: restart information saved on the restart file (unit= 13).

 all mcscf convergence criteria are satisfied.

 final mcscf convergence values:
 iter=    6 emc=    -77.6752100409 demc= 6.0552E-11 wnorm= 6.8607E-07 knorm= 2.2058E-08 apxde= 6.6272E-15    *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 wt 0.333 total energy=      -77.705531924, rel. (eV)=   9.885502
   DRT #1 state # 2 wt 0.333 total energy=      -77.251280912, rel. (eV)=  22.246306
   DRT #2 state # 1 wt 0.333 total energy=      -78.068817287, rel. (eV)=   0.000000
   ------------------------------------------------------------


 MO-coefficient print-out skipped (no flag 32)
 They may be found in the MOCOEF directory.

          natural orbitals of the final iteration,block  1    -  Ag 
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     2.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO    9        MO   10        MO   11
  occ(*)=     0.00000000     0.00000000     0.00000000

          natural orbitals of the final iteration,block  2    -  B3u
               MO    1        MO    2        MO    3        MO    4
  occ(*)=     1.19907189     0.10444987     0.00188443     0.00000000

          natural orbitals of the final iteration,block  3    -  B2u
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7
  occ(*)=     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

          natural orbitals of the final iteration,block  4    -  B1g
               MO    1        MO    2
  occ(*)=     0.00000000     0.00000000

          natural orbitals of the final iteration,block  5    -  B1u
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO    9        MO   10        MO   11
  occ(*)=     0.00000000     0.00000000     0.00000000

          natural orbitals of the final iteration,block  6    -  B2g
               MO    1        MO    2        MO    3        MO    4
  occ(*)=     0.45836289     0.23597376     0.00025716     0.00000000

          natural orbitals of the final iteration,block  7    -  B3g
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7
  occ(*)=     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

          natural orbitals of the final iteration,block  8    -  Au 
               MO    1        MO    2
  occ(*)=     0.00000000     0.00000000
 d1(*), fmc(*), and qmc(*) written to the 1-particle density matrix file.
        340 d2(*) elements written to the 2-particle density matrix file: mcd2fl                                                      
 Computing the requested mcscf (transition) density matrices (flag 30)
 Reading mcdenin ...
 Number of density matrices (ndens):                     2
 Number of unique bra states (ndbra):                     2
 *** Starting rdft_grd
 qind: F
  
  The spin-density matrix is not printed for singlet states 
  
 One particle density matrix:

              < 1|E_s| 1>           block   1

              < 1|E_s| 1>           block   2

                mo   1         mo   2         mo   3
   mo   1     0.98834976
   mo   2    -0.02126405     0.00569059
   mo   3    -0.07399304     0.00057296     0.00595965

              < 1|E_s| 1>           block   3

              < 1|E_s| 1>           block   4

              < 1|E_s| 1>           block   5

              < 1|E_s| 1>           block   6

                mo   4         mo   5         mo   6
   mo   4     0.99220874
   mo   5     0.04445483     0.00737243
   mo   6     0.01162659     0.00112483     0.00041883

              < 1|E_s| 1>           block   7

              < 1|E_s| 1>           block   8
  
  
  The spin-density matrix is not printed for singlet states 
  
 One particle density matrix:

              < 2|E_s| 2>           block   1

              < 2|E_s| 2>           block   2

                mo   1         mo   2         mo   3
   mo   1     0.69567086
   mo   2     0.06350118     0.30391991
   mo   3    -0.00789773    -0.00968285     0.00040923

              < 2|E_s| 2>           block   3

              < 2|E_s| 2>           block   4

              < 2|E_s| 2>           block   5

              < 2|E_s| 2>           block   6

                mo   4         mo   5         mo   6
   mo   4     0.30342385
   mo   5    -0.06170227     0.69590227
   mo   6    -0.00966548     0.01665593     0.00067388

              < 2|E_s| 2>           block   7

              < 2|E_s| 2>           block   8
  
 *** rdft_grd finished
 d1(*) and spind(*) written to the 1-particle density matrix file.
        298 d2(*) elements written to the 2-particle density matrix file: mcsd2fl.drt1.st01                                           
 d1(*) and spind(*) written to the 1-particle density matrix file.
        298 d2(*) elements written to the 2-particle density matrix file: mcsd2fl.drt1.st02                                           
  
  The spin-density properties are not printed for singlet states 

          state spec. NOs: DRT 1, State  1
 *** warning *** small active-orbital occupation. i=  1 nocc= 2.1202E-04
 *** warning *** small active-orbital occupation. i=  1 nocc= 2.1202E-04

          block  1
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     2.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO    9        MO   10        MO   11
  occ(*)=     0.00000000     0.00000000     0.00000000

          block  2
               MO    1        MO    2        MO    3        MO    4
  occ(*)=     0.99434824     0.00543973     0.00021202     0.00000000

          block  3
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7
  occ(*)=     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

          block  4
               MO    1        MO    2
  occ(*)=     0.00000000     0.00000000

          block  5
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO    9        MO   10        MO   11
  occ(*)=     0.00000000     0.00000000     0.00000000

          block  6
               MO    1        MO    2        MO    3        MO    4
  occ(*)=     0.99434824     0.00543973     0.00021202     0.00000000

          block  7
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7
  occ(*)=     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

          block  8
               MO    1        MO    2
  occ(*)=     0.00000000     0.00000000
  
  The spin-density properties are not printed for singlet states 

          state spec. NOs: DRT 1, State  2
 *** warning *** small active-orbital occupation. i=  1 nocc= 5.0118E-05
 *** warning *** small active-orbital occupation. i=  1 nocc= 5.0118E-05

          block  1
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     2.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO    9        MO   10        MO   11
  occ(*)=     0.00000000     0.00000000     0.00000000

          block  2
               MO    1        MO    2        MO    3        MO    4
  occ(*)=     0.70582998     0.29411990     0.00005012     0.00000000

          block  3
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7
  occ(*)=     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

          block  4
               MO    1        MO    2
  occ(*)=     0.00000000     0.00000000

          block  5
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO    9        MO   10        MO   11
  occ(*)=     0.00000000     0.00000000     0.00000000

          block  6
               MO    1        MO    2        MO    3        MO    4
  occ(*)=     0.70582998     0.29411990     0.00005012     0.00000000

          block  7
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7
  occ(*)=     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

          block  8
               MO    1        MO    2
  occ(*)=     0.00000000     0.00000000


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        Ag  partial gross atomic populations
   ao class       1Ag        2Ag        3Ag        4Ag        5Ag        6Ag 
    C1_ s       1.999302   1.478936   0.011194   0.000000   0.000000   0.000000
    C1_ p       0.000107   0.183883   1.381039   0.000000   0.000000   0.000000
    C1_ d       0.000067   0.015453   0.016069   0.000000   0.000000   0.000000
    H1_ s       0.000679   0.294765   0.575203   0.000000   0.000000   0.000000
    H1_ p      -0.000156   0.026963   0.016495   0.000000   0.000000   0.000000
 
   ao class       7Ag        8Ag        9Ag       10Ag       11Ag 

                        B3u partial gross atomic populations
   ao class       1B3u       2B3u       3B3u       4B3u
    C1_ p       1.166776   0.101259   0.000084   0.000000
    C1_ d       0.020740   0.003488   0.001741   0.000000
    H1_ p       0.011556  -0.000297   0.000059   0.000000

                        B2u partial gross atomic populations
   ao class       1B2u       2B2u       3B2u       4B2u       5B2u       6B2u
    C1_ p       1.165733   0.000000   0.000000   0.000000   0.000000   0.000000
    C1_ d       0.000142   0.000000   0.000000   0.000000   0.000000   0.000000
    H1_ s       0.815930   0.000000   0.000000   0.000000   0.000000   0.000000
    H1_ p       0.018195   0.000000   0.000000   0.000000   0.000000   0.000000
 
   ao class       7B2u

                        B1g partial gross atomic populations
   ao class       1B1g       2B1g

                        B1u partial gross atomic populations
   ao class       1B1u       2B1u       3B1u       4B1u       5B1u       6B1u
    C1_ s       1.999950   0.762497   0.000000   0.000000   0.000000   0.000000
    C1_ p       0.000368   0.387374   0.000000   0.000000   0.000000   0.000000
    C1_ d      -0.000182   0.007791   0.000000   0.000000   0.000000   0.000000
    H1_ s       0.000180   0.805462   0.000000   0.000000   0.000000   0.000000
    H1_ p      -0.000316   0.036876   0.000000   0.000000   0.000000   0.000000
 
   ao class       7B1u       8B1u       9B1u      10B1u      11B1u

                        B2g partial gross atomic populations
   ao class       1B2g       2B2g       3B2g       4B2g
    C1_ p       0.442909   0.237783   0.000006   0.000000
    C1_ d       0.006773  -0.000483   0.000223   0.000000
    H1_ p       0.008680  -0.001326   0.000029   0.000000

                        B3g partial gross atomic populations
   ao class       1B3g       2B3g       3B3g       4B3g       5B3g       6B3g
    C1_ p       0.816159   0.000000   0.000000   0.000000   0.000000   0.000000
    C1_ d       0.052081   0.000000   0.000000   0.000000   0.000000   0.000000
    H1_ s       1.120003   0.000000   0.000000   0.000000   0.000000   0.000000
    H1_ p       0.011757   0.000000   0.000000   0.000000   0.000000   0.000000
 
   ao class       7B3g

                        Au  partial gross atomic populations
   ao class       1Au        2Au 


                        gross atomic populations
     ao           C1_        H1_
      s         6.251879   3.612222
      p         5.883480   0.128515
      d         0.123904   0.000000
    total      12.259263   3.740737
 

 Trace:   16.00000000

 Note:
 For the regular analysis, the Trace means the total number of electrons.  
 If the spin-density matrix was considered, the  Trace means the 2*S.  
  
 Mulliken population for:
DRT 1, state 01


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        Ag  partial gross atomic populations
   ao class       1Ag        2Ag        3Ag        4Ag        5Ag        6Ag 
    C1_ s       1.999302   1.478936   0.011194   0.000000   0.000000   0.000000
    C1_ p       0.000107   0.183883   1.381039   0.000000   0.000000   0.000000
    C1_ d       0.000067   0.015453   0.016069   0.000000   0.000000   0.000000
    H1_ s       0.000679   0.294765   0.575203   0.000000   0.000000   0.000000
    H1_ p      -0.000156   0.026963   0.016495   0.000000   0.000000   0.000000
 
   ao class       7Ag        8Ag        9Ag       10Ag       11Ag 

                        B3u partial gross atomic populations
   ao class       1B3u       2B3u       3B3u       4B3u
    C1_ p       0.946930   0.005457   0.000007   0.000000
    C1_ d       0.040021  -0.000004   0.000198   0.000000
    H1_ p       0.007398  -0.000013   0.000007   0.000000

                        B2u partial gross atomic populations
   ao class       1B2u       2B2u       3B2u       4B2u       5B2u       6B2u
    C1_ p       1.165733   0.000000   0.000000   0.000000   0.000000   0.000000
    C1_ d       0.000142   0.000000   0.000000   0.000000   0.000000   0.000000
    H1_ s       0.815930   0.000000   0.000000   0.000000   0.000000   0.000000
    H1_ p       0.018195   0.000000   0.000000   0.000000   0.000000   0.000000
 
   ao class       7B2u

                        B1g partial gross atomic populations
   ao class       1B1g       2B1g

                        B1u partial gross atomic populations
   ao class       1B1u       2B1u       3B1u       4B1u       5B1u       6B1u
    C1_ s       1.999950   0.762497   0.000000   0.000000   0.000000   0.000000
    C1_ p       0.000368   0.387374   0.000000   0.000000   0.000000   0.000000
    C1_ d      -0.000182   0.007791   0.000000   0.000000   0.000000   0.000000
    H1_ s       0.000180   0.805462   0.000000   0.000000   0.000000   0.000000
    H1_ p      -0.000316   0.036876   0.000000   0.000000   0.000000   0.000000
 
   ao class       7B1u       8B1u       9B1u      10B1u      11B1u

                        B2g partial gross atomic populations
   ao class       1B2g       2B2g       3B2g       4B2g
    C1_ p       0.958076   0.005444   0.000007   0.000000
    C1_ d       0.017917   0.000010   0.000182   0.000000
    H1_ p       0.018355  -0.000014   0.000023   0.000000

                        B3g partial gross atomic populations
   ao class       1B3g       2B3g       3B3g       4B3g       5B3g       6B3g
    C1_ p       0.816159   0.000000   0.000000   0.000000   0.000000   0.000000
    C1_ d       0.052081   0.000000   0.000000   0.000000   0.000000   0.000000
    H1_ s       1.120003   0.000000   0.000000   0.000000   0.000000   0.000000
    H1_ p       0.011757   0.000000   0.000000   0.000000   0.000000   0.000000
 
   ao class       7B3g

                        Au  partial gross atomic populations
   ao class       1Au        2Au 


                        gross atomic populations
     ao           C1_        H1_
      s         6.251879   3.612222
      p         5.850582   0.135571
      d         0.149746   0.000000
    total      12.252207   3.747793
 

 Trace:   16.00000000

 Note:
 For the regular analysis, the Trace means the total number of electrons.  
 If the spin-density matrix was considered, the  Trace means the 2*S.  
  
 Mulliken population for:
DRT 1, state 02


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        Ag  partial gross atomic populations
   ao class       1Ag        2Ag        3Ag        4Ag        5Ag        6Ag 
    C1_ s       1.999302   1.478936   0.011194   0.000000   0.000000   0.000000
    C1_ p       0.000107   0.183883   1.381039   0.000000   0.000000   0.000000
    C1_ d       0.000067   0.015453   0.016069   0.000000   0.000000   0.000000
    H1_ s       0.000679   0.294765   0.575203   0.000000   0.000000   0.000000
    H1_ p      -0.000156   0.026963   0.016495   0.000000   0.000000   0.000000
 
   ao class       7Ag        8Ag        9Ag       10Ag       11Ag 

                        B3u partial gross atomic populations
   ao class       1B3u       2B3u       3B3u       4B3u
    C1_ p       0.694699   0.282650   0.000002   0.000000
    C1_ d       0.004808   0.011998   0.000046   0.000000
    H1_ p       0.006323  -0.000528   0.000002   0.000000

                        B2u partial gross atomic populations
   ao class       1B2u       2B2u       3B2u       4B2u       5B2u       6B2u
    C1_ p       1.165733   0.000000   0.000000   0.000000   0.000000   0.000000
    C1_ d       0.000142   0.000000   0.000000   0.000000   0.000000   0.000000
    H1_ s       0.815930   0.000000   0.000000   0.000000   0.000000   0.000000
    H1_ p       0.018195   0.000000   0.000000   0.000000   0.000000   0.000000
 
   ao class       7B2u

                        B1g partial gross atomic populations
   ao class       1B1g       2B1g

                        B1u partial gross atomic populations
   ao class       1B1u       2B1u       3B1u       4B1u       5B1u       6B1u
    C1_ s       1.999950   0.762497   0.000000   0.000000   0.000000   0.000000
    C1_ p       0.000368   0.387374   0.000000   0.000000   0.000000   0.000000
    C1_ d      -0.000182   0.007791   0.000000   0.000000   0.000000   0.000000
    H1_ s       0.000180   0.805462   0.000000   0.000000   0.000000   0.000000
    H1_ p      -0.000316   0.036876   0.000000   0.000000   0.000000   0.000000
 
   ao class       7B1u       8B1u       9B1u      10B1u      11B1u

                        B2g partial gross atomic populations
   ao class       1B2g       2B2g       3B2g       4B2g
    C1_ p       0.702737   0.293635   0.000000   0.000000
    C1_ d       0.003589  -0.002377   0.000044   0.000000
    H1_ p      -0.000496   0.002862   0.000006   0.000000

                        B3g partial gross atomic populations
   ao class       1B3g       2B3g       3B3g       4B3g       5B3g       6B3g
    C1_ p       0.816159   0.000000   0.000000   0.000000   0.000000   0.000000
    C1_ d       0.052081   0.000000   0.000000   0.000000   0.000000   0.000000
    H1_ s       1.120003   0.000000   0.000000   0.000000   0.000000   0.000000
    H1_ p       0.011757   0.000000   0.000000   0.000000   0.000000   0.000000
 
   ao class       7B3g

                        Au  partial gross atomic populations
   ao class       1Au        2Au 


                        gross atomic populations
     ao           C1_        H1_
      s         6.251879   3.612222
      p         5.908385   0.117982
      d         0.109531   0.000000
    total      12.269795   3.730205
 

 Trace:   16.00000000

 Note:
 For the regular analysis, the Trace means the total number of electrons.  
 If the spin-density matrix was considered, the  Trace means the 2*S.  
  
 !timer: mcscf                           cpu_time=     0.078 walltime=     0.201
 *** cpu_time / walltime =      0.391
 bummer (warning):timer: cpu_time << walltime.  If possible, increase core memory. event =1
