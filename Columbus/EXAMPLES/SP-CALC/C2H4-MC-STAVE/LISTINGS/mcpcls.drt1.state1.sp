

     ******************************************
     **    PROGRAM:              MCPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 original author: Daniel Robertson, FSU
 later revisions: Ron Shepard, ANL;
                  Michal Dallos, University Vienna



 This Version of Program mcpc is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



   ******  File header section  ******

 Headers form the restart file:
    Hermit Integral Program : SIFS version  hochtor2        Thu Jun  5 14:25:31 2003
    mdrt2_title                                                                     
    mdrt2_title                                                                     


   ******  DRT info section  ******

 Informations for the DRT no.  1
 Header form the DRT file: 
    mdrt2_title                                                                     
 Molecular symmetry group:   sym5
 Total number of electrons:   16
 Spin multiplicity:            1
 Number of active orbitals:    6
 Number of active electrons:   2
 Total number of CSFs:         9

   ***  Informations from the DRT number:   1


 Symmetry orbital summary:
 Symm.blocks:         1     2     3     4     5     6     7     8
 Symm.labels:         ag   b3u   b2u   b1g   b1u   b2g   b3g    au 

 List of doubly occupied orbitals:
  1 ag   2 ag   3 ag   1b2u   1b1u   2b1u   1b3g 

 List of active orbitals:
  1b3u   2b3u   3b3u   1b2g   2b2g   3b2g 

 Informations for the DRT no.  2
 Header form the DRT file: 
    mdrt2_title                                                                     
 Molecular symmetry group:    ag 
 Total number of electrons:   16
 Spin multiplicity:            1
 Number of active orbitals:    6
 Number of active electrons:   2
 Total number of CSFs:        12

   ***  Informations from the DRT number:   2


 Symmetry orbital summary:
 Symm.blocks:         1     2     3     4     5     6     7     8
 Symm.labels:         ag   b3u   b2u   b1g   b1u   b2g   b3g    au 

 List of doubly occupied orbitals:
  1 ag   2 ag   3 ag   1b2u   1b1u   2b1u   1b3g 

 List of active orbitals:
  1b3u   2b3u   3b3u   1b2g   2b2g   3b2g 


   ******  MCSCF convergence information:  ******

 MCSCF convergence criteria were satisfied.

 mcscf energy=   -77.6752100409    nuclear repulsion=    33.2966474218
 demc=             0.0000000001    wnorm=                 0.0000006851
 knorm=            0.0000000221    apxde=                 0.0000000000


 MCSCF calculation performmed for   2 symmetries.

 State averaging:
 No,  ssym, navst, wavst
  1   b1u     2   0.3333 0.3333
  2    ag     1   0.3333

 Input the DRT No of interest: [  1]:
In the DRT No.: 1 there are  2 states.

 Which one to take? [  1]:
 The CSFs for the state No  1 of the symmetry  ag  will be printed
 according to the following print options :

 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 0) end.

 input menu number [  0]: csfs will be printed based on coefficient magnitudes.

 input the coefficient threshold (end with 0.) [ 0.0000]:
 List of active orbitals:
  1b3u   2b3u   3b3u   1b2g   2b2g   3b2g 

   csf       coeff       coeff**2    step(*)
  -----  ------------  ------------  ------------
      1  0.9931094488  0.9862663773  100200
      7 -0.0749426144  0.0056163955  001200
      5 -0.0723388701  0.0052329121  010020
      2  0.0444212127  0.0019732441  100020
      4 -0.0180546362  0.0003259699  010200
      9 -0.0133035969  0.0001769857  001002
      8  0.0128945414  0.0001662692  001020
      6 -0.0114763953  0.0001317076  010002
      3  0.0104946939  0.0001101386  100002

 input the coefficient threshold (end with 0.) [ 0.0000]:
 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 0) end.

 input menu number [  0]: