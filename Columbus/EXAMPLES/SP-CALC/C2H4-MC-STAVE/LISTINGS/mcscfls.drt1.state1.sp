

     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 This program allows the csf mixing coefficient and orbital expansion coefficient
 optimization using the graphical unitary group approach and the exponential
 operator mcscf method.
 references:  r. shepard and j. simons,                    ' int. j. quantum chem. symp. 14, 211 (1980).
              r. shepard, i. shavitt, and j. simons, j. chem. phys. 76, 543 (1982).
              r. shepard in "ab initio methods in quantum chemistry ii" advances in chemical
                  physics 69, edited by k. p. lawley (wiley, new york, 1987) pp. 63-200.
 Original autor: Ron Shepard, ANL
 Later revisions: Michal Dallos, University Vienna

 This Version of Program MCSCF is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.4.0.2     **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 Workspace allocation information:
         8000000 of real*8 words (   61.04 MB) of work space has been allocated.

 user input information:

 ======== echo of the mcscf input ========
 ------------------------------------------------------------------------
  &input
  NPATH =  1, 2, 3, 9,10,12,13,17,19,21,23,
  
  
  
  
  
  NSTATE =0, NUNITV=1 ,
  NITER=1,NMITER=0
  TOL(1)= .00000001,
  NCOUPL =   1,
  FCIORB=
          2   1 40, 2   2 40, 2   3 40, 6   1 40, 6   2 40,
  6   3 40,
 /&end
 ------------------------------------------------------------------------


 ***  Integral file informations  ***


 input integral file : aoints                                                      

 Integral file header information:
 Hermit Integral Program : SIFS version  hochtor2        Thu Jun  5 14:25:31 2003

 Core type energy values:
 energy( 1)=  3.329664742176E+01, ietype=   -1,    core energy of type: Nuc.Rep.
 total ao core energy =   33.296647422


   ******  Basis set informations:  ******

 Number of irreps:                  8
 Total number of basis functions:  48

 irrep no.              1    2    3    4    5    6    7    8
 irrep label           Ag   B3u  B2u  B1g  B1u  B2g  B3g  Au 
 no. of bas.fcions.    11    4    7    2   11    4    7    2


 ***  MCSCF optimization procedure parmeters:  ***


 maximum number of mcscf iterations:        niter=     1

 maximum number of psci micro-iterations:   nmiter=    0
 maximum r,s subspace dimension allowed:    nvrsmx=   30

 tol(1)=  1.0000E-08. . . . delta-emc convergence criterion.
 tol(2)=  1.0000E-04. . . . wnorm convergence criterion.
 tol(3)=  1.0000E-04. . . . knorm convergence criterion.
 tol(4)=  1.0000E-08. . . . apxde convergence criterion.
 tol(5)=  1.0000E-04. . . . small diagonal matrix element tolerance.
 tol(6)=  1.0000E-06. . . . minimum ci-psci residual norm.
 tol(7)=  1.0000E-05. . . . maximum ci-psci residual norm.
 tol(8)=  1.0000E+00. . . . maximum abs(k(xy)) allowed.
 tol(9)=  1.0000E+00. . . . wnorm coupling tolerance.
 tol(10)= 0.0000E+00. . . . maximum psci emergency shift parameter.
 tol(11)= 0.0000E+00. . . . minimum psci emergency shift parameter.
 tol(12)= 0.0000E+00. . . . increment of psci emergency shift parameter.


 *** State averaging informations: ***


 MCSCF calculation performed for  1 DRT.

 DRT  first state   no.of aver.states   weights
  1   ground state          1             1.000

 The number of hmc(*) eigenvalues and eigenvectors calculated each iteration per symmetry:
 Symm.   no.of eigenv.(=ncol)
  Ag         2

 orbital coefficients are optimized for the ground state (nstate=0).

 Orbitals included in invariant subspaces:
   symmetry   orbital   mask
       2       1( 12)    40
       2       2( 13)    40
       2       3( 14)    40
       6       1( 36)    40
       6       2( 37)    40
       6       3( 38)    40

 npath(*) options:
  2:  orbital-state coupling terms will be included beginning on iteration ncoupl=  1
  3:  print intermediate timing information.
  9:  suppress the drt listing.
 10:  suppress the hmc(*) eigenvector listing.
 12:  diagonalize the hmc(*) matrix iteratively.
        nunitv= 1 nciitr=25 mxvadd=20 nvcimx=20
       rtolci(*),wnorm=     1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 0.0000E+00
   noldv =   0
 13:  get initial orbitals from the formatted file, mocoef.
 17:  print the final natural orbitals and occupations.
 19:  transform the virtual orbitals to diagonalize qvv(*).
 21:  write out the one- and two- electron density for further use (files:mcd1fl, mcd2fl).
 23:  use the old integral transformation.


   ******  DRT info section  ******


 Informations for the DRT no.  1

 DRT file header:
 mdrt2_title                                                                     
 Molecular symmetry group:   b1u 
 Total number of electrons:   16
 Spin multiplicity:            1
 Number of active orbitals:    6
 Number of active electrons:   2
 Total number of CSFs:         9

 !timer: initialization                  user+sys=     0.000 walltime=     0.000

 faar:   0 active-active rotations allowed out of:   6 possible.


 Number of active-double rotations:      0
 Number of active-active rotations:      0
 Number of double-virtual rotations:    54
 Number of active-virtual rotations:     6

 Size of orbital-Hessian matrix B:                     2016
 Size of the orbital-state Hessian matrix C:            540
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:           2556



   ****** Integral transformation section ******


 number of blocks to be transformed in-core is 106
 number of blocks to be transformed out of core is    0

 in-core ao list requires    1 segments.
 out of core ao list requires    0 segments.
 each segment has length ****** working precision words.

               ao integral sort statistics
 length of records:      1610
 number of buckets:   1
 scratch file used is da1:
 amount of core needed for in-core arrays:  2824

 twoao_o processed      92764 two electron ao integrals.

      92764 ao integrals were written into   87 records

 srtinc_o read in      93892 integrals and wrote out      93892 integrals.

 Source of the initial MO coeficients:

 Input MO coefficient file: mocoef                                                      


               starting mcscf iteration...   1
 !timer:                                 user+sys=     0.080 walltime=     1.000

 orbital-state coupling will be calculated this iteration.

 number of transformed integrals put on file:     26982
 number of records written:    10
 !timer: 2-e transformation              user+sys=     0.090 walltime=     0.000

 Size of orbital-Hessian matrix B:                     2016
 Size of the orbital-state Hessian matrix C:            540
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:           2556

 !timer: mosrt1                          user+sys=     0.010 walltime=     0.000
 !timer: mosrt2                          user+sys=     0.000 walltime=     0.000
 !timer: mosort                          user+sys=     0.010 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000

 trial vectors are generated internally.

 trial vector  1 is unit matrix column     1
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.010 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 ciiter=   6 noldhv=  2 noldv=  2
 !timer: hmc(*) diagonalization          user+sys=     0.010 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -77.7055319238     -111.0021793456        0.0000000000        0.0000010000
    2       -77.2512809123     -110.5479283341        0.0000000000        0.0100000000
 !timer: hmcvec                          user+sys=     0.010 walltime=     0.000
 !timer: cvdu                            user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: mqva                            user+sys=     0.000 walltime=     0.000
 !timer: mfva                            user+sys=     0.000 walltime=     0.000
 !timer: mfvd                            user+sys=     0.000 walltime=     0.000
 !timer: mfad                            user+sys=     0.000 walltime=     0.000
 !timer: mqad                            user+sys=     0.000 walltime=     0.000
 !timer: mqaa                            user+sys=     0.000 walltime=     0.000
 !timer: mfaa                            user+sys=     0.000 walltime=     0.000
 !timer: badad                           user+sys=     0.000 walltime=     0.000
 !timer: badadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvava                           user+sys=     0.000 walltime=     0.000
 !timer: bvavaf                          user+sys=     0.000 walltime=     0.000
 !timer: bvavd                           user+sys=     0.000 walltime=     0.000
 !timer: bvavdf                          user+sys=     0.000 walltime=     0.000
 !timer: bvdvd                           user+sys=     0.000 walltime=     0.000
 !timer: hbcon                           user+sys=     0.000 walltime=     0.000

  tol(10)=  0. eshsci=  0.012492582
 *** psci convergence not reached ***
 Total number of micro iterations:    0

 ***  micro: final psci convergence values:  ***
    imxov=  0 z0= 0.00000000 pnorm= 0.0000E+00 rznorm= 0.0000E+00 rpnorm= 0.0000E+00 noldr=  0 nnewr=  1 nolds=  0 nnews=  0
 *** warning *** small z0.

 !timer: Direct Bxr time contribution    user+sys=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  user+sys=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    user+sys=     0.000 walltime=     0.000
 !timer: solvek total                    user+sys=     0.000 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
   -22.577993   -2.124558   -1.221677

 qvv(*) eigenvalues. symmetry block  1
     0.393155    1.129013    1.391392    1.985917    3.153027    3.667152    4.961187    5.381818
 i,qaaresolved 1 -0.487250594
 i,qaaresolved 2  1.34528455
 i,qaaresolved 3  2.5401853

 qvv(*) eigenvalues. symmetry block  2
     3.831477

 fdd(*) eigenvalues. symmetry block  3
    -1.328035

 qvv(*) eigenvalues. symmetry block  3
     0.444997    1.193913    1.671224    3.468234    3.785442    5.572369

 qvv(*) eigenvalues. symmetry block  4
     2.223575    3.912047

 fdd(*) eigenvalues. symmetry block  5
   -22.575260   -1.616993

 qvv(*) eigenvalues. symmetry block  5
     0.468534    0.909085    1.509508    1.896159    2.416060    3.681314    4.448621    5.214213    6.208624
 i,qaaresolved 1  0.0443040058
 i,qaaresolved 2  1.56085347
 i,qaaresolved 3  3.91858408

 qvv(*) eigenvalues. symmetry block  6
     4.074808

 fdd(*) eigenvalues. symmetry block  7
    -1.043195

 qvv(*) eigenvalues. symmetry block  7
     0.595058    1.819791    2.475566    3.435364    4.847048    6.715549

 qvv(*) eigenvalues. symmetry block  8
     2.541218    4.689933
 *** warning *** small active-orbital occupation. i=  1 nocc= 2.1202E-04
 *** warning *** small active-orbital occupation. i=  1 nocc= 2.1202E-04
 !timer: motran                          user+sys=     0.000 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 user+sys=     0.120 walltime=     0.000

 all mcscf convergence criteria are not satisfied.
 iter=    1 emc=  -77.7055319238 demc= 7.7706E+01 wnorm= 9.9941E-02 knorm= 0.0000E+00 apxde=-6.2463E-03    *not converged* 

 final mcscf convergence values:
 iter=    1 emc=  -77.7055319238 demc= 7.7706E+01 wnorm= 9.9941E-02 knorm= 0.0000E+00 apxde=-6.2463E-03    *not converged* 




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 weight 1.000000 total energy=  -77.705531924
   ------------------------------------------------------------



          mcscf orbitals of the final iteration,  Ag  block   1

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
   1C1s       0.70789518    -0.02125812    -0.00419445     0.04430833     0.08307301    -0.18292772     0.48704798    -0.13481072
   2C1s       0.00581352     0.60895592    -0.04527383    -0.12265239     0.35863539    -0.63805014     2.25414431    -0.47025737
   3C1s      -0.00547862    -0.16589846     0.04523067    -1.31481302     0.17135522     0.87512771    -3.58613701     1.23194586
   4C1pz      0.00009656    -0.19215706     0.60524810    -0.19758765    -0.62248467     0.39670575     0.34528030     0.00446522
   5C1pz     -0.00034704     0.06340722    -0.14182451    -0.43686892     1.30687341    -0.26120556    -0.82020586     0.58755204
   6C1d0      0.00005798     0.00626140    -0.00649825    -0.00111976     0.01568525    -0.00887776     0.03693396    -0.00141581
   7C1d2+    -0.00013011    -0.00045070    -0.00596111     0.00709739     0.01743193     0.10416193     0.03786626    -0.31728394
   8H1s      -0.00029124     0.18611069     0.28707364     0.01914449    -0.39766694    -0.82103730    -0.05394613    -0.90606010
   9H1s       0.00123292    -0.08687924    -0.08065989     0.92002316     0.16984638     0.55627963     0.85471213     0.29957990
  10H1py      0.00026532    -0.01400472    -0.01377760    -0.00951912    -0.00544873    -0.08674980    -0.14849809    -0.00318511
  11H1pz      0.00005891    -0.01122019     0.00216074    -0.01003212     0.03168852    -0.02487705    -0.05390124    -0.17152526

               MO    9        MO   10        MO   11
   1C1s      -0.34971450    -0.18329942     0.38539758
   2C1s      -1.41194045    -0.62456947     1.81165451
   3C1s       1.10255577     1.03905435    -0.29708401
   4C1pz      0.37263706     0.56080416     0.81853354
   5C1pz     -0.86083433    -0.06780179    -0.14021489
   6C1d0      0.14899851     0.21481558    -0.07376167
   7C1d2+    -0.11166505     0.10298365    -0.30122434
   8H1s       0.02447667    -0.06193176    -1.24170895
   9H1s      -0.01491009    -0.20760172     0.37429927
  10H1py     -0.16434376     0.43109355     0.62560333
  11H1pz      0.28379097    -0.40700884     0.54466797

          mcscf orbitals of the final iteration,  B3u block   2

               MO   12        MO   13        MO   14        MO   15
  12C1px      0.55554859    -1.02179412     0.39636809    -0.27254125
  13C1px      0.04336479     1.08506687    -0.27439335    -0.09046099
  14C1d1+    -0.02801880     0.10915493     0.57900662    -0.27863586
  15H1px      0.01283761    -0.00908218     0.03954878     0.58469177

          mcscf orbitals of the final iteration,  B2u block   3

               MO   16        MO   17        MO   18        MO   19        MO   20        MO   21        MO   22
  16C1py      0.47042675    -0.33298454    -0.09388374     0.88527301     0.03173955    -0.12953450     1.61867682
  17C1py     -0.10000397    -0.61887841     0.44593272    -1.80014931    -0.62745284     0.26171177    -0.11109381
  18C1d1-    -0.00317254     0.00605824    -0.15277815    -0.23917063    -0.35471794    -0.45718470     0.78834029
  19H1s       0.35459512     0.05570864    -0.87738569    -0.02167612     0.71217866     0.37110562    -1.46892220
  20H1s      -0.12374527     1.14843080     0.66300896     1.11237275    -0.21015399    -0.46196206     0.29500536
  21H1py     -0.01160227    -0.01036981    -0.01506969    -0.14307120     0.34445877    -0.08179831     0.69655815
  22H1pz     -0.01152725    -0.00685101    -0.06786469    -0.06700664    -0.20784379     0.45500240     0.42185114

          mcscf orbitals of the final iteration,  B1g block   4

               MO   23        MO   24
  23C1d2-     0.48444497    -0.59271976
  24H1px      0.19466564     0.55631469

          mcscf orbitals of the final iteration,  B1u block   5

               MO   25        MO   26        MO   27        MO   28        MO   29        MO   30        MO   31        MO   32
  25C1s       0.70855286    -0.01340781     0.04957164     0.06712526    -0.00265026     0.11792502     0.63736523     0.19741054
  26C1s       0.00733862     0.47983603    -0.17797654     0.02399408    -0.43511677     0.46080648     2.75322050     0.94911157
  27C1s      -0.00863951    -0.10291268    -1.46014848    -3.53706913    -0.27286196     0.40020848    -7.69384757     0.15726372
  28C1pz     -0.00041518     0.23643329    -0.11392983     0.23623729     0.26276425     0.99063643    -0.11844411    -0.50693321
  29C1pz      0.00125221    -0.04570261    -0.26761926     3.17972852     0.18443628    -2.58411140     2.19910640    -0.95389396
  30C1d0      0.00015912    -0.00357693     0.00085945    -0.01103128     0.02996459     0.08147676    -0.06643666     0.02893278
  31C1d2+    -0.00033814    -0.00302584     0.01225314     0.01809291    -0.09247722     0.05553872    -0.04158997     0.16300339
  32H1s      -0.00059218     0.33394434     0.03304199    -0.18414621     0.90536724    -0.14168963     0.41288788     0.46566414
  33H1s       0.00101711    -0.13267896     1.06271345    -0.60904854    -0.89405861     0.99460089     0.07369108    -0.03778478
  34H1py      0.00045712    -0.02080505    -0.01753765    -0.01815173     0.09108746    -0.13358612    -0.09325182    -0.06654849
  35H1pz      0.00025877    -0.00930595    -0.00528388    -0.00288432     0.05891838     0.00124859    -0.08301979     0.43804277

               MO   33        MO   34        MO   35
  25C1s      -0.15261112     0.44546851    -0.05184853
  26C1s      -0.38768118     2.08504832     0.12585198
  27C1s       4.46681380    -0.86057892     2.94599375
  28C1pz     -1.32117745    -0.42221342     1.15502950
  29C1pz     -1.63861053     0.60881006    -1.14096970
  30C1d0      0.23222081     0.26577348    -0.15668465
  31C1d2+    -0.24406196    -0.00407239    -0.57292515
  32H1s      -0.46012827    -0.44423080    -1.73308582
  33H1s       0.52651807    -0.08155227     0.60429476
  34H1py     -0.25663262     0.62928046     0.43108333
  35H1pz      0.06431219    -0.14738179     0.69593616

          mcscf orbitals of the final iteration,  B2g block   6

               MO   36        MO   37        MO   38        MO   39
  36C1px      0.54721127    -1.13513676    -0.40746041     0.03501410
  37C1px      0.46705047     1.67057417    -0.36222421     0.32643906
  38C1d1+     0.02218513    -0.08183134     1.00284503     0.56819168
  39H1px      0.01667897    -0.02662566     0.13533991    -0.59088408

          mcscf orbitals of the final iteration,  B3g block   7

               MO   40        MO   41        MO   42        MO   43        MO   44        MO   45        MO   46
  40C1py      0.44984201    -0.29584348    -1.02821525     0.25254009     0.10916849     1.19420809     0.86200186
  41C1py     -0.08398036    -1.49710054     2.57439494    -3.10809629    -2.17097898     0.00472679     0.25520307
  42C1d1-     0.03737217    -0.02945877     0.12792420     0.25520872    -0.05252463    -0.29906002     1.94531629
  43H1s       0.43044374    -0.02923889     0.21514072     1.12190930     0.91815107    -0.35986859    -1.99134944
  44H1s      -0.09564102     1.68862659    -1.38213846     0.34516886     0.18813390    -0.29172929     0.79500990
  45H1py     -0.00747523    -0.01458727     0.12306619    -0.03082209     0.34835783     0.39398100     0.77370936
  46H1pz     -0.01274156    -0.01608321     0.13342660     0.14373917    -0.33825644     0.49060157     0.08832708

          mcscf orbitals of the final iteration,  Au  block   8

               MO   47        MO   48
  47C1d2-     0.39770309    -0.87759076
  48H1px      0.30607526     0.54314409

          natural orbitals of the final iteration, block  1

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8

  occ(*)=     2.00000000     2.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

   1C1s       0.70790180    -0.02095529    -0.00458304     0.04269246     0.08970836    -0.17898313     0.48711645    -0.13607395
   2C1s       0.00563383     0.60976504    -0.03264493    -0.13044036     0.38328491    -0.62255275     2.25423727    -0.47430330
   3C1s      -0.00543184    -0.16680173     0.04178283    -1.30638845     0.12836104     0.86725647    -3.58848815     1.24183750
   4C1pz      0.00011434    -0.20465770     0.60113627    -0.19939784    -0.63206537     0.38008569     0.35007927     0.00779411
   5C1pz     -0.00035672     0.06633238    -0.14048016    -0.43585035     1.30732076    -0.23053315    -0.82762548     0.58496353
   6C1d0      0.00005653     0.00639473    -0.00636710    -0.00095884     0.01633551    -0.00887462     0.03703536    -0.00131284
   7C1d2+    -0.00012958    -0.00032711    -0.00596918     0.00680577     0.01665587     0.10587669     0.03717487    -0.31900692
   8H1s      -0.00036598     0.18012190     0.29086856     0.02297254    -0.37157213    -0.82790625    -0.05509721    -0.91535381
   9H1s       0.00126430    -0.08518878    -0.08244279     0.91579874     0.15840221     0.56214170     0.85692741     0.30406171
  10H1py      0.00027043    -0.01371613    -0.01406483    -0.00966936    -0.00459434    -0.08621828    -0.14677052    -0.00385516
  11H1pz      0.00006213    -0.01126253     0.00192778    -0.01019360     0.03137406    -0.02296297    -0.05349248    -0.16607607

               MO    9        MO   10        MO   11

  occ(*)=     0.00000000     0.00000000     0.00000000

   1C1s      -0.35074620    -0.18399422     0.38414354
   2C1s      -1.41540472    -0.62895256     1.80605630
   3C1s       1.10588217     1.04102635    -0.29009615
   4C1pz      0.37745593     0.55317707     0.81955683
   5C1pz     -0.86494529    -0.05807487    -0.13928770
   6C1d0      0.15073187     0.21397291    -0.07249081
   7C1d2+    -0.10831749     0.10458472    -0.29960988
   8H1s       0.02956060    -0.05790206    -1.23837857
   9H1s      -0.01923443    -0.20965660     0.37088171
  10H1py     -0.16104564     0.42881548     0.62850191
  11H1pz      0.28159843    -0.41305354     0.54308147

          natural orbitals of the final iteration, block  2

               MO    1        MO    2        MO    3        MO    4

  occ(*)=     0.99434824     0.00543973     0.00021202     0.00000000

  12C1px      0.54623619    -1.07685738     0.22761665    -0.27254125
  13C1px      0.04040278     1.11823896    -0.04951332    -0.09046099
  14C1d1+    -0.07350148    -0.00867869     0.58520999    -0.27863586
  15H1px      0.01004171    -0.01670698     0.03783434     0.58469177

          natural orbitals of the final iteration, block  3

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7

  occ(*)=     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

  16C1py      0.47042675    -0.34177621    -0.09654391     0.88776728     0.02096722    -0.14023946     1.61460002
  17C1py     -0.10000397    -0.60495815     0.43918558    -1.80765107    -0.61760666     0.27906837    -0.10563203
  18C1d1-    -0.00317254     0.00948533    -0.15650587    -0.23773740    -0.36830456    -0.45063697     0.78554342
  19H1s       0.35459512     0.06137277    -0.87281087    -0.02241228     0.72800594     0.36370570    -1.46549888
  20H1s      -0.12374527     1.13488125     0.66935682     1.12120130    -0.22547915    -0.46098463     0.28985360
  21H1py     -0.01160227    -0.00974334    -0.01596485    -0.14019015     0.34018530    -0.09377819     0.69772243
  22H1pz     -0.01152725    -0.00683087    -0.06686250    -0.06496706    -0.19722520     0.45852439     0.42361148

          natural orbitals of the final iteration, block  4

               MO    1        MO    2

  occ(*)=     0.00000000     0.00000000

  23C1d2-     0.48837409    -0.58948654
  24H1px      0.19096350     0.55759634

          natural orbitals of the final iteration, block  5

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8

  occ(*)=     2.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

  25C1s       0.70855608    -0.01323660     0.04810282     0.06632838    -0.00309397     0.10782092     0.63994341     0.19875698
  26C1s       0.00722268     0.47983779    -0.18756172     0.01286886    -0.43202625     0.43251676     2.76488604     0.95258517
  27C1s      -0.00861464    -0.10291477    -1.44812236    -3.51873979    -0.18179739     0.53230644    -7.66852164     0.18245964
  28C1pz     -0.00047231     0.23643318    -0.11205539     0.25763436     0.29566610     0.99558775    -0.11499451    -0.51569189
  29C1pz      0.00126326    -0.04570231    -0.27074393     3.14508947     0.05624342    -2.64415769     2.16123382    -0.97277712
  30C1d0      0.00015998    -0.00357690     0.00137787    -0.01005503     0.03210678     0.07894367    -0.06296626     0.03376020
  31C1d2+    -0.00033741    -0.00302593     0.01208872     0.01780176    -0.09254840     0.05789574    -0.04307664     0.16387744
  32H1s      -0.00067287     0.33394418     0.03899317    -0.17240942     0.89851110    -0.17665777     0.40510476     0.47210119
  33H1s       0.00104917    -0.13267871     1.05697988    -0.61185760    -0.85876985     1.02660577     0.09035284    -0.03770006
  34H1py      0.00046215    -0.02080494    -0.01754182    -0.01803515     0.08744737    -0.13272417    -0.09411757    -0.06640576
  35H1pz      0.00026102    -0.00930589    -0.00541943    -0.00205023     0.05682087    -0.00048768    -0.08111288     0.43477697

               MO    9        MO   10        MO   11

  occ(*)=     0.00000000     0.00000000     0.00000000

  25C1s      -0.15121259     0.44458876    -0.05096936
  26C1s      -0.38162258     2.07473541     0.13145270
  27C1s       4.49621408    -0.91139227     2.96451583
  28C1pz     -1.31860939    -0.40181694     1.14506610
  29C1pz     -1.64606878     0.63299939    -1.14470512
  30C1d0      0.23711591     0.26315376    -0.15516935
  31C1d2+    -0.24300227     0.00042383    -0.57279848
  32H1s      -0.46942782    -0.43552070    -1.73433750
  33H1s       0.52830827    -0.09066329     0.60405162
  34H1py     -0.24862345     0.63201921     0.43262616
  35H1pz      0.05796315    -0.15297390     0.69773493

          natural orbitals of the final iteration, block  6

               MO    1        MO    2        MO    3        MO    4

  occ(*)=     0.99434824     0.00543973     0.00021202     0.00000000

  36C1px      0.49074909    -1.19862849    -0.27650438     0.03501410
  37C1px      0.53748001     1.59418951    -0.55662881     0.32643906
  38C1d1+     0.03024748     0.03373499     1.00540228     0.56819168
  39H1px      0.01705093    -0.01153850     0.13740499    -0.59088408

          natural orbitals of the final iteration, block  7

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7

  occ(*)=     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

  40C1py      0.44984201    -0.30634377    -1.03407670     0.25253053     0.10870548     1.18684538     0.86155254
  41C1py     -0.08398036    -1.47822428     2.58740600    -3.10617082    -2.17051437     0.01530306     0.26057964
  42C1d1-     0.03737217    -0.02780385     0.12868737     0.25925165    -0.05451210    -0.29897497     1.94471401
  43H1s       0.43044374    -0.02571142     0.21871547     1.11945637     0.91892666    -0.35623955    -1.99268346
  44H1s      -0.09564102     1.67775582    -1.39490856     0.34468255     0.18809897    -0.29789041     0.79365523
  45H1py     -0.00747523    -0.01419969     0.12052930    -0.02924916     0.34739212     0.39452122     0.77433567
  46H1pz     -0.01274156    -0.01581919     0.12985542     0.14332374    -0.33880269     0.49136277     0.08804697

          natural orbitals of the final iteration, block  8

               MO    1        MO    2

  occ(*)=     0.00000000     0.00000000

  47C1d2-     0.40332607    -0.87502078
  48H1px      0.30258381     0.54509688
 d1(*), fmc(*), and qmc(*) written to the 1-particle density matrix file.
        298 d2(*) elements written to the 2-particle density matrix file.
 !timer: writing the mc density files requser+sys=     0.000 walltime=     0.000


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!


                        Ag  partial gross atomic populations
   ao class       1Ag        2Ag        3Ag        4Ag        5Ag        6Ag 
    C1_ s       1.999328   1.475992   0.014111   0.000000   0.000000   0.000000
    C1_ p       0.000106   0.206675   1.358249   0.000000   0.000000   0.000000
    C1_ d       0.000066   0.015925   0.015599   0.000000   0.000000   0.000000
    H1_ s       0.000660   0.274888   0.595100   0.000000   0.000000   0.000000
    H1_ p      -0.000159   0.026520   0.016941   0.000000   0.000000   0.000000

   ao class       7Ag        8Ag        9Ag       10Ag       11Ag 

                        B3u partial gross atomic populations
   ao class       1B3u       2B3u       3B3u       4B3u
    C1_ p       0.946930   0.005457   0.000007   0.000000
    C1_ d       0.040021  -0.000004   0.000198   0.000000
    H1_ p       0.007398  -0.000013   0.000007   0.000000

                        B2u partial gross atomic populations
   ao class       1B2u       2B2u       3B2u       4B2u       5B2u       6B2u
    C1_ p       1.165733   0.000000   0.000000   0.000000   0.000000   0.000000
    C1_ d       0.000142   0.000000   0.000000   0.000000   0.000000   0.000000
    H1_ s       0.815930   0.000000   0.000000   0.000000   0.000000   0.000000
    H1_ p       0.018195   0.000000   0.000000   0.000000   0.000000   0.000000

   ao class       7B2u

                        B1g partial gross atomic populations
   ao class       1B1g       2B1g

                        B1u partial gross atomic populations
   ao class       1B1u       2B1u       3B1u       4B1u       5B1u       6B1u
    C1_ s       1.999985   0.762463   0.000000   0.000000   0.000000   0.000000
    C1_ p       0.000356   0.387386   0.000000   0.000000   0.000000   0.000000
    C1_ d      -0.000183   0.007792   0.000000   0.000000   0.000000   0.000000
    H1_ s       0.000162   0.805481   0.000000   0.000000   0.000000   0.000000
    H1_ p      -0.000319   0.036879   0.000000   0.000000   0.000000   0.000000

   ao class       7B1u       8B1u       9B1u      10B1u      11B1u

                        B2g partial gross atomic populations
   ao class       1B2g       2B2g       3B2g       4B2g
    C1_ p       0.958076   0.005444   0.000007   0.000000
    C1_ d       0.017917   0.000010   0.000182   0.000000
    H1_ p       0.018355  -0.000014   0.000023   0.000000

                        B3g partial gross atomic populations
   ao class       1B3g       2B3g       3B3g       4B3g       5B3g       6B3g
    C1_ p       0.816159   0.000000   0.000000   0.000000   0.000000   0.000000
    C1_ d       0.052081   0.000000   0.000000   0.000000   0.000000   0.000000
    H1_ s       1.120003   0.000000   0.000000   0.000000   0.000000   0.000000
    H1_ p       0.011757   0.000000   0.000000   0.000000   0.000000   0.000000

   ao class       7B3g

                        Au  partial gross atomic populations
   ao class       1Au        2Au 


                        gross atomic populations
     ao           C1_        H1_
      s         6.251879   3.612222
      p         5.850582   0.135571
      d         0.149746   0.000000
    total      12.252207   3.747793


 Total number of electrons:   16.00000000

 !timer: mcscf                           user+sys=     0.210 walltime=     1.000
