 total ao core energy =   33.296647422
 MCSCF calculation performed for  2 DRTs.

 DRT  first state   no.of aver. states   weights
  1   ground state          2             0.333 0.333
  2   ground state          1             0.333

 DRT file header:
 mdrt2_title                                                                     
 Molecular symmetry group:   b1u 
 Total number of electrons:    16
 Spin multiplicity:            1
 Number of active orbitals:    6
 Number of active electrons:   2
 Total number of CSFs:         9

 DRT file header:
 mdrt2_title                                                                     
 Molecular symmetry group:    ag 
 Total number of electrons:    16
 Spin multiplicity:            1
 Number of active orbitals:    6
 Number of active electrons:   2
 Total number of CSFs:        12

 Number of active-double rotations:      0
 Number of active-active rotations:      0
 Number of double-virtual rotations:    54
 Number of active-virtual rotations:     6

 iter=    1 emc=  -77.6727896010 demc= 7.7673E+01 wnorm= 1.4593E-01 knorm= 8.0933E-02 apxde= 1.9608E-03    *not converged* 
 iter=    2 emc=  -77.6748250691 demc= 2.0355E-03 wnorm= 2.4871E-03 knorm= 4.9162E-01 apxde= 2.9632E-04    *not converged* 
 iter=    3 emc=  -77.6751782145 demc= 3.5315E-04 wnorm= 4.9097E-04 knorm= 2.0577E-01 apxde= 2.9489E-05    *not converged* 
 iter=    4 emc=  -77.6752097577 demc= 3.1543E-05 wnorm= 8.2809E-05 knorm= 2.2029E-02 apxde= 2.7959E-07    *not converged* 
 iter=    5 emc=  -77.6752100408 demc= 2.8311E-07 wnorm= 1.1323E-06 knorm= 3.2723E-04 apxde= 5.9968E-11    *not converged* 

 final mcscf convergence values:
 iter=    6 emc=  -77.6752100409 demc= 6.0552E-11 wnorm= 6.8506E-07 knorm= 2.2051E-08 apxde= 7.2801E-15    *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 weight 0.333333 total energy=  -77.705531924
   DRT #1 state # 2 weight 0.333333 total energy=  -77.251280912
   DRT #2 state # 1 weight 0.333333 total energy=  -78.068817287
   ------------------------------------------------------------


