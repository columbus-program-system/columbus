

     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 This program allows the csf mixing coefficient and orbital expansion coefficient
 optimization using the graphical unitary group approach and the exponential
 operator mcscf method.
 references:  r. shepard and j. simons, ' int. j. quantum chem. symp. 14, 211 (1980).
              r. shepard, i. shavitt, and j. simons, j. chem. phys. 76, 543 (1982).
              r. shepard in "ab initio methods in quantum chemistry ii" advances in chemical
                  physics 69, edited by k. p. lawley (wiley, new york, 1987) pp. 63-200.
 Original autor: Ron Shepard, ANL
 Later revisions: Michal Dallos, University Vienna

 This Version of Program MCSCF is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.4.0.2     **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 Workspace allocation information:
       301465600 of real*8 words ( 2300.00 MB) of work space has been allocated.

 user input information:

 ======== echo of the mcscf input ========
 ------------------------------------------------------------------------
  &input
   niter=100,
   nmiter=50,
   nciitr=300,
   tol(3)=1.e-4,
   tol(2)=1.e-4,
   tol(1)=1.e-8,
   NSTATE=0,
   npath=1,3,9,10,13,17,19,21,-11,12, 2,-13,30
   ncoupl=5,
   tol(9)=1.e-3,
   FCIORB=  2,2,40,2,3,40,2,4,40,2,5,40,2,6,40,2,7,40,2,8,40,2,9,40,4,2,40,4,3,40
   NAVST(1) = 1,
   WAVST(1,1)=1 ,
   NAVST(2) = 2,
   WAVST(2,1)=1 ,
   WAVST(2,2)=1 ,
   NAVST(3) = 1,
   WAVST(3,1)=1 ,
   NAVST(4) = 1,
   WAVST(4,1)=1 ,
  &end
 ------------------------------------------------------------------------


 ***  Integral file informations  ***


 input integral file : /fhgfs/global/lv70151/plasser1/1679223/WORK/aoints       
    

 Integral file header information:
 Hermit Integral Program : SIFS version  r24n13            16:31:16.794 06-Sep-12
 user-specified FREEZE(*) used in program tran.                                  
 MO-coefficients from mcscf.x                                                    
  total ao core energy = -101.311908370                                          
 dummy occupation 1.0 for active orbitals                                        
 transformed using mo_mult.x                                                     
 SIFS file created by program tran.      r24n13            16:31:47.239 06-Sep-12

 Core type energy values:
 energy( 1)=  5.461735143427E+02, ietype=   -1,    core energy of type: Nuc.Rep.
 total ao core energy =  546.173514343


   ******  Basis set information:  ******

 Number of irreps:                  4
 Total number of basis functions: 185

 irrep no.              1    2    3    4
 irrep label           A1   B1   B2   A2 
 no. of bas.fcions.    69   35   56   25


 ***  MCSCF optimization procedure parmeters:  ***


 maximum number of mcscf iterations:        niter=   100

 maximum number of psci micro-iterations:   nmiter=   50
 maximum r,s subspace dimension allowed:    nvrsmx=   30

 tol(1)=  1.0000E-08. . . . delta-emc convergence criterion.
 tol(2)=  1.0000E-04. . . . wnorm convergence criterion.
 tol(3)=  1.0000E-04. . . . knorm convergence criterion.
 tol(4)=  1.0000E-08. . . . apxde convergence criterion.
 tol(5)=  1.0000E-04. . . . small diagonal matrix element tolerance.
 tol(6)=  1.0000E-06. . . . minimum ci-psci residual norm.
 tol(7)=  1.0000E-05. . . . maximum ci-psci residual norm.
 tol(8)=  1.0000E+00. . . . maximum abs(k(xy)) allowed.
 tol(9)=  1.0000E-03. . . . wnorm coupling tolerance.
 tol(10)= 0.0000E+00. . . . maximum psci emergency shift parameter.
 tol(11)= 0.0000E+00. . . . minimum psci emergency shift parameter.
 tol(12)= 0.0000E+00. . . . increment of psci emergency shift parameter.


 *** State averaging informations: ***


 MCSCF calculation performed for  4 DRTs.

 DRT  first state   no.of aver.states   weights
  1   ground state          1             0.200
  2   ground state          2             0.200 0.200
  3   ground state          1             0.200
  4   ground state          1             0.200

 The number of hmc(*) eigenvalues and eigenvectors calculated each iteration per DRT:
 DRT.   no.of eigenv.(=ncol)
    1        2
    2        3
    3        2
    4        2

 Orbitals included in invariant subspaces:
   symmetry   orbital   mask
       2       2( 71)    40
       2       3( 72)    40
       2       4( 73)    40
       2       5( 74)    40
       2       6( 75)    40
       2       7( 76)    40
       2       8( 77)    40
       2       9( 78)    40
       4       2(162)    40
       4       3(163)    40

 npath(*) options:
  2:  orbital-state coupling terms will be included beginning on iteration ncoupl=  5
  3:  print intermediate timing information.
  9:  suppress the drt listing.
 10:  suppress the hmc(*) eigenvector listing.
 12:  diagonalize the hmc(*) matrix iteratively.
        nunitv= 1 nciitr=** mxvadd=20 nvcimx=20
       rtolci(*),wnorm=     1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 0.0000E+00
   noldv =   0  0  0  0
 17:  print the final natural orbitals and occupations.
 19:  transform the virtual orbitals to diagonalize qvv(*).
 21:  write out the one- and two- electron density for further use (files:mcd1fl, mcd2fl).
 30:  Compute mcscf (transition) density matrices


   ******  DRT info section  ******


 Informations for the DRT no.  1

 DRT file header:
  title                                                                          
 Molecular symmetry group:    b2 
 Total number of electrons:   56
 Spin multiplicity:            1
 Number of active orbitals:   10
 Number of active electrons:  10
 Total number of CSFs:      9408

 Informations for the DRT no.  2

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a1 
 Total number of electrons:   56
 Spin multiplicity:            1
 Number of active orbitals:   10
 Number of active electrons:  10
 Total number of CSFs:      9996

 Informations for the DRT no.  3

 DRT file header:
  title                                                                          
 Molecular symmetry group:    b1 
 Total number of electrons:   55
 Spin multiplicity:            2
 Number of active orbitals:   10
 Number of active electrons:   9
 Total number of CSFs:     14112

 Informations for the DRT no.  4

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a1 
 Total number of electrons:   56
 Spin multiplicity:            3
 Number of active orbitals:   10
 Number of active electrons:  10
 Total number of CSFs:     14916
 

 faar:   0 active-active rotations allowed out of:  29 possible.


 Number of active-double rotations:        10
 Number of active-active rotations:         0
 Number of double-virtual rotations:     1155
 Number of active-virtual rotations:      252
 lenbfsdef=                131071  lenbfs=                  6618
  number of integrals per class 1:11 (cf adda 
 class  1 (pq|rs):         #         916
 class  2 (pq|ri):         #         550
 class  3 (pq|ia):         #       63045
 class  4 (pi|qa):         #      114540
 class  5 (pq|ra):         #       13476
 class  6 (pq|ij)/(pi|qj): #       18960
 class  7 (pq|ab):         #      184031
 class  8 (pa|qb):         #      362134
 class  9 p(bp,ai)         #      291060
 class 10p(ai,jp):        #       11550
 class 11p(ai,bj):        #      697027

 Size of orbital-Hessian matrix B:                  1037152
 Size of the orbital-state Hessian matrix C:       82792476
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:       83829628


 Source of the initial MO coeficients:

  Unit matrix used as initial orbitals.
 

               starting mcscf iteration...   1

 orbital-state coupling will not be calculated this iteration.

 *** Starting integral transformation ***

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     4, naopsy(1) =    69, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 64959, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 65000

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore= 301272154

 inoutp: segmentation information:
 in-core transformation space,   avcinc = 301092379
 address segment size,           sizesg = 300940893
 number of in-core blocks,       nincbk =        19
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:    5591409 transformed 1/r12    array elements were written in    1025 records.

 !timer: 2-e transformation              cpu_time=     6.744 walltime=    11.230

 mosort: allocated sort2 space, avc2is=   301134007 available sort2 space, avcisx=   301134259
 !timer: mosort                          cpu_time=     0.308 walltime=     0.717

 trial vectors are generated internally.

 trial vector  1 is unit matrix column    41
 ciiter=  21 noldhv= 14 noldv= 14
 !timer: hmc(*) diagonalization          cpu_time=     2.698 walltime=     4.319

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -455.4787970696     -354.1668886997        0.0000004296        0.0000010000
    2      -455.3828123530     -354.0709039832        0.0050763782        0.0100000000
 !timer: hmcvec                          cpu_time=     2.701 walltime=     4.333

 trial vectors are generated internally.

 trial vector  1 is unit matrix column    11
 ciiter=  44 noldhv=  5 noldv=  5
 !timer: hmc(*) diagonalization          cpu_time=     7.115 walltime=    10.995

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -455.6503002137     -354.3383918438        0.0000004195        0.0000010000
    2      -455.4319310823     -354.1200227125        0.0000008135        0.0000010000
    3      -455.3815724805     -354.0696641107        0.0027906961        0.0100000000
 !timer: hmcvec                          cpu_time=     7.118 walltime=    11.010

 trial vectors are generated internally.

 trial vector  1 is unit matrix column    21
 ciiter=  20 noldhv= 15 noldv= 15
 !timer: hmc(*) diagonalization          cpu_time=     5.071 walltime=     7.901

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -455.3917566108     -354.0798482409        0.0000009697        0.0000010000
    2      -455.2930898530     -353.9811814832        0.0043130352        0.0100000000
 !timer: hmcvec                          cpu_time=     5.076 walltime=     7.916

 trial vectors are generated internally.

 trial vector  1 is unit matrix column    24
 !timer: hmcxv                           cpu_time=     0.329 walltime=     0.500
 ciiter=  21 noldhv= 13 noldv= 13
 !timer: hmc(*) diagonalization          cpu_time=     6.231 walltime=     9.585

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -455.5212474347     -354.2093390648        0.0000008053        0.0000010000
    2      -455.4777840982     -354.1658757283        0.0053225447        0.0100000000
 !timer: hmcvec                          cpu_time=     6.237 walltime=     9.601
 !timer: rdft                            cpu_time=     1.505 walltime=     1.587
 !timer: rdft                            cpu_time=     3.556 walltime=     3.654
 !timer: rdft                            cpu_time=     3.722 walltime=     3.850
 !timer: rdft                            cpu_time=     3.798 walltime=     3.955
 !timer: hbcon                           cpu_time=    14.258 walltime=    18.516
 
  tol(10)=  0.000000000000000E+000  eshsci=  6.268365018931329E-005
 Total number of micro iterations:    6

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.99999936 pnorm= 0.0000E+00 rznorm= 8.9100E-07 rpnorm= 0.0000E+00 noldr=  6 nnewr=  6 nolds=  0 nnews=  0
 
 !timer: solvek total                    cpu_time=     0.595 walltime=     1.526

 fdd(*) eigenvalues. symmetry block  1
    -2.672706   -2.443669   -2.368760   -2.150028   -1.930182   -1.715923   -1.560276   -1.498222   -1.332669   -1.228544
    -1.179160   -1.110364

 qvv(*) eigenvalues. symmetry block  1
     0.260045    0.325922    0.351922    0.464938    0.518306    0.648534    0.820009    0.846348    0.892596    1.080471
     1.174807    1.254723    1.300189    1.317450    1.379678    1.434595    1.584731    1.647323    1.731930    1.770188
     1.828166    1.860514    1.989911    2.091949    2.127036    2.304718    2.414985    2.488473    2.641964    2.780723
     2.940599    2.997458    3.138769    3.283680    3.453021    3.536285    3.618090    3.767188    3.809169    3.837476
     3.894060    3.991406    4.074378    4.126558    4.229043    4.335232    4.483944    4.496548    4.670581    4.835424
     4.941847    5.286396    5.402236    5.479589    5.809869    5.814662    5.922841

 fdd(*) eigenvalues. symmetry block  2
    -1.311986
 i,qaaresolved                     1  -1.13440723340047     
 i,qaaresolved                     2  -1.01589269857525     
 i,qaaresolved                     3 -0.858459346733194     
 i,qaaresolved                     4 -0.515382267889332     
 i,qaaresolved                     5  6.262805059007619E-002
 i,qaaresolved                     6  0.551811708447636     
 i,qaaresolved                     7  0.811692486046661     
 i,qaaresolved                     8   1.67494063981771     

 qvv(*) eigenvalues. symmetry block  2
     0.641838    1.192724    1.247629    1.371741    1.541057    1.597547    1.667881    1.815401    2.048088    2.235682
     2.411188    2.562474    2.973977    3.226957    3.357992    3.412075    3.623137    3.711399    3.794155    4.035813
     4.205837    4.649062    4.724936    5.136352    5.785801    5.874971

 fdd(*) eigenvalues. symmetry block  3
    -2.132966   -2.064764   -1.744748   -1.426899   -1.280894   -1.264647   -1.171855   -1.065909   -0.930833

 qvv(*) eigenvalues. symmetry block  3
     0.332531    0.373817    0.418892    0.492305    0.547477    0.711559    0.842287    0.931337    1.143077    1.265950
     1.359200    1.424907    1.611211    1.653112    1.684447    1.700664    1.761042    1.834563    1.942859    2.183920
     2.238482    2.371542    2.565332    2.606910    2.805187    2.888949    3.141037    3.219262    3.481337    3.608049
     3.736473    3.783051    3.916548    4.012137    4.196353    4.259083    4.423025    4.444106    4.535383    4.675311
     4.912905    5.028801    5.263758    5.410325    5.500209    5.569585    5.698641

 fdd(*) eigenvalues. symmetry block  4
    -1.235978
 i,qaaresolved                     1 -0.728282643262961     
 i,qaaresolved                     2  0.165981333418050     

 qvv(*) eigenvalues. symmetry block  4
     0.444788    1.273591    1.390679    1.536565    1.689517    2.084383    2.222155    2.396757    2.468467    2.762632
     3.243174    3.318377    3.517872    3.748240    3.825045    4.034096    4.354837    4.442099    4.462723    4.910723
     5.427923    5.664630

 restrt: restart information saved on the restart file (unit= 13).
 !timer: mcscf iteration                 cpu_time=    43.060 walltime=    64.899

 not all mcscf convergence criteria are satisfied.
 iter=    1 emc=   -455.4948064822 demc= 4.5549E+02 wnorm= 5.0147E-04 knorm= 1.1301E-03 apxde= 3.7418E-08    *not conv.*     

               starting mcscf iteration...   2
 !timer:                                 cpu_time=    43.078 walltime=    64.977

 orbital-state coupling will not be calculated this iteration.

 *** Starting integral transformation ***

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     4, naopsy(1) =    69, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 64959, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 65000

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore= 301272154

 inoutp: segmentation information:
 in-core transformation space,   avcinc = 301092379
 address segment size,           sizesg = 300940893
 number of in-core blocks,       nincbk =        19
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:    5591409 transformed 1/r12    array elements were written in    1025 records.

 !timer: 2-e transformation              cpu_time=     3.353 walltime=     4.261

 mosort: allocated sort2 space, avc2is=   301134007 available sort2 space, avcisx=   301134259
 !timer: mosrt1                          cpu_time=     0.286 walltime=     0.859
 !timer: mosort                          cpu_time=     0.358 walltime=     1.187

   2 trial vectors read from nvfile (unit= 29).
 ciiter=   8 noldhv=  9 noldv=  9
 !timer: hmc(*) diagonalization          cpu_time=     1.019 walltime=     1.572

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -455.4788047719     -354.1668964020        0.0000009060        0.0000010000
    2      -455.3828074258     -354.0708990559        0.0051032524        0.0100000000
 !timer: hmcvec                          cpu_time=     1.025 walltime=     1.587

   3 trial vectors read from nvfile (unit= 29).
 ciiter=  11 noldhv= 19 noldv= 19
 !timer: hmc(*) diagonalization          cpu_time=     1.919 walltime=     2.804

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -455.6503104036     -354.3384020337        0.0000006212        0.0000010000
    2      -455.4319143384     -354.1200059685        0.0000008922        0.0000010000
    3      -455.3815791684     -354.0696707985        0.0028044494        0.0100000000
 !timer: hmcvec                          cpu_time=     1.925 walltime=     2.820

   2 trial vectors read from nvfile (unit= 29).
 ciiter=   8 noldhv=  9 noldv=  9
 !timer: hmc(*) diagonalization          cpu_time=     1.979 walltime=     2.920

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -455.3917460222     -354.0798376524        0.0000006199        0.0000010000
    2      -455.2930798296     -353.9811714597        0.0041584777        0.0100000000
 !timer: hmcvec                          cpu_time=     1.986 walltime=     2.936

   2 trial vectors read from nvfile (unit= 29).
 !timer: hmcxv                           cpu_time=     0.349 walltime=     0.502
 ciiter=   9 noldhv= 10 noldv= 10
 !timer: hmc(*) diagonalization          cpu_time=     2.703 walltime=     3.976

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -455.5212571413     -354.2093487715        0.0000008814        0.0000010000
    2      -455.4777984213     -354.1658900514        0.0054013727        0.0100000000
 !timer: hmcvec                          cpu_time=     2.709 walltime=     3.993
 !timer: rdft                            cpu_time=     2.073 walltime=     2.172
 !timer: rdft                            cpu_time=     4.997 walltime=     5.077
 !timer: rdft                            cpu_time=     3.504 walltime=     3.635
 !timer: rdft                            cpu_time=     5.747 walltime=     5.900
 !timer: hbcon                           cpu_time=    17.883 walltime=    22.027
 
  tol(10)=  0.000000000000000E+000  eshsci=  2.830600870973750E-005
 Total number of micro iterations:    6

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.99999990 pnorm= 0.0000E+00 rznorm= 3.8593E-07 rpnorm= 0.0000E+00 noldr=  6 nnewr=  6 nolds=  0 nnews=  0
 
 !timer: solvek total                    cpu_time=     0.416 walltime=     1.391

 fdd(*) eigenvalues. symmetry block  1
    -2.672717   -2.443672   -2.368758   -2.150026   -1.930182   -1.715922   -1.560276   -1.498224   -1.332669   -1.228542
    -1.179160   -1.110366

 qvv(*) eigenvalues. symmetry block  1
     0.260048    0.325923    0.351924    0.464937    0.518305    0.648531    0.820009    0.846350    0.892596    1.080469
     1.174806    1.254720    1.300189    1.317451    1.379675    1.434594    1.584735    1.647325    1.731927    1.770191
     1.828165    1.860518    1.989909    2.091947    2.127039    2.304719    2.414984    2.488472    2.641961    2.780722
     2.940598    2.997459    3.138769    3.283680    3.453023    3.536284    3.618090    3.767188    3.809169    3.837474
     3.894060    3.991406    4.074379    4.126557    4.229043    4.335233    4.483943    4.496549    4.670581    4.835423
     4.941847    5.286396    5.402237    5.479589    5.809869    5.814663    5.922838

 fdd(*) eigenvalues. symmetry block  2
    -1.311952
 i,qaaresolved                     1  -1.13442020440648     
 i,qaaresolved                     2  -1.01591619964148     
 i,qaaresolved                     3 -0.858485999652701     
 i,qaaresolved                     4 -0.515396836436133     
 i,qaaresolved                     5  6.263762931369354E-002
 i,qaaresolved                     6  0.551804539924546     
 i,qaaresolved                     7  0.811697894679912     
 i,qaaresolved                     8   1.67625442498503     

 qvv(*) eigenvalues. symmetry block  2
     0.641185    1.192342    1.247627    1.371682    1.541075    1.597533    1.667884    1.815351    2.048116    2.235684
     2.411114    2.562453    2.973939    3.226961    3.357987    3.412032    3.623141    3.711403    3.794153    4.035812
     4.205816    4.649055    4.724918    5.136356    5.785795    5.874968

 fdd(*) eigenvalues. symmetry block  3
    -2.132963   -2.064763   -1.744747   -1.426899   -1.280896   -1.264645   -1.171851   -1.065909   -0.930837

 qvv(*) eigenvalues. symmetry block  3
     0.332534    0.373814    0.418892    0.492309    0.547476    0.711562    0.842288    0.931337    1.143075    1.265951
     1.359200    1.424907    1.611213    1.653112    1.684451    1.700665    1.761040    1.834563    1.942863    2.183918
     2.238482    2.371542    2.565331    2.606910    2.805189    2.888951    3.141038    3.219261    3.481337    3.608047
     3.736475    3.783054    3.916551    4.012138    4.196353    4.259086    4.423023    4.444107    4.535380    4.675314
     4.912908    5.028798    5.263757    5.410329    5.500210    5.569588    5.698640

 fdd(*) eigenvalues. symmetry block  4
    -1.235973
 i,qaaresolved                     1 -0.728271201306938     
 i,qaaresolved                     2  0.165984844256310     

 qvv(*) eigenvalues. symmetry block  4
     0.444795    1.273596    1.390679    1.536565    1.689521    2.084385    2.222156    2.396758    2.468468    2.762628
     3.243182    3.318377    3.517872    3.748245    3.825044    4.034097    4.354835    4.442099    4.462723    4.910723
     5.427927    5.664626

 restrt: restart information saved on the restart file (unit= 13).
 !timer: mcscf iteration                 cpu_time=    29.678 walltime=    40.240

 not all mcscf convergence criteria are satisfied.
 iter=    2 emc=   -455.4948065355 demc= 5.3271E-08 wnorm= 2.2645E-04 knorm= 4.5303E-04 apxde= 6.7657E-09    *not conv.*     

               starting mcscf iteration...   3
 !timer:                                 cpu_time=    72.757 walltime=   105.217

 orbital-state coupling will not be calculated this iteration.

 *** Starting integral transformation ***

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     4, naopsy(1) =    69, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 64959, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 65000

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore= 301272154

 inoutp: segmentation information:
 in-core transformation space,   avcinc = 301092379
 address segment size,           sizesg = 300940893
 number of in-core blocks,       nincbk =        19
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:    5591409 transformed 1/r12    array elements were written in    1025 records.

 !timer: 2-e transformation              cpu_time=     3.171 walltime=     3.985

 mosort: allocated sort2 space, avc2is=   301134007 available sort2 space, avcisx=   301134259
 !timer: mosrt1                          cpu_time=     0.267 walltime=     0.848
 !timer: mosort                          cpu_time=     0.325 walltime=     1.161

   2 trial vectors read from nvfile (unit= 29).
 ciiter=   9 noldhv= 10 noldv= 10
 !timer: hmc(*) diagonalization          cpu_time=     1.070 walltime=     1.754

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -455.4788080018     -354.1668996319        0.0000006611        0.0000010000
    2      -455.3828060245     -354.0708976546        0.0050312640        0.0100000000
 !timer: hmcvec                          cpu_time=     1.077 walltime=     1.783

   3 trial vectors read from nvfile (unit= 29).
 ciiter=  12 noldhv=  3 noldv=  3
 !timer: hmc(*) diagonalization          cpu_time=     1.957 walltime=     2.994

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -455.6503149566     -354.3384065867        0.0000008952        0.0000010000
    2      -455.4319070565     -354.1199986866        0.0000008320        0.0000010000
    3      -455.3815818881     -354.0696735182        0.0028323068        0.0100000000
 !timer: hmcvec                          cpu_time=     1.962 walltime=     3.009

   2 trial vectors read from nvfile (unit= 29).
 ciiter=   8 noldhv=  9 noldv=  9
 !timer: hmc(*) diagonalization          cpu_time=     1.816 walltime=     2.867

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -455.3917413263     -354.0798329564        0.0000009015        0.0000010000
    2      -455.2930742431     -353.9811658733        0.0041889703        0.0100000000
 !timer: hmcvec                          cpu_time=     1.820 walltime=     2.882

   2 trial vectors read from nvfile (unit= 29).
 ciiter=  10 noldhv= 11 noldv= 11
 !timer: hmc(*) diagonalization          cpu_time=     2.791 walltime=     4.347

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -455.5212613845     -354.2093530146        0.0000007505        0.0000010000
    2      -455.4778074439     -354.1658990741        0.0059232732        0.0100000000
 !timer: hmcvec                          cpu_time=     2.798 walltime=     4.365
 !timer: rdft                            cpu_time=     1.972 walltime=     2.058
 !timer: rdft                            cpu_time=     4.993 walltime=     5.081
 !timer: rdft                            cpu_time=     3.234 walltime=     3.360
 !timer: rdft                            cpu_time=     5.250 walltime=     5.396
 !timer: hbcon                           cpu_time=    17.021 walltime=    19.881
 
  tol(10)=  0.000000000000000E+000  eshsci=  1.258830839563647E-005
 Total number of micro iterations:    5

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.99999998 pnorm= 0.0000E+00 rznorm= 4.7319E-07 rpnorm= 0.0000E+00 noldr=  5 nnewr=  5 nolds=  0 nnews=  0
 
 !timer: solvek total                    cpu_time=     0.396 walltime=     1.366

 fdd(*) eigenvalues. symmetry block  1
    -2.672723   -2.443674   -2.368757   -2.150025   -1.930182   -1.715921   -1.560277   -1.498224   -1.332669   -1.228541
    -1.179160   -1.110368

 qvv(*) eigenvalues. symmetry block  1
     0.260049    0.325923    0.351925    0.464937    0.518305    0.648530    0.820009    0.846350    0.892596    1.080468
     1.174805    1.254718    1.300188    1.317452    1.379674    1.434593    1.584736    1.647326    1.731926    1.770192
     1.828164    1.860520    1.989908    2.091946    2.127040    2.304719    2.414984    2.488472    2.641959    2.780721
     2.940597    2.997459    3.138768    3.283680    3.453024    3.536284    3.618091    3.767187    3.809169    3.837473
     3.894059    3.991406    4.074379    4.126557    4.229043    4.335233    4.483942    4.496550    4.670581    4.835423
     4.941846    5.286396    5.402237    5.479589    5.809869    5.814663    5.922837

 fdd(*) eigenvalues. symmetry block  2
    -1.311938
 i,qaaresolved                     1  -1.13442571384874     
 i,qaaresolved                     2  -1.01592669642304     
 i,qaaresolved                     3 -0.858498127776559     
 i,qaaresolved                     4 -0.515403779212868     
 i,qaaresolved                     5  6.264127135815042E-002
 i,qaaresolved                     6  0.551801114192179     
 i,qaaresolved                     7  0.811700113935640     
 i,qaaresolved                     8   1.67678347585537     

 qvv(*) eigenvalues. symmetry block  2
     0.640923    1.192190    1.247626    1.371658    1.541082    1.597527    1.667886    1.815329    2.048128    2.235685
     2.411085    2.562444    2.973922    3.226963    3.357985    3.412015    3.623143    3.711405    3.794153    4.035812
     4.205808    4.649052    4.724910    5.136358    5.785792    5.874966

 fdd(*) eigenvalues. symmetry block  3
    -2.132962   -2.064763   -1.744746   -1.426899   -1.280896   -1.264644   -1.171849   -1.065909   -0.930839

 qvv(*) eigenvalues. symmetry block  3
     0.332535    0.373813    0.418892    0.492310    0.547476    0.711563    0.842288    0.931337    1.143074    1.265952
     1.359199    1.424906    1.611213    1.653112    1.684452    1.700665    1.761039    1.834562    1.942865    2.183917
     2.238482    2.371541    2.565331    2.606909    2.805190    2.888952    3.141039    3.219261    3.481336    3.608046
     3.736475    3.783055    3.916553    4.012139    4.196352    4.259088    4.423023    4.444108    4.535378    4.675315
     4.912909    5.028797    5.263757    5.410331    5.500211    5.569590    5.698640

 fdd(*) eigenvalues. symmetry block  4
    -1.235971
 i,qaaresolved                     1 -0.728265906295621     
 i,qaaresolved                     2  0.165987270686133     

 qvv(*) eigenvalues. symmetry block  4
     0.444798    1.273598    1.390680    1.536565    1.689523    2.084386    2.222156    2.396758    2.468468    2.762627
     3.243185    3.318377    3.517872    3.748248    3.825044    4.034097    4.354834    4.442098    4.462723    4.910723
     5.427928    5.664623

 restrt: restart information saved on the restart file (unit= 13).
 !timer: mcscf iteration                 cpu_time=    28.591 walltime=    38.472

 not all mcscf convergence criteria are satisfied.
 iter=    3 emc=   -455.4948065451 demc= 9.6519E-09 wnorm= 1.0071E-04 knorm= 1.8479E-04 apxde= 1.2362E-09    *not conv.*     

               starting mcscf iteration...   4
 !timer:                                 cpu_time=   101.348 walltime=   143.690

 orbital-state coupling will not be calculated this iteration.

 *** Starting integral transformation ***

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     4, naopsy(1) =    69, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 64959, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 65000

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore= 301272154

 inoutp: segmentation information:
 in-core transformation space,   avcinc = 301092379
 address segment size,           sizesg = 300940893
 number of in-core blocks,       nincbk =        19
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:    5591409 transformed 1/r12    array elements were written in    1025 records.

 !timer: 2-e transformation              cpu_time=     3.212 walltime=     3.839

 mosort: allocated sort2 space, avc2is=   301134007 available sort2 space, avcisx=   301134259
 !timer: mosort                          cpu_time=     0.305 walltime=     0.723

   2 trial vectors read from nvfile (unit= 29).
 ciiter=   9 noldhv= 10 noldv= 10
 !timer: hmc(*) diagonalization          cpu_time=     1.033 walltime=     1.733

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -455.4788093686     -354.1669009987        0.0000007557        0.0000010000
    2      -455.3828046082     -354.0708962383        0.0050328777        0.0100000000
 !timer: hmcvec                          cpu_time=     1.038 walltime=     1.747

   3 trial vectors read from nvfile (unit= 29).
 ciiter=  12 noldhv=  4 noldv=  4
 !timer: hmc(*) diagonalization          cpu_time=     2.002 walltime=     3.055

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -455.6503169706     -354.3384086007        0.0000004068        0.0000010000
    2      -455.4319039069     -354.1199955370        0.0000007998        0.0000010000
    3      -455.3815829960     -354.0696746261        0.0028500865        0.0100000000
 !timer: hmcvec                          cpu_time=     2.006 walltime=     3.068

   2 trial vectors read from nvfile (unit= 29).
 ciiter=   9 noldhv= 10 noldv= 10
 !timer: hmc(*) diagonalization          cpu_time=     2.018 walltime=     3.267

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -455.3917392596     -354.0798308897        0.0000005476        0.0000010000
    2      -455.2930721197     -353.9811637498        0.0040952813        0.0100000000
 !timer: hmcvec                          cpu_time=     2.023 walltime=     3.283

   2 trial vectors read from nvfile (unit= 29).
 ciiter=  10 noldhv= 11 noldv= 11
 !timer: hmc(*) diagonalization          cpu_time=     2.706 walltime=     4.346

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -455.5212632288     -354.2093548590        0.0000008482        0.0000010000
    2      -455.4778095163     -354.1659011464        0.0059534491        0.0100000000
 !timer: hmcvec                          cpu_time=     2.713 walltime=     4.367
 !timer: rdft                            cpu_time=     1.481 walltime=     1.575
 !timer: rdft                            cpu_time=     4.743 walltime=     4.838
 !timer: rdft                            cpu_time=     3.271 walltime=     3.409
 !timer: rdft                            cpu_time=     5.842 walltime=     5.999
 !timer: hbcon                           cpu_time=    16.898 walltime=    21.474
 
  tol(10)=  0.000000000000000E+000  eshsci=  5.525484074249256E-006
 Total number of micro iterations:    4

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0=-1.00000000 pnorm= 0.0000E+00 rznorm= 7.0887E-07 rpnorm= 0.0000E+00 noldr=  4 nnewr=  4 nolds=  0 nnews=  0
 
 !timer: solvek total                    cpu_time=     0.535 walltime=     1.435

 fdd(*) eigenvalues. symmetry block  1
    -2.672726   -2.443675   -2.368757   -2.150025   -1.930182   -1.715921   -1.560277   -1.498225   -1.332669   -1.228541
    -1.179160   -1.110369

 qvv(*) eigenvalues. symmetry block  1
     0.260050    0.325924    0.351925    0.464937    0.518304    0.648530    0.820009    0.846351    0.892596    1.080468
     1.174804    1.254717    1.300188    1.317452    1.379673    1.434593    1.584737    1.647326    1.731925    1.770193
     1.828164    1.860521    1.989907    2.091946    2.127040    2.304719    2.414984    2.488472    2.641959    2.780721
     2.940596    2.997459    3.138768    3.283680    3.453024    3.536284    3.618091    3.767187    3.809168    3.837472
     3.894059    3.991406    4.074379    4.126557    4.229043    4.335233    4.483942    4.496550    4.670581    4.835422
     4.941846    5.286396    5.402238    5.479589    5.809869    5.814663    5.922836

 fdd(*) eigenvalues. symmetry block  2
    -1.311932
 i,qaaresolved                     1  -1.13442808980065     
 i,qaaresolved                     2  -1.01593130127540     
 i,qaaresolved                     3 -0.858503526065719     
 i,qaaresolved                     4 -0.515406994129744     
 i,qaaresolved                     5  6.264266238505276E-002
 i,qaaresolved                     6  0.551799673707540     
 i,qaaresolved                     7  0.811701061838939     
 i,qaaresolved                     8   1.67700030427341     

 qvv(*) eigenvalues. symmetry block  2
     0.640816    1.192129    1.247626    1.371647    1.541085    1.597524    1.667886    1.815320    2.048132    2.235685
     2.411074    2.562441    2.973915    3.226964    3.357984    3.412008    3.623143    3.711405    3.794152    4.035812
     4.205804    4.649051    4.724907    5.136358    5.785791    5.874965

 fdd(*) eigenvalues. symmetry block  3
    -2.132962   -2.064763   -1.744746   -1.426899   -1.280897   -1.264644   -1.171849   -1.065909   -0.930840

 qvv(*) eigenvalues. symmetry block  3
     0.332535    0.373812    0.418892    0.492311    0.547476    0.711563    0.842288    0.931337    1.143073    1.265953
     1.359199    1.424906    1.611213    1.653113    1.684453    1.700665    1.761038    1.834562    1.942865    2.183916
     2.238482    2.371541    2.565330    2.606909    2.805191    2.888952    3.141039    3.219260    3.481336    3.608046
     3.736476    3.783056    3.916553    4.012139    4.196352    4.259088    4.423022    4.444108    4.535378    4.675316
     4.912909    5.028797    5.263757    5.410332    5.500211    5.569590    5.698640

 fdd(*) eigenvalues. symmetry block  4
    -1.235970
 i,qaaresolved                     1 -0.728263523616613     
 i,qaaresolved                     2  0.165988603407307     

 qvv(*) eigenvalues. symmetry block  4
     0.444800    1.273599    1.390680    1.536565    1.689523    2.084387    2.222156    2.396758    2.468468    2.762626
     3.243186    3.318377    3.517872    3.748248    3.825044    4.034097    4.354833    4.442098    4.462723    4.910723
     5.427929    5.664622

 restrt: restart information saved on the restart file (unit= 13).
 !timer: mcscf iteration                 cpu_time=    28.754 walltime=    39.976

 all mcscf convergence criteria are satisfied.

 final mcscf convergence values:
 iter=    4 emc=   -455.4948065469 demc= 1.7652E-09 wnorm= 4.4204E-05 knorm= 7.5906E-05 apxde= 2.2630E-10    *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 wt 0.200 total energy=     -455.478809369, rel. (eV)=   4.666961
   DRT #2 state # 1 wt 0.200 total energy=     -455.650316971, rel. (eV)=   0.000000
   DRT #2 state # 2 wt 0.200 total energy=     -455.431903907, rel. (eV)=   5.943324
   DRT #3 state # 1 wt 0.200 total energy=     -455.391739260, rel. (eV)=   7.036261
   DRT #4 state # 1 wt 0.200 total energy=     -455.521263229, rel. (eV)=   3.511732
   ------------------------------------------------------------


 MO-coefficient print-out skipped (no flag 32)
 They may be found in the MOCOEF directory.

          natural orbitals of the final iteration,block  1    -  A1 
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000
               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   25        MO   26        MO   27        MO   28        MO   29        MO   30        MO   31        MO   32
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   33        MO   34        MO   35        MO   36        MO   37        MO   38        MO   39        MO   40
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   41        MO   42        MO   43        MO   44        MO   45        MO   46        MO   47        MO   48
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   49        MO   50        MO   51        MO   52        MO   53        MO   54        MO   55        MO   56
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   57        MO   58        MO   59        MO   60        MO   61        MO   62        MO   63        MO   64
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   65        MO   66        MO   67        MO   68        MO   69
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

          natural orbitals of the final iteration,block  2    -  B1 
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     1.95955252     1.94293420     1.89807759     1.31042765     0.49774639     0.07438470     0.04898499
               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     0.01102353     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   25        MO   26        MO   27        MO   28        MO   29        MO   30        MO   31        MO   32
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   33        MO   34        MO   35
  occ(*)=     0.00000000     0.00000000     0.00000000

          natural orbitals of the final iteration,block  3    -  B2 
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000
               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   25        MO   26        MO   27        MO   28        MO   29        MO   30        MO   31        MO   32
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   33        MO   34        MO   35        MO   36        MO   37        MO   38        MO   39        MO   40
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   41        MO   42        MO   43        MO   44        MO   45        MO   46        MO   47        MO   48
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   49        MO   50        MO   51        MO   52        MO   53        MO   54        MO   55        MO   56
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

          natural orbitals of the final iteration,block  4    -  A2 
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     1.80138809     0.25548034     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   25
  occ(*)=     0.00000000
 d1(*), fmc(*), and qmc(*) written to the 1-particle density matrix file.
       3239 d2(*) elements written to the 2-particle density matrix file: mcd2fl                                                      
 Computing the requested mcscf (transition) density matrices (flag 30)
 Reading mcdenin ...
 Number of density matrices (ndens):                     1
 Number of unique bra states (ndbra):                     1
 qind: F
 (Transition) density matrices:
 d1(*) written to the 1-particle density matrix file.
       3239 d2(*) elements written to the 2-particle density matrix file: mcsd2fl.drt1.st01                                           

          state spec. NOs: DRT 1, State  1

          block  1
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000
               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   25        MO   26        MO   27        MO   28        MO   29        MO   30        MO   31        MO   32
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   33        MO   34        MO   35        MO   36        MO   37        MO   38        MO   39        MO   40
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   41        MO   42        MO   43        MO   44        MO   45        MO   46        MO   47        MO   48
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   49        MO   50        MO   51        MO   52        MO   53        MO   54        MO   55        MO   56
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   57        MO   58        MO   59        MO   60        MO   61        MO   62        MO   63        MO   64
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   65        MO   66        MO   67        MO   68        MO   69
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

          block  2
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     1.97993470     1.93906916     1.86985930     1.32261498     0.44847199     0.10455080     0.05563318
               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     0.01039519     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   25        MO   26        MO   27        MO   28        MO   29        MO   30        MO   31        MO   32
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   33        MO   34        MO   35
  occ(*)=     0.00000000     0.00000000     0.00000000

          block  3
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000
               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   25        MO   26        MO   27        MO   28        MO   29        MO   30        MO   31        MO   32
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   33        MO   34        MO   35        MO   36        MO   37        MO   38        MO   39        MO   40
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   41        MO   42        MO   43        MO   44        MO   45        MO   46        MO   47        MO   48
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   49        MO   50        MO   51        MO   52        MO   53        MO   54        MO   55        MO   56
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

          block  4
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     1.58020218     0.68926851     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   25
  occ(*)=     0.00000000


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        A1  partial gross atomic populations
   ao class       1A1        2A1        3A1        4A1        5A1        6A1 
    to_ s       2.000000   2.000000   2.000000   2.000000   2.000000   2.000000
 
   ao class       7A1        8A1        9A1       10A1       11A1       12A1 
    to_ s       2.000000   2.000000   2.000000   2.000000   2.000000   2.000000
 
   ao class      13A1       14A1       15A1       16A1       17A1       18A1 
 
   ao class      19A1       20A1       21A1       22A1       23A1       24A1 
 
   ao class      25A1       26A1       27A1       28A1       29A1       30A1 
 
   ao class      31A1       32A1       33A1       34A1       35A1       36A1 
 
   ao class      37A1       38A1       39A1       40A1       41A1       42A1 
 
   ao class      43A1       44A1       45A1       46A1       47A1       48A1 
 
   ao class      49A1       50A1       51A1       52A1       53A1       54A1 
 
   ao class      55A1       56A1       57A1       58A1       59A1       60A1 
 
   ao class      61A1       62A1       63A1       64A1       65A1       66A1 
 
   ao class      67A1       68A1       69A1 

                        B1  partial gross atomic populations
   ao class       1B1        2B1        3B1        4B1        5B1        6B1 
    to_ s       2.000000   1.959553   1.942934   1.898078   1.310428   0.497746
 
   ao class       7B1        8B1        9B1       10B1       11B1       12B1 
    to_ s       0.074385   0.048985   0.011024   0.000000   0.000000   0.000000
 
   ao class      13B1       14B1       15B1       16B1       17B1       18B1 
 
   ao class      19B1       20B1       21B1       22B1       23B1       24B1 
 
   ao class      25B1       26B1       27B1       28B1       29B1       30B1 
 
   ao class      31B1       32B1       33B1       34B1       35B1 

                        B2  partial gross atomic populations
   ao class       1B2        2B2        3B2        4B2        5B2        6B2 
    to_ s       2.000000   2.000000   2.000000   2.000000   2.000000   2.000000
 
   ao class       7B2        8B2        9B2       10B2       11B2       12B2 
    to_ s       2.000000   2.000000   2.000000   0.000000   0.000000   0.000000
 
   ao class      13B2       14B2       15B2       16B2       17B2       18B2 
 
   ao class      19B2       20B2       21B2       22B2       23B2       24B2 
 
   ao class      25B2       26B2       27B2       28B2       29B2       30B2 
 
   ao class      31B2       32B2       33B2       34B2       35B2       36B2 
 
   ao class      37B2       38B2       39B2       40B2       41B2       42B2 
 
   ao class      43B2       44B2       45B2       46B2       47B2       48B2 
 
   ao class      49B2       50B2       51B2       52B2       53B2       54B2 
 
   ao class      55B2       56B2 

                        A2  partial gross atomic populations
   ao class       1A2        2A2        3A2        4A2        5A2        6A2 
    to_ s       2.000000   1.801388   0.255480   0.000000   0.000000   0.000000
 
   ao class       7A2        8A2        9A2       10A2       11A2       12A2 
 
   ao class      13A2       14A2       15A2       16A2       17A2       18A2 
 
   ao class      19A2       20A2       21A2       22A2       23A2       24A2 
 
   ao class      25A2 


                        gross atomic populations
     ao           to_
      s        55.800000
    total      55.800000
 

 Total number of electrons:   55.80000000

 Mulliken population for:
DRT 1, state 01


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        A1  partial gross atomic populations
   ao class       1A1        2A1        3A1        4A1        5A1        6A1 
    to_ s       2.000000   2.000000   2.000000   2.000000   2.000000   2.000000
 
   ao class       7A1        8A1        9A1       10A1       11A1       12A1 
    to_ s       2.000000   2.000000   2.000000   2.000000   2.000000   2.000000
 
   ao class      13A1       14A1       15A1       16A1       17A1       18A1 
 
   ao class      19A1       20A1       21A1       22A1       23A1       24A1 
 
   ao class      25A1       26A1       27A1       28A1       29A1       30A1 
 
   ao class      31A1       32A1       33A1       34A1       35A1       36A1 
 
   ao class      37A1       38A1       39A1       40A1       41A1       42A1 
 
   ao class      43A1       44A1       45A1       46A1       47A1       48A1 
 
   ao class      49A1       50A1       51A1       52A1       53A1       54A1 
 
   ao class      55A1       56A1       57A1       58A1       59A1       60A1 
 
   ao class      61A1       62A1       63A1       64A1       65A1       66A1 
 
   ao class      67A1       68A1       69A1 

                        B1  partial gross atomic populations
   ao class       1B1        2B1        3B1        4B1        5B1        6B1 
    to_ s       2.000000   1.979935   1.939069   1.869859   1.322615   0.448472
 
   ao class       7B1        8B1        9B1       10B1       11B1       12B1 
    to_ s       0.104551   0.055633   0.010395   0.000000   0.000000   0.000000
 
   ao class      13B1       14B1       15B1       16B1       17B1       18B1 
 
   ao class      19B1       20B1       21B1       22B1       23B1       24B1 
 
   ao class      25B1       26B1       27B1       28B1       29B1       30B1 
 
   ao class      31B1       32B1       33B1       34B1       35B1 

                        B2  partial gross atomic populations
   ao class       1B2        2B2        3B2        4B2        5B2        6B2 
    to_ s       2.000000   2.000000   2.000000   2.000000   2.000000   2.000000
 
   ao class       7B2        8B2        9B2       10B2       11B2       12B2 
    to_ s       2.000000   2.000000   2.000000   0.000000   0.000000   0.000000
 
   ao class      13B2       14B2       15B2       16B2       17B2       18B2 
 
   ao class      19B2       20B2       21B2       22B2       23B2       24B2 
 
   ao class      25B2       26B2       27B2       28B2       29B2       30B2 
 
   ao class      31B2       32B2       33B2       34B2       35B2       36B2 
 
   ao class      37B2       38B2       39B2       40B2       41B2       42B2 
 
   ao class      43B2       44B2       45B2       46B2       47B2       48B2 
 
   ao class      49B2       50B2       51B2       52B2       53B2       54B2 
 
   ao class      55B2       56B2 

                        A2  partial gross atomic populations
   ao class       1A2        2A2        3A2        4A2        5A2        6A2 
    to_ s       2.000000   1.580202   0.689269   0.000000   0.000000   0.000000
 
   ao class       7A2        8A2        9A2       10A2       11A2       12A2 
 
   ao class      13A2       14A2       15A2       16A2       17A2       18A2 
 
   ao class      19A2       20A2       21A2       22A2       23A2       24A2 
 
   ao class      25A2 


                        gross atomic populations
     ao           to_
      s        56.000000
    total      56.000000
 

 Total number of electrons:   56.00000000

 !timer: mcscf                           cpu_time=   130.353 walltime=   184.082
