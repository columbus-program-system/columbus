

     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 This program allows the csf mixing coefficient and orbital expansion coefficient
 optimization using the graphical unitary group approach and the exponential
 operator mcscf method.
 references:  r. shepard and j. simons, ' int. j. quantum chem. symp. 14, 211 (1980).
              r. shepard, i. shavitt, and j. simons, j. chem. phys. 76, 543 (1982).
              r. shepard in "ab initio methods in quantum chemistry ii" advances in chemical
                  physics 69, edited by k. p. lawley (wiley, new york, 1987) pp. 63-200.
 Original autor: Ron Shepard, ANL
 Later revisions: Michal Dallos, University Vienna

 This Version of Program MCSCF is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.4.0.2     **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 Workspace allocation information:
       301465600 of real*8 words ( 2300.00 MB) of work space has been allocated.

 user input information:

 ======== echo of the mcscf input ========
 ------------------------------------------------------------------------
  &input
   NITER=1,NMITER=0
   NMITER=0
   nciitr=300,
   tol(3)=1.e-4,
   tol(2)=1.e-4,
   tol(1)=1.e-8,
   NSTATE=0,
   npath=1,3,9,10,13,17,19,21,-11,12, 2,-13,30,13,18
   ncoupl=5,
   tol(9)=1.e-3,
   FCIORB=  2,2,40,2,3,40,2,4,40,2,5,40,2,6,40,2,7,40,2,8,40,2,9,40,4,2,40,4,3,40
  
  
  
  
  
  
  
  
  
  
 NAVST(1) = 1
 WAVST(1,1)=1
 /&end
 ------------------------------------------------------------------------


 ***  Integral file informations  ***


 input integral file : /fhgfs/global/lv70151/plasser1/1679223/WORK/WORK.4/aoints
    

 Integral file header information:
 Hermit Integral Program : SIFS version  r24n13            16:31:16.794 06-Sep-12
 user-specified FREEZE(*) used in program tran.                                  
 MO-coefficients from mcscf.x                                                    
  total ao core energy = -101.311908370                                          
 dummy occupation 1.0 for active orbitals                                        
 transformed using mo_mult.x                                                     
 SIFS file created by program tran.      r24n13            16:31:47.239 06-Sep-12

 Core type energy values:
 energy( 1)=  5.461735143427E+02, ietype=   -1,    core energy of type: Nuc.Rep.
 total ao core energy =  546.173514343


   ******  Basis set information:  ******

 Number of irreps:                  4
 Total number of basis functions: 185

 irrep no.              1    2    3    4
 irrep label           A1   B1   B2   A2 
 no. of bas.fcions.    69   35   56   25


 ***  MCSCF optimization procedure parmeters:  ***


 maximum number of mcscf iterations:        niter=     1

 maximum number of psci micro-iterations:   nmiter=    0
 maximum r,s subspace dimension allowed:    nvrsmx=   30

 tol(1)=  1.0000E-08. . . . delta-emc convergence criterion.
 tol(2)=  1.0000E-04. . . . wnorm convergence criterion.
 tol(3)=  1.0000E-04. . . . knorm convergence criterion.
 tol(4)=  1.0000E-08. . . . apxde convergence criterion.
 tol(5)=  1.0000E-04. . . . small diagonal matrix element tolerance.
 tol(6)=  1.0000E-06. . . . minimum ci-psci residual norm.
 tol(7)=  1.0000E-05. . . . maximum ci-psci residual norm.
 tol(8)=  1.0000E+00. . . . maximum abs(k(xy)) allowed.
 tol(9)=  1.0000E-03. . . . wnorm coupling tolerance.
 tol(10)= 0.0000E+00. . . . maximum psci emergency shift parameter.
 tol(11)= 0.0000E+00. . . . minimum psci emergency shift parameter.
 tol(12)= 0.0000E+00. . . . increment of psci emergency shift parameter.


 *** State averaging informations: ***


 MCSCF calculation performed for  1 DRT.

 DRT  first state   no.of aver.states   weights
  1   ground state          1             1.000

 The number of hmc(*) eigenvalues and eigenvectors calculated each iteration per DRT:
 DRT.   no.of eigenv.(=ncol)
    1        2

 orbital coefficients are optimized for the ground state (nstate=0).

 Orbitals included in invariant subspaces:
   symmetry   orbital   mask
       2       2( 71)    40
       2       3( 72)    40
       2       4( 73)    40
       2       5( 74)    40
       2       6( 75)    40
       2       7( 76)    40
       2       8( 77)    40
       2       9( 78)    40
       4       2(162)    40
       4       3(163)    40

 npath(*) options:
  2:  orbital-state coupling terms will be included beginning on iteration ncoupl=  5
  3:  print intermediate timing information.
  9:  suppress the drt listing.
 10:  suppress the hmc(*) eigenvector listing.
 12:  diagonalize the hmc(*) matrix iteratively.
        nunitv= 1 nciitr=** mxvadd=20 nvcimx=20
       rtolci(*),wnorm=     1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 0.0000E+00
   noldv =   0
 13:  get initial orbitals from the formatted file, mocoef.
 17:  print the final natural orbitals and occupations.
 18:  skip the 2-e integral transformation on the first iteration.
 19:  transform the virtual orbitals to diagonalize qvv(*).
 21:  write out the one- and two- electron density for further use (files:mcd1fl, mcd2fl).
 30:  Compute mcscf (transition) density matrices


   ******  DRT info section  ******


 Informations for the DRT no.  1

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a1 
 Total number of electrons:   56
 Spin multiplicity:            3
 Number of active orbitals:   10
 Number of active electrons:  10
 Total number of CSFs:     14916
 

 faar:   0 active-active rotations allowed out of:  29 possible.


 Number of active-double rotations:        10
 Number of active-active rotations:         0
 Number of double-virtual rotations:     1155
 Number of active-virtual rotations:      252
 lenbfsdef=                131071  lenbfs=                  6618
  number of integrals per class 1:11 (cf adda 
 class  1 (pq|rs):         #         916
 class  2 (pq|ri):         #         550
 class  3 (pq|ia):         #       63045
 class  4 (pi|qa):         #      114540
 class  5 (pq|ra):         #       13476
 class  6 (pq|ij)/(pi|qj): #       18960
 class  7 (pq|ab):         #      184031
 class  8 (pa|qb):         #      362134
 class  9 p(bp,ai)         #      291060
 class 10p(ai,jp):        #       11550
 class 11p(ai,bj):        #      697027

 Size of orbital-Hessian matrix B:                  1037152
 Size of the orbital-state Hessian matrix C:       21135972
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:       22173124


 Source of the initial MO coeficients:

 Input MO coefficient file: /fhgfs/global/lv70151/plasser1/1679223/WORK/WORK.4/mocoef   
 

               starting mcscf iteration...   1

 orbital-state coupling will not be calculated this iteration.

 *** Starting integral transformation ***

 mosort: allocated sort2 space, avc2is=   301204423 available sort2 space, avcisx=   301204675
 !timer: mosort                          cpu_time=     0.369 walltime=     0.718

 trial vectors are generated internally.

 trial vector  1 is unit matrix column    24
 !timer: hmcxv                           cpu_time=     0.345 walltime=     0.502
 ciiter=  21 noldhv= 13 noldv= 13
 !timer: hmc(*) diagonalization          cpu_time=     6.694 walltime=     9.474

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -455.5212632288     -354.2093548590        0.0000008040        0.0000010000
    2      -455.4777996978     -354.1658913279        0.0053155951        0.0100000000
 !timer: hmcvec                          cpu_time=     6.700 walltime=     9.494
 !timer: rdft                            cpu_time=     2.799 walltime=     2.937
 !timer: hbcon                           cpu_time=     3.024 walltime=     4.270
 
  tol(10)=  0.000000000000000E+000  eshsci=  2.254749385636136E-002
 *** psci convergence not reached ***
 Total number of micro iterations:    0

 ***  micro: final psci convergence values:  ***
    imxov=  0 z0= 0.00000000 pnorm= 0.0000E+00 rznorm= 0.0000E+00 rpnorm= 0.0000E+00 noldr=  0 nnewr=  1 nolds=  0 nnews=  0
 *** warning *** small z0.
 

 fdd(*) eigenvalues. symmetry block  1
    -2.518145   -2.403805   -2.289314   -2.053626   -1.827885   -1.626550   -1.486528   -1.386182   -1.252740   -1.150755
    -1.093793   -1.047979

 qvv(*) eigenvalues. symmetry block  1
     0.331005    0.388532    0.422108    0.530534    0.616090    0.701039    0.894512    0.922587    0.974431    1.160628
     1.273178    1.351289    1.374308    1.402470    1.479522    1.504168    1.673217    1.745526    1.772759    1.848604
     1.897890    1.941154    2.094753    2.156512    2.221941    2.386728    2.496607    2.558976    2.692828    2.851688
     3.007256    3.082444    3.234468    3.363442    3.539884    3.617990    3.705078    3.834557    3.895597    3.916741
     3.984757    4.083916    4.154268    4.224794    4.303079    4.431122    4.535454    4.582663    4.780117    4.900693
     5.045849    5.385969    5.488171    5.586067    5.895473    5.909452    6.012476

 fdd(*) eigenvalues. symmetry block  2
    -1.215319
 i,qaaresolved                     1  -1.03816837593770     
 i,qaaresolved                     2 -0.934407215613418     
 i,qaaresolved                     3 -0.777271105554592     
 i,qaaresolved                     4 -0.396507251867180     
 i,qaaresolved                     5  6.722256782210589E-002
 i,qaaresolved                     6  0.613404548333826     
 i,qaaresolved                     7  0.867351103429044     
 i,qaaresolved                     8   1.81903084492836     

 qvv(*) eigenvalues. symmetry block  2
     0.721812    1.283518    1.307228    1.443768    1.637445    1.664710    1.733530    1.903617    2.093580    2.311475
     2.502175    2.636398    3.078742    3.310647    3.424881    3.505354    3.712872    3.790117    3.873094    4.120678
     4.285994    4.742846    4.807124    5.248173    5.875109    5.949546

 fdd(*) eigenvalues. symmetry block  3
    -2.041638   -1.959991   -1.660382   -1.320609   -1.182280   -1.175569   -1.079970   -0.978001   -0.891638

 qvv(*) eigenvalues. symmetry block  3
     0.400645    0.419395    0.477753    0.563337    0.612520    0.802420    0.919917    1.024158    1.213160    1.341984
     1.439316    1.520752    1.684478    1.727847    1.763084    1.781826    1.831767    1.925622    2.030113    2.234896
     2.312018    2.444290    2.658240    2.676500    2.895102    2.974940    3.234333    3.305314    3.556132    3.666403
     3.815478    3.867761    4.012471    4.109729    4.286423    4.352485    4.483633    4.528136    4.642965    4.765303
     5.007959    5.139905    5.323012    5.498578    5.577653    5.663575    5.782163

 fdd(*) eigenvalues. symmetry block  4
    -1.141710
 i,qaaresolved                     1 -0.652793333502501     
 i,qaaresolved                     2  0.264369131568531     

 qvv(*) eigenvalues. symmetry block  4
     0.518272    1.363549    1.464617    1.614236    1.780220    2.180346    2.306833    2.475256    2.551469    2.815047
     3.326742    3.409155    3.613916    3.842116    3.902556    4.118140    4.409374    4.513877    4.551784    4.994842
     5.525551    5.797536

 restrt: restart information saved on the restart file (unit= 13).
 !timer: mcscf iteration                 cpu_time=    10.254 walltime=    14.869

 not all mcscf convergence criteria are satisfied.
 iter=    1 emc=   -455.5212632288 demc= 4.5552E+02 wnorm= 1.8038E-01 knorm= 0.0000E+00 apxde=-1.1274E-02    *not conv.*     

 final mcscf convergence values:
 iter=    1 emc=   -455.5212632288 demc= 4.5552E+02 wnorm= 1.8038E-01 knorm= 0.0000E+00 apxde=-1.1274E-02    *not conv.*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 wt 1.000 total energy=     -455.521263229, rel. (eV)=   0.000000
   ------------------------------------------------------------


 MO-coefficient print-out skipped (no flag 32)
 They may be found in the MOCOEF directory.

          natural orbitals of the final iteration,block  1    -  A1 
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000
               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   25        MO   26        MO   27        MO   28        MO   29        MO   30        MO   31        MO   32
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   33        MO   34        MO   35        MO   36        MO   37        MO   38        MO   39        MO   40
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   41        MO   42        MO   43        MO   44        MO   45        MO   46        MO   47        MO   48
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   49        MO   50        MO   51        MO   52        MO   53        MO   54        MO   55        MO   56
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   57        MO   58        MO   59        MO   60        MO   61        MO   62        MO   63        MO   64
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   65        MO   66        MO   67        MO   68        MO   69
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

          natural orbitals of the final iteration,block  2    -  B1 
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     1.98364138     1.94008996     1.91013576     1.15412164     0.85332229     0.08698977     0.05656403
               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     0.01037997     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   25        MO   26        MO   27        MO   28        MO   29        MO   30        MO   31        MO   32
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   33        MO   34        MO   35
  occ(*)=     0.00000000     0.00000000     0.00000000

          natural orbitals of the final iteration,block  3    -  B2 
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000
               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   25        MO   26        MO   27        MO   28        MO   29        MO   30        MO   31        MO   32
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   33        MO   34        MO   35        MO   36        MO   37        MO   38        MO   39        MO   40
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   41        MO   42        MO   43        MO   44        MO   45        MO   46        MO   47        MO   48
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   49        MO   50        MO   51        MO   52        MO   53        MO   54        MO   55        MO   56
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

          natural orbitals of the final iteration,block  4    -  A2 
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     1.78719815     0.21755705     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   25
  occ(*)=     0.00000000
 d1(*), fmc(*), and qmc(*) written to the 1-particle density matrix file.
       3239 d2(*) elements written to the 2-particle density matrix file: mcd2fl                                                      
 Computing the requested mcscf (transition) density matrices (flag 30)
 Reading mcdenin ...
 Number of density matrices (ndens):                     1
 Number of unique bra states (ndbra):                     1
 qind: F
 !timer: rdft_grd                        cpu_time=     0.408 walltime=     0.539
 (Transition) density matrices:
 d1(*) written to the 1-particle density matrix file.
       3239 d2(*) elements written to the 2-particle density matrix file: mcsd2fl.drt1.st01                                           

          state spec. NOs: DRT 1, State  1

          block  1
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000
               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   25        MO   26        MO   27        MO   28        MO   29        MO   30        MO   31        MO   32
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   33        MO   34        MO   35        MO   36        MO   37        MO   38        MO   39        MO   40
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   41        MO   42        MO   43        MO   44        MO   45        MO   46        MO   47        MO   48
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   49        MO   50        MO   51        MO   52        MO   53        MO   54        MO   55        MO   56
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   57        MO   58        MO   59        MO   60        MO   61        MO   62        MO   63        MO   64
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   65        MO   66        MO   67        MO   68        MO   69
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

          block  2
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     1.98364138     1.94008996     1.91013576     1.15412164     0.85332229     0.08698977     0.05656403
               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     0.01037997     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   25        MO   26        MO   27        MO   28        MO   29        MO   30        MO   31        MO   32
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   33        MO   34        MO   35
  occ(*)=     0.00000000     0.00000000     0.00000000

          block  3
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000
               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   25        MO   26        MO   27        MO   28        MO   29        MO   30        MO   31        MO   32
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   33        MO   34        MO   35        MO   36        MO   37        MO   38        MO   39        MO   40
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   41        MO   42        MO   43        MO   44        MO   45        MO   46        MO   47        MO   48
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   49        MO   50        MO   51        MO   52        MO   53        MO   54        MO   55        MO   56
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

          block  4
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     1.78719815     0.21755705     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   25
  occ(*)=     0.00000000


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        A1  partial gross atomic populations
   ao class       1A1        2A1        3A1        4A1        5A1        6A1 
    to_ s       2.000000   2.000000   2.000000   2.000000   2.000000   2.000000
 
   ao class       7A1        8A1        9A1       10A1       11A1       12A1 
    to_ s       2.000000   2.000000   2.000000   2.000000   2.000000   2.000000
 
   ao class      13A1       14A1       15A1       16A1       17A1       18A1 
 
   ao class      19A1       20A1       21A1       22A1       23A1       24A1 
 
   ao class      25A1       26A1       27A1       28A1       29A1       30A1 
 
   ao class      31A1       32A1       33A1       34A1       35A1       36A1 
 
   ao class      37A1       38A1       39A1       40A1       41A1       42A1 
 
   ao class      43A1       44A1       45A1       46A1       47A1       48A1 
 
   ao class      49A1       50A1       51A1       52A1       53A1       54A1 
 
   ao class      55A1       56A1       57A1       58A1       59A1       60A1 
 
   ao class      61A1       62A1       63A1       64A1       65A1       66A1 
 
   ao class      67A1       68A1       69A1 

                        B1  partial gross atomic populations
   ao class       1B1        2B1        3B1        4B1        5B1        6B1 
    to_ s       2.000000   1.983641   1.940090   1.910136   1.154122   0.853322
 
   ao class       7B1        8B1        9B1       10B1       11B1       12B1 
    to_ s       0.086990   0.056564   0.010380   0.000000   0.000000   0.000000
 
   ao class      13B1       14B1       15B1       16B1       17B1       18B1 
 
   ao class      19B1       20B1       21B1       22B1       23B1       24B1 
 
   ao class      25B1       26B1       27B1       28B1       29B1       30B1 
 
   ao class      31B1       32B1       33B1       34B1       35B1 

                        B2  partial gross atomic populations
   ao class       1B2        2B2        3B2        4B2        5B2        6B2 
    to_ s       2.000000   2.000000   2.000000   2.000000   2.000000   2.000000
 
   ao class       7B2        8B2        9B2       10B2       11B2       12B2 
    to_ s       2.000000   2.000000   2.000000   0.000000   0.000000   0.000000
 
   ao class      13B2       14B2       15B2       16B2       17B2       18B2 
 
   ao class      19B2       20B2       21B2       22B2       23B2       24B2 
 
   ao class      25B2       26B2       27B2       28B2       29B2       30B2 
 
   ao class      31B2       32B2       33B2       34B2       35B2       36B2 
 
   ao class      37B2       38B2       39B2       40B2       41B2       42B2 
 
   ao class      43B2       44B2       45B2       46B2       47B2       48B2 
 
   ao class      49B2       50B2       51B2       52B2       53B2       54B2 
 
   ao class      55B2       56B2 

                        A2  partial gross atomic populations
   ao class       1A2        2A2        3A2        4A2        5A2        6A2 
    to_ s       2.000000   1.787198   0.217557   0.000000   0.000000   0.000000
 
   ao class       7A2        8A2        9A2       10A2       11A2       12A2 
 
   ao class      13A2       14A2       15A2       16A2       17A2       18A2 
 
   ao class      19A2       20A2       21A2       22A2       23A2       24A2 
 
   ao class      25A2 


                        gross atomic populations
     ao           to_
      s        56.000000
    total      56.000000
 

 Total number of electrons:   56.00000000

 Mulliken population for:
DRT 1, state 01


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        A1  partial gross atomic populations
   ao class       1A1        2A1        3A1        4A1        5A1        6A1 
    to_ s       2.000000   2.000000   2.000000   2.000000   2.000000   2.000000
 
   ao class       7A1        8A1        9A1       10A1       11A1       12A1 
    to_ s       2.000000   2.000000   2.000000   2.000000   2.000000   2.000000
 
   ao class      13A1       14A1       15A1       16A1       17A1       18A1 
 
   ao class      19A1       20A1       21A1       22A1       23A1       24A1 
 
   ao class      25A1       26A1       27A1       28A1       29A1       30A1 
 
   ao class      31A1       32A1       33A1       34A1       35A1       36A1 
 
   ao class      37A1       38A1       39A1       40A1       41A1       42A1 
 
   ao class      43A1       44A1       45A1       46A1       47A1       48A1 
 
   ao class      49A1       50A1       51A1       52A1       53A1       54A1 
 
   ao class      55A1       56A1       57A1       58A1       59A1       60A1 
 
   ao class      61A1       62A1       63A1       64A1       65A1       66A1 
 
   ao class      67A1       68A1       69A1 

                        B1  partial gross atomic populations
   ao class       1B1        2B1        3B1        4B1        5B1        6B1 
    to_ s       2.000000   1.983641   1.940090   1.910136   1.154122   0.853322
 
   ao class       7B1        8B1        9B1       10B1       11B1       12B1 
    to_ s       0.086990   0.056564   0.010380   0.000000   0.000000   0.000000
 
   ao class      13B1       14B1       15B1       16B1       17B1       18B1 
 
   ao class      19B1       20B1       21B1       22B1       23B1       24B1 
 
   ao class      25B1       26B1       27B1       28B1       29B1       30B1 
 
   ao class      31B1       32B1       33B1       34B1       35B1 

                        B2  partial gross atomic populations
   ao class       1B2        2B2        3B2        4B2        5B2        6B2 
    to_ s       2.000000   2.000000   2.000000   2.000000   2.000000   2.000000
 
   ao class       7B2        8B2        9B2       10B2       11B2       12B2 
    to_ s       2.000000   2.000000   2.000000   0.000000   0.000000   0.000000
 
   ao class      13B2       14B2       15B2       16B2       17B2       18B2 
 
   ao class      19B2       20B2       21B2       22B2       23B2       24B2 
 
   ao class      25B2       26B2       27B2       28B2       29B2       30B2 
 
   ao class      31B2       32B2       33B2       34B2       35B2       36B2 
 
   ao class      37B2       38B2       39B2       40B2       41B2       42B2 
 
   ao class      43B2       44B2       45B2       46B2       47B2       48B2 
 
   ao class      49B2       50B2       51B2       52B2       53B2       54B2 
 
   ao class      55B2       56B2 

                        A2  partial gross atomic populations
   ao class       1A2        2A2        3A2        4A2        5A2        6A2 
    to_ s       2.000000   1.787198   0.217557   0.000000   0.000000   0.000000
 
   ao class       7A2        8A2        9A2       10A2       11A2       12A2 
 
   ao class      13A2       14A2       15A2       16A2       17A2       18A2 
 
   ao class      19A2       20A2       21A2       22A2       23A2       24A2 
 
   ao class      25A2 


                        gross atomic populations
     ao           to_
      s        56.000000
    total      56.000000
 

 Total number of electrons:   56.00000000

 !timer: mcscf                           cpu_time=    10.709 walltime=    15.611
