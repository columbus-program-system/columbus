 total ao core energy =  546.173514343
 MCSCF calculation performed for  4 DRTs.

 DRT  first state   no.of aver. states   weights
  1   ground state          1             0.200
  2   ground state          2             0.200 0.200
  3   ground state          1             0.200
  4   ground state          1             0.200

 DRT file header:
  title                                                                          
 Molecular symmetry group:    b2 
 Total number of electrons:    56
 Spin multiplicity:            1
 Number of active orbitals:   10
 Number of active electrons:  10
 Total number of CSFs:      9408

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a1 
 Total number of electrons:    56
 Spin multiplicity:            1
 Number of active orbitals:   10
 Number of active electrons:  10
 Total number of CSFs:      9996

 DRT file header:
  title                                                                          
 Molecular symmetry group:    b1 
 Total number of electrons:    55
 Spin multiplicity:            2
 Number of active orbitals:   10
 Number of active electrons:   9
 Total number of CSFs:     14112

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a1 
 Total number of electrons:    56
 Spin multiplicity:            3
 Number of active orbitals:   10
 Number of active electrons:  10
 Total number of CSFs:     14916

 Number of active-double rotations:        10
 Number of active-active rotations:         0
 Number of double-virtual rotations:     1155
 Number of active-virtual rotations:      252
 
 iter     emc (average)    demc       wnorm      knorm      apxde  qcoupl
    1   -455.4948064822  4.555E+02  5.015E-04  1.130E-03  3.742E-08  F   *not conv.*     
    2   -455.4948065355  5.327E-08  2.264E-04  4.530E-04  6.766E-09  F   *not conv.*     
    3   -455.4948065451  9.652E-09  1.007E-04  1.848E-04  1.236E-09  F   *not conv.*     

 final mcscf convergence values:
    4   -455.4948065469  1.765E-09  4.420E-05  7.591E-05  2.263E-10  F   *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 wt 0.200 total energy=     -455.478809369, rel. (eV)=   4.666961
   DRT #2 state # 1 wt 0.200 total energy=     -455.650316971, rel. (eV)=   0.000000
   DRT #2 state # 2 wt 0.200 total energy=     -455.431903907, rel. (eV)=   5.943324
   DRT #3 state # 1 wt 0.200 total energy=     -455.391739260, rel. (eV)=   7.036261
   DRT #4 state # 1 wt 0.200 total energy=     -455.521263229, rel. (eV)=   3.511732
   ------------------------------------------------------------


