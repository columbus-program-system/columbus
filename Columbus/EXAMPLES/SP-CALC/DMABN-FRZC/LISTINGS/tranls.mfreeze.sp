 program "tran"

 this program transforms one- and two-electron
 integral and density matrix arrays.

 references:  i. shavitt, in "methods of electronic structure theory",
                  h. f. schaefer, ed. (plenum, new york, 1977), p. 189.
              h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. symp. 15, 91 (1981).

 original author: Frank B. Brown.
 SIFS version:    Ron Shepard.

 version date: 20-feb-02

 This Version of Program TRAN is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              TRAN        **
     **    PROGRAM VERSION:      5.6b2       **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 workspace allocation parameters: lcore= 301465600

 === echo of the input records ===
 ------------------------------------------------------------------------
 &input
  LRC1MX=0
  LRC2MX=0
  FREEZE(1)=-1 -1 -1 -1 -1 -1 -1 -1
  FREEZE(78)=0 0 0 0 0
  FREEZE(118)=-1 -1 -1
  FREEZE(177)=0 0 0
 &end
 ------------------------------------------------------------------------

 program input parameters:

 prnopt    =     0, chkopt    =     0, ortopt    =     0, denopt    =     0
 mapin(1 ) =    -1, nsymao    =    -1, naopsy(1) =    -1, freeze(1) =    -1
 mapout(1) =    -1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     4, tr1e      =     0
 LDAMIN    =   127, LDAMAX    =  8191, LDAINC    =    64
 LRC1MX    =     0, LRC2MX    =     0, LRCSCR    = 32768

 THRESH    =  5.0000E-12  [cutoff threshold]

 file description                 unit filename
 ---- -----------                 ---- --------
   1: input file:                   5  tranin                                                      
   2: listing file:                 6  tranls                                                      
   3: input ao integral file:      30  aoints                                                      
   4: input mo density file:       30  modens                                                      
   5: output mo integral file:     31  moints                                                      
   6: output ao density file:      31  aodens                                                      
   7: scratch file:                34  trscr1                                                      
   8: ci drt file:                 33  cidrtfl                                                     
   9: orbital coefficient file:    32  mocoef                                                      
  10: da1 scratch file:            20  trda1s                                                      
  11: da2 scratch file:            21  trda2s                                                      
  12: split input 2-e ao file:     35  aoints2                                                     
  13: split input d2mo file:       35  modens2                                                     
  14: FSPLIT=2 output 2-e mo file: 31  moints2                                                     
  15: FSPLIT=2 output d2ao file:   31  aodens2                                                     
  16: input file:                   0                                                              


 input file header information:
 Hermit Integral Program : SIFS version  r24n13            16:31:16.794 06-Sep-12

 input energy(*) values:
 energy( 1)=  5.461735143427E+02, ietype=   -1,    core energy of type: Nuc.Rep.

 total ao core energy =   5.461735143427E+02

 nsym = 4 nbft= 204

 symmetry  =    1    2    3    4
 SLABEL(*) =  A1   B1   B2   A2 
 NBPSY(*)  =   82   35   62   25

 INFOAO(*) =          2      4096      3272      4096      2730         0

 input basis function labels, i:AOLAB(i)=
   1:  1C1s     2:  2C1s     3:  3C1s     4:  4C1pz    5:  5C1pz    6:  6C1d0    7:  7C1d2+   8:  8C2s     9:  9C2s    10: 10C2s  
  11: 11C2py   12: 12C2pz   13: 13C2py   14: 14C2pz   15: 15C2d1-  16: 16C2d0   17: 17C2d2+  18: 18C3s    19: 19C3s    20: 20C3s  
  21: 21C3py   22: 22C3pz   23: 23C3py   24: 24C3pz   25: 25C3d1-  26: 26C3d0   27: 27C3d2+  28: 28C4s    29: 29C4s    30: 30C4s  
  31: 31C4pz   32: 32C4pz   33: 33C4d0   34: 34C4d2+  35: 35C5s    36: 36C5s    37: 37C5s    38: 38C5pz   39: 39C5pz   40: 40C5d0 
  41: 41C5d2+  42: 42C6s    43: 43C6s    44: 44C6s    45: 45C6py   46: 46C6pz   47: 47C6py   48: 48C6pz   49: 49C6d1-  50: 50C6d0 
  51: 51C6d2+  52: 52N1s    53: 53N1s    54: 54N1s    55: 55N1pz   56: 56N1pz   57: 57N1d0   58: 58N1d2+  59: 59N2s    60: 60N2s  
  61: 61N2s    62: 62N2pz   63: 63N2pz   64: 64N2d0   65: 65N2d2+  66: 66H1s    67: 67H1s    68: 68H1py   69: 69H1pz   70: 70H2s  
  71: 71H2s    72: 72H2py   73: 73H2pz   74: 74H3s    75: 75H3s    76: 76H3py   77: 77H3pz   78: 78H4s    79: 79H4s    80: 80H4px 
  81: 81H4py   82: 82H4pz   83: 83C1px   84: 84C1px   85: 85C1d1+  86: 86C2px   87: 87C2px   88: 88C2d2-  89: 89C2d1+  90: 90C3px 
  91: 91C3px   92: 92C3d2-  93: 93C3d1+  94: 94C4px   95: 95C4px   96: 96C4d1+  97: 97C5px   98: 98C5px   99: 99C5d1+ 100:100C6px 
 101:101C6px  102:102C6d2- 103:103C6d1+ 104:104N1px  105:105N1px  106:106N1d1+ 107:107N2px  108:108N2px  109:109N2d1+ 110:110H1px 
 111:111H2px  112:112H3px  113:113H4s   114:114H4s   115:115H4px  116:116H4py  117:117H4pz  118:118C1py  119:119C1py  120:120C1d1-
 121:121C2s   122:122C2s   123:123C2s   124:124C2py  125:125C2pz  126:126C2py  127:127C2pz  128:128C2d1- 129:129C2d0  130:130C2d2+
 131:131C3s   132:132C3s   133:133C3s   134:134C3py  135:135C3pz  136:136C3py  137:137C3pz  138:138C3d1- 139:139C3d0  140:140C3d2+
 141:141C4py  142:142C4py  143:143C4d1- 144:144C5py  145:145C5py  146:146C5d1- 147:147C6s   148:148C6s   149:149C6s   150:150C6py 
 151:151C6pz  152:152C6py  153:153C6pz  154:154C6d1- 155:155C6d0  156:156C6d2+ 157:157N1py  158:158N1py  159:159N1d1- 160:160N2py 
 161:161N2py  162:162N2d1- 163:163H1s   164:164H1s   165:165H1py  166:166H1pz  167:167H2s   168:168H2s   169:169H2py  170:170H2pz 
 171:171H3s   172:172H3s   173:173H3py  174:174H3pz  175:175H4s   176:176H4s   177:177H4px  178:178H4py  179:179H4pz  180:180C1d2-
 181:181C2px  182:182C2px  183:183C2d2- 184:184C2d1+ 185:185C3px  186:186C3px  187:187C3d2- 188:188C3d1+ 189:189C4d2- 190:190C5d2-
 191:191C6px  192:192C6px  193:193C6d2- 194:194C6d1+ 195:195N1d2- 196:196N2d2- 197:197H1px  198:198H2px  199:199H3px  200:200H4s  
 201:201H4s   202:202H4px  203:203H4py  204:204H4pz 
 user-specified FREEZE(*) used in program tran.                                  

 transformation information:
 number of symmetry irreps in point group, nsym   =  4
 total number of basis functions,          nbft   =204
 number of orbitals to be transformed,     norbt  =185
 number of frozen core orbitals,           nfrzct = 11
 number of frozen virtual orbitals,        nfrzvt =  8

 symmetry blocking information:
 symmetry  =    1    2    3    4
 SLABEL(*) =  A1   B1   B2   A2 
 NBPSY(*)  =   82   35   62   25
 NMPSY(*)  =   69   35   56   25
 NFCPSY(*) =    8    0    3    0
 INFOMO=                     1                  4096                  3272
                  4096                  2730                     0
                     0                     0                     0
                     0
 IFMTM1,IFMTM2=                     0                     0
                   185

 mocoef file titles:
 MO-coefficients from mcscf.x                                                    
  total ao core energy = -101.311908370                                          
 dummy occupation 1.0 for active orbitals                                        
 transformed using mo_mult.x                                                     

 job title for this run:
 SIFS file created by program tran.      r24n13            16:31:47.239 06-Sep-12

 trmain: score =   0.000000000000E+00

 frzden: (tr(dao*sao)-2*nfrzct)= -1.0658E-13

 inoutp: segmentation information:
 in-core transformation space,   avcinc = 301351166
 address segment size,           sizesg = 301369971
 number of in-core blocks,       nincbk =        19
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0

 inoutp: approximate total flop counts:
           type      in-core   out-of-core
         ------  ------------  ------------
          total  26752071993.            0. grand total =  26752071993.

 input file sort statistics:
 length of records,           csort%LENDAR =  8191
 number of buckets,           csort%NBUK   =     1
 number of values per bucket, csort%NPBUK  =  5460

 fixed workspace needed for the ao sort,  itotal =     67183
 workspace available for da sort buffers, avcore = 301417528

 beginning the first half-sort of the input 1/r12    array elements...

   50517212 array elements in 18505 records were read by twoaof,
   50517212 array elements were written into  9253 da records of length    8191.
 !timer: twoaof required                 cpu_time=     4.511 walltime=     7.349

 output file header information:
 Hermit Integral Program : SIFS version  r24n13            16:31:16.794 06-Sep-12
 user-specified FREEZE(*) used in program tran.                                  
 MO-coefficients from mcscf.x                                                    
  total ao core energy = -101.311908370                                          
 dummy occupation 1.0 for active orbitals                                        
 transformed using mo_mult.x                                                     
 SIFS file created by program tran.      r24n13            16:31:47.239 06-Sep-12

 output energy(*) values:
 energy( 1)=  5.461735143427E+02, ietype=   -1,    core energy of type: Nuc.Rep.

 total mo core energy =   5.461735143427E+02

 inputc%NSYMMO = 4 norbt= 185

 symmetry  =    1    2    3    4
 SLABEL(*) =  A1   B1   B2   A2 
 NMOPSY(*) =   69   35   56   25

 INFOMO(*) =          1      4096      3272      4096      2730         0

 output orbital labels, i:MOLAB(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012  13:tout:013  14:tout:014  15:tout:015  16:tout:016  17:tout:017  18:tout:018  19:tout:019  20:tout:020
  21:tout:021  22:tout:022  23:tout:023  24:tout:024  25:tout:025  26:tout:026  27:tout:027  28:tout:028  29:tout:029  30:tout:030
  31:tout:031  32:tout:032  33:tout:033  34:tout:034  35:tout:035  36:tout:036  37:tout:037  38:tout:038  39:tout:039  40:tout:040
  41:tout:041  42:tout:042  43:tout:043  44:tout:044  45:tout:045  46:tout:046  47:tout:047  48:tout:048  49:tout:049  50:tout:050
  51:tout:051  52:tout:052  53:tout:053  54:tout:054  55:tout:055  56:tout:056  57:tout:057  58:tout:058  59:tout:059  60:tout:060
  61:tout:061  62:tout:062  63:tout:063  64:tout:064  65:tout:065  66:tout:066  67:tout:067  68:tout:068  69:tout:069  70:tout:070
  71:tout:071  72:tout:072  73:tout:073  74:tout:074  75:tout:075  76:tout:076  77:tout:077  78:tout:078  79:tout:079  80:tout:080
  81:tout:081  82:tout:082  83:tout:083  84:tout:084  85:tout:085  86:tout:086  87:tout:087  88:tout:088  89:tout:089  90:tout:090
  91:tout:091  92:tout:092  93:tout:093  94:tout:094  95:tout:095  96:tout:096  97:tout:097  98:tout:098  99:tout:099 100:tout:100
 101:tout:101 102:tout:102 103:tout:103 104:tout:104 105:tout:105 106:tout:106 107:tout:107 108:tout:108 109:tout:109 110:tout:110
 111:tout:111 112:tout:112 113:tout:113 114:tout:114 115:tout:115 116:tout:116 117:tout:117 118:tout:118 119:tout:119 120:tout:120
 121:tout:121 122:tout:122 123:tout:123 124:tout:124 125:tout:125 126:tout:126 127:tout:127 128:tout:128 129:tout:129 130:tout:130
 131:tout:131 132:tout:132 133:tout:133 134:tout:134 135:tout:135 136:tout:136 137:tout:137 138:tout:138 139:tout:139 140:tout:140
 141:tout:141 142:tout:142 143:tout:143 144:tout:144 145:tout:145 146:tout:146 147:tout:147 148:tout:148 149:tout:149 150:tout:150
 151:tout:151 152:tout:152 153:tout:153 154:tout:154 155:tout:155 156:tout:156 157:tout:157 158:tout:158 159:tout:159 160:tout:160
 161:tout:161 162:tout:162 163:tout:163 164:tout:164 165:tout:165 166:tout:166 167:tout:167 168:tout:168 169:tout:169 170:tout:170
 171:tout:171 172:tout:172 173:tout:173 174:tout:174 175:tout:175 176:tout:176 177:tout:177 178:tout:178 179:tout:179 180:tout:180
 181:tout:181 182:tout:182 183:tout:183 184:tout:184 185:tout:185

 tran1e: S1(*)    array: ITYPEA= 0 ITYPEB=   0 numaot= 5800 nummot=  185 nrecao=  2 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  2.200000000000E+01 fcormo=  2.200000000000E+01

 tran1e: V1(*)    array: ITYPEA= 0 ITYPEB=   2 numaot= 6194 nummot= 4966 nrecao=  2 nrecmo=  2
         fcorao=  0.000000000000E+00 fcore= -1.108344785286E+03 fcormo= -1.108344785286E+03

 tran1e: T1(*)    array: ITYPEA= 0 ITYPEB=   1 numaot= 5823 nummot= 4966 nrecao=  2 nrecmo=  2
         fcorao=  0.000000000000E+00 fcore=  3.769907621014E+02 fcormo=  3.769907621014E+02

 tran1e: X(*)     array: ITYPEA= 1 ITYPEB=   0 numaot= 4150 nummot= 3815 nrecao=  2 nrecmo=  2
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: Y(*)     array: ITYPEA= 1 ITYPEB=   1 numaot= 5618 nummot= 4739 nrecao=  2 nrecmo=  2
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: Z(*)     array: ITYPEA= 1 ITYPEB=   2 numaot= 6022 nummot= 4966 nrecao=  2 nrecmo=  2
         fcorao=  0.000000000000E+00 fcore=  3.877720612767E+01 fcormo=  3.877720612767E+01

 tran1e: XX(*)    array: ITYPEA= 1 ITYPEB=   3 numaot= 5946 nummot= 4966 nrecao=  2 nrecmo=  2
         fcorao=  0.000000000000E+00 fcore=  6.795320628075E-01 fcormo=  6.795320628075E-01

 tran1e: XY(*)    array: ITYPEA= 1 ITYPEB=   4 numaot= 3947 nummot= 3685 nrecao=  2 nrecmo=  2
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: XZ(*)    array: ITYPEA= 1 ITYPEB=   5 numaot= 4296 nummot= 3815 nrecao=  2 nrecmo=  2
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: YY(*)    array: ITYPEA= 1 ITYPEB=   6 numaot= 5967 nummot= 4966 nrecao=  2 nrecmo=  2
         fcorao=  0.000000000000E+00 fcore=  6.482599531084E+01 fcormo=  6.482599531084E+01

 tran1e: YZ(*)    array: ITYPEA= 1 ITYPEB=   7 numaot= 5825 nummot= 4739 nrecao=  2 nrecmo=  2
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: ZZ(*)    array: ITYPEA= 1 ITYPEB=   8 numaot= 6040 nummot= 4966 nrecao=  2 nrecmo=  2
         fcorao=  0.000000000000E+00 fcore=  5.125933269904E+02 fcormo=  5.125933269904E+02

 tran1e: XXX(*)   array: ITYPEA= 1 ITYPEB=   9 numaot= 4160 nummot= 3815 nrecao=  2 nrecmo=  2
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: XXY(*)   array: ITYPEA= 1 ITYPEB=  10 numaot= 5547 nummot= 4739 nrecao=  2 nrecmo=  2
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: XXZ(*)   array: ITYPEA= 1 ITYPEB=  11 numaot= 6182 nummot= 4966 nrecao=  2 nrecmo=  2
         fcorao=  0.000000000000E+00 fcore=  1.136408645658E+00 fcormo=  1.136408645658E+00

 tran1e: XYY(*)   array: ITYPEA= 1 ITYPEB=  12 numaot= 4093 nummot= 3815 nrecao=  2 nrecmo=  2
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: XYZ(*)   array: ITYPEA= 1 ITYPEB=  13 numaot= 4079 nummot= 3685 nrecao=  2 nrecmo=  2
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: XZZ(*)   array: ITYPEA= 1 ITYPEB=  14 numaot= 4303 nummot= 3815 nrecao=  2 nrecmo=  2
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: YYY(*)   array: ITYPEA= 1 ITYPEB=  15 numaot= 5651 nummot= 4739 nrecao=  2 nrecmo=  2
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: YYZ(*)   array: ITYPEA= 1 ITYPEB=  16 numaot= 6181 nummot= 4966 nrecao=  2 nrecmo=  2
         fcorao=  0.000000000000E+00 fcore= -1.598383891248E+00 fcormo= -1.598383891248E+00

 tran1e: YZZ(*)   array: ITYPEA= 1 ITYPEB=  17 numaot= 5836 nummot= 4739 nrecao=  2 nrecmo=  2
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: ZZZ(*)   array: ITYPEA= 1 ITYPEB=  18 numaot= 6045 nummot= 4966 nrecao=  2 nrecmo=  2
         fcorao=  0.000000000000E+00 fcore=  2.868992648545E+03 fcormo=  2.868992648545E+03

 tran1e: XXXX(*)  array: ITYPEA= 1 ITYPEB=  19 numaot= 5954 nummot= 4966 nrecao=  2 nrecmo=  2
         fcorao=  0.000000000000E+00 fcore=  1.015381353538E-01 fcormo=  1.015381353538E-01

 tran1e: XXXY(*)  array: ITYPEA= 1 ITYPEB=  20 numaot= 3983 nummot= 3685 nrecao=  2 nrecmo=  2
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: XXXZ(*)  array: ITYPEA= 1 ITYPEB=  21 numaot= 4302 nummot= 3815 nrecao=  2 nrecmo=  2
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: XXYY(*)  array: ITYPEA= 1 ITYPEB=  22 numaot= 5824 nummot= 4966 nrecao=  2 nrecmo=  2
         fcorao=  0.000000000000E+00 fcore=  2.120910589669E+00 fcormo=  2.120910589669E+00

 tran1e: XXYZ(*)  array: ITYPEA= 1 ITYPEB=  23 numaot= 5756 nummot= 4739 nrecao=  2 nrecmo=  2
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: XXZZ(*)  array: ITYPEA= 1 ITYPEB=  24 numaot= 6185 nummot= 4966 nrecao=  2 nrecmo=  2
         fcorao=  0.000000000000E+00 fcore=  1.474852387400E+01 fcormo=  1.474852387400E+01

 tran1e: XYYY(*)  array: ITYPEA= 1 ITYPEB=  25 numaot= 3985 nummot= 3685 nrecao=  2 nrecmo=  2
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: XYYZ(*)  array: ITYPEA= 1 ITYPEB=  26 numaot= 4226 nummot= 3815 nrecao=  2 nrecmo=  2
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: XYZZ(*)  array: ITYPEA= 1 ITYPEB=  27 numaot= 4083 nummot= 3685 nrecao=  2 nrecmo=  2
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: XZZZ(*)  array: ITYPEA= 1 ITYPEB=  28 numaot= 4309 nummot= 3815 nrecao=  2 nrecmo=  2
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: YYYY(*)  array: ITYPEA= 1 ITYPEB=  29 numaot= 6002 nummot= 4966 nrecao=  2 nrecmo=  2
         fcorao=  0.000000000000E+00 fcore=  3.558811928694E+02 fcormo=  3.558811928694E+02

 tran1e: YYYZ(*)  array: ITYPEA= 1 ITYPEB=  30 numaot= 5835 nummot= 4739 nrecao=  2 nrecmo=  2
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: YYZZ(*)  array: ITYPEA= 1 ITYPEB=  31 numaot= 6193 nummot= 4966 nrecao=  2 nrecmo=  2
         fcorao=  0.000000000000E+00 fcore=  7.340891741471E+02 fcormo=  7.340891741471E+02

 tran1e: YZZZ(*)  array: ITYPEA= 1 ITYPEB=  32 numaot= 5843 nummot= 4739 nrecao=  2 nrecmo=  2
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: YZZZ(*)  array: ITYPEA= 1 ITYPEB=  33 numaot= 6050 nummot= 4966 nrecao=  2 nrecmo=  2
         fcorao=  0.000000000000E+00 fcore=  2.951117880802E+04 fcormo=  2.951117880802E+04

 tran1e: Im(lx)   array: ITYPEA= 2 ITYPEB=   6 numaot= 5838 nummot= 4739 nrecao=  2 nrecmo=  2
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: Im(ly)   array: ITYPEA= 2 ITYPEB=   7 numaot= 4282 nummot= 3815 nrecao=  2 nrecmo=  2
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: Im(lz)   array: ITYPEA= 2 ITYPEB=   8 numaot= 3260 nummot= 3685 nrecao=  1 nrecmo=  2
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: transformation of the input 1-e arrays is complete.
 frozen core density AO

 vfcout: VFC(*)   array, ITYPEA= 0 ITYPEB=   4 nummot= 4966 nrecmo=  2
         fcormo=  8.386860047216E+01

 beginning the last half-sort of the in-core 1/r12    array elements...

 srtinc:  50517212 array elements were read, and          56885553 array elements were written in  1737 records.
 !timer: srtinc required                 cpu_time=     1.660 walltime=     4.147

 beginning the transformation of the in-core 1/r12    array elements...
 executing in-core transformation trainc

 trainc:  56885553 sorted array elements were read,  37634966 transformed array elements were written.
 !timer: trainc required                 cpu_time=     7.636 walltime=    10.326

 1-e and 2-e transformation complete.

 trmain:   37634966 transformed 1/r12    array elements were written in   13786 records.

 !timer: tran required                   cpu_time=    13.839 walltime=    22.246
