1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      09/20/00    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

================================================================================
four external integ    0.25 MB location: local disk    
three external inte    0.25 MB location: local disk    
four external integ    0.25 MB location: local disk    
three external inte    0.25 MB location: local disk    
diagonal integrals     0.12 MB location: local disk    
off-diagonal integr    0.12 MB location: local disk    
 nsubmx= 16 lenci= 1311
global arrays:        43263   (    0.33 MB)
vdisk:                    0   (    0.00 MB)
drt:                 252658   (    0.96 MB)
================================================================================
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core            7999999 DP per process
 CIUDG version 5.9.3 (05-Dec-2002)
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 3,
  GSET = 3,
  NROOT = 1
  IVMODE = 3
  NBKITR = 1
  NVBKMN = 2
  NVBKMX = 7
  RTOLBK = 1e-4,1e-4,
  NITER = 90
  NVCIMN = 4
  NVCIMX = 16
  RTOLCI = 1e-3,1e-3,
  IDEN  = 1
  CSFPRN = 10,
 /&end
 ------------------------------------------------------------------------
 bummer (warning):2:changed keyword: nbkitr=         0
 bummer (warning):changed keyword: davcor=         0

 ** list of control variables **
 nrfitr =   30      nvrfmx =    7      nvrfmn =    4
 lvlprt =    0      nroot  =    1      noldv  =    0      noldhv =    0
 nunitv =    1      ntype  =    3      nbkitr =    0      niter  =   90
 ivmode =    3      vout   =    1      istrt  =    0      iortls =    0
 nvbkmx =    7      ibktv  =   -1      ibkthv =   -1      frcsub =    0
 nvcimx =    7      icitv  =   -1      icithv =   -1      maxseg =    4
 iden   =    1      itran  =    0      froot  =    0      rtmode =    0
 ftcalc =    1      lrtshift=1.0000    ncouple=    1      skipso  =   F
 ncorel =    0      csfprn  =  10      ctol   = 1.00E-02  davcor  =   0


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-04    1.000E-03
 This is a AQCC calculation USING the mrci-code /  March 1997 (tm)
 =============================================
 ==========================================

 units and filenames:
    1: ( 6)    ciudgls                                                     
    2: ( 5)    ciudgin                                                     
    3: ( 7)    ciudgsm                                                     
    4: ( 4)    cihdiag                                                     
    5: ( 3)    cihpseu                                                     
    6: (10)    cihvfl                                                      
    7: (11)    civfl                                                       
    8: (12)    diagint                                                     
    9: (13)    ofdgint                                                     
   10: (17)    cidrtfl                                                     
   11: (20)    ciftdfl                                                     
   12: (21)    ciftofl                                                     
   13: (24)    ciftifl                                                     
   14: ( 8)    ciflind                                                     
   15: (15)    civin                                                       
   16: (16)    civout                                                      
   18: (18)    d1fl                                                        
   20: (53)    cifvfl                                                      
   21: (25)    ciscr4                                                      
   22: (41)    ciftotd                                                     
   23: (42)    flacpfd                                                     
   26: (26)    ciscr5                                                      
   27: (47)    restart                                                     
   28: (48)    civout                                                      
   29: (49)    cirefv                                                      
   31: (31)    fil4w                                                       
   32: (32)    fil4x                                                       
   33: (33)    fil3w                                                       
   34: (34)    fil3x                                                       
   36: (36)    aoints                                                      
   37: (37)    aoints2                                                     
   38: (38)    drtfil                                                      
   39: (50)    cisrtif                                                     
   42: (50)    mocoef                                                      
   45: (52)    nocoef_ci                                                   
   46: (53)    civfl_restart                                               
 ------------------------------------------------------------------------

 workspace allocation information: lcore=   7999999 mem1=1108557832 ifirst=-264082442

 integral file titles:
 Hermit Integral Program : SIFS version  hochtor2        Thu Jun  5 14:25:35 2003
 h2o pvdz                                                                        
 mo coefficients generated by scfpq                                              
 SIFS file created by program tran.      hochtor2        Thu Jun  5 14:25:35 2003

 core energy values from the integral file:
 energy( 1)=  9.187211027960E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -8.346734396832E+01, ietype=    5,   fcore energy of type: Vref(*) 

 total core repulsion energy = -7.428013294036E+01

 drt header information:
 h2o pvdz                                                                        
 spnorb, spnodd, lxyzir,hmult F F 0 0 0 0
 nmot  =    24 niot  =     5 nfct  =     0 nfvt  =     0
 nrow  =    20 nsym  =     4 ssym  =     1 lenbuf=  1600
 nwalk,xbar:         31        1        5       10       15
 nvalwt,nvalw:       31        1        5       10       15
 ncsft:            1311
 total number of valid internal walks:      31
 nvalz,nvaly,nvalx,nvalw =        1       5      10      15

 cisrt info file parameters:
 file number  12 blocksize   4095
 mxbld   4095
 nd4ext,nd2ext,nd0ext   380   190    30
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int     5955     5445     2496      618       66        0        0        0
 minbl4,minbl3,maxbl2   195   186   198
 maxbuf 32767
 number of external orbitals per symmetry block:   8   6   3   2
 nmsym   4 number of internal orbitals   5

 formula file title:
 Hermit Integral Program : SIFS version  hochtor2        Thu Jun  5 14:25:35 2003
 h2o pvdz                                                                        
 mo coefficients generated by scfpq                                              
 SIFS file created by program tran.      hochtor2        Thu Jun  5 14:25:35 2003
 h2o pvdz                                                                        
 file nmb.  24 block size     0
 pthz,pthy,pthx,pthw:     1     5    10    15 total internal walks:      31
 maxlp3,n3xtlp,n2xtlp,n1xtlp,n0xtlp,n2lp,n1lp,n0lp     0     0     0     0
 orbsym(*)= 1 1 2 1 3

 setref:        1 references kept,
                0 references were marked as invalid, out of
                1 total.
 limcnvrt: found 1 valid internal walksout of  1
  walks (skipping trailing invalids)
  ... adding  1 segmentation marks segtype= 1
 limcnvrt: found 5 valid internal walksout of  5
  walks (skipping trailing invalids)
  ... adding  5 segmentation marks segtype= 2
 limcnvrt: found 10 valid internal walksout of  10
  walks (skipping trailing invalids)
  ... adding  10 segmentation marks segtype= 3
 limcnvrt: found 15 valid internal walksout of  15
  walks (skipping trailing invalids)
  ... adding  15 segmentation marks segtype= 4

 number of external paths / symmetry
 vertex x      47      54      36      34
 vertex w      66      54      36      34



                   segmentation summary for type diagonal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1           1|         1|         0|         1|         0|         1|
 -------------------------------------------------------------------------------
  Y 2           5|        33|         1|         5|         1|         2|
 -------------------------------------------------------------------------------
  X 3          10|       445|        34|        10|         6|         3|
 -------------------------------------------------------------------------------
  W 4          15|       832|       479|        15|        16|         4|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>      1311


 297 dimension of the ci-matrix ->>>      1311


 297 dimension of the ci-matrix ->>>         7

                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      10       1        445          1      10       1
     2  4   1    25      two-ext wz   2X  4 1      15       1        832          1      15       1
     3  4   3    26      two-ext wx   2X  4 3      15      10        832        445      15      10
     4  2   1    11      one-ext yz   1X  2 1       5       1         33          1       5       1
     5  3   2    15      1ex3ex  yx   3X  3 2      10       5        445         33      10       5
     6  4   2    16      1ex3ex  yw   3X  4 2      15       5        832         33      15       5
     7  1   1     1      allint zz    OX  1 1       1       1          1          1       1       1
     8  2   2     5      0ex2ex yy    OX  2 2       5       5         33         33       5       5
     9  3   3     6      0ex2ex xx    OX  3 3      10      10        445        445      10      10
    10  4   4     7      0ex2ex ww    OX  4 4      15      15        832        832      15      15
    11  1   1    75      dg-024ext z  DG  1 1       1       1          1          1       1       1
    12  2   2    45      4exdg024 y   DG  2 2       5       5         33         33       5       5
    13  3   3    46      4exdg024 x   DG  3 3      10      10        445        445      10      10
    14  4   4    47      4exdg024 w   DG  4 4      15      15        832        832      15      15
----------------------------------------------------------------------------------------------------
 DIAGEL COUNTS: DG0X,DG2X,DG4X= 122 69 30
 diagonal elements written to file   4

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:         1 2x:         0 4x:         0
All internal counts: zz :         0 yy:         0 xx:         0 ww:         0
One-external counts: yz :         0 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         0    task #   2:         0    task #   3:         0    task #   4:         0
task #   5:         0    task #   6:         0    task #   7:        -1    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:         0    task #  12:         0
task #  13:         0    task #  14:         0    task #
 reference space has dimension       1

    root           eigenvalues
    ----           ------------
       1         -76.0267601011

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------


         vector  1 from unit 11 written to unit 49 filename cirefv                                                      

         vector  1 from unit 11 written to unit 49 filename cirefv                                                      

    ------------------------------------------------------------

 setopt2: mraqcc energy evaluation      gvalue=  0.377777778

    ------------------------------------------------------------


    reference energies:

  reference state used is:   1with energy      -76.0267601011

  ### active excitation selection ###

 Inactive orbitals: 1 2 3 4 5
    there are    1 all-active excitations of which    1 are references.

  ### end active excitation selection ###


################################################################################
 one electron density matrix calculation 
################################################################################


 workspace allocation information: mxcore= 7999996
 the acpf density matrix will be calculated

 space required:

    space required for calls in multd2:
       onex          112
       allin           0
       diagon        330
    max.      ---------
       maxnex        330

    total core space usage:
       maxnex        330
    max k-seg        832
    max k-ind         15
              ---------
       totmax       2024
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       0  DYX=       0  DYW=       0
   D0Z=       0  D0Y=       0  D0X=       0  D0W=       0
  DDZI=       1 DDYI=       0 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              1311
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               7
 number of roots to converge:                             1
 number of iterations:                                   90
 residual norm convergence criteria:               0.001000

          starting ci iteration   1

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       122 2x:        69 4x:        30
All internal counts: zz :         0 yy:         6 xx:        24 ww:        55
One-external counts: yz :         9 yx:        82 yw:       111
Two-external counts: yy :        19 ww:        84 xx:        56 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:         8
task #   5:        75    task #   6:       104    task #   7:        -1    task #   8:         5
task #   9:        21    task #  10:        52    task #  11:         0    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  1  1    -76.0267601011 -2.2204E-15  2.5326E-01  9.9884E-01  1.0000E-03

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.00   0.00   0.00   0.00
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.01   0.01   0.00   0.01
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.01   0.00   0.00   0.01
   10    7    0   0.02   0.00   0.00   0.02
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.01   0.00   0.00   0.01
   14   47    0   0.00   0.00   0.00   0.00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1400s 

          starting ci iteration   2

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       122 2x:        69 4x:        30
All internal counts: zz :         0 yy:         6 xx:        24 ww:        55
One-external counts: yz :         9 yx:        82 yw:       111
Two-external counts: yy :        19 ww:        84 xx:        56 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:         8
task #   5:        75    task #   6:       104    task #   7:        -1    task #   8:         5
task #   9:        21    task #  10:        52    task #  11:         0    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97616924    -0.21701065

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  2  1    -76.2257785482  1.9902E-01  6.4820E-03  1.5892E-01  1.0000E-03
 mraqcc  #  2  2    -71.9997616038 -2.2804E+00  0.0000E+00  1.7212E+00  1.0000E-03

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.00   0.00   0.00   0.00
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.02   0.00   0.00   0.02
    7    1    0   0.00   0.00   0.00   0.00
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.01   0.00   0.00   0.01
   10    7    0   0.02   0.00   0.00   0.02
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.01   0.00   0.00   0.01
   14   47    0   0.01   0.00   0.00   0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1300s 
time spent in multnx:                   0.1300s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration   3

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       122 2x:        69 4x:        30
All internal counts: zz :         0 yy:         6 xx:        24 ww:        55
One-external counts: yz :         9 yx:        82 yw:       111
Two-external counts: yy :        19 ww:        84 xx:        56 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:         8
task #   5:        75    task #   6:       104    task #   7:        -1    task #   8:         5
task #   9:        21    task #  10:        52    task #  11:         0    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.97385796     0.03606324    -0.22427690

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  3  1    -76.2379965650  1.2218E-02  2.9939E-04  3.1517E-02  1.0000E-03
 mraqcc  #  3  2    -73.2378522794  1.2381E+00  0.0000E+00  1.8251E+00  1.0000E-03
 mraqcc  #  3  3    -72.1160465783 -2.1641E+00  0.0000E+00  1.9060E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.00   0.00   0.00   0.00
    5   15    0   0.02   0.01   0.00   0.02
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.01   0.00   0.00   0.01
    9    6    0   0.02   0.00   0.00   0.02
   10    7    0   0.01   0.00   0.00   0.01
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.01   0.00   0.00   0.01
   13   46    0   0.00   0.00   0.00   0.00
   14   47    0   0.01   0.00   0.00   0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1300s 
time spent in multnx:                   0.1300s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration   4

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       122 2x:        69 4x:        30
All internal counts: zz :         0 yy:         6 xx:        24 ww:        55
One-external counts: yz :         9 yx:        82 yw:       111
Two-external counts: yy :        19 ww:        84 xx:        56 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:         8
task #   5:        75    task #   6:       104    task #   7:        -1    task #   8:         5
task #   9:        21    task #  10:        52    task #  11:         0    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.97310693     0.10578717     0.04193210     0.20028401

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  4  1    -76.2386990507  7.0249E-04  2.3009E-05  7.5651E-03  1.0000E-03
 mraqcc  #  4  2    -74.1384499148  9.0060E-01  0.0000E+00  1.2045E+00  1.0000E-03
 mraqcc  #  4  3    -73.0585080712  9.4246E-01  0.0000E+00  1.6584E+00  1.0000E-04
 mraqcc  #  4  4    -71.6805667462 -2.5996E+00  0.0000E+00  2.2447E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.01   0.00   0.00   0.01
    9    6    0   0.02   0.00   0.00   0.02
   10    7    0   0.02   0.00   0.00   0.02
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.00   0.00   0.00   0.00
   14   47    0   0.01   0.00   0.00   0.00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1300s 
time spent in multnx:                   0.1200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1400s 

          starting ci iteration   5

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       122 2x:        69 4x:        30
All internal counts: zz :         0 yy:         6 xx:        24 ww:        55
One-external counts: yz :         9 yx:        82 yw:       111
Two-external counts: yy :        19 ww:        84 xx:        56 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:         8
task #   5:        75    task #   6:       104    task #   7:        -1    task #   8:         5
task #   9:        21    task #  10:        52    task #  11:         0    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     0.97299034     0.05500315     0.09434487    -0.08167940     0.18625780

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  5  1    -76.2387418494  4.2799E-05  1.4262E-06  2.1420E-03  1.0000E-03
 mraqcc  #  5  2    -74.8630857505  7.2464E-01  0.0000E+00  9.9627E-01  1.0000E-03
 mraqcc  #  5  3    -74.0525464423  9.9404E-01  0.0000E+00  1.3762E+00  1.0000E-04
 mraqcc  #  5  4    -72.6297269404  9.4916E-01  0.0000E+00  1.4152E+00  1.0000E-04
 mraqcc  #  5  5    -71.5032636204 -2.7769E+00  0.0000E+00  2.3940E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.00   0.00   0.00   0.00
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.01   0.00   0.00   0.01
   10    7    0   0.02   0.00   0.00   0.02
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.01   0.00   0.00   0.01
   14   47    0   0.00   0.00   0.00   0.00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.1300s 

          starting ci iteration   6

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       122 2x:        69 4x:        30
All internal counts: zz :         0 yy:         6 xx:        24 ww:        55
One-external counts: yz :         9 yx:        82 yw:       111
Two-external counts: yy :        19 ww:        84 xx:        56 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:         8
task #   5:        75    task #   6:       104    task #   7:        -1    task #   8:         5
task #   9:        21    task #  10:        52    task #  11:         0    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.97297937     0.06089776     0.05137162    -0.09214784    -0.09994317    -0.16877115

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  6  1    -76.2387446986  2.8492E-06  7.0639E-08  4.4069E-04  1.0000E-03
 mraqcc  #  6  2    -74.9158999046  5.2814E-02  0.0000E+00  8.9119E-01  1.0000E-03
 mraqcc  #  6  3    -74.3785710781  3.2602E-01  0.0000E+00  1.1068E+00  1.0000E-04
 mraqcc  #  6  4    -73.1783402538  5.4861E-01  0.0000E+00  1.8376E+00  1.0000E-04
 mraqcc  #  6  5    -72.6140612756  1.1108E+00  0.0000E+00  1.3010E+00  1.0000E-04
 mraqcc  #  6  6    -71.3244442839 -2.9557E+00  0.0000E+00  2.3124E+00  1.0000E-04


 mraqcc   convergence criteria satisfied after  6 iterations.

 final mraqcc   convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  6  1    -76.2387446986  2.8492E-06  7.0639E-08  4.4069E-04  1.0000E-03
 mraqcc  #  6  2    -74.9158999046  5.2814E-02  0.0000E+00  8.9119E-01  1.0000E-03
 mraqcc  #  6  3    -74.3785710781  3.2602E-01  0.0000E+00  1.1068E+00  1.0000E-04
 mraqcc  #  6  4    -73.1783402538  5.4861E-01  0.0000E+00  1.8376E+00  1.0000E-04
 mraqcc  #  6  5    -72.6140612756  1.1108E+00  0.0000E+00  1.3010E+00  1.0000E-04
 mraqcc  #  6  6    -71.3244442839 -2.9557E+00  0.0000E+00  2.3124E+00  1.0000E-04

####################CIUDGINFO####################

   aqcc vector at position   1 energy=  -76.238744698556

################END OF CIUDGINFO################


 a4den factor =  1.034309471 for root   1
    1 of the   7 expansion vectors are transformed.
    1 of the   6 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
 maximum overlap with reference  1(overlap=  0.972979371)
 reference energy= -76.0267601
 total aqcc energy= -76.2387447
 diagonal element shift (lrtshift) =  -0.211984597

information on vector: 1from unit 11 written to unit 16filename civout                                                      


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -76.2387446986

                                                       internal orbitals

                                          level       1    2    3    4    5

                                          orbital     1    2   12    3   19

                                         symmetry   a1   a1   b1   a1   b2 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1  0.972979                        +-   +-   +-   +-   +- 
 x   1  1      35 -0.014821    1( a1 )   1( b2 )   +-   +-   +-    -    - 
 x   1  1      36  0.018406    2( a1 )   1( b2 )   +-   +-   +-    -    - 
 x   1  1      37 -0.032726    3( a1 )   1( b2 )   +-   +-   +-    -    - 
 x   1  1      46 -0.011549    4( a1 )   2( b2 )   +-   +-   +-    -    - 
 x   1  1      47 -0.011937    5( a1 )   2( b2 )   +-   +-   +-    -    - 
 x   1  1      54 -0.010624    4( a1 )   3( b2 )   +-   +-   +-    -    - 
 x   1  1      55 -0.010686    5( a1 )   3( b2 )   +-   +-   +-    -    - 
 x   1  1      57 -0.014052    7( a1 )   3( b2 )   +-   +-   +-    -    - 
 x   1  1      60 -0.015229    2( b1 )   1( a2 )   +-   +-   +-    -    - 
 x   1  1      71  0.026866    1( b1 )   1( b2 )   +-   +-    -   +-    - 
 x   1  1      72  0.015085    2( b1 )   1( b2 )   +-   +-    -   +-    - 
 x   1  1      73 -0.027351    3( b1 )   1( b2 )   +-   +-    -   +-    - 
 x   1  1      75 -0.010047    5( b1 )   1( b2 )   +-   +-    -   +-    - 
 x   1  1      78  0.014258    2( b1 )   2( b2 )   +-   +-    -   +-    - 
 x   1  1      90 -0.020900    2( a1 )   1( a2 )   +-   +-    -   +-    - 
 x   1  1      93  0.011664    5( a1 )   1( a2 )   +-   +-    -   +-    - 
 x   1  1      94  0.011188    6( a1 )   1( a2 )   +-   +-    -   +-    - 
 x   1  1      98 -0.010388    2( a1 )   2( a2 )   +-   +-    -   +-    - 
 x   1  1     102  0.011352    6( a1 )   2( a2 )   +-   +-    -   +-    - 
 x   1  1     104  0.012249    8( a1 )   2( a2 )   +-   +-    -   +-    - 
 x   1  1     105  0.010514    1( a1 )   1( b1 )   +-   +-    -    -   +- 
 x   1  1     107  0.025377    3( a1 )   1( b1 )   +-   +-    -    -   +- 
 x   1  1     115  0.023175    3( a1 )   2( b1 )   +-   +-    -    -   +- 
 x   1  1     116  0.020697    4( a1 )   2( b1 )   +-   +-    -    -   +- 
 x   1  1     119  0.011709    7( a1 )   2( b1 )   +-   +-    -    -   +- 
 x   1  1     122  0.013170    2( a1 )   3( b1 )   +-   +-    -    -   +- 
 x   1  1     123 -0.018603    3( a1 )   3( b1 )   +-   +-    -    -   +- 
 x   1  1     139 -0.012460    3( a1 )   5( b1 )   +-   +-    -    -   +- 
 x   1  1     159 -0.015359    1( a1 )   1( b2 )   +-    -   +-   +-    - 
 x   1  1     163  0.015299    5( a1 )   1( b2 )   +-    -   +-   +-    - 
 x   1  1     196  0.010968    1( a1 )   3( a1 )   +-    -   +-    -   +- 
 x   1  1     242 -0.011069    1( a1 )   1( b1 )   +-    -    -   +-   +- 
 x   1  1     258  0.010518    1( a1 )   3( b1 )   +-    -    -   +-   +- 
 w   1  1     480 -0.011833    1( a1 )   1( a1 )   +-   +-   +-   +-      
 w   1  1     481  0.010998    1( a1 )   2( a1 )   +-   +-   +-   +-      
 w   1  1     494 -0.013243    5( a1 )   5( a1 )   +-   +-   +-   +-      
 w   1  1     496  0.011024    2( a1 )   6( a1 )   +-   +-   +-   +-      
 w   1  1     513 -0.010209    6( a1 )   8( a1 )   +-   +-   +-   +-      
 w   1  1     537 -0.049445    1( b2 )   1( b2 )   +-   +-   +-   +-      
 w   1  1     541 -0.010152    2( b2 )   3( b2 )   +-   +-   +-   +-      
 w   1  1     542 -0.012682    3( b2 )   3( b2 )   +-   +-   +-   +-      
 w   1  1     544 -0.011103    1( a2 )   2( a2 )   +-   +-   +-   +-      
 w   1  1     545 -0.012370    2( a2 )   2( a2 )   +-   +-   +-   +-      
 w   1  1     547 -0.013844    2( a1 )   1( b2 )   +-   +-   +-   +     - 
 w   1  1     548  0.035896    3( a1 )   1( b2 )   +-   +-   +-   +     - 
 w   1  1     549 -0.010031    4( a1 )   1( b2 )   +-   +-   +-   +     - 
 w   1  1     550  0.010036    5( a1 )   1( b2 )   +-   +-   +-   +     - 
 w   1  1     571  0.012600    2( b1 )   1( a2 )   +-   +-   +-   +     - 
 w   1  1     577  0.010377    2( b1 )   2( a2 )   +-   +-   +-   +     - 
 w   1  1     582 -0.015989    1( a1 )   1( a1 )   +-   +-   +-        +- 
 w   1  1     583  0.016644    1( a1 )   2( a1 )   +-   +-   +-        +- 
 w   1  1     584 -0.015298    2( a1 )   2( a1 )   +-   +-   +-        +- 
 w   1  1     585 -0.012873    1( a1 )   3( a1 )   +-   +-   +-        +- 
 w   1  1     586  0.017619    2( a1 )   3( a1 )   +-   +-   +-        +- 
 w   1  1     587 -0.034128    3( a1 )   3( a1 )   +-   +-   +-        +- 
 w   1  1     591 -0.014970    4( a1 )   4( a1 )   +-   +-   +-        +- 
 w   1  1     594 -0.016607    3( a1 )   5( a1 )   +-   +-   +-        +- 
 w   1  1     609 -0.011493    7( a1 )   7( a1 )   +-   +-   +-        +- 
 w   1  1     619 -0.018388    1( b1 )   2( b1 )   +-   +-   +-        +- 
 w   1  1     620 -0.025145    2( b1 )   2( b1 )   +-   +-   +-        +- 
 w   1  1     629  0.013185    2( b1 )   5( b1 )   +-   +-   +-        +- 
 w   1  1     644 -0.010516    3( b2 )   3( b2 )   +-   +-   +-        +- 
 w   1  1     648 -0.025900    1( b1 )   1( b2 )   +-   +-   +    +-    - 
 w   1  1     649 -0.015646    2( b1 )   1( b2 )   +-   +-   +    +-    - 
 w   1  1     650  0.027996    3( b1 )   1( b2 )   +-   +-   +    +-    - 
 w   1  1     652  0.011536    5( b1 )   1( b2 )   +-   +-   +    +-    - 
 w   1  1     655 -0.011167    2( b1 )   2( b2 )   +-   +-   +    +-    - 
 w   1  1     682  0.021275    1( a1 )   1( b1 )   +-   +-   +     -   +- 
 w   1  1     683 -0.023431    2( a1 )   1( b1 )   +-   +-   +     -   +- 
 w   1  1     684  0.019727    3( a1 )   1( b1 )   +-   +-   +     -   +- 
 w   1  1     690  0.019552    1( a1 )   2( b1 )   +-   +-   +     -   +- 
 w   1  1     691 -0.036982    2( a1 )   2( b1 )   +-   +-   +     -   +- 
 w   1  1     692  0.010880    3( a1 )   2( b1 )   +-   +-   +     -   +- 
 w   1  1     694  0.010148    5( a1 )   2( b1 )   +-   +-   +     -   +- 
 w   1  1     695  0.012749    6( a1 )   2( b1 )   +-   +-   +     -   +- 
 w   1  1     699  0.011871    2( a1 )   3( b1 )   +-   +-   +     -   +- 
 w   1  1     700 -0.020306    3( a1 )   3( b1 )   +-   +-   +     -   +- 
 w   1  1     731  0.013636    2( b2 )   1( a2 )   +-   +-   +     -   +- 
 w   1  1     736 -0.014450    1( a1 )   1( a1 )   +-   +-        +-   +- 
 w   1  1     737  0.019715    1( a1 )   2( a1 )   +-   +-        +-   +- 
 w   1  1     738 -0.027907    2( a1 )   2( a1 )   +-   +-        +-   +- 
 w   1  1     772 -0.028430    1( b1 )   1( b1 )   +-   +-        +-   +- 
 w   1  1     773 -0.028875    1( b1 )   2( b1 )   +-   +-        +-   +- 
 w   1  1     774 -0.035296    2( b1 )   2( b1 )   +-   +-        +-   +- 
 w   1  1     775  0.026090    1( b1 )   3( b1 )   +-   +-        +-   +- 
 w   1  1     776  0.012450    2( b1 )   3( b1 )   +-   +-        +-   +- 
 w   1  1     777 -0.021375    3( b1 )   3( b1 )   +-   +-        +-   +- 
 w   1  1     783  0.014948    2( b1 )   5( b1 )   +-   +-        +-   +- 
 w   1  1     786 -0.010244    5( b1 )   5( b1 )   +-   +-        +-   +- 
 w   1  1     799 -0.014771    1( a2 )   1( a2 )   +-   +-        +-   +- 
 w   1  1     802  0.026149    1( a1 )   1( b2 )   +-   +    +-   +-    - 
 w   1  1     803 -0.018238    2( a1 )   1( b2 )   +-   +    +-   +-    - 
 w   1  1     805  0.014172    4( a1 )   1( b2 )   +-   +    +-   +-    - 
 w   1  1     806 -0.021304    5( a1 )   1( b2 )   +-   +    +-   +-    - 
 w   1  1     807  0.016892    6( a1 )   1( b2 )   +-   +    +-   +-    - 
 w   1  1     819  0.011013    2( a1 )   3( b2 )   +-   +    +-   +-    - 
 w   1  1     832 -0.011410    1( b1 )   2( a2 )   +-   +    +-   +-    - 
 w   1  1     838 -0.016251    1( a1 )   1( a1 )   +-   +    +-    -   +- 
 w   1  1     839  0.021628    1( a1 )   2( a1 )   +-   +    +-    -   +- 
 w   1  1     840 -0.018982    2( a1 )   2( a1 )   +-   +    +-    -   +- 
 w   1  1     841 -0.016123    1( a1 )   3( a1 )   +-   +    +-    -   +- 
 w   1  1     846 -0.012548    3( a1 )   4( a1 )   +-   +    +-    -   +- 
 w   1  1     850  0.019816    3( a1 )   5( a1 )   +-   +    +-    -   +- 
 w   1  1     904  0.021010    1( a1 )   1( b1 )   +-   +     -   +-   +- 
 w   1  1     905 -0.012038    2( a1 )   1( b1 )   +-   +     -   +-   +- 
 w   1  1     907  0.012843    4( a1 )   1( b1 )   +-   +     -   +-   +- 
 w   1  1     908 -0.012074    5( a1 )   1( b1 )   +-   +     -   +-   +- 
 w   1  1     912  0.012549    1( a1 )   2( b1 )   +-   +     -   +-   +- 
 w   1  1     913 -0.021084    2( a1 )   2( b1 )   +-   +     -   +-   +- 
 w   1  1     915  0.014115    4( a1 )   2( b1 )   +-   +     -   +-   +- 
 w   1  1     919 -0.010392    8( a1 )   2( b1 )   +-   +     -   +-   +- 
 w   1  1     920 -0.021826    1( a1 )   3( b1 )   +-   +     -   +-   +- 
 w   1  1     921  0.014677    2( a1 )   3( b1 )   +-   +     -   +-   +- 
 w   1  1     924  0.017622    5( a1 )   3( b1 )   +-   +     -   +-   +- 
 w   1  1     953  0.012204    2( b2 )   1( a2 )   +-   +     -   +-   +- 
 w   1  1     958 -0.012231    1( a1 )   1( a1 )   +-        +-   +-   +- 
 w   1  1     959  0.014733    1( a1 )   2( a1 )   +-        +-   +-   +- 
 w   1  1     960 -0.013497    2( a1 )   2( a1 )   +-        +-   +-   +- 

 ci coefficient statistics:
           rq > 0.1                1
      0.1> rq > 0.01             118
     0.01> rq > 0.001            584
    0.001> rq > 0.0001           422
   0.0001> rq > 0.00001          140
  0.00001> rq > 0.000001          42
 0.000001> rq                      4
           all                  1311
NO coefficients and occupation numbers are written to nocoef_ci.1
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=       3  D0X=       9  D0W=      15
  DDZI=       1 DDYI=       9 DDXI=      26 DDWI=      34
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================
 root #  1: Scaling(2) with   1.03430947corresponding to ref #  1


*****   symmetry block  A1    *****

 occupation numbers of nos
   1.9999183      1.9851087      1.9673296      0.0242604      0.0110162
   0.0056078      0.0045158      0.0011489      0.0006090      0.0004983
   0.0000585


*****   symmetry block  B1    *****

 occupation numbers of nos
   1.9646606      0.0262557      0.0058604      0.0011749      0.0006767
   0.0005047      0.0000405


*****   symmetry block  B2    *****

 occupation numbers of nos
   1.9737044      0.0162107      0.0044112      0.0005502


*****   symmetry block  A2    *****

 occupation numbers of nos
   0.0051323      0.0007462


 total number of electrons =   10.0000000000



          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!


                        A1  partial gross atomic populations
   ao class       1A1        2A1        3A1        4A1        5A1        6A1 
    O1_ s       2.000572   1.689991  -0.051278   0.001802   0.005449   0.001325
    O1_ p       0.000089   0.041380   1.452113   0.010753   0.004382   0.000038
    O1_ d       0.000000   0.000187   0.002029   0.000304   0.000346   0.002810
    H1_ s      -0.000185   0.200928   0.521426   0.011838   0.000389   0.000297
    H1_ p      -0.000557   0.052623   0.043041  -0.000436   0.000451   0.001139

   ao class       7A1        8A1        9A1       10A1       11A1 
    O1_ s       0.000202   0.000221   0.000015   0.000011   0.000005
    O1_ p       0.000264   0.000183  -0.000001   0.000047   0.000005
    O1_ d       0.003584   0.000445   0.000015   0.000117   0.000001
    H1_ s       0.000047   0.000347   0.000129   0.000006   0.000031
    H1_ p       0.000419  -0.000047   0.000451   0.000317   0.000017

                        B1  partial gross atomic populations
   ao class       1B1        2B1        3B1        4B1        5B1        6B1 
    O1_ p       1.125024   0.013359   0.002033   0.000323   0.000003   0.000070
    O1_ d       0.008372   0.000494   0.002779   0.000421   0.000086   0.000000
    H1_ s       0.804278   0.012834   0.000626   0.000145   0.000293   0.000009
    H1_ p       0.026987  -0.000431   0.000422   0.000286   0.000293   0.000425

   ao class       7B1 
    O1_ p       0.000006
    O1_ d       0.000001
    H1_ s       0.000017
    H1_ p       0.000017

                        B2  partial gross atomic populations
   ao class       1B2        2B2        3B2        4B2 
    O1_ p       1.925426   0.015384   0.000106   0.000028
    O1_ d       0.001150   0.000046   0.003777   0.000077
    H1_ p       0.047129   0.000780   0.000528   0.000445

                        A2  partial gross atomic populations
   ao class       1A2        2A2 
    O1_ d       0.003818   0.000191
    H1_ p       0.001314   0.000555


                        gross atomic populations
     ao           O1_        H1_
      s         3.648315   1.553454
      p         4.591016   0.176167
      d         0.031049   0.000000
    total       8.270380   1.729620


 Total number of electrons:   10.00000000

