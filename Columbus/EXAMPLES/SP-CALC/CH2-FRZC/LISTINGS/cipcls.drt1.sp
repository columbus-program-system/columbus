
 program cipc      

 print the csf info for mrsdci wave functions

 written by: ron shepard

 version date: 06-jun-96

 This Version of Program cipc is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 workspace allocation parameters: lencor= 353894400 mem1=         0 ifirst=         1

 drt header information:
  cidrt_title                                                                    
 nmot  =    12 niot  =     5 nfct  =     0 nfvt  =     0
 nrow  =    26 nsym  =     1 ssym  =     1 lenbuf=  1600
 spnorb=     F spnodd=     F lxyzir(1:3)= 0 0 0
 nwalk,xbar:         43       22       21        0        0
 nvalwt,nvalw:       43       22       21        0        0
 ncsft:             169
 map(*)=     8  9 10 11 12  1  2  3  4  5  6  7
 mu(*)=      2  2  0  0  0
 syml(*) =   1  1  1  1  1
 rmo(*)=     1  2  3  4  5

 indx01:    43 indices saved in indxv(*)
 test nroots froot                      1                    -1
===================================ROOT # 1===================================

 rdhciv: CI vector file information:
  cidrt_title                                                                    
 energy computed by program ciudg.       r24n12            17:45:20.966 30-Aug-12

 lenrec =   32768 lenci =       169 ninfo =  6 nenrgy =  5 ntitle =  2

 Max. overlap with ref vector #        1
 Valid ci vector #        1
 Method:        0       99% overlap
 energy( 1)=  6.593365133910E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -3.444754749468E+01, ietype=    6,   fcore energy of type: H1(*)   
 energy( 3)= -9.676846526811E+00, ietype=    5,   fcore energy of type: Vref(*) 
 energy( 4)= -3.887824124496E+01, ietype=-1026,   total energy of type: MRSDCI  
 energy( 5)=  7.444384104435E-04, ietype=-2055, cnvginf energy of type: CI-Resid
==================================================================================
 test nroots froot                      2                    -1
===================================ROOT # 2===================================

 rdhciv: CI vector file information:
  cidrt_title                                                                    
 energy computed by program ciudg.       r24n12            17:45:20.966 30-Aug-12

 lenrec =   32768 lenci =       169 ninfo =  6 nenrgy =  7 ntitle =  2

 Max. overlap with ref vector #        2
 Valid ci vector #        2
 Method:        0       99% overlap
 energy( 1)=  6.593365133910E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -3.444754749468E+01, ietype=    6,   fcore energy of type: H1(*)   
 energy( 3)= -9.676846526811E+00, ietype=    5,   fcore energy of type: Vref(*) 
 energy( 4)= -3.882540573776E+01, ietype=-1026,   total energy of type: MRSDCI  
 energy( 5)=  3.703459431452E-04, ietype=-2055, cnvginf energy of type: CI-Resid
 energy( 6)=  2.160062738010E-06, ietype=-2056, cnvginf energy of type: CI-D.E. 
 energy( 7)=  9.368481525982E-08, ietype=-2057, cnvginf energy of type: CI-ApxDE
==================================================================================

 space is available for 117953807 coefficients.

 updated histogram parameters:
 csfmn = 0.0000E+00 csfmx = 1.0000E+00 fhist = 5.0000E-01 nhist =  20

 this program will print the csfs generated from
 the drt according to the following print options :

 1) run in batch mode: all valid roots are automatically
    analysed and csf info is printed by default contribution
    threshold 0.01 
 2) run in interactive mode
 3) generate files for cioverlap without symmetry
 4) generate files for cioverlap with symmetry

 input menu number [  1]:
================================================================================
===================================VECTOR # 1===================================
================================================================================


 rdcivnew:      18 coefficients were selected.
 workspace: ncsfmx=     169
 ncsfmx=                   169

 histogram parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =     169 ncsft =     169 ncsf =      18
 nhist =  20 fhist = 0.50000

    cmin                cmax        num  '*'=     1 csfs.
 ----------          ----------   ----- ---------|---------|---------|---------|
 5.0000E-01 <= |c| < 1.0000E+00       1 *
 2.5000E-01 <= |c| < 5.0000E-01       0
 1.2500E-01 <= |c| < 2.5000E-01       1 *
 6.2500E-02 <= |c| < 1.2500E-01       0
 3.1250E-02 <= |c| < 6.2500E-02       7 *******
 1.5625E-02 <= |c| < 3.1250E-02       3 ***
 7.8125E-03 <= |c| < 1.5625E-02       6 ******
 3.9062E-03 <= |c| < 7.8125E-03       0
 1.9531E-03 <= |c| < 3.9062E-03       0
 9.7656E-04 <= |c| < 1.9531E-03       0
 4.8828E-04 <= |c| < 9.7656E-04       0
 2.4414E-04 <= |c| < 4.8828E-04       0
 1.2207E-04 <= |c| < 2.4414E-04       0
 6.1035E-05 <= |c| < 1.2207E-04       0
 3.0518E-05 <= |c| < 6.1035E-05       0
 1.5259E-05 <= |c| < 3.0518E-05       0
 7.6294E-06 <= |c| < 1.5259E-05       0
 3.8147E-06 <= |c| < 7.6294E-06       0
 0.0000E+00 <= |c| < 3.8147E-06       0
                                  ----- ---------|---------|---------|---------|
                  total read =       18 total stored =      18

 from the selected csfs,
 min(|csfvec(:)|) = 1.0632E-02    max(|csfvec(:)|) = 9.6728E-01
 norm=   1.00000000000000     
 csfs will be printed based on coefficient magnitudes.

 current csfvec(*) selection parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =     169 ncsft =     169 ncsf =      18

 i:slabel(i) =  1: a  
 
 internal level =    1    2    3    4    5
 syml(*)        =    1    1    1    1    1
 label          =  a    a    a    a    a  
 rmo(*)         =    1    2    3    4    5

 printing selected csfs in sorted order from cmin = 0.00000 to cmax = 1.00000

   indcsf     c     c**2   v  lab:rmo  lab:rmo   step(*)
  ------- -------- ------- - ---- --- ---- --- ------------
          1  0.96728 0.93562 z*                    33300
          4 -0.22046 0.04860 z*                    33030
          6 -0.05998 0.00360 z*                    33003
         17 -0.04923 0.00242 z                     13230
         59 -0.04319 0.00187 y           a  :  7  132102
         60  0.03608 0.00130 y           a  :  8  132102
        121 -0.03488 0.00122 y           a  :  6  123102
          3  0.03321 0.00110 z*                    33102
         63 -0.03206 0.00103 y           a  : 11  132102
        127 -0.02683 0.00072 y           a  : 12  123102
         23  0.02392 0.00057 y           a  :  6  133200
        125 -0.01872 0.00035 y           a  : 10  123102
         94 -0.01454 0.00021 y           a  :  7  131202
        152  0.01389 0.00019 y           a  :  9  113220
         33  0.01375 0.00019 y           a  :  9  133020
         98 -0.01292 0.00017 y           a  : 11  131202
        156 -0.01283 0.00016 y           a  :  6  113202
        162 -0.01063 0.00011 y           a  : 12  113202
           18 csfs were printed in this range.

================================================================================
===================================VECTOR # 2===================================
================================================================================


 rdcivnew:      16 coefficients were selected.
 workspace: ncsfmx=     169
 ncsfmx=                   169

 histogram parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =     169 ncsft =     169 ncsf =      16
 nhist =  20 fhist = 0.50000

    cmin                cmax        num  '*'=     1 csfs.
 ----------          ----------   ----- ---------|---------|---------|---------|
 5.0000E-01 <= |c| < 1.0000E+00       1 *
 2.5000E-01 <= |c| < 5.0000E-01       0
 1.2500E-01 <= |c| < 2.5000E-01       0
 6.2500E-02 <= |c| < 1.2500E-01       0
 3.1250E-02 <= |c| < 6.2500E-02       4 ****
 1.5625E-02 <= |c| < 3.1250E-02       8 ********
 7.8125E-03 <= |c| < 1.5625E-02       3 ***
 3.9062E-03 <= |c| < 7.8125E-03       0
 1.9531E-03 <= |c| < 3.9062E-03       0
 9.7656E-04 <= |c| < 1.9531E-03       0
 4.8828E-04 <= |c| < 9.7656E-04       0
 2.4414E-04 <= |c| < 4.8828E-04       0
 1.2207E-04 <= |c| < 2.4414E-04       0
 6.1035E-05 <= |c| < 1.2207E-04       0
 3.0518E-05 <= |c| < 6.1035E-05       0
 1.5259E-05 <= |c| < 3.0518E-05       0
 7.6294E-06 <= |c| < 1.5259E-05       0
 3.8147E-06 <= |c| < 7.6294E-06       0
 0.0000E+00 <= |c| < 3.8147E-06       0
                                  ----- ---------|---------|---------|---------|
                  total read =       16 total stored =      16

 from the selected csfs,
 min(|csfvec(:)|) = 1.1198E-02    max(|csfvec(:)|) = 9.9327E-01
 norm=   1.00000000000000     
 csfs will be printed based on coefficient magnitudes.

 current csfvec(*) selection parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =     169 ncsft =     169 ncsf =      16

 i:slabel(i) =  1: a  
 
 internal level =    1    2    3    4    5
 syml(*)        =    1    1    1    1    1
 label          =  a    a    a    a    a  
 rmo(*)         =    1    2    3    4    5

 printing selected csfs in sorted order from cmin = 0.00000 to cmax = 1.00000

   indcsf     c     c**2   v  lab:rmo  lab:rmo   step(*)
  ------- -------- ------- - ---- --- ---- --- ------------
          2  0.99327 0.98659 z*                    33120
         40  0.06049 0.00366 y           a  :  9  133002
          5  0.03705 0.00137 z*                    33012
        149 -0.03449 0.00119 y           a  :  6  113220
         73 -0.03229 0.00104 y           a  :  7  132012
         74  0.02976 0.00089 y           a  :  8  132012
        135 -0.02772 0.00077 y           a  :  6  123012
         15 -0.02555 0.00065 z                     13320
        153  0.02553 0.00065 y           a  : 10  113220
         77 -0.02419 0.00059 y           a  : 11  132012
        155 -0.02315 0.00054 y           a  : 12  113220
        141 -0.02012 0.00041 y           a  : 12  123012
         18  0.01672 0.00028 z                     13212
        139 -0.01502 0.00023 y           a  : 10  123012
         30 -0.01435 0.00021 y           a  :  6  133020
         91 -0.01120 0.00013 y           a  : 11  131220
           16 csfs were printed in this range.
