1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=    22)...
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -38.8603784430 -6.6613E-15  1.8158E-02  1.8644E-01  1.0000E-03
 mr-sdci #  1  2    -38.8057836019  3.1086E-15  0.0000E+00  1.8680E-01  1.0000E-04

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  1  1    -38.8603784430 -6.6613E-15  1.8158E-02  1.8644E-01  1.0000E-03
 mr-sdci #  1  2    -38.8057836019  3.1086E-15  0.0000E+00  1.8680E-01  1.0000E-04

 from bk iterations: iconv=   1

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -38.8779045997  1.7526E-02  4.5020E-04  1.8639E-02  1.0000E-03
 mr-sdci #  1  2    -38.8057836019 -2.2204E-16  0.0000E+00  1.8680E-01  1.0000E-03
 mr-sdci #  2  1    -38.8782220801  3.1748E-04  2.0508E-05  5.3844E-03  1.0000E-03
 mr-sdci #  2  2    -38.8057836019  2.2204E-16  0.0000E+00  1.8680E-01  1.0000E-03
 mr-sdci #  3  1    -38.8782412450  1.9165E-05  0.0000E+00  7.4444E-04  1.0000E-03
 mr-sdci #  3  2    -38.8057836019  0.0000E+00  2.0978E-02  1.8680E-01  1.0000E-03
 mr-sdci #  4  1    -38.8782412450 -6.6613E-16  0.0000E+00  7.4444E-04  1.0000E-03
 mr-sdci #  4  2    -38.8251948656  1.9411E-02  1.9238E-04  1.5634E-02  1.0000E-03
 mr-sdci #  5  1    -38.8782412450  1.1102E-15  0.0000E+00  7.4444E-04  1.0000E-03
 mr-sdci #  5  2    -38.8253895152  1.9465E-04  2.1898E-05  4.2847E-03  1.0000E-03
 mr-sdci #  6  1    -38.8782412450  0.0000E+00  0.0000E+00  7.4444E-04  1.0000E-03
 mr-sdci #  6  2    -38.8254035777  1.4062E-05  2.3956E-06  1.6559E-03  1.0000E-03
 mr-sdci #  7  1    -38.8782412450  0.0000E+00  0.0000E+00  7.4444E-04  1.0000E-03
 mr-sdci #  7  2    -38.8254057378  2.1601E-06  9.3685E-08  3.7035E-04  1.0000E-03

 mr-sdci  convergence criteria satisfied after  7 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  7  1    -38.8782412450  0.0000E+00  0.0000E+00  7.4444E-04  1.0000E-03
 mr-sdci #  7  2    -38.8254057378  2.1601E-06  9.3685E-08  3.7035E-04  1.0000E-03

 number of reference csfs (nref) is     6.  root number (iroot) is  1.
 c0**2 =   0.98892579  c**2 (all zwalks) =   0.99144496

 eref      =    -38.860160793329   "relaxed" cnot**2         =   0.988925787406
 eci       =    -38.878241244957   deltae = eci - eref       =  -0.018080451628
 eci+dv1   =    -38.878441471722   dv1 = (1-cnot**2)*deltae  =  -0.000200226765
 eci+dv2   =    -38.878443713906   dv2 = dv1 / cnot**2       =  -0.000202468949
 eci+dv3   =    -38.878446006876   dv3 = dv1 / (2*cnot**2-1) =  -0.000204761919
 eci+pople =    -38.878376726183   (  6e- scaled deltae )    =  -0.018215932854

 number of reference csfs (nref) is     6.  root number (iroot) is  2.
 c0**2 =   0.98796479  c**2 (all zwalks) =   0.98899416

 eref      =    -38.805763883570   "relaxed" cnot**2         =   0.987964791822
 eci       =    -38.825405737760   deltae = eci - eref       =  -0.019641854189
 eci+dv1   =    -38.825642131564   dv1 = (1-cnot**2)*deltae  =  -0.000236393804
 eci+dv2   =    -38.825645011270   dv2 = dv1 / cnot**2       =  -0.000239273511
 eci+dv3   =    -38.825647962002   dv3 = dv1 / (2*cnot**2-1) =  -0.000242224243
 eci+pople =    -38.825565898480   (  6e- scaled deltae )    =  -0.019802014910
