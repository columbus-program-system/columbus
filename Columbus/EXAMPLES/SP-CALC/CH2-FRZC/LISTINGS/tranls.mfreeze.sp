 program "tran"

 this program transforms one- and two-electron
 integral and density matrix arrays.

 references:  i. shavitt, in "methods of electronic structure theory",
                  h. f. schaefer, ed. (plenum, new york, 1977), p. 189.
              h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. symp. 15, 91 (1981).

 original author: Frank B. Brown.
 SIFS version:    Ron Shepard.

 version date: 20-feb-02

 This Version of Program TRAN is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              TRAN        **
     **    PROGRAM VERSION:      5.6b2       **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 workspace allocation parameters: lcore= 353894400

 === echo of the input records ===
 ------------------------------------------------------------------------
 &input
  LRC1MX=0
  LRC2MX=0
  FREEZE(1)=-1
 &end
 ------------------------------------------------------------------------

 program input parameters:

 prnopt    =     0, chkopt    =     0, ortopt    =     0, denopt    =     0
 mapin(1 ) =    -1, nsymao    =    -1, naopsy(1) =    -1, freeze(1) =    -1
 mapout(1) =    -1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     4, tr1e      =     0
 LDAMIN    =   127, LDAMAX    =  8191, LDAINC    =    64
 LRC1MX    =     0, LRC2MX    =     0, LRCSCR    = 32768

 THRESH    =  5.0000E-12  [cutoff threshold]

 file description                 unit filename
 ---- -----------                 ---- --------
   1: input file:                   5  tranin                                                      
   2: listing file:                 6  tranls                                                      
   3: input ao integral file:      30  aoints                                                      
   4: input mo density file:       30  modens                                                      
   5: output mo integral file:     31  moints                                                      
   6: output ao density file:      31  aodens                                                      
   7: scratch file:                34  trscr1                                                      
   8: ci drt file:                 33  cidrtfl                                                     
   9: orbital coefficient file:    32  mocoef                                                      
  10: da1 scratch file:            20  trda1s                                                      
  11: da2 scratch file:            21  trda2s                                                      
  12: split input 2-e ao file:     35  aoints2                                                     
  13: split input d2mo file:       35  modens2                                                     
  14: FSPLIT=2 output 2-e mo file: 31  moints2                                                     
  15: FSPLIT=2 output d2ao file:   31  aodens2                                                     
  16: input file:                   0                                                              


 input file header information:
 Hermit Integral Program : SIFS version  r24n12            17:45:20.188 30-Aug-12

 input energy(*) values:
 energy( 1)=  6.593365133910E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total ao core energy =   6.593365133910E+00

 nsym = 1 nbft=  13

 symmetry  =    1
 SLABEL(*) =  A  
 NBPSY(*)  =   13

 INFOAO(*) =          2      4096      3272      4096      2730         0

 input basis function labels, i:AOLAB(i)=
   1:  1C1s     2:  2C1s     3:  3C1s     4:  4C1px    5:  5C1py    6:  6C1pz    7:  7C1px    8:  8C1py    9:  9C1pz   10: 10H1s  
  11: 11H1s    12: 12H2s    13: 13H2s  
 user-specified FREEZE(*) used in program tran.                                  

 transformation information:
 number of symmetry irreps in point group, nsym   =  1
 total number of basis functions,          nbft   = 13
 number of orbitals to be transformed,     norbt  = 12
 number of frozen core orbitals,           nfrzct =  1
 number of frozen virtual orbitals,        nfrzvt =  0

 symmetry blocking information:
 symmetry  =    1
 SLABEL(*) =  A  
 NBPSY(*)  =   13
 NMPSY(*)  =   12
 NFCPSY(*) =    1
 INFOMO=                     1                  4096                  3272
                  4096                  2730                     0
                     0                     0                     0
                     0
 IFMTM1,IFMTM2=                     0                     0
                    12

 mocoef file titles:
 Hermit Integral Program : SIFS version  r24n12            17:45:20.188 30-Aug-12
 mo coefficients generated by scfpq                                              

 job title for this run:
 SIFS file created by program tran.      r24n12            17:45:20.329 30-Aug-12

 trmain: score =   0.000000000000E+00

 frzden: (tr(dao*sao)-2*nfrzct)=  2.5757E-14

 inoutp: segmentation information:
 in-core transformation space,   avcinc = 353810455
 address segment size,           sizesg = 353809595
 number of in-core blocks,       nincbk =         1
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0

 inoutp: approximate total flop counts:
           type      in-core   out-of-core
         ------  ------------  ------------
          total       768638.            0. grand total =       768638.

 input file sort statistics:
 length of records,           csort%LENDAR =  8191
 number of buckets,           csort%NBUK   =     1
 number of values per bucket, csort%NPBUK  =  5460

 fixed workspace needed for the ao sort,  itotal =     37364
 workspace available for da sort buffers, avcore = 353876147

 beginning the first half-sort of the input 1/r12    array elements...

       2260 array elements in     1 records were read by twoaof,
       2260 array elements were written into     1 da records of length    8191.

 output file header information:
 Hermit Integral Program : SIFS version  r24n12            17:45:20.188 30-Aug-12
 user-specified FREEZE(*) used in program tran.                                  
 mo coefficients generated by scfpq                                              
 SIFS file created by program tran.      r24n12            17:45:20.329 30-Aug-12

 output energy(*) values:
 energy( 1)=  6.593365133910E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total mo core energy =   6.593365133910E+00

 inputc%NSYMMO = 1 norbt=  12

 symmetry  =    1
 SLABEL(*) =  A  
 NMOPSY(*) =   12

 INFOMO(*) =          1      4096      3272      4096      2730         0

 output orbital labels, i:MOLAB(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012

 tran1e: S1(*)    array: ITYPEA= 0 ITYPEB=   0 numaot=   53 nummot=   12 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  2.000000000000E+00 fcormo=  2.000000000000E+00

 tran1e: V1(*)    array: ITYPEA= 0 ITYPEB=   2 numaot=   59 nummot=   58 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore= -7.000187523105E+01 fcormo= -7.000187523105E+01

 tran1e: T1(*)    array: ITYPEA= 0 ITYPEB=   1 numaot=   53 nummot=   58 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  3.204919830500E+01 fcormo=  3.204919830500E+01

 tran1e: X(*)     array: ITYPEA= 1 ITYPEB=   0 numaot=   59 nummot=   57 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore= -3.063580098709E-01 fcormo= -3.063580098709E-01

 tran1e: Y(*)     array: ITYPEA= 1 ITYPEB=   1 numaot=   14 nummot=   20 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: Z(*)     array: ITYPEA= 1 ITYPEB=   2 numaot=   42 nummot=   55 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore= -7.835285714106E-13 fcormo= -7.835285714106E-13

 tran1e: XX(*)    array: ITYPEA= 1 ITYPEB=   3 numaot=   59 nummot=   58 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  1.119980559651E-01 fcormo=  1.119980559651E-01

 tran1e: XY(*)    array: ITYPEA= 1 ITYPEB=   4 numaot=   18 nummot=   20 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: XZ(*)    array: ITYPEA= 1 ITYPEB=   5 numaot=   46 nummot=   55 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  1.080844313654E-13 fcormo=  1.080844313654E-13

 tran1e: YY(*)    array: ITYPEA= 1 ITYPEB=   6 numaot=   53 nummot=   57 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  6.506819769515E-02 fcormo=  6.506819769515E-02

 tran1e: YZ(*)    array: ITYPEA= 1 ITYPEB=   7 numaot=   12 nummot=   20 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: ZZ(*)    array: ITYPEA= 1 ITYPEB=   8 numaot=   53 nummot=   58 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  6.507094789903E-02 fcormo=  6.507094789903E-02

 tran1e: XXX(*)   array: ITYPEA= 1 ITYPEB=   9 numaot=   59 nummot=   58 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore= -3.718987646424E-02 fcormo= -3.718987646424E-02

 tran1e: XXY(*)   array: ITYPEA= 1 ITYPEB=  10 numaot=   18 nummot=   20 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: XXZ(*)   array: ITYPEA= 1 ITYPEB=  11 numaot=   46 nummot=   55 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore= -1.332181291482E-13 fcormo= -1.332181291482E-13

 tran1e: XYY(*)   array: ITYPEA= 1 ITYPEB=  12 numaot=   59 nummot=   58 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore= -1.000569952040E-02 fcormo= -1.000569952040E-02

 tran1e: XYZ(*)   array: ITYPEA= 1 ITYPEB=  13 numaot=   12 nummot=   20 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: XZZ(*)   array: ITYPEA= 1 ITYPEB=  14 numaot=   59 nummot=   58 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore= -9.985767044339E-03 fcormo= -9.985767044339E-03

 tran1e: YYY(*)   array: ITYPEA= 1 ITYPEB=  15 numaot=   14 nummot=   19 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: YYZ(*)   array: ITYPEA= 1 ITYPEB=  16 numaot=   42 nummot=   55 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore= -1.007851545886E-13 fcormo= -1.007851545886E-13

 tran1e: YZZ(*)   array: ITYPEA= 1 ITYPEB=  17 numaot=   14 nummot=   20 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: ZZZ(*)   array: ITYPEA= 1 ITYPEB=  18 numaot=   42 nummot=   55 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore= -3.045530848182E-13 fcormo= -3.045530848182E-13

 tran1e: XXXX(*)  array: ITYPEA= 1 ITYPEB=  19 numaot=   59 nummot=   57 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  2.058127884083E-02 fcormo=  2.058127884083E-02

 tran1e: XXXY(*)  array: ITYPEA= 1 ITYPEB=  20 numaot=   18 nummot=   20 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: XXXZ(*)  array: ITYPEA= 1 ITYPEB=  21 numaot=   46 nummot=   55 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  3.636271615575E-14 fcormo=  3.636271615575E-14

 tran1e: XXYY(*)  array: ITYPEA= 1 ITYPEB=  22 numaot=   59 nummot=   58 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  4.950501633025E-03 fcormo=  4.950501633025E-03

 tran1e: XXYZ(*)  array: ITYPEA= 1 ITYPEB=  23 numaot=   12 nummot=   20 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: XXZZ(*)  array: ITYPEA= 1 ITYPEB=  24 numaot=   59 nummot=   58 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  4.980532408542E-03 fcormo=  4.980532408542E-03

 tran1e: XYYY(*)  array: ITYPEA= 1 ITYPEB=  25 numaot=   18 nummot=   20 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: XYYZ(*)  array: ITYPEA= 1 ITYPEB=  26 numaot=   46 nummot=   55 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  2.156029151260E-14 fcormo=  2.156029151260E-14

 tran1e: XYZZ(*)  array: ITYPEA= 1 ITYPEB=  27 numaot=   18 nummot=   20 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: XZZZ(*)  array: ITYPEA= 1 ITYPEB=  28 numaot=   46 nummot=   55 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  6.149499854648E-15 fcormo=  6.149499854648E-15

 tran1e: YYYY(*)  array: ITYPEA= 1 ITYPEB=  29 numaot=   53 nummot=   58 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  1.025308594673E-02 fcormo=  1.025308594673E-02

 tran1e: YYYZ(*)  array: ITYPEA= 1 ITYPEB=  30 numaot=   12 nummot=   20 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: YYZZ(*)  array: ITYPEA= 1 ITYPEB=  31 numaot=   53 nummot=   58 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  3.402781434893E-03 fcormo=  3.402781434893E-03

 tran1e: YZZZ(*)  array: ITYPEA= 1 ITYPEB=  32 numaot=   12 nummot=   20 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: YZZZ(*)  array: ITYPEA= 1 ITYPEB=  33 numaot=   53 nummot=   58 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  1.029606054548E-02 fcormo=  1.029606054548E-02

 tran1e: Im(lx)   array: ITYPEA= 2 ITYPEB=   6 numaot=   12 nummot=   19 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: Im(ly)   array: ITYPEA= 2 ITYPEB=   7 numaot=   42 nummot=   45 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: Im(lz)   array: ITYPEA= 2 ITYPEB=   8 numaot=   18 nummot=   20 nrecao=  1 nrecmo=  1
         fcorao=  0.000000000000E+00 fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00

 tran1e: transformation of the input 1-e arrays is complete.
 frozen core density AO

 vfcout: VFC(*)   array, ITYPEA= 0 ITYPEB=   4 nummot=   57 nrecmo=  1
         fcormo=  3.505129431382E+00

 beginning the last half-sort of the in-core 1/r12    array elements...

 srtinc:      2260 array elements were read, and              4186 array elements were written in     1 records.

 beginning the transformation of the in-core 1/r12    array elements...
 executing in-core transformation trainc

 trainc:      4186 sorted array elements were read,      1714 transformed array elements were written.

 1-e and 2-e transformation complete.

 trmain:       1714 transformed 1/r12    array elements were written in       1 records.

