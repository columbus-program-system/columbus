
 Work memory size (LMWORK) :   353894400 =    0.00 megabytes.

 Default basis set library used :
        /sphome/kedziora/dalton/basis/                              


    $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    $$$$$$$$$$$  DALTON - An electronic structure program  $$$$$$$$$$$
    $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

               This is output from DALTON (beta-version 0.9) 

                          Principal authors:

            Trygve Helgaker,     University of Oslo,        Norway 
            Hans Joergen Jensen, University of Odense,      Denmark
            Poul Joergensen,     University of Aarhus,      Denmark
            Henrik Koch,         University of Aarhus,      Denmark
            Jeppe Olsen,         University of Lund,        Sweden 
            Hans Aagren,         University of Linkoeping,  Sweden 

                          Contributors:

            Torgeir Andersen,    University of Oslo,        Norway 
            Keld L. Bak,         University of Copenhagen,  Denmark
            Vebjoern Bakken,     University of Oslo,        Norway 
            Ove Christiansen,    University of Aarhus,      Denmark
            Paal Dahle,          University of Oslo,        Norway 
            Erik K. Dalskov,     University of Odense,      Denmark
            Thomas Enevoldsen,   University of Odense,      Denmark
            Asger Halkier,       University of Aarhus,      Denmark
            Hanne Heiberg,       University of Oslo,        Norway 
            Dan Jonsson,         University of Linkoeping,  Sweden 
            Sheela Kirpekar,     University of Odense,      Denmark
            Rika Kobayashi,      University of Aarhus,      Denmark
            Alfredo S. de Meras, Valencia University,       Spain  
            Kurt Mikkelsen,      University of Aarhus,      Denmark
            Patrick Norman,      University of Linkoeping,  Sweden 
            Martin J. Packer,    University of Sheffield,   UK     
            Kenneth Ruud,        University of Oslo,        Norway 
            Trond Saue,          University of Oslo,        Norway 
            Peter Taylor,        San Diego Superc. Center,  USA    
            Olav Vahtras,        University of Linkoeping,  Sweden

                                             Release Date:  August 1996
------------------------------------------------------------------------


      
     NOTE:
      
     This is an experimental code for the evaluation of molecular
     properties using (MC)SCF/CC wave functions. The authors accept
      no responsibility for the performance of the code or for the
     correctness of the results.
      
     The code (in whole or part) is not to be reproduced for further
     distribution without the written permission of T. Helgaker,
     H. J. Aa. Jensen or P. Taylor.
      
     If results obtained with this code are published, an
     appropriate citation would be:
      
     T. Helgaker, H. J. Aa. Jensen, P.Joergensen, H. Koch,
     J. Olsen, H. Aagren, T. Andersen, K. L. Bak, V. Bakken,
     O. Christiansen, P. Dahle, E. K. Dalskov, T. Enevoldsen,
     A. Halkier, H. Heiberg, D. Jonsson, S. Kirpekar, R. Kobayashi,
     A. S. de Meras, K. V. Mikkelsen, P. Norman, M. J. Packer,
     K. Ruud, T.Saue, P. R. Taylor, and O. Vahtras:
     DALTON, an electronic structure program"



     ******************************************
     **    PROGRAM:              DALTON      **
     **    PROGRAM VERSION:      5.4.0.0     **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************



 <<<<<<<<<< OUTPUT FROM GENERAL INPUT PROCESSING >>>>>>>>>>




 Default print level:        0

    Integral sections will be executed
    Starting in Integral Section -



 *************************************************************************
 ****************** Output from HERMIT input processing ******************
 *************************************************************************



 Default print level:        2


 Calculation of one- and two-electron Hamiltonian integrals.


 The following one-electron property integrals are calculated:

          - overlap integrals
          - Cartesian multipole moment integrals of orders 4 and lower
          - electronic angular momentum around the origin


 Changes of defaults for READIN:
 -------------------------------


 Maximum number of primitives per integral block :   10



 *************************************************************************
 ****************** Output from READIN input processing ******************
 *************************************************************************



  Title Cards
  -----------

                                                                          
                                                                          


                      SYMGRP:Point group information
                      ------------------------------

Point group: C1 

   * Character table

        |  E 
   -----+-----
    A   |   1

   * Direct product table

        | A  
   -----+-----
    A   | A  


  Atoms and basis sets
  --------------------

  Number of atom types:     2
  Total number of atoms:    3

  label    atoms   charge   prim    cont     basis   
  ----------------------------------------------------------------------
  C  1        1       6      22       9      [10s4p|3s2p]                                 
  H  1        1       1       4       2      [4s|2s]                                      
  H  2        1       1       4       2      [4s|2s]                                      
  ----------------------------------------------------------------------
  ----------------------------------------------------------------------
  total:      3       8      30      13

  Threshold for integrals:  1.00D-15


  Cartesian Coordinates
  ---------------------

  Total number of coordinates:  9


   1   C  1     x     -0.1530009800
   2            y      0.0000000000
   3            z      0.0000000000

   4   H  1     x      0.9440157100
   5            y      0.0000000000
   6            z      1.5666988600

   7   H  2     x      0.9440157100
   8            y      0.0000000000
   9            z     -1.5666988600



   Interatomic separations (in Angstroms):
   ---------------------------------------

            C  1        H  1        H  2

   C  1    0.000000
   H  1    1.012097    0.000000
   H  2    1.012097    1.658122    0.000000




  Bond distances (angstroms):
  ---------------------------

                  atom 1     atom 2                           distance
                  ------     ------                           --------
  bond distance:    H  1       C  1                           1.012097
  bond distance:    H  2       C  1                           1.012097


  Bond angles (degrees):
  ----------------------

                  atom 1     atom 2     atom 3                   angle
                  ------     ------     ------                   -----
  bond angle:       H  2       C  1       H  1                 110.000


  Nuclear repulsion energy :    6.593365133910


  Orbital exponents and contraction coefficients
  ----------------------------------------------


  C  1   1s    1     3047.524900    0.0018  0.0000  0.0000
   seg. cont.  2      457.369510    0.0140  0.0000  0.0000
               3      103.948690    0.0688  0.0000  0.0000
               4       29.210155    0.2322  0.0000  0.0000
               5        9.286663    0.4679  0.0000  0.0000
               6        3.163927    0.3623  0.0000  0.0000
               7        7.868272    0.0000 -0.1193  0.0000
               8        1.881288    0.0000 -0.1609  0.0000
               9        0.544249    0.0000  1.1435  0.0000
              10        0.168714    0.0000  0.0000  1.0000

  C  1   2px  11        7.868272    0.0690  0.0000
   seg. cont. 12        1.881288    0.3164  0.0000
              13        0.544249    0.7443  0.0000
              14        0.168714    0.0000  1.0000

  C  1   2py  15        7.868272    0.0690  0.0000
   seg. cont. 16        1.881288    0.3164  0.0000
              17        0.544249    0.7443  0.0000
              18        0.168714    0.0000  1.0000

  C  1   2pz  19        7.868272    0.0690  0.0000
   seg. cont. 20        1.881288    0.3164  0.0000
              21        0.544249    0.7443  0.0000
              22        0.168714    0.0000  1.0000

  H  1   1s   23       18.731137    0.0335  0.0000
   seg. cont. 24        2.825394    0.2347  0.0000
              25        0.640122    0.8138  0.0000
              26        0.161278    0.0000  1.0000

  H  2   1s   27       18.731137    0.0335  0.0000
   seg. cont. 28        2.825394    0.2347  0.0000
              29        0.640122    0.8138  0.0000
              30        0.161278    0.0000  1.0000


  Contracted Orbitals
  -------------------

   1  C  1    1s       1     2     3     4     5     6
   2  C  1    1s       7     8     9
   3  C  1    1s      10
   4  C  1    2px     11    12    13
   5  C  1    2py     15    16    17
   6  C  1    2pz     19    20    21
   7  C  1    2px     14
   8  C  1    2py     18
   9  C  1    2pz     22
  10  H  1    1s      23    24    25
  11  H  1    1s      26
  12  H  2    1s      27    28    29
  13  H  2    1s      30




  Symmetry Orbitals
  -----------------

  Number of orbitals in each symmetry:        13


  Symmetry  A  ( 1)

    1     C  1     1s         1
    2     C  1     1s         2
    3     C  1     1s         3
    4     C  1     2px        4
    5     C  1     2py        5
    6     C  1     2pz        6
    7     C  1     2px        7
    8     C  1     2py        8
    9     C  1     2pz        9
   10     H  1     1s        10
   11     H  1     1s        11
   12     H  2     1s        12
   13     H  2     1s        13

  Symmetries of electric field:  A  (1)  A  (1)  A  (1)

  Symmetries of magnetic field:  A  (1)  A  (1)  A  (1)


 Copy of input to READIN
 -----------------------

INTGRL                                                                          
                                                                                
                                                                                
s   2    0           0.10D-14                                                   
       6.0    1    2    3    2                                                  
C  1  -0.153000980000000   0.000000000000000   0.000000000000000       *        
H   6   1                                                                       
       3047.52490000         0.00183470                                         
        457.36951000         0.01403730                                         
        103.94869000         0.06884260                                         
         29.21015500         0.23218440                                         
          9.28666300         0.46794130                                         
          3.16392700         0.36231200                                         
H   3   1                                                                       
          7.86827240        -0.11933240                                         
          1.88128850        -0.16085420                                         
          0.54424930         1.14345640                                         
H   1   1                                                                       
          0.16871440         1.00000000                                         
H   3   1                                                                       
          7.86827240         0.06899910                                         
          1.88128850         0.31642400                                         
          0.54424930         0.74430830                                         
H   1   1                                                                       
          0.16871440         1.00000000                                         
       1.0    2    1    2                                                       
H  1   0.944015710000000   0.000000000000000   1.566698860000000       *        
H  2   0.944015710000000   0.000000000000000  -1.566698860000000       *        
H   3   1                                                                       
         18.73113700         0.03349460                                         
          2.82539370         0.23472695                                         
          0.64012170         0.81375733                                         
H   1   1                                                                       
          0.16127780         1.00000000                                         




 ************************************************************************
 ************************** Output from HERONE **************************
 ************************************************************************

found      53 non-vanashing overlap integrals
found      59 non-vanashing nuclear attraction integrals
found      53 non-vanashing kinetic energy integrals






 found      59 non-vanashing integrals ( typea=  1 typeb=  0)
 found      14 non-vanashing integrals ( typea=  1 typeb=  1)
 found      42 non-vanashing integrals ( typea=  1 typeb=  2)


 found      59 non-vanashing integrals ( typea=  1 typeb=  3)
 found      18 non-vanashing integrals ( typea=  1 typeb=  4)
 found      46 non-vanashing integrals ( typea=  1 typeb=  5)
 found      53 non-vanashing integrals ( typea=  1 typeb=  6)
 found      12 non-vanashing integrals ( typea=  1 typeb=  7)
 found      53 non-vanashing integrals ( typea=  1 typeb=  8)


 found      59 non-vanashing integrals ( typea=  1 typeb=  9)
 found      18 non-vanashing integrals ( typea=  1 typeb= 10)
 found      46 non-vanashing integrals ( typea=  1 typeb= 11)
 found      59 non-vanashing integrals ( typea=  1 typeb= 12)
 found      12 non-vanashing integrals ( typea=  1 typeb= 13)
 found      59 non-vanashing integrals ( typea=  1 typeb= 14)
 found      14 non-vanashing integrals ( typea=  1 typeb= 15)
 found      42 non-vanashing integrals ( typea=  1 typeb= 16)
 found      14 non-vanashing integrals ( typea=  1 typeb= 17)
 found      42 non-vanashing integrals ( typea=  1 typeb= 18)


 found      59 non-vanashing integrals ( typea=  1 typeb= 19)
 found      18 non-vanashing integrals ( typea=  1 typeb= 20)
 found      46 non-vanashing integrals ( typea=  1 typeb= 21)
 found      59 non-vanashing integrals ( typea=  1 typeb= 22)
 found      12 non-vanashing integrals ( typea=  1 typeb= 23)
 found      59 non-vanashing integrals ( typea=  1 typeb= 24)
 found      18 non-vanashing integrals ( typea=  1 typeb= 25)
 found      46 non-vanashing integrals ( typea=  1 typeb= 26)
 found      18 non-vanashing integrals ( typea=  1 typeb= 27)
 found      46 non-vanashing integrals ( typea=  1 typeb= 28)
 found      53 non-vanashing integrals ( typea=  1 typeb= 29)
 found      12 non-vanashing integrals ( typea=  1 typeb= 30)
 found      53 non-vanashing integrals ( typea=  1 typeb= 31)
 found      12 non-vanashing integrals ( typea=  1 typeb= 32)
 found      53 non-vanashing integrals ( typea=  1 typeb= 33)


 found      12 non-vanashing integrals ( typea=  2 typeb=  6)
 found      42 non-vanashing integrals ( typea=  2 typeb=  7)
 found      18 non-vanashing integrals ( typea=  2 typeb=  8)




 ************************************************************************
 ************************** Output from TWOINT **************************
 ************************************************************************


 Number of two-electron integrals written:      2260 (54.0%)
 Kilobytes written:                               44




 >>>> Total CPU  time used in HERMIT:   0.00 seconds
 >>>> Total wall time used in HERMIT:   0.00 seconds

- End of Integral Section
