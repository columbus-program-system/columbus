1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      2009-03.    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


================ Computing sorting integral file structure ================

                    -----z----- -----y----- -----x----- -----w----- ---total---

                CSFs        28         638        3352        1542        5560
      internal walks       105          90          36          15         246
valid internal walks        28          90          36          15         169
 getinfoarray: info=                     6 :                     1
                  8192                  6552                  8192
                  5460                     0
 icd(3)=                  2976 ci%nnlev=                   595  l2rec=
                  8192  n2max=                  5460
 lcore1,lcore2=              13068137              13044821
 lencor,maxblo              13107200                 60000
========================================
 current settings:
 minbl3         381
 minbl4         381
 locmaxbl3     4288
 locmaxbuf     2144
 maxbl3       60000
 maxbl3       60000
 maxbl4       60000
 maxbuf       30006
========================================

 sorted 4-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)

 sorted 3-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)
 Orig.  diagonal integrals:  1electron:        34
                             0ext.    :        42
                             2ext.    :       336
                             4ext.    :       812


 Orig. off-diag. integrals:  4ext.    :     24411
                             3ext.    :     18720
                             2ext.    :      6792
                             1ext.    :      1272
                             0ext.    :        99
                             2ext. SO :         0
                             1ext. SO :         0
                             0ext. SO :         0
                             1electron:       128


 Sorted integrals            3ext.  w :     17370 x :     16020
                             4ext.  w :     20955 x :     17777


Cycle #  1 sortfile size=    163835(       5 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13068137
Cycle #  2 sortfile size=    163835(       5 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13068137
 minimum size of srtscr:     98301 WP (     3 records)
 maximum size of srtscr:    163835 WP (     5 records)
diagi   file:      4 records  of   1536 WP each=>       6144 WP total
ofdgi   file:      8 records  of   1536 WP each=>      12288 WP total
fil3w   file:      1 records  of  30006 WP each=>      30006 WP total
fil3x   file:      1 records  of  30006 WP each=>      30006 WP total
fil4w   file:      1 records  of  30006 WP each=>      30006 WP total
fil4x   file:      1 records  of  30006 WP each=>      30006 WP total
 compressed index vector length=                    67
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 3,
  GSET = 3,
   DAVCOR =10,
  NCOREL = 4
   nroot=1
  IVMODE = 3
  NBKITR = 1
   nvbkmn=2
  RTOLBK = 1e-3
   niter=30
   nvcimn=2
   rtolci=1e-3
  NVCIMX = 6
  NVRFMX = 6
  NVBKMX = 6
  IDEN  = 1
  CSFPRN = 10,
  update_mode=1
  nvrfmn=2
   lrtshift=-0.0465482970
 /&end
 ------------------------------------------------------------------------
lodens (list->root)=  1
invlodens (root->list)=  1
 LRT calculation: diagonal shift= -4.654829700000000E-002
 bummer (warning):2:changed keyword: nbkitr=                      0
 USING SEGMENTS OF EQUAL SIZE

****************  list of control variables  ****************
 lvlprt =    0      nroot  =    1      noldv  =   0      noldhv =   0
 nunitv =    1      nbkitr =    0      niter  =  30      davcor =  10
 csfprn =   10      ivmode =    3      istrt  =   0      vout   =   0
 iortls =    0      nvbkmx =    6      ibktv  =  -1      ibkthv =  -1
 nvcimx =    6      icitv  =   -1      icithv =  -1      frcsub =   0
 nvbkmn =    2      nvcimn =    2      maxseg =   4      nrfitr =  30
 ncorel =    4      nvrfmx =    6      nvrfmn =   2      iden   =   1
 itran  =    0      froot  =    0      rtmode =   0      ncouple=   1
 skipso =    F      dalton2=    0      molcas =   0      finalv =   0
 finalw =    0      cosmocalc=   0    with_tsklst=   0
 nsegwx =    1     1     1     1
 nseg0x =    1     1     1     1
 nseg1x =    1     1     1     1
 nseg2x =    1     1     1     1
 nseg3x =    1     1     1     1
 nseg4x =    1     1     1     1
 no0ex  =      0    no1ex  =      0    no2ex  =     0    no3ex  =     0
 no4ex  =      0    nodiag =      0
 cdg4ex =    1      c3ex1ex=    1      c2ex0ex=   1
 fileloc=    0     0     0     0     0     0     0     1     1     1
 directhd=   1      noaqccshift_zyxw=      0
 critical_crit=-1.00000    critical_delta= 0.05000

 ctol   = 0.010000    lrtshift=-.046548    smalld =0.001000


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-03    1.000E-03
 Computing density:                    .drt1.state1
 using                      1  nodes and                      1  cores.
 szdg/szodg per processor=                  6144                 12288
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core           13107199 DP per process

********** Integral sort section *************


 workspace allocation information: lencor=  13107199

 echo of the input for program cisrt:
 ------------------------------------------------------------------------
  &input
  maxbl3=60000
  maxbl4=60000
  &end
 ------------------------------------------------------------------------
 
 ( 6) listing file:                    ciudgls             
 ( 5) input file:                      cisrtin   
 (17) cidrt file:                      cidrtfl             
 (11) transformed integrals file:      moints    
 (12) diagonal integral file:          diagint             
 (13) off-diagonal integral file:      ofdgint             
 (31) 4-external w integrals file:     fil4w               
 (32) 4-external x integrals file:     fil4x               
 (33) 3-external w integrals file:     fil3w               
 (34) 3-external x integrals file:     fil3x               
 (21) scratch da sorting file:         srtscr              
 (12) 2-e integral file [fsplit=2]:    moints2   

 input integral file header information:
                                                                                 
 aoints SIFS file created by argos.      zam792            17:56:38.564 17-Dec-13
  cidrt_title                                                                    
 MO-coefficients from mcscf.x                                                    
  with dummy occupation 1.0 for active orbitals                                  
  total ao core energy =    0.000000000                                          
 MCSCF energy =     -46.301878576                                                
 SIFS file created by program tran.      zam792            17:56:38.888 17-Dec-13

 input energy(*) values:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total core energy =   0.000000000000E+00

 nsym = 4 nmot=  34

 symmetry  =    1    2    3    4
 slabel(*) =   a1   a2   b1   b2
 nmpsy(*)  =   14    4    8    8

 info(*) =          1      8192      6552      8192      5460         0

 orbital labels, i:molab(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012  13:tout:013  14:tout:014  15:tout:015  16:tout:016  17:tout:017  18:tout:018  19:tout:019  20:tout:020
  21:tout:021  22:tout:022  23:tout:023  24:tout:024  25:tout:025  26:tout:026  27:tout:027  28:tout:028  29:tout:029  30:tout:030
  31:tout:031  32:tout:032  33:tout:033  34:tout:034

 input parameters:
 prnopt=  0
 ldamin=    4095 ldamax=   32767 ldainc=      64
 maxbuf=   30006 maxbl3=   60000 maxbl4=   60000 intmxo=    1536
  Using 32 bit compression 

 drt information:
  cidrt_title                                                                    
 nmotd =  38 nfctd =   4 nfvtc =   0 nmot  =  34
 nlevel =  34 niot  =   6 lowinl=  29
 orbital-to-level map(*)
   -1  -1  29  30  31   1   2   3   4   5   6   7   8   9  10  11  32  12  13  14
   -1  33  15  16  17  18  19  20  21  -1  34  22  23  24  25  26  27  28
 compressed map(*)
   29  30  31   1   2   3   4   5   6   7   8   9  10  11  32  12  13  14  33  15
   16  17  18  19  20  21  34  22  23  24  25  26  27  28
 levsym(*)
    1   1   1   1   1   1   1   1   1   1   1   2   2   2   3   3   3   3   3   3
    3   4   4   4   4   4   4   4   1   1   1   2   3   4
 repartitioning mu(*)=
   0.  0.  0.  0.  0.  0.

 new core energy added to the energy(*) list.
 from the integral file: h1_core= -4.349858765627E+01

 indxdg: diagonal integral statistics.
 total number of integrals contributing to diagonal matrix elements:      1190
 number with all external indices:       812
 number with half external - half internal indices:       336
 number with all internal indices:        42

 indxof: off-diagonal integral statistics.
    4-external integrals: num=      24411 strt=          1
    3-external integrals: num=      18720 strt=      24412
    2-external integrals: num=       6792 strt=      43132
    1-external integrals: num=       1272 strt=      49924
    0-external integrals: num=         99 strt=      51196

 total number of off-diagonal integrals:       51294


 indxof(2nd)  ittp=   3 numx(ittp)=        6792
 indxof(2nd)  ittp=   4 numx(ittp)=        1272
 indxof(2nd)  ittp=   5 numx(ittp)=          99

 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg=  12931980
 pro2e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652

 pro2e:     25126 integrals read in     5 records.

 pro2e:         0 integrals 34-ext integrals skipped.
 pro1e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652
 pro1e: eref =    0.000000000000000E+00
 total size of srtscr:                     4  records of                  32767 
 WP =               1048544 Bytes
 putdg        1     596    1191    1786    3322   36089   57934    3018   46706   90394
   123161  131353  136813  158652

 putf:       4 buffers of length    1536 written to file 12
 diagonal integral file completed.

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep4e:    17 blocks of linear combinations of 4-external integrals processed.
 number of sorted 4-external integrals      38732
 number of original 4-external integrals    24411


 putf34: external integral file complete. nfilw=    31 nfilx=    32 nrecw=     1 nrecx=     1 lbufp= 30006

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep3e:    19 blocks of linear combinations of 3-external integrals processed.
 number of sorted 3-external integrals      33390
 number of original 3-external integrals    18720


 putf34: external integral file complete. nfilw=    33 nfilx=    34 nrecw=     1 nrecx=     1 lbufp= 30006
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      6792         3     43132     43132      6792     51294
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      1272         4     49924     49924      1272     51294
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd        99         5     51196     51196        99     51294

 putf:       8 buffers of length    1536 written to file 13
 off-diagonal files sort completed.
 executing brd_struct for cisrtinfo
cisrtinfo:
bufszi  1536
 diagfile 4ext:     812 2ext:     336 0ext:      42
 fil4w,fil4x  :   24411 fil3w,fil3x :   18720
 ofdgint  2ext:    6792 1ext:    1272 0ext:      99so0ext:       0so1ext:       0so2ext:       0
buffer minbl4     381 minbl3     381 maxbl2     384nbas:  11   3   7   7   0   0   0   0 maxbuf 30006
 CIUDG version 5.9.7 ( 5-Oct-2004)

 workspace allocation information: lcore=  13107199

 core energy values from the integral file:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -4.349858765627E+01, ietype=    6,   fcore energy of type: H1(*)   

 total core repulsion energy = -4.349858765627E+01
 nmot  =    38 niot  =     6 nfct  =     4 nfvt  =     0
 nrow  =    36 nsym  =     4 ssym  =     2 lenbuf=  1600
 nwalk,xbar:        246      105       90       36       15
 nvalwt,nvalw:      169       28       90       36       15
 ncsft:            5560
 total number of valid internal walks:     169
 nvalz,nvaly,nvalx,nvalw =       28      90      36      15

 cisrt info file parameters:
 file number  12 blocksize   1536
 mxbld   1536
 nd4ext,nd2ext,nd0ext   812   336    42
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int    24411    18720     6792     1272       99        0        0        0
 minbl4,minbl3,maxbl2   381   381   384
 maxbuf 30006
 number of external orbitals per symmetry block:  11   3   7   7
 nmsym   4 number of internal orbitals   6
 executing brd_struct for drt
 executing brd_struct for orbinf
 executing brd_struct for momap
 calcthrxt: niot,maxw1=                     6                    66
 block size     0
 pthz,pthy,pthx,pthw:   105    90    36    15 total internal walks:     246
 maxlp3,n2lp,n1lp,n0lp    66     0     0     0
 orbsym(*)= 1 1 1 2 3 4

 setref:       28 references kept,
               77 references were marked as invalid, out of
              105 total.
 nmb.of records onel     1
 nmb.of records 2-ext     5
 nmb.of records 1-ext     1
 nmb.of records 0-ext     1
 nmb.of records 2-int     0
 nmb.of records 1-int     0
 nmb.of records 0-int     0
 ---------memory usage in DP -----------------
 < n-ex core usage >
     routines:
    fourex            61797
    threx             60409
    twoex              3549
    onex               1823
    allin              1536
    diagon             2109
               =======
   maximum            61797
 
  __ static summary __ 
   reflst                28
   hrfspc                28
               -------
   static->              56
 
  __ core required  __ 
   totstc                56
   max n-ex           61797
               -------
   totnec->           61853
 
  __ core available __ 
   totspc          13107199
   totnec -           61853
               -------
   totvec->        13045346

 number of external paths / symmetry
 vertex x     100      82      98      98
 vertex w     128      82      98      98
segment: free space=    13045346
 reducing frespc by                   910 to               13044436 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1          40|        28|         0|        28|         0|         1|
 -------------------------------------------------------------------------------
  Y 2          90|       638|        28|        90|        28|         2|
 -------------------------------------------------------------------------------
  X 3          36|      3352|       666|        36|       118|         3|
 -------------------------------------------------------------------------------
  W 4          15|      1542|      4018|        15|       154|         4|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=          40DP  conft+indsym=         360DP  drtbuffer=         510 DP

dimension of the ci-matrix ->>>      5560

 executing brd_struct for civct
 gentasklist: ntask=                    20
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS   
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      36      40       3352         28      36      28
     2  4   1    25      two-ext wz   2X  4 1      15      40       1542         28      15      28
     3  4   3    26      two-ext wx*  WX  4 3      15      36       1542       3352      15      36
     4  4   3    27      two-ext wx+  WX  4 3      15      36       1542       3352      15      36
     5  2   1    11      one-ext yz   1X  2 1      90      40        638         28      90      28
     6  3   2    15      1ex3ex yx    3X  3 2      36      90       3352        638      36      90
     7  4   2    16      1ex3ex yw    3X  4 2      15      90       1542        638      15      90
     8  1   1     1      allint zz    OX  1 1      40      40         28         28      28      28
     9  2   2     5      0ex2ex yy    OX  2 2      90      90        638        638      90      90
    10  3   3     6      0ex2ex xx*   OX  3 3      36      36       3352       3352      36      36
    11  3   3    18      0ex2ex xx+   OX  3 3      36      36       3352       3352      36      36
    12  4   4     7      0ex2ex ww*   OX  4 4      15      15       1542       1542      15      15
    13  4   4    19      0ex2ex ww+   OX  4 4      15      15       1542       1542      15      15
    14  2   2    42      four-ext y   4X  2 2      90      90        638        638      90      90
    15  3   3    43      four-ext x   4X  3 3      36      36       3352       3352      36      36
    16  4   4    44      four-ext w   4X  4 4      15      15       1542       1542      15      15
    17  1   1    75      dg-024ext z  OX  1 1      40      40         28         28      28      28
    18  2   2    76      dg-024ext y  OX  2 2      90      90        638        638      90      90
    19  3   3    77      dg-024ext x  OX  3 3      36      36       3352       3352      36      36
    20  4   4    78      dg-024ext w  OX  4 4      15      15       1542       1542      15      15
----------------------------------------------------------------------------------------------------
REDTASK #   1 TIME=  19.000 N=  1 (task/type/sgbra)=(   1/24/0) (
REDTASK #   2 TIME=  18.000 N=  1 (task/type/sgbra)=(   2/25/0) (
REDTASK #   3 TIME=  17.000 N=  1 (task/type/sgbra)=(   3/26/1) (
REDTASK #   4 TIME=  16.000 N=  1 (task/type/sgbra)=(   4/27/2) (
REDTASK #   5 TIME=  15.000 N=  1 (task/type/sgbra)=(   5/11/0) (
REDTASK #   6 TIME=  14.000 N=  1 (task/type/sgbra)=(   6/15/0) (
REDTASK #   7 TIME=  13.000 N=  1 (task/type/sgbra)=(   7/16/0) (
REDTASK #   8 TIME=  12.000 N=  1 (task/type/sgbra)=(   8/ 1/0) (
REDTASK #   9 TIME=  11.000 N=  1 (task/type/sgbra)=(   9/ 5/0) (
REDTASK #  10 TIME=  10.000 N=  1 (task/type/sgbra)=(  10/ 6/1) (
REDTASK #  11 TIME=   9.000 N=  1 (task/type/sgbra)=(  11/18/2) (
REDTASK #  12 TIME=   8.000 N=  1 (task/type/sgbra)=(  12/ 7/1) (
REDTASK #  13 TIME=   7.000 N=  1 (task/type/sgbra)=(  13/19/2) (
REDTASK #  14 TIME=   6.000 N=  1 (task/type/sgbra)=(  14/42/1) (
REDTASK #  15 TIME=   5.000 N=  1 (task/type/sgbra)=(  15/43/1) (
REDTASK #  16 TIME=   4.000 N=  1 (task/type/sgbra)=(  16/44/1) (
REDTASK #  17 TIME=   3.000 N=  1 (task/type/sgbra)=(  17/75/1) (
REDTASK #  18 TIME=   2.000 N=  1 (task/type/sgbra)=(  18/76/1) (
REDTASK #  19 TIME=   1.000 N=  1 (task/type/sgbra)=(  19/77/1) (
REDTASK #  20 TIME=   0.000 N=  1 (task/type/sgbra)=(  20/78/1) (
 initializing v-file: 1:                  5560

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         216 2x:           0 4x:           0
All internal counts: zz :         356 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 reference space has dimension      28
 dsyevx: computed roots 1 to    2(converged:   2)

    root           eigenvalues
    ----           ------------
       1         -46.3018785770
       2         -46.3018785757

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=                    28

         vector  1 from unit 11 written to unit 49 filename cirefv              
 ncorel,neli=                     4                     4
  This is a  mraqcc energy evaluation      

    ------------------------------------------------------------

 setopt2: mraqcc energy evaluation      gvalue=  0.833333333333333      ncorel=
                     4

    ------------------------------------------------------------


    reference energies:

  reference state used is:   1with energy      -46.3018785770

  ### begin active excitation selection ###

 inactive(1:nintern)=AAAAAA
 There are         28 reference CSFs out of         28 valid zwalks.
 There are    0 inactive and    6 active orbitals.
      28 active-active  and        0 inactive excitations.

  ### end active excitation selection ###


 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              5560
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                   30
 residual norm convergence criteria:               0.001000

          starting ci iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         807 2x:         336 4x:         141
All internal counts: zz :         356 yy:         930 xx:         352 ww:          66
One-external counts: yz :        1509 yx:        2405 yw:        1220
Two-external counts: yy :        1285 ww:         180 xx:         682 xz:         159 wz:         100 wx:         510
Three-ext.   counts: yx :         657 yw:         358

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1
   ht   1    -2.80329092
calca4: root=   1 anorm**2=  0.00000000 scale:  1.00000000

          calcsovref: tciref block   1

              civs   1
 refs   1    1.00000    

          calcsovref: scrb block   1

                ci   1
 civs   1    1.00000    

          calcsovref: sovref block   1

              v      1
 ref    1    1.00000    

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  1  1    -46.3018785770  7.5495E-15  5.2163E-02  2.1139E-01  1.0000E-03
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   2

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         807 2x:         336 4x:         141
All internal counts: zz :         356 yy:         930 xx:         352 ww:          66
One-external counts: yz :        1509 yx:        2405 yw:        1220
Two-external counts: yy :        1285 ww:         180 xx:         682 xz:         159 wz:         100 wx:         510
Three-ext.   counts: yx :         657 yw:         358

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2
   ht   1    -2.80329092
   ht   2    -0.05216288    -0.18207924
calca4: root=   1 anorm**2=  0.05783880 scale:  1.02851291
calca4: root=   2 anorm**2=  0.94216120 scale:  1.39361444

          calcsovref: tciref block   1

              civs   1       civs   2
 refs   1    1.00000       1.430610E-13

          calcsovref: scrb block   1

                ci   1         ci   2
 civs   1   0.970650      -0.240497    
 civs   2   0.822134        3.31815    

          calcsovref: sovref block   1

              v      1       v      2
 ref    1   0.970650      -0.240497    

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97064989    -0.24049698
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  2  1    -46.3460601987  4.4182E-02  1.8903E-03  4.0319E-02  1.0000E-03
 mraqcc  #  2  2    -45.5821850085  2.0836E+00  0.0000E+00  4.7001E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   3

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         807 2x:         336 4x:         141
All internal counts: zz :         356 yy:         930 xx:         352 ww:          66
One-external counts: yz :        1509 yx:        2405 yw:        1220
Two-external counts: yy :        1285 ww:         180 xx:         682 xz:         159 wz:         100 wx:         510
Three-ext.   counts: yx :         657 yw:         358

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3
   ht   1    -2.80329092
   ht   2    -0.05216288    -0.18207924
   ht   3     0.00184590    -0.00311825    -0.00660242
calca4: root=   1 anorm**2=  0.06686580 scale:  1.03289196
calca4: root=   2 anorm**2=  0.94373491 scale:  1.39417894
calca4: root=   3 anorm**2=  0.95756136 scale:  1.39912878

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3
 refs   1    1.00000       1.430610E-13  -6.538397E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3
 civs   1   0.966650      -0.172837       0.189369    
 civs   2   0.857031        1.66057       -2.86322    
 civs   3    1.10125        15.5861        9.77693    

          calcsovref: sovref block   1

              v      1       v      2       v      3
 ref    1   0.965930      -0.183027       0.182977    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.96592989    -0.18302731     0.18297664
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  3  1    -46.3481458033  2.0856E-03  2.6084E-04  1.4659E-02  1.0000E-03
 mraqcc  #  3  2    -45.8297222959  2.4754E-01  0.0000E+00  4.0112E-01  1.0000E-04
 mraqcc  #  3  3    -45.4849388543  1.9864E+00  0.0000E+00  4.8252E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   4

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         807 2x:         336 4x:         141
All internal counts: zz :         356 yy:         930 xx:         352 ww:          66
One-external counts: yz :        1509 yx:        2405 yw:        1220
Two-external counts: yy :        1285 ww:         180 xx:         682 xz:         159 wz:         100 wx:         510
Three-ext.   counts: yx :         657 yw:         358

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4
   ht   1    -2.80329092
   ht   2    -0.05216288    -0.18207924
   ht   3     0.00184590    -0.00311825    -0.00660242
   ht   4    -0.00340090    -0.00232724    -0.00050608    -0.00095513
calca4: root=   1 anorm**2=  0.07024178 scale:  1.03452491
calca4: root=   2 anorm**2=  0.95567728 scale:  1.39845532
calca4: root=   3 anorm**2=  0.93756054 scale:  1.39196284
calca4: root=   4 anorm**2=  0.76055300 scale:  1.32685832

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4
 refs   1    1.00000       1.430610E-13  -6.538397E-04   1.226054E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4
 civs   1   0.963909       0.185410       0.189896      -6.451552E-02
 civs   2   0.860619       -1.01767       -3.17105       0.211431    
 civs   3    1.22686       -12.9764        3.82349       -12.5495    
 civs   4   0.918092       -25.0631        17.2951        37.0407    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4
 ref    1   0.964232       0.163165       0.208601      -1.089621E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.96423232     0.16316542     0.20860094    -0.01089621
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  4  1    -46.3483853300  2.3953E-04  4.5877E-05  6.3971E-03  1.0000E-03
 mraqcc  #  4  2    -45.9831130028  1.5339E-01  0.0000E+00  2.2061E-01  1.0000E-04
 mraqcc  #  4  3    -45.5056928412  2.0754E-02  0.0000E+00  4.8873E-01  1.0000E-04
 mraqcc  #  4  4    -45.3974079025  1.8988E+00  0.0000E+00  5.5499E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000999
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   5

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         807 2x:         336 4x:         141
All internal counts: zz :         356 yy:         930 xx:         352 ww:          66
One-external counts: yz :        1509 yx:        2405 yw:        1220
Two-external counts: yy :        1285 ww:         180 xx:         682 xz:         159 wz:         100 wx:         510
Three-ext.   counts: yx :         657 yw:         358

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1    -2.80329092
   ht   2    -0.05216288    -0.18207924
   ht   3     0.00184590    -0.00311825    -0.00660242
   ht   4    -0.00340090    -0.00232724    -0.00050608    -0.00095513
   ht   5     0.00250234    -0.00039021    -0.00030114     0.00004085    -0.00018675
calca4: root=   1 anorm**2=  0.07122854 scale:  1.03500171
calca4: root=   2 anorm**2=  0.96783056 scale:  1.40279384
calca4: root=   3 anorm**2=  0.71701540 scale:  1.31034934
calca4: root=   4 anorm**2=  0.66879947 scale:  1.29182021
calca4: root=   5 anorm**2=  0.94122999 scale:  1.39328030

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1    1.00000       1.430610E-13  -6.538397E-04   1.226054E-03  -8.901476E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 civs   1  -0.963951       0.144044       0.235937       6.575911E-02  -3.249692E-02
 civs   2  -0.861065      -0.779514       -2.37298       -2.20586       0.317148    
 civs   3   -1.24661       -11.1350       -1.57699        3.71976       -14.7699    
 civs   4   -1.06309       -26.0355       0.483722        24.3664        33.3150    
 civs   5  -0.825253       -38.3737        80.7793       -54.0770        38.7907    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1  -0.963705       0.153562       0.165655       0.141338      -1.652326E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1    -0.96370515     0.15356186     0.16565534     0.14133800    -0.01652326
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  5  1    -46.3484231912  3.7861E-05  3.7560E-06  1.7501E-03  1.0000E-03
 mraqcc  #  5  2    -46.0458985650  6.2786E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  5  3    -45.5512584774  4.5566E-02  0.0000E+00  5.6249E-01  1.0000E-04
 mraqcc  #  5  4    -45.4838902217  8.6482E-02  0.0000E+00  5.9948E-01  1.0000E-04
 mraqcc  #  5  5    -45.3782391456  1.8797E+00  0.0000E+00  5.3359E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   6

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         807 2x:         336 4x:         141
All internal counts: zz :         356 yy:         930 xx:         352 ww:          66
One-external counts: yz :        1509 yx:        2405 yw:        1220
Two-external counts: yy :        1285 ww:         180 xx:         682 xz:         159 wz:         100 wx:         510
Three-ext.   counts: yx :         657 yw:         358

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1    -2.80329092
   ht   2    -0.05216288    -0.18207924
   ht   3     0.00184590    -0.00311825    -0.00660242
   ht   4    -0.00340090    -0.00232724    -0.00050608    -0.00095513
   ht   5     0.00250234    -0.00039021    -0.00030114     0.00004085    -0.00018675
   ht   6     0.00044958    -0.00010343     0.00001070     0.00003576    -0.00001262    -0.00002370
calca4: root=   1 anorm**2=  0.07129821 scale:  1.03503537
calca4: root=   2 anorm**2=  0.93952297 scale:  1.39266757
calca4: root=   3 anorm**2=  0.60652584 scale:  1.26748800
calca4: root=   4 anorm**2=  0.66627230 scale:  1.29084170
calca4: root=   5 anorm**2=  0.82186048 scale:  1.34976312
calca4: root=   6 anorm**2=  0.87782585 scale:  1.37033786

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1    1.00000       1.430610E-13  -6.538397E-04   1.226054E-03  -8.901476E-04  -1.597371E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1  -0.963819      -0.136539      -5.701445E-02   0.231521      -8.470094E-02   6.489038E-03
 civs   2  -0.861086       0.587515       0.808841       -2.22138        2.28498       0.292465    
 civs   3   -1.24844        9.40722        6.59090       -1.44097       -8.05489        12.6653    
 civs   4   -1.07662        24.6503        5.22994       -1.46066       -15.7322       -39.4940    
 civs   5  -0.902307        41.9279        1.08400        82.9297        60.7756       -22.5382    
 civs   6   0.941344       -125.503        286.458        12.1588       -74.3290       -77.6344    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.963669      -0.129742      -0.101634       0.154910      -0.140949      -1.775049E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.96366941    -0.12974169    -0.10163448     0.15491001    -0.14094895    -0.01775049

 trial vector basis is being transformed.  new dimension:   2
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  6  1    -46.3484267269  3.5357E-06  6.8266E-07  6.6177E-04  1.0000E-03
 mraqcc  #  6  2    -46.0739947889  2.8096E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  6  3    -45.8945743611  3.4332E-01  0.0000E+00  4.5130E-01  1.0000E-04
 mraqcc  #  6  4    -45.5504459518  6.6556E-02  0.0000E+00  5.7920E-01  1.0000E-04
 mraqcc  #  6  5    -45.4535471634  7.5308E-02  0.0000E+00  5.6387E-01  1.0000E-04
 mraqcc  #  6  6    -45.3546759453  1.8561E+00  0.0000E+00  5.4000E-01  1.0000E-04
 
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mraqcc   convergence criteria satisfied after  6 iterations.

 final mraqcc   convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  6  1    -46.3484267269  3.5357E-06  6.8266E-07  6.6177E-04  1.0000E-03
 mraqcc  #  6  2    -46.0739947889  2.8096E-02  0.0000E+00  0.0000E+00  1.0000E-04

####################CIUDGINFO####################

   aqcc(lrt) vector at position   1 energy=  -46.348426726936

################END OF CIUDGINFO################

 
 a4den factor for root 1  =  1.012025940
diagon:itrnv=   0
    1 of the   3 expansion vectors are transformed.
    1 of the   2 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
maximum overlap with reference    1(overlap= 0.96367)
reference energy =                         -46.3018785770
total aqcc energy =                        -46.3484267269
diagonal element shift (lrtshift) =         -0.0465481500

 information on vector: 1 from unit 11 written to unit 48 filename civout              


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -46.3484267269

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     3    4    5   17   22   31

                                         symmetry   a1   a1   a1   a2   b1   b2 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  3  1       1  0.283754                        +-   +         +            
 z*  3  1       2 -0.720931                        +-        +    +            
 z*  3  1       3 -0.554624                        +-                  +    +  
 z*  3  1       5  0.015308                        +     -   +    +            
 z*  3  1       6  0.021975                        +     -             +    +  
 z*  3  1       7  0.039572                        +    +     -   +            
 z*  3  1       8  0.024476                        +    +    +     -           
 z*  3  1       9 -0.014992                        +    +               -   +  
 z*  3  1      10 -0.067218                        +    +              +     - 
 z*  3  1      11  0.055588                        +         +-   +            
 z*  3  1      12 -0.020830                        +          -        +    +  
 z*  3  1      14  0.043738                        +         +         +     - 
 z*  3  1      16  0.045068                        +              +         +- 
 z*  3  1      17  0.035328                             +-   +    +            
 z*  3  1      18  0.026997                             +-             +    +  
 z*  3  1      23 -0.011834                             +         +    +-      
 z*  3  1      24 -0.017070                             +         +         +- 
 z*  3  1      25  0.020062                                  +-        +    +  
 z*  3  1      26  0.035654                                  +    +    +-      
 z*  3  1      27  0.025176                                  +    +         +- 
 z*  3  1      28  0.027135                                       +-   +    +  
 y   3  1      77  0.010643              4( b2 )    -   +              +       
 y   3  1      92  0.016689              5( a1 )    -        +    +            
 y   3  1      94 -0.010146              7( a1 )    -        +    +            
 y   3  1     131  0.012824              5( a1 )    -                  +    +  
 y   3  1     204  0.022928              5( a1 )   +          -   +            
 y   3  1     229 -0.016212              5( a1 )   +         +     -           
 y   3  1     288  0.017697              5( a1 )   +                    -   +  
 y   3  1     299 -0.012516              5( a1 )   +                   +     - 
 y   3  1     339  0.011264              3( a1 )         -   +    +            
 y   3  1     343 -0.012352              7( a1 )         -   +    +            
 y   3  1     569 -0.011808              4( b1 )             +    +     -      
 y   3  1     623 -0.011410              2( a2 )                   -   +    +  
 x   3  1     672  0.011227    6( a1 )   1( a2 )   +-                          
 x   3  1     679 -0.011690    2( a1 )   2( a2 )   +-                          
 x   3  1     683 -0.019647    6( a1 )   2( a2 )   +-                          
 x   3  1     697 -0.013521    9( a1 )   3( a2 )   +-                          
 x   3  1     724  0.015252    4( b1 )   4( b2 )   +-                          
 x   3  1     740  0.011242    6( b1 )   6( b2 )   +-                          
 x   3  1     748 -0.011184    7( b1 )   7( b2 )   +-                          
 x   3  1     835 -0.015541    5( a1 )   1( a2 )    -        +                 
 x   3  1     846  0.018031    5( a1 )   2( a2 )    -        +                 
 x   3  1     853  0.011539    1( a1 )   3( a2 )    -        +                 
 x   3  1     856  0.011980    4( a1 )   3( a2 )    -        +                 
 x   3  1     870  0.014031    7( b1 )   1( b2 )    -        +                 
 x   3  1     884  0.014632    7( b1 )   3( b2 )    -        +                 
 x   3  1     906  0.013866    1( b1 )   7( b2 )    -        +                 
 x   3  1     908  0.014459    3( b1 )   7( b2 )    -        +                 
 x   3  1     920 -0.016085    2( a1 )   5( a1 )    -             +            
 x   3  1     927  0.017924    5( a1 )   6( a1 )    -             +            
 x   3  1     941  0.013795    1( a1 )   9( a1 )    -             +            
 x   3  1     944  0.014374    4( a1 )   9( a1 )    -             +            
 x   3  1     981  0.010991    1( b1 )   6( b1 )    -             +            
 x   3  1     983  0.011482    3( b1 )   6( b1 )    -             +            
 x   3  1    1002  0.013123    1( b2 )   6( b2 )    -             +            
 x   3  1    1004  0.013626    3( b2 )   6( b2 )    -             +            
 x   3  1    1043 -0.010707   10( a1 )   1( b2 )    -                  +       
 x   3  1    1049 -0.011990    5( a1 )   2( b2 )    -                  +       
 x   3  1    1065 -0.011133   10( a1 )   3( b2 )    -                  +       
 x   3  1    1071  0.013911    5( a1 )   4( b2 )    -                  +       
 x   3  1    1089  0.010598    1( a1 )   6( b2 )    -                  +       
 x   3  1    1092  0.011025    4( a1 )   6( b2 )    -                  +       
 x   3  1    1126  0.011989    5( a1 )   2( b1 )    -                       +  
 x   3  1    1148 -0.013911    5( a1 )   4( b1 )    -                       +  
 x   3  1    1166 -0.011207    1( a1 )   6( b1 )    -                       +  
 x   3  1    1169 -0.011681    4( a1 )   6( b1 )    -                       +  
 w   3  1    4052 -0.016512    1( b1 )   1( b2 )   +    +                      
 w   3  1    4054 -0.012612    3( b1 )   1( b2 )   +    +                      
 w   3  1    4066 -0.012611    1( b1 )   3( b2 )   +    +                      
 w   3  1    4068 -0.010285    3( b1 )   3( b2 )   +    +                      
 w   3  1    4105  0.010172    5( a1 )   1( a2 )   +         +                 
 w   3  1    4134  0.041830    1( b1 )   1( b2 )   +         +                 
 w   3  1    4136  0.032004    3( b1 )   1( b2 )   +         +                 
 w   3  1    4148  0.031882    1( b1 )   3( b2 )   +         +                 
 w   3  1    4150  0.026041    3( b1 )   3( b2 )   +         +                 
 w   3  1    4154  0.010317    7( b1 )   3( b2 )   +         +                 
 w   3  1    4176 -0.011339    1( b1 )   7( b2 )   +         +                 
 w   3  1    4178 -0.012068    3( b1 )   7( b2 )   +         +                 
 w   3  1    4183  0.034250    1( a1 )   1( a1 )   +              +            
 w   3  1    4189  0.036990    1( a1 )   4( a1 )   +              +            
 w   3  1    4192  0.021331    4( a1 )   4( a1 )   +              +            
 w   3  1    4194 -0.010527    2( a1 )   5( a1 )   +              +            
 w   3  1    4219 -0.011084    1( a1 )   9( a1 )   +              +            
 w   3  1    4222 -0.011802    4( a1 )   9( a1 )   +              +            
 w   3  1    4283 -0.028449    1( b2 )   1( b2 )   +              +            
 w   3  1    4286 -0.030728    1( b2 )   3( b2 )   +              +            
 w   3  1    4288 -0.017726    3( b2 )   3( b2 )   +              +            
 w   3  1    4332  0.032252    1( a1 )   1( b2 )   +                   +       
 w   3  1    4335  0.024641    4( a1 )   1( b2 )   +                   +       
 w   3  1    4354  0.024619    1( a1 )   3( b2 )   +                   +       
 w   3  1    4357  0.020084    4( a1 )   3( b2 )   +                   +       
 w   3  1    4409 -0.032261    1( a1 )   1( b1 )   +                        +  
 w   3  1    4412 -0.024568    4( a1 )   1( b1 )   +                        +  
 w   3  1    4431 -0.024702    1( a1 )   3( b1 )   +                        +  
 w   3  1    4434 -0.020087    4( a1 )   3( b1 )   +                        +  
 w   3  1    4589 -0.020116    1( a1 )   1( a1 )        +         +            
 w   3  1    4595 -0.016863    1( a1 )   4( a1 )        +         +            
 w   3  1    4661 -0.022095    1( b1 )   1( b1 )        +         +            
 w   3  1    4664 -0.018707    1( b1 )   3( b1 )        +         +            
 w   3  1    4689 -0.025858    1( b2 )   1( b2 )        +         +            
 w   3  1    4692 -0.022256    1( b2 )   3( b2 )        +         +            
 w   3  1    4694 -0.011202    3( b2 )   3( b2 )        +         +            
 w   3  1    4913  0.059735    1( a1 )   1( a1 )             +    +            
 w   3  1    4919  0.050977    1( a1 )   4( a1 )             +    +            
 w   3  1    4922  0.025662    4( a1 )   4( a1 )             +    +            
 w   3  1    4927  0.018217    5( a1 )   5( a1 )             +    +            
 w   3  1    4985  0.059852    1( b1 )   1( b1 )             +    +            
 w   3  1    4988  0.050933    1( b1 )   3( b1 )             +    +            
 w   3  1    4990  0.025595    3( b1 )   3( b1 )             +    +            
 w   3  1    5013  0.052584    1( b2 )   1( b2 )             +    +            
 w   3  1    5016  0.044401    1( b2 )   3( b2 )             +    +            
 w   3  1    5018  0.022423    3( b2 )   3( b2 )             +    +            
 w   3  1    5433  0.040900    1( a1 )   1( a1 )                       +    +  
 w   3  1    5439  0.034593    1( a1 )   4( a1 )                       +    +  
 w   3  1    5442  0.017481    4( a1 )   4( a1 )                       +    +  
 w   3  1    5447  0.014059    5( a1 )   5( a1 )                       +    +  
 w   3  1    5505  0.045864    1( b1 )   1( b1 )                       +    +  
 w   3  1    5508  0.038955    1( b1 )   3( b1 )                       +    +  
 w   3  1    5510  0.019571    3( b1 )   3( b1 )                       +    +  
 w   3  1    5533  0.046036    1( b2 )   1( b2 )                       +    +  
 w   3  1    5536  0.039320    1( b2 )   3( b2 )                       +    +  
 w   3  1    5538  0.019807    3( b2 )   3( b2 )                       +    +  

 ci coefficient statistics:
           rq > 0.1                3
      0.1> rq > 0.01             119
     0.01> rq > 0.001            837
    0.001> rq > 0.0001          1261
   0.0001> rq > 0.00001          469
  0.00001> rq > 0.000001          73
 0.000001> rq                   2798
           all                  5560
  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.283753650938
     2     2     -0.720931456946
     3     3     -0.554624090504
     4     4     -0.001485590782
     5     5      0.015307770015
     6     6      0.021974616063
     7     7      0.039572327029
     8     8      0.024475600469
     9     9     -0.014991697210
    10    10     -0.067217563530
    11    11      0.055587757462
    12    12     -0.020829702664
    13    13     -0.007276281596
    14    14      0.043737637332
    15    15      0.009006454135
    16    16      0.045068193656
    17    17      0.035327864885
    18    18      0.026997434539
    19    19      0.009115824452
    20    20      0.004713238937
    21    21      0.003451857875
    22    22     -0.006061784675
    23    23     -0.011834320909
    24    24     -0.017070117579
    25    25      0.020062305370
    26    26      0.035653707433
    27    27      0.025176158128
    28    28      0.027134835125

 number of reference csfs (nref) is    28.  root number (iroot) is  1.
 c0**2 =   0.92870179  c0**2(renormalized) =   0.99491657
 c**2 (all zwalks) =   0.92870179  c**2(all zwalks, renormalized) =   0.99491657
 passed aftci ... 
 readint2: molcas,dalton2=                     0                     0
 files%faoints=aoints              
lodens (list->root)=  1
sifcfg setup: record length 4096 DP
# d1 elements per record  3272
# d2 elements per record  2730
  The AQCC density will be calculated.
 item #                     1 suffix=:.drt1.state1:
 read_civout: repnuc=  -43.4985876562748     
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method: 30 last record  1max overlap with ref# 96% root-following 0
 MR-CISD energy:   -46.34842673    -2.84983907
 residuum:     0.00066177
 deltae:     0.00000354
 apxde:     0.00000068
 a4den:     1.01202594
 reference energy:   -46.30187858    -2.80329092

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.96366941    -0.12974169    -0.10163448     0.15491001    -0.14094895    -0.01775049     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.96366941    -0.12974169     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 rdcirefv: extracting reference vector # 1(blocksize=    28 #zcsf=      28)
 =========== Executing IN-CORE method ==========
================================================================================
   DYZ=       0  DYX=       0  DYW=       0
   D0Z=      37  D0Y=       0  D0X=       0  D0W=       0
  DDZI=      96 DDYI=       0 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
 root #                      1 : Scaling(1) with    1.01202593979382      
 corresponding to ref #                      1
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=     139  DYX=     275  DYW=     150
   D0Z=      37  D0Y=     125  D0X=      38  D0W=      12
  DDZI=      96 DDYI=     240 DDXI=      66 DDWI=      30
  DDZE=       0 DDYE=      90 DDXE=      36 DDWE=      15
================================================================================
Trace of MO density:     4.000000
    4  correlated and     8  frozen core electrons

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     1.86500113    -0.03191981    -0.01253663     0.00000000     0.00206525    -0.00735753
   MO   4     0.00000000     0.00000000    -0.03191981     0.09929182    -0.21636062     0.00000000    -0.00239183     0.00102282
   MO   5     0.00000000     0.00000000    -0.01253663    -0.21636062     0.56619498     0.00000000     0.00503454     0.00219158
   MO   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.02483202     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000     0.00206525    -0.00239183     0.00503454     0.00000000     0.00098134     0.00017651
   MO   8     0.00000000     0.00000000    -0.00735753     0.00102282     0.00219158     0.00000000     0.00017651     0.00045931
   MO   9     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.01733126     0.00000000     0.00000000
   MO  10     0.00000000     0.00000000    -0.03042976    -0.00052037    -0.00019384     0.00000000     0.00001081    -0.00003895
   MO  11     0.00000000     0.00000000     0.00347647    -0.00257260     0.00443098     0.00000000     0.00109134     0.00014713
   MO  12     0.00000000     0.00000000     0.00838416    -0.00126897    -0.00292608     0.00000000    -0.00034140    -0.00055212
   MO  13     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00361667     0.00000000     0.00000000
   MO  14     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00028567     0.00000000     0.00000000
   MO  15     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00078533     0.00000000     0.00000000
   MO  16     0.00000000     0.00000000    -0.00023747     0.00003894     0.00001520     0.00000000    -0.00000194     0.00000696

                MO   9         MO  10         MO  11         MO  12         MO  13         MO  14         MO  15         MO  16
   MO   3     0.00000000    -0.03042976     0.00347647     0.00838416     0.00000000     0.00000000     0.00000000    -0.00023747
   MO   4     0.00000000    -0.00052037    -0.00257260    -0.00126897     0.00000000     0.00000000     0.00000000     0.00003894
   MO   5     0.00000000    -0.00019384     0.00443098    -0.00292608     0.00000000     0.00000000     0.00000000     0.00001520
   MO   6     0.01733126     0.00000000     0.00000000     0.00000000     0.00361667    -0.00028567     0.00078533     0.00000000
   MO   7     0.00000000     0.00001081     0.00109134    -0.00034140     0.00000000     0.00000000     0.00000000    -0.00000194
   MO   8     0.00000000    -0.00003895     0.00014713    -0.00055212     0.00000000     0.00000000     0.00000000     0.00000696
   MO   9     0.01248515     0.00000000     0.00000000     0.00000000     0.00288440    -0.00028566     0.00064261     0.00000000
   MO  10     0.00000000     0.00571487     0.00002037     0.00004958     0.00000000     0.00000000     0.00000000    -0.00026458
   MO  11     0.00000000     0.00002037     0.00137936    -0.00037205     0.00000000     0.00000000     0.00000000    -0.00000375
   MO  12     0.00000000     0.00004958    -0.00037205     0.00074216     0.00000000     0.00000000     0.00000000    -0.00000911
   MO  13     0.00288440     0.00000000     0.00000000     0.00000000     0.00088463    -0.00014351     0.00020372     0.00000000
   MO  14    -0.00028566     0.00000000     0.00000000     0.00000000    -0.00014351     0.00150877    -0.00047158     0.00000000
   MO  15     0.00064261     0.00000000     0.00000000     0.00000000     0.00020372    -0.00047158     0.00118569     0.00000000
   MO  16     0.00000000    -0.00026458    -0.00000375    -0.00000911     0.00000000     0.00000000     0.00000000     0.00001756

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     2.00000000     1.86620978     0.65113753     0.03767090     0.01452108     0.00508979     0.00240961
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     0.00183995     0.00084026     0.00053401     0.00032596     0.00007248     0.00001274     0.00001115     0.00000357

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4
   MO   1     0.64870552     0.00552073    -0.00515293     0.00000000
   MO   2     0.00552073     0.00099611    -0.00112106     0.00000000
   MO   3    -0.00515293    -0.00112106     0.00144306     0.00000000
   MO   4     0.00000000     0.00000000     0.00000000     0.00157179

Natural orbital populations,block 2
              MO     1       MO     2       MO     3       MO     4       MO
  occ(*)=     0.64879374     0.00227609     0.00157179     0.00007486

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     0.33957005     0.00000000     0.00370957     0.00000000    -0.00379470     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.02406844     0.00000000     0.01652155     0.00000000     0.00329197    -0.00007439
   MO   4     0.00000000     0.00370957     0.00000000     0.00070883     0.00000000    -0.00080165     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.01652155     0.00000000     0.01170278     0.00000000     0.00259878    -0.00010326
   MO   6     0.00000000    -0.00379470     0.00000000    -0.00080165     0.00000000     0.00101964     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000     0.00329197     0.00000000     0.00259878     0.00000000     0.00079343    -0.00007617
   MO   8     0.00000000     0.00000000    -0.00007439     0.00000000    -0.00010326     0.00000000    -0.00007617     0.00136483
   MO   9     0.00000000     0.00000000     0.00116029     0.00000000     0.00106473     0.00000000     0.00045517     0.00057790

                MO   9
   MO   2     0.00000000
   MO   3     0.00116029
   MO   4     0.00000000
   MO   5     0.00106473
   MO   6     0.00000000
   MO   7     0.00045517
   MO   8     0.00057790
   MO   9     0.00142413

Natural orbital populations,block 3
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     0.33965336     0.03609802     0.00196615     0.00159811     0.00096033     0.00031850     0.00004704
              MO     9       MO
  occ(*)=     0.00001062

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     0.34260685     0.00000000     0.00410035     0.00000000    -0.00432625     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.02498284     0.00000000     0.01748991     0.00000000    -0.00368044     0.00090348
   MO   4     0.00000000     0.00410035     0.00000000     0.00075987     0.00000000    -0.00089192     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.01748991     0.00000000     0.01263803     0.00000000    -0.00294025     0.00077408
   MO   6     0.00000000    -0.00432625     0.00000000    -0.00089192     0.00000000     0.00115758     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000    -0.00368044     0.00000000    -0.00294025     0.00000000     0.00090221    -0.00028075
   MO   8     0.00000000     0.00000000     0.00090348     0.00000000     0.00077408     0.00000000    -0.00028075     0.00154214
   MO   9     0.00000000     0.00000000    -0.00114833     0.00000000    -0.00093849     0.00000000     0.00029654     0.00002105

                MO   9
   MO   2     0.00000000
   MO   3    -0.00114833
   MO   4     0.00000000
   MO   5    -0.00093849
   MO   6     0.00000000
   MO   7     0.00029654
   MO   8     0.00002105
   MO   9     0.00136308

Natural orbital populations,block 4
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     0.34271109     0.03805628     0.00176906     0.00154055     0.00132316     0.00049713     0.00004415
              MO     9       MO
  occ(*)=     0.00001118


 total number of electrons =   12.0000000000

 test slabel:                     4
  a1  a2  b1  b2 


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        a1  partial gross atomic populations
   ao class       1a1        2a1        3a1        4a1        5a1        6a1 
     3_ s       0.000000   2.000000   1.865494   0.000000   0.000000   0.000223
     3_ p       2.000000   0.000000   0.000000   0.000000   0.037635   0.000000
     3_ d       0.000000   0.000000   0.000716   0.651138   0.000000   0.014298
     3_ f       0.000000   0.000000   0.000000   0.000000   0.000036   0.000000
 
   ao class       7a1        8a1        9a1       10a1       11a1       12a1 
     3_ s       0.005013   0.000000   0.000000   0.000000   0.000000   0.000000
     3_ p       0.000000   0.000000   0.000027   0.000004   0.000524   0.000000
     3_ d       0.000077   0.002410   0.000000   0.000000   0.000000   0.000326
     3_ f       0.000000   0.000000   0.001813   0.000836   0.000010   0.000000
 
   ao class      13a1       14a1       15a1       16a1 
     3_ s       0.000000   0.000000   0.000000   0.000004
     3_ p       0.000000   0.000000   0.000011   0.000000
     3_ d       0.000072   0.000013   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000000

                        a2  partial gross atomic populations
   ao class       1a2        2a2        3a2        4a2 
     3_ d       0.648794   0.002276   0.000000   0.000075
     3_ f       0.000000   0.000000   0.001572   0.000000

                        b1  partial gross atomic populations
   ao class       1b1        2b1        3b1        4b1        5b1        6b1 
     3_ p       2.000000   0.000000   0.036021   0.000038   0.000000   0.000272
     3_ d       0.000000   0.339653   0.000000   0.000000   0.001598   0.000000
     3_ f       0.000000   0.000000   0.000077   0.001928   0.000000   0.000689
 
   ao class       7b1        8b1        9b1 
     3_ p       0.000223   0.000000   0.000011
     3_ d       0.000000   0.000047   0.000000
     3_ f       0.000095   0.000000   0.000000

                        b2  partial gross atomic populations
   ao class       1b2        2b2        3b2        4b2        5b2        6b2 
     3_ p       2.000000   0.000000   0.037951   0.000000   0.000029   0.000068
     3_ d       0.000000   0.342711   0.000000   0.001769   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000105   0.000000   0.001511   0.001255
 
   ao class       7b2        8b2        9b2 
     3_ p       0.000463   0.000000   0.000011
     3_ d       0.000000   0.000044   0.000000
     3_ f       0.000034   0.000000   0.000000


                        gross atomic populations
     ao            3_
      s         3.870734
      p         6.113290
      d         2.006016
      f         0.009960
    total      12.000000
 

 Total number of electrons:   12.00000000

 DA ...
