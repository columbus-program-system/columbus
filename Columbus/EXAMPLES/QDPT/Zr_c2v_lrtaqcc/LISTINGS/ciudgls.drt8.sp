1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      2009-03.    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


================ Computing sorting integral file structure ================

                    -----z----- -----y----- -----x----- -----w----- ---total---

                CSFs        28         630        3416        1526        5600
      internal walks       105          90          36          15         246
valid internal walks        28          90          36          15         169
 getinfoarray: info=                     6 :                     1
                  8192                  6552                  8192
                  5460                     0
 icd(3)=                  2976 ci%nnlev=                   595  l2rec=
                  8192  n2max=                  5460
 lcore1,lcore2=              13068137              13044821
 lencor,maxblo              13107200                 60000
========================================
 current settings:
 minbl3         381
 minbl4         381
 locmaxbl3     4288
 locmaxbuf     2144
 maxbl3       60000
 maxbl3       60000
 maxbl4       60000
 maxbuf       30006
========================================

 sorted 4-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)

 sorted 3-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)
 Orig.  diagonal integrals:  1electron:        34
                             0ext.    :        42
                             2ext.    :       336
                             4ext.    :       812


 Orig. off-diag. integrals:  4ext.    :     24411
                             3ext.    :     18720
                             2ext.    :      6792
                             1ext.    :      1272
                             0ext.    :        99
                             2ext. SO :         0
                             1ext. SO :         0
                             0ext. SO :         0
                             1electron:       128


 Sorted integrals            3ext.  w :     17370 x :     16020
                             4ext.  w :     20955 x :     17777


Cycle #  1 sortfile size=    163835(       5 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13068137
Cycle #  2 sortfile size=    163835(       5 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13068137
 minimum size of srtscr:     98301 WP (     3 records)
 maximum size of srtscr:    163835 WP (     5 records)
diagi   file:      4 records  of   1536 WP each=>       6144 WP total
ofdgi   file:      8 records  of   1536 WP each=>      12288 WP total
fil3w   file:      1 records  of  30006 WP each=>      30006 WP total
fil3x   file:      1 records  of  30006 WP each=>      30006 WP total
fil4w   file:      1 records  of  30006 WP each=>      30006 WP total
fil4x   file:      1 records  of  30006 WP each=>      30006 WP total
 compressed index vector length=                    63
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 3,
  GSET = 3,
   DAVCOR =10,
  NCOREL = 4
   nroot=1
  IVMODE = 3
  NBKITR = 1
   nvbkmn=2
  RTOLBK = 1e-3
   niter=30
   nvcimn=2
   rtolci=1e-3
  NVCIMX = 6
  NVRFMX = 6
  NVBKMX = 6
  IDEN  = 1
  CSFPRN = 10,
  update_mode=1
  nvrfmn=2
   lrtshift=-0.0465482970
 /&end
 ------------------------------------------------------------------------
lodens (list->root)=  1
invlodens (root->list)=  1
 LRT calculation: diagonal shift= -4.654829700000000E-002
 bummer (warning):2:changed keyword: nbkitr=                      0
 USING SEGMENTS OF EQUAL SIZE

****************  list of control variables  ****************
 lvlprt =    0      nroot  =    1      noldv  =   0      noldhv =   0
 nunitv =    1      nbkitr =    0      niter  =  30      davcor =  10
 csfprn =   10      ivmode =    3      istrt  =   0      vout   =   0
 iortls =    0      nvbkmx =    6      ibktv  =  -1      ibkthv =  -1
 nvcimx =    6      icitv  =   -1      icithv =  -1      frcsub =   0
 nvbkmn =    2      nvcimn =    2      maxseg =   4      nrfitr =  30
 ncorel =    4      nvrfmx =    6      nvrfmn =   2      iden   =   1
 itran  =    0      froot  =    0      rtmode =   0      ncouple=   1
 skipso =    F      dalton2=    0      molcas =   0      finalv =   0
 finalw =    0      cosmocalc=   0    with_tsklst=   0
 nsegwx =    1     1     1     1
 nseg0x =    1     1     1     1
 nseg1x =    1     1     1     1
 nseg2x =    1     1     1     1
 nseg3x =    1     1     1     1
 nseg4x =    1     1     1     1
 no0ex  =      0    no1ex  =      0    no2ex  =     0    no3ex  =     0
 no4ex  =      0    nodiag =      0
 cdg4ex =    1      c3ex1ex=    1      c2ex0ex=   1
 fileloc=    0     0     0     0     0     0     0     1     1     1
 directhd=   1      noaqccshift_zyxw=      0
 critical_crit=-1.00000    critical_delta= 0.05000

 ctol   = 0.010000    lrtshift=-.046548    smalld =0.001000


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-03    1.000E-03
 Computing density:                    .drt1.state1
 using                      1  nodes and                      1  cores.
 szdg/szodg per processor=                  6144                 12288
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core           13107199 DP per process

********** Integral sort section *************


 workspace allocation information: lencor=  13107199

 echo of the input for program cisrt:
 ------------------------------------------------------------------------
  &input
  maxbl3=60000
  maxbl4=60000
  &end
 ------------------------------------------------------------------------
 
 ( 6) listing file:                    ciudgls             
 ( 5) input file:                      cisrtin   
 (17) cidrt file:                      cidrtfl             
 (11) transformed integrals file:      moints    
 (12) diagonal integral file:          diagint             
 (13) off-diagonal integral file:      ofdgint             
 (31) 4-external w integrals file:     fil4w               
 (32) 4-external x integrals file:     fil4x               
 (33) 3-external w integrals file:     fil3w               
 (34) 3-external x integrals file:     fil3x               
 (21) scratch da sorting file:         srtscr              
 (12) 2-e integral file [fsplit=2]:    moints2   

 input integral file header information:
                                                                                 
 aoints SIFS file created by argos.      zam792            17:56:38.564 17-Dec-13
  cidrt_title                                                                    
 MO-coefficients from mcscf.x                                                    
  with dummy occupation 1.0 for active orbitals                                  
  total ao core energy =    0.000000000                                          
 MCSCF energy =     -46.301878576                                                
 SIFS file created by program tran.      zam792            17:56:38.888 17-Dec-13

 input energy(*) values:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total core energy =   0.000000000000E+00

 nsym = 4 nmot=  34

 symmetry  =    1    2    3    4
 slabel(*) =   a1   a2   b1   b2
 nmpsy(*)  =   14    4    8    8

 info(*) =          1      8192      6552      8192      5460         0

 orbital labels, i:molab(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012  13:tout:013  14:tout:014  15:tout:015  16:tout:016  17:tout:017  18:tout:018  19:tout:019  20:tout:020
  21:tout:021  22:tout:022  23:tout:023  24:tout:024  25:tout:025  26:tout:026  27:tout:027  28:tout:028  29:tout:029  30:tout:030
  31:tout:031  32:tout:032  33:tout:033  34:tout:034

 input parameters:
 prnopt=  0
 ldamin=    4095 ldamax=   32767 ldainc=      64
 maxbuf=   30006 maxbl3=   60000 maxbl4=   60000 intmxo=    1536
  Using 32 bit compression 

 drt information:
  cidrt_title                                                                    
 nmotd =  38 nfctd =   4 nfvtc =   0 nmot  =  34
 nlevel =  34 niot  =   6 lowinl=  29
 orbital-to-level map(*)
   -1  -1  29  30  31   1   2   3   4   5   6   7   8   9  10  11  32  12  13  14
   -1  33  15  16  17  18  19  20  21  -1  34  22  23  24  25  26  27  28
 compressed map(*)
   29  30  31   1   2   3   4   5   6   7   8   9  10  11  32  12  13  14  33  15
   16  17  18  19  20  21  34  22  23  24  25  26  27  28
 levsym(*)
    1   1   1   1   1   1   1   1   1   1   1   2   2   2   3   3   3   3   3   3
    3   4   4   4   4   4   4   4   1   1   1   2   3   4
 repartitioning mu(*)=
   0.  0.  0.  0.  0.  0.

 new core energy added to the energy(*) list.
 from the integral file: h1_core= -4.349858765627E+01

 indxdg: diagonal integral statistics.
 total number of integrals contributing to diagonal matrix elements:      1190
 number with all external indices:       812
 number with half external - half internal indices:       336
 number with all internal indices:        42

 indxof: off-diagonal integral statistics.
    4-external integrals: num=      24411 strt=          1
    3-external integrals: num=      18720 strt=      24412
    2-external integrals: num=       6792 strt=      43132
    1-external integrals: num=       1272 strt=      49924
    0-external integrals: num=         99 strt=      51196

 total number of off-diagonal integrals:       51294


 indxof(2nd)  ittp=   3 numx(ittp)=        6792
 indxof(2nd)  ittp=   4 numx(ittp)=        1272
 indxof(2nd)  ittp=   5 numx(ittp)=          99

 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg=  12931980
 pro2e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652

 pro2e:     25126 integrals read in     5 records.

 pro2e:         0 integrals 34-ext integrals skipped.
 pro1e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652
 pro1e: eref =    0.000000000000000E+00
 total size of srtscr:                     4  records of                  32767 
 WP =               1048544 Bytes
 putdg        1     596    1191    1786    3322   36089   57934    3018   46706   90394
   123161  131353  136813  158652

 putf:       4 buffers of length    1536 written to file 12
 diagonal integral file completed.

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep4e:    17 blocks of linear combinations of 4-external integrals processed.
 number of sorted 4-external integrals      38732
 number of original 4-external integrals    24411


 putf34: external integral file complete. nfilw=    31 nfilx=    32 nrecw=     1 nrecx=     1 lbufp= 30006

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep3e:    19 blocks of linear combinations of 3-external integrals processed.
 number of sorted 3-external integrals      33390
 number of original 3-external integrals    18720


 putf34: external integral file complete. nfilw=    33 nfilx=    34 nrecw=     1 nrecx=     1 lbufp= 30006
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      6792         3     43132     43132      6792     51294
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      1272         4     49924     49924      1272     51294
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd        99         5     51196     51196        99     51294

 putf:       8 buffers of length    1536 written to file 13
 off-diagonal files sort completed.
 executing brd_struct for cisrtinfo
cisrtinfo:
bufszi  1536
 diagfile 4ext:     812 2ext:     336 0ext:      42
 fil4w,fil4x  :   24411 fil3w,fil3x :   18720
 ofdgint  2ext:    6792 1ext:    1272 0ext:      99so0ext:       0so1ext:       0so2ext:       0
buffer minbl4     381 minbl3     381 maxbl2     384nbas:  11   3   7   7   0   0   0   0 maxbuf 30006
 CIUDG version 5.9.7 ( 5-Oct-2004)

 workspace allocation information: lcore=  13107199

 core energy values from the integral file:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -4.349858765627E+01, ietype=    6,   fcore energy of type: H1(*)   

 total core repulsion energy = -4.349858765627E+01
 nmot  =    38 niot  =     6 nfct  =     4 nfvt  =     0
 nrow  =    36 nsym  =     4 ssym  =     4 lenbuf=  1600
 nwalk,xbar:        246      105       90       36       15
 nvalwt,nvalw:      169       28       90       36       15
 ncsft:            5600
 total number of valid internal walks:     169
 nvalz,nvaly,nvalx,nvalw =       28      90      36      15

 cisrt info file parameters:
 file number  12 blocksize   1536
 mxbld   1536
 nd4ext,nd2ext,nd0ext   812   336    42
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int    24411    18720     6792     1272       99        0        0        0
 minbl4,minbl3,maxbl2   381   381   384
 maxbuf 30006
 number of external orbitals per symmetry block:  11   3   7   7
 nmsym   4 number of internal orbitals   6
 executing brd_struct for drt
 executing brd_struct for orbinf
 executing brd_struct for momap
 calcthrxt: niot,maxw1=                     6                    66
 block size     0
 pthz,pthy,pthx,pthw:   105    90    36    15 total internal walks:     246
 maxlp3,n2lp,n1lp,n0lp    66     0     0     0
 orbsym(*)= 1 1 1 2 3 4

 setref:       28 references kept,
               77 references were marked as invalid, out of
              105 total.
 nmb.of records onel     1
 nmb.of records 2-ext     5
 nmb.of records 1-ext     1
 nmb.of records 0-ext     1
 nmb.of records 2-int     0
 nmb.of records 1-int     0
 nmb.of records 0-int     0
 ---------memory usage in DP -----------------
 < n-ex core usage >
     routines:
    fourex            61797
    threx             60409
    twoex              3549
    onex               1823
    allin              1536
    diagon             2109
               =======
   maximum            61797
 
  __ static summary __ 
   reflst                28
   hrfspc                28
               -------
   static->              56
 
  __ core required  __ 
   totstc                56
   max n-ex           61797
               -------
   totnec->           61853
 
  __ core available __ 
   totspc          13107199
   totnec -           61853
               -------
   totvec->        13045346

 number of external paths / symmetry
 vertex x     100      82      98      98
 vertex w     128      82      98      98
segment: free space=    13045346
 reducing frespc by                   946 to               13044400 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1          55|        28|         0|        28|         0|         1|
 -------------------------------------------------------------------------------
  Y 2          90|       630|        28|        90|        28|         2|
 -------------------------------------------------------------------------------
  X 3          36|      3416|       658|        36|       118|         3|
 -------------------------------------------------------------------------------
  W 4          15|      1526|      4074|        15|       154|         4|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=          76DP  conft+indsym=         360DP  drtbuffer=         510 DP

dimension of the ci-matrix ->>>      5600

 executing brd_struct for civct
 gentasklist: ntask=                    20
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS   
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      36      55       3416         28      36      28
     2  4   1    25      two-ext wz   2X  4 1      15      55       1526         28      15      28
     3  4   3    26      two-ext wx*  WX  4 3      15      36       1526       3416      15      36
     4  4   3    27      two-ext wx+  WX  4 3      15      36       1526       3416      15      36
     5  2   1    11      one-ext yz   1X  2 1      90      55        630         28      90      28
     6  3   2    15      1ex3ex yx    3X  3 2      36      90       3416        630      36      90
     7  4   2    16      1ex3ex yw    3X  4 2      15      90       1526        630      15      90
     8  1   1     1      allint zz    OX  1 1      55      55         28         28      28      28
     9  2   2     5      0ex2ex yy    OX  2 2      90      90        630        630      90      90
    10  3   3     6      0ex2ex xx*   OX  3 3      36      36       3416       3416      36      36
    11  3   3    18      0ex2ex xx+   OX  3 3      36      36       3416       3416      36      36
    12  4   4     7      0ex2ex ww*   OX  4 4      15      15       1526       1526      15      15
    13  4   4    19      0ex2ex ww+   OX  4 4      15      15       1526       1526      15      15
    14  2   2    42      four-ext y   4X  2 2      90      90        630        630      90      90
    15  3   3    43      four-ext x   4X  3 3      36      36       3416       3416      36      36
    16  4   4    44      four-ext w   4X  4 4      15      15       1526       1526      15      15
    17  1   1    75      dg-024ext z  OX  1 1      55      55         28         28      28      28
    18  2   2    76      dg-024ext y  OX  2 2      90      90        630        630      90      90
    19  3   3    77      dg-024ext x  OX  3 3      36      36       3416       3416      36      36
    20  4   4    78      dg-024ext w  OX  4 4      15      15       1526       1526      15      15
----------------------------------------------------------------------------------------------------
REDTASK #   1 TIME=  19.000 N=  1 (task/type/sgbra)=(   1/24/0) (
REDTASK #   2 TIME=  18.000 N=  1 (task/type/sgbra)=(   2/25/0) (
REDTASK #   3 TIME=  17.000 N=  1 (task/type/sgbra)=(   3/26/1) (
REDTASK #   4 TIME=  16.000 N=  1 (task/type/sgbra)=(   4/27/2) (
REDTASK #   5 TIME=  15.000 N=  1 (task/type/sgbra)=(   5/11/0) (
REDTASK #   6 TIME=  14.000 N=  1 (task/type/sgbra)=(   6/15/0) (
REDTASK #   7 TIME=  13.000 N=  1 (task/type/sgbra)=(   7/16/0) (
REDTASK #   8 TIME=  12.000 N=  1 (task/type/sgbra)=(   8/ 1/0) (
REDTASK #   9 TIME=  11.000 N=  1 (task/type/sgbra)=(   9/ 5/0) (
REDTASK #  10 TIME=  10.000 N=  1 (task/type/sgbra)=(  10/ 6/1) (
REDTASK #  11 TIME=   9.000 N=  1 (task/type/sgbra)=(  11/18/2) (
REDTASK #  12 TIME=   8.000 N=  1 (task/type/sgbra)=(  12/ 7/1) (
REDTASK #  13 TIME=   7.000 N=  1 (task/type/sgbra)=(  13/19/2) (
REDTASK #  14 TIME=   6.000 N=  1 (task/type/sgbra)=(  14/42/1) (
REDTASK #  15 TIME=   5.000 N=  1 (task/type/sgbra)=(  15/43/1) (
REDTASK #  16 TIME=   4.000 N=  1 (task/type/sgbra)=(  16/44/1) (
REDTASK #  17 TIME=   3.000 N=  1 (task/type/sgbra)=(  17/75/1) (
REDTASK #  18 TIME=   2.000 N=  1 (task/type/sgbra)=(  18/76/1) (
REDTASK #  19 TIME=   1.000 N=  1 (task/type/sgbra)=(  19/77/1) (
REDTASK #  20 TIME=   0.000 N=  1 (task/type/sgbra)=(  20/78/1) (
 initializing v-file: 1:                  5600

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         216 2x:           0 4x:           0
All internal counts: zz :         356 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 reference space has dimension      28
 dsyevx: computed roots 1 to    2(converged:   2)

    root           eigenvalues
    ----           ------------
       1         -46.3018785761
       2         -46.3018785741

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=                    28

         vector  1 from unit 11 written to unit 49 filename cirefv              
 ncorel,neli=                     4                     4
  This is a  mraqcc energy evaluation      

    ------------------------------------------------------------

 setopt2: mraqcc energy evaluation      gvalue=  0.833333333333333      ncorel=
                     4

    ------------------------------------------------------------


    reference energies:

  reference state used is:   1with energy      -46.3018785761

  ### begin active excitation selection ###

 inactive(1:nintern)=AAAAAA
 There are         28 reference CSFs out of         28 valid zwalks.
 There are    0 inactive and    6 active orbitals.
      28 active-active  and        0 inactive excitations.

  ### end active excitation selection ###


 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              5600
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                   30
 residual norm convergence criteria:               0.001000

          starting ci iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         807 2x:         336 4x:         141
All internal counts: zz :         356 yy:         930 xx:         352 ww:          66
One-external counts: yz :        1491 yx:        2405 yw:        1220
Two-external counts: yy :        1285 ww:         180 xx:         682 xz:         155 wz:         100 wx:         510
Three-ext.   counts: yx :         665 yw:         370

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1
   ht   1    -2.80329092
calca4: root=   1 anorm**2=  0.00000000 scale:  1.00000000

          calcsovref: tciref block   1

              civs   1
 refs   1    1.00000    

          calcsovref: scrb block   1

                ci   1
 civs   1    1.00000    

          calcsovref: sovref block   1

              v      1
 ref    1    1.00000    

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  1  1    -46.3018785761  1.9096E-14  5.2649E-02  2.1139E-01  1.0000E-03
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   2

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         807 2x:         336 4x:         141
All internal counts: zz :         356 yy:         930 xx:         352 ww:          66
One-external counts: yz :        1491 yx:        2405 yw:        1220
Two-external counts: yy :        1285 ww:         180 xx:         682 xz:         155 wz:         100 wx:         510
Three-ext.   counts: yx :         665 yw:         370

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2
   ht   1    -2.80329092
   ht   2    -0.05264908    -0.18825492
calca4: root=   1 anorm**2=  0.05838179 scale:  1.02877684
calca4: root=   2 anorm**2=  0.94161821 scale:  1.39341961

          calcsovref: tciref block   1

              civs   1       civs   2
 refs   1    1.00000       3.779288E-14

          calcsovref: scrb block   1

                ci   1         ci   2
 civs   1   0.970370      -0.241623    
 civs   2   0.813763        3.26811    

          calcsovref: sovref block   1

              v      1       v      2
 ref    1   0.970370      -0.241623    

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97037014    -0.24162324
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  2  1    -46.3460306766  4.4152E-02  1.9251E-03  4.0568E-02  1.0000E-03
 mraqcc  #  2  2    -45.5897657176  2.0912E+00  0.0000E+00  4.6720E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   3

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         807 2x:         336 4x:         141
All internal counts: zz :         356 yy:         930 xx:         352 ww:          66
One-external counts: yz :        1491 yx:        2405 yw:        1220
Two-external counts: yy :        1285 ww:         180 xx:         682 xz:         155 wz:         100 wx:         510
Three-ext.   counts: yx :         665 yw:         370

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3
   ht   1    -2.80329092
   ht   2    -0.05264908    -0.18825492
   ht   3     0.00099795    -0.00202251    -0.00679245
calca4: root=   1 anorm**2=  0.06682725 scale:  1.03287330
calca4: root=   2 anorm**2=  0.94802939 scale:  1.39571823
calca4: root=   3 anorm**2=  0.95757683 scale:  1.39913431

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3
 refs   1    1.00000       3.779288E-14  -3.577549E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3
 civs   1   0.966349      -0.171844       0.191526    
 civs   2   0.848715        1.61918       -2.82864    
 civs   3    1.09520        15.6678        9.14904    

          calcsovref: sovref block   1

              v      1       v      2       v      3
 ref    1   0.965957      -0.177449       0.188253    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.96595687    -0.17744944     0.18825255
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  3  1    -46.3481430361  2.1124E-03  2.5783E-04  1.4522E-02  1.0000E-03
 mraqcc  #  3  2    -45.8210331633  2.3127E-01  0.0000E+00  4.0779E-01  1.0000E-04
 mraqcc  #  3  3    -45.5110244236  2.0124E+00  0.0000E+00  4.7621E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   4

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         807 2x:         336 4x:         141
All internal counts: zz :         356 yy:         930 xx:         352 ww:          66
One-external counts: yz :        1491 yx:        2405 yw:        1220
Two-external counts: yy :        1285 ww:         180 xx:         682 xz:         155 wz:         100 wx:         510
Three-ext.   counts: yx :         665 yw:         370

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4
   ht   1    -2.80329092
   ht   2    -0.05264908    -0.18825492
   ht   3     0.00099795    -0.00202251    -0.00679245
   ht   4    -0.00267597    -0.00244138    -0.00052764    -0.00092884
calca4: root=   1 anorm**2=  0.07025679 scale:  1.03453216
calca4: root=   2 anorm**2=  0.96028712 scale:  1.40010254
calca4: root=   3 anorm**2=  0.95261197 scale:  1.39735893
calca4: root=   4 anorm**2=  0.78707772 scale:  1.33681626

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4
 refs   1    1.00000       3.779288E-14  -3.577549E-04   9.681753E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4
 civs   1   0.963749       0.179394       0.202234      -2.086864E-02
 civs   2   0.852323      -0.975477       -3.12326      -0.346376    
 civs   3    1.22051       -12.7604        5.25691       -11.8842    
 civs   4   0.943782       -25.9548        11.1008        39.7462    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4
 ref    1   0.964226       0.158831       0.211101       2.186428E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.96422571     0.15883070     0.21110078     0.02186428
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  4  1    -46.3483864246  2.4339E-04  4.4604E-05  6.3068E-03  1.0000E-03
 mraqcc  #  4  2    -45.9843581944  1.6333E-01  0.0000E+00  2.1789E-01  1.0000E-04
 mraqcc  #  4  3    -45.5210489527  1.0025E-02  0.0000E+00  4.7749E-01  1.0000E-04
 mraqcc  #  4  4    -45.3958548849  1.8973E+00  0.0000E+00  5.4063E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   5

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         807 2x:         336 4x:         141
All internal counts: zz :         356 yy:         930 xx:         352 ww:          66
One-external counts: yz :        1491 yx:        2405 yw:        1220
Two-external counts: yy :        1285 ww:         180 xx:         682 xz:         155 wz:         100 wx:         510
Three-ext.   counts: yx :         665 yw:         370

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1    -2.80329092
   ht   2    -0.05264908    -0.18825492
   ht   3     0.00099795    -0.00202251    -0.00679245
   ht   4    -0.00267597    -0.00244138    -0.00052764    -0.00092884
   ht   5     0.00308621    -0.00039663    -0.00027226     0.00001155    -0.00016181
calca4: root=   1 anorm**2=  0.07122888 scale:  1.03500187
calca4: root=   2 anorm**2=  0.97115648 scale:  1.40397880
calca4: root=   3 anorm**2=  0.92012471 scale:  1.38568565
calca4: root=   4 anorm**2=  0.67484943 scale:  1.29415974
calca4: root=   5 anorm**2=  0.78274207 scale:  1.33519364

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1    1.00000       3.779288E-14  -3.577549E-04   9.681753E-04  -1.098326E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 civs   1  -0.964005       0.131268      -0.253968       7.357173E-02  -2.170198E-02
 civs   2  -0.852753      -0.754689        3.12060       0.687339      -0.349239    
 civs   3   -1.23934       -10.9174       -1.53015       -9.27461       -11.8112    
 civs   4   -1.08624       -27.0103       -8.90078       -5.57788        39.7803    
 civs   5  -0.824306       -37.9076       -42.4618        102.488      -0.898241    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1  -0.963708       0.150658      -0.215401      -4.107638E-02   2.202442E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1    -0.96370760     0.15065760    -0.21540103    -0.04107638     0.02202442
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  5  1    -46.3484231931  3.6768E-05  3.6684E-06  1.7541E-03  1.0000E-03
 mraqcc  #  5  2    -46.0464502406  6.2092E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  5  3    -45.5361554082  1.5106E-02  0.0000E+00  4.7916E-01  1.0000E-04
 mraqcc  #  5  4    -45.4350099407  3.9155E-02  0.0000E+00  6.3645E-01  1.0000E-04
 mraqcc  #  5  5    -45.3958520372  1.8973E+00  0.0000E+00  5.4131E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   6

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         807 2x:         336 4x:         141
All internal counts: zz :         356 yy:         930 xx:         352 ww:          66
One-external counts: yz :        1491 yx:        2405 yw:        1220
Two-external counts: yy :        1285 ww:         180 xx:         682 xz:         155 wz:         100 wx:         510
Three-ext.   counts: yx :         665 yw:         370

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1    -2.80329092
   ht   2    -0.05264908    -0.18825492
   ht   3     0.00099795    -0.00202251    -0.00679245
   ht   4    -0.00267597    -0.00244138    -0.00052764    -0.00092884
   ht   5     0.00308621    -0.00039663    -0.00027226     0.00001155    -0.00016181
   ht   6     0.00090282    -0.00010678     0.00002483     0.00001603     0.00000126    -0.00001483
calca4: root=   1 anorm**2=  0.07129839 scale:  1.03503545
calca4: root=   2 anorm**2=  0.97415833 scale:  1.40504745
calca4: root=   3 anorm**2=  0.92395882 scale:  1.38706843
calca4: root=   4 anorm**2=  0.95801451 scale:  1.39929072
calca4: root=   5 anorm**2=  0.42433510 scale:  1.19345511
calca4: root=   6 anorm**2=  0.93251453 scale:  1.39014910

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1    1.00000       3.779288E-14  -3.577549E-04   9.681753E-04  -1.098326E-03  -3.213503E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1  -0.963720       0.140228      -5.837302E-02  -0.209248      -0.126213      -0.139771    
 civs   2  -0.852773      -0.597254        1.20679        2.91707      -0.111778       0.787297    
 civs   3   -1.24123       -9.60921        6.62471       -7.71099       -3.45427        11.8585    
 civs   4   -1.10066       -25.5625        5.53107       -7.57958        29.4368       -28.9531    
 civs   5  -0.907814       -44.6352       -33.7111        10.1544       -73.6602       -71.4119    
 civs   6    1.01552        110.808        326.883       -106.639       -70.5338       -146.171    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.963671       0.132332      -0.123406      -0.190712       7.091773E-03  -4.663976E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.96367134     0.13233234    -0.12340617    -0.19071199     0.00709177    -0.04663976

 trial vector basis is being transformed.  new dimension:   2
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  6  1    -46.3484269185  3.7254E-06  3.2002E-07  5.1347E-04  1.0000E-03
 mraqcc  #  6  2    -46.0720573143  2.5607E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  6  3    -45.7917283953  2.5557E-01  0.0000E+00  3.9794E-01  1.0000E-04
 mraqcc  #  6  4    -45.4945399794  5.9530E-02  0.0000E+00  4.7561E-01  1.0000E-04
 mraqcc  #  6  5    -45.4068658920  1.1014E-02  0.0000E+00  6.3473E-01  1.0000E-04
 mraqcc  #  6  6    -45.3708629684  1.8723E+00  0.0000E+00  5.6241E-01  1.0000E-04
 
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mraqcc   convergence criteria satisfied after  6 iterations.

 final mraqcc   convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  6  1    -46.3484269185  3.7254E-06  3.2002E-07  5.1347E-04  1.0000E-03
 mraqcc  #  6  2    -46.0720573143  2.5607E-02  0.0000E+00  0.0000E+00  1.0000E-04

####################CIUDGINFO####################

   aqcc(lrt) vector at position   1 energy=  -46.348426918456

################END OF CIUDGINFO################

 
 a4den factor for root 1  =  1.012025970
diagon:itrnv=   0
    1 of the   3 expansion vectors are transformed.
    1 of the   2 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
maximum overlap with reference    1(overlap= 0.96367)
reference energy =                         -46.3018785761
total aqcc energy =                        -46.3484269185
diagonal element shift (lrtshift) =         -0.0465483423

 information on vector: 1 from unit 11 written to unit 48 filename civout              


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -46.3484269185

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     3    4    5   17   22   31

                                         symmetry   a1   a1   a1   a2   b1   b2 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  3  1       1  0.881745                        +-   +                   +  
 z*  3  1       2  0.010805                        +-        +              +  
 z*  3  1       3  0.360987                        +-             +    +       
 z*  3  1       4 -0.011510                        +    +-                  +  
 z*  3  1       5  0.026311                        +     -   +              +  
 z*  3  1       6  0.018627                        +     -        +    +       
 z*  3  1       7  0.037086                        +    +     -             +  
 z*  3  1       8 -0.016003                        +    +    +               - 
 z*  3  1       9 -0.020962                        +    +          -   +       
 z*  3  1      10 -0.075213                        +    +         +     -      
 z*  3  1      11 -0.052900                        +         +-             +  
 z*  3  1      13 -0.015826                        +         +     -   +       
 z*  3  1      14 -0.029447                        +         +    +     -      
 z*  3  1      15  0.020644                        +              +-        +  
 z*  3  1      16  0.043884                        +                   +-   +  
 z*  3  1      18 -0.011519                             +-        +    +       
 z*  3  1      19  0.039895                             +    +-             +  
 z*  3  1      23  0.041582                             +         +-        +  
 z*  3  1      24  0.036100                             +              +-   +  
 z*  3  1      25 -0.018972                                  +-   +    +       
 z*  3  1      28 -0.017593                                       +    +    +- 
 y   3  1      74  0.012270              4( b1 )    -   +         +            
 y   3  1      85 -0.020329              5( a1 )    -   +                   +  
 y   3  1     107 -0.012090              6( a1 )    -        +              +  
 y   3  1     162 -0.028071              5( a1 )   +     -                  +  
 y   3  1     190  0.019852              5( a1 )   +    +                    - 
 y   3  1     257 -0.011475              5( a1 )   +               -   +       
 y   3  1     402  0.010258              7( a1 )        +     -             +  
 y   3  1     446 -0.011952              1( a2 )        +          -        +  
 y   3  1     447  0.014898              2( a2 )        +          -        +  
 y   3  1     471 -0.011036              2( b1 )        +               -   +  
 y   3  1     473  0.013525              4( b1 )        +               -   +  
 x   3  1     679 -0.014258    3( a2 )   7( b1 )   +-                          
 x   3  1     697  0.010071    7( a1 )   2( b2 )   +-                          
 x   3  1     715  0.011110    3( a1 )   4( b2 )   +-                          
 x   3  1     718 -0.016606    6( a1 )   4( b2 )   +-                          
 x   3  1     719 -0.017612    7( a1 )   4( b2 )   +-                          
 x   3  1     743 -0.014645    9( a1 )   6( b2 )   +-                          
 x   3  1     755 -0.014636   10( a1 )   7( b2 )   +-                          
 x   3  1     759  0.013705    3( a2 )   1( b1 )    -   +                      
 x   3  1     765  0.014269    3( a2 )   3( b1 )    -   +                      
 x   3  1     787  0.016917   10( a1 )   1( b2 )    -   +                      
 x   3  1     793  0.019054    5( a1 )   2( b2 )    -   +                      
 x   3  1     809  0.017610   10( a1 )   3( b2 )    -   +                      
 x   3  1     815 -0.022088    5( a1 )   4( b2 )    -   +                      
 x   3  1     833 -0.017881    1( a1 )   6( b2 )    -   +                      
 x   3  1     836 -0.018602    4( a1 )   6( b2 )    -   +                      
 x   3  1    1140 -0.011348    2( a1 )   5( a1 )    -                       +  
 x   3  1    1141  0.015295    3( a1 )   5( a1 )    -                       +  
 x   3  1    1147  0.015153    5( a1 )   6( a1 )    -                       +  
 x   3  1    1152  0.016071    5( a1 )   7( a1 )    -                       +  
 x   3  1    1161  0.014486    1( a1 )   9( a1 )    -                       +  
 x   3  1    1164  0.015084    4( a1 )   9( a1 )    -                       +  
 x   3  1    1201  0.014779    1( b1 )   6( b1 )    -                       +  
 x   3  1    1203  0.015344    3( b1 )   6( b1 )    -                       +  
 x   3  1    1206 -0.012175    1( b1 )   7( b1 )    -                       +  
 x   3  1    1208 -0.012694    3( b1 )   7( b1 )    -                       +  
 x   3  1    1227 -0.011984    1( b2 )   7( b2 )    -                       +  
 x   3  1    1229 -0.012497    3( b2 )   7( b2 )    -                       +  
 x   3  1    2206  0.010162    1( a1 )   1( b2 )        +     -                
 x   3  1    3781 -0.011213    1( a1 )   1( b1 )                        -   +  
 w   3  1    4096 -0.051273    1( a1 )   1( b2 )   +    +                      
 w   3  1    4099 -0.039102    4( a1 )   1( b2 )   +    +                      
 w   3  1    4105  0.014044   10( a1 )   1( b2 )   +    +                      
 w   3  1    4111 -0.012476    5( a1 )   2( b2 )   +    +                      
 w   3  1    4118 -0.039183    1( a1 )   3( b2 )   +    +                      
 w   3  1    4121 -0.031906    4( a1 )   3( b2 )   +    +                      
 w   3  1    4127  0.014925   10( a1 )   3( b2 )   +    +                      
 w   3  1    4151  0.013842    1( a1 )   6( b2 )   +    +                      
 w   3  1    4154  0.014752    4( a1 )   6( b2 )   +    +                      
 w   3  1    4271 -0.020964    1( a1 )   1( b1 )   +              +            
 w   3  1    4274 -0.015977    4( a1 )   1( b1 )   +              +            
 w   3  1    4293 -0.016036    1( a1 )   3( b1 )   +              +            
 w   3  1    4296 -0.013047    4( a1 )   3( b1 )   +              +            
 w   3  1    4402  0.020976    1( b1 )   1( b2 )   +                   +       
 w   3  1    4404  0.015947    3( b1 )   1( b2 )   +                   +       
 w   3  1    4416  0.016080    1( b1 )   3( b2 )   +                   +       
 w   3  1    4418  0.013051    3( b1 )   3( b2 )   +                   +       
 w   3  1    4451  0.027854    1( a1 )   1( a1 )   +                        +  
 w   3  1    4457  0.030076    1( a1 )   4( a1 )   +                        +  
 w   3  1    4460  0.017349    4( a1 )   4( a1 )   +                        +  
 w   3  1    4463  0.010013    3( a1 )   5( a1 )   +                        +  
 w   3  1    4523 -0.040918    1( b1 )   1( b1 )   +                        +  
 w   3  1    4526 -0.044185    1( b1 )   3( b1 )   +                        +  
 w   3  1    4528 -0.025475    3( b1 )   3( b1 )   +                        +  
 w   3  1    4533 -0.010824    1( b1 )   5( b1 )   +                        +  
 w   3  1    4535 -0.010287    3( b1 )   5( b1 )   +                        +  
 w   3  1    4538 -0.012827    1( b1 )   6( b1 )   +                        +  
 w   3  1    4540 -0.013591    3( b1 )   6( b1 )   +                        +  
 w   3  1    4546  0.010196    3( b1 )   7( b1 )   +                        +  
 w   3  1    4551  0.013057    1( b2 )   1( b2 )   +                        +  
 w   3  1    4554  0.014097    1( b2 )   3( b2 )   +                        +  
 w   3  1    4857 -0.068101    1( a1 )   1( a1 )        +                   +  
 w   3  1    4863 -0.057839    1( a1 )   4( a1 )        +                   +  
 w   3  1    4866 -0.029149    4( a1 )   4( a1 )        +                   +  
 w   3  1    4871 -0.022305    5( a1 )   5( a1 )        +                   +  
 w   3  1    4929 -0.070719    1( b1 )   1( b1 )        +                   +  
 w   3  1    4932 -0.060197    1( b1 )   3( b1 )        +                   +  
 w   3  1    4934 -0.030298    3( b1 )   3( b1 )        +                   +  
 w   3  1    4957 -0.071884    1( b2 )   1( b2 )        +                   +  
 w   3  1    4960 -0.061195    1( b2 )   3( b2 )        +                   +  
 w   3  1    4962 -0.030767    3( b2 )   3( b2 )        +                   +  
 w   3  1    5293 -0.029934    1( a1 )   1( a1 )                  +    +       
 w   3  1    5299 -0.025636    1( a1 )   4( a1 )                  +    +       
 w   3  1    5302 -0.012920    4( a1 )   4( a1 )                  +    +       
 w   3  1    5365 -0.026541    1( b1 )   1( b1 )                  +    +       
 w   3  1    5368 -0.022462    1( b1 )   3( b1 )                  +    +       
 w   3  1    5370 -0.011342    3( b1 )   3( b1 )                  +    +       
 w   3  1    5393 -0.029719    1( b2 )   1( b2 )                  +    +       
 w   3  1    5396 -0.025196    1( b2 )   3( b2 )                  +    +       
 w   3  1    5398 -0.012634    3( b2 )   3( b2 )                  +    +       

 ci coefficient statistics:
           rq > 0.1                2
      0.1> rq > 0.01             109
     0.01> rq > 0.001            764
    0.001> rq > 0.0001          1303
   0.0001> rq > 0.00001          488
  0.00001> rq > 0.000001          97
 0.000001> rq                   2837
           all                  5600
  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.881745193404
     2     2      0.010805337063
     3     3      0.360986760414
     4     4     -0.011509564002
     5     5      0.026310596566
     6     6      0.018626591123
     7     7      0.037086284822
     8     8     -0.016002636521
     9     9     -0.020961785037
    10    10     -0.075212541056
    11    11     -0.052899696158
    12    12      0.006681622759
    13    13     -0.015826149890
    14    14     -0.029446680164
    15    15      0.020644165670
    16    16      0.043884161618
    17    17      0.002598977503
    18    18     -0.011518938009
    19    19      0.039894547730
    20    20     -0.003418148172
    21    21     -0.005038711942
    22    22      0.005929840192
    23    23      0.041582141627
    24    24      0.036099545874
    25    25     -0.018971751400
    26    26     -0.004062981788
    27    27      0.008081742644
    28    28     -0.017592971925

 number of reference csfs (nref) is    28.  root number (iroot) is  1.
 c0**2 =   0.92870161  c0**2(renormalized) =   0.99491654
 c**2 (all zwalks) =   0.92870161  c**2(all zwalks, renormalized) =   0.99491654
 passed aftci ... 
 readint2: molcas,dalton2=                     0                     0
 files%faoints=aoints              
lodens (list->root)=  1
sifcfg setup: record length 4096 DP
# d1 elements per record  3272
# d2 elements per record  2730
  The AQCC density will be calculated.
 item #                     1 suffix=:.drt1.state1:
 read_civout: repnuc=  -43.4985876562748     
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method: 30 last record  1max overlap with ref# 96% root-following 0
 MR-CISD energy:   -46.34842692    -2.84983926
 residuum:     0.00051347
 deltae:     0.00000373
 apxde:     0.00000032
 a4den:     1.01202597
 reference energy:   -46.30187858    -2.80329092

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.96367134     0.13233234    -0.12340617    -0.19071199     0.00709177    -0.04663976     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.96367134     0.13233234     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 rdcirefv: extracting reference vector # 1(blocksize=    28 #zcsf=      28)
 =========== Executing IN-CORE method ==========
================================================================================
   DYZ=       0  DYX=       0  DYW=       0
   D0Z=      37  D0Y=       0  D0X=       0  D0W=       0
  DDZI=      96 DDYI=       0 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
 root #                      1 : Scaling(1) with    1.01202597006648      
 corresponding to ref #                      1
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=     135  DYX=     275  DYW=     150
   D0Z=      37  D0Y=     125  D0X=      38  D0W=      12
  DDZI=      96 DDYI=     240 DDXI=      66 DDWI=      30
  DDZE=       0 DDYE=      90 DDXE=      36 DDWE=      15
================================================================================
Trace of MO density:     4.000000
    4  correlated and     8  frozen core electrons

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     1.86507620    -0.00022513     0.02541635     0.00000000     0.00452560     0.00336320
   MO   4     0.00000000     0.00000000    -0.00022513     0.83726137     0.00892714     0.00000000    -0.00417398     0.00533928
   MO   5     0.00000000     0.00000000     0.02541635     0.00892714     0.01538545     0.00000000     0.00156603     0.00108320
   MO   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.02494187     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000     0.00452560    -0.00417398     0.00156603     0.00000000     0.00071402    -0.00037490
   MO   8     0.00000000     0.00000000     0.00336320     0.00533928     0.00108320     0.00000000    -0.00037490     0.00090038
   MO   9     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.01745222     0.00000000     0.00000000
   MO  10     0.00000000     0.00000000    -0.03032863    -0.00000747     0.00041500     0.00000000     0.00002478     0.00001808
   MO  11     0.00000000     0.00000000     0.00488019    -0.00439050     0.00192090     0.00000000     0.00087387    -0.00053783
   MO  12     0.00000000     0.00000000    -0.00460527    -0.00432245    -0.00164205     0.00000000     0.00032155    -0.00095768
   MO  13     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00366197     0.00000000     0.00000000
   MO  14     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00012393     0.00000000     0.00000000
   MO  15     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00222627     0.00000000     0.00000000
   MO  16     0.00000000     0.00000000    -0.00023383     0.00000038    -0.00003096     0.00000000    -0.00000430    -0.00000318

                MO   9         MO  10         MO  11         MO  12         MO  13         MO  14         MO  15         MO  16
   MO   3     0.00000000    -0.03032863     0.00488019    -0.00460527     0.00000000     0.00000000     0.00000000    -0.00023383
   MO   4     0.00000000    -0.00000747    -0.00439050    -0.00432245     0.00000000     0.00000000     0.00000000     0.00000038
   MO   5     0.00000000     0.00041500     0.00192090    -0.00164205     0.00000000     0.00000000     0.00000000    -0.00003096
   MO   6     0.01745222     0.00000000     0.00000000     0.00000000     0.00366197    -0.00012393    -0.00222627     0.00000000
   MO   7     0.00000000     0.00002478     0.00087387     0.00032155     0.00000000     0.00000000     0.00000000    -0.00000430
   MO   8     0.00000000     0.00001808    -0.00053783    -0.00095768     0.00000000     0.00000000     0.00000000    -0.00000318
   MO   9     0.01260206     0.00000000     0.00000000     0.00000000     0.00292441    -0.00013768    -0.00189709     0.00000000
   MO  10     0.00000000     0.00571047     0.00002968    -0.00002738     0.00000000     0.00000000     0.00000000    -0.00026463
   MO  11     0.00000000     0.00002968     0.00120286     0.00057373     0.00000000     0.00000000     0.00000000    -0.00000532
   MO  12     0.00000000    -0.00002738     0.00057373     0.00116644     0.00000000     0.00000000     0.00000000     0.00000499
   MO  13     0.00292441     0.00000000     0.00000000     0.00000000     0.00089715    -0.00008045    -0.00067894     0.00000000
   MO  14    -0.00013768     0.00000000     0.00000000     0.00000000    -0.00008045     0.00139603     0.00003200     0.00000000
   MO  15    -0.00189709     0.00000000     0.00000000     0.00000000    -0.00067894     0.00003200     0.00175758     0.00000000
   MO  16     0.00000000    -0.00026463    -0.00000532     0.00000499     0.00000000     0.00000000     0.00000000     0.00001755

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     2.00000000     1.86596118     0.83745876     0.03811917     0.01564844     0.00516031     0.00275364
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     0.00166616     0.00139293     0.00040573     0.00034543     0.00009064     0.00001271     0.00001070     0.00000363

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4
   MO   1     0.15253291     0.00265506    -0.00304677     0.00000000
   MO   2     0.00265506     0.00054135    -0.00062000     0.00000000
   MO   3    -0.00304677    -0.00062000     0.00078059     0.00000000
   MO   4     0.00000000     0.00000000     0.00000000     0.00131857

Natural orbital populations,block 2
              MO     1       MO     2       MO     3       MO     4       MO
  occ(*)=     0.15264082     0.00131857     0.00118458     0.00002945

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     0.15486315     0.00000000     0.00295460     0.00000000    -0.00345404     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.02470048     0.00000000     0.01720155     0.00000000     0.00356306     0.00152582
   MO   4     0.00000000     0.00295460     0.00000000     0.00058033     0.00000000    -0.00068928     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.01720155     0.00000000     0.01236259     0.00000000     0.00283789     0.00130330
   MO   6     0.00000000    -0.00345404     0.00000000    -0.00068928     0.00000000     0.00088664     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000     0.00356306     0.00000000     0.00283789     0.00000000     0.00086974     0.00046929
   MO   8     0.00000000     0.00000000     0.00152582     0.00000000     0.00130330     0.00000000     0.00046929     0.00134202
   MO   9     0.00000000     0.00000000    -0.00096048     0.00000000    -0.00086661     0.00000000    -0.00035718    -0.00079701

                MO   9
   MO   2     0.00000000
   MO   3    -0.00096048
   MO   4     0.00000000
   MO   5    -0.00086661
   MO   6     0.00000000
   MO   7    -0.00035718
   MO   8    -0.00079701
   MO   9     0.00108334

Natural orbital populations,block 3
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     0.15499769     0.03753837     0.00195907     0.00130518     0.00044772     0.00040226     0.00002724
              MO     9       MO
  occ(*)=     0.00001075

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     0.83625228     0.00000000     0.00665870     0.00000000    -0.00597888     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.02422649     0.00000000     0.01669601     0.00000000    -0.00335857    -0.00184556
   MO   4     0.00000000     0.00665870     0.00000000     0.00117593     0.00000000    -0.00132107     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.01669601     0.00000000     0.01187247     0.00000000    -0.00265793    -0.00163820
   MO   6     0.00000000    -0.00597888     0.00000000    -0.00132107     0.00000000     0.00170722     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000    -0.00335857     0.00000000    -0.00265793     0.00000000     0.00081223     0.00065056
   MO   8     0.00000000     0.00000000    -0.00184556     0.00000000    -0.00163820     0.00000000     0.00065056     0.00199380
   MO   9     0.00000000     0.00000000     0.00016286     0.00000000     0.00010649     0.00000000    -0.00000638    -0.00044223

                MO   9
   MO   2     0.00000000
   MO   3     0.00016286
   MO   4     0.00000000
   MO   5     0.00010649
   MO   6     0.00000000
   MO   7    -0.00000638
   MO   8    -0.00044223
   MO   9     0.00106845

Natural orbital populations,block 4
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     0.83634835     0.03654952     0.00269533     0.00211255     0.00094568     0.00035507     0.00009175
              MO     9       MO
  occ(*)=     0.00001063


 total number of electrons =   12.0000000000

 test slabel:                     4
  a1  a2  b1  b2 


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        a1  partial gross atomic populations
   ao class       1a1        2a1        3a1        4a1        5a1        6a1 
     3_ s       0.000000   2.000000   1.865568   0.000000   0.000000   0.000107
     3_ p       2.000000   0.000000   0.000000   0.000000   0.037864   0.000000
     3_ d       0.000000   0.000000   0.000394   0.837459   0.000000   0.015541
     3_ f       0.000000   0.000000   0.000000   0.000000   0.000255   0.000000
 
   ao class       7a1        8a1        9a1       10a1       11a1       12a1 
     3_ s       0.005126   0.000000   0.000000   0.000000   0.000000   0.000000
     3_ p       0.000000   0.000000   0.000208   0.000000   0.000358   0.000000
     3_ d       0.000035   0.002754   0.000000   0.000000   0.000000   0.000345
     3_ f       0.000000   0.000000   0.001458   0.001393   0.000048   0.000000
 
   ao class      13a1       14a1       15a1       16a1 
     3_ s       0.000000   0.000000   0.000000   0.000004
     3_ p       0.000000   0.000000   0.000011   0.000000
     3_ d       0.000091   0.000013   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000000

                        a2  partial gross atomic populations
   ao class       1a2        2a2        3a2        4a2 
     3_ d       0.152641   0.000000   0.001185   0.000029
     3_ f       0.000000   0.001319   0.000000   0.000000

                        b1  partial gross atomic populations
   ao class       1b1        2b1        3b1        4b1        5b1        6b1 
     3_ p       2.000000   0.000000   0.037364   0.000141   0.000000   0.000417
     3_ d       0.000000   0.154998   0.000000   0.000000   0.001305   0.000000
     3_ f       0.000000   0.000000   0.000175   0.001819   0.000000   0.000031
 
   ao class       7b1        8b1        9b1 
     3_ p       0.000001   0.000000   0.000011
     3_ d       0.000000   0.000027   0.000000
     3_ f       0.000402   0.000000   0.000000

                        b2  partial gross atomic populations
   ao class       1b2        2b2        3b2        4b2        5b2        6b2 
     3_ p       2.000000   0.000000   0.036356   0.000000   0.000146   0.000106
     3_ d       0.000000   0.836348   0.000000   0.002695   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000194   0.000000   0.001966   0.000840
 
   ao class       7b2        8b2        9b2 
     3_ p       0.000293   0.000000   0.000011
     3_ d       0.000000   0.000092   0.000000
     3_ f       0.000062   0.000000   0.000000


                        gross atomic populations
     ao            3_
      s         3.870804
      p         6.113285
      d         2.005951
      f         0.009960
    total      12.000000
 

 Total number of electrons:   12.00000000

 DA ...
