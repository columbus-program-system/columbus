
 program "mcdrt 4.1 a3"

 distinct row table specification and csf
 selection for mcscf wavefunction optimization.

 programmed by: ron shepard

 version date: 17-oct-91


 This Version of Program mcdrt is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              MCDRT       **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 expanded keystroke file:
 /bigscratch/Columbus_C70/tests/Zr_c2v/scr/WORK/mcdrtky                          
 
 input the spin multiplicity [  0]: spin multiplicity:    3    triplet 
 input the total number of electrons [  0]: nelt:     12
 input the number of irreps (1-8) [  0]: nsym:      4
 enter symmetry labels:(y,[n]) enter 4 labels (a4):
 enter symmetry label, default=   1
 enter symmetry label, default=   2
 enter symmetry label, default=   3
 enter symmetry label, default=   4
 input the molecular spatial symmetry (irrep 1:nsym) [  0]: spatial symmetry is irrep number:      1
 
 input the list of doubly-occupied orbitals (sym(i),rmo(i),i=1,ndot):
 number of doubly-occupied orbitals:      4
 number of inactive electrons:      8
 number of active electrons:      4
 level(*)        1   2   3   4
 symd(*)         1   1   3   4
 slabel(*)     a1  a1  b1  b2 
 doub(*)         1   2   1   1
 
 input the active orbitals (sym(i),rmo(i),i=1,nact):
 nact:      6
 level(*)        1   2   3   4   5   6
 syml(*)         1   1   1   2   3   4
 slabel(*)     a1  a1  a1  a2  b1  b2 
 modrt(*)        3   4   5   1   2   2
 input the minimum cumulative occupation for each active level:
  a1  a1  a1  a2  b1  b2 
    3   4   5   1   2   2
 input the maximum cumulative occupation for each active level:
  a1  a1  a1  a2  b1  b2 
    3   4   5   1   2   2
 slabel(*)     a1  a1  a1  a2  b1  b2 
 modrt(*)        3   4   5   1   2   2
 occmin(*)       0   0   0   0   0   4
 occmax(*)       4   4   4   4   4   4
 input the minimum b value for each active level:
  a1  a1  a1  a2  b1  b2 
    3   4   5   1   2   2
 input the maximum b value for each active level:
  a1  a1  a1  a2  b1  b2 
    3   4   5   1   2   2
 slabel(*)     a1  a1  a1  a2  b1  b2 
 modrt(*)        3   4   5   1   2   2
 bmin(*)         0   0   0   0   0   0
 bmax(*)         4   4   4   4   4   4
 input the step masks for each active level:
 modrt:smask=
   3:1111   4:1111   5:1111   1:1111   2:1111   2:1111
 input the number of vertices to be deleted [  0]: number of vertices to be removed (a priori):      0
 number of rows in the drt:     27
 are any arcs to be manually removed?(y,[n])
 nwalk=      21
 input the range of drt levels to print (l1,l2):
 levprt(*)       0   6

 level  0 through level  6 of the drt:

 row lev a b syml lab rmo  l0  l1  l2  l3 isym xbar   y0    y1    y2    xp     z

  27   6 1 2   4 b2    2    0   0   0   0   1     1     0     0     0    21     0
                                            2     0     0     0     0    28     0
                                            3     0     0     0     0    28     0
                                            4     0     0     0     0    28     0
 ........................................

  23   5 1 2   3 b1    2   27   0   0   0   1     1     0     0     0     9     0
                                            2     0     0     0     0    12     0
                                            3     0     0     0     0    12     0
                                            4     0     0     0     0    12     0

  24   5 1 1   3 b1    2    0  27   0   0   1     0     0     0     0    14     0
                                            2     0     0     0     0    10     0
                                            3     0     0     0     0    10     0
                                            4     1     1     0     0     6     9

  25   5 0 3   3 b1    2    0   0  27   0   1     0     0     0     0     1     0
                                            2     0     0     0     0     3     0
                                            3     0     0     0     0     3     0
                                            4     1     1     1     0     3    15

  26   5 0 2   3 b1    2    0   0   0  27   1     1     1     1     1     3    18
                                            2     0     0     0     0     3     0
                                            3     0     0     0     0     3     0
                                            4     0     0     0     0     1     0
 ........................................

  17   4 1 2   2 a2    1   23   0   0   0   1     1     0     0     0     6     0
                                            2     0     0     0     0     9     0
                                            3     0     0     0     0     0     0
                                            4     0     0     0     0     0     0

  18   4 1 1   2 a2    1   24  23   0   0   1     0     0     0     0    11     0
                                            2     0     0     0     0     9     0
                                            3     1     1     0     0     0     6
                                            4     1     0     0     0     0     9

  19   4 1 0   2 a2    1    0  24   0   0   1     0     0     0     0     7     0
                                            2     1     1     0     0     3     9
                                            3     0     0     0     0     0     0
                                            4     0     0     0     0     0     0

  20   4 0 3   2 a2    1   25   0  23   0   1     0     0     0     0     1     0
                                            2     0     0     0     0     3     0
                                            3     1     1     1     0     0     6
                                            4     1     0     0     0     0    15

  21   4 0 2   2 a2    1   26  25  24  23   1     2     1     1     1     3     6
                                            2     2     2     1     0     3    12
                                            3     0     0     0     0     0     0
                                            4     0     0     0     0     0     0

  22   4 0 1   2 a2    1    0  26   0  24   1     0     0     0     0     3     0
                                            2     0     0     0     0     1     0
                                            3     1     1     0     0     0    21
                                            4     1     1     1     1     0    15
 ........................................

  10   3 1 2   1 a1    5   17   0   0   0   1     1     0     0     0     3     0
                                            2     0     0     0     0     0     0
                                            3     0     0     0     0     0     0
                                            4     0     0     0     0     0     0

  11   3 1 1   1 a1    5   18  17   0   0   1     0     0     0     0     8     0
                                            2     1     1     0     0     0     3
                                            3     1     0     0     0     0     6
                                            4     1     0     0     0     0     9

  12   3 1 0   1 a1    5   19  18   0   0   1     0     0     0     0     6     0
                                            2     1     0     0     0     0     9
                                            3     1     1     0     0     0     9
                                            4     1     1     0     0     0     6

  13   3 0 3   1 a1    5   20   0  17   0   1     0     0     0     0     1     0
                                            2     1     1     1     0     0     3
                                            3     1     0     0     0     0     6
                                            4     1     0     0     0     0    15

  14   3 0 2   1 a1    5   21  20  18  17   1     3     1     1     1     3     3
                                            2     2     0     0     0     0    12
                                            3     2     2     1     0     0     9
                                            4     2     2     1     0     0     6

  15   3 0 1   1 a1    5   22  21  19  18   1     3     3     1     0     3     9
                                            2     2     2     0     0     0     9
                                            3     2     1     1     1     0     6
                                            4     2     1     1     1     0     9

  16   3 0 0   1 a1    5    0  22   0  19   1     0     0     0     0     1     0
                                            2     1     1     1     1     0    12
                                            3     1     1     0     0     0    15
                                            4     1     1     0     0     0    21
 ........................................

   5   2 1 1   1 a1    4   11  10   0   0   1     1     1     0     0     2     0
                                            2     1     0     0     0     0     3
                                            3     1     0     0     0     0     6
                                            4     1     0     0     0     0     9

   6   2 1 0   1 a1    4   12  11   0   0   1     0     0     0     0     3     0
                                            2     2     1     0     0     0     3
                                            3     2     1     0     0     0     6
                                            4     2     1     0     0     0     9

   7   2 0 2   1 a1    4   14  13  11  10   1     4     1     1     1     1     2
                                            2     4     2     1     0     0     3
                                            3     4     2     1     0     0     6
                                            4     4     2     1     0     0     9

   8   2 0 1   1 a1    4   15  14  12  11   1     6     3     0     0     2     4
                                            2     6     4     2     1     0     3
                                            3     6     4     2     1     0     6
                                            4     6     4     2     1     0     9

   9   2 0 0   1 a1    4   16  15   0  12   1     3     3     0     0     1    11
                                            2     4     3     1     1     0     9
                                            3     4     3     1     1     0     9
                                            4     4     3     1     1     0     6
 ........................................

   2   1 1 0   1 a1    3    6   5   0   0   1     1     1     0     0     1     0
                                            2     3     1     0     0     0     3
                                            3     3     1     0     0     0     6
                                            4     3     1     0     0     0     9

   3   1 0 1   1 a1    3    8   7   6   5   1    11     5     1     1     1     1
                                            2    13     7     3     1     0     3
                                            3    13     7     3     1     0     6
                                            4    13     7     3     1     0     9

   4   1 0 0   1 a1    3    9   8   0   6   1     9     6     0     0     1     5
                                            2    12     8     2     2     0     3
                                            3    12     8     2     2     0     6
                                            4    12     8     2     2     0     9
 ........................................

   1   0 0 0   0       0    4   3   0   2   1    21    12     1     1     1     0
                                            2    28    16     3     3     0     3
                                            3    28    16     3     3     0     6
                                            4    28    16     3     3     0     9
 ........................................

 initial csf selection step:
 total number of walks in the drt, nwalk=      21
 keep all of these walks?(y,[n]) individual walks will be generated from the drt.
 apply orbital-group occupation restrictions?(y,[n]) apply reference occupation restrictions?(y,[n]) manually select individual walks?(y,[n])
 step-vector based csf selection complete.
       21 csfs selected from      21 total walks.

 beginning step-vector based csf selection.
 enter [step_vector/disposition] pairs:

 enter the active orbital step vector, (-1/ to end):

 step-vector based csf selection complete.
       21 csfs selected from      21 total walks.

 beginning numerical walk selection:
 enter positive walk numbers to add walks,
 negative walk numbers to delete walks, and zero to end.

 input walk number (0 to end) [  0]:
 final csf selection complete.
       21 csfs selected from      21 total walks.
  drt construction and csf selection complete.
 
 input a title card, default=mdrt2_title
  title                                                                         
  
 input a drt file name, default=mcdrtfl
 drt and indexing arrays written to file:
 /bigscratch/Columbus_C70/tests/Zr_c2v/scr/WORK/mcdrtfl                          
 
 write the drt file?([y],n) include step(*) vectors?([y],n) drt file is being written...


   List of selected configurations (step vectors)


   CSF#     1    3 1 1 0 0 0
   CSF#     2    1 3 1 0 0 0
   CSF#     3    1 1 3 0 0 0
   CSF#     4    1 1 0 3 0 0
   CSF#     5    1 1 0 0 3 0
   CSF#     6    1 1 0 0 0 3
   CSF#     7    1 0 1 3 0 0
   CSF#     8    1 0 1 0 3 0
   CSF#     9    1 0 1 0 0 3
   CSF#    10    1 0 0 2 1 1
   CSF#    11    1 0 0 1 2 1
   CSF#    12    1 0 0 1 1 2
   CSF#    13    0 1 1 3 0 0
   CSF#    14    0 1 1 0 3 0
   CSF#    15    0 1 1 0 0 3
   CSF#    16    0 1 0 2 1 1
   CSF#    17    0 1 0 1 2 1
   CSF#    18    0 1 0 1 1 2
   CSF#    19    0 0 1 2 1 1
   CSF#    20    0 0 1 1 2 1
   CSF#    21    0 0 1 1 1 2
