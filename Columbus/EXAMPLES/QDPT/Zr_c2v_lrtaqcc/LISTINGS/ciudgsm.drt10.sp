1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.

  This is a  mraqcc energy evaluation      

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  1  1    -46.2453833591 -4.4409E-16  5.4956E-02  2.1660E-01  1.0000E-03
 mraqcc  #  2  1    -46.2903531922  4.4970E-02  1.2548E-03  2.8340E-02  1.0000E-03
 mraqcc  #  3  1    -46.2917669809  1.4138E-03  1.2530E-04  9.7043E-03  1.0000E-03
 mraqcc  #  4  1    -46.2919567037  1.8972E-04  1.0126E-05  2.9720E-03  1.0000E-03
 mraqcc  #  5  1    -46.2919677533  1.1050E-05  1.4692E-06  9.6224E-04  1.0000E-03

 mraqcc   convergence criteria satisfied after  5 iterations.

 final mraqcc   convergence information:
 mraqcc  #  5  1    -46.2919677533  1.1050E-05  1.4692E-06  9.6224E-04  1.0000E-03

 number of reference csfs (nref) is     4.  root number (iroot) is  1.
 c0**2 =   0.92346860  c0**2(renormalized) =   0.99414295
 c**2 (all zwalks) =   0.92346860  c**2(all zwalks, renormalized) =   0.99414295
