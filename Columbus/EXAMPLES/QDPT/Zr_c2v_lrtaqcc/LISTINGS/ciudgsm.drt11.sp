1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.

  This is a  mraqcc energy evaluation      

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  1  1    -46.2453833562  3.1086E-15  5.5427E-02  2.1660E-01  1.0000E-03
 mraqcc  #  2  1    -46.2902901072  4.4907E-02  1.3305E-03  2.9116E-02  1.0000E-03
 mraqcc  #  3  1    -46.2917577587  1.4677E-03  1.3587E-04  9.8922E-03  1.0000E-03
 mraqcc  #  4  1    -46.2919387971  1.8104E-04  4.1983E-05  4.7468E-03  1.0000E-03
 mraqcc  #  5  1    -46.2919641838  2.5387E-05  4.2349E-06  1.9396E-03  1.0000E-03
 mraqcc  #  6  1    -46.2919686173  4.4335E-06  2.8383E-07  4.3358E-04  1.0000E-03

 mraqcc   convergence criteria satisfied after  6 iterations.

 final mraqcc   convergence information:
 mraqcc  #  6  1    -46.2919686173  4.4335E-06  2.8383E-07  4.3358E-04  1.0000E-03

 number of reference csfs (nref) is     4.  root number (iroot) is  1.
 c0**2 =   0.92342326  c0**2(renormalized) =   0.99413600
 c**2 (all zwalks) =   0.92342326  c**2(all zwalks, renormalized) =   0.99413600
