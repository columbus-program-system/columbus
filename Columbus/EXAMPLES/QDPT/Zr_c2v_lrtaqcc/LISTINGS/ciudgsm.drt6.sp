1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.

  This is a  mraqcc energy evaluation      

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  1  1    -46.3018785770  7.5495E-15  5.2163E-02  2.1139E-01  1.0000E-03
 mraqcc  #  2  1    -46.3460601987  4.4182E-02  1.8903E-03  4.0319E-02  1.0000E-03
 mraqcc  #  3  1    -46.3481458033  2.0856E-03  2.6084E-04  1.4659E-02  1.0000E-03
 mraqcc  #  4  1    -46.3483853300  2.3953E-04  4.5877E-05  6.3971E-03  1.0000E-03
 mraqcc  #  5  1    -46.3484231912  3.7861E-05  3.7560E-06  1.7501E-03  1.0000E-03
 mraqcc  #  6  1    -46.3484267269  3.5357E-06  6.8266E-07  6.6177E-04  1.0000E-03

 mraqcc   convergence criteria satisfied after  6 iterations.

 final mraqcc   convergence information:
 mraqcc  #  6  1    -46.3484267269  3.5357E-06  6.8266E-07  6.6177E-04  1.0000E-03

 number of reference csfs (nref) is    28.  root number (iroot) is  1.
 c0**2 =   0.92870179  c0**2(renormalized) =   0.99491657
 c**2 (all zwalks) =   0.92870179  c**2(all zwalks, renormalized) =   0.99491657
