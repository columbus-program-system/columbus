1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.

  This is a  mraqcc energy evaluation      

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  1  1    -46.2453833560  2.2204E-15  5.5550E-02  2.1660E-01  1.0000E-03
 mraqcc  #  2  1    -46.2902448417  4.4861E-02  1.3682E-03  2.9255E-02  1.0000E-03
 mraqcc  #  3  1    -46.2917521057  1.5073E-03  1.3184E-04  9.9074E-03  1.0000E-03
 mraqcc  #  4  1    -46.2919556676  2.0356E-04  1.0571E-05  3.2329E-03  1.0000E-03
 mraqcc  #  5  1    -46.2919683298  1.2662E-05  5.7639E-07  6.3064E-04  1.0000E-03

 mraqcc   convergence criteria satisfied after  5 iterations.

 final mraqcc   convergence information:
 mraqcc  #  5  1    -46.2919683298  1.2662E-05  5.7639E-07  6.3064E-04  1.0000E-03

 number of reference csfs (nref) is     3.  root number (iroot) is  1.
 c0**2 =   0.92339842  c0**2(renormalized) =   0.99413220
 c**2 (all zwalks) =   0.92339842  c**2(all zwalks, renormalized) =   0.99413220
