1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      2009-03.    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


================ Computing sorting integral file structure ================

                    -----z----- -----y----- -----x----- -----w----- ---total---

                CSFs         3         124        1412           0        1539
      internal walks        15          20          15           0          50
valid internal walks         3          20          15           0          38
 getinfoarray: info=                     6 :                     1
                  8192                  6552                  8192
                  5460                     0
 icd(3)=                  2976 ci%nnlev=                   595  l2rec=
                  8192  n2max=                  5460
 lcore1,lcore2=              13068137              13044821
 lencor,maxblo              13107200                 60000
========================================
 current settings:
 minbl3         381
 minbl4         381
 locmaxbl3     4288
 locmaxbuf     2144
 maxbl3       60000
 maxbl3       60000
 maxbl4       60000
 maxbuf       30006
========================================

 sorted 4-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)

 sorted 3-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)
 Orig.  diagonal integrals:  1electron:        34
                             0ext.    :        42
                             2ext.    :       336
                             4ext.    :       812


 Orig. off-diag. integrals:  4ext.    :     24411
                             3ext.    :     18720
                             2ext.    :      6792
                             1ext.    :      1272
                             0ext.    :        99
                             2ext. SO :         0
                             1ext. SO :         0
                             0ext. SO :         0
                             1electron:       128


 Sorted integrals            3ext.  w :     17370 x :     16020
                             4ext.  w :     20955 x :     17777


Cycle #  1 sortfile size=    163835(       5 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13068137
Cycle #  2 sortfile size=    163835(       5 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13068137
 minimum size of srtscr:     98301 WP (     3 records)
 maximum size of srtscr:    163835 WP (     5 records)
diagi   file:      4 records  of   1536 WP each=>       6144 WP total
ofdgi   file:      8 records  of   1536 WP each=>      12288 WP total
fil3w   file:      1 records  of  30006 WP each=>      30006 WP total
fil3x   file:      1 records  of  30006 WP each=>      30006 WP total
fil4w   file:      1 records  of  30006 WP each=>      30006 WP total
fil4x   file:      1 records  of  30006 WP each=>      30006 WP total
 compressed index vector length=                     9
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 3,
  GSET = 3,
   DAVCOR =10,
  NCOREL = 4
   nroot=1
  IVMODE = 3
  NBKITR = 1
   nvbkmn=2
  RTOLBK = 1e-3
   niter=30
   nvcimn=2
   rtolci=1e-3
  NVCIMX = 6
  NVRFMX = 6
  NVBKMX = 6
  IDEN  = 1
  CSFPRN = 10,
  update_mode=1
  nvrfmn=2
   lrtshift=-0.0465482970
 /&end
 ------------------------------------------------------------------------
lodens (list->root)=  1
invlodens (root->list)=  1
 LRT calculation: diagonal shift= -4.654829700000000E-002
 bummer (warning):2:changed keyword: nbkitr=                      0
 USING SEGMENTS OF EQUAL SIZE

****************  list of control variables  ****************
 lvlprt =    0      nroot  =    1      noldv  =   0      noldhv =   0
 nunitv =    1      nbkitr =    0      niter  =  30      davcor =  10
 csfprn =   10      ivmode =    3      istrt  =   0      vout   =   0
 iortls =    0      nvbkmx =    6      ibktv  =  -1      ibkthv =  -1
 nvcimx =    6      icitv  =   -1      icithv =  -1      frcsub =   0
 nvbkmn =    2      nvcimn =    2      maxseg =   4      nrfitr =  30
 ncorel =    4      nvrfmx =    6      nvrfmn =   2      iden   =   1
 itran  =    0      froot  =    0      rtmode =   0      ncouple=   1
 skipso =    F      dalton2=    0      molcas =   0      finalv =   0
 finalw =    0      cosmocalc=   0    with_tsklst=   0
 nsegwx =    1     1     1     1
 nseg0x =    1     1     1     1
 nseg1x =    1     1     1     1
 nseg2x =    1     1     1     1
 nseg3x =    1     1     1     1
 nseg4x =    1     1     1     1
 no0ex  =      0    no1ex  =      0    no2ex  =     0    no3ex  =     0
 no4ex  =      0    nodiag =      0
 cdg4ex =    1      c3ex1ex=    1      c2ex0ex=   1
 fileloc=    0     0     0     0     0     0     0     1     1     1
 directhd=   1      noaqccshift_zyxw=      0
 critical_crit=-1.00000    critical_delta= 0.05000

 ctol   = 0.010000    lrtshift=-.046548    smalld =0.001000


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-03    1.000E-03
 Computing density:                    .drt1.state1
 using                      1  nodes and                      1  cores.
 szdg/szodg per processor=                  6144                 12288
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core           13107199 DP per process

********** Integral sort section *************


 workspace allocation information: lencor=  13107199

 echo of the input for program cisrt:
 ------------------------------------------------------------------------
  &input
  maxbl3=60000
  maxbl4=60000
  &end
 ------------------------------------------------------------------------
 
 ( 6) listing file:                    ciudgls             
 ( 5) input file:                      cisrtin   
 (17) cidrt file:                      cidrtfl             
 (11) transformed integrals file:      moints    
 (12) diagonal integral file:          diagint             
 (13) off-diagonal integral file:      ofdgint             
 (31) 4-external w integrals file:     fil4w               
 (32) 4-external x integrals file:     fil4x               
 (33) 3-external w integrals file:     fil3w               
 (34) 3-external x integrals file:     fil3x               
 (21) scratch da sorting file:         srtscr              
 (12) 2-e integral file [fsplit=2]:    moints2   

 input integral file header information:
                                                                                 
 aoints SIFS file created by argos.      zam792            17:56:38.564 17-Dec-13
  cidrt_title                                                                    
 MO-coefficients from mcscf.x                                                    
  with dummy occupation 1.0 for active orbitals                                  
  total ao core energy =    0.000000000                                          
 MCSCF energy =     -46.301878576                                                
 SIFS file created by program tran.      zam792            17:56:38.888 17-Dec-13

 input energy(*) values:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total core energy =   0.000000000000E+00

 nsym = 4 nmot=  34

 symmetry  =    1    2    3    4
 slabel(*) =   a1   a2   b1   b2
 nmpsy(*)  =   14    4    8    8

 info(*) =          1      8192      6552      8192      5460         0

 orbital labels, i:molab(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012  13:tout:013  14:tout:014  15:tout:015  16:tout:016  17:tout:017  18:tout:018  19:tout:019  20:tout:020
  21:tout:021  22:tout:022  23:tout:023  24:tout:024  25:tout:025  26:tout:026  27:tout:027  28:tout:028  29:tout:029  30:tout:030
  31:tout:031  32:tout:032  33:tout:033  34:tout:034

 input parameters:
 prnopt=  0
 ldamin=    4095 ldamax=   32767 ldainc=      64
 maxbuf=   30006 maxbl3=   60000 maxbl4=   60000 intmxo=    1536
  Using 32 bit compression 

 drt information:
  cidrt_title                                                                    
 nmotd =  38 nfctd =   4 nfvtc =   0 nmot  =  34
 nlevel =  34 niot  =   6 lowinl=  29
 orbital-to-level map(*)
   -1  -1  29  30  31   1   2   3   4   5   6   7   8   9  10  11  32  12  13  14
   -1  33  15  16  17  18  19  20  21  -1  34  22  23  24  25  26  27  28
 compressed map(*)
   29  30  31   1   2   3   4   5   6   7   8   9  10  11  32  12  13  14  33  15
   16  17  18  19  20  21  34  22  23  24  25  26  27  28
 levsym(*)
    1   1   1   1   1   1   1   1   1   1   1   2   2   2   3   3   3   3   3   3
    3   4   4   4   4   4   4   4   1   1   1   2   3   4
 repartitioning mu(*)=
   0.  0.  0.  0.  0.  0.

 new core energy added to the energy(*) list.
 from the integral file: h1_core= -4.349858765627E+01

 indxdg: diagonal integral statistics.
 total number of integrals contributing to diagonal matrix elements:      1190
 number with all external indices:       812
 number with half external - half internal indices:       336
 number with all internal indices:        42

 indxof: off-diagonal integral statistics.
    4-external integrals: num=      24411 strt=          1
    3-external integrals: num=      18720 strt=      24412
    2-external integrals: num=       6792 strt=      43132
    1-external integrals: num=       1272 strt=      49924
    0-external integrals: num=         99 strt=      51196

 total number of off-diagonal integrals:       51294


 indxof(2nd)  ittp=   3 numx(ittp)=        6792
 indxof(2nd)  ittp=   4 numx(ittp)=        1272
 indxof(2nd)  ittp=   5 numx(ittp)=          99

 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg=  12931980
 pro2e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652

 pro2e:     25126 integrals read in     5 records.

 pro2e:         0 integrals 34-ext integrals skipped.
 pro1e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652
 pro1e: eref =    0.000000000000000E+00
 total size of srtscr:                     4  records of                  32767 
 WP =               1048544 Bytes
 putdg        1     596    1191    1786    3322   36089   57934    3018   46706   90394
   123161  131353  136813  158652

 putf:       4 buffers of length    1536 written to file 12
 diagonal integral file completed.

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep4e:    17 blocks of linear combinations of 4-external integrals processed.
 number of sorted 4-external integrals      38732
 number of original 4-external integrals    24411


 putf34: external integral file complete. nfilw=    31 nfilx=    32 nrecw=     1 nrecx=     1 lbufp= 30006

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep3e:    19 blocks of linear combinations of 3-external integrals processed.
 number of sorted 3-external integrals      33390
 number of original 3-external integrals    18720


 putf34: external integral file complete. nfilw=    33 nfilx=    34 nrecw=     1 nrecx=     1 lbufp= 30006
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      6792         3     43132     43132      6792     51294
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      1272         4     49924     49924      1272     51294
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd        99         5     51196     51196        99     51294

 putf:       8 buffers of length    1536 written to file 13
 off-diagonal files sort completed.
 executing brd_struct for cisrtinfo
cisrtinfo:
bufszi  1536
 diagfile 4ext:     812 2ext:     336 0ext:      42
 fil4w,fil4x  :   24411 fil3w,fil3x :   18720
 ofdgint  2ext:    6792 1ext:    1272 0ext:      99so0ext:       0so1ext:       0so2ext:       0
buffer minbl4     381 minbl3     381 maxbl2     384nbas:  11   3   7   7   0   0   0   0 maxbuf 30006
 CIUDG version 5.9.7 ( 5-Oct-2004)

 workspace allocation information: lcore=  13107199

 core energy values from the integral file:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -4.349858765627E+01, ietype=    6,   fcore energy of type: H1(*)   

 total core repulsion energy = -4.349858765627E+01
 nmot  =    38 niot  =     6 nfct  =     4 nfvt  =     0
 nrow  =    23 nsym  =     4 ssym  =     1 lenbuf=  1600
 nwalk,xbar:         50       15       20       15        0
 nvalwt,nvalw:       38        3       20       15        0
 ncsft:            1539
 total number of valid internal walks:      38
 nvalz,nvaly,nvalx,nvalw =        3      20      15       0

 cisrt info file parameters:
 file number  12 blocksize   1536
 mxbld   1536
 nd4ext,nd2ext,nd0ext   812   336    42
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int    24411    18720     6792     1272       99        0        0        0
 minbl4,minbl3,maxbl2   381   381   384
 maxbuf 30006
 number of external orbitals per symmetry block:  11   3   7   7
 nmsym   4 number of internal orbitals   6
 executing brd_struct for drt
 executing brd_struct for orbinf
 executing brd_struct for momap
 calcthrxt: niot,maxw1=                     6                    11
 block size     0
 pthz,pthy,pthx,pthw:    15    20    15     0 total internal walks:      50
 maxlp3,n2lp,n1lp,n0lp    11     0     0     0
 orbsym(*)= 1 1 1 2 3 4

 setref:        3 references kept,
               12 references were marked as invalid, out of
               15 total.
 nmb.of records onel     1
 nmb.of records 2-ext     5
 nmb.of records 1-ext     1
 nmb.of records 0-ext     1
 nmb.of records 2-int     0
 nmb.of records 1-int     0
 nmb.of records 0-int     0
 ---------memory usage in DP -----------------
 < n-ex core usage >
     routines:
    fourex            61797
    threx             60079
    twoex              3549
    onex               1823
    allin              1536
    diagon             2109
               =======
   maximum            61797
 
  __ static summary __ 
   reflst                 3
   hrfspc                 3
               -------
   static->               6
 
  __ core required  __ 
   totstc                 6
   max n-ex           61797
               -------
   totnec->           61803
 
  __ core available __ 
   totspc          13107199
   totnec -           61803
               -------
   totvec->        13045396

 number of external paths / symmetry
 vertex x     100      82      98      98
 vertex w     128      82      98      98
segment: free space=    13045396
 reducing frespc by                   337 to               13045059 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1           3|         3|         0|         3|         0|         1|
 -------------------------------------------------------------------------------
  Y 2          20|       124|         3|        20|         3|         2|
 -------------------------------------------------------------------------------
  X 3          15|      1412|       127|        15|        23|         3|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=           4DP  conft+indsym=          80DP  drtbuffer=         253 DP

dimension of the ci-matrix ->>>      1539

 executing brd_struct for civct
 gentasklist: ntask=                    12
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS   
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      15       3       1412          3      15       3
     2  2   1    11      one-ext yz   1X  2 1      20       3        124          3      20       3
     3  3   2    15      1ex3ex yx    3X  3 2      15      20       1412        124      15      20
     4  1   1     1      allint zz    OX  1 1       3       3          3          3       3       3
     5  2   2     5      0ex2ex yy    OX  2 2      20      20        124        124      20      20
     6  3   3     6      0ex2ex xx*   OX  3 3      15      15       1412       1412      15      15
     7  3   3    18      0ex2ex xx+   OX  3 3      15      15       1412       1412      15      15
     8  2   2    42      four-ext y   4X  2 2      20      20        124        124      20      20
     9  3   3    43      four-ext x   4X  3 3      15      15       1412       1412      15      15
    10  1   1    75      dg-024ext z  OX  1 1       3       3          3          3       3       3
    11  2   2    76      dg-024ext y  OX  2 2      20      20        124        124      20      20
    12  3   3    77      dg-024ext x  OX  3 3      15      15       1412       1412      15      15
----------------------------------------------------------------------------------------------------
REDTASK #   1 TIME=  11.000 N=  1 (task/type/sgbra)=(   1/24/0) (
REDTASK #   2 TIME=  10.000 N=  1 (task/type/sgbra)=(   2/11/0) (
REDTASK #   3 TIME=   9.000 N=  1 (task/type/sgbra)=(   3/15/0) (
REDTASK #   4 TIME=   8.000 N=  1 (task/type/sgbra)=(   4/ 1/0) (
REDTASK #   5 TIME=   7.000 N=  1 (task/type/sgbra)=(   5/ 5/0) (
REDTASK #   6 TIME=   6.000 N=  1 (task/type/sgbra)=(   6/ 6/1) (
REDTASK #   7 TIME=   5.000 N=  1 (task/type/sgbra)=(   7/18/2) (
REDTASK #   8 TIME=   4.000 N=  1 (task/type/sgbra)=(   8/42/1) (
REDTASK #   9 TIME=   3.000 N=  1 (task/type/sgbra)=(   9/43/1) (
REDTASK #  10 TIME=   2.000 N=  1 (task/type/sgbra)=(  10/75/1) (
REDTASK #  11 TIME=   1.000 N=  1 (task/type/sgbra)=(  11/76/1) (
REDTASK #  12 TIME=   0.000 N=  1 (task/type/sgbra)=(  12/77/1) (
 initializing v-file: 1:                  1539

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:          30 2x:           0 4x:           0
All internal counts: zz :          12 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 reference space has dimension       3
 dsyevx: computed roots 1 to    2(converged:   2)

    root           eigenvalues
    ----           ------------
       1         -46.2453833560
       2         -46.0438684033

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=                     3

         vector  1 from unit 11 written to unit 49 filename cirefv              
 ncorel,neli=                     4                     4
  This is a  mraqcc energy evaluation      

    ------------------------------------------------------------

 setopt2: mraqcc energy evaluation      gvalue=  0.833333333333333      ncorel=
                     4

    ------------------------------------------------------------


    reference energies:

  reference state used is:   1with energy      -46.2453833560

  ### begin active excitation selection ###

 inactive(1:nintern)=AAAAAA
 There are          3 reference CSFs out of          3 valid zwalks.
 There are    0 inactive and    6 active orbitals.
       3 active-active  and        0 inactive excitations.

  ### end active excitation selection ###


 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              1539
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                   30
 residual norm convergence criteria:               0.001000

          starting ci iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         195 2x:          90 4x:          35
All internal counts: zz :          12 yy:          72 xx:          66 ww:           0
One-external counts: yz :          84 yx:         360 yw:           0
Two-external counts: yy :         150 ww:           0 xx:         180 xz:          18 wz:           0 wx:           0
Three-ext.   counts: yx :         147 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1
   ht   1    -2.74679570
calca4: root=   1 anorm**2=  0.00000000 scale:  1.00000000

          calcsovref: tciref block   1

              civs   1
 refs   1    1.00000    

          calcsovref: scrb block   1

                ci   1
 civs   1    1.00000    

          calcsovref: sovref block   1

              v      1
 ref    1    1.00000    

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  1  1    -46.2453833560  2.2204E-15  5.5550E-02  2.1660E-01  1.0000E-03
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   2

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         195 2x:          90 4x:          35
All internal counts: zz :          12 yy:          72 xx:          66 ww:           0
One-external counts: yz :          84 yx:         360 yw:           0
Two-external counts: yy :         150 ww:           0 xx:         180 xz:          18 wz:           0 wx:           0
Three-ext.   counts: yx :         147 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2
   ht   1    -2.74679570
   ht   2    -0.05555007    -0.26395218
calca4: root=   1 anorm**2=  0.07212829 scale:  1.03543628
calca4: root=   2 anorm**2=  0.92787171 scale:  1.38847820

          calcsovref: tciref block   1

              civs   1       civs   2
 refs   1    1.00000      -8.881784E-13

          calcsovref: scrb block   1

                ci   1         ci   2
 civs   1   0.963261      -0.268567    
 civs   2   0.777917        2.79013    

          calcsovref: sovref block   1

              v      1       v      2
 ref    1   0.963261      -0.268567    

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.96326098    -0.26856710
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  2  1    -46.2902448417  4.4861E-02  1.3682E-03  2.9255E-02  1.0000E-03
 mraqcc  #  2  2    -45.6682768514  2.1697E+00  0.0000E+00  4.8344E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   3

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         195 2x:          90 4x:          35
All internal counts: zz :          12 yy:          72 xx:          66 ww:           0
One-external counts: yz :          84 yx:         360 yw:           0
Two-external counts: yy :         150 ww:           0 xx:         180 xz:          18 wz:           0 wx:           0
Three-ext.   counts: yx :         147 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3
   ht   1    -2.74679570
   ht   2    -0.05555007    -0.26395218
   ht   3    -0.00022693     0.01264372    -0.00739295
calca4: root=   1 anorm**2=  0.07113433 scale:  1.03495620
calca4: root=   2 anorm**2=  0.99794402 scale:  1.41348648
calca4: root=   3 anorm**2=  0.93091670 scale:  1.38957429

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3
 refs   1    1.00000      -8.881784E-13   3.339454E-11

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3
 civs   1   0.963777       4.529619E-02   0.262836    
 civs   2   0.799991      -0.365084       -2.87052    
 civs   3    1.09971        17.1999       -6.99663    

          calcsovref: sovref block   1

              v      1       v      2       v      3
 ref    1   0.963777       4.529619E-02   0.262836    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.96377677     0.04529619     0.26283569
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  3  1    -46.2917521057  1.5073E-03  1.3184E-04  9.9074E-03  1.0000E-03
 mraqcc  #  3  2    -45.8838240437  2.1555E-01  0.0000E+00  2.5779E-01  1.0000E-04
 mraqcc  #  3  3    -45.6326601581  2.1341E+00  0.0000E+00  5.0231E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   4

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         195 2x:          90 4x:          35
All internal counts: zz :          12 yy:          72 xx:          66 ww:           0
One-external counts: yz :          84 yx:         360 yw:           0
Two-external counts: yy :         150 ww:           0 xx:         180 xz:          18 wz:           0 wx:           0
Three-ext.   counts: yx :         147 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4
   ht   1    -2.74679570
   ht   2    -0.05555007    -0.26395218
   ht   3    -0.00022693     0.01264372    -0.00739295
   ht   4     0.00006491    -0.00398823    -0.00033870    -0.00061805
calca4: root=   1 anorm**2=  0.07587169 scale:  1.03724235
calca4: root=   2 anorm**2=  0.98798395 scale:  1.40995885
calca4: root=   3 anorm**2=  0.96796594 scale:  1.40284209
calca4: root=   4 anorm**2=  0.96797685 scale:  1.40284598

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4
 refs   1    1.00000      -8.881784E-13   3.339454E-11  -5.197547E-11

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4
 civs   1   0.961316      -0.109329       0.178877       0.178668    
 civs   2   0.802661       0.369636       -1.46290       -2.62790    
 civs   3    1.24708        11.0869        10.7843       -10.7226    
 civs   4    1.54349        44.5539       -23.1034        42.0887    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4
 ref    1   0.961316      -0.109329       0.178877       0.178668    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.96131591    -0.10932854     0.17887657     0.17866773
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  4  1    -46.2919556676  2.0356E-04  1.0571E-05  3.2329E-03  1.0000E-03
 mraqcc  #  4  2    -46.0610105786  1.7719E-01  0.0000E+00  6.0523E-03  1.0000E-04
 mraqcc  #  4  3    -45.8131450076  1.8048E-01  0.0000E+00  3.4322E-01  1.0000E-04
 mraqcc  #  4  4    -45.3994262744  1.9008E+00  0.0000E+00  4.6948E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000999
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   5

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         195 2x:          90 4x:          35
All internal counts: zz :          12 yy:          72 xx:          66 ww:           0
One-external counts: yz :          84 yx:         360 yw:           0
Two-external counts: yy :         150 ww:           0 xx:         180 xz:          18 wz:           0 wx:           0
Three-ext.   counts: yx :         147 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1    -2.74679570
   ht   2    -0.05555007    -0.26395218
   ht   3    -0.00022693     0.01264372    -0.00739295
   ht   4     0.00006491    -0.00398823    -0.00033870    -0.00061805
   ht   5     0.00000546    -0.00033406    -0.00007015    -0.00006152    -0.00003612
calca4: root=   1 anorm**2=  0.07660158 scale:  1.03759413
calca4: root=   2 anorm**2=  0.98658979 scale:  1.40946436
calca4: root=   3 anorm**2=  0.97972042 scale:  1.40702538
calca4: root=   4 anorm**2=  0.96620204 scale:  1.40221327
calca4: root=   5 anorm**2=  0.98215072 scale:  1.40788874

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1    1.00000      -8.881784E-13   3.339454E-11  -5.197547E-11   8.043175E-11

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 civs   1   0.960936       0.114391       0.141369       0.171211      -0.119239    
 civs   2   0.802755      -0.305142       -1.11898       -2.04062        1.91989    
 civs   3    1.25613       -8.53283        13.3071       -7.01430        7.64244    
 civs   4    1.63937       -41.1830       -13.1635       -7.15793       -52.1815    
 civs   5    1.19785       -75.3797       -63.2913        195.374        142.831    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1   0.960936       0.114391       0.141369       0.171211      -0.119239    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     0.96093620     0.11439071     0.14136946     0.17121094    -0.11923877
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  5  1    -46.2919683298  1.2662E-05  5.7639E-07  6.3064E-04  1.0000E-03
 mraqcc  #  5  2    -46.1072418559  4.6231E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  5  3    -45.8355370995  2.2392E-02  0.0000E+00  2.8153E-01  1.0000E-04
 mraqcc  #  5  4    -45.5704819333  1.7106E-01  0.0000E+00  4.6846E-01  1.0000E-04
 mraqcc  #  5  5    -45.3145519088  1.8160E+00  0.0000E+00  4.5415E-01  1.0000E-04
 
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mraqcc   convergence criteria satisfied after  5 iterations.

 final mraqcc   convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  5  1    -46.2919683298  1.2662E-05  5.7639E-07  6.3064E-04  1.0000E-03
 mraqcc  #  5  2    -46.1072418559  4.6231E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  5  3    -45.8355370995  2.2392E-02  0.0000E+00  2.8153E-01  1.0000E-04
 mraqcc  #  5  4    -45.5704819333  1.7106E-01  0.0000E+00  4.6846E-01  1.0000E-04
 mraqcc  #  5  5    -45.3145519088  1.8160E+00  0.0000E+00  4.5415E-01  1.0000E-04

####################CIUDGINFO####################

   aqcc(lrt) vector at position   1 energy=  -46.291968329803

################END OF CIUDGINFO################

 
 a4den factor for root 1  =  1.012932033
diagon:itrnv=   0
    1 of the   6 expansion vectors are transformed.
    1 of the   5 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
maximum overlap with reference    1(overlap= 0.96094)
reference energy =                         -46.2453833560
total aqcc energy =                        -46.2919683298
diagonal element shift (lrtshift) =         -0.0465849738

 information on vector: 1 from unit 11 written to unit 48 filename civout              


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -46.2919683298

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     3    4    5   17   22   31

                                         symmetry   a1   a1   a1   a2   b1   b2 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  5  1       1  0.960936                        +              +    +    +  
 y   5  1      15 -0.013625              1( a2 )   +    +         +            
 y   5  1      19  0.017571              2( b1 )   +    +              +       
 y   5  1      32 -0.012488              1( a2 )   +         +    +            
 y   5  1      43  0.017978              2( b2 )   +         +              +  
 y   5  1      50 -0.103416              2( b2 )   +              +    +       
 y   5  1      52  0.075443              4( b2 )   +              +    +       
 y   5  1      57  0.103409              2( b1 )   +              +         +  
 y   5  1      59 -0.075443              4( b1 )   +              +         +  
 y   5  1      63 -0.103387              1( a2 )   +                   +    +  
 y   5  1      64  0.075441              2( a2 )   +                   +    +  
 y   5  1     121  0.012491              5( a1 )                  +    +    +  
 x   5  1     359  0.024230   10( a1 )   3( a2 )   +              +            
 x   5  1     361 -0.033658    1( b1 )   1( b2 )   +              +            
 x   5  1     363 -0.025043    3( b1 )   1( b2 )   +              +            
 x   5  1     366  0.010293    6( b1 )   1( b2 )   +              +            
 x   5  1     369 -0.016516    2( b1 )   2( b2 )   +              +            
 x   5  1     371  0.015273    4( b1 )   2( b2 )   +              +            
 x   5  1     375 -0.025042    1( b1 )   3( b2 )   +              +            
 x   5  1     377 -0.019750    3( b1 )   3( b2 )   +              +            
 x   5  1     380  0.010743    6( b1 )   3( b2 )   +              +            
 x   5  1     383  0.015272    2( b1 )   4( b2 )   +              +            
 x   5  1     385 -0.022010    4( b1 )   4( b2 )   +              +            
 x   5  1     396  0.010212    1( b1 )   6( b2 )   +              +            
 x   5  1     398  0.010662    3( b1 )   6( b2 )   +              +            
 x   5  1     401 -0.019739    6( b1 )   6( b2 )   +              +            
 x   5  1     410 -0.033660    1( a1 )   1( b1 )   +                   +       
 x   5  1     413 -0.025041    4( a1 )   1( b1 )   +                   +       
 x   5  1     432 -0.025045    1( a1 )   3( b1 )   +                   +       
 x   5  1     435 -0.019746    4( a1 )   3( b1 )   +                   +       
 x   5  1     476 -0.010153    1( a1 )   7( b1 )   +                   +       
 x   5  1     479 -0.010594    4( a1 )   7( b1 )   +                   +       
 x   5  1     485  0.018254   10( a1 )   7( b1 )   +                   +       
 x   5  1     490  0.016515    1( a2 )   2( b2 )   +                   +       
 x   5  1     491 -0.015273    2( a2 )   2( b2 )   +                   +       
 x   5  1     496 -0.015272    1( a2 )   4( b2 )   +                   +       
 x   5  1     497  0.022011    2( a2 )   4( b2 )   +                   +       
 x   5  1     504  0.021344    3( a2 )   6( b2 )   +                   +       
 x   5  1     507 -0.013495    3( a2 )   7( b2 )   +                   +       
 x   5  1     511 -0.016515    1( a2 )   2( b1 )   +                        +  
 x   5  1     512  0.015273    2( a2 )   2( b1 )   +                        +  
 x   5  1     517  0.015272    1( a2 )   4( b1 )   +                        +  
 x   5  1     518 -0.022011    2( a2 )   4( b1 )   +                        +  
 x   5  1     525 -0.019088    3( a2 )   6( b1 )   +                        +  
 x   5  1     528 -0.016533    3( a2 )   7( b1 )   +                        +  
 x   5  1     529  0.033660    1( a1 )   1( b2 )   +                        +  
 x   5  1     532  0.025043    4( a1 )   1( b2 )   +                        +  
 x   5  1     551  0.025046    1( a1 )   3( b2 )   +                        +  
 x   5  1     554  0.019745    4( a1 )   3( b2 )   +                        +  
 x   5  1     598 -0.010207    4( a1 )   7( b2 )   +                        +  
 x   5  1     603 -0.014920    9( a1 )   7( b2 )   +                        +  
 x   5  1     604 -0.011479   10( a1 )   7( b2 )   +                        +  
 x   5  1    1264 -0.016131    3( a2 )   1( b1 )                  +    +       
 x   5  1    1270 -0.014768    3( a2 )   3( b1 )                  +    +       
 x   5  1    1292 -0.018013   10( a1 )   1( b2 )                  +    +       
 x   5  1    1298 -0.022629    5( a1 )   2( b2 )                  +    +       
 x   5  1    1314 -0.016635   10( a1 )   3( b2 )                  +    +       
 x   5  1    1320  0.016845    5( a1 )   4( b2 )                  +    +       
 x   5  1    1338  0.019268    1( a1 )   6( b2 )                  +    +       
 x   5  1    1341  0.017782    4( a1 )   6( b2 )                  +    +       
 x   5  1    1368 -0.015698    9( a1 )   1( b1 )                  +         +  
 x   5  1    1369 -0.011335   10( a1 )   1( b1 )                  +         +  
 x   5  1    1375  0.022629    5( a1 )   2( b1 )                  +         +  
 x   5  1    1390 -0.014454    9( a1 )   3( b1 )                  +         +  
 x   5  1    1391 -0.010493   10( a1 )   3( b1 )                  +         +  
 x   5  1    1397 -0.016845    5( a1 )   4( b1 )                  +         +  
 x   5  1    1415 -0.019330    1( a1 )   6( b1 )                  +         +  
 x   5  1    1418 -0.017833    4( a1 )   6( b1 )                  +         +  
 x   5  1    1439  0.016130    3( a2 )   1( b2 )                  +         +  
 x   5  1    1445  0.014767    3( a2 )   3( b2 )                  +         +  
 x   5  1    1462 -0.022629    5( a1 )   1( a2 )                       +    +  
 x   5  1    1473  0.016846    5( a1 )   2( a2 )                       +    +  
 x   5  1    1480  0.016130    1( a1 )   3( a2 )                       +    +  
 x   5  1    1483  0.014769    4( a1 )   3( a2 )                       +    +  
 x   5  1    1497  0.018985    7( b1 )   1( b2 )                       +    +  
 x   5  1    1511  0.017507    7( b1 )   3( b2 )                       +    +  
 x   5  1    1533  0.018207    1( b1 )   7( b2 )                       +    +  
 x   5  1    1535  0.016782    3( b1 )   7( b2 )                       +    +  

 ci coefficient statistics:
           rq > 0.1                4
      0.1> rq > 0.01              74
     0.01> rq > 0.001            132
    0.001> rq > 0.0001           255
   0.0001> rq > 0.00001          180
  0.00001> rq > 0.000001          42
 0.000001> rq                    852
           all                  1539
  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.960936195412
     2     2     -0.000169504775
     3     3      0.000132255626

 number of reference csfs (nref) is     3.  root number (iroot) is  1.
 c0**2 =   0.92339842  c0**2(renormalized) =   0.99413220
 c**2 (all zwalks) =   0.92339842  c**2(all zwalks, renormalized) =   0.99413220
 passed aftci ... 
 readint2: molcas,dalton2=                     0                     0
 files%faoints=aoints              
lodens (list->root)=  1
sifcfg setup: record length 4096 DP
# d1 elements per record  3272
# d2 elements per record  2730
  The AQCC density will be calculated.
 item #                     1 suffix=:.drt1.state1:
 read_civout: repnuc=  -43.4985876562748     
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method: 30 last record  1max overlap with ref# 96% root-following 0
 MR-CISD energy:   -46.29196833    -2.79338067
 residuum:     0.00063064
 deltae:     0.00001266
 apxde:     0.00000058
 a4den:     1.01293203
 reference energy:   -46.24538336    -2.74679570

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.96093620     0.11439071     0.14136946     0.17121094    -0.11923877     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.96093620     0.11439071     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 rdcirefv: extracting reference vector # 1(blocksize=     3 #zcsf=       3)
 =========== Executing IN-CORE method ==========
================================================================================
   DYZ=       0  DYX=       0  DYW=       0
   D0Z=       3  D0Y=       0  D0X=       0  D0W=       0
  DDZI=      12 DDYI=       0 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
 root #                      1 : Scaling(1) with    1.01293203271507      
 corresponding to ref #                      1
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=      12  DYX=      60  DYW=       0
   D0Z=       3  D0Y=      18  D0X=      12  D0W=       0
  DDZI=      12 DDYI=      60 DDXI=      30 DDWI=       0
  DDZE=       0 DDYE=      20 DDXE=      15 DDWE=       0
================================================================================
Trace of MO density:     4.000000
    4  correlated and     8  frozen core electrons

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.99088304    -0.00013507     0.00010560     0.00000000    -0.00005894     0.00001603
   MO   4     0.00000000     0.00000000    -0.00013507     0.00069987     0.00000041     0.00000000     0.00003358    -0.00004420
   MO   5     0.00000000     0.00000000     0.00010560     0.00000041     0.00069958     0.00000000    -0.00004413    -0.00003362
   MO   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00497909     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000    -0.00005894     0.00003358    -0.00004413     0.00000000     0.00000646     0.00000000
   MO   8     0.00000000     0.00000000     0.00001603    -0.00004420    -0.00003362     0.00000000     0.00000000     0.00000646
   MO   9     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00398824     0.00000000     0.00000000
   MO  10     0.00000000     0.00000000     0.00113867    -0.00000759     0.00000593     0.00000000    -0.00000197     0.00000053
   MO  11     0.00000000     0.00000000    -0.00004078     0.00003223    -0.00003335     0.00000000     0.00000824    -0.00000097
   MO  12     0.00000000     0.00000000    -0.00000569     0.00003343     0.00003226     0.00000000    -0.00000097    -0.00000824
   MO  13     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00129740     0.00000000     0.00000000
   MO  14     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00095791     0.00000000     0.00000000
   MO  15     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00028077     0.00000000     0.00000000
   MO  16     0.00000000     0.00000000    -0.00048229     0.00000043    -0.00000034     0.00000000     0.00000013    -0.00000003

                MO   9         MO  10         MO  11         MO  12         MO  13         MO  14         MO  15         MO  16
   MO   3     0.00000000     0.00113867    -0.00004078    -0.00000569     0.00000000     0.00000000     0.00000000    -0.00048229
   MO   4     0.00000000    -0.00000759     0.00003223     0.00003343     0.00000000     0.00000000     0.00000000     0.00000043
   MO   5     0.00000000     0.00000593    -0.00003335     0.00003226     0.00000000     0.00000000     0.00000000    -0.00000034
   MO   6     0.00398824     0.00000000     0.00000000     0.00000000     0.00129740     0.00095791    -0.00028077     0.00000000
   MO   7     0.00000000    -0.00000197     0.00000824    -0.00000097     0.00000000     0.00000000     0.00000000     0.00000013
   MO   8     0.00000000     0.00000053    -0.00000097    -0.00000824     0.00000000     0.00000000     0.00000000    -0.00000003
   MO   9     0.00330221     0.00000000     0.00000000     0.00000000     0.00114173     0.00099322    -0.00029110     0.00000000
   MO  10     0.00000000     0.00257999    -0.00000245    -0.00000035     0.00000000     0.00000000     0.00000000    -0.00012552
   MO  11     0.00000000    -0.00000245     0.00001766     0.00000000     0.00000000     0.00000000     0.00000000     0.00000015
   MO  12     0.00000000    -0.00000035     0.00000000     0.00001767     0.00000000     0.00000000     0.00000000     0.00000002
   MO  13     0.00114173     0.00000000     0.00000000     0.00000000     0.00044111     0.00049372    -0.00014470     0.00000000
   MO  14     0.00099322     0.00000000     0.00000000     0.00000000     0.00049372     0.00120155     0.00034827     0.00000000
   MO  15    -0.00029110     0.00000000     0.00000000     0.00000000    -0.00014470     0.00034827     0.00228745     0.00000000
   MO  16     0.00000000    -0.00012552     0.00000015     0.00000002     0.00000000     0.00000000     0.00000000     0.00000774

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     2.00000000     0.99088462     0.00887339     0.00258479     0.00238955     0.00090028     0.00070777
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     0.00070681     0.00004650     0.00001598     0.00001597     0.00000169     0.00000144     0.00000054     0.00000054

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4
   MO   1     0.96795539     0.09456889    -0.06664484     0.00000000
   MO   2     0.09456889     0.01275896    -0.00965462     0.00000000
   MO   3    -0.06664484    -0.00965462     0.00760730     0.00000000
   MO   4     0.00000000     0.00000000     0.00000000     0.00348931

Natural orbital populations,block 2
              MO     1       MO     2       MO     3       MO     4       MO
  occ(*)=     0.98187281     0.00633310     0.00348931     0.00011574

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     0.96794762     0.00000000     0.09459015     0.00000000    -0.06664640     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.00497877     0.00000000     0.00398827     0.00000000     0.00129740    -0.00065402
   MO   4     0.00000000     0.09459015     0.00000000     0.01276269     0.00000000    -0.00965727     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.00398827     0.00000000     0.00330245     0.00000000     0.00114178    -0.00067821
   MO   6     0.00000000    -0.06664640     0.00000000    -0.00965727     0.00000000     0.00760847     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000     0.00129740     0.00000000     0.00114178     0.00000000     0.00044112    -0.00033711
   MO   8     0.00000000     0.00000000    -0.00065402     0.00000000    -0.00067821     0.00000000    -0.00033711     0.00183645
   MO   9     0.00000000     0.00000000     0.00075488     0.00000000     0.00078269     0.00000000     0.00038902     0.00063871

                MO   9
   MO   2     0.00000000
   MO   3     0.00075488
   MO   4     0.00000000
   MO   5     0.00078269
   MO   6     0.00000000
   MO   7     0.00038902
   MO   8     0.00063871
   MO   9     0.00165234

Natural orbital populations,block 3
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     0.98186956     0.00887366     0.00633419     0.00238971     0.00089972     0.00011503     0.00004634
              MO     9       MO
  occ(*)=     0.00000169

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     0.96794555     0.00000000     0.09459673     0.00000000    -0.06664657     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.00497884     0.00000000     0.00398828     0.00000000    -0.00129740    -0.00053386
   MO   4     0.00000000     0.09459673     0.00000000     0.01276385     0.00000000    -0.00965794     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.00398828     0.00000000     0.00330244     0.00000000    -0.00114179    -0.00055359
   MO   6     0.00000000    -0.06664657     0.00000000    -0.00965794     0.00000000     0.00760869     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000    -0.00129740     0.00000000    -0.00114179     0.00000000     0.00044113     0.00027517
   MO   8     0.00000000     0.00000000    -0.00053386     0.00000000    -0.00055359     0.00000000     0.00027517     0.00202104
   MO   9     0.00000000     0.00000000    -0.00084407     0.00000000    -0.00087521     0.00000000     0.00043501    -0.00058285

                MO   9
   MO   2     0.00000000
   MO   3    -0.00084407
   MO   4     0.00000000
   MO   5    -0.00087521
   MO   6     0.00000000
   MO   7     0.00043501
   MO   8    -0.00058285
   MO   9     0.00146772

Natural orbital populations,block 4
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     0.98186884     0.00887368     0.00633437     0.00238955     0.00089986     0.00011487     0.00004638
              MO     9       MO
  occ(*)=     0.00000169


 total number of electrons =   12.0000000000

 test slabel:                     4
  a1  a2  b1  b2 


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        a1  partial gross atomic populations
   ao class       1a1        2a1        3a1        4a1        5a1        6a1 
     3_ s       0.000000   2.000000   0.990885   0.000000   0.002585   0.000000
     3_ p       2.000000   0.000000   0.000000   0.008553   0.000000   0.000000
     3_ d       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000320   0.000000   0.002390
 
   ao class       7a1        8a1        9a1       10a1       11a1       12a1 
     3_ s       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     3_ p       0.000126   0.000000   0.000000   0.000042   0.000000   0.000000
     3_ d       0.000000   0.000708   0.000707   0.000000   0.000016   0.000016
     3_ f       0.000774   0.000000   0.000000   0.000005   0.000000   0.000000
 
   ao class      13a1       14a1       15a1       16a1 
     3_ s       0.000000   0.000001   0.000000   0.000000
     3_ p       0.000002   0.000000   0.000000   0.000000
     3_ d       0.000000   0.000000   0.000001   0.000001
     3_ f       0.000000   0.000000   0.000000   0.000000

                        a2  partial gross atomic populations
   ao class       1a2        2a2        3a2        4a2 
     3_ d       0.981873   0.006333   0.000000   0.000116
     3_ f       0.000000   0.000000   0.003489   0.000000

                        b1  partial gross atomic populations
   ao class       1b1        2b1        3b1        4b1        5b1        6b1 
     3_ p       2.000000   0.000000   0.008553   0.000000   0.000000   0.000126
     3_ d       0.000000   0.981870   0.000000   0.006334   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000321   0.000000   0.002390   0.000774
 
   ao class       7b1        8b1        9b1 
     3_ p       0.000000   0.000042   0.000002
     3_ d       0.000115   0.000000   0.000000
     3_ f       0.000000   0.000005   0.000000

                        b2  partial gross atomic populations
   ao class       1b2        2b2        3b2        4b2        5b2        6b2 
     3_ p       2.000000   0.000000   0.008553   0.000000   0.000000   0.000126
     3_ d       0.000000   0.981869   0.000000   0.006334   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000321   0.000000   0.002390   0.000774
 
   ao class       7b2        8b2        9b2 
     3_ p       0.000000   0.000042   0.000002
     3_ d       0.000115   0.000000   0.000000
     3_ f       0.000000   0.000005   0.000000


                        gross atomic populations
     ao            3_
      s         2.993471
      p         6.026167
      d         2.966406
      f         0.013956
    total      12.000000
 

 Total number of electrons:   12.00000000

 DA ...
