1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.

  This is a  mraqcc energy evaluation      

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  1  1    -46.3018785760 -7.1054E-15  5.2706E-02  2.1139E-01  1.0000E-03
 mraqcc  #  2  1    -46.3459600354  4.4081E-02  1.9613E-03  4.0884E-02  1.0000E-03
 mraqcc  #  3  1    -46.3481233327  2.1633E-03  2.7174E-04  1.4903E-02  1.0000E-03
 mraqcc  #  4  1    -46.3483834704  2.6014E-04  4.7430E-05  6.5325E-03  1.0000E-03
 mraqcc  #  5  1    -46.3484226708  3.9200E-05  4.2517E-06  1.8690E-03  1.0000E-03
 mraqcc  #  6  1    -46.3484268264  4.1556E-06  4.1851E-07  5.9787E-04  1.0000E-03

 mraqcc   convergence criteria satisfied after  6 iterations.

 final mraqcc   convergence information:
 mraqcc  #  6  1    -46.3484268264  4.1556E-06  4.1851E-07  5.9787E-04  1.0000E-03

 number of reference csfs (nref) is    21.  root number (iroot) is  1.
 c0**2 =   0.92870142  c0**2(renormalized) =   0.99491651
 c**2 (all zwalks) =   0.92870142  c**2(all zwalks, renormalized) =   0.99491651
