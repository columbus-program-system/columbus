1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.

  This is a  mraqcc energy evaluation      

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  1  1    -46.2740792724  2.2204E-15  5.7941E-02  2.2790E-01  1.0000E-03
 mraqcc  #  2  1    -46.3234577145  4.9378E-02  2.5849E-03  4.6126E-02  1.0000E-03
 mraqcc  #  3  1    -46.3261409093  2.6832E-03  4.1110E-04  1.7868E-02  1.0000E-03
 mraqcc  #  4  1    -46.3264972830  3.5637E-04  7.8142E-05  8.2538E-03  1.0000E-03
 mraqcc  #  5  1    -46.3265608748  6.3592E-05  7.6560E-06  2.5149E-03  1.0000E-03
 mraqcc  #  6  1    -46.3265680245  7.1497E-06  8.2274E-07  8.4352E-04  1.0000E-03

 mraqcc   convergence criteria satisfied after  6 iterations.

 final mraqcc   convergence information:
 mraqcc  #  6  1    -46.3265680245  7.1497E-06  8.2274E-07  8.4352E-04  1.0000E-03

 number of reference csfs (nref) is    24.  root number (iroot) is  1.
 c0**2 =   0.92001641  c0**2(renormalized) =   0.99360263
 c**2 (all zwalks) =   0.92001641  c**2(all zwalks, renormalized) =   0.99360263
