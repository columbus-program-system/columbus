1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      2009-03.    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


================ Computing sorting integral file structure ================

                    -----z----- -----y----- -----x----- -----w----- ---total---

                CSFs        21         622        3424        1496        5563
      internal walks       105          90          36          15         246
valid internal walks        21          90          36          15         162
 getinfoarray: info=                     6 :                     1
                  8192                  6552                  8192
                  5460                     0
 icd(3)=                  2976 ci%nnlev=                   595  l2rec=
                  8192  n2max=                  5460
 lcore1,lcore2=              13068137              13044821
 lencor,maxblo              13107200                 60000
========================================
 current settings:
 minbl3         381
 minbl4         381
 locmaxbl3     4288
 locmaxbuf     2144
 maxbl3       60000
 maxbl3       60000
 maxbl4       60000
 maxbuf       30006
========================================

 sorted 4-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)

 sorted 3-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)
 Orig.  diagonal integrals:  1electron:        34
                             0ext.    :        42
                             2ext.    :       336
                             4ext.    :       812


 Orig. off-diag. integrals:  4ext.    :     24411
                             3ext.    :     18720
                             2ext.    :      6792
                             1ext.    :      1272
                             0ext.    :        99
                             2ext. SO :         0
                             1ext. SO :         0
                             0ext. SO :         0
                             1electron:       128


 Sorted integrals            3ext.  w :     17370 x :     16020
                             4ext.  w :     20955 x :     17777


Cycle #  1 sortfile size=    163835(       5 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13068137
Cycle #  2 sortfile size=    163835(       5 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13068137
 minimum size of srtscr:     98301 WP (     3 records)
 maximum size of srtscr:    163835 WP (     5 records)
diagi   file:      4 records  of   1536 WP each=>       6144 WP total
ofdgi   file:      8 records  of   1536 WP each=>      12288 WP total
fil3w   file:      1 records  of  30006 WP each=>      30006 WP total
fil3x   file:      1 records  of  30006 WP each=>      30006 WP total
fil4w   file:      1 records  of  30006 WP each=>      30006 WP total
fil4x   file:      1 records  of  30006 WP each=>      30006 WP total
 compressed index vector length=                    57
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 3,
  GSET = 3,
   DAVCOR =10,
  NCOREL = 4
   nroot=1
  IVMODE = 3
  NBKITR = 1
   nvbkmn=2
  RTOLBK = 1e-3
   niter=30
   nvcimn=2
   rtolci=1e-3
  NVCIMX = 6
  NVRFMX = 6
  NVBKMX = 6
  IDEN  = 1
  CSFPRN = 10,
  update_mode=1
  nvrfmn=2
   lrtshift=-0.0465482970
 /&end
 ------------------------------------------------------------------------
lodens (list->root)=  1
invlodens (root->list)=  1
 LRT calculation: diagonal shift= -4.654829700000000E-002
 bummer (warning):2:changed keyword: nbkitr=                      0
 USING SEGMENTS OF EQUAL SIZE

****************  list of control variables  ****************
 lvlprt =    0      nroot  =    1      noldv  =   0      noldhv =   0
 nunitv =    1      nbkitr =    0      niter  =  30      davcor =  10
 csfprn =   10      ivmode =    3      istrt  =   0      vout   =   0
 iortls =    0      nvbkmx =    6      ibktv  =  -1      ibkthv =  -1
 nvcimx =    6      icitv  =   -1      icithv =  -1      frcsub =   0
 nvbkmn =    2      nvcimn =    2      maxseg =   4      nrfitr =  30
 ncorel =    4      nvrfmx =    6      nvrfmn =   2      iden   =   1
 itran  =    0      froot  =    0      rtmode =   0      ncouple=   1
 skipso =    F      dalton2=    0      molcas =   0      finalv =   0
 finalw =    0      cosmocalc=   0    with_tsklst=   0
 nsegwx =    1     1     1     1
 nseg0x =    1     1     1     1
 nseg1x =    1     1     1     1
 nseg2x =    1     1     1     1
 nseg3x =    1     1     1     1
 nseg4x =    1     1     1     1
 no0ex  =      0    no1ex  =      0    no2ex  =     0    no3ex  =     0
 no4ex  =      0    nodiag =      0
 cdg4ex =    1      c3ex1ex=    1      c2ex0ex=   1
 fileloc=    0     0     0     0     0     0     0     1     1     1
 directhd=   1      noaqccshift_zyxw=      0
 critical_crit=-1.00000    critical_delta= 0.05000

 ctol   = 0.010000    lrtshift=-.046548    smalld =0.001000


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-03    1.000E-03
 Computing density:                    .drt1.state1
 using                      1  nodes and                      1  cores.
 szdg/szodg per processor=                  6144                 12288
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core           13107199 DP per process

********** Integral sort section *************


 workspace allocation information: lencor=  13107199

 echo of the input for program cisrt:
 ------------------------------------------------------------------------
  &input
  maxbl3=60000
  maxbl4=60000
  &end
 ------------------------------------------------------------------------
 
 ( 6) listing file:                    ciudgls             
 ( 5) input file:                      cisrtin   
 (17) cidrt file:                      cidrtfl             
 (11) transformed integrals file:      moints    
 (12) diagonal integral file:          diagint             
 (13) off-diagonal integral file:      ofdgint             
 (31) 4-external w integrals file:     fil4w               
 (32) 4-external x integrals file:     fil4x               
 (33) 3-external w integrals file:     fil3w               
 (34) 3-external x integrals file:     fil3x               
 (21) scratch da sorting file:         srtscr              
 (12) 2-e integral file [fsplit=2]:    moints2   

 input integral file header information:
                                                                                 
 aoints SIFS file created by argos.      zam792            17:56:38.564 17-Dec-13
  cidrt_title                                                                    
 MO-coefficients from mcscf.x                                                    
  with dummy occupation 1.0 for active orbitals                                  
  total ao core energy =    0.000000000                                          
 MCSCF energy =     -46.301878576                                                
 SIFS file created by program tran.      zam792            17:56:38.888 17-Dec-13

 input energy(*) values:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total core energy =   0.000000000000E+00

 nsym = 4 nmot=  34

 symmetry  =    1    2    3    4
 slabel(*) =   a1   a2   b1   b2
 nmpsy(*)  =   14    4    8    8

 info(*) =          1      8192      6552      8192      5460         0

 orbital labels, i:molab(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012  13:tout:013  14:tout:014  15:tout:015  16:tout:016  17:tout:017  18:tout:018  19:tout:019  20:tout:020
  21:tout:021  22:tout:022  23:tout:023  24:tout:024  25:tout:025  26:tout:026  27:tout:027  28:tout:028  29:tout:029  30:tout:030
  31:tout:031  32:tout:032  33:tout:033  34:tout:034

 input parameters:
 prnopt=  0
 ldamin=    4095 ldamax=   32767 ldainc=      64
 maxbuf=   30006 maxbl3=   60000 maxbl4=   60000 intmxo=    1536
  Using 32 bit compression 

 drt information:
  cidrt_title                                                                    
 nmotd =  38 nfctd =   4 nfvtc =   0 nmot  =  34
 nlevel =  34 niot  =   6 lowinl=  29
 orbital-to-level map(*)
   -1  -1  29  30  31   1   2   3   4   5   6   7   8   9  10  11  32  12  13  14
   -1  33  15  16  17  18  19  20  21  -1  34  22  23  24  25  26  27  28
 compressed map(*)
   29  30  31   1   2   3   4   5   6   7   8   9  10  11  32  12  13  14  33  15
   16  17  18  19  20  21  34  22  23  24  25  26  27  28
 levsym(*)
    1   1   1   1   1   1   1   1   1   1   1   2   2   2   3   3   3   3   3   3
    3   4   4   4   4   4   4   4   1   1   1   2   3   4
 repartitioning mu(*)=
   0.  0.  0.  0.  0.  0.

 new core energy added to the energy(*) list.
 from the integral file: h1_core= -4.349858765627E+01

 indxdg: diagonal integral statistics.
 total number of integrals contributing to diagonal matrix elements:      1190
 number with all external indices:       812
 number with half external - half internal indices:       336
 number with all internal indices:        42

 indxof: off-diagonal integral statistics.
    4-external integrals: num=      24411 strt=          1
    3-external integrals: num=      18720 strt=      24412
    2-external integrals: num=       6792 strt=      43132
    1-external integrals: num=       1272 strt=      49924
    0-external integrals: num=         99 strt=      51196

 total number of off-diagonal integrals:       51294


 indxof(2nd)  ittp=   3 numx(ittp)=        6792
 indxof(2nd)  ittp=   4 numx(ittp)=        1272
 indxof(2nd)  ittp=   5 numx(ittp)=          99

 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg=  12931980
 pro2e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652

 pro2e:     25126 integrals read in     5 records.

 pro2e:         0 integrals 34-ext integrals skipped.
 pro1e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652
 pro1e: eref =    0.000000000000000E+00
 total size of srtscr:                     4  records of                  32767 
 WP =               1048544 Bytes
 putdg        1     596    1191    1786    3322   36089   57934    3018   46706   90394
   123161  131353  136813  158652

 putf:       4 buffers of length    1536 written to file 12
 diagonal integral file completed.

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep4e:    17 blocks of linear combinations of 4-external integrals processed.
 number of sorted 4-external integrals      38732
 number of original 4-external integrals    24411


 putf34: external integral file complete. nfilw=    31 nfilx=    32 nrecw=     1 nrecx=     1 lbufp= 30006

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep3e:    19 blocks of linear combinations of 3-external integrals processed.
 number of sorted 3-external integrals      33390
 number of original 3-external integrals    18720


 putf34: external integral file complete. nfilw=    33 nfilx=    34 nrecw=     1 nrecx=     1 lbufp= 30006
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      6792         3     43132     43132      6792     51294
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      1272         4     49924     49924      1272     51294
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd        99         5     51196     51196        99     51294

 putf:       8 buffers of length    1536 written to file 13
 off-diagonal files sort completed.
 executing brd_struct for cisrtinfo
cisrtinfo:
bufszi  1536
 diagfile 4ext:     812 2ext:     336 0ext:      42
 fil4w,fil4x  :   24411 fil3w,fil3x :   18720
 ofdgint  2ext:    6792 1ext:    1272 0ext:      99so0ext:       0so1ext:       0so2ext:       0
buffer minbl4     381 minbl3     381 maxbl2     384nbas:  11   3   7   7   0   0   0   0 maxbuf 30006
 CIUDG version 5.9.7 ( 5-Oct-2004)

 workspace allocation information: lcore=  13107199

 core energy values from the integral file:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -4.349858765627E+01, ietype=    6,   fcore energy of type: H1(*)   

 total core repulsion energy = -4.349858765627E+01
 nmot  =    38 niot  =     6 nfct  =     4 nfvt  =     0
 nrow  =    36 nsym  =     4 ssym  =     1 lenbuf=  1600
 nwalk,xbar:        246      105       90       36       15
 nvalwt,nvalw:      162       21       90       36       15
 ncsft:            5563
 total number of valid internal walks:     162
 nvalz,nvaly,nvalx,nvalw =       21      90      36      15

 cisrt info file parameters:
 file number  12 blocksize   1536
 mxbld   1536
 nd4ext,nd2ext,nd0ext   812   336    42
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int    24411    18720     6792     1272       99        0        0        0
 minbl4,minbl3,maxbl2   381   381   384
 maxbuf 30006
 number of external orbitals per symmetry block:  11   3   7   7
 nmsym   4 number of internal orbitals   6
 executing brd_struct for drt
 executing brd_struct for orbinf
 executing brd_struct for momap
 calcthrxt: niot,maxw1=                     6                    66
 block size     0
 pthz,pthy,pthx,pthw:   105    90    36    15 total internal walks:     246
 maxlp3,n2lp,n1lp,n0lp    66     0     0     0
 orbsym(*)= 1 1 1 2 3 4

 setref:       21 references kept,
               84 references were marked as invalid, out of
              105 total.
 nmb.of records onel     1
 nmb.of records 2-ext     5
 nmb.of records 1-ext     1
 nmb.of records 0-ext     1
 nmb.of records 2-int     0
 nmb.of records 1-int     0
 nmb.of records 0-int     0
 ---------memory usage in DP -----------------
 < n-ex core usage >
     routines:
    fourex            61797
    threx             60409
    twoex              3549
    onex               1823
    allin              1536
    diagon             2109
               =======
   maximum            61797
 
  __ static summary __ 
   reflst                21
   hrfspc                21
               -------
   static->              42
 
  __ core required  __ 
   totstc                42
   max n-ex           61797
               -------
   totnec->           61839
 
  __ core available __ 
   totspc          13107199
   totnec -           61839
               -------
   totvec->        13045360

 number of external paths / symmetry
 vertex x     100      82      98      98
 vertex w     128      82      98      98
segment: free space=    13045360
 reducing frespc by                   906 to               13044454 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1          32|        21|         0|        21|         0|         1|
 -------------------------------------------------------------------------------
  Y 2          90|       622|        21|        90|        21|         2|
 -------------------------------------------------------------------------------
  X 3          36|      3424|       643|        36|       111|         3|
 -------------------------------------------------------------------------------
  W 4          15|      1496|      4067|        15|       147|         4|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=          36DP  conft+indsym=         360DP  drtbuffer=         510 DP

dimension of the ci-matrix ->>>      5563

 executing brd_struct for civct
 gentasklist: ntask=                    20
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS   
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      36      32       3424         21      36      21
     2  4   1    25      two-ext wz   2X  4 1      15      32       1496         21      15      21
     3  4   3    26      two-ext wx*  WX  4 3      15      36       1496       3424      15      36
     4  4   3    27      two-ext wx+  WX  4 3      15      36       1496       3424      15      36
     5  2   1    11      one-ext yz   1X  2 1      90      32        622         21      90      21
     6  3   2    15      1ex3ex yx    3X  3 2      36      90       3424        622      36      90
     7  4   2    16      1ex3ex yw    3X  4 2      15      90       1496        622      15      90
     8  1   1     1      allint zz    OX  1 1      32      32         21         21      21      21
     9  2   2     5      0ex2ex yy    OX  2 2      90      90        622        622      90      90
    10  3   3     6      0ex2ex xx*   OX  3 3      36      36       3424       3424      36      36
    11  3   3    18      0ex2ex xx+   OX  3 3      36      36       3424       3424      36      36
    12  4   4     7      0ex2ex ww*   OX  4 4      15      15       1496       1496      15      15
    13  4   4    19      0ex2ex ww+   OX  4 4      15      15       1496       1496      15      15
    14  2   2    42      four-ext y   4X  2 2      90      90        622        622      90      90
    15  3   3    43      four-ext x   4X  3 3      36      36       3424       3424      36      36
    16  4   4    44      four-ext w   4X  4 4      15      15       1496       1496      15      15
    17  1   1    75      dg-024ext z  OX  1 1      32      32         21         21      21      21
    18  2   2    76      dg-024ext y  OX  2 2      90      90        622        622      90      90
    19  3   3    77      dg-024ext x  OX  3 3      36      36       3424       3424      36      36
    20  4   4    78      dg-024ext w  OX  4 4      15      15       1496       1496      15      15
----------------------------------------------------------------------------------------------------
REDTASK #   1 TIME=  19.000 N=  1 (task/type/sgbra)=(   1/24/0) (
REDTASK #   2 TIME=  18.000 N=  1 (task/type/sgbra)=(   2/25/0) (
REDTASK #   3 TIME=  17.000 N=  1 (task/type/sgbra)=(   3/26/1) (
REDTASK #   4 TIME=  16.000 N=  1 (task/type/sgbra)=(   4/27/2) (
REDTASK #   5 TIME=  15.000 N=  1 (task/type/sgbra)=(   5/11/0) (
REDTASK #   6 TIME=  14.000 N=  1 (task/type/sgbra)=(   6/15/0) (
REDTASK #   7 TIME=  13.000 N=  1 (task/type/sgbra)=(   7/16/0) (
REDTASK #   8 TIME=  12.000 N=  1 (task/type/sgbra)=(   8/ 1/0) (
REDTASK #   9 TIME=  11.000 N=  1 (task/type/sgbra)=(   9/ 5/0) (
REDTASK #  10 TIME=  10.000 N=  1 (task/type/sgbra)=(  10/ 6/1) (
REDTASK #  11 TIME=   9.000 N=  1 (task/type/sgbra)=(  11/18/2) (
REDTASK #  12 TIME=   8.000 N=  1 (task/type/sgbra)=(  12/ 7/1) (
REDTASK #  13 TIME=   7.000 N=  1 (task/type/sgbra)=(  13/19/2) (
REDTASK #  14 TIME=   6.000 N=  1 (task/type/sgbra)=(  14/42/1) (
REDTASK #  15 TIME=   5.000 N=  1 (task/type/sgbra)=(  15/43/1) (
REDTASK #  16 TIME=   4.000 N=  1 (task/type/sgbra)=(  16/44/1) (
REDTASK #  17 TIME=   3.000 N=  1 (task/type/sgbra)=(  17/75/1) (
REDTASK #  18 TIME=   2.000 N=  1 (task/type/sgbra)=(  18/76/1) (
REDTASK #  19 TIME=   1.000 N=  1 (task/type/sgbra)=(  19/77/1) (
REDTASK #  20 TIME=   0.000 N=  1 (task/type/sgbra)=(  20/78/1) (
 initializing v-file: 1:                  5563

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         162 2x:           0 4x:           0
All internal counts: zz :         210 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 reference space has dimension      21
 dsyevx: computed roots 1 to    2(converged:   2)

    root           eigenvalues
    ----           ------------
       1         -46.3018785760
       2         -46.1888399386

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=                    21

         vector  1 from unit 11 written to unit 49 filename cirefv              
 ncorel,neli=                     4                     4
  This is a  mraqcc energy evaluation      

    ------------------------------------------------------------

 setopt2: mraqcc energy evaluation      gvalue=  0.833333333333333      ncorel=
                     4

    ------------------------------------------------------------


    reference energies:

  reference state used is:   1with energy      -46.3018785760

  ### begin active excitation selection ###

 inactive(1:nintern)=AAAAAA
 There are         21 reference CSFs out of         21 valid zwalks.
 There are    0 inactive and    6 active orbitals.
      21 active-active  and        0 inactive excitations.

  ### end active excitation selection ###


 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              5563
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                   30
 residual norm convergence criteria:               0.001000

          starting ci iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         753 2x:         336 4x:         141
All internal counts: zz :         210 yy:         930 xx:         352 ww:          66
One-external counts: yz :        1183 yx:        2405 yw:        1220
Two-external counts: yy :        1285 ww:         180 xx:         682 xz:         129 wz:          75 wx:         510
Three-ext.   counts: yx :         676 yw:         361

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1
   ht   1    -2.80329092
calca4: root=   1 anorm**2=  0.00000000 scale:  1.00000000

          calcsovref: tciref block   1

              civs   1
 refs   1    1.00000    

          calcsovref: scrb block   1

                ci   1
 civs   1    1.00000    

          calcsovref: sovref block   1

              v      1
 ref    1    1.00000    

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  1  1    -46.3018785760 -7.1054E-15  5.2706E-02  2.1139E-01  1.0000E-03
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   2

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         753 2x:         336 4x:         141
All internal counts: zz :         210 yy:         930 xx:         352 ww:          66
One-external counts: yz :        1183 yx:        2405 yw:        1220
Two-external counts: yy :        1285 ww:         180 xx:         682 xz:         129 wz:          75 wx:         510
Three-ext.   counts: yx :         676 yw:         361

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2
   ht   1    -2.80329092
   ht   2    -0.05270574    -0.18847445
calca4: root=   1 anorm**2=  0.05818880 scale:  1.02868304
calca4: root=   2 anorm**2=  0.94181120 scale:  1.39348886

          calcsovref: tciref block   1

              civs   1       civs   2
 refs   1    1.00000       2.709087E-13

          calcsovref: scrb block   1

                ci   1         ci   2
 civs   1   0.970470      -0.241224    
 civs   2   0.811671        3.26544    

          calcsovref: sovref block   1

              v      1       v      2
 ref    1   0.970470      -0.241224    

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97046958    -0.24122355
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  2  1    -46.3459600354  4.4081E-02  1.9613E-03  4.0884E-02  1.0000E-03
 mraqcc  #  2  2    -45.5884008650  2.0898E+00  0.0000E+00  4.6726E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   3

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         753 2x:         336 4x:         141
All internal counts: zz :         210 yy:         930 xx:         352 ww:          66
One-external counts: yz :        1183 yx:        2405 yw:        1220
Two-external counts: yy :        1285 ww:         180 xx:         682 xz:         129 wz:          75 wx:         510
Three-ext.   counts: yx :         676 yw:         361

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3
   ht   1    -2.80329092
   ht   2    -0.05270574    -0.18847445
   ht   3    -0.00030189    -0.00181417    -0.00689508
calca4: root=   1 anorm**2=  0.06666061 scale:  1.03279263
calca4: root=   2 anorm**2=  0.94869941 scale:  1.39595824
calca4: root=   3 anorm**2=  0.95763393 scale:  1.39915472

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3
 refs   1    1.00000       2.709087E-13   1.043971E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3
 civs   1   0.965928      -0.177775       0.188103    
 civs   2   0.847427        1.60902       -2.83138    
 civs   3    1.10090        15.6328        8.94055    

          calcsovref: sovref block   1

              v      1       v      2       v      3
 ref    1   0.966043      -0.176143       0.189036    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.96604288    -0.17614312     0.18903638
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  3  1    -46.3481233327  2.1633E-03  2.7174E-04  1.4903E-02  1.0000E-03
 mraqcc  #  3  2    -45.8196073213  2.3121E-01  0.0000E+00  4.0848E-01  1.0000E-04
 mraqcc  #  3  3    -45.5128914155  2.0143E+00  0.0000E+00  4.7592E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000999
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   4

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         753 2x:         336 4x:         141
All internal counts: zz :         210 yy:         930 xx:         352 ww:          66
One-external counts: yz :        1183 yx:        2405 yw:        1220
Two-external counts: yy :        1285 ww:         180 xx:         682 xz:         129 wz:          75 wx:         510
Three-ext.   counts: yx :         676 yw:         361

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4
   ht   1    -2.80329092
   ht   2    -0.05270574    -0.18847445
   ht   3    -0.00030189    -0.00181417    -0.00689508
   ht   4    -0.00200928    -0.00244192    -0.00056052    -0.00096460
calca4: root=   1 anorm**2=  0.07023942 scale:  1.03452377
calca4: root=   2 anorm**2=  0.96228083 scale:  1.40081435
calca4: root=   3 anorm**2=  0.95289670 scale:  1.39746080
calca4: root=   4 anorm**2=  0.79946433 scale:  1.34144114

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4
 refs   1    1.00000       2.709087E-13   1.043971E-04   7.304194E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4
 civs   1   0.963407       0.177630       0.203677      -4.552265E-03
 civs   2   0.851271      -0.957109       -3.12327      -0.364419    
 civs   3    1.23234       -12.6919        5.10569       -11.8277    
 civs   4   0.957061       -25.5174        10.4483        39.0340    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4
 ref    1   0.964234       0.157667       0.211842       2.272416E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.96423432     0.15766655     0.21184203     0.02272416
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  4  1    -46.3483834704  2.6014E-04  4.7430E-05  6.5325E-03  1.0000E-03
 mraqcc  #  4  2    -45.9873847786  1.6778E-01  0.0000E+00  2.1144E-01  1.0000E-04
 mraqcc  #  4  3    -45.5231510213  1.0260E-02  0.0000E+00  4.7689E-01  1.0000E-04
 mraqcc  #  4  4    -45.3860663041  1.8875E+00  0.0000E+00  5.3431E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   5

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         753 2x:         336 4x:         141
All internal counts: zz :         210 yy:         930 xx:         352 ww:          66
One-external counts: yz :        1183 yx:        2405 yw:        1220
Two-external counts: yy :        1285 ww:         180 xx:         682 xz:         129 wz:          75 wx:         510
Three-ext.   counts: yx :         676 yw:         361

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1    -2.80329092
   ht   2    -0.05270574    -0.18847445
   ht   3    -0.00030189    -0.00181417    -0.00689508
   ht   4    -0.00200928    -0.00244192    -0.00056052    -0.00096460
   ht   5     0.00379749    -0.00035490    -0.00026294    -0.00000073    -0.00016218
calca4: root=   1 anorm**2=  0.07123107 scale:  1.03500293
calca4: root=   2 anorm**2=  0.97214094 scale:  1.40432936
calca4: root=   3 anorm**2=  0.93584474 scale:  1.39134638
calca4: root=   4 anorm**2=  0.93213581 scale:  1.39001288
calca4: root=   5 anorm**2=  0.53025516 scale:  1.23703482

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1    1.00000       2.709087E-13   1.043971E-04   7.304194E-04  -1.352218E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 civs   1  -0.963890       0.121500       0.258493       8.726860E-02  -8.015139E-02
 civs   2  -0.851732      -0.742718       -3.16345       0.302295      -0.465713    
 civs   3   -1.25202       -10.8836        1.50732       -13.9566       -5.02146    
 civs   4   -1.10115       -26.6905        10.3504        16.9732        34.7401    
 civs   5  -0.826467       -36.1085        35.1216        84.8345       -61.1317    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1  -0.963708       0.149695       0.218719      -1.650568E-02   2.736268E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1    -0.96370767     0.14969478     0.21871866    -0.01650568     0.02736268
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  5  1    -46.3484226708  3.9200E-05  4.2517E-06  1.8690E-03  1.0000E-03
 mraqcc  #  5  2    -46.0481807901  6.0796E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  5  3    -45.5367192235  1.3568E-02  0.0000E+00  4.7468E-01  1.0000E-04
 mraqcc  #  5  4    -45.4188964859  3.2830E-02  0.0000E+00  5.7834E-01  1.0000E-04
 mraqcc  #  5  5    -45.3697737333  1.8712E+00  0.0000E+00  5.9703E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   6

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         753 2x:         336 4x:         141
All internal counts: zz :         210 yy:         930 xx:         352 ww:          66
One-external counts: yz :        1183 yx:        2405 yw:        1220
Two-external counts: yy :        1285 ww:         180 xx:         682 xz:         129 wz:          75 wx:         510
Three-ext.   counts: yx :         676 yw:         361

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1    -2.80329092
   ht   2    -0.05270574    -0.18847445
   ht   3    -0.00030189    -0.00181417    -0.00689508
   ht   4    -0.00200928    -0.00244192    -0.00056052    -0.00096460
   ht   5     0.00379749    -0.00035490    -0.00026294    -0.00000073    -0.00016218
   ht   6     0.00168019    -0.00010702     0.00002909     0.00001027     0.00000263    -0.00001776
calca4: root=   1 anorm**2=  0.07129858 scale:  1.03503554
calca4: root=   2 anorm**2=  0.97425692 scale:  1.40508253
calca4: root=   3 anorm**2=  0.89892978 scale:  1.37801661
calca4: root=   4 anorm**2=  0.95155506 scale:  1.39698069
calca4: root=   5 anorm**2=  0.89503846 scale:  1.37660396
calca4: root=   6 anorm**2=  0.47301132 scale:  1.21367678

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1    1.00000       2.709087E-13   1.043971E-04   7.304194E-04  -1.352218E-03  -5.985702E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1  -0.963376       0.151960       4.173210E-03   0.227439      -7.934188E-02  -0.248998    
 civs   2  -0.851756      -0.600814        1.15403       -2.96263       0.572583       0.342598    
 civs   3   -1.25410       -9.72537        6.14162        6.67234        12.6448        2.29234    
 civs   4   -1.11641       -25.3331        6.24210        9.82310       -37.7608        10.8893    
 civs   5  -0.914069       -43.1919       -37.6113       -11.8427       -25.2828       -97.0161    
 civs   6   0.977389        96.3260        298.135        94.1020       -60.5625       -151.352    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.963671       0.133188      -0.118222       0.194998      -3.516420E-02  -1.902301E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.96367099     0.13318800    -0.11822211     0.19499825    -0.03516420    -0.01902301

 trial vector basis is being transformed.  new dimension:   2
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  6  1    -46.3484268264  4.1556E-06  4.1851E-07  5.9787E-04  1.0000E-03
 mraqcc  #  6  2    -46.0713403964  2.3160E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  6  3    -45.7923605052  2.5564E-01  0.0000E+00  4.0628E-01  1.0000E-04
 mraqcc  #  6  4    -45.4988434534  7.9947E-02  0.0000E+00  4.7707E-01  1.0000E-04
 mraqcc  #  6  5    -45.3904821289  2.0708E-02  0.0000E+00  5.3119E-01  1.0000E-04
 mraqcc  #  6  6    -45.3209602239  1.8224E+00  0.0000E+00  6.4779E-01  1.0000E-04
 
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mraqcc   convergence criteria satisfied after  6 iterations.

 final mraqcc   convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  6  1    -46.3484268264  4.1556E-06  4.1851E-07  5.9787E-04  1.0000E-03
 mraqcc  #  6  2    -46.0713403964  2.3160E-02  0.0000E+00  0.0000E+00  1.0000E-04

####################CIUDGINFO####################

   aqcc(lrt) vector at position   1 energy=  -46.348426826401

################END OF CIUDGINFO################

 
 a4den factor for root 1  =  1.012026002
diagon:itrnv=   0
    1 of the   3 expansion vectors are transformed.
    1 of the   2 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
maximum overlap with reference    1(overlap= 0.96367)
reference energy =                         -46.3018785760
total aqcc energy =                        -46.3484268264
diagonal element shift (lrtshift) =         -0.0465482504

 information on vector: 1 from unit 11 written to unit 48 filename civout              


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -46.3484268264

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     3    4    5   17   22   31

                                         symmetry   a1   a1   a1   a2   b1   b2 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  3  1       1 -0.952850                        +-   +    +                 
 z*  3  1       4  0.052472                        +    +         +-           
 z*  3  1       5 -0.067841                        +    +              +-      
 z*  3  1       6  0.015379                        +    +                   +- 
 z*  3  1       7  0.048071                        +         +    +-           
 z*  3  1       8  0.021386                        +         +         +-      
 z*  3  1       9 -0.069438                        +         +              +- 
 z*  3  1      13  0.042326                             +    +    +-           
 z*  3  1      14  0.042336                             +    +         +-      
 z*  3  1      15  0.042336                             +    +              +- 
 y   3  1      65  0.022015              5( a1 )    -   +    +                 
 y   3  1      78  0.012812              4( b1 )    -   +              +       
 y   3  1     102  0.013117              4( b2 )    -        +              +  
 y   3  1     138  0.030316              5( a1 )   +     -   +                 
 y   3  1     166 -0.021439              5( a1 )   +    +     -                
 y   3  1     343 -0.010264              4( b2 )         -   +              +  
 y   3  1     392  0.011346              1( a2 )        +    +     -           
 y   3  1     393 -0.014164              2( a2 )        +    +     -           
 y   3  1     396  0.011349              2( b1 )        +    +          -      
 y   3  1     398 -0.014163              4( b1 )        +    +          -      
 y   3  1     403  0.011350              2( b2 )        +    +               - 
 y   3  1     405 -0.014163              4( b2 )        +    +               - 
 x   3  1     646 -0.010873    2( a1 )   3( a1 )   +-                          
 x   3  1     656  0.014850    3( a1 )   6( a1 )   +-                          
 x   3  1     660  0.014850    2( a1 )   7( a1 )   +-                          
 x   3  1     664  0.026149    6( a1 )   7( a1 )   +-                          
 x   3  1     688  0.014822    9( a1 )  10( a1 )   +-                          
 x   3  1     722 -0.014822    6( b1 )   7( b1 )   +-                          
 x   3  1     743 -0.014822    6( b2 )   7( b2 )   +-                          
 x   3  1     751  0.016378    2( a1 )   5( a1 )    -   +                      
 x   3  1     752  0.012461    3( a1 )   5( a1 )    -   +                      
 x   3  1     758 -0.017164    5( a1 )   6( a1 )    -   +                      
 x   3  1     763  0.016575    5( a1 )   7( a1 )    -   +                      
 x   3  1     772 -0.011943    1( a1 )   9( a1 )    -   +                      
 x   3  1     775 -0.012424    4( a1 )   9( a1 )    -   +                      
 x   3  1     780  0.013859    1( a1 )  10( a1 )    -   +                      
 x   3  1     783  0.014432    4( a1 )  10( a1 )    -   +                      
 x   3  1     817 -0.012810    1( b1 )   7( b1 )    -   +                      
 x   3  1     819 -0.013341    3( b1 )   7( b1 )    -   +                      
 x   3  1     833 -0.014359    1( b2 )   6( b2 )    -   +                      
 x   3  1     835 -0.014943    3( b2 )   6( b2 )    -   +                      
 x   3  1     838 -0.014428    1( b2 )   7( b2 )    -   +                      
 x   3  1     840 -0.015009    3( b2 )   7( b2 )    -   +                      
 x   3  1     851  0.012460    2( a1 )   5( a1 )    -        +                 
 x   3  1     852 -0.016379    3( a1 )   5( a1 )    -        +                 
 x   3  1     858 -0.016575    5( a1 )   6( a1 )    -        +                 
 x   3  1     863 -0.017164    5( a1 )   7( a1 )    -        +                 
 x   3  1     872 -0.016555    1( a1 )   9( a1 )    -        +                 
 x   3  1     875 -0.017230    4( a1 )   9( a1 )    -        +                 
 x   3  1     912 -0.015723    1( b1 )   6( b1 )    -        +                 
 x   3  1     914 -0.016361    3( b1 )   6( b1 )    -        +                 
 x   3  1     917  0.013289    1( b1 )   7( b1 )    -        +                 
 x   3  1     919  0.013824    3( b1 )   7( b1 )    -        +                 
 x   3  1     938  0.012793    1( b2 )   7( b2 )    -        +                 
 x   3  1     940  0.013322    3( b2 )   7( b2 )    -        +                 
 x   3  1    2479  0.010642    1( a1 )   1( b2 )        +                    - 
 x   3  1    3016  0.010395    1( a1 )   1( b1 )             +          -      
 w   3  1    4068 -0.033308    1( a1 )   1( a1 )   +    +                      
 w   3  1    4074 -0.035963    1( a1 )   4( a1 )   +    +                      
 w   3  1    4077 -0.020740    4( a1 )   4( a1 )   +    +                      
 w   3  1    4079  0.010722    2( a1 )   5( a1 )   +    +                      
 w   3  1    4104  0.010527    1( a1 )   9( a1 )   +    +                      
 w   3  1    4107  0.011157    4( a1 )   9( a1 )   +    +                      
 w   3  1    4143 -0.010551    1( b1 )   3( b1 )   +    +                      
 w   3  1    4168  0.043104    1( b2 )   1( b2 )   +    +                      
 w   3  1    4171  0.046542    1( b2 )   3( b2 )   +    +                      
 w   3  1    4173  0.026835    3( b2 )   3( b2 )   +    +                      
 w   3  1    4178 -0.011406    1( b2 )   5( b2 )   +    +                      
 w   3  1    4180 -0.010833    3( b2 )   5( b2 )   +    +                      
 w   3  1    4183  0.011026    1( b2 )   6( b2 )   +    +                      
 w   3  1    4185  0.011759    3( b2 )   6( b2 )   +    +                      
 w   3  1    4189  0.012486    1( b2 )   7( b2 )   +    +                      
 w   3  1    4191  0.013243    3( b2 )   7( b2 )   +    +                      
 w   3  1    4196 -0.030515    1( a1 )   1( a1 )   +         +                 
 w   3  1    4202 -0.032946    1( a1 )   4( a1 )   +         +                 
 w   3  1    4205 -0.019001    4( a1 )   4( a1 )   +         +                 
 w   3  1    4208 -0.010724    3( a1 )   5( a1 )   +         +                 
 w   3  1    4232  0.013003    1( a1 )   9( a1 )   +         +                 
 w   3  1    4235  0.013852    4( a1 )   9( a1 )   +         +                 
 w   3  1    4268  0.044130    1( b1 )   1( b1 )   +         +                 
 w   3  1    4271  0.047651    1( b1 )   3( b1 )   +         +                 
 w   3  1    4273  0.027473    3( b1 )   3( b1 )   +         +                 
 w   3  1    4278  0.011677    1( b1 )   5( b1 )   +         +                 
 w   3  1    4280  0.011092    3( b1 )   5( b1 )   +         +                 
 w   3  1    4283  0.012463    1( b1 )   6( b1 )   +         +                 
 w   3  1    4285  0.013271    3( b1 )   6( b1 )   +         +                 
 w   3  1    4289 -0.011480    1( b1 )   7( b1 )   +         +                 
 w   3  1    4291 -0.012177    3( b1 )   7( b1 )   +         +                 
 w   3  1    4296 -0.013585    1( b2 )   1( b2 )   +         +                 
 w   3  1    4299 -0.014667    1( b2 )   3( b2 )   +         +                 
 w   3  1    4602  0.075893    1( a1 )   1( a1 )        +    +                 
 w   3  1    4608  0.064548    1( a1 )   4( a1 )        +    +                 
 w   3  1    4611  0.032496    4( a1 )   4( a1 )        +    +                 
 w   3  1    4616  0.024094    5( a1 )   5( a1 )        +    +                 
 w   3  1    4674  0.075901    1( b1 )   1( b1 )        +    +                 
 w   3  1    4677  0.064556    1( b1 )   3( b1 )        +    +                 
 w   3  1    4679  0.032502    3( b1 )   3( b1 )        +    +                 
 w   3  1    4702  0.075899    1( b2 )   1( b2 )        +    +                 
 w   3  1    4705  0.064554    1( b2 )   3( b2 )        +    +                 
 w   3  1    4707  0.032500    3( b2 )   3( b2 )        +    +                 

 ci coefficient statistics:
           rq > 0.1                1
      0.1> rq > 0.01              99
     0.01> rq > 0.001            600
    0.001> rq > 0.0001          1203
   0.0001> rq > 0.00001          548
  0.00001> rq > 0.000001         116
 0.000001> rq                   2996
           all                  5563
  iref  icsf         v(icsf)             hv(icsf)
     1     1     -0.952850348068
     2     2      0.000010664753
     3     3     -0.000002751322
     4     4      0.052471588290
     5     5     -0.067840834455
     6     6      0.015379448581
     7     7      0.048070978822
     8     8      0.021386006639
     9     9     -0.069438478906
    10    10      0.002694841570
    11    11     -0.001593186470
    12    12      0.001135545145
    13    13      0.042326138795
    14    14      0.042335762906
    15    15      0.042335597161
    16    16      0.003985051671
    17    17      0.000251900419
    18    18     -0.009391578267
    19    19     -0.004349183409
    20    20     -0.008998284720
    21    21     -0.002082896541

 number of reference csfs (nref) is    21.  root number (iroot) is  1.
 c0**2 =   0.92870142  c0**2(renormalized) =   0.99491651
 c**2 (all zwalks) =   0.92870142  c**2(all zwalks, renormalized) =   0.99491651
 passed aftci ... 
 readint2: molcas,dalton2=                     0                     0
 files%faoints=aoints              
lodens (list->root)=  1
sifcfg setup: record length 4096 DP
# d1 elements per record  3272
# d2 elements per record  2730
  The AQCC density will be calculated.
 item #                     1 suffix=:.drt1.state1:
 read_civout: repnuc=  -43.4985876562748     
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method: 30 last record  1max overlap with ref# 96% root-following 0
 MR-CISD energy:   -46.34842683    -2.84983917
 residuum:     0.00059787
 deltae:     0.00000416
 apxde:     0.00000042
 a4den:     1.01202600
 reference energy:   -46.30187858    -2.80329092

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.96367099     0.13318800    -0.11822211     0.19499825    -0.03516420    -0.01902301     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.96367099     0.13318800     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 rdcirefv: extracting reference vector # 1(blocksize=    21 #zcsf=      21)
 =========== Executing IN-CORE method ==========
================================================================================
   DYZ=       0  DYX=       0  DYW=       0
   D0Z=      21  D0Y=       0  D0X=       0  D0W=       0
  DDZI=      72 DDYI=       0 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
 root #                      1 : Scaling(1) with    1.01202600235402      
 corresponding to ref #                      1
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=     114  DYX=     275  DYW=     150
   D0Z=      21  D0Y=     125  D0X=      38  D0W=      12
  DDZI=      72 DDYI=     240 DDXI=      66 DDWI=      30
  DDZE=       0 DDYE=      90 DDXE=      36 DDWE=      15
================================================================================
Trace of MO density:     4.000000
    4  correlated and     8  frozen core electrons

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     1.86509391    -0.00000762     0.00000105     0.00000000     0.00001264    -0.00000430
   MO   4     0.00000000     0.00000000    -0.00000762     0.97403932     0.00000190     0.00000000    -0.00452262     0.00595099
   MO   5     0.00000000     0.00000000     0.00000105     0.00000190     0.97403816     0.00000000     0.00594429     0.00452785
   MO   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.02461457     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000     0.00001264    -0.00452262     0.00594429     0.00000000     0.00130465    -0.00000002
   MO   8     0.00000000     0.00000000    -0.00000430     0.00595099     0.00452785     0.00000000    -0.00000002     0.00130527
   MO   9     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.01711044     0.00000000     0.00000000
   MO  10     0.00000000     0.00000000    -0.03038471    -0.00000239     0.00000282     0.00000000     0.00000078    -0.00000028
   MO  11     0.00000000     0.00000000     0.00000811    -0.00457406     0.00473631     0.00000000     0.00145529    -0.00017189
   MO  12     0.00000000     0.00000000     0.00000096    -0.00473837    -0.00457463     0.00000000    -0.00017184    -0.00145546
   MO  13     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00352687     0.00000000     0.00000000
   MO  14     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00251760     0.00000000     0.00000000
   MO  15     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00073808     0.00000000     0.00000000
   MO  16     0.00000000     0.00000000    -0.00023236     0.00000018    -0.00000017     0.00000000    -0.00000005     0.00000002

                MO   9         MO  10         MO  11         MO  12         MO  13         MO  14         MO  15         MO  16
   MO   3     0.00000000    -0.03038471     0.00000811     0.00000096     0.00000000     0.00000000     0.00000000    -0.00023236
   MO   4     0.00000000    -0.00000239    -0.00457406    -0.00473837     0.00000000     0.00000000     0.00000000     0.00000018
   MO   5     0.00000000     0.00000282     0.00473631    -0.00457463     0.00000000     0.00000000     0.00000000    -0.00000017
   MO   6     0.01711044     0.00000000     0.00000000     0.00000000     0.00352687    -0.00251760     0.00073808     0.00000000
   MO   7     0.00000000     0.00000078     0.00145529    -0.00017184     0.00000000     0.00000000     0.00000000    -0.00000005
   MO   8     0.00000000    -0.00000028    -0.00017189    -0.00145546     0.00000000     0.00000000     0.00000000     0.00000002
   MO   9     0.01227480     0.00000000     0.00000000     0.00000000     0.00280604    -0.00218597     0.00064087     0.00000000
   MO  10     0.00000000     0.00571037     0.00000075     0.00000014     0.00000000     0.00000000     0.00000000    -0.00026457
   MO  11     0.00000000     0.00000075     0.00189922     0.00000001     0.00000000     0.00000000     0.00000000    -0.00000005
   MO  12     0.00000000     0.00000014     0.00000001     0.00189928     0.00000000     0.00000000     0.00000000    -0.00000001
   MO  13     0.00280604     0.00000000     0.00000000     0.00000000     0.00085947    -0.00082169     0.00024089     0.00000000
   MO  14    -0.00218597     0.00000000     0.00000000     0.00000000    -0.00082169     0.00208106    -0.00027755     0.00000000
   MO  15     0.00064087     0.00000000     0.00000000     0.00000000     0.00024089    -0.00027755     0.00121552     0.00000000
   MO  16     0.00000000    -0.00026457    -0.00000005    -0.00000001     0.00000000     0.00000000     0.00000000     0.00001755

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     2.00000000     1.86559033     0.97414297     0.97413884     0.03754406     0.00522780     0.00299818
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     0.00299786     0.00201481     0.00113417     0.00034179     0.00010413     0.00010391     0.00001060     0.00000370

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4
   MO   1     0.01607122     0.00200313    -0.00266358     0.00000000
   MO   2     0.00200313     0.00043386    -0.00051470     0.00000000
   MO   3    -0.00266358    -0.00051470     0.00064885     0.00000000
   MO   4     0.00000000     0.00000000     0.00000000     0.00006701

Natural orbital populations,block 2
              MO     1       MO     2       MO     3       MO     4       MO
  occ(*)=     0.01677812     0.00036309     0.00006701     0.00001271

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     0.01606803     0.00000000     0.00200597     0.00000000    -0.00266534     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.02462538     0.00000000     0.01711880     0.00000000     0.00352912     0.00172009
   MO   4     0.00000000     0.00200597     0.00000000     0.00043478     0.00000000    -0.00051564     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.01711880     0.00000000     0.01228117     0.00000000     0.00280773     0.00149313
   MO   6     0.00000000    -0.00266534     0.00000000    -0.00051564     0.00000000     0.00064971     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000     0.00352912     0.00000000     0.00280773     0.00000000     0.00085996     0.00056115
   MO   8     0.00000000     0.00000000     0.00172009     0.00000000     0.00149313     0.00000000     0.00056115     0.00157500
   MO   9     0.00000000     0.00000000    -0.00198503     0.00000000    -0.00172330     0.00000000    -0.00064767    -0.00050921

                MO   9
   MO   2     0.00000000
   MO   3    -0.00198503
   MO   4     0.00000000
   MO   5    -0.00172330
   MO   6     0.00000000
   MO   7    -0.00064767
   MO   8    -0.00050921
   MO   9     0.00172180

Natural orbital populations,block 3
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     0.03756229     0.01677642     0.00201476     0.00113392     0.00036340     0.00034173     0.00001269
              MO     9       MO
  occ(*)=     0.00001060

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     0.01606571     0.00000000     0.00200669     0.00000000    -0.00266556     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.02462302     0.00000000     0.01711699     0.00000000    -0.00352864     0.00140390
   MO   4     0.00000000     0.00200669     0.00000000     0.00043506     0.00000000    -0.00051589     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.01711699     0.00000000     0.01227977     0.00000000    -0.00280736     0.00121865
   MO   6     0.00000000    -0.00266556     0.00000000    -0.00051589     0.00000000     0.00064988     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000    -0.00352864     0.00000000    -0.00280736     0.00000000     0.00085985    -0.00045798
   MO   8     0.00000000     0.00000000     0.00140390     0.00000000     0.00121865     0.00000000    -0.00045798     0.00142779
   MO   9     0.00000000     0.00000000     0.00221927     0.00000000     0.00192670     0.00000000    -0.00072413     0.00046470

                MO   9
   MO   2     0.00000000
   MO   3     0.00221927
   MO   4     0.00000000
   MO   5     0.00192670
   MO   6     0.00000000
   MO   7    -0.00072413
   MO   8     0.00046470
   MO   9     0.00186900

Natural orbital populations,block 4
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     0.03755835     0.01677446     0.00201476     0.00113399     0.00036349     0.00034173     0.00001269
              MO     9       MO
  occ(*)=     0.00001060


 total number of electrons =   12.0000000000

 test slabel:                     4
  a1  a2  b1  b2 


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        a1  partial gross atomic populations
   ao class       1a1        2a1        3a1        4a1        5a1        6a1 
     3_ s       2.000000   0.000000   1.865590   0.000000   0.000000   0.000000
     3_ p       0.000000   2.000000   0.000000   0.000000   0.000000   0.037173
     3_ d       0.000000   0.000000   0.000000   0.974143   0.974139   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000000   0.000000   0.000371
 
   ao class       7a1        8a1        9a1       10a1       11a1       12a1 
     3_ s       0.005228   0.000000   0.000000   0.000000   0.000000   0.000000
     3_ p       0.000000   0.000000   0.000000   0.000265   0.000000   0.000300
     3_ d       0.000000   0.002998   0.002998   0.000000   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000000   0.001750   0.001134   0.000041
 
   ao class      13a1       14a1       15a1       16a1 
     3_ s       0.000000   0.000000   0.000000   0.000004
     3_ p       0.000000   0.000000   0.000011   0.000000
     3_ d       0.000104   0.000104   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000000

                        a2  partial gross atomic populations
   ao class       1a2        2a2        3a2        4a2 
     3_ d       0.016778   0.000363   0.000000   0.000013
     3_ f       0.000000   0.000000   0.000067   0.000000

                        b1  partial gross atomic populations
   ao class       1b1        2b1        3b1        4b1        5b1        6b1 
     3_ p       2.000000   0.037190   0.000000   0.000265   0.000000   0.000000
     3_ d       0.000000   0.000000   0.016776   0.000000   0.000000   0.000363
     3_ f       0.000000   0.000372   0.000000   0.001749   0.001134   0.000000
 
   ao class       7b1        8b1        9b1 
     3_ p       0.000300   0.000000   0.000011
     3_ d       0.000000   0.000013   0.000000
     3_ f       0.000041   0.000000   0.000000

                        b2  partial gross atomic populations
   ao class       1b2        2b2        3b2        4b2        5b2        6b2 
     3_ p       2.000000   0.037186   0.000000   0.000265   0.000000   0.000000
     3_ d       0.000000   0.000000   0.016774   0.000000   0.000000   0.000363
     3_ f       0.000000   0.000372   0.000000   0.001749   0.001134   0.000000
 
   ao class       7b2        8b2        9b2 
     3_ p       0.000300   0.000000   0.000011
     3_ d       0.000000   0.000013   0.000000
     3_ f       0.000041   0.000000   0.000000


                        gross atomic populations
     ao            3_
      s         3.870822
      p         6.113278
      d         2.005943
      f         0.009957
    total      12.000000
 

 Total number of electrons:   12.00000000

 DA ...
