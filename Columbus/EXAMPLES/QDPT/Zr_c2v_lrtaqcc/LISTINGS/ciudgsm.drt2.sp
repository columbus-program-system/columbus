1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.

  This is a  mraqcc energy evaluation      

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  1  1    -46.2740792789 -3.9968E-15  5.7760E-02  2.2790E-01  1.0000E-03
 mraqcc  #  2  1    -46.3234735939  4.9394E-02  2.5628E-03  4.5966E-02  1.0000E-03
 mraqcc  #  3  1    -46.3261421365  2.6685E-03  4.1059E-04  1.7821E-02  1.0000E-03
 mraqcc  #  4  1    -46.3264961355  3.5400E-04  8.0065E-05  8.2915E-03  1.0000E-03
 mraqcc  #  5  1    -46.3265606889  6.4553E-05  7.7667E-06  2.5511E-03  1.0000E-03
 mraqcc  #  6  1    -46.3265680318  7.3429E-06  8.0366E-07  8.3703E-04  1.0000E-03

 mraqcc   convergence criteria satisfied after  6 iterations.

 final mraqcc   convergence information:
 mraqcc  #  6  1    -46.3265680318  7.3429E-06  8.0366E-07  8.3703E-04  1.0000E-03

 number of reference csfs (nref) is    24.  root number (iroot) is  1.
 c0**2 =   0.92001730  c0**2(renormalized) =   0.99360277
 c**2 (all zwalks) =   0.92001730  c**2(all zwalks, renormalized) =   0.99360277
