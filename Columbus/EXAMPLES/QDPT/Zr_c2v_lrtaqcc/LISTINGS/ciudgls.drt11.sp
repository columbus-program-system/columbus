1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      2009-03.    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


================ Computing sorting integral file structure ================

                    -----z----- -----y----- -----x----- -----w----- ---total---

                CSFs         4         140        1414           0        1558
      internal walks        15          20          15           0          50
valid internal walks         4          20          15           0          39
 getinfoarray: info=                     6 :                     1
                  8192                  6552                  8192
                  5460                     0
 icd(3)=                  2976 ci%nnlev=                   595  l2rec=
                  8192  n2max=                  5460
 lcore1,lcore2=              13068137              13044821
 lencor,maxblo              13107200                 60000
========================================
 current settings:
 minbl3         381
 minbl4         381
 locmaxbl3     4288
 locmaxbuf     2144
 maxbl3       60000
 maxbl3       60000
 maxbl4       60000
 maxbuf       30006
========================================

 sorted 4-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)

 sorted 3-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)
 Orig.  diagonal integrals:  1electron:        34
                             0ext.    :        42
                             2ext.    :       336
                             4ext.    :       812


 Orig. off-diag. integrals:  4ext.    :     24411
                             3ext.    :     18720
                             2ext.    :      6792
                             1ext.    :      1272
                             0ext.    :        99
                             2ext. SO :         0
                             1ext. SO :         0
                             0ext. SO :         0
                             1electron:       128


 Sorted integrals            3ext.  w :     17370 x :     16020
                             4ext.  w :     20955 x :     17777


Cycle #  1 sortfile size=    163835(       5 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13068137
Cycle #  2 sortfile size=    163835(       5 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13068137
 minimum size of srtscr:     98301 WP (     3 records)
 maximum size of srtscr:    163835 WP (     5 records)
diagi   file:      4 records  of   1536 WP each=>       6144 WP total
ofdgi   file:      8 records  of   1536 WP each=>      12288 WP total
fil3w   file:      1 records  of  30006 WP each=>      30006 WP total
fil3x   file:      1 records  of  30006 WP each=>      30006 WP total
fil4w   file:      1 records  of  30006 WP each=>      30006 WP total
fil4x   file:      1 records  of  30006 WP each=>      30006 WP total
 compressed index vector length=                    14
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 3,
  GSET = 3,
   DAVCOR =10,
  NCOREL = 4
   nroot=1
  IVMODE = 3
  NBKITR = 1
   nvbkmn=2
  RTOLBK = 1e-3
   niter=30
   nvcimn=2
   rtolci=1e-3
  NVCIMX = 6
  NVRFMX = 6
  NVBKMX = 6
  IDEN  = 1
  CSFPRN = 10,
  update_mode=1
  nvrfmn=2
   lrtshift=-0.0465482970
 /&end
 ------------------------------------------------------------------------
lodens (list->root)=  1
invlodens (root->list)=  1
 LRT calculation: diagonal shift= -4.654829700000000E-002
 bummer (warning):2:changed keyword: nbkitr=                      0
 USING SEGMENTS OF EQUAL SIZE

****************  list of control variables  ****************
 lvlprt =    0      nroot  =    1      noldv  =   0      noldhv =   0
 nunitv =    1      nbkitr =    0      niter  =  30      davcor =  10
 csfprn =   10      ivmode =    3      istrt  =   0      vout   =   0
 iortls =    0      nvbkmx =    6      ibktv  =  -1      ibkthv =  -1
 nvcimx =    6      icitv  =   -1      icithv =  -1      frcsub =   0
 nvbkmn =    2      nvcimn =    2      maxseg =   4      nrfitr =  30
 ncorel =    4      nvrfmx =    6      nvrfmn =   2      iden   =   1
 itran  =    0      froot  =    0      rtmode =   0      ncouple=   1
 skipso =    F      dalton2=    0      molcas =   0      finalv =   0
 finalw =    0      cosmocalc=   0    with_tsklst=   0
 nsegwx =    1     1     1     1
 nseg0x =    1     1     1     1
 nseg1x =    1     1     1     1
 nseg2x =    1     1     1     1
 nseg3x =    1     1     1     1
 nseg4x =    1     1     1     1
 no0ex  =      0    no1ex  =      0    no2ex  =     0    no3ex  =     0
 no4ex  =      0    nodiag =      0
 cdg4ex =    1      c3ex1ex=    1      c2ex0ex=   1
 fileloc=    0     0     0     0     0     0     0     1     1     1
 directhd=   1      noaqccshift_zyxw=      0
 critical_crit=-1.00000    critical_delta= 0.05000

 ctol   = 0.010000    lrtshift=-.046548    smalld =0.001000


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-03    1.000E-03
 Computing density:                    .drt1.state1
 using                      1  nodes and                      1  cores.
 szdg/szodg per processor=                  6144                 12288
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core           13107199 DP per process

********** Integral sort section *************


 workspace allocation information: lencor=  13107199

 echo of the input for program cisrt:
 ------------------------------------------------------------------------
  &input
  maxbl3=60000
  maxbl4=60000
  &end
 ------------------------------------------------------------------------
 
 ( 6) listing file:                    ciudgls             
 ( 5) input file:                      cisrtin   
 (17) cidrt file:                      cidrtfl             
 (11) transformed integrals file:      moints    
 (12) diagonal integral file:          diagint             
 (13) off-diagonal integral file:      ofdgint             
 (31) 4-external w integrals file:     fil4w               
 (32) 4-external x integrals file:     fil4x               
 (33) 3-external w integrals file:     fil3w               
 (34) 3-external x integrals file:     fil3x               
 (21) scratch da sorting file:         srtscr              
 (12) 2-e integral file [fsplit=2]:    moints2   

 input integral file header information:
                                                                                 
 aoints SIFS file created by argos.      zam792            17:56:38.564 17-Dec-13
  cidrt_title                                                                    
 MO-coefficients from mcscf.x                                                    
  with dummy occupation 1.0 for active orbitals                                  
  total ao core energy =    0.000000000                                          
 MCSCF energy =     -46.301878576                                                
 SIFS file created by program tran.      zam792            17:56:38.888 17-Dec-13

 input energy(*) values:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total core energy =   0.000000000000E+00

 nsym = 4 nmot=  34

 symmetry  =    1    2    3    4
 slabel(*) =   a1   a2   b1   b2
 nmpsy(*)  =   14    4    8    8

 info(*) =          1      8192      6552      8192      5460         0

 orbital labels, i:molab(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012  13:tout:013  14:tout:014  15:tout:015  16:tout:016  17:tout:017  18:tout:018  19:tout:019  20:tout:020
  21:tout:021  22:tout:022  23:tout:023  24:tout:024  25:tout:025  26:tout:026  27:tout:027  28:tout:028  29:tout:029  30:tout:030
  31:tout:031  32:tout:032  33:tout:033  34:tout:034

 input parameters:
 prnopt=  0
 ldamin=    4095 ldamax=   32767 ldainc=      64
 maxbuf=   30006 maxbl3=   60000 maxbl4=   60000 intmxo=    1536
  Using 32 bit compression 

 drt information:
  cidrt_title                                                                    
 nmotd =  38 nfctd =   4 nfvtc =   0 nmot  =  34
 nlevel =  34 niot  =   6 lowinl=  29
 orbital-to-level map(*)
   -1  -1  29  30  31   1   2   3   4   5   6   7   8   9  10  11  32  12  13  14
   -1  33  15  16  17  18  19  20  21  -1  34  22  23  24  25  26  27  28
 compressed map(*)
   29  30  31   1   2   3   4   5   6   7   8   9  10  11  32  12  13  14  33  15
   16  17  18  19  20  21  34  22  23  24  25  26  27  28
 levsym(*)
    1   1   1   1   1   1   1   1   1   1   1   2   2   2   3   3   3   3   3   3
    3   4   4   4   4   4   4   4   1   1   1   2   3   4
 repartitioning mu(*)=
   0.  0.  0.  0.  0.  0.

 new core energy added to the energy(*) list.
 from the integral file: h1_core= -4.349858765627E+01

 indxdg: diagonal integral statistics.
 total number of integrals contributing to diagonal matrix elements:      1190
 number with all external indices:       812
 number with half external - half internal indices:       336
 number with all internal indices:        42

 indxof: off-diagonal integral statistics.
    4-external integrals: num=      24411 strt=          1
    3-external integrals: num=      18720 strt=      24412
    2-external integrals: num=       6792 strt=      43132
    1-external integrals: num=       1272 strt=      49924
    0-external integrals: num=         99 strt=      51196

 total number of off-diagonal integrals:       51294


 indxof(2nd)  ittp=   3 numx(ittp)=        6792
 indxof(2nd)  ittp=   4 numx(ittp)=        1272
 indxof(2nd)  ittp=   5 numx(ittp)=          99

 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg=  12931980
 pro2e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652

 pro2e:     25126 integrals read in     5 records.

 pro2e:         0 integrals 34-ext integrals skipped.
 pro1e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652
 pro1e: eref =    0.000000000000000E+00
 total size of srtscr:                     4  records of                  32767 
 WP =               1048544 Bytes
 putdg        1     596    1191    1786    3322   36089   57934    3018   46706   90394
   123161  131353  136813  158652

 putf:       4 buffers of length    1536 written to file 12
 diagonal integral file completed.

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep4e:    17 blocks of linear combinations of 4-external integrals processed.
 number of sorted 4-external integrals      38732
 number of original 4-external integrals    24411


 putf34: external integral file complete. nfilw=    31 nfilx=    32 nrecw=     1 nrecx=     1 lbufp= 30006

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep3e:    19 blocks of linear combinations of 3-external integrals processed.
 number of sorted 3-external integrals      33390
 number of original 3-external integrals    18720


 putf34: external integral file complete. nfilw=    33 nfilx=    34 nrecw=     1 nrecx=     1 lbufp= 30006
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      6792         3     43132     43132      6792     51294
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      1272         4     49924     49924      1272     51294
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd        99         5     51196     51196        99     51294

 putf:       8 buffers of length    1536 written to file 13
 off-diagonal files sort completed.
 executing brd_struct for cisrtinfo
cisrtinfo:
bufszi  1536
 diagfile 4ext:     812 2ext:     336 0ext:      42
 fil4w,fil4x  :   24411 fil3w,fil3x :   18720
 ofdgint  2ext:    6792 1ext:    1272 0ext:      99so0ext:       0so1ext:       0so2ext:       0
buffer minbl4     381 minbl3     381 maxbl2     384nbas:  11   3   7   7   0   0   0   0 maxbuf 30006
 CIUDG version 5.9.7 ( 5-Oct-2004)

 workspace allocation information: lcore=  13107199

 core energy values from the integral file:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -4.349858765627E+01, ietype=    6,   fcore energy of type: H1(*)   

 total core repulsion energy = -4.349858765627E+01
 nmot  =    38 niot  =     6 nfct  =     4 nfvt  =     0
 nrow  =    23 nsym  =     4 ssym  =     3 lenbuf=  1600
 nwalk,xbar:         50       15       20       15        0
 nvalwt,nvalw:       39        4       20       15        0
 ncsft:            1558
 total number of valid internal walks:      39
 nvalz,nvaly,nvalx,nvalw =        4      20      15       0

 cisrt info file parameters:
 file number  12 blocksize   1536
 mxbld   1536
 nd4ext,nd2ext,nd0ext   812   336    42
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int    24411    18720     6792     1272       99        0        0        0
 minbl4,minbl3,maxbl2   381   381   384
 maxbuf 30006
 number of external orbitals per symmetry block:  11   3   7   7
 nmsym   4 number of internal orbitals   6
 executing brd_struct for drt
 executing brd_struct for orbinf
 executing brd_struct for momap
 calcthrxt: niot,maxw1=                     6                    11
 block size     0
 pthz,pthy,pthx,pthw:    15    20    15     0 total internal walks:      50
 maxlp3,n2lp,n1lp,n0lp    11     0     0     0
 orbsym(*)= 1 1 1 2 3 4

 setref:        4 references kept,
               11 references were marked as invalid, out of
               15 total.
 nmb.of records onel     1
 nmb.of records 2-ext     5
 nmb.of records 1-ext     1
 nmb.of records 0-ext     1
 nmb.of records 2-int     0
 nmb.of records 1-int     0
 nmb.of records 0-int     0
 ---------memory usage in DP -----------------
 < n-ex core usage >
     routines:
    fourex            61797
    threx             60079
    twoex              3549
    onex               1823
    allin              1536
    diagon             2109
               =======
   maximum            61797
 
  __ static summary __ 
   reflst                 4
   hrfspc                 4
               -------
   static->               8
 
  __ core required  __ 
   totstc                 8
   max n-ex           61797
               -------
   totnec->           61805
 
  __ core available __ 
   totspc          13107199
   totnec -           61805
               -------
   totvec->        13045394

 number of external paths / symmetry
 vertex x     100      82      98      98
 vertex w     128      82      98      98
segment: free space=    13045394
 reducing frespc by                   349 to               13045045 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1           8|         4|         0|         4|         0|         1|
 -------------------------------------------------------------------------------
  Y 2          20|       140|         4|        20|         4|         2|
 -------------------------------------------------------------------------------
  X 3          15|      1414|       144|        15|        24|         3|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=          16DP  conft+indsym=          80DP  drtbuffer=         253 DP

dimension of the ci-matrix ->>>      1558

 executing brd_struct for civct
 gentasklist: ntask=                    12
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS   
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      15       8       1414          4      15       4
     2  2   1    11      one-ext yz   1X  2 1      20       8        140          4      20       4
     3  3   2    15      1ex3ex yx    3X  3 2      15      20       1414        140      15      20
     4  1   1     1      allint zz    OX  1 1       8       8          4          4       4       4
     5  2   2     5      0ex2ex yy    OX  2 2      20      20        140        140      20      20
     6  3   3     6      0ex2ex xx*   OX  3 3      15      15       1414       1414      15      15
     7  3   3    18      0ex2ex xx+   OX  3 3      15      15       1414       1414      15      15
     8  2   2    42      four-ext y   4X  2 2      20      20        140        140      20      20
     9  3   3    43      four-ext x   4X  3 3      15      15       1414       1414      15      15
    10  1   1    75      dg-024ext z  OX  1 1       8       8          4          4       4       4
    11  2   2    76      dg-024ext y  OX  2 2      20      20        140        140      20      20
    12  3   3    77      dg-024ext x  OX  3 3      15      15       1414       1414      15      15
----------------------------------------------------------------------------------------------------
REDTASK #   1 TIME=  11.000 N=  1 (task/type/sgbra)=(   1/24/0) (
REDTASK #   2 TIME=  10.000 N=  1 (task/type/sgbra)=(   2/11/0) (
REDTASK #   3 TIME=   9.000 N=  1 (task/type/sgbra)=(   3/15/0) (
REDTASK #   4 TIME=   8.000 N=  1 (task/type/sgbra)=(   4/ 1/0) (
REDTASK #   5 TIME=   7.000 N=  1 (task/type/sgbra)=(   5/ 5/0) (
REDTASK #   6 TIME=   6.000 N=  1 (task/type/sgbra)=(   6/ 6/1) (
REDTASK #   7 TIME=   5.000 N=  1 (task/type/sgbra)=(   7/18/2) (
REDTASK #   8 TIME=   4.000 N=  1 (task/type/sgbra)=(   8/42/1) (
REDTASK #   9 TIME=   3.000 N=  1 (task/type/sgbra)=(   9/43/1) (
REDTASK #  10 TIME=   2.000 N=  1 (task/type/sgbra)=(  10/75/1) (
REDTASK #  11 TIME=   1.000 N=  1 (task/type/sgbra)=(  11/76/1) (
REDTASK #  12 TIME=   0.000 N=  1 (task/type/sgbra)=(  12/77/1) (
 initializing v-file: 1:                  1558

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:          40 2x:           0 4x:           0
All internal counts: zz :          15 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 reference space has dimension       4
 dsyevx: computed roots 1 to    2(converged:   2)

    root           eigenvalues
    ----           ------------
       1         -46.2453833562
       2         -46.2453833445

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=                     4

         vector  1 from unit 11 written to unit 49 filename cirefv              
 ncorel,neli=                     4                     4
  This is a  mraqcc energy evaluation      

    ------------------------------------------------------------

 setopt2: mraqcc energy evaluation      gvalue=  0.833333333333333      ncorel=
                     4

    ------------------------------------------------------------


    reference energies:

  reference state used is:   1with energy      -46.2453833562

  ### begin active excitation selection ###

 inactive(1:nintern)=AAAAAA
 There are          4 reference CSFs out of          4 valid zwalks.
 There are    0 inactive and    6 active orbitals.
       4 active-active  and        0 inactive excitations.

  ### end active excitation selection ###


 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              1558
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                   30
 residual norm convergence criteria:               0.001000

          starting ci iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         205 2x:          90 4x:          35
All internal counts: zz :          15 yy:          72 xx:          66 ww:           0
One-external counts: yz :         112 yx:         360 yw:           0
Two-external counts: yy :         150 ww:           0 xx:         180 xz:          24 wz:           0 wx:           0
Three-ext.   counts: yx :         151 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1
   ht   1    -2.74679570
calca4: root=   1 anorm**2=  0.00000000 scale:  1.00000000

          calcsovref: tciref block   1

              civs   1
 refs   1    1.00000    

          calcsovref: scrb block   1

                ci   1
 civs   1    1.00000    

          calcsovref: sovref block   1

              v      1
 ref    1    1.00000    

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  1  1    -46.2453833562  3.1086E-15  5.5427E-02  2.1660E-01  1.0000E-03
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   2

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         205 2x:          90 4x:          35
All internal counts: zz :          15 yy:          72 xx:          66 ww:           0
One-external counts: yz :         112 yx:         360 yw:           0
Two-external counts: yy :         150 ww:           0 xx:         180 xz:          24 wz:           0 wx:           0
Three-ext.   counts: yx :         151 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2
   ht   1    -2.74679570
   ht   2    -0.05542705    -0.26301063
calca4: root=   1 anorm**2=  0.07229413 scale:  1.03551636
calca4: root=   2 anorm**2=  0.92770587 scale:  1.38841848

          calcsovref: tciref block   1

              civs   1       civs   2
 refs   1    1.00000      -2.363593E-13

          calcsovref: scrb block   1

                ci   1         ci   2
 civs   1   0.963175      -0.268876    
 civs   2   0.780360        2.79543    

          calcsovref: sovref block   1

              v      1       v      2
 ref    1   0.963175      -0.268876    

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.96317489    -0.26887567
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  2  1    -46.2902901072  4.4907E-02  1.3305E-03  2.9116E-02  1.0000E-03
 mraqcc  #  2  2    -45.6691227545  2.1705E+00  0.0000E+00  4.8377E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   3

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         205 2x:          90 4x:          35
All internal counts: zz :          15 yy:          72 xx:          66 ww:           0
One-external counts: yz :         112 yx:         360 yw:           0
Two-external counts: yy :         150 ww:           0 xx:         180 xz:          24 wz:           0 wx:           0
Three-ext.   counts: yx :         151 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3
   ht   1    -2.74679570
   ht   2    -0.05542705    -0.26301063
   ht   3    -0.00074295     0.01223394    -0.00710085
calca4: root=   1 anorm**2=  0.07127720 scale:  1.03502522
calca4: root=   2 anorm**2=  0.98889819 scale:  1.41028302
calca4: root=   3 anorm**2=  0.92939687 scale:  1.38902731

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3
 refs   1    1.00000      -2.363593E-13   1.903773E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3
 civs   1   0.963475       4.354301E-02   0.264260    
 civs   2   0.801919      -0.378206       -2.87210    
 civs   3    1.10119        17.5120       -7.15909    

          calcsovref: sovref block   1

              v      1       v      2       v      3
 ref    1   0.963684       4.687689E-02   0.262897    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.96368438     0.04687689     0.26289726
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  3  1    -46.2917577587  1.4677E-03  1.3587E-04  9.8922E-03  1.0000E-03
 mraqcc  #  3  2    -45.8803877202  2.1126E-01  0.0000E+00  2.6969E-01  1.0000E-04
 mraqcc  #  3  3    -45.6338620583  2.1353E+00  0.0000E+00  5.0227E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   4

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         205 2x:          90 4x:          35
All internal counts: zz :          15 yy:          72 xx:          66 ww:           0
One-external counts: yz :         112 yx:         360 yw:           0
Two-external counts: yy :         150 ww:           0 xx:         180 xz:          24 wz:           0 wx:           0
Three-ext.   counts: yx :         151 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4
   ht   1    -2.74679570
   ht   2    -0.05542705    -0.26301063
   ht   3    -0.00074295     0.01223394    -0.00710085
   ht   4     0.00086640    -0.00381676    -0.00024846    -0.00083724
calca4: root=   1 anorm**2=  0.07522112 scale:  1.03692869
calca4: root=   2 anorm**2=  0.87208924 scale:  1.36824312
calca4: root=   3 anorm**2=  0.90905108 scale:  1.38168415
calca4: root=   4 anorm**2=  0.84545065 scale:  1.35847365

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4
 refs   1    1.00000      -2.363593E-13   1.903773E-04  -2.927071E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4
 civs   1   0.961791      -8.977733E-02   0.145173       0.214745    
 civs   2   0.804310       0.339125       -1.23571       -2.69389    
 civs   3    1.23601        10.3828        12.3747       -10.0354    
 civs   4    1.33198        42.3433       -20.4621        29.9048    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4
 ref    1   0.961636      -0.100195       0.153518       0.204081    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.96163610    -0.10019488     0.15351831     0.20408116
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  4  1    -46.2919387971  1.8104E-04  4.1983E-05  4.7468E-03  1.0000E-03
 mraqcc  #  4  2    -46.0613504443  1.8096E-01  0.0000E+00  1.4448E-01  1.0000E-04
 mraqcc  #  4  3    -45.8252884487  1.9143E-01  0.0000E+00  3.5111E-01  1.0000E-04
 mraqcc  #  4  4    -45.4937801089  1.9952E+00  0.0000E+00  5.1837E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   5

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         205 2x:          90 4x:          35
All internal counts: zz :          15 yy:          72 xx:          66 ww:           0
One-external counts: yz :         112 yx:         360 yw:           0
Two-external counts: yy :         150 ww:           0 xx:         180 xz:          24 wz:           0 wx:           0
Three-ext.   counts: yx :         151 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1    -2.74679570
   ht   2    -0.05542705    -0.26301063
   ht   3    -0.00074295     0.01223394    -0.00710085
   ht   4     0.00086640    -0.00381676    -0.00024846    -0.00083724
   ht   5    -0.00097605    -0.00093207    -0.00030610     0.00029733    -0.00078497
calca4: root=   1 anorm**2=  0.07627854 scale:  1.03743845
calca4: root=   2 anorm**2=  0.91670168 scale:  1.38444996
calca4: root=   3 anorm**2=  0.19697358 scale:  1.09406288
calca4: root=   4 anorm**2=  0.96649491 scale:  1.40231769
calca4: root=   5 anorm**2=  0.96480497 scale:  1.40171501

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1    1.00000      -2.363593E-13   1.903773E-04  -2.927071E-04   3.607679E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 civs   1   0.961089      -0.112920       5.031075E-03  -0.171625      -0.185975    
 civs   2   0.804600       0.334417      -0.102827        1.43535        2.65951    
 civs   3    1.25487        9.74055       -3.63663       -11.6331        11.4661    
 civs   4    1.51857        38.3387       -21.5760        19.6511       -37.2084    
 civs   5   0.604663        35.6128        44.2089        12.3600       -23.7234    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1   0.961102      -0.109440       2.660335E-02  -0.175132      -0.181459    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     0.96110170    -0.10943979     0.02660335    -0.17513213    -0.18145926
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  5  1    -46.2919641838  2.5387E-05  4.2349E-06  1.9396E-03  1.0000E-03
 mraqcc  #  5  2    -46.0831397528  2.1789E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  5  3    -46.0269013894  2.0161E-01  0.0000E+00  4.2985E-01  1.0000E-04
 mraqcc  #  5  4    -45.8137814414  3.2000E-01  0.0000E+00  3.4015E-01  1.0000E-04
 mraqcc  #  5  5    -45.4043871245  1.9058E+00  0.0000E+00  4.6576E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   6

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         205 2x:          90 4x:          35
All internal counts: zz :          15 yy:          72 xx:          66 ww:           0
One-external counts: yz :         112 yx:         360 yw:           0
Two-external counts: yy :         150 ww:           0 xx:         180 xz:          24 wz:           0 wx:           0
Three-ext.   counts: yx :         151 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1    -2.74679570
   ht   2    -0.05542705    -0.26301063
   ht   3    -0.00074295     0.01223394    -0.00710085
   ht   4     0.00086640    -0.00381676    -0.00024846    -0.00083724
   ht   5    -0.00097605    -0.00093207    -0.00030610     0.00029733    -0.00078497
   ht   6    -0.00004656    -0.00006493     0.00000558    -0.00009466     0.00010400    -0.00003460
calca4: root=   1 anorm**2=  0.07657674 scale:  1.03758216
calca4: root=   2 anorm**2=  0.93391886 scale:  1.39065411
calca4: root=   3 anorm**2=  0.14199806 scale:  1.06864309
calca4: root=   4 anorm**2=  0.97672274 scale:  1.40595972
calca4: root=   5 anorm**2=  0.91610442 scale:  1.38423424
calca4: root=   6 anorm**2=  0.94762452 scale:  1.39557319

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1    1.00000      -2.363593E-13   1.903773E-04  -2.927071E-04   3.607679E-04   1.733138E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1   0.960889       0.108361       4.540409E-02  -0.140506       0.138697      -0.158474    
 civs   2   0.804620      -0.281032      -0.105512        1.06276       -1.74026        2.23858    
 civs   3    1.25810       -8.08665       -3.11244       -13.6671       -4.30361        9.74605    
 civs   4    1.55114       -38.7790       -2.84375        8.44812       -17.6950       -48.2923    
 civs   5   0.710231       -24.1287       -46.6192        28.7963        41.9699       -2.24978    
 civs   6    1.04688       -105.385        65.6639        97.9729        275.354        166.846    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.960949       0.107641       2.996326E-02  -0.133494       0.162971      -0.140403    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.96094882     0.10764079     0.02996326    -0.13349398     0.16297064    -0.14040282

 trial vector basis is being transformed.  new dimension:   2
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  6  1    -46.2919686173  4.4335E-06  2.8383E-07  4.3358E-04  1.0000E-03
 mraqcc  #  6  2    -46.1109869016  2.7847E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  6  3    -46.0541728451  2.7271E-02  0.0000E+00  4.2136E-01  1.0000E-04
 mraqcc  #  6  4    -45.8345793967  2.0798E-02  0.0000E+00  2.8066E-01  1.0000E-04
 mraqcc  #  6  5    -45.6488724289  2.4449E-01  0.0000E+00  4.6988E-01  1.0000E-04
 mraqcc  #  6  6    -45.3259245825  1.8273E+00  0.0000E+00  4.5916E-01  1.0000E-04
 
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mraqcc   convergence criteria satisfied after  6 iterations.

 final mraqcc   convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  6  1    -46.2919686173  4.4335E-06  2.8383E-07  4.3358E-04  1.0000E-03
 mraqcc  #  6  2    -46.1109869016  2.7847E-02  0.0000E+00  0.0000E+00  1.0000E-04

####################CIUDGINFO####################

   aqcc(lrt) vector at position   1 energy=  -46.291968617317

################END OF CIUDGINFO################

 
 a4den factor for root 1  =  1.012927784
diagon:itrnv=   0
    1 of the   3 expansion vectors are transformed.
    1 of the   2 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
maximum overlap with reference    1(overlap= 0.96095)
reference energy =                         -46.2453833562
total aqcc energy =                        -46.2919686173
diagonal element shift (lrtshift) =         -0.0465852611

 information on vector: 1 from unit 11 written to unit 48 filename civout              


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -46.2919686173

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     3    4    5   17   22   31

                                         symmetry   a1   a1   a1   a2   b1   b2 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  5  1       1  0.731873                        +    +    +         +       
 z*  5  1       2 -0.590936                        +    +         +         +  
 z*  5  1       3 -0.196420                        +         +    +         +  
 y   5  1       6 -0.085711              2( b1 )   +    +    +                 
 y   5  1       8  0.060397              4( b1 )   +    +    +                 
 y   5  1      13  0.053143              2( b2 )   +    +         +            
 y   5  1      15 -0.042001              4( b2 )   +    +         +            
 y   5  1      20  0.049324              2( a1 )   +    +              +       
 y   5  1      21  0.040929              3( a1 )   +    +              +       
 y   5  1      24  0.036100              6( a1 )   +    +              +       
 y   5  1      25 -0.036447              7( a1 )   +    +              +       
 y   5  1      30 -0.069559              1( a2 )   +    +                   +  
 y   5  1      31  0.048866              2( a2 )   +    +                   +  
 y   5  1      36 -0.010133              4( b2 )   +         +    +            
 y   5  1      41  0.057878              2( a1 )   +         +         +       
 y   5  1      42 -0.064698              3( a1 )   +         +         +       
 y   5  1      45  0.044278              6( a1 )   +         +         +       
 y   5  1      46  0.041712              7( a1 )   +         +         +       
 y   5  1      51 -0.020683              1( a2 )   +         +              +  
 y   5  1      52  0.015226              2( a2 )   +         +              +  
 y   5  1      58 -0.013531              2( a1 )   +              +         +  
 y   5  1      59  0.074753              3( a1 )   +              +         +  
 y   5  1      62 -0.018253              6( a1 )   +              +         +  
 y   5  1      63 -0.049157              7( a1 )   +              +         +  
 y   5  1      69  0.010847              2( b2 )   +                   +    +  
 x   5  1     154  0.010165   10( a1 )   1( b1 )   +    +                      
 x   5  1     176  0.010617   10( a1 )   3( b1 )   +    +                      
 x   5  1     183  0.011262    6( a1 )   4( b1 )   +    +                      
 x   5  1     184 -0.011281    7( a1 )   4( b1 )   +    +                      
 x   5  1     208  0.012334    9( a1 )   6( b1 )   +    +                      
 x   5  1     220  0.015755   10( a1 )   7( b1 )   +    +                      
 x   5  1     232  0.013069    2( a2 )   4( b2 )   +    +                      
 x   5  1     242 -0.016361    3( a2 )   7( b2 )   +    +                      
 x   5  1     243  0.042109    1( a1 )   1( b1 )   +         +                 
 x   5  1     246  0.031315    4( a1 )   1( b1 )   +         +                 
 x   5  1     256  0.010681    3( a1 )   2( b1 )   +         +                 
 x   5  1     265  0.031522    1( a1 )   3( b1 )   +         +                 
 x   5  1     268  0.024600    4( a1 )   3( b1 )   +         +                 
 x   5  1     281  0.013282    6( a1 )   4( b1 )   +         +                 
 x   5  1     282  0.012703    7( a1 )   4( b1 )   +         +                 
 x   5  1     306  0.024753    9( a1 )   6( b1 )   +         +                 
 x   5  1     307  0.013385   10( a1 )   6( b1 )   +         +                 
 x   5  1     401 -0.012432    7( a1 )   4( b2 )   +              +            
 x   5  1     437 -0.010388   10( a1 )   7( b2 )   +              +            
 x   5  1     441  0.012007    2( a1 )   3( a1 )   +                   +       
 x   5  1     451 -0.011071    3( a1 )   6( a1 )   +                   +       
 x   5  1     455 -0.011107    2( a1 )   7( a1 )   +                   +       
 x   5  1     459 -0.016034    6( a1 )   7( a1 )   +                   +       
 x   5  1     483 -0.011658    9( a1 )  10( a1 )   +                   +       
 x   5  1     517  0.012109    6( b1 )   7( b1 )   +                   +       
 x   5  1     538  0.012351    6( b2 )   7( b2 )   +                   +       
 x   5  1     541  0.011489    3( a1 )   1( a2 )   +                        +  
 x   5  1     552 -0.010654    3( a1 )   2( a2 )   +                        +  
 x   5  1     556  0.014606    7( a1 )   2( a2 )   +                        +  
 x   5  1     572 -0.039783    1( b1 )   1( b2 )   +                        +  
 x   5  1     574 -0.029862    3( b1 )   1( b2 )   +                        +  
 x   5  1     586 -0.029599    1( b1 )   3( b2 )   +                        +  
 x   5  1     588 -0.023278    3( b1 )   3( b2 )   +                        +  
 x   5  1     620 -0.023247    7( b1 )   7( b2 )   +                        +  
 x   5  1     629  0.012275    9( a1 )   1( b1 )        +    +                 
 x   5  1     636 -0.017653    5( a1 )   2( b1 )        +    +                 
 x   5  1     651  0.011311    9( a1 )   3( b1 )        +    +                 
 x   5  1     658  0.012635    5( a1 )   4( b1 )        +    +                 
 x   5  1     676  0.015070    1( a1 )   6( b1 )        +    +                 
 x   5  1     679  0.013875    4( a1 )   6( b1 )        +    +                 
 x   5  1     700 -0.011604    3( a2 )   1( b2 )        +    +                 
 x   5  1     706 -0.010671    3( a2 )   3( b2 )        +    +                 
 x   5  1     749  0.010657   10( a1 )   1( b2 )        +         +            
 x   5  1     755  0.013294    5( a1 )   2( b2 )        +         +            
 x   5  1     777 -0.010570    5( a1 )   4( b2 )        +         +            
 x   5  1     795 -0.012502    1( a1 )   6( b2 )        +         +            
 x   5  1     798 -0.011369    4( a1 )   6( b2 )        +         +            
 x   5  1     824 -0.012920    2( a1 )   5( a1 )        +              +       
 x   5  1     825 -0.010037    3( a1 )   5( a1 )        +              +       
 x   5  1     853 -0.010023    1( a1 )  10( a1 )        +              +       
 x   5  1     890  0.010007    1( b1 )   7( b1 )        +              +       
 x   5  1     906  0.011595    1( b2 )   6( b2 )        +              +       
 x   5  1     908  0.010505    3( b2 )   6( b2 )        +              +       
 x   5  1     911  0.011394    1( b2 )   7( b2 )        +              +       
 x   5  1     913  0.010406    3( b2 )   7( b2 )        +              +       
 x   5  1     921 -0.014249    5( a1 )   1( a2 )        +                   +  
 x   5  1     932  0.010159    5( a1 )   2( a2 )        +                   +  
 x   5  1     956  0.012088    7( b1 )   1( b2 )        +                   +  
 x   5  1     970  0.011116    7( b1 )   3( b2 )        +                   +  
 x   5  1     992  0.011424    1( b1 )   7( b2 )        +                   +  
 x   5  1     994  0.010544    3( b1 )   7( b2 )        +                   +  
 x   5  1    1104 -0.011036    2( a1 )   5( a1 )             +         +       
 x   5  1    1105  0.013845    3( a1 )   5( a1 )             +         +       
 x   5  1    1125  0.012982    1( a1 )   9( a1 )             +         +       
 x   5  1    1128  0.011928    4( a1 )   9( a1 )             +         +       
 x   5  1    1165  0.012300    1( b1 )   6( b1 )             +         +       
 x   5  1    1167  0.011326    3( b1 )   6( b1 )             +         +       
 x   5  1    1191 -0.010073    1( b2 )   7( b2 )             +         +       
 x   5  1    1369 -0.014511    3( a1 )   5( a1 )                  +         +  
 x   5  1    1434  0.011034    1( b1 )   7( b1 )                  +         +  
 x   5  1    1436  0.010140    3( b1 )   7( b1 )                  +         +  
 x   5  1    1455  0.010972    1( b2 )   7( b2 )                  +         +  
 x   5  1    1457  0.010147    3( b2 )   7( b2 )                  +         +  

 ci coefficient statistics:
           rq > 0.1                3
      0.1> rq > 0.01              95
     0.01> rq > 0.001            310
    0.001> rq > 0.0001           250
   0.0001> rq > 0.00001           75
  0.00001> rq > 0.000001          22
 0.000001> rq                    803
           all                  1558
  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.731872594629
     2     2     -0.590935774021
     3     3     -0.196419649160
     4     4     -0.000022206121

 number of reference csfs (nref) is     4.  root number (iroot) is  1.
 c0**2 =   0.92342326  c0**2(renormalized) =   0.99413600
 c**2 (all zwalks) =   0.92342326  c**2(all zwalks, renormalized) =   0.99413600
 passed aftci ... 
 readint2: molcas,dalton2=                     0                     0
 files%faoints=aoints              
lodens (list->root)=  1
sifcfg setup: record length 4096 DP
# d1 elements per record  3272
# d2 elements per record  2730
  The AQCC density will be calculated.
 item #                     1 suffix=:.drt1.state1:
 read_civout: repnuc=  -43.4985876562748     
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method: 30 last record  1max overlap with ref# 96% root-following 0
 MR-CISD energy:   -46.29196862    -2.79338096
 residuum:     0.00043358
 deltae:     0.00000443
 apxde:     0.00000028
 a4den:     1.01292778
 reference energy:   -46.24538336    -2.74679570

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.96094882     0.10764079     0.02996326    -0.13349398     0.16297064    -0.14040282     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.96094882     0.10764079     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 rdcirefv: extracting reference vector # 1(blocksize=     4 #zcsf=       4)
 =========== Executing IN-CORE method ==========
================================================================================
   DYZ=       0  DYX=       0  DYW=       0
   D0Z=       3  D0Y=       0  D0X=       0  D0W=       0
  DDZI=      16 DDYI=       0 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
 root #                      1 : Scaling(1) with    1.01292778411167      
 corresponding to ref #                      1
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=      16  DYX=      60  DYW=       0
   D0Z=       3  D0Y=      18  D0X=      12  D0W=       0
  DDZI=      16 DDYI=      60 DDXI=      30 DDWI=       0
  DDZE=       0 DDYE=      20 DDXE=      15 DDWE=       0
================================================================================
Trace of MO density:     4.000000
    4  correlated and     8  frozen core electrons

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.99088025    -0.00022493    -0.00009090     0.00000000    -0.00087343     0.00259873
   MO   4     0.00000000     0.00000000    -0.00022493     0.92218724     0.11975108     0.00000000    -0.04808185     0.08761138
   MO   5     0.00000000     0.00000000    -0.00009090     0.11975108     0.60757626     0.00000000     0.03131373     0.04231137
   MO   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00422003     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000    -0.00087343    -0.04808185     0.03131373     0.00000000     0.00699985    -0.00302873
   MO   8     0.00000000     0.00000000     0.00259873     0.08761138     0.04231137     0.00000000    -0.00302873     0.01315201
   MO   9     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00338633     0.00000000     0.00000000
   MO  10     0.00000000     0.00000000     0.00138438     0.00003417     0.00000275     0.00000000    -0.00000393     0.00001018
   MO  11     0.00000000     0.00000000    -0.00115214    -0.03988836     0.02056344     0.00000000     0.00565835    -0.00320742
   MO  12     0.00000000     0.00000000    -0.00243625    -0.05517366    -0.03318977     0.00000000     0.00141981    -0.00949495
   MO  13     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00110301     0.00000000     0.00000000
   MO  14     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00004332     0.00000000     0.00000000
   MO  15     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00020691     0.00000000     0.00000000
   MO  16     0.00000000     0.00000000    -0.00049241     0.00000620     0.00000160     0.00000000     0.00000007    -0.00000027

                MO   9         MO  10         MO  11         MO  12         MO  13         MO  14         MO  15         MO  16
   MO   3     0.00000000     0.00138438    -0.00115214    -0.00243625     0.00000000     0.00000000     0.00000000    -0.00049241
   MO   4     0.00000000     0.00003417    -0.03988836    -0.05517366     0.00000000     0.00000000     0.00000000     0.00000620
   MO   5     0.00000000     0.00000275     0.02056344    -0.03318977     0.00000000     0.00000000     0.00000000     0.00000160
   MO   6     0.00338633     0.00000000     0.00000000     0.00000000     0.00110301     0.00004332     0.00020691     0.00000000
   MO   7     0.00000000    -0.00000393     0.00565835     0.00141981     0.00000000     0.00000000     0.00000000     0.00000007
   MO   8     0.00000000     0.00001018    -0.00320742    -0.00949495     0.00000000     0.00000000     0.00000000    -0.00000027
   MO   9     0.00282337     0.00000000     0.00000000     0.00000000     0.00098439    -0.00002378     0.00015280     0.00000000
   MO  10     0.00000000     0.00258124    -0.00000852    -0.00001662     0.00000000     0.00000000     0.00000000    -0.00012556
   MO  11     0.00000000    -0.00000852     0.00481704     0.00175533     0.00000000     0.00000000     0.00000000     0.00000038
   MO  12     0.00000000    -0.00001662     0.00175533     0.00721197     0.00000000     0.00000000     0.00000000     0.00000080
   MO  13     0.00098439     0.00000000     0.00000000     0.00000000     0.00038644    -0.00005155     0.00003872     0.00000000
   MO  14    -0.00002378     0.00000000     0.00000000     0.00000000    -0.00005155     0.00232449     0.00048029     0.00000000
   MO  15     0.00015280     0.00000000     0.00000000     0.00000000     0.00003872     0.00048029     0.00199354     0.00000000
   MO  16     0.00000000    -0.00012556     0.00000038     0.00000080     0.00000000     0.00000000     0.00000000     0.00000772

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     2.00000000     0.99090254     0.97879658     0.57322045     0.00730789     0.00617984     0.00357758
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     0.00266370     0.00258533     0.00164380     0.00013010     0.00008601     0.00006382     0.00000239     0.00000143

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4
   MO   1     0.40455431     0.04317061    -0.02944933     0.00000000
   MO   2     0.04317061     0.00603135    -0.00446034     0.00000000
   MO   3    -0.02944933    -0.00446034     0.00342144     0.00000000
   MO   4     0.00000000     0.00000000     0.00000000     0.00145460

Natural orbital populations,block 2
              MO     1       MO     2       MO     3       MO     4       MO
  occ(*)=     0.41134789     0.00262790     0.00145460     0.00003131

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     0.55847604     0.00000000     0.05998451     0.00000000    -0.04087451     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.00691315     0.00000000     0.00551233     0.00000000     0.00177987     0.00041513
   MO   4     0.00000000     0.05998451     0.00000000     0.00840492     0.00000000    -0.00620833     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.00551233     0.00000000     0.00451031     0.00000000     0.00153165     0.00036216
   MO   6     0.00000000    -0.04087451     0.00000000    -0.00620833     0.00000000     0.00475198     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000     0.00177987     0.00000000     0.00153165     0.00000000     0.00057443     0.00013899
   MO   8     0.00000000     0.00000000     0.00041513     0.00000000     0.00036216     0.00000000     0.00013899     0.00236227
   MO   9     0.00000000     0.00000000    -0.00072791     0.00000000    -0.00066157     0.00000000    -0.00027295    -0.00063643

                MO   9
   MO   2     0.00000000
   MO   3    -0.00072791
   MO   4     0.00000000
   MO   5    -0.00066157
   MO   6     0.00000000
   MO   7    -0.00027295
   MO   8    -0.00063643
   MO   9     0.00233752

Natural orbital populations,block 3
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     0.56797043     0.01199143     0.00362216     0.00285164     0.00170276     0.00014914     0.00004035
              MO     9       MO
  occ(*)=     0.00000272

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     0.41244345     0.00000000     0.03090534     0.00000000    -0.02417445     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.00384904     0.00000000     0.00309626     0.00000000    -0.00101123     0.00010111
   MO   4     0.00000000     0.03090534     0.00000000     0.00365708     0.00000000    -0.00303767     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.00309626     0.00000000     0.00259532     0.00000000    -0.00091084     0.00008254
   MO   6     0.00000000    -0.02417445     0.00000000    -0.00303767     0.00000000     0.00264746     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000    -0.00101123     0.00000000    -0.00091084     0.00000000     0.00036132    -0.00002751
   MO   8     0.00000000     0.00000000     0.00010111     0.00000000     0.00008254     0.00000000    -0.00002751     0.00119126
   MO   9     0.00000000     0.00000000     0.00006710     0.00000000     0.00016655     0.00000000    -0.00013918    -0.00027470

                MO   9
   MO   2     0.00000000
   MO   3     0.00006710
   MO   4     0.00000000
   MO   5     0.00016655
   MO   6     0.00000000
   MO   7    -0.00013918
   MO   8    -0.00027470
   MO   9     0.00230129

Natural orbital populations,block 4
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     0.41619866     0.00668442     0.00249092     0.00236811     0.00112259     0.00012081     0.00005842
              MO     9       MO
  occ(*)=     0.00000229


 total number of electrons =   12.0000000000

 test slabel:                     4
  a1  a2  b1  b2 


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        a1  partial gross atomic populations
   ao class       1a1        2a1        3a1        4a1        5a1        6a1 
     3_ s       2.000000   0.000000   0.990470   0.000412   0.000000   0.000000
     3_ p       0.000000   2.000000   0.000000   0.000000   0.000000   0.007290
     3_ d       0.000000   0.000000   0.000433   0.978384   0.573220   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000000   0.000000   0.000018
 
   ao class       7a1        8a1        9a1       10a1       11a1       12a1 
     3_ s       0.000000   0.000000   0.000000   0.002585   0.000000   0.000000
     3_ p       0.000000   0.000000   0.000005   0.000000   0.000003   0.000130
     3_ d       0.006180   0.003578   0.000000   0.000000   0.000000   0.000000
     3_ f       0.000000   0.000000   0.002659   0.000000   0.001641   0.000000
 
   ao class      13a1       14a1       15a1       16a1 
     3_ s       0.000000   0.000000   0.000000   0.000001
     3_ p       0.000000   0.000000   0.000002   0.000000
     3_ d       0.000086   0.000064   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000000

                        a2  partial gross atomic populations
   ao class       1a2        2a2        3a2        4a2 
     3_ d       0.411348   0.002628   0.000000   0.000031
     3_ f       0.000000   0.000000   0.001455   0.000000

                        b1  partial gross atomic populations
   ao class       1b1        2b1        3b1        4b1        5b1        6b1 
     3_ p       2.000000   0.000000   0.011798   0.000000   0.000045   0.000003
     3_ d       0.000000   0.567970   0.000000   0.003622   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000194   0.000000   0.002807   0.001699
 
   ao class       7b1        8b1        9b1 
     3_ p       0.000149   0.000000   0.000003
     3_ d       0.000000   0.000040   0.000000
     3_ f       0.000000   0.000000   0.000000

                        b2  partial gross atomic populations
   ao class       1b2        2b2        3b2        4b2        5b2        6b2 
     3_ p       2.000000   0.000000   0.006670   0.000000   0.000011   0.000002
     3_ d       0.000000   0.416199   0.000000   0.002491   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000014   0.000000   0.002357   0.001121
 
   ao class       7b2        8b2        9b2 
     3_ p       0.000120   0.000000   0.000002
     3_ d       0.000000   0.000058   0.000000
     3_ f       0.000001   0.000000   0.000000


                        gross atomic populations
     ao            3_
      s         2.993469
      p         6.026233
      d         2.966332
      f         0.013965
    total      12.000000
 

 Total number of electrons:   12.00000000

 DA ...
