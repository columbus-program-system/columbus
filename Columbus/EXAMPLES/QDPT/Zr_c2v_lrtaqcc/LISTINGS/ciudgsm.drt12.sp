1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.

  This is a  mraqcc energy evaluation      

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  1  1    -46.2453833562  9.3259E-15  5.5393E-02  2.1660E-01  1.0000E-03
 mraqcc  #  2  1    -46.2902677028  4.4884E-02  1.3509E-03  2.9193E-02  1.0000E-03
 mraqcc  #  3  1    -46.2917584116  1.4907E-03  1.3001E-04  9.8268E-03  1.0000E-03
 mraqcc  #  4  1    -46.2919521397  1.9373E-04  1.8586E-05  3.6112E-03  1.0000E-03
 mraqcc  #  5  1    -46.2919651359  1.2996E-05  4.2337E-06  1.7721E-03  1.0000E-03
 mraqcc  #  6  1    -46.2919687200  3.5841E-06  1.3467E-07  3.2736E-04  1.0000E-03

 mraqcc   convergence criteria satisfied after  6 iterations.

 final mraqcc   convergence information:
 mraqcc  #  6  1    -46.2919687200  3.5841E-06  1.3467E-07  3.2736E-04  1.0000E-03

 number of reference csfs (nref) is     4.  root number (iroot) is  1.
 c0**2 =   0.92341318  c0**2(renormalized) =   0.99413446
 c**2 (all zwalks) =   0.92341318  c**2(all zwalks, renormalized) =   0.99413446
