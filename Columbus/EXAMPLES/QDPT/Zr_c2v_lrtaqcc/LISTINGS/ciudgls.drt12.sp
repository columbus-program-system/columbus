1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      2009-03.    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


================ Computing sorting integral file structure ================

                    -----z----- -----y----- -----x----- -----w----- ---total---

                CSFs         4         140        1414           0        1558
      internal walks        15          20          15           0          50
valid internal walks         4          20          15           0          39
 getinfoarray: info=                     6 :                     1
                  8192                  6552                  8192
                  5460                     0
 icd(3)=                  2976 ci%nnlev=                   595  l2rec=
                  8192  n2max=                  5460
 lcore1,lcore2=              13068137              13044821
 lencor,maxblo              13107200                 60000
========================================
 current settings:
 minbl3         381
 minbl4         381
 locmaxbl3     4288
 locmaxbuf     2144
 maxbl3       60000
 maxbl3       60000
 maxbl4       60000
 maxbuf       30006
========================================

 sorted 4-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)

 sorted 3-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)
 Orig.  diagonal integrals:  1electron:        34
                             0ext.    :        42
                             2ext.    :       336
                             4ext.    :       812


 Orig. off-diag. integrals:  4ext.    :     24411
                             3ext.    :     18720
                             2ext.    :      6792
                             1ext.    :      1272
                             0ext.    :        99
                             2ext. SO :         0
                             1ext. SO :         0
                             0ext. SO :         0
                             1electron:       128


 Sorted integrals            3ext.  w :     17370 x :     16020
                             4ext.  w :     20955 x :     17777


Cycle #  1 sortfile size=    163835(       5 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13068137
Cycle #  2 sortfile size=    163835(       5 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13068137
 minimum size of srtscr:     98301 WP (     3 records)
 maximum size of srtscr:    163835 WP (     5 records)
diagi   file:      4 records  of   1536 WP each=>       6144 WP total
ofdgi   file:      8 records  of   1536 WP each=>      12288 WP total
fil3w   file:      1 records  of  30006 WP each=>      30006 WP total
fil3x   file:      1 records  of  30006 WP each=>      30006 WP total
fil4w   file:      1 records  of  30006 WP each=>      30006 WP total
fil4x   file:      1 records  of  30006 WP each=>      30006 WP total
 compressed index vector length=                    12
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 3,
  GSET = 3,
   DAVCOR =10,
  NCOREL = 4
   nroot=1
  IVMODE = 3
  NBKITR = 1
   nvbkmn=2
  RTOLBK = 1e-3
   niter=30
   nvcimn=2
   rtolci=1e-3
  NVCIMX = 6
  NVRFMX = 6
  NVBKMX = 6
  IDEN  = 1
  CSFPRN = 10,
  update_mode=1
  nvrfmn=2
   lrtshift=-0.0465482970
 /&end
 ------------------------------------------------------------------------
lodens (list->root)=  1
invlodens (root->list)=  1
 LRT calculation: diagonal shift= -4.654829700000000E-002
 bummer (warning):2:changed keyword: nbkitr=                      0
 USING SEGMENTS OF EQUAL SIZE

****************  list of control variables  ****************
 lvlprt =    0      nroot  =    1      noldv  =   0      noldhv =   0
 nunitv =    1      nbkitr =    0      niter  =  30      davcor =  10
 csfprn =   10      ivmode =    3      istrt  =   0      vout   =   0
 iortls =    0      nvbkmx =    6      ibktv  =  -1      ibkthv =  -1
 nvcimx =    6      icitv  =   -1      icithv =  -1      frcsub =   0
 nvbkmn =    2      nvcimn =    2      maxseg =   4      nrfitr =  30
 ncorel =    4      nvrfmx =    6      nvrfmn =   2      iden   =   1
 itran  =    0      froot  =    0      rtmode =   0      ncouple=   1
 skipso =    F      dalton2=    0      molcas =   0      finalv =   0
 finalw =    0      cosmocalc=   0    with_tsklst=   0
 nsegwx =    1     1     1     1
 nseg0x =    1     1     1     1
 nseg1x =    1     1     1     1
 nseg2x =    1     1     1     1
 nseg3x =    1     1     1     1
 nseg4x =    1     1     1     1
 no0ex  =      0    no1ex  =      0    no2ex  =     0    no3ex  =     0
 no4ex  =      0    nodiag =      0
 cdg4ex =    1      c3ex1ex=    1      c2ex0ex=   1
 fileloc=    0     0     0     0     0     0     0     1     1     1
 directhd=   1      noaqccshift_zyxw=      0
 critical_crit=-1.00000    critical_delta= 0.05000

 ctol   = 0.010000    lrtshift=-.046548    smalld =0.001000


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-03    1.000E-03
 Computing density:                    .drt1.state1
 using                      1  nodes and                      1  cores.
 szdg/szodg per processor=                  6144                 12288
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core           13107199 DP per process

********** Integral sort section *************


 workspace allocation information: lencor=  13107199

 echo of the input for program cisrt:
 ------------------------------------------------------------------------
  &input
  maxbl3=60000
  maxbl4=60000
  &end
 ------------------------------------------------------------------------
 
 ( 6) listing file:                    ciudgls             
 ( 5) input file:                      cisrtin   
 (17) cidrt file:                      cidrtfl             
 (11) transformed integrals file:      moints    
 (12) diagonal integral file:          diagint             
 (13) off-diagonal integral file:      ofdgint             
 (31) 4-external w integrals file:     fil4w               
 (32) 4-external x integrals file:     fil4x               
 (33) 3-external w integrals file:     fil3w               
 (34) 3-external x integrals file:     fil3x               
 (21) scratch da sorting file:         srtscr              
 (12) 2-e integral file [fsplit=2]:    moints2   

 input integral file header information:
                                                                                 
 aoints SIFS file created by argos.      zam792            17:56:38.564 17-Dec-13
  cidrt_title                                                                    
 MO-coefficients from mcscf.x                                                    
  with dummy occupation 1.0 for active orbitals                                  
  total ao core energy =    0.000000000                                          
 MCSCF energy =     -46.301878576                                                
 SIFS file created by program tran.      zam792            17:56:38.888 17-Dec-13

 input energy(*) values:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total core energy =   0.000000000000E+00

 nsym = 4 nmot=  34

 symmetry  =    1    2    3    4
 slabel(*) =   a1   a2   b1   b2
 nmpsy(*)  =   14    4    8    8

 info(*) =          1      8192      6552      8192      5460         0

 orbital labels, i:molab(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012  13:tout:013  14:tout:014  15:tout:015  16:tout:016  17:tout:017  18:tout:018  19:tout:019  20:tout:020
  21:tout:021  22:tout:022  23:tout:023  24:tout:024  25:tout:025  26:tout:026  27:tout:027  28:tout:028  29:tout:029  30:tout:030
  31:tout:031  32:tout:032  33:tout:033  34:tout:034

 input parameters:
 prnopt=  0
 ldamin=    4095 ldamax=   32767 ldainc=      64
 maxbuf=   30006 maxbl3=   60000 maxbl4=   60000 intmxo=    1536
  Using 32 bit compression 

 drt information:
  cidrt_title                                                                    
 nmotd =  38 nfctd =   4 nfvtc =   0 nmot  =  34
 nlevel =  34 niot  =   6 lowinl=  29
 orbital-to-level map(*)
   -1  -1  29  30  31   1   2   3   4   5   6   7   8   9  10  11  32  12  13  14
   -1  33  15  16  17  18  19  20  21  -1  34  22  23  24  25  26  27  28
 compressed map(*)
   29  30  31   1   2   3   4   5   6   7   8   9  10  11  32  12  13  14  33  15
   16  17  18  19  20  21  34  22  23  24  25  26  27  28
 levsym(*)
    1   1   1   1   1   1   1   1   1   1   1   2   2   2   3   3   3   3   3   3
    3   4   4   4   4   4   4   4   1   1   1   2   3   4
 repartitioning mu(*)=
   0.  0.  0.  0.  0.  0.

 new core energy added to the energy(*) list.
 from the integral file: h1_core= -4.349858765627E+01

 indxdg: diagonal integral statistics.
 total number of integrals contributing to diagonal matrix elements:      1190
 number with all external indices:       812
 number with half external - half internal indices:       336
 number with all internal indices:        42

 indxof: off-diagonal integral statistics.
    4-external integrals: num=      24411 strt=          1
    3-external integrals: num=      18720 strt=      24412
    2-external integrals: num=       6792 strt=      43132
    1-external integrals: num=       1272 strt=      49924
    0-external integrals: num=         99 strt=      51196

 total number of off-diagonal integrals:       51294


 indxof(2nd)  ittp=   3 numx(ittp)=        6792
 indxof(2nd)  ittp=   4 numx(ittp)=        1272
 indxof(2nd)  ittp=   5 numx(ittp)=          99

 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg=  12931980
 pro2e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652

 pro2e:     25126 integrals read in     5 records.

 pro2e:         0 integrals 34-ext integrals skipped.
 pro1e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652
 pro1e: eref =    0.000000000000000E+00
 total size of srtscr:                     4  records of                  32767 
 WP =               1048544 Bytes
 putdg        1     596    1191    1786    3322   36089   57934    3018   46706   90394
   123161  131353  136813  158652

 putf:       4 buffers of length    1536 written to file 12
 diagonal integral file completed.

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep4e:    17 blocks of linear combinations of 4-external integrals processed.
 number of sorted 4-external integrals      38732
 number of original 4-external integrals    24411


 putf34: external integral file complete. nfilw=    31 nfilx=    32 nrecw=     1 nrecx=     1 lbufp= 30006

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep3e:    19 blocks of linear combinations of 3-external integrals processed.
 number of sorted 3-external integrals      33390
 number of original 3-external integrals    18720


 putf34: external integral file complete. nfilw=    33 nfilx=    34 nrecw=     1 nrecx=     1 lbufp= 30006
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      6792         3     43132     43132      6792     51294
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      1272         4     49924     49924      1272     51294
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd        99         5     51196     51196        99     51294

 putf:       8 buffers of length    1536 written to file 13
 off-diagonal files sort completed.
 executing brd_struct for cisrtinfo
cisrtinfo:
bufszi  1536
 diagfile 4ext:     812 2ext:     336 0ext:      42
 fil4w,fil4x  :   24411 fil3w,fil3x :   18720
 ofdgint  2ext:    6792 1ext:    1272 0ext:      99so0ext:       0so1ext:       0so2ext:       0
buffer minbl4     381 minbl3     381 maxbl2     384nbas:  11   3   7   7   0   0   0   0 maxbuf 30006
 CIUDG version 5.9.7 ( 5-Oct-2004)

 workspace allocation information: lcore=  13107199

 core energy values from the integral file:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -4.349858765627E+01, ietype=    6,   fcore energy of type: H1(*)   

 total core repulsion energy = -4.349858765627E+01
 nmot  =    38 niot  =     6 nfct  =     4 nfvt  =     0
 nrow  =    23 nsym  =     4 ssym  =     4 lenbuf=  1600
 nwalk,xbar:         50       15       20       15        0
 nvalwt,nvalw:       39        4       20       15        0
 ncsft:            1558
 total number of valid internal walks:      39
 nvalz,nvaly,nvalx,nvalw =        4      20      15       0

 cisrt info file parameters:
 file number  12 blocksize   1536
 mxbld   1536
 nd4ext,nd2ext,nd0ext   812   336    42
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int    24411    18720     6792     1272       99        0        0        0
 minbl4,minbl3,maxbl2   381   381   384
 maxbuf 30006
 number of external orbitals per symmetry block:  11   3   7   7
 nmsym   4 number of internal orbitals   6
 executing brd_struct for drt
 executing brd_struct for orbinf
 executing brd_struct for momap
 calcthrxt: niot,maxw1=                     6                    11
 block size     0
 pthz,pthy,pthx,pthw:    15    20    15     0 total internal walks:      50
 maxlp3,n2lp,n1lp,n0lp    11     0     0     0
 orbsym(*)= 1 1 1 2 3 4

 setref:        4 references kept,
               11 references were marked as invalid, out of
               15 total.
 nmb.of records onel     1
 nmb.of records 2-ext     5
 nmb.of records 1-ext     1
 nmb.of records 0-ext     1
 nmb.of records 2-int     0
 nmb.of records 1-int     0
 nmb.of records 0-int     0
 ---------memory usage in DP -----------------
 < n-ex core usage >
     routines:
    fourex            61797
    threx             60079
    twoex              3549
    onex               1823
    allin              1536
    diagon             2109
               =======
   maximum            61797
 
  __ static summary __ 
   reflst                 4
   hrfspc                 4
               -------
   static->               8
 
  __ core required  __ 
   totstc                 8
   max n-ex           61797
               -------
   totnec->           61805
 
  __ core available __ 
   totspc          13107199
   totnec -           61805
               -------
   totvec->        13045394

 number of external paths / symmetry
 vertex x     100      82      98      98
 vertex w     128      82      98      98
segment: free space=    13045394
 reducing frespc by                   345 to               13045049 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1           6|         4|         0|         4|         0|         1|
 -------------------------------------------------------------------------------
  Y 2          20|       140|         4|        20|         4|         2|
 -------------------------------------------------------------------------------
  X 3          15|      1414|       144|        15|        24|         3|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=          12DP  conft+indsym=          80DP  drtbuffer=         253 DP

dimension of the ci-matrix ->>>      1558

 executing brd_struct for civct
 gentasklist: ntask=                    12
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS   
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      15       6       1414          4      15       4
     2  2   1    11      one-ext yz   1X  2 1      20       6        140          4      20       4
     3  3   2    15      1ex3ex yx    3X  3 2      15      20       1414        140      15      20
     4  1   1     1      allint zz    OX  1 1       6       6          4          4       4       4
     5  2   2     5      0ex2ex yy    OX  2 2      20      20        140        140      20      20
     6  3   3     6      0ex2ex xx*   OX  3 3      15      15       1414       1414      15      15
     7  3   3    18      0ex2ex xx+   OX  3 3      15      15       1414       1414      15      15
     8  2   2    42      four-ext y   4X  2 2      20      20        140        140      20      20
     9  3   3    43      four-ext x   4X  3 3      15      15       1414       1414      15      15
    10  1   1    75      dg-024ext z  OX  1 1       6       6          4          4       4       4
    11  2   2    76      dg-024ext y  OX  2 2      20      20        140        140      20      20
    12  3   3    77      dg-024ext x  OX  3 3      15      15       1414       1414      15      15
----------------------------------------------------------------------------------------------------
REDTASK #   1 TIME=  11.000 N=  1 (task/type/sgbra)=(   1/24/0) (
REDTASK #   2 TIME=  10.000 N=  1 (task/type/sgbra)=(   2/11/0) (
REDTASK #   3 TIME=   9.000 N=  1 (task/type/sgbra)=(   3/15/0) (
REDTASK #   4 TIME=   8.000 N=  1 (task/type/sgbra)=(   4/ 1/0) (
REDTASK #   5 TIME=   7.000 N=  1 (task/type/sgbra)=(   5/ 5/0) (
REDTASK #   6 TIME=   6.000 N=  1 (task/type/sgbra)=(   6/ 6/1) (
REDTASK #   7 TIME=   5.000 N=  1 (task/type/sgbra)=(   7/18/2) (
REDTASK #   8 TIME=   4.000 N=  1 (task/type/sgbra)=(   8/42/1) (
REDTASK #   9 TIME=   3.000 N=  1 (task/type/sgbra)=(   9/43/1) (
REDTASK #  10 TIME=   2.000 N=  1 (task/type/sgbra)=(  10/75/1) (
REDTASK #  11 TIME=   1.000 N=  1 (task/type/sgbra)=(  11/76/1) (
REDTASK #  12 TIME=   0.000 N=  1 (task/type/sgbra)=(  12/77/1) (
 initializing v-file: 1:                  1558

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:          40 2x:           0 4x:           0
All internal counts: zz :          15 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 reference space has dimension       4
 dsyevx: computed roots 1 to    2(converged:   2)

    root           eigenvalues
    ----           ------------
       1         -46.2453833562
       2         -46.2453833457

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=                     4

         vector  1 from unit 11 written to unit 49 filename cirefv              
 ncorel,neli=                     4                     4
  This is a  mraqcc energy evaluation      

    ------------------------------------------------------------

 setopt2: mraqcc energy evaluation      gvalue=  0.833333333333333      ncorel=
                     4

    ------------------------------------------------------------


    reference energies:

  reference state used is:   1with energy      -46.2453833562

  ### begin active excitation selection ###

 inactive(1:nintern)=AAAAAA
 There are          4 reference CSFs out of          4 valid zwalks.
 There are    0 inactive and    6 active orbitals.
       4 active-active  and        0 inactive excitations.

  ### end active excitation selection ###


 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              1558
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                   30
 residual norm convergence criteria:               0.001000

          starting ci iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         205 2x:          90 4x:          35
All internal counts: zz :          15 yy:          72 xx:          66 ww:           0
One-external counts: yz :         112 yx:         360 yw:           0
Two-external counts: yy :         150 ww:           0 xx:         180 xz:          24 wz:           0 wx:           0
Three-ext.   counts: yx :         161 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1
   ht   1    -2.74679570
calca4: root=   1 anorm**2=  0.00000000 scale:  1.00000000

          calcsovref: tciref block   1

              civs   1
 refs   1    1.00000    

          calcsovref: scrb block   1

                ci   1
 civs   1    1.00000    

          calcsovref: sovref block   1

              v      1
 ref    1    1.00000    

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  1  1    -46.2453833562  9.3259E-15  5.5393E-02  2.1660E-01  1.0000E-03
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   2

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         205 2x:          90 4x:          35
All internal counts: zz :          15 yy:          72 xx:          66 ww:           0
One-external counts: yz :         112 yx:         360 yw:           0
Two-external counts: yy :         150 ww:           0 xx:         180 xz:          24 wz:           0 wx:           0
Three-ext.   counts: yx :         161 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2
   ht   1    -2.74679570
   ht   2    -0.05539251    -0.26274818
calca4: root=   1 anorm**2=  0.07224786 scale:  1.03549402
calca4: root=   2 anorm**2=  0.92775214 scale:  1.38843514

          calcsovref: tciref block   1

              civs   1       civs   2
 refs   1    1.00000       8.196454E-13

          calcsovref: scrb block   1

                ci   1         ci   2
 civs   1   0.963199      -0.268790    
 civs   2   0.780477        2.79681    

          calcsovref: sovref block   1

              v      1       v      2
 ref    1   0.963199      -0.268790    

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.96319891    -0.26878962
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  2  1    -46.2902677028  4.4884E-02  1.3509E-03  2.9193E-02  1.0000E-03
 mraqcc  #  2  2    -45.6690126804  2.1704E+00  0.0000E+00  4.8357E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   3

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         205 2x:          90 4x:          35
All internal counts: zz :          15 yy:          72 xx:          66 ww:           0
One-external counts: yz :         112 yx:         360 yw:           0
Two-external counts: yy :         150 ww:           0 xx:         180 xz:          24 wz:           0 wx:           0
Three-ext.   counts: yx :         161 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3
   ht   1    -2.74679570
   ht   2    -0.05539251    -0.26274818
   ht   3     0.00010184     0.01246310    -0.00725391
calca4: root=   1 anorm**2=  0.07124095 scale:  1.03500771
calca4: root=   2 anorm**2=  0.99547026 scale:  1.41261115
calca4: root=   3 anorm**2=  0.93048612 scale:  1.38941935

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3
 refs   1    1.00000       8.196454E-13  -1.185028E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3
 civs   1   0.963847       4.827652E-02   0.262056    
 civs   2   0.802383      -0.372874       -2.87580    
 civs   3    1.10158        17.3433       -7.08736    

          calcsovref: sovref block   1

              v      1       v      2       v      3
 ref    1   0.963716       4.622129E-02   0.262896    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.96371645     0.04622129     0.26289579
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  3  1    -46.2917584116  1.4907E-03  1.3001E-04  9.8268E-03  1.0000E-03
 mraqcc  #  3  2    -45.8824477651  2.1344E-01  0.0000E+00  2.6308E-01  1.0000E-04
 mraqcc  #  3  3    -45.6334189909  2.1348E+00  0.0000E+00  5.0208E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   4

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         205 2x:          90 4x:          35
All internal counts: zz :          15 yy:          72 xx:          66 ww:           0
One-external counts: yz :         112 yx:         360 yw:           0
Two-external counts: yy :         150 ww:           0 xx:         180 xz:          24 wz:           0 wx:           0
Three-ext.   counts: yx :         161 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4
   ht   1    -2.74679570
   ht   2    -0.05539251    -0.26274818
   ht   3     0.00010184     0.01246310    -0.00725391
   ht   4    -0.00037352    -0.00389122    -0.00032962    -0.00064954
calca4: root=   1 anorm**2=  0.07571670 scale:  1.03716763
calca4: root=   2 anorm**2=  0.96165884 scale:  1.40059232
calca4: root=   3 anorm**2=  0.95219534 scale:  1.39720984
calca4: root=   4 anorm**2=  0.92455999 scale:  1.38728512

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4
 refs   1    1.00000       8.196454E-13  -1.185028E-04   1.589922E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4
 civs   1   0.961302      -0.112312       0.179802       0.176269    
 civs   2   0.804925       0.360996       -1.43133       -2.63869    
 civs   3    1.24365        10.9641        11.1364       -10.7414    
 civs   4    1.48959        44.4304       -22.7865        39.6331    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4
 ref    1   0.961392      -0.106547       0.174859       0.183843    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.96139161    -0.10654694     0.17485901     0.18384298
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  4  1    -46.2919521397  1.9373E-04  1.8586E-05  3.6112E-03  1.0000E-03
 mraqcc  #  4  2    -46.0610446889  1.7860E-01  0.0000E+00  6.6193E-02  1.0000E-04
 mraqcc  #  4  3    -45.8144412632  1.8102E-01  0.0000E+00  3.4904E-01  1.0000E-04
 mraqcc  #  4  4    -45.4236451836  1.9251E+00  0.0000E+00  4.8529E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   5

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         205 2x:          90 4x:          35
All internal counts: zz :          15 yy:          72 xx:          66 ww:           0
One-external counts: yz :         112 yx:         360 yw:           0
Two-external counts: yy :         150 ww:           0 xx:         180 xz:          24 wz:           0 wx:           0
Three-ext.   counts: yx :         161 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1    -2.74679570
   ht   2    -0.05539251    -0.26274818
   ht   3     0.00010184     0.01246310    -0.00725391
   ht   4    -0.00037352    -0.00389122    -0.00032962    -0.00064954
   ht   5     0.00091879    -0.00047077    -0.00013035     0.00002992    -0.00025277
calca4: root=   1 anorm**2=  0.07635075 scale:  1.03747325
calca4: root=   2 anorm**2=  0.88208070 scale:  1.37188946
calca4: root=   3 anorm**2=  0.33327976 scale:  1.15467734
calca4: root=   4 anorm**2=  0.95180624 scale:  1.39707059
calca4: root=   5 anorm**2=  0.96134557 scale:  1.40048048

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1    1.00000       8.196454E-13  -1.185028E-04   1.589922E-04  -3.315856E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 civs   1  -0.961191       9.507123E-02   5.837929E-02   0.179822      -0.180190    
 civs   2  -0.805051      -0.308811      -0.121529       -1.43103        2.66388    
 civs   3   -1.25311       -8.76061       -6.39801        11.1327        11.1015    
 civs   4   -1.58940       -38.9413       -22.4915       -22.8042       -39.8323    
 civs   5  -0.699210       -54.0112        84.1664       0.166623       -15.2943    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1  -0.961063       0.107827       2.765313E-02   0.174822      -0.182768    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1    -0.96106286     0.10782737     0.02765313     0.17482196    -0.18276768
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  5  1    -46.2919651359  1.2996E-05  4.2337E-06  1.7721E-03  1.0000E-03
 mraqcc  #  5  2    -46.0954007425  3.4356E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  5  3    -45.9772639744  1.6282E-01  0.0000E+00  4.3846E-01  1.0000E-04
 mraqcc  #  5  4    -45.8144407425  3.9080E-01  0.0000E+00  3.4925E-01  1.0000E-04
 mraqcc  #  5  5    -45.4099970443  1.9114E+00  0.0000E+00  4.6942E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   6

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         205 2x:          90 4x:          35
All internal counts: zz :          15 yy:          72 xx:          66 ww:           0
One-external counts: yz :         112 yx:         360 yw:           0
Two-external counts: yy :         150 ww:           0 xx:         180 xz:          24 wz:           0 wx:           0
Three-ext.   counts: yx :         161 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1    -2.74679570
   ht   2    -0.05539251    -0.26274818
   ht   3     0.00010184     0.01246310    -0.00725391
   ht   4    -0.00037352    -0.00389122    -0.00032962    -0.00064954
   ht   5     0.00091879    -0.00047077    -0.00013035     0.00002992    -0.00025277
   ht   6     0.00045043     0.00003393    -0.00000894     0.00007199    -0.00008953    -0.00005785
calca4: root=   1 anorm**2=  0.07658682 scale:  1.03758702
calca4: root=   2 anorm**2=  0.97815459 scale:  1.40646884
calca4: root=   3 anorm**2=  0.10380565 scale:  1.05062155
calca4: root=   4 anorm**2=  0.98179345 scale:  1.40776186
calca4: root=   5 anorm**2=  0.96730999 scale:  1.40260828
calca4: root=   6 anorm**2=  0.98481095 scale:  1.40883319

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1    1.00000       8.196454E-13  -1.185028E-04   1.589922E-04  -3.315856E-04  -1.641307E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1   0.960992       0.114664      -2.074800E-02  -0.133045      -0.177879       0.122075    
 civs   2   0.805068      -0.292476      -5.497566E-02    1.05717        2.00442       -2.00971    
 civs   3    1.25569       -8.31000       -1.44635       -13.6472        5.91464       -8.52790    
 civs   4    1.61692       -40.7903      -0.637937        10.9869        11.3018        53.3976    
 civs   5   0.892017       -46.3931       -54.0381        46.3999       -103.427       -58.1218    
 civs   6  -0.846550        81.7069       -111.289       -70.5659        213.713        181.663    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.960944       0.111136       1.550614E-02  -0.133485      -0.177565       0.121031    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.96094356     0.11113583     0.01550614    -0.13348477    -0.17756460     0.12103097

 trial vector basis is being transformed.  new dimension:   2
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  6  1    -46.2919687200  3.5841E-06  1.3467E-07  3.2736E-04  1.0000E-03
 mraqcc  #  6  2    -46.1097160549  1.4315E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  6  3    -46.0556854454  7.8421E-02  0.0000E+00  4.3065E-01  1.0000E-04
 mraqcc  #  6  4    -45.8377506840  2.3310E-02  0.0000E+00  2.7348E-01  1.0000E-04
 mraqcc  #  6  5    -45.6115169475  2.0152E-01  0.0000E+00  4.6562E-01  1.0000E-04
 mraqcc  #  6  6    -45.2863989178  1.7878E+00  0.0000E+00  4.3653E-01  1.0000E-04
 
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000999
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mraqcc   convergence criteria satisfied after  6 iterations.

 final mraqcc   convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  6  1    -46.2919687200  3.5841E-06  1.3467E-07  3.2736E-04  1.0000E-03
 mraqcc  #  6  2    -46.1097160549  1.4315E-02  0.0000E+00  0.0000E+00  1.0000E-04

####################CIUDGINFO####################

   aqcc(lrt) vector at position   1 energy=  -46.291968719951

################END OF CIUDGINFO################

 
 a4den factor for root 1  =  1.012929509
diagon:itrnv=   0
    1 of the   3 expansion vectors are transformed.
    1 of the   2 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
maximum overlap with reference    1(overlap= 0.96094)
reference energy =                         -46.2453833562
total aqcc energy =                        -46.2919687200
diagonal element shift (lrtshift) =         -0.0465853638

 information on vector: 1 from unit 11 written to unit 48 filename civout              


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -46.2919687200

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     3    4    5   17   22   31

                                         symmetry   a1   a1   a1   a2   b1   b2 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  5  1       1  0.419351                        +    +    +              +  
 z*  5  1       2 -0.022848                        +    +         +    +       
 z*  5  1       3 -0.864312                        +         +    +    +       
 y   5  1       6 -0.049103              2( b2 )   +    +    +                 
 y   5  1       8  0.034604              4( b2 )   +    +    +                 
 y   5  1      13 -0.014275              2( b1 )   +    +         +            
 y   5  1      19 -0.011358              1( a2 )   +    +              +       
 y   5  1      23  0.043310              2( a1 )   +    +                   +  
 y   5  1      24  0.028800              3( a1 )   +    +                   +  
 y   5  1      27  0.026688              6( a1 )   +    +                   +  
 y   5  1      28 -0.023869              7( a1 )   +    +                   +  
 y   5  1      34  0.085732              2( b1 )   +         +    +            
 y   5  1      36 -0.064808              4( b1 )   +         +    +            
 y   5  1      40 -0.098382              1( a2 )   +         +         +       
 y   5  1      41  0.070109              2( a2 )   +         +         +       
 y   5  1      44  0.010673              2( a1 )   +         +              +  
 y   5  1      45 -0.035055              3( a1 )   +         +              +  
 y   5  1      48  0.015932              6( a1 )   +         +              +  
 y   5  1      49  0.024112              7( a1 )   +         +              +  
 y   5  1      55  0.069138              2( a1 )   +              +    +       
 y   5  1      56  0.065512              3( a1 )   +              +    +       
 y   5  1      59  0.045794              6( a1 )   +              +    +       
 y   5  1      60 -0.051239              7( a1 )   +              +    +       
 y   5  1      69  0.011478              2( b1 )   +                   +    +  
 y   5  1     121 -0.011465              5( a1 )             +    +    +       
 x   5  1     166  0.027966    1( a1 )   1( b2 )   +    +                      
 x   5  1     169  0.020719    4( a1 )   1( b2 )   +    +                      
 x   5  1     188  0.021010    1( a1 )   3( b2 )   +    +                      
 x   5  1     191  0.016332    4( a1 )   3( b2 )   +    +                      
 x   5  1     230 -0.013199   10( a1 )   6( b2 )   +    +                      
 x   5  1     246  0.014728    1( a2 )   2( b1 )   +         +                 
 x   5  1     247 -0.013386    2( a2 )   2( b1 )   +         +                 
 x   5  1     252 -0.013867    1( a2 )   4( b1 )   +         +                 
 x   5  1     253  0.019622    2( a2 )   4( b1 )   +         +                 
 x   5  1     260  0.011657    3( a2 )   6( b1 )   +         +                 
 x   5  1     263  0.019113    3( a2 )   7( b1 )   +         +                 
 x   5  1     264 -0.023913    1( a1 )   1( b2 )   +         +                 
 x   5  1     267 -0.017856    4( a1 )   1( b2 )   +         +                 
 x   5  1     272 -0.011573    9( a1 )   1( b2 )   +         +                 
 x   5  1     286 -0.017873    1( a1 )   3( b2 )   +         +                 
 x   5  1     289 -0.013986    4( a1 )   3( b2 )   +         +                 
 x   5  1     294 -0.012094    9( a1 )   3( b2 )   +         +                 
 x   5  1     338  0.011402    9( a1 )   7( b2 )   +         +                 
 x   5  1     339  0.015865   10( a1 )   7( b2 )   +         +                 
 x   5  1     341  0.016704    1( a1 )   1( b1 )   +              +            
 x   5  1     344  0.012408    4( a1 )   1( b1 )   +              +            
 x   5  1     353 -0.011678    2( a1 )   2( b1 )   +              +            
 x   5  1     363  0.012557    1( a1 )   3( b1 )   +              +            
 x   5  1     375  0.010649    2( a1 )   4( b1 )   +              +            
 x   5  1     379  0.014112    6( a1 )   4( b1 )   +              +            
 x   5  1     380 -0.013044    7( a1 )   4( b1 )   +              +            
 x   5  1     404  0.016729    9( a1 )   6( b1 )   +              +            
 x   5  1     435 -0.010610    3( a2 )   6( b2 )   +              +            
 x   5  1     440  0.011238    2( a1 )   1( a2 )   +                   +       
 x   5  1     441  0.010809    3( a1 )   1( a2 )   +                   +       
 x   5  1     445 -0.011105    7( a1 )   1( a2 )   +                   +       
 x   5  1     451 -0.010375    2( a1 )   2( a2 )   +                   +       
 x   5  1     455 -0.013202    6( a1 )   2( a2 )   +                   +       
 x   5  1     456  0.015946    7( a1 )   2( a2 )   +                   +       
 x   5  1     470  0.012331   10( a1 )   3( a2 )   +                   +       
 x   5  1     472 -0.042113    1( b1 )   1( b2 )   +                   +       
 x   5  1     474 -0.031380    3( b1 )   1( b2 )   +                   +       
 x   5  1     486 -0.031531    1( b1 )   3( b2 )   +                   +       
 x   5  1     488 -0.024624    3( b1 )   3( b2 )   +                   +       
 x   5  1     520 -0.026838    7( b1 )   7( b2 )   +                   +       
 x   5  1     657 -0.010117    5( a1 )   2( b2 )        +    +                 
 x   5  1    1007 -0.013618    9( a1 )   1( b1 )             +    +            
 x   5  1    1008 -0.010274   10( a1 )   1( b1 )             +    +            
 x   5  1    1014  0.019925    5( a1 )   2( b1 )             +    +            
 x   5  1    1029 -0.012626    9( a1 )   3( b1 )             +    +            
 x   5  1    1036 -0.015273    5( a1 )   4( b1 )             +    +            
 x   5  1    1054 -0.018138    1( a1 )   6( b1 )             +    +            
 x   5  1    1057 -0.016555    4( a1 )   6( b1 )             +    +            
 x   5  1    1078  0.013843    3( a2 )   1( b2 )             +    +            
 x   5  1    1084  0.012808    3( a2 )   3( b2 )             +    +            
 x   5  1    1101 -0.020666    5( a1 )   1( a2 )             +         +       
 x   5  1    1112  0.014949    5( a1 )   2( a2 )             +         +       
 x   5  1    1119  0.013694    1( a1 )   3( a2 )             +         +       
 x   5  1    1122  0.012625    4( a1 )   3( a2 )             +         +       
 x   5  1    1136  0.017481    7( b1 )   1( b2 )             +         +       
 x   5  1    1150  0.016078    7( b1 )   3( b2 )             +         +       
 x   5  1    1172  0.016727    1( b1 )   7( b2 )             +         +       
 x   5  1    1174  0.015400    3( b1 )   7( b2 )             +         +       
 x   5  1    1286 -0.015670    2( a1 )   5( a1 )                  +    +       
 x   5  1    1287 -0.013156    3( a1 )   5( a1 )                  +    +       
 x   5  1    1293  0.010681    5( a1 )   6( a1 )                  +    +       
 x   5  1    1298 -0.010612    5( a1 )   7( a1 )                  +    +       
 x   5  1    1315 -0.013081    1( a1 )  10( a1 )                  +    +       
 x   5  1    1318 -0.012000    4( a1 )  10( a1 )                  +    +       
 x   5  1    1352  0.012527    1( b1 )   7( b1 )                  +    +       
 x   5  1    1354  0.011516    3( b1 )   7( b1 )                  +    +       
 x   5  1    1368  0.012645    1( b2 )   6( b2 )                  +    +       
 x   5  1    1370  0.011644    3( b2 )   6( b2 )                  +    +       
 x   5  1    1373  0.013176    1( b2 )   7( b2 )                  +    +       
 x   5  1    1375  0.012150    3( b2 )   7( b2 )                  +    +       

 ci coefficient statistics:
           rq > 0.1                2
      0.1> rq > 0.01              93
     0.01> rq > 0.001            289
    0.001> rq > 0.0001           264
   0.0001> rq > 0.00001           81
  0.00001> rq > 0.000001          25
 0.000001> rq                    804
           all                  1558
  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.419351397747
     2     2     -0.022848031467
     3     3     -0.864312183173
     4     4      0.000008765718

 number of reference csfs (nref) is     4.  root number (iroot) is  1.
 c0**2 =   0.92341318  c0**2(renormalized) =   0.99413446
 c**2 (all zwalks) =   0.92341318  c**2(all zwalks, renormalized) =   0.99413446
 passed aftci ... 
 readint2: molcas,dalton2=                     0                     0
 files%faoints=aoints              
lodens (list->root)=  1
sifcfg setup: record length 4096 DP
# d1 elements per record  3272
# d2 elements per record  2730
  The AQCC density will be calculated.
 item #                     1 suffix=:.drt1.state1:
 read_civout: repnuc=  -43.4985876562748     
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method: 30 last record  1max overlap with ref# 96% root-following 0
 MR-CISD energy:   -46.29196872    -2.79338106
 residuum:     0.00032736
 deltae:     0.00000358
 apxde:     0.00000013
 a4den:     1.01292951
 reference energy:   -46.24538336    -2.74679570

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.96094356     0.11113583     0.01550614    -0.13348477    -0.17756460     0.12103097     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.96094356     0.11113583     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 rdcirefv: extracting reference vector # 1(blocksize=     4 #zcsf=       4)
 =========== Executing IN-CORE method ==========
================================================================================
   DYZ=       0  DYX=       0  DYW=       0
   D0Z=       3  D0Y=       0  D0X=       0  D0W=       0
  DDZI=      16 DDYI=       0 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
 root #                      1 : Scaling(1) with    1.01292950876695      
 corresponding to ref #                      1
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=      16  DYX=      60  DYW=       0
   D0Z=       3  D0Y=      18  D0X=      12  D0W=       0
  DDZI=      16 DDYI=      60 DDXI=      30 DDWI=       0
  DDZE=       0 DDYE=      20 DDXE=      15 DDWE=       0
================================================================================
Trace of MO density:     4.000000
    4  correlated and     8  frozen core electrons

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.99088155     0.00000143     0.00019296     0.00000000    -0.00169598    -0.00136342
   MO   4     0.00000000     0.00000000     0.00000143     0.18830380     0.01718624     0.00000000    -0.00231039     0.01537048
   MO   5     0.00000000     0.00000000     0.00019296     0.01718624     0.96455569     0.00000000     0.07417210     0.06580683
   MO   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00389851     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000    -0.00169598    -0.00231039     0.07417210     0.00000000     0.00791380     0.00613905
   MO   8     0.00000000     0.00000000    -0.00136342     0.01537048     0.06580683     0.00000000     0.00613905     0.00727108
   MO   9     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00313325     0.00000000     0.00000000
   MO  10     0.00000000     0.00000000     0.00141570     0.00000255    -0.00003077     0.00000000    -0.00000735    -0.00000540
   MO  11     0.00000000     0.00000000    -0.00149600    -0.00479543     0.04663765     0.00000000     0.00555026     0.00380877
   MO  12     0.00000000     0.00000000     0.00152645    -0.01037353    -0.05026577     0.00000000    -0.00515797    -0.00586660
   MO  13     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00102216     0.00000000     0.00000000
   MO  14     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00023402     0.00000000     0.00000000
   MO  15     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00052076     0.00000000     0.00000000
   MO  16     0.00000000     0.00000000    -0.00049238     0.00000007    -0.00000533     0.00000000     0.00000017     0.00000014

                MO   9         MO  10         MO  11         MO  12         MO  13         MO  14         MO  15         MO  16
   MO   3     0.00000000     0.00141570    -0.00149600     0.00152645     0.00000000     0.00000000     0.00000000    -0.00049238
   MO   4     0.00000000     0.00000255    -0.00479543    -0.01037353     0.00000000     0.00000000     0.00000000     0.00000007
   MO   5     0.00000000    -0.00003077     0.04663765    -0.05026577     0.00000000     0.00000000     0.00000000    -0.00000533
   MO   6     0.00313325     0.00000000     0.00000000     0.00000000     0.00102216     0.00023402     0.00052076     0.00000000
   MO   7     0.00000000    -0.00000735     0.00555026    -0.00515797     0.00000000     0.00000000     0.00000000     0.00000017
   MO   8     0.00000000    -0.00000540     0.00380877    -0.00586660     0.00000000     0.00000000     0.00000000     0.00000014
   MO   9     0.00262280     0.00000000     0.00000000     0.00000000     0.00091904     0.00019220     0.00060544     0.00000000
   MO  10     0.00000000     0.00257947    -0.00001106     0.00001068     0.00000000     0.00000000     0.00000000    -0.00012535
   MO  11     0.00000000    -0.00001106     0.00414572    -0.00335935     0.00000000     0.00000000     0.00000000     0.00000050
   MO  12     0.00000000     0.00001068    -0.00335935     0.00492149     0.00000000     0.00000000     0.00000000    -0.00000050
   MO  13     0.00091904     0.00000000     0.00000000     0.00000000     0.00036394     0.00006560     0.00033875     0.00000000
   MO  14     0.00019220     0.00000000     0.00000000     0.00000000     0.00006560     0.00187044    -0.00011980     0.00000000
   MO  15     0.00060544     0.00000000     0.00000000     0.00000000     0.00033875    -0.00011980     0.00177296     0.00000000
   MO  16     0.00000000    -0.00012535     0.00000050    -0.00000050     0.00000000     0.00000000     0.00000000     0.00000771

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     2.00000000     0.99089642     0.98013854     0.18968374     0.00690767     0.00610078     0.00258347
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     0.00195136     0.00156758     0.00108433     0.00009989     0.00007033     0.00002127     0.00000216     0.00000144

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4
   MO   1     0.78050501     0.08146391    -0.05606285     0.00000000
   MO   2     0.08146391     0.01127987    -0.00838887     0.00000000
   MO   3    -0.05606285    -0.00838887     0.00647290     0.00000000
   MO   4     0.00000000     0.00000000     0.00000000     0.00208381

Natural orbital populations,block 2
              MO     1       MO     2       MO     3       MO     4       MO
  occ(*)=     0.79311476     0.00507426     0.00208381     0.00006876

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     0.78803002     0.00000000     0.06980515     0.00000000    -0.05102824     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.00462313     0.00000000     0.00370542     0.00000000     0.00120428    -0.00038000
   MO   4     0.00000000     0.06980515     0.00000000     0.00902250     0.00000000    -0.00703443     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.00370542     0.00000000     0.00307703     0.00000000     0.00106640    -0.00043065
   MO   6     0.00000000    -0.05102824     0.00000000    -0.00703443     0.00000000     0.00573475     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000     0.00120428     0.00000000     0.00106640     0.00000000     0.00041457    -0.00023491
   MO   8     0.00000000     0.00000000    -0.00038000     0.00000000    -0.00043065     0.00000000    -0.00023491     0.00164544
   MO   9     0.00000000     0.00000000     0.00051081     0.00000000     0.00046845     0.00000000     0.00019677     0.00088252

                MO   9
   MO   2     0.00000000
   MO   3     0.00051081
   MO   4     0.00000000
   MO   5     0.00046845
   MO   6     0.00000000
   MO   7     0.00019677
   MO   8     0.00088252
   MO   9     0.00258525

Natural orbital populations,block 3
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     0.79757875     0.00810793     0.00509633     0.00309777     0.00102924     0.00011219     0.00010816
              MO     9       MO
  occ(*)=     0.00000234

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     0.18382490     0.00000000     0.01965407     0.00000000    -0.01338774     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.00647291     0.00000000     0.00516417     0.00000000    -0.00166842     0.00088574
   MO   4     0.00000000     0.01965407     0.00000000     0.00276281     0.00000000    -0.00204314     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.00516417     0.00000000     0.00423341     0.00000000    -0.00144132     0.00085569
   MO   6     0.00000000    -0.01338774     0.00000000    -0.00204314     0.00000000     0.00157155     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000    -0.00166842     0.00000000    -0.00144132     0.00000000     0.00054329    -0.00038724
   MO   8     0.00000000     0.00000000     0.00088574     0.00000000     0.00085569     0.00000000    -0.00038724     0.00138294
   MO   9     0.00000000     0.00000000     0.00025556     0.00000000     0.00020031     0.00000000    -0.00006061     0.00061257

                MO   9
   MO   2     0.00000000
   MO   3     0.00025556
   MO   4     0.00000000
   MO   5     0.00020031
   MO   6     0.00000000
   MO   7    -0.00006061
   MO   8     0.00061257
   MO   9     0.00262494

Natural orbital populations,block 4
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     0.18692132     0.01128241     0.00280717     0.00122156     0.00104515     0.00012012     0.00001637
              MO     9       MO
  occ(*)=     0.00000264


 total number of electrons =   12.0000000000

 test slabel:                     4
  a1  a2  b1  b2 


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        a1  partial gross atomic populations
   ao class       1a1        2a1        3a1        4a1        5a1        6a1 
     3_ s       2.000000   0.000000   0.990598   0.000286   0.000000   0.000000
     3_ p       0.000000   2.000000   0.000000   0.000000   0.000000   0.006708
     3_ d       0.000000   0.000000   0.000299   0.979852   0.189684   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000000   0.000000   0.000199
 
   ao class       7a1        8a1        9a1       10a1       11a1       12a1 
     3_ s       0.000000   0.002583   0.000000   0.000000   0.000000   0.000000
     3_ p       0.000000   0.000000   0.000009   0.000068   0.000000   0.000098
     3_ d       0.006101   0.000000   0.000000   0.000000   0.001084   0.000000
     3_ f       0.000000   0.000000   0.001943   0.001499   0.000000   0.000002
 
   ao class      13a1       14a1       15a1       16a1 
     3_ s       0.000000   0.000000   0.000000   0.000001
     3_ p       0.000000   0.000000   0.000002   0.000000
     3_ d       0.000070   0.000021   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000000

                        a2  partial gross atomic populations
   ao class       1a2        2a2        3a2        4a2 
     3_ d       0.793115   0.005074   0.000000   0.000069
     3_ f       0.000000   0.000000   0.002084   0.000000

                        b1  partial gross atomic populations
   ao class       1b1        2b1        3b1        4b1        5b1        6b1 
     3_ p       2.000000   0.000000   0.007950   0.000000   0.000010   0.000047
     3_ d       0.000000   0.797579   0.000000   0.005096   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000158   0.000000   0.003087   0.000983
 
   ao class       7b1        8b1        9b1 
     3_ p       0.000000   0.000105   0.000002
     3_ d       0.000112   0.000000   0.000000
     3_ f       0.000000   0.000003   0.000000

                        b2  partial gross atomic populations
   ao class       1b2        2b2        3b2        4b2        5b2        6b2 
     3_ p       2.000000   0.000000   0.011064   0.000023   0.000000   0.000044
     3_ d       0.000000   0.186921   0.000000   0.000000   0.001222   0.000000
     3_ f       0.000000   0.000000   0.000219   0.002785   0.000000   0.001001
 
   ao class       7b2        8b2        9b2 
     3_ p       0.000116   0.000000   0.000003
     3_ d       0.000000   0.000016   0.000000
     3_ f       0.000004   0.000000   0.000000


                        gross atomic populations
     ao            3_
      s         2.993469
      p         6.026250
      d         2.966316
      f         0.013966
    total      12.000000
 

 Total number of electrons:   12.00000000

 DA ...
