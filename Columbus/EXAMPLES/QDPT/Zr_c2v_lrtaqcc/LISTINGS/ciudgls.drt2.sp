1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      2009-03.    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


================ Computing sorting integral file structure ================

                    -----z----- -----y----- -----x----- -----w----- ---total---

                CSFs        24         482        1430        2034        3970
      internal walks       105          70          15          21         211
valid internal walks        24          70          15          21         130
 getinfoarray: info=                     6 :                     1
                  8192                  6552                  8192
                  5460                     0
 icd(3)=                  2976 ci%nnlev=                   595  l2rec=
                  8192  n2max=                  5460
 lcore1,lcore2=              13068137              13044821
 lencor,maxblo              13107200                 60000
========================================
 current settings:
 minbl3         381
 minbl4         381
 locmaxbl3     4288
 locmaxbuf     2144
 maxbl3       60000
 maxbl3       60000
 maxbl4       60000
 maxbuf       30006
========================================

 sorted 4-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)

 sorted 3-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)
 Orig.  diagonal integrals:  1electron:        34
                             0ext.    :        42
                             2ext.    :       336
                             4ext.    :       812


 Orig. off-diag. integrals:  4ext.    :     24411
                             3ext.    :     18720
                             2ext.    :      6792
                             1ext.    :      1272
                             0ext.    :        99
                             2ext. SO :         0
                             1ext. SO :         0
                             0ext. SO :         0
                             1electron:       128


 Sorted integrals            3ext.  w :     17370 x :     16020
                             4ext.  w :     20955 x :     17777


Cycle #  1 sortfile size=    163835(       5 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13068137
Cycle #  2 sortfile size=    163835(       5 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13068137
 minimum size of srtscr:     98301 WP (     3 records)
 maximum size of srtscr:    163835 WP (     5 records)
diagi   file:      4 records  of   1536 WP each=>       6144 WP total
ofdgi   file:      8 records  of   1536 WP each=>      12288 WP total
fil3w   file:      1 records  of  30006 WP each=>      30006 WP total
fil3x   file:      1 records  of  30006 WP each=>      30006 WP total
fil4w   file:      1 records  of  30006 WP each=>      30006 WP total
fil4x   file:      1 records  of  30006 WP each=>      30006 WP total
 compressed index vector length=                    61
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 3,
  GSET = 3,
   DAVCOR =10,
  NCOREL = 4
   nroot=1
  IVMODE = 3
  NBKITR = 1
   nvbkmn=2
  RTOLBK = 1e-3
   niter=30
   nvcimn=2
   rtolci=1e-3
  NVCIMX = 6
  NVRFMX = 6
  NVBKMX = 6
  IDEN  = 1
  CSFPRN = 10,
  update_mode=1
  nvrfmn=2
   lrtshift=-0.0465482970
 /&end
 ------------------------------------------------------------------------
lodens (list->root)=  1
invlodens (root->list)=  1
 LRT calculation: diagonal shift= -4.654829700000000E-002
 bummer (warning):2:changed keyword: nbkitr=                      0
 USING SEGMENTS OF EQUAL SIZE

****************  list of control variables  ****************
 lvlprt =    0      nroot  =    1      noldv  =   0      noldhv =   0
 nunitv =    1      nbkitr =    0      niter  =  30      davcor =  10
 csfprn =   10      ivmode =    3      istrt  =   0      vout   =   0
 iortls =    0      nvbkmx =    6      ibktv  =  -1      ibkthv =  -1
 nvcimx =    6      icitv  =   -1      icithv =  -1      frcsub =   0
 nvbkmn =    2      nvcimn =    2      maxseg =   4      nrfitr =  30
 ncorel =    4      nvrfmx =    6      nvrfmn =   2      iden   =   1
 itran  =    0      froot  =    0      rtmode =   0      ncouple=   1
 skipso =    F      dalton2=    0      molcas =   0      finalv =   0
 finalw =    0      cosmocalc=   0    with_tsklst=   0
 nsegwx =    1     1     1     1
 nseg0x =    1     1     1     1
 nseg1x =    1     1     1     1
 nseg2x =    1     1     1     1
 nseg3x =    1     1     1     1
 nseg4x =    1     1     1     1
 no0ex  =      0    no1ex  =      0    no2ex  =     0    no3ex  =     0
 no4ex  =      0    nodiag =      0
 cdg4ex =    1      c3ex1ex=    1      c2ex0ex=   1
 fileloc=    0     0     0     0     0     0     0     1     1     1
 directhd=   1      noaqccshift_zyxw=      0
 critical_crit=-1.00000    critical_delta= 0.05000

 ctol   = 0.010000    lrtshift=-.046548    smalld =0.001000


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-03    1.000E-03
 Computing density:                    .drt1.state1
 using                      1  nodes and                      1  cores.
 szdg/szodg per processor=                  6144                 12288
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core           13107199 DP per process

********** Integral sort section *************


 workspace allocation information: lencor=  13107199

 echo of the input for program cisrt:
 ------------------------------------------------------------------------
  &input
  maxbl3=60000
  maxbl4=60000
  &end
 ------------------------------------------------------------------------
 
 ( 6) listing file:                    ciudgls             
 ( 5) input file:                      cisrtin   
 (17) cidrt file:                      cidrtfl             
 (11) transformed integrals file:      moints    
 (12) diagonal integral file:          diagint             
 (13) off-diagonal integral file:      ofdgint             
 (31) 4-external w integrals file:     fil4w               
 (32) 4-external x integrals file:     fil4x               
 (33) 3-external w integrals file:     fil3w               
 (34) 3-external x integrals file:     fil3x               
 (21) scratch da sorting file:         srtscr              
 (12) 2-e integral file [fsplit=2]:    moints2   

 input integral file header information:
                                                                                 
 aoints SIFS file created by argos.      zam792            17:56:38.564 17-Dec-13
  cidrt_title                                                                    
 MO-coefficients from mcscf.x                                                    
  with dummy occupation 1.0 for active orbitals                                  
  total ao core energy =    0.000000000                                          
 MCSCF energy =     -46.301878576                                                
 SIFS file created by program tran.      zam792            17:56:38.888 17-Dec-13

 input energy(*) values:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total core energy =   0.000000000000E+00

 nsym = 4 nmot=  34

 symmetry  =    1    2    3    4
 slabel(*) =   a1   a2   b1   b2
 nmpsy(*)  =   14    4    8    8

 info(*) =          1      8192      6552      8192      5460         0

 orbital labels, i:molab(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012  13:tout:013  14:tout:014  15:tout:015  16:tout:016  17:tout:017  18:tout:018  19:tout:019  20:tout:020
  21:tout:021  22:tout:022  23:tout:023  24:tout:024  25:tout:025  26:tout:026  27:tout:027  28:tout:028  29:tout:029  30:tout:030
  31:tout:031  32:tout:032  33:tout:033  34:tout:034

 input parameters:
 prnopt=  0
 ldamin=    4095 ldamax=   32767 ldainc=      64
 maxbuf=   30006 maxbl3=   60000 maxbl4=   60000 intmxo=    1536
  Using 32 bit compression 

 drt information:
  cidrt_title                                                                    
 nmotd =  38 nfctd =   4 nfvtc =   0 nmot  =  34
 nlevel =  34 niot  =   6 lowinl=  29
 orbital-to-level map(*)
   -1  -1  29  30  31   1   2   3   4   5   6   7   8   9  10  11  32  12  13  14
   -1  33  15  16  17  18  19  20  21  -1  34  22  23  24  25  26  27  28
 compressed map(*)
   29  30  31   1   2   3   4   5   6   7   8   9  10  11  32  12  13  14  33  15
   16  17  18  19  20  21  34  22  23  24  25  26  27  28
 levsym(*)
    1   1   1   1   1   1   1   1   1   1   1   2   2   2   3   3   3   3   3   3
    3   4   4   4   4   4   4   4   1   1   1   2   3   4
 repartitioning mu(*)=
   0.  0.  0.  0.  0.  0.

 new core energy added to the energy(*) list.
 from the integral file: h1_core= -4.349858765627E+01

 indxdg: diagonal integral statistics.
 total number of integrals contributing to diagonal matrix elements:      1190
 number with all external indices:       812
 number with half external - half internal indices:       336
 number with all internal indices:        42

 indxof: off-diagonal integral statistics.
    4-external integrals: num=      24411 strt=          1
    3-external integrals: num=      18720 strt=      24412
    2-external integrals: num=       6792 strt=      43132
    1-external integrals: num=       1272 strt=      49924
    0-external integrals: num=         99 strt=      51196

 total number of off-diagonal integrals:       51294


 indxof(2nd)  ittp=   3 numx(ittp)=        6792
 indxof(2nd)  ittp=   4 numx(ittp)=        1272
 indxof(2nd)  ittp=   5 numx(ittp)=          99

 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg=  12931980
 pro2e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652

 pro2e:     25126 integrals read in     5 records.

 pro2e:         0 integrals 34-ext integrals skipped.
 pro1e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652
 pro1e: eref =    0.000000000000000E+00
 total size of srtscr:                     4  records of                  32767 
 WP =               1048544 Bytes
 putdg        1     596    1191    1786    3322   36089   57934    3018   46706   90394
   123161  131353  136813  158652

 putf:       4 buffers of length    1536 written to file 12
 diagonal integral file completed.

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep4e:    17 blocks of linear combinations of 4-external integrals processed.
 number of sorted 4-external integrals      38732
 number of original 4-external integrals    24411


 putf34: external integral file complete. nfilw=    31 nfilx=    32 nrecw=     1 nrecx=     1 lbufp= 30006

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep3e:    19 blocks of linear combinations of 3-external integrals processed.
 number of sorted 3-external integrals      33390
 number of original 3-external integrals    18720


 putf34: external integral file complete. nfilw=    33 nfilx=    34 nrecw=     1 nrecx=     1 lbufp= 30006
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      6792         3     43132     43132      6792     51294
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      1272         4     49924     49924      1272     51294
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd        99         5     51196     51196        99     51294

 putf:       8 buffers of length    1536 written to file 13
 off-diagonal files sort completed.
 executing brd_struct for cisrtinfo
cisrtinfo:
bufszi  1536
 diagfile 4ext:     812 2ext:     336 0ext:      42
 fil4w,fil4x  :   24411 fil3w,fil3x :   18720
 ofdgint  2ext:    6792 1ext:    1272 0ext:      99so0ext:       0so1ext:       0so2ext:       0
buffer minbl4     381 minbl3     381 maxbl2     384nbas:  11   3   7   7   0   0   0   0 maxbuf 30006
 CIUDG version 5.9.7 ( 5-Oct-2004)

 workspace allocation information: lcore=  13107199

 core energy values from the integral file:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -4.349858765627E+01, ietype=    6,   fcore energy of type: H1(*)   

 total core repulsion energy = -4.349858765627E+01
 nmot  =    38 niot  =     6 nfct  =     4 nfvt  =     0
 nrow  =    32 nsym  =     4 ssym  =     2 lenbuf=  1600
 nwalk,xbar:        211      105       70       15       21
 nvalwt,nvalw:      130       24       70       15       21
 ncsft:            3970
 total number of valid internal walks:     130
 nvalz,nvaly,nvalx,nvalw =       24      70      15      21

 cisrt info file parameters:
 file number  12 blocksize   1536
 mxbld   1536
 nd4ext,nd2ext,nd0ext   812   336    42
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int    24411    18720     6792     1272       99        0        0        0
 minbl4,minbl3,maxbl2   381   381   384
 maxbuf 30006
 number of external orbitals per symmetry block:  11   3   7   7
 nmsym   4 number of internal orbitals   6
 executing brd_struct for drt
 executing brd_struct for orbinf
 executing brd_struct for momap
 calcthrxt: niot,maxw1=                     6                    31
 block size     0
 pthz,pthy,pthx,pthw:   105    70    15    21 total internal walks:     211
 maxlp3,n2lp,n1lp,n0lp    31     0     0     0
 orbsym(*)= 1 1 1 2 3 4

 setref:       24 references kept,
               81 references were marked as invalid, out of
              105 total.
 nmb.of records onel     1
 nmb.of records 2-ext     5
 nmb.of records 1-ext     1
 nmb.of records 0-ext     1
 nmb.of records 2-int     0
 nmb.of records 1-int     0
 nmb.of records 0-int     0
 ---------memory usage in DP -----------------
 < n-ex core usage >
     routines:
    fourex            61797
    threx             60199
    twoex              3549
    onex               1823
    allin              1536
    diagon             2109
               =======
   maximum            61797
 
  __ static summary __ 
   reflst                24
   hrfspc                24
               -------
   static->              48
 
  __ core required  __ 
   totstc                48
   max n-ex           61797
               -------
   totnec->           61845
 
  __ core available __ 
   totspc          13107199
   totnec -           61845
               -------
   totvec->        13045354

 number of external paths / symmetry
 vertex x     100      82      98      98
 vertex w     128      82      98      98
segment: free space=    13045354
 reducing frespc by                   782 to               13044572 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1          40|        24|         0|        24|         0|         1|
 -------------------------------------------------------------------------------
  Y 2          70|       482|        24|        70|        24|         2|
 -------------------------------------------------------------------------------
  X 3          15|      1430|       506|        15|        94|         3|
 -------------------------------------------------------------------------------
  W 4          21|      2034|      1936|        21|       109|         4|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=          68DP  conft+indsym=         280DP  drtbuffer=         434 DP

dimension of the ci-matrix ->>>      3970

 executing brd_struct for civct
 gentasklist: ntask=                    20
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS   
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      15      40       1430         24      15      24
     2  4   1    25      two-ext wz   2X  4 1      21      40       2034         24      21      24
     3  4   3    26      two-ext wx*  WX  4 3      21      15       2034       1430      21      15
     4  4   3    27      two-ext wx+  WX  4 3      21      15       2034       1430      21      15
     5  2   1    11      one-ext yz   1X  2 1      70      40        482         24      70      24
     6  3   2    15      1ex3ex yx    3X  3 2      15      70       1430        482      15      70
     7  4   2    16      1ex3ex yw    3X  4 2      21      70       2034        482      21      70
     8  1   1     1      allint zz    OX  1 1      40      40         24         24      24      24
     9  2   2     5      0ex2ex yy    OX  2 2      70      70        482        482      70      70
    10  3   3     6      0ex2ex xx*   OX  3 3      15      15       1430       1430      15      15
    11  3   3    18      0ex2ex xx+   OX  3 3      15      15       1430       1430      15      15
    12  4   4     7      0ex2ex ww*   OX  4 4      21      21       2034       2034      21      21
    13  4   4    19      0ex2ex ww+   OX  4 4      21      21       2034       2034      21      21
    14  2   2    42      four-ext y   4X  2 2      70      70        482        482      70      70
    15  3   3    43      four-ext x   4X  3 3      15      15       1430       1430      15      15
    16  4   4    44      four-ext w   4X  4 4      21      21       2034       2034      21      21
    17  1   1    75      dg-024ext z  OX  1 1      40      40         24         24      24      24
    18  2   2    76      dg-024ext y  OX  2 2      70      70        482        482      70      70
    19  3   3    77      dg-024ext x  OX  3 3      15      15       1430       1430      15      15
    20  4   4    78      dg-024ext w  OX  4 4      21      21       2034       2034      21      21
----------------------------------------------------------------------------------------------------
REDTASK #   1 TIME=  19.000 N=  1 (task/type/sgbra)=(   1/24/0) (
REDTASK #   2 TIME=  18.000 N=  1 (task/type/sgbra)=(   2/25/0) (
REDTASK #   3 TIME=  17.000 N=  1 (task/type/sgbra)=(   3/26/1) (
REDTASK #   4 TIME=  16.000 N=  1 (task/type/sgbra)=(   4/27/2) (
REDTASK #   5 TIME=  15.000 N=  1 (task/type/sgbra)=(   5/11/0) (
REDTASK #   6 TIME=  14.000 N=  1 (task/type/sgbra)=(   6/15/0) (
REDTASK #   7 TIME=  13.000 N=  1 (task/type/sgbra)=(   7/16/0) (
REDTASK #   8 TIME=  12.000 N=  1 (task/type/sgbra)=(   8/ 1/0) (
REDTASK #   9 TIME=  11.000 N=  1 (task/type/sgbra)=(   9/ 5/0) (
REDTASK #  10 TIME=  10.000 N=  1 (task/type/sgbra)=(  10/ 6/1) (
REDTASK #  11 TIME=   9.000 N=  1 (task/type/sgbra)=(  11/18/2) (
REDTASK #  12 TIME=   8.000 N=  1 (task/type/sgbra)=(  12/ 7/1) (
REDTASK #  13 TIME=   7.000 N=  1 (task/type/sgbra)=(  13/19/2) (
REDTASK #  14 TIME=   6.000 N=  1 (task/type/sgbra)=(  14/42/1) (
REDTASK #  15 TIME=   5.000 N=  1 (task/type/sgbra)=(  15/43/1) (
REDTASK #  16 TIME=   4.000 N=  1 (task/type/sgbra)=(  16/44/1) (
REDTASK #  17 TIME=   3.000 N=  1 (task/type/sgbra)=(  17/75/1) (
REDTASK #  18 TIME=   2.000 N=  1 (task/type/sgbra)=(  18/76/1) (
REDTASK #  19 TIME=   1.000 N=  1 (task/type/sgbra)=(  19/77/1) (
REDTASK #  20 TIME=   0.000 N=  1 (task/type/sgbra)=(  20/78/1) (
 initializing v-file: 1:                  3970

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         176 2x:           0 4x:           0
All internal counts: zz :         271 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 reference space has dimension      24
 dsyevx: computed roots 1 to    2(converged:   2)

    root           eigenvalues
    ----           ------------
       1         -46.2740792789
       2         -46.2533198520

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=                    24

         vector  1 from unit 11 written to unit 49 filename cirefv              
 ncorel,neli=                     4                     4
  This is a  mraqcc energy evaluation      

    ------------------------------------------------------------

 setopt2: mraqcc energy evaluation      gvalue=  0.833333333333333      ncorel=
                     4

    ------------------------------------------------------------


    reference energies:

  reference state used is:   1with energy      -46.2740792789

  ### begin active excitation selection ###

 inactive(1:nintern)=AAAAAA
 There are         24 reference CSFs out of         24 valid zwalks.
 There are    0 inactive and    6 active orbitals.
      24 active-active  and        0 inactive excitations.

  ### end active excitation selection ###


 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              3970
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                   30
 residual norm convergence criteria:               0.001000

          starting ci iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         602 2x:         246 4x:         106
All internal counts: zz :         271 yy:         620 xx:          66 ww:         132
One-external counts: yz :        1062 yx:        1000 yw:        1210
Two-external counts: yy :         865 ww:         252 xx:         180 xz:          72 wz:         104 wx:         360
Three-ext.   counts: yx :         320 yw:         395

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1
   ht   1    -2.77549162
calca4: root=   1 anorm**2=  0.00000000 scale:  1.00000000

          calcsovref: tciref block   1

              civs   1
 refs   1    1.00000    

          calcsovref: scrb block   1

                ci   1
 civs   1    1.00000    

          calcsovref: sovref block   1

              v      1
 ref    1    1.00000    

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  1  1    -46.2740792789 -3.9968E-15  5.7760E-02  2.2790E-01  1.0000E-03
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   2

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         602 2x:         246 4x:         106
All internal counts: zz :         271 yy:         620 xx:          66 ww:         132
One-external counts: yz :        1062 yx:        1000 yw:        1210
Two-external counts: yy :         865 ww:         252 xx:         180 xz:          72 wz:         104 wx:         360
Three-ext.   counts: yx :         320 yw:         395

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2
   ht   1    -2.77549162
   ht   2    -0.05776026    -0.19278773
calca4: root=   1 anorm**2=  0.06313871 scale:  1.03108618
calca4: root=   2 anorm**2=  0.93686129 scale:  1.39171164

          calcsovref: tciref block   1

              civs   1       civs   2
 refs   1    1.00000       6.067981E-15

          calcsovref: scrb block   1

                ci   1         ci   2
 civs   1   0.967916      -0.251274    
 civs   2   0.827724        3.18842    

          calcsovref: sovref block   1

              v      1       v      2
 ref    1   0.967916      -0.251274    

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.96791595    -0.25127418
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  2  1    -46.3234735939  4.9394E-02  2.5628E-03  4.5966E-02  1.0000E-03
 mraqcc  #  2  2    -45.5411593387  2.0426E+00  0.0000E+00  5.0008E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   3

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         602 2x:         246 4x:         106
All internal counts: zz :         271 yy:         620 xx:          66 ww:         132
One-external counts: yz :        1062 yx:        1000 yw:        1210
Two-external counts: yy :         865 ww:         252 xx:         180 xz:          72 wz:         104 wx:         360
Three-ext.   counts: yx :         320 yw:         395

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3
   ht   1    -2.77549162
   ht   2    -0.05776026    -0.19278773
   ht   3     0.00717856    -0.00285375    -0.00924194
calca4: root=   1 anorm**2=  0.07361156 scale:  1.03615228
calca4: root=   2 anorm**2=  0.93425702 scale:  1.39077569
calca4: root=   3 anorm**2=  0.95214057 scale:  1.39719024

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3
 refs   1    1.00000       6.067981E-15  -2.586984E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3
 civs   1   0.965085      -0.156405       0.213897    
 civs   2   0.867439        1.58157       -2.75633    
 civs   3    1.03884        13.2614        7.90587    

          calcsovref: sovref block   1

              v      1       v      2       v      3
 ref    1   0.962397      -0.190712       0.193444    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.96239711    -0.19071218     0.19344420
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  3  1    -46.3261421365  2.6685E-03  4.1059E-04  1.7821E-02  1.0000E-03
 mraqcc  #  3  2    -45.7949646845  2.5381E-01  0.0000E+00  4.1163E-01  1.0000E-04
 mraqcc  #  3  3    -45.4511353584  1.9525E+00  0.0000E+00  4.9931E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   4

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         602 2x:         246 4x:         106
All internal counts: zz :         271 yy:         620 xx:          66 ww:         132
One-external counts: yz :        1062 yx:        1000 yw:        1210
Two-external counts: yy :         865 ww:         252 xx:         180 xz:          72 wz:         104 wx:         360
Three-ext.   counts: yx :         320 yw:         395

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4
   ht   1    -2.77549162
   ht   2    -0.05776026    -0.19278773
   ht   3     0.00717856    -0.00285375    -0.00924194
   ht   4    -0.00058470    -0.00354303    -0.00050513    -0.00156865
calca4: root=   1 anorm**2=  0.07821272 scale:  1.03837022
calca4: root=   2 anorm**2=  0.94647753 scale:  1.39516219
calca4: root=   3 anorm**2=  0.95542284 scale:  1.39836434
calca4: root=   4 anorm**2=  0.75854112 scale:  1.32609997

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4
 refs   1    1.00000       6.067981E-15  -2.586984E-03   2.336073E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4
 civs   1  -0.962903       0.156238       0.221908       2.929441E-02
 civs   2  -0.872166       -1.03212       -2.92369      -0.848014    
 civs   3   -1.17546       -11.0855        6.62651       -8.44941    
 civs   4  -0.861894       -19.7182        4.44857        31.0325    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4
 ref    1  -0.960064       0.180310       0.205804       5.840233E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1    -0.96006367     0.18030956     0.20580421     0.05840233
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  4  1    -46.3264961355  3.5400E-04  8.0065E-05  8.2915E-03  1.0000E-03
 mraqcc  #  4  2    -45.9503161582  1.5535E-01  0.0000E+00  2.4519E-01  1.0000E-04
 mraqcc  #  4  3    -45.4522020517  1.0667E-03  0.0000E+00  5.0386E-01  1.0000E-04
 mraqcc  #  4  4    -45.4013211242  1.9027E+00  0.0000E+00  5.5053E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   5

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         602 2x:         246 4x:         106
All internal counts: zz :         271 yy:         620 xx:          66 ww:         132
One-external counts: yz :        1062 yx:        1000 yw:        1210
Two-external counts: yy :         865 ww:         252 xx:         180 xz:          72 wz:         104 wx:         360
Three-ext.   counts: yx :         320 yw:         395

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1    -2.77549162
   ht   2    -0.05776026    -0.19278773
   ht   3     0.00717856    -0.00285375    -0.00924194
   ht   4    -0.00058470    -0.00354303    -0.00050513    -0.00156865
   ht   5    -0.00403960     0.00081034     0.00044301    -0.00002594    -0.00030884
calca4: root=   1 anorm**2=  0.07980988 scale:  1.03913901
calca4: root=   2 anorm**2=  0.93962260 scale:  1.39270334
calca4: root=   3 anorm**2=  0.85840734 scale:  1.36323415
calca4: root=   4 anorm**2=  0.96742254 scale:  1.40264840
calca4: root=   5 anorm**2=  0.51986682 scale:  1.23282879

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1    1.00000       6.067981E-15  -2.586984E-03   2.336073E-04   1.449601E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 civs   1  -0.963221       0.109630       0.272821       1.766902E-02  -4.987527E-03
 civs   2  -0.872840      -0.775512       -2.86141      -0.886013      -0.941470    
 civs   3   -1.20020       -9.23574      -0.416003        12.3630       -3.13092    
 civs   4   -1.01887       -20.7865        6.95731       -10.6667        28.4143    
 civs   5   0.806212        30.1498       -47.2678        52.0007        36.7404    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1  -0.959185       0.172372       0.207003       5.857458E-02   6.300886E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1    -0.95918535     0.17237200     0.20700303     0.05857458     0.06300886
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  5  1    -46.3265606889  6.4553E-05  7.7667E-06  2.5511E-03  1.0000E-03
 mraqcc  #  5  2    -46.0246482214  7.4332E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  5  3    -45.4698043479  1.7602E-02  0.0000E+00  5.4118E-01  1.0000E-04
 mraqcc  #  5  4    -45.4267406600  2.5420E-02  0.0000E+00  5.3887E-01  1.0000E-04
 mraqcc  #  5  5    -45.3917109404  1.8931E+00  0.0000E+00  6.1152E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   6

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         602 2x:         246 4x:         106
All internal counts: zz :         271 yy:         620 xx:          66 ww:         132
One-external counts: yz :        1062 yx:        1000 yw:        1210
Two-external counts: yy :         865 ww:         252 xx:         180 xz:          72 wz:         104 wx:         360
Three-ext.   counts: yx :         320 yw:         395

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1    -2.77549162
   ht   2    -0.05776026    -0.19278773
   ht   3     0.00717856    -0.00285375    -0.00924194
   ht   4    -0.00058470    -0.00354303    -0.00050513    -0.00156865
   ht   5    -0.00403960     0.00081034     0.00044301    -0.00002594    -0.00030884
   ht   6     0.00082038    -0.00015663     0.00004156     0.00002419     0.00000956    -0.00003083
calca4: root=   1 anorm**2=  0.07998270 scale:  1.03922216
calca4: root=   2 anorm**2=  0.96110358 scale:  1.40039408
calca4: root=   3 anorm**2=  0.87702683 scale:  1.37004629
calca4: root=   4 anorm**2=  0.91028292 scale:  1.38212985
calca4: root=   5 anorm**2=  0.66319434 scale:  1.28964892
calca4: root=   6 anorm**2=  0.70620169 scale:  1.30621656

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1    1.00000       6.067981E-15  -2.586984E-03   2.336073E-04   1.449601E-03  -2.944416E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1  -0.962998       0.111563      -7.232106E-02   0.198105      -0.171594       7.178328E-02
 civs   2  -0.872883      -0.657348        1.19753       -2.32579       0.909843       -1.54480    
 civs   3   -1.20299       -8.47985        4.49304        9.77473        7.07489       -3.52411    
 civs   4   -1.03671       -20.1902        1.82082       -6.40112       -5.29990        30.4708    
 civs   5   0.897898        34.2273        21.9820        3.80429        72.4648        20.5031    
 civs   6   0.945424        65.3796        239.334        60.0930       -76.3995        51.7899    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.959105       0.159149      -0.122124       0.159143      -6.359435E-02   0.102491    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.95910481     0.15914946    -0.12212373     0.15914348    -0.06359435     0.10249060

 trial vector basis is being transformed.  new dimension:   2
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  6  1    -46.3265680318  7.3429E-06  8.0366E-07  8.3703E-04  1.0000E-03
 mraqcc  #  6  2    -46.0456988278  2.1051E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  6  3    -45.7118389894  2.4203E-01  0.0000E+00  4.5596E-01  1.0000E-04
 mraqcc  #  6  4    -45.4318034269  5.0628E-03  0.0000E+00  5.1007E-01  1.0000E-04
 mraqcc  #  6  5    -45.4199140632  2.8203E-02  0.0000E+00  6.3577E-01  1.0000E-04
 mraqcc  #  6  6    -45.3861519613  1.8876E+00  0.0000E+00  5.6643E-01  1.0000E-04
 
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mraqcc   convergence criteria satisfied after  6 iterations.

 final mraqcc   convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  6  1    -46.3265680318  7.3429E-06  8.0366E-07  8.3703E-04  1.0000E-03
 mraqcc  #  6  2    -46.0456988278  2.1051E-02  0.0000E+00  0.0000E+00  1.0000E-04

####################CIUDGINFO####################

   aqcc(lrt) vector at position   1 energy=  -46.326568031798

################END OF CIUDGINFO################

 
 a4den factor for root 1  =  1.013510552
diagon:itrnv=   0
    1 of the   3 expansion vectors are transformed.
    1 of the   2 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
maximum overlap with reference    1(overlap= 0.95910)
reference energy =                         -46.2740792789
total aqcc energy =                        -46.3265680318
diagonal element shift (lrtshift) =         -0.0524887529

 information on vector: 1 from unit 11 written to unit 48 filename civout              


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -46.3265680318

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     3    4    5   17   22   31

                                         symmetry   a1   a1   a1   a2   b1   b2 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1 -0.467981                        +-   +          -           
 z*  1  1       2  0.510788                        +-        +     -           
 z*  1  1       3  0.600650                        +-                  +     - 
 z*  1  1       4  0.022618                        +    +-         -           
 z*  1  1       5 -0.074346                        +     -   +     -           
 z*  1  1       6 -0.087310                        +     -             +     - 
 z*  1  1       7 -0.128727                        +    +     -    -           
 z*  1  1       8  0.095400                        +    +               -    - 
 z*  1  1       9  0.041031                        +         +-    -           
 z*  1  1      10  0.095285                        +          -        +     - 
 z*  1  1      11  0.087400                        +         +          -    - 
 z*  1  1      12 -0.084680                        +               -   +-      
 z*  1  1      13 -0.084681                        +               -        +- 
 z*  1  1      14 -0.033544                             +-   +     -           
 z*  1  1      15 -0.028766                             +-             +     - 
 z*  1  1      16 -0.030745                             +    +-    -           
 z*  1  1      17 -0.036384                             +     -        +     - 
 z*  1  1      18 -0.018299                             +    +          -    - 
 z*  1  1      20  0.025145                             +          -        +- 
 z*  1  1      21 -0.024255                                  +-        +     - 
 z*  1  1      22 -0.025771                                  +     -   +-      
 z*  1  1      24 -0.026488                                       +-   +     - 
 y   1  1      25  0.013524              1( a2 )   +-    -                     
 y   1  1      26 -0.010479              2( a2 )   +-    -                     
 y   1  1      28 -0.014763              1( a2 )   +-         -                
 y   1  1      29  0.011438              2( a2 )   +-         -                
 y   1  1      32 -0.019900              2( a1 )   +-              -           
 y   1  1      35 -0.059699              5( a1 )   +-              -           
 y   1  1      36 -0.015472              6( a1 )   +-              -           
 y   1  1      43 -0.017368              2( b2 )   +-                   -      
 y   1  1      45  0.013448              4( b2 )   +-                   -      
 y   1  1      50 -0.017369              2( b1 )   +-                        - 
 y   1  1      52  0.013449              4( b1 )   +-                        - 
 y   1  1      59  0.016521              1( a2 )    -   +     -                
 y   1  1      60 -0.016306              2( a2 )    -   +     -                
 y   1  1      66  0.015834              5( a1 )    -   +          -           
 y   1  1      81  0.014037              2( b1 )    -   +                    - 
 y   1  1      83 -0.013852              4( b1 )    -   +                    - 
 y   1  1      94 -0.017280              5( a1 )    -        +     -           
 y   1  1     102 -0.013708              2( b2 )    -        +          -      
 y   1  1     104  0.013529              4( b2 )    -        +          -      
 y   1  1     133  0.010396              2( a2 )    -                  +-      
 y   1  1     136  0.014287              2( a1 )    -                  +     - 
 y   1  1     139 -0.020326              5( a1 )    -                  +     - 
 y   1  1     140  0.014169              6( a1 )    -                  +     - 
 y   1  1     147  0.010397              2( a2 )    -                       +- 
 y   1  1     157  0.011492              6( a1 )   +     -         -           
 y   1  1     166 -0.011910              4( b2 )   +     -              -      
 y   1  1     182 -0.012844              6( a1 )   +          -    -           
 y   1  1     198  0.012193              4( b1 )   +          -              - 
 y   1  1     205 -0.011023              4( b1 )   +               -    -      
 y   1  1     212 -0.011023              4( b2 )   +               -         - 
 y   1  1     222  0.012483              7( a1 )   +                    -    - 
 y   1  1     260 -0.010747              3( a1 )         -   +     -           
 y   1  1     264  0.012612              7( a1 )         -   +     -           
 y   1  1     305 -0.010758              3( a1 )         -             +     - 
 y   1  1     309  0.012692              7( a1 )         -             +     - 
 y   1  1     418  0.011808              7( a1 )              -        +     - 
 y   1  1     477  0.011205              2( a2 )                   -   +     - 
 x   1  1     511  0.011494    5( a1 )   1( a2 )    -    -                     
 x   1  1     522 -0.011534    5( a1 )   2( a2 )    -    -                     
 x   1  1     546 -0.010673    7( b1 )   1( b2 )    -    -                     
 x   1  1     560 -0.010861    7( b1 )   3( b2 )    -    -                     
 x   1  1     593 -0.012546    5( a1 )   1( a2 )    -         -                
 x   1  1     604  0.012589    5( a1 )   2( a2 )    -         -                
 x   1  1     642  0.010045    7( b1 )   3( b2 )    -         -                
 x   1  1     664  0.011328    1( b1 )   7( b2 )    -         -                
 x   1  1     666  0.011576    3( b1 )   7( b2 )    -         -                
 x   1  1     678  0.016938    2( a1 )   5( a1 )    -              -           
 x   1  1     685 -0.017068    5( a1 )   6( a1 )    -              -           
 x   1  1     699 -0.014279    1( a1 )   9( a1 )    -              -           
 x   1  1     702 -0.014724    4( a1 )   9( a1 )    -              -           
 x   1  1     739 -0.011812    1( b1 )   6( b1 )    -              -           
 x   1  1     741 -0.012256    3( b1 )   6( b1 )    -              -           
 x   1  1     760 -0.011534    1( b2 )   6( b2 )    -              -           
 x   1  1     762 -0.012028    3( b2 )   6( b2 )    -              -           
 x   1  1     801 -0.012769   10( a1 )   1( b2 )    -                   -      
 x   1  1     807 -0.014757    5( a1 )   2( b2 )    -                   -      
 x   1  1     823 -0.013154   10( a1 )   3( b2 )    -                   -      
 x   1  1     829  0.014805    5( a1 )   4( b2 )    -                   -      
 x   1  1     847  0.011841    1( a1 )   6( b2 )    -                   -      
 x   1  1     850  0.012079    4( a1 )   6( b2 )    -                   -      
 x   1  1     884 -0.014757    5( a1 )   2( b1 )    -                        - 
 x   1  1     906  0.014805    5( a1 )   4( b1 )    -                        - 
 x   1  1     924  0.011992    1( a1 )   6( b1 )    -                        - 
 x   1  1     927  0.012290    4( a1 )   6( b1 )    -                        - 
 x   1  1    1000 -0.011497    1( b1 )   1( b2 )         -    -                
 x   1  1    1247  0.011272    1( a1 )   1( b1 )         -                   - 
 x   1  1    1466 -0.011024    1( a1 )   1( b2 )              -         -      
 x   1  1    1641 -0.010003    1( a1 )   1( b1 )                   -    -      
 x   1  1    1760 -0.010002    1( a1 )   1( b2 )                   -         - 
 w   1  1    1938 -0.011264    2( a1 )   1( a2 )   +-                          
 w   1  1    1942 -0.014508    6( a1 )   1( a2 )   +-                          
 w   1  1    1949  0.014448    2( a1 )   2( a2 )   +-                          
 w   1  1    1953  0.024629    6( a1 )   2( a2 )   +-                          
 w   1  1    1967  0.024993    9( a1 )   3( a2 )   +-                          
 w   1  1    1970 -0.028396    1( b1 )   1( b2 )   +-                          
 w   1  1    1972 -0.026196    3( b1 )   1( b2 )   +-                          
 w   1  1    1974 -0.010503    5( b1 )   1( b2 )   +-                          
 w   1  1    1980  0.012589    4( b1 )   2( b2 )   +-                          
 w   1  1    1984 -0.026196    1( b1 )   3( b2 )   +-                          
 w   1  1    1986 -0.025431    3( b1 )   3( b2 )   +-                          
 w   1  1    1988 -0.011902    5( b1 )   3( b2 )   +-                          
 w   1  1    1992  0.012589    2( b1 )   4( b2 )   +-                          
 w   1  1    1994 -0.021368    4( b1 )   4( b2 )   +-                          
 w   1  1    1998  0.010503    1( b1 )   5( b2 )   +-                          
 w   1  1    2000  0.011902    3( b1 )   5( b2 )   +-                          
 w   1  1    2010 -0.020587    6( b1 )   6( b2 )   +-                          
 w   1  1    2011 -0.014099    7( b1 )   6( b2 )   +-                          
 w   1  1    2017  0.011083    6( b1 )   7( b2 )   +-                          
 w   1  1    2052 -0.024170    1( b1 )   1( b2 )   +     -                     
 w   1  1    2054 -0.017522    3( b1 )   1( b2 )   +     -                     
 w   1  1    2066 -0.018469    1( b1 )   3( b2 )   +     -                     
 w   1  1    2068 -0.014466    3( b1 )   3( b2 )   +     -                     
 w   1  1    2134  0.026384    1( b1 )   1( b2 )   +          -                
 w   1  1    2136  0.020077    3( b1 )   1( b2 )   +          -                
 w   1  1    2148  0.019210    1( b1 )   3( b2 )   +          -                
 w   1  1    2150  0.015790    3( b1 )   3( b2 )   +          -                
 w   1  1    2183 -0.020183    1( a1 )   1( a1 )   +               -           
 w   1  1    2189 -0.024890    1( a1 )   4( a1 )   +               -           
 w   1  1    2192 -0.015445    4( a1 )   4( a1 )   +               -           
 w   1  1    2194  0.010350    2( a1 )   5( a1 )   +               -           
 w   1  1    2219  0.010078    1( a1 )   9( a1 )   +               -           
 w   1  1    2222  0.010837    4( a1 )   9( a1 )   +               -           
 w   1  1    2255  0.023497    1( b1 )   1( b1 )   +               -           
 w   1  1    2258  0.021139    1( b1 )   3( b1 )   +               -           
 w   1  1    2260  0.010721    3( b1 )   3( b1 )   +               -           
 w   1  1    2283  0.023496    1( b2 )   1( b2 )   +               -           
 w   1  1    2286  0.021138    1( b2 )   3( b2 )   +               -           
 w   1  1    2288  0.010721    3( b2 )   3( b2 )   +               -           
 w   1  1    2332  0.031020    1( a1 )   1( b2 )   +                    -      
 w   1  1    2335  0.022774    4( a1 )   1( b2 )   +                    -      
 w   1  1    2354  0.023412    1( a1 )   3( b2 )   +                    -      
 w   1  1    2357  0.018565    4( a1 )   3( b2 )   +                    -      
 w   1  1    2409  0.031023    1( a1 )   1( b1 )   +                         - 
 w   1  1    2412  0.022777    4( a1 )   1( b1 )   +                         - 
 w   1  1    2431  0.023414    1( a1 )   3( b1 )   +                         - 
 w   1  1    2434  0.018566    4( a1 )   3( b1 )   +                         - 
 w   1  1    2622 -0.016071    1( b1 )   1( b2 )        +     -                
 w   1  1    2624 -0.010338    3( b1 )   1( b2 )        +     -                
 w   1  1    2636 -0.010263    1( b1 )   3( b2 )        +     -                
 w   1  1    2671  0.030508    1( a1 )   1( a1 )        +          -           
 w   1  1    2677  0.026216    1( a1 )   4( a1 )        +          -           
 w   1  1    2680  0.013472    4( a1 )   4( a1 )        +          -           
 w   1  1    2685  0.011530    5( a1 )   5( a1 )        +          -           
 w   1  1    2743  0.037330    1( b1 )   1( b1 )        +          -           
 w   1  1    2746  0.032030    1( b1 )   3( b1 )        +          -           
 w   1  1    2748  0.016262    3( b1 )   3( b1 )        +          -           
 w   1  1    2771  0.042355    1( b2 )   1( b2 )        +          -           
 w   1  1    2774  0.036878    1( b2 )   3( b2 )        +          -           
 w   1  1    2776  0.018769    3( b2 )   3( b2 )        +          -           
 w   1  1    2897 -0.013659    1( a1 )   1( b1 )        +                    - 
 w   1  1    3077 -0.033298    1( a1 )   1( a1 )             +     -           
 w   1  1    3083 -0.028615    1( a1 )   4( a1 )             +     -           
 w   1  1    3086 -0.014704    4( a1 )   4( a1 )             +     -           
 w   1  1    3091 -0.012584    5( a1 )   5( a1 )             +     -           
 w   1  1    3149 -0.045790    1( b1 )   1( b1 )             +     -           
 w   1  1    3152 -0.039829    1( b1 )   3( b1 )             +     -           
 w   1  1    3154 -0.020267    3( b1 )   3( b1 )             +     -           
 w   1  1    3177 -0.041185    1( b2 )   1( b2 )             +     -           
 w   1  1    3180 -0.035386    1( b2 )   3( b2 )             +     -           
 w   1  1    3182 -0.017969    3( b2 )   3( b2 )             +     -           
 w   1  1    3226  0.013339    1( a1 )   1( b2 )             +          -      
 w   1  1    3712  0.012165    1( b1 )   1( b2 )                       +-      
 w   1  1    3761 -0.035771    1( a1 )   1( a1 )                       +     - 
 w   1  1    3767 -0.030387    1( a1 )   4( a1 )                       +     - 
 w   1  1    3770 -0.015604    4( a1 )   4( a1 )                       +     - 
 w   1  1    3775 -0.014797    5( a1 )   5( a1 )                       +     - 
 w   1  1    3833 -0.052831    1( b1 )   1( b1 )                       +     - 
 w   1  1    3836 -0.045865    1( b1 )   3( b1 )                       +     - 
 w   1  1    3838 -0.023330    3( b1 )   3( b1 )                       +     - 
 w   1  1    3861 -0.052829    1( b2 )   1( b2 )                       +     - 
 w   1  1    3864 -0.045863    1( b2 )   3( b2 )                       +     - 
 w   1  1    3866 -0.023330    3( b2 )   3( b2 )                       +     - 
 w   1  1    3922  0.012163    1( b1 )   1( b2 )                            +- 

 ci coefficient statistics:
           rq > 0.1                4
      0.1> rq > 0.01             171
     0.01> rq > 0.001            881
    0.001> rq > 0.0001           706
   0.0001> rq > 0.00001          197
  0.00001> rq > 0.000001          27
 0.000001> rq                   1984
           all                  3970
  iref  icsf         v(icsf)             hv(icsf)
     1     1     -0.467981107107
     2     2      0.510787959933
     3     3      0.600650229661
     4     4      0.022618203194
     5     5     -0.074346282563
     6     6     -0.087310294129
     7     7     -0.128726818972
     8     8      0.095399555875
     9     9      0.041031322785
    10    10      0.095285379845
    11    11      0.087400207668
    12    12     -0.084679996281
    13    13     -0.084681049776
    14    14     -0.033543884494
    15    15     -0.028766323025
    16    16     -0.030744681919
    17    17     -0.036384406040
    18    18     -0.018299460072
    19    19      0.006079346335
    20    20      0.025144784269
    21    21     -0.024255436747
    22    22     -0.025770887419
    23    23     -0.008309499853
    24    24     -0.026488213425

 number of reference csfs (nref) is    24.  root number (iroot) is  1.
 c0**2 =   0.92001730  c0**2(renormalized) =   0.99360277
 c**2 (all zwalks) =   0.92001730  c**2(all zwalks, renormalized) =   0.99360277
 passed aftci ... 
 readint2: molcas,dalton2=                     0                     0
 files%faoints=aoints              
lodens (list->root)=  1
sifcfg setup: record length 4096 DP
# d1 elements per record  3272
# d2 elements per record  2730
  The AQCC density will be calculated.
 item #                     1 suffix=:.drt1.state1:
 read_civout: repnuc=  -43.4985876562748     
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method: 30 last record  1max overlap with ref# 96% root-following 0
 MR-CISD energy:   -46.32656803    -2.82798038
 residuum:     0.00083703
 deltae:     0.00000734
 apxde:     0.00000080
 a4den:     1.01351055
 reference energy:   -46.27407928    -2.77549162

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.95910481     0.15914946    -0.12212373     0.15914348    -0.06359435     0.10249060     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.95910481     0.15914946     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 rdcirefv: extracting reference vector # 1(blocksize=    24 #zcsf=      24)
 =========== Executing IN-CORE method ==========
================================================================================
   DYZ=       0  DYX=       0  DYW=       0
   D0Z=      34  D0Y=       0  D0X=       0  D0W=       0
  DDZI=      80 DDYI=       0 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
 root #                      1 : Scaling(1) with    1.01351055228568      
 corresponding to ref #                      1
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=     105  DYX=     130  DYW=     160
   D0Z=      34  D0Y=     103  D0X=      12  D0W=      18
  DDZI=      80 DDYI=     180 DDXI=      30 DDWI=      36
  DDZE=       0 DDYE=      70 DDXE=      15 DDWE=      21
================================================================================
Trace of MO density:     4.000000
    4  correlated and     8  frozen core electrons

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     1.80555987    -0.13890713     0.15157054     0.00000000     0.02246239    -0.00205594
   MO   4     0.00000000     0.00000000    -0.13890713     0.28315855    -0.26181989     0.00000000    -0.01255106     0.00432821
   MO   5     0.00000000     0.00000000     0.15157054    -0.26181989     0.32904587     0.00000000     0.01427777     0.00160524
   MO   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.01651049     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000     0.02246239    -0.01255106     0.01427777     0.00000000     0.00179141    -0.00010128
   MO   8     0.00000000     0.00000000    -0.00205594     0.00432821     0.00160524     0.00000000    -0.00010128     0.00069484
   MO   9     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.01178357     0.00000000     0.00000000
   MO  10     0.00000000     0.00000000    -0.04074395    -0.02410377     0.02630871     0.00000000     0.00135699    -0.00012437
   MO  11     0.00000000     0.00000000     0.01809438    -0.01103727     0.01183622     0.00000000     0.00187956    -0.00019344
   MO  12     0.00000000     0.00000000    -0.00047569    -0.00370280    -0.00396921     0.00000000    -0.00012326    -0.00080255
   MO  13     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00249722     0.00000000     0.00000000
   MO  14     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00134604     0.00000000     0.00000000
   MO  15     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00039461     0.00000000     0.00000000
   MO  16     0.00000000     0.00000000     0.00037467     0.00107648    -0.00117494     0.00000000    -0.00007230     0.00000662

                MO   9         MO  10         MO  11         MO  12         MO  13         MO  14         MO  15         MO  16
   MO   3     0.00000000    -0.04074395     0.01809438    -0.00047569     0.00000000     0.00000000     0.00000000     0.00037467
   MO   4     0.00000000    -0.02410377    -0.01103727    -0.00370280     0.00000000     0.00000000     0.00000000     0.00107648
   MO   5     0.00000000     0.02630871     0.01183622    -0.00396921     0.00000000     0.00000000     0.00000000    -0.00117494
   MO   6     0.01178357     0.00000000     0.00000000     0.00000000     0.00249722     0.00134604    -0.00039461     0.00000000
   MO   7     0.00000000     0.00135699     0.00187956    -0.00012326     0.00000000     0.00000000     0.00000000    -0.00007230
   MO   8     0.00000000    -0.00012437    -0.00019344    -0.00080255     0.00000000     0.00000000     0.00000000     0.00000662
   MO   9     0.00882112     0.00000000     0.00000000     0.00000000     0.00214311     0.00085574    -0.00025088     0.00000000
   MO  10     0.00000000     0.00886241     0.00115030    -0.00003010     0.00000000     0.00000000     0.00000000    -0.00039091
   MO  11     0.00000000     0.00115030     0.00222847    -0.00003235     0.00000000     0.00000000     0.00000000    -0.00006749
   MO  12     0.00000000    -0.00003010    -0.00003235     0.00099812     0.00000000     0.00000000     0.00000000     0.00000177
   MO  13     0.00214311     0.00000000     0.00000000     0.00000000     0.00072276     0.00021618    -0.00006337     0.00000000
   MO  14     0.00085574     0.00000000     0.00000000     0.00000000     0.00021618     0.00200758    -0.00032045     0.00000000
   MO  15    -0.00025088     0.00000000     0.00000000     0.00000000    -0.00006337    -0.00032045     0.00100840     0.00000000
   MO  16     0.00000000    -0.00039091    -0.00006749     0.00000177     0.00000000     0.00000000     0.00000000     0.00002246

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     2.00000000     1.84007104     0.53968885     0.04439618     0.02561422     0.00519465     0.00238030
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     0.00198870     0.00091447     0.00055107     0.00054227     0.00006239     0.00001453     0.00001070     0.00000300

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4
   MO   1     0.55686236     0.01841286    -0.01578209     0.00000000
   MO   2     0.01841286     0.00188400    -0.00196784     0.00000000
   MO   3    -0.01578209    -0.00196784     0.00236328     0.00000000
   MO   4     0.00000000     0.00000000     0.00000000     0.00210870

Natural orbital populations,block 2
              MO     1       MO     2       MO     3       MO     4       MO
  occ(*)=     0.55792413     0.00307100     0.00210870     0.00011450

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     0.43848881     0.00000000     0.01544782     0.00000000    -0.01353772     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.02760966     0.00000000     0.01926613     0.00000000     0.00415716     0.00149199
   MO   4     0.00000000     0.01544782     0.00000000     0.00152796     0.00000000    -0.00162533     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.01926613     0.00000000     0.01388732     0.00000000     0.00333691     0.00102336
   MO   6     0.00000000    -0.01353772     0.00000000    -0.00162533     0.00000000     0.00192617     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000     0.00415716     0.00000000     0.00333691     0.00000000     0.00109247     0.00029396
   MO   8     0.00000000     0.00000000     0.00149199     0.00000000     0.00102336     0.00000000     0.00029396     0.00187135
   MO   9     0.00000000     0.00000000     0.00183760     0.00000000     0.00154610     0.00000000     0.00057157     0.00049815

                MO   9
   MO   2     0.00000000
   MO   3     0.00183760
   MO   4     0.00000000
   MO   5     0.00154610
   MO   6     0.00000000
   MO   7     0.00057157
   MO   8     0.00049815
   MO   9     0.00127133

Natural orbital populations,block 3
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     0.43945617     0.04212639     0.00241876     0.00198658     0.00111924     0.00048554     0.00006801
              MO     9       MO
  occ(*)=     0.00001437

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     0.43849042     0.00000000     0.01544699     0.00000000    -0.01353714     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.02760801     0.00000000     0.01926508     0.00000000    -0.00415705     0.00174988
   MO   4     0.00000000     0.01544699     0.00000000     0.00152780     0.00000000    -0.00162524     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.01926508     0.00000000     0.01388664     0.00000000    -0.00333684     0.00124293
   MO   6     0.00000000    -0.01353714     0.00000000    -0.00162524     0.00000000     0.00192614     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000    -0.00415705     0.00000000    -0.00333684     0.00000000     0.00109245    -0.00037608
   MO   8     0.00000000     0.00000000     0.00174988     0.00000000     0.00124293     0.00000000    -0.00037608     0.00200520
   MO   9     0.00000000     0.00000000    -0.00159398     0.00000000    -0.00137579     0.00000000     0.00052118    -0.00038724

                MO   9
   MO   2     0.00000000
   MO   3    -0.00159398
   MO   4     0.00000000
   MO   5    -0.00137579
   MO   6     0.00000000
   MO   7     0.00052118
   MO   8    -0.00038724
   MO   9     0.00113759

Natural orbital populations,block 4
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     0.43945768     0.04212409     0.00241869     0.00198659     0.00111925     0.00048558     0.00006799
              MO     9       MO
  occ(*)=     0.00001437


 total number of electrons =   12.0000000000

 test slabel:                     4
  a1  a2  b1  b2 


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        a1  partial gross atomic populations
   ao class       1a1        2a1        3a1        4a1        5a1        6a1 
     3_ s       2.000000   0.000000   1.792721   0.016807   0.000000   0.000000
     3_ p       0.000000   2.000000   0.000000   0.000000   0.000000   0.025485
     3_ d       0.000000   0.000000   0.047350   0.522881   0.044396   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000000   0.000000   0.000129
 
   ao class       7a1        8a1        9a1       10a1       11a1       12a1 
     3_ s       0.004706   0.000207   0.000000   0.000000   0.000000   0.000000
     3_ p       0.000000   0.000000   0.000018   0.000000   0.000000   0.000541
     3_ d       0.000489   0.002174   0.000000   0.000000   0.000551   0.000000
     3_ f       0.000000   0.000000   0.001971   0.000914   0.000000   0.000001
 
   ao class      13a1       14a1       15a1       16a1 
     3_ s       0.000001   0.000000   0.000000   0.000003
     3_ p       0.000000   0.000000   0.000011   0.000000
     3_ d       0.000062   0.000015   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000000

                        a2  partial gross atomic populations
   ao class       1a2        2a2        3a2        4a2 
     3_ d       0.557924   0.003071   0.000000   0.000115
     3_ f       0.000000   0.000000   0.002109   0.000000

                        b1  partial gross atomic populations
   ao class       1b1        2b1        3b1        4b1        5b1        6b1 
     3_ p       2.000000   0.000000   0.041884   0.000000   0.000044   0.000299
     3_ d       0.000000   0.439456   0.000000   0.002419   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000243   0.000000   0.001943   0.000820
 
   ao class       7b1        8b1        9b1 
     3_ p       0.000348   0.000000   0.000014
     3_ d       0.000000   0.000068   0.000000
     3_ f       0.000137   0.000000   0.000000

                        b2  partial gross atomic populations
   ao class       1b2        2b2        3b2        4b2        5b2        6b2 
     3_ p       2.000000   0.000000   0.041881   0.000000   0.000044   0.000299
     3_ d       0.000000   0.439458   0.000000   0.002419   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000243   0.000000   0.001943   0.000820
 
   ao class       7b2        8b2        9b2 
     3_ p       0.000348   0.000000   0.000014
     3_ d       0.000000   0.000068   0.000000
     3_ f       0.000137   0.000000   0.000000


                        gross atomic populations
     ao            3_
      s         3.814445
      p         6.111231
      d         2.062914
      f         0.011410
    total      12.000000
 

 Total number of electrons:   12.00000000

 DA ...
