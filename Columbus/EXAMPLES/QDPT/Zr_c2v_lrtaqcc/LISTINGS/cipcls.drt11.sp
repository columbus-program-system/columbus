
 program cipc      

 print the csf info for mrsdci wave functions

 written by: ron shepard

 version date: 06-jun-96

 This Version of Program cipc is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 workspace allocation parameters: lencor=  13107200 mem1=         0 ifirst=         1

 drt header information:
  cidrt_title                                                                    
 nmot  =    38 niot  =     6 nfct  =     4 nfvt  =     0
 nrow  =    23 nsym  =     4 ssym  =     3 lenbuf=  1600
 spnorb=     F spnodd=     F lxyzir(1:3)= 0 0 0
 nwalk,xbar:         50       15       20       15        0
 nvalwt,nvalw:       39        4       20       15        0
 ncsft:            1558
 map(*)=    -1 -1 29 30 31  1  2  3  4  5  6  7  8  9 10 11 32 12 13 14
            -1 33 15 16 17 18 19 20 21 -1 34 22 23 24 25 26 27 28
 mu(*)=      0  0  0  0  0  0
 syml(*) =   1  1  1  2  3  4
 rmo(*)=     3  4  5  1  2  2

 indx01:    39 indices saved in indxv(*)
 test nroots froot                      1                     1
===================================ROOT # 1===================================

 rdhciv: CI vector file information:
  cidrt_title                                                                    
 energy computed by program ciudg.       zam792            17:56:40.092 17-Dec-13

 lenrec =   32768 lenci =      1558 ninfo =  6 nenrgy =  8 ntitle =  2

 Max. overlap with ref vector #        1
 Valid ci vector #        1
 Method:       30       96% overlap
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -4.349858765627E+01, ietype=    6,   fcore energy of type: H1(*)   
 energy( 3)= -4.629196861732E+01, ietype=-1026,   total energy of type: MRSDCI  
 energy( 4)=  4.335812007076E-04, ietype=-2055, cnvginf energy of type: CI-Resid
 energy( 5)=  4.433475566223E-06, ietype=-2056, cnvginf energy of type: CI-D.E. 
 energy( 6)=  2.838252015745E-07, ietype=-2057, cnvginf energy of type: CI-ApxDE
 energy( 7)=  1.012927784112E+00, ietype=-1039,   total energy of type: a4den   
 energy( 8)= -4.624538335620E+01, ietype=-1041,   total energy of type: E(ref)  
==================================================================================
space sufficient for valid walk range           1         39
               respectively csf range           1       1558

 space is available for   4357921 coefficients.

 updated histogram parameters:
 csfmn = 0.0000E+00 csfmx = 1.0000E+00 fhist = 5.0000E-01 nhist =  20

 this program will print the csfs generated from
 the drt according to the following print options :

 1) run in batch mode: all valid roots are automatically
    analysed and csf info is printed by default contribution
    threshold 0.01 
 2) run in interactive mode
 3) generate files for cioverlap without symmetry
 4) generate files for cioverlap with symmetry

 input menu number [  1]:
================================================================================
===================================VECTOR # 1===================================
================================================================================


 rdcivnew:      98 coefficients were selected.
 workspace: ncsfmx=    1558
 ncsfmx=                  1558

 histogram parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =    1558 ncsft =    1558 ncsf =      98
 nhist =  20 fhist = 0.50000

    cmin                cmax        num  '*'=     2 csfs.
 ----------          ----------   ----- ---------|---------|---------|---------|
 5.0000E-01 <= |c| < 1.0000E+00       2 *
 2.5000E-01 <= |c| < 5.0000E-01       0
 1.2500E-01 <= |c| < 2.5000E-01       1 *
 6.2500E-02 <= |c| < 1.2500E-01       4 **
 3.1250E-02 <= |c| < 6.2500E-02      16 ********
 1.5625E-02 <= |c| < 3.1250E-02      12 ******
 7.8125E-03 <= |c| < 1.5625E-02      63 ********************************
 3.9062E-03 <= |c| < 7.8125E-03       0
 1.9531E-03 <= |c| < 3.9062E-03       0
 9.7656E-04 <= |c| < 1.9531E-03       0
 4.8828E-04 <= |c| < 9.7656E-04       0
 2.4414E-04 <= |c| < 4.8828E-04       0
 1.2207E-04 <= |c| < 2.4414E-04       0
 6.1035E-05 <= |c| < 1.2207E-04       0
 3.0518E-05 <= |c| < 6.1035E-05       0
 1.5259E-05 <= |c| < 3.0518E-05       0
 7.6294E-06 <= |c| < 1.5259E-05       0
 3.8147E-06 <= |c| < 7.6294E-06       0
 0.0000E+00 <= |c| < 3.8147E-06       0
                                  ----- ---------|---------|---------|---------|
                  total read =       98 total stored =      98

 from the selected csfs,
 min(|csfvec(:)|) = 1.0007E-02    max(|csfvec(:)|) = 7.3187E-01
 norm=   1.00000000000000     
 csfs will be printed based on coefficient magnitudes.

 current csfvec(*) selection parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =    1558 ncsft =    1558 ncsf =      98

 i:slabel(i) =  1: a1   2: a2   3: b1   4: b2 
 
 frozen orbital =    1    2    3    4
 symfc(*)       =    1    1    3    4
 label          =  a1   a1   b1   b2 
 rmo(*)         =    1    2    1    1
 
 internal level =    1    2    3    4    5    6
 syml(*)        =    1    1    1    2    3    4
 label          =  a1   a1   a1   a2   b1   b2 
 rmo(*)         =    3    4    5    1    2    2

 printing selected csfs in sorted order from cmin = 0.00000 to cmax = 1.00000

   indcsf     c     c**2   v  lab:rmo  lab:rmo   step(*)
  ------- -------- ------- - ---- --- ---- --- ------------
          1  0.73187 0.53564 z*                    111010
          2 -0.59094 0.34921 z*                    110101
          3 -0.19642 0.03858 z*                    101101
          6 -0.08571 0.00735 y           b1 :  4  1111000
         59  0.07475 0.00559 y           a1 :  8  1100101
         30 -0.06956 0.00484 y           a2 :  2  1110001
         42 -0.06470 0.00419 y           a1 :  8  1101010
          8  0.06040 0.00365 y           b1 :  6  1111000
         41  0.05788 0.00335 y           a1 :  7  1101010
         13  0.05314 0.00282 y           b2 :  4  1110100
         20  0.04932 0.00243 y           a1 :  7  1110010
         63 -0.04916 0.00242 y           a1 : 12  1100101
         31  0.04887 0.00239 y           a2 :  3  1110001
         45  0.04428 0.00196 y           a1 : 11  1101010
        243  0.04211 0.00177 x  a1 :  6  b1 :  3 11101000
         15 -0.04200 0.00176 y           b2 :  6  1110100
         46  0.04171 0.00174 y           a1 : 12  1101010
         21  0.04093 0.00168 y           a1 :  8  1110010
        572 -0.03978 0.00158 x  b1 :  3  b2 :  3 11100001
         25 -0.03645 0.00133 y           a1 : 12  1110010
         24  0.03610 0.00130 y           a1 : 11  1110010
        265  0.03152 0.00099 x  a1 :  6  b1 :  5 11101000
        246  0.03131 0.00098 x  a1 :  9  b1 :  3 11101000
        574 -0.02986 0.00089 x  b1 :  5  b2 :  3 11100001
        586 -0.02960 0.00088 x  b1 :  3  b2 :  5 11100001
        306  0.02475 0.00061 x  a1 : 14  b1 :  8 11101000
        268  0.02460 0.00061 x  a1 :  9  b1 :  5 11101000
        588 -0.02328 0.00054 x  b1 :  5  b2 :  5 11100001
        620 -0.02325 0.00054 x  b1 :  9  b2 :  9 11100001
         51 -0.02068 0.00043 y           a2 :  2  1101001
         62 -0.01825 0.00033 y           a1 : 11  1100101
        636 -0.01765 0.00031 x  a1 : 10  b1 :  4 11011000
        242 -0.01636 0.00027 x  a2 :  4  b2 :  9 11110000
        459 -0.01603 0.00026 x  a1 : 11  a1 : 12 11100010
        220  0.01576 0.00025 x  a1 : 15  b1 :  9 11110000
         52  0.01523 0.00023 y           a2 :  3  1101001
        676  0.01507 0.00023 x  a1 :  6  b1 :  8 11011000
        556  0.01461 0.00021 x  a1 : 12  a2 :  3 11100001
       1369 -0.01451 0.00021 x  a1 :  8  a1 : 10 11000101
        921 -0.01425 0.00020 x  a1 : 10  a2 :  2 11010001
        679  0.01387 0.00019 x  a1 :  9  b1 :  8 11011000
       1105  0.01385 0.00019 x  a1 :  8  a1 : 10 11001010
         58 -0.01353 0.00018 y           a1 :  7  1100101
        307  0.01339 0.00018 x  a1 : 15  b1 :  8 11101000
        755  0.01329 0.00018 x  a1 : 10  b2 :  4 11010100
        281  0.01328 0.00018 x  a1 : 11  b1 :  6 11101000
        232  0.01307 0.00017 x  a2 :  3  b2 :  6 11110000
       1125  0.01298 0.00017 x  a1 :  6  a1 : 14 11001010
        824 -0.01292 0.00017 x  a1 :  7  a1 : 10 11010010
        282  0.01270 0.00016 x  a1 : 12  b1 :  6 11101000
        658  0.01264 0.00016 x  a1 : 10  b1 :  6 11011000
        795 -0.01250 0.00016 x  a1 :  6  b2 :  8 11010100
        401 -0.01243 0.00015 x  a1 : 12  b2 :  6 11100100
        538  0.01235 0.00015 x  b2 :  8  b2 :  9 11100010
        208  0.01233 0.00015 x  a1 : 14  b1 :  8 11110000
       1165  0.01230 0.00015 x  b1 :  3  b1 :  8 11001010
        629  0.01228 0.00015 x  a1 : 14  b1 :  3 11011000
        517  0.01211 0.00015 x  b1 :  8  b1 :  9 11100010
        956  0.01209 0.00015 x  b1 :  9  b2 :  3 11010001
        441  0.01201 0.00014 x  a1 :  7  a1 :  8 11100010
       1128  0.01193 0.00014 x  a1 :  9  a1 : 14 11001010
        483 -0.01166 0.00014 x  a1 : 14  a1 : 15 11100010
        700 -0.01160 0.00013 x  a2 :  4  b2 :  3 11011000
        906  0.01160 0.00013 x  b2 :  3  b2 :  8 11010010
        541  0.01149 0.00013 x  a1 :  8  a2 :  2 11100001
        992  0.01142 0.00013 x  b1 :  3  b2 :  9 11010001
        911  0.01139 0.00013 x  b2 :  3  b2 :  9 11010010
        798 -0.01137 0.00013 x  a1 :  9  b2 :  8 11010100
       1167  0.01133 0.00013 x  b1 :  5  b1 :  8 11001010
        651  0.01131 0.00013 x  a1 : 14  b1 :  5 11011000
        184 -0.01128 0.00013 x  a1 : 12  b1 :  6 11110000
        183  0.01126 0.00013 x  a1 : 11  b1 :  6 11110000
        970  0.01112 0.00012 x  b1 :  9  b2 :  5 11010001
        455 -0.01111 0.00012 x  a1 :  7  a1 : 12 11100010
        451 -0.01107 0.00012 x  a1 :  8  a1 : 11 11100010
       1104 -0.01104 0.00012 x  a1 :  7  a1 : 10 11001010
       1434  0.01103 0.00012 x  b1 :  3  b1 :  9 11000101
       1455  0.01097 0.00012 x  b2 :  3  b2 :  9 11000101
         69  0.01085 0.00012 y           b2 :  4  1100011
        256  0.01068 0.00011 x  a1 :  8  b1 :  4 11101000
        706 -0.01067 0.00011 x  a2 :  4  b2 :  5 11011000
        749  0.01066 0.00011 x  a1 : 15  b2 :  3 11010100
        552 -0.01065 0.00011 x  a1 :  8  a2 :  3 11100001
        176  0.01062 0.00011 x  a1 : 15  b1 :  5 11110000
        777 -0.01057 0.00011 x  a1 : 10  b2 :  6 11010100
        994  0.01054 0.00011 x  b1 :  5  b2 :  9 11010001
        908  0.01051 0.00011 x  b2 :  5  b2 :  8 11010010
        913  0.01041 0.00011 x  b2 :  5  b2 :  9 11010010
        437 -0.01039 0.00011 x  a1 : 15  b2 :  9 11100100
        154  0.01017 0.00010 x  a1 : 15  b1 :  3 11110000
        932  0.01016 0.00010 x  a1 : 10  a2 :  3 11010001
       1457  0.01015 0.00010 x  b2 :  5  b2 :  9 11000101
       1436  0.01014 0.00010 x  b1 :  5  b1 :  9 11000101
         36 -0.01013 0.00010 y           b2 :  6  1101100
       1191 -0.01007 0.00010 x  b2 :  3  b2 :  9 11001010
        825 -0.01004 0.00010 x  a1 :  8  a1 : 10 11010010
        853 -0.01002 0.00010 x  a1 :  6  a1 : 15 11010010
        890  0.01001 0.00010 x  b1 :  3  b1 :  9 11010010
           98 csfs were printed in this range.
