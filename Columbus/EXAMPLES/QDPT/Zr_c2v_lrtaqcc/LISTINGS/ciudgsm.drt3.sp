1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.

  This is a  mraqcc energy evaluation      

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  1  1    -46.2740792781 -7.1054E-15  5.7906E-02  2.2790E-01  1.0000E-03
 mraqcc  #  2  1    -46.3234605324  4.9381E-02  2.5791E-03  4.6094E-02  1.0000E-03
 mraqcc  #  3  1    -46.3261421245  2.6816E-03  4.0980E-04  1.7831E-02  1.0000E-03
 mraqcc  #  4  1    -46.3264972451  3.5512E-04  7.8388E-05  8.2484E-03  1.0000E-03
 mraqcc  #  5  1    -46.3265608423  6.3597E-05  7.6720E-06  2.5212E-03  1.0000E-03
 mraqcc  #  6  1    -46.3265680325  7.1902E-06  8.1026E-07  8.3855E-04  1.0000E-03

 mraqcc   convergence criteria satisfied after  6 iterations.

 final mraqcc   convergence information:
 mraqcc  #  6  1    -46.3265680325  7.1902E-06  8.1026E-07  8.3855E-04  1.0000E-03

 number of reference csfs (nref) is    24.  root number (iroot) is  1.
 c0**2 =   0.92001679  c0**2(renormalized) =   0.99360269
 c**2 (all zwalks) =   0.92001679  c**2(all zwalks, renormalized) =   0.99360269
