 
 program cidrt 7.0  

 distinct row table construction, reference csf selection, and internal
 walk selection for multireference single- and double-excitation
configuration interaction.

 references:  r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).
              h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. symp. 15, 91 (1981).

 based on the initial version by  Ron Shepard

 extended for spin-orbit CI calculations ( Russ Pitzer, OSU)

 and large active spaces (Thomas Müller, FZ(21 Juelich)

 This Version of Program CIDRT is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de

*********************** File revision status: ***********************
* cidrt1.F9 Revision: 1.1.6.2           Date: 2013/04/11 14:37:29   * 
* cidrt2.F9 Revision: 1.1.6.2           Date: 2013/04/11 14:37:29   * 
* cidrt3.F9 Revision: 1.1.6.2           Date: 2013/04/11 14:37:29   * 
* cidrt4.F9 Revision: 1.1.6.2           Date: 2013/04/11 14:37:29   * 
********************************************************************

 workspace allocation parameters: lencor=  13107200 mem1=         0 ifirst=         1
 expanded "keystrokes" are being written to file:
 /bigscratch/Columbus_C70/tests/Zr/scr/WORK/cidrtky                              
 Spin-Orbit CI Calculation?(y,[n]) Spin-Free Calculation
 
 input the spin multiplicity [  0]: spin multiplicity, smult            :   5    quintet 
 input the total number of electrons [  0]: total number of electrons, nelt     :    12
 input the number of irreps (1:8) [  0]: point group dimension, nsym         :     1
 enter symmetry labels:(y,[n]) enter 1 labels (a4):
 enter symmetry label, default=   1
 symmetry labels: (symmetry, slabel)
 ( 1,  a  ) 
 input nmpsy(*):
 nmpsy(*)=        38
 
   symmetry block summary
 block(*)=         1
 slabel(*)=      a  
 nmpsy(*)=        38
 
 total molecular orbitals            :    38
 input the molecular spatial symmetry (irrep 1:nsym) [  0]: state spatial symmetry label        :  a  
 
 input the frozen core orbitals (sym(i),rmo(i),i=1,nfct):
 total frozen core orbitals, nfct    :     4
 
 fcorb(*)=         1   2   3   4
 slabel(*)=      a   a   a   a  
 
 number of frozen core orbitals      :     4
 number of frozen core electrons     :     8
 number of internal electrons        :     4
 
 input the frozen virtual orbitals (sym(i),rmo(i),i=1,nfvt):
 total frozen virtual orbitals, nfvt :     0

 no frozen virtual orbitals entered
 
 input the internal orbitals (sym(i),rmo(i),i=1,niot):
 niot                                :     6
 
 modrt(*)=         5   6   7   8   9  10
 slabel(*)=      a   a   a   a   a   a  
 
 total number of orbitals            :    38
 number of frozen core orbitals      :     4
 number of frozen virtual orbitals   :     0
 number of internal orbitals         :     6
 number of external orbitals         :    28
 
 orbital-to-level mapping vector
 map(*)=          -1  -1  -1  -1  29  30  31  32  33  34   1   2   3   4   5
                   6   7   8   9  10  11  12  13  14  15  16  17  18  19  20
                  21  22  23  24  25  26  27  28
 
 input the number of ref-csf doubly-occupied orbitals [  0]: (ref) doubly-occupied orbitals      :     0
 
 no. of internal orbitals            :     6
 no. of doubly-occ. (ref) orbitals   :     0
 no. active (ref) orbitals           :     6
 no. of active electrons             :     4
 
 input the active-orbital, active-electron occmnr(*):
   5  6  7  8  9 10
 input the active-orbital, active-electron occmxr(*):
   5  6  7  8  9 10
 
 actmo(*) =        5   6   7   8   9  10
 occmnr(*)=        0   0   0   0   0   4
 occmxr(*)=        4   4   4   4   4   4
 reference csf cumulative electron occupations:
 modrt(*)=         5   6   7   8   9  10
 occmnr(*)=        0   0   0   0   0   4
 occmxr(*)=        4   4   4   4   4   4
 
 input the active-orbital bminr(*):
   5  6  7  8  9 10
 input the active-orbital bmaxr(*):
   5  6  7  8  9 10
 reference csf b-value constraints:
 modrt(*)=         5   6   7   8   9  10
 bminr(*)=         0   0   0   0   0   0
 bmaxr(*)=         4   4   4   4   4   4
 input the active orbital smaskr(*):
   5  6  7  8  9 10
 modrt:smaskr=
   5:1111   6:1111   7:1111   8:1111   9:1111  10:1111
 
 input the maximum excitation level from the reference csfs [  2]: maximum excitation from ref. csfs:  :     2
 number of internal electrons:       :     4
 
 input the internal-orbital mrsdci occmin(*):
   5  6  7  8  9 10
 input the internal-orbital mrsdci occmax(*):
   5  6  7  8  9 10
 mrsdci csf cumulative electron occupations:
 modrt(*)=         5   6   7   8   9  10
 occmin(*)=        0   0   0   0   0   2
 occmax(*)=        4   4   4   4   4   4
 
 input the internal-orbital mrsdci bmin(*):
   5  6  7  8  9 10
 input the internal-orbital mrsdci bmax(*):
   5  6  7  8  9 10
 mrsdci b-value constraints:
 modrt(*)=         5   6   7   8   9  10
 bmin(*)=          0   0   0   0   0   0
 bmax(*)=          4   4   4   4   4   4
 
 input the internal-orbital smask(*):
   5  6  7  8  9 10
 modrt:smask=
   5:1111   6:1111   7:1111   8:1111   9:1111  10:1111
 
 internal orbital summary:
 block(*)=         1   1   1   1   1   1
 slabel(*)=      a   a   a   a   a   a  
 rmo(*)=           5   6   7   8   9  10
 modrt(*)=         5   6   7   8   9  10
 
 reference csf info:
 occmnr(*)=        0   0   0   0   0   4
 occmxr(*)=        4   4   4   4   4   4
 
 bminr(*)=         0   0   0   0   0   0
 bmaxr(*)=         4   4   4   4   4   4
 
 
 mrsdci csf info:
 occmin(*)=        0   0   0   0   0   2
 occmax(*)=        4   4   4   4   4   4
 
 bmin(*)=          0   0   0   0   0   0
 bmax(*)=          4   4   4   4   4   4
 

 a priori removal of distinct rows:

 input the level, a, and b values for the vertices 
 to be removed (-1/ to end).

 input level, a, and b (-1/ to end):
 no vertices marked for removal
 
 impose generalized interacting space restrictions?(y,[n]) generalized interacting space restrictions will not be imposed.
 multp(*)=
  hmult                     0
 lxyzir   0   0   0
 symmetry of spin functions (spnir)
       --------------------------Ms ----------------------------
   S     1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19
   1     1  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   2     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   3     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   4     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   5     1  0  0  0  1  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   6     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   7     0  0  0  1  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   8     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   9     1  0  0  0  1  0  0  0  1  0  0  0  0  0  0  0  0  0  0
  10     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  11     0  0  0  1  0  0  0  1  0  0  0  0  0  0  0  0  0  0  0
  12     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  13     1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  0  0  0
  14     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  15     0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  0  0  0  0
  16     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  17     1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0
  18     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  19     0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0

 number of rows in the drt :  23

 manual arc removal step:


 input the level, a, b, and step values 
 for the arcs to be removed (-1/ to end).

 input the level, a, b, and step (-1/ to end):
 remarc:   0 arcs removed out of   0 specified.

 xbarz=          15
 xbary=          20
 xbarx=          15
 xbarw=           0
        --------
 nwalk=          50
 input the range of drt levels to print (l1,l2):
 levprt(*)        -1   0

 reference-csf selection step 1:
 total number of z-walks in the drt, nzwalk=      15

 input the list of allowed reference symmetries:
 allowed reference symmetries:             1
 allowed reference symmetry labels:      a  
 keep all of the z-walks as references?(y,[n]) y or n required
 keep all of the z-walks as references?(y,[n]) all z-walks are initially deleted.
 
 generate walks while applying reference drt restrictions?([y],n) reference drt restrictions will be imposed on the z-walks.
 
 impose additional orbital-group occupation restrictions?(y,[n]) 
 apply primary reference occupation restrictions?(y,[n]) 
 manually select individual walks?(y,[n])
 step 1 reference csf selection complete.
       15 csfs initially selected from      15 total walks.

 beginning step-vector based selection.
 enter [internal_orbital_step_vector/disposition] pairs:

 enter internal orbital step vector, (-1/ to end):
   5  6  7  8  9 10

 step 2 reference csf selection complete.
       15 csfs currently selected from      15 total walks.

 beginning numerical walk based selection.
 enter positive walk numbers to add walks,
 negative walk numbers to delete walks, and zero to end:

 input reference walk number (0 to end) [  0]:
 numerical walk-number based selection complete.
       15 reference csfs selected from      15 total z-walks.
 
 input the reference occupations, mu(*):
 reference occupations:
 mu(*)=            2   0   0   0   0   0
 
 number of step vectors saved:     15

 exlimw: beginning excitation-based walk selection...
 exlimw: nref=                    15

  number of valid internal walks of each symmetry:

       a  
      ----
 z      15
 y      20
 x      15
 w       0

 csfs grouped by internal walk symmetry:

       a  
      ----
 z      15
 y     560
 x    5670
 w       0

 total csf counts:
 z-vertex:       15
 y-vertex:      560
 x-vertex:     5670
 w-vertex:        0
           --------
 total:        6245
 
 this is an obsolete prompt.(y,[n])
 final mrsdci walk selection step:

 nvalw(*)=      15      20      15       0 nvalwt=      50

 enter positive walk numbers to add walks,
 negative walk numbers to delete walks, and zero to end.

 input mrsdci walk number (0 to end) [  0]:
 end of manual mrsdci walk selection.
 number added=   0 number removed=   0

 nvalw(*)=      15      20      15       0 nvalwt=      50

 lprune input numv1,nwalk=                    50                    50
 lprune input xbar(1,1),nref=                    15                    15

 lprune: l(*,*,*) pruned with nwalk=      50 nvalwt=      50=  15  20  15   0
 lprune:  z-drt, nprune=    12
 lprune:  y-drt, nprune=    10
 lprune: wx-drt, nprune=    12

 xbarz=          15
 xbary=          20
 xbarx=          15
 xbarw=           0
        --------
 nwalk=          50
 levprt(*)        -1   0

 beginning the reference csf index recomputation...

     iref   iwalk  step-vector
   ------  ------  ------------
        1       1  111100
        2       2  111010
        3       3  111001
        4       4  110110
        5       5  110101
        6       6  110011
        7       7  101110
        8       8  101101
        9       9  101011
       10      10  100111
       11      11  011110
       12      12  011101
       13      13  011011
       14      14  010111
       15      15  001111
 indx01:    15 elements set in vec01(*)

 beginning the valid upper walk index recomputation...
 indx01:    50 elements set in vec01(*)

 beginning the final csym(*) computation...

  number of valid internal walks of each symmetry:

       a  
      ----
 z      15
 y      20
 x      15
 w       0

 csfs grouped by internal walk symmetry:

       a  
      ----
 z      15
 y     560
 x    5670
 w       0

 total csf counts:
 z-vertex:       15
 y-vertex:      560
 x-vertex:     5670
 w-vertex:        0
           --------
 total:        6245
 
 input a title card, default=cidrt_title
 title card:
  cidrt_title                                                                   
  
 
 input a drt file name, default=cidrtfl
 drt and indexing arrays will be written to file:
 /bigscratch/Columbus_C70/tests/Zr/scr/WORK/cidrtfl                              
 
 write the drt file?([y],n) drt file is being written...
 wrtstr:  a  
nwalk=      50 cpos=       7 maxval=    9 cmprfactor=   86.00 %.
nwalk=      50 cpos=       1 maxval=   99 cmprfactor=   96.00 %.
nwalk=      50 cpos=       1 maxval=  999 cmprfactor=   94.00 %.
 compressed with: nwalk=      50 cpos=       1 maxval=   99 cmprfactor=   96.00 %.
initial index vector length:        50
compressed index vector length:         1reduction:  98.00%
nwalk=      15 cpos=       2 maxval=    9 cmprfactor=   86.67 %.
nwalk=      15 cpos=       1 maxval=   99 cmprfactor=   86.67 %.
 compressed with: nwalk=      15 cpos=       2 maxval=    9 cmprfactor=   86.67 %.
initial ref vector length:        15
compressed ref vector length:         2reduction:  86.67%
