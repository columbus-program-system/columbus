1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.

  This is a  mraqcc energy evaluation      

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  1  1    -46.2748581090 -1.0769E-14  6.1608E-02  2.2388E-01  1.0000E-04
 mraqcc  #  2  1    -46.3238446165  4.8987E-02  1.5502E-03  3.1238E-02  1.0000E-04
 mraqcc  #  3  1    -46.3255241914  1.6796E-03  1.5993E-04  1.0708E-02  1.0000E-04
 mraqcc  #  4  1    -46.3257071273  1.8294E-04  5.7166E-05  5.6724E-03  1.0000E-04
 mraqcc  #  5  1    -46.3257468714  3.9744E-05  4.2270E-06  1.9209E-03  1.0000E-04
 mraqcc  #  6  1    -46.3257519726  5.1012E-06  2.0905E-07  3.9556E-04  1.0000E-04
 mraqcc  #  7  1    -46.3257521933  2.2069E-07  2.1535E-08  1.0468E-04  1.0000E-04
 mraqcc  #  8  1    -46.3257522200  2.6728E-08  7.5763E-09  6.7966E-05  1.0000E-04

 mraqcc   convergence criteria satisfied after  8 iterations.

 final mraqcc   convergence information:
 mraqcc  #  8  1    -46.3257522200  2.6728E-08  7.5763E-09  6.7966E-05  1.0000E-04

 number of reference csfs (nref) is    15.  root number (iroot) is  1.
 c0**2 =   0.91620899  c0**2(renormalized) =   0.99297907
 c**2 (all zwalks) =   0.91620899  c**2(all zwalks, renormalized) =   0.99297907
