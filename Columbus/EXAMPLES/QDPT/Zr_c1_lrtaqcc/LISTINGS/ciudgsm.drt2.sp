1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.

  This is a  mraqcc energy evaluation      

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  1  1    -46.3008935272  5.3291E-15  6.0428E-02  2.1537E-01  1.0000E-04
 mraqcc  #  2  1    -46.3501090388  4.9216E-02  2.6248E-03  4.5041E-02  1.0000E-04
 mraqcc  #  3  1    -46.3529334911  2.8245E-03  4.1026E-04  1.7280E-02  1.0000E-04
 mraqcc  #  4  1    -46.3532965252  3.6303E-04  8.2594E-05  8.2869E-03  1.0000E-04
 mraqcc  #  5  1    -46.3533642720  6.7747E-05  7.5432E-06  2.3627E-03  1.0000E-04
 mraqcc  #  6  1    -46.3533722177  7.9457E-06  7.4911E-07  7.7739E-04  1.0000E-04
 mraqcc  #  7  1    -46.3533728908  6.7314E-07  1.4226E-07  2.9879E-04  1.0000E-04
 mraqcc  #  8  1    -46.3533730097  1.1890E-07  2.6650E-08  1.3443E-04  1.0000E-04
 mraqcc  #  9  1    -46.3533730351  2.5386E-08  7.6965E-09  7.3765E-05  1.0000E-04

 mraqcc   convergence criteria satisfied after  9 iterations.

 final mraqcc   convergence information:
 mraqcc  #  9  1    -46.3533730351  2.5386E-08  7.6965E-09  7.3765E-05  1.0000E-04

 number of reference csfs (nref) is   105.  root number (iroot) is  1.
 c0**2 =   0.90666663  c0**2(renormalized) =   0.99128888
 c**2 (all zwalks) =   0.90666663  c**2(all zwalks, renormalized) =   0.99128888
