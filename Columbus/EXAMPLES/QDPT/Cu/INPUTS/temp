 
 program cidrt 7.0  

 distinct row table construction, reference csf selection, and internal
 walk selection for multireference single- and double-excitation
configuration interaction.

 references:  r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).
              h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. symp. 15, 91 (1981).

 based on the initial version by  Ron Shepard

 extended for spin-orbit CI calculations ( Russ Pitzer, OSU)

 and large active spaces (Thomas Müller, FZ(21 Juelich)

 This Version of Program CIDRT is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de

*********************** File revision status: ***********************
* cidrt1.F9 Revision: 1.1.6.2           Date: 2013/04/11 14:37:29   * 
* cidrt2.F9 Revision: 1.1.6.5           Date: 2014/11/06 13:11:03   * 
* cidrt3.F9 Revision: 1.1.6.2           Date: 2013/04/11 14:37:29   * 
* cidrt4.F9 Revision: 1.1.6.2           Date: 2013/04/11 14:37:29   * 
********************************************************************

 workspace allocation parameters: lencor= 131072000 mem1=         0 ifirst=         1
 expanded "keystrokes" are being written to file:
 /bigscratch/Columbus_C70/Pert_Cu_SO/cidrtky                                     
 Spin-Orbit CI Calculation?(y,[n]) Spin-Orbit CI Calculation
 
 input the highest multiplicity [  0]: 
 highest spin multiplicity=                     2 odd electron case= T
 input lxyzir(*):
                     4                     3                     2
 input the total number of electrons [  0]: total number of electrons, nelt     :    19
 input the number of irreps (1:8) [  0]: point group dimension, nsym         :     8
 enter symmetry labels:(y,[n]) enter 8 labels (a4):
 enter symmetry label, default=   1
 enter symmetry label, default=   2
 enter symmetry label, default=   3
 enter symmetry label, default=   4
 enter symmetry label, default=   5
 enter symmetry label, default=   6
 enter symmetry label, default=   7
 enter symmetry label, default=   8
 symmetry labels: (symmetry, slabel)
 ( 1,  ag ) ( 2,  b1g) ( 3,  b2g) ( 4,  b3g) ( 5,  au ) ( 6,  b1u) ( 7,  b2u) ( 8,  b3u) 
 input nmpsy(*):
 nmpsy(*)=        10   3   3   3   1   6   6   6
 
   symmetry block summary
 block(*)=         1   2   3   4   5   6   7   8
 slabel(*)=      ag  b1g b2g b3g au  b1u b2u b3u
 nmpsy(*)=        10   3   3   3   1   6   6   6
 
 total molecular orbitals            :    38
 input the molecular spatial symmetry (irrep 1:nsym) [  0]: state spatial symmetry label        :  ag 
 
 input the frozen core orbitals (sym(i),rmo(i),i=1,nfct):
 total frozen core orbitals, nfct    :     4
 
 fcorb(*)=         1  21  27  33
 slabel(*)=      ag  b1u b2u b3u
 
 number of frozen core orbitals      :     4
 number of frozen core electrons     :     8
 number of internal electrons        :    11
 
 input the frozen virtual orbitals (sym(i),rmo(i),i=1,nfvt):
 total frozen virtual orbitals, nfvt :     0

 no frozen virtual orbitals entered
 
 input the internal orbitals (sym(i),rmo(i),i=1,niot):
 niot                                :     6
 
 modrt(*)=         2   3   4  11  14  17
 slabel(*)=      ag  ag  ag  b1g b2g b3g
 
 total number of orbitals            :    38
 number of frozen core orbitals      :     4
 number of frozen virtual orbitals   :     0
 number of internal orbitals         :     6
 number of external orbitals         :    28
 
 orbital-to-level mapping vector
 map(*)=          -1  29  30  31   1   2   3   4   5   6  32   7   8  33   9
                  10  34  11  12  13  -1  14  15  16  17  18  -1  19  20  21
                  22  23  -1  24  25  26  27  28
 
 input the number of ref-csf doubly-occupied orbitals [  0]: (ref) doubly-occupied orbitals      :     0
 
 no. of internal orbitals            :     6
 no. of doubly-occ. (ref) orbitals   :     0
 no. active (ref) orbitals           :     6
 no. of active electrons             :    11
 
 input the active-orbital, active-electron occmnr(*):
   2  3  4 11 14 17
 input the active-orbital, active-electron occmxr(*):
   2  3  4 11 14 17
 
 actmo(*) =        2   3   4  11  14  17
 occmnr(*)=        0   0   0   0   0   9
 occmxr(*)=       11  11  11  11  11  11
 reference csf cumulative electron occupations:
 modrt(*)=         2   3   4  11  14  17
 occmnr(*)=        0   0   0   0   0   9
 occmxr(*)=       11  11  11  11  11  11
 
 input the active-orbital bminr(*):
   2  3  4 11 14 17
 input the active-orbital bmaxr(*):
   2  3  4 11 14 17
 reference csf b-value constraints:
 modrt(*)=         2   3   4  11  14  17
 bminr(*)=         0   0   0   0   0   0
 bmaxr(*)=        11  11  11  11  11  11
 input the active orbital smaskr(*):
   2  3  4 11 14 17
 modrt:smaskr=
   2:1111   3:1111   4:1111  11:1111  14:1111  17:1111
 
 input the maximum excitation level from the reference csfs [  2]: maximum excitation from ref. csfs:  :     2
 number of internal electrons:       :    11
 
 input the internal-orbital mrsdci occmin(*):
   2  3  4 11 14 17
 input the internal-orbital mrsdci occmax(*):
   2  3  4 11 14 17
 mrsdci csf cumulative electron occupations:
 modrt(*)=         2   3   4  11  14  17
 occmin(*)=        0   0   0   0   0   7
 occmax(*)=       11  11  11  11  11  11
 
 input the internal-orbital mrsdci bmin(*):
   2  3  4 11 14 17
 input the internal-orbital mrsdci bmax(*):
   2  3  4 11 14 17
 mrsdci b-value constraints:
 modrt(*)=         2   3   4  11  14  17
 bmin(*)=          0   0   0   0   0   0
 bmax(*)=         11  11  11  11  11  11
 
 input the internal-orbital smask(*):
   2  3  4 11 14 17
 modrt:smask=
   2:1111   3:1111   4:1111  11:1111  14:1111  17:1111
 
 internal orbital summary:
 block(*)=         1   1   1   2   3   4
 slabel(*)=      ag  ag  ag  b1g b2g b3g
 rmo(*)=           2   3   4   1   1   1
 modrt(*)=         2   3   4  11  14  17
 
 reference csf info:
 occmnr(*)=        0   0   0   0   0   9
 occmxr(*)=       11  11  11  11  11  11
 
 bminr(*)=         0   0   0   0   0   0
 bmaxr(*)=        11  11  11  11  11  11
 
 
 mrsdci csf info:
 occmin(*)=        0   0   0   0   0   7
 occmax(*)=       11  11  11  11  11  11
 
 bmin(*)=          0   0   0   0   0   0
 bmax(*)=         11  11  11  11  11  11
 

 a priori removal of distinct rows:

 input the level, a, and b values for the vertices 
 to be removed (-1/ to end).

 input level, a, and b (-1/ to end):
 no vertices marked for removal
 
 impose generalized interacting space restrictions?(y,[n]) generalized interacting space restrictions will not be imposed.
 multp(*)=   1   3
  hmult                     3
 lxyzir   4   3   2
 symmetry of spin functions (spnir)
       --------------------------Ms ----------------------------
   S     1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19
   1     1  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   2     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   3     2  3  4  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   4     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   5     1  4  3  2  1  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   6     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   7     2  3  4  1  2  3  4  0  0  0  0  0  0  0  0  0  0  0  0
   8     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   9     1  4  3  2  1  4  3  2  1  0  0  0  0  0  0  0  0  0  0
  10     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  11     2  3  4  1  2  3  4  1  2  3  4  0  0  0  0  0  0  0  0
  12     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  13     1  4  3  2  1  4  3  2  1  4  3  2  1  0  0  0  0  0  0
  14     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  15     2  3  4  1  2  3  4  1  2  3  4  1  2  3  4  0  0  0  0
  16     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  17     1  4  3  2  1  4  3  2  1  4  3  2  1  4  3  2  1  0  0
  18     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  19     2  3  4  1  2  3  4  1  2  3  4  1  2  3  4  1  2  3  4

 number of rows in the drt :  39

 manual arc removal step:


 input the level, a, b, and step values 
 for the arcs to be removed (-1/ to end).

 input the level, a, b, and step (-1/ to end):
 remarc:   0 arcs removed out of   0 specified.

 xbarz=          24
 xbary=         144
 xbarx=         360
 xbarw=         280
        --------
 nwalk=         808
 input the range of drt levels to print (l1,l2):
 levprt(*)        -1   0

 reference-csf selection step 1:
 total number of z-walks in the drt, nzwalk=      24

 input the list of allowed reference symmetries:
 allowed reference symmetries:             1    2    3    4    5    6    7    8
 allowed reference symmetry labels:      ag   b1g  b2g  b3g  au   b1u  b2u  b3u
 keep all of the z-walks as references?(y,[n]) all z-walks are initially deleted.
 
 generate walks while applying reference drt restrictions?([y],n) reference drt restrictions will be imposed on the z-walks.
 
 impose additional orbital-group occupation restrictions?(y,[n]) 
 apply primary reference occupation restrictions?(y,[n]) 
 manually select individual walks?(y,[n])
 step 1 reference csf selection complete.
       12 csfs initially selected from      24 total walks.

 beginning step-vector based selection.
 enter [internal_orbital_step_vector/disposition] pairs:

 enter internal orbital step vector, (-1/ to end):
   2  3  4 11 14 17  0

 step 2 reference csf selection complete.
       12 csfs currently selected from      24 total walks.

 beginning numerical walk based selection.
 enter positive walk numbers to add walks,
 negative walk numbers to delete walks, and zero to end:

 input reference walk number (0 to end) [  0]:
 numerical walk-number based selection complete.
       12 reference csfs selected from      24 total z-walks.
 
 input the reference occupations, mu(*):
 reference occupations:
 mu(*)=            0   0   0   0   0   0   0
 
                    12  saved step vectors for mult                     2

 exlimw: beginning excitation-based walk selection...
 exlimw: nref=                    12

  number of valid internal walks of each symmetry:

       ag      b1g     b2g     b3g     au      b1u     b2u     b3u
      ----    ----    ----    ----    ----    ----    ----    ----
 z       6       0       0       0       0       0       0       0
 y      36      36      36      36       0       0       0       0
 x      90      90      90      90       0       0       0       0
 w      70      70      70      70       0       0       0       0

 csfs grouped by internal walk symmetry:

       ag      b1g     b2g     b3g     au      b1u     b2u     b3u
      ----    ----    ----    ----    ----    ----    ----    ----
 z       6       0       0       0       0       0       0       0
 y     216      72      72      72       0       0       0       0
 x    4320    4140    4140    4140       0       0       0       0
 w    5320    3220    3220    3220       0       0       0       0

 total csf counts:
 z-vertex:        6
 y-vertex:      432
 x-vertex:    16740
 w-vertex:    14980
           --------
 total:       32158
 copy_limvec: valid walks          6       144       360       280
 crosscheck nvwalkt=                   790 nwalk=                   808
total number of valid walks (nvalw)=         6       144       360       280
 
 this is an obsolete prompt.(y,[n])
 final mrsdci walk selection step:

 nvalw(*)=       6     144     360     280 nvalwt=     790

 enter positive walk numbers to add walks,
 negative walk numbers to delete walks, and zero to end.

 input mrsdci walk number (0 to end) [  0]:
 end of manual mrsdci walk selection.
 number added=   0 number removed=   0

 nvalw(*)=       6     144     360     280 nvalwt=     790

 lprune input numv1,nwalk=                   790                   808
 lprune input xbar(1,1),nref=                    24                    12

 lprune: l(*,*,*) pruned with nwalk=     808 nvalwt=     790=   6 144 360 280
 lprune:  z-drt, nprune=    69
 lprune:  y-drt, nprune=    41
 lprune: wx-drt, nprune=     7

 xbarz=          24
 xbary=         144
 xbarx=         360
 xbarw=         280
        --------
 nwalk=         808
 levprt(*)        -1   0

 beginning the reference csf index recomputation...
 SO case: only a single ms value is used for ref-CSF pattern definition
 odd-electron case: last step vector entry corresponds to S-1(2), S+1(1) case

     iref   iwalk  step-vector
   ------  ------  ------------
        1       1  3333312
        2       2  3333311
        3       5  3333132
        4       6  3333131
        5       9  3331332
        6      10  3331331
        7      13  3313332
        8      14  3313331
        9      17  3133332
       10      18  3133331
       11      21  1333332
       12      22  1333331
 indx01:    12 elements set in vec01(*)

 beginning the valid upper walk index recomputation...
 indx01:   790 elements set in vec01(*)

 beginning the final csym(*) computation...

  number of valid internal walks of each symmetry:

       ag      b1g     b2g     b3g     au      b1u     b2u     b3u
      ----    ----    ----    ----    ----    ----    ----    ----
 z       6       0       0       0       0       0       0       0
 y      36      36      36      36       0       0       0       0
 x      90      90      90      90       0       0       0       0
 w      70      70      70      70       0       0       0       0

 csfs grouped by internal walk symmetry:

       ag      b1g     b2g     b3g     au      b1u     b2u     b3u
      ----    ----    ----    ----    ----    ----    ----    ----
 z       6       0       0       0       0       0       0       0
 y     216      72      72      72       0       0       0       0
 x    4320    4140    4140    4140       0       0       0       0
 w    5320    3220    3220    3220       0       0       0       0

 total csf counts:
 z-vertex:        6
 y-vertex:      432
 x-vertex:    16740
 w-vertex:    14980
           --------
 total:       32158
 
 input a title card, default=cidrt_title
 title card:
  cidrt_title                                                                   
  
 
 input a drt file name, default=cidrtfl
 drt and indexing arrays will be written to file:
 /bigscratch/Columbus_C70/Pert_Cu_SO/cidrtfl                                     
 
 write the drt file?([y],n) drt file is being written...
 wrtstr:  ag  b1g b2g b3g au  b1u b2u b3u
nwalk=     808 cpos=     111 maxval=    9 cmprfactor=   86.26 %.
nwalk=     808 cpos=      21 maxval=   99 cmprfactor=   94.80 %.
nwalk=     808 cpos=      14 maxval=  999 cmprfactor=   94.80 %.
 compressed with: nwalk=     808 cpos=      21 maxval=   99 cmprfactor=   94.80 %.
initial index vector length:       808
compressed index vector length:        21reduction:  97.40%
nwalk=      24 cpos=      12 maxval=    9 cmprfactor=   50.00 %.
nwalk=      24 cpos=      12 maxval=   99 cmprfactor=    0.00 %.
 compressed with: nwalk=      24 cpos=      12 maxval=    9 cmprfactor=   50.00 %.
initial ref vector length:        24
compressed ref vector length:        12reduction:  50.00%
