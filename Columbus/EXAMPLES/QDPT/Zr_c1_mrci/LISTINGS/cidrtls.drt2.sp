 
 program cidrt 7.0  

 distinct row table construction, reference csf selection, and internal
 walk selection for multireference single- and double-excitation
configuration interaction.

 references:  r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).
              h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. symp. 15, 91 (1981).

 based on the initial version by  Ron Shepard

 extended for spin-orbit CI calculations ( Russ Pitzer, OSU)

 and large active spaces (Thomas Müller, FZ(21 Juelich)

 This Version of Program CIDRT is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de

*********************** File revision status: ***********************
* cidrt1.F9 Revision: 1.1.6.2           Date: 2013/04/11 14:37:29   * 
* cidrt2.F9 Revision: 1.1.6.2           Date: 2013/04/11 14:37:29   * 
* cidrt3.F9 Revision: 1.1.6.2           Date: 2013/04/11 14:37:29   * 
* cidrt4.F9 Revision: 1.1.6.2           Date: 2013/04/11 14:37:29   * 
********************************************************************

 workspace allocation parameters: lencor=  13107200 mem1=         0 ifirst=         1
 expanded "keystrokes" are being written to file:
 /bigscratch/Columbus_C70/tests/Zr/scr/WORK/cidrtky                              
 Spin-Orbit CI Calculation?(y,[n]) Spin-Free Calculation
 
 input the spin multiplicity [  0]: spin multiplicity, smult            :   3    triplet 
 input the total number of electrons [  0]: total number of electrons, nelt     :    12
 input the number of irreps (1:8) [  0]: point group dimension, nsym         :     1
 enter symmetry labels:(y,[n]) enter 1 labels (a4):
 enter symmetry label, default=   1
 symmetry labels: (symmetry, slabel)
 ( 1,  a  ) 
 input nmpsy(*):
 nmpsy(*)=        38
 
   symmetry block summary
 block(*)=         1
 slabel(*)=      a  
 nmpsy(*)=        38
 
 total molecular orbitals            :    38
 input the molecular spatial symmetry (irrep 1:nsym) [  0]: state spatial symmetry label        :  a  
 
 input the frozen core orbitals (sym(i),rmo(i),i=1,nfct):
 total frozen core orbitals, nfct    :     4
 
 fcorb(*)=         1   2   3   4
 slabel(*)=      a   a   a   a  
 
 number of frozen core orbitals      :     4
 number of frozen core electrons     :     8
 number of internal electrons        :     4
 
 input the frozen virtual orbitals (sym(i),rmo(i),i=1,nfvt):
 total frozen virtual orbitals, nfvt :     0

 no frozen virtual orbitals entered
 
 input the internal orbitals (sym(i),rmo(i),i=1,niot):
 niot                                :     6
 
 modrt(*)=         5   6   7   8   9  10
 slabel(*)=      a   a   a   a   a   a  
 
 total number of orbitals            :    38
 number of frozen core orbitals      :     4
 number of frozen virtual orbitals   :     0
 number of internal orbitals         :     6
 number of external orbitals         :    28
 
 orbital-to-level mapping vector
 map(*)=          -1  -1  -1  -1  29  30  31  32  33  34   1   2   3   4   5
                   6   7   8   9  10  11  12  13  14  15  16  17  18  19  20
                  21  22  23  24  25  26  27  28
 
 input the number of ref-csf doubly-occupied orbitals [  0]: (ref) doubly-occupied orbitals      :     0
 
 no. of internal orbitals            :     6
 no. of doubly-occ. (ref) orbitals   :     0
 no. active (ref) orbitals           :     6
 no. of active electrons             :     4
 
 input the active-orbital, active-electron occmnr(*):
   5  6  7  8  9 10
 input the active-orbital, active-electron occmxr(*):
   5  6  7  8  9 10
 
 actmo(*) =        5   6   7   8   9  10
 occmnr(*)=        0   0   0   0   0   4
 occmxr(*)=        4   4   4   4   4   4
 reference csf cumulative electron occupations:
 modrt(*)=         5   6   7   8   9  10
 occmnr(*)=        0   0   0   0   0   4
 occmxr(*)=        4   4   4   4   4   4
 
 input the active-orbital bminr(*):
   5  6  7  8  9 10
 input the active-orbital bmaxr(*):
   5  6  7  8  9 10
 reference csf b-value constraints:
 modrt(*)=         5   6   7   8   9  10
 bminr(*)=         0   0   0   0   0   0
 bmaxr(*)=         4   4   4   4   4   4
 input the active orbital smaskr(*):
   5  6  7  8  9 10
 modrt:smaskr=
   5:1111   6:1111   7:1111   8:1111   9:1111  10:1111
 
 input the maximum excitation level from the reference csfs [  2]: maximum excitation from ref. csfs:  :     2
 number of internal electrons:       :     4
 
 input the internal-orbital mrsdci occmin(*):
   5  6  7  8  9 10
 input the internal-orbital mrsdci occmax(*):
   5  6  7  8  9 10
 mrsdci csf cumulative electron occupations:
 modrt(*)=         5   6   7   8   9  10
 occmin(*)=        0   0   0   0   0   2
 occmax(*)=        4   4   4   4   4   4
 
 input the internal-orbital mrsdci bmin(*):
   5  6  7  8  9 10
 input the internal-orbital mrsdci bmax(*):
   5  6  7  8  9 10
 mrsdci b-value constraints:
 modrt(*)=         5   6   7   8   9  10
 bmin(*)=          0   0   0   0   0   0
 bmax(*)=          4   4   4   4   4   4
 
 input the internal-orbital smask(*):
   5  6  7  8  9 10
 modrt:smask=
   5:1111   6:1111   7:1111   8:1111   9:1111  10:1111
 
 internal orbital summary:
 block(*)=         1   1   1   1   1   1
 slabel(*)=      a   a   a   a   a   a  
 rmo(*)=           5   6   7   8   9  10
 modrt(*)=         5   6   7   8   9  10
 
 reference csf info:
 occmnr(*)=        0   0   0   0   0   4
 occmxr(*)=        4   4   4   4   4   4
 
 bminr(*)=         0   0   0   0   0   0
 bmaxr(*)=         4   4   4   4   4   4
 
 
 mrsdci csf info:
 occmin(*)=        0   0   0   0   0   2
 occmax(*)=        4   4   4   4   4   4
 
 bmin(*)=          0   0   0   0   0   0
 bmax(*)=          4   4   4   4   4   4
 

 a priori removal of distinct rows:

 input the level, a, and b values for the vertices 
 to be removed (-1/ to end).

 input level, a, and b (-1/ to end):
 no vertices marked for removal
 
 impose generalized interacting space restrictions?(y,[n]) generalized interacting space restrictions will not be imposed.
 multp(*)=
  hmult                     0
 lxyzir   0   0   0
 symmetry of spin functions (spnir)
       --------------------------Ms ----------------------------
   S     1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19
   1     1  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   2     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   3     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   4     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   5     1  0  0  0  1  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   6     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   7     0  0  0  1  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   8     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   9     1  0  0  0  1  0  0  0  1  0  0  0  0  0  0  0  0  0  0
  10     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  11     0  0  0  1  0  0  0  1  0  0  0  0  0  0  0  0  0  0  0
  12     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  13     1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  0  0  0
  14     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  15     0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  0  0  0  0
  16     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  17     1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0
  18     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  19     0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0

 number of rows in the drt :  36

 manual arc removal step:


 input the level, a, b, and step values 
 for the arcs to be removed (-1/ to end).

 input the level, a, b, and step (-1/ to end):
 remarc:   0 arcs removed out of   0 specified.

 xbarz=         105
 xbary=          90
 xbarx=          36
 xbarw=          15
        --------
 nwalk=         246
 input the range of drt levels to print (l1,l2):
 levprt(*)        -1   0

 reference-csf selection step 1:
 total number of z-walks in the drt, nzwalk=     105

 input the list of allowed reference symmetries:
 allowed reference symmetries:             1
 allowed reference symmetry labels:      a  
 keep all of the z-walks as references?(y,[n]) y or n required
 keep all of the z-walks as references?(y,[n]) all z-walks are initially deleted.
 
 generate walks while applying reference drt restrictions?([y],n) reference drt restrictions will be imposed on the z-walks.
 
 impose additional orbital-group occupation restrictions?(y,[n]) 
 apply primary reference occupation restrictions?(y,[n]) 
 manually select individual walks?(y,[n])
 step 1 reference csf selection complete.
      105 csfs initially selected from     105 total walks.

 beginning step-vector based selection.
 enter [internal_orbital_step_vector/disposition] pairs:

 enter internal orbital step vector, (-1/ to end):
   5  6  7  8  9 10

 step 2 reference csf selection complete.
      105 csfs currently selected from     105 total walks.

 beginning numerical walk based selection.
 enter positive walk numbers to add walks,
 negative walk numbers to delete walks, and zero to end:

 input reference walk number (0 to end) [  0]:
 numerical walk-number based selection complete.
      105 reference csfs selected from     105 total z-walks.
 
 input the reference occupations, mu(*):
 reference occupations:
 mu(*)=            2   0   0   0   0   0
 
 number of step vectors saved:    105

 exlimw: beginning excitation-based walk selection...
 exlimw: nref=                   105

  number of valid internal walks of each symmetry:

       a  
      ----
 z     105
 y      90
 x      36
 w      15

 csfs grouped by internal walk symmetry:

       a  
      ----
 z     105
 y    2520
 x   13608
 w    6090

 total csf counts:
 z-vertex:      105
 y-vertex:     2520
 x-vertex:    13608
 w-vertex:     6090
           --------
 total:       22323
 
 this is an obsolete prompt.(y,[n])
 final mrsdci walk selection step:

 nvalw(*)=     105      90      36      15 nvalwt=     246

 enter positive walk numbers to add walks,
 negative walk numbers to delete walks, and zero to end.

 input mrsdci walk number (0 to end) [  0]:
 end of manual mrsdci walk selection.
 number added=   0 number removed=   0

 nvalw(*)=     105      90      36      15 nvalwt=     246

 lprune input numv1,nwalk=                   246                   246
 lprune input xbar(1,1),nref=                   105                   105

 lprune: l(*,*,*) pruned with nwalk=     246 nvalwt=     246= 105  90  36  15
 lprune:  z-drt, nprune=    22
 lprune:  y-drt, nprune=    20
 lprune: wx-drt, nprune=    29

 xbarz=         105
 xbary=          90
 xbarx=          36
 xbarw=          15
        --------
 nwalk=         246
 levprt(*)        -1   0

 beginning the reference csf index recomputation...

     iref   iwalk  step-vector
   ------  ------  ------------
        1       1  311000
        2       2  310100
        3       3  310010
        4       4  310001
        5       5  301100
        6       6  301010
        7       7  301001
        8       8  300110
        9       9  300101
       10      10  300011
       11      11  131000
       12      12  130100
       13      13  130010
       14      14  130001
       15      15  121100
       16      16  121010
       17      17  121001
       18      18  120110
       19      19  120101
       20      20  120011
       21      21  113000
       22      22  112100
       23      23  112010
       24      24  112001
       25      25  111200
       26      26  111020
       27      27  111002
       28      28  110300
       29      29  110210
       30      30  110201
       31      31  110120
       32      32  110102
       33      33  110030
       34      34  110021
       35      35  110012
       36      36  110003
       37      37  103100
       38      38  103010
       39      39  103001
       40      40  102110
       41      41  102101
       42      42  102011
       43      43  101300
       44      44  101210
       45      45  101201
       46      46  101120
       47      47  101102
       48      48  101030
       49      49  101021
       50      50  101012
       51      51  101003
       52      52  100310
       53      53  100301
       54      54  100211
       55      55  100130
       56      56  100121
       57      57  100112
       58      58  100103
       59      59  100031
       60      60  100013
       61      61  031100
       62      62  031010
       63      63  031001
       64      64  030110
       65      65  030101
       66      66  030011
       67      67  013100
       68      68  013010
       69      69  013001
       70      70  012110
       71      71  012101
       72      72  012011
       73      73  011300
       74      74  011210
       75      75  011201
       76      76  011120
       77      77  011102
       78      78  011030
       79      79  011021
       80      80  011012
       81      81  011003
       82      82  010310
       83      83  010301
       84      84  010211
       85      85  010130
       86      86  010121
       87      87  010112
       88      88  010103
       89      89  010031
       90      90  010013
       91      91  003110
       92      92  003101
       93      93  003011
       94      94  001310
       95      95  001301
       96      96  001211
       97      97  001130
       98      98  001121
       99      99  001112
      100     100  001103
      101     101  001031
      102     102  001013
      103     103  000311
      104     104  000131
      105     105  000113
 indx01:   105 elements set in vec01(*)

 beginning the valid upper walk index recomputation...
 indx01:   246 elements set in vec01(*)

 beginning the final csym(*) computation...

  number of valid internal walks of each symmetry:

       a  
      ----
 z     105
 y      90
 x      36
 w      15

 csfs grouped by internal walk symmetry:

       a  
      ----
 z     105
 y    2520
 x   13608
 w    6090

 total csf counts:
 z-vertex:      105
 y-vertex:     2520
 x-vertex:    13608
 w-vertex:     6090
           --------
 total:       22323
 
 input a title card, default=cidrt_title
 title card:
  cidrt_title                                                                   
  
 
 input a drt file name, default=cidrtfl
 drt and indexing arrays will be written to file:
 /bigscratch/Columbus_C70/tests/Zr/scr/WORK/cidrtfl                              
 
 write the drt file?([y],n) drt file is being written...
 wrtstr:  a  
nwalk=     246 cpos=      31 maxval=    9 cmprfactor=   87.40 %.
nwalk=     246 cpos=       3 maxval=   99 cmprfactor=   97.56 %.
nwalk=     246 cpos=       1 maxval=  999 cmprfactor=   98.78 %.
nwalk=     246 cpos=       1 maxval= 9999 cmprfactor=   98.37 %.
 compressed with: nwalk=     246 cpos=       1 maxval=  999 cmprfactor=   98.78 %.
initial index vector length:       246
compressed index vector length:         1reduction:  99.59%
nwalk=     105 cpos=      13 maxval=    9 cmprfactor=   87.62 %.
nwalk=     105 cpos=       2 maxval=   99 cmprfactor=   96.19 %.
nwalk=     105 cpos=       1 maxval=  999 cmprfactor=   97.14 %.
 compressed with: nwalk=     105 cpos=       2 maxval=   99 cmprfactor=   96.19 %.
initial ref vector length:       105
compressed ref vector length:         2reduction:  98.10%
