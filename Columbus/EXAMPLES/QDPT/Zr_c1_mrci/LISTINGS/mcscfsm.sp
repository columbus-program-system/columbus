 total ao core energy =    0.000000000
 MCSCF calculation performed for  1 DRT.

 DRT  first state   no.of aver. states   weights
  1   ground state          7             0.143 0.143 0.143 0.143 0.143 0.143 0.143

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a  
 Total number of electrons:    12
 Spin multiplicity:            3
 Number of active orbitals:    5
 Number of active electrons:   2
 Total number of CSFs:        10

 Number of active-double rotations:        25
 Number of active-active rotations:         0
 Number of double-virtual rotations:      140
 Number of active-virtual rotations:      140
 
 iter     emc (average)    demc       wnorm      knorm      apxde  qcoupl
    1    -45.9835589188  4.598E+01  8.828E-01  1.000E+00  1.908E-01  F   *not conv.*     
    2    -46.0783536185  9.479E-02  3.621E-01  7.276E-01  9.186E-02  F   *not conv.*     
    3    -46.2056959869  1.273E-01  1.870E-01  6.536E-01  6.164E-02  F   *not conv.*     
    4    -46.2814067240  7.571E-02  1.263E-01  2.989E-01  1.365E-02  F   *not conv.*     
    5    -46.2968098978  1.540E-02  1.957E-02  5.710E-02  4.360E-04  F   *not conv.*     
    6    -46.2972551432  4.452E-04  1.181E-03  1.384E-03  3.771E-07  F   *not conv.*     
    7    -46.2972555321  3.890E-07  6.609E-05  5.260E-05  6.842E-10  F   *not conv.*     

 final mcscf convergence values:
    8    -46.2972555329  7.238E-10  3.848E-06  2.989E-06  2.247E-12  T   *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 wt 0.143 total energy=      -46.297255588, rel. (eV)=   0.000000
   DRT #1 state # 2 wt 0.143 total energy=      -46.297255588, rel. (eV)=   0.000000
   DRT #1 state # 3 wt 0.143 total energy=      -46.297255533, rel. (eV)=   0.000001
   DRT #1 state # 4 wt 0.143 total energy=      -46.297255533, rel. (eV)=   0.000001
   DRT #1 state # 5 wt 0.143 total energy=      -46.297255500, rel. (eV)=   0.000002
   DRT #1 state # 6 wt 0.143 total energy=      -46.297255500, rel. (eV)=   0.000002
   DRT #1 state # 7 wt 0.143 total energy=      -46.297255489, rel. (eV)=   0.000003
   ------------------------------------------------------------


