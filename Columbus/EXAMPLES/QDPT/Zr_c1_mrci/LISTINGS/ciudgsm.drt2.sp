1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=   105)...
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.3008935272 -4.4409E-16  5.9377E-02  2.1537E-01  1.0000E-04

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  1  1    -46.3008935272 -4.4409E-16  5.9377E-02  2.1537E-01  1.0000E-04

 from bk iterations: iconv=   1

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.3494814752  4.8588E-02  2.5017E-03  4.4348E-02  1.0000E-04
 mr-sdci #  2  1    -46.3521721955  2.6907E-03  3.7925E-04  1.6751E-02  1.0000E-04
 mr-sdci #  3  1    -46.3525052417  3.3305E-04  7.4730E-05  7.9042E-03  1.0000E-04
 mr-sdci #  4  1    -46.3525665286  6.1287E-05  6.5577E-06  2.2293E-03  1.0000E-04
 mr-sdci #  5  1    -46.3525734414  6.9128E-06  6.4199E-07  7.2349E-04  1.0000E-04
 mr-sdci #  6  1    -46.3525740122  5.7079E-07  1.2417E-07  2.7852E-04  1.0000E-04
 mr-sdci #  7  1    -46.3525741162  1.0402E-07  2.3218E-08  1.2540E-04  1.0000E-04
 mr-sdci #  8  1    -46.3525741381  2.1882E-08  6.7867E-09  6.9917E-05  1.0000E-04

 mr-sdci  convergence criteria satisfied after  8 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  8  1    -46.3525741381  2.1882E-08  6.7867E-09  6.9917E-05  1.0000E-04

 number of reference csfs (nref) is   105.  root number (iroot) is  1.
 c0**2 =   0.91059980  c**2 (all zwalks) =   0.91059980

 eref      =    -46.300864681998   "relaxed" cnot**2         =   0.910599797521
 eci       =    -46.352574138102   deltae = eci - eref       =  -0.051709456104
 eci+dv1   =    -46.357196973948   dv1 = (1-cnot**2)*deltae  =  -0.004622835846
 eci+dv2   =    -46.357650831352   dv2 = dv1 / cnot**2       =  -0.005076693250
 eci+dv3   =    -46.358203507471   dv3 = dv1 / (2*cnot**2-1) =  -0.005629369369
 eci+pople =    -46.355106397390   (  4e- scaled deltae )    =  -0.054241715392
