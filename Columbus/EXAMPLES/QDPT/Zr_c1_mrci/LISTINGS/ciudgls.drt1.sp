1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      2009-03.    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


================ Computing sorting integral file structure ================

                    -----z----- -----y----- -----x----- -----w----- ---total---

                CSFs       105        1960        5670        8526       16261
      internal walks       105          70          15          21         211
valid internal walks       105          70          15          21         211
 getinfoarray: info=                     6 :                     1
                  8192                  6552                  8192
                  5460                     0
 icd(3)=                  1191 ci%nnlev=                   595  l2rec=
                  8192  n2max=                  5460
 lcore1,lcore2=              13069922              13046606
 lencor,maxblo              13107200                 60000
========================================
 current settings:
 minbl3        1215
 minbl4        1215
 locmaxbl3    22684
 locmaxbuf    11342
 maxbl3       60000
 maxbl3       60000
 maxbl4       60000
 maxbuf       30006
========================================

 sorted 4-external integrals:     3 records of integral w-combinations 
                                  3 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)

 sorted 3-external integrals:     3 records of integral w-combinations 
                                  3 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)
 Orig.  diagonal integrals:  1electron:        34
                             0ext.    :        42
                             2ext.    :       336
                             4ext.    :       812


 Orig. off-diag. integrals:  4ext.    :     93177
                             3ext.    :     72576
                             2ext.    :     25578
                             1ext.    :      4704
                             0ext.    :       315
                             2ext. SO :         0
                             1ext. SO :         0
                             0ext. SO :         0
                             1electron:       406


 Sorted integrals            3ext.  w :     68040 x :     63504
                             4ext.  w :     82215 x :     71253


Cycle #  1 sortfile size=    393204(      12 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13069922
Cycle #  2 sortfile size=    393204(      12 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13069922
 minimum size of srtscr:    327670 WP (    10 records)
 maximum size of srtscr:    393204 WP (    12 records)
diagi   file:      4 records  of   1536 WP each=>       6144 WP total
ofdgi   file:     23 records  of   1536 WP each=>      35328 WP total
fil3w   file:      3 records  of  30006 WP each=>      90018 WP total
fil3x   file:      3 records  of  30006 WP each=>      90018 WP total
fil4w   file:      3 records  of  30006 WP each=>      90018 WP total
fil4x   file:      3 records  of  30006 WP each=>      90018 WP total
 compressed index vector length=                     1
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 0,
  GSET = 0,
   DAVCOR =10,
  NCOREL = 4
   nroot=1
  IVMODE = 3
  NBKITR = 1
  RTOLBK = 1e-4,1e-4,1e-4,1e-4,1e-4,1e-4,1e-4,1e-4,1e-4,
  NITER = 200
   rtolci=1e-4
  NVCIMX = 25
  NVRFMX = 25
  NVBKMX = 25
  IDEN  = 1
  CSFPRN = 10,
  update_mode=1
  nvcimn=2
  nvrfmn=2
  nvbkmn=2
 /&end
 ------------------------------------------------------------------------
lodens (list->root)=  1
invlodens (root->list)=  1
 USING SEGMENTS OF EQUAL SIZE

****************  list of control variables  ****************
 lvlprt =    0      nroot  =    1      noldv  =   0      noldhv =   0
 nunitv =    1      nbkitr =    1      niter  = 200      davcor =  10
 csfprn =   10      ivmode =    3      istrt  =   0      vout   =   0
 iortls =    0      nvbkmx =   25      ibktv  =  -1      ibkthv =  -1
 nvcimx =   25      icitv  =   -1      icithv =  -1      frcsub =   0
 nvbkmn =    2      nvcimn =    2      maxseg =   4      nrfitr =  30
 ncorel =    4      nvrfmx =   25      nvrfmn =   2      iden   =   1
 itran  =    0      froot  =    0      rtmode =   0      ncouple=   1
 skipso =    F      dalton2=    0      molcas =   0      finalv =   0
 finalw =    0      cosmocalc=   0    with_tsklst=   0
 nsegwx =    1     1     1     1
 nseg0x =    1     1     1     1
 nseg1x =    1     1     1     1
 nseg2x =    1     1     1     1
 nseg3x =    1     1     1     1
 nseg4x =    1     1     1     1
 no0ex  =      0    no1ex  =      0    no2ex  =     0    no3ex  =     0
 no4ex  =      0    nodiag =      0
 cdg4ex =    1      c3ex1ex=    1      c2ex0ex=   1
 fileloc=    0     0     0     0     0     0     0     1     1     1
 directhd=   1      noaqccshift_zyxw=      0
 critical_crit=-1.00000    critical_delta= 0.05000

 ctol   = 0.010000    lrtshift=1.000000    smalld =0.001000


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-04    1.000E-04
 Computing density:                    .drt1.state1
 using                      1  nodes and                      1  cores.
 szdg/szodg per processor=                  6144                 35328
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core           13107199 DP per process

********** Integral sort section *************


 workspace allocation information: lencor=  13107199

 echo of the input for program cisrt:
 ------------------------------------------------------------------------
  &input
  maxbl3=60000
  maxbl4=60000
  &end
 ------------------------------------------------------------------------
 
 ( 6) listing file:                    ciudgls             
 ( 5) input file:                      cisrtin   
 (17) cidrt file:                      cidrtfl             
 (11) transformed integrals file:      moints    
 (12) diagonal integral file:          diagint             
 (13) off-diagonal integral file:      ofdgint             
 (31) 4-external w integrals file:     fil4w               
 (32) 4-external x integrals file:     fil4x               
 (33) 3-external w integrals file:     fil3w               
 (34) 3-external x integrals file:     fil3x               
 (21) scratch da sorting file:         srtscr              
 (12) 2-e integral file [fsplit=2]:    moints2   

 input integral file header information:
                                                                                 
 aoints SIFS file created by argos.      zam792            16:57:17.826 17-Dec-13
  cidrt_title                                                                    
 MO-coefficients from mcscf.x                                                    
  with dummy occupation 1.0 for active orbitals                                  
  total ao core energy =    0.000000000                                          
 MCSCF energy =     -46.297255533                                                
 SIFS file created by program tran.      zam792            16:57:18.312 17-Dec-13

 input energy(*) values:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total core energy =   0.000000000000E+00

 nsym = 1 nmot=  34

 symmetry  =    1
 slabel(*) =    a
 nmpsy(*)  =   34

 info(*) =          1      8192      6552      8192      5460         0

 orbital labels, i:molab(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012  13:tout:013  14:tout:014  15:tout:015  16:tout:016  17:tout:017  18:tout:018  19:tout:019  20:tout:020
  21:tout:021  22:tout:022  23:tout:023  24:tout:024  25:tout:025  26:tout:026  27:tout:027  28:tout:028  29:tout:029  30:tout:030
  31:tout:031  32:tout:032  33:tout:033  34:tout:034

 input parameters:
 prnopt=  0
 ldamin=    4095 ldamax=   32767 ldainc=      64
 maxbuf=   30006 maxbl3=   60000 maxbl4=   60000 intmxo=    1536
  Using 32 bit compression 

 drt information:
  cidrt_title                                                                    
 nmotd =  38 nfctd =   4 nfvtc =   0 nmot  =  34
 nlevel =  34 niot  =   6 lowinl=  29
 orbital-to-level map(*)
   -1  -1  -1  -1  29  30  31  32  33  34   1   2   3   4   5   6   7   8   9  10
   11  12  13  14  15  16  17  18  19  20  21  22  23  24  25  26  27  28
 compressed map(*)
   29  30  31  32  33  34   1   2   3   4   5   6   7   8   9  10  11  12  13  14
   15  16  17  18  19  20  21  22  23  24  25  26  27  28
 levsym(*)
    1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1
    1   1   1   1   1   1   1   1   1   1   1   1   1   1
 repartitioning mu(*)=
   2.  0.  0.  0.  0.  0.

 new core energy added to the energy(*) list.
 from the integral file: h1_core= -4.355613553355E+01

 indxdg: diagonal integral statistics.
 total number of integrals contributing to diagonal matrix elements:      1190
 number with all external indices:       812
 number with half external - half internal indices:       336
 number with all internal indices:        42

 indxof: off-diagonal integral statistics.
    4-external integrals: num=      93177 strt=          1
    3-external integrals: num=      72576 strt=      93178
    2-external integrals: num=      25578 strt=     165754
    1-external integrals: num=       4704 strt=     191332
    0-external integrals: num=        315 strt=     196036

 total number of off-diagonal integrals:      196350


 indxof(2nd)  ittp=   3 numx(ittp)=       25578
 indxof(2nd)  ittp=   4 numx(ittp)=        4704
 indxof(2nd)  ittp=   5 numx(ittp)=         315

 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg=  12931980
 pro2e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652

 pro2e:     90013 integrals read in    17 records.

 pro2e:         0 integrals 34-ext integrals skipped.
 pro1e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652
 pro1e: eref =   -1.721733008562787E+00
 total size of srtscr:                     7  records of                  32767 
 WP =               1834952 Bytes

 new core energy added to the energy(*) list.
 from the hamiltonian repartitioning, eref= -1.721733008563E+00
 putdg        1     596    1191    1786    3322   36089   57934    3018   46706   90394
   123161  131353  136813  158652

 putf:       4 buffers of length    1536 written to file 12
 diagonal integral file completed.

 putd34:     3 records of integral w-combinations and
             3 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep4e:     3 blocks of linear combinations of 4-external integrals processed.
 number of sorted 4-external integrals     153468
 number of original 4-external integrals    93177


 putf34: external integral file complete. nfilw=    31 nfilx=    32 nrecw=     3 nrecx=     3 lbufp= 30006

 putd34:     3 records of integral w-combinations and
             3 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep3e:     6 blocks of linear combinations of 3-external integrals processed.
 number of sorted 3-external integrals     131544
 number of original 3-external integrals    72576


 putf34: external integral file complete. nfilw=    33 nfilx=    34 nrecw=     3 nrecx=     3 lbufp= 30006
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd     25578         3    165754    165754     25578    196350
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      4704         4    191332    191332      4704    196350
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd       315         5    196036    196036       315    196350

 putf:      23 buffers of length    1536 written to file 13
 off-diagonal files sort completed.
 executing brd_struct for cisrtinfo
cisrtinfo:
bufszi  1536
 diagfile 4ext:     812 2ext:     336 0ext:      42
 fil4w,fil4x  :   93177 fil3w,fil3x :   72576
 ofdgint  2ext:   25578 1ext:    4704 0ext:     315so0ext:       0so1ext:       0so2ext:       0
buffer minbl4    1215 minbl3    1215 maxbl2    1218nbas:  28   0   0   0   0   0   0   0 maxbuf 30006
 CIUDG version 5.9.7 ( 5-Oct-2004)

 workspace allocation information: lcore=  13107199

 core energy values from the integral file:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -4.355613553355E+01, ietype=    6,   fcore energy of type: H1(*)   
 energy( 3)= -1.721733008563E+00, ietype=    5,   fcore energy of type: Vref(*) 

 total core repulsion energy = -4.527786854211E+01
 nmot  =    38 niot  =     6 nfct  =     4 nfvt  =     0
 nrow  =    32 nsym  =     1 ssym  =     1 lenbuf=  1600
 nwalk,xbar:        211      105       70       15       21
 nvalwt,nvalw:      211      105       70       15       21
 ncsft:           16261
 total number of valid internal walks:     211
 nvalz,nvaly,nvalx,nvalw =      105      70      15      21

 cisrt info file parameters:
 file number  12 blocksize   1536
 mxbld   1536
 nd4ext,nd2ext,nd0ext   812   336    42
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int    93177    72576    25578     4704      315        0        0        0
 minbl4,minbl3,maxbl2  1215  1215  1218
 maxbuf 30006
 number of external orbitals per symmetry block:  28
 nmsym   1 number of internal orbitals   6
 executing brd_struct for drt
 executing brd_struct for orbinf
 executing brd_struct for momap
 calcthrxt: niot,maxw1=                     6                    31
 block size     0
 pthz,pthy,pthx,pthw:   105    70    15    21 total internal walks:     211
 maxlp3,n2lp,n1lp,n0lp    31     0     0     0
 orbsym(*)= 1 1 1 1 1 1

 setref:      105 references kept,
                0 references were marked as invalid, out of
              105 total.
 nmb.of records onel     1
 nmb.of records 2-ext    17
 nmb.of records 1-ext     4
 nmb.of records 0-ext     1
 nmb.of records 2-int     0
 nmb.of records 1-int     0
 nmb.of records 0-int     0
 ---------memory usage in DP -----------------
 < n-ex core usage >
     routines:
    fourex            62738
    threx             60199
    twoex              9922
    onex               3217
    allin              1536
    diagon             2109
               =======
   maximum            62738
 
  __ static summary __ 
   reflst               105
   hrfspc               105
               -------
   static->             105
 
  __ core required  __ 
   totstc               105
   max n-ex           62738
               -------
   totnec->           62843
 
  __ core available __ 
   totspc          13107199
   totnec -           62843
               -------
   totvec->        13044356

 number of external paths / symmetry
 vertex x     378
 vertex w     406
segment: free space=    13044356
 reducing frespc by                   928 to               13043428 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1         105|       105|         0|       105|         0|         1|
 -------------------------------------------------------------------------------
  Y 2          70|      1960|       105|        70|       105|         2|
 -------------------------------------------------------------------------------
  X 3          15|      5670|      2065|        15|       175|         3|
 -------------------------------------------------------------------------------
  W 4          21|      8526|      7735|        21|       190|         4|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=           4DP  conft+indsym=         420DP  drtbuffer=         504 DP

dimension of the ci-matrix ->>>     16261

 executing brd_struct for civct
 gentasklist: ntask=                    20
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS   
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      15     105       5670        105      15     105
     2  4   1    25      two-ext wz   2X  4 1      21     105       8526        105      21     105
     3  4   3    26      two-ext wx*  WX  4 3      21      15       8526       5670      21      15
     4  4   3    27      two-ext wx+  WX  4 3      21      15       8526       5670      21      15
     5  2   1    11      one-ext yz   1X  2 1      70     105       1960        105      70     105
     6  3   2    15      1ex3ex yx    3X  3 2      15      70       5670       1960      15      70
     7  4   2    16      1ex3ex yw    3X  4 2      21      70       8526       1960      21      70
     8  1   1     1      allint zz    OX  1 1     105     105        105        105     105     105
     9  2   2     5      0ex2ex yy    OX  2 2      70      70       1960       1960      70      70
    10  3   3     6      0ex2ex xx*   OX  3 3      15      15       5670       5670      15      15
    11  3   3    18      0ex2ex xx+   OX  3 3      15      15       5670       5670      15      15
    12  4   4     7      0ex2ex ww*   OX  4 4      21      21       8526       8526      21      21
    13  4   4    19      0ex2ex ww+   OX  4 4      21      21       8526       8526      21      21
    14  2   2    42      four-ext y   4X  2 2      70      70       1960       1960      70      70
    15  3   3    43      four-ext x   4X  3 3      15      15       5670       5670      15      15
    16  4   4    44      four-ext w   4X  4 4      21      21       8526       8526      21      21
    17  1   1    75      dg-024ext z  OX  1 1     105     105        105        105     105     105
    18  2   2    76      dg-024ext y  OX  2 2      70      70       1960       1960      70      70
    19  3   3    77      dg-024ext x  OX  3 3      15      15       5670       5670      15      15
    20  4   4    78      dg-024ext w  OX  4 4      21      21       8526       8526      21      21
----------------------------------------------------------------------------------------------------
REDTASK #   1 TIME=  19.000 N=  1 (task/type/sgbra)=(   1/24/0) (
REDTASK #   2 TIME=  18.000 N=  1 (task/type/sgbra)=(   2/25/0) (
REDTASK #   3 TIME=  17.000 N=  1 (task/type/sgbra)=(   3/26/1) (
REDTASK #   4 TIME=  16.000 N=  1 (task/type/sgbra)=(   4/27/2) (
REDTASK #   5 TIME=  15.000 N=  1 (task/type/sgbra)=(   5/11/0) (
REDTASK #   6 TIME=  14.000 N=  1 (task/type/sgbra)=(   6/15/0) (
REDTASK #   7 TIME=  13.000 N=  1 (task/type/sgbra)=(   7/16/0) (
REDTASK #   8 TIME=  12.000 N=  1 (task/type/sgbra)=(   8/ 1/0) (
REDTASK #   9 TIME=  11.000 N=  1 (task/type/sgbra)=(   9/ 5/0) (
REDTASK #  10 TIME=  10.000 N=  1 (task/type/sgbra)=(  10/ 6/1) (
REDTASK #  11 TIME=   9.000 N=  1 (task/type/sgbra)=(  11/18/2) (
REDTASK #  12 TIME=   8.000 N=  1 (task/type/sgbra)=(  12/ 7/1) (
REDTASK #  13 TIME=   7.000 N=  1 (task/type/sgbra)=(  13/19/2) (
REDTASK #  14 TIME=   6.000 N=  1 (task/type/sgbra)=(  14/42/1) (
REDTASK #  15 TIME=   5.000 N=  1 (task/type/sgbra)=(  15/43/1) (
REDTASK #  16 TIME=   4.000 N=  1 (task/type/sgbra)=(  16/44/1) (
REDTASK #  17 TIME=   3.000 N=  1 (task/type/sgbra)=(  17/75/1) (
REDTASK #  18 TIME=   2.000 N=  1 (task/type/sgbra)=(  18/76/1) (
REDTASK #  19 TIME=   1.000 N=  1 (task/type/sgbra)=(  19/77/1) (
REDTASK #  20 TIME=   0.000 N=  1 (task/type/sgbra)=(  20/78/1) (
 initializing v-file: 1:                 16261

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         865 2x:           0 4x:           0
All internal counts: zz :        5410 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 reference space has dimension     105
 dsyevx: computed roots 1 to    2(converged:   2)

    root           eigenvalues
    ----           ------------
       1         -46.2712888828
       2         -46.2712888476

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=                   105

         vector  1 from unit 11 written to unit 49 filename cirefv              

 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=   105)...
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:             16261
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:              25
 number of roots to converge:                             1
 number of iterations:                                    1
 residual norm convergence criteria:               0.000100

          starting bk iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:        1490 2x:         305 4x:         106
All internal counts: zz :        5410 yy:           0 xx:           0 ww:           0
One-external counts: yz :        4477 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:         285 wz:         435 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1
   ht   1    -0.99342034

          calcsovref: tciref block   1

              civs   1
 refs   1    1.00000    

          calcsovref: scrb block   1

                ci   1
 civs   1    1.00000    

          calcsovref: sovref block   1

              v      1
 ref    1    1.00000    

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.2712888828 -7.7716E-15  6.4597E-02  2.3403E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.2712888828 -7.7716E-15  6.4597E-02  2.3403E-01  1.0000E-04
 
diagon:itrnv=   2
 expansion vectors are not transformed.
 matrix-vector products are not transformed.

    2 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:             16261
 number of initial trial vectors:                         2
 number of initial matrix-vector products:                1
 maximum dimension of the subspace vectors:              25
 number of roots to converge:                             1
 number of iterations:                                  200
 residual norm convergence criteria:               0.000100

          starting ci iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:        1490 2x:         305 4x:         106
All internal counts: zz :        5410 yy:        2859 xx:         390 ww:         640
One-external counts: yz :        4477 yx:        1070 yw:        1295
Two-external counts: yy :         900 ww:         280 xx:         200 xz:         285 wz:         435 wx:         360
Three-ext.   counts: yx :         130 yw:         160

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2
   ht   1    -0.99342034
   ht   2    -0.06459681    -0.04626619

          calcsovref: tciref block   1

              civs   1       civs   2
 refs   1    1.00000      -6.912349E-15

          calcsovref: scrb block   1

                ci   1         ci   2
 civs   1   0.960065      -0.279778    
 civs   2   0.820874        2.81685    

          calcsovref: sovref block   1

              v      1       v      2
 ref    1   0.960065      -0.279778    

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.96006474    -0.27977792

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.3265203915  5.5232E-02  3.0513E-03  4.8844E-02  1.0000E-04
 mr-sdci #  1  2    -45.6209180407  3.4305E-01  0.0000E+00  5.1648E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   2

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:        1490 2x:         305 4x:         106
All internal counts: zz :        5410 yy:        2859 xx:         390 ww:         640
One-external counts: yz :        4477 yx:        1070 yw:        1295
Two-external counts: yy :         900 ww:         280 xx:         200 xz:         285 wz:         435 wx:         360
Three-ext.   counts: yx :         130 yw:         160

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3
   ht   1    -0.99342034
   ht   2    -0.06459681    -0.04626619
   ht   3     0.00563741    -0.00299935    -0.00305327

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3
 refs   1    1.00000      -6.912349E-15  -5.692230E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3
 civs   1   0.958690      -0.135876       0.261124    
 civs   2   0.863628        1.23391       -2.51821    
 civs   3    1.09789        11.8310        5.98339    

          calcsovref: sovref block   1

              v      1       v      2       v      3
 ref    1   0.952440      -0.203220       0.227066    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.95244036    -0.20322050     0.22706562

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1    -46.3298822594  3.3619E-03  5.1000E-04  2.0342E-02  1.0000E-04
 mr-sdci #  2  2    -45.8780605477  2.5714E-01  0.0000E+00  4.2350E-01  1.0000E-04
 mr-sdci #  2  3    -45.5553531624  2.7748E-01  0.0000E+00  5.2118E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   3

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:        1490 2x:         305 4x:         106
All internal counts: zz :        5410 yy:        2859 xx:         390 ww:         640
One-external counts: yz :        4477 yx:        1070 yw:        1295
Two-external counts: yy :         900 ww:         280 xx:         200 xz:         285 wz:         435 wx:         360
Three-ext.   counts: yx :         130 yw:         160

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4
   ht   1    -0.99342034
   ht   2    -0.06459681    -0.04626619
   ht   3     0.00563741    -0.00299935    -0.00305327
   ht   4    -0.00122398    -0.00180156    -0.00042578    -0.00039040

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4
 refs   1    1.00000      -6.912349E-15  -5.692230E-03   1.322679E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4
 civs   1   0.955142       0.168536      -0.223368       0.130003    
 civs   2   0.868752      -0.884362        1.77696       -2.02433    
 civs   3    1.24715       -10.1038       -8.20442       -2.45869    
 civs   4   0.907027       -16.3606        12.2009        25.0724    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4
 ref    1   0.949243       0.204409      -0.160529       0.177161    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.94924254     0.20440897    -0.16052896     0.17716100

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1    -46.3303450618  4.6280E-04  1.1004E-04  8.9651E-03  1.0000E-04
 mr-sdci #  3  2    -45.9981596654  1.2010E-01  0.0000E+00  2.8682E-01  1.0000E-04
 mr-sdci #  3  3    -45.5639701858  8.6170E-03  0.0000E+00  5.2550E-01  1.0000E-04
 mr-sdci #  3  4    -45.5201966768  2.4233E-01  0.0000E+00  5.8380E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   4

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:        1490 2x:         305 4x:         106
All internal counts: zz :        5410 yy:        2859 xx:         390 ww:         640
One-external counts: yz :        4477 yx:        1070 yw:        1295
Two-external counts: yy :         900 ww:         280 xx:         200 xz:         285 wz:         435 wx:         360
Three-ext.   counts: yx :         130 yw:         160

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1    -0.99342034
   ht   2    -0.06459681    -0.04626619
   ht   3     0.00563741    -0.00299935    -0.00305327
   ht   4    -0.00122398    -0.00180156    -0.00042578    -0.00039040
   ht   5     0.00244151    -0.00016295    -0.00041391     0.00000321    -0.00016163

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1    1.00000      -6.912349E-15  -5.692230E-03   1.322679E-03  -2.440530E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 civs   1  -0.955744       9.663699E-02   0.247121       0.193168       1.213604E-02
 civs   2  -0.869315      -0.577590       -1.21619       -2.49379      -0.274580    
 civs   3   -1.27564       -7.98512       -2.30318        3.95321       -10.3787    
 civs   4   -1.08171       -16.2776       -11.3892        13.5301        22.9127    
 civs   5  -0.811608       -26.9165        44.8256       -12.1472        30.8628    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1  -0.947933       0.186251       0.135769       0.218207       2.619831E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1    -0.94793306     0.18625070     0.13576879     0.21820652     0.02619831

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1    -46.3304343783  8.9317E-05  1.5355E-05  3.2516E-03  1.0000E-04
 mr-sdci #  4  2    -46.0805477240  8.2388E-02  0.0000E+00  1.9765E-01  1.0000E-04
 mr-sdci #  4  3    -45.6942729688  1.3030E-01  0.0000E+00  5.9483E-01  1.0000E-04
 mr-sdci #  4  4    -45.5287212840  8.5246E-03  0.0000E+00  5.5954E-01  1.0000E-04
 mr-sdci #  4  5    -45.4885998006  2.1073E-01  0.0000E+00  5.4516E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   5

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:        1490 2x:         305 4x:         106
All internal counts: zz :        5410 yy:        2859 xx:         390 ww:         640
One-external counts: yz :        4477 yx:        1070 yw:        1295
Two-external counts: yy :         900 ww:         280 xx:         200 xz:         285 wz:         435 wx:         360
Three-ext.   counts: yx :         130 yw:         160

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1    -0.99342034
   ht   2    -0.06459681    -0.04626619
   ht   3     0.00563741    -0.00299935    -0.00305327
   ht   4    -0.00122398    -0.00180156    -0.00042578    -0.00039040
   ht   5     0.00244151    -0.00016295    -0.00041391     0.00000321    -0.00016163
   ht   6    -0.00056670    -0.00016745     0.00004790     0.00002793     0.00001415    -0.00002321

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1    1.00000      -6.912349E-15  -5.692230E-03   1.322679E-03  -2.440530E-03   5.773739E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1   0.956448      -1.811146E-02  -0.242780       0.154932      -0.168757      -6.975290E-02
 civs   2   0.869297       0.342438        1.03733      -0.644257        2.20533        1.29664    
 civs   3    1.28067        5.95173        6.69562      -5.983829E-02   -8.13020        6.93858    
 civs   4    1.11308        14.1153        7.41613       -14.1610       -2.76412       -25.7193    
 civs   5   0.957580        30.5356       -11.1299        39.4085        21.0154       -28.1428    
 civs   6   -1.04665       -68.4392        129.554        48.5502       -19.7588       -54.3944    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.947689      -0.147358      -0.169120       6.839637E-02  -0.188831      -0.105990    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.94768922    -0.14735805    -0.16912013     0.06839637    -0.18883118    -0.10598990

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1    -46.3304504496  1.6071E-05  3.7448E-06  1.8107E-03  1.0000E-04
 mr-sdci #  5  2    -46.1294319925  4.8884E-02  0.0000E+00  1.8410E-01  1.0000E-04
 mr-sdci #  5  3    -45.8824781211  1.8821E-01  0.0000E+00  3.9922E-01  1.0000E-04
 mr-sdci #  5  4    -45.6667443579  1.3802E-01  0.0000E+00  6.3601E-01  1.0000E-04
 mr-sdci #  5  5    -45.5174805885  2.8881E-02  0.0000E+00  5.3779E-01  1.0000E-04
 mr-sdci #  5  6    -45.4500103200  1.7214E-01  0.0000E+00  5.5358E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   6

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:        1490 2x:         305 4x:         106
All internal counts: zz :        5410 yy:        2859 xx:         390 ww:         640
One-external counts: yz :        4477 yx:        1070 yw:        1295
Two-external counts: yy :         900 ww:         280 xx:         200 xz:         285 wz:         435 wx:         360
Three-ext.   counts: yx :         130 yw:         160

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1    -0.99342034
   ht   2    -0.06459681    -0.04626619
   ht   3     0.00563741    -0.00299935    -0.00305327
   ht   4    -0.00122398    -0.00180156    -0.00042578    -0.00039040
   ht   5     0.00244151    -0.00016295    -0.00041391     0.00000321    -0.00016163
   ht   6    -0.00056670    -0.00016745     0.00004790     0.00002793     0.00001415    -0.00002321
   ht   7     0.00007314     0.00004869     0.00000035     0.00000834    -0.00001552     0.00000172    -0.00000353

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1    1.00000      -6.912349E-15  -5.692230E-03   1.322679E-03  -2.440530E-03   5.773739E-04  -7.595870E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1   0.956733       3.295365E-02   0.218000       4.474283E-03   0.238407       7.661025E-02  -6.895810E-02
 civs   2   0.869277       0.188461      -0.857421      -0.235252       -2.06698       -1.74889       0.189093    
 civs   3    1.28192        4.17224       -7.79242       -1.05121        6.13309       -2.10180        8.65879    
 civs   4    1.12096        11.0278       -12.4704        8.26113       -8.61391        25.9390       -5.61055    
 civs   5   0.994408        29.3928       0.919417       -21.4872        7.20119        3.14637       -53.5644    
 civs   6   -1.31077       -88.6680       -79.9501       -97.5165        3.07197        59.9942       -3.06985    
 civs   7    1.08306        111.982        168.238       -139.511       -132.963        89.3703        255.516    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.947653      -0.107644       0.184678       2.811854E-02   0.186401       0.143055      -1.612239E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.94765293    -0.10764413     0.18467763     0.02811854     0.18640150     0.14305499    -0.01612239

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  6  1    -46.3304545055  4.0559E-06  1.2648E-06  8.7608E-04  1.0000E-04
 mr-sdci #  6  2    -46.1633185850  3.3887E-02  0.0000E+00  1.6869E-01  1.0000E-04
 mr-sdci #  6  3    -45.9817260697  9.9248E-02  0.0000E+00  2.6852E-01  1.0000E-04
 mr-sdci #  6  4    -45.7291167932  6.2372E-02  0.0000E+00  6.1585E-01  1.0000E-04
 mr-sdci #  6  5    -45.5572891537  3.9809E-02  0.0000E+00  5.0994E-01  1.0000E-04
 mr-sdci #  6  6    -45.4631892950  1.3179E-02  0.0000E+00  5.6418E-01  1.0000E-04
 mr-sdci #  6  7    -45.3783738750  1.0051E-01  0.0000E+00  6.3237E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   7

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:        1490 2x:         305 4x:         106
All internal counts: zz :        5410 yy:        2859 xx:         390 ww:         640
One-external counts: yz :        4477 yx:        1070 yw:        1295
Two-external counts: yy :         900 ww:         280 xx:         200 xz:         285 wz:         435 wx:         360
Three-ext.   counts: yx :         130 yw:         160

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1    -0.99342034
   ht   2    -0.06459681    -0.04626619
   ht   3     0.00563741    -0.00299935    -0.00305327
   ht   4    -0.00122398    -0.00180156    -0.00042578    -0.00039040
   ht   5     0.00244151    -0.00016295    -0.00041391     0.00000321    -0.00016163
   ht   6    -0.00056670    -0.00016745     0.00004790     0.00002793     0.00001415    -0.00002321
   ht   7     0.00007314     0.00004869     0.00000035     0.00000834    -0.00001552     0.00000172    -0.00000353
   ht   8     0.00035764     0.00002353    -0.00000681    -0.00000155    -0.00000539     0.00000455    -0.00000091    -0.00000285

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1    1.00000      -6.912349E-15  -5.692230E-03   1.322679E-03  -2.440530E-03   5.773739E-04  -7.595870E-05  -3.600251E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1   0.957264      -0.131553       0.200692      -7.097671E-02  -3.940180E-02   0.231801       0.100296      -8.313499E-02
 civs   2   0.869244      -4.389207E-02  -0.630589       0.827821       0.751395       -1.75079       -1.84223       0.290708    
 civs   3    1.28238       -1.80205       -7.86214        2.59555        2.05489        7.03731      -0.807855        8.40470    
 civs   4    1.12400       -5.81063       -15.2591        5.90147       -6.78805       -11.3159        24.6406       -6.43196    
 civs   5    1.00865       -19.2959       -18.9645       -28.2186        9.22906        2.08191       -1.07837       -52.5962    
 civs   6   -1.41302        76.9815       -5.96304        47.7453        129.173        19.7536        74.0534       -12.9267    
 civs   7    1.50242       -155.635        98.1415       -122.626        107.820       -136.025        87.4455        255.551    
 civs   8    1.24174       -228.467        218.575        371.875        197.620        106.776        75.1412       -40.1229    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.947612       5.663444E-02   0.181956      -0.106080      -8.735798E-02   0.154991       0.149179      -2.355089E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.94761232     0.05663444     0.18195551    -0.10607998    -0.08735798     0.15499066     0.14917938    -0.02355089

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  7  1    -46.3304560760  1.5705E-06  3.6742E-07  5.3435E-04  1.0000E-04
 mr-sdci #  7  2    -46.2199469557  5.6628E-02  0.0000E+00  1.3819E-01  1.0000E-04
 mr-sdci #  7  3    -46.0574837970  7.5758E-02  0.0000E+00  1.8321E-01  1.0000E-04
 mr-sdci #  7  4    -45.7729713152  4.3855E-02  0.0000E+00  5.0201E-01  1.0000E-04
 mr-sdci #  7  5    -45.7173456707  1.6006E-01  0.0000E+00  5.7630E-01  1.0000E-04
 mr-sdci #  7  6    -45.5458243691  8.2635E-02  0.0000E+00  5.2802E-01  1.0000E-04
 mr-sdci #  7  7    -45.4566912253  7.8317E-02  0.0000E+00  5.5346E-01  1.0000E-04
 mr-sdci #  7  8    -45.3761931877  9.8325E-02  0.0000E+00  6.3619E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.000999
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   8

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:        1490 2x:         305 4x:         106
All internal counts: zz :        5410 yy:        2859 xx:         390 ww:         640
One-external counts: yz :        4477 yx:        1070 yw:        1295
Two-external counts: yy :         900 ww:         280 xx:         200 xz:         285 wz:         435 wx:         360
Three-ext.   counts: yx :         130 yw:         160

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1    -0.99342034
   ht   2    -0.06459681    -0.04626619
   ht   3     0.00563741    -0.00299935    -0.00305327
   ht   4    -0.00122398    -0.00180156    -0.00042578    -0.00039040
   ht   5     0.00244151    -0.00016295    -0.00041391     0.00000321    -0.00016163
   ht   6    -0.00056670    -0.00016745     0.00004790     0.00002793     0.00001415    -0.00002321
   ht   7     0.00007314     0.00004869     0.00000035     0.00000834    -0.00001552     0.00000172    -0.00000353
   ht   8     0.00035764     0.00002353    -0.00000681    -0.00000155    -0.00000539     0.00000455    -0.00000091    -0.00000285
   ht   9     0.00003666     0.00000221    -0.00000541     0.00000503    -0.00000118    -0.00000011    -0.00000035    -0.00000035

                ht   9
   ht   9    -0.00000034

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1    1.00000      -6.912349E-15  -5.692230E-03   1.322679E-03  -2.440530E-03   5.773739E-04  -7.595870E-05  -3.600251E-04

              civs   9
 refs   1  -3.688970E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1   0.957430      -0.149829       0.169869      -0.101096      -3.432482E-02   0.147794       0.218724       1.755921E-03
 civs   2   0.869233      -2.083460E-02  -0.511688       0.866292       0.285219       -1.38469       -1.90093        1.14715    
 civs   3    1.28250       -1.30238       -7.03494        5.75549       7.253004E-02    5.01577        2.45829       -1.03477    
 civs   4    1.12474       -4.29282       -15.4383        1.58267        8.79183       -7.57682        3.75126       -22.0590    
 civs   5    1.01219       -15.7349       -23.1205       -10.7933       -26.9698       -5.32505        14.2672        23.2723    
 civs   6   -1.43837        67.5433        21.6162        103.422       -33.7898       -71.3302        90.2864       -29.2584    
 civs   7    1.60638       -153.841        58.9250       -79.4627       -142.174       -130.175       -73.1672       -183.652    
 civs   8    1.54958       -265.927        213.451        202.659        209.508       -160.490        245.529        59.0805    
 civs   9    1.05970       -230.200        295.660        655.948       -173.975        480.953       -437.228       -328.372    

                ci   9
 civs   1   5.284985E-02
 civs   2  -0.513351    
 civs   3   -9.03746    
 civs   4    16.2426    
 civs   5    44.6391    
 civs   6   -2.41899    
 civs   7   -212.550    
 civs   8   -75.1591    
 civs   9    564.438    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.947598       4.522352E-02   0.166170      -0.136834      -3.500849E-02   0.130959       0.160293      -9.042770E-02

              v      9
 ref    1   3.781937E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.94759788     0.04522352     0.16617010    -0.13683372    -0.03500849     0.13095939     0.16029251    -0.09042770

                ci   9
 ref:   1     0.03781937

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  8  1    -46.3304564653  3.8935E-07  6.7790E-08  2.3498E-04  1.0000E-04
 mr-sdci #  8  2    -46.2373267947  1.7380E-02  0.0000E+00  8.4252E-02  1.0000E-04
 mr-sdci #  8  3    -46.0822972991  2.4814E-02  0.0000E+00  1.4246E-01  1.0000E-04
 mr-sdci #  8  4    -45.8678680142  9.4897E-02  0.0000E+00  3.3088E-01  1.0000E-04
 mr-sdci #  8  5    -45.7570994625  3.9754E-02  0.0000E+00  5.9166E-01  1.0000E-04
 mr-sdci #  8  6    -45.5962325828  5.0408E-02  0.0000E+00  4.7097E-01  1.0000E-04
 mr-sdci #  8  7    -45.4969898394  4.0299E-02  0.0000E+00  5.8158E-01  1.0000E-04
 mr-sdci #  8  8    -45.4368916012  6.0698E-02  0.0000E+00  6.3643E-01  1.0000E-04
 mr-sdci #  8  9    -45.3187912979  4.0923E-02  0.0000E+00  6.1469E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   9

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:        1490 2x:         305 4x:         106
All internal counts: zz :        5410 yy:        2859 xx:         390 ww:         640
One-external counts: yz :        4477 yx:        1070 yw:        1295
Two-external counts: yy :         900 ww:         280 xx:         200 xz:         285 wz:         435 wx:         360
Three-ext.   counts: yx :         130 yw:         160

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1    -0.99342034
   ht   2    -0.06459681    -0.04626619
   ht   3     0.00563741    -0.00299935    -0.00305327
   ht   4    -0.00122398    -0.00180156    -0.00042578    -0.00039040
   ht   5     0.00244151    -0.00016295    -0.00041391     0.00000321    -0.00016163
   ht   6    -0.00056670    -0.00016745     0.00004790     0.00002793     0.00001415    -0.00002321
   ht   7     0.00007314     0.00004869     0.00000035     0.00000834    -0.00001552     0.00000172    -0.00000353
   ht   8     0.00035764     0.00002353    -0.00000681    -0.00000155    -0.00000539     0.00000455    -0.00000091    -0.00000285
   ht   9     0.00003666     0.00000221    -0.00000541     0.00000503    -0.00000118    -0.00000011    -0.00000035    -0.00000035
   ht  10     0.00001195     0.00000035    -0.00000286     0.00000060    -0.00000014     0.00000001    -0.00000003    -0.00000021

                ht   9         ht  10
   ht   9    -0.00000034
   ht  10    -0.00000004    -0.00000007

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1    1.00000      -6.912349E-15  -5.692230E-03   1.322679E-03  -2.440530E-03   5.773739E-04  -7.595870E-05  -3.600251E-04

              civs   9       civs  10
 refs   1  -3.688970E-05  -1.200971E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1  -0.957461      -0.152080      -0.144594       0.125346      -2.317369E-02   0.156022       5.755111E-02   0.214091    
 civs   2  -0.869228      -1.561060E-02   0.434133      -0.726068       0.155727       -1.60215      -0.471353       -1.88652    
 civs   3   -1.28251       -1.17388        6.28719       -6.31073      -0.357670        2.15044        4.06383        2.31413    
 civs   4   -1.12485       -3.81909        14.8728       -4.68375        8.65761       -5.21679       -5.69501        7.13372    
 civs   5   -1.01270       -14.3955        24.6502        6.24240       -25.2663        7.17849       -11.0625        12.6461    
 civs   6    1.44205        63.1272       -35.8931       -84.6884       -44.5650       -58.2528       -57.5284        97.2248    
 civs   7   -1.62149       -148.846       -31.4292        110.057       -137.155       -54.6237       -110.472       -89.9812    
 civs   8   -1.59431       -268.486       -193.441       -70.0477        173.079       -230.826       -73.7391        293.479    
 civs   9   -1.21367       -275.408       -361.321       -498.192       -247.460       -144.621        698.192       -174.779    
 civs  10  -0.834473       -279.048       -488.808       -1151.61        199.375        1648.15       -1092.23       -946.075    

                ci   9         ci  10
 civs   1  -2.296927E-02   3.065604E-02
 civs   2  -0.975194      -0.409821    
 civs   3   0.965356       -9.63809    
 civs   4    21.1132        16.6017    
 civs   5   -25.3021        44.2221    
 civs   6    19.2121       -16.8116    
 civs   7    199.303       -195.934    
 civs   8   -93.8436       -148.826    
 civs   9    318.143        558.910    
 civs  10    203.266        628.304    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.947593       4.260992E-02  -0.150363       0.140009      -1.891448E-02   0.158522       4.296978E-02   0.154610    

              v      9       v     10
 ref    1   7.677454E-02   3.014529E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.94759277     0.04260992    -0.15036290     0.14000900    -0.01891448     0.15852196     0.04296978     0.15461041

                ci   9         ci  10
 ref:   1     0.07677454     0.03014529

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  9  1    -46.3304565219  5.6569E-08  1.2144E-08  9.3035E-05  1.0000E-04
 mr-sdci #  9  2    -46.2428245144  5.4977E-03  0.0000E+00  7.3112E-02  1.0000E-04
 mr-sdci #  9  3    -46.0945110453  1.2214E-02  0.0000E+00  1.3285E-01  1.0000E-04
 mr-sdci #  9  4    -45.9439669698  7.6099E-02  0.0000E+00  2.9169E-01  1.0000E-04
 mr-sdci #  9  5    -45.7588105778  1.7111E-03  0.0000E+00  5.9613E-01  1.0000E-04
 mr-sdci #  9  6    -45.6223279275  2.6095E-02  0.0000E+00  5.3223E-01  1.0000E-04
 mr-sdci #  9  7    -45.5842951503  8.7305E-02  0.0000E+00  5.2445E-01  1.0000E-04
 mr-sdci #  9  8    -45.4723104991  3.5419E-02  0.0000E+00  5.7664E-01  1.0000E-04
 mr-sdci #  9  9    -45.4360362768  1.1724E-01  0.0000E+00  6.4557E-01  1.0000E-04
 mr-sdci #  9 10    -45.3010322157  2.3164E-02  0.0000E+00  6.1813E-01  1.0000E-04
 
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mr-sdci  convergence criteria satisfied after  9 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  9  1    -46.3304565219  5.6569E-08  1.2144E-08  9.3035E-05  1.0000E-04
 mr-sdci #  9  2    -46.2428245144  5.4977E-03  0.0000E+00  7.3112E-02  1.0000E-04
 mr-sdci #  9  3    -46.0945110453  1.2214E-02  0.0000E+00  1.3285E-01  1.0000E-04
 mr-sdci #  9  4    -45.9439669698  7.6099E-02  0.0000E+00  2.9169E-01  1.0000E-04
 mr-sdci #  9  5    -45.7588105778  1.7111E-03  0.0000E+00  5.9613E-01  1.0000E-04
 mr-sdci #  9  6    -45.6223279275  2.6095E-02  0.0000E+00  5.3223E-01  1.0000E-04
 mr-sdci #  9  7    -45.5842951503  8.7305E-02  0.0000E+00  5.2445E-01  1.0000E-04
 mr-sdci #  9  8    -45.4723104991  3.5419E-02  0.0000E+00  5.7664E-01  1.0000E-04
 mr-sdci #  9  9    -45.4360362768  1.1724E-01  0.0000E+00  6.4557E-01  1.0000E-04
 mr-sdci #  9 10    -45.3010322157  2.3164E-02  0.0000E+00  6.1813E-01  1.0000E-04

####################CIUDGINFO####################

   ci vector at position   1 energy=  -46.330456521916

################END OF CIUDGINFO################

 
diagon:itrnv=   0
    1 of the  11 expansion vectors are transformed.
    1 of the  10 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
maximum overlap with reference    1(overlap= 0.94759)

 information on vector: 1 from unit 11 written to unit 48 filename civout              


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -46.3304565219

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     5    6    7    8    9   10

                                         symmetry   a    a    a    a    a    a  

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1 -0.476871                        +-   +-                     
 z*  1  1       6 -0.476262                        +-        +-                
 z*  1  1      10  0.238671                        +-             +-           
 z*  1  1      13  0.238079                        +-                  +-      
 z*  1  1      15  0.476280                        +-                       +- 
 z*  1  1      19  0.158617                        +    +-                   - 
 z*  1  1      25  0.148778                        +     -        +     -      
 z*  1  1      38  0.158388                        +         +-              - 
 z*  1  1      39  0.105073                        +          -   +-           
 z*  1  1      42 -0.104898                        +          -        +-      
 z*  1  1      49 -0.023093                        +              +-         - 
 z*  1  1      54 -0.023401                        +                   +-    - 
 z*  1  1      56  0.019327                             +-   +-                
 z*  1  1      60  0.025316                             +-        +-           
 z*  1  1      63  0.025379                             +-             +-      
 z*  1  1      75  0.023700                             +    +     -    -      
 z*  1  1      81 -0.011878                             +          -   +     - 
 z*  1  1      83 -0.020503                             +         +     -    - 
 z*  1  1      86  0.025297                                  +-   +-           
 z*  1  1      89  0.025291                                  +-        +-      
 z*  1  1      93 -0.016647                                  +    +-         - 
 z*  1  1      98  0.016808                                  +         +-    - 
 z*  1  1     100 -0.050694                                       +-   +-      
 z*  1  1     102 -0.034993                                       +-        +- 
 z*  1  1     105 -0.034963                                            +-   +- 
 y   1  1     109 -0.025205              4( a  )   +-    -                     
 y   1  1     119  0.022352             14( a  )   +-    -                     
 y   1  1     138  0.025156              5( a  )   +-         -                
 y   1  1     146  0.022312             13( a  )   +-         -                
 y   1  1     167 -0.012617              6( a  )   +-              -           
 y   1  1     176  0.011188             15( a  )   +-              -           
 y   1  1     196 -0.012586              7( a  )   +-                   -      
 y   1  1     205  0.011160             16( a  )   +-                   -      
 y   1  1     225 -0.025165              8( a  )   +-                        - 
 y   1  1     229 -0.055726             12( a  )   +-                        - 
 y   1  1     234 -0.022318             17( a  )   +-                        - 
 y   1  1     253 -0.017856              8( a  )    -   +-                     
 y   1  1     257  0.024968             12( a  )    -   +-                     
 y   1  1     262 -0.020814             17( a  )    -   +-                     
 y   1  1     361 -0.012041              4( a  )    -   +                    - 
 y   1  1     393 -0.017828              8( a  )    -        +-                
 y   1  1     397  0.024922             12( a  )    -        +-                
 y   1  1     402 -0.020780             17( a  )    -        +-                
 y   1  1     474  0.012035              5( a  )    -        +               - 
 y   1  1     502  0.013345              5( a  )    -             +-           
 y   1  1     509 -0.012496             12( a  )    -             +-           
 y   1  1     510  0.012941             13( a  )    -             +-           
 y   1  1     529 -0.018909              4( a  )    -             +     -      
 y   1  1     539  0.018299             14( a  )    -             +     -      
 y   1  1     586 -0.013364              5( a  )    -                  +-      
 y   1  1     593 -0.012465             12( a  )    -                  +-      
 y   1  1     594 -0.012908             13( a  )    -                  +-      
 y   1  1     649 -0.024923             12( a  )    -                       +- 
 y   1  1     700 -0.013517              7( a  )   +     -         -           
 y   1  1     709  0.015223             16( a  )   +     -         -           
 y   1  1     727 -0.013506              6( a  )   +     -              -      
 y   1  1     736  0.015210             15( a  )   +     -              -      
 y   1  1     753  0.020911              4( a  )   +     -                   - 
 y   1  1     763 -0.024287             14( a  )   +     -                   - 
 y   1  1     783 -0.013482              6( a  )   +          -    -           
 y   1  1     792  0.015194             15( a  )   +          -    -           
 y   1  1     812  0.013460              7( a  )   +          -         -      
 y   1  1     821 -0.015169             16( a  )   +          -         -      
 y   1  1     838 -0.020873              5( a  )   +          -              - 
 y   1  1     846 -0.024252             13( a  )   +          -              - 
 y   1  1     988  0.010720             15( a  )        +-         -           
 y   1  1    1017  0.010735             16( a  )        +-              -      
 y   1  1    1520  0.010710             15( a  )             +-    -           
 y   1  1    1549  0.010706             16( a  )             +-         -      
 y   1  1    1848  0.015470              7( a  )                  +-    -      
 y   1  1    1857 -0.014321             16( a  )                  +-    -      
 y   1  1    1877  0.010311              8( a  )                  +-         - 
 y   1  1    1903 -0.015466              6( a  )                   -   +-      
 y   1  1    1912  0.014315             15( a  )                   -   +-      
 y   1  1    1959 -0.012514              6( a  )                   -        +- 
 y   1  1    1968  0.011653             15( a  )                   -        +- 
 y   1  1    2017  0.010316              8( a  )                       +-    - 
 y   1  1    2044 -0.012505              7( a  )                        -   +- 
 y   1  1    2053  0.011647             16( a  )                        -   +- 
 x   1  1    2124  0.018873    4( a  )  12( a  )    -    -                     
 x   1  1    2155  0.019425   12( a  )  14( a  )    -    -                     
 x   1  1    2257  0.011864    2( a  )  21( a  )    -    -                     
 x   1  1    2264  0.012441    9( a  )  21( a  )    -    -                     
 x   1  1    2276 -0.011858    1( a  )  22( a  )    -    -                     
 x   1  1    2285  0.012435   10( a  )  22( a  )    -    -                     
 x   1  1    2299 -0.013596    3( a  )  23( a  )    -    -                     
 x   1  1    2307  0.014192   11( a  )  23( a  )    -    -                     
 x   1  1    2503 -0.018844    5( a  )  12( a  )    -         -                
 x   1  1    2521  0.019397   12( a  )  13( a  )    -         -                
 x   1  1    2634 -0.011841    1( a  )  21( a  )    -         -                
 x   1  1    2643  0.012417   10( a  )  21( a  )    -         -                
 x   1  1    2655 -0.011847    2( a  )  22( a  )    -         -                
 x   1  1    2662 -0.012423    9( a  )  22( a  )    -         -                
 x   1  1    2699  0.013577    3( a  )  24( a  )    -         -                
 x   1  1    2707 -0.014171   11( a  )  24( a  )    -         -                
 x   1  1    3640  0.018849    8( a  )  12( a  )    -                        - 
 x   1  1    3709 -0.019401   12( a  )  17( a  )    -                        - 
 x   1  1    3855 -0.013065    2( a  )  25( a  )    -                        - 
 x   1  1    3862 -0.013441    9( a  )  25( a  )    -                        - 
 x   1  1    3878 -0.013058    1( a  )  26( a  )    -                        - 
 x   1  1    3887  0.013432   10( a  )  26( a  )    -                        - 
 x   1  1    3905 -0.012772    3( a  )  27( a  )    -                        - 
 x   1  1    3913  0.013951   11( a  )  27( a  )    -                        - 
 x   1  1    3956  0.013907    1( a  )   2( a  )         -    -                
 x   1  1    6602  0.017099    1( a  )   2( a  )                   -    -      
 x   1  1    6630  0.010569    1( a  )   9( a  )                   -    -      
 x   1  1    6639  0.010567    2( a  )  10( a  )                   -    -      
 x   1  1    6982 -0.012973    2( a  )   3( a  )                   -         - 
 x   1  1    7359  0.012951    1( a  )   3( a  )                        -    - 
 w   1  1    7736  0.010983    1( a  )   1( a  )   +-                          
 w   1  1    7738  0.010955    2( a  )   2( a  )   +-                          
 w   1  1    7741 -0.021938    3( a  )   3( a  )   +-                          
 w   1  1    7773  0.014200    2( a  )   9( a  )   +-                          
 w   1  1    7781 -0.014235    1( a  )  10( a  )   +-                          
 w   1  1    7793  0.028435    3( a  )  11( a  )   +-                          
 w   1  1    7801 -0.019487   11( a  )  11( a  )   +-                          
 w   1  1    7818  0.013337    5( a  )  13( a  )   +-                          
 w   1  1    7826  0.016340   13( a  )  13( a  )   +-                          
 w   1  1    7830 -0.013360    4( a  )  14( a  )   +-                          
 w   1  1    7840  0.016369   14( a  )  14( a  )   +-                          
 w   1  1    7879 -0.013340    8( a  )  17( a  )   +-                          
 w   1  1    7888 -0.016344   17( a  )  17( a  )   +-                          
 w   1  1    7928 -0.011462    3( a  )  20( a  )   +-                          
 w   1  1    7936  0.013042   11( a  )  20( a  )   +-                          
 w   1  1    7966  0.019899   21( a  )  21( a  )   +-                          
 w   1  1    7988  0.019899   22( a  )  22( a  )   +-                          
 w   1  1    8060 -0.011948   25( a  )  25( a  )   +-                          
 w   1  1    8086 -0.011929   26( a  )  26( a  )   +-                          
 w   1  1    8113 -0.015915   27( a  )  27( a  )   +-                          
 w   1  1    8143 -0.036373    1( a  )   2( a  )   +     -                     
 w   1  1    8178 -0.026199    1( a  )   9( a  )   +     -                     
 w   1  1    8188  0.026201    2( a  )  10( a  )   +     -                     
 w   1  1    8195  0.020231    9( a  )  10( a  )   +     -                     
 w   1  1    8211  0.010991    4( a  )  12( a  )   +     -                     
 w   1  1    8360 -0.010569    9( a  )  21( a  )   +     -                     
 w   1  1    8382 -0.010569   10( a  )  22( a  )   +     -                     
 w   1  1    8548 -0.025712    1( a  )   1( a  )   +          -                
 w   1  1    8550  0.025669    2( a  )   2( a  )   +          -                
 w   1  1    8585  0.026147    2( a  )   9( a  )   +          -                
 w   1  1    8592  0.014276    9( a  )   9( a  )   +          -                
 w   1  1    8593  0.026191    1( a  )  10( a  )   +          -                
 w   1  1    8602 -0.014300   10( a  )  10( a  )   +          -                
 w   1  1    8618 -0.010973    5( a  )  12( a  )   +          -                
 w   1  1    8767 -0.010555   10( a  )  21( a  )   +          -                
 w   1  1    8787  0.010555    9( a  )  22( a  )   +          -                
 w   1  1    8958 -0.018207    2( a  )   3( a  )   +               -           
 w   1  1    8992 -0.012258    3( a  )   9( a  )   +               -           
 w   1  1    9010  0.013970    2( a  )  11( a  )   +               -           
 w   1  1    9017  0.010127    9( a  )  11( a  )   +               -           
 w   1  1    9363  0.018162    1( a  )   3( a  )   +                    -      
 w   1  1    9407 -0.012225    3( a  )  10( a  )   +                    -      
 w   1  1    9415 -0.013939    1( a  )  11( a  )   +                    -      
 w   1  1    9424  0.010102   10( a  )  11( a  )   +                    -      
 w   1  1    9771  0.038949    3( a  )   3( a  )   +                         - 
 w   1  1    9823 -0.036507    3( a  )  11( a  )   +                         - 
 w   1  1    9831  0.018737   11( a  )  11( a  )   +                         - 
 w   1  1    9839  0.010976    8( a  )  12( a  )   +                         - 
 w   1  1   10119  0.011592    3( a  )  27( a  )   +                         - 
 w   1  1   10127 -0.012715   11( a  )  27( a  )   +                         - 
 w   1  1   10172  0.035702    1( a  )   1( a  )        +-                     
 w   1  1   10174  0.035695    2( a  )   2( a  )        +-                     
 w   1  1   10177  0.064456    3( a  )   3( a  )        +-                     
 w   1  1   10209  0.028619    2( a  )   9( a  )        +-                     
 w   1  1   10216  0.013631    9( a  )   9( a  )        +-                     
 w   1  1   10217 -0.028624    1( a  )  10( a  )        +-                     
 w   1  1   10226  0.013633   10( a  )  10( a  )        +-                     
 w   1  1   10229 -0.054851    3( a  )  11( a  )        +-                     
 w   1  1   10237  0.026567   11( a  )  11( a  )        +-                     
 w   1  1   10249  0.012871   12( a  )  12( a  )        +-                     
 w   1  1   11797 -0.014542    1( a  )   2( a  )        +                    - 
 w   1  1   12202  0.035639    1( a  )   1( a  )             +-                
 w   1  1   12204  0.035664    2( a  )   2( a  )             +-                
 w   1  1   12207  0.064365    3( a  )   3( a  )             +-                
 w   1  1   12239  0.028593    2( a  )   9( a  )             +-                
 w   1  1   12246  0.013619    9( a  )   9( a  )             +-                
 w   1  1   12247 -0.028571    1( a  )  10( a  )             +-                
 w   1  1   12256  0.013607   10( a  )  10( a  )             +-                
 w   1  1   12259 -0.054771    3( a  )  11( a  )             +-                
 w   1  1   12267  0.026528   11( a  )  11( a  )             +-                
 w   1  1   12279  0.012853   12( a  )  12( a  )             +-                
 w   1  1   13420 -0.010413    1( a  )   1( a  )             +               - 
 w   1  1   13422  0.010139    2( a  )   2( a  )             +               - 
 w   1  1   13826 -0.032264    1( a  )   1( a  )                  +-           
 w   1  1   13828 -0.013722    2( a  )   2( a  )                  +-           
 w   1  1   13831 -0.022021    3( a  )   3( a  )                  +-           
 w   1  1   13863 -0.011540    2( a  )   9( a  )                  +-           
 w   1  1   13871  0.027455    1( a  )  10( a  )                  +-           
 w   1  1   13880 -0.013299   10( a  )  10( a  )                  +-           
 w   1  1   13883  0.017116    3( a  )  11( a  )                  +-           
 w   1  1   14233 -0.018564    1( a  )   2( a  )                  +     -      
 w   1  1   14268 -0.011264    1( a  )   9( a  )                  +     -      
 w   1  1   14278  0.011265    2( a  )  10( a  )                  +     -      
 w   1  1   15044 -0.013674    1( a  )   1( a  )                       +-      
 w   1  1   15046 -0.032184    2( a  )   2( a  )                       +-      
 w   1  1   15049 -0.021981    3( a  )   3( a  )                       +-      
 w   1  1   15081 -0.027387    2( a  )   9( a  )                       +-      
 w   1  1   15088 -0.013266    9( a  )   9( a  )                       +-      
 w   1  1   15089  0.011502    1( a  )  10( a  )                       +-      
 w   1  1   15101  0.017083    3( a  )  11( a  )                       +-      
 w   1  1   15856 -0.046410    1( a  )   1( a  )                            +- 
 w   1  1   15858 -0.046407    2( a  )   2( a  )                            +- 
 w   1  1   15861 -0.042876    3( a  )   3( a  )                            +- 
 w   1  1   15893 -0.039034    2( a  )   9( a  )                            +- 
 w   1  1   15900 -0.018966    9( a  )   9( a  )                            +- 
 w   1  1   15901  0.039037    1( a  )  10( a  )                            +- 
 w   1  1   15910 -0.018969   10( a  )  10( a  )                            +- 
 w   1  1   15913  0.033885    3( a  )  11( a  )                            +- 
 w   1  1   15921 -0.015830   11( a  )  11( a  )                            +- 
 w   1  1   15933 -0.012856   12( a  )  12( a  )                            +- 

 ci coefficient statistics:
           rq > 0.1               10
      0.1> rq > 0.01             199
     0.01> rq > 0.001            865
    0.001> rq > 0.0001           726
   0.0001> rq > 0.00001          444
  0.00001> rq > 0.000001         769
 0.000001> rq                  13248
           all                 16261
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         865 2x:           0 4x:           0
All internal counts: zz :        5410 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


  iref  icsf         v(icsf)             hv(icsf)
     1     1     -0.476870695061     22.066142221984
     2     2     -0.000000689106      0.000031875842
     3     3     -0.000021007730      0.000972086580
     4     4     -0.000000028807      0.000001332942
     5     5     -0.000011657684      0.000539433778
     6     6     -0.476261538161     22.037963909524
     7     7     -0.000000031217      0.000001444497
     8     8      0.000021398293     -0.000990159171
     9     9      0.001491981855     -0.069038193674
    10    10      0.238671031497    -11.043981152859
    11    11     -0.000010716356      0.000495875874
    12    12     -0.000000037720      0.000001745375
    13    13      0.238079149169    -11.016593047387
    14    14      0.000012417481     -0.000574591800
    15    15      0.476279569467    -22.038801756909
    16    16      0.000178899450     -0.008276628289
    17    17     -0.000000020152      0.000000932159
    18    18      0.000003665598     -0.000169548765
    19    19      0.158616863704     -7.337246525458
    20    20      0.000001457705     -0.000067458905
    21    21      0.000004687773     -0.000216875811
    22    22     -0.000000007606      0.000000351853
    23    23      0.000000127986     -0.000005916710
    24    24      0.000010649376     -0.000492699342
    25    25      0.148778023633     -6.883096982215
    26    26     -0.000002819041      0.000130420545
    27    27     -0.000014201711      0.000657007220
    28    28      0.000000018093     -0.000000837081
    29    29     -0.000002968798      0.000137330141
    30    30     -0.000002676758      0.000123837911
    31    31      0.000000003947     -0.000000182590
    32    32      0.000000218420     -0.000010098253
    33    33     -0.000106953275      0.004948081611
    34    34      0.000004629989     -0.000214202514
    35    35      0.000000005448     -0.000000252061
    36    36     -0.000000020632      0.000000954341
    37    37      0.000003628924     -0.000167852095
    38    38      0.158387504026     -7.326642206151
    39    39      0.105072977133     -4.861125229285
    40    40     -0.000017123819      0.000792225872
    41    41      0.000000018658     -0.000000863230
    42    42     -0.104897604724      4.853011660875
    43    43      0.000002760335     -0.000127704629
    44    44     -0.000004245864      0.000196397176
    45    45     -0.000002718193      0.000125754901
    46    46      0.000000005910     -0.000000273416
    47    47     -0.000004710317      0.000217918970
    48    48      0.000003538160     -0.000163652952
    49    49     -0.023092917004      1.069424936435
    50    50     -0.000000009755      0.000000451398
    51    51     -0.000001336728      0.000061842256
    52    52     -0.000000002597      0.000000120303
    53    53     -0.000002307584      0.000106758804
    54    54     -0.023401422259      1.083697651252
    55    55      0.000000870411     -0.000040306403
    56    56      0.019326922350     -0.893544507852
    57    57      0.000000000644     -0.000000029775
    58    58     -0.000000419876      0.000019413640
    59    59     -0.000058121310      0.002686579699
    60    60      0.025315898674     -1.170035945226
    61    61      0.000000542493     -0.000025075198
    62    62      0.000000004436     -0.000000204981
    63    63      0.025378939979     -1.172949552882
    64    64     -0.000000972758      0.000044960161
    65    65      0.000023713086     -0.001097364738
    66    66     -0.000000432793      0.000020010404
    67    67     -0.000000000797      0.000000036875
    68    68     -0.000000941361      0.000043508702
    69    69     -0.000000235411      0.000010881951
    70    70      0.000034854381     -0.001610723619
    71    71     -0.000000005365      0.000000247909
    72    72      0.000000355462     -0.000016424237
    73    73      0.000000000044     -0.000000002039
    74    74      0.000000036666     -0.000001694194
    75    75      0.023700052247     -1.095263818809
    76    76     -0.000001735946      0.000080223705
    77    77     -0.000000001054      0.000000048735
    78    78     -0.000000003836      0.000000177292
    79    79     -0.000002350248      0.000108617991
    80    80      0.000000997784     -0.000046120849
    81    81     -0.011878053071      0.548927867376
    82    82      0.000001645957     -0.000076074871
    83    83     -0.020502946769      0.947515630034
    84    84      0.000001583818     -0.000073188224
    85    85     -0.000000000400      0.000000018468
    86    86      0.025297043131     -1.169167111993
    87    87      0.000001133449     -0.000052385202
    88    88      0.000000004498     -0.000000207868
    89    89      0.025290568593     -1.168868156887
    90    90     -0.000000965046      0.000044603826
    91    91     -0.000007677315      0.000353061366
    92    92      0.000001049953     -0.000048531934
    93    93     -0.016646838696      0.769313145736
    94    94      0.000000004179     -0.000000193183
    95    95      0.000001004957     -0.000046443866
    96    96     -0.000000000325      0.000000014996
    97    97      0.000002583747     -0.000119406196
    98    98      0.016807954559     -0.776760092327
    99    99     -0.000001663109      0.000076867752
   100   100     -0.050694255574      2.342954452296
   101   101      0.000000120476     -0.000005561688
   102   102     -0.034993392234      1.617455886328
   103   103      0.000000002827     -0.000000130689
   104   104     -0.000000104036      0.000004803268
   105   105     -0.034963239904      1.616062167231

 number of reference csfs (nref) is   105.  root number (iroot) is  1.
 c0**2 =   0.89986404  c**2 (all zwalks) =   0.89986404

 pople ci energy extrapolation is computed with  4 correlated electrons.

 eref      =    -46.271011526023   "relaxed" cnot**2         =   0.899864036453
 eci       =    -46.330456521916   deltae = eci - eref       =  -0.059444995893
 eci+dv1   =    -46.336409103858   dv1 = (1-cnot**2)*deltae  =  -0.005952581942
 eci+dv2   =    -46.337071501181   dv2 = dv1 / cnot**2       =  -0.006614979264
 eci+dv3   =    -46.337899779373   dv3 = dv1 / (2*cnot**2-1) =  -0.007443257456
 eci+pople =    -46.333753835285   (  4e- scaled deltae )    =  -0.062742309262
 passed aftci ... 
 readint2: molcas,dalton2=                     0                     0
 files%faoints=aoints              
lodens (list->root)=  1
sifcfg setup: record length 4096 DP
# d1 elements per record  3272
# d2 elements per record  2730
  The MR-CISD density will be calculated.
 item #                     1 suffix=:.drt1.state1:
 read_civout: repnuc=  -45.2778685421096     
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method:  0 last record  1max overlap with ref# 95% root-following 0
 MR-CISD energy:   -46.33045652    -1.05258798
 residuum:     0.00009303
 deltae:     0.00000006
 apxde:     0.00000001

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.94759277     0.04260992    -0.15036290     0.14000900    -0.01891448     0.15852196     0.04296978     0.15461041

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1     0.07677454     0.03014529     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.94759277     0.04260992     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=     430  DYX=     130  DYW=     160
   D0Z=     828  D0Y=     515  D0X=      60  D0W=      90
  DDZI=     365 DDYI=     215 DDXI=      40 DDWI=      50
  DDZE=       0 DDYE=      70 DDXE=      15 DDWE=      21
================================================================================
Trace of MO density:     4.000000
    4  correlated and     8  frozen core electrons

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   4     0.00000000     0.00000000     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.00000000     0.00000000     1.74500018    -0.00000850     0.00051491    -0.00000003
   MO   6     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000850     0.57370157     0.00000119     0.00000800
   MO   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00051491     0.00000119     0.57217225     0.00000001
   MO   8     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000003     0.00000800     0.00000001     0.18852830
   MO   9     0.00000000     0.00000000     0.00000000     0.00000000     0.00000869     0.00000001    -0.00000812    -0.00000816
   MO  10     0.00000000     0.00000000     0.00000000     0.00000000     0.23562230    -0.00000034    -0.00000047    -0.00000004
   MO  11     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  12     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  13     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  14     0.00000000     0.00000000     0.00000000     0.00000000     0.00000077    -0.02580672     0.00018150     0.00000007
   MO  15     0.00000000     0.00000000     0.00000000     0.00000000    -0.00004958     0.00018219     0.02572611    -0.00000006
   MO  16     0.00000000     0.00000000     0.00000000     0.00000000     0.00000022     0.00000105     0.00000014     0.01206953
   MO  17     0.00000000     0.00000000     0.00000000     0.00000000     0.00000103     0.00000001    -0.00000033     0.00000144
   MO  18     0.00000000     0.00000000     0.00000000     0.00000000     0.02823554     0.00000006     0.00010133    -0.00000010
   MO  19     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  20     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  21     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  22     0.00000000     0.00000000     0.00000000     0.00000000    -0.05991092    -0.00000097     0.00005891     0.00000000
   MO  23     0.00000000     0.00000000     0.00000000     0.00000000     0.00013302    -0.00010372     0.02338781     0.00000002
   MO  24     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000038     0.02345541     0.00010353     0.00000018
   MO  25     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000007    -0.00000036     0.00000005    -0.01182081
   MO  26     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000092    -0.00000003     0.00000032    -0.00004528
   MO  27     0.00000000     0.00000000     0.00000000     0.00000000     0.02287007     0.00000013    -0.00008495    -0.00000004
   MO  28     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  29     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  30     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  31     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  32     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  33     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  34     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  35     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  36     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  37     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  38     0.00000000     0.00000000     0.00000000     0.00000000     0.00090830     0.00000005    -0.00000287     0.00000000

                MO   9         MO  10         MO  11         MO  12         MO  13         MO  14         MO  15         MO  16
   MO   5     0.00000869     0.23562230     0.00000000     0.00000000     0.00000000     0.00000077    -0.00004958     0.00000022
   MO   6     0.00000001    -0.00000034     0.00000000     0.00000000     0.00000000    -0.02580672     0.00018219     0.00000105
   MO   7    -0.00000812    -0.00000047     0.00000000     0.00000000     0.00000000     0.00018150     0.02572611     0.00000014
   MO   8    -0.00000816    -0.00000004     0.00000000     0.00000000     0.00000000     0.00000007    -0.00000006     0.01206953
   MO   9     0.18789074     0.00001352     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000027    -0.00000202
   MO  10     0.00001352     0.55367926     0.00000000     0.00000000     0.00000000    -0.00000055    -0.00009444     0.00000018
   MO  11     0.00000000     0.00000000     0.02491727    -0.00000029     0.00000059     0.00000000     0.00000000     0.00000000
   MO  12     0.00000000     0.00000000    -0.00000029     0.02489184     0.00000020     0.00000000     0.00000000     0.00000000
   MO  13     0.00000000     0.00000000     0.00000059     0.00000020     0.04020745     0.00000000     0.00000000     0.00000000
   MO  14     0.00000000    -0.00000055     0.00000000     0.00000000     0.00000000     0.00264063    -0.00000006    -0.00000005
   MO  15    -0.00000027    -0.00009444     0.00000000     0.00000000     0.00000000    -0.00000006     0.00263233     0.00000001
   MO  16    -0.00000202     0.00000018     0.00000000     0.00000000     0.00000000    -0.00000005     0.00000001     0.00169063
   MO  17     0.01204682     0.00000043     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000002    -0.00000002
   MO  18     0.00000044     0.02395010     0.00000000     0.00000000     0.00000000     0.00000000     0.00000040     0.00000001
   MO  19     0.00000000     0.00000000    -0.00002690     0.01650850    -0.00000001     0.00000000     0.00000000     0.00000000
   MO  20     0.00000000     0.00000000    -0.01652468    -0.00002650     0.00000048     0.00000000     0.00000000     0.00000000
   MO  21     0.00000000     0.00000000    -0.00000091    -0.00000021    -0.02624855     0.00000000     0.00000000     0.00000000
   MO  22     0.00000099     0.02695504     0.00000000     0.00000000     0.00000000     0.00000003    -0.00000206     0.00000001
   MO  23    -0.00000021     0.00008122     0.00000000     0.00000000     0.00000000     0.00003099     0.00269651     0.00000002
   MO  24    -0.00000001     0.00000020     0.00000000     0.00000000     0.00000000    -0.00270477     0.00003103     0.00000009
   MO  25     0.00004570    -0.00000007     0.00000000     0.00000000     0.00000000    -0.00000002     0.00000002    -0.00173683
   MO  26    -0.01180158    -0.00000047     0.00000000     0.00000000     0.00000000     0.00000000     0.00000003    -0.00000642
   MO  27     0.00000035     0.02237556     0.00000000     0.00000000     0.00000000    -0.00000015    -0.00002011     0.00000001
   MO  28     0.00000000     0.00000000     0.00000307    -0.00323313     0.00000003     0.00000000     0.00000000     0.00000000
   MO  29     0.00000000     0.00000000    -0.00323633    -0.00000300     0.00000002     0.00000000     0.00000000     0.00000000
   MO  30     0.00000000     0.00000000     0.00000013     0.00000006     0.00515753     0.00000000     0.00000000     0.00000000
   MO  31     0.00000000     0.00000000    -0.00000001     0.00000129     0.00000000     0.00000000     0.00000000     0.00000000
   MO  32     0.00000000     0.00000000     0.00000008    -0.00000637     0.00000000     0.00000000     0.00000000     0.00000000
   MO  33     0.00000000     0.00000000     0.00000000    -0.00000001     0.00000001     0.00000000     0.00000000     0.00000000
   MO  34     0.00000000     0.00000000     0.00000001     0.00000000     0.00000839     0.00000000     0.00000000     0.00000000
   MO  35     0.00000000     0.00000000    -0.00000087     0.00043134    -0.00000001     0.00000000     0.00000000     0.00000000
   MO  36     0.00000000     0.00000000     0.00042734     0.00000097     0.00000020     0.00000000     0.00000000     0.00000000
   MO  37     0.00000000     0.00000000     0.00000014     0.00000005     0.00411263     0.00000000     0.00000000     0.00000000
   MO  38    -0.00000005    -0.00131354     0.00000000     0.00000000     0.00000000     0.00000000     0.00000014     0.00000000

                MO  17         MO  18         MO  19         MO  20         MO  21         MO  22         MO  23         MO  24
   MO   5     0.00000103     0.02823554     0.00000000     0.00000000     0.00000000    -0.05991092     0.00013302    -0.00000038
   MO   6     0.00000001     0.00000006     0.00000000     0.00000000     0.00000000    -0.00000097    -0.00010372     0.02345541
   MO   7    -0.00000033     0.00010133     0.00000000     0.00000000     0.00000000     0.00005891     0.02338781     0.00010353
   MO   8     0.00000144    -0.00000010     0.00000000     0.00000000     0.00000000     0.00000000     0.00000002     0.00000018
   MO   9     0.01204682     0.00000044     0.00000000     0.00000000     0.00000000     0.00000099    -0.00000021    -0.00000001
   MO  10     0.00000043     0.02395010     0.00000000     0.00000000     0.00000000     0.02695504     0.00008122     0.00000020
   MO  11     0.00000000     0.00000000    -0.00002690    -0.01652468    -0.00000091     0.00000000     0.00000000     0.00000000
   MO  12     0.00000000     0.00000000     0.01650850    -0.00002650    -0.00000021     0.00000000     0.00000000     0.00000000
   MO  13     0.00000000     0.00000000    -0.00000001     0.00000048    -0.02624855     0.00000000     0.00000000     0.00000000
   MO  14     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000003     0.00003099    -0.00270477
   MO  15    -0.00000002     0.00000040     0.00000000     0.00000000     0.00000000    -0.00000206     0.00269651     0.00003103
   MO  16    -0.00000002     0.00000001     0.00000000     0.00000000     0.00000000     0.00000001     0.00000002     0.00000009
   MO  17     0.00168906     0.00000003     0.00000000     0.00000000     0.00000000     0.00000004    -0.00000002     0.00000000
   MO  18     0.00000003     0.00252974     0.00000000     0.00000000     0.00000000     0.00117152     0.00002013     0.00000008
   MO  19     0.00000000     0.00000000     0.01141885     0.00000014    -0.00000005     0.00000000     0.00000000     0.00000000
   MO  20     0.00000000     0.00000000     0.00000014     0.01142925     0.00000004     0.00000000     0.00000000     0.00000000
   MO  21     0.00000000     0.00000000    -0.00000005     0.00000004     0.01767445     0.00000000     0.00000000     0.00000000
   MO  22     0.00000004     0.00117152     0.00000000     0.00000000     0.00000000     0.01041388     0.00000668    -0.00000002
   MO  23    -0.00000002     0.00002013     0.00000000     0.00000000     0.00000000     0.00000668     0.00297099    -0.00000003
   MO  24     0.00000000     0.00000008     0.00000000     0.00000000     0.00000000    -0.00000002    -0.00000003     0.00297967
   MO  25     0.00000645     0.00000001     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000003
   MO  26    -0.00173523    -0.00000004     0.00000000     0.00000000     0.00000000    -0.00000005     0.00000003     0.00000000
   MO  27     0.00000003     0.00261673     0.00000000     0.00000000     0.00000000     0.00114859     0.00000013     0.00000000
   MO  28     0.00000000     0.00000000    -0.00255184     0.00000170    -0.00000001     0.00000000     0.00000000     0.00000000
   MO  29     0.00000000     0.00000000     0.00000176     0.00255404     0.00000006     0.00000000     0.00000000     0.00000000
   MO  30     0.00000000     0.00000000     0.00000002     0.00000003    -0.00387632     0.00000000     0.00000000     0.00000000
   MO  31     0.00000000     0.00000000     0.00000099    -0.00000078     0.00000000     0.00000000     0.00000000     0.00000000
   MO  32     0.00000000     0.00000000    -0.00000486    -0.00000021     0.00000000     0.00000000     0.00000000     0.00000000
   MO  33     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  34     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000578     0.00000000     0.00000000     0.00000000
   MO  35     0.00000000     0.00000000     0.00010100     0.00000001     0.00000000     0.00000000     0.00000000     0.00000000
   MO  36     0.00000000     0.00000000     0.00000009    -0.00009803    -0.00000015     0.00000000     0.00000000     0.00000000
   MO  37     0.00000000     0.00000000     0.00000002    -0.00000001    -0.00296688     0.00000000     0.00000000     0.00000000
   MO  38     0.00000000    -0.00007708     0.00000000     0.00000000     0.00000000    -0.00045135    -0.00000046     0.00000000

                MO  25         MO  26         MO  27         MO  28         MO  29         MO  30         MO  31         MO  32
   MO   5    -0.00000007    -0.00000092     0.02287007     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   6    -0.00000036    -0.00000003     0.00000013     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   7     0.00000005     0.00000032    -0.00008495     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   8    -0.01182081    -0.00004528    -0.00000004     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   9     0.00004570    -0.01180158     0.00000035     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  10    -0.00000007    -0.00000047     0.02237556     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  11     0.00000000     0.00000000     0.00000000     0.00000307    -0.00323633     0.00000013    -0.00000001     0.00000008
   MO  12     0.00000000     0.00000000     0.00000000    -0.00323313    -0.00000300     0.00000006     0.00000129    -0.00000637
   MO  13     0.00000000     0.00000000     0.00000000     0.00000003     0.00000002     0.00515753     0.00000000     0.00000000
   MO  14    -0.00000002     0.00000000    -0.00000015     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  15     0.00000002     0.00000003    -0.00002011     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  16    -0.00173683    -0.00000642     0.00000001     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  17     0.00000645    -0.00173523     0.00000003     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  18     0.00000001    -0.00000004     0.00261673     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  19     0.00000000     0.00000000     0.00000000    -0.00255184     0.00000176     0.00000002     0.00000099    -0.00000486
   MO  20     0.00000000     0.00000000     0.00000000     0.00000170     0.00255404     0.00000003    -0.00000078    -0.00000021
   MO  21     0.00000000     0.00000000     0.00000000    -0.00000001     0.00000006    -0.00387632     0.00000000     0.00000000
   MO  22     0.00000000    -0.00000005     0.00114859     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  23     0.00000000     0.00000003     0.00000013     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  24    -0.00000003     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  25     0.00186229    -0.00000002     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  26    -0.00000002     0.00186045    -0.00000005     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  27     0.00000000    -0.00000005     0.00300724     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  28     0.00000000     0.00000000     0.00000000     0.00081008    -0.00000001     0.00000000    -0.00000035     0.00000168
   MO  29     0.00000000     0.00000000     0.00000000    -0.00000001     0.00081070    -0.00000001    -0.00000066    -0.00000015
   MO  30     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000001     0.00118352     0.00000000     0.00000000
   MO  31     0.00000000     0.00000000     0.00000000    -0.00000035    -0.00000066     0.00000000     0.00237001     0.00000000
   MO  32     0.00000000     0.00000000     0.00000000     0.00000168    -0.00000015     0.00000000     0.00000000     0.00237001
   MO  33     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000001    -0.00000003
   MO  34     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000165     0.00000004     0.00000001
   MO  35     0.00000000     0.00000000     0.00000000     0.00005989    -0.00000008     0.00000000    -0.00000032     0.00000150
   MO  36     0.00000000     0.00000000     0.00000000     0.00000006     0.00006084     0.00000005    -0.00000033    -0.00000007
   MO  37     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000002     0.00091003     0.00000000     0.00000000
   MO  38     0.00000000     0.00000000    -0.00007972     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                MO  33         MO  34         MO  35         MO  36         MO  37         MO  38
   MO   5     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00090830
   MO   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000005
   MO   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000287
   MO   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   9     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000005
   MO  10     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00131354
   MO  11     0.00000000     0.00000001    -0.00000087     0.00042734     0.00000014     0.00000000
   MO  12    -0.00000001     0.00000000     0.00043134     0.00000097     0.00000005     0.00000000
   MO  13     0.00000001     0.00000839    -0.00000001     0.00000020     0.00411263     0.00000000
   MO  14     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  15     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000014
   MO  16     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  17     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  18     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00007708
   MO  19     0.00000000     0.00000000     0.00010100     0.00000009     0.00000002     0.00000000
   MO  20     0.00000000     0.00000000     0.00000001    -0.00009803    -0.00000001     0.00000000
   MO  21     0.00000000    -0.00000578     0.00000000    -0.00000015    -0.00296688     0.00000000
   MO  22     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00045135
   MO  23     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000046
   MO  24     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  25     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  26     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  27     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00007972
   MO  28     0.00000000     0.00000000     0.00005989     0.00000006     0.00000000     0.00000000
   MO  29     0.00000000     0.00000000    -0.00000008     0.00006084    -0.00000002     0.00000000
   MO  30     0.00000000     0.00000165     0.00000000     0.00000005     0.00091003     0.00000000
   MO  31     0.00000001     0.00000004    -0.00000032    -0.00000033     0.00000000     0.00000000
   MO  32    -0.00000003     0.00000001     0.00000150    -0.00000007     0.00000000     0.00000000
   MO  33     0.00095571     0.00000000    -0.00000002     0.00000000     0.00000002     0.00000000
   MO  34     0.00000000     0.00095420     0.00000000     0.00000002     0.00000242     0.00000000
   MO  35    -0.00000002     0.00000000     0.00144878     0.00000002     0.00000000     0.00000000
   MO  36     0.00000000     0.00000002     0.00000002     0.00144708     0.00000002     0.00000000
   MO  37     0.00000002     0.00000242     0.00000000     0.00000002     0.00183489     0.00000000
   MO  38     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00002671

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     1.79251046     0.57583393     0.57429741     0.51288456
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     0.19005835     0.18942041     0.05869626     0.03649929     0.03646325     0.00629114     0.00340959     0.00339995
              MO    17       MO    18       MO    19       MO    20       MO    21       MO    22       MO    23       MO    24
  occ(*)=     0.00287211     0.00237001     0.00237001     0.00198877     0.00198560     0.00157313     0.00148627     0.00148496
              MO    25       MO    26       MO    27       MO    28       MO    29       MO    30       MO    31       MO    32
  occ(*)=     0.00095571     0.00095419     0.00061659     0.00060268     0.00060266     0.00009459     0.00007835     0.00007815
              MO    33       MO    34       MO    35       MO    36       MO    37       MO    38       MO
  occ(*)=     0.00003420     0.00003413     0.00001737     0.00001736     0.00001434     0.00000420


 total number of electrons =   12.0000000000

 test slabel:                     1
  a  


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        a   partial gross atomic populations
   ao class       1a         2a         3a         4a         5a         6a  
     3_ s       0.000000   2.000000   0.000000   0.000000   1.729068   0.000000
     3_ p       2.000000   0.000000   2.000000   2.000000   0.000000   0.000000
     3_ d       0.000000   0.000000   0.000000   0.000000   0.063443   0.575834
     3_ f       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
 
   ao class       7a         8a         9a        10a        11a        12a  
     3_ s       0.000000   0.020419   0.000000   0.000000   0.000000   0.000000
     3_ p       0.000000   0.000000   0.000000   0.000000   0.058221   0.036495
     3_ d       0.574297   0.492466   0.190058   0.189420   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000000   0.000475   0.000005
 
   ao class      13a        14a        15a        16a        17a        18a  
     3_ s       0.000000   0.005691   0.000000   0.000000   0.000257   0.000000
     3_ p       0.036458   0.000000   0.000000   0.000000   0.000000   0.000000
     3_ d       0.000000   0.000600   0.003410   0.003400   0.002616   0.000000
     3_ f       0.000005   0.000000   0.000000   0.000000   0.000000   0.002370
 
   ao class      19a        20a        21a        22a        23a        24a  
     3_ s       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     3_ p       0.000000   0.000000   0.000000   0.000339   0.000070   0.000071
     3_ d       0.000000   0.001989   0.001986   0.000000   0.000000   0.000000
     3_ f       0.002370   0.000000   0.000000   0.001234   0.001416   0.001414
 
   ao class      25a        26a        27a        28a        29a        30a  
     3_ s       0.000000   0.000000   0.000000   0.000000   0.000000   0.000001
     3_ p       0.000000   0.000000   0.000491   0.000574   0.000575   0.000000
     3_ d       0.000000   0.000000   0.000000   0.000000   0.000000   0.000094
     3_ f       0.000956   0.000954   0.000126   0.000028   0.000028   0.000000
 
   ao class      31a        32a        33a        34a        35a        36a  
     3_ s       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     3_ p       0.000000   0.000000   0.000000   0.000000   0.000017   0.000017
     3_ d       0.000078   0.000078   0.000034   0.000034   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
 
   ao class      37a        38a  
     3_ s       0.000000   0.000004
     3_ p       0.000014   0.000000
     3_ d       0.000000   0.000000
     3_ f       0.000000   0.000000


                        gross atomic populations
     ao            3_
      s         3.755439
      p         6.133343
      d         2.099837
      f         0.011381
    total      12.000000
 

 Total number of electrons:   12.00000000

 DA ...
