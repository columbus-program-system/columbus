1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=    28)...
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.3018785761  1.9096E-14  5.1977E-02  2.1139E-01  1.0000E-03

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  1  1    -46.3018785761  1.9096E-14  5.1977E-02  2.1139E-01  1.0000E-03

 from bk iterations: iconv=   1

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.3455856398  4.3707E-02  1.8568E-03  4.0095E-02  1.0000E-03
 mr-sdci #  2  1    -46.3476186362  2.0330E-03  2.4189E-04  1.4151E-02  1.0000E-03
 mr-sdci #  3  1    -46.3478457706  2.2713E-04  4.1077E-05  6.0676E-03  1.0000E-03
 mr-sdci #  4  1    -46.3478795938  3.3823E-05  3.2757E-06  1.6721E-03  1.0000E-03
 mr-sdci #  5  1    -46.3478829178  3.3240E-06  2.8081E-07  4.8258E-04  1.0000E-03

 mr-sdci  convergence criteria satisfied after  5 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  5  1    -46.3478829178  3.3240E-06  2.8081E-07  4.8258E-04  1.0000E-03

 number of reference csfs (nref) is    28.  root number (iroot) is  1.
 c0**2 =   0.93098618  c**2 (all zwalks) =   0.93098618

 eref      =    -46.301867310060   "relaxed" cnot**2         =   0.930986178089
 eci       =    -46.347882917770   deltae = eci - eref       =  -0.046015607710
 eci+dv1   =    -46.351058630725   dv1 = (1-cnot**2)*deltae  =  -0.003175712956
 eci+dv2   =    -46.351294045701   dv2 = dv1 / cnot**2       =  -0.003411127931
 eci+dv3   =    -46.351567157679   dv3 = dv1 / (2*cnot**2-1) =  -0.003684239910
 eci+pople =    -46.349586145036   (  4e- scaled deltae )    =  -0.047718834976
