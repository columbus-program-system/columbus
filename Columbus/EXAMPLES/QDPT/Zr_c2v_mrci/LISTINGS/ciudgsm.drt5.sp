1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=    21)...
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.3018785760  8.8818E-15  5.2032E-02  2.1139E-01  1.0000E-03

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  1  1    -46.3018785760  8.8818E-15  5.2032E-02  2.1139E-01  1.0000E-03

 from bk iterations: iconv=   1

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.3455178994  4.3639E-02  1.8920E-03  4.0410E-02  1.0000E-03
 mr-sdci #  2  1    -46.3476003123  2.0824E-03  2.5496E-04  1.4525E-02  1.0000E-03
 mr-sdci #  3  1    -46.3478430852  2.4277E-04  4.3662E-05  6.2846E-03  1.0000E-03
 mr-sdci #  4  1    -46.3478791263  3.6041E-05  3.8025E-06  1.7823E-03  1.0000E-03
 mr-sdci #  5  1    -46.3478828351  3.7089E-06  3.7007E-07  5.6412E-04  1.0000E-03

 mr-sdci  convergence criteria satisfied after  5 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  5  1    -46.3478828351  3.7089E-06  3.7007E-07  5.6412E-04  1.0000E-03

 number of reference csfs (nref) is    21.  root number (iroot) is  1.
 c0**2 =   0.93098595  c**2 (all zwalks) =   0.93098595

 eref      =    -46.301867251409   "relaxed" cnot**2         =   0.930985948996
 eci       =    -46.347882835135   deltae = eci - eref       =  -0.046015583727
 eci+dv1   =    -46.351058556978   dv1 = (1-cnot**2)*deltae  =  -0.003175721842
 eci+dv2   =    -46.351293973451   dv2 = dv1 / cnot**2       =  -0.003411138316
 eci+dv3   =    -46.351567087313   dv3 = dv1 / (2*cnot**2-1) =  -0.003684252178
 eci+pople =    -46.349586067570   (  4e- scaled deltae )    =  -0.047718816161
