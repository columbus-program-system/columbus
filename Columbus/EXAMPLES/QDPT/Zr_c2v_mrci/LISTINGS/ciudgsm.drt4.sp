1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=    24)...
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.2740792724  2.2204E-15  5.7232E-02  2.2790E-01  1.0000E-03

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  1  1    -46.2740792724  2.2204E-15  5.7232E-02  2.2790E-01  1.0000E-03

 from bk iterations: iconv=   1

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.3229759636  4.8897E-02  2.4910E-03  4.5571E-02  1.0000E-03
 mr-sdci #  2  1    -46.3255597033  2.5837E-03  3.8644E-04  1.7424E-02  1.0000E-03
 mr-sdci #  3  1    -46.3258930006  3.3330E-04  7.2160E-05  7.9571E-03  1.0000E-03
 mr-sdci #  4  1    -46.3259516884  5.8688E-05  6.9148E-06  2.4052E-03  1.0000E-03
 mr-sdci #  5  1    -46.3259581327  6.4443E-06  7.4551E-07  8.0428E-04  1.0000E-03

 mr-sdci  convergence criteria satisfied after  5 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  5  1    -46.3259581327  6.4443E-06  7.4551E-07  8.0428E-04  1.0000E-03

 number of reference csfs (nref) is    24.  root number (iroot) is  1.
 c0**2 =   0.92261706  c**2 (all zwalks) =   0.92261706

 eref      =    -46.274037273244   "relaxed" cnot**2         =   0.922617059931
 eci       =    -46.325958132727   deltae = eci - eref       =  -0.051920859483
 eci+dv1   =    -46.329975921485   dv1 = (1-cnot**2)*deltae  =  -0.004017788758
 eci+dv2   =    -46.330312906698   dv2 = dv1 / cnot**2       =  -0.004354773971
 eci+dv3   =    -46.330711595305   dv3 = dv1 / (2*cnot**2-1) =  -0.004753462577
 eci+pople =    -46.328131703790   (  4e- scaled deltae )    =  -0.054094430546
