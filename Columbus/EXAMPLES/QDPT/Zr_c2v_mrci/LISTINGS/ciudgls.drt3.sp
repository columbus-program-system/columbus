1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      2009-03.    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


================ Computing sorting integral file structure ================

                    -----z----- -----y----- -----x----- -----w----- ---total---

                CSFs        24         490        1414        2114        4042
      internal walks       105          70          15          21         211
valid internal walks        24          70          15          21         130
 getinfoarray: info=                     6 :                     1
                  8192                  6552                  8192
                  5460                     0
 icd(3)=                  2976 ci%nnlev=                   595  l2rec=
                  8192  n2max=                  5460
 lcore1,lcore2=              13068137              13044821
 lencor,maxblo              13107200                 60000
========================================
 current settings:
 minbl3         381
 minbl4         381
 locmaxbl3     4288
 locmaxbuf     2144
 maxbl3       60000
 maxbl3       60000
 maxbl4       60000
 maxbuf       30006
========================================

 sorted 4-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)

 sorted 3-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)
 Orig.  diagonal integrals:  1electron:        34
                             0ext.    :        42
                             2ext.    :       336
                             4ext.    :       812


 Orig. off-diag. integrals:  4ext.    :     24411
                             3ext.    :     18720
                             2ext.    :      6792
                             1ext.    :      1272
                             0ext.    :        99
                             2ext. SO :         0
                             1ext. SO :         0
                             0ext. SO :         0
                             1electron:       128


 Sorted integrals            3ext.  w :     17370 x :     16020
                             4ext.  w :     20955 x :     17777


Cycle #  1 sortfile size=    163835(       5 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13068137
Cycle #  2 sortfile size=    163835(       5 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13068137
 minimum size of srtscr:     98301 WP (     3 records)
 maximum size of srtscr:    163835 WP (     5 records)
diagi   file:      4 records  of   1536 WP each=>       6144 WP total
ofdgi   file:      8 records  of   1536 WP each=>      12288 WP total
fil3w   file:      1 records  of  30006 WP each=>      30006 WP total
fil3x   file:      1 records  of  30006 WP each=>      30006 WP total
fil4w   file:      1 records  of  30006 WP each=>      30006 WP total
fil4x   file:      1 records  of  30006 WP each=>      30006 WP total
 compressed index vector length=                    63
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 0,
  GSET = 0,
   DAVCOR =10,
  NCOREL = 4
   nroot=1
  IVMODE = 3
  NBKITR = 1
   nvbkmn=2
  RTOLBK = 1e-3
   niter=30
   nvcimn=2
   rtolci=1e-3
  NVCIMX = 6
  NVRFMX = 6
  NVBKMX = 6
  IDEN  = 1
  CSFPRN = 10,
  update_mode=1
  nvrfmn=2
 /&end
 ------------------------------------------------------------------------
lodens (list->root)=  1
invlodens (root->list)=  1
 USING SEGMENTS OF EQUAL SIZE

****************  list of control variables  ****************
 lvlprt =    0      nroot  =    1      noldv  =   0      noldhv =   0
 nunitv =    1      nbkitr =    1      niter  =  30      davcor =  10
 csfprn =   10      ivmode =    3      istrt  =   0      vout   =   0
 iortls =    0      nvbkmx =    6      ibktv  =  -1      ibkthv =  -1
 nvcimx =    6      icitv  =   -1      icithv =  -1      frcsub =   0
 nvbkmn =    2      nvcimn =    2      maxseg =   4      nrfitr =  30
 ncorel =    4      nvrfmx =    6      nvrfmn =   2      iden   =   1
 itran  =    0      froot  =    0      rtmode =   0      ncouple=   1
 skipso =    F      dalton2=    0      molcas =   0      finalv =   0
 finalw =    0      cosmocalc=   0    with_tsklst=   0
 nsegwx =    1     1     1     1
 nseg0x =    1     1     1     1
 nseg1x =    1     1     1     1
 nseg2x =    1     1     1     1
 nseg3x =    1     1     1     1
 nseg4x =    1     1     1     1
 no0ex  =      0    no1ex  =      0    no2ex  =     0    no3ex  =     0
 no4ex  =      0    nodiag =      0
 cdg4ex =    1      c3ex1ex=    1      c2ex0ex=   1
 fileloc=    0     0     0     0     0     0     0     1     1     1
 directhd=   1      noaqccshift_zyxw=      0
 critical_crit=-1.00000    critical_delta= 0.05000

 ctol   = 0.010000    lrtshift=1.000000    smalld =0.001000


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-03    1.000E-03
 Computing density:                    .drt1.state1
 using                      1  nodes and                      1  cores.
 szdg/szodg per processor=                  6144                 12288
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core           13107199 DP per process

********** Integral sort section *************


 workspace allocation information: lencor=  13107199

 echo of the input for program cisrt:
 ------------------------------------------------------------------------
  &input
  maxbl3=60000
  maxbl4=60000
  &end
 ------------------------------------------------------------------------
 
 ( 6) listing file:                    ciudgls             
 ( 5) input file:                      cisrtin   
 (17) cidrt file:                      cidrtfl             
 (11) transformed integrals file:      moints    
 (12) diagonal integral file:          diagint             
 (13) off-diagonal integral file:      ofdgint             
 (31) 4-external w integrals file:     fil4w               
 (32) 4-external x integrals file:     fil4x               
 (33) 3-external w integrals file:     fil3w               
 (34) 3-external x integrals file:     fil3x               
 (21) scratch da sorting file:         srtscr              
 (12) 2-e integral file [fsplit=2]:    moints2   

 input integral file header information:
                                                                                 
 aoints SIFS file created by argos.      zam792            17:53:52.086 17-Dec-13
  cidrt_title                                                                    
 MO-coefficients from mcscf.x                                                    
  with dummy occupation 1.0 for active orbitals                                  
  total ao core energy =    0.000000000                                          
 MCSCF energy =     -46.301878576                                                
 SIFS file created by program tran.      zam792            17:53:52.407 17-Dec-13

 input energy(*) values:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total core energy =   0.000000000000E+00

 nsym = 4 nmot=  34

 symmetry  =    1    2    3    4
 slabel(*) =   a1   a2   b1   b2
 nmpsy(*)  =   14    4    8    8

 info(*) =          1      8192      6552      8192      5460         0

 orbital labels, i:molab(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012  13:tout:013  14:tout:014  15:tout:015  16:tout:016  17:tout:017  18:tout:018  19:tout:019  20:tout:020
  21:tout:021  22:tout:022  23:tout:023  24:tout:024  25:tout:025  26:tout:026  27:tout:027  28:tout:028  29:tout:029  30:tout:030
  31:tout:031  32:tout:032  33:tout:033  34:tout:034

 input parameters:
 prnopt=  0
 ldamin=    4095 ldamax=   32767 ldainc=      64
 maxbuf=   30006 maxbl3=   60000 maxbl4=   60000 intmxo=    1536
  Using 32 bit compression 

 drt information:
  cidrt_title                                                                    
 nmotd =  38 nfctd =   4 nfvtc =   0 nmot  =  34
 nlevel =  34 niot  =   6 lowinl=  29
 orbital-to-level map(*)
   -1  -1  29  30  31   1   2   3   4   5   6   7   8   9  10  11  32  12  13  14
   -1  33  15  16  17  18  19  20  21  -1  34  22  23  24  25  26  27  28
 compressed map(*)
   29  30  31   1   2   3   4   5   6   7   8   9  10  11  32  12  13  14  33  15
   16  17  18  19  20  21  34  22  23  24  25  26  27  28
 levsym(*)
    1   1   1   1   1   1   1   1   1   1   1   2   2   2   3   3   3   3   3   3
    3   4   4   4   4   4   4   4   1   1   1   2   3   4
 repartitioning mu(*)=
   0.  0.  0.  0.  0.  0.

 new core energy added to the energy(*) list.
 from the integral file: h1_core= -4.349858765627E+01

 indxdg: diagonal integral statistics.
 total number of integrals contributing to diagonal matrix elements:      1190
 number with all external indices:       812
 number with half external - half internal indices:       336
 number with all internal indices:        42

 indxof: off-diagonal integral statistics.
    4-external integrals: num=      24411 strt=          1
    3-external integrals: num=      18720 strt=      24412
    2-external integrals: num=       6792 strt=      43132
    1-external integrals: num=       1272 strt=      49924
    0-external integrals: num=         99 strt=      51196

 total number of off-diagonal integrals:       51294


 indxof(2nd)  ittp=   3 numx(ittp)=        6792
 indxof(2nd)  ittp=   4 numx(ittp)=        1272
 indxof(2nd)  ittp=   5 numx(ittp)=          99

 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg=  12931980
 pro2e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652

 pro2e:     25126 integrals read in     5 records.

 pro2e:         0 integrals 34-ext integrals skipped.
 pro1e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652
 pro1e: eref =    0.000000000000000E+00
 total size of srtscr:                     4  records of                  32767 
 WP =               1048544 Bytes
 putdg        1     596    1191    1786    3322   36089   57934    3018   46706   90394
   123161  131353  136813  158652

 putf:       4 buffers of length    1536 written to file 12
 diagonal integral file completed.

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep4e:    17 blocks of linear combinations of 4-external integrals processed.
 number of sorted 4-external integrals      38732
 number of original 4-external integrals    24411


 putf34: external integral file complete. nfilw=    31 nfilx=    32 nrecw=     1 nrecx=     1 lbufp= 30006

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep3e:    19 blocks of linear combinations of 3-external integrals processed.
 number of sorted 3-external integrals      33390
 number of original 3-external integrals    18720


 putf34: external integral file complete. nfilw=    33 nfilx=    34 nrecw=     1 nrecx=     1 lbufp= 30006
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      6792         3     43132     43132      6792     51294
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      1272         4     49924     49924      1272     51294
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd        99         5     51196     51196        99     51294

 putf:       8 buffers of length    1536 written to file 13
 off-diagonal files sort completed.
 executing brd_struct for cisrtinfo
cisrtinfo:
bufszi  1536
 diagfile 4ext:     812 2ext:     336 0ext:      42
 fil4w,fil4x  :   24411 fil3w,fil3x :   18720
 ofdgint  2ext:    6792 1ext:    1272 0ext:      99so0ext:       0so1ext:       0so2ext:       0
buffer minbl4     381 minbl3     381 maxbl2     384nbas:  11   3   7   7   0   0   0   0 maxbuf 30006
 CIUDG version 5.9.7 ( 5-Oct-2004)

 workspace allocation information: lcore=  13107199

 core energy values from the integral file:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -4.349858765627E+01, ietype=    6,   fcore energy of type: H1(*)   

 total core repulsion energy = -4.349858765627E+01
 nmot  =    38 niot  =     6 nfct  =     4 nfvt  =     0
 nrow  =    32 nsym  =     4 ssym  =     3 lenbuf=  1600
 nwalk,xbar:        211      105       70       15       21
 nvalwt,nvalw:      130       24       70       15       21
 ncsft:            4042
 total number of valid internal walks:     130
 nvalz,nvaly,nvalx,nvalw =       24      70      15      21

 cisrt info file parameters:
 file number  12 blocksize   1536
 mxbld   1536
 nd4ext,nd2ext,nd0ext   812   336    42
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int    24411    18720     6792     1272       99        0        0        0
 minbl4,minbl3,maxbl2   381   381   384
 maxbuf 30006
 number of external orbitals per symmetry block:  11   3   7   7
 nmsym   4 number of internal orbitals   6
 executing brd_struct for drt
 executing brd_struct for orbinf
 executing brd_struct for momap
 calcthrxt: niot,maxw1=                     6                    31
 block size     0
 pthz,pthy,pthx,pthw:   105    70    15    21 total internal walks:     211
 maxlp3,n2lp,n1lp,n0lp    31     0     0     0
 orbsym(*)= 1 1 1 2 3 4

 setref:       24 references kept,
               81 references were marked as invalid, out of
              105 total.
 nmb.of records onel     1
 nmb.of records 2-ext     5
 nmb.of records 1-ext     1
 nmb.of records 0-ext     1
 nmb.of records 2-int     0
 nmb.of records 1-int     0
 nmb.of records 0-int     0
 ---------memory usage in DP -----------------
 < n-ex core usage >
     routines:
    fourex            61797
    threx             60199
    twoex              3549
    onex               1823
    allin              1536
    diagon             2109
               =======
   maximum            61797
 
  __ static summary __ 
   reflst                24
   hrfspc                24
               -------
   static->              24
 
  __ core required  __ 
   totstc                24
   max n-ex           61797
               -------
   totnec->           61821
 
  __ core available __ 
   totspc          13107199
   totnec -           61821
               -------
   totvec->        13045378

 number of external paths / symmetry
 vertex x     100      82      98      98
 vertex w     128      82      98      98
segment: free space=    13045378
 reducing frespc by                   774 to               13044604 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1          47|        24|         0|        24|         0|         1|
 -------------------------------------------------------------------------------
  Y 2          70|       490|        24|        70|        24|         2|
 -------------------------------------------------------------------------------
  X 3          15|      1414|       514|        15|        94|         3|
 -------------------------------------------------------------------------------
  W 4          21|      2114|      1928|        21|       109|         4|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=          60DP  conft+indsym=         280DP  drtbuffer=         434 DP

dimension of the ci-matrix ->>>      4042

 executing brd_struct for civct
 gentasklist: ntask=                    20
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS   
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      15      47       1414         24      15      24
     2  4   1    25      two-ext wz   2X  4 1      21      47       2114         24      21      24
     3  4   3    26      two-ext wx*  WX  4 3      21      15       2114       1414      21      15
     4  4   3    27      two-ext wx+  WX  4 3      21      15       2114       1414      21      15
     5  2   1    11      one-ext yz   1X  2 1      70      47        490         24      70      24
     6  3   2    15      1ex3ex yx    3X  3 2      15      70       1414        490      15      70
     7  4   2    16      1ex3ex yw    3X  4 2      21      70       2114        490      21      70
     8  1   1     1      allint zz    OX  1 1      47      47         24         24      24      24
     9  2   2     5      0ex2ex yy    OX  2 2      70      70        490        490      70      70
    10  3   3     6      0ex2ex xx*   OX  3 3      15      15       1414       1414      15      15
    11  3   3    18      0ex2ex xx+   OX  3 3      15      15       1414       1414      15      15
    12  4   4     7      0ex2ex ww*   OX  4 4      21      21       2114       2114      21      21
    13  4   4    19      0ex2ex ww+   OX  4 4      21      21       2114       2114      21      21
    14  2   2    42      four-ext y   4X  2 2      70      70        490        490      70      70
    15  3   3    43      four-ext x   4X  3 3      15      15       1414       1414      15      15
    16  4   4    44      four-ext w   4X  4 4      21      21       2114       2114      21      21
    17  1   1    75      dg-024ext z  OX  1 1      47      47         24         24      24      24
    18  2   2    76      dg-024ext y  OX  2 2      70      70        490        490      70      70
    19  3   3    77      dg-024ext x  OX  3 3      15      15       1414       1414      15      15
    20  4   4    78      dg-024ext w  OX  4 4      21      21       2114       2114      21      21
----------------------------------------------------------------------------------------------------
REDTASK #   1 TIME=  19.000 N=  1 (task/type/sgbra)=(   1/24/0) (
REDTASK #   2 TIME=  18.000 N=  1 (task/type/sgbra)=(   2/25/0) (
REDTASK #   3 TIME=  17.000 N=  1 (task/type/sgbra)=(   3/26/1) (
REDTASK #   4 TIME=  16.000 N=  1 (task/type/sgbra)=(   4/27/2) (
REDTASK #   5 TIME=  15.000 N=  1 (task/type/sgbra)=(   5/11/0) (
REDTASK #   6 TIME=  14.000 N=  1 (task/type/sgbra)=(   6/15/0) (
REDTASK #   7 TIME=  13.000 N=  1 (task/type/sgbra)=(   7/16/0) (
REDTASK #   8 TIME=  12.000 N=  1 (task/type/sgbra)=(   8/ 1/0) (
REDTASK #   9 TIME=  11.000 N=  1 (task/type/sgbra)=(   9/ 5/0) (
REDTASK #  10 TIME=  10.000 N=  1 (task/type/sgbra)=(  10/ 6/1) (
REDTASK #  11 TIME=   9.000 N=  1 (task/type/sgbra)=(  11/18/2) (
REDTASK #  12 TIME=   8.000 N=  1 (task/type/sgbra)=(  12/ 7/1) (
REDTASK #  13 TIME=   7.000 N=  1 (task/type/sgbra)=(  13/19/2) (
REDTASK #  14 TIME=   6.000 N=  1 (task/type/sgbra)=(  14/42/1) (
REDTASK #  15 TIME=   5.000 N=  1 (task/type/sgbra)=(  15/43/1) (
REDTASK #  16 TIME=   4.000 N=  1 (task/type/sgbra)=(  16/44/1) (
REDTASK #  17 TIME=   3.000 N=  1 (task/type/sgbra)=(  17/75/1) (
REDTASK #  18 TIME=   2.000 N=  1 (task/type/sgbra)=(  18/76/1) (
REDTASK #  19 TIME=   1.000 N=  1 (task/type/sgbra)=(  19/77/1) (
REDTASK #  20 TIME=   0.000 N=  1 (task/type/sgbra)=(  20/78/1) (
 initializing v-file: 1:                  4042

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         176 2x:           0 4x:           0
All internal counts: zz :         271 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 reference space has dimension      24
 dsyevx: computed roots 1 to    2(converged:   2)

    root           eigenvalues
    ----           ------------
       1         -46.2740792781
       2         -46.2533198525

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=                    24

         vector  1 from unit 11 written to unit 49 filename cirefv              

 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=    24)...
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              4042
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                    1
 residual norm convergence criteria:               0.001000

          starting bk iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         602 2x:         246 4x:         106
All internal counts: zz :         271 yy:           0 xx:           0 ww:           0
One-external counts: yz :        1072 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:          72 wz:         104 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1
   ht   1    -2.77549162

          calcsovref: tciref block   1

              civs   1
 refs   1    1.00000    

          calcsovref: scrb block   1

                ci   1
 civs   1    1.00000    

          calcsovref: sovref block   1

              v      1
 ref    1    1.00000    

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.2740792781 -7.1054E-15  5.7198E-02  2.2790E-01  1.0000E-03
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.2740792781 -7.1054E-15  5.7198E-02  2.2790E-01  1.0000E-03
 
diagon:itrnv=   2
 expansion vectors are not transformed.
 matrix-vector products are not transformed.

    2 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              4042
 number of initial trial vectors:                         2
 number of initial matrix-vector products:                1
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                   30
 residual norm convergence criteria:               0.001000

          starting ci iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         602 2x:         246 4x:         106
All internal counts: zz :         271 yy:         620 xx:          66 ww:         132
One-external counts: yz :        1072 yx:        1000 yw:        1210
Two-external counts: yy :         865 ww:         252 xx:         180 xz:          72 wz:         104 wx:         360
Three-ext.   counts: yx :         320 yw:         395

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2
   ht   1    -2.77549162
   ht   2    -0.05719782    -0.18659037

          calcsovref: tciref block   1

              civs   1       civs   2
 refs   1    1.00000      -3.196956E-14

          calcsovref: scrb block   1

                ci   1         ci   2
 civs   1   0.968731      -0.248113    
 civs   2   0.828184        3.23356    

          calcsovref: sovref block   1

              v      1       v      2
 ref    1   0.968731      -0.248113    

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.96873117    -0.24811270

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.3229786118  4.8899E-02  2.4855E-03  4.5539E-02  1.0000E-03
 mr-sdci #  1  2    -45.5286413250  2.0301E+00  0.0000E+00  5.3048E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   2

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         602 2x:         246 4x:         106
All internal counts: zz :         271 yy:         620 xx:          66 ww:         132
One-external counts: yz :        1072 yx:        1000 yw:        1210
Two-external counts: yy :         865 ww:         252 xx:         180 xz:          72 wz:         104 wx:         360
Three-ext.   counts: yx :         320 yw:         395

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3
   ht   1    -2.77549162
   ht   2    -0.05719782    -0.18659037
   ht   3     0.00685396    -0.00269846    -0.00876154

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3
 refs   1    1.00000      -3.196956E-14  -2.470445E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3
 civs   1   0.966061      -0.153636       0.211319    
 civs   2   0.867160        1.60652       -2.79446    
 civs   3    1.03662        13.5941        8.08079    

          calcsovref: sovref block   1

              v      1       v      2       v      3
 ref    1   0.963500      -0.187220       0.191355    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.96349983    -0.18721957     0.19135548

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1    -46.3255608524  2.5822E-03  3.8521E-04  1.7388E-02  1.0000E-03
 mr-sdci #  2  2    -45.7830680198  2.5443E-01  0.0000E+00  4.5430E-01  1.0000E-04
 mr-sdci #  2  3    -45.4389060590  1.9403E+00  0.0000E+00  5.2807E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   3

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         602 2x:         246 4x:         106
All internal counts: zz :         271 yy:         620 xx:          66 ww:         132
One-external counts: yz :        1072 yx:        1000 yw:        1210
Two-external counts: yy :         865 ww:         252 xx:         180 xz:          72 wz:         104 wx:         360
Three-ext.   counts: yx :         320 yw:         395

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4
   ht   1    -2.77549162
   ht   2    -0.05719782    -0.18659037
   ht   3     0.00685396    -0.00269846    -0.00876154
   ht   4    -0.00041319    -0.00333157    -0.00048530    -0.00143128

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4
 refs   1    1.00000      -3.196956E-14  -2.470445E-03   1.702334E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4
 civs   1  -0.964072       0.151830       0.219379       3.122903E-02
 civs   2  -0.871677       -1.05003       -2.96113      -0.870340    
 civs   3   -1.16886       -11.3503        6.79383       -8.65931    
 civs   4  -0.861874       -20.5697        4.50120        32.4045    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4
 ref    1  -0.961332       0.176368       0.203361       5.813771E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1    -0.96133157     0.17636819     0.20336146     0.05813771

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1    -46.3258929574  3.3211E-04  7.2397E-05  7.9523E-03  1.0000E-03
 mr-sdci #  3  2    -45.9402826808  1.5721E-01  0.0000E+00  3.1351E-01  1.0000E-04
 mr-sdci #  3  3    -45.4400068942  1.1008E-03  0.0000E+00  5.3234E-01  1.0000E-04
 mr-sdci #  3  4    -45.3843570359  1.8858E+00  0.0000E+00  5.7291E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   4

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         602 2x:         246 4x:         106
All internal counts: zz :         271 yy:         620 xx:          66 ww:         132
One-external counts: yz :        1072 yx:        1000 yw:        1210
Two-external counts: yy :         865 ww:         252 xx:         180 xz:          72 wz:         104 wx:         360
Three-ext.   counts: yx :         320 yw:         395

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1    -2.77549162
   ht   2    -0.05719782    -0.18659037
   ht   3     0.00685396    -0.00269846    -0.00876154
   ht   4    -0.00041319    -0.00333157    -0.00048530    -0.00143128
   ht   5    -0.00425509     0.00073909     0.00041147    -0.00001782    -0.00027374

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1    1.00000      -3.196956E-14  -2.470445E-03   1.702334E-04   1.527742E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 civs   1  -0.964541       9.901519E-02   0.279099       4.637335E-03  -8.625497E-03
 civs   2  -0.872304      -0.786732       -2.90129      -0.900774      -0.953593    
 civs   3   -1.19208       -9.42541      -0.367015        12.6131       -3.54033    
 civs   4   -1.01404       -21.6067        6.75225       -10.2575        30.0972    
 civs   5   0.810787        32.2823       -49.9640        56.5290        37.1821    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1  -0.960530       0.167941       0.204823       5.809294E-02   6.204889E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1    -0.96053031     0.16794101     0.20482308     0.05809294     0.06204889

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1    -46.3259516593  5.8702E-05  6.9288E-06  2.4110E-03  1.0000E-03
 mr-sdci #  4  2    -46.0164606872  7.6178E-02  0.0000E+00  1.8415E-01  1.0000E-04
 mr-sdci #  4  3    -45.4582968771  1.8290E-02  0.0000E+00  5.6665E-01  1.0000E-04
 mr-sdci #  4  4    -45.4127001267  2.8343E-02  0.0000E+00  5.7151E-01  1.0000E-04
 mr-sdci #  4  5    -45.3750256285  1.8764E+00  0.0000E+00  6.2345E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   5

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         602 2x:         246 4x:         106
All internal counts: zz :         271 yy:         620 xx:          66 ww:         132
One-external counts: yz :        1072 yx:        1000 yw:        1210
Two-external counts: yy :         865 ww:         252 xx:         180 xz:          72 wz:         104 wx:         360
Three-ext.   counts: yx :         320 yw:         395

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1    -2.77549162
   ht   2    -0.05719782    -0.18659037
   ht   3     0.00685396    -0.00269846    -0.00876154
   ht   4    -0.00041319    -0.00333157    -0.00048530    -0.00143128
   ht   5    -0.00425509     0.00073909     0.00041147    -0.00001782    -0.00027374
   ht   6     0.00098546    -0.00013287     0.00003320     0.00001943     0.00000950    -0.00002797

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1    1.00000      -3.196956E-14  -2.470445E-03   1.702334E-04   1.527742E-03  -3.540649E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1  -0.964281       0.106182      -4.494612E-02   0.260062      -0.110537       4.687027E-02
 civs   2  -0.872345      -0.674264        1.11983       -2.68424       0.114678       -1.36181    
 civs   3   -1.19462       -8.69682        4.41496        6.30433        10.6335       -3.80198    
 civs   4   -1.03083       -21.0274        2.20187       -1.88473       -8.64224        31.6811    
 civs   5   0.900286        36.5105        22.0395       -21.9698        72.6844        26.5506    
 civs   6   0.935374        68.2912        256.474        83.7012       -49.7411        40.0187    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.960461       0.155687      -0.112616       0.180967      -9.623244E-03   8.804928E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.96046069     0.15568691    -0.11261608     0.18096736    -0.00962324     0.08804928

 trial vector basis is being transformed.  new dimension:   2

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1    -46.3259581403  6.4810E-06  7.3399E-07  7.9951E-04  1.0000E-03
 mr-sdci #  5  2    -46.0364917795  2.0031E-02  0.0000E+00  1.5000E-01  1.0000E-04
 mr-sdci #  5  3    -45.7097321689  2.5144E-01  0.0000E+00  4.9354E-01  1.0000E-04
 mr-sdci #  5  4    -45.4233172260  1.0617E-02  0.0000E+00  5.6145E-01  1.0000E-04
 mr-sdci #  5  5    -45.4097900204  3.4764E-02  0.0000E+00  6.2420E-01  1.0000E-04
 mr-sdci #  5  6    -45.3712109466  1.8726E+00  0.0000E+00  5.9615E-01  1.0000E-04
 
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.001000
time for vector access                 0.000000

 mr-sdci  convergence criteria satisfied after  5 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1    -46.3259581403  6.4810E-06  7.3399E-07  7.9951E-04  1.0000E-03
 mr-sdci #  5  2    -46.0364917795  2.0031E-02  0.0000E+00  1.5000E-01  1.0000E-04

####################CIUDGINFO####################

   ci vector at position   1 energy=  -46.325958140317

################END OF CIUDGINFO################

 
diagon:itrnv=   0
    1 of the   3 expansion vectors are transformed.
    1 of the   2 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
maximum overlap with reference    1(overlap= 0.96046)

 information on vector: 1 from unit 11 written to unit 48 filename civout              


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -46.3259581403

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     3    4    5   17   22   31

                                         symmetry   a1   a1   a1   a2   b1   b2 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1 -0.208633                        +-   +               -      
 z*  1  1       2 -0.661795                        +-        +          -      
 z*  1  1       3  0.601360                        +-             +          - 
 z*  1  1       4 -0.054641                        +    +-              -      
 z*  1  1       5  0.042929                        +     -   +          -      
 z*  1  1       6 -0.038951                        +     -        +          - 
 z*  1  1       7  0.074215                        +    +     -         -      
 z*  1  1       8  0.123555                        +    +          -         - 
 z*  1  1       9  0.118353                        +         +-         -      
 z*  1  1      10 -0.123419                        +          -   +          - 
 z*  1  1      11 -0.038952                        +         +     -         - 
 z*  1  1      12  0.084678                        +              +-    -      
 z*  1  1      13 -0.084669                        +                    -   +- 
 z*  1  1      14  0.043450                             +-   +          -      
 z*  1  1      15 -0.047738                             +-        +          - 
 z*  1  1      16 -0.013711                             +    +-         -      
 z*  1  1      17  0.020980                             +     -   +          - 
 z*  1  1      18  0.018313                             +    +     -         - 
 z*  1  1      20  0.019294                             +               -   +- 
 z*  1  1      22 -0.025940                                  +    +-    -      
 z*  1  1      23  0.018174                                  +          -   +- 
 z*  1  1      24  0.026529                                       +    +-    - 
 y   1  1      33  0.018397              2( b1 )   +-         -                
 y   1  1      35 -0.014497              4( b1 )   +-         -                
 y   1  1      40 -0.016721              2( b2 )   +-              -           
 y   1  1      42  0.013172              4( b2 )   +-              -           
 y   1  1      47  0.011111              2( a1 )   +-                   -      
 y   1  1      48  0.015728              3( a1 )   +-                   -      
 y   1  1      50 -0.058580              5( a1 )   +-                   -      
 y   1  1      52 -0.013335              7( a1 )   +-                   -      
 y   1  1      57 -0.016724              1( a2 )   +-                        - 
 y   1  1      58  0.013174              2( a2 )   +-                        - 
 y   1  1      92  0.013642              1( a2 )    -   +                    - 
 y   1  1      93 -0.013715              2( a2 )    -   +                    - 
 y   1  1      96 -0.011011              2( b1 )    -        +-                
 y   1  1      98  0.013950              4( b1 )    -        +-                
 y   1  1     103  0.010288              2( b2 )    -        +     -           
 y   1  1     105 -0.010346              4( b2 )    -        +     -           
 y   1  1     113  0.021887              5( a1 )    -        +          -      
 y   1  1     126  0.010301              4( b1 )    -             +-           
 y   1  1     135 -0.011383              3( a1 )    -             +          - 
 y   1  1     137 -0.019900              5( a1 )    -             +          - 
 y   1  1     139  0.012326              7( a1 )    -             +          - 
 y   1  1     161  0.010299              4( b1 )    -                       +- 
 y   1  1     206  0.015489              7( a1 )   +          -         -      
 y   1  1     212 -0.012140              2( a2 )   +          -              - 
 y   1  1     215  0.010950              2( a2 )   +               -    -      
 y   1  1     222  0.010932              6( a1 )   +               -         - 
 y   1  1     231 -0.010947              4( b2 )   +                    -    - 
 y   1  1     282 -0.012682              6( a1 )         -   +          -      
 y   1  1     302  0.010539              2( a1 )         -        +          - 
 y   1  1     306  0.013771              6( a1 )         -        +          - 
 y   1  1     439 -0.010009              2( a2 )             +     -    -      
 x   1  1     621 -0.012435    9( a1 )   1( b1 )    -         -                
 x   1  1     628  0.016012    5( a1 )   2( b1 )    -         -                
 x   1  1     643 -0.012873    9( a1 )   3( b1 )    -         -                
 x   1  1     650 -0.016214    5( a1 )   4( b1 )    -         -                
 x   1  1     668 -0.012960    1( a1 )   6( b1 )    -         -                
 x   1  1     671 -0.013667    4( a1 )   6( b1 )    -         -                
 x   1  1     741 -0.010843   10( a1 )   1( b2 )    -              -           
 x   1  1     747 -0.014554    5( a1 )   2( b2 )    -              -           
 x   1  1     763 -0.010941   10( a1 )   3( b2 )    -              -           
 x   1  1     769  0.014739    5( a1 )   4( b2 )    -              -           
 x   1  1     787  0.012918    1( a1 )   6( b2 )    -              -           
 x   1  1     790  0.013296    4( a1 )   6( b2 )    -              -           
 x   1  1     817 -0.013706    3( a1 )   5( a1 )    -                   -      
 x   1  1     828 -0.014940    5( a1 )   7( a1 )    -                   -      
 x   1  1     845 -0.010334    1( a1 )  10( a1 )    -                   -      
 x   1  1     848 -0.010867    4( a1 )  10( a1 )    -                   -      
 x   1  1     882  0.011809    1( b1 )   7( b1 )    -                   -      
 x   1  1     884  0.012176    3( b1 )   7( b1 )    -                   -      
 x   1  1     903  0.012555    1( b2 )   7( b2 )    -                   -      
 x   1  1     905  0.012926    3( b2 )   7( b2 )    -                   -      
 x   1  1     913 -0.014555    5( a1 )   1( a2 )    -                        - 
 x   1  1     924  0.014739    5( a1 )   2( a2 )    -                        - 
 x   1  1     948  0.011863    7( b1 )   1( b2 )    -                        - 
 x   1  1     962  0.012192    7( b1 )   3( b2 )    -                        - 
 x   1  1     984  0.011192    1( b1 )   7( b2 )    -                        - 
 x   1  1     986  0.011521    3( b1 )   7( b2 )    -                        - 
 x   1  1     991 -0.010968    1( a1 )   1( b1 )         -    -                
 x   1  1    1320 -0.010680    1( b1 )   1( b2 )         -                   - 
 w   1  1    1929 -0.027661    1( a1 )   1( b1 )   +-                          
 w   1  1    1932 -0.025649    4( a1 )   1( b1 )   +-                          
 w   1  1    1936 -0.010365    8( a1 )   1( b1 )   +-                          
 w   1  1    1946 -0.012644    7( a1 )   2( b1 )   +-                          
 w   1  1    1951 -0.025648    1( a1 )   3( b1 )   +-                          
 w   1  1    1954 -0.025037    4( a1 )   3( b1 )   +-                          
 w   1  1    1958 -0.011811    8( a1 )   3( b1 )   +-                          
 w   1  1    1964 -0.011748    3( a1 )   4( b1 )   +-                          
 w   1  1    1967 -0.011726    6( a1 )   4( b1 )   +-                          
 w   1  1    1968  0.021591    7( a1 )   4( b1 )   +-                          
 w   1  1    1973 -0.010365    1( a1 )   5( b1 )   +-                          
 w   1  1    1976 -0.011811    4( a1 )   5( b1 )   +-                          
 w   1  1    1993  0.016488   10( a1 )   6( b1 )   +-                          
 w   1  1    2004  0.020675   10( a1 )   7( b1 )   +-                          
 w   1  1    2010  0.012475    2( a2 )   2( b2 )   +-                          
 w   1  1    2015  0.012476    1( a2 )   4( b2 )   +-                          
 w   1  1    2016 -0.021304    2( a2 )   4( b2 )   +-                          
 w   1  1    2023 -0.013905    3( a2 )   6( b2 )   +-                          
 w   1  1    2026 -0.021990    3( a2 )   7( b2 )   +-                          
 w   1  1    2027 -0.010461    1( a1 )   1( b1 )   +     -                     
 w   1  1    2125 -0.033215    1( a1 )   1( b1 )   +          -                
 w   1  1    2128 -0.024668    4( a1 )   1( b1 )   +          -                
 w   1  1    2147 -0.025031    1( a1 )   3( b1 )   +          -                
 w   1  1    2150 -0.020092    4( a1 )   3( b1 )   +          -                
 w   1  1    2183  0.010679    4( a1 )   6( b1 )   +          -                
 w   1  1    2244  0.030177    1( a1 )   1( b2 )   +               -           
 w   1  1    2247  0.022877    4( a1 )   1( b2 )   +               -           
 w   1  1    2266  0.022276    1( a1 )   3( b2 )   +               -           
 w   1  1    2269  0.018255    4( a1 )   3( b2 )   +               -           
 w   1  1    2321  0.022789    1( a1 )   1( a1 )   +                    -      
 w   1  1    2327  0.020653    1( a1 )   4( a1 )   +                    -      
 w   1  1    2330  0.010573    4( a1 )   4( a1 )   +                    -      
 w   1  1    2393  0.022786    1( b1 )   1( b1 )   +                    -      
 w   1  1    2396  0.020645    1( b1 )   3( b1 )   +                    -      
 w   1  1    2398  0.010568    3( b1 )   3( b1 )   +                    -      
 w   1  1    2421 -0.019737    1( b2 )   1( b2 )   +                    -      
 w   1  1    2424 -0.024376    1( b2 )   3( b2 )   +                    -      
 w   1  1    2426 -0.015174    3( b2 )   3( b2 )   +                    -      
 w   1  1    2482  0.030188    1( b1 )   1( b2 )   +                         - 
 w   1  1    2484  0.022886    3( b1 )   1( b2 )   +                         - 
 w   1  1    2496  0.022284    1( b1 )   3( b2 )   +                         - 
 w   1  1    2498  0.018260    3( b1 )   3( b2 )   +                         - 
 w   1  1    2825  0.020542    1( a1 )   1( a1 )        +               -      
 w   1  1    2831  0.018157    1( a1 )   4( a1 )        +               -      
 w   1  1    2897  0.014188    1( b1 )   1( b1 )        +               -      
 w   1  1    2900  0.011991    1( b1 )   3( b1 )        +               -      
 w   1  1    2925  0.013352    1( b2 )   1( b2 )        +               -      
 w   1  1    2928  0.011502    1( b2 )   3( b2 )        +               -      
 w   1  1    2986 -0.013238    1( b1 )   1( b2 )        +                    - 
 w   1  1    3035  0.015304    1( a1 )   1( b1 )             +-                
 w   1  1    3038  0.010680    4( a1 )   1( b1 )             +-                
 w   1  1    3057  0.010354    1( a1 )   3( b1 )             +-                
 w   1  1    3231  0.054080    1( a1 )   1( a1 )             +          -      
 w   1  1    3237  0.046861    1( a1 )   4( a1 )             +          -      
 w   1  1    3240  0.023964    4( a1 )   4( a1 )             +          -      
 w   1  1    3245  0.016203    5( a1 )   5( a1 )             +          -      
 w   1  1    3303  0.056089    1( b1 )   1( b1 )             +          -      
 w   1  1    3306  0.048808    1( b1 )   3( b1 )             +          -      
 w   1  1    3308  0.024976    3( b1 )   3( b1 )             +          -      
 w   1  1    3331  0.042345    1( b2 )   1( b2 )             +          -      
 w   1  1    3334  0.036490    1( b2 )   3( b2 )             +          -      
 w   1  1    3336  0.018846    3( b2 )   3( b2 )             +          -      
 w   1  1    3441  0.011786    1( a1 )   1( b1 )                  +-           
 w   1  1    3621 -0.051712    1( a1 )   1( a1 )                  +          - 
 w   1  1    3627 -0.045072    1( a1 )   4( a1 )                  +          - 
 w   1  1    3630 -0.023073    4( a1 )   4( a1 )                  +          - 
 w   1  1    3635 -0.014728    5( a1 )   5( a1 )                  +          - 
 w   1  1    3693 -0.051718    1( b1 )   1( b1 )                  +          - 
 w   1  1    3696 -0.045076    1( b1 )   3( b1 )                  +          - 
 w   1  1    3698 -0.023074    3( b1 )   3( b1 )                  +          - 
 w   1  1    3721 -0.035191    1( b2 )   1( b2 )                  +          - 
 w   1  1    3724 -0.029969    1( b2 )   3( b2 )                  +          - 
 w   1  1    3726 -0.015471    3( b2 )   3( b2 )                  +          - 
 w   1  1    3945  0.011779    1( a1 )   1( b1 )                            +- 

 ci coefficient statistics:
           rq > 0.1                6
      0.1> rq > 0.01             149
     0.01> rq > 0.001            879
    0.001> rq > 0.0001           747
   0.0001> rq > 0.00001          189
  0.00001> rq > 0.000001          26
 0.000001> rq                   2046
           all                  4042
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         176 2x:           0 4x:           0
All internal counts: zz :         271 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


  iref  icsf         v(icsf)             hv(icsf)
     1     1     -0.208632965227      9.654379292938
     2     2     -0.661794570352     30.624179275250
     3     3      0.601360434585    -27.827616853795
     4     4     -0.054641474868      2.529731869837
     5     5      0.042928786747     -1.986573911487
     6     6     -0.038951117438      1.802508779678
     7     7      0.074214857209     -3.434391217205
     8     8      0.123555494176     -5.717682626496
     9     9      0.118352777251     -5.475809398662
    10    10     -0.123419214430      5.711382821022
    11    11     -0.038952294840      1.802566336708
    12    12      0.084677927139     -3.917466330699
    13    13     -0.084669271917      3.917064459988
    14    14      0.043449799389     -2.009125742990
    15    15     -0.047738129773      2.207476883713
    16    16     -0.013710630337      0.633978675024
    17    17      0.020979759412     -0.970261861233
    18    18      0.018312906425     -0.846919165705
    19    19      0.005396164172     -0.249648739832
    20    20      0.019294471745     -0.892221265872
    21    21     -0.005370937592      0.248102334341
    22    22     -0.025939849896      1.199350217060
    23    23      0.018174311733     -0.840207436569
    24    24      0.026528500953     -1.226594137067

 number of reference csfs (nref) is    24.  root number (iroot) is  1.
 c0**2 =   0.92261740  c**2 (all zwalks) =   0.92261740

 pople ci energy extrapolation is computed with  4 correlated electrons.

 eref      =    -46.274037333461   "relaxed" cnot**2         =   0.922617399951
 eci       =    -46.325958140317   deltae = eci - eref       =  -0.051920806857
 eci+dv1   =    -46.329975907348   dv1 = (1-cnot**2)*deltae  =  -0.004017767031
 eci+dv2   =    -46.330312889134   dv2 = dv1 / cnot**2       =  -0.004354748817
 eci+dv3   =    -46.330711573365   dv3 = dv1 / (2*cnot**2-1) =  -0.004753433048
 eci+pople =    -46.328131698861   (  4e- scaled deltae )    =  -0.054094365401
 passed aftci ... 
 readint2: molcas,dalton2=                     0                     0
 files%faoints=aoints              
lodens (list->root)=  1
sifcfg setup: record length 4096 DP
# d1 elements per record  3272
# d2 elements per record  2730
  The MR-CISD density will be calculated.
 item #                     1 suffix=:.drt1.state1:
 read_civout: repnuc=  -43.4985876562748     
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method:  0 last record  1max overlap with ref# 96% root-following 0
 MR-CISD energy:   -46.32595814    -2.82737048
 residuum:     0.00079951
 deltae:     0.00000648
 apxde:     0.00000073

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.96046069     0.15568691    -0.11261608     0.18096736    -0.00962324     0.08804928     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.96046069     0.15568691     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=     106  DYX=     130  DYW=     160
   D0Z=      34  D0Y=     103  D0X=      12  D0W=      18
  DDZI=      80 DDYI=     180 DDXI=      30 DDWI=      36
  DDZE=       0 DDYE=      70 DDXE=      15 DDWE=      21
================================================================================
Trace of MO density:     4.000000
    4  correlated and     8  frozen core electrons

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     1.81032701    -0.06219855    -0.19686816     0.00000000    -0.01242082    -0.01759181
   MO   4     0.00000000     0.00000000    -0.06219855     0.09075768     0.15103617     0.00000000    -0.00009424     0.00678285
   MO   5     0.00000000     0.00000000    -0.19686816     0.15103617     0.52219972     0.00000000     0.01106353     0.01347789
   MO   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.02608866     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000    -0.01242082    -0.00009424     0.01106353     0.00000000     0.00100876     0.00049135
   MO   8     0.00000000     0.00000000    -0.01759181     0.00678285     0.01347789     0.00000000     0.00049135     0.00135720
   MO   9     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.01831015     0.00000000     0.00000000
   MO  10     0.00000000     0.00000000    -0.03929101    -0.01040600    -0.03301519     0.00000000    -0.00074183    -0.00105058
   MO  11     0.00000000     0.00000000    -0.00841251    -0.00220917     0.00858445     0.00000000     0.00106658     0.00030823
   MO  12     0.00000000     0.00000000     0.01549270    -0.00659253    -0.01243849     0.00000000    -0.00061388    -0.00152107
   MO  13     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00397971     0.00000000     0.00000000
   MO  14     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00100734     0.00000000     0.00000000
   MO  15     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00204129     0.00000000     0.00000000
   MO  16     0.00000000     0.00000000     0.00036569     0.00047171     0.00149632     0.00000000     0.00004006     0.00005673

                MO   9         MO  10         MO  11         MO  12         MO  13         MO  14         MO  15         MO  16
   MO   3     0.00000000    -0.03929101    -0.00841251     0.01549270     0.00000000     0.00000000     0.00000000     0.00036569
   MO   4     0.00000000    -0.01040600    -0.00220917    -0.00659253     0.00000000     0.00000000     0.00000000     0.00047171
   MO   5     0.00000000    -0.03301519     0.00858445    -0.01243849     0.00000000     0.00000000     0.00000000     0.00149632
   MO   6     0.01831015     0.00000000     0.00000000     0.00000000     0.00397971     0.00100734     0.00204129     0.00000000
   MO   7     0.00000000    -0.00074183     0.00106658    -0.00061388     0.00000000     0.00000000     0.00000000     0.00004006
   MO   8     0.00000000    -0.00105058     0.00030823    -0.00152107     0.00000000     0.00000000     0.00000000     0.00005673
   MO   9     0.01327631     0.00000000     0.00000000     0.00000000     0.00321548     0.00094749     0.00152733     0.00000000
   MO  10     0.00000000     0.00849600    -0.00052568     0.00096820     0.00000000     0.00000000     0.00000000    -0.00037989
   MO  11     0.00000000    -0.00052568     0.00125136    -0.00050015     0.00000000     0.00000000     0.00000000     0.00003138
   MO  12     0.00000000     0.00096820    -0.00050015     0.00189993     0.00000000     0.00000000     0.00000000    -0.00005779
   MO  13     0.00321548     0.00000000     0.00000000     0.00000000     0.00106314     0.00038833     0.00049613     0.00000000
   MO  14     0.00094749     0.00000000     0.00000000     0.00000000     0.00038833     0.00098154     0.00009823     0.00000000
   MO  15     0.00152733     0.00000000     0.00000000     0.00000000     0.00049613     0.00009823     0.00210719     0.00000000
   MO  16     0.00000000    -0.00037989     0.00003138    -0.00005779     0.00000000     0.00000000     0.00000000     0.00002207

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     2.00000000     1.84492002     0.54014737     0.04421906     0.03998367     0.00506933     0.00233795
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     0.00195236     0.00109535     0.00054717     0.00047157     0.00006124     0.00001458     0.00001389     0.00000302

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4
   MO   1     0.43873555     0.01476607    -0.01317938     0.00000000
   MO   2     0.01476607     0.00144829    -0.00156354     0.00000000
   MO   3    -0.01317938    -0.00156354     0.00187699     0.00000000
   MO   4     0.00000000     0.00000000     0.00000000     0.00207255

Natural orbital populations,block 2
              MO     1       MO     2       MO     3       MO     4       MO
  occ(*)=     0.43963311     0.00236239     0.00207255     0.00006533

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     0.55756259     0.00000000     0.01758270     0.00000000    -0.01535165     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.02609317     0.00000000     0.01831282     0.00000000     0.00397965    -0.00195166
   MO   4     0.00000000     0.01758270     0.00000000     0.00178499     0.00000000    -0.00189182     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.01831282     0.00000000     0.01327784     0.00000000     0.00321539    -0.00162333
   MO   6     0.00000000    -0.01535165     0.00000000    -0.00189182     0.00000000     0.00230157     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000     0.00397965     0.00000000     0.00321539     0.00000000     0.00106307    -0.00059519
   MO   8     0.00000000     0.00000000    -0.00195166     0.00000000    -0.00162333     0.00000000    -0.00059519     0.00140058
   MO   9     0.00000000     0.00000000    -0.00117103     0.00000000    -0.00077128     0.00000000    -0.00020686     0.00055310

                MO   9
   MO   2     0.00000000
   MO   3    -0.00117103
   MO   4     0.00000000
   MO   5    -0.00077128
   MO   6     0.00000000
   MO   7    -0.00020686
   MO   8     0.00055310
   MO   9     0.00168795

Natural orbital populations,block 3
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     0.55854486     0.03998949     0.00299416     0.00195248     0.00109539     0.00047134     0.00011015
              MO     9       MO
  occ(*)=     0.00001389

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     0.43873547     0.00000000     0.01476171     0.00000000    -0.01317695     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.01568112     0.00000000     0.01124043     0.00000000    -0.00239452    -0.00072226
   MO   4     0.00000000     0.01476171     0.00000000     0.00144749     0.00000000    -0.00156302     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.01124043     0.00000000     0.00845292     0.00000000    -0.00206649    -0.00046372
   MO   6     0.00000000    -0.01317695     0.00000000    -0.00156302     0.00000000     0.00187670     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000    -0.00239452     0.00000000    -0.00206649     0.00000000     0.00070307     0.00011896
   MO   8     0.00000000     0.00000000    -0.00072226     0.00000000    -0.00046372     0.00000000     0.00011896     0.00123062
   MO   9     0.00000000     0.00000000    -0.00114268     0.00000000    -0.00073362     0.00000000     0.00018819     0.00052707

                MO   9
   MO   2     0.00000000
   MO   3    -0.00114268
   MO   4     0.00000000
   MO   5    -0.00073362
   MO   6     0.00000000
   MO   7     0.00018819
   MO   8     0.00052707
   MO   9     0.00173088

Natural orbital populations,block 4
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     0.43963259     0.02441268     0.00236181     0.00195248     0.00089735     0.00052571     0.00006526
              MO     9       MO
  occ(*)=     0.00001040


 total number of electrons =   12.0000000000

 test slabel:                     4
  a1  a2  b1  b2 


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        a1  partial gross atomic populations
   ao class       1a1        2a1        3a1        4a1        5a1        6a1 
     3_ s       0.000000   2.000000   1.797382   0.016659   0.000000   0.000000
     3_ p       2.000000   0.000000   0.000000   0.000000   0.000000   0.039745
     3_ d       0.000000   0.000000   0.047538   0.523488   0.044219   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000000   0.000000   0.000239
 
   ao class       7a1        8a1        9a1       10a1       11a1       12a1 
     3_ s       0.004599   0.000201   0.000000   0.000000   0.000000   0.000000
     3_ p       0.000000   0.000000   0.000044   0.000283   0.000000   0.000342
     3_ d       0.000470   0.002137   0.000000   0.000000   0.000547   0.000000
     3_ f       0.000000   0.000000   0.001908   0.000812   0.000000   0.000129
 
   ao class      13a1       14a1       15a1       16a1 
     3_ s       0.000001   0.000000   0.000000   0.000003
     3_ p       0.000000   0.000000   0.000014   0.000000
     3_ d       0.000060   0.000015   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000000

                        a2  partial gross atomic populations
   ao class       1a2        2a2        3a2        4a2 
     3_ d       0.439633   0.002362   0.000000   0.000065
     3_ f       0.000000   0.000000   0.002073   0.000000

                        b1  partial gross atomic populations
   ao class       1b1        2b1        3b1        4b1        5b1        6b1 
     3_ p       2.000000   0.000000   0.039751   0.000000   0.000044   0.000283
     3_ d       0.000000   0.558545   0.000000   0.002994   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000239   0.000000   0.001908   0.000812
 
   ao class       7b1        8b1        9b1 
     3_ p       0.000342   0.000000   0.000014
     3_ d       0.000000   0.000110   0.000000
     3_ f       0.000129   0.000000   0.000000

                        b2  partial gross atomic populations
   ao class       1b2        2b2        3b2        4b2        5b2        6b2 
     3_ p       2.000000   0.000000   0.024285   0.000000   0.000017   0.000000
     3_ d       0.000000   0.439633   0.000000   0.002362   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000127   0.000000   0.001936   0.000897
 
   ao class       7b2        8b2        9b2 
     3_ p       0.000525   0.000000   0.000010
     3_ d       0.000000   0.000065   0.000000
     3_ f       0.000001   0.000000   0.000000


                        gross atomic populations
     ao            3_
      s         3.818845
      p         6.105699
      d         2.064244
      f         0.011211
    total      12.000000
 

 Total number of electrons:   12.00000000

 DA ...
