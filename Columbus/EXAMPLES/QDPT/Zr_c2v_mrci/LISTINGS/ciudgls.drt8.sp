1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      2009-03.    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


================ Computing sorting integral file structure ================

                    -----z----- -----y----- -----x----- -----w----- ---total---

                CSFs        28         630        3416        1526        5600
      internal walks       105          90          36          15         246
valid internal walks        28          90          36          15         169
 getinfoarray: info=                     6 :                     1
                  8192                  6552                  8192
                  5460                     0
 icd(3)=                  2976 ci%nnlev=                   595  l2rec=
                  8192  n2max=                  5460
 lcore1,lcore2=              13068137              13044821
 lencor,maxblo              13107200                 60000
========================================
 current settings:
 minbl3         381
 minbl4         381
 locmaxbl3     4288
 locmaxbuf     2144
 maxbl3       60000
 maxbl3       60000
 maxbl4       60000
 maxbuf       30006
========================================

 sorted 4-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)

 sorted 3-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)
 Orig.  diagonal integrals:  1electron:        34
                             0ext.    :        42
                             2ext.    :       336
                             4ext.    :       812


 Orig. off-diag. integrals:  4ext.    :     24411
                             3ext.    :     18720
                             2ext.    :      6792
                             1ext.    :      1272
                             0ext.    :        99
                             2ext. SO :         0
                             1ext. SO :         0
                             0ext. SO :         0
                             1electron:       128


 Sorted integrals            3ext.  w :     17370 x :     16020
                             4ext.  w :     20955 x :     17777


Cycle #  1 sortfile size=    163835(       5 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13068137
Cycle #  2 sortfile size=    163835(       5 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13068137
 minimum size of srtscr:     98301 WP (     3 records)
 maximum size of srtscr:    163835 WP (     5 records)
diagi   file:      4 records  of   1536 WP each=>       6144 WP total
ofdgi   file:      8 records  of   1536 WP each=>      12288 WP total
fil3w   file:      1 records  of  30006 WP each=>      30006 WP total
fil3x   file:      1 records  of  30006 WP each=>      30006 WP total
fil4w   file:      1 records  of  30006 WP each=>      30006 WP total
fil4x   file:      1 records  of  30006 WP each=>      30006 WP total
 compressed index vector length=                    63
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 0,
  GSET = 0,
   DAVCOR =10,
  NCOREL = 4
   nroot=1
  IVMODE = 3
  NBKITR = 1
   nvbkmn=2
  RTOLBK = 1e-3
   niter=30
   nvcimn=2
   rtolci=1e-3
  NVCIMX = 6
  NVRFMX = 6
  NVBKMX = 6
  IDEN  = 1
  CSFPRN = 10,
  update_mode=1
  nvrfmn=2
 /&end
 ------------------------------------------------------------------------
lodens (list->root)=  1
invlodens (root->list)=  1
 USING SEGMENTS OF EQUAL SIZE

****************  list of control variables  ****************
 lvlprt =    0      nroot  =    1      noldv  =   0      noldhv =   0
 nunitv =    1      nbkitr =    1      niter  =  30      davcor =  10
 csfprn =   10      ivmode =    3      istrt  =   0      vout   =   0
 iortls =    0      nvbkmx =    6      ibktv  =  -1      ibkthv =  -1
 nvcimx =    6      icitv  =   -1      icithv =  -1      frcsub =   0
 nvbkmn =    2      nvcimn =    2      maxseg =   4      nrfitr =  30
 ncorel =    4      nvrfmx =    6      nvrfmn =   2      iden   =   1
 itran  =    0      froot  =    0      rtmode =   0      ncouple=   1
 skipso =    F      dalton2=    0      molcas =   0      finalv =   0
 finalw =    0      cosmocalc=   0    with_tsklst=   0
 nsegwx =    1     1     1     1
 nseg0x =    1     1     1     1
 nseg1x =    1     1     1     1
 nseg2x =    1     1     1     1
 nseg3x =    1     1     1     1
 nseg4x =    1     1     1     1
 no0ex  =      0    no1ex  =      0    no2ex  =     0    no3ex  =     0
 no4ex  =      0    nodiag =      0
 cdg4ex =    1      c3ex1ex=    1      c2ex0ex=   1
 fileloc=    0     0     0     0     0     0     0     1     1     1
 directhd=   1      noaqccshift_zyxw=      0
 critical_crit=-1.00000    critical_delta= 0.05000

 ctol   = 0.010000    lrtshift=1.000000    smalld =0.001000


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-03    1.000E-03
 Computing density:                    .drt1.state1
 using                      1  nodes and                      1  cores.
 szdg/szodg per processor=                  6144                 12288
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core           13107199 DP per process

********** Integral sort section *************


 workspace allocation information: lencor=  13107199

 echo of the input for program cisrt:
 ------------------------------------------------------------------------
  &input
  maxbl3=60000
  maxbl4=60000
  &end
 ------------------------------------------------------------------------
 
 ( 6) listing file:                    ciudgls             
 ( 5) input file:                      cisrtin   
 (17) cidrt file:                      cidrtfl             
 (11) transformed integrals file:      moints    
 (12) diagonal integral file:          diagint             
 (13) off-diagonal integral file:      ofdgint             
 (31) 4-external w integrals file:     fil4w               
 (32) 4-external x integrals file:     fil4x               
 (33) 3-external w integrals file:     fil3w               
 (34) 3-external x integrals file:     fil3x               
 (21) scratch da sorting file:         srtscr              
 (12) 2-e integral file [fsplit=2]:    moints2   

 input integral file header information:
                                                                                 
 aoints SIFS file created by argos.      zam792            17:53:52.086 17-Dec-13
  cidrt_title                                                                    
 MO-coefficients from mcscf.x                                                    
  with dummy occupation 1.0 for active orbitals                                  
  total ao core energy =    0.000000000                                          
 MCSCF energy =     -46.301878576                                                
 SIFS file created by program tran.      zam792            17:53:52.407 17-Dec-13

 input energy(*) values:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total core energy =   0.000000000000E+00

 nsym = 4 nmot=  34

 symmetry  =    1    2    3    4
 slabel(*) =   a1   a2   b1   b2
 nmpsy(*)  =   14    4    8    8

 info(*) =          1      8192      6552      8192      5460         0

 orbital labels, i:molab(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012  13:tout:013  14:tout:014  15:tout:015  16:tout:016  17:tout:017  18:tout:018  19:tout:019  20:tout:020
  21:tout:021  22:tout:022  23:tout:023  24:tout:024  25:tout:025  26:tout:026  27:tout:027  28:tout:028  29:tout:029  30:tout:030
  31:tout:031  32:tout:032  33:tout:033  34:tout:034

 input parameters:
 prnopt=  0
 ldamin=    4095 ldamax=   32767 ldainc=      64
 maxbuf=   30006 maxbl3=   60000 maxbl4=   60000 intmxo=    1536
  Using 32 bit compression 

 drt information:
  cidrt_title                                                                    
 nmotd =  38 nfctd =   4 nfvtc =   0 nmot  =  34
 nlevel =  34 niot  =   6 lowinl=  29
 orbital-to-level map(*)
   -1  -1  29  30  31   1   2   3   4   5   6   7   8   9  10  11  32  12  13  14
   -1  33  15  16  17  18  19  20  21  -1  34  22  23  24  25  26  27  28
 compressed map(*)
   29  30  31   1   2   3   4   5   6   7   8   9  10  11  32  12  13  14  33  15
   16  17  18  19  20  21  34  22  23  24  25  26  27  28
 levsym(*)
    1   1   1   1   1   1   1   1   1   1   1   2   2   2   3   3   3   3   3   3
    3   4   4   4   4   4   4   4   1   1   1   2   3   4
 repartitioning mu(*)=
   0.  0.  0.  0.  0.  0.

 new core energy added to the energy(*) list.
 from the integral file: h1_core= -4.349858765627E+01

 indxdg: diagonal integral statistics.
 total number of integrals contributing to diagonal matrix elements:      1190
 number with all external indices:       812
 number with half external - half internal indices:       336
 number with all internal indices:        42

 indxof: off-diagonal integral statistics.
    4-external integrals: num=      24411 strt=          1
    3-external integrals: num=      18720 strt=      24412
    2-external integrals: num=       6792 strt=      43132
    1-external integrals: num=       1272 strt=      49924
    0-external integrals: num=         99 strt=      51196

 total number of off-diagonal integrals:       51294


 indxof(2nd)  ittp=   3 numx(ittp)=        6792
 indxof(2nd)  ittp=   4 numx(ittp)=        1272
 indxof(2nd)  ittp=   5 numx(ittp)=          99

 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg=  12931980
 pro2e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652

 pro2e:     25126 integrals read in     5 records.

 pro2e:         0 integrals 34-ext integrals skipped.
 pro1e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652
 pro1e: eref =    0.000000000000000E+00
 total size of srtscr:                     4  records of                  32767 
 WP =               1048544 Bytes
 putdg        1     596    1191    1786    3322   36089   57934    3018   46706   90394
   123161  131353  136813  158652

 putf:       4 buffers of length    1536 written to file 12
 diagonal integral file completed.

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep4e:    17 blocks of linear combinations of 4-external integrals processed.
 number of sorted 4-external integrals      38732
 number of original 4-external integrals    24411


 putf34: external integral file complete. nfilw=    31 nfilx=    32 nrecw=     1 nrecx=     1 lbufp= 30006

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep3e:    19 blocks of linear combinations of 3-external integrals processed.
 number of sorted 3-external integrals      33390
 number of original 3-external integrals    18720


 putf34: external integral file complete. nfilw=    33 nfilx=    34 nrecw=     1 nrecx=     1 lbufp= 30006
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      6792         3     43132     43132      6792     51294
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      1272         4     49924     49924      1272     51294
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd        99         5     51196     51196        99     51294

 putf:       8 buffers of length    1536 written to file 13
 off-diagonal files sort completed.
 executing brd_struct for cisrtinfo
cisrtinfo:
bufszi  1536
 diagfile 4ext:     812 2ext:     336 0ext:      42
 fil4w,fil4x  :   24411 fil3w,fil3x :   18720
 ofdgint  2ext:    6792 1ext:    1272 0ext:      99so0ext:       0so1ext:       0so2ext:       0
buffer minbl4     381 minbl3     381 maxbl2     384nbas:  11   3   7   7   0   0   0   0 maxbuf 30006
 CIUDG version 5.9.7 ( 5-Oct-2004)

 workspace allocation information: lcore=  13107199

 core energy values from the integral file:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -4.349858765627E+01, ietype=    6,   fcore energy of type: H1(*)   

 total core repulsion energy = -4.349858765627E+01
 nmot  =    38 niot  =     6 nfct  =     4 nfvt  =     0
 nrow  =    36 nsym  =     4 ssym  =     4 lenbuf=  1600
 nwalk,xbar:        246      105       90       36       15
 nvalwt,nvalw:      169       28       90       36       15
 ncsft:            5600
 total number of valid internal walks:     169
 nvalz,nvaly,nvalx,nvalw =       28      90      36      15

 cisrt info file parameters:
 file number  12 blocksize   1536
 mxbld   1536
 nd4ext,nd2ext,nd0ext   812   336    42
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int    24411    18720     6792     1272       99        0        0        0
 minbl4,minbl3,maxbl2   381   381   384
 maxbuf 30006
 number of external orbitals per symmetry block:  11   3   7   7
 nmsym   4 number of internal orbitals   6
 executing brd_struct for drt
 executing brd_struct for orbinf
 executing brd_struct for momap
 calcthrxt: niot,maxw1=                     6                    66
 block size     0
 pthz,pthy,pthx,pthw:   105    90    36    15 total internal walks:     246
 maxlp3,n2lp,n1lp,n0lp    66     0     0     0
 orbsym(*)= 1 1 1 2 3 4

 setref:       28 references kept,
               77 references were marked as invalid, out of
              105 total.
 nmb.of records onel     1
 nmb.of records 2-ext     5
 nmb.of records 1-ext     1
 nmb.of records 0-ext     1
 nmb.of records 2-int     0
 nmb.of records 1-int     0
 nmb.of records 0-int     0
 ---------memory usage in DP -----------------
 < n-ex core usage >
     routines:
    fourex            61797
    threx             60409
    twoex              3549
    onex               1823
    allin              1536
    diagon             2109
               =======
   maximum            61797
 
  __ static summary __ 
   reflst                28
   hrfspc                28
               -------
   static->              28
 
  __ core required  __ 
   totstc                28
   max n-ex           61797
               -------
   totnec->           61825
 
  __ core available __ 
   totspc          13107199
   totnec -           61825
               -------
   totvec->        13045374

 number of external paths / symmetry
 vertex x     100      82      98      98
 vertex w     128      82      98      98
segment: free space=    13045374
 reducing frespc by                   946 to               13044428 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1          55|        28|         0|        28|         0|         1|
 -------------------------------------------------------------------------------
  Y 2          90|       630|        28|        90|        28|         2|
 -------------------------------------------------------------------------------
  X 3          36|      3416|       658|        36|       118|         3|
 -------------------------------------------------------------------------------
  W 4          15|      1526|      4074|        15|       154|         4|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=          76DP  conft+indsym=         360DP  drtbuffer=         510 DP

dimension of the ci-matrix ->>>      5600

 executing brd_struct for civct
 gentasklist: ntask=                    20
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS   
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      36      55       3416         28      36      28
     2  4   1    25      two-ext wz   2X  4 1      15      55       1526         28      15      28
     3  4   3    26      two-ext wx*  WX  4 3      15      36       1526       3416      15      36
     4  4   3    27      two-ext wx+  WX  4 3      15      36       1526       3416      15      36
     5  2   1    11      one-ext yz   1X  2 1      90      55        630         28      90      28
     6  3   2    15      1ex3ex yx    3X  3 2      36      90       3416        630      36      90
     7  4   2    16      1ex3ex yw    3X  4 2      15      90       1526        630      15      90
     8  1   1     1      allint zz    OX  1 1      55      55         28         28      28      28
     9  2   2     5      0ex2ex yy    OX  2 2      90      90        630        630      90      90
    10  3   3     6      0ex2ex xx*   OX  3 3      36      36       3416       3416      36      36
    11  3   3    18      0ex2ex xx+   OX  3 3      36      36       3416       3416      36      36
    12  4   4     7      0ex2ex ww*   OX  4 4      15      15       1526       1526      15      15
    13  4   4    19      0ex2ex ww+   OX  4 4      15      15       1526       1526      15      15
    14  2   2    42      four-ext y   4X  2 2      90      90        630        630      90      90
    15  3   3    43      four-ext x   4X  3 3      36      36       3416       3416      36      36
    16  4   4    44      four-ext w   4X  4 4      15      15       1526       1526      15      15
    17  1   1    75      dg-024ext z  OX  1 1      55      55         28         28      28      28
    18  2   2    76      dg-024ext y  OX  2 2      90      90        630        630      90      90
    19  3   3    77      dg-024ext x  OX  3 3      36      36       3416       3416      36      36
    20  4   4    78      dg-024ext w  OX  4 4      15      15       1526       1526      15      15
----------------------------------------------------------------------------------------------------
REDTASK #   1 TIME=  19.000 N=  1 (task/type/sgbra)=(   1/24/0) (
REDTASK #   2 TIME=  18.000 N=  1 (task/type/sgbra)=(   2/25/0) (
REDTASK #   3 TIME=  17.000 N=  1 (task/type/sgbra)=(   3/26/1) (
REDTASK #   4 TIME=  16.000 N=  1 (task/type/sgbra)=(   4/27/2) (
REDTASK #   5 TIME=  15.000 N=  1 (task/type/sgbra)=(   5/11/0) (
REDTASK #   6 TIME=  14.000 N=  1 (task/type/sgbra)=(   6/15/0) (
REDTASK #   7 TIME=  13.000 N=  1 (task/type/sgbra)=(   7/16/0) (
REDTASK #   8 TIME=  12.000 N=  1 (task/type/sgbra)=(   8/ 1/0) (
REDTASK #   9 TIME=  11.000 N=  1 (task/type/sgbra)=(   9/ 5/0) (
REDTASK #  10 TIME=  10.000 N=  1 (task/type/sgbra)=(  10/ 6/1) (
REDTASK #  11 TIME=   9.000 N=  1 (task/type/sgbra)=(  11/18/2) (
REDTASK #  12 TIME=   8.000 N=  1 (task/type/sgbra)=(  12/ 7/1) (
REDTASK #  13 TIME=   7.000 N=  1 (task/type/sgbra)=(  13/19/2) (
REDTASK #  14 TIME=   6.000 N=  1 (task/type/sgbra)=(  14/42/1) (
REDTASK #  15 TIME=   5.000 N=  1 (task/type/sgbra)=(  15/43/1) (
REDTASK #  16 TIME=   4.000 N=  1 (task/type/sgbra)=(  16/44/1) (
REDTASK #  17 TIME=   3.000 N=  1 (task/type/sgbra)=(  17/75/1) (
REDTASK #  18 TIME=   2.000 N=  1 (task/type/sgbra)=(  18/76/1) (
REDTASK #  19 TIME=   1.000 N=  1 (task/type/sgbra)=(  19/77/1) (
REDTASK #  20 TIME=   0.000 N=  1 (task/type/sgbra)=(  20/78/1) (
 initializing v-file: 1:                  5600

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         216 2x:           0 4x:           0
All internal counts: zz :         356 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 reference space has dimension      28
 dsyevx: computed roots 1 to    2(converged:   2)

    root           eigenvalues
    ----           ------------
       1         -46.3018785761
       2         -46.3018785741

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=                    28

         vector  1 from unit 11 written to unit 49 filename cirefv              

 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=    28)...
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              5600
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                    1
 residual norm convergence criteria:               0.001000

          starting bk iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         807 2x:         336 4x:         141
All internal counts: zz :         356 yy:           0 xx:           0 ww:           0
One-external counts: yz :        1491 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:         155 wz:         100 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1
   ht   1    -2.80329092

          calcsovref: tciref block   1

              civs   1
 refs   1    1.00000    

          calcsovref: scrb block   1

                ci   1
 civs   1    1.00000    

          calcsovref: sovref block   1

              v      1
 ref    1    1.00000    

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.3018785761  1.9096E-14  5.1977E-02  2.1139E-01  1.0000E-03
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.3018785761  1.9096E-14  5.1977E-02  2.1139E-01  1.0000E-03
 
diagon:itrnv=   2
 expansion vectors are not transformed.
 matrix-vector products are not transformed.

    2 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              5600
 number of initial trial vectors:                         2
 number of initial matrix-vector products:                1
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                   30
 residual norm convergence criteria:               0.001000

          starting ci iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         807 2x:         336 4x:         141
All internal counts: zz :         356 yy:         930 xx:         352 ww:          66
One-external counts: yz :        1491 yx:        2405 yw:        1220
Two-external counts: yy :        1285 ww:         180 xx:         682 xz:         155 wz:         100 wx:         510
Three-ext.   counts: yx :         665 yw:         370

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2
   ht   1    -2.80329092
   ht   2    -0.05197660    -0.18083887

          calcsovref: tciref block   1

              civs   1       civs   2
 refs   1    1.00000       3.779288E-14

          calcsovref: scrb block   1

                ci   1         ci   2
 civs   1   0.971164      -0.238414    
 civs   2   0.816650        3.32657    

          calcsovref: sovref block   1

              v      1       v      2
 ref    1   0.971164      -0.238414    

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97116353    -0.23841435

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.3455856398  4.3707E-02  1.8568E-03  4.0095E-02  1.0000E-03
 mr-sdci #  1  2    -45.5766555438  2.0781E+00  0.0000E+00  5.0086E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000999
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   2

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         807 2x:         336 4x:         141
All internal counts: zz :         356 yy:         930 xx:         352 ww:          66
One-external counts: yz :        1491 yx:        2405 yw:        1220
Two-external counts: yy :        1285 ww:         180 xx:         682 xz:         155 wz:         100 wx:         510
Three-ext.   counts: yx :         665 yw:         370

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3
   ht   1    -2.80329092
   ht   2    -0.05197660    -0.18083887
   ht   3     0.00105062    -0.00204754    -0.00640735

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3
 refs   1    1.00000       3.779288E-14  -3.758927E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3
 civs   1   0.967365      -0.168726       0.189172    
 civs   2   0.850866        1.65045       -2.87841    
 civs   3    1.09293        16.0687        9.43542    

          calcsovref: sovref block   1

              v      1       v      2       v      3
 ref    1   0.966954      -0.174766       0.185625    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.96695397    -0.17476647     0.18562517

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1    -46.3476186362  2.0330E-03  2.4189E-04  1.4151E-02  1.0000E-03
 mr-sdci #  2  2    -45.8107394551  2.3408E-01  0.0000E+00  4.5073E-01  1.0000E-04
 mr-sdci #  2  3    -45.4960575554  1.9975E+00  0.0000E+00  5.0894E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   3

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         807 2x:         336 4x:         141
All internal counts: zz :         356 yy:         930 xx:         352 ww:          66
One-external counts: yz :        1491 yx:        2405 yw:        1220
Two-external counts: yy :        1285 ww:         180 xx:         682 xz:         155 wz:         100 wx:         510
Three-ext.   counts: yx :         665 yw:         370

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4
   ht   1    -2.80329092
   ht   2    -0.05197660    -0.18083887
   ht   3     0.00105062    -0.00204754    -0.00640735
   ht   4    -0.00247791    -0.00224725    -0.00049191    -0.00085494

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4
 refs   1    1.00000       3.779288E-14  -3.758927E-04   8.963061E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4
 civs   1   0.964978       0.174625       0.199701      -2.472620E-02
 civs   2   0.854304      -0.996283       -3.18505      -0.265922    
 civs   3    1.21425       -13.1167        5.12437       -12.3090    
 civs   4   0.938780       -26.9835        12.4601        41.0016    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4
 ref    1   0.965363       0.155370       0.208943       1.665066E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.96536312     0.15536973     0.20894271     0.01665066

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1    -46.3478457706  2.2713E-04  4.1077E-05  6.0676E-03  1.0000E-03
 mr-sdci #  3  2    -45.9743499187  1.6361E-01  0.0000E+00  2.9402E-01  1.0000E-04
 mr-sdci #  3  3    -45.5075718841  1.1514E-02  0.0000E+00  5.0989E-01  1.0000E-04
 mr-sdci #  3  4    -45.3840341258  1.8854E+00  0.0000E+00  5.6521E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   4

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         807 2x:         336 4x:         141
All internal counts: zz :         356 yy:         930 xx:         352 ww:          66
One-external counts: yz :        1491 yx:        2405 yw:        1220
Two-external counts: yy :        1285 ww:         180 xx:         682 xz:         155 wz:         100 wx:         510
Three-ext.   counts: yx :         665 yw:         370

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1    -2.80329092
   ht   2    -0.05197660    -0.18083887
   ht   3     0.00105062    -0.00204754    -0.00640735
   ht   4    -0.00247791    -0.00224725    -0.00049191    -0.00085494
   ht   5     0.00297689    -0.00037185    -0.00025422     0.00001352    -0.00014823

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1    1.00000       3.779288E-14  -3.758927E-04   8.963061E-04  -1.059512E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 civs   1  -0.965258       0.125551      -0.254038       6.955553E-02  -1.086051E-02
 civs   2  -0.854712      -0.771648        3.15496       0.830418      -0.217042    
 civs   3   -1.23221       -11.2271       -1.28401       -7.68391       -13.3931    
 civs   4   -1.07847       -28.0405       -9.13287       -12.0770        40.0382    
 civs   5  -0.823388       -40.0667       -47.3931        104.545        15.2071    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1  -0.964889       0.147090      -0.211527      -4.914739E-02   1.394826E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1    -0.96488899     0.14708985    -0.21152730    -0.04914739     0.01394826

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1    -46.3478795938  3.3823E-05  3.2757E-06  1.6721E-03  1.0000E-03
 mr-sdci #  4  2    -46.0374240681  6.3074E-02  0.0000E+00  1.7327E-01  1.0000E-04
 mr-sdci #  4  3    -45.5236445111  1.6073E-02  0.0000E+00  5.1217E-01  1.0000E-04
 mr-sdci #  4  4    -45.4300123106  4.5978E-02  0.0000E+00  6.5785E-01  1.0000E-04
 mr-sdci #  4  5    -45.3831316996  1.8845E+00  0.0000E+00  5.5872E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   5

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         807 2x:         336 4x:         141
All internal counts: zz :         356 yy:         930 xx:         352 ww:          66
One-external counts: yz :        1491 yx:        2405 yw:        1220
Two-external counts: yy :        1285 ww:         180 xx:         682 xz:         155 wz:         100 wx:         510
Three-ext.   counts: yx :         665 yw:         370

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1    -2.80329092
   ht   2    -0.05197660    -0.18083887
   ht   3     0.00105062    -0.00204754    -0.00640735
   ht   4    -0.00247791    -0.00224725    -0.00049191    -0.00085494
   ht   5     0.00297689    -0.00037185    -0.00025422     0.00001352    -0.00014823
   ht   6     0.00085139    -0.00009071     0.00002136     0.00001475     0.00000098    -0.00001293

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1    1.00000       3.779288E-14  -3.758927E-04   8.963061E-04  -1.059512E-03  -3.031072E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1  -0.964992       0.135463      -5.410688E-02  -0.209158      -0.138869      -0.125843    
 civs   2  -0.854731      -0.612716        1.21756        2.99138      -1.095039E-02   0.726763    
 civs   3   -1.23397       -9.87892        6.88386       -7.58701       -2.58702        12.5698    
 civs   4   -1.09219       -26.5534        5.64650       -8.80868        27.9622       -32.3870    
 civs   5  -0.904297       -47.0469       -35.3945        9.64723       -82.2947       -68.5230    
 civs   6    1.01473        118.540        349.694       -113.128       -87.6980       -148.828    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.964856       0.129293      -0.120127      -0.190132       9.406304E-04  -4.188539E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.96485606     0.12929264    -0.12012736    -0.19013246     0.00094063    -0.04188539

 trial vector basis is being transformed.  new dimension:   2

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1    -46.3478829178  3.3240E-06  2.8081E-07  4.8258E-04  1.0000E-03
 mr-sdci #  5  2    -46.0633649981  2.5941E-02  0.0000E+00  1.3805E-01  1.0000E-04
 mr-sdci #  5  3    -45.7794346529  2.5579E-01  0.0000E+00  4.4347E-01  1.0000E-04
 mr-sdci #  5  4    -45.4819788658  5.1967E-02  0.0000E+00  5.0650E-01  1.0000E-04
 mr-sdci #  5  5    -45.4061565236  2.3025E-02  0.0000E+00  6.5326E-01  1.0000E-04
 mr-sdci #  5  6    -45.3551904512  1.8566E+00  0.0000E+00  5.8248E-01  1.0000E-04
 
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mr-sdci  convergence criteria satisfied after  5 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1    -46.3478829178  3.3240E-06  2.8081E-07  4.8258E-04  1.0000E-03
 mr-sdci #  5  2    -46.0633649981  2.5941E-02  0.0000E+00  1.3805E-01  1.0000E-04

####################CIUDGINFO####################

   ci vector at position   1 energy=  -46.347882917770

################END OF CIUDGINFO################

 
diagon:itrnv=   0
    1 of the   3 expansion vectors are transformed.
    1 of the   2 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
maximum overlap with reference    1(overlap= 0.96486)

 information on vector: 1 from unit 11 written to unit 48 filename civout              


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -46.3478829178

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     3    4    5   17   22   31

                                         symmetry   a1   a1   a1   a2   b1   b2 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  3  1       1  0.882809                        +-   +                   +  
 z*  3  1       2  0.010811                        +-        +              +  
 z*  3  1       3  0.361417                        +-             +    +       
 z*  3  1       4 -0.011536                        +    +-                  +  
 z*  3  1       5  0.026363                        +     -   +              +  
 z*  3  1       6  0.018665                        +     -        +    +       
 z*  3  1       7  0.037164                        +    +     -             +  
 z*  3  1       8 -0.016039                        +    +    +               - 
 z*  3  1       9 -0.021006                        +    +          -   +       
 z*  3  1      10 -0.075377                        +    +         +     -      
 z*  3  1      11 -0.053013                        +         +-             +  
 z*  3  1      13 -0.015869                        +         +     -   +       
 z*  3  1      14 -0.029507                        +         +    +     -      
 z*  3  1      15  0.020685                        +              +-        +  
 z*  3  1      16  0.043982                        +                   +-   +  
 z*  3  1      18 -0.011543                             +-        +    +       
 z*  3  1      19  0.040000                             +    +-             +  
 z*  3  1      23  0.041696                             +         +-        +  
 z*  3  1      24  0.036191                             +              +-   +  
 z*  3  1      25 -0.019027                                  +-   +    +       
 z*  3  1      28 -0.017643                                       +    +    +- 
 y   3  1      74  0.012172              4( b1 )    -   +         +            
 y   3  1      85 -0.019883              5( a1 )    -   +                   +  
 y   3  1     107 -0.011992              6( a1 )    -        +              +  
 y   3  1     162 -0.027605              5( a1 )   +     -                  +  
 y   3  1     190  0.019522              5( a1 )   +    +                    - 
 y   3  1     257 -0.011286              5( a1 )   +               -   +       
 y   3  1     402  0.010254              7( a1 )        +     -             +  
 y   3  1     446 -0.011872              1( a2 )        +          -        +  
 y   3  1     447  0.014890              2( a2 )        +          -        +  
 y   3  1     471 -0.010959              2( b1 )        +               -   +  
 y   3  1     473  0.013517              4( b1 )        +               -   +  
 x   3  1     679 -0.014227    3( a2 )   7( b1 )   +-                          
 x   3  1     715  0.011021    3( a1 )   4( b2 )   +-                          
 x   3  1     718 -0.016536    6( a1 )   4( b2 )   +-                          
 x   3  1     719 -0.017538    7( a1 )   4( b2 )   +-                          
 x   3  1     743 -0.014612    9( a1 )   6( b2 )   +-                          
 x   3  1     755 -0.014604   10( a1 )   7( b2 )   +-                          
 x   3  1     759  0.013676    3( a2 )   1( b1 )    -   +                      
 x   3  1     765  0.014217    3( a2 )   3( b1 )    -   +                      
 x   3  1     787  0.016881   10( a1 )   1( b2 )    -   +                      
 x   3  1     793  0.018852    5( a1 )   2( b2 )    -   +                      
 x   3  1     809  0.017543   10( a1 )   3( b2 )    -   +                      
 x   3  1     815 -0.021978    5( a1 )   4( b2 )    -   +                      
 x   3  1     833 -0.017842    1( a1 )   6( b2 )    -   +                      
 x   3  1     836 -0.018531    4( a1 )   6( b2 )    -   +                      
 x   3  1    1140 -0.011228    2( a1 )   5( a1 )    -                       +  
 x   3  1    1141  0.015133    3( a1 )   5( a1 )    -                       +  
 x   3  1    1147  0.015078    5( a1 )   6( a1 )    -                       +  
 x   3  1    1152  0.015991    5( a1 )   7( a1 )    -                       +  
 x   3  1    1161  0.014456    1( a1 )   9( a1 )    -                       +  
 x   3  1    1164  0.015029    4( a1 )   9( a1 )    -                       +  
 x   3  1    1201  0.014744    1( b1 )   6( b1 )    -                       +  
 x   3  1    1203  0.015284    3( b1 )   6( b1 )    -                       +  
 x   3  1    1206 -0.012151    1( b1 )   7( b1 )    -                       +  
 x   3  1    1208 -0.012646    3( b1 )   7( b1 )    -                       +  
 x   3  1    1227 -0.011960    1( b2 )   7( b2 )    -                       +  
 x   3  1    1229 -0.012451    3( b2 )   7( b2 )    -                       +  
 x   3  1    3781 -0.010727    1( a1 )   1( b1 )                        -   +  
 w   3  1    4096 -0.049888    1( a1 )   1( b2 )   +    +                      
 w   3  1    4099 -0.038230    4( a1 )   1( b2 )   +    +                      
 w   3  1    4105  0.014019   10( a1 )   1( b2 )   +    +                      
 w   3  1    4111 -0.012342    5( a1 )   2( b2 )   +    +                      
 w   3  1    4118 -0.038306    1( a1 )   3( b2 )   +    +                      
 w   3  1    4121 -0.031374    4( a1 )   3( b2 )   +    +                      
 w   3  1    4127  0.014893   10( a1 )   3( b2 )   +    +                      
 w   3  1    4151  0.013829    1( a1 )   6( b2 )   +    +                      
 w   3  1    4154  0.014728    4( a1 )   6( b2 )   +    +                      
 w   3  1    4271 -0.020399    1( a1 )   1( b1 )   +              +            
 w   3  1    4274 -0.015622    4( a1 )   1( b1 )   +              +            
 w   3  1    4293 -0.015677    1( a1 )   3( b1 )   +              +            
 w   3  1    4296 -0.012830    4( a1 )   3( b1 )   +              +            
 w   3  1    4402  0.020410    1( b1 )   1( b2 )   +                   +       
 w   3  1    4404  0.015594    3( b1 )   1( b2 )   +                   +       
 w   3  1    4416  0.015718    1( b1 )   3( b2 )   +                   +       
 w   3  1    4418  0.012834    3( b1 )   3( b2 )   +                   +       
 w   3  1    4451  0.027104    1( a1 )   1( a1 )   +                        +  
 w   3  1    4457  0.029407    1( a1 )   4( a1 )   +                        +  
 w   3  1    4460  0.017061    4( a1 )   4( a1 )   +                        +  
 w   3  1    4523 -0.039816    1( b1 )   1( b1 )   +                        +  
 w   3  1    4526 -0.043201    1( b1 )   3( b1 )   +                        +  
 w   3  1    4528 -0.025052    3( b1 )   3( b1 )   +                        +  
 w   3  1    4533 -0.010638    1( b1 )   5( b1 )   +                        +  
 w   3  1    4535 -0.010186    3( b1 )   5( b1 )   +                        +  
 w   3  1    4538 -0.012795    1( b1 )   6( b1 )   +                        +  
 w   3  1    4540 -0.013556    3( b1 )   6( b1 )   +                        +  
 w   3  1    4546  0.010179    3( b1 )   7( b1 )   +                        +  
 w   3  1    4551  0.012705    1( b2 )   1( b2 )   +                        +  
 w   3  1    4554  0.013783    1( b2 )   3( b2 )   +                        +  
 w   3  1    4857 -0.066746    1( a1 )   1( a1 )        +                   +  
 w   3  1    4863 -0.056896    1( a1 )   4( a1 )        +                   +  
 w   3  1    4866 -0.028855    4( a1 )   4( a1 )        +                   +  
 w   3  1    4871 -0.022186    5( a1 )   5( a1 )        +                   +  
 w   3  1    4929 -0.069284    1( b1 )   1( b1 )        +                   +  
 w   3  1    4932 -0.059198    1( b1 )   3( b1 )        +                   +  
 w   3  1    4934 -0.029984    3( b1 )   3( b1 )        +                   +  
 w   3  1    4957 -0.070412    1( b2 )   1( b2 )        +                   +  
 w   3  1    4960 -0.060172    1( b2 )   3( b2 )        +                   +  
 w   3  1    4962 -0.030445    3( b2 )   3( b2 )        +                   +  
 w   3  1    5293 -0.029314    1( a1 )   1( a1 )                  +    +       
 w   3  1    5299 -0.025200    1( a1 )   4( a1 )                  +    +       
 w   3  1    5302 -0.012781    4( a1 )   4( a1 )                  +    +       
 w   3  1    5365 -0.026028    1( b1 )   1( b1 )                  +    +       
 w   3  1    5368 -0.022106    1( b1 )   3( b1 )                  +    +       
 w   3  1    5370 -0.011232    3( b1 )   3( b1 )                  +    +       
 w   3  1    5393 -0.029109    1( b2 )   1( b2 )                  +    +       
 w   3  1    5396 -0.024778    1( b2 )   3( b2 )                  +    +       
 w   3  1    5398 -0.012505    3( b2 )   3( b2 )                  +    +       

 ci coefficient statistics:
           rq > 0.1                2
      0.1> rq > 0.01             106
     0.01> rq > 0.001            765
    0.001> rq > 0.0001          1302
   0.0001> rq > 0.00001          489
  0.00001> rq > 0.000001         101
 0.000001> rq                   2835
           all                  5600
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         216 2x:           0 4x:           0
All internal counts: zz :         356 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.882809194308    -40.875725271095
     2     2      0.010810732690     -0.500544883984
     3     3      0.361417065499    -16.734281982348
     4     4     -0.011535974843      0.534224487821
     5     5      0.026362614669     -1.220838789086
     6     6      0.018665476876     -0.864373173794
     7     7      0.037163854676     -1.720992423229
     8     8     -0.016038915677      0.742733363942
     9     9     -0.021005782510      0.972752031670
    10    10     -0.075377334951      3.490648336918
    11    11     -0.053013470408      2.455006280481
    12    12      0.006707576705     -0.310576264951
    13    13     -0.015869327712      0.734876471395
    14    14     -0.029507188569      1.366466919138
    15    15      0.020685467036     -0.957907683839
    16    16      0.043981740765     -2.036752794970
    17    17      0.002609719323     -0.120842597762
    18    18     -0.011543359707      0.534138242429
    19    19      0.039999582296     -1.851202590482
    20    20     -0.003431070460      0.158861259615
    21    21     -0.005058511570      0.234212681456
    22    22      0.005952774826     -0.275616772330
    23    23      0.041695804052     -1.929748520584
    24    24      0.036190607398     -1.674850620694
    25    25     -0.019026569816      0.880622890341
    26    26     -0.004079399228      0.188888210569
    27    27      0.008112746452     -0.375619580192
    28    28     -0.017643241163      0.816575082898

 number of reference csfs (nref) is    28.  root number (iroot) is  1.
 c0**2 =   0.93098618  c**2 (all zwalks) =   0.93098618

 pople ci energy extrapolation is computed with  4 correlated electrons.

 eref      =    -46.301867310060   "relaxed" cnot**2         =   0.930986178089
 eci       =    -46.347882917770   deltae = eci - eref       =  -0.046015607710
 eci+dv1   =    -46.351058630725   dv1 = (1-cnot**2)*deltae  =  -0.003175712956
 eci+dv2   =    -46.351294045701   dv2 = dv1 / cnot**2       =  -0.003411127931
 eci+dv3   =    -46.351567157679   dv3 = dv1 / (2*cnot**2-1) =  -0.003684239910
 eci+pople =    -46.349586145036   (  4e- scaled deltae )    =  -0.047718834976
 passed aftci ... 
 readint2: molcas,dalton2=                     0                     0
 files%faoints=aoints              
lodens (list->root)=  1
sifcfg setup: record length 4096 DP
# d1 elements per record  3272
# d2 elements per record  2730
  The MR-CISD density will be calculated.
 item #                     1 suffix=:.drt1.state1:
 read_civout: repnuc=  -43.4985876562748     
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method:  0 last record  1max overlap with ref# 96% root-following 0
 MR-CISD energy:   -46.34788292    -2.84929526
 residuum:     0.00048258
 deltae:     0.00000332
 apxde:     0.00000028

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.96485606     0.12929264    -0.12012736    -0.19013246     0.00094063    -0.04188539     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.96485606     0.12929264     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=     135  DYX=     275  DYW=     150
   D0Z=      37  D0Y=     125  D0X=      38  D0W=      12
  DDZI=      96 DDYI=     240 DDXI=      66 DDWI=      30
  DDZE=       0 DDYE=      90 DDXE=      36 DDWE=      15
================================================================================
Trace of MO density:     4.000000
    4  correlated and     8  frozen core electrons

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     1.86975870    -0.00022990     0.02556818     0.00000000     0.00436180     0.00324094
   MO   4     0.00000000     0.00000000    -0.00022990     0.83787437     0.00892653     0.00000000    -0.00390049     0.00498052
   MO   5     0.00000000     0.00000000     0.02556818     0.00892653     0.01537221     0.00000000     0.00153567     0.00106045
   MO   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.02363510     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000     0.00436180    -0.00390049     0.00153567     0.00000000     0.00068954    -0.00035946
   MO   8     0.00000000     0.00000000     0.00324094     0.00498052     0.00106045     0.00000000    -0.00035946     0.00086974
   MO   9     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.01662058     0.00000000     0.00000000
   MO  10     0.00000000     0.00000000    -0.02930412    -0.00000732     0.00040517     0.00000000     0.00002420     0.00001767
   MO  11     0.00000000     0.00000000     0.00478651    -0.00419892     0.00189946     0.00000000     0.00084991    -0.00052129
   MO  12     0.00000000     0.00000000    -0.00451648    -0.00412527    -0.00162226     0.00000000     0.00031081    -0.00093252
   MO  13     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00350804     0.00000000     0.00000000
   MO  14     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00012025     0.00000000     0.00000000
   MO  15     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00213543     0.00000000     0.00000000
   MO  16     0.00000000     0.00000000    -0.00022726     0.00000037    -0.00003033     0.00000000    -0.00000411    -0.00000303

                MO   9         MO  10         MO  11         MO  12         MO  13         MO  14         MO  15         MO  16
   MO   3     0.00000000    -0.02930412     0.00478651    -0.00451648     0.00000000     0.00000000     0.00000000    -0.00022726
   MO   4     0.00000000    -0.00000732    -0.00419892    -0.00412527     0.00000000     0.00000000     0.00000000     0.00000037
   MO   5     0.00000000     0.00040517     0.00189946    -0.00162226     0.00000000     0.00000000     0.00000000    -0.00003033
   MO   6     0.01662058     0.00000000     0.00000000     0.00000000     0.00350804    -0.00012025    -0.00213543     0.00000000
   MO   7     0.00000000     0.00002420     0.00084991     0.00031081     0.00000000     0.00000000     0.00000000    -0.00000411
   MO   8     0.00000000     0.00001767    -0.00052129    -0.00093252     0.00000000     0.00000000     0.00000000    -0.00000303
   MO   9     0.01206520     0.00000000     0.00000000     0.00000000     0.00281916    -0.00013454    -0.00183362     0.00000000
   MO  10     0.00000000     0.00552199     0.00002962    -0.00002737     0.00000000     0.00000000     0.00000000    -0.00025884
   MO  11     0.00000000     0.00002962     0.00117817     0.00055923     0.00000000     0.00000000     0.00000000    -0.00000520
   MO  12     0.00000000    -0.00002737     0.00055923     0.00114364     0.00000000     0.00000000     0.00000000     0.00000487
   MO  13     0.00281916     0.00000000     0.00000000     0.00000000     0.00087296    -0.00007913    -0.00066303     0.00000000
   MO  14    -0.00013454     0.00000000     0.00000000     0.00000000    -0.00007913     0.00137381     0.00003172     0.00000000
   MO  15    -0.00183362     0.00000000     0.00000000     0.00000000    -0.00066303     0.00003172     0.00172744     0.00000000
   MO  16     0.00000000    -0.00025884    -0.00000520     0.00000487     0.00000000     0.00000000     0.00000000     0.00001731

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     2.00000000     1.87061070     0.83806056     0.03626159     0.01560978     0.00501023     0.00268691
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     0.00163751     0.00137069     0.00039423     0.00034368     0.00008733     0.00001286     0.00001048     0.00000360

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4
   MO   1     0.15262208     0.00255030    -0.00297804     0.00000000
   MO   2     0.00255030     0.00052494    -0.00060514     0.00000000
   MO   3    -0.00297804    -0.00060514     0.00076729     0.00000000
   MO   4     0.00000000     0.00000000     0.00000000     0.00129709

Natural orbital populations,block 2
              MO     1       MO     2       MO     3       MO     4       MO
  occ(*)=     0.15272358     0.00129709     0.00116183     0.00002891

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     0.15495424     0.00000000     0.00283894     0.00000000    -0.00337791     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.02341189     0.00000000     0.01638655     0.00000000     0.00341471     0.00146374
   MO   4     0.00000000     0.00283894     0.00000000     0.00056089     0.00000000    -0.00067081     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.01638655     0.00000000     0.01183969     0.00000000     0.00273687     0.00125985
   MO   6     0.00000000    -0.00337791     0.00000000    -0.00067081     0.00000000     0.00086974     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000     0.00341471     0.00000000     0.00273687     0.00000000     0.00084664     0.00045836
   MO   8     0.00000000     0.00000000     0.00146374     0.00000000     0.00125985     0.00000000     0.00045836     0.00131743
   MO   9     0.00000000     0.00000000    -0.00092339     0.00000000    -0.00083985     0.00000000    -0.00034968    -0.00078290

                MO   9
   MO   2     0.00000000
   MO   3    -0.00092339
   MO   4     0.00000000
   MO   5    -0.00083985
   MO   6     0.00000000
   MO   7    -0.00034968
   MO   8    -0.00078290
   MO   9     0.00106539

Natural orbital populations,block 3
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     0.15508093     0.03571610     0.00192418     0.00127708     0.00043470     0.00039553     0.00002686
              MO     9       MO
  occ(*)=     0.00001052

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     0.83686704     0.00000000     0.00621031     0.00000000    -0.00570630     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.02297190     0.00000000     0.01591322     0.00000000    -0.00322142    -0.00177304
   MO   4     0.00000000     0.00621031     0.00000000     0.00113399     0.00000000    -0.00128441     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.01591322     0.00000000     0.01137714     0.00000000    -0.00256544    -0.00158641
   MO   6     0.00000000    -0.00570630     0.00000000    -0.00128441     0.00000000     0.00167072     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000    -0.00322142     0.00000000    -0.00256544     0.00000000     0.00079133     0.00063650
   MO   8     0.00000000     0.00000000    -0.00177304     0.00000000    -0.00158641     0.00000000     0.00063650     0.00195938
   MO   9     0.00000000     0.00000000     0.00015482     0.00000000     0.00010142     0.00000000    -0.00000564    -0.00043397

                MO   9
   MO   2     0.00000000
   MO   3     0.00015482
   MO   4     0.00000000
   MO   5     0.00010142
   MO   6     0.00000000
   MO   7    -0.00000564
   MO   8    -0.00043397
   MO   9     0.00105105

Natural orbital populations,block 4
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     0.83695230     0.03479056     0.00263109     0.00207531     0.00092963     0.00034489     0.00008837
              MO     9       MO
  occ(*)=     0.00001041


 total number of electrons =   12.0000000000

 test slabel:                     4
  a1  a2  b1  b2 


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        a1  partial gross atomic populations
   ao class       1a1        2a1        3a1        4a1        5a1        6a1 
     3_ s       0.000000   2.000000   1.870216   0.000000   0.000000   0.000099
     3_ p       2.000000   0.000000   0.000000   0.000000   0.036013   0.000000
     3_ d       0.000000   0.000000   0.000395   0.838061   0.000000   0.015511
     3_ f       0.000000   0.000000   0.000000   0.000000   0.000249   0.000000
 
   ao class       7a1        8a1        9a1       10a1       11a1       12a1 
     3_ s       0.004979   0.000000   0.000000   0.000000   0.000000   0.000000
     3_ p       0.000000   0.000000   0.000201   0.000000   0.000348   0.000000
     3_ d       0.000031   0.002687   0.000000   0.000000   0.000000   0.000344
     3_ f       0.000000   0.000000   0.001436   0.001371   0.000046   0.000000
 
   ao class      13a1       14a1       15a1       16a1 
     3_ s       0.000000   0.000000   0.000000   0.000004
     3_ p       0.000000   0.000000   0.000010   0.000000
     3_ d       0.000087   0.000013   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000000

                        a2  partial gross atomic populations
   ao class       1a2        2a2        3a2        4a2 
     3_ d       0.152724   0.000000   0.001162   0.000029
     3_ f       0.000000   0.001297   0.000000   0.000000

                        b1  partial gross atomic populations
   ao class       1b1        2b1        3b1        4b1        5b1        6b1 
     3_ p       2.000000   0.000000   0.035545   0.000137   0.000000   0.000405
     3_ d       0.000000   0.155081   0.000000   0.000000   0.001277   0.000000
     3_ f       0.000000   0.000000   0.000171   0.001788   0.000000   0.000030
 
   ao class       7b1        8b1        9b1 
     3_ p       0.000001   0.000000   0.000011
     3_ d       0.000000   0.000027   0.000000
     3_ f       0.000395   0.000000   0.000000

                        b2  partial gross atomic populations
   ao class       1b2        2b2        3b2        4b2        5b2        6b2 
     3_ p       2.000000   0.000000   0.034600   0.000000   0.000142   0.000102
     3_ d       0.000000   0.836952   0.000000   0.002631   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000190   0.000000   0.001933   0.000828
 
   ao class       7b2        8b2        9b2 
     3_ p       0.000286   0.000000   0.000010
     3_ d       0.000000   0.000088   0.000000
     3_ f       0.000059   0.000000   0.000000


                        gross atomic populations
     ao            3_
      s         3.875298
      p         6.107812
      d         2.007099
      f         0.009792
    total      12.000000
 

 Total number of electrons:   12.00000000

 DA ...
