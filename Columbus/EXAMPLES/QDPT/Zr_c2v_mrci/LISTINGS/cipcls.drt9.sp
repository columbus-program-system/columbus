
 program cipc      

 print the csf info for mrsdci wave functions

 written by: ron shepard

 version date: 06-jun-96

 This Version of Program cipc is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 workspace allocation parameters: lencor=  13107200 mem1=         0 ifirst=         1

 drt header information:
  cidrt_title                                                                    
 nmot  =    38 niot  =     6 nfct  =     4 nfvt  =     0
 nrow  =    23 nsym  =     4 ssym  =     1 lenbuf=  1600
 spnorb=     F spnodd=     F lxyzir(1:3)= 0 0 0
 nwalk,xbar:         50       15       20       15        0
 nvalwt,nvalw:       38        3       20       15        0
 ncsft:            1539
 map(*)=    -1 -1 29 30 31  1  2  3  4  5  6  7  8  9 10 11 32 12 13 14
            -1 33 15 16 17 18 19 20 21 -1 34 22 23 24 25 26 27 28
 mu(*)=      0  0  0  0  0  0
 syml(*) =   1  1  1  2  3  4
 rmo(*)=     3  4  5  1  2  2

 indx01:    38 indices saved in indxv(*)
 test nroots froot                      1                     1
===================================ROOT # 1===================================

 rdhciv: CI vector file information:
  cidrt_title                                                                    
 energy computed by program ciudg.       zam792            17:53:53.290 17-Dec-13

 lenrec =   32768 lenci =      1539 ninfo =  6 nenrgy =  6 ntitle =  2

 Max. overlap with ref vector #        1
 Valid ci vector #        1
 Method:        0       96% overlap
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -4.349858765627E+01, ietype=    6,   fcore energy of type: H1(*)   
 energy( 3)= -4.629138747375E+01, ietype=-1026,   total energy of type: MRSDCI  
 energy( 4)=  5.716780620592E-04, ietype=-2055, cnvginf energy of type: CI-Resid
 energy( 5)=  1.091664650144E-05, ietype=-2056, cnvginf energy of type: CI-D.E. 
 energy( 6)=  4.660218375590E-07, ietype=-2057, cnvginf energy of type: CI-ApxDE
==================================================================================
space sufficient for valid walk range           1         38
               respectively csf range           1       1539

 space is available for   4357922 coefficients.

 updated histogram parameters:
 csfmn = 0.0000E+00 csfmx = 1.0000E+00 fhist = 5.0000E-01 nhist =  20

 this program will print the csfs generated from
 the drt according to the following print options :

 1) run in batch mode: all valid roots are automatically
    analysed and csf info is printed by default contribution
    threshold 0.01 
 2) run in interactive mode
 3) generate files for cioverlap without symmetry
 4) generate files for cioverlap with symmetry

 input menu number [  1]:
================================================================================
===================================VECTOR # 1===================================
================================================================================


 rdcivnew:      78 coefficients were selected.
 workspace: ncsfmx=    1539
 ncsfmx=                  1539

 histogram parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =    1539 ncsft =    1539 ncsf =      78
 nhist =  20 fhist = 0.50000

    cmin                cmax        num  '*'=     2 csfs.
 ----------          ----------   ----- ---------|---------|---------|---------|
 5.0000E-01 <= |c| < 1.0000E+00       1 *
 2.5000E-01 <= |c| < 5.0000E-01       0
 1.2500E-01 <= |c| < 2.5000E-01       0
 6.2500E-02 <= |c| < 1.2500E-01       6 ***
 3.1250E-02 <= |c| < 6.2500E-02       3 **
 1.5625E-02 <= |c| < 3.1250E-02      43 **********************
 7.8125E-03 <= |c| < 1.5625E-02      25 *************
 3.9062E-03 <= |c| < 7.8125E-03       0
 1.9531E-03 <= |c| < 3.9062E-03       0
 9.7656E-04 <= |c| < 1.9531E-03       0
 4.8828E-04 <= |c| < 9.7656E-04       0
 2.4414E-04 <= |c| < 4.8828E-04       0
 1.2207E-04 <= |c| < 2.4414E-04       0
 6.1035E-05 <= |c| < 1.2207E-04       0
 3.0518E-05 <= |c| < 6.1035E-05       0
 1.5259E-05 <= |c| < 3.0518E-05       0
 7.6294E-06 <= |c| < 1.5259E-05       0
 3.8147E-06 <= |c| < 7.6294E-06       0
 0.0000E+00 <= |c| < 3.8147E-06       0
                                  ----- ---------|---------|---------|---------|
                  total read =       78 total stored =      78

 from the selected csfs,
 min(|csfvec(:)|) = 1.0036E-02    max(|csfvec(:)|) = 9.6264E-01
 norm=   1.00000000000000     
 csfs will be printed based on coefficient magnitudes.

 current csfvec(*) selection parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =    1539 ncsft =    1539 ncsf =      78

 i:slabel(i) =  1: a1   2: a2   3: b1   4: b2 
 
 frozen orbital =    1    2    3    4
 symfc(*)       =    1    1    3    4
 label          =  a1   a1   b1   b2 
 rmo(*)         =    1    2    1    1
 
 internal level =    1    2    3    4    5    6
 syml(*)        =    1    1    1    2    3    4
 label          =  a1   a1   a1   a2   b1   b2 
 rmo(*)         =    3    4    5    1    2    2

 printing selected csfs in sorted order from cmin = 0.00000 to cmax = 1.00000

   indcsf     c     c**2   v  lab:rmo  lab:rmo   step(*)
  ------- -------- ------- - ---- --- ---- --- ------------
          1  0.96264 0.92668 z*                    100111
         50 -0.10018 0.01004 y           b2 :  4  1100110
         57  0.10017 0.01003 y           b1 :  4  1100101
         63 -0.10015 0.01003 y           a2 :  2  1100011
         52  0.07476 0.00559 y           b2 :  6  1100110
         59 -0.07476 0.00559 y           b1 :  6  1100101
         64  0.07476 0.00559 y           a2 :  3  1100011
        529  0.03195 0.00102 x  a1 :  6  b2 :  3 11100001
        410 -0.03194 0.00102 x  a1 :  6  b1 :  3 11100010
        361 -0.03194 0.00102 x  b1 :  3  b2 :  3 11100100
        359  0.02422 0.00059 x  a1 : 15  a2 :  4 11100100
        551  0.02403 0.00058 x  a1 :  6  b2 :  5 11100001
        432 -0.02403 0.00058 x  a1 :  6  b1 :  5 11100010
        363 -0.02403 0.00058 x  b1 :  5  b2 :  3 11100100
        532  0.02403 0.00058 x  a1 :  9  b2 :  3 11100001
        375 -0.02403 0.00058 x  b1 :  3  b2 :  5 11100100
        413 -0.02403 0.00058 x  a1 :  9  b1 :  3 11100010
       1375  0.02221 0.00049 x  a1 : 10  b1 :  4 11000101
       1298 -0.02221 0.00049 x  a1 : 10  b2 :  4 11000110
       1462 -0.02221 0.00049 x  a1 : 10  a2 :  2 11000011
        497  0.02205 0.00049 x  a2 :  3  b2 :  6 11100010
        518 -0.02205 0.00049 x  a2 :  3  b1 :  6 11100001
        385 -0.02205 0.00049 x  b1 :  6  b2 :  6 11100100
        504  0.02134 0.00046 x  a2 :  4  b2 :  8 11100010
        401 -0.01973 0.00039 x  b1 :  8  b2 :  8 11100100
       1415 -0.01925 0.00037 x  a1 :  6  b1 :  8 11000101
       1338  0.01918 0.00037 x  a1 :  6  b2 :  8 11000110
        377 -0.01915 0.00037 x  b1 :  5  b2 :  5 11100100
        435 -0.01915 0.00037 x  a1 :  9  b1 :  5 11100010
        554  0.01915 0.00037 x  a1 :  9  b2 :  5 11100001
        525 -0.01908 0.00036 x  a2 :  4  b1 :  8 11100001
       1497  0.01890 0.00036 x  b1 :  9  b2 :  3 11000011
        485  0.01825 0.00033 x  a1 : 15  b1 :  9 11100010
       1533  0.01813 0.00033 x  b1 :  3  b2 :  9 11000011
       1292 -0.01793 0.00032 x  a1 : 15  b2 :  3 11000110
       1418 -0.01776 0.00032 x  a1 :  9  b1 :  8 11000101
       1341  0.01771 0.00031 x  a1 :  9  b2 :  8 11000110
       1511  0.01744 0.00030 x  b1 :  9  b2 :  5 11000011
         43  0.01716 0.00029 y           b2 :  4  1101001
       1473  0.01680 0.00028 x  a1 : 10  a2 :  3 11000011
       1397 -0.01679 0.00028 x  a1 : 10  b1 :  6 11000101
       1320  0.01679 0.00028 x  a1 : 10  b2 :  6 11000110
         19  0.01677 0.00028 y           b1 :  4  1110010
       1535  0.01672 0.00028 x  b1 :  5  b2 :  9 11000011
       1314 -0.01657 0.00027 x  a1 : 15  b2 :  5 11000110
        528 -0.01653 0.00027 x  a2 :  4  b1 :  9 11100001
        369 -0.01623 0.00026 x  b1 :  4  b2 :  4 11100100
        490  0.01623 0.00026 x  a2 :  2  b2 :  4 11100010
        511 -0.01623 0.00026 x  a2 :  2  b1 :  4 11100001
       1264 -0.01604 0.00026 x  a2 :  4  b1 :  3 11000110
       1480  0.01604 0.00026 x  a1 :  6  a2 :  4 11000011
       1439  0.01604 0.00026 x  a2 :  4  b2 :  3 11000101
       1368 -0.01563 0.00024 x  a1 : 14  b1 :  3 11000101
        491 -0.01519 0.00023 x  a2 :  3  b2 :  4 11100010
        512  0.01519 0.00023 x  a2 :  3  b1 :  4 11100001
        371  0.01519 0.00023 x  b1 :  6  b2 :  4 11100100
        383  0.01519 0.00023 x  b1 :  4  b2 :  6 11100100
        517  0.01519 0.00023 x  a2 :  2  b1 :  6 11100001
        496 -0.01519 0.00023 x  a2 :  2  b2 :  6 11100010
        603 -0.01491 0.00022 x  a1 : 14  b2 :  9 11100001
       1483  0.01470 0.00022 x  a1 :  9  a2 :  4 11000011
       1270 -0.01470 0.00022 x  a2 :  4  b1 :  5 11000110
       1445  0.01470 0.00022 x  a2 :  4  b2 :  5 11000101
       1390 -0.01440 0.00021 x  a1 : 14  b1 :  5 11000101
        507 -0.01349 0.00018 x  a2 :  4  b2 :  9 11100010
         15 -0.01300 0.00017 y           a2 :  2  1110100
        121  0.01278 0.00016 y           a1 : 10  1000111
         32 -0.01191 0.00014 y           a2 :  2  1101100
        604 -0.01148 0.00013 x  a1 : 15  b2 :  9 11100001
       1369 -0.01128 0.00013 x  a1 : 15  b1 :  3 11000101
        380  0.01067 0.00011 x  b1 :  8  b2 :  5 11100100
        398  0.01059 0.00011 x  b1 :  5  b2 :  8 11100100
        479 -0.01052 0.00011 x  a1 :  9  b1 :  9 11100010
       1391 -0.01045 0.00011 x  a1 : 15  b1 :  5 11000101
        366  0.01018 0.00010 x  b1 :  8  b2 :  3 11100100
        598 -0.01014 0.00010 x  a1 :  9  b2 :  9 11100001
        396  0.01010 0.00010 x  b1 :  3  b2 :  8 11100100
        476 -0.01004 0.00010 x  a1 :  6  b1 :  9 11100010
           78 csfs were printed in this range.
