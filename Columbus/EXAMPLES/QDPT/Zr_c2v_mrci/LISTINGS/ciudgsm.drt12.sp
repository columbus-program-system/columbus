1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=     4)...
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.2453833562  9.3259E-15  5.4499E-02  2.1660E-01  1.0000E-03

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  1  1    -46.2453833562  9.3259E-15  5.4499E-02  2.1660E-01  1.0000E-03

 from bk iterations: iconv=   1

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.2897931972  4.4410E-02  1.2745E-03  2.8636E-02  1.0000E-03
 mr-sdci #  2  1    -46.2911986513  1.4055E-03  1.1909E-04  9.5037E-03  1.0000E-03
 mr-sdci #  3  1    -46.2913733613  1.7471E-04  1.6361E-05  3.4027E-03  1.0000E-03
 mr-sdci #  4  1    -46.2913846889  1.1328E-05  3.6434E-06  1.6597E-03  1.0000E-03
 mr-sdci #  5  1    -46.2913877885  3.0996E-06  1.0994E-07  2.9841E-04  1.0000E-03

 mr-sdci  convergence criteria satisfied after  5 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  5  1    -46.2913877885  3.0996E-06  1.0994E-07  2.9841E-04  1.0000E-03

 number of reference csfs (nref) is     4.  root number (iroot) is  1.
 c0**2 =   0.92669760  c**2 (all zwalks) =   0.92669760

 eref      =    -46.245383351589   "relaxed" cnot**2         =   0.926697600489
 eci       =    -46.291387788491   deltae = eci - eref       =  -0.046004436902
 eci+dv1   =    -46.294760024104   dv1 = (1-cnot**2)*deltae  =  -0.003372235613
 eci+dv2   =    -46.295026770195   dv2 = dv1 / cnot**2       =  -0.003638981704
 eci+dv3   =    -46.295339340608   dv3 = dv1 / (2*cnot**2-1) =  -0.003951552117
 eci+pople =    -46.293204442112   (  4e- scaled deltae )    =  -0.047821090523
