1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      2009-03.    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


================ Computing sorting integral file structure ================

                    -----z----- -----y----- -----x----- -----w----- ---total---

                CSFs        24         490        1414        2114        4042
      internal walks       105          70          15          21         211
valid internal walks        24          70          15          21         130
 getinfoarray: info=                     6 :                     1
                  8192                  6552                  8192
                  5460                     0
 icd(3)=                  2976 ci%nnlev=                   595  l2rec=
                  8192  n2max=                  5460
 lcore1,lcore2=              13068137              13044821
 lencor,maxblo              13107200                 60000
========================================
 current settings:
 minbl3         381
 minbl4         381
 locmaxbl3     4288
 locmaxbuf     2144
 maxbl3       60000
 maxbl3       60000
 maxbl4       60000
 maxbuf       30006
========================================

 sorted 4-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)

 sorted 3-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)
 Orig.  diagonal integrals:  1electron:        34
                             0ext.    :        42
                             2ext.    :       336
                             4ext.    :       812


 Orig. off-diag. integrals:  4ext.    :     24411
                             3ext.    :     18720
                             2ext.    :      6792
                             1ext.    :      1272
                             0ext.    :        99
                             2ext. SO :         0
                             1ext. SO :         0
                             0ext. SO :         0
                             1electron:       128


 Sorted integrals            3ext.  w :     17370 x :     16020
                             4ext.  w :     20955 x :     17777


Cycle #  1 sortfile size=    163835(       5 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13068137
Cycle #  2 sortfile size=    163835(       5 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13068137
 minimum size of srtscr:     98301 WP (     3 records)
 maximum size of srtscr:    163835 WP (     5 records)
diagi   file:      4 records  of   1536 WP each=>       6144 WP total
ofdgi   file:      8 records  of   1536 WP each=>      12288 WP total
fil3w   file:      1 records  of  30006 WP each=>      30006 WP total
fil3x   file:      1 records  of  30006 WP each=>      30006 WP total
fil4w   file:      1 records  of  30006 WP each=>      30006 WP total
fil4x   file:      1 records  of  30006 WP each=>      30006 WP total
 compressed index vector length=                    61
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 0,
  GSET = 0,
   DAVCOR =10,
  NCOREL = 4
   nroot=1
  IVMODE = 3
  NBKITR = 1
   nvbkmn=2
  RTOLBK = 1e-3
   niter=30
   nvcimn=2
   rtolci=1e-3
  NVCIMX = 6
  NVRFMX = 6
  NVBKMX = 6
  IDEN  = 1
  CSFPRN = 10,
  update_mode=1
  nvrfmn=2
 /&end
 ------------------------------------------------------------------------
lodens (list->root)=  1
invlodens (root->list)=  1
 USING SEGMENTS OF EQUAL SIZE

****************  list of control variables  ****************
 lvlprt =    0      nroot  =    1      noldv  =   0      noldhv =   0
 nunitv =    1      nbkitr =    1      niter  =  30      davcor =  10
 csfprn =   10      ivmode =    3      istrt  =   0      vout   =   0
 iortls =    0      nvbkmx =    6      ibktv  =  -1      ibkthv =  -1
 nvcimx =    6      icitv  =   -1      icithv =  -1      frcsub =   0
 nvbkmn =    2      nvcimn =    2      maxseg =   4      nrfitr =  30
 ncorel =    4      nvrfmx =    6      nvrfmn =   2      iden   =   1
 itran  =    0      froot  =    0      rtmode =   0      ncouple=   1
 skipso =    F      dalton2=    0      molcas =   0      finalv =   0
 finalw =    0      cosmocalc=   0    with_tsklst=   0
 nsegwx =    1     1     1     1
 nseg0x =    1     1     1     1
 nseg1x =    1     1     1     1
 nseg2x =    1     1     1     1
 nseg3x =    1     1     1     1
 nseg4x =    1     1     1     1
 no0ex  =      0    no1ex  =      0    no2ex  =     0    no3ex  =     0
 no4ex  =      0    nodiag =      0
 cdg4ex =    1      c3ex1ex=    1      c2ex0ex=   1
 fileloc=    0     0     0     0     0     0     0     1     1     1
 directhd=   1      noaqccshift_zyxw=      0
 critical_crit=-1.00000    critical_delta= 0.05000

 ctol   = 0.010000    lrtshift=1.000000    smalld =0.001000


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-03    1.000E-03
 Computing density:                    .drt1.state1
 using                      1  nodes and                      1  cores.
 szdg/szodg per processor=                  6144                 12288
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core           13107199 DP per process

********** Integral sort section *************


 workspace allocation information: lencor=  13107199

 echo of the input for program cisrt:
 ------------------------------------------------------------------------
  &input
  maxbl3=60000
  maxbl4=60000
  &end
 ------------------------------------------------------------------------
 
 ( 6) listing file:                    ciudgls             
 ( 5) input file:                      cisrtin   
 (17) cidrt file:                      cidrtfl             
 (11) transformed integrals file:      moints    
 (12) diagonal integral file:          diagint             
 (13) off-diagonal integral file:      ofdgint             
 (31) 4-external w integrals file:     fil4w               
 (32) 4-external x integrals file:     fil4x               
 (33) 3-external w integrals file:     fil3w               
 (34) 3-external x integrals file:     fil3x               
 (21) scratch da sorting file:         srtscr              
 (12) 2-e integral file [fsplit=2]:    moints2   

 input integral file header information:
                                                                                 
 aoints SIFS file created by argos.      zam792            17:53:52.086 17-Dec-13
  cidrt_title                                                                    
 MO-coefficients from mcscf.x                                                    
  with dummy occupation 1.0 for active orbitals                                  
  total ao core energy =    0.000000000                                          
 MCSCF energy =     -46.301878576                                                
 SIFS file created by program tran.      zam792            17:53:52.407 17-Dec-13

 input energy(*) values:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total core energy =   0.000000000000E+00

 nsym = 4 nmot=  34

 symmetry  =    1    2    3    4
 slabel(*) =   a1   a2   b1   b2
 nmpsy(*)  =   14    4    8    8

 info(*) =          1      8192      6552      8192      5460         0

 orbital labels, i:molab(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012  13:tout:013  14:tout:014  15:tout:015  16:tout:016  17:tout:017  18:tout:018  19:tout:019  20:tout:020
  21:tout:021  22:tout:022  23:tout:023  24:tout:024  25:tout:025  26:tout:026  27:tout:027  28:tout:028  29:tout:029  30:tout:030
  31:tout:031  32:tout:032  33:tout:033  34:tout:034

 input parameters:
 prnopt=  0
 ldamin=    4095 ldamax=   32767 ldainc=      64
 maxbuf=   30006 maxbl3=   60000 maxbl4=   60000 intmxo=    1536
  Using 32 bit compression 

 drt information:
  cidrt_title                                                                    
 nmotd =  38 nfctd =   4 nfvtc =   0 nmot  =  34
 nlevel =  34 niot  =   6 lowinl=  29
 orbital-to-level map(*)
   -1  -1  29  30  31   1   2   3   4   5   6   7   8   9  10  11  32  12  13  14
   -1  33  15  16  17  18  19  20  21  -1  34  22  23  24  25  26  27  28
 compressed map(*)
   29  30  31   1   2   3   4   5   6   7   8   9  10  11  32  12  13  14  33  15
   16  17  18  19  20  21  34  22  23  24  25  26  27  28
 levsym(*)
    1   1   1   1   1   1   1   1   1   1   1   2   2   2   3   3   3   3   3   3
    3   4   4   4   4   4   4   4   1   1   1   2   3   4
 repartitioning mu(*)=
   0.  0.  0.  0.  0.  0.

 new core energy added to the energy(*) list.
 from the integral file: h1_core= -4.349858765627E+01

 indxdg: diagonal integral statistics.
 total number of integrals contributing to diagonal matrix elements:      1190
 number with all external indices:       812
 number with half external - half internal indices:       336
 number with all internal indices:        42

 indxof: off-diagonal integral statistics.
    4-external integrals: num=      24411 strt=          1
    3-external integrals: num=      18720 strt=      24412
    2-external integrals: num=       6792 strt=      43132
    1-external integrals: num=       1272 strt=      49924
    0-external integrals: num=         99 strt=      51196

 total number of off-diagonal integrals:       51294


 indxof(2nd)  ittp=   3 numx(ittp)=        6792
 indxof(2nd)  ittp=   4 numx(ittp)=        1272
 indxof(2nd)  ittp=   5 numx(ittp)=          99

 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg=  12931980
 pro2e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652

 pro2e:     25126 integrals read in     5 records.

 pro2e:         0 integrals 34-ext integrals skipped.
 pro1e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652
 pro1e: eref =    0.000000000000000E+00
 total size of srtscr:                     4  records of                  32767 
 WP =               1048544 Bytes
 putdg        1     596    1191    1786    3322   36089   57934    3018   46706   90394
   123161  131353  136813  158652

 putf:       4 buffers of length    1536 written to file 12
 diagonal integral file completed.

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep4e:    17 blocks of linear combinations of 4-external integrals processed.
 number of sorted 4-external integrals      38732
 number of original 4-external integrals    24411


 putf34: external integral file complete. nfilw=    31 nfilx=    32 nrecw=     1 nrecx=     1 lbufp= 30006

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep3e:    19 blocks of linear combinations of 3-external integrals processed.
 number of sorted 3-external integrals      33390
 number of original 3-external integrals    18720


 putf34: external integral file complete. nfilw=    33 nfilx=    34 nrecw=     1 nrecx=     1 lbufp= 30006
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      6792         3     43132     43132      6792     51294
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      1272         4     49924     49924      1272     51294
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd        99         5     51196     51196        99     51294

 putf:       8 buffers of length    1536 written to file 13
 off-diagonal files sort completed.
 executing brd_struct for cisrtinfo
cisrtinfo:
bufszi  1536
 diagfile 4ext:     812 2ext:     336 0ext:      42
 fil4w,fil4x  :   24411 fil3w,fil3x :   18720
 ofdgint  2ext:    6792 1ext:    1272 0ext:      99so0ext:       0so1ext:       0so2ext:       0
buffer minbl4     381 minbl3     381 maxbl2     384nbas:  11   3   7   7   0   0   0   0 maxbuf 30006
 CIUDG version 5.9.7 ( 5-Oct-2004)

 workspace allocation information: lcore=  13107199

 core energy values from the integral file:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -4.349858765627E+01, ietype=    6,   fcore energy of type: H1(*)   

 total core repulsion energy = -4.349858765627E+01
 nmot  =    38 niot  =     6 nfct  =     4 nfvt  =     0
 nrow  =    32 nsym  =     4 ssym  =     4 lenbuf=  1600
 nwalk,xbar:        211      105       70       15       21
 nvalwt,nvalw:      130       24       70       15       21
 ncsft:            4042
 total number of valid internal walks:     130
 nvalz,nvaly,nvalx,nvalw =       24      70      15      21

 cisrt info file parameters:
 file number  12 blocksize   1536
 mxbld   1536
 nd4ext,nd2ext,nd0ext   812   336    42
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int    24411    18720     6792     1272       99        0        0        0
 minbl4,minbl3,maxbl2   381   381   384
 maxbuf 30006
 number of external orbitals per symmetry block:  11   3   7   7
 nmsym   4 number of internal orbitals   6
 executing brd_struct for drt
 executing brd_struct for orbinf
 executing brd_struct for momap
 calcthrxt: niot,maxw1=                     6                    31
 block size     0
 pthz,pthy,pthx,pthw:   105    70    15    21 total internal walks:     211
 maxlp3,n2lp,n1lp,n0lp    31     0     0     0
 orbsym(*)= 1 1 1 2 3 4

 setref:       24 references kept,
               81 references were marked as invalid, out of
              105 total.
 nmb.of records onel     1
 nmb.of records 2-ext     5
 nmb.of records 1-ext     1
 nmb.of records 0-ext     1
 nmb.of records 2-int     0
 nmb.of records 1-int     0
 nmb.of records 0-int     0
 ---------memory usage in DP -----------------
 < n-ex core usage >
     routines:
    fourex            61797
    threx             60199
    twoex              3549
    onex               1823
    allin              1536
    diagon             2109
               =======
   maximum            61797
 
  __ static summary __ 
   reflst                24
   hrfspc                24
               -------
   static->              24
 
  __ core required  __ 
   totstc                24
   max n-ex           61797
               -------
   totnec->           61821
 
  __ core available __ 
   totspc          13107199
   totnec -           61821
               -------
   totvec->        13045378

 number of external paths / symmetry
 vertex x     100      82      98      98
 vertex w     128      82      98      98
segment: free space=    13045378
 reducing frespc by                   774 to               13044604 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1          47|        24|         0|        24|         0|         1|
 -------------------------------------------------------------------------------
  Y 2          70|       490|        24|        70|        24|         2|
 -------------------------------------------------------------------------------
  X 3          15|      1414|       514|        15|        94|         3|
 -------------------------------------------------------------------------------
  W 4          21|      2114|      1928|        21|       109|         4|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=          60DP  conft+indsym=         280DP  drtbuffer=         434 DP

dimension of the ci-matrix ->>>      4042

 executing brd_struct for civct
 gentasklist: ntask=                    20
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS   
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      15      47       1414         24      15      24
     2  4   1    25      two-ext wz   2X  4 1      21      47       2114         24      21      24
     3  4   3    26      two-ext wx*  WX  4 3      21      15       2114       1414      21      15
     4  4   3    27      two-ext wx+  WX  4 3      21      15       2114       1414      21      15
     5  2   1    11      one-ext yz   1X  2 1      70      47        490         24      70      24
     6  3   2    15      1ex3ex yx    3X  3 2      15      70       1414        490      15      70
     7  4   2    16      1ex3ex yw    3X  4 2      21      70       2114        490      21      70
     8  1   1     1      allint zz    OX  1 1      47      47         24         24      24      24
     9  2   2     5      0ex2ex yy    OX  2 2      70      70        490        490      70      70
    10  3   3     6      0ex2ex xx*   OX  3 3      15      15       1414       1414      15      15
    11  3   3    18      0ex2ex xx+   OX  3 3      15      15       1414       1414      15      15
    12  4   4     7      0ex2ex ww*   OX  4 4      21      21       2114       2114      21      21
    13  4   4    19      0ex2ex ww+   OX  4 4      21      21       2114       2114      21      21
    14  2   2    42      four-ext y   4X  2 2      70      70        490        490      70      70
    15  3   3    43      four-ext x   4X  3 3      15      15       1414       1414      15      15
    16  4   4    44      four-ext w   4X  4 4      21      21       2114       2114      21      21
    17  1   1    75      dg-024ext z  OX  1 1      47      47         24         24      24      24
    18  2   2    76      dg-024ext y  OX  2 2      70      70        490        490      70      70
    19  3   3    77      dg-024ext x  OX  3 3      15      15       1414       1414      15      15
    20  4   4    78      dg-024ext w  OX  4 4      21      21       2114       2114      21      21
----------------------------------------------------------------------------------------------------
REDTASK #   1 TIME=  19.000 N=  1 (task/type/sgbra)=(   1/24/0) (
REDTASK #   2 TIME=  18.000 N=  1 (task/type/sgbra)=(   2/25/0) (
REDTASK #   3 TIME=  17.000 N=  1 (task/type/sgbra)=(   3/26/1) (
REDTASK #   4 TIME=  16.000 N=  1 (task/type/sgbra)=(   4/27/2) (
REDTASK #   5 TIME=  15.000 N=  1 (task/type/sgbra)=(   5/11/0) (
REDTASK #   6 TIME=  14.000 N=  1 (task/type/sgbra)=(   6/15/0) (
REDTASK #   7 TIME=  13.000 N=  1 (task/type/sgbra)=(   7/16/0) (
REDTASK #   8 TIME=  12.000 N=  1 (task/type/sgbra)=(   8/ 1/0) (
REDTASK #   9 TIME=  11.000 N=  1 (task/type/sgbra)=(   9/ 5/0) (
REDTASK #  10 TIME=  10.000 N=  1 (task/type/sgbra)=(  10/ 6/1) (
REDTASK #  11 TIME=   9.000 N=  1 (task/type/sgbra)=(  11/18/2) (
REDTASK #  12 TIME=   8.000 N=  1 (task/type/sgbra)=(  12/ 7/1) (
REDTASK #  13 TIME=   7.000 N=  1 (task/type/sgbra)=(  13/19/2) (
REDTASK #  14 TIME=   6.000 N=  1 (task/type/sgbra)=(  14/42/1) (
REDTASK #  15 TIME=   5.000 N=  1 (task/type/sgbra)=(  15/43/1) (
REDTASK #  16 TIME=   4.000 N=  1 (task/type/sgbra)=(  16/44/1) (
REDTASK #  17 TIME=   3.000 N=  1 (task/type/sgbra)=(  17/75/1) (
REDTASK #  18 TIME=   2.000 N=  1 (task/type/sgbra)=(  18/76/1) (
REDTASK #  19 TIME=   1.000 N=  1 (task/type/sgbra)=(  19/77/1) (
REDTASK #  20 TIME=   0.000 N=  1 (task/type/sgbra)=(  20/78/1) (
 initializing v-file: 1:                  4042

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         176 2x:           0 4x:           0
All internal counts: zz :         271 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 reference space has dimension      24
 dsyevx: computed roots 1 to    2(converged:   2)

    root           eigenvalues
    ----           ------------
       1         -46.2740792724
       2         -46.2533198521

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=                    24

         vector  1 from unit 11 written to unit 49 filename cirefv              

 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=    24)...
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              4042
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                    1
 residual norm convergence criteria:               0.001000

          starting bk iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         602 2x:         246 4x:         106
All internal counts: zz :         271 yy:           0 xx:           0 ww:           0
One-external counts: yz :        1072 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:          72 wz:         104 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1
   ht   1    -2.77549162

          calcsovref: tciref block   1

              civs   1
 refs   1    1.00000    

          calcsovref: scrb block   1

                ci   1
 civs   1    1.00000    

          calcsovref: sovref block   1

              v      1
 ref    1    1.00000    

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.2740792724  2.2204E-15  5.7232E-02  2.2790E-01  1.0000E-03
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.2740792724  2.2204E-15  5.7232E-02  2.2790E-01  1.0000E-03
 
diagon:itrnv=   2
 expansion vectors are not transformed.
 matrix-vector products are not transformed.

    2 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              4042
 number of initial trial vectors:                         2
 number of initial matrix-vector products:                1
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                   30
 residual norm convergence criteria:               0.001000

          starting ci iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         602 2x:         246 4x:         106
All internal counts: zz :         271 yy:         620 xx:          66 ww:         132
One-external counts: yz :        1072 yx:        1000 yw:        1210
Two-external counts: yy :         865 ww:         252 xx:         180 xz:          72 wz:         104 wx:         360
Three-ext.   counts: yx :         328 yw:         393

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2
   ht   1    -2.77549162
   ht   2    -0.05723221    -0.18689176

          calcsovref: tciref block   1

              civs   1       civs   2
 refs   1    1.00000       1.494216E-14

          calcsovref: scrb block   1

                ci   1         ci   2
 civs   1   0.968725      -0.248137    
 civs   2   0.827636        3.23108    

          calcsovref: sovref block   1

              v      1       v      2
 ref    1   0.968725      -0.248137    

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.96872490    -0.24813720

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.3229759636  4.8897E-02  2.4910E-03  4.5571E-02  1.0000E-03
 mr-sdci #  1  2    -45.5288384640  2.0303E+00  0.0000E+00  5.3026E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000999
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   2

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         602 2x:         246 4x:         106
All internal counts: zz :         271 yy:         620 xx:          66 ww:         132
One-external counts: yz :        1072 yx:        1000 yw:        1210
Two-external counts: yy :         865 ww:         252 xx:         180 xz:          72 wz:         104 wx:         360
Three-ext.   counts: yx :         328 yw:         393

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3
   ht   1    -2.77549162
   ht   2    -0.05723221    -0.18689176
   ht   3     0.00684081    -0.00265817    -0.00878124

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3
 refs   1    1.00000       1.494216E-14  -2.466007E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3
 civs   1   0.966060      -0.153722       0.211236    
 civs   2   0.866612        1.60805       -2.79073    
 civs   3    1.03493        13.5759        8.07104    

          calcsovref: sovref block   1

              v      1       v      2       v      3
 ref    1   0.963508      -0.187200       0.191333    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.96350802    -0.18720029     0.19133310

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1    -46.3255597033  2.5837E-03  3.8644E-04  1.7424E-02  1.0000E-03
 mr-sdci #  2  2    -45.7821948378  2.5336E-01  0.0000E+00  4.5522E-01  1.0000E-04
 mr-sdci #  2  3    -45.4394564553  1.9409E+00  0.0000E+00  5.2749E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   3

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         602 2x:         246 4x:         106
All internal counts: zz :         271 yy:         620 xx:          66 ww:         132
One-external counts: yz :        1072 yx:        1000 yw:        1210
Two-external counts: yy :         865 ww:         252 xx:         180 xz:          72 wz:         104 wx:         360
Three-ext.   counts: yx :         328 yw:         393

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4
   ht   1    -2.77549162
   ht   2    -0.05723221    -0.18689176
   ht   3     0.00684081    -0.00265817    -0.00878124
   ht   4    -0.00047732    -0.00334663    -0.00049141    -0.00142882

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4
 refs   1    1.00000       1.494216E-14  -2.466007E-03   1.934259E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4
 civs   1  -0.964046       0.152370       0.219158       3.106542E-02
 civs   2  -0.871143       -1.05004       -2.95545      -0.881297    
 civs   3   -1.16735       -11.3382        6.80607       -8.62869    
 civs   4  -0.862218       -20.5450        4.38949        32.4511    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4
 ref    1  -0.961334       0.176356       0.203224       5.862071E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1    -0.96133369     0.17635561     0.20322366     0.05862071

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1    -46.3258930006  3.3330E-04  7.2160E-05  7.9571E-03  1.0000E-03
 mr-sdci #  3  2    -45.9400181470  1.5782E-01  0.0000E+00  3.1404E-01  1.0000E-04
 mr-sdci #  3  3    -45.4405951804  1.1387E-03  0.0000E+00  5.3171E-01  1.0000E-04
 mr-sdci #  3  4    -45.3801766491  1.8816E+00  0.0000E+00  5.7325E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   4

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         602 2x:         246 4x:         106
All internal counts: zz :         271 yy:         620 xx:          66 ww:         132
One-external counts: yz :        1072 yx:        1000 yw:        1210
Two-external counts: yy :         865 ww:         252 xx:         180 xz:          72 wz:         104 wx:         360
Three-ext.   counts: yx :         328 yw:         393

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1    -2.77549162
   ht   2    -0.05723221    -0.18689176
   ht   3     0.00684081    -0.00265817    -0.00878124
   ht   4    -0.00047732    -0.00334663    -0.00049141    -0.00142882
   ht   5    -0.00437394     0.00074325     0.00041084    -0.00001416    -0.00027159

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1    1.00000       1.494216E-14  -2.466007E-03   1.934259E-04   1.570520E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 civs   1  -0.964547       9.796232E-02   0.281220      -3.701464E-03  -1.514330E-02
 civs   2  -0.871769      -0.785550       -2.93103      -0.838596      -0.912604    
 civs   3   -1.19051       -9.39923      -0.170152        12.6063       -3.55805    
 civs   4   -1.01386       -21.5681        6.88139       -10.2651        30.0817    
 civs   5   0.813255        32.4315       -48.4686        57.7678        37.9830    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1  -0.960530       0.167903       0.206850       5.395134E-02   5.910257E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1    -0.96052986     0.16790333     0.20685021     0.05395134     0.05910257

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1    -46.3259516884  5.8688E-05  6.9148E-06  2.4052E-03  1.0000E-03
 mr-sdci #  4  2    -46.0166523007  7.6634E-02  0.0000E+00  1.8387E-01  1.0000E-04
 mr-sdci #  4  3    -45.4576188473  1.7024E-02  0.0000E+00  5.6438E-01  1.0000E-04
 mr-sdci #  4  4    -45.4126790665  3.2502E-02  0.0000E+00  5.7344E-01  1.0000E-04
 mr-sdci #  4  5    -45.3694630102  1.8709E+00  0.0000E+00  6.2466E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   5

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         602 2x:         246 4x:         106
All internal counts: zz :         271 yy:         620 xx:          66 ww:         132
One-external counts: yz :        1072 yx:        1000 yw:        1210
Two-external counts: yy :         865 ww:         252 xx:         180 xz:          72 wz:         104 wx:         360
Three-ext.   counts: yx :         328 yw:         393

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1    -2.77549162
   ht   2    -0.05723221    -0.18689176
   ht   3     0.00684081    -0.00265817    -0.00878124
   ht   4    -0.00047732    -0.00334663    -0.00049141    -0.00142882
   ht   5    -0.00437394     0.00074325     0.00041084    -0.00001416    -0.00027159
   ht   6     0.00106741    -0.00012952     0.00003219     0.00001877     0.00000977    -0.00002828

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1    1.00000       1.494216E-14  -2.466007E-03   1.934259E-04   1.570520E-03  -3.836016E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1  -0.964263       0.107040      -3.553416E-02   0.264363      -0.119215       3.073910E-02
 civs   2  -0.871811      -0.675215        1.09208       -2.74548       0.190963       -1.24264    
 civs   3   -1.19304       -8.68418        4.34679        6.18401        10.7230       -3.76262    
 civs   4   -1.03050       -20.9978        2.29196      -0.666439       -9.48187        31.4846    
 civs   5   0.902538        36.6344        21.6292       -20.3614        72.4103        29.8289    
 civs   6   0.931953        67.8732        256.862        82.1679       -49.5863        33.6374    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.960460       0.155892      -0.110374       0.185487      -1.474858E-02   8.005128E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.96046043     0.15589209    -0.11037385     0.18548666    -0.01474858     0.08005128

 trial vector basis is being transformed.  new dimension:   2

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1    -46.3259581327  6.4443E-06  7.4551E-07  8.0428E-04  1.0000E-03
 mr-sdci #  5  2    -46.0363419548  1.9690E-02  0.0000E+00  1.5013E-01  1.0000E-04
 mr-sdci #  5  3    -45.7122979361  2.5468E-01  0.0000E+00  4.9509E-01  1.0000E-04
 mr-sdci #  5  4    -45.4244402518  1.1761E-02  0.0000E+00  5.5743E-01  1.0000E-04
 mr-sdci #  5  5    -45.4095151635  4.0052E-02  0.0000E+00  6.2219E-01  1.0000E-04
 mr-sdci #  5  6    -45.3665130357  1.8679E+00  0.0000E+00  6.0235E-01  1.0000E-04
 
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mr-sdci  convergence criteria satisfied after  5 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1    -46.3259581327  6.4443E-06  7.4551E-07  8.0428E-04  1.0000E-03
 mr-sdci #  5  2    -46.0363419548  1.9690E-02  0.0000E+00  1.5013E-01  1.0000E-04

####################CIUDGINFO####################

   ci vector at position   1 energy=  -46.325958132727

################END OF CIUDGINFO################

 
diagon:itrnv=   0
    1 of the   3 expansion vectors are transformed.
    1 of the   2 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
maximum overlap with reference    1(overlap= 0.96046)

 information on vector: 1 from unit 11 written to unit 48 filename civout              


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -46.3259581327

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     3    4    5   17   22   31

                                         symmetry   a1   a1   a1   a2   b1   b2 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1 -0.677502                        +-   +                    - 
 z*  1  1       2 -0.150035                        +-        +               - 
 z*  1  1       3 -0.601348                        +-             +     -      
 z*  1  1       4 -0.127584                        +    +-                   - 
 z*  1  1       5 -0.031523                        +     -   +               - 
 z*  1  1       6 -0.126344                        +     -        +     -      
 z*  1  1       7 -0.054696                        +    +     -              - 
 z*  1  1       8 -0.028016                        +    +          -    -      
 z*  1  1       9  0.063848                        +         +-              - 
 z*  1  1      10 -0.028014                        +          -   +     -      
 z*  1  1      11  0.126492                        +         +     -    -      
 z*  1  1      12 -0.084678                        +              +-         - 
 z*  1  1      13 -0.084665                        +                   +-    - 
 z*  1  1      16 -0.044493                             +    +-              - 
 z*  1  1      17 -0.015450                             +     -   +     -      
 z*  1  1      18  0.018311                             +    +     -    -      
 z*  1  1      19 -0.025361                             +         +-         - 
 z*  1  1      20 -0.019776                             +              +-    - 
 z*  1  1      21  0.049999                                  +-   +     -      
 z*  1  1      23 -0.017632                                  +         +-    - 
 z*  1  1      24  0.026523                                       +     -   +- 
 y   1  1      26  0.018831              2( b2 )   +-    -                     
 y   1  1      28 -0.014841              4( b2 )   +-    -                     
 y   1  1      40  0.016721              2( b1 )   +-              -           
 y   1  1      42 -0.013173              4( b1 )   +-              -           
 y   1  1      46  0.016725              1( a2 )   +-                   -      
 y   1  1      47 -0.013174              2( a2 )   +-                   -      
 y   1  1      51  0.017484              3( a1 )   +-                        - 
 y   1  1      53  0.058582              5( a1 )   +-                        - 
 y   1  1      55 -0.012938              7( a1 )   +-                        - 
 y   1  1      61  0.012008              2( b2 )    -   +-                     
 y   1  1      63 -0.014952              4( b2 )    -   +-                     
 y   1  1      88  0.022409              5( a1 )    -   +                    - 
 y   1  1     103 -0.010306              2( b1 )    -        +     -           
 y   1  1     105  0.010366              4( b1 )    -        +     -           
 y   1  1     109  0.013327              1( a2 )    -        +          -      
 y   1  1     110 -0.013399              2( a2 )    -        +          -      
 y   1  1     126 -0.010302              4( b2 )    -             +-           
 y   1  1     132 -0.012650              3( a1 )    -             +     -      
 y   1  1     134  0.019903              5( a1 )    -             +     -      
 y   1  1     136  0.011958              7( a1 )    -             +     -      
 y   1  1     147 -0.010299              4( b2 )    -                  +-      
 y   1  1     180 -0.011858              2( a2 )   +     -              -      
 y   1  1     184  0.010555              3( a1 )   +     -                   - 
 y   1  1     188 -0.015159              7( a1 )   +     -                   - 
 y   1  1     219 -0.010608              6( a1 )   +               -    -      
 y   1  1     226 -0.010950              2( a2 )   +               -         - 
 y   1  1     231 -0.010947              4( b1 )   +                    -    - 
 y   1  1     348  0.010017              6( a1 )        +     -              - 
 y   1  1     366 -0.010005              2( a2 )        +          -         - 
 y   1  1     404  0.012333              2( a1 )              -   +     -      
 y   1  1     408  0.013829              6( a1 )              -   +     -      
 x   1  1     517  0.010116    3( a2 )   1( b1 )    -    -                     
 x   1  1     523  0.010076    3( a2 )   3( b1 )    -    -                     
 x   1  1     545  0.012121   10( a1 )   1( b2 )    -    -                     
 x   1  1     551  0.016390    5( a1 )   2( b2 )    -    -                     
 x   1  1     567  0.012602   10( a1 )   3( b2 )    -    -                     
 x   1  1     573 -0.016597    5( a1 )   4( b2 )    -    -                     
 x   1  1     591 -0.013094    1( a1 )   6( b2 )    -    -                     
 x   1  1     594 -0.013777    4( a1 )   6( b2 )    -    -                     
 x   1  1     719 -0.010122    9( a1 )   1( b1 )    -              -           
 x   1  1     726  0.014554    5( a1 )   2( b1 )    -              -           
 x   1  1     741 -0.010561    9( a1 )   3( b1 )    -              -           
 x   1  1     748 -0.014739    5( a1 )   4( b1 )    -              -           
 x   1  1     766 -0.012606    1( a1 )   6( b1 )    -              -           
 x   1  1     769 -0.012976    4( a1 )   6( b1 )    -              -           
 x   1  1     813  0.014554    5( a1 )   1( a2 )    -                   -      
 x   1  1     824 -0.014740    5( a1 )   2( a2 )    -                   -      
 x   1  1     848 -0.012046    7( b1 )   1( b2 )    -                   -      
 x   1  1     862 -0.012399    7( b1 )   3( b2 )    -                   -      
 x   1  1     884 -0.011491    1( b1 )   7( b2 )    -                   -      
 x   1  1     886 -0.011866    3( b1 )   7( b2 )    -                   -      
 x   1  1     899 -0.015235    3( a1 )   5( a1 )    -                        - 
 x   1  1     910 -0.014493    5( a1 )   7( a1 )    -                        - 
 x   1  1     919 -0.010426    1( a1 )   9( a1 )    -                        - 
 x   1  1     922 -0.010592    4( a1 )   9( a1 )    -                        - 
 x   1  1     961 -0.010013    3( b1 )   6( b1 )    -                        - 
 x   1  1     964  0.011226    1( b1 )   7( b1 )    -                        - 
 x   1  1     966  0.011558    3( b1 )   7( b1 )    -                        - 
 x   1  1     985  0.011563    1( b2 )   7( b2 )    -                        - 
 x   1  1     987  0.011864    3( b2 )   7( b2 )    -                        - 
 x   1  1    1012 -0.010966    1( a1 )   1( b2 )         -    -                
 x   1  1    1500  0.010414    1( b1 )   1( b2 )              -         -      
 w   1  1    1933 -0.012476    2( a2 )   2( b1 )   +-                          
 w   1  1    1938 -0.012476    1( a2 )   4( b1 )   +-                          
 w   1  1    1939  0.021305    2( a2 )   4( b1 )   +-                          
 w   1  1    1946  0.017036    3( a2 )   6( b1 )   +-                          
 w   1  1    1949 -0.019665    3( a2 )   7( b1 )   +-                          
 w   1  1    1950  0.027659    1( a1 )   1( b2 )   +-                          
 w   1  1    1953  0.025647    4( a1 )   1( b2 )   +-                          
 w   1  1    1957  0.010364    8( a1 )   1( b2 )   +-                          
 w   1  1    1963  0.010123    3( a1 )   2( b2 )   +-                          
 w   1  1    1967 -0.012266    7( a1 )   2( b2 )   +-                          
 w   1  1    1972  0.025646    1( a1 )   3( b2 )   +-                          
 w   1  1    1975  0.025035    4( a1 )   3( b2 )   +-                          
 w   1  1    1979  0.011810    8( a1 )   3( b2 )   +-                          
 w   1  1    1985 -0.013060    3( a1 )   4( b2 )   +-                          
 w   1  1    1988  0.012842    6( a1 )   4( b2 )   +-                          
 w   1  1    1989  0.020946    7( a1 )   4( b2 )   +-                          
 w   1  1    1994 -0.010364    1( a1 )   5( b2 )   +-                          
 w   1  1    1997 -0.011811    4( a1 )   5( b2 )   +-                          
 w   1  1    2013  0.012868    9( a1 )   6( b2 )   +-                          
 w   1  1    2014  0.014785   10( a1 )   6( b2 )   +-                          
 w   1  1    2025 -0.018410   10( a1 )   7( b2 )   +-                          
 w   1  1    2048 -0.034000    1( a1 )   1( b2 )   +     -                     
 w   1  1    2051 -0.025306    4( a1 )   1( b2 )   +     -                     
 w   1  1    2063 -0.010013    5( a1 )   2( b2 )   +     -                     
 w   1  1    2070 -0.025567    1( a1 )   3( b2 )   +     -                     
 w   1  1    2073 -0.020567    4( a1 )   3( b2 )   +     -                     
 w   1  1    2079  0.010184   10( a1 )   3( b2 )   +     -                     
 w   1  1    2103  0.010073    1( a1 )   6( b2 )   +     -                     
 w   1  1    2106  0.010955    4( a1 )   6( b2 )   +     -                     
 w   1  1    2223 -0.030178    1( a1 )   1( b1 )   +               -           
 w   1  1    2226 -0.022878    4( a1 )   1( b1 )   +               -           
 w   1  1    2245 -0.022277    1( a1 )   3( b1 )   +               -           
 w   1  1    2248 -0.018256    4( a1 )   3( b1 )   +               -           
 w   1  1    2354 -0.030187    1( b1 )   1( b2 )   +                    -      
 w   1  1    2356 -0.022283    3( b1 )   1( b2 )   +                    -      
 w   1  1    2368 -0.022885    1( b1 )   3( b2 )   +                    -      
 w   1  1    2370 -0.018260    3( b1 )   3( b2 )   +                    -      
 w   1  1    2403 -0.022786    1( a1 )   1( a1 )   +                         - 
 w   1  1    2409 -0.020653    1( a1 )   4( a1 )   +                         - 
 w   1  1    2412 -0.010572    4( a1 )   4( a1 )   +                         - 
 w   1  1    2475  0.019736    1( b1 )   1( b1 )   +                         - 
 w   1  1    2478  0.024372    1( b1 )   3( b1 )   +                         - 
 w   1  1    2480  0.015173    3( b1 )   3( b1 )   +                         - 
 w   1  1    2503 -0.022782    1( b2 )   1( b2 )   +                         - 
 w   1  1    2506 -0.020645    1( b2 )   3( b2 )   +                         - 
 w   1  1    2508 -0.010567    3( b2 )   3( b2 )   +                         - 
 w   1  1    2552 -0.016270    1( a1 )   1( b2 )        +-                     
 w   1  1    2555 -0.011261    4( a1 )   1( b2 )        +-                     
 w   1  1    2574 -0.011021    1( a1 )   3( b2 )        +-                     
 w   1  1    2907  0.055668    1( a1 )   1( a1 )        +                    - 
 w   1  1    2913  0.048267    1( a1 )   4( a1 )        +                    - 
 w   1  1    2916  0.024685    4( a1 )   4( a1 )        +                    - 
 w   1  1    2921  0.016586    5( a1 )   5( a1 )        +                    - 
 w   1  1    2979  0.043349    1( b1 )   1( b1 )        +                    - 
 w   1  1    2982  0.037354    1( b1 )   3( b1 )        +                    - 
 w   1  1    2984  0.019292    3( b1 )   3( b1 )        +                    - 
 w   1  1    3007  0.057113    1( b2 )   1( b2 )        +                    - 
 w   1  1    3010  0.049670    1( b2 )   3( b2 )        +                    - 
 w   1  1    3012  0.025414    3( b2 )   3( b2 )        +                    - 
 w   1  1    3133  0.010002    1( a1 )   1( b1 )             +     -           
 w   1  1    3264 -0.012933    1( b1 )   1( b2 )             +          -      
 w   1  1    3313  0.015744    1( a1 )   1( a1 )             +               - 
 w   1  1    3319  0.014000    1( a1 )   4( a1 )             +               - 
 w   1  1    3462 -0.011784    1( a1 )   1( b2 )                  +-           
 w   1  1    3539  0.051715    1( a1 )   1( a1 )                  +     -      
 w   1  1    3545  0.045073    1( a1 )   4( a1 )                  +     -      
 w   1  1    3548  0.023073    4( a1 )   4( a1 )                  +     -      
 w   1  1    3553  0.014728    5( a1 )   5( a1 )                  +     -      
 w   1  1    3611  0.035194    1( b1 )   1( b1 )                  +     -      
 w   1  1    3614  0.029970    1( b1 )   3( b1 )                  +     -      
 w   1  1    3616  0.015472    3( b1 )   3( b1 )                  +     -      
 w   1  1    3639  0.051719    1( b2 )   1( b2 )                  +     -      
 w   1  1    3642  0.045076    1( b2 )   3( b2 )                  +     -      
 w   1  1    3644  0.023074    3( b2 )   3( b2 )                  +     -      
 w   1  1    3770 -0.011778    1( a1 )   1( b2 )                       +-      

 ci coefficient statistics:
           rq > 0.1                6
      0.1> rq > 0.01             152
     0.01> rq > 0.001            863
    0.001> rq > 0.0001           743
   0.0001> rq > 0.00001          204
  0.00001> rq > 0.000001          26
 0.000001> rq                   2048
           all                  4042
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         176 2x:           0 4x:           0
All internal counts: zz :         271 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


  iref  icsf         v(icsf)             hv(icsf)
     1     1     -0.677502496397     31.351054096603
     2     2     -0.150034806548      6.942781600736
     3     3     -0.601347533094     27.827020398959
     4     4     -0.127584018266      5.902995749164
     5     5     -0.031522896214      1.458769875050
     6     6     -0.126343536106      5.846709870023
     7     7     -0.054695632422      2.531106564765
     8     8     -0.028016318961      1.296490958654
     9     9      0.063847787667     -2.955770007418
    10    10     -0.028014146854      1.296387332029
    11    11      0.126491818740     -5.853564035793
    12    12     -0.084678281395      3.917482639348
    13    13     -0.084664533911      3.916845461405
    14    14      0.009858290641     -0.455847181337
    15    15      0.003117812768     -0.143899738275
    16    16     -0.044493041580      2.057361030155
    17    17     -0.015450202666      0.714532843174
    18    18      0.018311338770     -0.846847072930
    19    19     -0.025361191491      1.172584836284
    20    20     -0.019775646541      0.914263151203
    21    21      0.049999284565     -2.312048103062
    22    22      0.007645217957     -0.353633458398
    23    23     -0.017631750846      0.815351942303
    24    24      0.026522767262     -1.226330935738

 number of reference csfs (nref) is    24.  root number (iroot) is  1.
 c0**2 =   0.92261706  c**2 (all zwalks) =   0.92261706

 pople ci energy extrapolation is computed with  4 correlated electrons.

 eref      =    -46.274037273244   "relaxed" cnot**2         =   0.922617059931
 eci       =    -46.325958132727   deltae = eci - eref       =  -0.051920859483
 eci+dv1   =    -46.329975921485   dv1 = (1-cnot**2)*deltae  =  -0.004017788758
 eci+dv2   =    -46.330312906698   dv2 = dv1 / cnot**2       =  -0.004354773971
 eci+dv3   =    -46.330711595305   dv3 = dv1 / (2*cnot**2-1) =  -0.004753462577
 eci+pople =    -46.328131703790   (  4e- scaled deltae )    =  -0.054094430546
 passed aftci ... 
 readint2: molcas,dalton2=                     0                     0
 files%faoints=aoints              
lodens (list->root)=  1
sifcfg setup: record length 4096 DP
# d1 elements per record  3272
# d2 elements per record  2730
  The MR-CISD density will be calculated.
 item #                     1 suffix=:.drt1.state1:
 read_civout: repnuc=  -43.4985876562748     
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method:  0 last record  1max overlap with ref# 96% root-following 0
 MR-CISD energy:   -46.32595813    -2.82737048
 residuum:     0.00080428
 deltae:     0.00000644
 apxde:     0.00000075

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.96046043     0.15589209    -0.11037385     0.18548666    -0.01474858     0.08005128     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.96046043     0.15589209     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=     106  DYX=     130  DYW=     160
   D0Z=      34  D0Y=     103  D0X=      12  D0W=      18
  DDZI=      80 DDYI=     180 DDXI=      30 DDWI=      36
  DDZE=       0 DDYE=      70 DDXE=      15 DDWE=      21
================================================================================
Trace of MO density:     4.000000
    4  correlated and     8  frozen core electrons

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     1.81033095     0.20154513     0.04473910     0.00000000    -0.00902023     0.01955413
   MO   4     0.00000000     0.00000000     0.20154513     0.54520820     0.11119464     0.00000000    -0.00828544     0.01578633
   MO   5     0.00000000     0.00000000     0.04473910     0.11119464     0.06776371     0.00000000     0.00205657     0.00529394
   MO   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.02608900     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000    -0.00902023    -0.00828544     0.00205657     0.00000000     0.00084501    -0.00039675
   MO   8     0.00000000     0.00000000     0.01955413     0.01578633     0.00529394     0.00000000    -0.00039675     0.00152119
   MO   9     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.01831023     0.00000000     0.00000000
   MO  10     0.00000000     0.00000000    -0.03929573     0.03379886     0.00748301     0.00000000    -0.00053884     0.00116776
   MO  11     0.00000000     0.00000000    -0.00921315    -0.00902819     0.00265930     0.00000000     0.00100887    -0.00058049
   MO  12     0.00000000     0.00000000    -0.01503039    -0.01251251    -0.00562831     0.00000000     0.00027490    -0.00157907
   MO  13     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00397953     0.00000000     0.00000000
   MO  14     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00025417     0.00000000     0.00000000
   MO  15     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00226184     0.00000000     0.00000000
   MO  16     0.00000000     0.00000000     0.00036573    -0.00153179    -0.00033920     0.00000000     0.00002910    -0.00006306

                MO   9         MO  10         MO  11         MO  12         MO  13         MO  14         MO  15         MO  16
   MO   3     0.00000000    -0.03929573    -0.00921315    -0.01503039     0.00000000     0.00000000     0.00000000     0.00036573
   MO   4     0.00000000     0.03379886    -0.00902819    -0.01251251     0.00000000     0.00000000     0.00000000    -0.00153179
   MO   5     0.00000000     0.00748301     0.00265930    -0.00562831     0.00000000     0.00000000     0.00000000    -0.00033920
   MO   6     0.01831023     0.00000000     0.00000000     0.00000000     0.00397953    -0.00025417    -0.00226184     0.00000000
   MO   7     0.00000000    -0.00053884     0.00100887     0.00027490     0.00000000     0.00000000     0.00000000     0.00002910
   MO   8     0.00000000     0.00116776    -0.00058049    -0.00157907     0.00000000     0.00000000     0.00000000    -0.00006306
   MO   9     0.01327625     0.00000000     0.00000000     0.00000000     0.00321535    -0.00002703    -0.00179703     0.00000000
   MO  10     0.00000000     0.00849635    -0.00057584    -0.00093934     0.00000000     0.00000000     0.00000000    -0.00037990
   MO  11     0.00000000    -0.00057584     0.00130563     0.00053140     0.00000000     0.00000000     0.00000000     0.00003437
   MO  12     0.00000000    -0.00093934     0.00053140     0.00184589     0.00000000     0.00000000     0.00000000     0.00005607
   MO  13     0.00321535     0.00000000     0.00000000     0.00000000     0.00106308     0.00005907    -0.00062720     0.00000000
   MO  14    -0.00002703     0.00000000     0.00000000     0.00000000     0.00005907     0.00122032     0.00047060     0.00000000
   MO  15    -0.00179703     0.00000000     0.00000000     0.00000000    -0.00062720     0.00047060     0.00186817     0.00000000
   MO  16     0.00000000    -0.00037990     0.00003437     0.00005607     0.00000000     0.00000000     0.00000000     0.00002207

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     2.00000000     1.84492163     0.54016703     0.04421693     0.03998384     0.00506936     0.00233806
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     0.00195226     0.00109535     0.00054718     0.00047150     0.00006123     0.00001457     0.00001389     0.00000302

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4
   MO   1     0.43872100     0.01476586    -0.01317932     0.00000000
   MO   2     0.01476586     0.00144830    -0.00156362     0.00000000
   MO   3    -0.01317932    -0.00156362     0.00187713     0.00000000
   MO   4     0.00000000     0.00000000     0.00000000     0.00207269

Natural orbital populations,block 2
              MO     1       MO     2       MO     3       MO     4       MO
  occ(*)=     0.43961858     0.00236254     0.00207269     0.00006531

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     0.43871807     0.00000000     0.01476234     0.00000000    -0.01317723     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.01568124     0.00000000     0.01124035     0.00000000     0.00239452    -0.00088504
   MO   4     0.00000000     0.01476234     0.00000000     0.00144768     0.00000000    -0.00156321     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.01124035     0.00000000     0.00845273     0.00000000     0.00206646    -0.00056825
   MO   6     0.00000000    -0.01317723     0.00000000    -0.00156321     0.00000000     0.00187689     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000     0.00239452     0.00000000     0.00206646     0.00000000     0.00070305    -0.00014578
   MO   8     0.00000000     0.00000000    -0.00088504     0.00000000    -0.00056825     0.00000000    -0.00014578     0.00139759
   MO   9     0.00000000     0.00000000     0.00102201     0.00000000     0.00065621     0.00000000     0.00016836    -0.00057733

                MO   9
   MO   2     0.00000000
   MO   3     0.00102201
   MO   4     0.00000000
   MO   5     0.00065621
   MO   6     0.00000000
   MO   7     0.00016836
   MO   8    -0.00057733
   MO   9     0.00156382

Natural orbital populations,block 3
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     0.43961528     0.02441264     0.00236209     0.00195227     0.00089743     0.00052571     0.00006526
              MO     9       MO
  occ(*)=     0.00001040

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     0.55757674     0.00000000     0.01758197     0.00000000    -0.01535180     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.02609203     0.00000000     0.01831203     0.00000000    -0.00397945    -0.00210471
   MO   4     0.00000000     0.01758197     0.00000000     0.00178492     0.00000000    -0.00189182     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.01831203     0.00000000     0.01327727     0.00000000    -0.00321523    -0.00172032
   MO   6     0.00000000    -0.01535180     0.00000000    -0.00189182     0.00000000     0.00230161     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000    -0.00397945     0.00000000    -0.00321523     0.00000000     0.00106300     0.00061933
   MO   8     0.00000000     0.00000000    -0.00210471     0.00000000    -0.00172032     0.00000000     0.00061933     0.00157034
   MO   9     0.00000000     0.00000000     0.00086612     0.00000000     0.00051992     0.00000000    -0.00011550    -0.00057083

                MO   9
   MO   2     0.00000000
   MO   3     0.00086612
   MO   4     0.00000000
   MO   5     0.00051992
   MO   6     0.00000000
   MO   7    -0.00011550
   MO   8    -0.00057083
   MO   9     0.00151807

Natural orbital populations,block 4
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     0.55855894     0.03998775     0.00299420     0.00195239     0.00109536     0.00047132     0.00011012
              MO     9       MO
  occ(*)=     0.00001389


 total number of electrons =   12.0000000000

 test slabel:                     4
  a1  a2  b1  b2 


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        a1  partial gross atomic populations
   ao class       1a1        2a1        3a1        4a1        5a1        6a1 
     3_ s       2.000000   0.000000   1.797386   0.016659   0.000000   0.000000
     3_ p       0.000000   2.000000   0.000000   0.000000   0.000000   0.039745
     3_ d       0.000000   0.000000   0.047535   0.523508   0.044217   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000000   0.000000   0.000239
 
   ao class       7a1        8a1        9a1       10a1       11a1       12a1 
     3_ s       0.004599   0.000201   0.000000   0.000000   0.000000   0.000000
     3_ p       0.000000   0.000000   0.000044   0.000283   0.000000   0.000342
     3_ d       0.000470   0.002137   0.000000   0.000000   0.000547   0.000000
     3_ f       0.000000   0.000000   0.001908   0.000812   0.000000   0.000129
 
   ao class      13a1       14a1       15a1       16a1 
     3_ s       0.000001   0.000000   0.000000   0.000003
     3_ p       0.000000   0.000000   0.000014   0.000000
     3_ d       0.000060   0.000015   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000000

                        a2  partial gross atomic populations
   ao class       1a2        2a2        3a2        4a2 
     3_ d       0.439619   0.002363   0.000000   0.000065
     3_ f       0.000000   0.000000   0.002073   0.000000

                        b1  partial gross atomic populations
   ao class       1b1        2b1        3b1        4b1        5b1        6b1 
     3_ p       2.000000   0.000000   0.024285   0.000000   0.000017   0.000000
     3_ d       0.000000   0.439615   0.000000   0.002362   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000127   0.000000   0.001936   0.000897
 
   ao class       7b1        8b1        9b1 
     3_ p       0.000525   0.000000   0.000010
     3_ d       0.000000   0.000065   0.000000
     3_ f       0.000001   0.000000   0.000000

                        b2  partial gross atomic populations
   ao class       1b2        2b2        3b2        4b2        5b2        6b2 
     3_ p       2.000000   0.000000   0.039749   0.000000   0.000044   0.000284
     3_ d       0.000000   0.558559   0.000000   0.002994   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000239   0.000000   0.001908   0.000812
 
   ao class       7b2        8b2        9b2 
     3_ p       0.000342   0.000000   0.000014
     3_ d       0.000000   0.000110   0.000000
     3_ f       0.000129   0.000000   0.000000


                        gross atomic populations
     ao            3_
      s         3.818849
      p         6.105698
      d         2.064242
      f         0.011211
    total      12.000000
 

 Total number of electrons:   12.00000000

 DA ...
