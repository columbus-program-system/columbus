
 program cipc      

 print the csf info for mrsdci wave functions

 written by: ron shepard

 version date: 06-jun-96

 This Version of Program cipc is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 workspace allocation parameters: lencor=  13107200 mem1=         0 ifirst=         1

 drt header information:
  cidrt_title                                                                    
 nmot  =    38 niot  =     6 nfct  =     4 nfvt  =     0
 nrow  =    23 nsym  =     4 ssym  =     4 lenbuf=  1600
 spnorb=     F spnodd=     F lxyzir(1:3)= 0 0 0
 nwalk,xbar:         50       15       20       15        0
 nvalwt,nvalw:       39        4       20       15        0
 ncsft:            1558
 map(*)=    -1 -1 29 30 31  1  2  3  4  5  6  7  8  9 10 11 32 12 13 14
            -1 33 15 16 17 18 19 20 21 -1 34 22 23 24 25 26 27 28
 mu(*)=      0  0  0  0  0  0
 syml(*) =   1  1  1  2  3  4
 rmo(*)=     3  4  5  1  2  2

 indx01:    39 indices saved in indxv(*)
 test nroots froot                      1                     1
===================================ROOT # 1===================================

 rdhciv: CI vector file information:
  cidrt_title                                                                    
 energy computed by program ciudg.       zam792            17:53:53.469 17-Dec-13

 lenrec =   32768 lenci =      1558 ninfo =  6 nenrgy =  6 ntitle =  2

 Max. overlap with ref vector #        1
 Valid ci vector #        1
 Method:        0       96% overlap
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -4.349858765627E+01, ietype=    6,   fcore energy of type: H1(*)   
 energy( 3)= -4.629138778849E+01, ietype=-1026,   total energy of type: MRSDCI  
 energy( 4)=  2.984105807517E-04, ietype=-2055, cnvginf energy of type: CI-Resid
 energy( 5)=  3.099565148545E-06, ietype=-2056, cnvginf energy of type: CI-D.E. 
 energy( 6)=  1.099371421583E-07, ietype=-2057, cnvginf energy of type: CI-ApxDE
==================================================================================
space sufficient for valid walk range           1         39
               respectively csf range           1       1558

 space is available for   4357921 coefficients.

 updated histogram parameters:
 csfmn = 0.0000E+00 csfmx = 1.0000E+00 fhist = 5.0000E-01 nhist =  20

 this program will print the csfs generated from
 the drt according to the following print options :

 1) run in batch mode: all valid roots are automatically
    analysed and csf info is printed by default contribution
    threshold 0.01 
 2) run in interactive mode
 3) generate files for cioverlap without symmetry
 4) generate files for cioverlap with symmetry

 input menu number [  1]:
================================================================================
===================================VECTOR # 1===================================
================================================================================


 rdcivnew:      94 coefficients were selected.
 workspace: ncsfmx=    1558
 ncsfmx=                  1558

 histogram parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =    1558 ncsft =    1558 ncsf =      94
 nhist =  20 fhist = 0.50000

    cmin                cmax        num  '*'=     2 csfs.
 ----------          ----------   ----- ---------|---------|---------|---------|
 5.0000E-01 <= |c| < 1.0000E+00       1 *
 2.5000E-01 <= |c| < 5.0000E-01       1 *
 1.2500E-01 <= |c| < 2.5000E-01       0
 6.2500E-02 <= |c| < 1.2500E-01       6 ***
 3.1250E-02 <= |c| < 6.2500E-02       7 ****
 1.5625E-02 <= |c| < 3.1250E-02      30 ***************
 7.8125E-03 <= |c| < 1.5625E-02      49 *************************
 3.9062E-03 <= |c| < 7.8125E-03       0
 1.9531E-03 <= |c| < 3.9062E-03       0
 9.7656E-04 <= |c| < 1.9531E-03       0
 4.8828E-04 <= |c| < 9.7656E-04       0
 2.4414E-04 <= |c| < 4.8828E-04       0
 1.2207E-04 <= |c| < 2.4414E-04       0
 6.1035E-05 <= |c| < 1.2207E-04       0
 3.0518E-05 <= |c| < 6.1035E-05       0
 1.5259E-05 <= |c| < 3.0518E-05       0
 7.6294E-06 <= |c| < 1.5259E-05       0
 3.8147E-06 <= |c| < 7.6294E-06       0
 0.0000E+00 <= |c| < 3.8147E-06       0
                                  ----- ---------|---------|---------|---------|
                  total read =       94 total stored =      94

 from the selected csfs,
 min(|csfvec(:)|) = 1.0232E-02    max(|csfvec(:)|) = 8.6586E-01
 norm=  0.999999999999998     
 csfs will be printed based on coefficient magnitudes.

 current csfvec(*) selection parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =    1558 ncsft =    1558 ncsf =      94

 i:slabel(i) =  1: a1   2: a2   3: b1   4: b2 
 
 frozen orbital =    1    2    3    4
 symfc(*)       =    1    1    3    4
 label          =  a1   a1   b1   b2 
 rmo(*)         =    1    2    1    1
 
 internal level =    1    2    3    4    5    6
 syml(*)        =    1    1    1    2    3    4
 label          =  a1   a1   a1   a2   b1   b2 
 rmo(*)         =    3    4    5    1    2    2

 printing selected csfs in sorted order from cmin = 0.00000 to cmax = 1.00000

   indcsf     c     c**2   v  lab:rmo  lab:rmo   step(*)
  ------- -------- ------- - ---- --- ---- --- ------------
          3 -0.86586 0.74971 z*                    101110
          1  0.42007 0.17646 z*                    111001
         40 -0.09523 0.00907 y           a2 :  2  1101010
         34  0.08314 0.00691 y           b1 :  4  1101100
         41  0.06945 0.00482 y           a2 :  3  1101010
         55  0.06702 0.00449 y           a1 :  7  1100110
         36 -0.06426 0.00413 y           b1 :  6  1101100
         56  0.06336 0.00401 y           a1 :  8  1100110
         60 -0.05074 0.00257 y           a1 : 12  1100110
          6 -0.04751 0.00226 y           b2 :  4  1111000
         59  0.04541 0.00206 y           a1 : 11  1100110
         23  0.04185 0.00175 y           a1 :  7  1110001
        472 -0.03997 0.00160 x  b1 :  3  b2 :  3 11100010
          8  0.03427 0.00117 y           b2 :  6  1111000
         45 -0.03396 0.00115 y           a1 :  8  1101001
        486 -0.03024 0.00091 x  b1 :  3  b2 :  5 11100010
        474 -0.03010 0.00091 x  b1 :  5  b2 :  3 11100010
         24  0.02788 0.00078 y           a1 :  8  1110001
        520 -0.02684 0.00072 x  b1 :  9  b2 :  9 11100010
        166  0.02653 0.00070 x  a1 :  6  b2 :  3 11110000
         27  0.02641 0.00070 y           a1 : 11  1110001
        488 -0.02389 0.00057 x  b1 :  5  b2 :  5 11100010
         49  0.02389 0.00057 y           a1 : 12  1101001
         28 -0.02364 0.00056 y           a1 : 12  1110001
          2 -0.02288 0.00052 z*                    110110
        264 -0.02270 0.00052 x  a1 :  6  b2 :  3 11101000
       1101 -0.02028 0.00041 x  a1 : 10  a2 :  2 11001010
        188  0.02015 0.00041 x  a1 :  6  b2 :  5 11110000
        169  0.01987 0.00039 x  a1 :  9  b2 :  3 11110000
        253  0.01965 0.00039 x  a2 :  3  b1 :  6 11101000
       1014  0.01958 0.00038 x  a1 : 10  b1 :  4 11001100
        263  0.01910 0.00036 x  a2 :  4  b1 :  9 11101000
       1054 -0.01805 0.00033 x  a1 :  6  b1 :  8 11001100
       1136  0.01739 0.00030 x  b1 :  9  b2 :  3 11001010
        286 -0.01714 0.00029 x  a1 :  6  b2 :  5 11101000
        267 -0.01713 0.00029 x  a1 :  9  b2 :  3 11101000
        404  0.01672 0.00028 x  a1 : 14  b1 :  8 11100100
       1172  0.01664 0.00028 x  b1 :  3  b2 :  9 11001010
       1057 -0.01649 0.00027 x  a1 :  9  b1 :  8 11001100
        456  0.01603 0.00026 x  a1 : 12  a2 :  3 11100010
       1150  0.01601 0.00026 x  b1 :  9  b2 :  5 11001010
         48  0.01587 0.00025 y           a1 : 11  1101001
        339  0.01585 0.00025 x  a1 : 15  b2 :  9 11101000
        341  0.01585 0.00025 x  a1 :  6  b1 :  3 11100100
        191  0.01584 0.00025 x  a1 :  9  b2 :  5 11110000
       1286 -0.01539 0.00024 x  a1 :  7  a1 : 10 11000110
       1174  0.01534 0.00024 x  b1 :  5  b2 :  9 11001010
       1036 -0.01523 0.00023 x  a1 : 10  b1 :  6 11001100
       1112  0.01492 0.00022 x  a1 : 10  a2 :  3 11001010
        246  0.01447 0.00021 x  a2 :  2  b1 :  4 11101000
        379  0.01415 0.00020 x  a1 : 11  b1 :  6 11100100
        252 -0.01378 0.00019 x  a2 :  2  b1 :  6 11101000
       1078  0.01378 0.00019 x  a2 :  4  b2 :  3 11001100
       1119  0.01363 0.00019 x  a1 :  6  a2 :  4 11001010
         13 -0.01359 0.00018 y           b1 :  4  1110100
        289 -0.01357 0.00018 x  a1 :  9  b2 :  5 11101000
       1007 -0.01356 0.00018 x  a1 : 14  b1 :  3 11001100
        247 -0.01332 0.00018 x  a2 :  3  b1 :  4 11101000
        455 -0.01321 0.00017 x  a1 : 11  a2 :  3 11100010
        230 -0.01320 0.00017 x  a1 : 15  b2 :  8 11110000
       1373  0.01311 0.00017 x  b2 :  3  b2 :  9 11000110
        380 -0.01304 0.00017 x  a1 : 12  b1 :  6 11100100
       1315 -0.01303 0.00017 x  a1 :  6  a1 : 15 11000110
       1287 -0.01290 0.00017 x  a1 :  8  a1 : 10 11000110
       1084  0.01276 0.00016 x  a2 :  4  b2 :  5 11001100
       1368  0.01259 0.00016 x  b2 :  3  b2 :  8 11000110
       1029 -0.01258 0.00016 x  a1 : 14  b1 :  5 11001100
       1122  0.01258 0.00016 x  a1 :  9  a2 :  4 11001010
       1352  0.01246 0.00016 x  b1 :  3  b1 :  9 11000110
        470  0.01233 0.00015 x  a1 : 15  a2 :  4 11100010
       1375  0.01210 0.00015 x  b2 :  5  b2 :  9 11000110
        363  0.01204 0.00015 x  a1 :  6  b1 :  5 11100100
        294 -0.01203 0.00014 x  a1 : 14  b2 :  5 11101000
       1318 -0.01196 0.00014 x  a1 :  9  a1 : 15 11000110
        344  0.01190 0.00014 x  a1 :  9  b1 :  3 11100100
        121 -0.01171 0.00014 y           a1 : 10  1001110
        260  0.01165 0.00014 x  a2 :  4  b1 :  8 11101000
       1370  0.01160 0.00013 x  b2 :  5  b2 :  8 11000110
        353 -0.01148 0.00013 x  a1 :  7  b1 :  4 11100100
       1354  0.01146 0.00013 x  b1 :  5  b1 :  9 11000110
        272 -0.01146 0.00013 x  a1 : 14  b2 :  3 11101000
        338  0.01139 0.00013 x  a1 : 14  b2 :  9 11101000
        445 -0.01107 0.00012 x  a1 : 12  a2 :  2 11100010
        440  0.01103 0.00012 x  a1 :  7  a2 :  2 11100010
         69  0.01096 0.00012 y           b1 :  4  1100011
         19 -0.01088 0.00012 y           a2 :  2  1110010
       1293  0.01065 0.00011 x  a1 : 10  a1 : 11 11000110
        441  0.01065 0.00011 x  a1 :  8  a2 :  2 11100010
        435 -0.01060 0.00011 x  a2 :  4  b2 :  8 11100100
        375  0.01060 0.00011 x  a1 :  7  b1 :  6 11100100
       1298 -0.01059 0.00011 x  a1 : 10  a1 : 12 11000110
         44  0.01056 0.00011 y           a1 :  7  1101001
        451 -0.01031 0.00011 x  a1 :  7  a2 :  3 11100010
       1008 -0.01023 0.00010 x  a1 : 15  b1 :  3 11001100
           94 csfs were printed in this range.
