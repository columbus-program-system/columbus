1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=    28)...
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.3018785770  7.5495E-15  5.1510E-02  2.1139E-01  1.0000E-03

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  1  1    -46.3018785770  7.5495E-15  5.1510E-02  2.1139E-01  1.0000E-03

 from bk iterations: iconv=   1

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.3456133303  4.3735E-02  1.8243E-03  3.9854E-02  1.0000E-03
 mr-sdci #  2  1    -46.3476209660  2.0076E-03  2.4487E-04  1.4283E-02  1.0000E-03
 mr-sdci #  3  1    -46.3478447599  2.2379E-04  4.2256E-05  6.1555E-03  1.0000E-03
 mr-sdci #  4  1    -46.3478795796  3.4820E-05  3.3796E-06  1.6706E-03  1.0000E-03
 mr-sdci #  5  1    -46.3478827354  3.1558E-06  6.2265E-07  6.3216E-04  1.0000E-03

 mr-sdci  convergence criteria satisfied after  5 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  5  1    -46.3478827354  3.1558E-06  6.2265E-07  6.3216E-04  1.0000E-03

 number of reference csfs (nref) is    28.  root number (iroot) is  1.
 c0**2 =   0.93098666  c**2 (all zwalks) =   0.93098666

 eref      =    -46.301866959168   "relaxed" cnot**2         =   0.930986659322
 eci       =    -46.347882735374   deltae = eci - eref       =  -0.046015776205
 eci+dv1   =    -46.351058437814   dv1 = (1-cnot**2)*deltae  =  -0.003175702440
 eci+dv2   =    -46.351293850246   dv2 = dv1 / cnot**2       =  -0.003411114873
 eci+dv3   =    -46.351566958970   dv3 = dv1 / (2*cnot**2-1) =  -0.003684223596
 eci+pople =    -46.349585956154   (  4e- scaled deltae )    =  -0.047718996986
