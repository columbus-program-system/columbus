
 program cipc      

 print the csf info for mrsdci wave functions

 written by: ron shepard

 version date: 06-jun-96

 This Version of Program cipc is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 workspace allocation parameters: lencor=  13107200 mem1=         0 ifirst=         1

 drt header information:
  cidrt_title                                                                    
 nmot  =    38 niot  =     6 nfct  =     4 nfvt  =     0
 nrow  =    36 nsym  =     4 ssym  =     3 lenbuf=  1600
 spnorb=     F spnodd=     F lxyzir(1:3)= 0 0 0
 nwalk,xbar:        246      105       90       36       15
 nvalwt,nvalw:      169       28       90       36       15
 ncsft:            5600
 map(*)=    -1 -1 29 30 31  1  2  3  4  5  6  7  8  9 10 11 32 12 13 14
            -1 33 15 16 17 18 19 20 21 -1 34 22 23 24 25 26 27 28
 mu(*)=      0  0  0  0  0  0
 syml(*) =   1  1  1  2  3  4
 rmo(*)=     3  4  5  1  2  2

 indx01:   169 indices saved in indxv(*)
 test nroots froot                      1                     1
===================================ROOT # 1===================================

 rdhciv: CI vector file information:
  cidrt_title                                                                    
 energy computed by program ciudg.       zam792            17:53:53.120 17-Dec-13

 lenrec =   32768 lenci =      5600 ninfo =  6 nenrgy =  6 ntitle =  2

 Max. overlap with ref vector #        1
 Valid ci vector #        1
 Method:        0       96% overlap
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -4.349858765627E+01, ietype=    6,   fcore energy of type: H1(*)   
 energy( 3)= -4.634788292896E+01, ietype=-1026,   total energy of type: MRSDCI  
 energy( 4)=  4.699145604986E-04, ietype=-2055, cnvginf energy of type: CI-Resid
 energy( 5)=  3.209115302827E-06, ietype=-2056, cnvginf energy of type: CI-D.E. 
 energy( 6)=  2.719864189783E-07, ietype=-2057, cnvginf energy of type: CI-ApxDE
==================================================================================
space sufficient for valid walk range           1        169
               respectively csf range           1       5600

 space is available for   4357761 coefficients.

 updated histogram parameters:
 csfmn = 0.0000E+00 csfmx = 1.0000E+00 fhist = 5.0000E-01 nhist =  20

 this program will print the csfs generated from
 the drt according to the following print options :

 1) run in batch mode: all valid roots are automatically
    analysed and csf info is printed by default contribution
    threshold 0.01 
 2) run in interactive mode
 3) generate files for cioverlap without symmetry
 4) generate files for cioverlap with symmetry

 input menu number [  1]:
================================================================================
===================================VECTOR # 1===================================
================================================================================


 rdcivnew:     119 coefficients were selected.
 workspace: ncsfmx=    5600
 ncsfmx=                  5600

 histogram parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =    5600 ncsft =    5600 ncsf =     119
 nhist =  20 fhist = 0.50000

    cmin                cmax        num  '*'=     2 csfs.
 ----------          ----------   ----- ---------|---------|---------|---------|
 5.0000E-01 <= |c| < 1.0000E+00       2 *
 2.5000E-01 <= |c| < 5.0000E-01       0
 1.2500E-01 <= |c| < 2.5000E-01       1 *
 6.2500E-02 <= |c| < 1.2500E-01       0
 3.1250E-02 <= |c| < 6.2500E-02      23 ************
 1.5625E-02 <= |c| < 3.1250E-02      39 ********************
 7.8125E-03 <= |c| < 1.5625E-02      54 ***************************
 3.9062E-03 <= |c| < 7.8125E-03       0
 1.9531E-03 <= |c| < 3.9062E-03       0
 9.7656E-04 <= |c| < 1.9531E-03       0
 4.8828E-04 <= |c| < 9.7656E-04       0
 2.4414E-04 <= |c| < 4.8828E-04       0
 1.2207E-04 <= |c| < 2.4414E-04       0
 6.1035E-05 <= |c| < 1.2207E-04       0
 3.0518E-05 <= |c| < 6.1035E-05       0
 1.5259E-05 <= |c| < 3.0518E-05       0
 7.6294E-06 <= |c| < 1.5259E-05       0
 3.8147E-06 <= |c| < 7.6294E-06       0
 0.0000E+00 <= |c| < 3.8147E-06       0
                                  ----- ---------|---------|---------|---------|
                  total read =      119 total stored =     119

 from the selected csfs,
 min(|csfvec(:)|) = 1.0003E-02    max(|csfvec(:)|) = 6.8173E-01
 norm=   1.00000000000000     
 csfs will be printed based on coefficient magnitudes.

 current csfvec(*) selection parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =    5600 ncsft =    5600 ncsf =     119

 i:slabel(i) =  1: a1   2: a2   3: b1   4: b2 
 
 frozen orbital =    1    2    3    4
 symfc(*)       =    1    1    3    4
 label          =  a1   a1   b1   b2 
 rmo(*)         =    1    2    1    1
 
 internal level =    1    2    3    4    5    6
 syml(*)        =    1    1    1    2    3    4
 label          =  a1   a1   a1   a2   b1   b2 
 rmo(*)         =    3    4    5    1    2    2

 printing selected csfs in sorted order from cmin = 0.00000 to cmax = 1.00000

   indcsf     c     c**2   v  lab:rmo  lab:rmo   step(*)
  ------- -------- ------- - ---- --- ---- --- ------------
          3  0.68173 0.46476 z*                    300101
          2 -0.64971 0.42212 z*                    301010
          1  0.15229 0.02319 z*                    310010
         14  0.06111 0.00373 z*                    101102
          7  0.05741 0.00330 z*                    112010
       5375 -0.05524 0.00305 w           a1 :  6  3000101
       5447 -0.05511 0.00304 w           b1 :  3  3000101
       5155  0.05248 0.00275 w           b1 :  3  3001010
       5183  0.05185 0.00269 w           b2 :  3  3001010
       5475 -0.04917 0.00242 w           b2 :  3  3000101
       5083  0.04750 0.00226 w           a1 :  6  3001010
       5381 -0.04734 0.00224 w  a1 :  6  a1 :  9 12000101
       5450 -0.04706 0.00221 w  b1 :  3  b1 :  5 12000101
         10  0.04546 0.00207 z*                    110102
       5158  0.04482 0.00201 w  b1 :  3  b1 :  5 12001010
       5186  0.04438 0.00197 w  b2 :  3  b2 :  5 12001010
         16 -0.04359 0.00190 z*                    100013
       5478 -0.04176 0.00174 w  b2 :  3  b2 :  5 12000101
       5089  0.04043 0.00163 w  a1 :  6  a1 :  9 12001010
       4530  0.03853 0.00148 w  b1 :  3  b2 :  3 12100001
       4292 -0.03852 0.00148 w  a1 :  6  b2 :  3 12100100
       4173  0.03669 0.00135 w  a1 :  6  b1 :  3 12101000
         12 -0.03494 0.00122 z*                    102101
         18 -0.03398 0.00115 z*                    030101
         28  0.03334 0.00111 z*                    000131
         26 -0.03177 0.00101 z*                    001310
         17  0.03072 0.00094 z*                    031010
          8  0.03014 0.00091 z*                    111020
       4532  0.02962 0.00088 w  b1 :  5  b2 :  3 12100001
       4295 -0.02956 0.00087 w  a1 :  9  b2 :  3 12100100
       4314 -0.02955 0.00087 w  a1 :  6  b2 :  5 12100100
       4544  0.02950 0.00087 w  b1 :  3  b2 :  5 12100001
       4375  0.02905 0.00084 w  a1 :  6  a1 :  9 12100010
       4472 -0.02867 0.00082 w  b2 :  3  b2 :  5 12100010
       4195  0.02821 0.00080 w  a1 :  6  b1 :  5 12101000
       4176  0.02809 0.00079 w  a1 :  9  b1 :  3 12101000
       4369  0.02677 0.00072 w           a1 :  6  3100010
       4469 -0.02642 0.00070 w           b2 :  3  3100010
         11 -0.02574 0.00066 z*                    103010
       4546  0.02423 0.00059 w  b1 :  5  b2 :  5 12100001
       4317 -0.02423 0.00059 w  a1 :  9  b2 :  5 12100100
         27  0.02423 0.00059 z*                    001013
       5384 -0.02398 0.00057 w           a1 :  9  3000101
          5  0.02386 0.00057 z*                    121010
         25 -0.02379 0.00057 z*                    003101
       5452 -0.02379 0.00057 w           b1 :  5  3000101
       4198  0.02308 0.00053 w  a1 :  9  b1 :  5 12101000
       5160  0.02265 0.00051 w           b1 :  5  3001010
       5188  0.02247 0.00051 w           b2 :  5  3001010
        260 -0.02133 0.00045 y           a1 : 10  1100201
       5480 -0.02122 0.00045 w           b2 :  5  3000101
       5092  0.02053 0.00042 w           a1 :  9  3001010
        215  0.02030 0.00041 y           a1 : 10  1102010
        746 -0.01863 0.00035 x  a2 :  3  b2 :  6 11300000
       5389 -0.01715 0.00029 w           a1 : 10  3000101
          4 -0.01711 0.00029 z*                    130010
       1011 -0.01698 0.00029 x  a1 : 10  b2 :  6 11200100
       1166  0.01698 0.00029 x  a1 : 10  a2 :  3 11200001
       4378  0.01685 0.00028 w           a1 :  9  3100010
       4474 -0.01662 0.00028 w           b2 :  5  3100010
       5097  0.01632 0.00027 w           a1 : 10  3001010
        892  0.01617 0.00026 x  a1 : 10  b1 :  6 11201000
         13 -0.01597 0.00026 z*                    101201
       4875 -0.01580 0.00025 w           b2 :  3  3010010
        697 -0.01563 0.00024 x  a1 : 11  b1 :  6 11300000
        120 -0.01536 0.00024 y           a1 : 10  1200101
        274  0.01509 0.00023 y           a1 : 10  1100102
          9  0.01495 0.00022 z*                    110201
        734 -0.01477 0.00022 x  a1 : 15  b1 :  9 11300000
        722 -0.01473 0.00022 x  a1 : 14  b1 :  8 11300000
        103  0.01462 0.00021 y           a1 : 10  1201010
       1032 -0.01462 0.00021 x  a1 :  9  b2 :  8 11200100
        989  0.01456 0.00021 x  a1 : 10  b2 :  4 11200100
       1155 -0.01456 0.00021 x  a1 : 10  a2 :  2 11200001
        236 -0.01436 0.00021 y           a1 : 10  1101020
       1065  0.01426 0.00020 x  a1 : 10  a1 : 11 11200010
       1029 -0.01410 0.00020 x  a1 :  6  b2 :  8 11200100
       1204  0.01387 0.00019 x  b1 :  9  b2 :  5 11200001
        870 -0.01387 0.00019 x  a1 : 10  b1 :  4 11201000
        756  0.01385 0.00019 x  a2 :  4  b2 :  9 11300000
       4878 -0.01383 0.00019 w  b2 :  3  b2 :  5 12010010
       1228  0.01357 0.00018 x  b1 :  5  b2 :  9 11200001
        913  0.01357 0.00018 x  a1 :  9  b1 :  8 11201000
       1190  0.01333 0.00018 x  b1 :  9  b2 :  3 11200001
        910  0.01306 0.00017 x  a1 :  6  b1 :  8 11201000
       1226  0.01304 0.00017 x  b1 :  3  b2 :  9 11200001
       1058 -0.01299 0.00017 x  a1 :  7  a1 : 10 11200010
       4350  0.01283 0.00016 w  a1 :  9  b2 :  8 12100100
          6 -0.01278 0.00016 z*                    120101
       1005  0.01270 0.00016 x  a1 : 15  b2 :  5 11200100
         19  0.01243 0.00015 z*                    013010
        983  0.01221 0.00015 x  a1 : 15  b2 :  3 11200100
       4847 -0.01220 0.00015 w           b1 :  3  3010010
       1142  0.01215 0.00015 x  b2 :  5  b2 :  8 11200010
       4347  0.01210 0.00015 w  a1 :  6  b2 :  8 12100100
        627  0.01187 0.00014 y           b1 :  6  1000121
       1140  0.01175 0.00014 x  b2 :  3  b2 :  8 11200010
       4486 -0.01158 0.00013 w  b2 :  5  b2 :  8 12100010
        961  0.01153 0.00013 x  a2 :  4  b1 :  5 11200100
        541 -0.01145 0.00013 y           a2 :  3  1001210
       1176  0.01128 0.00013 x  a1 :  9  a2 :  4 11200001
        885  0.01108 0.00012 x  a1 : 14  b1 :  5 11201000
       4574 -0.01105 0.00012 w  b1 :  5  b2 :  9 12100001
        955  0.01104 0.00012 x  a2 :  4  b1 :  3 11200100
        350 -0.01102 0.00012 y           a1 : 12  1021010
       4484 -0.01097 0.00012 w  b2 :  3  b2 :  8 12100010
       1173  0.01088 0.00012 x  a1 :  6  a2 :  4 11200001
        863  0.01067 0.00011 x  a1 : 14  b1 :  3 11201000
        940 -0.01061 0.00011 x  a2 :  4  b2 :  5 11201000
        745  0.01061 0.00011 x  a2 :  2  b2 :  6 11300000
        740  0.01061 0.00011 x  a2 :  3  b2 :  4 11300000
       4850 -0.01040 0.00011 w  b1 :  3  b1 :  5 12010010
       4572 -0.01039 0.00011 w  b1 :  3  b2 :  9 12100001
       4231 -0.01036 0.00011 w  a1 :  9  b1 :  8 12101000
       1082  0.01034 0.00011 x  a1 :  9  a1 : 14 11200010
        934 -0.01022 0.00010 x  a2 :  4  b2 :  3 11201000
       4550  0.01013 0.00010 w  b1 :  9  b2 :  5 12100001
       4279  0.01004 0.00010 w  a2 :  4  b1 :  5 12100100
        366  0.01000 0.00010 y           a1 : 11  1020101
          119 csfs were printed in this range.
