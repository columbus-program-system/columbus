1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=     4)...
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.2453833562  3.1086E-15  5.4533E-02  2.1660E-01  1.0000E-03

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  1  1    -46.2453833562  3.1086E-15  5.4533E-02  2.1660E-01  1.0000E-03

 from bk iterations: iconv=   1

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.2898140783  4.4431E-02  1.2553E-03  2.8562E-02  1.0000E-03
 mr-sdci #  2  1    -46.2911980096  1.3839E-03  1.2451E-04  9.5671E-03  1.0000E-03
 mr-sdci #  3  1    -46.2913614786  1.6347E-04  3.7279E-05  4.4912E-03  1.0000E-03
 mr-sdci #  4  1    -46.2913838897  2.2411E-05  3.6371E-06  1.8151E-03  1.0000E-03
 mr-sdci #  5  1    -46.2913877003  3.8107E-06  2.3749E-07  3.9816E-04  1.0000E-03

 mr-sdci  convergence criteria satisfied after  5 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  5  1    -46.2913877003  3.8107E-06  2.3749E-07  3.9816E-04  1.0000E-03

 number of reference csfs (nref) is     4.  root number (iroot) is  1.
 c0**2 =   0.92670622  c**2 (all zwalks) =   0.92670622

 eref      =    -46.245383330736   "relaxed" cnot**2         =   0.926706220452
 eci       =    -46.291387700345   deltae = eci - eref       =  -0.046004369609
 eci+dv1   =    -46.294759534469   dv1 = (1-cnot**2)*deltae  =  -0.003371834124
 eci+dv2   =    -46.295026214957   dv2 = dv1 / cnot**2       =  -0.003638514612
 eci+dv3   =    -46.295338702185   dv3 = dv1 / (2*cnot**2-1) =  -0.003951001840
 eci+pople =    -46.293204121502   (  4e- scaled deltae )    =  -0.047820790766
