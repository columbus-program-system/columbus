1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      2009-03.    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


================ Computing sorting integral file structure ================

                    -----z----- -----y----- -----x----- -----w----- ---total---

                CSFs         4         140        1414           0        1558
      internal walks        15          20          15           0          50
valid internal walks         4          20          15           0          39
 getinfoarray: info=                     6 :                     1
                  8192                  6552                  8192
                  5460                     0
 icd(3)=                  2976 ci%nnlev=                   595  l2rec=
                  8192  n2max=                  5460
 lcore1,lcore2=              13068137              13044821
 lencor,maxblo              13107200                 60000
========================================
 current settings:
 minbl3         381
 minbl4         381
 locmaxbl3     4288
 locmaxbuf     2144
 maxbl3       60000
 maxbl3       60000
 maxbl4       60000
 maxbuf       30006
========================================

 sorted 4-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)

 sorted 3-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)
 Orig.  diagonal integrals:  1electron:        34
                             0ext.    :        42
                             2ext.    :       336
                             4ext.    :       812


 Orig. off-diag. integrals:  4ext.    :     24411
                             3ext.    :     18720
                             2ext.    :      6792
                             1ext.    :      1272
                             0ext.    :        99
                             2ext. SO :         0
                             1ext. SO :         0
                             0ext. SO :         0
                             1electron:       128


 Sorted integrals            3ext.  w :     17370 x :     16020
                             4ext.  w :     20955 x :     17777


Cycle #  1 sortfile size=    163835(       5 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13068137
Cycle #  2 sortfile size=    163835(       5 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13068137
 minimum size of srtscr:     98301 WP (     3 records)
 maximum size of srtscr:    163835 WP (     5 records)
diagi   file:      4 records  of   1536 WP each=>       6144 WP total
ofdgi   file:      8 records  of   1536 WP each=>      12288 WP total
fil3w   file:      1 records  of  30006 WP each=>      30006 WP total
fil3x   file:      1 records  of  30006 WP each=>      30006 WP total
fil4w   file:      1 records  of  30006 WP each=>      30006 WP total
fil4x   file:      1 records  of  30006 WP each=>      30006 WP total
 compressed index vector length=                    12
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 0,
  GSET = 0,
   DAVCOR =10,
  NCOREL = 4
   nroot=1
  IVMODE = 3
  NBKITR = 1
   nvbkmn=2
  RTOLBK = 1e-3
   niter=30
   nvcimn=2
   rtolci=1e-3
  NVCIMX = 6
  NVRFMX = 6
  NVBKMX = 6
  IDEN  = 1
  CSFPRN = 10,
  update_mode=1
  nvrfmn=2
 /&end
 ------------------------------------------------------------------------
lodens (list->root)=  1
invlodens (root->list)=  1
 USING SEGMENTS OF EQUAL SIZE

****************  list of control variables  ****************
 lvlprt =    0      nroot  =    1      noldv  =   0      noldhv =   0
 nunitv =    1      nbkitr =    1      niter  =  30      davcor =  10
 csfprn =   10      ivmode =    3      istrt  =   0      vout   =   0
 iortls =    0      nvbkmx =    6      ibktv  =  -1      ibkthv =  -1
 nvcimx =    6      icitv  =   -1      icithv =  -1      frcsub =   0
 nvbkmn =    2      nvcimn =    2      maxseg =   4      nrfitr =  30
 ncorel =    4      nvrfmx =    6      nvrfmn =   2      iden   =   1
 itran  =    0      froot  =    0      rtmode =   0      ncouple=   1
 skipso =    F      dalton2=    0      molcas =   0      finalv =   0
 finalw =    0      cosmocalc=   0    with_tsklst=   0
 nsegwx =    1     1     1     1
 nseg0x =    1     1     1     1
 nseg1x =    1     1     1     1
 nseg2x =    1     1     1     1
 nseg3x =    1     1     1     1
 nseg4x =    1     1     1     1
 no0ex  =      0    no1ex  =      0    no2ex  =     0    no3ex  =     0
 no4ex  =      0    nodiag =      0
 cdg4ex =    1      c3ex1ex=    1      c2ex0ex=   1
 fileloc=    0     0     0     0     0     0     0     1     1     1
 directhd=   1      noaqccshift_zyxw=      0
 critical_crit=-1.00000    critical_delta= 0.05000

 ctol   = 0.010000    lrtshift=1.000000    smalld =0.001000


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-03    1.000E-03
 Computing density:                    .drt1.state1
 using                      1  nodes and                      1  cores.
 szdg/szodg per processor=                  6144                 12288
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core           13107199 DP per process

********** Integral sort section *************


 workspace allocation information: lencor=  13107199

 echo of the input for program cisrt:
 ------------------------------------------------------------------------
  &input
  maxbl3=60000
  maxbl4=60000
  &end
 ------------------------------------------------------------------------
 
 ( 6) listing file:                    ciudgls             
 ( 5) input file:                      cisrtin   
 (17) cidrt file:                      cidrtfl             
 (11) transformed integrals file:      moints    
 (12) diagonal integral file:          diagint             
 (13) off-diagonal integral file:      ofdgint             
 (31) 4-external w integrals file:     fil4w               
 (32) 4-external x integrals file:     fil4x               
 (33) 3-external w integrals file:     fil3w               
 (34) 3-external x integrals file:     fil3x               
 (21) scratch da sorting file:         srtscr              
 (12) 2-e integral file [fsplit=2]:    moints2   

 input integral file header information:
                                                                                 
 aoints SIFS file created by argos.      zam792            17:53:52.086 17-Dec-13
  cidrt_title                                                                    
 MO-coefficients from mcscf.x                                                    
  with dummy occupation 1.0 for active orbitals                                  
  total ao core energy =    0.000000000                                          
 MCSCF energy =     -46.301878576                                                
 SIFS file created by program tran.      zam792            17:53:52.407 17-Dec-13

 input energy(*) values:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total core energy =   0.000000000000E+00

 nsym = 4 nmot=  34

 symmetry  =    1    2    3    4
 slabel(*) =   a1   a2   b1   b2
 nmpsy(*)  =   14    4    8    8

 info(*) =          1      8192      6552      8192      5460         0

 orbital labels, i:molab(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012  13:tout:013  14:tout:014  15:tout:015  16:tout:016  17:tout:017  18:tout:018  19:tout:019  20:tout:020
  21:tout:021  22:tout:022  23:tout:023  24:tout:024  25:tout:025  26:tout:026  27:tout:027  28:tout:028  29:tout:029  30:tout:030
  31:tout:031  32:tout:032  33:tout:033  34:tout:034

 input parameters:
 prnopt=  0
 ldamin=    4095 ldamax=   32767 ldainc=      64
 maxbuf=   30006 maxbl3=   60000 maxbl4=   60000 intmxo=    1536
  Using 32 bit compression 

 drt information:
  cidrt_title                                                                    
 nmotd =  38 nfctd =   4 nfvtc =   0 nmot  =  34
 nlevel =  34 niot  =   6 lowinl=  29
 orbital-to-level map(*)
   -1  -1  29  30  31   1   2   3   4   5   6   7   8   9  10  11  32  12  13  14
   -1  33  15  16  17  18  19  20  21  -1  34  22  23  24  25  26  27  28
 compressed map(*)
   29  30  31   1   2   3   4   5   6   7   8   9  10  11  32  12  13  14  33  15
   16  17  18  19  20  21  34  22  23  24  25  26  27  28
 levsym(*)
    1   1   1   1   1   1   1   1   1   1   1   2   2   2   3   3   3   3   3   3
    3   4   4   4   4   4   4   4   1   1   1   2   3   4
 repartitioning mu(*)=
   0.  0.  0.  0.  0.  0.

 new core energy added to the energy(*) list.
 from the integral file: h1_core= -4.349858765627E+01

 indxdg: diagonal integral statistics.
 total number of integrals contributing to diagonal matrix elements:      1190
 number with all external indices:       812
 number with half external - half internal indices:       336
 number with all internal indices:        42

 indxof: off-diagonal integral statistics.
    4-external integrals: num=      24411 strt=          1
    3-external integrals: num=      18720 strt=      24412
    2-external integrals: num=       6792 strt=      43132
    1-external integrals: num=       1272 strt=      49924
    0-external integrals: num=         99 strt=      51196

 total number of off-diagonal integrals:       51294


 indxof(2nd)  ittp=   3 numx(ittp)=        6792
 indxof(2nd)  ittp=   4 numx(ittp)=        1272
 indxof(2nd)  ittp=   5 numx(ittp)=          99

 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg=  12931980
 pro2e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652

 pro2e:     25126 integrals read in     5 records.

 pro2e:         0 integrals 34-ext integrals skipped.
 pro1e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652
 pro1e: eref =    0.000000000000000E+00
 total size of srtscr:                     4  records of                  32767 
 WP =               1048544 Bytes
 putdg        1     596    1191    1786    3322   36089   57934    3018   46706   90394
   123161  131353  136813  158652

 putf:       4 buffers of length    1536 written to file 12
 diagonal integral file completed.

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep4e:    17 blocks of linear combinations of 4-external integrals processed.
 number of sorted 4-external integrals      38732
 number of original 4-external integrals    24411


 putf34: external integral file complete. nfilw=    31 nfilx=    32 nrecw=     1 nrecx=     1 lbufp= 30006

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep3e:    19 blocks of linear combinations of 3-external integrals processed.
 number of sorted 3-external integrals      33390
 number of original 3-external integrals    18720


 putf34: external integral file complete. nfilw=    33 nfilx=    34 nrecw=     1 nrecx=     1 lbufp= 30006
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      6792         3     43132     43132      6792     51294
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      1272         4     49924     49924      1272     51294
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd        99         5     51196     51196        99     51294

 putf:       8 buffers of length    1536 written to file 13
 off-diagonal files sort completed.
 executing brd_struct for cisrtinfo
cisrtinfo:
bufszi  1536
 diagfile 4ext:     812 2ext:     336 0ext:      42
 fil4w,fil4x  :   24411 fil3w,fil3x :   18720
 ofdgint  2ext:    6792 1ext:    1272 0ext:      99so0ext:       0so1ext:       0so2ext:       0
buffer minbl4     381 minbl3     381 maxbl2     384nbas:  11   3   7   7   0   0   0   0 maxbuf 30006
 CIUDG version 5.9.7 ( 5-Oct-2004)

 workspace allocation information: lcore=  13107199

 core energy values from the integral file:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -4.349858765627E+01, ietype=    6,   fcore energy of type: H1(*)   

 total core repulsion energy = -4.349858765627E+01
 nmot  =    38 niot  =     6 nfct  =     4 nfvt  =     0
 nrow  =    23 nsym  =     4 ssym  =     4 lenbuf=  1600
 nwalk,xbar:         50       15       20       15        0
 nvalwt,nvalw:       39        4       20       15        0
 ncsft:            1558
 total number of valid internal walks:      39
 nvalz,nvaly,nvalx,nvalw =        4      20      15       0

 cisrt info file parameters:
 file number  12 blocksize   1536
 mxbld   1536
 nd4ext,nd2ext,nd0ext   812   336    42
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int    24411    18720     6792     1272       99        0        0        0
 minbl4,minbl3,maxbl2   381   381   384
 maxbuf 30006
 number of external orbitals per symmetry block:  11   3   7   7
 nmsym   4 number of internal orbitals   6
 executing brd_struct for drt
 executing brd_struct for orbinf
 executing brd_struct for momap
 calcthrxt: niot,maxw1=                     6                    11
 block size     0
 pthz,pthy,pthx,pthw:    15    20    15     0 total internal walks:      50
 maxlp3,n2lp,n1lp,n0lp    11     0     0     0
 orbsym(*)= 1 1 1 2 3 4

 setref:        4 references kept,
               11 references were marked as invalid, out of
               15 total.
 nmb.of records onel     1
 nmb.of records 2-ext     5
 nmb.of records 1-ext     1
 nmb.of records 0-ext     1
 nmb.of records 2-int     0
 nmb.of records 1-int     0
 nmb.of records 0-int     0
 ---------memory usage in DP -----------------
 < n-ex core usage >
     routines:
    fourex            61797
    threx             60079
    twoex              3549
    onex               1823
    allin              1536
    diagon             2109
               =======
   maximum            61797
 
  __ static summary __ 
   reflst                 4
   hrfspc                 4
               -------
   static->               4
 
  __ core required  __ 
   totstc                 4
   max n-ex           61797
               -------
   totnec->           61801
 
  __ core available __ 
   totspc          13107199
   totnec -           61801
               -------
   totvec->        13045398

 number of external paths / symmetry
 vertex x     100      82      98      98
 vertex w     128      82      98      98
segment: free space=    13045398
 reducing frespc by                   345 to               13045053 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1           6|         4|         0|         4|         0|         1|
 -------------------------------------------------------------------------------
  Y 2          20|       140|         4|        20|         4|         2|
 -------------------------------------------------------------------------------
  X 3          15|      1414|       144|        15|        24|         3|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=          12DP  conft+indsym=          80DP  drtbuffer=         253 DP

dimension of the ci-matrix ->>>      1558

 executing brd_struct for civct
 gentasklist: ntask=                    12
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS   
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      15       6       1414          4      15       4
     2  2   1    11      one-ext yz   1X  2 1      20       6        140          4      20       4
     3  3   2    15      1ex3ex yx    3X  3 2      15      20       1414        140      15      20
     4  1   1     1      allint zz    OX  1 1       6       6          4          4       4       4
     5  2   2     5      0ex2ex yy    OX  2 2      20      20        140        140      20      20
     6  3   3     6      0ex2ex xx*   OX  3 3      15      15       1414       1414      15      15
     7  3   3    18      0ex2ex xx+   OX  3 3      15      15       1414       1414      15      15
     8  2   2    42      four-ext y   4X  2 2      20      20        140        140      20      20
     9  3   3    43      four-ext x   4X  3 3      15      15       1414       1414      15      15
    10  1   1    75      dg-024ext z  OX  1 1       6       6          4          4       4       4
    11  2   2    76      dg-024ext y  OX  2 2      20      20        140        140      20      20
    12  3   3    77      dg-024ext x  OX  3 3      15      15       1414       1414      15      15
----------------------------------------------------------------------------------------------------
REDTASK #   1 TIME=  11.000 N=  1 (task/type/sgbra)=(   1/24/0) (
REDTASK #   2 TIME=  10.000 N=  1 (task/type/sgbra)=(   2/11/0) (
REDTASK #   3 TIME=   9.000 N=  1 (task/type/sgbra)=(   3/15/0) (
REDTASK #   4 TIME=   8.000 N=  1 (task/type/sgbra)=(   4/ 1/0) (
REDTASK #   5 TIME=   7.000 N=  1 (task/type/sgbra)=(   5/ 5/0) (
REDTASK #   6 TIME=   6.000 N=  1 (task/type/sgbra)=(   6/ 6/1) (
REDTASK #   7 TIME=   5.000 N=  1 (task/type/sgbra)=(   7/18/2) (
REDTASK #   8 TIME=   4.000 N=  1 (task/type/sgbra)=(   8/42/1) (
REDTASK #   9 TIME=   3.000 N=  1 (task/type/sgbra)=(   9/43/1) (
REDTASK #  10 TIME=   2.000 N=  1 (task/type/sgbra)=(  10/75/1) (
REDTASK #  11 TIME=   1.000 N=  1 (task/type/sgbra)=(  11/76/1) (
REDTASK #  12 TIME=   0.000 N=  1 (task/type/sgbra)=(  12/77/1) (
 initializing v-file: 1:                  1558

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:          40 2x:           0 4x:           0
All internal counts: zz :          15 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 reference space has dimension       4
 dsyevx: computed roots 1 to    2(converged:   2)

    root           eigenvalues
    ----           ------------
       1         -46.2453833562
       2         -46.2453833457

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=                     4

         vector  1 from unit 11 written to unit 49 filename cirefv              

 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=     4)...
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              1558
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                    1
 residual norm convergence criteria:               0.001000

          starting bk iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         205 2x:          90 4x:          35
All internal counts: zz :          15 yy:           0 xx:           0 ww:           0
One-external counts: yz :         112 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:          24 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1
   ht   1    -2.74679570

          calcsovref: tciref block   1

              civs   1
 refs   1    1.00000    

          calcsovref: scrb block   1

                ci   1
 civs   1    1.00000    

          calcsovref: sovref block   1

              v      1
 ref    1    1.00000    

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.2453833562  9.3259E-15  5.4499E-02  2.1660E-01  1.0000E-03
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.2453833562  9.3259E-15  5.4499E-02  2.1660E-01  1.0000E-03
 
diagon:itrnv=   2
 expansion vectors are not transformed.
 matrix-vector products are not transformed.

    2 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              1558
 number of initial trial vectors:                         2
 number of initial matrix-vector products:                1
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                   30
 residual norm convergence criteria:               0.001000

          starting ci iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         205 2x:          90 4x:          35
All internal counts: zz :          15 yy:          72 xx:          66 ww:           0
One-external counts: yz :         112 yx:         360 yw:           0
Two-external counts: yy :         150 ww:           0 xx:         180 xz:          24 wz:           0 wx:           0
Three-ext.   counts: yx :         161 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2
   ht   1    -2.74679570
   ht   2    -0.05449895    -0.24540643

          calcsovref: tciref block   1

              civs   1       civs   2
 refs   1    1.00000       8.196454E-13

          calcsovref: scrb block   1

                ci   1         ci   2
 civs   1   0.964803      -0.262973    
 civs   2   0.786194        2.88442    

          calcsovref: sovref block   1

              v      1       v      2
 ref    1   0.964803      -0.262973    

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.96480329    -0.26297264

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.2897931972  4.4410E-02  1.2745E-03  2.8636E-02  1.0000E-03
 mr-sdci #  1  2    -45.6476112591  2.1490E+00  0.0000E+00  5.1922E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   2

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         205 2x:          90 4x:          35
All internal counts: zz :          15 yy:          72 xx:          66 ww:           0
One-external counts: yz :         112 yx:         360 yw:           0
Two-external counts: yy :         150 ww:           0 xx:         180 xz:          24 wz:           0 wx:           0
Three-ext.   counts: yx :         161 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3
   ht   1    -2.74679570
   ht   2    -0.05449895    -0.24540643
   ht   3     0.00012669     0.01079029    -0.00659061

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3
 refs   1    1.00000       8.196454E-13  -1.166009E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3
 civs   1   0.965237       3.940146E-02   0.258400    
 civs   2   0.807421      -0.323486       -2.96002    
 civs   3    1.10097        18.3227       -6.73400    

          calcsovref: sovref block   1

              v      1       v      2       v      3
 ref    1   0.965108       3.726502E-02   0.259185    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.96510843     0.03726502     0.25918534

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1    -46.2911986513  1.4055E-03  1.1909E-04  9.5037E-03  1.0000E-03
 mr-sdci #  2  2    -45.8674808429  2.1987E-01  0.0000E+00  3.3266E-01  1.0000E-04
 mr-sdci #  2  3    -45.6179494817  2.1194E+00  0.0000E+00  5.3420E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   3

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         205 2x:          90 4x:          35
All internal counts: zz :          15 yy:          72 xx:          66 ww:           0
One-external counts: yz :         112 yx:         360 yw:           0
Two-external counts: yy :         150 ww:           0 xx:         180 xz:          24 wz:           0 wx:           0
Three-ext.   counts: yx :         161 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4
   ht   1    -2.74679570
   ht   2    -0.05449895    -0.24540643
   ht   3     0.00012669     0.01079029    -0.00659061
   ht   4    -0.00037673    -0.00346180    -0.00031234    -0.00057829

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4
 refs   1    1.00000       8.196454E-13  -1.166009E-04   1.573819E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4
 civs   1   0.962952      -0.109126       0.174761       0.174353    
 civs   2   0.809780       0.373318       -1.47678       -2.69522    
 civs   3    1.23689        11.4731        11.7525       -10.9806    
 civs   4    1.46658        46.7390       -24.6180        41.6065    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4
 ref    1   0.963038      -0.103108       0.169516       0.182181    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.96303838    -0.10310799     0.16951628     0.18218138

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1    -46.2913733613  1.7471E-04  1.6361E-05  3.4027E-03  1.0000E-03
 mr-sdci #  3  2    -46.0517078812  1.8423E-01  0.0000E+00  2.1034E-01  1.0000E-04
 mr-sdci #  3  3    -45.7920941197  1.7414E-01  0.0000E+00  4.0457E-01  1.0000E-04
 mr-sdci #  3  4    -45.4147587683  1.9162E+00  0.0000E+00  5.1802E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   4

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         205 2x:          90 4x:          35
All internal counts: zz :          15 yy:          72 xx:          66 ww:           0
One-external counts: yz :         112 yx:         360 yw:           0
Two-external counts: yy :         150 ww:           0 xx:         180 xz:          24 wz:           0 wx:           0
Three-ext.   counts: yx :         161 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1    -2.74679570
   ht   2    -0.05449895    -0.24540643
   ht   3     0.00012669     0.01079029    -0.00659061
   ht   4    -0.00037673    -0.00346180    -0.00031234    -0.00057829
   ht   5     0.00088905    -0.00042057    -0.00011481     0.00003107    -0.00022623

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1    1.00000       8.196454E-13  -1.166009E-04   1.573819E-04  -3.210920E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 civs   1  -0.962876       8.850295E-02   6.316586E-02   0.174647      -0.178151    
 civs   2  -0.809896      -0.314535      -0.147658       -1.47975        2.71811    
 civs   3   -1.24565       -9.00354       -6.85136        11.7755        11.3644    
 civs   4   -1.56157       -40.1506       -25.1398       -24.4866       -41.9712    
 civs   5  -0.692339       -60.5791        86.7036       -1.32791       -16.5429    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1  -0.962754       0.102685       3.216834E-02   0.169846      -0.180770    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1    -0.96275406     0.10268525     0.03216834     0.16984640    -0.18076989

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1    -46.2913846889  1.1328E-05  3.6434E-06  1.6597E-03  1.0000E-03
 mr-sdci #  4  2    -46.0873743095  3.5666E-02  0.0000E+00  1.9673E-01  1.0000E-04
 mr-sdci #  4  3    -45.9783434176  1.8625E-01  0.0000E+00  4.5057E-01  1.0000E-04
 mr-sdci #  4  4    -45.7920602700  3.7730E-01  0.0000E+00  4.0333E-01  1.0000E-04
 mr-sdci #  4  5    -45.4002020373  1.9016E+00  0.0000E+00  5.0291E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   5

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         205 2x:          90 4x:          35
All internal counts: zz :          15 yy:          72 xx:          66 ww:           0
One-external counts: yz :         112 yx:         360 yw:           0
Two-external counts: yy :         150 ww:           0 xx:         180 xz:          24 wz:           0 wx:           0
Three-ext.   counts: yx :         161 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1    -2.74679570
   ht   2    -0.05449895    -0.24540643
   ht   3     0.00012669     0.01079029    -0.00659061
   ht   4    -0.00037673    -0.00346180    -0.00031234    -0.00057829
   ht   5     0.00088905    -0.00042057    -0.00011481     0.00003107    -0.00022623
   ht   6     0.00042375     0.00003272    -0.00000948     0.00006473    -0.00007953    -0.00004950

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1    1.00000       8.196454E-13  -1.166009E-04   1.573819E-04  -3.210920E-04  -1.544133E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1   0.962698       0.111178      -1.843913E-02  -0.124421      -0.182840       0.113905    
 civs   2   0.809913      -0.301753      -6.587099E-02    1.04665        2.15951       -1.96298    
 civs   3    1.24802       -8.66011       -1.74089       -14.4529        5.91656       -8.60497    
 civs   4    1.58756       -42.9542       -1.99471        11.5881        10.4724        57.0425    
 civs   5   0.881764       -48.2307       -59.3864        52.6929       -106.913       -66.8933    
 civs   6  -0.850719        93.2289       -115.268       -84.2297        226.046        210.154    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.962651       0.106518       1.831739E-02  -0.124825      -0.182457       0.112914    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.96265102     0.10651808     0.01831739    -0.12482493    -0.18245689     0.11291417

 trial vector basis is being transformed.  new dimension:   2

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1    -46.2913877885  3.0996E-06  1.0994E-07  2.9841E-04  1.0000E-03
 mr-sdci #  5  2    -46.1012094530  1.3835E-02  0.0000E+00  1.0536E-01  1.0000E-04
 mr-sdci #  5  3    -46.0575239417  7.9181E-02  0.0000E+00  4.3344E-01  1.0000E-04
 mr-sdci #  5  4    -45.8192371570  2.7177E-02  0.0000E+00  3.3704E-01  1.0000E-04
 mr-sdci #  5  5    -45.5926267128  1.9242E-01  0.0000E+00  5.0293E-01  1.0000E-04
 mr-sdci #  5  6    -45.2600338021  1.7614E+00  0.0000E+00  4.7006E-01  1.0000E-04
 
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mr-sdci  convergence criteria satisfied after  5 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1    -46.2913877885  3.0996E-06  1.0994E-07  2.9841E-04  1.0000E-03
 mr-sdci #  5  2    -46.1012094530  1.3835E-02  0.0000E+00  1.0536E-01  1.0000E-04

####################CIUDGINFO####################

   ci vector at position   1 energy=  -46.291387788491

################END OF CIUDGINFO################

 
diagon:itrnv=   0
    1 of the   3 expansion vectors are transformed.
    1 of the   2 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
maximum overlap with reference    1(overlap= 0.96265)

 information on vector: 1 from unit 11 written to unit 48 filename civout              


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -46.2913877885

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     3    4    5   17   22   31

                                         symmetry   a1   a1   a1   a2   b1   b2 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  5  1       1  0.420073                        +    +    +              +  
 z*  5  1       2 -0.022884                        +    +         +    +       
 z*  5  1       3 -0.865859                        +         +    +    +       
 y   5  1       6 -0.047507              2( b2 )   +    +    +                 
 y   5  1       8  0.034266              4( b2 )   +    +    +                 
 y   5  1      13 -0.013591              2( b1 )   +    +         +            
 y   5  1      19 -0.010875              1( a2 )   +    +              +       
 y   5  1      23  0.041848              2( a1 )   +    +                   +  
 y   5  1      24  0.027878              3( a1 )   +    +                   +  
 y   5  1      27  0.026406              6( a1 )   +    +                   +  
 y   5  1      28 -0.023638              7( a1 )   +    +                   +  
 y   5  1      34  0.083143              2( b1 )   +         +    +            
 y   5  1      36 -0.064262              4( b1 )   +         +    +            
 y   5  1      40 -0.095233              1( a2 )   +         +         +       
 y   5  1      41  0.069450              2( a2 )   +         +         +       
 y   5  1      44  0.010563              2( a1 )   +         +              +  
 y   5  1      45 -0.033958              3( a1 )   +         +              +  
 y   5  1      48  0.015873              6( a1 )   +         +              +  
 y   5  1      49  0.023888              7( a1 )   +         +              +  
 y   5  1      55  0.067019              2( a1 )   +              +    +       
 y   5  1      56  0.063355              3( a1 )   +              +    +       
 y   5  1      59  0.045405              6( a1 )   +              +    +       
 y   5  1      60 -0.050742              7( a1 )   +              +    +       
 y   5  1      69  0.010956              2( b1 )   +                   +    +  
 y   5  1     121 -0.011709              5( a1 )             +    +    +       
 x   5  1     166  0.026534    1( a1 )   1( b2 )   +    +                      
 x   5  1     169  0.019868    4( a1 )   1( b2 )   +    +                      
 x   5  1     188  0.020149    1( a1 )   3( b2 )   +    +                      
 x   5  1     191  0.015843    4( a1 )   3( b2 )   +    +                      
 x   5  1     230 -0.013197   10( a1 )   6( b2 )   +    +                      
 x   5  1     246  0.014468    1( a2 )   2( b1 )   +         +                 
 x   5  1     247 -0.013319    2( a2 )   2( b1 )   +         +                 
 x   5  1     252 -0.013776    1( a2 )   4( b1 )   +         +                 
 x   5  1     253  0.019655    2( a2 )   4( b1 )   +         +                 
 x   5  1     260  0.011654    3( a2 )   6( b1 )   +         +                 
 x   5  1     263  0.019101    3( a2 )   7( b1 )   +         +                 
 x   5  1     264 -0.022697    1( a1 )   1( b2 )   +         +                 
 x   5  1     267 -0.017127    4( a1 )   1( b2 )   +         +                 
 x   5  1     272 -0.011459    9( a1 )   1( b2 )   +         +                 
 x   5  1     286 -0.017143    1( a1 )   3( b2 )   +         +                 
 x   5  1     289 -0.013569    4( a1 )   3( b2 )   +         +                 
 x   5  1     294 -0.012025    9( a1 )   3( b2 )   +         +                 
 x   5  1     338  0.011394    9( a1 )   7( b2 )   +         +                 
 x   5  1     339  0.015855   10( a1 )   7( b2 )   +         +                 
 x   5  1     341  0.015853    1( a1 )   1( b1 )   +              +            
 x   5  1     344  0.011900    4( a1 )   1( b1 )   +              +            
 x   5  1     353 -0.011476    2( a1 )   2( b1 )   +              +            
 x   5  1     363  0.012044    1( a1 )   3( b1 )   +              +            
 x   5  1     375  0.010597    2( a1 )   4( b1 )   +              +            
 x   5  1     379  0.014147    6( a1 )   4( b1 )   +              +            
 x   5  1     380 -0.013038    7( a1 )   4( b1 )   +              +            
 x   5  1     404  0.016723    9( a1 )   6( b1 )   +              +            
 x   5  1     435 -0.010605    3( a2 )   6( b2 )   +              +            
 x   5  1     440  0.011035    2( a1 )   1( a2 )   +                   +       
 x   5  1     441  0.010645    3( a1 )   1( a2 )   +                   +       
 x   5  1     445 -0.011067    7( a1 )   1( a2 )   +                   +       
 x   5  1     451 -0.010311    2( a1 )   2( a2 )   +                   +       
 x   5  1     455 -0.013208    6( a1 )   2( a2 )   +                   +       
 x   5  1     456  0.016030    7( a1 )   2( a2 )   +                   +       
 x   5  1     470  0.012333   10( a1 )   3( a2 )   +                   +       
 x   5  1     472 -0.039967    1( b1 )   1( b2 )   +                   +       
 x   5  1     474 -0.030096    3( b1 )   1( b2 )   +                   +       
 x   5  1     486 -0.030241    1( b1 )   3( b2 )   +                   +       
 x   5  1     488 -0.023889    3( b1 )   3( b2 )   +                   +       
 x   5  1     520 -0.026836    7( b1 )   7( b2 )   +                   +       
 x   5  1    1007 -0.013560    9( a1 )   1( b1 )             +    +            
 x   5  1    1008 -0.010232   10( a1 )   1( b1 )             +    +            
 x   5  1    1014  0.019580    5( a1 )   2( b1 )             +    +            
 x   5  1    1029 -0.012580    9( a1 )   3( b1 )             +    +            
 x   5  1    1036 -0.015232    5( a1 )   4( b1 )             +    +            
 x   5  1    1054 -0.018050    1( a1 )   6( b1 )             +    +            
 x   5  1    1057 -0.016486    4( a1 )   6( b1 )             +    +            
 x   5  1    1078  0.013775    3( a2 )   1( b2 )             +    +            
 x   5  1    1084  0.012756    3( a2 )   3( b2 )             +    +            
 x   5  1    1101 -0.020277    5( a1 )   1( a2 )             +         +       
 x   5  1    1112  0.014917    5( a1 )   2( a2 )             +         +       
 x   5  1    1119  0.013631    1( a1 )   3( a2 )             +         +       
 x   5  1    1122  0.012576    4( a1 )   3( a2 )             +         +       
 x   5  1    1136  0.017393    7( b1 )   1( b2 )             +         +       
 x   5  1    1150  0.016011    7( b1 )   3( b2 )             +         +       
 x   5  1    1172  0.016642    1( b1 )   7( b2 )             +         +       
 x   5  1    1174  0.015336    3( b1 )   7( b2 )             +         +       
 x   5  1    1286 -0.015394    2( a1 )   5( a1 )                  +    +       
 x   5  1    1287 -0.012901    3( a1 )   5( a1 )                  +    +       
 x   5  1    1293  0.010653    5( a1 )   6( a1 )                  +    +       
 x   5  1    1298 -0.010590    5( a1 )   7( a1 )                  +    +       
 x   5  1    1315 -0.013026    1( a1 )  10( a1 )                  +    +       
 x   5  1    1318 -0.011957    4( a1 )  10( a1 )                  +    +       
 x   5  1    1352  0.012457    1( b1 )   7( b1 )                  +    +       
 x   5  1    1354  0.011464    3( b1 )   7( b1 )                  +    +       
 x   5  1    1368  0.012595    1( b2 )   6( b2 )                  +    +       
 x   5  1    1370  0.011603    3( b2 )   6( b2 )                  +    +       
 x   5  1    1373  0.013105    1( b2 )   7( b2 )                  +    +       
 x   5  1    1375  0.012097    3( b2 )   7( b2 )                  +    +       

 ci coefficient statistics:
           rq > 0.1                2
      0.1> rq > 0.01              92
     0.01> rq > 0.001            289
    0.001> rq > 0.0001           265
   0.0001> rq > 0.00001           81
  0.00001> rq > 0.000001          25
 0.000001> rq                    804
           all                  1558
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:          40 2x:           0 4x:           0
All internal counts: zz :          15 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.420073183995    -19.426439505323
     2     2     -0.022883816961      1.058282459567
     3     3     -0.865859371626     40.042001137756
     4     4      0.000008134986     -0.000374570291

 number of reference csfs (nref) is     4.  root number (iroot) is  1.
 c0**2 =   0.92669760  c**2 (all zwalks) =   0.92669760

 pople ci energy extrapolation is computed with  4 correlated electrons.

 eref      =    -46.245383351589   "relaxed" cnot**2         =   0.926697600489
 eci       =    -46.291387788491   deltae = eci - eref       =  -0.046004436902
 eci+dv1   =    -46.294760024104   dv1 = (1-cnot**2)*deltae  =  -0.003372235613
 eci+dv2   =    -46.295026770195   dv2 = dv1 / cnot**2       =  -0.003638981704
 eci+dv3   =    -46.295339340608   dv3 = dv1 / (2*cnot**2-1) =  -0.003951552117
 eci+pople =    -46.293204442112   (  4e- scaled deltae )    =  -0.047821090523
 passed aftci ... 
 readint2: molcas,dalton2=                     0                     0
 files%faoints=aoints              
lodens (list->root)=  1
sifcfg setup: record length 4096 DP
# d1 elements per record  3272
# d2 elements per record  2730
  The MR-CISD density will be calculated.
 item #                     1 suffix=:.drt1.state1:
 read_civout: repnuc=  -43.4985876562748     
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method:  0 last record  1max overlap with ref# 96% root-following 0
 MR-CISD energy:   -46.29138779    -2.79280013
 residuum:     0.00029841
 deltae:     0.00000310
 apxde:     0.00000011

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.96265102     0.10651808     0.01831739    -0.12482493    -0.18245689     0.11291417     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.96265102     0.10651808     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=      16  DYX=      60  DYW=       0
   D0Z=       3  D0Y=      18  D0X=      12  D0W=       0
  DDZI=      16 DDYI=      60 DDXI=      30 DDWI=       0
  DDZE=       0 DDYE=      20 DDXE=      15 DDWE=       0
================================================================================
Trace of MO density:     4.000000
    4  correlated and     8  frozen core electrons

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.99110336     0.00000114     0.00018005     0.00000000    -0.00165038    -0.00132693
   MO   4     0.00000000     0.00000000     0.00000114     0.18833773     0.01750734     0.00000000    -0.00231268     0.01472526
   MO   5     0.00000000     0.00000000     0.00018005     0.01750734     0.96659346     0.00000000     0.07106898     0.06295290
   MO   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00359558     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000    -0.00165038    -0.00231268     0.07106898     0.00000000     0.00736863     0.00569338
   MO   8     0.00000000     0.00000000    -0.00132693     0.01472526     0.06295290     0.00000000     0.00569338     0.00674927
   MO   9     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00292046     0.00000000     0.00000000
   MO  10     0.00000000     0.00000000     0.00207188     0.00000172    -0.00004144     0.00000000    -0.00000866    -0.00000654
   MO  11     0.00000000     0.00000000    -0.00147003    -0.00475538     0.04577471     0.00000000     0.00528875     0.00361671
   MO  12     0.00000000     0.00000000     0.00149981    -0.01017593    -0.04928023     0.00000000    -0.00490053    -0.00557529
   MO  13     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00096546     0.00000000     0.00000000
   MO  14     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00021784     0.00000000     0.00000000
   MO  15     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00049004     0.00000000     0.00000000
   MO  16     0.00000000     0.00000000    -0.00050887     0.00000006    -0.00000490     0.00000000     0.00000022     0.00000018

                MO   9         MO  10         MO  11         MO  12         MO  13         MO  14         MO  15         MO  16
   MO   3     0.00000000     0.00207188    -0.00147003     0.00149981     0.00000000     0.00000000     0.00000000    -0.00050887
   MO   4     0.00000000     0.00000172    -0.00475538    -0.01017593     0.00000000     0.00000000     0.00000000     0.00000006
   MO   5     0.00000000    -0.00004144     0.04577471    -0.04928023     0.00000000     0.00000000     0.00000000    -0.00000490
   MO   6     0.00292046     0.00000000     0.00000000     0.00000000     0.00096546     0.00021784     0.00049004     0.00000000
   MO   7     0.00000000    -0.00000866     0.00528875    -0.00490053     0.00000000     0.00000000     0.00000000     0.00000022
   MO   8     0.00000000    -0.00000654     0.00361671    -0.00557529     0.00000000     0.00000000     0.00000000     0.00000018
   MO   9     0.00247374     0.00000000     0.00000000     0.00000000     0.00087932     0.00018178     0.00058326     0.00000000
   MO  10     0.00000000     0.00249374    -0.00001214     0.00001187     0.00000000     0.00000000     0.00000000    -0.00012308
   MO  11     0.00000000    -0.00001214     0.00403703    -0.00326642     0.00000000     0.00000000     0.00000000     0.00000054
   MO  12     0.00000000     0.00001187    -0.00326642     0.00478621     0.00000000     0.00000000     0.00000000    -0.00000054
   MO  13     0.00087932     0.00000000     0.00000000     0.00000000     0.00035324     0.00006323     0.00033154     0.00000000
   MO  14     0.00018178     0.00000000     0.00000000     0.00000000     0.00006323     0.00183434    -0.00011814     0.00000000
   MO  15     0.00058326     0.00000000     0.00000000     0.00000000     0.00033154    -0.00011814     0.00173969     0.00000000
   MO  16     0.00000000    -0.00012308     0.00000054    -0.00000054     0.00000000     0.00000000     0.00000000     0.00000764

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     2.00000000     0.99112007     0.98111566     0.18959278     0.00644798     0.00600788     0.00249537
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     0.00191403     0.00153644     0.00105248     0.00009600     0.00007110     0.00002032     0.00000213     0.00000140

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4
   MO   1     0.78224692     0.07799592    -0.05498639     0.00000000
   MO   2     0.07799592     0.01047687    -0.00797637     0.00000000
   MO   3    -0.05498639    -0.00797637     0.00629653     0.00000000
   MO   4     0.00000000     0.00000000     0.00000000     0.00204512

Natural orbital populations,block 2
              MO     1       MO     2       MO     3       MO     4       MO
  occ(*)=     0.79396204     0.00498999     0.00204512     0.00006829

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     0.78916003     0.00000000     0.06698373     0.00000000    -0.05012860     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.00424972     0.00000000     0.00344305     0.00000000     0.00113462    -0.00035726
   MO   4     0.00000000     0.06698373     0.00000000     0.00840582     0.00000000    -0.00670420     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.00344305     0.00000000     0.00289381     0.00000000     0.00101807    -0.00041437
   MO   6     0.00000000    -0.05012860     0.00000000    -0.00670420     0.00000000     0.00558428     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000     0.00113462     0.00000000     0.00101807     0.00000000     0.00040182    -0.00022977
   MO   8     0.00000000     0.00000000    -0.00035726     0.00000000    -0.00041437     0.00000000    -0.00022977     0.00161373
   MO   9     0.00000000     0.00000000     0.00047687     0.00000000     0.00044598     0.00000000     0.00019101     0.00086810

                MO   9
   MO   2     0.00000000
   MO   3     0.00047687
   MO   4     0.00000000
   MO   5     0.00044598
   MO   6     0.00000000
   MO   7     0.00019101
   MO   8     0.00086810
   MO   9     0.00253897

Natural orbital populations,block 3
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     0.79808526     0.00753999     0.00495926     0.00304312     0.00100857     0.00010561     0.00010406
              MO     9       MO
  occ(*)=     0.00000232

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     0.18417931     0.00000000     0.01880634     0.00000000    -0.01312468     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.00591844     0.00000000     0.00477471     0.00000000    -0.00156552     0.00082822
   MO   4     0.00000000     0.01880634     0.00000000     0.00256457     0.00000000    -0.00194166     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.00477471     0.00000000     0.00396288     0.00000000    -0.00137096     0.00081684
   MO   6     0.00000000    -0.01312468     0.00000000    -0.00194166     0.00000000     0.00152833     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000    -0.00156552     0.00000000    -0.00137096     0.00000000     0.00052531    -0.00037679
   MO   8     0.00000000     0.00000000     0.00082822     0.00000000     0.00081684     0.00000000    -0.00037679     0.00135590
   MO   9     0.00000000     0.00000000     0.00023750     0.00000000     0.00018868     0.00000000    -0.00005806     0.00060252

                MO   9
   MO   2     0.00000000
   MO   3     0.00023750
   MO   4     0.00000000
   MO   5     0.00018868
   MO   6     0.00000000
   MO   7    -0.00005806
   MO   8     0.00060252
   MO   9     0.00257798

Natural orbital populations,block 4
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     0.18705319     0.01043980     0.00275746     0.00120241     0.00102467     0.00011596     0.00001661
              MO     9       MO
  occ(*)=     0.00000263


 total number of electrons =   12.0000000000

 test slabel:                     4
  a1  a2  b1  b2 


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        a1  partial gross atomic populations
   ao class       1a1        2a1        3a1        4a1        5a1        6a1 
     3_ s       2.000000   0.000000   0.990807   0.000301   0.000000   0.000000
     3_ p       0.000000   2.000000   0.000000   0.000000   0.000000   0.006247
     3_ d       0.000000   0.000000   0.000313   0.980814   0.189593   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000000   0.000000   0.000201
 
   ao class       7a1        8a1        9a1       10a1       11a1       12a1 
     3_ s       0.000000   0.002495   0.000000   0.000000   0.000000   0.000000
     3_ p       0.000000   0.000000   0.000009   0.000070   0.000000   0.000094
     3_ d       0.006008   0.000000   0.000000   0.000000   0.001052   0.000000
     3_ f       0.000000   0.000000   0.001905   0.001466   0.000000   0.000002
 
   ao class      13a1       14a1       15a1       16a1 
     3_ s       0.000000   0.000000   0.000000   0.000001
     3_ p       0.000000   0.000000   0.000002   0.000000
     3_ d       0.000071   0.000020   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000000

                        a2  partial gross atomic populations
   ao class       1a2        2a2        3a2        4a2 
     3_ d       0.793962   0.004990   0.000000   0.000068
     3_ f       0.000000   0.000000   0.002045   0.000000

                        b1  partial gross atomic populations
   ao class       1b1        2b1        3b1        4b1        5b1        6b1 
     3_ p       2.000000   0.000000   0.007384   0.000000   0.000011   0.000047
     3_ d       0.000000   0.798085   0.000000   0.004959   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000156   0.000000   0.003032   0.000962
 
   ao class       7b1        8b1        9b1 
     3_ p       0.000000   0.000101   0.000002
     3_ d       0.000106   0.000000   0.000000
     3_ f       0.000000   0.000003   0.000000

                        b2  partial gross atomic populations
   ao class       1b2        2b2        3b2        4b2        5b2        6b2 
     3_ p       2.000000   0.000000   0.010223   0.000024   0.000000   0.000044
     3_ d       0.000000   0.187053   0.000000   0.000000   0.001202   0.000000
     3_ f       0.000000   0.000000   0.000216   0.002733   0.000000   0.000980
 
   ao class       7b2        8b2        9b2 
     3_ p       0.000112   0.000000   0.000003
     3_ d       0.000000   0.000017   0.000000
     3_ f       0.000004   0.000000   0.000000


                        gross atomic populations
     ao            3_
      s         2.993605
      p         6.024375
      d         2.968315
      f         0.013706
    total      12.000000
 

 Total number of electrons:   12.00000000

 DA ...
