

     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 This program allows the csf mixing coefficient and orbital expansion coefficient
 optimization using the graphical unitary group approach and the exponential
 operator mcscf method.
 references:  r. shepard and j. simons, ' int. j. quantum chem. symp. 14, 211 (1980).
              r. shepard, i. shavitt, and j. simons, j. chem. phys. 76, 543 (1982).
              r. shepard in "ab initio methods in quantum chemistry ii" advances in chemical
                  physics 69, edited by k. p. lawley (wiley, new york, 1987) pp. 63-200.
 Original autor: Ron Shepard, ANL
 Later revisions: Michal Dallos, University Vienna

 This Version of Program MCSCF is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.4.0.2     **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 Workspace allocation information:
        13107200 of real*8 words (  100.00 MB) of work space has been allocated.

 user input information:

 ======== echo of the mcscf input ========
 ------------------------------------------------------------------------
  &input
   niter=100,
   nmiter=50,
   nciitr=300,
   tol(3)=1.e-4,
   tol(2)=1.e-4,
   tol(1)=1.e-8,
   NSTATE=0,
   npath=1,3,9,10,13,17,19,21,-11,12, 2,
   ncoupl=5,
   tol(9)=1.e-3,
   FCIORB=  1,3,40,1,4,40,1,5,40,2,1,40,3,2,40,4,2,40
   NAVST(1) = 1,
   WAVST(1,1)=1 ,
   NAVST(2) = 2,
   WAVST(2,1)=1 ,
   WAVST(2,2)=1 ,
   NAVST(3) = 2,
   WAVST(3,1)=1 ,
   WAVST(3,2)=1 ,
   NAVST(4) = 2,
   WAVST(4,1)=1 ,
   WAVST(4,2)=1 ,
  &end
 ------------------------------------------------------------------------


 ***  Integral file informations  ***


 input integral file : /bigscratch/Columbus_C70/tests/Zr_c2v/scr/WORK/aoints    
    

 Integral file header information:
                                                                                 
 aoints SIFS file created by argos.      zam792            17:53:52.086 17-Dec-13

 Core type energy values:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 total ao core energy =    0.000000000


   ******  Basis set information:  ******

 Number of irreps:                  4
 Total number of basis functions:  38

 irrep no.              1    2    3    4
 irrep label            a1   a2   b1   b2
 no. of bas.fcions.    16    4    9    9
 inconsistent sifs parameter
 i,info(i),infoloc(i):     5  2700  2730
 bummer (warning):inconsistent sifs parameter                       5


 ***  MCSCF optimization procedure parmeters:  ***


 maximum number of mcscf iterations:        niter=   100

 maximum number of psci micro-iterations:   nmiter=   50
 maximum r,s subspace dimension allowed:    nvrsmx=   30

 tol(1)=  1.0000E-08. . . . delta-emc convergence criterion.
 tol(2)=  1.0000E-04. . . . wnorm convergence criterion.
 tol(3)=  1.0000E-04. . . . knorm convergence criterion.
 tol(4)=  1.0000E-08. . . . apxde convergence criterion.
 tol(5)=  1.0000E-04. . . . small diagonal matrix element tolerance.
 tol(6)=  1.0000E-06. . . . minimum ci-psci residual norm.
 tol(7)=  1.0000E-05. . . . maximum ci-psci residual norm.
 tol(8)=  1.0000E+00. . . . maximum abs(k(xy)) allowed.
 tol(9)=  1.0000E-03. . . . wnorm coupling tolerance.
 tol(10)= 0.0000E+00. . . . maximum psci emergency shift parameter.
 tol(11)= 0.0000E+00. . . . minimum psci emergency shift parameter.
 tol(12)= 0.0000E+00. . . . increment of psci emergency shift parameter.


 *** State averaging informations: ***


 MCSCF calculation performed for  4 DRTs.

 DRT  first state   no.of aver.states   weights
  1   ground state          1             0.143
  2   ground state          2             0.143 0.143
  3   ground state          2             0.143 0.143
  4   ground state          2             0.143 0.143

 The number of hmc(*) eigenvalues and eigenvectors calculated each iteration per DRT:
 DRT.   no.of eigenv.(=ncol)
    1        2
    2        3
    3        3
    4        3

 Orbitals included in invariant subspaces:
   symmetry   orbital   mask
       1       3(  3)    40
       1       4(  4)    40
       1       5(  5)    40
       2       1( 17)    40
       3       2( 22)    40
       4       2( 31)    40

 npath(*) options:
  2:  orbital-state coupling terms will be included beginning on iteration ncoupl=  5
  3:  print intermediate timing information.
  9:  suppress the drt listing.
 10:  suppress the hmc(*) eigenvector listing.
 12:  diagonalize the hmc(*) matrix iteratively.
        nunitv= 1 nciitr=** mxvadd=20 nvcimx=20
       rtolci(*),wnorm=     1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 0.0000E+00
   noldv =   0  0  0  0
 13:  get initial orbitals from the formatted file, mocoef.
 17:  print the final natural orbitals and occupations.
 19:  transform the virtual orbitals to diagonalize qvv(*).
 21:  write out the one- and two- electron density for further use (files:mcd1fl, mcd2fl).


   ******  DRT info section  ******


 Informations for the DRT no.  1

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a1 
 Total number of electrons:   12
 Spin multiplicity:            3
 Number of active orbitals:    6
 Number of active electrons:   4
 Total number of CSFs:        21

 Informations for the DRT no.  2

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a2 
 Total number of electrons:   12
 Spin multiplicity:            3
 Number of active orbitals:    6
 Number of active electrons:   4
 Total number of CSFs:        28

 Informations for the DRT no.  3

 DRT file header:
  title                                                                          
 Molecular symmetry group:    b1 
 Total number of electrons:   12
 Spin multiplicity:            3
 Number of active orbitals:    6
 Number of active electrons:   4
 Total number of CSFs:        28

 Informations for the DRT no.  4

 DRT file header:
  title                                                                          
 Molecular symmetry group:    b2 
 Total number of electrons:   12
 Spin multiplicity:            3
 Number of active orbitals:    6
 Number of active electrons:   4
 Total number of CSFs:        28
 

 faar:   0 active-active rotations allowed out of:   3 possible.


 Number of active-double rotations:         8
 Number of active-active rotations:         0
 Number of double-virtual rotations:       36
 Number of active-virtual rotations:       50
 lenbfsdef=                131071  lenbfs=                   228
  number of integrals per class 1:11 (cf adda 
 class  1 (pq|rs):         #          75
 class  2 (pq|ri):         #         136
 class  3 (pq|ia):         #         628
 class  4 (pi|qa):         #        1040
 class  5 (pq|ra):         #         922
 class  6 (pq|ij)/(pi|qj): #         159
 class  7 (pq|ab):         #        2264
 class  8 (pa|qb):         #        4276
 class  9 p(bp,ai)         #        1800
 class 10p(ai,jp):        #         288
 class 11p(ai,bj):        #         818

 Size of orbital-Hessian matrix B:                     4830
 Size of the orbital-state Hessian matrix C:          17766
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:          22596


 Source of the initial MO coeficients:

 Input MO coefficient file: /bigscratch/Columbus_C70/tests/Zr_c2v/scr/WORK/mocoef       
 

               starting mcscf iteration...   1

 orbital-state coupling will not be calculated this iteration.

 *** Starting integral transformation ***

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     4, naopsy(1) =    16, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 64959, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 65000

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore=  13101852

 inoutp: segmentation information:
 in-core transformation space,   avcinc =  12935592
 address segment size,           sizesg =  12784106
 number of in-core blocks,       nincbk =        19
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:      11152 transformed 1/r12    array elements were written in       3 records.


 mosort: allocated sort2 space, avc2is=    12963705 available sort2 space, avcisx=    12963957

 trial vectors are generated internally.

 trial vector  1 is unit matrix column     1
 ciiter=   6 noldhv=  9 noldv=  9

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -46.3018785762      -46.3018785762        0.0000005318        0.0000010000
    2       -46.1882122360      -46.1882122360        0.0060649410        0.0100000000

 trial vectors are generated internally.

 trial vector  1 is unit matrix column     3
 ciiter=   9 noldhv= 17 noldv= 17

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -46.3018785773      -46.3018785773        0.0000003946        0.0000010000
    2       -46.3018785755      -46.3018785755        0.0000002162        0.0000010000
    3       -46.2754437792      -46.2754437792        0.0041082263        0.0100000000

 trial vectors are generated internally.

 trial vector  1 is unit matrix column     1
 ciiter=   9 noldhv= 18 noldv= 18

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -46.3018785772      -46.3018785772        0.0000002623        0.0000010000
    2       -46.3018785736      -46.3018785736        0.0000002887        0.0000010000
    3       -46.2755103257      -46.2755103257        0.0015722689        0.0100000000

 trial vectors are generated internally.

 trial vector  1 is unit matrix column     2
 ciiter=   8 noldhv= 16 noldv= 16

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -46.3018785762      -46.3018785762        0.0000001431        0.0000010000
    2       -46.3018785736      -46.3018785736        0.0000009522        0.0000010000
    3       -46.2755109226      -46.2755109226        0.0015808344        0.0100000000
 
  tol(10)=  0.000000000000000E+000  eshsci=  8.043863319749284E-008
 Total number of micro iterations:    1

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 1.00000000 pnorm= 0.0000E+00 rznorm= 7.8475E-07 rpnorm= 0.0000E+00 noldr=  1 nnewr=  1 nolds=  0 nnews=  0
 

 fdd(*) eigenvalues. symmetry block  1
    -4.931544   -2.981942
 i,qaaresolved                     1 -0.487590063230102     
 i,qaaresolved                     2  7.987431825616559E-002
 i,qaaresolved                     3  7.987435266171708E-002

 qvv(*) eigenvalues. symmetry block  1
     0.059177    0.293487    0.293487    0.411582    0.532049    1.340227    1.340227    1.839154    1.913512    1.913512
    15.924389
 i,qaaresolved                     1  7.987431146545310E-002

 qvv(*) eigenvalues. symmetry block  2
     0.293487    1.340227    1.913512

 fdd(*) eigenvalues. symmetry block  3
    -2.981942
 i,qaaresolved                     1  7.987428133193472E-002

 qvv(*) eigenvalues. symmetry block  3
     0.059177    0.293487    0.411582    1.340227    1.839154    1.913512    1.913512

 fdd(*) eigenvalues. symmetry block  4
    -2.981942
 i,qaaresolved                     1  7.987432879757433E-002

 qvv(*) eigenvalues. symmetry block  4
     0.059177    0.293487    0.411582    1.340227    1.839154    1.913512    1.913512

 restrt: restart information saved on the restart file (unit= 13).

 not all mcscf convergence criteria are satisfied.
 iter=    1 emc=    -46.3018785757 demc= 4.6302E+01 wnorm= 6.4351E-07 knorm= 5.4324E-08 apxde= 1.7931E-14    *not conv.*     

               starting mcscf iteration...   2

 orbital-state coupling will not be calculated this iteration.

 *** Starting integral transformation ***

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     4, naopsy(1) =    16, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 64959, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 65000

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore=  13101852

 inoutp: segmentation information:
 in-core transformation space,   avcinc =  12935592
 address segment size,           sizesg =  12784106
 number of in-core blocks,       nincbk =        19
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:      11542 transformed 1/r12    array elements were written in       3 records.


 mosort: allocated sort2 space, avc2is=    12963705 available sort2 space, avcisx=    12963957

   2 trial vectors read from nvfile (unit= 29).
 ciiter=   8 noldhv= 12 noldv= 12

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -46.3018785760      -46.3018785760        0.0000006612        0.0000010000
    2       -46.1884941341      -46.1884941341        0.0031356994        0.0100000000

   3 trial vectors read from nvfile (unit= 29).
 ciiter=   8 noldhv= 18 noldv= 18

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -46.3018785770      -46.3018785770        0.0000000922        0.0000010000
    2       -46.3018785757      -46.3018785757        0.0000001038        0.0000010000
    3       -46.2753526415      -46.2753526415        0.0053051248        0.0100000000

   3 trial vectors read from nvfile (unit= 29).
 ciiter=   8 noldhv= 18 noldv= 18

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -46.3018785769      -46.3018785769        0.0000001956        0.0000010000
    2       -46.3018785739      -46.3018785739        0.0000005525        0.0000010000
    3       -46.2754850232      -46.2754850232        0.0027508038        0.0100000000

   3 trial vectors read from nvfile (unit= 29).
 ciiter=   8 noldhv= 18 noldv= 18

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -46.3018785761      -46.3018785761        0.0000000986        0.0000010000
    2       -46.3018785741      -46.3018785741        0.0000001569        0.0000010000
    3       -46.2755058442      -46.2755058442        0.0017678882        0.0100000000
 
  tol(10)=  0.000000000000000E+000  eshsci=  1.006642762318570E-007
 Total number of micro iterations:    1

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 1.00000000 pnorm= 0.0000E+00 rznorm= 5.2360E-07 rpnorm= 0.0000E+00 noldr=  1 nnewr=  1 nolds=  0 nnews=  0
 

 fdd(*) eigenvalues. symmetry block  1
    -4.931544   -2.981942
 i,qaaresolved                     1 -0.487590097297800     
 i,qaaresolved                     2  7.987429404632221E-002
 i,qaaresolved                     3  7.987433767540404E-002

 qvv(*) eigenvalues. symmetry block  1
     0.059177    0.293487    0.293487    0.411582    0.532049    1.340227    1.340227    1.839154    1.913512    1.913512
    15.924389
 i,qaaresolved                     1  7.987431397875505E-002

 qvv(*) eigenvalues. symmetry block  2
     0.293487    1.340227    1.913512

 fdd(*) eigenvalues. symmetry block  3
    -2.981942
 i,qaaresolved                     1  7.987429755963973E-002

 qvv(*) eigenvalues. symmetry block  3
     0.059177    0.293487    0.411582    1.340227    1.839154    1.913512    1.913512

 fdd(*) eigenvalues. symmetry block  4
    -2.981942
 i,qaaresolved                     1  7.987430693965933E-002

 qvv(*) eigenvalues. symmetry block  4
     0.059177    0.293487    0.411582    1.340227    1.839154    1.913512    1.913512

 restrt: restart information saved on the restart file (unit= 13).

 all mcscf convergence criteria are satisfied.

 final mcscf convergence values:
 iter=    2 emc=    -46.3018785757 demc= 3.0553E-13 wnorm= 8.0531E-07 knorm= 3.1037E-08 apxde= 1.2776E-14    *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 wt 0.143 total energy=      -46.301878576, rel. (eV)=   0.000000
   DRT #2 state # 1 wt 0.143 total energy=      -46.301878577, rel. (eV)=   0.000000
   DRT #2 state # 2 wt 0.143 total energy=      -46.301878576, rel. (eV)=   0.000000
   DRT #3 state # 1 wt 0.143 total energy=      -46.301878577, rel. (eV)=   0.000000
   DRT #3 state # 2 wt 0.143 total energy=      -46.301878574, rel. (eV)=   0.000000
   DRT #4 state # 1 wt 0.143 total energy=      -46.301878576, rel. (eV)=   0.000000
   DRT #4 state # 2 wt 0.143 total energy=      -46.301878574, rel. (eV)=   0.000000
   ------------------------------------------------------------


 MO-coefficient print-out skipped (no flag 32)
 They may be found in the MOCOEF directory.

          natural orbitals of the final iteration,block  1    -   a1
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     2.00000000     1.97147102     0.40570581     0.40570577     0.00000000     0.00000000     0.00000000
               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

          natural orbitals of the final iteration,block  2    -   a2
               MO    1        MO    2        MO    3        MO    4
  occ(*)=     0.40570574     0.00000000     0.00000000     0.00000000

          natural orbitals of the final iteration,block  3    -   b1
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     0.40570583     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO    9
  occ(*)=     0.00000000

          natural orbitals of the final iteration,block  4    -   b2
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     0.40570583     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO    9
  occ(*)=     0.00000000
 d1(*), fmc(*), and qmc(*) written to the 1-particle density matrix file.
        163 d2(*) elements written to the 2-particle density matrix file: mcd2fl                                                      


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                         a1 partial gross atomic populations
   ao class       1 a1       2 a1       3 a1       4 a1       5 a1       6 a1
     3_ s       2.000000   0.000000   1.971471   0.000000   0.000000   0.000000
     3_ p       0.000000   2.000000   0.000000   0.000000   0.000000   0.000000
     3_ d       0.000000   0.000000   0.000000   0.405706   0.405706   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
 
   ao class       7 a1       8 a1       9 a1      10 a1      11 a1      12 a1
 
   ao class      13 a1      14 a1      15 a1      16 a1

                         a2 partial gross atomic populations
   ao class       1 a2       2 a2       3 a2       4 a2
     3_ d       0.405706   0.000000   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000000

                         b1 partial gross atomic populations
   ao class       1 b1       2 b1       3 b1       4 b1       5 b1       6 b1
     3_ p       2.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     3_ d       0.000000   0.405706   0.000000   0.000000   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
 
   ao class       7 b1       8 b1       9 b1

                         b2 partial gross atomic populations
   ao class       1 b2       2 b2       3 b2       4 b2       5 b2       6 b2
     3_ p       2.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     3_ d       0.000000   0.405706   0.000000   0.000000   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
 
   ao class       7 b2       8 b2       9 b2


                        gross atomic populations
     ao            3_
      s         3.971471
      p         6.000000
      d         2.028529
      f         0.000000
    total      12.000000
 

 Total number of electrons:   12.00000000

 !timer: mcscf                           cpu_time=     0.025 walltime=     0.025
