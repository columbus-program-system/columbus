1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      09/20/00    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 using llenci= -1
================================================================================
 nsubmx= 6 lenci= 4656
global arrays:        60528   (              0.46 MB)
vdisk:                    0   (              0.00 MB)
drt:                 252658   (              0.96 MB)
================================================================================
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core            9999999 DP per process
 CIUDG version 5.9.4 (29-Aug-2003)
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 0,
  GSET = 0,
   DAVCOR =10,
  NCOREL = 10
  NROOT = 1
  IVMODE = 3
  NBKITR = 0
  NVBKMN = 1
  NVBKMX = 6
  RTOLBK = 1e-3,
  NITER = 20
  NVCIMN = 3
  NVCIMX = 6
  RTOLCI = 1e-3,
  IDEN  = 1
  CSFPRN = 10,
  lvlprt = 5
  frcsub = 2
  cosmocalc = 1
  /end
 ------------------------------------------------------------------------

 ** list of control variables **
 nrfitr =   30      nvrfmx =    6      nvrfmn =    3
 lvlprt =    5      nroot  =    1      noldv  =    0      noldhv =    0
 nunitv =    1      ntype  =    0      nbkitr =    0      niter  =   20
 ivmode =    3      vout   =    0      istrt  =    0      iortls =    0
 nvbkmx =    6      ibktv  =   -1      ibkthv =   -1      frcsub =    2
 nvcimx =    6      icitv  =   -1      icithv =   -1      maxseg =    4
 iden   =    1      itran  =    0      froot  =    0      rtmode =    0
 ftcalc =    1      lrtshift=1.0000    ncouple=    1      skipso  =   F
 ncorel =   10      csfprn  =  10      ctol   = 1.00E-02  davcor  =  10


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-03    1.000E-03

 units and filenames:
    1: ( 6)    ciudgls                                                     
    2: ( 5)    ciudgin                                                     
    3: ( 7)    ciudgsm                                                     
    4: ( 4)    cihdiag                                                     
    5: ( 3)    cihpseu                                                     
    6: (10)    cihvfl                                                      
    7: (11)    civfl                                                       
    8: (12)    diagint                                                     
    9: (13)    ofdgint                                                     
   10: (17)    cidrtfl                                                     
   11: (20)    ciftdfl                                                     
   12: (21)    ciftofl                                                     
   13: (24)    ciftifl                                                     
   14: ( 8)    ciflind                                                     
   15: (15)    civin                                                       
   16: (16)    civout                                                      
   18: (18)    d1fl                                                        
   20: (53)    cifvfl                                                      
   21: (25)    ciscr4                                                      
   22: (41)    ciftotd                                                     
   23: (42)    flacpfd                                                     
   26: (26)    ciscr5                                                      
   27: (47)    restart                                                     
   28: (48)    civout                                                      
   29: (49)    cirefv                                                      
   31: (31)    fil4w                                                       
   32: (32)    fil4x                                                       
   33: (33)    fil3w                                                       
   34: (34)    fil3x                                                       
   36: (36)    aoints                                                      
   37: (37)    aoints2                                                     
   38: (38)    drtfil                                                      
   39: (50)    cisrtif                                                     
   42: (50)    mocoef                                                      
   45: (52)    nocoef_ci                                                   
   46: (53)    civfl_restart                                               
   54: (54)    civcosmo                                                    
 ------------------------------------------------------------------------

 workspace allocation information: lcore=   9999999 mem1=1108566024 ifirst=-264081238

 integral file titles:
 Hermit Integral Program : SIFS version  hochtor2        Thu Oct  7 15:41:48 2004
  cidrt_title                                                                    
  title                                                                          
 mofmt: formatted orbitals label=morbl   hochtor2        Thu Oct  7 15:41:49 2004
 SIFS file created by program tran.      hochtor2        Thu Oct  7 15:41:49 2004

 core energy values from the integral file:
 energy( 1)=  9.317913916904E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total core repulsion energy =  9.317913916904E+00

 drt header information:
  cidrt_title                                                                    
 spnorb, spnodd, lxyzir,hmult F F 0 0 0 0
 nmot  =    24 niot  =     5 nfct  =     0 nfvt  =     0
 nrow  =    20 nsym  =     1 ssym  =     1 lenbuf=  1600
 nwalk,xbar:         31        1        5       10       15
 nvalwt,nvalw:       31        1        5       10       15
 ncsft:            4656
 total number of valid internal walks:      31
 nvalz,nvaly,nvalx,nvalw =        1       5      10      15

 cisrt info file parameters:
 file number  12 blocksize     64
 mxbld    384
 nd4ext,nd2ext,nd0ext   380   190    30
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int    21375    19665     8550     1995      165        0        0        0
 minbl4,minbl3,maxbl2   567   567   570
 maxbuf 32767
 number of external orbitals per symmetry block:  19
 nmsym   1 number of internal orbitals   5

 formula file title:
 Hermit Integral Program : SIFS version  hochtor2        Thu Oct  7 15:41:48 2004
  cidrt_title                                                                    
  title                                                                          
 mofmt: formatted orbitals label=morbl   hochtor2        Thu Oct  7 15:41:49 2004
 SIFS file created by program tran.      hochtor2        Thu Oct  7 15:41:49 2004
  cidrt_title                                                                    
 file nmb.  24 block size     0
 pthz,pthy,pthx,pthw:     1     5    10    15 total internal walks:      31
 maxlp3,n3xtlp,n2xtlp,n1xtlp,n0xtlp,n2lp,n1lp,n0lp     0     0     0     0
 orbsym(*)= 1 1 1 1 1

 setref:        1 references kept,
                0 references were marked as invalid, out of
                1 total.
 limcnvrt: found 1 valid internal walksout of  1
  walks (skipping trailing invalids)
  ... adding  1 segmentation marks segtype= 1
 limcnvrt: found 5 valid internal walksout of  5
  walks (skipping trailing invalids)
  ... adding  5 segmentation marks segtype= 2
 limcnvrt: found 10 valid internal walksout of  10
  walks (skipping trailing invalids)
  ... adding  10 segmentation marks segtype= 3
 limcnvrt: found 15 valid internal walksout of  15
  walks (skipping trailing invalids)
  ... adding  15 segmentation marks segtype= 4
 nmb.of records 4-ext     3
 nmb.of records 3-ext   134
 nmb.of records 2-ext    32
 nmb.of records 1-ext     3
 nmb.of records 0-ext     0
 nmb.of records 2-int     0
 nmb.of records 1-int     0
 nmb.of records 0-int
 mxorb=  19

 < n-ex core usage >
     routines:
    fourex       66636
    threx        33149
    twoex         3959
    onex           863
    allin           64
    diagon         938
               =======
   maximum       66636

  __ static summary __ 
   reflst            1
   hrfspc            1
               -------
   static->          1

  __ core required  __ 
   totstc            1
   max n-ex      66636
               -------
   totnec->      66637

  __ core available __ 
   totspc      9999999
   totnec -      66637
               -------
   totvec->    9933362

 number of external paths / symmetry
 vertex x     171
 vertex w     190
================ pruneseg on segment    1(   1)================
 original DRT
iwalk=   1step: 3 3 3 3 3
 level  0 through level  4 of the drt:

 row lev  l0  l1  l2  l3     y0      y1      y2    xbar 

  17  4   0   0   0  20       1       1       1       1
  18  4   0   0   0   0       0       0       0       0
  19  4   0   0   0   0       0       0       0       0
 ....................................

  13  3   0   0   0  17       1       1       1       1
  14  3   0   0   0   0       0       0       0       0
  15  3   0   0   0   0       0       0       0       0
  16  3   0   0   0   0       0       0       0       0
 ....................................

   9  2   0   0   0  13       1       1       1       1
  10  2   0   0   0   0       0       0       0       0
  11  2   0   0   0   0       0       0       0       0
  12  2   0   0   0   0       0       0       0       0
 ....................................

   5  1   0   0   0   9       1       1       1       1
   6  1   0   0   0   0       0       0       0       0
   7  1   0   0   0   0       0       0       0       0
   8  1   0   0   0   0       0       0       0       0
 ....................................

   1  0   0   0   0   5       1       1       1       1
   2  0   0   0   0   0       0       0       0       0
   3  0   0   0   0   0       0       0       0       0
   4  0   0   0   0   0       0       0       0       0
 ....................................


 lprune: l(*,*,*) pruned with nwalk=       1 nvalwt=       1 nprune=       0
 ynew( 1):
  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0
  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0
  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
 xbarnew( 1):
  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  1
 limn array= 1
  1
 limvec array= 1
  2
 reduced DRT,invalid= 0
iwalk=   1step: 3 3 3 3 3
 level  0 through level  4 of the drt:

 row lev  l0  l1  l2  l3     y0      y1      y2    xbar 

  17  4   0   0   0  20       1       1       1       1
  18  4   0   0   0   0       0       0       0       0
  19  4   0   0   0   0       0       0       0       0
 ....................................

  13  3   0   0   0  17       1       1       1       1
  14  3   0   0   0   0       0       0       0       0
  15  3   0   0   0   0       0       0       0       0
  16  3   0   0   0   0       0       0       0       0
 ....................................

   9  2   0   0   0  13       1       1       1       1
  10  2   0   0   0   0       0       0       0       0
  11  2   0   0   0   0       0       0       0       0
  12  2   0   0   0   0       0       0       0       0
 ....................................

   5  1   0   0   0   9       1       1       1       1
   6  1   0   0   0   0       0       0       0       0
   7  1   0   0   0   0       0       0       0       0
   8  1   0   0   0   0       0       0       0       0
 ....................................

   1  0   0   0   0   5       1       1       1       1
   2  0   0   0   0   0       0       0       0       0
   3  0   0   0   0   0       0       0       0       0
   4  0   0   0   0   0       0       0       0       0
 ....................................

================ pruneseg on segment    2(   2)================
 original DRT
iwalk=   2step: 3 3 3 3 2
iwalk=   3step: 3 3 3 2 3
iwalk=   4step: 3 3 2 3 3
iwalk=   5step: 3 2 3 3 3
iwalk=   6step: 2 3 3 3 3
 level  0 through level  4 of the drt:

 row lev  l0  l1  l2  l3     y0      y1      y2    xbar 

  17  4   0   0   0  20       1       1       1       1
  18  4   0   0  20   0       1       1       0       1
  19  4   0   0   0   0       0       0       0       0
 ....................................

  13  3   0   0   0  17       1       1       1       1
  14  3   0   0  17  18       2       2       1       2
  15  3   0   0   0   0       0       0       0       0
  16  3   0   0   0   0       0       0       0       0
 ....................................

   9  2   0   0   0  13       1       1       1       1
  10  2   0   0  13  14       3       3       2       3
  11  2   0   0   0   0       0       0       0       0
  12  2   0   0   0   0       0       0       0       0
 ....................................

   5  1   0   0   0   9       1       1       1       1
   6  1   0   0   9  10       4       4       3       4
   7  1   0   0   0   0       0       0       0       0
   8  1   0   0   0   0       0       0       0       0
 ....................................

   1  0   0   0   0   0       0       0       0       0
   2  0   0   0   5   6       6       6       5       5
   3  0   0   0   0   0       0       0       0       0
   4  0   0   0   0   0       0       0       0       0
 ....................................


 lprune: l(*,*,*) pruned with nwalk=       5 nvalwt=       4 nprune=       0
 ynew( 2):
  0  5  0  0  1  4  0  0  1  3  0  0  1  2  0  0  1  1  0  0
  0  5  0  0  1  4  0  0  1  3  0  0  1  2  0  0  1  1  0  0
  0  4  0  0  1  3  0  0  1  2  0  0  1  1  0  0  1  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
 xbarnew( 2):
  0  5  0  0  1  4  0  0  1  3  0  0  1  2  0  0  1  1  0  1
 limn array= 5
  1  2  3  4  5
 limvec array= 5
  0  2  2  2  2
 reduced DRT,invalid= 0
iwalk=   1step: 3 3 3 3 2
iwalk=   2step: 3 3 3 2 3
iwalk=   3step: 3 3 2 3 3
iwalk=   4step: 3 2 3 3 3
iwalk=   5step: 2 3 3 3 3
 level  0 through level  4 of the drt:

 row lev  l0  l1  l2  l3     y0      y1      y2    xbar 

  17  4   0   0   0  20       1       1       1       1
  18  4   0   0  20   0       1       1       0       1
  19  4   0   0   0   0       0       0       0       0
 ....................................

  13  3   0   0   0  17       1       1       1       1
  14  3   0   0  17  18       2       2       1       2
  15  3   0   0   0   0       0       0       0       0
  16  3   0   0   0   0       0       0       0       0
 ....................................

   9  2   0   0   0  13       1       1       1       1
  10  2   0   0  13  14       3       3       2       3
  11  2   0   0   0   0       0       0       0       0
  12  2   0   0   0   0       0       0       0       0
 ....................................

   5  1   0   0   0   9       1       1       1       1
   6  1   0   0   9  10       4       4       3       4
   7  1   0   0   0   0       0       0       0       0
   8  1   0   0   0   0       0       0       0       0
 ....................................

   1  0   0   0   0   0       0       0       0       0
   2  0   0   0   5   6       5       5       4       5
   3  0   0   0   0   0       0       0       0       0
   4  0   0   0   0   0       0       0       0       0
 ....................................

================ pruneseg on segment    3(   3)================
 original DRT
iwalk=   7step: 3 3 3 2 2
iwalk=   8step: 3 3 2 3 2
iwalk=   9step: 3 3 2 2 3
iwalk=  10step: 3 2 3 3 2
iwalk=  11step: 3 2 3 2 3
iwalk=  12step: 3 2 2 3 3
iwalk=  13step: 2 3 3 3 2
iwalk=  14step: 2 3 3 2 3
iwalk=  15step: 2 3 2 3 3
iwalk=  16step: 2 2 3 3 3
 level  0 through level  4 of the drt:

 row lev  l0  l1  l2  l3     y0      y1      y2    xbar 

  17  4   0   0   0  20       1       1       1       1
  18  4   0   0  20   0       1       1       0       1
  19  4  20   0   0   0       0       0       0       1
 ....................................

  13  3   0   0   0  17       1       1       1       1
  14  3   0   0  17  18       2       2       1       2
  15  3   0   0  18   0       1       1       0       1
  16  3  17  18   0  19       2       1       1       3
 ....................................

   9  2   0   0   0  13       1       1       1       1
  10  2   0   0  13  14       3       3       2       3
  11  2   0   0  14  15       3       3       1       3
  12  2  13  14   0  16       5       3       3       6
 ....................................

   5  1   0   0   0   9       1       1       1       1
   6  1   0   0   9  10       4       4       3       4
   7  1   0   0  10  11       6       6       3       6
   8  1   9  10   0  12       9       6       6      10
 ....................................

   1  0   0   0   0   0       0       0       0       0
   2  0   0   0   0   0       0       0       0       0
   3  0   0   0   6   7      16      16      12      10
   4  0   5   6   0   8      30      26      26      15
 ....................................


 lprune: l(*,*,*) pruned with nwalk=      10 nvalwt=      10 nprune=      14
 ynew( 3):
  0  0 10  0  0  4  6  0  1  3  3  0  1  2  1  0  1  1  0  0
  0  0 10  0  0  4  6  0  1  3  3  0  1  2  1  0  1  1  0  0
  0  0  6  0  0  3  3  0  1  2  1  0  1  1  0  0  1  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
 xbarnew( 3):
  0  0 10  0  0  4  6  0  1  3  3  0  1  2  1  0  1  1  0  1
 limn array= 10
  1  2  3  4  5  6  7  8  9 10
 limvec array= 10
  2  2  2  2  2  2  2  2  2  2
 reduced DRT,invalid= 0
iwalk=   1step: 3 3 3 2 2
iwalk=   2step: 3 3 2 3 2
iwalk=   3step: 3 3 2 2 3
iwalk=   4step: 3 2 3 3 2
iwalk=   5step: 3 2 3 2 3
iwalk=   6step: 3 2 2 3 3
iwalk=   7step: 2 3 3 3 2
iwalk=   8step: 2 3 3 2 3
iwalk=   9step: 2 3 2 3 3
iwalk=  10step: 2 2 3 3 3
 level  0 through level  4 of the drt:

 row lev  l0  l1  l2  l3     y0      y1      y2    xbar 

  17  4   0   0   0  20       1       1       1       1
  18  4   0   0  20   0       1       1       0       1
  19  4   0   0   0   0       0       0       0       0
 ....................................

  13  3   0   0   0  17       1       1       1       1
  14  3   0   0  17  18       2       2       1       2
  15  3   0   0  18   0       1       1       0       1
  16  3   0   0   0   0       0       0       0       0
 ....................................

   9  2   0   0   0  13       1       1       1       1
  10  2   0   0  13  14       3       3       2       3
  11  2   0   0  14  15       3       3       1       3
  12  2   0   0   0   0       0       0       0       0
 ....................................

   5  1   0   0   0   0       0       0       0       0
   6  1   0   0   9  10       4       4       3       4
   7  1   0   0  10  11       6       6       3       6
   8  1   0   0   0   0       0       0       0       0
 ....................................

   1  0   0   0   0   0       0       0       0       0
   2  0   0   0   0   0       0       0       0       0
   3  0   0   0   6   7      10      10       6      10
   4  0   0   0   0   0       0       0       0       0
 ....................................

================ pruneseg on segment    4(   3)================
 original DRT
iwalk=  17step: 3 3 3 3 0
iwalk=  18step: 3 3 3 1 2
iwalk=  19step: 3 3 3 0 3
iwalk=  20step: 3 3 1 3 2
iwalk=  21step: 3 3 1 2 3
iwalk=  22step: 3 3 0 3 3
iwalk=  23step: 3 1 3 3 2
iwalk=  24step: 3 1 3 2 3
iwalk=  25step: 3 1 2 3 3
iwalk=  26step: 3 0 3 3 3
iwalk=  27step: 1 3 3 3 2
iwalk=  28step: 1 3 3 2 3
iwalk=  29step: 1 3 2 3 3
iwalk=  30step: 1 2 3 3 3
iwalk=  31step: 0 3 3 3 3
 level  0 through level  4 of the drt:

 row lev  l0  l1  l2  l3     y0      y1      y2    xbar 

  17  4   0   0   0  20       1       1       1       1
  18  4   0   0  20   0       1       1       0       1
  19  4  20   0   0   0       0       0       0       1
 ....................................

  13  3   0   0   0  17       1       1       1       1
  14  3   0   0  17  18       2       2       1       2
  15  3   0   0  18   0       1       1       0       1
  16  3  17  18   0  19       2       1       1       3
 ....................................

   9  2   0   0   0  13       1       1       1       1
  10  2   0   0  13  14       3       3       2       3
  11  2   0   0  14  15       3       3       1       3
  12  2  13  14   0  16       5       3       3       6
 ....................................

   5  1   0   0   0   9       1       1       1       1
   6  1   0   0   9  10       4       4       3       4
   7  1   0   0  10  11       6       6       3       6
   8  1   9  10   0  12       9       6       6      10
 ....................................

   1  0   0   0   0   0       0       0       0       0
   2  0   0   0   0   0       0       0       0       0
   3  0   0   0   6   7      16      16      12      10
   4  0   5   6   0   8      30      26      26      15
 ....................................


 lprune: l(*,*,*) pruned with nwalk=      15 nvalwt=      15 nprune=       7
 ynew( 3):
  0  0  0 14  1  4  0  9  1  3  0  5  1  2  0  2  1  1  0  0
  0  0  0 10  1  4  0  6  1  3  0  3  1  2  0  1  1  1  0  0
  0  0  0 10  1  3  0  6  1  2  0  3  1  1  0  1  1  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
 xbarnew( 3):
  0  0  0 15  1  4  0 10  1  3  0  6  1  2  0  3  1  1  1  1
 limn array= 15
  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15
 limvec array= 15
  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2
 reduced DRT,invalid= 0
iwalk=   1step: 3 3 3 3 0
iwalk=   2step: 3 3 3 1 2
iwalk=   3step: 3 3 3 0 3
iwalk=   4step: 3 3 1 3 2
iwalk=   5step: 3 3 1 2 3
iwalk=   6step: 3 3 0 3 3
iwalk=   7step: 3 1 3 3 2
iwalk=   8step: 3 1 3 2 3
iwalk=   9step: 3 1 2 3 3
iwalk=  10step: 3 0 3 3 3
iwalk=  11step: 1 3 3 3 2
iwalk=  12step: 1 3 3 2 3
iwalk=  13step: 1 3 2 3 3
iwalk=  14step: 1 2 3 3 3
iwalk=  15step: 0 3 3 3 3
 level  0 through level  4 of the drt:

 row lev  l0  l1  l2  l3     y0      y1      y2    xbar 

  17  4   0   0   0  20       1       1       1       1
  18  4   0   0  20   0       1       1       0       1
  19  4  20   0   0   0       0       0       0       1
 ....................................

  13  3   0   0   0  17       1       1       1       1
  14  3   0   0  17  18       2       2       1       2
  15  3   0   0   0   0       0       0       0       0
  16  3  17  18   0  19       2       1       1       3
 ....................................

   9  2   0   0   0  13       1       1       1       1
  10  2   0   0  13  14       3       3       2       3
  11  2   0   0   0   0       0       0       0       0
  12  2  13  14   0  16       5       3       3       6
 ....................................

   5  1   0   0   0   9       1       1       1       1
   6  1   0   0   9  10       4       4       3       4
   7  1   0   0   0   0       0       0       0       0
   8  1   9  10   0  12       9       6       6      10
 ....................................

   1  0   0   0   0   0       0       0       0       0
   2  0   0   0   0   0       0       0       0       0
   3  0   0   0   0   0       0       0       0       0
   4  0   5   6   0   8      14      10      10      15
 ....................................




                   segmentation summary for type diagonal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1           1|         1|         0|         1|         0|         1|
 -------------------------------------------------------------------------------
  Y 2           5|        95|         1|         5|         1|         2|
 -------------------------------------------------------------------------------
  X 3          10|      1710|        96|        10|         6|         3|
 -------------------------------------------------------------------------------
  W 4          15|      2850|      1806|        15|        16|         4|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>      4656


 297 dimension of the ci-matrix ->>>      4656


 297 dimension of the ci-matrix ->>>         7

                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      10       1       1710          1      10       1
     2  4   1    25      two-ext wz   2X  4 1      15       1       2850          1      15       1
     3  4   3    26      two-ext wx   2X  4 3      15      10       2850       1710      15      10
     4  2   1    11      one-ext yz   1X  2 1       5       1         95          1       5       1
     5  3   2    15      1ex3ex  yx   3X  3 2      10       5       1710         95      10       5
     6  4   2    16      1ex3ex  yw   3X  4 2      15       5       2850         95      15       5
     7  1   1     1      allint zz    OX  1 1       1       1          1          1       1       1
     8  2   2     5      0ex2ex yy    OX  2 2       5       5         95         95       5       5
     9  3   3     6      0ex2ex xx    OX  3 3      10      10       1710       1710      10      10
    10  4   4     7      0ex2ex ww    OX  4 4      15      15       2850       2850      15      15
    11  1   1    75      dg-024ext z  DG  1 1       1       1          1          1       1       1
    12  2   2    45      4exdg024 y   DG  2 2       5       5         95         95       5       5
    13  3   3    46      4exdg024 x   DG  3 3      10      10       1710       1710      10      10
    14  4   4    47      4exdg024 w   DG  4 4      15      15       2850       2850      15      15
----------------------------------------------------------------------------------------------------
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.333640E+01-0.311405E+01-0.411071E+01-0.409434E+01-0.474346E+01-0.533548E+01-0.433126E+01-0.358815E+01-0.343744E+01-0.318602E+01
 -0.403221E+01-0.264879E+01-0.363589E+01-0.367752E+01-0.366332E+01-0.363400E+01-0.338294E+01-0.303138E+01-0.288889E+01-0.330431E+02
 -0.787128E+01-0.671895E+01-0.694549E+01-0.710686E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         2        65         3         2        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         3       129         4         3        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         4       193         5         4        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         5       257         6         5        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         6       321         7         6        12        64       128         6       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.333640E+01-0.311405E+01-0.411071E+01-0.409434E+01-0.474346E+01-0.533548E+01-0.433126E+01-0.358815E+01-0.343744E+01-0.318602E+01
 -0.403221E+01-0.264879E+01-0.363589E+01-0.367752E+01-0.366332E+01-0.363400E+01-0.338294E+01-0.303138E+01-0.288889E+01-0.330431E+02
 -0.787128E+01-0.671895E+01-0.694549E+01-0.710686E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         2        65         3         2        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         3       129         4         3        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         4       193         5         4        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         5       257         6         5        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         6       321         7         6        12        64       128         6       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.333640E+01-0.311405E+01-0.411071E+01-0.409434E+01-0.474346E+01-0.533548E+01-0.433126E+01-0.358815E+01-0.343744E+01-0.318602E+01
 -0.403221E+01-0.264879E+01-0.363589E+01-0.367752E+01-0.366332E+01-0.363400E+01-0.338294E+01-0.303138E+01-0.288889E+01-0.330431E+02
 -0.787128E+01-0.671895E+01-0.694549E+01-0.710686E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         2        65         3         2        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         3       129         4         3        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         4       193         5         4        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         5       257         6         5        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         6       321         7         6        12        64       128         6       380       380
 DIAGEL COUNTS: DG0X,DG2X,DG4X= 440 145 30
 diagonal elements written to file   4

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        15 2x:         0 4x:         0
All internal counts: zz :         0 yy:         0 xx:         0 ww:         0
One-external counts: yz :         0 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         0    task #   2:         0    task #   3:         0    task #   4:         0
task #   5:         0    task #   6:         0    task #   7:        -1    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:        14    task #  12:         0
task #  13:         0    task #  14:         0    task #
 reference space has dimension       1

          hamiltonian matrix in the reference space 

              rcsf   1
 rcsf   1   -76.02703281

    root           eigenvalues
    ----           ------------
       1         -76.0270328082

          eigenvectors in the subspace basis

                v:   1
 rcsf   1     1.00000000

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt= 1

         vector  1 from unit 11 written to unit 49 filename cirefv                                                      
 ufvoutnew: ... writing  recamt= 1

         vector  1 from unit 11 written to unit 49 filename cirefv                                                      

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              4656
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                   20
 residual norm convergence criteria:               0.001000

 elast before the main iterative loop 
 elast =  -76.0270328

 rtflw before main loop =  0

          starting ci iteration   1

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.31791392

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= T F

################################################################################
 one electron density matrix calculation 
################################################################################


 workspace allocation information: mxcore= 9999997

 space required:

    space required for calls in multd2:
       onex          475
       allin           0
       diagon        950
    max.      ---------
       maxnex        950

    total core space usage:
       maxnex        950
    max k-seg       2850
    max k-ind         15
              ---------
       totmax       6680

 total core array space available  9999997
 total used                           6680
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=      10  D0X=      30  D0W=      50
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================

          ci-one-electron density block   1

                mo   1         mo   2         mo   3         mo   4         mo   5         mo   6         mo   7         mo   8
   mo   1     2.00000000
   mo   2     0.00000000     2.00000000
   mo   3     0.00000000     0.00000000     2.00000000
   mo   4     0.00000000     0.00000000     0.00000000     2.00000000
   mo   5     0.00000000     0.00000000     0.00000000     0.00000000     2.00000000

                mo   9         mo  10         mo  11         mo  12         mo  13         mo  14         mo  15         mo  16

                mo  17         mo  18         mo  19         mo  20         mo  21         mo  22         mo  23         mo  24


*****   symmetry block SYM1   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       2.00000000
       2       0.00000000     2.00000000
       3       0.00000000     0.00000000     2.00000000
       4       0.00000000     0.00000000     0.00000000     2.00000000

               Column   5     Column   6     Column   7     Column   8
       5       2.00000000

               Column   9     Column  10     Column  11     Column  12

               Column  13     Column  14     Column  15     Column  16

               Column  17     Column  18     Column  19     Column  20

               Column  21     Column  22     Column  23     Column  24

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7        eno   8
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    8   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno   9        eno  10        eno  11        eno  12        eno  13        eno  14        eno  15        eno  16
 emo    9   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   10   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   11   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   12   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   13   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   14   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   15   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   16   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno  17        eno  18        eno  19        eno  20        eno  21        eno  22        eno  23        eno  24
 emo   17   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   18   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   19   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   20   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   21   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   22   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   23   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   24   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   2.0000000      2.0000000      2.0000000      2.0000000      2.0000000
   0.0000000      0.0000000      0.0000000      0.0000000      0.0000000
   0.0000000      0.0000000      0.0000000      0.0000000      0.0000000
   0.0000000      0.0000000      0.0000000      0.0000000      0.0000000
   0.0000000      0.0000000      0.0000000      0.0000000

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7         no   8
  mo    1   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo    2   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo    3   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
  mo    5   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
  mo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01
  mo   24   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00

                no   9         no  10         no  11         no  12         no  13         no  14         no  15         no  16
  mo    8   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo    9   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo   10   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo   11   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo   12   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
  mo   13   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
  mo   14   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
  mo   15   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

                no  17         no  18         no  19         no  20         no  21         no  22         no  23         no  24
  mo   16   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo   17   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo   18   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo   19   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo   20   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
  mo   21   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
  mo   22   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
  mo   23   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01
  eigenvectors of nos in ao-basis, column    1
  0.0000000E+00  0.0000000E+00  0.0000000E+00  0.0000000E+00  0.9188403E+00  0.0000000E+00  0.0000000E+00  0.7204964E-01
  0.0000000E+00  0.1416552E-14 -0.1830008E-01  0.0000000E+00  0.0000000E+00  0.0000000E+00  0.0000000E+00  0.0000000E+00
  0.0000000E+00  0.3154239E-01  0.0000000E+00  0.0000000E+00  0.0000000E+00  0.0000000E+00  0.3154239E-01  0.0000000E+00
  eigenvectors of nos in ao-basis, column    2
  0.1000431E+01  0.2482130E-02 -0.1419915E-02  0.7781734E-14  0.0000000E+00 -0.2616439E-02 -0.3398887E-13  0.0000000E+00
  0.1645654E-02  0.0000000E+00  0.0000000E+00  0.3376021E-05  0.3967485E-14  0.8587611E-04 -0.5438131E-03  0.6927337E-03
 -0.6163335E-03  0.0000000E+00 -0.5060470E-03 -0.5438131E-03  0.6927337E-03  0.6163335E-03  0.0000000E+00 -0.5060470E-03
  eigenvectors of nos in ao-basis, column    3
 -0.8844768E-02  0.8674603E+00 -0.1297648E+00  0.6089053E-13  0.0000000E+00 -0.1174189E+00  0.6568151E-13  0.0000000E+00
  0.6892078E-01  0.0000000E+00  0.0000000E+00  0.3306414E-03 -0.5614132E-14  0.1611783E-02  0.3379710E+00 -0.1596253E+00
  0.3836313E-01  0.0000000E+00  0.2045257E-01  0.3379710E+00 -0.1596253E+00 -0.3836313E-01  0.0000000E+00  0.2045257E-01
  eigenvectors of nos in ao-basis, column    4
  0.4616762E-13  0.1371997E-12 -0.1998070E-12  0.7158542E+00  0.0000000E+00  0.1612520E-12 -0.1132335E+00  0.0000000E+00
  0.1762196E-13  0.0000000E+00  0.0000000E+00 -0.4966912E-15 -0.2722905E-01 -0.6927103E-15 -0.5635816E+00  0.1958934E+00
 -0.2355515E-01  0.0000000E+00 -0.3267843E-01  0.5635816E+00 -0.1958934E+00 -0.2355515E-01  0.0000000E+00  0.3267843E-01
  eigenvectors of nos in ao-basis, column    5
 -0.2070810E-02  0.2955600E+00  0.1824047E+00 -0.1630789E-12  0.0000000E+00  0.7982083E+00  0.8010964E-13  0.0000000E+00
 -0.1316750E-02  0.0000000E+00  0.0000000E+00 -0.5179269E-02  0.7124025E-14 -0.2323002E-02 -0.3484059E+00  0.1370807E+00
 -0.3161476E-01  0.0000000E+00  0.8311562E-02 -0.3484059E+00  0.1370807E+00  0.3161476E-01  0.0000000E+00  0.8311562E-02
  eigenvectors of nos in ao-basis, column    6
 -0.1687550E-13 -0.6976727E-13  0.1630899E-12  0.7277607E+00 -0.2063855E-20 -0.2635885E-13  0.8341926E+00 -0.6919644E-19
 -0.4339350E-13  0.8446766E-19  0.9486414E-19  0.5717816E-14 -0.1329366E+01  0.1531364E-13  0.1926243E+01 -0.6729536E+00
  0.6201121E+00  0.1166532E-18  0.4908839E+00 -0.1926243E+01  0.6729536E+00  0.6201121E+00  0.5660713E-19 -0.4908839E+00
  eigenvectors of nos in ao-basis, column    7
 -0.5211847E-01  0.1431756E+00  0.9350625E+00 -0.3212687E-12 -0.2651782E-16 -0.2693111E+00  0.2294226E-12  0.6097966E-16
 -0.2052835E+00 -0.1917530E-15 -0.1461129E-16  0.2090738E-02 -0.2192932E-13  0.5372106E-02 -0.9729573E-01 -0.7866691E+00
 -0.1885770E-01 -0.7385777E-16 -0.1685441E-01 -0.9729573E-01 -0.7866691E+00  0.1885770E-01  0.4913963E-17 -0.1685441E-01
  eigenvectors of nos in ao-basis, column    8
 -0.7525888E-13 -0.3615981E-12  0.3645833E-12 -0.4064254E+00  0.6143126E-16  0.1928467E-12 -0.4850854E+00 -0.4125148E-16
 -0.2481429E-12 -0.2448182E-16 -0.2575400E-16 -0.2153392E-14  0.2228350E-01  0.5706246E-14 -0.3830673E-01 -0.1445818E+01
 -0.2186979E-01 -0.2822300E-16 -0.1841129E-01  0.3830673E-01  0.1445818E+01 -0.2186979E-01 -0.4770756E-16  0.1841129E-01
  eigenvectors of nos in ao-basis, column    9
  0.8133027E-13  0.3949953E-12 -0.7847117E-12 -0.3878087E+00  0.9167769E-16 -0.4204844E-12 -0.3070854E+00 -0.1259703E-15
  0.3890379E-12  0.1296804E-15  0.1898080E-15 -0.1720719E-14 -0.1128502E+00 -0.4338076E-13 -0.1609845E+01  0.1468783E+01
  0.7759868E-01  0.1210108E-15  0.1590354E+00  0.1609845E+01 -0.1468783E+01  0.7759868E-01 -0.6645719E-16 -0.1590354E+00
  eigenvectors of nos in ao-basis, column   10
  0.6009755E-01  0.4905569E+00 -0.5872575E+00  0.1274610E-12 -0.4148646E-16 -0.4708518E+00  0.3521379E-12  0.3190314E-16
  0.2280175E+00  0.1095879E-15 -0.7270697E-16  0.1275336E-03  0.9135192E-13 -0.5458997E-01 -0.1328241E+01  0.1203349E+01
  0.3054664E+00  0.3029621E-16  0.5596935E-01 -0.1328241E+01  0.1203349E+01 -0.3054664E+00  0.4008870E-17  0.5596935E-01
  eigenvectors of nos in ao-basis, column   11
 -0.1171771E-02 -0.2149045E+00 -0.6662232E+00  0.4008957E-12  0.1272162E-15 -0.1100021E+01 -0.9162349E-12 -0.3235579E-15
  0.1802690E+01  0.4183721E-17 -0.8150565E-16 -0.3135894E-02  0.4076522E-13  0.2248781E-02  0.9562774E+00 -0.3614770E+00
  0.9392462E-01  0.3124207E-15  0.2398654E+00  0.9562774E+00 -0.3614770E+00 -0.9392462E-01  0.7696705E-16  0.2398654E+00
  eigenvectors of nos in ao-basis, column   12
 -0.1865057E-16 -0.1137790E-16  0.4372914E-15  0.2311638E-15  0.1409556E+01 -0.1502495E-16 -0.2137865E-17 -0.1685359E+01
 -0.2644028E-15  0.2619468E-14 -0.4378665E-02 -0.2227742E-16  0.2920819E-16  0.1910002E-16 -0.2112112E-15  0.1351552E-15
 -0.2569473E-18  0.5475679E-02  0.6083716E-17 -0.6046545E-15  0.1695000E-15  0.2264732E-15  0.5475679E-02 -0.1656291E-15
  eigenvectors of nos in ao-basis, column   13
  0.6769483E-13  0.1301094E-12 -0.7410199E-12 -0.1057726E+01 -0.1337786E-16 -0.2734863E-12  0.2269170E+01 -0.3776348E-16
  0.4949036E-12  0.2616358E-18  0.1265581E-15 -0.5399661E-14 -0.4799202E-01  0.1756388E-14  0.6650249E+00  0.5106979E+00
  0.3041767E+00  0.4995213E-16  0.1883073E+00 -0.6650249E+00 -0.5106979E+00  0.3041767E+00  0.8462649E-16 -0.1883073E+00
  eigenvectors of nos in ao-basis, column   14
  0.2091554E+00  0.1071580E+01 -0.1181893E+01  0.2281662E-12  0.3799142E-16  0.1763751E+00 -0.4822797E-12  0.1095098E-14
 -0.7970975E+00 -0.1337744E-15  0.5119991E-16 -0.3282162E-01  0.3109784E-13 -0.1255569E-03 -0.5377055E+00  0.4909475E+00
 -0.3086257E+00 -0.2410793E-17  0.5610314E+00 -0.5377055E+00  0.4909475E+00  0.3086257E+00 -0.2622459E-14  0.5610314E+00
  eigenvectors of nos in ao-basis, column   15
 -0.2948458E-15 -0.1543599E-14  0.1780871E-14  0.9962454E-16 -0.1039696E-13 -0.2101302E-15  0.1481156E-15  0.4657262E-13
  0.1318351E-14 -0.1289159E+00 -0.1874740E-13  0.5522893E-16  0.1935210E-16  0.1444038E-17  0.1106991E-14 -0.8594874E-15
  0.5663967E-15  0.6862173E+00 -0.1023255E-14  0.6499074E-15 -0.7245662E-15 -0.5506128E-15 -0.6862173E+00 -0.1231443E-14
  eigenvectors of nos in ao-basis, column   16
  0.1124007E-14  0.5190615E-14 -0.6901306E-14 -0.8661637E-16 -0.6688636E-01  0.7454745E-15  0.3311698E-16 -0.5949782E+00
 -0.7786919E-15  0.3482762E-13 -0.1598336E+00 -0.3942046E-16 -0.9962211E-17  0.5635439E-16  0.4204281E-15  0.4277037E-15
 -0.2403051E-15  0.7710122E+00  0.7735285E-15  0.5615269E-15  0.4375273E-15  0.5413873E-16  0.7710122E+00  0.6927030E-15
  eigenvectors of nos in ao-basis, column   17
 -0.7152723E+00 -0.3091336E+01  0.4825988E+01 -0.3740154E-13 -0.1754659E-15 -0.1908556E+00 -0.1542837E-11 -0.5249664E-15
 -0.8798870E+00 -0.3829584E-16 -0.1404789E-15 -0.3319167E-01 -0.4320577E-13 -0.5345980E-01 -0.1438511E+01  0.3295761E+00
 -0.3798899E+00  0.8949531E-15  0.3789287E-01 -0.1438511E+01  0.3295761E+00  0.3798899E+00  0.7857435E-15  0.3789287E-01
  eigenvectors of nos in ao-basis, column   18
  0.1254386E-11  0.5561242E-11 -0.8613367E-11  0.1732794E-01 -0.1896560E-17  0.4631480E-12 -0.9280177E+00 -0.1893239E-16
  0.1418057E-11 -0.2085640E-15 -0.1692849E-15  0.4726852E-13 -0.3028578E-01  0.7661613E-13 -0.6786853E+00  0.2460613E+00
  0.4823549E+00  0.1561555E-16 -0.6903832E+00  0.6786853E+00 -0.2460613E+00  0.4823549E+00  0.1887728E-16  0.6903832E+00
  eigenvectors of nos in ao-basis, column   19
  0.2629732E-12  0.1389613E-11  0.1945139E-12  0.1251840E+01 -0.2292075E-15 -0.1022185E-11 -0.4128890E+00  0.2467346E-15
  0.2119138E-12  0.1032580E-15 -0.4718214E-16 -0.2675803E-13  0.1472896E+00 -0.1247686E-12  0.5466282E+00  0.5377930E-01
  0.7276124E+00  0.1203127E-16  0.5654252E+00 -0.5466282E+00 -0.5377930E-01  0.7276124E+00  0.4511230E-16 -0.5654252E+00
  eigenvectors of nos in ao-basis, column   20
 -0.3068618E+00 -0.1538593E+01  0.1120258E+00  0.1321128E-11  0.1044481E-15  0.9807091E+00 -0.4518524E-12 -0.6503741E-16
 -0.2798825E+00 -0.1478313E-16 -0.3504715E-15  0.1641279E-01  0.1596852E-12  0.1132260E+00  0.8363142E+00 -0.2663892E+00
  0.7517673E+00 -0.3931772E-16  0.5305955E+00  0.8363142E+00 -0.2663892E+00 -0.7517673E+00 -0.1275737E-15  0.5305955E+00
  eigenvectors of nos in ao-basis, column   21
  0.2164986E-16  0.1198196E-16 -0.1021751E-14  0.2353443E-15  0.7240906E-02  0.3682210E-15 -0.3947190E-15 -0.3229147E+00
  0.2052502E-15  0.1822167E-12  0.1045714E+01 -0.1759278E-15  0.2730361E-16  0.2510648E-16  0.7780825E-15 -0.4884518E-15
  0.5190693E-15  0.4066771E+00  0.3040973E-16  0.5472104E-15  0.1565809E-15 -0.4277323E-15  0.4066771E+00  0.2941472E-15
  eigenvectors of nos in ao-basis, column   22
  0.1050887E-15  0.2357625E-15 -0.1977831E-14 -0.6737873E-16 -0.4970292E-14  0.5004176E-15 -0.9749280E-16  0.8383453E-13
  0.8871752E-15  0.1072280E+01 -0.1747192E-12 -0.2994074E-15 -0.3474570E-17 -0.3470492E-15  0.2216376E-14 -0.1323823E-14
  0.7823238E-15  0.3811176E+00 -0.1835534E-15  0.2143708E-14 -0.9715350E-15 -0.7529339E-15 -0.3811176E+00  0.2083587E-15
  eigenvectors of nos in ao-basis, column   23
  0.8598546E-01  0.3028774E+00 -0.8098987E+00 -0.2434517E-13 -0.8213752E-16  0.5946500E-01  0.7280752E-13  0.3190450E-15
  0.5469864E+00 -0.9636920E-15 -0.5635709E-15 -0.3159243E+00 -0.2776325E-13  0.6761892E-01  0.5941891E+00 -0.2522491E+00
  0.3743832E+00 -0.6404347E-15 -0.3122271E+00  0.5941891E+00 -0.2522491E+00 -0.3743832E+00  0.8279087E-16 -0.3122271E+00
  eigenvectors of nos in ao-basis, column   24
 -0.1166195E+00 -0.2416777E+00  0.2434799E+01 -0.1887791E-13 -0.7061382E-16 -0.6217072E+00 -0.4086266E-13  0.5771820E-16
 -0.6211116E+00  0.7576342E-15  0.8430511E-16  0.2467611E-01  0.3836063E-13  0.5861624E+00 -0.2169264E+01  0.8956706E+00
 -0.6236886E+00  0.2976308E-15 -0.5088806E+00 -0.2169264E+01  0.8956706E+00  0.6236886E+00 -0.2294555E-15 -0.5088806E+00


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***


 cosurf_and_phi
  2.466315385316 -0.061539892486  2.837440149000 -0.034370604262
  0.006359863210 -2.199287675687  3.114544749440 -0.062933376365
 -0.480526931872  1.061509344774  3.755560521090 -0.064148417365
  1.262739748330  2.872870034186  1.567866969040 -0.042635124542
  1.897949994968 -2.634366215369  0.570962659525 -0.015380963593
 -2.790166444607 -0.824331206483  2.170430257018 -0.019321468883
 -2.061296060778  2.493528161233  1.034251636268 -0.020819237183
 -0.002991703587  2.420846177436 -1.447622679829  0.011948642117
 -1.114435211431 -2.949559801670 -0.067898604227 -0.016077390874
  1.955709175590 -0.984462672186  3.123501329999 -0.046406945930
  1.000900154469 -1.706149424342  3.300403738319 -0.059492387183
  1.652947872482  0.360731428865  3.496571379926 -0.052788977835
  0.594940552015  0.670555958619  3.845537291721 -0.063791583883
  2.390078334690  1.202722009900  2.566708449184 -0.033900743429
  1.989334673621  2.229216060677  2.001028520754 -0.036548139281
  2.919143888229  0.378144896597  2.099775822683 -0.014391344019
  2.780367109803 -0.921046401167  2.130496470151 -0.019086870536
  2.449941849009 -2.011800733381  1.439000205618 -0.018021977970
 -0.231540571447 -1.263621120083  3.706953994699 -0.064793919300
 -0.352106389130 -0.107618694495  3.950679056227 -0.064905503156
  0.105380882100  1.878884201281  3.371423292662 -0.063982836097
  0.783266721831  2.590884380442  2.520834408126 -0.056610004193
  2.011402526412  2.551496219550  0.814855178005 -0.017769143502
  1.440220538007 -2.843720084632  1.356653310131 -0.037509340940
  0.871209499702 -2.660587439486  2.372618950166 -0.054793133390
 -2.947488997374  0.298617382934  2.058332654596 -0.012862348920
 -2.674954162172  1.563027715274  1.704200253745 -0.015931714437
 -1.353448939749  2.910920816403  0.212056013565 -0.018613926005
 -0.743422400795  2.810699746106 -0.731960510989 -0.004323943353
  0.099626804209  1.855073908563 -1.945822518898  0.029601871749
  0.019900458911 -1.968682238442 -1.864952523830  0.026388425644
 -0.601526683996 -2.669374282549 -1.032942810695  0.002601915334
 -1.976701730993 -2.578422698078  0.816296596419 -0.018629331230
 -2.525639241426 -1.850752473194  1.593331825886 -0.018492509989
 -1.124962231191 -1.881571875050  3.121019991266 -0.057552898647
 -2.117398618237 -1.513942036836  2.667866282109 -0.040465303694
 -2.336046769995 -0.152318885910  2.976109389266 -0.038215813431
 -1.478775916646  0.469153703894  3.577445396081 -0.055391145912
 -1.328587214463  1.668007282102  3.174271253825 -0.055657843358
 -1.771761195387  2.287385908353  2.202255939279 -0.042540089613
 -1.026730149698  3.017318998523  1.358608629268 -0.042473317530
  0.119020599620  3.173161356543  1.415159765853 -0.048074223322
  0.915237473358  3.108118624501  0.463299650838 -0.029353214118
  0.462814589486  2.837210699514 -0.795516582628 -0.005197620869
  1.026812912753 -3.017319139618  0.083991013862 -0.020597445718
 -0.070059393274 -3.176418651270  0.035610954337 -0.025715677406
 -0.970268948715 -3.083313212042  1.062474740253 -0.038809338274
 -0.534203029846 -2.828286137107  2.231267851728 -0.055654832963
  0.871897873699 -0.575451888002  3.799144800425 -0.061994639249
  1.408719892640  1.640288870679  3.148120355694 -0.054583395021
  2.650053602975  1.633766196698  1.655441764425 -0.015879240488
  1.964286464189 -2.001027831890  2.365093852582 -0.040762147359
 -1.342877128538 -0.760711330777  3.581800927521 -0.057046417880
 -0.531365927285  2.661712102822  2.509437292848 -0.057962221475
  1.142904167226  2.885766749848 -0.243475252462 -0.011486698850
  0.342129526197 -3.189261637688  1.246854200824 -0.045388289319
 -2.346459445268  1.135413320199  2.662806558936 -0.035693232547
 -0.258565605057  3.205542092831  0.249849913336 -0.029543194065
  0.450566961335 -2.699565911226 -1.032013446782  0.001225636567
 -1.684533476150 -2.430522251364  2.070179818920 -0.042598308929
 -3.368784049130  0.036613212616 -1.854972615293  0.058700004553
 -3.278396927799  1.580834404538 -0.078603229040  0.032623887513
 -3.656110378332 -0.713812229513  0.361831391088  0.033722006190
 -2.408052717333 -2.075622947728 -1.226189024363  0.036814251624
 -1.295829484895 -0.567704086865 -2.747607986898  0.054759631720
 -1.846557165647  1.681693338816 -2.099716493181  0.045977223701
 -3.831862607217  0.357468890277 -0.654813288033  0.049463690965
 -3.527736967644 -1.045671231370 -1.064852892071  0.050344088398
 -2.622906831544 -1.024738705101 -2.241124222290  0.055870497229
 -2.402781990555  0.378472303440 -2.579763034114  0.058853455544
 -3.126606927219  1.313275381963 -1.541857021673  0.051509509531
 -3.633182667875  0.531520753705  0.561856665829  0.030800813749
 -3.086813000261 -1.794874586082 -0.179700355757  0.030594537327
 -1.615880090830 -1.674393887320 -2.147504235692  0.044936561186
 -1.240258025012  0.765881087348 -2.687977655831  0.053488967638
 -2.430292517958  2.154437082790 -0.969924007583  0.032557462001
  3.368776503197 -0.036639040867 -1.854966842961  0.058700404880
  3.278392620256 -1.580850766330 -0.078589062660  0.032623186712
  3.656106873706  0.713798214804  0.361832640492  0.033722266754
  2.408046317685  2.075600470298 -1.226192756897  0.036815137409
  1.295820311657  0.567673501678 -2.747601655947  0.054760163553
  1.846549173548 -1.681720471299 -2.099699178989  0.045976594295
  3.831857249217 -0.357488322766 -0.654806650060  0.049463717170
  3.527730862121  1.045649613724 -1.064853177123  0.050344831858
  2.622898581638  1.024710819001 -2.241122746235  0.055871358329
  2.402773123306 -0.378501994157 -2.579753678917  0.058853673796
  3.126599952113 -1.313299541573 -1.541844004398  0.051509134312
  3.633179527908 -0.531533702443  0.561864593522  0.030800511732
  3.086808508398  1.794857685487 -0.179703829578  0.030595206142
  1.615872011596  1.674366500124 -2.147504385867  0.044937411492
  1.240248960489 -0.765911354740 -2.687964116774  0.053488827352
  2.430286585511 -2.154458194501 -0.969905238283  0.032556543067
  0.000006852884  1.035694667087 -2.361676372823  0.046979034298
 -0.298806015181  0.969607486526 -2.408809074493  0.048501242400
  0.298815704890  0.969607820141 -2.408807386530  0.048501318921
  0.000007656756 -1.035723195601 -2.361670853435  0.046978535694
 -0.298805262603 -0.969636498141 -2.408803907288  0.048500758230
  0.298816457468 -0.969636367899 -2.408802219325  0.048500852478
 -0.667712940797 -1.245186997899 -2.338574529312  0.046522342270
 -1.218112188165 -2.025138759161 -1.616566947615  0.032936789143
 -2.084175601108 -2.294008802021 -0.480477682822  0.019038748523
 -2.876631896395 -1.658047550445  0.559052047065  0.016971188367
 -3.282909221331 -0.368099862162  1.091996180628  0.019662927091
 -3.142757910518  1.067034798172  0.908143333087  0.018448454351
 -2.511458431963  2.081290260960  0.080011352455  0.016716562473
 -1.638016880752  2.274609499719 -1.065756198713  0.024575829877
 -0.866948462969  1.570740798135 -2.077229430584  0.042722811879
 -0.467915446486 -1.694563900014 -1.941501402531  0.035437689916
 -1.299753513329 -2.453901457341 -0.850306853788  0.010627894760
 -2.242033801688 -2.245340287831  0.385760998463  0.000604800965
 -2.923088754321 -1.151144046328  1.279154738758  0.003603462701
 -3.074287016292  0.397098864814  1.477489335582  0.005422302861
 -2.635990824421  1.788708515315  0.902534843950  0.001304837506
 -1.781079179779  2.474786487342 -0.218927030025  0.003408856753
 -0.846758459860  2.184720175398 -1.444553386477  0.021850296046
 -0.255852300976  1.360631011730 -2.219692209228  0.041864758849
  0.667723897920  1.245157743773 -2.338577856151  0.046523113973
  1.218123388571  2.025110412626 -1.616576841774  0.032937948849
  2.084186643139  2.293984793080 -0.480493513306  0.019040012419
  2.876642520254  1.658030225134  0.559035126479  0.016972220142
  3.282919570196  0.368088739237  1.091983758387  0.019663385996
  3.142768358070 -1.067043033853  0.908139383421  0.018448361562
  2.511469270857 -2.081300494445  0.080016452498  0.016716167673
  1.638028059662 -2.274626132712 -1.065745290088  0.024575234678
  0.866959534092 -1.570765766224 -2.077218589767  0.042722258984
  0.467932162408  1.694535407938 -1.941510815499  0.035438633029
  1.299770347024  2.453875604722 -0.850324284820  0.010629136127
  2.242050270258  2.245320705184  0.385739526123  0.000606082864
  2.923104801922  1.151131828431  1.279135167181  0.003604423008
  3.074302957696 -0.397105856761  1.477477125466  0.005422740189
  2.636007061705 -1.788714946455  0.902532611959  0.001304993646
  1.781095866662 -2.474797662024 -0.218920775640  0.003408775380
  0.846775315928 -2.184739733041 -1.444543821640  0.021849845672
  0.255868904327 -1.360657816862 -2.219684436601  0.041864253161
  0.427045418766 -0.580992742084 -2.585105089006  0.052119483206
  0.427044967836  0.580963354323 -2.585108185092  0.052119763747
 -0.427035838172 -0.000014913339 -2.543076305315  0.056973507105
 end_of_phi


 nsubv after electrostatic potential =  0

  Confirmation of dielectric energy for ground state
  edielnew =   0.


 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************


 cosurf_and_qcos_from cosmo(3
  2.466315385316 -0.061539892486  2.837440149000  0.002077140923
  0.006359863210 -2.199287675687  3.114544749440  0.005886102891
 -0.480526931872  1.061509344774  3.755560521090  0.005343431650
  1.262739748330  2.872870034186  1.567866969040  0.004810827954
  1.897949994968 -2.634366215369  0.570962659525  0.001824604073
 -2.790166444607 -0.824331206483  2.170430257018  0.000891852477
 -2.061296060778  2.493528161233  1.034251636268  0.002780625884
 -0.002991703587  2.420846177436 -1.447622679829  0.000942404166
 -1.114435211431 -2.949559801670 -0.067898604227  0.002957344011
  1.955709175590 -0.984462672186  3.123501329999  0.004303493256
  1.000900154469 -1.706149424342  3.300403738319  0.005709657447
  1.652947872482  0.360731428865  3.496571379926  0.004824755304
  0.594940552015  0.670555958619  3.845537291721  0.005213072630
  2.390078334690  1.202722009900  2.566708449184  0.003136366764
  1.989334673621  2.229216060677  2.001028520754  0.003276569962
  2.919143888229  0.378144896597  2.099775822683  0.000362185917
  2.780367109803 -0.921046401167  2.130496470151  0.001299817186
  2.449941849009 -2.011800733381  1.439000205618  0.001784061155
 -0.231540571447 -1.263621120083  3.706953994699  0.006080026411
 -0.352106389130 -0.107618694495  3.950679056227  0.005825527335
  0.105380882100  1.878884201281  3.371423292662  0.007583514818
  0.783266721831  2.590884380442  2.520834408126  0.006804894924
  2.011402526412  2.551496219550  0.814855178005  0.002321750227
  1.440220538007 -2.843720084632  1.356653310131  0.004418526225
  0.871209499702 -2.660587439486  2.372618950166  0.007350481653
 -2.947488997374  0.298617382934  2.058332654596  0.000344890719
 -2.674954162172  1.563027715274  1.704200253745  0.001057208535
 -1.353448939749  2.910920816403  0.212056013565  0.003277208180
 -0.743422400795  2.810699746106 -0.731960510989  0.002320944059
  0.099626804209  1.855073908563 -1.945822518898 -0.000034664968
  0.019900458911 -1.968682238442 -1.864952523830 -0.000164829984
 -0.601526683996 -2.669374282549 -1.032942810695  0.001438213030
 -1.976701730993 -2.578422698078  0.816296596419  0.002478056129
 -2.525639241426 -1.850752473194  1.593331825886  0.001586154715
 -1.124962231191 -1.881571875050  3.121019991266  0.005627637494
 -2.117398618237 -1.513942036836  2.667866282109  0.003675603509
 -2.336046769995 -0.152318885910  2.976109389266  0.003739117315
 -1.478775916646  0.469153703894  3.577445396081  0.004767281819
 -1.328587214463  1.668007282102  3.174271253825  0.005749609935
 -1.771761195387  2.287385908353  2.202255939279  0.005523478927
 -1.026730149698  3.017318998523  1.358608629268  0.005533754659
  0.119020599620  3.173161356543  1.415159765853  0.006805939132
  0.915237473358  3.108118624501  0.463299650838  0.004775203766
  0.462814589486  2.837210699514 -0.795516582628  0.002413969479
  1.026812912753 -3.017319139618  0.083991013862  0.004040918572
 -0.070059393274 -3.176418651270  0.035610954337  0.004601054555
 -0.970268948715 -3.083313212042  1.062474740253  0.006367477508
 -0.534203029846 -2.828286137107  2.231267851728  0.007644016918
  0.871897873699 -0.575451888002  3.799144800425  0.006059799258
  1.408719892640  1.640288870679  3.148120355694  0.006574562377
  2.650053602975  1.633766196698  1.655441764425  0.000994969243
  1.964286464189 -2.001027831890  2.365093852582  0.004450768811
 -1.342877128538 -0.760711330777  3.581800927521  0.004977287367
 -0.531365927285  2.661712102822  2.509437292848  0.007677209227
  1.142904167226  2.885766749848 -0.243475252462  0.001414745224
  0.342129526197 -3.189261637688  1.246854200824  0.006634039931
 -2.346459445268  1.135413320199  2.662806558936  0.003234585825
 -0.258565605057  3.205542092831  0.249849913336  0.004410828882
  0.450566961335 -2.699565911226 -1.032013446782  0.002504483837
 -1.684533476150 -2.430522251364  2.070179818920  0.004745537968
 -3.368784049130  0.036613212616 -1.854972615293 -0.007557421010
 -3.278396927799  1.580834404538 -0.078603229040 -0.005581037803
 -3.656110378332 -0.713812229513  0.361831391088 -0.006306411298
 -2.408052717333 -2.075622947728 -1.226189024363 -0.005148558275
 -1.295829484895 -0.567704086865 -2.747607986898 -0.005527781711
 -1.846557165647  1.681693338816 -2.099716493181 -0.005735737583
 -3.831862607217  0.357468890277 -0.654813288033 -0.008196962323
 -3.527736967644 -1.045671231370 -1.064852892071 -0.008080312575
 -2.622906831544 -1.024738705101 -2.241124222290 -0.007916206506
 -2.402781990555  0.378472303440 -2.579763034114 -0.007684768672
 -3.126606927219  1.313275381963 -1.541857021673 -0.008184080278
 -3.633182667875  0.531520753705  0.561856665829 -0.004431846792
 -3.086813000261 -1.794874586082 -0.179700355757 -0.004352372974
 -1.615880090830 -1.674393887320 -2.147504235692 -0.004339147011
 -1.240258025012  0.765881087348 -2.687977655831 -0.004539252871
 -2.430292517958  2.154437082790 -0.969924007583 -0.003880542196
  3.368776503197 -0.036639040867 -1.854966842961 -0.007563511268
  3.278392620256 -1.580850766330 -0.078589062660 -0.005567179534
  3.656106873706  0.713798214804  0.361832640492 -0.006301657824
  2.408046317685  2.075600470298 -1.226192756897 -0.005156567393
  1.295820311657  0.567673501678 -2.747601655947 -0.005915185537
  1.846549173548 -1.681720471299 -2.099699178989 -0.005693548432
  3.831857249217 -0.357488322766 -0.654806650060 -0.008196882697
  3.527730862121  1.045649613724 -1.064853177123 -0.008083759097
  2.622898581638  1.024710819001 -2.241122746235 -0.007904117440
  2.402773123306 -0.378501994157 -2.579753678917 -0.007676144450
  3.126599952113 -1.313299541573 -1.541844004398 -0.008188508547
  3.633179527908 -0.531533702443  0.561864593522 -0.004427815261
  3.086808508398  1.794857685487 -0.179703829578 -0.004350159929
  1.615872011596  1.674366500124 -2.147504385867 -0.004309960377
  1.240248960489 -0.765911354740 -2.687964116774 -0.004778346333
  2.430286585511 -2.154458194501 -0.969905238283 -0.003866470910
  0.000006852884  1.035694667087 -2.361676372823 -0.000239007612
 -0.298806015181  0.969607486526 -2.408809074493 -0.000293708784
  0.298815704890  0.969607820141 -2.408807386530 -0.000214463779
  0.000007656756 -1.035723195601 -2.361670853435 -0.000233904303
 -0.298805262603 -0.969636498141 -2.408803907288 -0.000279855259
  0.298816457468 -0.969636367899 -2.408802219325 -0.000212921335
 -0.667712940797 -1.245186997899 -2.338574529312 -0.000825090617
 -1.218112188165 -2.025138759161 -1.616566947615 -0.001238820868
 -2.084175601108 -2.294008802021 -0.480477682822 -0.000844228118
 -2.876631896395 -1.658047550445  0.559052047065 -0.001317432647
 -3.282909221331 -0.368099862162  1.091996180628 -0.001790315553
 -3.142757910518  1.067034798172  0.908143333087 -0.001592383423
 -2.511458431963  2.081290260960  0.080011352455 -0.001097655629
 -1.638016880752  2.274609499719 -1.065756198713 -0.000994588871
 -0.866948462969  1.570740798135 -2.077229430584 -0.001724722506
 -0.467915446486 -1.694563900014 -1.941501402531 -0.001171879374
 -1.299753513329 -2.453901457341 -0.850306853788 -0.000117484767
 -2.242033801688 -2.245340287831  0.385760998463 -0.000257669475
 -2.923088754321 -1.151144046328  1.279154738758 -0.000890784280
 -3.074287016292  0.397098864814  1.477489335582 -0.001169906361
 -2.635990824421  1.788708515315  0.902534843950 -0.000502851595
 -1.781079179779  2.474786487342 -0.218927030025 -0.000093859691
 -0.846758459860  2.184720175398 -1.444553386477 -0.000652386112
 -0.255852300976  1.360631011730 -2.219692209228 -0.000759990947
  0.667723897920  1.245157743773 -2.338577856151 -0.000766674937
  1.218123388571  2.025110412626 -1.616576841774 -0.001250815474
  2.084186643139  2.293984793080 -0.480493513306 -0.000822902715
  2.876642520254  1.658030225134  0.559035126479 -0.001312713855
  3.282919570196  0.368088739237  1.091983758387 -0.001778970947
  3.142768358070 -1.067043033853  0.908139383421 -0.001586275692
  2.511469270857 -2.081300494445  0.080016452498 -0.001040080399
  1.638028059662 -2.274626132712 -1.065745290088 -0.000951834357
  0.866959534092 -1.570765766224 -2.077218589767 -0.001680426803
  0.467932162408  1.694535407938 -1.941510815499 -0.001228745758
  1.299770347024  2.453875604722 -0.850324284820 -0.000121322384
  2.242050270258  2.245320705184  0.385739526123 -0.000190285053
  2.923104801922  1.151131828431  1.279135167181 -0.000915127196
  3.074302957696 -0.397105856761  1.477477125466 -0.001199228329
  2.636007061705 -1.788714946455  0.902532611959 -0.000545964278
  1.781095866662 -2.474797662024 -0.218920775640 -0.000145564759
  0.846775315928 -2.184739733041 -1.444543821640 -0.000708902932
  0.255868904327 -1.360657816862 -2.219684436601 -0.000707982396
  0.427045418766 -0.580992742084 -2.585105089006 -0.001930768153
  0.427044967836  0.580963354323 -2.585108185092 -0.001810762163
 -0.427035838172 -0.000014913339 -2.543076305315 -0.005292540966
 end_of_qcos

 sum of the negative charges =  -0.240145551

 sum of the positive charges =   0.235255592

 total sum =  -0.00488995917

 *** qcos is copied to qcosdalton ***

 fepsi =   1.
 *** qcosdalton is mult by fepsi ***
 cosurf and qcosdalton
  2.466315385316 -0.061539892486  2.837440149000  0.002077140923
  0.006359863210 -2.199287675687  3.114544749440  0.005886102891
 -0.480526931872  1.061509344774  3.755560521090  0.005343431650
  1.262739748330  2.872870034186  1.567866969040  0.004810827954
  1.897949994968 -2.634366215369  0.570962659525  0.001824604073
 -2.790166444607 -0.824331206483  2.170430257018  0.000891852477
 -2.061296060778  2.493528161233  1.034251636268  0.002780625884
 -0.002991703587  2.420846177436 -1.447622679829  0.000942404166
 -1.114435211431 -2.949559801670 -0.067898604227  0.002957344011
  1.955709175590 -0.984462672186  3.123501329999  0.004303493256
  1.000900154469 -1.706149424342  3.300403738319  0.005709657447
  1.652947872482  0.360731428865  3.496571379926  0.004824755304
  0.594940552015  0.670555958619  3.845537291721  0.005213072630
  2.390078334690  1.202722009900  2.566708449184  0.003136366764
  1.989334673621  2.229216060677  2.001028520754  0.003276569962
  2.919143888229  0.378144896597  2.099775822683  0.000362185917
  2.780367109803 -0.921046401167  2.130496470151  0.001299817186
  2.449941849009 -2.011800733381  1.439000205618  0.001784061155
 -0.231540571447 -1.263621120083  3.706953994699  0.006080026411
 -0.352106389130 -0.107618694495  3.950679056227  0.005825527335
  0.105380882100  1.878884201281  3.371423292662  0.007583514818
  0.783266721831  2.590884380442  2.520834408126  0.006804894924
  2.011402526412  2.551496219550  0.814855178005  0.002321750227
  1.440220538007 -2.843720084632  1.356653310131  0.004418526225
  0.871209499702 -2.660587439486  2.372618950166  0.007350481653
 -2.947488997374  0.298617382934  2.058332654596  0.000344890719
 -2.674954162172  1.563027715274  1.704200253745  0.001057208535
 -1.353448939749  2.910920816403  0.212056013565  0.003277208180
 -0.743422400795  2.810699746106 -0.731960510989  0.002320944059
  0.099626804209  1.855073908563 -1.945822518898 -0.000034664968
  0.019900458911 -1.968682238442 -1.864952523830 -0.000164829984
 -0.601526683996 -2.669374282549 -1.032942810695  0.001438213030
 -1.976701730993 -2.578422698078  0.816296596419  0.002478056129
 -2.525639241426 -1.850752473194  1.593331825886  0.001586154715
 -1.124962231191 -1.881571875050  3.121019991266  0.005627637494
 -2.117398618237 -1.513942036836  2.667866282109  0.003675603509
 -2.336046769995 -0.152318885910  2.976109389266  0.003739117315
 -1.478775916646  0.469153703894  3.577445396081  0.004767281819
 -1.328587214463  1.668007282102  3.174271253825  0.005749609935
 -1.771761195387  2.287385908353  2.202255939279  0.005523478927
 -1.026730149698  3.017318998523  1.358608629268  0.005533754659
  0.119020599620  3.173161356543  1.415159765853  0.006805939132
  0.915237473358  3.108118624501  0.463299650838  0.004775203766
  0.462814589486  2.837210699514 -0.795516582628  0.002413969479
  1.026812912753 -3.017319139618  0.083991013862  0.004040918572
 -0.070059393274 -3.176418651270  0.035610954337  0.004601054555
 -0.970268948715 -3.083313212042  1.062474740253  0.006367477508
 -0.534203029846 -2.828286137107  2.231267851728  0.007644016918
  0.871897873699 -0.575451888002  3.799144800425  0.006059799258
  1.408719892640  1.640288870679  3.148120355694  0.006574562377
  2.650053602975  1.633766196698  1.655441764425  0.000994969243
  1.964286464189 -2.001027831890  2.365093852582  0.004450768811
 -1.342877128538 -0.760711330777  3.581800927521  0.004977287367
 -0.531365927285  2.661712102822  2.509437292848  0.007677209227
  1.142904167226  2.885766749848 -0.243475252462  0.001414745224
  0.342129526197 -3.189261637688  1.246854200824  0.006634039931
 -2.346459445268  1.135413320199  2.662806558936  0.003234585825
 -0.258565605057  3.205542092831  0.249849913336  0.004410828882
  0.450566961335 -2.699565911226 -1.032013446782  0.002504483837
 -1.684533476150 -2.430522251364  2.070179818920  0.004745537968
 -3.368784049130  0.036613212616 -1.854972615293 -0.007557421010
 -3.278396927799  1.580834404538 -0.078603229040 -0.005581037803
 -3.656110378332 -0.713812229513  0.361831391088 -0.006306411298
 -2.408052717333 -2.075622947728 -1.226189024363 -0.005148558275
 -1.295829484895 -0.567704086865 -2.747607986898 -0.005527781711
 -1.846557165647  1.681693338816 -2.099716493181 -0.005735737583
 -3.831862607217  0.357468890277 -0.654813288033 -0.008196962323
 -3.527736967644 -1.045671231370 -1.064852892071 -0.008080312575
 -2.622906831544 -1.024738705101 -2.241124222290 -0.007916206506
 -2.402781990555  0.378472303440 -2.579763034114 -0.007684768672
 -3.126606927219  1.313275381963 -1.541857021673 -0.008184080278
 -3.633182667875  0.531520753705  0.561856665829 -0.004431846792
 -3.086813000261 -1.794874586082 -0.179700355757 -0.004352372974
 -1.615880090830 -1.674393887320 -2.147504235692 -0.004339147011
 -1.240258025012  0.765881087348 -2.687977655831 -0.004539252871
 -2.430292517958  2.154437082790 -0.969924007583 -0.003880542196
  3.368776503197 -0.036639040867 -1.854966842961 -0.007563511268
  3.278392620256 -1.580850766330 -0.078589062660 -0.005567179534
  3.656106873706  0.713798214804  0.361832640492 -0.006301657824
  2.408046317685  2.075600470298 -1.226192756897 -0.005156567393
  1.295820311657  0.567673501678 -2.747601655947 -0.005915185537
  1.846549173548 -1.681720471299 -2.099699178989 -0.005693548432
  3.831857249217 -0.357488322766 -0.654806650060 -0.008196882697
  3.527730862121  1.045649613724 -1.064853177123 -0.008083759097
  2.622898581638  1.024710819001 -2.241122746235 -0.007904117440
  2.402773123306 -0.378501994157 -2.579753678917 -0.007676144450
  3.126599952113 -1.313299541573 -1.541844004398 -0.008188508547
  3.633179527908 -0.531533702443  0.561864593522 -0.004427815261
  3.086808508398  1.794857685487 -0.179703829578 -0.004350159929
  1.615872011596  1.674366500124 -2.147504385867 -0.004309960377
  1.240248960489 -0.765911354740 -2.687964116774 -0.004778346333
  2.430286585511 -2.154458194501 -0.969905238283 -0.003866470910
  0.000006852884  1.035694667087 -2.361676372823 -0.000239007612
 -0.298806015181  0.969607486526 -2.408809074493 -0.000293708784
  0.298815704890  0.969607820141 -2.408807386530 -0.000214463779
  0.000007656756 -1.035723195601 -2.361670853435 -0.000233904303
 -0.298805262603 -0.969636498141 -2.408803907288 -0.000279855259
  0.298816457468 -0.969636367899 -2.408802219325 -0.000212921335
 -0.667712940797 -1.245186997899 -2.338574529312 -0.000825090617
 -1.218112188165 -2.025138759161 -1.616566947615 -0.001238820868
 -2.084175601108 -2.294008802021 -0.480477682822 -0.000844228118
 -2.876631896395 -1.658047550445  0.559052047065 -0.001317432647
 -3.282909221331 -0.368099862162  1.091996180628 -0.001790315553
 -3.142757910518  1.067034798172  0.908143333087 -0.001592383423
 -2.511458431963  2.081290260960  0.080011352455 -0.001097655629
 -1.638016880752  2.274609499719 -1.065756198713 -0.000994588871
 -0.866948462969  1.570740798135 -2.077229430584 -0.001724722506
 -0.467915446486 -1.694563900014 -1.941501402531 -0.001171879374
 -1.299753513329 -2.453901457341 -0.850306853788 -0.000117484767
 -2.242033801688 -2.245340287831  0.385760998463 -0.000257669475
 -2.923088754321 -1.151144046328  1.279154738758 -0.000890784280
 -3.074287016292  0.397098864814  1.477489335582 -0.001169906361
 -2.635990824421  1.788708515315  0.902534843950 -0.000502851595
 -1.781079179779  2.474786487342 -0.218927030025 -0.000093859691
 -0.846758459860  2.184720175398 -1.444553386477 -0.000652386112
 -0.255852300976  1.360631011730 -2.219692209228 -0.000759990947
  0.667723897920  1.245157743773 -2.338577856151 -0.000766674937
  1.218123388571  2.025110412626 -1.616576841774 -0.001250815474
  2.084186643139  2.293984793080 -0.480493513306 -0.000822902715
  2.876642520254  1.658030225134  0.559035126479 -0.001312713855
  3.282919570196  0.368088739237  1.091983758387 -0.001778970947
  3.142768358070 -1.067043033853  0.908139383421 -0.001586275692
  2.511469270857 -2.081300494445  0.080016452498 -0.001040080399
  1.638028059662 -2.274626132712 -1.065745290088 -0.000951834357
  0.866959534092 -1.570765766224 -2.077218589767 -0.001680426803
  0.467932162408  1.694535407938 -1.941510815499 -0.001228745758
  1.299770347024  2.453875604722 -0.850324284820 -0.000121322384
  2.242050270258  2.245320705184  0.385739526123 -0.000190285053
  2.923104801922  1.151131828431  1.279135167181 -0.000915127196
  3.074302957696 -0.397105856761  1.477477125466 -0.001199228329
  2.636007061705 -1.788714946455  0.902532611959 -0.000545964278
  1.781095866662 -2.474797662024 -0.218920775640 -0.000145564759
  0.846775315928 -2.184739733041 -1.444543821640 -0.000708902932
  0.255868904327 -1.360657816862 -2.219684436601 -0.000707982396
  0.427045418766 -0.580992742084 -2.585105089006 -0.001930768153
  0.427044967836  0.580963354323 -2.585108185092 -0.001810762163
 -0.427035838172 -0.000014913339 -2.543076305315 -0.005292540966
 end of cosurf and qcosdalton

 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 ** Total nuclear repulsion energy ** =   9.35969258317765
 screening nuclear repulsion energy   0.00672980141

 Total-screening nuclear repulsion energy   9.35296278


 Adding T+Vsolv ...
 maxdens  300
 *** End of DALTON-COSMO calculation ***

  original map vector  20 21 22 23 24 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17
 18 19 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0


          sorted Vsolv matrix  block   1

               nbf   1        nbf   2        nbf   3        nbf   4        nbf   5        nbf   6        nbf   7        nbf   8
  nbf   1    -3.31914420
  nbf   2     0.00001149    -3.09165276
  nbf   3     0.00001644    -0.98129809    -4.10225551
  nbf   4    -1.01838902     0.00000395     0.00001321    -4.07882880
  nbf   5    -0.42307970     0.00001113     0.00000877    -0.44094083    -4.75210498
  nbf   6    -0.00000456    -0.00000321     0.00000198     0.00000123     0.00000154    -5.34846983
  nbf   7    -0.00000445    -1.17036607    -0.38197374     0.00000625     0.00000665    -0.00000251    -4.33669666
  nbf   8    -0.41249016     0.00000856     0.00002461     0.03978337     0.09581695    -0.00000399    -0.00000434    -3.58739254
  nbf   9     0.00000251     0.00000076     0.00000160     0.00000112    -0.00000001    -0.00000179     0.00000072    -0.00000215
  nbf  10    -0.00000006     0.00000028     0.00000111     0.00000059     0.00000556     0.24713661     0.00000112    -0.00000059
  nbf  11     0.71247225     0.00000234     0.00000542    -0.06900573    -0.45060694     0.00000213     0.00000216     0.37418689
  nbf  12    -0.00001917    -0.02232513    -0.02659577    -0.00000043    -0.00003655     0.00000083    -0.10346762    -0.00003324
  nbf  13     0.00000918     0.51184494     1.29172158     0.00000746     0.00000411     0.00000168     1.13369988     0.00000376
  nbf  14     0.60219843     0.00000583     0.00000220     1.39132613     0.61428354     0.00000208     0.00000621     0.29798366
  nbf  15     0.00000094     0.00000000    -0.00000012     0.00000142     0.00000174     0.06480194     0.00000112     0.00000067
  nbf  16     0.00000136     0.00000164     0.00000250     0.00000043     0.00000033    -0.00000158    -0.00000024    -0.00000069
  nbf  17     0.01612412    -0.00000581    -0.00001223     0.28459188    -0.03476642     0.00000096     0.00000081    -0.91900258
  nbf  18    -0.11618280    -0.00000552    -0.00000171    -0.29344839    -0.90081959     0.00000035    -0.00000072    -0.23500848
  nbf  19    -0.00000575     0.16688253    -0.06182044     0.00000062    -0.00000076    -0.00000003     1.19156765    -0.00000660
  nbf  20     0.18301633     0.00000000    -0.00000004     0.16556699    -0.11845848    -0.00000041     0.00000003     0.20495675
  nbf  21    -0.99280126    -0.00000429    -0.00001407    -0.72366601     0.04776843     0.00000154     0.00000375    -0.60117844
  nbf  22     0.00000609     1.23621911     0.65592338     0.00000826     0.00000888    -0.00000222     1.45021768     0.00001087
  nbf  23     0.54070615     0.00000627     0.00001113     0.73124756     1.71697654     0.00000018     0.00000304    -0.42066359
  nbf  24     0.00000178     0.00000130    -0.00000138    -0.00000151     0.00000006    -2.13414477     0.00000145     0.00000213

               nbf   9        nbf  10        nbf  11        nbf  12        nbf  13        nbf  14        nbf  15        nbf  16
  nbf   9    -3.42330119
  nbf  10     0.00001544    -3.17820739
  nbf  11    -0.00000285    -0.00000176    -4.03664046
  nbf  12    -0.00000380     0.00000151    -0.00000703    -2.63324986
  nbf  13    -0.00000179    -0.00000166     0.00000077     0.09049476    -3.62338375
  nbf  14    -0.00000167    -0.00000147    -0.18644350     0.00000088     0.00001730    -3.66481183
  nbf  15     0.00000477     0.90716980    -0.00000062     0.00000062    -0.00000001     0.00000029    -3.66998914
  nbf  16     0.90196966     0.00000546    -0.00000034    -0.00000081    -0.00000062    -0.00000024     0.00000529    -3.63954921
  nbf  17     0.00000114     0.00000039    -0.42317798     0.00002009    -0.00000454     0.04521866    -0.00000006     0.00000153
  nbf  18     0.00000055    -0.00000140     0.56827758     0.00000849    -0.00000750    -0.46485052    -0.00000078     0.00000045
  nbf  19    -0.00000106    -0.00000056    -0.00000037    -0.08859802     0.44902125     0.00000677    -0.00000098    -0.00000003
  nbf  20    -0.00000001     0.00000003    -0.38282204     0.00000000     0.00000007    -0.26333522    -0.00000001     0.00000005
  nbf  21    -0.00000264    -0.00000146     1.08742287     0.00001202     0.00000194     0.59818017    -0.00000032     0.00000060
  nbf  22     0.00000336     0.00000201     0.00000545     0.01884590    -0.54187387     0.00000049     0.00000088    -0.00000140
  nbf  23     0.00000105     0.00000493     0.54848474    -0.00002353     0.00000186    -0.34225951    -0.00000260     0.00000110
  nbf  24     0.00000232     0.07788137    -0.00000187    -0.00000053    -0.00000112    -0.00000018     0.08127911     0.00000222

               nbf  17        nbf  18        nbf  19        nbf  20        nbf  21        nbf  22        nbf  23        nbf  24
  nbf  17    -3.38750818
  nbf  18    -0.05583230    -3.03509862
  nbf  19     0.00000544    -0.00000649    -2.89433126
  nbf  20     0.00966744     0.05945761    -0.00000019   -33.05174917
  nbf  21    -0.01750684    -0.18419566     0.00000123     0.58204752    -7.87312296
  nbf  22    -0.00000520     0.00000112    -0.45843066    -0.00000003    -0.00000711    -6.71622218
  nbf  23    -0.12262671     0.30572537    -0.00000404     0.18758237    -0.25069844     0.00001242    -6.95563276
  nbf  24    -0.00000081     0.00000128    -0.00000009    -0.00000037    -0.00000420     0.00000224    -0.00000106    -7.11691032
 insert_onel_diag: nmin2=   380

 onel.diag.
 -0.331914E+01-0.309165E+01-0.410226E+01-0.407883E+01-0.475210E+01-0.534847E+01-0.433670E+01-0.358739E+01-0.342330E+01-0.317821E+01
 -0.403664E+01-0.263325E+01-0.362338E+01-0.366481E+01-0.366999E+01-0.363955E+01-0.338751E+01-0.303510E+01-0.289433E+01-0.330517E+02
 -0.787312E+01-0.671622E+01-0.695563E+01-0.711691E+01

 i,start,strti,fin,filind,bufszi,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12        64         6       380       380
 i,start,strti,fin,filind,bufszi,nmbuf,nmin2, nd4ext = 
         2        65         3         2        12        64         6       380       380
 i,start,strti,fin,filind,bufszi,nmbuf,nmin2, nd4ext = 
         3       129         4         3        12        64         6       380       380
 i,start,strti,fin,filind,bufszi,nmbuf,nmin2, nd4ext = 
         4       193         5         4        12        64         6       380       380
 i,start,strti,fin,filind,bufszi,nmbuf,nmin2, nd4ext = 
         5       257         6         5        12        64         6       380       380
 i,start,strti,fin,filind,bufszi,nmbuf,nmin2, nd4ext = 
         6       321         7         6        12        64         6       380       380

 4 external diag. modified integrals

  0.299711954 -3.3191442  0.362025844  0.213403987  0.294044049 -3.09165276
  0.345182252  0.318068021  0.34837146  0.293493931  0.497019376 -4.10225551
  0.382867967  0.309072536  0.341195502  0.316765031  0.62103433  0.362850338
  0.531836961 -4.0788288  0.349706612  0.299527054  0.308820027  0.298754537
  0.450260207  0.419223806  0.448476864  0.401601457  0.518535861 -4.75210498
  0.349531774  0.304524501  0.31278089  0.302655019  0.429312411  0.416826853
  0.436921903  0.413400849  0.530775438  0.48280831  0.60679891 -5.34846983
  0.33535564  0.293407704  0.341034277  0.276576801  0.455225842  0.389302194
  0.443351075  0.383054684  0.486303633  0.428587076  0.499742288  0.459023099
  0.475434104 -4.33669666  0.342526573  0.315362897  0.319917749  0.300731659
  0.504389096  0.410870033  0.47482656  0.425010382  0.519270202  0.400100848
  0.453053263  0.422413645  0.446074502  0.406547185  0.476405093 -3.58739254
  0.361060191  0.337659006  0.340604687  0.320198588  0.504515292  0.452245513
  0.527488651  0.469039301  0.434901697  0.420706511  0.454829088  0.417262113
  0.426620519  0.400804422  0.469386808  0.441599006  0.555764969 -3.42330119
  0.348856386  0.329501947  0.326880472  0.312634683  0.490451682  0.436629022
  0.509180653  0.444435002  0.4400708  0.419071921  0.459403052  0.417691971
  0.415311401  0.398901525  0.466192986  0.43088403  0.694750225  0.346946891
  0.501631969 -3.17820739  0.343227876  0.31280524  0.321132402  0.300850073
  0.455432102  0.418963752  0.451109274  0.419591702  0.534502331  0.438245352
  0.597905727  0.464004009  0.508095073  0.408470727  0.460615989  0.424227034
  0.469521607  0.441123033  0.462583019  0.438238584  0.525774157 -4.03664046
  0.356976431  0.333496982  0.334773679  0.315431831  0.522936179  0.430785245
  0.55704189  0.430105919  0.46546176  0.385379923  0.398185515  0.390667038
  0.424333725  0.380522726  0.592974188  0.354449455  0.516100041  0.471421051
  0.491982029  0.453400696  0.428233148  0.409923576  0.53092711 -2.63324986
  0.370351482  0.34231438  0.377526483  0.320334123  0.57663332  0.44254372
  0.563518308  0.477512966  0.511232086  0.469131823  0.508472502  0.490097443
  0.529535469  0.421869959  0.499545867  0.467575397  0.539123293  0.488956188
  0.512741012  0.475743005  0.515621907  0.477676678  0.512163042  0.474196449
  0.592144122 -3.62338375  0.388203856  0.340302294  0.365601016  0.328125926
  0.536368018  0.473948976  0.615526601  0.455378076  0.520967947  0.463416877
  0.546457699  0.489981129  0.48581785  0.455063687  0.509165453  0.46499119
  0.535052608  0.504064949  0.518029026  0.481875024  0.516561248  0.489811629
  0.517714869  0.482829445  0.717425907  0.445100321  0.607835731 -3.66481183
  0.361030445  0.336028491  0.330453775  0.322428351  0.486741558  0.469418774
  0.514189842  0.450314389  0.575178104  0.539226259  0.630438312  0.577562728
  0.510895782  0.499926663  0.503939385  0.490371469  0.52041881  0.467839603
  0.554043059  0.445274854  0.591832537  0.558697254  0.453957255  0.445032703
  0.568738744  0.548069735  0.596778585  0.560916765  0.750165186 -3.66998914
  0.353015389  0.338054232  0.358115278  0.317291888  0.522721553  0.462375435
  0.489534774  0.469144866  0.543456123  0.532600344  0.632201055  0.577477753
  0.547273656  0.513624874  0.507616587  0.483570727  0.564197636  0.455577875
  0.524876517  0.46718588  0.585160178  0.562784851  0.458775281  0.447244069
  0.587371194  0.556188564  0.58998874  0.568912261  0.721594827  0.645098881
  0.755127757 -3.63954921  0.366037972  0.335416332  0.343455146  0.320873443
  0.50940609  0.476326958  0.527676719  0.456821536  0.589439367  0.537772005
  0.579789349  0.559672288  0.53269644  0.49854365  0.567546289  0.469810852
  0.495610134  0.480892777  0.493939617  0.479062411  0.584343602  0.533457424
  0.502525388  0.442000941  0.582221799  0.560044775  0.596954899  0.56140811
  0.713528596  0.656785472  0.69947109  0.625487345  0.729807063 -3.38750818
  0.353987843  0.337616982  0.351789554  0.321182817  0.550334788  0.456688986
  0.522995786  0.46803517  0.566057852  0.507622395  0.615570104  0.560439732
  0.539018372  0.503813066  0.510463413  0.481771744  0.500103129  0.48786777
  0.496692123  0.477703001  0.585735173  0.540040646  0.469784387  0.453592071
  0.609978757  0.53850005  0.643846451  0.534124473  0.686162365  0.635949394
  0.700906311  0.666564985  0.679632027  0.617528828  0.696112735 -3.03509862
  0.358856446  0.335907395  0.354506012  0.319503646  0.548011068  0.469120392
  0.531509325  0.460743131  0.577501451  0.534350115  0.577624788  0.562928162
  0.587296462  0.4866605  0.514004213  0.4958057  0.493523583  0.479933652
  0.487740555  0.474497517  0.570979461  0.537487177  0.471960345  0.452931079
  0.634007692  0.54138526  0.615254278  0.550385293  0.691778305  0.63576614
  0.699509207  0.640665078  0.699234432  0.657364716  0.711993958  0.616407541
  0.721280492 -2.89433126
 i,start,strti,fin,filind,bufszi,nmbuf,nmin2, nd4ext = 
         1         1         2         7        12        64         6       380       380
 i,start,strti,fin,filind,bufszi,nmbuf,nmin2, nd4ext = 
         2        65         3         2        12        64         6       380       380
 i,start,strti,fin,filind,bufszi,nmbuf,nmin2, nd4ext = 
         3       129         4         3        12        64         6       380       380
 i,start,strti,fin,filind,bufszi,nmbuf,nmin2, nd4ext = 
         4       193         5         4        12        64         6       380       380
 i,start,strti,fin,filind,bufszi,nmbuf,nmin2, nd4ext = 
         5       257         6         5        12        64         6       380       380
 i,start,strti,fin,filind,bufszi,nmbuf,nmin2, nd4ext = 
         6       321         7         6        12        64         6       380       380

 all internall diag. modified integrals

  4.73971342 -33.0517492  1.04569563  0.0636788604  0.755179462 -7.87312296
  0.878477817  0.0193493095  0.682719812  0.155759544  0.668673763 -6.71622218
  0.986988872  0.0301374391  0.681192227  0.125736446  0.608457144
  0.0402117459  0.732528817 -6.95563276  1.03831833  0.0301199817  0.713926708
  0.128455235  0.624391224  0.0304166993  0.665957609  0.0433645062
  0.760228983 -7.11691032
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331914E+01-0.309165E+01-0.410226E+01-0.407883E+01-0.475210E+01-0.534847E+01-0.433670E+01-0.358739E+01-0.342330E+01-0.317821E+01
 -0.403664E+01-0.263325E+01-0.362338E+01-0.366481E+01-0.366999E+01-0.363955E+01-0.338751E+01-0.303510E+01-0.289433E+01-0.330517E+02
 -0.787312E+01-0.671622E+01-0.695563E+01-0.711691E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         2        65         3         2        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         3       129         4         3        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         4       193         5         4        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         5       257         6         5        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         6       321         7         6        12        64       128         6       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331914E+01-0.309165E+01-0.410226E+01-0.407883E+01-0.475210E+01-0.534847E+01-0.433670E+01-0.358739E+01-0.342330E+01-0.317821E+01
 -0.403664E+01-0.263325E+01-0.362338E+01-0.366481E+01-0.366999E+01-0.363955E+01-0.338751E+01-0.303510E+01-0.289433E+01-0.330517E+02
 -0.787312E+01-0.671622E+01-0.695563E+01-0.711691E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         2        65         3         2        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         3       129         4         3        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         4       193         5         4        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         5       257         6         5        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         6       321         7         6        12        64       128         6       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331914E+01-0.309165E+01-0.410226E+01-0.407883E+01-0.475210E+01-0.534847E+01-0.433670E+01-0.358739E+01-0.342330E+01-0.317821E+01
 -0.403664E+01-0.263325E+01-0.362338E+01-0.366481E+01-0.366999E+01-0.363955E+01-0.338751E+01-0.303510E+01-0.289433E+01-0.330517E+02
 -0.787312E+01-0.671622E+01-0.695563E+01-0.711691E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         2        65         3         2        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         3       129         4         3        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         4       193         5         4        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         5       257         6         5        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         6       321         7         6        12        64       128         6       380       380
 DIAGEL COUNTS: DG0X,DG2X,DG4X= 440 145 30
 =========== Executing IN-CORE method ==========
 norm multnew:  1.
 threx: total number of loops= 14
 threx: total number of loops= 24
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331914E+01-0.309165E+01-0.410226E+01-0.407883E+01-0.475210E+01-0.534847E+01-0.433670E+01-0.358739E+01-0.342330E+01-0.317821E+01
 -0.403664E+01-0.263325E+01-0.362338E+01-0.366481E+01-0.366999E+01-0.363955E+01-0.338751E+01-0.303510E+01-0.289433E+01-0.330517E+02
 -0.787312E+01-0.671622E+01-0.695563E+01-0.711691E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         2        65         3         2        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         3       129         4         3        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         4       193         5         4        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         5       257         6         5        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         6       321         7         6        12        64       128         6       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331914E+01-0.309165E+01-0.410226E+01-0.407883E+01-0.475210E+01-0.534847E+01-0.433670E+01-0.358739E+01-0.342330E+01-0.317821E+01
 -0.403664E+01-0.263325E+01-0.362338E+01-0.366481E+01-0.366999E+01-0.363955E+01-0.338751E+01-0.303510E+01-0.289433E+01-0.330517E+02
 -0.787312E+01-0.671622E+01-0.695563E+01-0.711691E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         2        65         3         2        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         3       129         4         3        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         4       193         5         4        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         5       257         6         5        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         6       321         7         6        12        64       128         6       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331914E+01-0.309165E+01-0.410226E+01-0.407883E+01-0.475210E+01-0.534847E+01-0.433670E+01-0.358739E+01-0.342330E+01-0.317821E+01
 -0.403664E+01-0.263325E+01-0.362338E+01-0.366481E+01-0.366999E+01-0.363955E+01-0.338751E+01-0.303510E+01-0.289433E+01-0.330517E+02
 -0.787312E+01-0.671622E+01-0.695563E+01-0.711691E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         2        65         3         2        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         3       129         4         3        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         4       193         5         4        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         5       257         6         5        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         6       321         7         6        12        64       128         6       380       380


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        40 xx:       135 ww:       255
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        20 yw:        25

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        39
task #   9:       127    task #  10:       247    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 matrix-vector product formed for vector   1
 rtolcosmo =   0.001
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference-subspace vector overlap matrix  block   1

                ci   1
 ref:   1     1.00000000

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000
 subspace has dimension  1

          (h-repnuc*1) matrix in the subspace basis

                x:   1
   x:   1   -85.40090838

          overlap matrix in the subspace basis

                x:   1
   x:   1     1.00000000

          eigenvectors and eigenvalues in the subspace basis

                v:   1

   energy   -85.40090838

   x:   1     1.00000000

          eigenvectors and eigenvalues in the schmidt orthonormalized basis

                v:   1

   energy   -85.40090838

  zx:   1     1.00000000

          <ref|baseci> overlap matrix  block   1

                ci   1
 ref:   1     1.00000000

  write the transformed vectors back to cosmofile

 rtflw =  0
 freezing =  F

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -76.0479455968 -9.2970E+00  2.5188E-01  1.0015E+00  1.0000E-03


 total energy in cosmo calc 
e(rootcalc) + repnuc=     -76.0479455968
dielectric energy =      -0.0104563943
deltaediel =       0.0104563943
e(rootcalc) + repnuc - ediel =     -76.0374892025
e(rootcalc) + repnuc - edielnew =     -76.0479455968
deltaelast =      76.0374892025

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.02   0.00   0.00   0.01         0.    0.0000
    2   25    0   0.02   0.00   0.00   0.01         0.    0.0000
    3   26    0   0.04   0.00   0.00   0.03         0.    0.0000
    4   11    0   0.01   0.00   0.00   0.01         0.    0.0000
    5   15    0   0.02   0.00   0.00   0.01         0.    0.0000
    6   16    0   0.03   0.00   0.00   0.02         0.    0.0000
    7    1    0   0.00   0.00   0.00   0.00         0.    0.0000
    8    5    0   0.03   0.00   0.00   0.02         0.    0.0000
    9    6    0   0.03   0.01   0.00   0.03         0.    0.0000
   10    7    0   0.03   0.00   0.00   0.03         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.02   0.00   0.00   0.01         0.    0.0000
   14   47    0   0.02   0.00   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.2700s 
time spent in multnx:                   0.2000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0100s 
time for vector access in mult:         0.0200s 
total time per CI iteration:            3.5600s 

          starting ci iteration   2

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.35296278

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=      10  D0X=      30  D0W=      50
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================

          ci-one-electron density block   1

                mo   1         mo   2         mo   3         mo   4         mo   5         mo   6         mo   7         mo   8
   mo   1     2.00000000
   mo   2     0.00000000     2.00000000
   mo   3     0.00000000     0.00000000     2.00000000
   mo   4     0.00000000     0.00000000     0.00000000     2.00000000
   mo   5     0.00000000     0.00000000     0.00000000     0.00000000     2.00000000

                mo   9         mo  10         mo  11         mo  12         mo  13         mo  14         mo  15         mo  16

                mo  17         mo  18         mo  19         mo  20         mo  21         mo  22         mo  23         mo  24


*****   symmetry block SYM1   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       2.00000000
       2       0.00000000     2.00000000
       3       0.00000000     0.00000000     2.00000000
       4       0.00000000     0.00000000     0.00000000     2.00000000

               Column   5     Column   6     Column   7     Column   8
       5       2.00000000

               Column   9     Column  10     Column  11     Column  12

               Column  13     Column  14     Column  15     Column  16

               Column  17     Column  18     Column  19     Column  20

               Column  21     Column  22     Column  23     Column  24

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7        eno   8
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    8   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno   9        eno  10        eno  11        eno  12        eno  13        eno  14        eno  15        eno  16
 emo    9   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   10   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   11   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   12   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   13   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   14   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   15   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   16   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno  17        eno  18        eno  19        eno  20        eno  21        eno  22        eno  23        eno  24
 emo   17   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   18   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   19   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   20   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   21   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   22   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   23   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   24   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   2.0000000      2.0000000      2.0000000      2.0000000      2.0000000
   0.0000000      0.0000000      0.0000000      0.0000000      0.0000000
   0.0000000      0.0000000      0.0000000      0.0000000      0.0000000
   0.0000000      0.0000000      0.0000000      0.0000000      0.0000000
   0.0000000      0.0000000      0.0000000      0.0000000

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7         no   8
  mo    1   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo    2   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo    3   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
  mo    5   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
  mo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01
  mo   24   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00

                no   9         no  10         no  11         no  12         no  13         no  14         no  15         no  16
  mo    8   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo    9   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo   10   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo   11   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo   12   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
  mo   13   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
  mo   14   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
  mo   15   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

                no  17         no  18         no  19         no  20         no  21         no  22         no  23         no  24
  mo   16   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo   17   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo   18   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo   19   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo   20   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
  mo   21   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
  mo   22   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
  mo   23   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01
  eigenvectors of nos in ao-basis, column    1
  0.0000000E+00  0.0000000E+00  0.0000000E+00  0.0000000E+00  0.9188403E+00  0.0000000E+00  0.0000000E+00  0.7204964E-01
  0.0000000E+00  0.1416552E-14 -0.1830008E-01  0.0000000E+00  0.0000000E+00  0.0000000E+00  0.0000000E+00  0.0000000E+00
  0.0000000E+00  0.3154239E-01  0.0000000E+00  0.0000000E+00  0.0000000E+00  0.0000000E+00  0.3154239E-01  0.0000000E+00
  eigenvectors of nos in ao-basis, column    2
  0.1000431E+01  0.2482130E-02 -0.1419915E-02  0.7781734E-14  0.0000000E+00 -0.2616439E-02 -0.3398887E-13  0.0000000E+00
  0.1645654E-02  0.0000000E+00  0.0000000E+00  0.3376021E-05  0.3967485E-14  0.8587611E-04 -0.5438131E-03  0.6927337E-03
 -0.6163335E-03  0.0000000E+00 -0.5060470E-03 -0.5438131E-03  0.6927337E-03  0.6163335E-03  0.0000000E+00 -0.5060470E-03
  eigenvectors of nos in ao-basis, column    3
 -0.8844768E-02  0.8674603E+00 -0.1297648E+00  0.6089053E-13  0.0000000E+00 -0.1174189E+00  0.6568151E-13  0.0000000E+00
  0.6892078E-01  0.0000000E+00  0.0000000E+00  0.3306414E-03 -0.5614132E-14  0.1611783E-02  0.3379710E+00 -0.1596253E+00
  0.3836313E-01  0.0000000E+00  0.2045257E-01  0.3379710E+00 -0.1596253E+00 -0.3836313E-01  0.0000000E+00  0.2045257E-01
  eigenvectors of nos in ao-basis, column    4
  0.4616762E-13  0.1371997E-12 -0.1998070E-12  0.7158542E+00  0.0000000E+00  0.1612520E-12 -0.1132335E+00  0.0000000E+00
  0.1762196E-13  0.0000000E+00  0.0000000E+00 -0.4966912E-15 -0.2722905E-01 -0.6927103E-15 -0.5635816E+00  0.1958934E+00
 -0.2355515E-01  0.0000000E+00 -0.3267843E-01  0.5635816E+00 -0.1958934E+00 -0.2355515E-01  0.0000000E+00  0.3267843E-01
  eigenvectors of nos in ao-basis, column    5
 -0.2070810E-02  0.2955600E+00  0.1824047E+00 -0.1630789E-12  0.0000000E+00  0.7982083E+00  0.8010964E-13  0.0000000E+00
 -0.1316750E-02  0.0000000E+00  0.0000000E+00 -0.5179269E-02  0.7124025E-14 -0.2323002E-02 -0.3484059E+00  0.1370807E+00
 -0.3161476E-01  0.0000000E+00  0.8311562E-02 -0.3484059E+00  0.1370807E+00  0.3161476E-01  0.0000000E+00  0.8311562E-02
  eigenvectors of nos in ao-basis, column    6
 -0.1687550E-13 -0.6976727E-13  0.1630899E-12  0.7277607E+00 -0.2063855E-20 -0.2635885E-13  0.8341926E+00 -0.6919644E-19
 -0.4339350E-13  0.8446766E-19  0.9486414E-19  0.5717816E-14 -0.1329366E+01  0.1531364E-13  0.1926243E+01 -0.6729536E+00
  0.6201121E+00  0.1166532E-18  0.4908839E+00 -0.1926243E+01  0.6729536E+00  0.6201121E+00  0.5660713E-19 -0.4908839E+00
  eigenvectors of nos in ao-basis, column    7
 -0.5211847E-01  0.1431756E+00  0.9350625E+00 -0.3212687E-12 -0.2651782E-16 -0.2693111E+00  0.2294226E-12  0.6097966E-16
 -0.2052835E+00 -0.1917530E-15 -0.1461129E-16  0.2090738E-02 -0.2192932E-13  0.5372106E-02 -0.9729573E-01 -0.7866691E+00
 -0.1885770E-01 -0.7385777E-16 -0.1685441E-01 -0.9729573E-01 -0.7866691E+00  0.1885770E-01  0.4913963E-17 -0.1685441E-01
  eigenvectors of nos in ao-basis, column    8
 -0.7525888E-13 -0.3615981E-12  0.3645833E-12 -0.4064254E+00  0.6143126E-16  0.1928467E-12 -0.4850854E+00 -0.4125148E-16
 -0.2481429E-12 -0.2448182E-16 -0.2575400E-16 -0.2153392E-14  0.2228350E-01  0.5706246E-14 -0.3830673E-01 -0.1445818E+01
 -0.2186979E-01 -0.2822300E-16 -0.1841129E-01  0.3830673E-01  0.1445818E+01 -0.2186979E-01 -0.4770756E-16  0.1841129E-01
  eigenvectors of nos in ao-basis, column    9
  0.8133027E-13  0.3949953E-12 -0.7847117E-12 -0.3878087E+00  0.9167769E-16 -0.4204844E-12 -0.3070854E+00 -0.1259703E-15
  0.3890379E-12  0.1296804E-15  0.1898080E-15 -0.1720719E-14 -0.1128502E+00 -0.4338076E-13 -0.1609845E+01  0.1468783E+01
  0.7759868E-01  0.1210108E-15  0.1590354E+00  0.1609845E+01 -0.1468783E+01  0.7759868E-01 -0.6645719E-16 -0.1590354E+00
  eigenvectors of nos in ao-basis, column   10
  0.6009755E-01  0.4905569E+00 -0.5872575E+00  0.1274610E-12 -0.4148646E-16 -0.4708518E+00  0.3521379E-12  0.3190314E-16
  0.2280175E+00  0.1095879E-15 -0.7270697E-16  0.1275336E-03  0.9135192E-13 -0.5458997E-01 -0.1328241E+01  0.1203349E+01
  0.3054664E+00  0.3029621E-16  0.5596935E-01 -0.1328241E+01  0.1203349E+01 -0.3054664E+00  0.4008870E-17  0.5596935E-01
  eigenvectors of nos in ao-basis, column   11
 -0.1171771E-02 -0.2149045E+00 -0.6662232E+00  0.4008957E-12  0.1272162E-15 -0.1100021E+01 -0.9162349E-12 -0.3235579E-15
  0.1802690E+01  0.4183721E-17 -0.8150565E-16 -0.3135894E-02  0.4076522E-13  0.2248781E-02  0.9562774E+00 -0.3614770E+00
  0.9392462E-01  0.3124207E-15  0.2398654E+00  0.9562774E+00 -0.3614770E+00 -0.9392462E-01  0.7696705E-16  0.2398654E+00
  eigenvectors of nos in ao-basis, column   12
 -0.1865057E-16 -0.1137790E-16  0.4372914E-15  0.2311638E-15  0.1409556E+01 -0.1502495E-16 -0.2137865E-17 -0.1685359E+01
 -0.2644028E-15  0.2619468E-14 -0.4378665E-02 -0.2227742E-16  0.2920819E-16  0.1910002E-16 -0.2112112E-15  0.1351552E-15
 -0.2569473E-18  0.5475679E-02  0.6083716E-17 -0.6046545E-15  0.1695000E-15  0.2264732E-15  0.5475679E-02 -0.1656291E-15
  eigenvectors of nos in ao-basis, column   13
  0.6769483E-13  0.1301094E-12 -0.7410199E-12 -0.1057726E+01 -0.1337786E-16 -0.2734863E-12  0.2269170E+01 -0.3776348E-16
  0.4949036E-12  0.2616358E-18  0.1265581E-15 -0.5399661E-14 -0.4799202E-01  0.1756388E-14  0.6650249E+00  0.5106979E+00
  0.3041767E+00  0.4995213E-16  0.1883073E+00 -0.6650249E+00 -0.5106979E+00  0.3041767E+00  0.8462649E-16 -0.1883073E+00
  eigenvectors of nos in ao-basis, column   14
  0.2091554E+00  0.1071580E+01 -0.1181893E+01  0.2281662E-12  0.3799142E-16  0.1763751E+00 -0.4822797E-12  0.1095098E-14
 -0.7970975E+00 -0.1337744E-15  0.5119991E-16 -0.3282162E-01  0.3109784E-13 -0.1255569E-03 -0.5377055E+00  0.4909475E+00
 -0.3086257E+00 -0.2410793E-17  0.5610314E+00 -0.5377055E+00  0.4909475E+00  0.3086257E+00 -0.2622459E-14  0.5610314E+00
  eigenvectors of nos in ao-basis, column   15
 -0.2948458E-15 -0.1543599E-14  0.1780871E-14  0.9962454E-16 -0.1039696E-13 -0.2101302E-15  0.1481156E-15  0.4657262E-13
  0.1318351E-14 -0.1289159E+00 -0.1874740E-13  0.5522893E-16  0.1935210E-16  0.1444038E-17  0.1106991E-14 -0.8594874E-15
  0.5663967E-15  0.6862173E+00 -0.1023255E-14  0.6499074E-15 -0.7245662E-15 -0.5506128E-15 -0.6862173E+00 -0.1231443E-14
  eigenvectors of nos in ao-basis, column   16
  0.1124007E-14  0.5190615E-14 -0.6901306E-14 -0.8661637E-16 -0.6688636E-01  0.7454745E-15  0.3311698E-16 -0.5949782E+00
 -0.7786919E-15  0.3482762E-13 -0.1598336E+00 -0.3942046E-16 -0.9962211E-17  0.5635439E-16  0.4204281E-15  0.4277037E-15
 -0.2403051E-15  0.7710122E+00  0.7735285E-15  0.5615269E-15  0.4375273E-15  0.5413873E-16  0.7710122E+00  0.6927030E-15
  eigenvectors of nos in ao-basis, column   17
 -0.7152723E+00 -0.3091336E+01  0.4825988E+01 -0.3740154E-13 -0.1754659E-15 -0.1908556E+00 -0.1542837E-11 -0.5249664E-15
 -0.8798870E+00 -0.3829584E-16 -0.1404789E-15 -0.3319167E-01 -0.4320577E-13 -0.5345980E-01 -0.1438511E+01  0.3295761E+00
 -0.3798899E+00  0.8949531E-15  0.3789287E-01 -0.1438511E+01  0.3295761E+00  0.3798899E+00  0.7857435E-15  0.3789287E-01
  eigenvectors of nos in ao-basis, column   18
  0.1254386E-11  0.5561242E-11 -0.8613367E-11  0.1732794E-01 -0.1896560E-17  0.4631480E-12 -0.9280177E+00 -0.1893239E-16
  0.1418057E-11 -0.2085640E-15 -0.1692849E-15  0.4726852E-13 -0.3028578E-01  0.7661613E-13 -0.6786853E+00  0.2460613E+00
  0.4823549E+00  0.1561555E-16 -0.6903832E+00  0.6786853E+00 -0.2460613E+00  0.4823549E+00  0.1887728E-16  0.6903832E+00
  eigenvectors of nos in ao-basis, column   19
  0.2629732E-12  0.1389613E-11  0.1945139E-12  0.1251840E+01 -0.2292075E-15 -0.1022185E-11 -0.4128890E+00  0.2467346E-15
  0.2119138E-12  0.1032580E-15 -0.4718214E-16 -0.2675803E-13  0.1472896E+00 -0.1247686E-12  0.5466282E+00  0.5377930E-01
  0.7276124E+00  0.1203127E-16  0.5654252E+00 -0.5466282E+00 -0.5377930E-01  0.7276124E+00  0.4511230E-16 -0.5654252E+00
  eigenvectors of nos in ao-basis, column   20
 -0.3068618E+00 -0.1538593E+01  0.1120258E+00  0.1321128E-11  0.1044481E-15  0.9807091E+00 -0.4518524E-12 -0.6503741E-16
 -0.2798825E+00 -0.1478313E-16 -0.3504715E-15  0.1641279E-01  0.1596852E-12  0.1132260E+00  0.8363142E+00 -0.2663892E+00
  0.7517673E+00 -0.3931772E-16  0.5305955E+00  0.8363142E+00 -0.2663892E+00 -0.7517673E+00 -0.1275737E-15  0.5305955E+00
  eigenvectors of nos in ao-basis, column   21
  0.2164986E-16  0.1198196E-16 -0.1021751E-14  0.2353443E-15  0.7240906E-02  0.3682210E-15 -0.3947190E-15 -0.3229147E+00
  0.2052502E-15  0.1822167E-12  0.1045714E+01 -0.1759278E-15  0.2730361E-16  0.2510648E-16  0.7780825E-15 -0.4884518E-15
  0.5190693E-15  0.4066771E+00  0.3040973E-16  0.5472104E-15  0.1565809E-15 -0.4277323E-15  0.4066771E+00  0.2941472E-15
  eigenvectors of nos in ao-basis, column   22
  0.1050887E-15  0.2357625E-15 -0.1977831E-14 -0.6737873E-16 -0.4970292E-14  0.5004176E-15 -0.9749280E-16  0.8383453E-13
  0.8871752E-15  0.1072280E+01 -0.1747192E-12 -0.2994074E-15 -0.3474570E-17 -0.3470492E-15  0.2216376E-14 -0.1323823E-14
  0.7823238E-15  0.3811176E+00 -0.1835534E-15  0.2143708E-14 -0.9715350E-15 -0.7529339E-15 -0.3811176E+00  0.2083587E-15
  eigenvectors of nos in ao-basis, column   23
  0.8598546E-01  0.3028774E+00 -0.8098987E+00 -0.2434517E-13 -0.8213752E-16  0.5946500E-01  0.7280752E-13  0.3190450E-15
  0.5469864E+00 -0.9636920E-15 -0.5635709E-15 -0.3159243E+00 -0.2776325E-13  0.6761892E-01  0.5941891E+00 -0.2522491E+00
  0.3743832E+00 -0.6404347E-15 -0.3122271E+00  0.5941891E+00 -0.2522491E+00 -0.3743832E+00  0.8279087E-16 -0.3122271E+00
  eigenvectors of nos in ao-basis, column   24
 -0.1166195E+00 -0.2416777E+00  0.2434799E+01 -0.1887791E-13 -0.7061382E-16 -0.6217072E+00 -0.4086266E-13  0.5771820E-16
 -0.6211116E+00  0.7576342E-15  0.8430511E-16  0.2467611E-01  0.3836063E-13  0.5861624E+00 -0.2169264E+01  0.8956706E+00
 -0.6236886E+00  0.2976308E-15 -0.5088806E+00 -0.2169264E+01  0.8956706E+00  0.6236886E+00 -0.2294555E-15 -0.5088806E+00


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***


 cosurf_and_phi
  2.466315385316 -0.061539892486  2.837440149000 -0.034370604262
  0.006359863210 -2.199287675687  3.114544749440 -0.062933376365
 -0.480526931872  1.061509344774  3.755560521090 -0.064148417365
  1.262739748330  2.872870034186  1.567866969040 -0.042635124542
  1.897949994968 -2.634366215369  0.570962659525 -0.015380963593
 -2.790166444607 -0.824331206483  2.170430257018 -0.019321468883
 -2.061296060778  2.493528161233  1.034251636268 -0.020819237183
 -0.002991703587  2.420846177436 -1.447622679829  0.011948642117
 -1.114435211431 -2.949559801670 -0.067898604227 -0.016077390874
  1.955709175590 -0.984462672186  3.123501329999 -0.046406945930
  1.000900154469 -1.706149424342  3.300403738319 -0.059492387183
  1.652947872482  0.360731428865  3.496571379926 -0.052788977835
  0.594940552015  0.670555958619  3.845537291721 -0.063791583883
  2.390078334690  1.202722009900  2.566708449184 -0.033900743429
  1.989334673621  2.229216060677  2.001028520754 -0.036548139281
  2.919143888229  0.378144896597  2.099775822683 -0.014391344019
  2.780367109803 -0.921046401167  2.130496470151 -0.019086870536
  2.449941849009 -2.011800733381  1.439000205618 -0.018021977970
 -0.231540571447 -1.263621120083  3.706953994699 -0.064793919300
 -0.352106389130 -0.107618694495  3.950679056227 -0.064905503156
  0.105380882100  1.878884201281  3.371423292662 -0.063982836097
  0.783266721831  2.590884380442  2.520834408126 -0.056610004193
  2.011402526412  2.551496219550  0.814855178005 -0.017769143502
  1.440220538007 -2.843720084632  1.356653310131 -0.037509340940
  0.871209499702 -2.660587439486  2.372618950166 -0.054793133390
 -2.947488997374  0.298617382934  2.058332654596 -0.012862348920
 -2.674954162172  1.563027715274  1.704200253745 -0.015931714437
 -1.353448939749  2.910920816403  0.212056013565 -0.018613926005
 -0.743422400795  2.810699746106 -0.731960510989 -0.004323943353
  0.099626804209  1.855073908563 -1.945822518898  0.029601871749
  0.019900458911 -1.968682238442 -1.864952523830  0.026388425644
 -0.601526683996 -2.669374282549 -1.032942810695  0.002601915334
 -1.976701730993 -2.578422698078  0.816296596419 -0.018629331230
 -2.525639241426 -1.850752473194  1.593331825886 -0.018492509989
 -1.124962231191 -1.881571875050  3.121019991266 -0.057552898647
 -2.117398618237 -1.513942036836  2.667866282109 -0.040465303694
 -2.336046769995 -0.152318885910  2.976109389266 -0.038215813431
 -1.478775916646  0.469153703894  3.577445396081 -0.055391145912
 -1.328587214463  1.668007282102  3.174271253825 -0.055657843358
 -1.771761195387  2.287385908353  2.202255939279 -0.042540089613
 -1.026730149698  3.017318998523  1.358608629268 -0.042473317530
  0.119020599620  3.173161356543  1.415159765853 -0.048074223322
  0.915237473358  3.108118624501  0.463299650838 -0.029353214118
  0.462814589486  2.837210699514 -0.795516582628 -0.005197620869
  1.026812912753 -3.017319139618  0.083991013862 -0.020597445718
 -0.070059393274 -3.176418651270  0.035610954337 -0.025715677406
 -0.970268948715 -3.083313212042  1.062474740253 -0.038809338274
 -0.534203029846 -2.828286137107  2.231267851728 -0.055654832963
  0.871897873699 -0.575451888002  3.799144800425 -0.061994639249
  1.408719892640  1.640288870679  3.148120355694 -0.054583395021
  2.650053602975  1.633766196698  1.655441764425 -0.015879240488
  1.964286464189 -2.001027831890  2.365093852582 -0.040762147359
 -1.342877128538 -0.760711330777  3.581800927521 -0.057046417880
 -0.531365927285  2.661712102822  2.509437292848 -0.057962221475
  1.142904167226  2.885766749848 -0.243475252462 -0.011486698850
  0.342129526197 -3.189261637688  1.246854200824 -0.045388289319
 -2.346459445268  1.135413320199  2.662806558936 -0.035693232547
 -0.258565605057  3.205542092831  0.249849913336 -0.029543194065
  0.450566961335 -2.699565911226 -1.032013446782  0.001225636567
 -1.684533476150 -2.430522251364  2.070179818920 -0.042598308929
 -3.368784049130  0.036613212616 -1.854972615293  0.058700004553
 -3.278396927799  1.580834404538 -0.078603229040  0.032623887513
 -3.656110378332 -0.713812229513  0.361831391088  0.033722006190
 -2.408052717333 -2.075622947728 -1.226189024363  0.036814251624
 -1.295829484895 -0.567704086865 -2.747607986898  0.054759631720
 -1.846557165647  1.681693338816 -2.099716493181  0.045977223701
 -3.831862607217  0.357468890277 -0.654813288033  0.049463690965
 -3.527736967644 -1.045671231370 -1.064852892071  0.050344088398
 -2.622906831544 -1.024738705101 -2.241124222290  0.055870497229
 -2.402781990555  0.378472303440 -2.579763034114  0.058853455544
 -3.126606927219  1.313275381963 -1.541857021673  0.051509509531
 -3.633182667875  0.531520753705  0.561856665829  0.030800813749
 -3.086813000261 -1.794874586082 -0.179700355757  0.030594537327
 -1.615880090830 -1.674393887320 -2.147504235692  0.044936561186
 -1.240258025012  0.765881087348 -2.687977655831  0.053488967638
 -2.430292517958  2.154437082790 -0.969924007583  0.032557462001
  3.368776503197 -0.036639040867 -1.854966842961  0.058700404880
  3.278392620256 -1.580850766330 -0.078589062660  0.032623186712
  3.656106873706  0.713798214804  0.361832640492  0.033722266754
  2.408046317685  2.075600470298 -1.226192756897  0.036815137409
  1.295820311657  0.567673501678 -2.747601655947  0.054760163553
  1.846549173548 -1.681720471299 -2.099699178989  0.045976594295
  3.831857249217 -0.357488322766 -0.654806650060  0.049463717170
  3.527730862121  1.045649613724 -1.064853177123  0.050344831858
  2.622898581638  1.024710819001 -2.241122746235  0.055871358329
  2.402773123306 -0.378501994157 -2.579753678917  0.058853673796
  3.126599952113 -1.313299541573 -1.541844004398  0.051509134312
  3.633179527908 -0.531533702443  0.561864593522  0.030800511732
  3.086808508398  1.794857685487 -0.179703829578  0.030595206142
  1.615872011596  1.674366500124 -2.147504385867  0.044937411492
  1.240248960489 -0.765911354740 -2.687964116774  0.053488827352
  2.430286585511 -2.154458194501 -0.969905238283  0.032556543067
  0.000006852884  1.035694667087 -2.361676372823  0.046979034298
 -0.298806015181  0.969607486526 -2.408809074493  0.048501242400
  0.298815704890  0.969607820141 -2.408807386530  0.048501318921
  0.000007656756 -1.035723195601 -2.361670853435  0.046978535694
 -0.298805262603 -0.969636498141 -2.408803907288  0.048500758230
  0.298816457468 -0.969636367899 -2.408802219325  0.048500852478
 -0.667712940797 -1.245186997899 -2.338574529312  0.046522342270
 -1.218112188165 -2.025138759161 -1.616566947615  0.032936789143
 -2.084175601108 -2.294008802021 -0.480477682822  0.019038748523
 -2.876631896395 -1.658047550445  0.559052047065  0.016971188367
 -3.282909221331 -0.368099862162  1.091996180628  0.019662927091
 -3.142757910518  1.067034798172  0.908143333087  0.018448454351
 -2.511458431963  2.081290260960  0.080011352455  0.016716562473
 -1.638016880752  2.274609499719 -1.065756198713  0.024575829877
 -0.866948462969  1.570740798135 -2.077229430584  0.042722811879
 -0.467915446486 -1.694563900014 -1.941501402531  0.035437689916
 -1.299753513329 -2.453901457341 -0.850306853788  0.010627894760
 -2.242033801688 -2.245340287831  0.385760998463  0.000604800965
 -2.923088754321 -1.151144046328  1.279154738758  0.003603462701
 -3.074287016292  0.397098864814  1.477489335582  0.005422302861
 -2.635990824421  1.788708515315  0.902534843950  0.001304837506
 -1.781079179779  2.474786487342 -0.218927030025  0.003408856753
 -0.846758459860  2.184720175398 -1.444553386477  0.021850296046
 -0.255852300976  1.360631011730 -2.219692209228  0.041864758849
  0.667723897920  1.245157743773 -2.338577856151  0.046523113973
  1.218123388571  2.025110412626 -1.616576841774  0.032937948849
  2.084186643139  2.293984793080 -0.480493513306  0.019040012419
  2.876642520254  1.658030225134  0.559035126479  0.016972220142
  3.282919570196  0.368088739237  1.091983758387  0.019663385996
  3.142768358070 -1.067043033853  0.908139383421  0.018448361562
  2.511469270857 -2.081300494445  0.080016452498  0.016716167673
  1.638028059662 -2.274626132712 -1.065745290088  0.024575234678
  0.866959534092 -1.570765766224 -2.077218589767  0.042722258984
  0.467932162408  1.694535407938 -1.941510815499  0.035438633029
  1.299770347024  2.453875604722 -0.850324284820  0.010629136127
  2.242050270258  2.245320705184  0.385739526123  0.000606082864
  2.923104801922  1.151131828431  1.279135167181  0.003604423008
  3.074302957696 -0.397105856761  1.477477125466  0.005422740189
  2.636007061705 -1.788714946455  0.902532611959  0.001304993646
  1.781095866662 -2.474797662024 -0.218920775640  0.003408775380
  0.846775315928 -2.184739733041 -1.444543821640  0.021849845672
  0.255868904327 -1.360657816862 -2.219684436601  0.041864253161
  0.427045418766 -0.580992742084 -2.585105089006  0.052119483206
  0.427044967836  0.580963354323 -2.585108185092  0.052119763747
 -0.427035838172 -0.000014913339 -2.543076305315  0.056973507105
 end_of_phi


 nsubv after electrostatic potential =  1

  Confirmation of dielectric energy for ground state
  edielnew =  -0.0104563943

 =========== Executing IN-CORE method ==========
 norm multnew:  0.0791428405
 threx: total number of loops= 14
 threx: total number of loops= 24
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331914E+01-0.309165E+01-0.410226E+01-0.407883E+01-0.475210E+01-0.534847E+01-0.433670E+01-0.358739E+01-0.342330E+01-0.317821E+01
 -0.403664E+01-0.263325E+01-0.362338E+01-0.366481E+01-0.366999E+01-0.363955E+01-0.338751E+01-0.303510E+01-0.289433E+01-0.330517E+02
 -0.787312E+01-0.671622E+01-0.695563E+01-0.711691E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         2        65         3         2        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         3       129         4         3        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         4       193         5         4        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         5       257         6         5        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         6       321         7         6        12        64       128         6       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331914E+01-0.309165E+01-0.410226E+01-0.407883E+01-0.475210E+01-0.534847E+01-0.433670E+01-0.358739E+01-0.342330E+01-0.317821E+01
 -0.403664E+01-0.263325E+01-0.362338E+01-0.366481E+01-0.366999E+01-0.363955E+01-0.338751E+01-0.303510E+01-0.289433E+01-0.330517E+02
 -0.787312E+01-0.671622E+01-0.695563E+01-0.711691E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         2        65         3         2        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         3       129         4         3        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         4       193         5         4        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         5       257         6         5        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         6       321         7         6        12        64       128         6       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331914E+01-0.309165E+01-0.410226E+01-0.407883E+01-0.475210E+01-0.534847E+01-0.433670E+01-0.358739E+01-0.342330E+01-0.317821E+01
 -0.403664E+01-0.263325E+01-0.362338E+01-0.366481E+01-0.366999E+01-0.363955E+01-0.338751E+01-0.303510E+01-0.289433E+01-0.330517E+02
 -0.787312E+01-0.671622E+01-0.695563E+01-0.711691E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         2        65         3         2        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         3       129         4         3        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         4       193         5         4        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         5       257         6         5        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         6       321         7         6        12        64       128         6       380       380


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        40 xx:       135 ww:       255
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        20 yw:        25

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        39
task #   9:       127    task #  10:       247    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 matrix-vector product formed for vector   2
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference-subspace vector overlap matrix  block   1

                ci   1         ci   2
 ref:   1     1.00000000     0.00000000

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97633721    -0.21625367
 subspace has dimension  2

          (h-repnuc*1) matrix in the subspace basis

                x:   1         x:   2
   x:   1   -85.40090838
   x:   2    -0.25187978    -6.45464988

          overlap matrix in the subspace basis

                x:   1         x:   2
   x:   1     1.00000000
   x:   2     0.00000000     0.07914284

          eigenvectors and eigenvalues in the subspace basis

                v:   1         v:   2

   energy   -85.59922135   -81.35865206

   x:   1     0.97633721    -0.21625367
   x:   2     0.76870139     3.47051580

          eigenvectors and eigenvalues in the schmidt orthonormalized basis

                v:   1         v:   2

   energy   -85.59922135   -81.35865206

  zx:   1     0.97633721    -0.21625367
  zx:   2     0.21625367     0.97633721

          <ref|baseci> overlap matrix  block   1

                ci   1         ci   2
 ref:   1     1.00000000     0.00000000

  write the transformed vectors back to cosmofile

 rtflw =  0
 freezing =  F

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1    -76.2462585708 -9.1546E+00  6.4774E-03  1.5760E-01  1.0000E-03
 mr-sdci #  2  2    -72.0056892796  7.2006E+01  0.0000E+00  1.7220E+00  1.0000E-04


 total energy in cosmo calc 
e(rootcalc) + repnuc=     -76.2462585708
dielectric energy =      -0.0104563943
deltaediel =       0.0104563943
e(rootcalc) + repnuc - ediel =     -76.2358021765
e(rootcalc) + repnuc - edielnew =     -76.2462585708
deltaelast =      76.2358021765

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.01   0.00   0.00   0.01         0.    0.0000
    2   25    0   0.01   0.00   0.00   0.01         0.    0.0000
    3   26    0   0.02   0.00   0.00   0.01         0.    0.0000
    4   11    0   0.02   0.00   0.00   0.01         0.    0.0000
    5   15    0   0.01   0.00   0.00   0.01         0.    0.0000
    6   16    0   0.02   0.00   0.00   0.01         0.    0.0000
    7    1    0   0.01   0.00   0.00   0.00         0.    0.0000
    8    5    0   0.02   0.00   0.00   0.01         0.    0.0000
    9    6    0   0.03   0.00   0.00   0.03         0.    0.0000
   10    7    0   0.02   0.00   0.00   0.02         0.    0.0000
   11   75    0   0.01   0.00   0.00   0.01         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.01   0.00   0.00   0.01         0.    0.0000
   14   47    0   0.02   0.00   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.2100s 
time spent in multnx:                   0.1600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0200s 
total time per CI iteration:            3.0800s 

          starting ci iteration   3

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.35296278

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=      10  D0X=      30  D0W=      50
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================

          ci-one-electron density block   1

                mo   1         mo   2         mo   3         mo   4         mo   5         mo   6         mo   7         mo   8
   mo   1     1.99994006
   mo   2    -0.00004030     1.98830072
   mo   3     0.00000000     0.00000020     1.97100432
   mo   4    -0.00012930     0.00350194    -0.00000060     1.97339477
   mo   5     0.00000000     0.00000001    -0.00000002    -0.00000013     1.97467402
   mo   6    -0.00001189     0.00192612    -0.00001525    -0.02689911    -0.00000690     0.00630079
   mo   7     0.00000000     0.00000504    -0.01662831    -0.00001686    -0.00000418     0.00000040     0.00818021
   mo   8    -0.00000001     0.00001189    -0.01417521    -0.00001592     0.00000235     0.00000038     0.00692009     0.00925194
   mo   9    -0.00002918     0.00258691    -0.00000951    -0.00335291     0.00000228     0.00608637     0.00000014     0.00000014
   mo  10    -0.00006957     0.00246380    -0.00001024     0.00066687    -0.00000003     0.00388105     0.00000010     0.00000010
   mo  11     0.00000003    -0.00000124     0.00000250    -0.00000020    -0.00400926     0.00000001    -0.00000002    -0.00000002
   mo  12     0.00000000    -0.00000249     0.00137104    -0.00000365    -0.00000176     0.00000004     0.00668743     0.00407073
   mo  13     0.00001498    -0.00001587    -0.00000951    -0.01104214    -0.00000215     0.00152012     0.00000019     0.00000018
   mo  14     0.00000000     0.00000163    -0.00000268    -0.00000095    -0.00000160     0.00000002     0.00000003     0.00000002
   mo  15     0.00000000     0.00000082    -0.00000152    -0.00000388    -0.00797777     0.00000009     0.00000003     0.00000000
   mo  16    -0.00002849     0.00136012    -0.00000393    -0.00064908     0.00000136    -0.00110549     0.00000005     0.00000005
   mo  17     0.00000000    -0.00000628    -0.00053377     0.00001637     0.00000038    -0.00000025     0.00021545     0.00014327
   mo  18     0.00000001    -0.00000069    -0.00008945    -0.00000099     0.00000069     0.00000001    -0.00258026    -0.00352668
   mo  19     0.00003756    -0.00039170    -0.00000029    -0.00042707     0.00000016    -0.00249837     0.00000000     0.00000000
   mo  20     0.00000000     0.00000013    -0.00000039     0.00000112     0.00171200    -0.00000002     0.00000000     0.00000001
   mo  21     0.00000000    -0.00000020     0.00000054    -0.00000049    -0.00000110     0.00000001    -0.00000001     0.00000000
   mo  22     0.00000204     0.00010035     0.00000198    -0.00134052     0.00000032     0.00000661    -0.00000001     0.00000000
   mo  23    -0.00002234     0.00009865    -0.00000038     0.00023759    -0.00000045     0.00060767     0.00000000     0.00000000
   mo  24     0.00000000    -0.00000041    -0.00058949     0.00000144     0.00000004    -0.00000002    -0.00088160     0.00025176

                mo   9         mo  10         mo  11         mo  12         mo  13         mo  14         mo  15         mo  16
   mo   9     0.00833775
   mo  10     0.00387436     0.01191314
   mo  11     0.00000000     0.00000000     0.01780675
   mo  12     0.00000000     0.00000000     0.00000000     0.00690250
   mo  13     0.00020213    -0.00056106     0.00000000     0.00000002     0.00320540
   mo  14     0.00000000     0.00000000     0.00000000     0.00000000     0.00000001     0.00258371
   mo  15     0.00000000     0.00000000    -0.00185918     0.00000001     0.00000003     0.00000001     0.00195723
   mo  16     0.00044404     0.00302291     0.00000000     0.00000000    -0.00106342     0.00000000     0.00000000     0.00401611
   mo  17    -0.00000004    -0.00000001     0.00000000     0.00030353    -0.00000010     0.00000000     0.00000000    -0.00000001
   mo  18     0.00000000     0.00000000     0.00000000    -0.00176074     0.00000000     0.00000000     0.00000000     0.00000000
   mo  19    -0.00343191    -0.00093181     0.00000000     0.00000000    -0.00063753     0.00000000     0.00000000    -0.00007361
   mo  20     0.00000000     0.00000000     0.00018381     0.00000000    -0.00000001     0.00000000    -0.00150923     0.00000000
   mo  21     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00170347     0.00000000     0.00000000
   mo  22    -0.00043171     0.00028722     0.00000000     0.00000000     0.00142108     0.00000000     0.00000000     0.00056615
   mo  23     0.00016517     0.00173439     0.00000000     0.00000000     0.00018478     0.00000000     0.00000000    -0.00050628
   mo  24     0.00000000     0.00000000     0.00000000    -0.00155879    -0.00000001     0.00000000     0.00000000     0.00000000

                mo  17         mo  18         mo  19         mo  20         mo  21         mo  22         mo  23         mo  24
   mo  17     0.00038600
   mo  18    -0.00013617     0.00198159
   mo  19     0.00000000     0.00000000     0.00220692
   mo  20     0.00000000     0.00000000     0.00000000     0.00199551
   mo  21     0.00000000     0.00000000     0.00000000     0.00000000     0.00188124
   mo  22    -0.00000002     0.00000000    -0.00013384     0.00000000     0.00000000     0.00149683
   mo  23     0.00000000     0.00000000     0.00043775     0.00000000     0.00000000    -0.00007162     0.00121496
   mo  24    -0.00000985    -0.00035779     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00106754


*****   symmetry block SYM1   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       1.99994006
       2      -0.00004030     1.98830072
       3       0.00000000     0.00000020     1.97100432
       4      -0.00012930     0.00350194    -0.00000060     1.97339477
       5       0.00000000     0.00000001    -0.00000002    -0.00000013
       6      -0.00001189     0.00192612    -0.00001525    -0.02689911
       7       0.00000000     0.00000504    -0.01662831    -0.00001686
       8      -0.00000001     0.00001189    -0.01417521    -0.00001592
       9      -0.00002918     0.00258691    -0.00000951    -0.00335291
      10      -0.00006957     0.00246380    -0.00001024     0.00066687
      11       0.00000003    -0.00000124     0.00000250    -0.00000020
      12       0.00000000    -0.00000249     0.00137104    -0.00000365
      13       0.00001498    -0.00001587    -0.00000951    -0.01104214
      14       0.00000000     0.00000163    -0.00000268    -0.00000095
      15       0.00000000     0.00000082    -0.00000152    -0.00000388
      16      -0.00002849     0.00136012    -0.00000393    -0.00064908
      17       0.00000000    -0.00000628    -0.00053377     0.00001637
      18       0.00000001    -0.00000069    -0.00008945    -0.00000099
      19       0.00003756    -0.00039170    -0.00000029    -0.00042707
      20       0.00000000     0.00000013    -0.00000039     0.00000112
      21       0.00000000    -0.00000020     0.00000054    -0.00000049
      22       0.00000204     0.00010035     0.00000198    -0.00134052
      23      -0.00002234     0.00009865    -0.00000038     0.00023759
      24       0.00000000    -0.00000041    -0.00058949     0.00000144

               Column   5     Column   6     Column   7     Column   8
       5       1.97467402
       6      -0.00000690     0.00630079
       7      -0.00000418     0.00000040     0.00818021
       8       0.00000235     0.00000038     0.00692009     0.00925194
       9       0.00000228     0.00608637     0.00000014     0.00000014
      10      -0.00000003     0.00388105     0.00000010     0.00000010
      11      -0.00400926     0.00000001    -0.00000002    -0.00000002
      12      -0.00000176     0.00000004     0.00668743     0.00407073
      13      -0.00000215     0.00152012     0.00000019     0.00000018
      14      -0.00000160     0.00000002     0.00000003     0.00000002
      15      -0.00797777     0.00000009     0.00000003     0.00000000
      16       0.00000136    -0.00110549     0.00000005     0.00000005
      17       0.00000038    -0.00000025     0.00021545     0.00014327
      18       0.00000069     0.00000001    -0.00258026    -0.00352668
      19       0.00000016    -0.00249837     0.00000000     0.00000000
      20       0.00171200    -0.00000002     0.00000000     0.00000001
      21      -0.00000110     0.00000001    -0.00000001     0.00000000
      22       0.00000032     0.00000661    -0.00000001     0.00000000
      23      -0.00000045     0.00060767     0.00000000     0.00000000
      24       0.00000004    -0.00000002    -0.00088160     0.00025176

               Column   9     Column  10     Column  11     Column  12
       9       0.00833775
      10       0.00387436     0.01191314
      11       0.00000000     0.00000000     0.01780675
      12       0.00000000     0.00000000     0.00000000     0.00690250
      13       0.00020213    -0.00056106     0.00000000     0.00000002
      14       0.00000000     0.00000000     0.00000000     0.00000000
      15       0.00000000     0.00000000    -0.00185918     0.00000001
      16       0.00044404     0.00302291     0.00000000     0.00000000
      17      -0.00000004    -0.00000001     0.00000000     0.00030353
      18       0.00000000     0.00000000     0.00000000    -0.00176074
      19      -0.00343191    -0.00093181     0.00000000     0.00000000
      20       0.00000000     0.00000000     0.00018381     0.00000000
      21       0.00000000     0.00000000     0.00000000     0.00000000
      22      -0.00043171     0.00028722     0.00000000     0.00000000
      23       0.00016517     0.00173439     0.00000000     0.00000000
      24       0.00000000     0.00000000     0.00000000    -0.00155879

               Column  13     Column  14     Column  15     Column  16
      13       0.00320540
      14       0.00000001     0.00258371
      15       0.00000003     0.00000001     0.00195723
      16      -0.00106342     0.00000000     0.00000000     0.00401611
      17      -0.00000010     0.00000000     0.00000000    -0.00000001
      18       0.00000000     0.00000000     0.00000000     0.00000000
      19      -0.00063753     0.00000000     0.00000000    -0.00007361
      20      -0.00000001     0.00000000    -0.00150923     0.00000000
      21       0.00000000    -0.00170347     0.00000000     0.00000000
      22       0.00142108     0.00000000     0.00000000     0.00056615
      23       0.00018478     0.00000000     0.00000000    -0.00050628
      24      -0.00000001     0.00000000     0.00000000     0.00000000

               Column  17     Column  18     Column  19     Column  20
      17       0.00038600
      18      -0.00013617     0.00198159
      19       0.00000000     0.00000000     0.00220692
      20       0.00000000     0.00000000     0.00000000     0.00199551
      21       0.00000000     0.00000000     0.00000000     0.00000000
      22      -0.00000002     0.00000000    -0.00013384     0.00000000
      23       0.00000000     0.00000000     0.00043775     0.00000000
      24      -0.00000985    -0.00035779     0.00000000     0.00000000

               Column  21     Column  22     Column  23     Column  24
      21       0.00188124
      22       0.00000000     0.00149683
      23       0.00000000    -0.00007162     0.00121496
      24       0.00000000     0.00000000     0.00000000     0.00106754

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7        eno   8
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    8   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno   9        eno  10        eno  11        eno  12        eno  13        eno  14        eno  15        eno  16
 emo    9   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   10   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   11   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   12   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   13   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   14   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   15   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   16   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno  17        eno  18        eno  19        eno  20        eno  21        eno  22        eno  23        eno  24
 emo   17   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   18   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   19   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   20   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   21   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   22   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   23   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   24   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   1.9999410      1.9890990      1.9747160      1.9730424      1.9712496
   0.0210628      0.0192653      0.0180253      0.0094479      0.0048692
   0.0045091      0.0039718      0.0034594      0.0033229      0.0007692
   0.0007606      0.0004932      0.0004507      0.0004079      0.0003694
   0.0003434      0.0003426      0.0000437      0.0000379

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7         no   8
  mo    1   0.999971E+00   0.628062E-02  -0.231027E-07   0.434950E-02   0.169086E-06   0.712731E-08   0.398934E-04  -0.146606E-07
  mo    2  -0.516014E-02   0.975104E+00   0.108970E-05  -0.221677E+00  -0.981532E-05  -0.456183E-05  -0.211976E-02   0.667634E-06
  mo    3   0.965835E-08   0.598362E-06  -0.140839E-05  -0.420382E-04   0.999937E+00   0.901510E-02   0.979709E-05  -0.133982E-05
  mo    4  -0.563303E-02   0.221640E+00  -0.545421E-05   0.974998E+00   0.405880E-04   0.109048E-04   0.742616E-02  -0.137139E-06
  mo    5  -0.164470E-08   0.135114E-06   0.999989E+00   0.550134E-05   0.139955E-05   0.111229E-05   0.102528E-05   0.153432E-02
  mo    6   0.649410E-04  -0.205532E-02  -0.342617E-05  -0.135621E-01  -0.835543E-05   0.159214E-04   0.475201E+00   0.506602E-06
  mo    7   0.330343E-07   0.598513E-06  -0.211265E-05  -0.861668E-05  -0.849325E-02   0.604866E+00  -0.186927E-04   0.872148E-06
  mo    8   0.104155E-07   0.406897E-05   0.119741E-05  -0.898026E-05  -0.725305E-02   0.580847E+00  -0.172294E-04   0.121939E-05
  mo    9  -0.117951E-04   0.894888E-03   0.115940E-05  -0.199808E-02  -0.496289E-05   0.176998E-04   0.559149E+00   0.124329E-05
  mo   10  -0.432302E-04   0.128878E-02  -0.178661E-07   0.230122E-04  -0.525386E-05   0.186329E-04   0.630725E+00   0.144241E-05
  mo   11   0.185051E-07  -0.635624E-06  -0.204483E-02   0.300015E-07   0.127535E-05  -0.169159E-05  -0.191942E-05   0.992783E+00
  mo   12   0.152162E-07  -0.162111E-05  -0.898997E-06  -0.160678E-05   0.654185E-03   0.488835E+00  -0.165151E-04   0.800313E-06
  mo   13   0.387561E-04  -0.124228E-02  -0.106520E-05  -0.547450E-02  -0.506359E-05   0.233567E-05   0.277569E-01  -0.178551E-06
  mo   14  -0.185062E-08   0.696256E-06  -0.810988E-06  -0.654111E-06  -0.136356E-05   0.280634E-06   0.104665E-06  -0.778171E-07
  mo   15   0.765332E-08  -0.297163E-07  -0.404266E-02  -0.203457E-05  -0.777801E-06   0.336512E-06   0.590816E-06  -0.117759E+00
  mo   16  -0.160810E-04   0.599487E-03   0.697105E-06  -0.464653E-03  -0.202022E-05   0.306121E-05   0.103125E+00   0.434098E-06
  mo   17  -0.300753E-07  -0.125379E-05   0.192685E-06   0.880754E-05  -0.272167E-03   0.188333E-01  -0.182862E-05   0.563207E-07
  mo   18   0.957506E-08  -0.457044E-06   0.351890E-06  -0.379714E-06  -0.218165E-04  -0.233428E+00   0.780500E-05  -0.417152E-06
  mo   19   0.209589E-04  -0.238920E-03   0.856351E-07  -0.144617E-03  -0.130574E-06  -0.649316E-05  -0.216291E+00  -0.396294E-06
  mo   20  -0.297141E-08   0.189372E-06   0.870729E-03   0.547907E-06  -0.194658E-06  -0.344962E-07  -0.191659E-06   0.226352E-01
  mo   21   0.165124E-08  -0.155090E-06  -0.554803E-06  -0.218368E-06   0.274732E-06  -0.929391E-07   0.213732E-07  -0.634116E-07
  mo   22   0.455651E-05  -0.100964E-03   0.165203E-06  -0.677892E-03   0.975343E-06   0.135499E-06   0.305135E-02   0.885239E-08
  mo   23  -0.121091E-04   0.750649E-04  -0.230433E-06   0.101613E-03  -0.196047E-06   0.219323E-05   0.739390E-01   0.123957E-06
  mo   24  -0.199231E-08  -0.376588E-07   0.227048E-07   0.774277E-06  -0.296828E-03  -0.535620E-01   0.182352E-05  -0.865540E-07

                no   9         no  10         no  11         no  12         no  13         no  14         no  15         no  16
  mo    1   0.179864E-04  -0.209106E-08  -0.566190E-05   0.830807E-10   0.118161E-05   0.121340E-09  -0.227167E-05  -0.130864E-08
  mo    2  -0.171115E-03   0.414353E-05   0.287078E-03  -0.703959E-06  -0.246508E-03   0.170381E-06  -0.252080E-03  -0.333345E-05
  mo    3  -0.164728E-05  -0.346385E-02   0.282021E-05   0.122570E-05   0.157191E-05  -0.250170E-06   0.205851E-05   0.347166E-02
  mo    4  -0.696237E-02  -0.223009E-05   0.531635E-02   0.214014E-06   0.184829E-02  -0.172726E-05   0.172715E-03   0.632163E-05
  mo    5  -0.137861E-05   0.159423E-05   0.212117E-05   0.291730E-06  -0.429381E-06  -0.353320E-02  -0.784943E-06  -0.601834E-06
  mo    6  -0.362214E+00  -0.203084E-05   0.192209E+00  -0.272679E-06  -0.111643E+00  -0.405859E-06  -0.913831E-01   0.420854E-04
  mo    7   0.950762E-06   0.185788E+00   0.261752E-05   0.347044E-06   0.830297E-06  -0.182254E-06   0.391234E-04   0.111314E+00
  mo    8  -0.812384E-06  -0.627412E+00  -0.921194E-05  -0.102652E-05  -0.169343E-05  -0.162675E-06   0.107266E-03   0.351978E+00
  mo    9  -0.390442E+00   0.757727E-05  -0.400015E+00   0.253580E-06   0.501217E-01  -0.398946E-06   0.401163E+00  -0.130376E-03
  mo   10   0.634560E+00  -0.398899E-05   0.273067E+00  -0.258306E-06  -0.130966E+00   0.843408E-06  -0.233712E+00   0.696269E-04
  mo   11  -0.423421E-06   0.262512E-06   0.714266E-06   0.386217E-06   0.252201E-06  -0.958759E-01  -0.112197E-06  -0.484321E-08
  mo   12   0.274425E-05   0.606354E+00   0.797115E-05   0.502921E-06   0.603477E-06   0.683711E-06  -0.836167E-04  -0.261909E+00
  mo   13  -0.236072E+00  -0.994672E-05   0.609218E+00  -0.298962E-06   0.486428E+00   0.387254E-05   0.803019E-01  -0.239150E-04
  mo   14  -0.159420E-06  -0.117750E-05   0.765363E-06   0.775223E+00  -0.292656E-06   0.227590E-05   0.122759E-06   0.832361E-06
  mo   15  -0.253919E-06   0.479628E-06   0.282905E-05   0.264355E-05   0.209487E-05  -0.666177E+00  -0.320588E-06   0.359039E-06
  mo   16   0.434124E+00   0.289283E-05  -0.426234E+00   0.731489E-06   0.570674E+00  -0.486476E-06   0.247331E+00  -0.683111E-04
  mo   17   0.668339E-06   0.228076E-01  -0.216330E-05   0.235660E-07  -0.204325E-05   0.980434E-07  -0.822832E-04  -0.262113E+00
  mo   18  -0.505756E-06   0.273990E+00   0.550946E-05   0.758071E-06   0.226528E-05   0.268087E-06   0.221881E-03   0.704763E+00
  mo   19   0.249461E+00  -0.360841E-05   0.157214E+00  -0.180865E-06  -0.252131E+00  -0.697178E-06   0.509064E+00  -0.144473E-03
  mo   20   0.152986E-06  -0.508213E-06  -0.233704E-05  -0.231069E-05  -0.220299E-05   0.739597E+00  -0.623759E-06   0.145831E-06
  mo   21   0.400274E-07   0.796893E-06  -0.398967E-06  -0.631688E+00   0.418394E-06  -0.276051E-05   0.179502E-06   0.156541E-05
  mo   22   0.287920E-01  -0.481718E-05   0.275227E+00  -0.801402E-08   0.512471E+00   0.290342E-05   0.140325E+00  -0.540679E-04
  mo   23   0.799267E-01  -0.338817E-05   0.274155E+00  -0.355216E-06  -0.281763E+00   0.475137E-06   0.655468E+00  -0.213199E-03
  mo   24  -0.592062E-06  -0.358565E+00  -0.581841E-05  -0.554245E-06  -0.144085E-05  -0.465520E-06  -0.146372E-03  -0.479289E+00

                no  17         no  18         no  19         no  20         no  21         no  22         no  23         no  24
  mo    1   0.349632E-09  -0.368376E-08  -0.147203E-04  -0.152701E-09   0.168087E-07   0.497656E-05   0.625177E-05   0.148146E-08
  mo    2  -0.441943E-06  -0.790486E-06  -0.265041E-03  -0.301578E-06   0.279788E-05   0.142182E-03  -0.247708E-04  -0.161349E-06
  mo    3   0.669566E-06   0.321521E-02   0.192453E-05   0.586272E-06   0.181737E-02  -0.111701E-04   0.885813E-06   0.252376E-02
  mo    4   0.532018E-06   0.498446E-05   0.624535E-02   0.101294E-05  -0.201865E-04  -0.367339E-02   0.588356E-02   0.429014E-06
  mo    5   0.964392E-06   0.126688E-05   0.148333E-05   0.253869E-02  -0.218627E-06  -0.682642E-06   0.212867E-05   0.107221E-05
  mo    6   0.254146E-05   0.447988E-04   0.511841E+00  -0.119934E-04  -0.327573E-03  -0.897244E-01   0.561254E+00   0.100879E-04
  mo    7   0.447996E-05   0.556995E+00  -0.498893E-04  -0.403754E-05   0.115841E+00  -0.429371E-03  -0.123057E-04   0.513316E+00
  mo    8  -0.267084E-05  -0.241988E+00   0.707466E-05   0.177234E-05   0.669534E-01  -0.260447E-03   0.434676E-05  -0.286336E+00
  mo    9  -0.721459E-06  -0.105674E-04  -0.582151E-01  -0.816496E-06   0.123286E-03   0.314222E-01  -0.455051E+00  -0.961404E-05
  mo   10  -0.421580E-06  -0.518912E-05  -0.654218E-01   0.570447E-06   0.369327E-04   0.953387E-02  -0.220926E+00  -0.365786E-05
  mo   11   0.718242E-06   0.778588E-06   0.250538E-05   0.720109E-01  -0.216046E-06   0.255875E-05  -0.177451E-06  -0.361816E-07
  mo   12  -0.119897E-05  -0.166991E+00   0.216765E-04   0.358177E-06  -0.862484E-01   0.325890E-03   0.124518E-04  -0.538009E+00
  mo   13   0.115302E-06   0.135733E-05  -0.186975E+00   0.234616E-04  -0.198519E-02  -0.509696E+00  -0.185761E+00  -0.514432E-05
  mo   14   0.631688E+00  -0.505536E-05  -0.249266E-05  -0.637818E-05  -0.101186E-05   0.151847E-05  -0.473316E-06  -0.429911E-06
  mo   15   0.657205E-05   0.655232E-05   0.243797E-04   0.736427E+00  -0.208073E-05   0.268814E-04  -0.391890E-05  -0.153710E-05
  mo   16   0.118492E-05   0.159242E-04   0.723908E-01   0.991014E-05  -0.111377E-02  -0.286642E+00   0.380761E+00   0.593410E-05
  mo   17  -0.203814E-06  -0.263699E+00  -0.217294E-04   0.550394E-05   0.921932E+00  -0.359455E-02   0.107517E-04   0.104468E+00
  mo   18   0.158036E-05   0.319544E+00  -0.543770E-04  -0.291917E-05   0.334997E+00  -0.127693E-02   0.515551E-05  -0.399271E+00
  mo   19   0.275118E-05   0.522989E-04   0.637043E+00  -0.144961E-04  -0.844307E-03  -0.226316E+00  -0.294129E+00  -0.790377E-05
  mo   20   0.696701E-05   0.658115E-05   0.235674E-04   0.672669E+00  -0.195630E-05   0.236839E-04  -0.227857E-05  -0.969211E-06
  mo   21   0.775223E+00  -0.583433E-05  -0.302346E-05  -0.716099E-05  -0.101391E-05   0.178550E-05  -0.523947E-06  -0.291796E-06
  mo   22  -0.973564E-06  -0.980556E-05   0.252562E+00  -0.361461E-04   0.295514E-02   0.757204E+00  -0.627613E-01   0.606441E-06
  mo   23  -0.207409E-05  -0.433452E-04  -0.469573E+00   0.124294E-04   0.576980E-03   0.155924E+00   0.398928E+00   0.769344E-05
  mo   24   0.580375E-05   0.657006E+00  -0.475637E-04  -0.637807E-05   0.111623E+00  -0.416373E-03   0.957385E-05  -0.441279E+00
  eigenvectors of nos in ao-basis, column    1
  0.1000471E+01 -0.3582950E-02 -0.1835547E-02 -0.1663038E-07  0.2403930E-07 -0.6432917E-02  0.3650658E-07 -0.3490024E-07
  0.1190835E-02  0.2009167E-08 -0.4381429E-08  0.2898196E-04  0.3537162E-08  0.8797579E-04 -0.3083809E-03  0.6906893E-03
 -0.6259178E-03  0.4101242E-08 -0.6334907E-03 -0.3083978E-03  0.6907460E-03  0.6259092E-03  0.5382459E-08 -0.6335483E-03
  eigenvectors of nos in ao-basis, column    2
 -0.3273569E-02  0.9083896E+00 -0.8482182E-01 -0.2994812E-06 -0.7684407E-06  0.6049653E-01 -0.3965348E-05  0.1037519E-05
  0.7030041E-01 -0.2560579E-06  0.2030889E-06 -0.7830217E-03 -0.3636233E-06  0.9785590E-03  0.2519522E+00 -0.1232884E+00
  0.3072051E-01  0.4735587E-06  0.2136945E-01  0.2519671E+00 -0.1232966E+00 -0.3072284E-01 -0.3637930E-06  0.2136765E-01
  eigenvectors of nos in ao-basis, column    3
 -0.4798033E-06 -0.3907247E-05 -0.1070986E-05  0.7973359E-06  0.9162250E+00 -0.4168885E-05 -0.1528557E-05  0.7761927E-01
  0.1462956E-05 -0.4903551E-06 -0.1673425E-01 -0.2299923E-07 -0.8490052E-07 -0.2186505E-06 -0.2610191E-06  0.6740477E-05
  0.1227116E-05  0.2876725E-01 -0.1816078E-06  0.2830015E-05 -0.1518118E-05 -0.7033092E-06  0.2876879E-01 -0.5479084E-06
  eigenvectors of nos in ao-basis, column    4
  0.4041325E-02  0.8852085E-01  0.2000888E+00 -0.2116826E-04  0.5237198E-05  0.8077152E+00  0.6807229E-06  0.1379407E-05
 -0.9805768E-02 -0.1498260E-06  0.7973406E-06 -0.4742341E-02  0.6912177E-06 -0.2562698E-02 -0.4077219E+00  0.1747494E+00
 -0.3823903E-01 -0.1704257E-05  0.6780832E-03 -0.4077873E+00  0.1747657E+00  0.3824792E-01 -0.6400849E-06  0.6903103E-03
  eigenvectors of nos in ao-basis, column    5
  0.8480219E-06  0.2343239E-05  0.2231942E-05  0.7211340E+00  0.3134245E-05  0.4346041E-04 -0.1053807E+00 -0.1522946E-05
 -0.3111575E-05  0.4703732E-06 -0.1104344E-06 -0.2969532E-06 -0.2622986E-01  0.1490198E-06 -0.5515171E+00  0.1979795E+00
 -0.2406378E-01 -0.1458720E-05 -0.3352491E-01  0.5515000E+00 -0.1979673E+00 -0.2406220E-01  0.2032616E-06  0.3351600E-01
  eigenvectors of nos in ao-basis, column    6
  0.2842789E-06  0.8764838E-05  0.1117633E-04 -0.1312560E+01 -0.1385132E-05 -0.3177412E-04  0.6706686E+00  0.2741998E-05
  0.3000195E-04 -0.1358350E-06 -0.1028070E-06 -0.3543988E-06 -0.3952429E-01 -0.4758763E-06 -0.8818200E+00  0.2581596E+00
 -0.1365262E-01  0.4284028E-06  0.1716320E-02  0.8817633E+00 -0.2581394E+00 -0.1364912E-01  0.1140919E-06 -0.1712297E-02
  eigenvectors of nos in ao-basis, column    7
 -0.1805200E-02  0.2339292E+00  0.3156053E+00  0.4982669E-04 -0.1804366E-05 -0.1351579E+01 -0.2423095E-04  0.3019147E-05
  0.1070207E+01  0.9425016E-08 -0.3052110E-06 -0.7975547E-02  0.8344811E-06 -0.1303310E-01 -0.6917930E+00  0.2430703E+00
 -0.3455144E-01  0.4793810E-06  0.4073257E-01 -0.6918349E+00  0.2430824E+00  0.3455093E-01  0.3194434E-06  0.4073108E-01
  eigenvectors of nos in ao-basis, column    8
 -0.2135929E-06 -0.3978050E-07  0.1227198E-05 -0.3217205E-05  0.1408833E+01 -0.3075876E-05  0.1218002E-05 -0.1610330E+01
  0.2625022E-05 -0.5796307E-07  0.3811673E-01 -0.1716485E-07 -0.6818656E-07 -0.5531031E-07 -0.2313506E-05  0.1446558E-05
  0.7445885E-07 -0.7610407E-01  0.1148348E-06 -0.2932603E-07 -0.5402666E-08 -0.3343056E-07 -0.7610392E-01  0.4598731E-08
  eigenvectors of nos in ao-basis, column    9
 -0.4485840E+00 -0.2371377E+01  0.2039913E+01 -0.5205663E-05 -0.1845466E-05 -0.3500014E+00  0.5296664E-05  0.7159633E-06
  0.8317190E+00  0.6347232E-07  0.2276469E-06 -0.1245151E-01  0.7183490E-06  0.7467507E-01  0.7178265E+00 -0.3901529E+00
  0.3782897E-02 -0.2735034E-06  0.1031056E+00  0.7178222E+00 -0.3901510E+00 -0.3782143E-02 -0.8522014E-07  0.1031068E+00
  eigenvectors of nos in ao-basis, column   10
 -0.2529600E-05 -0.7470931E-05  0.1632875E-04 -0.3935922E+00  0.1799106E-05 -0.4924739E-05  0.1045453E+01 -0.4488213E-06
  0.1104816E-05  0.1006291E-05 -0.6384301E-06  0.1631631E-05  0.5622678E+00 -0.3295382E-05  0.8517104E+00 -0.6195106E+00
  0.1197817E+00 -0.2894700E-06 -0.2575103E-01 -0.8517281E+00  0.6195234E+00  0.1197782E+00  0.7191568E-06  0.2574185E-01
  eigenvectors of nos in ao-basis, column   11
  0.3413489E+00  0.1520100E+01 -0.2081167E+01 -0.1278727E-05  0.2749672E-05  0.2931802E-01  0.1420680E-04 -0.1979523E-05
  0.1872946E+00 -0.5264720E-06 -0.2937996E-05 -0.8398665E-01  0.9250356E-05  0.2432921E+00  0.7578644E+00 -0.4377312E+00
 -0.7618084E-01  0.1674781E-05  0.2235338E+00  0.7578410E+00 -0.4377131E+00  0.7618272E-01  0.9284766E-06  0.2235305E+00
  eigenvectors of nos in ao-basis, column   12
 -0.4538864E-06 -0.2626477E-05  0.2903434E-05  0.1148547E-05  0.6188989E-06  0.3423137E-06  0.3520917E-06 -0.1456593E-05
 -0.5391063E-06 -0.7772842E+00 -0.2845879E-05 -0.2473785E-07  0.9138021E-06 -0.2858226E-06 -0.5330430E-06 -0.5119222E-06
  0.1776404E-06  0.2912254E+00 -0.8319948E-07 -0.1760446E-05  0.1674090E-05  0.3345878E-06 -0.2912231E+00 -0.1337999E-06
  eigenvectors of nos in ao-basis, column   13
 -0.1431711E+00 -0.5945797E+00  0.1003625E+01  0.2557986E-05 -0.1951112E-06  0.8729469E-01  0.1067584E-05 -0.9910080E-06
 -0.5657352E+00  0.4863629E-06 -0.2631772E-05 -0.2077259E+00  0.2448789E-05 -0.1932580E+00 -0.5592914E+00  0.3081913E+00
 -0.1838222E+00  0.6657311E-06  0.1174076E+00 -0.5592955E+00  0.3081962E+00  0.1838217E+00  0.7484676E-06  0.1174041E+00
  eigenvectors of nos in ao-basis, column   14
  0.1564484E-05  0.6693040E-05 -0.9241655E-05 -0.7665911E-06 -0.8847544E-01 -0.2059944E-05  0.1128138E-05  0.3188646E+00
  0.3561703E-06 -0.3253437E-05  0.8803688E+00 -0.1022477E-05  0.6436598E-06  0.4471972E-06  0.8120653E-06  0.1566695E-05
 -0.5587639E-06 -0.2134891E+00  0.7816783E-06  0.7163896E-06  0.2135951E-06  0.8765738E-06 -0.2134901E+00  0.8421045E-06
  eigenvectors of nos in ao-basis, column   15
 -0.3515548E+00 -0.1343918E+01  0.2472753E+01  0.2022274E-03 -0.8624582E-06  0.1600157E+00 -0.3792459E-03  0.5247006E-06
 -0.1065569E+01  0.1766510E-06 -0.5861767E-06 -0.3005556E-01  0.2224786E-03  0.4151894E+00 -0.2059657E+01  0.1176337E+01
  0.9994802E-02 -0.3735680E-06 -0.8479665E-01 -0.2058986E+01  0.1176039E+01 -0.9968859E-02 -0.6788691E-06 -0.8501852E-01
  eigenvectors of nos in ao-basis, column   16
  0.9831112E-04  0.3690621E-03 -0.7217802E-03  0.6266732E+00 -0.5827747E-06 -0.2463355E-04 -0.1204357E+01 -0.2959109E-06
  0.3093602E-03  0.1571247E-05  0.1061457E-06  0.1232069E-04  0.7241281E+00 -0.1338487E-03 -0.1106466E+01  0.5185211E+00
  0.3428305E-01  0.1484904E-05  0.3487096E+00  0.1107772E+01 -0.5192948E+00  0.3427697E-01 -0.8506641E-06 -0.3486268E+00
  eigenvectors of nos in ao-basis, column   17
 -0.1681653E-05 -0.7691549E-05  0.4864187E-05  0.7161063E-05  0.1509391E-05  0.4319437E-05  0.2286037E-06 -0.7300989E-05
 -0.2626155E-05  0.7498214E+00  0.6214270E-05  0.2620747E-06 -0.7035822E-05 -0.9832986E-06  0.1956319E-04 -0.1922153E-04
  0.6134797E-05  0.7289340E+00  0.5868191E-05 -0.1070673E-04  0.1028260E-04  0.1794904E-05 -0.7289181E+00 -0.3885182E-06
  eigenvectors of nos in ao-basis, column   18
 -0.2591390E-04 -0.1176009E-03  0.3607372E-04  0.9199918E+00  0.1870915E-05  0.7754562E-04  0.8567638E-01 -0.7244561E-05
 -0.2919373E-04 -0.5604320E-05  0.5808129E-05  0.2396006E-05 -0.7707031E+00 -0.2020722E-04  0.1874666E+01 -0.1735319E+01
  0.4309466E+00  0.2079906E-05  0.6050061E+00 -0.1874444E+01  0.1735140E+01  0.4308437E+00  0.1346519E-04 -0.6049028E+00
  eigenvectors of nos in ao-basis, column   19
 -0.2400159E+00 -0.1153912E+01 -0.1486091E+00 -0.1070803E-03  0.3434389E-05  0.8514639E+00  0.7393734E-04 -0.2623123E-04
  0.1005413E+00 -0.2920652E-05  0.2070992E-04 -0.7595250E-01  0.5287577E-04 -0.1841192E+00  0.1660463E+01 -0.1169967E+01
  0.8626770E+00  0.2557906E-04  0.3683927E+00  0.1660668E+01 -0.1170203E+01 -0.8628197E+00  0.3130466E-04  0.3684588E+00
  eigenvectors of nos in ao-basis, column   20
 -0.1714824E-05  0.6547565E-06  0.6711972E-04 -0.7206211E-05  0.5944980E-01 -0.1801583E-04 -0.7062333E-05 -0.7765539E+00
 -0.4757319E-04 -0.6856336E-05  0.5853513E+00  0.1035692E-04  0.7559010E-05  0.2646005E-05 -0.1055004E-03  0.6155769E-04
 -0.4630445E-04  0.8418205E+00  0.2926101E-05 -0.6468517E-04  0.3310176E-04  0.4009781E-04  0.8418347E+00  0.1927971E-04
  eigenvectors of nos in ao-basis, column   21
  0.8518064E-03  0.3372499E-02 -0.4519235E-02  0.5360538E+00 -0.3804059E-06 -0.1175472E-02 -0.1173442E+01  0.2218068E-05
  0.4218898E-02 -0.9567515E-06 -0.1708211E-05 -0.8317761E-03 -0.1278517E+00  0.4938656E-03 -0.3957955E+00  0.5595867E-01
  0.7352447E+00 -0.3488701E-05 -0.4028617E+00  0.4005539E+00 -0.5787057E-01  0.7328534E+00 -0.1327151E-05  0.3972635E+00
  eigenvectors of nos in ao-basis, column   22
  0.2213585E+00  0.8793471E+00 -0.1149283E+01 -0.2041004E-02  0.1352952E-05 -0.3131199E+00  0.4544729E-02 -0.2800330E-04
  0.1081951E+01  0.1718806E-05  0.2047133E-04 -0.2130375E+00  0.4787855E-03  0.1301939E+00  0.5878515E+00 -0.2316114E+00
  0.2917722E+00  0.3207253E-04 -0.7255519E+00  0.5846550E+00 -0.2310704E+00 -0.2974374E+00  0.2862756E-04 -0.7287187E+00
  eigenvectors of nos in ao-basis, column   23
 -0.3292088E+00 -0.1132668E+01  0.3986587E+01  0.4386833E-05  0.1951402E-05 -0.3348054E+00  0.2866945E-04  0.3519884E-05
 -0.1003990E+01 -0.5007993E-06 -0.1794538E-05  0.2008095E-01 -0.1367988E-04  0.2033036E+00 -0.1260001E+01 -0.4226252E+00
 -0.7512414E+00 -0.4406476E-05 -0.5171330E+00 -0.1260031E+01 -0.4226796E+00  0.7512798E+00 -0.3357511E-05 -0.5171399E+00
  eigenvectors of nos in ao-basis, column   24
 -0.4837392E-05 -0.1587077E-04  0.6968820E-04 -0.3458672E+00  0.1029979E-05 -0.8343078E-05 -0.1682396E+01  0.1365746E-05
 -0.1422418E-04 -0.2574643E-06 -0.7873000E-06 -0.1305258E-06  0.5941520E+00  0.3908650E-05 -0.1057102E+01 -0.1135811E+01
 -0.7109357E+00 -0.1951880E-05 -0.6708918E+00  0.1057061E+01  0.1135791E+01 -0.7109064E+00 -0.1139438E-05  0.6708667E+00


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***


 cosurf_and_phi
  2.466315385316 -0.061539892486  2.837440149000 -0.038332470060
  0.006359863210 -2.199287675687  3.114544749440 -0.067617497374
 -0.480526931872  1.061509344774  3.755560521090 -0.069523268200
  1.262739748330  2.872870034186  1.567866969040 -0.045257550728
  1.897949994968 -2.634366215369  0.570962659525 -0.016047130201
 -2.790166444607 -0.824331206483  2.170430257018 -0.022154451817
 -2.061296060778  2.493528161233  1.034251636268 -0.022215256056
 -0.002991703587  2.420846177436 -1.447622679829  0.014577083741
 -1.114435211431 -2.949559801670 -0.067898604227 -0.016060998894
  1.955709175590 -0.984462672186  3.123501329999 -0.050884590991
  1.000900154469 -1.706149424342  3.300403738319 -0.064332634068
  1.652947872482  0.360731428865  3.496571379926 -0.057769027365
  0.594940552015  0.670555958619  3.845537291721 -0.069257816264
  2.390078334690  1.202722009900  2.566708449184 -0.037518823473
  1.989334673621  2.229216060677  2.001028520754 -0.039525326616
  2.919143888229  0.378144896597  2.099775822683 -0.017033335319
  2.780367109803 -0.921046401167  2.130496470151 -0.021863692123
  2.449941849009 -2.011800733381  1.439000205618 -0.019855625667
 -0.231540571447 -1.263621120083  3.706953994699 -0.070122809698
 -0.352106389130 -0.107618694495  3.950679056227 -0.070490957959
  0.105380882100  1.878884201281  3.371423292662 -0.068952008771
  0.783266721831  2.590884380442  2.520834408126 -0.060568431039
  2.011402526412  2.551496219550  0.814855178005 -0.018806951909
  1.440220538007 -2.843720084632  1.356653310131 -0.039760375091
  0.871209499702 -2.660587439486  2.372618950166 -0.058557995048
 -2.947488997374  0.298617382934  2.058332654596 -0.015414664856
 -2.674954162172  1.563027715274  1.704200253745 -0.018065953242
 -1.353448939749  2.910920816403  0.212056013565 -0.018989573022
 -0.743422400795  2.810699746106 -0.731960510989 -0.003151680637
  0.099626804209  1.855073908563 -1.945822518898  0.033616324505
  0.019900458911 -1.968682238442 -1.864952523830  0.030158807585
 -0.601526683996 -2.669374282549 -1.032942810695  0.004385590648
 -1.976701730993 -2.578422698078  0.816296596419 -0.019690858055
 -2.525639241426 -1.850752473194  1.593331825886 -0.020537317191
 -1.124962231191 -1.881571875050  3.121019991266 -0.062173884163
 -2.117398618237 -1.513942036836  2.667866282109 -0.044315548439
 -2.336046769995 -0.152318885910  2.976109389266 -0.042404083185
 -1.478775916646  0.469153703894  3.577445396081 -0.060488261581
 -1.328587214463  1.668007282102  3.174271253825 -0.060314233863
 -1.771761195387  2.287385908353  2.202255939279 -0.045879272107
 -1.026730149698  3.017318998523  1.358608629268 -0.044864515423
  0.119020599620  3.173161356543  1.415159765853 -0.050678049654
  0.915237473358  3.108118624501  0.463299650838 -0.030392549470
  0.462814589486  2.837210699514 -0.795516582628 -0.004010712509
  1.026812912753 -3.017319139618  0.083991013862 -0.020919231729
 -0.070059393274 -3.176418651270  0.035610954337 -0.026200645133
 -0.970268948715 -3.083313212042  1.062474740253 -0.040778499357
 -0.534203029846 -2.828286137107  2.231267851728 -0.059284253752
  0.871897873699 -0.575451888002  3.799144800425 -0.067395470115
  1.408719892640  1.640288870679  3.148120355694 -0.059197330483
  2.650053602975  1.633766196698  1.655441764425 -0.017948505734
  1.964286464189 -2.001027831890  2.365093852582 -0.044257709527
 -1.342877128538 -0.760711330777  3.581800927521 -0.062164915867
 -0.531365927285  2.661712102822  2.509437292848 -0.061931647178
  1.142904167226  2.885766749848 -0.243475252462 -0.011107835988
  0.342129526197 -3.189261637688  1.246854200824 -0.047743917847
 -2.346459445268  1.135413320199  2.662806558936 -0.039459644898
 -0.258565605057  3.205542092831  0.249849913336 -0.030383126660
  0.450566961335 -2.699565911226 -1.032013446782  0.002939030014
 -1.684533476150 -2.430522251364  2.070179818920 -0.045783378824
 -3.368784049130  0.036613212616 -1.854972615293  0.062123213526
 -3.278396927799  1.580834404538 -0.078603229040  0.034127502718
 -3.656110378332 -0.713812229513  0.361831391088  0.034788986269
 -2.408052717333 -2.075622947728 -1.226189024363  0.039600217307
 -1.295829484895 -0.567704086865 -2.747607986898  0.059655519128
 -1.846557165647  1.681693338816 -2.099716493181  0.049862895531
 -3.831862607217  0.357468890277 -0.654813288033  0.051792349907
 -3.527736967644 -1.045671231370 -1.064852892071  0.053043935786
 -2.622906831544 -1.024738705101 -2.241124222290  0.059720836867
 -2.402781990555  0.378472303440 -2.579763034114  0.063061182986
 -3.126606927219  1.313275381963 -1.541857021673  0.054655087813
 -3.633182667875  0.531520753705  0.561856665829  0.031569056507
 -3.086813000261 -1.794874586082 -0.179700355757  0.032164660479
 -1.615880090830 -1.674393887320 -2.147504235692  0.048959422178
 -1.240258025012  0.765881087348 -2.687977655831  0.058358894457
 -2.430292517958  2.154437082790 -0.969924007583  0.035016168993
  3.368776503197 -0.036639040867 -1.854966842961  0.062114665731
  3.278392620256 -1.580850766330 -0.078589062660  0.034121781811
  3.656106873706  0.713798214804  0.361832640492  0.034783282027
  2.408046317685  2.075600470298 -1.226192756897  0.039592157839
  1.295820311657  0.567673501678 -2.747601655947  0.059647549150
  1.846549173548 -1.681720471299 -2.099699178989  0.049855244832
  3.831857249217 -0.357488322766 -0.654806650060  0.051785339995
  3.527730862121  1.045649613724 -1.064853177123  0.053036259816
  2.622898581638  1.024710819001 -2.241122746235  0.059711880960
  2.402773123306 -0.378501994157 -2.579753678917  0.063052131628
  3.126599952113 -1.313299541573 -1.541844004398  0.054647087156
  3.633179527908 -0.531533702443  0.561864593522  0.031563987962
  3.086808508398  1.794857685487 -0.179703829578  0.032157916727
  1.615872011596  1.674366500124 -2.147504385867  0.048951058875
  1.240248960489 -0.765911354740 -2.687964116774  0.058351506804
  2.430286585511 -2.154458194501 -0.969905238283  0.035009705803
  0.000006852884  1.035694667087 -2.361676372823  0.052324127158
 -0.298806015181  0.969607486526 -2.408809074493  0.053873525320
  0.298815704890  0.969607820141 -2.408807386530  0.053870845225
  0.000007656756 -1.035723195601 -2.361670853435  0.052324715726
 -0.298805262603 -0.969636498141 -2.408803907288  0.053874122858
  0.298816457468 -0.969636367899 -2.408802219325  0.053871294884
 -0.667712940797 -1.245186997899 -2.338574529312  0.051505909611
 -1.218112188165 -2.025138759161 -1.616566947615  0.036578758618
 -2.084175601108 -2.294008802021 -0.480477682822  0.020802048527
 -2.876631896395 -1.658047550445  0.559052047065  0.017301972205
 -3.282909221331 -0.368099862162  1.091996180628  0.019297794099
 -3.142757910518  1.067034798172  0.908143333087  0.018320827018
 -2.511458431963  2.081290260960  0.080011352455  0.017686628386
 -1.638016880752  2.274609499719 -1.065756198713  0.027240804813
 -0.866948462969  1.570740798135 -2.077229430584  0.047324859025
 -0.467915446486 -1.694563900014 -1.941501402531  0.039855718821
 -1.299753513329 -2.453901457341 -0.850306853788  0.012656582461
 -2.242033801688 -2.245340287831  0.385760998463  0.000687829478
 -2.923088754321 -1.151144046328  1.279154738758  0.002448398349
 -3.074287016292  0.397098864814  1.477489335582  0.003997178181
 -2.635990824421  1.788708515315  0.902534843950  0.000662174693
 -1.781079179779  2.474786487342 -0.218927030025  0.004386440156
 -0.846758459860  2.184720175398 -1.444553386477  0.025047995435
 -0.255852300976  1.360631011730 -2.219692209228  0.046790645135
  0.667723897920  1.245157743773 -2.338577856151  0.051500064896
  1.218123388571  2.025110412626 -1.616576841774  0.036571175668
  2.084186643139  2.293984793080 -0.480493513306  0.020795098346
  2.876642520254  1.658030225134  0.559035126479  0.017296773054
  3.282919570196  0.368088739237  1.091983758387  0.019293922688
  3.142768358070 -1.067043033853  0.908139383421  0.018317343461
  2.511469270857 -2.081300494445  0.080016452498  0.017682500289
  1.638028059662 -2.274626132712 -1.065745290088  0.027235486567
  0.866959534092 -1.570765766224 -2.077218589767  0.047319393983
  0.467932162408  1.694535407938 -1.941510815499  0.039851184628
  1.299770347024  2.453875604722 -0.850324284820  0.012650403443
  2.242050270258  2.245320705184  0.385739526123  0.000682781149
  2.923104801922  1.151131828431  1.279135167181  0.002445030957
  3.074302957696 -0.397105856761  1.477477125466  0.003994913814
  2.636007061705 -1.788714946455  0.902532611959  0.000660166042
  1.781095866662 -2.474797662024 -0.218920775640  0.004383674763
  0.846775315928 -2.184739733041 -1.444543821640  0.025044802957
  0.255868904327 -1.360657816862 -2.219684436601  0.046789278145
  0.427045418766 -0.580992742084 -2.585105089006  0.057622419044
  0.427044967836  0.580963354323 -2.585108185092  0.057622194048
 -0.427035838172 -0.000014913339 -2.543076305315  0.062898569735
 end_of_phi


 nsubv after electrostatic potential =  2

  Confirmation of dielectric energy for ground state
  edielnew =  -0.0111934298

 =========== Executing IN-CORE method ==========
 norm multnew:  0.00222408662
 threx: total number of loops= 14
 threx: total number of loops= 24
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331914E+01-0.309165E+01-0.410226E+01-0.407883E+01-0.475210E+01-0.534847E+01-0.433670E+01-0.358739E+01-0.342330E+01-0.317821E+01
 -0.403664E+01-0.263325E+01-0.362338E+01-0.366481E+01-0.366999E+01-0.363955E+01-0.338751E+01-0.303510E+01-0.289433E+01-0.330517E+02
 -0.787312E+01-0.671622E+01-0.695563E+01-0.711691E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         2        65         3         2        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         3       129         4         3        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         4       193         5         4        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         5       257         6         5        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         6       321         7         6        12        64       128         6       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331914E+01-0.309165E+01-0.410226E+01-0.407883E+01-0.475210E+01-0.534847E+01-0.433670E+01-0.358739E+01-0.342330E+01-0.317821E+01
 -0.403664E+01-0.263325E+01-0.362338E+01-0.366481E+01-0.366999E+01-0.363955E+01-0.338751E+01-0.303510E+01-0.289433E+01-0.330517E+02
 -0.787312E+01-0.671622E+01-0.695563E+01-0.711691E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         2        65         3         2        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         3       129         4         3        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         4       193         5         4        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         5       257         6         5        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         6       321         7         6        12        64       128         6       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331914E+01-0.309165E+01-0.410226E+01-0.407883E+01-0.475210E+01-0.534847E+01-0.433670E+01-0.358739E+01-0.342330E+01-0.317821E+01
 -0.403664E+01-0.263325E+01-0.362338E+01-0.366481E+01-0.366999E+01-0.363955E+01-0.338751E+01-0.303510E+01-0.289433E+01-0.330517E+02
 -0.787312E+01-0.671622E+01-0.695563E+01-0.711691E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         2        65         3         2        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         3       129         4         3        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         4       193         5         4        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         5       257         6         5        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         6       321         7         6        12        64       128         6       380       380


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        40 xx:       135 ww:       255
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        20 yw:        25

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        39
task #   9:       127    task #  10:       247    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 matrix-vector product formed for vector   3
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference-subspace vector overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     1.00000000     0.00000000     0.00000000

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.97611527     0.02424344     0.21589637
 subspace has dimension  3

          (h-repnuc*1) matrix in the subspace basis

                x:   1         x:   2         x:   3
   x:   1   -85.40090838
   x:   2    -0.25187978    -6.45464988
   x:   3    -0.00054635     0.22809435    -0.18307051

          overlap matrix in the subspace basis

                x:   1         x:   2         x:   3
   x:   1     1.00000000
   x:   2     0.00000000     0.07914284
   x:   3     0.00000000    -0.00275501     0.00222409

          eigenvectors and eigenvalues in the subspace basis

                v:   1         v:   2         v:   3

   energy   -85.60510889   -82.32256197   -81.26557200

   x:   1     0.97611527     0.02424344     0.21589637
   x:   2     0.78937290    -0.34115210    -3.53062087
   x:   3     0.90813080    20.68180733    -6.42826253

          eigenvectors and eigenvalues in the schmidt orthonormalized basis

                v:   1         v:   2         v:   3

   energy   -85.60510889   -82.32256197   -81.26557200

  zx:   1     0.97611527     0.02424344     0.21589637
  zx:   2     0.21317568    -0.29851187    -0.93029393
  zx:   3     0.04189410     0.95409796    -0.29655011

          <ref|baseci> overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     1.00000000     0.00000000     0.00000000

  write the transformed vectors back to cosmofile

 rtflw =  0
 freezing =  F

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1    -76.2521461042 -9.3471E+00  2.5856E-04  2.9323E-02  1.0000E-03
 mr-sdci #  3  2    -72.9695991839 -8.3891E+00  0.0000E+00  1.5625E+00  1.0000E-04
 mr-sdci #  3  3    -71.9126092208  7.1913E+01  0.0000E+00  2.0310E+00  1.0000E-04


 total energy in cosmo calc 
e(rootcalc) + repnuc=     -76.2521461042
dielectric energy =      -0.0104563943
deltaediel =       0.0104563943
e(rootcalc) + repnuc - ediel =     -76.2416897099
e(rootcalc) + repnuc - edielnew =     -76.2521461042
deltaelast =      76.2416897099

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.02   0.00   0.00   0.01         0.    0.0000
    2   25    0   0.01   0.00   0.00   0.01         0.    0.0000
    3   26    0   0.02   0.00   0.00   0.02         0.    0.0000
    4   11    0   0.02   0.00   0.00   0.01         0.    0.0000
    5   15    0   0.02   0.00   0.00   0.02         0.    0.0000
    6   16    0   0.02   0.00   0.00   0.02         0.    0.0000
    7    1    0   0.01   0.00   0.00   0.01         0.    0.0000
    8    5    0   0.02   0.00   0.00   0.02         0.    0.0000
    9    6    0   0.02   0.00   0.00   0.02         0.    0.0000
   10    7    0   0.04   0.00   0.00   0.03         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.01   0.00   0.00   0.01         0.    0.0000
   14   47    0   0.03   0.00   0.00   0.03         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.2400s 
time spent in multnx:                   0.2100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0200s 
total time per CI iteration:            3.2000s 

          starting ci iteration   4

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.35296278

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=      10  D0X=      30  D0W=      50
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================

          ci-one-electron density block   1

                mo   1         mo   2         mo   3         mo   4         mo   5         mo   6         mo   7         mo   8
   mo   1     1.99991240
   mo   2    -0.00020385     1.98567026
   mo   3     0.00000000     0.00000005     1.97054575
   mo   4    -0.00016277     0.00373650    -0.00000001     1.97384683
   mo   5     0.00000000    -0.00000005     0.00000001    -0.00000005     1.97619361
   mo   6     0.00014902    -0.00156516    -0.00001512    -0.01494650    -0.00000634     0.00620062
   mo   7     0.00000000     0.00000667    -0.00728407    -0.00001774    -0.00000406    -0.00000009     0.00691368
   mo   8    -0.00000001     0.00001378    -0.01264637    -0.00001714     0.00000203    -0.00000016     0.00664089     0.01025667
   mo   9     0.00008438     0.00429553    -0.00000965    -0.00140857     0.00000162     0.00652291    -0.00000018    -0.00000024
   mo  10    -0.00020515     0.00943593    -0.00001253     0.00536406     0.00000020     0.00238959    -0.00000019    -0.00000012
   mo  11     0.00000003    -0.00000159     0.00000281    -0.00000045    -0.01259476     0.00000019     0.00000009    -0.00000009
   mo  12    -0.00000001    -0.00000270    -0.00427801    -0.00000536    -0.00000224     0.00000002     0.00577776     0.00369752
   mo  13     0.00019326    -0.00080368    -0.00001128    -0.00781827    -0.00000275     0.00177662    -0.00000002    -0.00000011
   mo  14     0.00000000     0.00000197    -0.00000285    -0.00000106    -0.00000081    -0.00000002    -0.00000003    -0.00000001
   mo  15     0.00000000     0.00000100    -0.00000176    -0.00000436    -0.00335489     0.00000001    -0.00000001     0.00000000
   mo  16    -0.00035854     0.00045216    -0.00000429     0.00268017     0.00000152    -0.00185587    -0.00000010    -0.00000004
   mo  17    -0.00000001    -0.00000793     0.00077849     0.00001871     0.00000045     0.00000000     0.00024422     0.00021997
   mo  18     0.00000003    -0.00000053     0.01080480    -0.00000046     0.00000083    -0.00000008    -0.00273430    -0.00435475
   mo  19    -0.00010360    -0.00345177    -0.00000002     0.00763511     0.00000038    -0.00292853    -0.00000011    -0.00000007
   mo  20     0.00000000     0.00000019    -0.00000045     0.00000131     0.00220553    -0.00000002     0.00000000     0.00000000
   mo  21     0.00000000    -0.00000022     0.00000058    -0.00000055    -0.00000128     0.00000001    -0.00000001    -0.00000001
   mo  22     0.00000875     0.00010888     0.00000223    -0.00149446     0.00000027    -0.00004108     0.00000004     0.00000003
   mo  23     0.00004219     0.00139499    -0.00000051    -0.00061808    -0.00000046     0.00056245     0.00000001     0.00000000
   mo  24    -0.00000001    -0.00000061     0.00005231     0.00000172     0.00000009     0.00000000    -0.00076155     0.00046518

                mo   9         mo  10         mo  11         mo  12         mo  13         mo  14         mo  15         mo  16
   mo   9     0.00965148
   mo  10     0.00308848     0.00998586
   mo  11    -0.00000007    -0.00000001     0.01431541
   mo  12     0.00000001     0.00000003     0.00000002     0.00643569
   mo  13     0.00036120    -0.00063065     0.00000005    -0.00000001     0.00380306
   mo  14    -0.00000002     0.00000000     0.00000008    -0.00000002     0.00000000     0.00308815
   mo  15    -0.00000002    -0.00000006    -0.00132168    -0.00000001     0.00000002    -0.00000005     0.00231116
   mo  16     0.00012559     0.00255042    -0.00000003    -0.00000006    -0.00117687     0.00000001    -0.00000002     0.00438874
   mo  17     0.00000013     0.00000026    -0.00000001     0.00034071    -0.00000003     0.00000000     0.00000000     0.00000006
   mo  18    -0.00000011    -0.00000015     0.00000001    -0.00175586     0.00000001     0.00000000     0.00000000    -0.00000003
   mo  19    -0.00429276    -0.00078396     0.00000000    -0.00000011    -0.00078802     0.00000000    -0.00000001    -0.00014632
   mo  20    -0.00000001     0.00000000     0.00006863     0.00000000    -0.00000001     0.00000000    -0.00191661     0.00000000
   mo  21     0.00000000    -0.00000001     0.00000002     0.00000000     0.00000000    -0.00212519     0.00000000    -0.00000001
   mo  22    -0.00055313     0.00034260    -0.00000001     0.00000003     0.00180555     0.00000000     0.00000000     0.00074882
   mo  23     0.00013782     0.00182096     0.00000000     0.00000003     0.00023520     0.00000000     0.00000000    -0.00069930
   mo  24     0.00000001     0.00000000     0.00000001    -0.00169059     0.00000000     0.00000000     0.00000000     0.00000002

                mo  17         mo  18         mo  19         mo  20         mo  21         mo  22         mo  23         mo  24
   mo  17     0.00052647
   mo  18    -0.00019952     0.00264786
   mo  19     0.00000002     0.00000005     0.00290202
   mo  20     0.00000000     0.00000000     0.00000001     0.00266994
   mo  21     0.00000000     0.00000001     0.00000000     0.00000001     0.00252885
   mo  22     0.00000000     0.00000000    -0.00018096     0.00000000     0.00000000     0.00205361
   mo  23     0.00000002    -0.00000002     0.00066474     0.00000000     0.00000000    -0.00009247     0.00167999
   mo  24     0.00000944    -0.00061459     0.00000002     0.00000000     0.00000000     0.00000000    -0.00000001     0.00147189


*****   symmetry block SYM1   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       1.99991240
       2      -0.00020385     1.98567026
       3       0.00000000     0.00000005     1.97054575
       4      -0.00016277     0.00373650    -0.00000001     1.97384683
       5       0.00000000    -0.00000005     0.00000001    -0.00000005
       6       0.00014902    -0.00156516    -0.00001512    -0.01494650
       7       0.00000000     0.00000667    -0.00728407    -0.00001774
       8      -0.00000001     0.00001378    -0.01264637    -0.00001714
       9       0.00008438     0.00429553    -0.00000965    -0.00140857
      10      -0.00020515     0.00943593    -0.00001253     0.00536406
      11       0.00000003    -0.00000159     0.00000281    -0.00000045
      12      -0.00000001    -0.00000270    -0.00427801    -0.00000536
      13       0.00019326    -0.00080368    -0.00001128    -0.00781827
      14       0.00000000     0.00000197    -0.00000285    -0.00000106
      15       0.00000000     0.00000100    -0.00000176    -0.00000436
      16      -0.00035854     0.00045216    -0.00000429     0.00268017
      17      -0.00000001    -0.00000793     0.00077849     0.00001871
      18       0.00000003    -0.00000053     0.01080480    -0.00000046
      19      -0.00010360    -0.00345177    -0.00000002     0.00763511
      20       0.00000000     0.00000019    -0.00000045     0.00000131
      21       0.00000000    -0.00000022     0.00000058    -0.00000055
      22       0.00000875     0.00010888     0.00000223    -0.00149446
      23       0.00004219     0.00139499    -0.00000051    -0.00061808
      24      -0.00000001    -0.00000061     0.00005231     0.00000172

               Column   5     Column   6     Column   7     Column   8
       5       1.97619361
       6      -0.00000634     0.00620062
       7      -0.00000406    -0.00000009     0.00691368
       8       0.00000203    -0.00000016     0.00664089     0.01025667
       9       0.00000162     0.00652291    -0.00000018    -0.00000024
      10       0.00000020     0.00238959    -0.00000019    -0.00000012
      11      -0.01259476     0.00000019     0.00000009    -0.00000009
      12      -0.00000224     0.00000002     0.00577776     0.00369752
      13      -0.00000275     0.00177662    -0.00000002    -0.00000011
      14      -0.00000081    -0.00000002    -0.00000003    -0.00000001
      15      -0.00335489     0.00000001    -0.00000001     0.00000000
      16       0.00000152    -0.00185587    -0.00000010    -0.00000004
      17       0.00000045     0.00000000     0.00024422     0.00021997
      18       0.00000083    -0.00000008    -0.00273430    -0.00435475
      19       0.00000038    -0.00292853    -0.00000011    -0.00000007
      20       0.00220553    -0.00000002     0.00000000     0.00000000
      21      -0.00000128     0.00000001    -0.00000001    -0.00000001
      22       0.00000027    -0.00004108     0.00000004     0.00000003
      23      -0.00000046     0.00056245     0.00000001     0.00000000
      24       0.00000009     0.00000000    -0.00076155     0.00046518

               Column   9     Column  10     Column  11     Column  12
       9       0.00965148
      10       0.00308848     0.00998586
      11      -0.00000007    -0.00000001     0.01431541
      12       0.00000001     0.00000003     0.00000002     0.00643569
      13       0.00036120    -0.00063065     0.00000005    -0.00000001
      14      -0.00000002     0.00000000     0.00000008    -0.00000002
      15      -0.00000002    -0.00000006    -0.00132168    -0.00000001
      16       0.00012559     0.00255042    -0.00000003    -0.00000006
      17       0.00000013     0.00000026    -0.00000001     0.00034071
      18      -0.00000011    -0.00000015     0.00000001    -0.00175586
      19      -0.00429276    -0.00078396     0.00000000    -0.00000011
      20      -0.00000001     0.00000000     0.00006863     0.00000000
      21       0.00000000    -0.00000001     0.00000002     0.00000000
      22      -0.00055313     0.00034260    -0.00000001     0.00000003
      23       0.00013782     0.00182096     0.00000000     0.00000003
      24       0.00000001     0.00000000     0.00000001    -0.00169059

               Column  13     Column  14     Column  15     Column  16
      13       0.00380306
      14       0.00000000     0.00308815
      15       0.00000002    -0.00000005     0.00231116
      16      -0.00117687     0.00000001    -0.00000002     0.00438874
      17      -0.00000003     0.00000000     0.00000000     0.00000006
      18       0.00000001     0.00000000     0.00000000    -0.00000003
      19      -0.00078802     0.00000000    -0.00000001    -0.00014632
      20      -0.00000001     0.00000000    -0.00191661     0.00000000
      21       0.00000000    -0.00212519     0.00000000    -0.00000001
      22       0.00180555     0.00000000     0.00000000     0.00074882
      23       0.00023520     0.00000000     0.00000000    -0.00069930
      24       0.00000000     0.00000000     0.00000000     0.00000002

               Column  17     Column  18     Column  19     Column  20
      17       0.00052647
      18      -0.00019952     0.00264786
      19       0.00000002     0.00000005     0.00290202
      20       0.00000000     0.00000000     0.00000001     0.00266994
      21       0.00000000     0.00000001     0.00000000     0.00000001
      22       0.00000000     0.00000000    -0.00018096     0.00000000
      23       0.00000002    -0.00000002     0.00066474     0.00000000
      24       0.00000944    -0.00061459     0.00000002     0.00000000

               Column  21     Column  22     Column  23     Column  24
      21       0.00252885
      22       0.00000000     0.00205361
      23       0.00000000    -0.00009247     0.00167999
      24       0.00000000     0.00000000    -0.00000001     0.00147189

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7        eno   8
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    8   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno   9        eno  10        eno  11        eno  12        eno  13        eno  14        eno  15        eno  16
 emo    9   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   10   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   11   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   12   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   13   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   14   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   15   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   16   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno  17        eno  18        eno  19        eno  20        eno  21        eno  22        eno  23        eno  24
 emo   17   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   18   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   19   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   20   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   21   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   22   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   23   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   24   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   1.9999174      1.9868339      1.9762826      1.9729365      1.9707243
   0.0203498      0.0185355      0.0143915      0.0101209      0.0056409
   0.0053466      0.0049520      0.0044103      0.0043161      0.0010219
   0.0010197      0.0006650      0.0005700      0.0004999      0.0004970
   0.0004580      0.0004304      0.0000444      0.0000353

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7         no   8
  mo    1   0.999820E+00   0.185956E-01  -0.460949E-07   0.377212E-02  -0.630541E-06   0.228285E-07  -0.461565E-04  -0.192157E-07
  mo    2  -0.167778E-01   0.959287E+00  -0.987080E-06  -0.281879E+00   0.456745E-04  -0.474595E-05  -0.364221E-02   0.948489E-06
  mo    3   0.989904E-07  -0.248816E-05   0.101166E-05   0.153228E-03   0.999954E+00   0.876307E-02   0.131209E-04  -0.156120E-05
  mo    4  -0.885802E-02   0.281744E+00   0.106124E-04   0.959397E+00  -0.146475E-03   0.101720E-04   0.475363E-02   0.275162E-07
  mo    5   0.124656E-06  -0.206210E-05   0.999977E+00  -0.104938E-04  -0.100818E-05   0.113121E-05   0.130286E-05   0.615100E-02
  mo    6   0.154494E-03  -0.287101E-02  -0.330003E-05  -0.708032E-02  -0.664114E-05  -0.162126E-03   0.516534E+00   0.571049E-05
  mo    7   0.214953E-07   0.725938E-06  -0.206510E-05  -0.102258E-04  -0.374501E-02   0.561005E+00   0.163144E-03   0.435861E-05
  mo    8  -0.438483E-07   0.426182E-05   0.101500E-05  -0.113833E-04  -0.647943E-02   0.639950E+00   0.177633E-03  -0.781999E-05
  mo    9   0.127653E-04   0.188417E-02   0.803326E-06  -0.133611E-02  -0.474674E-05  -0.199472E-03   0.673866E+00  -0.132165E-04
  mo   10  -0.206567E-03   0.534294E-02   0.126856E-06   0.125612E-02  -0.658559E-05  -0.125836E-03   0.423636E+00  -0.102226E-04
  mo   11   0.317552E-07  -0.823389E-06  -0.641813E-02   0.763529E-07   0.144141E-05   0.226201E-05   0.107589E-04   0.993080E+00
  mo   12   0.424416E-07  -0.205699E-05  -0.114314E-05  -0.261146E-05  -0.220588E-02   0.440960E+00   0.147999E-03   0.115753E-05
  mo   13   0.138603E-03  -0.150190E-02  -0.143886E-05  -0.370400E-02  -0.517142E-05  -0.362484E-04   0.741385E-01   0.442600E-05
  mo   14  -0.120477E-07   0.803676E-06  -0.410020E-06  -0.798836E-06  -0.144706E-05  -0.320538E-05  -0.240875E-05   0.742699E-05
  mo   15   0.990535E-08  -0.130397E-06  -0.169632E-02  -0.224903E-05  -0.894436E-06  -0.177107E-05  -0.454585E-05  -0.114434E+00
  mo   16  -0.195823E-03   0.606685E-03   0.791154E-06   0.125079E-02  -0.236992E-05  -0.713359E-05   0.930645E-02  -0.589668E-05
  mo   17  -0.211555E-07  -0.117361E-05   0.225591E-06   0.102920E-04   0.392987E-03   0.247512E-01   0.259442E-04  -0.555868E-06
  mo   18   0.205984E-07  -0.342346E-06   0.425573E-06   0.735058E-06   0.551124E-02  -0.281465E+00  -0.961313E-04   0.140102E-05
  mo   19  -0.569434E-04  -0.586902E-03   0.240078E-06   0.422618E-02  -0.633742E-06   0.786650E-04  -0.301273E+00   0.378418E-05
  mo   20  -0.640921E-08   0.276934E-06   0.111891E-02   0.600665E-06  -0.227999E-06  -0.444438E-07  -0.397885E-07   0.256829E-01
  mo   21   0.526740E-08  -0.187520E-06  -0.647933E-06  -0.235567E-06   0.294227E-06  -0.238263E-06   0.962237E-07  -0.256854E-06
  mo   22   0.101067E-04  -0.160088E-03   0.125690E-06  -0.745578E-03   0.124299E-05   0.253194E-05  -0.401796E-02  -0.115533E-06
  mo   23   0.120591E-04   0.590413E-03  -0.237581E-06  -0.500589E-03  -0.191621E-06  -0.158337E-04   0.568263E-01  -0.952738E-06
  mo   24  -0.713150E-08  -0.504320E-07   0.475709E-07   0.930709E-06   0.266574E-04  -0.371516E-01  -0.131996E-04  -0.337877E-06

                no   9         no  10         no  11         no  12         no  13         no  14         no  15         no  16
  mo    1   0.217757E-03  -0.245505E-07  -0.934248E-04  -0.112293E-08   0.435254E-04   0.573655E-09   0.156604E-04   0.102565E-07
  mo    2  -0.342973E-02   0.391451E-05  -0.126008E-03  -0.821287E-06   0.267464E-03   0.209348E-06   0.619242E-03  -0.358962E-05
  mo    3   0.148872E-05  -0.277943E-02   0.496425E-05   0.126985E-05   0.135874E-05  -0.289993E-06   0.199230E-05  -0.111106E-02
  mo    4  -0.640001E-02  -0.107145E-06   0.306085E-02   0.317347E-06   0.173948E-02  -0.191811E-05  -0.139669E-02   0.403854E-05
  mo    5  -0.142669E-05   0.160571E-05   0.197570E-05  -0.228042E-06  -0.306534E-06  -0.255513E-02  -0.763392E-06  -0.808484E-06
  mo    6  -0.263868E+00   0.336350E-04   0.195231E+00   0.235714E-05  -0.132251E+00  -0.364498E-05  -0.125240E+00  -0.203404E-03
  mo    7  -0.949066E-05   0.220156E+00  -0.495651E-04   0.283715E-05   0.410875E-05  -0.444006E-06  -0.610238E-04   0.430233E-01
  mo    8   0.374753E-05  -0.540736E+00   0.134123E-03  -0.431905E-05   0.312267E-05  -0.751277E-06  -0.641270E-03   0.404308E+00
  mo    9  -0.194945E+00  -0.956721E-04  -0.349897E+00  -0.269277E-05   0.471258E-01   0.107247E-05   0.401844E+00   0.642579E-03
  mo   10   0.765167E+00   0.730356E-04   0.302214E+00   0.165866E-05  -0.150233E+00  -0.728274E-05  -0.285688E+00  -0.434611E-03
  mo   11   0.941466E-05  -0.635023E-05  -0.803274E-05  -0.864838E-05   0.421214E-05  -0.945959E-01   0.351210E-05   0.207881E-05
  mo   12  -0.260065E-05   0.646956E+00  -0.148708E-03   0.877322E-05  -0.107891E-05   0.977725E-07   0.447136E-03  -0.282475E+00
  mo   13  -0.245330E+00   0.151780E-03   0.623546E+00   0.174234E-04   0.433322E+00   0.928484E-05   0.577868E-01   0.734174E-04
  mo   14   0.256413E-05  -0.868812E-05  -0.127055E-04   0.751819E+00  -0.833661E-05  -0.156449E-04   0.540940E-06  -0.386920E-05
  mo   15  -0.556458E-05   0.116612E-05  -0.468207E-06  -0.212733E-04   0.145879E-04  -0.652274E+00  -0.847496E-07  -0.769339E-06
  mo   16   0.454910E+00  -0.830721E-04  -0.365399E+00  -0.370675E-06   0.571565E+00   0.106772E-04   0.282864E+00   0.427621E-03
  mo   17   0.130506E-04   0.183504E-01   0.928705E-05   0.190331E-05   0.124607E-05   0.315362E-06   0.376222E-03  -0.254328E+00
  mo   18  -0.633701E-05   0.277759E+00  -0.757577E-04   0.248444E-05  -0.152081E-05   0.917713E-06  -0.978567E-03   0.613941E+00
  mo   19   0.161853E+00   0.424397E-04   0.171258E+00  -0.143525E-05  -0.275295E+00  -0.701293E-05   0.430719E+00   0.641188E-03
  mo   20   0.173161E-05  -0.112024E-05   0.905267E-06   0.156631E-04  -0.163422E-04   0.752053E+00   0.190785E-05  -0.127091E-05
  mo   21  -0.162039E-05   0.755993E-05   0.108167E-04  -0.659369E+00   0.897745E-05   0.223128E-04   0.199377E-05  -0.452372E-05
  mo   22   0.307472E-01   0.778844E-04   0.327381E+00   0.109155E-04   0.515555E+00   0.109216E-04   0.132790E+00   0.249063E-03
  mo   23   0.112093E+00   0.722884E-04   0.298798E+00   0.152253E-05  -0.318868E+00  -0.906995E-05   0.674322E+00   0.109282E-02
  mo   24   0.164498E-05  -0.403842E+00   0.101389E-03  -0.533672E-05   0.469153E-05  -0.924358E-06   0.883545E-03  -0.559716E+00

                no  17         no  18         no  19         no  20         no  21         no  22         no  23         no  24
  mo    1  -0.125311E-08  -0.152807E-07  -0.917964E-08   0.274364E-04   0.154840E-07   0.329430E-04   0.207800E-04   0.491421E-08
  mo    2  -0.597709E-06  -0.288347E-05  -0.851365E-06   0.146065E-02   0.324620E-05   0.148084E-02   0.145365E-02  -0.281055E-07
  mo    3   0.779722E-06  -0.127570E-02   0.759489E-06   0.176730E-06  -0.170060E-02   0.593224E-05   0.681469E-06   0.106414E-02
  mo    4   0.568393E-06   0.649242E-05   0.120132E-05   0.125518E-03  -0.598987E-05   0.871400E-03   0.453607E-02   0.856861E-06
  mo    5   0.643341E-06   0.106917E-05   0.978942E-03   0.859193E-06  -0.103651E-06   0.112117E-05   0.181509E-05   0.969855E-06
  mo    6  -0.501738E-05  -0.133410E-03  -0.125830E-03   0.424493E+00   0.187190E-03   0.280617E+00   0.577302E+00   0.915285E-04
  mo    7  -0.158649E-04   0.553255E+00  -0.377223E-05   0.600845E-04   0.217753E+00   0.497152E-04  -0.813378E-04   0.530502E+00
  mo    8   0.101824E-04  -0.184132E+00   0.722428E-06  -0.520599E-04   0.493460E-01  -0.108982E-04   0.472171E-04  -0.313403E+00
  mo    9   0.877090E-06   0.449960E-05  -0.190480E-05   0.488812E-02   0.693963E-05  -0.180824E-01  -0.470549E+00  -0.776275E-04
  mo   10   0.191406E-05   0.326328E-04   0.153963E-04  -0.416143E-01  -0.307652E-04  -0.621088E-02  -0.194242E+00  -0.279656E-04
  mo   11  -0.112610E-04  -0.411375E-05   0.693031E-01   0.171284E-04   0.661346E-06  -0.336846E-05  -0.719100E-05  -0.367348E-05
  mo   12   0.387100E-05  -0.130707E+00   0.250257E-05   0.139198E-04  -0.127124E+00  -0.172199E-05   0.845861E-04  -0.523412E+00
  mo   13   0.543305E-05   0.165029E-04   0.112642E-03  -0.396479E+00  -0.383204E-06   0.404701E+00  -0.182772E+00  -0.344509E-04
  mo   14   0.659369E+00   0.218007E-04   0.731404E-04   0.973716E-05  -0.258337E-05  -0.551793E-06   0.161473E-05   0.132077E-05
  mo   15  -0.730509E-04   0.196836E-05   0.749293E+00   0.222359E-03   0.118696E-04   0.726624E-05  -0.174034E-05   0.208720E-06
  mo   16  -0.152317E-05  -0.512602E-04   0.825170E-05  -0.336967E-01   0.851674E-04   0.324450E+00   0.382556E+00   0.571592E-04
  mo   17   0.140533E-04  -0.427020E+00  -0.125195E-04  -0.330744E-03   0.863708E+00  -0.328462E-03  -0.102833E-04   0.776503E-01
  mo   18  -0.663592E-05   0.366303E+00  -0.664342E-05  -0.267325E-04   0.401274E+00  -0.156124E-04   0.646870E-04  -0.414094E+00
  mo   19  -0.702069E-05  -0.199070E-03  -0.158225E-03   0.512710E+00   0.288317E-03   0.489947E+00  -0.299106E+00  -0.500791E-04
  mo   20  -0.733622E-04   0.178676E-05   0.658601E+00   0.195054E-03   0.992158E-05   0.653302E-05  -0.135326E-05  -0.241333E-06
  mo   21   0.751819E+00   0.222265E-04   0.739569E-04   0.907331E-05  -0.135152E-05  -0.120238E-06   0.633346E-06   0.869077E-06
  mo   22  -0.809079E-05  -0.205704E-04  -0.154625E-03   0.538377E+00  -0.813617E-05  -0.560098E+00  -0.697780E-01  -0.594377E-05
  mo   23   0.287213E-05   0.120560E-03   0.100565E-03  -0.327103E+00  -0.164490E-03  -0.313173E+00   0.365327E+00   0.547865E-04
  mo   24  -0.201232E-04   0.571310E+00  -0.490246E-05   0.751089E-04   0.164233E+00   0.125509E-04   0.695206E-04  -0.410951E+00
  eigenvectors of nos in ao-basis, column    1
  0.1000597E+01 -0.1377608E-01 -0.1677707E-02  0.5446889E-07  0.1585911E-06 -0.7537793E-02  0.9331509E-07 -0.4836128E-07
  0.1752971E-03  0.7201270E-08 -0.1070568E-07  0.4279903E-04  0.1385003E-07  0.9082003E-04 -0.3218024E-02  0.2151880E-02
 -0.1013167E-02  0.2876638E-08 -0.9433131E-03 -0.3218132E-02  0.2151987E-02  0.1013182E-02  0.1539638E-07 -0.9433534E-03
  eigenvectors of nos in ao-basis, column    2
  0.9141524E-02  0.9120534E+00 -0.7426295E-01 -0.2038846E-05 -0.3044622E-05  0.1048789E+00 -0.4858383E-05  0.1227289E-05
  0.7679839E-01 -0.3046803E-06  0.3517769E-06 -0.1079831E-02 -0.2461400E-06  0.1023558E-02  0.2269877E+00 -0.1117059E+00
  0.2837661E-01  0.4225626E-06  0.2200709E-01  0.2270003E+00 -0.1117127E+00 -0.2837881E-01 -0.5374965E-06  0.2200519E-01
  eigenvectors of nos in ao-basis, column    3
 -0.7412313E-06 -0.2086906E-05  0.3287214E-05  0.2950292E-05  0.9098944E+00  0.8943718E-05 -0.2363853E-05  0.8351281E-01
  0.1607121E-05 -0.6419075E-06 -0.1683038E-01 -0.8315053E-07 -0.1406340E-06 -0.2332371E-06 -0.6943272E-05  0.8512362E-05
  0.6657671E-06  0.3065315E-01 -0.3098153E-06 -0.1515096E-05  0.2376926E-06 -0.2650739E-06  0.3065421E-01 -0.4285319E-06
  eigenvectors of nos in ao-basis, column    4
  0.1595457E-02  0.2266817E-01  0.2151709E+00  0.1227975E-03 -0.9379705E-05  0.8035604E+00 -0.2389863E-04  0.2594077E-06
 -0.1669900E-01 -0.1496113E-06  0.1179297E-05 -0.4708464E-02 -0.4430866E-05 -0.2576688E-02 -0.4215681E+00  0.1772567E+00
 -0.3742463E-01 -0.2458291E-05  0.3238790E-02 -0.4214198E+00  0.1771993E+00  0.3742666E-01 -0.1182383E-05  0.3265498E-02
  eigenvectors of nos in ao-basis, column    5
  0.2743101E-06 -0.2354478E-05 -0.3854801E-04  0.7291150E+00  0.1163571E-05 -0.1118993E-03 -0.1170455E+00 -0.1896146E-05
 -0.1067345E-05  0.5020426E-06 -0.8332198E-07  0.6204682E-06 -0.2570978E-01  0.6497801E-06 -0.5515772E+00  0.1910017E+00
 -0.2042276E-01 -0.1687112E-05 -0.3120149E-01  0.5517251E+00 -0.1910599E+00 -0.2043685E-01  0.7461069E-07  0.3119023E-01
  eigenvectors of nos in ao-basis, column    6
 -0.2790100E-04 -0.2283635E-03  0.2847522E-04 -0.1315283E+01  0.4345970E-05  0.3667938E-03  0.5932181E+00 -0.2662695E-05
 -0.2150131E-03  0.1577400E-06  0.2059944E-06  0.1502960E-05 -0.7393683E-01  0.9886935E-05 -0.1005332E+01  0.3715844E+00
 -0.4456925E-01 -0.3625915E-05 -0.2028679E-01  0.1005905E+01 -0.3718260E+00 -0.4459658E-01  0.9548743E-06  0.2026633E-01
  eigenvectors of nos in ao-basis, column    7
  0.1073831E+00  0.8109913E+00 -0.1284846E+00 -0.4118411E-03  0.1666617E-04 -0.1237915E+01  0.2052657E-03 -0.1532121E-04
  0.7905873E+00  0.4137045E-06  0.6140192E-06 -0.5203778E-02 -0.2126838E-04 -0.3465679E-01 -0.9742536E+00  0.4243992E+00
 -0.5425800E-01 -0.5037327E-05 -0.1500630E-01 -0.9736601E+00  0.4241726E+00  0.5423662E-01 -0.1804824E-05 -0.1495419E-01
  eigenvectors of nos in ao-basis, column    8
  0.2975816E-05  0.1470629E-04 -0.1569697E-04  0.4175943E-06  0.1413293E+01  0.2204351E-04  0.2746060E-05 -0.1613460E+01
 -0.2141882E-04 -0.1232877E-05  0.4068637E-01  0.1680327E-06  0.1638923E-05  0.9075159E-06  0.3336377E-04 -0.3578781E-04
 -0.5711253E-06 -0.7214823E-01  0.1475245E-05  0.4235904E-05 -0.1115090E-05  0.1028083E-05 -0.7215823E-01  0.1564920E-05
  eigenvectors of nos in ao-basis, column    9
 -0.4353899E+00 -0.2238689E+01  0.2108751E+01 -0.2892217E-06  0.1234431E-04 -0.7227758E+00 -0.1073942E-04 -0.1321823E-04
  0.1086028E+01 -0.2068068E-05  0.2685063E-05 -0.1428243E-01 -0.4065522E-05  0.7277554E-01  0.4053668E+00 -0.2249014E+00
 -0.1645656E-01 -0.2437605E-05  0.7577513E-01  0.4054015E+00 -0.2249413E+00  0.1646131E-01 -0.4721590E-05  0.7579824E-01
  eigenvectors of nos in ao-basis, column   10
  0.6876106E-04  0.3058207E-03 -0.4241591E-03 -0.5119403E+00 -0.7561731E-05 -0.9615734E-06  0.1059031E+01  0.1048604E-04
  0.4114589E-04  0.9226395E-05 -0.1359416E-05 -0.2451864E-04  0.6121646E+00  0.6244066E-04  0.6555129E+00 -0.4915431E+00
  0.1105817E+00 -0.2621326E-05 -0.2193381E-01 -0.6551972E+00  0.4913677E+00  0.1106260E+00  0.3540102E-05  0.2204400E-01
  eigenvectors of nos in ao-basis, column   11
  0.3008738E+00  0.1353354E+01 -0.1831558E+01  0.1080879E-03 -0.9469374E-05 -0.3643779E-01 -0.2479107E-03  0.1366665E-04
  0.1949608E+00  0.1323644E-04  0.1020503E-05 -0.1021812E+00 -0.1554613E-03  0.2569502E+00  0.6134013E+00 -0.3581867E+00
 -0.7097686E-01 -0.4570836E-05  0.2222160E+00  0.6137375E+00 -0.3584340E+00  0.7093361E-01  0.4621819E-05  0.2222268E+00
  eigenvectors of nos in ao-basis, column   12
  0.4829619E-05  0.2300417E-04 -0.2483071E-04 -0.8589509E-05 -0.1086360E-04  0.5970279E-06  0.1247024E-04  0.2215852E-04
 -0.6300166E-05 -0.8039498E+00  0.1982137E-04 -0.3996536E-05  0.7497753E-05  0.1646995E-05 -0.5577851E-06  0.3815743E-06
 -0.2273312E-05  0.2646039E+00  0.3352318E-05 -0.4056907E-05  0.3428016E-05  0.5589133E-05 -0.2646241E+00  0.6667343E-05
  eigenvectors of nos in ao-basis, column   13
 -0.1422609E+00 -0.6085668E+00  0.9704946E+00  0.7650473E-06  0.4561532E-05  0.1063007E+00 -0.2168912E-05 -0.1052337E-04
 -0.5241213E+00  0.1070106E-04 -0.1943377E-04 -0.2082651E+00 -0.6744560E-05 -0.2174521E+00 -0.4812962E+00  0.2740427E+00
 -0.1631980E+00  0.2315542E-05  0.8884193E-01 -0.4812976E+00  0.2740518E+00  0.1632024E+00  0.6914068E-05  0.8884042E-01
  eigenvectors of nos in ao-basis, column   14
 -0.1280629E-05 -0.5583303E-05  0.9276512E-05  0.6423812E-06 -0.8661214E-01  0.5942673E-05 -0.7418812E-06  0.3044839E+00
 -0.1534421E-04  0.2594242E-04  0.8911483E+00 -0.4423083E-05  0.1432521E-05 -0.6033681E-05 -0.7385329E-05  0.5841593E-05
 -0.2432488E-05 -0.1976695E+00  0.1225084E-05 -0.7309295E-05  0.5280361E-05  0.2901755E-05 -0.1976650E+00  0.1697068E-05
  eigenvectors of nos in ao-basis, column   15
 -0.3586060E+00 -0.1357241E+01  0.2716250E+01 -0.7735107E-03  0.4268547E-05  0.1248771E+00  0.2032874E-02 -0.6539797E-05
 -0.1159322E+01  0.2068146E-05  0.2007204E-05 -0.2883526E-01 -0.1280588E-02  0.4146331E+00 -0.2253458E+01  0.1261211E+01
 -0.7389268E-01  0.1836752E-05 -0.1572535E+00 -0.2257943E+01  0.1263570E+01  0.7410246E-01 -0.4253763E-06 -0.1564614E+00
  eigenvectors of nos in ao-basis, column   16
 -0.5435323E-03 -0.2040869E-02  0.4231022E-02  0.4805156E+00  0.2229586E-05  0.1294981E-03 -0.1270263E+01 -0.2693654E-05
 -0.1752211E-02 -0.4351892E-05 -0.1200352E-05 -0.5680057E-04  0.8111166E+00  0.6699761E-03 -0.1413283E+01  0.7362728E+00
 -0.7864727E-01 -0.5503320E-05  0.2580165E+00  0.1406097E+01 -0.7322455E+00 -0.7838251E-01  0.3255041E-05 -0.2586188E+00
  eigenvectors of nos in ao-basis, column   17
  0.3664404E-05  0.1713799E-04 -0.7313431E-05 -0.2374572E-04 -0.1092701E-04 -0.8546397E-05 -0.1382387E-04  0.8617861E-04
 -0.2596459E-05  0.7211571E+00 -0.6500232E-04  0.2364433E-05  0.2363854E-04  0.3494509E-06 -0.8246300E-04  0.6979903E-04
 -0.1892717E-04  0.7389160E+00 -0.1980246E-04  0.4869075E-04 -0.4353332E-04  0.2462862E-05 -0.7390883E+00  0.2163269E-04
  eigenvectors of nos in ao-basis, column   18
  0.9255751E-04  0.4225791E-03 -0.1265598E-03  0.8508198E+00 -0.4934888E-05 -0.2552780E-03  0.2133393E+00  0.5262084E-05
  0.8856482E-04  0.2102255E-04  0.1552279E-05  0.6950827E-05 -0.6531795E+00  0.4856629E-04  0.1779212E+01 -0.1606939E+01
  0.3484998E+00  0.2568637E-04  0.7181872E+00 -0.1779892E+01  0.1607453E+01  0.3489229E+00 -0.2117545E-04 -0.7184730E+00
  eigenvectors of nos in ao-basis, column   19
  0.4760884E-04  0.2448615E-03  0.1325844E-03 -0.1295163E-04  0.5323743E-01 -0.1896904E-03  0.1747245E-04 -0.7752152E+00
 -0.1467114E-03  0.6987354E-04  0.5686249E+00  0.4444525E-04  0.5611455E-05  0.2957973E-04 -0.4903631E-03  0.3284796E-03
 -0.2873219E-03  0.8460410E+00 -0.1453187E-04 -0.4816425E-03  0.3128733E-03  0.2612602E-03  0.8458842E+00 -0.2075410E-04
  eigenvectors of nos in ao-basis, column   20
 -0.1534824E+00 -0.7940048E+00 -0.4474326E+00 -0.3362018E-05  0.1147251E-04  0.6037809E+00  0.3990365E-03 -0.2240904E-03
  0.5388911E+00  0.8473856E-05  0.1683391E-03 -0.1545930E+00 -0.8722688E-04 -0.9350545E-01  0.1633224E+01 -0.1084634E+01
  0.9156704E+00  0.2610264E-03  0.3009933E-01  0.1632334E+01 -0.1084055E+01 -0.9159375E+00  0.2407467E-03  0.2961268E-01
  eigenvectors of nos in ao-basis, column   21
 -0.1402921E-03 -0.6321420E-03  0.2398007E-03  0.6624272E+00  0.1148911E-06  0.3432133E-03 -0.1239271E+01 -0.1138807E-04
 -0.1496842E-03 -0.1116169E-05  0.8476972E-05  0.9500785E-06 -0.1799500E+00 -0.6829921E-04 -0.2214308E+00 -0.1843476E+00
  0.7711477E+00  0.1089902E-04 -0.3085917E+00  0.2222651E+00  0.1837010E+00  0.7705876E+00  0.1547469E-04  0.3090559E+00
  eigenvectors of nos in ao-basis, column   22
 -0.3250955E+00 -0.1382913E+01  0.1110592E+01 -0.2601276E-04 -0.4156565E-05  0.5916414E+00  0.2963860E-03 -0.6750200E-06
 -0.9298683E+00 -0.5779436E-07  0.5664510E-05  0.1538101E+00 -0.6777783E-05 -0.1808833E+00  0.6318677E-01 -0.2046752E+00
  0.9426629E-01  0.7851630E-05  0.8265802E+00  0.6268734E-01 -0.2043198E+00 -0.9459551E-01  0.8700580E-05  0.8261384E+00
  eigenvectors of nos in ao-basis, column   23
 -0.3268205E+00 -0.1131518E+01  0.3920928E+01  0.5715936E-04 -0.8361730E-05 -0.3466558E+00  0.2576474E-03  0.1372264E-04
 -0.9481495E+00  0.4709603E-06 -0.1138689E-05  0.2118452E-01 -0.9379819E-04  0.1834711E+00 -0.1153978E+01 -0.4887500E+00
 -0.7405925E+00 -0.5248541E-06 -0.4933589E+00 -0.1154297E+01 -0.4891189E+00  0.7408253E+00 -0.3223717E-05 -0.4935643E+00
  eigenvectors of nos in ao-basis, column   24
 -0.4902228E-04 -0.1703420E-03  0.5991284E-03 -0.3557887E+00 -0.4302544E-05 -0.5716176E-04 -0.1592828E+01  0.6214757E-05
 -0.1330024E-03  0.7616252E-06 -0.2873894E-06  0.1905998E-05  0.5552409E+00  0.2765469E-04 -0.9352822E+00 -0.1221129E+01
 -0.7139505E+00  0.1310816E-05 -0.6477660E+00  0.9349606E+00  0.1220950E+01 -0.7137241E+00 -0.1164300E-05  0.6476013E+00


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***


 cosurf_and_phi
  2.466315385316 -0.061539892486  2.837440149000 -0.036995058213
  0.006359863210 -2.199287675687  3.114544749440 -0.066299132243
 -0.480526931872  1.061509344774  3.755560521090 -0.068037271044
  1.262739748330  2.872870034186  1.567866969040 -0.044333714067
  1.897949994968 -2.634366215369  0.570962659525 -0.015542600092
 -2.790166444607 -0.824331206483  2.170430257018 -0.021051807002
 -2.061296060778  2.493528161233  1.034251636268 -0.021528059467
 -0.002991703587  2.420846177436 -1.447622679829  0.014462138206
 -1.114435211431 -2.949559801670 -0.067898604227 -0.015687588576
  1.955709175590 -0.984462672186  3.123501329999 -0.049493291813
  1.000900154469 -1.706149424342  3.300403738319 -0.062946564729
  1.652947872482  0.360731428865  3.496571379926 -0.056295023157
  0.594940552015  0.670555958619  3.845537291721 -0.067745579959
  2.390078334690  1.202722009900  2.566708449184 -0.036273046813
  1.989334673621  2.229216060677  2.001028520754 -0.038472028461
  2.919143888229  0.378144896597  2.099775822683 -0.015966678514
  2.780367109803 -0.921046401167  2.130496470151 -0.020776672569
  2.449941849009 -2.011800733381  1.439000205618 -0.019041383045
 -0.231540571447 -1.263621120083  3.706953994699 -0.068653304270
 -0.352106389130 -0.107618694495  3.950679056227 -0.068957323677
  0.105380882100  1.878884201281  3.371423292662 -0.067568756312
  0.783266721831  2.590884380442  2.520834408126 -0.059390631531
  2.011402526412  2.551496219550  0.814855178005 -0.018209995707
  1.440220538007 -2.843720084632  1.356653310131 -0.038904720812
  0.871209499702 -2.660587439486  2.372618950166 -0.057416692651
 -2.947488997374  0.298617382934  2.058332654596 -0.014369360453
 -2.674954162172  1.563027715274  1.704200253745 -0.017157929570
 -1.353448939749  2.910920816403  0.212056013565 -0.018539852879
 -0.743422400795  2.810699746106 -0.731960510989 -0.003002704878
  0.099626804209  1.855073908563 -1.945822518898  0.033189306217
  0.019900458911 -1.968682238442 -1.864952523830  0.029791426066
 -0.601526683996 -2.669374282549 -1.032942810695  0.004416704050
 -1.976701730993 -2.578422698078  0.816296596419 -0.019086988941
 -2.525639241426 -1.850752473194  1.593331825886 -0.019662924094
 -1.124962231191 -1.881571875050  3.121019991266 -0.060829572450
 -2.117398618237 -1.513942036836  2.667866282109 -0.043049393773
 -2.336046769995 -0.152318885910  2.976109389266 -0.041031545905
 -1.478775916646  0.469153703894  3.577445396081 -0.059004505739
 -1.328587214463  1.668007282102  3.174271253825 -0.058946585891
 -1.771761195387  2.287385908353  2.202255939279 -0.044765375582
 -1.026730149698  3.017318998523  1.358608629268 -0.043998767427
  0.119020599620  3.173161356543  1.415159765853 -0.049791062286
  0.915237473358  3.108118624501  0.463299650838 -0.029798527518
  0.462814589486  2.837210699514 -0.795516582628 -0.003846242243
  1.026812912753 -3.017319139618  0.083991013862 -0.020471716728
 -0.070059393274 -3.176418651270  0.035610954337 -0.025708665147
 -0.970268948715 -3.083313212042  1.062474740253 -0.039998399054
 -0.534203029846 -2.828286137107  2.231267851728 -0.058184822213
  0.871897873699 -0.575451888002  3.799144800425 -0.065885459642
  1.408719892640  1.640288870679  3.148120355694 -0.057832182646
  2.650053602975  1.633766196698  1.655441764425 -0.017059613397
  1.964286464189 -2.001027831890  2.365093852582 -0.043089418020
 -1.342877128538 -0.760711330777  3.581800927521 -0.060688313479
 -0.531365927285  2.661712102822  2.509437292848 -0.060761459193
  1.142904167226  2.885766749848 -0.243475252462 -0.010816941901
  0.342129526197 -3.189261637688  1.246854200824 -0.046902123958
 -2.346459445268  1.135413320199  2.662806558936 -0.038184905089
 -0.258565605057  3.205542092831  0.249849913336 -0.029826442891
  0.450566961335 -2.699565911226 -1.032013446782  0.002996756221
 -1.684533476150 -2.430522251364  2.070179818920 -0.044710353641
 -3.368784049130  0.036613212616 -1.854972615293  0.060491817999
 -3.278396927799  1.580834404538 -0.078603229040  0.033544837442
 -3.656110378332 -0.713812229513  0.361831391088  0.034316132994
 -2.408052717333 -2.075622947728 -1.226189024363  0.038708366740
 -1.295829484895 -0.567704086865 -2.747607986898  0.058325355846
 -1.846557165647  1.681693338816 -2.099716493181  0.048702893382
 -3.831862607217  0.357468890277 -0.654813288033  0.050607089580
 -3.527736967644 -1.045671231370 -1.064852892071  0.051759496270
 -2.622906831544 -1.024738705101 -2.241124222290  0.058179814352
 -2.402781990555  0.378472303440 -2.579763034114  0.061430581188
 -3.126606927219  1.313275381963 -1.541857021673  0.053278849724
 -3.633182667875  0.531520753705  0.561856665829  0.031248858248
 -3.086813000261 -1.794874586082 -0.179700355757  0.031609196398
 -1.615880090830 -1.674393887320 -2.147504235692  0.047863928189
 -1.240258025012  0.765881087348 -2.687977655831  0.057081462422
 -2.430292517958  2.154437082790 -0.969924007583  0.034269772576
  3.368776503197 -0.036639040867 -1.854966842961  0.060482539592
  3.278392620256 -1.580850766330 -0.078589062660  0.033539155756
  3.656106873706  0.713798214804  0.361832640492  0.034310547771
  2.408046317685  2.075600470298 -1.226192756897  0.038699672089
  1.295820311657  0.567673501678 -2.747601655947  0.058316372801
  1.846549173548 -1.681720471299 -2.099699178989  0.048694496723
  3.831857249217 -0.357488322766 -0.654806650060  0.050599849381
  3.527730862121  1.045649613724 -1.064853177123  0.051751338054
  2.622898581638  1.024710819001 -2.241122746235  0.058169900305
  2.402773123306 -0.378501994157 -2.579753678917  0.061420530578
  3.126599952113 -1.313299541573 -1.541844004398  0.053270296443
  3.633179527908 -0.531533702443  0.561864593522  0.031244059929
  3.086808508398  1.794857685487 -0.179703829578  0.031602268037
  1.615872011596  1.674366500124 -2.147504385867  0.047854638730
  1.240248960489 -0.765911354740 -2.687964116774  0.057073169241
  2.430286585511 -2.154458194501 -0.969905238283  0.034262988586
  0.000006852884  1.035694667087 -2.361676372823  0.051565245059
 -0.298806015181  0.969607486526 -2.408809074493  0.053043182117
  0.298815704890  0.969607820141 -2.408807386530  0.053040152836
  0.000007656756 -1.035723195601 -2.361670853435  0.051565892096
 -0.298805262603 -0.969636498141 -2.408803907288  0.053043849978
  0.298816457468 -0.969636367899 -2.408802219325  0.053040644356
 -0.667712940797 -1.245186997899 -2.338574529312  0.050622507183
 -1.218112188165 -2.025138759161 -1.616566947615  0.035942885618
 -2.084175601108 -2.294008802021 -0.480477682822  0.020521797729
 -2.876631896395 -1.658047550445  0.559052047065  0.017383862751
 -3.282909221331 -0.368099862162  1.091996180628  0.019595721267
 -3.142757910518  1.067034798172  0.908143333087  0.018541488525
 -2.511458431963  2.081290260960  0.080011352455  0.017592782668
 -1.638016880752  2.274609499719 -1.065756198713  0.026778734959
 -0.866948462969  1.570740798135 -2.077229430584  0.046518717835
 -0.467915446486 -1.694563900014 -1.941501402531  0.039332631435
 -1.299753513329 -2.453901457341 -0.850306853788  0.012551182047
 -2.242033801688 -2.245340287831  0.385760998463  0.000992985358
 -2.923088754321 -1.151144046328  1.279154738758  0.003110898719
 -3.074287016292  0.397098864814  1.477489335582  0.004748325941
 -2.635990824421  1.788708515315  0.902534843950  0.001165693550
 -1.781079179779  2.474786487342 -0.218927030025  0.004482892002
 -0.846758459860  2.184720175398 -1.444553386477  0.024741761476
 -0.255852300976  1.360631011730 -2.219692209228  0.046123896798
  0.667723897920  1.245157743773 -2.338577856151  0.050615925678
  1.218123388571  2.025110412626 -1.616576841774  0.035934507613
  2.084186643139  2.293984793080 -0.480493513306  0.020514450934
  2.876642520254  1.658030225134  0.559035126479  0.017378795861
  3.282919570196  0.368088739237  1.091983758387  0.019592329947
  3.142768358070 -1.067043033853  0.908139383421  0.018538473951
  2.511469270857 -2.081300494445  0.080016452498  0.017588752479
  1.638028059662 -2.274626132712 -1.065745290088  0.026773012455
  0.866959534092 -1.570765766224 -2.077218589767  0.046512589594
  0.467932162408  1.694535407938 -1.941510815499  0.039327575824
  1.299770347024  2.453875604722 -0.850324284820  0.012544532918
  2.242050270258  2.245320705184  0.385739526123  0.000987921816
  2.923104801922  1.151131828431  1.279135167181  0.003107978590
  3.074302957696 -0.397105856761  1.477477125466  0.004746698157
  2.636007061705 -1.788714946455  0.902532611959  0.001164114034
  1.781095866662 -2.474797662024 -0.218920775640  0.004480076861
  0.846775315928 -2.184739733041 -1.444543821640  0.024738182892
  0.255868904327 -1.360657816862 -2.219684436601  0.046122321466
  0.427045418766 -0.580992742084 -2.585105089006  0.056641232503
  0.427044967836  0.580963354323 -2.585108185092  0.056640984264
 -0.427035838172 -0.000014913339 -2.543076305315  0.061874209354
 end_of_phi


 nsubv after electrostatic potential =  3

  Confirmation of dielectric energy for ground state
  edielnew =  -0.0109493987

 =========== Executing IN-CORE method ==========
 norm multnew:  0.00011520145
 threx: total number of loops= 14
 threx: total number of loops= 24
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331914E+01-0.309165E+01-0.410226E+01-0.407883E+01-0.475210E+01-0.534847E+01-0.433670E+01-0.358739E+01-0.342330E+01-0.317821E+01
 -0.403664E+01-0.263325E+01-0.362338E+01-0.366481E+01-0.366999E+01-0.363955E+01-0.338751E+01-0.303510E+01-0.289433E+01-0.330517E+02
 -0.787312E+01-0.671622E+01-0.695563E+01-0.711691E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         2        65         3         2        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         3       129         4         3        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         4       193         5         4        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         5       257         6         5        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         6       321         7         6        12        64       128         6       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331914E+01-0.309165E+01-0.410226E+01-0.407883E+01-0.475210E+01-0.534847E+01-0.433670E+01-0.358739E+01-0.342330E+01-0.317821E+01
 -0.403664E+01-0.263325E+01-0.362338E+01-0.366481E+01-0.366999E+01-0.363955E+01-0.338751E+01-0.303510E+01-0.289433E+01-0.330517E+02
 -0.787312E+01-0.671622E+01-0.695563E+01-0.711691E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         2        65         3         2        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         3       129         4         3        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         4       193         5         4        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         5       257         6         5        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         6       321         7         6        12        64       128         6       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331914E+01-0.309165E+01-0.410226E+01-0.407883E+01-0.475210E+01-0.534847E+01-0.433670E+01-0.358739E+01-0.342330E+01-0.317821E+01
 -0.403664E+01-0.263325E+01-0.362338E+01-0.366481E+01-0.366999E+01-0.363955E+01-0.338751E+01-0.303510E+01-0.289433E+01-0.330517E+02
 -0.787312E+01-0.671622E+01-0.695563E+01-0.711691E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         2        65         3         2        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         3       129         4         3        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         4       193         5         4        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         5       257         6         5        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         6       321         7         6        12        64       128         6       380       380


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        40 xx:       135 ww:       255
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        20 yw:        25

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        39
task #   9:       127    task #  10:       247    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 matrix-vector product formed for vector   4
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference-subspace vector overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     1.00000000     0.00000000     0.00000000     0.00000000

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.97563470    -0.08829986    -0.02857518    -0.19880527
 subspace has dimension  4

          (h-repnuc*1) matrix in the subspace basis

                x:   1         x:   2         x:   3         x:   4
   x:   1   -85.40090838
   x:   2    -0.25187978    -6.45464988
   x:   3    -0.00054635     0.22809435    -0.18307051
   x:   4     0.00011205    -0.04711533     0.00582665    -0.00958797

          overlap matrix in the subspace basis

                x:   1         x:   2         x:   3         x:   4
   x:   1     1.00000000
   x:   2     0.00000000     0.07914284
   x:   3     0.00000000    -0.00275501     0.00222409
   x:   4     0.00000000     0.00054875    -0.00007138     0.00011520

          eigenvectors and eigenvalues in the subspace basis

                v:   1         v:   2         v:   3         v:   4

   energy   -85.60536356   -83.38935609   -82.32172799   -80.93735892

   x:   1     0.97563470    -0.08829986    -0.02857518    -0.19880527
   x:   2     0.79032299     0.73347898     0.39480765     3.49597750
   x:   3     0.94727408     5.13401470   -20.49081503     5.31368943
   x:   4     0.98491490    88.65072182     2.32632058   -34.87535331

          eigenvectors and eigenvalues in the schmidt orthonormalized basis

                v:   1         v:   2         v:   3         v:   4

   energy   -85.60536356   -83.38935609   -82.32172799   -80.93735892

  zx:   1     0.97563470    -0.08829986    -0.02857518    -0.19880527
  zx:   2     0.21498081     0.32898905     0.31627375     0.86343522
  zx:   3     0.04258377     0.13638476    -0.94792324     0.28465304
  zx:   4     0.01033515     0.93025187     0.02441113    -0.36596276

          <ref|baseci> overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     1.00000000     0.00000000     0.00000000     0.00000000

  write the transformed vectors back to cosmofile

 rtflw =  0
 freezing =  F

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1    -76.2524007743 -9.3527E+00  1.9155E-05  7.5867E-03  1.0000E-03
 mr-sdci #  4  2    -74.0363933121 -8.2862E+00  0.0000E+00  1.3563E+00  1.0000E-04
 mr-sdci #  4  3    -72.9687652086 -8.2968E+00  0.0000E+00  1.5524E+00  1.0000E-04
 mr-sdci #  4  4    -71.5843961343  7.1584E+01  0.0000E+00  2.0664E+00  1.0000E-04


 total energy in cosmo calc 
e(rootcalc) + repnuc=     -76.2524007743
dielectric energy =      -0.0104563943
deltaediel =       0.0104563943
e(rootcalc) + repnuc - ediel =     -76.2419443800
e(rootcalc) + repnuc - edielnew =     -76.2524007743
deltaelast =      76.2419443800

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.01   0.00   0.00   0.01         0.    0.0000
    2   25    0   0.01   0.00   0.00   0.00         0.    0.0000
    3   26    0   0.03   0.00   0.00   0.02         0.    0.0000
    4   11    0   0.01   0.00   0.00   0.01         0.    0.0000
    5   15    0   0.02   0.00   0.00   0.02         0.    0.0000
    6   16    0   0.02   0.00   0.00   0.02         0.    0.0000
    7    1    0   0.01   0.00   0.00   0.01         0.    0.0000
    8    5    0   0.02   0.00   0.00   0.02         0.    0.0000
    9    6    0   0.02   0.00   0.00   0.02         0.    0.0000
   10    7    0   0.03   0.00   0.00   0.03         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.01   0.00   0.00   0.01         0.    0.0000
   14   47    0   0.02   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.2100s 
time spent in multnx:                   0.1800s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0200s 
total time per CI iteration:            3.0900s 

          starting ci iteration   5

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.35296278

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=      10  D0X=      30  D0W=      50
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================

          ci-one-electron density block   1

                mo   1         mo   2         mo   3         mo   4         mo   5         mo   6         mo   7         mo   8
   mo   1     1.99991263
   mo   2    -0.00021627     1.98515078
   mo   3     0.00000000    -0.00000001     1.96980696
   mo   4    -0.00015994     0.00406502    -0.00000013     1.97330176
   mo   5     0.00000000    -0.00000006    -0.00000001    -0.00000008     1.97628557
   mo   6     0.00013764    -0.00348011    -0.00001598    -0.02116761    -0.00000767     0.00694599
   mo   7     0.00000000     0.00000655    -0.01070069    -0.00001986    -0.00000472    -0.00000002     0.00749877
   mo   8    -0.00000001     0.00001400    -0.01255644    -0.00001847     0.00000228    -0.00000015     0.00713954     0.01057903
   mo   9     0.00006886     0.00355290    -0.00000995    -0.00060747     0.00000182     0.00705007    -0.00000017    -0.00000028
   mo  10    -0.00022394     0.01014232    -0.00001281     0.00314860     0.00000017     0.00267330    -0.00000015    -0.00000010
   mo  11     0.00000004    -0.00000158     0.00000281    -0.00000041    -0.01133629     0.00000018     0.00000008    -0.00000008
   mo  12    -0.00000001    -0.00000287    -0.00589637    -0.00000559    -0.00000229     0.00000007     0.00607338     0.00402507
   mo  13     0.00021270    -0.00103939    -0.00001156    -0.00739574    -0.00000286     0.00193696    -0.00000005    -0.00000018
   mo  14     0.00000000     0.00000202    -0.00000287    -0.00000108    -0.00000091    -0.00000003    -0.00000002    -0.00000002
   mo  15     0.00000000     0.00000097    -0.00000179    -0.00000449    -0.00302793     0.00000002    -0.00000001    -0.00000001
   mo  16    -0.00040237    -0.00039187    -0.00000437     0.00259465     0.00000148    -0.00191215    -0.00000008    -0.00000003
   mo  17    -0.00000001    -0.00000806     0.00100153     0.00001970     0.00000045    -0.00000003     0.00026719     0.00023782
   mo  18     0.00000003    -0.00000051     0.01168666    -0.00000062     0.00000087    -0.00000010    -0.00284801    -0.00438629
   mo  19    -0.00011377    -0.00466825    -0.00000018     0.00750976     0.00000042    -0.00310222    -0.00000012    -0.00000007
   mo  20     0.00000000     0.00000019    -0.00000045     0.00000129     0.00187859    -0.00000003     0.00000000     0.00000000
   mo  21     0.00000000    -0.00000022     0.00000055    -0.00000056    -0.00000130     0.00000001    -0.00000001    -0.00000002
   mo  22     0.00001094     0.00013926     0.00000232    -0.00106130     0.00000026    -0.00005499     0.00000003     0.00000003
   mo  23     0.00005365     0.00184638    -0.00000046     0.00031650    -0.00000044     0.00063170     0.00000000    -0.00000001
   mo  24    -0.00000001    -0.00000062    -0.00024271     0.00000177     0.00000009    -0.00000001    -0.00081541     0.00036173

                mo   9         mo  10         mo  11         mo  12         mo  13         mo  14         mo  15         mo  16
   mo   9     0.01003891
   mo  10     0.00327556     0.01005568
   mo  11    -0.00000005    -0.00000002     0.01439272
   mo  12     0.00000004     0.00000003     0.00000002     0.00646413
   mo  13     0.00053129    -0.00062440     0.00000006    -0.00000002     0.00374365
   mo  14    -0.00000002     0.00000000     0.00000006    -0.00000002     0.00000000     0.00299939
   mo  15    -0.00000003    -0.00000005    -0.00138889    -0.00000001     0.00000002    -0.00000005     0.00226419
   mo  16     0.00002943     0.00264231    -0.00000003    -0.00000006    -0.00124248     0.00000001    -0.00000002     0.00439997
   mo  17     0.00000017     0.00000024    -0.00000001     0.00035657    -0.00000002     0.00000000     0.00000000     0.00000006
   mo  18    -0.00000013    -0.00000015     0.00000001    -0.00181764     0.00000001     0.00000000     0.00000000    -0.00000003
   mo  19    -0.00435858    -0.00079758     0.00000000    -0.00000012    -0.00084449     0.00000000    -0.00000001    -0.00005482
   mo  20    -0.00000001     0.00000000     0.00004864     0.00000000    -0.00000001     0.00000000    -0.00183612     0.00000000
   mo  21     0.00000000    -0.00000001     0.00000002     0.00000000     0.00000000    -0.00203269     0.00000000    -0.00000001
   mo  22    -0.00056035     0.00030343    -0.00000001     0.00000002     0.00173163     0.00000000     0.00000000     0.00070762
   mo  23     0.00022590     0.00176700     0.00000000     0.00000003     0.00025143     0.00000000     0.00000000    -0.00069173
   mo  24     0.00000001     0.00000000     0.00000001    -0.00167693     0.00000001     0.00000000     0.00000000     0.00000002

                mo  17         mo  18         mo  19         mo  20         mo  21         mo  22         mo  23         mo  24
   mo  17     0.00052443
   mo  18    -0.00020277     0.00262715
   mo  19     0.00000002     0.00000005     0.00289300
   mo  20     0.00000000     0.00000000     0.00000001     0.00257039
   mo  21     0.00000000     0.00000001     0.00000000     0.00000001     0.00244288
   mo  22     0.00000000     0.00000001    -0.00016442     0.00000000     0.00000000     0.00199016
   mo  23     0.00000003    -0.00000002     0.00062482     0.00000000     0.00000000    -0.00008900     0.00165204
   mo  24     0.00000432    -0.00058723     0.00000002     0.00000000     0.00000000     0.00000000    -0.00000001     0.00145984


*****   symmetry block SYM1   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       1.99991263
       2      -0.00021627     1.98515078
       3       0.00000000    -0.00000001     1.96980696
       4      -0.00015994     0.00406502    -0.00000013     1.97330176
       5       0.00000000    -0.00000006    -0.00000001    -0.00000008
       6       0.00013764    -0.00348011    -0.00001598    -0.02116761
       7       0.00000000     0.00000655    -0.01070069    -0.00001986
       8      -0.00000001     0.00001400    -0.01255644    -0.00001847
       9       0.00006886     0.00355290    -0.00000995    -0.00060747
      10      -0.00022394     0.01014232    -0.00001281     0.00314860
      11       0.00000004    -0.00000158     0.00000281    -0.00000041
      12      -0.00000001    -0.00000287    -0.00589637    -0.00000559
      13       0.00021270    -0.00103939    -0.00001156    -0.00739574
      14       0.00000000     0.00000202    -0.00000287    -0.00000108
      15       0.00000000     0.00000097    -0.00000179    -0.00000449
      16      -0.00040237    -0.00039187    -0.00000437     0.00259465
      17      -0.00000001    -0.00000806     0.00100153     0.00001970
      18       0.00000003    -0.00000051     0.01168666    -0.00000062
      19      -0.00011377    -0.00466825    -0.00000018     0.00750976
      20       0.00000000     0.00000019    -0.00000045     0.00000129
      21       0.00000000    -0.00000022     0.00000055    -0.00000056
      22       0.00001094     0.00013926     0.00000232    -0.00106130
      23       0.00005365     0.00184638    -0.00000046     0.00031650
      24      -0.00000001    -0.00000062    -0.00024271     0.00000177

               Column   5     Column   6     Column   7     Column   8
       5       1.97628557
       6      -0.00000767     0.00694599
       7      -0.00000472    -0.00000002     0.00749877
       8       0.00000228    -0.00000015     0.00713954     0.01057903
       9       0.00000182     0.00705007    -0.00000017    -0.00000028
      10       0.00000017     0.00267330    -0.00000015    -0.00000010
      11      -0.01133629     0.00000018     0.00000008    -0.00000008
      12      -0.00000229     0.00000007     0.00607338     0.00402507
      13      -0.00000286     0.00193696    -0.00000005    -0.00000018
      14      -0.00000091    -0.00000003    -0.00000002    -0.00000002
      15      -0.00302793     0.00000002    -0.00000001    -0.00000001
      16       0.00000148    -0.00191215    -0.00000008    -0.00000003
      17       0.00000045    -0.00000003     0.00026719     0.00023782
      18       0.00000087    -0.00000010    -0.00284801    -0.00438629
      19       0.00000042    -0.00310222    -0.00000012    -0.00000007
      20       0.00187859    -0.00000003     0.00000000     0.00000000
      21      -0.00000130     0.00000001    -0.00000001    -0.00000002
      22       0.00000026    -0.00005499     0.00000003     0.00000003
      23      -0.00000044     0.00063170     0.00000000    -0.00000001
      24       0.00000009    -0.00000001    -0.00081541     0.00036173

               Column   9     Column  10     Column  11     Column  12
       9       0.01003891
      10       0.00327556     0.01005568
      11      -0.00000005    -0.00000002     0.01439272
      12       0.00000004     0.00000003     0.00000002     0.00646413
      13       0.00053129    -0.00062440     0.00000006    -0.00000002
      14      -0.00000002     0.00000000     0.00000006    -0.00000002
      15      -0.00000003    -0.00000005    -0.00138889    -0.00000001
      16       0.00002943     0.00264231    -0.00000003    -0.00000006
      17       0.00000017     0.00000024    -0.00000001     0.00035657
      18      -0.00000013    -0.00000015     0.00000001    -0.00181764
      19      -0.00435858    -0.00079758     0.00000000    -0.00000012
      20      -0.00000001     0.00000000     0.00004864     0.00000000
      21       0.00000000    -0.00000001     0.00000002     0.00000000
      22      -0.00056035     0.00030343    -0.00000001     0.00000002
      23       0.00022590     0.00176700     0.00000000     0.00000003
      24       0.00000001     0.00000000     0.00000001    -0.00167693

               Column  13     Column  14     Column  15     Column  16
      13       0.00374365
      14       0.00000000     0.00299939
      15       0.00000002    -0.00000005     0.00226419
      16      -0.00124248     0.00000001    -0.00000002     0.00439997
      17      -0.00000002     0.00000000     0.00000000     0.00000006
      18       0.00000001     0.00000000     0.00000000    -0.00000003
      19      -0.00084449     0.00000000    -0.00000001    -0.00005482
      20      -0.00000001     0.00000000    -0.00183612     0.00000000
      21       0.00000000    -0.00203269     0.00000000    -0.00000001
      22       0.00173163     0.00000000     0.00000000     0.00070762
      23       0.00025143     0.00000000     0.00000000    -0.00069173
      24       0.00000001     0.00000000     0.00000000     0.00000002

               Column  17     Column  18     Column  19     Column  20
      17       0.00052443
      18      -0.00020277     0.00262715
      19       0.00000002     0.00000005     0.00289300
      20       0.00000000     0.00000000     0.00000001     0.00257039
      21       0.00000000     0.00000001     0.00000000     0.00000001
      22       0.00000000     0.00000001    -0.00016442     0.00000000
      23       0.00000003    -0.00000002     0.00062482     0.00000000
      24       0.00000432    -0.00058723     0.00000002     0.00000000

               Column  21     Column  22     Column  23     Column  24
      21       0.00244288
      22       0.00000000     0.00199016
      23       0.00000000    -0.00008900     0.00165204
      24       0.00000000     0.00000000    -0.00000001     0.00145984

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7        eno   8
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    8   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno   9        eno  10        eno  11        eno  12        eno  13        eno  14        eno  15        eno  16
 emo    9   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   10   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   11   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   12   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   13   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   14   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   15   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   16   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno  17        eno  18        eno  19        eno  20        eno  21        eno  22        eno  23        eno  24
 emo   17   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   18   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   19   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   20   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   21   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   22   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   23   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   24   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   1.9999180      1.9865250      1.9763575      1.9722941      1.9700349
   0.0213816      0.0196238      0.0144951      0.0102636      0.0054399
   0.0051918      0.0047728      0.0042460      0.0041589      0.0010286
   0.0010274      0.0006695      0.0005853      0.0005038      0.0005013
   0.0004565      0.0004391      0.0000507      0.0000346

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7         no   8
  mo    1   0.999811E+00   0.191569E-01  -0.634067E-07   0.326725E-02  -0.508392E-06   0.256718E-07  -0.424081E-04  -0.211944E-07
  mo    2  -0.172724E-01   0.953039E+00  -0.821472E-06  -0.302289E+00   0.519813E-04  -0.483432E-05  -0.308208E-02   0.949181E-06
  mo    3   0.865645E-07  -0.471780E-05  -0.239413E-06   0.156775E-03   0.999942E+00   0.101757E-01   0.144172E-04  -0.155938E-05
  mo    4  -0.890211E-02   0.302142E+00   0.926791E-05   0.953143E+00  -0.148238E-03   0.103992E-04   0.680545E-02   0.125827E-08
  mo    5   0.133114E-06  -0.204509E-05   0.999982E+00  -0.913077E-05   0.235881E-06   0.127028E-05   0.169738E-05   0.553334E-02
  mo    6   0.193925E-03  -0.489169E-02  -0.399480E-05  -0.974509E-02  -0.666013E-05  -0.188962E-03   0.536396E+00   0.440310E-05
  mo    7   0.308882E-07   0.155850E-06  -0.239421E-05  -0.115481E-04  -0.549355E-02   0.570368E+00   0.188528E-03   0.348119E-05
  mo    8  -0.440765E-07   0.395670E-05   0.115090E-05  -0.121800E-04  -0.644732E-02   0.637688E+00   0.200590E-03  -0.693978E-05
  mo    9   0.695808E-05   0.161454E-02   0.904836E-06  -0.887859E-03  -0.497113E-05  -0.223588E-03   0.671250E+00  -0.110646E-04
  mo   10  -0.214665E-03   0.536783E-02   0.968734E-07  -0.472798E-04  -0.652227E-05  -0.136994E-03   0.407188E+00  -0.112366E-04
  mo   11   0.334181E-07  -0.817185E-06  -0.577682E-02   0.969752E-07   0.143553E-05   0.215651E-05   0.971788E-05   0.992783E+00
  mo   12   0.444964E-07  -0.221009E-05  -0.116549E-05  -0.280114E-05  -0.303824E-02   0.438453E+00   0.165324E-03   0.114656E-05
  mo   13   0.148927E-03  -0.163019E-02  -0.148906E-05  -0.343396E-02  -0.535281E-05  -0.459613E-04   0.849502E-01   0.506506E-05
  mo   14  -0.129823E-07   0.806421E-06  -0.462789E-06  -0.833145E-06  -0.145680E-05  -0.321481E-05  -0.277745E-05   0.474198E-05
  mo   15   0.107419E-07  -0.214642E-06  -0.153063E-02  -0.230732E-05  -0.909359E-06  -0.195448E-05  -0.439569E-05  -0.117562E+00
  mo   16  -0.210341E-03   0.215796E-03   0.768864E-06   0.132753E-02  -0.242729E-05  -0.249829E-05  -0.296170E-02  -0.625102E-05
  mo   17  -0.243510E-07  -0.872247E-06   0.228316E-06   0.108355E-04   0.505796E-03   0.251873E-01   0.284592E-04  -0.594770E-06
  mo   18   0.241414E-07  -0.375753E-06   0.443645E-06   0.755335E-06   0.596490E-02  -0.270905E+00  -0.103914E-03   0.154398E-05
  mo   19  -0.503453E-04  -0.109716E-02   0.254807E-06   0.436967E-02  -0.753568E-06   0.872374E-04  -0.291866E+00   0.309988E-05
  mo   20  -0.631040E-08   0.287109E-06   0.953034E-03   0.586805E-06  -0.229529E-06  -0.479409E-07  -0.289364E-06   0.230228E-01
  mo   21   0.562158E-08  -0.192193E-06  -0.656575E-06  -0.236843E-06   0.283304E-06  -0.340002E-06   0.153547E-06   0.598741E-06
  mo   22   0.901849E-05  -0.953929E-04   0.126885E-06  -0.537138E-03   0.125481E-05   0.288807E-05  -0.578011E-02  -0.781541E-07
  mo   23   0.942324E-05   0.938022E-03  -0.222239E-06  -0.132819E-03  -0.216373E-06  -0.186357E-04   0.583134E-01  -0.115445E-05
  mo   24  -0.767904E-08  -0.255764E-07   0.472965E-07   0.934729E-06  -0.121387E-03  -0.408067E-01  -0.160634E-04  -0.156423E-06

                no   9         no  10         no  11         no  12         no  13         no  14         no  15         no  16
  mo    1   0.236478E-03  -0.310918E-07  -0.102743E-03  -0.115056E-08   0.459944E-04   0.235161E-09   0.193845E-04   0.905927E-07
  mo    2  -0.383128E-02   0.397365E-05  -0.250819E-03  -0.837447E-06   0.378782E-03   0.196203E-06   0.935801E-03   0.571645E-06
  mo    3   0.165834E-05  -0.210205E-02   0.497510E-05   0.128347E-05   0.147580E-05  -0.302100E-06   0.966407E-05  -0.169509E-02
  mo    4  -0.613534E-02   0.509699E-07   0.362291E-02   0.332031E-06   0.120855E-02  -0.197670E-05  -0.270929E-02  -0.858162E-05
  mo    5  -0.156523E-05   0.176407E-05   0.215423E-05  -0.162887E-06  -0.332515E-06  -0.226431E-02  -0.947177E-06  -0.896988E-06
  mo    6  -0.249793E+00   0.390661E-04   0.190975E+00   0.267699E-05  -0.127180E+00  -0.316681E-05  -0.137910E+00  -0.775638E-03
  mo    7  -0.982508E-05   0.222875E+00  -0.600672E-04   0.295960E-05   0.500845E-05  -0.593280E-06  -0.196146E-03   0.373971E-01
  mo    8   0.699289E-05  -0.544150E+00   0.161833E-03  -0.359520E-05   0.444923E-05  -0.993770E-06  -0.226227E-02   0.402347E+00
  mo    9  -0.173683E+00  -0.117664E-03  -0.365311E+00  -0.274229E-05   0.464893E-01   0.719400E-06   0.402935E+00   0.226934E-02
  mo   10   0.768274E+00   0.941325E-04   0.313862E+00   0.155606E-05  -0.144644E+00  -0.595221E-05  -0.283008E+00  -0.157238E-02
  mo   11   0.110139E-04  -0.561252E-05  -0.693849E-05  -0.599258E-05   0.285133E-05  -0.948451E-01   0.276656E-05   0.177242E-05
  mo   12  -0.802374E-05   0.638610E+00  -0.174097E-03   0.733087E-05  -0.175067E-05   0.447423E-06   0.162890E-02  -0.290827E+00
  mo   13  -0.252928E+00   0.175736E-03   0.607698E+00   0.177345E-04   0.446149E+00   0.806018E-05   0.584354E-01   0.303655E-03
  mo   14   0.314606E-05  -0.722094E-05  -0.127129E-04   0.753533E+00  -0.841059E-05  -0.139874E-04   0.868047E-07  -0.564601E-05
  mo   15  -0.587232E-05   0.156302E-05   0.197904E-06  -0.207668E-04   0.112745E-04  -0.654086E+00  -0.274658E-06  -0.918967E-06
  mo   16   0.467755E+00  -0.947870E-04  -0.363833E+00  -0.600666E-06   0.558893E+00   0.660150E-05   0.280787E+00   0.155118E-02
  mo   17   0.113063E-04   0.195781E-01   0.938447E-05   0.207254E-05   0.169310E-05   0.399034E-06   0.139071E-02  -0.252346E+00
  mo   18  -0.597162E-05   0.284842E+00  -0.927776E-04   0.243576E-05  -0.212637E-05   0.120517E-05  -0.345746E-02   0.615431E+00
  mo   19   0.154266E+00   0.550051E-04   0.184233E+00  -0.144119E-05  -0.277806E+00  -0.580710E-05   0.424872E+00   0.234271E-02
  mo   20   0.184396E-05  -0.152999E-05   0.353448E-07   0.142930E-04  -0.123037E-04   0.750447E+00   0.148185E-05  -0.177955E-05
  mo   21  -0.179363E-05   0.674212E-05   0.108360E-04  -0.657410E+00   0.932682E-05   0.218105E-04   0.148108E-05  -0.642227E-05
  mo   22   0.252539E-01   0.914504E-04   0.319479E+00   0.109012E-04   0.522032E+00   0.877963E-05   0.136461E+00   0.816759E-03
  mo   23   0.996907E-01   0.889600E-04   0.306388E+00   0.160000E-05  -0.315653E+00  -0.665364E-05   0.676090E+00   0.382228E-02
  mo   24   0.309657E-05  -0.406057E+00   0.120658E-03  -0.453032E-05   0.678869E-05  -0.146791E-05   0.312793E-02  -0.556481E+00

                no  17         no  18         no  19         no  20         no  21         no  22         no  23         no  24
  mo    1  -0.158214E-08  -0.206534E-07   0.436015E-04   0.963380E-08   0.362927E-07  -0.350251E-04   0.248109E-04   0.382994E-08
  mo    2  -0.623623E-06  -0.299696E-05   0.268013E-02   0.267518E-06   0.478334E-05  -0.197443E-02   0.173693E-02  -0.458981E-07
  mo    3   0.761307E-06  -0.590663E-03   0.132295E-05   0.769703E-06  -0.169060E-02  -0.691157E-05   0.623008E-06   0.166886E-02
  mo    4   0.564012E-06   0.634289E-05   0.196309E-02   0.179021E-05  -0.523621E-05  -0.169261E-02   0.608083E-02   0.899231E-06
  mo    5   0.686289E-06   0.126480E-05   0.842931E-06   0.936874E-03  -0.128911E-06  -0.117528E-05   0.219690E-05   0.116413E-05
  mo    6  -0.510487E-05  -0.123488E-03   0.473561E+00   0.119553E-03   0.258057E-03  -0.202718E+00   0.559235E+00   0.557901E-04
  mo    7  -0.149686E-04   0.564928E+00   0.865808E-04  -0.590548E-05   0.184922E+00   0.542103E-04  -0.492899E-04   0.519844E+00
  mo    8   0.114083E-04  -0.187226E+00  -0.528462E-04   0.162512E-05   0.556723E-01   0.259332E-04   0.280702E-04  -0.311725E+00
  mo    9   0.132743E-05   0.319882E-05  -0.972020E-02  -0.290521E-05  -0.100409E-04   0.243308E-01  -0.469673E+00  -0.502619E-04
  mo   10   0.178415E-05   0.388480E-04  -0.603685E-01  -0.122221E-04  -0.408911E-04   0.127732E-01  -0.201468E+00  -0.173313E-04
  mo   11  -0.113942E-04  -0.338470E-05  -0.215018E-04   0.731611E-01   0.113663E-05   0.274881E-05  -0.553535E-05  -0.275087E-05
  mo   12   0.273447E-05  -0.149625E+00   0.377971E-05   0.328892E-05  -0.127421E+00  -0.570956E-04   0.530004E-04  -0.526043E+00
  mo   13   0.594280E-05   0.175928E-04  -0.324569E+00  -0.850804E-04   0.270805E-03  -0.467918E+00  -0.180952E+00  -0.242693E-04
  mo   14   0.657410E+00   0.206682E-04   0.100278E-04   0.815924E-04  -0.472510E-05   0.268106E-05   0.136676E-05   0.872438E-06
  mo   15  -0.820033E-04   0.494297E-05  -0.186203E-03   0.747227E+00   0.130159E-04  -0.484922E-05  -0.263984E-05   0.816226E-07
  mo   16  -0.143674E-05  -0.547158E-04   0.337562E-01   0.785184E-05   0.258661E-03  -0.329943E+00   0.384206E+00   0.328909E-04
  mo   17   0.139408E-04  -0.377456E+00  -0.306566E-03  -0.130616E-04   0.887094E+00   0.735087E-03   0.938365E-06   0.767587E-01
  mo   18  -0.451404E-05   0.384477E+00   0.319507E-05  -0.829690E-05   0.376483E+00   0.199260E-03   0.404842E-04  -0.420846E+00
  mo   19  -0.704418E-05  -0.191916E-03   0.587403E+00   0.141912E-03   0.454783E-03  -0.395320E+00  -0.313108E+00  -0.342145E-04
  mo   20  -0.825587E-04   0.488376E-05  -0.164038E-03   0.660529E+00   0.109689E-04  -0.514369E-05  -0.142673E-05  -0.138619E-06
  mo   21   0.753533E+00   0.218454E-04   0.952749E-05   0.836115E-04  -0.303979E-05   0.198927E-05   0.489201E-06   0.730052E-06
  mo   22  -0.887842E-05  -0.231831E-04   0.432482E+00   0.111619E-03  -0.381518E-03   0.643239E+00  -0.729714E-01  -0.300882E-06
  mo   23   0.355646E-05   0.105947E-03  -0.365271E+00  -0.880372E-04  -0.267694E-03   0.245560E+00   0.377246E+00   0.326971E-04
  mo   24  -0.208805E-04   0.577165E+00   0.947459E-04  -0.732436E-05   0.133375E+00   0.764763E-04   0.461396E-04  -0.415769E+00
  eigenvectors of nos in ao-basis, column    1
  0.1000601E+01 -0.1416748E-01 -0.1662950E-02  0.4365277E-07  0.1686514E-06 -0.7501229E-02  0.9594434E-07 -0.5108404E-07
  0.1209813E-03  0.7701527E-08 -0.1089811E-07  0.4350094E-04  0.1567133E-07  0.9053851E-04 -0.3347733E-02  0.2186115E-02
 -0.1025491E-02  0.3331375E-08 -0.9462926E-03 -0.3347864E-02  0.2186254E-02  0.1025508E-02  0.1686370E-07 -0.9463420E-03
  eigenvectors of nos in ao-basis, column    2
  0.1017950E-01  0.9140286E+00 -0.7247807E-01 -0.3141476E-05 -0.3014548E-05  0.1218758E+00 -0.4828240E-05  0.1264897E-05
  0.7714856E-01 -0.3100449E-06  0.3755442E-06 -0.1194929E-02 -0.2034405E-06  0.1141340E-02  0.2178381E+00 -0.1064168E+00
  0.2706184E-01  0.3624194E-06  0.2151867E-01  0.2178478E+00 -0.1064235E+00 -0.2706377E-01 -0.5978448E-06  0.2151719E-01
  eigenvectors of nos in ao-basis, column    3
 -0.7156470E-06 -0.2394794E-05  0.2321008E-05  0.2181872E-05  0.9107900E+00  0.8023813E-05 -0.2165212E-05  0.8238727E-01
  0.1778626E-05 -0.6443710E-06 -0.1703321E-01 -0.7484777E-07 -0.1252219E-06 -0.2272504E-06 -0.5984419E-05  0.9379457E-05
  0.8365485E-06  0.3071705E-01 -0.2642187E-06 -0.1539336E-05  0.2632443E-06 -0.3264413E-06  0.3071819E-01 -0.5280322E-06
  eigenvectors of nos in ao-basis, column    4
  0.1383795E-02  0.3036965E-02  0.2155867E+00  0.1264214E-03 -0.8094451E-05  0.8028642E+00 -0.2435400E-04  0.3620085E-06
 -0.2023605E-01 -0.1465560E-06  0.1149086E-05 -0.4750081E-02 -0.4476723E-05 -0.2395039E-02 -0.4286804E+00  0.1831619E+00
 -0.3810072E-01 -0.2489788E-05  0.2504669E-02 -0.4285298E+00  0.1831016E+00  0.3810296E-01 -0.1165821E-05  0.2532609E-02
  eigenvectors of nos in ao-basis, column    5
  0.3755754E-06  0.2646069E-05 -0.3976205E-04  0.7311466E+00  0.2299357E-05 -0.1141288E-03 -0.1185100E+00 -0.1787219E-05
 -0.3128500E-06  0.4915859E-06 -0.1052781E-06  0.6329648E-06 -0.2545186E-01  0.6521533E-06 -0.5522188E+00  0.1932995E+00
 -0.2034197E-01 -0.1670878E-05 -0.3121460E-01  0.5523732E+00 -0.1933610E+00 -0.2035655E-01  0.1125379E-06  0.3120325E-01
  eigenvectors of nos in ao-basis, column    6
 -0.3555557E-04 -0.2787892E-03  0.5273000E-04 -0.1303982E+01  0.4337289E-05  0.4054143E-03  0.5757082E+00 -0.2364613E-05
 -0.2319244E-03  0.4986275E-07  0.2295709E-06  0.1601542E-05 -0.6699003E-01  0.1013917E-04 -0.1006041E+01  0.3568436E+00
 -0.4011645E-01 -0.3810192E-05 -0.1746466E-01  0.1006686E+01 -0.3571091E+00 -0.4014705E-01  0.8610885E-06  0.1743823E-01
  eigenvectors of nos in ao-basis, column    7
  0.1140292E+00  0.8513221E+00 -0.1629986E+00 -0.4602411E-03  0.1554944E-04 -0.1209923E+01  0.2235591E-03 -0.1354704E-04
  0.7539559E+00  0.5227024E-06  0.3263728E-06 -0.4321307E-02 -0.2157563E-04 -0.3197591E-01 -0.9735743E+00  0.4123060E+00
 -0.5021132E-01 -0.5247469E-05 -0.9019651E-02 -0.9729036E+00  0.4120629E+00  0.5019041E-01 -0.1552642E-05 -0.8965255E-02
  eigenvectors of nos in ao-basis, column    8
  0.3796367E-05  0.1867857E-04 -0.2057584E-04  0.7560967E-06  0.1412498E+01  0.2211368E-04  0.3004729E-05 -0.1610285E+01
 -0.2234803E-04  0.3070170E-07  0.3841727E-01  0.1316579E-06  0.1301532E-05  0.6064775E-06  0.2878842E-04 -0.2918573E-04
 -0.1389174E-06 -0.7566480E-01  0.1798054E-05  0.1535855E-05  0.6226110E-06  0.1159648E-05 -0.7567176E-01  0.1186179E-05
  eigenvectors of nos in ao-basis, column    9
 -0.4423021E+00 -0.2262023E+01  0.2151828E+01  0.5929283E-05  0.1449263E-04 -0.7435848E+00 -0.2122014E-04 -0.1577666E-04
  0.1095134E+01 -0.2328849E-05  0.2847265E-05 -0.1313359E-01 -0.6006625E-05  0.6251038E-01  0.3813950E+00 -0.2186064E+00
 -0.1252049E-01 -0.2291499E-05  0.7769339E-01  0.3814392E+00 -0.2186486E+00  0.1252310E-01 -0.5242099E-05  0.7771326E-01
  eigenvectors of nos in ao-basis, column   10
  0.7588060E-04  0.3340067E-03 -0.4740372E-03 -0.4951323E+00 -0.6405879E-05 -0.5930951E-05  0.1033832E+01  0.9150296E-05
  0.5781230E-04  0.8160326E-05 -0.1857466E-05 -0.2864272E-04  0.6169433E+00  0.7645316E-04  0.6537782E+00 -0.5024662E+00
  0.1120714E+00 -0.1777786E-05 -0.2203976E-01 -0.6533921E+00  0.5022463E+00  0.1121230E+00  0.2993404E-05  0.2217132E-01
  eigenvectors of nos in ao-basis, column   11
  0.2901650E+00  0.1296722E+01 -0.1781504E+01  0.1211903E-03 -0.7813770E-05 -0.3594266E-01 -0.2859279E-03  0.1171989E-04
  0.2118965E+00  0.1325809E-04 -0.3712437E-08 -0.9886689E-01 -0.1857291E-03  0.2630955E+00  0.6411171E+00 -0.3794011E+00
 -0.6818093E-01 -0.4397081E-05  0.2208780E+00  0.6415200E+00 -0.3797066E+00  0.6812621E-01  0.4790914E-05  0.2208896E+00
  eigenvectors of nos in ao-basis, column   12
  0.5031321E-05  0.2406852E-04 -0.2570756E-04 -0.6855774E-05 -0.7104028E-05  0.7046104E-06  0.9449806E-05  0.1782832E-04
 -0.6663082E-05 -0.8020698E+00  0.1829482E-04 -0.3991893E-05  0.6403351E-05  0.1707154E-05 -0.1374765E-05 -0.8789474E-07
 -0.2211255E-05  0.2665265E+00  0.3539344E-05 -0.3423884E-05  0.3661454E-05  0.5848493E-05 -0.2665470E+00  0.6669764E-05
  eigenvectors of nos in ao-basis, column   13
 -0.1298675E+00 -0.5514444E+00  0.8977584E+00  0.1455186E-05  0.2870383E-05  0.9925395E-01 -0.2965699E-05 -0.7564547E-05
 -0.5120498E+00  0.1108522E-04 -0.1467456E-04 -0.2102776E+00 -0.9735780E-05 -0.2146623E+00 -0.4692710E+00  0.2712120E+00
 -0.1635547E+00  0.1477438E-05  0.9178533E-01 -0.4692738E+00  0.2712232E+00  0.1635610E+00  0.5911189E-05  0.9178293E-01
  eigenvectors of nos in ao-basis, column   14
  0.4946520E-06  0.2196244E-05 -0.1433079E-05  0.3843132E-06 -0.8658660E-01  0.4586128E-05 -0.4499760E-06  0.3065216E+00
 -0.1157277E-04  0.2519017E-04  0.8897543E+00 -0.3494419E-05  0.2202487E-05 -0.4382646E-05 -0.4970260E-05  0.5430166E-05
 -0.1895663E-05 -0.1997111E+00  0.6214818E-06 -0.4271791E-05  0.3993579E-05  0.2372014E-05 -0.1997085E+00  0.1356726E-05
  eigenvectors of nos in ao-basis, column   15
 -0.3543540E+00 -0.1342409E+01  0.2691578E+01 -0.2786661E-02  0.3058422E-05  0.1176376E+00  0.7231244E-02 -0.5045991E-05
 -0.1147759E+01  0.1576946E-05  0.1598713E-05 -0.3002755E-01 -0.4537041E-02  0.4152484E+00 -0.2248776E+01  0.1270167E+01
 -0.7623619E-01  0.1000178E-05 -0.1622371E+00 -0.2264613E+01  0.1278475E+01  0.7707375E-01 -0.2478910E-06 -0.1593784E+00
  eigenvectors of nos in ao-basis, column   16
 -0.1961663E-02 -0.7412625E-02  0.1502247E-01  0.4962320E+00  0.1722720E-05  0.5903574E-03 -0.1285575E+01 -0.1930385E-05
 -0.6347684E-02 -0.6158605E-05 -0.1705369E-05 -0.1830739E-03  0.8074879E+00  0.2346432E-02 -0.1418524E+01  0.7406864E+00
 -0.7746425E-01 -0.7772853E-05  0.2566520E+00  0.1393162E+01 -0.7263627E+00 -0.7658524E-01  0.4871199E-05 -0.2585821E+00
  eigenvectors of nos in ao-basis, column   17
  0.3600556E-05  0.1726175E-04 -0.5454413E-05 -0.2129322E-04 -0.1054305E-04 -0.9014362E-05 -0.1861538E-04  0.9470229E-04
 -0.4043441E-05  0.7232474E+00 -0.7318852E-04  0.2610462E-05  0.2489779E-04  0.6646011E-06 -0.8853036E-04  0.7207360E-04
 -0.1898019E-04  0.7382140E+00 -0.1876227E-04  0.4857473E-04 -0.4229220E-04  0.4016547E-05 -0.7384077E+00  0.2093134E-04
  eigenvectors of nos in ao-basis, column   18
  0.9393472E-04  0.4254666E-03 -0.1712985E-03  0.8956469E+00 -0.3904026E-05 -0.2474830E-03  0.1170031E+00  0.1277566E-05
  0.1052444E-03  0.2075986E-04  0.4308633E-05  0.7613820E-05 -0.6582882E+00  0.4095726E-04  0.1758398E+01 -0.1628682E+01
  0.3830102E+00  0.2832712E-04  0.6928408E+00 -0.1758985E+01  0.1629143E+01  0.3834042E+00 -0.1668999E-04 -0.6930990E+00
  eigenvectors of nos in ao-basis, column   19
 -0.2176755E+00 -0.1057747E+01 -0.1385656E+00  0.4989460E-04 -0.1826690E-04  0.7098924E+00  0.3448718E-03  0.2000559E-03
  0.3199699E+00  0.8923385E-05 -0.1416972E-03 -0.1253029E+00 -0.1085212E-03 -0.1171802E+00  0.1576367E+01 -0.1103864E+01
  0.9010459E+00 -0.1998539E-03  0.1590215E+00  0.1575415E+01 -0.1103184E+01 -0.9012292E+00 -0.2208786E-03  0.1585202E+00
  eigenvectors of nos in ao-basis, column   20
 -0.5348181E-04 -0.2596251E-03 -0.2847655E-04 -0.1710098E-04  0.5878911E-01  0.1680540E-03  0.1917877E-04 -0.7811135E+00
  0.8970379E-04  0.7913635E-04  0.5709549E+00 -0.3229591E-04  0.8416501E-05 -0.2762779E-04  0.3800255E-03 -0.2628936E-03
  0.2066785E-03  0.8452610E+00  0.3441585E-04  0.4008567E-03 -0.2909424E-03 -0.2379617E-03  0.8450853E+00  0.3103146E-04
  eigenvectors of nos in ao-basis, column   21
 -0.2835157E-03 -0.1216593E-02  0.9092559E-03  0.6205517E+00  0.6925371E-06  0.5636164E-03 -0.1263172E+01 -0.1321112E-04
 -0.7417958E-03 -0.2650359E-05  0.9387292E-05  0.1046111E-03 -0.1447186E+00 -0.1432161E-03 -0.3196735E+00 -0.1024746E+00
  0.7462653E+00  0.1009741E-04 -0.3519367E+00  0.3200119E+00  0.1019781E+00  0.7459198E+00  0.1889934E-04  0.3532242E+00
  eigenvectors of nos in ao-basis, column   22
  0.2981105E+00  0.1240218E+01 -0.1219030E+01  0.3411897E-03  0.3081803E-05 -0.4936995E+00 -0.8636875E-03 -0.1712537E-06
  0.1043309E+01  0.1787425E-05 -0.4594294E-05 -0.1777871E+00 -0.9336902E-04  0.1579826E+00  0.2443603E+00  0.8814731E-02
  0.7319290E-01 -0.3254713E-05 -0.8031482E+00  0.2450014E+00  0.8676077E-02 -0.7213169E-01 -0.8450581E-05 -0.8024188E+00
  eigenvectors of nos in ao-basis, column   23
 -0.3239864E+00 -0.1116873E+01  0.3944425E+01  0.3780750E-04 -0.5617538E-05 -0.3543803E+00  0.1563893E-03  0.1151869E-04
 -0.9653814E+00  0.3483631E-06 -0.1085982E-05  0.2212036E-01 -0.6222863E-04  0.1884040E+00 -0.1203608E+01 -0.4541293E+00
 -0.7610831E+00 -0.1452242E-05 -0.5061766E+00 -0.1203812E+01 -0.4543514E+00  0.7612389E+00 -0.3700916E-05 -0.5062971E+00
  eigenvectors of nos in ao-basis, column   24
 -0.2784748E-04 -0.9575232E-04  0.3568367E-03 -0.3608662E+00 -0.2814320E-05 -0.3602936E-04 -0.1594614E+01  0.4716276E-05
 -0.7465293E-04  0.6703487E-06 -0.1672600E-06  0.2051035E-06  0.5603610E+00  0.1651858E-04 -0.9519602E+00 -0.1201790E+01
 -0.7226878E+00  0.9051333E-06 -0.6533575E+00  0.9517810E+00  0.1201669E+01 -0.7225492E+00 -0.8487024E-06  0.6532476E+00


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***


 cosurf_and_phi
  2.466315385316 -0.061539892486  2.837440149000 -0.037323951864
  0.006359863210 -2.199287675687  3.114544749440 -0.066628577454
 -0.480526931872  1.061509344774  3.755560521090 -0.068421559433
  1.262739748330  2.872870034186  1.567866969040 -0.044517968849
  1.897949994968 -2.634366215369  0.570962659525 -0.015599173146
 -2.790166444607 -0.824331206483  2.170430257018 -0.021306608552
 -2.061296060778  2.493528161233  1.034251636268 -0.021643759199
 -0.002991703587  2.420846177436 -1.447622679829  0.014704290427
 -1.114435211431 -2.949559801670 -0.067898604227 -0.015665567843
  1.955709175590 -0.984462672186  3.123501329999 -0.049842802525
  1.000900154469 -1.706149424342  3.300403738319 -0.063297604396
  1.652947872482  0.360731428865  3.496571379926 -0.056673980827
  0.594940552015  0.670555958619  3.845537291721 -0.068138187266
  2.390078334690  1.202722009900  2.566708449184 -0.036571146362
  1.989334673621  2.229216060677  2.001028520754 -0.038704472091
  2.919143888229  0.378144896597  2.099775822683 -0.016213520203
  2.780367109803 -0.921046401167  2.130496470151 -0.021026805166
  2.449941849009 -2.011800733381  1.439000205618 -0.019205787827
 -0.231540571447 -1.263621120083  3.706953994699 -0.069032283423
 -0.352106389130 -0.107618694495  3.950679056227 -0.069356601351
  0.105380882100  1.878884201281  3.371423292662 -0.067919932538
  0.783266721831  2.590884380442  2.520834408126 -0.059670084050
  2.011402526412  2.551496219550  0.814855178005 -0.018297838573
  1.440220538007 -2.843720084632  1.356653310131 -0.039064480545
  0.871209499702 -2.660587439486  2.372618950166 -0.057682528993
 -2.947488997374  0.298617382934  2.058332654596 -0.014610802663
 -2.674954162172  1.563027715274  1.704200253745 -0.017354736781
 -1.353448939749  2.910920816403  0.212056013565 -0.018554592010
 -0.743422400795  2.810699746106 -0.731960510989 -0.002882909713
  0.099626804209  1.855073908563 -1.945822518898  0.033533445272
  0.019900458911 -1.968682238442 -1.864952523830  0.030118208713
 -0.601526683996 -2.669374282549 -1.032942810695  0.004586216725
 -1.976701730993 -2.578422698078  0.816296596419 -0.019175076682
 -2.525639241426 -1.850752473194  1.593331825886 -0.019846149458
 -1.124962231191 -1.881571875050  3.121019991266 -0.061166144198
 -2.117398618237 -1.513942036836  2.667866282109 -0.043354901737
 -2.336046769995 -0.152318885910  2.976109389266 -0.041372875691
 -1.478775916646  0.469153703894  3.577445396081 -0.059387189657
 -1.328587214463  1.668007282102  3.174271253825 -0.059290604811
 -1.771761195387  2.287385908353  2.202255939279 -0.045018779700
 -1.026730149698  3.017318998523  1.358608629268 -0.044160208712
  0.119020599620  3.173161356543  1.415159765853 -0.049960035849
  0.915237473358  3.108118624501  0.463299650838 -0.029852449906
  0.462814589486  2.837210699514 -0.795516582628 -0.003718925600
  1.026812912753 -3.017319139618  0.083991013862 -0.020472847045
 -0.070059393274 -3.176418651270  0.035610954337 -0.025709464382
 -0.970268948715 -3.083313212042  1.062474740253 -0.040125944092
 -0.534203029846 -2.828286137107  2.231267851728 -0.058436039101
  0.871897873699 -0.575451888002  3.799144800425 -0.066277312206
  1.408719892640  1.640288870679  3.148120355694 -0.058175048561
  2.650053602975  1.633766196698  1.655441764425 -0.017250575060
  1.964286464189 -2.001027831890  2.365093852582 -0.043361485758
 -1.342877128538 -0.760711330777  3.581800927521 -0.061068840513
 -0.531365927285  2.661712102822  2.509437292848 -0.061038788459
  1.142904167226  2.885766749848 -0.243475252462 -0.010768776792
  0.342129526197 -3.189261637688  1.246854200824 -0.047052550832
 -2.346459445268  1.135413320199  2.662806558936 -0.038492848331
 -0.258565605057  3.205542092831  0.249849913336 -0.029856152608
  0.450566961335 -2.699565911226 -1.032013446782  0.003164512306
 -1.684533476150 -2.430522251364  2.070179818920 -0.044949176143
 -3.368784049130  0.036613212616 -1.854972615293  0.060674305295
 -3.278396927799  1.580834404538 -0.078603229040  0.033593521085
 -3.656110378332 -0.713812229513  0.361831391088  0.034323332510
 -2.408052717333 -2.075622947728 -1.226189024363  0.038876564822
 -1.295829484895 -0.567704086865 -2.747607986898  0.058688651824
 -1.846557165647  1.681693338816 -2.099716493181  0.048971067774
 -3.831862607217  0.357468890277 -0.654813288033  0.050701962639
 -3.527736967644 -1.045671231370 -1.064852892071  0.051887833435
 -2.622906831544 -1.024738705101 -2.241124222290  0.058415485222
 -2.402781990555  0.378472303440 -2.579763034114  0.061698777666
 -3.126606927219  1.313275381963 -1.541857021673  0.053449447322
 -3.633182667875  0.531520753705  0.561856665829  0.031233859786
 -3.086813000261 -1.794874586082 -0.179700355757  0.031667701473
 -1.615880090830 -1.674393887320 -2.147504235692  0.048151549786
 -1.240258025012  0.765881087348 -2.687977655831  0.057445594526
 -2.430292517958  2.154437082790 -0.969924007583  0.034413404545
  3.368776503197 -0.036639040867 -1.854966842961  0.060664405093
  3.278392620256 -1.580850766330 -0.078589062660  0.033587425026
  3.656106873706  0.713798214804  0.361832640492  0.034317267177
  2.408046317685  2.075600470298 -1.226192756897  0.038867275341
  1.295820311657  0.567673501678 -2.747601655947  0.058679133851
  1.846549173548 -1.681720471299 -2.099699178989  0.048962200510
  3.831857249217 -0.357488322766 -0.654806650060  0.050694191608
  3.527730862121  1.045649613724 -1.064853177123  0.051879078075
  2.622898581638  1.024710819001 -2.241122746235  0.058404921049
  2.402773123306 -0.378501994157 -2.579753678917  0.061688112683
  3.126599952113 -1.313299541573 -1.541844004398  0.053440351206
  3.633179527908 -0.531533702443  0.561864593522  0.031228642254
  3.086808508398  1.794857685487 -0.179703829578  0.031660235780
  1.615872011596  1.674366500124 -2.147504385867  0.048141676281
  1.240248960489 -0.765911354740 -2.687964116774  0.057436839218
  2.430286585511 -2.154458194501 -0.969905238283  0.034406208699
  0.000006852884  1.035694667087 -2.361676372823  0.052003848410
 -0.298806015181  0.969607486526 -2.408809074493  0.053479995908
  0.298815704890  0.969607820141 -2.408807386530  0.053476796298
  0.000007656756 -1.035723195601 -2.361670853435  0.052004554705
 -0.298805262603 -0.969636498141 -2.408803907288  0.053480722093
  0.298816457468 -0.969636367899 -2.408802219325  0.053477338911
 -0.667712940797 -1.245186997899 -2.338574529312  0.051018983523
 -1.218112188165 -2.025138759161 -1.616566947615  0.036213788559
 -2.084175601108 -2.294008802021 -0.480477682822  0.020617216232
 -2.876631896395 -1.658047550445  0.559052047065  0.017344249186
 -3.282909221331 -0.368099862162  1.091996180628  0.019490051749
 -3.142757910518  1.067034798172  0.908143333087  0.018458508279
 -2.511458431963  2.081290260960  0.080011352455  0.017613564074
 -1.638016880752  2.274609499719 -1.065756198713  0.026959111443
 -0.866948462969  1.570740798135 -2.077229430584  0.046875641891
 -0.467915446486 -1.694563900014 -1.941501402531  0.039693766294
 -1.299753513329 -2.453901457341 -0.850306853788  0.012704092052
 -2.242033801688 -2.245340287831  0.385760998463  0.000963138688
 -2.923088754321 -1.151144046328  1.279154738758  0.002960405424
 -3.074287016292  0.397098864814  1.477489335582  0.004570727873
 -2.635990824421  1.788708515315  0.902534843950  0.001065874791
 -1.781079179779  2.474786487342 -0.218927030025  0.004538020524
 -0.846758459860  2.184720175398 -1.444553386477  0.025000020998
 -0.255852300976  1.360631011730 -2.219692209228  0.046529993490
  0.667723897920  1.245157743773 -2.338577856151  0.051012000161
  1.218123388571  2.025110412626 -1.616576841774  0.036204869233
  2.084186643139  2.293984793080 -0.480493513306  0.020609319488
  2.876642520254  1.658030225134  0.559035126479  0.017338694719
  3.282919570196  0.368088739237  1.091983758387  0.019486246794
  3.142768358070 -1.067043033853  0.908139383421  0.018455138214
  2.511469270857 -2.081300494445  0.080016452498  0.017609196205
  1.638028059662 -2.274626132712 -1.065745290088  0.026953042166
  0.866959534092 -1.570765766224 -2.077218589767  0.046869193906
  0.467932162408  1.694535407938 -1.941510815499  0.039688386373
  1.299770347024  2.453875604722 -0.850324284820  0.012696976189
  2.242050270258  2.245320705184  0.385739526123  0.000957614610
  2.923104801922  1.151131828431  1.279135167181  0.002957081903
  3.074302957696 -0.397105856761  1.477477125466  0.004568769705
  2.636007061705 -1.788714946455  0.902532611959  0.001064023005
  1.781095866662 -2.474797662024 -0.218920775640  0.004534956204
  0.846775315928 -2.184739733041 -1.444543821640  0.024996241508
  0.255868904327 -1.360657816862 -2.219684436601  0.046528355014
  0.427045418766 -0.580992742084 -2.585105089006  0.057084741524
  0.427044967836  0.580963354323 -2.585108185092  0.057084464470
 -0.427035838172 -0.000014913339 -2.543076305315  0.062342809607
 end_of_phi


 nsubv after electrostatic potential =  4

  Confirmation of dielectric energy for ground state
  edielnew =  -0.010998054

 =========== Executing IN-CORE method ==========
 norm multnew:  1.03278845E-05
 threx: total number of loops= 14
 threx: total number of loops= 24
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331914E+01-0.309165E+01-0.410226E+01-0.407883E+01-0.475210E+01-0.534847E+01-0.433670E+01-0.358739E+01-0.342330E+01-0.317821E+01
 -0.403664E+01-0.263325E+01-0.362338E+01-0.366481E+01-0.366999E+01-0.363955E+01-0.338751E+01-0.303510E+01-0.289433E+01-0.330517E+02
 -0.787312E+01-0.671622E+01-0.695563E+01-0.711691E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         2        65         3         2        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         3       129         4         3        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         4       193         5         4        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         5       257         6         5        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         6       321         7         6        12        64       128         6       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331914E+01-0.309165E+01-0.410226E+01-0.407883E+01-0.475210E+01-0.534847E+01-0.433670E+01-0.358739E+01-0.342330E+01-0.317821E+01
 -0.403664E+01-0.263325E+01-0.362338E+01-0.366481E+01-0.366999E+01-0.363955E+01-0.338751E+01-0.303510E+01-0.289433E+01-0.330517E+02
 -0.787312E+01-0.671622E+01-0.695563E+01-0.711691E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         2        65         3         2        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         3       129         4         3        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         4       193         5         4        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         5       257         6         5        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         6       321         7         6        12        64       128         6       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331914E+01-0.309165E+01-0.410226E+01-0.407883E+01-0.475210E+01-0.534847E+01-0.433670E+01-0.358739E+01-0.342330E+01-0.317821E+01
 -0.403664E+01-0.263325E+01-0.362338E+01-0.366481E+01-0.366999E+01-0.363955E+01-0.338751E+01-0.303510E+01-0.289433E+01-0.330517E+02
 -0.787312E+01-0.671622E+01-0.695563E+01-0.711691E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         2        65         3         2        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         3       129         4         3        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         4       193         5         4        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         5       257         6         5        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         6       321         7         6        12        64       128         6       380       380


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        40 xx:       135 ww:       255
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        20 yw:        25

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        39
task #   9:       127    task #  10:       247    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 matrix-vector product formed for vector   5
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference-subspace vector overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     1.00000000     0.00000000     0.00000000     0.00000000     0.00000000

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     0.97557585    -0.02988360     0.08718653    -0.09375014    -0.17597768
 subspace has dimension  5

          (h-repnuc*1) matrix in the subspace basis

                x:   1         x:   2         x:   3         x:   4         x:   5
   x:   1   -85.40090838
   x:   2    -0.25187978    -6.45464988
   x:   3    -0.00054635     0.22809435    -0.18307051
   x:   4     0.00011205    -0.04711533     0.00582665    -0.00958797
   x:   5    -0.00000093     0.00038994    -0.00326741     0.00090299    -0.00086133

          overlap matrix in the subspace basis

                x:   1         x:   2         x:   3         x:   4         x:   5
   x:   1     1.00000000
   x:   2     0.00000000     0.07914284
   x:   3     0.00000000    -0.00275501     0.00222409
   x:   4     0.00000000     0.00054875    -0.00007138     0.00011520
   x:   5     0.00000000    -0.00000454     0.00003817    -0.00001078     0.00001033

          eigenvectors and eigenvalues in the subspace basis

                v:   1         v:   2         v:   3         v:   4         v:   5

   energy   -85.60538074   -83.60950542   -83.38898961   -81.81008879   -80.68145175

   x:   1     0.97557585    -0.02988360     0.08718653    -0.09375014    -0.17597768
   x:   2     0.79036229     0.20223976    -0.72536774     1.37477468     3.25545312
   x:   3     0.94991817     8.34666754    -4.71190203   -17.21599012    10.68587719
   x:   4     1.05134918    19.80902587   -87.97159056     3.38178259   -42.92177341
   x:   5     0.89695904   272.59201010    10.76682901   158.31032945  -120.32135524

          eigenvectors and eigenvalues in the schmidt orthonormalized basis

                v:   1         v:   2         v:   3         v:   4         v:   5

   energy   -85.60538074   -83.60950542   -83.38898961   -81.81008879   -80.68145175

  zx:   1     0.97557585    -0.02988360     0.08718653    -0.09375014    -0.17597768
  zx:   2     0.21508109     0.00939765    -0.32968990     0.55939590     0.72940565
  zx:   3     0.04336944     0.58718526    -0.10881080    -0.66761600     0.44247220
  zx:   4     0.01019371    -0.04698093    -0.93319134    -0.11251757    -0.33790917
  zx:   5     0.00265700     0.80748060     0.03189384     0.46895182    -0.35641969

          <ref|baseci> overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     1.00000000     0.00000000     0.00000000     0.00000000     0.00000000

  write the transformed vectors back to cosmofile

 rtflw =  0
 freezing =  F

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1    -76.2524179555 -9.3529E+00  8.1750E-07  1.7118E-03  1.0000E-03
 mr-sdci #  5  2    -74.2565426430 -9.1328E+00  0.0000E+00  1.1442E+00  1.0000E-04
 mr-sdci #  5  3    -74.0360268238 -8.2857E+00  0.0000E+00  1.3767E+00  1.0000E-04
 mr-sdci #  5  4    -72.4571260057 -8.4802E+00  0.0000E+00  1.2136E+00  1.0000E-04
 mr-sdci #  5  5    -71.3284889668  7.1328E+01  0.0000E+00  2.3248E+00  1.0000E-04


 total energy in cosmo calc 
e(rootcalc) + repnuc=     -76.2524179555
dielectric energy =      -0.0104563943
deltaediel =       0.0104563943
e(rootcalc) + repnuc - ediel =     -76.2419615612
e(rootcalc) + repnuc - edielnew =     -76.2524179555
deltaelast =      76.2419615612

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.01   0.00   0.00   0.00         0.    0.0000
    2   25    0   0.02   0.00   0.00   0.01         0.    0.0000
    3   26    0   0.02   0.00   0.00   0.02         0.    0.0000
    4   11    0   0.01   0.00   0.00   0.01         0.    0.0000
    5   15    0   0.02   0.00   0.00   0.02         0.    0.0000
    6   16    0   0.02   0.00   0.00   0.02         0.    0.0000
    7    1    0   0.00   0.00   0.00   0.00         0.    0.0000
    8    5    0   0.01   0.00   0.00   0.01         0.    0.0000
    9    6    0   0.03   0.00   0.00   0.03         0.    0.0000
   10    7    0   0.02   0.00   0.00   0.02         0.    0.0000
   11   75    0   0.01   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.01   0.00   0.00   0.01         0.    0.0000
   14   47    0   0.02   0.00   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.009998
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.2000s 
time spent in multnx:                   0.1700s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            3.1200s 

          starting ci iteration   6

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.35296278

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=      10  D0X=      30  D0W=      50
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================

          ci-one-electron density block   1

                mo   1         mo   2         mo   3         mo   4         mo   5         mo   6         mo   7         mo   8
   mo   1     1.99991386
   mo   2    -0.00021122     1.98510045
   mo   3     0.00000000    -0.00000004     1.96967305
   mo   4    -0.00015621     0.00414514    -0.00000004     1.97327455
   mo   5     0.00000000    -0.00000007    -0.00000001    -0.00000006     1.97620895
   mo   6     0.00012425    -0.00387583    -0.00001600    -0.01935635    -0.00000754     0.00697846
   mo   7     0.00000000     0.00000672    -0.00987658    -0.00002000    -0.00000468    -0.00000008     0.00753954
   mo   8    -0.00000001     0.00001411    -0.01193137    -0.00001850     0.00000221    -0.00000022     0.00720830     0.01062711
   mo   9     0.00006069     0.00301959    -0.00000985    -0.00021846     0.00000171     0.00712493    -0.00000021    -0.00000032
   mo  10    -0.00021780     0.01029916    -0.00001286     0.00330248     0.00000015     0.00263563    -0.00000019    -0.00000012
   mo  11     0.00000004    -0.00000158     0.00000280    -0.00000043    -0.01162024     0.00000021     0.00000009    -0.00000009
   mo  12    -0.00000001    -0.00000289    -0.00591876    -0.00000576    -0.00000234     0.00000006     0.00613580     0.00410989
   mo  13     0.00020664    -0.00114151    -0.00001159    -0.00745335    -0.00000289     0.00196239    -0.00000006    -0.00000019
   mo  14     0.00000000     0.00000202    -0.00000287    -0.00000107    -0.00000082    -0.00000003    -0.00000003    -0.00000002
   mo  15     0.00000000     0.00000096    -0.00000179    -0.00000449    -0.00294230     0.00000001    -0.00000001    -0.00000001
   mo  16    -0.00039511    -0.00029190    -0.00000435     0.00287671     0.00000152    -0.00196102    -0.00000010    -0.00000004
   mo  17    -0.00000001    -0.00000809     0.00096518     0.00001963     0.00000045     0.00000000     0.00027552     0.00024687
   mo  18     0.00000003    -0.00000045     0.01162267    -0.00000056     0.00000087    -0.00000010    -0.00287021    -0.00439913
   mo  19    -0.00011186    -0.00450504    -0.00000018     0.00755978     0.00000042    -0.00312158    -0.00000012    -0.00000007
   mo  20     0.00000000     0.00000019    -0.00000045     0.00000129     0.00188170    -0.00000003     0.00000000     0.00000000
   mo  21     0.00000000    -0.00000022     0.00000056    -0.00000056    -0.00000129     0.00000001    -0.00000001    -0.00000002
   mo  22     0.00001097     0.00012990     0.00000232    -0.00108412     0.00000027    -0.00006440     0.00000003     0.00000003
   mo  23     0.00005336     0.00176170    -0.00000045     0.00036837    -0.00000045     0.00064129     0.00000000    -0.00000002
   mo  24    -0.00000001    -0.00000060    -0.00030103     0.00000177     0.00000009     0.00000000    -0.00083444     0.00033645

                mo   9         mo  10         mo  11         mo  12         mo  13         mo  14         mo  15         mo  16
   mo   9     0.01013493
   mo  10     0.00336578     0.01011783
   mo  11    -0.00000005    -0.00000002     0.01452566
   mo  12     0.00000004     0.00000003     0.00000002     0.00653304
   mo  13     0.00054664    -0.00063941     0.00000006    -0.00000002     0.00372297
   mo  14    -0.00000003     0.00000000     0.00000006    -0.00000002     0.00000000     0.00296842
   mo  15    -0.00000003    -0.00000006    -0.00139740    -0.00000001     0.00000002    -0.00000005     0.00223972
   mo  16     0.00001885     0.00265507    -0.00000003    -0.00000007    -0.00125577     0.00000001    -0.00000002     0.00439103
   mo  17     0.00000019     0.00000025    -0.00000001     0.00035992    -0.00000002     0.00000000     0.00000000     0.00000006
   mo  18    -0.00000013    -0.00000016     0.00000001    -0.00184875     0.00000001     0.00000000     0.00000000    -0.00000003
   mo  19    -0.00438193    -0.00083002     0.00000000    -0.00000012    -0.00084394     0.00000000    -0.00000001    -0.00005216
   mo  20    -0.00000001     0.00000000     0.00004229     0.00000000    -0.00000001     0.00000000    -0.00181974     0.00000000
   mo  21     0.00000000    -0.00000001     0.00000002     0.00000000     0.00000000    -0.00201709     0.00000000    -0.00000001
   mo  22    -0.00056695     0.00029342    -0.00000001     0.00000002     0.00171720     0.00000000     0.00000000     0.00070058
   mo  23     0.00024947     0.00177670     0.00000000     0.00000003     0.00025206     0.00000000     0.00000000    -0.00068644
   mo  24     0.00000001     0.00000000     0.00000001    -0.00168849     0.00000001     0.00000000     0.00000000     0.00000002

                mo  17         mo  18         mo  19         mo  20         mo  21         mo  22         mo  23         mo  24
   mo  17     0.00051423
   mo  18    -0.00020418     0.00262196
   mo  19     0.00000001     0.00000005     0.00288430
   mo  20     0.00000000     0.00000000     0.00000001     0.00254686
   mo  21     0.00000000     0.00000001     0.00000000     0.00000001     0.00242057
   mo  22    -0.00000001     0.00000001    -0.00015932     0.00000000     0.00000000     0.00197199
   mo  23     0.00000003    -0.00000002     0.00061040     0.00000000     0.00000000    -0.00008799     0.00163996
   mo  24     0.00000304    -0.00057394     0.00000002     0.00000000     0.00000000    -0.00000001    -0.00000001     0.00145057


*****   symmetry block SYM1   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       1.99991386
       2      -0.00021122     1.98510045
       3       0.00000000    -0.00000004     1.96967305
       4      -0.00015621     0.00414514    -0.00000004     1.97327455
       5       0.00000000    -0.00000007    -0.00000001    -0.00000006
       6       0.00012425    -0.00387583    -0.00001600    -0.01935635
       7       0.00000000     0.00000672    -0.00987658    -0.00002000
       8      -0.00000001     0.00001411    -0.01193137    -0.00001850
       9       0.00006069     0.00301959    -0.00000985    -0.00021846
      10      -0.00021780     0.01029916    -0.00001286     0.00330248
      11       0.00000004    -0.00000158     0.00000280    -0.00000043
      12      -0.00000001    -0.00000289    -0.00591876    -0.00000576
      13       0.00020664    -0.00114151    -0.00001159    -0.00745335
      14       0.00000000     0.00000202    -0.00000287    -0.00000107
      15       0.00000000     0.00000096    -0.00000179    -0.00000449
      16      -0.00039511    -0.00029190    -0.00000435     0.00287671
      17      -0.00000001    -0.00000809     0.00096518     0.00001963
      18       0.00000003    -0.00000045     0.01162267    -0.00000056
      19      -0.00011186    -0.00450504    -0.00000018     0.00755978
      20       0.00000000     0.00000019    -0.00000045     0.00000129
      21       0.00000000    -0.00000022     0.00000056    -0.00000056
      22       0.00001097     0.00012990     0.00000232    -0.00108412
      23       0.00005336     0.00176170    -0.00000045     0.00036837
      24      -0.00000001    -0.00000060    -0.00030103     0.00000177

               Column   5     Column   6     Column   7     Column   8
       5       1.97620895
       6      -0.00000754     0.00697846
       7      -0.00000468    -0.00000008     0.00753954
       8       0.00000221    -0.00000022     0.00720830     0.01062711
       9       0.00000171     0.00712493    -0.00000021    -0.00000032
      10       0.00000015     0.00263563    -0.00000019    -0.00000012
      11      -0.01162024     0.00000021     0.00000009    -0.00000009
      12      -0.00000234     0.00000006     0.00613580     0.00410989
      13      -0.00000289     0.00196239    -0.00000006    -0.00000019
      14      -0.00000082    -0.00000003    -0.00000003    -0.00000002
      15      -0.00294230     0.00000001    -0.00000001    -0.00000001
      16       0.00000152    -0.00196102    -0.00000010    -0.00000004
      17       0.00000045     0.00000000     0.00027552     0.00024687
      18       0.00000087    -0.00000010    -0.00287021    -0.00439913
      19       0.00000042    -0.00312158    -0.00000012    -0.00000007
      20       0.00188170    -0.00000003     0.00000000     0.00000000
      21      -0.00000129     0.00000001    -0.00000001    -0.00000002
      22       0.00000027    -0.00006440     0.00000003     0.00000003
      23      -0.00000045     0.00064129     0.00000000    -0.00000002
      24       0.00000009     0.00000000    -0.00083444     0.00033645

               Column   9     Column  10     Column  11     Column  12
       9       0.01013493
      10       0.00336578     0.01011783
      11      -0.00000005    -0.00000002     0.01452566
      12       0.00000004     0.00000003     0.00000002     0.00653304
      13       0.00054664    -0.00063941     0.00000006    -0.00000002
      14      -0.00000003     0.00000000     0.00000006    -0.00000002
      15      -0.00000003    -0.00000006    -0.00139740    -0.00000001
      16       0.00001885     0.00265507    -0.00000003    -0.00000007
      17       0.00000019     0.00000025    -0.00000001     0.00035992
      18      -0.00000013    -0.00000016     0.00000001    -0.00184875
      19      -0.00438193    -0.00083002     0.00000000    -0.00000012
      20      -0.00000001     0.00000000     0.00004229     0.00000000
      21       0.00000000    -0.00000001     0.00000002     0.00000000
      22      -0.00056695     0.00029342    -0.00000001     0.00000002
      23       0.00024947     0.00177670     0.00000000     0.00000003
      24       0.00000001     0.00000000     0.00000001    -0.00168849

               Column  13     Column  14     Column  15     Column  16
      13       0.00372297
      14       0.00000000     0.00296842
      15       0.00000002    -0.00000005     0.00223972
      16      -0.00125577     0.00000001    -0.00000002     0.00439103
      17      -0.00000002     0.00000000     0.00000000     0.00000006
      18       0.00000001     0.00000000     0.00000000    -0.00000003
      19      -0.00084394     0.00000000    -0.00000001    -0.00005216
      20      -0.00000001     0.00000000    -0.00181974     0.00000000
      21       0.00000000    -0.00201709     0.00000000    -0.00000001
      22       0.00171720     0.00000000     0.00000000     0.00070058
      23       0.00025206     0.00000000     0.00000000    -0.00068644
      24       0.00000001     0.00000000     0.00000000     0.00000002

               Column  17     Column  18     Column  19     Column  20
      17       0.00051423
      18      -0.00020418     0.00262196
      19       0.00000001     0.00000005     0.00288430
      20       0.00000000     0.00000000     0.00000001     0.00254686
      21       0.00000000     0.00000001     0.00000000     0.00000001
      22      -0.00000001     0.00000001    -0.00015932     0.00000000
      23       0.00000003    -0.00000002     0.00061040     0.00000000
      24       0.00000304    -0.00057394     0.00000002     0.00000000

               Column  21     Column  22     Column  23     Column  24
      21       0.00242057
      22       0.00000000     0.00197199
      23       0.00000000    -0.00008799     0.00163996
      24       0.00000000    -0.00000001    -0.00000001     0.00145057

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7        eno   8
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    8   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno   9        eno  10        eno  11        eno  12        eno  13        eno  14        eno  15        eno  16
 emo    9   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   10   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   11   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   12   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   13   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   14   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   15   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   16   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno  17        eno  18        eno  19        eno  20        eno  21        eno  22        eno  23        eno  24
 emo   17   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   18   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   19   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   20   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   21   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   22   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   23   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   24   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   1.9999190      1.9865228      1.9762839      1.9721841      1.9698838
   0.0215958      0.0198223      0.0146242      0.0103305      0.0054101
   0.0051533      0.0047301      0.0042096      0.0041203      0.0010170
   0.0010134      0.0006589      0.0005713      0.0004936      0.0004928
   0.0004485      0.0004316      0.0000501      0.0000331

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7         no   8
  mo    1   0.999820E+00   0.187369E-01  -0.552910E-07   0.313109E-02  -0.556528E-06   0.236926E-07  -0.372275E-04  -0.215726E-07
  mo    2  -0.168739E-01   0.951669E+00  -0.890057E-06  -0.306596E+00   0.594186E-04  -0.490251E-05  -0.279128E-02   0.962056E-06
  mo    3   0.797509E-07  -0.453935E-05   0.280323E-06   0.179425E-03   0.999946E+00   0.971987E-02   0.145644E-04  -0.155789E-05
  mo    4  -0.872199E-02   0.306454E+00   0.115499E-04   0.951776E+00  -0.169600E-03   0.105211E-04   0.614544E-02   0.200068E-07
  mo    5   0.142346E-06  -0.272006E-05   0.999981E+00  -0.113108E-04  -0.282605E-06   0.129970E-05   0.171468E-05   0.568601E-02
  mo    6   0.180036E-03  -0.484647E-02  -0.394032E-05  -0.878379E-02  -0.662353E-05  -0.206595E-03   0.535595E+00   0.508855E-05
  mo    7   0.300457E-07   0.163759E-06  -0.238148E-05  -0.116934E-04  -0.507336E-02   0.570152E+00   0.206454E-03   0.380835E-05
  mo    8  -0.429222E-07   0.395209E-05   0.111303E-05  -0.123199E-04  -0.612776E-02   0.636717E+00   0.220452E-03  -0.765486E-05
  mo    9   0.628032E-05   0.141441E-02   0.849790E-06  -0.620221E-03  -0.494968E-05  -0.245694E-03   0.672533E+00  -0.129428E-04
  mo   10  -0.211298E-03   0.546734E-02   0.895265E-07  -0.195268E-04  -0.655270E-05  -0.150248E-03   0.406418E+00  -0.128339E-04
  mo   11   0.332898E-07  -0.815424E-06  -0.592219E-02   0.106789E-06   0.143358E-05   0.261146E-05   0.114435E-04   0.992919E+00
  mo   12   0.452233E-07  -0.225909E-05  -0.119303E-05  -0.294219E-05  -0.304851E-02   0.440639E+00   0.181546E-03   0.796805E-06
  mo   13   0.146119E-03  -0.170382E-02  -0.151369E-05  -0.343788E-02  -0.528705E-05  -0.491002E-04   0.851983E-01   0.513255E-05
  mo   14  -0.127343E-07   0.806152E-06  -0.416210E-06  -0.832962E-06  -0.145710E-05  -0.331777E-05  -0.285105E-05   0.513531E-05
  mo   15   0.105444E-07  -0.226707E-06  -0.148715E-02  -0.230303E-05  -0.907227E-06  -0.202627E-05  -0.468796E-05  -0.116610E+00
  mo   16  -0.208614E-03   0.313765E-03   0.791620E-06   0.144689E-02  -0.246693E-05  -0.205663E-05  -0.500745E-02  -0.665912E-05
  mo   17  -0.238790E-07  -0.848610E-06   0.228066E-06   0.108165E-04   0.487418E-03   0.254827E-01   0.301545E-04  -0.603671E-06
  mo   18   0.224759E-07  -0.336560E-06   0.443773E-06   0.907498E-06   0.593169E-02  -0.269845E+00  -0.112969E-03   0.171958E-05
  mo   19  -0.512179E-04  -0.991254E-03   0.263979E-06   0.437166E-02  -0.850828E-06   0.959060E-04  -0.291153E+00   0.380454E-05
  mo   20  -0.611181E-08   0.289888E-06   0.954596E-03   0.586018E-06  -0.229518E-06  -0.391137E-07  -0.203516E-06   0.219331E-01
  mo   21   0.551959E-08  -0.192482E-06  -0.650876E-06  -0.234887E-06   0.287335E-06  -0.291188E-06   0.167708E-06   0.447745E-06
  mo   22   0.914162E-05  -0.105775E-03   0.126862E-06  -0.546287E-03   0.127107E-05   0.337867E-05  -0.669993E-02  -0.665882E-07
  mo   23   0.101585E-04   0.904923E-03  -0.226945E-06  -0.986157E-04  -0.214844E-06  -0.209237E-04   0.593132E-01  -0.133637E-05
  mo   24  -0.829215E-08  -0.137993E-07   0.467042E-07   0.924748E-06  -0.150933E-03  -0.423684E-01  -0.181011E-04  -0.144083E-06

                no   9         no  10         no  11         no  12         no  13         no  14         no  15         no  16
  mo    1   0.229432E-03  -0.304313E-07  -0.994288E-04  -0.116814E-08   0.442888E-04   0.221173E-09  -0.594426E-07   0.201650E-04
  mo    2  -0.406320E-02   0.398549E-05  -0.316454E-03  -0.838506E-06   0.385672E-03   0.197299E-06  -0.686212E-05   0.104937E-02
  mo    3   0.164226E-05  -0.204323E-02   0.499542E-05   0.128506E-05   0.150431E-05  -0.303259E-06  -0.185520E-02  -0.365500E-05
  mo    4  -0.606886E-02   0.355043E-07   0.350238E-02   0.329340E-06   0.132499E-02  -0.197412E-05   0.119960E-04  -0.268691E-02
  mo    5  -0.158652E-05   0.174970E-05   0.212621E-05  -0.197954E-06  -0.307395E-06  -0.224196E-02  -0.878106E-06  -0.914046E-06
  mo    6  -0.256647E+00   0.380821E-04   0.189723E+00   0.277192E-05  -0.124533E+00  -0.316319E-05   0.269249E-03  -0.134496E+00
  mo    7  -0.105929E-04   0.219526E+00  -0.586691E-04   0.298118E-05   0.505200E-05  -0.610901E-06   0.401753E-01   0.945862E-04
  mo    8   0.710378E-05  -0.545500E+00   0.161598E-03  -0.391370E-05   0.468909E-05  -0.925072E-06   0.401163E+00   0.811177E-03
  mo    9  -0.167090E+00  -0.117962E-03  -0.366612E+00  -0.288907E-05   0.458989E-01   0.622680E-06  -0.805903E-03   0.401289E+00
  mo   10   0.767424E+00   0.947863E-04   0.319842E+00   0.184260E-05  -0.143880E+00  -0.616000E-05   0.596333E-03  -0.285118E+00
  mo   11   0.123395E-04  -0.590875E-05  -0.745205E-05  -0.640534E-05   0.312947E-05  -0.933530E-01   0.179704E-05   0.304187E-05
  mo   12  -0.710401E-05   0.638577E+00  -0.172926E-03   0.800743E-05  -0.187660E-05   0.326882E-06  -0.292647E+00  -0.597408E-03
  mo   13  -0.254979E+00   0.173962E-03   0.603248E+00   0.180648E-04   0.449643E+00   0.837437E-05  -0.140893E-03   0.569364E-01
  mo   14   0.320434E-05  -0.782968E-05  -0.131438E-04   0.753183E+00  -0.834156E-05  -0.142487E-04  -0.591943E-05   0.681740E-07
  mo   15  -0.604226E-05   0.149372E-05   0.956323E-07  -0.211609E-04   0.117762E-04  -0.653759E+00  -0.968390E-06  -0.395403E-06
  mo   16   0.468610E+00  -0.938040E-04  -0.363077E+00  -0.937048E-06   0.556142E+00   0.709812E-05  -0.600749E-03   0.282656E+00
  mo   17   0.114408E-04   0.192547E-01   0.972657E-05   0.211555E-05   0.167358E-05   0.393987E-06  -0.248151E+00  -0.529393E-03
  mo   18  -0.660876E-05   0.284944E+00  -0.927002E-04   0.260501E-05  -0.243173E-05   0.119911E-05   0.617372E+00   0.124999E-02
  mo   19   0.149803E+00   0.558212E-04   0.187195E+00  -0.130786E-05  -0.277901E+00  -0.602267E-05  -0.913305E-03   0.427520E+00
  mo   20   0.185016E-05  -0.143887E-05   0.146618E-06   0.145933E-04  -0.129198E-04   0.750919E+00  -0.188509E-05   0.157310E-05
  mo   21  -0.181407E-05   0.728511E-05   0.111660E-04  -0.657811E+00   0.931098E-05   0.222849E-04  -0.673849E-05   0.146967E-05
  mo   22   0.239825E-01   0.905417E-04   0.317526E+00   0.110229E-04   0.523785E+00   0.913501E-05  -0.224821E-03   0.136794E+00
  mo   23   0.979482E-01   0.891029E-04   0.309342E+00   0.187144E-05  -0.314052E+00  -0.704948E-05  -0.134034E-02   0.674500E+00
  mo   24   0.296775E-05  -0.406065E+00   0.120311E-03  -0.492867E-05   0.708560E-05  -0.142843E-05  -0.555946E+00  -0.112537E-02

                no  17         no  18         no  19         no  20         no  21         no  22         no  23         no  24
  mo    1  -0.169866E-08  -0.227311E-07   0.459623E-04   0.440515E-07   0.413070E-07  -0.369808E-04   0.267037E-04   0.392422E-08
  mo    2  -0.629055E-06  -0.315230E-05   0.265586E-02   0.222105E-05   0.491913E-05  -0.197614E-02   0.172436E-02  -0.809649E-07
  mo    3   0.762168E-06  -0.752691E-03   0.126638E-05   0.771292E-06  -0.174235E-02  -0.699667E-05   0.694981E-06   0.152716E-02
  mo    4   0.561665E-06   0.634750E-05   0.149299E-02   0.276942E-05  -0.547267E-05  -0.147112E-02   0.559692E-02   0.819879E-06
  mo    5   0.650432E-06   0.124352E-05   0.111755E-06   0.915477E-03  -0.130998E-06  -0.116753E-05   0.211250E-05   0.113668E-05
  mo    6  -0.576456E-05  -0.142147E-03   0.471641E+00   0.467179E-03   0.274471E-03  -0.205735E+00   0.559289E+00   0.503607E-04
  mo    7  -0.160621E-04   0.564870E+00   0.102400E-03  -0.560360E-05   0.182498E+00   0.516746E-04  -0.430767E-04   0.522218E+00
  mo    8   0.119557E-04  -0.186757E+00  -0.571915E-04   0.154307E-05   0.536690E-01   0.271424E-04   0.250643E-04  -0.313516E+00
  mo    9   0.152157E-05   0.628941E-05  -0.113106E-01  -0.113627E-04  -0.134994E-04   0.258841E-01  -0.470569E+00  -0.450579E-04
  mo   10   0.185968E-05   0.400868E-04  -0.547314E-01  -0.511615E-04  -0.392043E-04   0.104329E-01  -0.196103E+00  -0.140270E-04
  mo   11  -0.117193E-04  -0.387813E-05  -0.764962E-04   0.732216E-01   0.106908E-05   0.288928E-05  -0.654142E-05  -0.325372E-05
  mo   12   0.295508E-05  -0.146715E+00   0.737847E-06   0.330490E-05  -0.125106E+00  -0.583750E-04   0.467809E-04  -0.524622E+00
  mo   13   0.618520E-05   0.218060E-04  -0.329299E+00  -0.331585E-03   0.285332E-03  -0.465271E+00  -0.182943E+00  -0.223396E-04
  mo   14   0.657811E+00   0.219413E-04   0.106730E-04   0.833454E-04  -0.458912E-05   0.234860E-05   0.166307E-05   0.115668E-05
  mo   15  -0.837401E-04   0.448900E-05  -0.739555E-03   0.747662E+00   0.135840E-04  -0.767043E-05  -0.217344E-05   0.293556E-06
  mo   16  -0.178852E-05  -0.605904E-04   0.325256E-01   0.302580E-04   0.275593E-03  -0.331075E+00   0.385610E+00   0.294416E-04
  mo   17   0.142196E-04  -0.372708E+00  -0.327495E-03  -0.143230E-04   0.890416E+00   0.784918E-03   0.325634E-06   0.751570E-01
  mo   18  -0.496665E-05   0.387058E+00   0.148547E-04  -0.806196E-05   0.371162E+00   0.202453E-03   0.365241E-04  -0.420987E+00
  mo   19  -0.763529E-05  -0.214983E-03   0.583485E+00   0.571453E-03   0.480703E-03  -0.398627E+00  -0.313632E+00  -0.296763E-04
  mo   20  -0.842301E-04   0.440215E-05  -0.652525E-03   0.660029E+00   0.114205E-04  -0.746118E-05  -0.124946E-05  -0.313374E-07
  mo   21   0.753183E+00   0.229990E-04   0.101259E-04   0.853048E-04  -0.293250E-05   0.166597E-05   0.676244E-06   0.844933E-06
  mo   22  -0.903027E-05  -0.273818E-04   0.436088E+00   0.437632E-03  -0.403403E-03   0.640307E+00  -0.729863E-01   0.217982E-06
  mo   23   0.389952E-05   0.122697E-03  -0.366466E+00  -0.358789E-03  -0.285146E-03   0.248802E+00   0.376050E+00   0.278458E-04
  mo   24  -0.220828E-04   0.579476E+00   0.111452E-03  -0.691183E-05   0.132504E+00   0.743162E-04   0.413190E-04  -0.413386E+00
  eigenvectors of nos in ao-basis, column    1
  0.1000605E+01 -0.1377878E-01 -0.1683397E-02  0.3537814E-07  0.1769671E-06 -0.7406043E-02  0.9815775E-07 -0.5014941E-07
  0.1575578E-03  0.7560196E-08 -0.1082726E-07  0.4265992E-04  0.1622837E-07  0.9098149E-04 -0.3273580E-02  0.2156155E-02
 -0.1016393E-02  0.3681710E-08 -0.9380334E-03 -0.3273711E-02  0.2156289E-02  0.1016408E-02  0.1695143E-07 -0.9380807E-03
  eigenvectors of nos in ao-basis, column    2
  0.9633057E-02  0.9134611E+00 -0.7092001E-01 -0.2905293E-05 -0.3631427E-05  0.1255451E+00 -0.4990344E-05  0.1219579E-05
  0.7712999E-01 -0.3103200E-06  0.3927234E-06 -0.1214598E-02 -0.2158517E-06  0.1127129E-02  0.2162821E+00 -0.1059788E+00
  0.2690247E-01  0.3326721E-06  0.2157730E-01  0.2162920E+00 -0.1059854E+00 -0.2690435E-01 -0.6270028E-06  0.2157582E-01
  eigenvectors of nos in ao-basis, column    3
 -0.7414820E-06 -0.1907164E-05  0.2962819E-05  0.2592297E-05  0.9105815E+00  0.9875873E-05 -0.2281420E-05  0.8260584E-01
  0.1733884E-05 -0.6442652E-06 -0.1703787E-01 -0.8646456E-07 -0.1326811E-06 -0.2323147E-06 -0.6995139E-05  0.9599602E-05
  0.7277860E-06  0.3075042E-01 -0.2865826E-06 -0.2046743E-05  0.4552498E-06 -0.2661409E-06  0.3075149E-01 -0.4934508E-06
  eigenvectors of nos in ao-basis, column    4
  0.1162875E-02 -0.1227576E-02  0.2172912E+00  0.1430806E-03 -0.1008398E-04  0.8018205E+00 -0.2717885E-04  0.1861020E-06
 -0.2074629E-01 -0.1444831E-06  0.1187429E-05 -0.4742536E-02 -0.5037869E-05 -0.2394927E-02 -0.4303423E+00  0.1832919E+00
 -0.3822430E-01 -0.2554637E-05  0.2398867E-02 -0.4301666E+00  0.1832229E+00  0.3822556E-01 -0.1232412E-05  0.2428191E-02
  eigenvectors of nos in ao-basis, column    5
  0.3784670E-06  0.3153173E-05 -0.4487114E-04  0.7308025E+00  0.1820057E-05 -0.1321162E-03 -0.1188296E+00 -0.1822558E-05
  0.1879267E-06  0.4959465E-06 -0.9611067E-07  0.7387012E-06 -0.2544325E-01  0.7058132E-06 -0.5528114E+00  0.1931665E+00
 -0.2038005E-01 -0.1684264E-05 -0.3119424E-01  0.5529855E+00 -0.1932361E+00 -0.2039641E-01  0.9648948E-07  0.3118278E-01
  eigenvectors of nos in ao-basis, column    6
 -0.3927462E-04 -0.3066976E-03  0.5892850E-04 -0.1305961E+01  0.5010471E-05  0.4445708E-03  0.5791085E+00 -0.3089391E-05
 -0.2558648E-03  0.1154786E-06  0.2477455E-06  0.1621956E-05 -0.6475459E-01  0.1087131E-04 -0.1005357E+01  0.3579240E+00
 -0.3956540E-01 -0.3910582E-05 -0.1756049E-01  0.1006065E+01 -0.3582171E+00 -0.3959852E-01  0.8647951E-06  0.1753356E-01
  eigenvectors of nos in ao-basis, column    7
  0.1152536E+00  0.8570320E+00 -0.1710533E+00 -0.5050721E-03  0.1801789E-04 -0.1209568E+01  0.2460243E-03 -0.1630795E-04
  0.7533238E+00  0.5473753E-06  0.4549876E-06 -0.3930158E-02 -0.2287631E-04 -0.3133610E-01 -0.9749505E+00  0.4150200E+00
 -0.4957663E-01 -0.5473014E-05 -0.8901525E-02 -0.9742158E+00  0.4147512E+00  0.4955382E-01 -0.1687967E-05 -0.8844304E-02
  eigenvectors of nos in ao-basis, column    8
  0.3761106E-05  0.1851251E-04 -0.2018866E-04  0.1500106E-05  0.1412758E+01  0.2547855E-04  0.2217839E-05 -0.1610718E+01
 -0.2556812E-04 -0.1819157E-06  0.3712220E-01  0.1525204E-06  0.1415993E-05  0.7048170E-06  0.3225939E-04 -0.3356481E-04
 -0.1318854E-06 -0.7536805E-01  0.1709761E-05  0.2944825E-05 -0.3510739E-06  0.1076743E-05 -0.7537544E-01  0.1246211E-05
  eigenvectors of nos in ao-basis, column    9
 -0.4411301E+00 -0.2257708E+01  0.2144994E+01  0.4324966E-05  0.1635304E-04 -0.7477236E+00 -0.1876204E-04 -0.1791328E-04
  0.1099016E+01 -0.2358277E-05  0.2875501E-05 -0.1282048E-01 -0.6006611E-05  0.6045302E-01  0.3715558E+00 -0.2057027E+00
 -0.1290444E-01 -0.2381214E-05  0.7576661E-01  0.3716005E+00 -0.2057485E+00  0.1290670E-01 -0.5396209E-05  0.7578710E-01
  eigenvectors of nos in ao-basis, column   10
  0.7449440E-04  0.3270796E-03 -0.4672064E-03 -0.4930549E+00 -0.6831342E-05 -0.6101925E-05  0.1036043E+01  0.9660328E-05
  0.5886112E-04  0.8821048E-05 -0.1749539E-05 -0.2831717E-04  0.6170565E+00  0.7652803E-04  0.6562858E+00 -0.4996809E+00
  0.1119420E+00 -0.2007021E-05 -0.2192414E-01 -0.6558987E+00  0.4994605E+00  0.1119929E+00  0.3185740E-05  0.2205539E-01
  eigenvectors of nos in ao-basis, column   11
  0.2872563E+00  0.1281558E+01 -0.1767893E+01  0.1193399E-03 -0.8555771E-05 -0.4163646E-01 -0.2845174E-03  0.1260833E-04
  0.2217816E+00  0.1366749E-04  0.1317558E-06 -0.9802819E-01 -0.1852666E-03  0.2650680E+00  0.6449194E+00 -0.3817376E+00
 -0.6725274E-01 -0.4604296E-05  0.2204683E+00  0.6453219E+00 -0.3820402E+00  0.6719865E-01  0.4923538E-05  0.2204803E+00
  eigenvectors of nos in ao-basis, column   12
  0.5264793E-05  0.2510673E-04 -0.2716029E-04 -0.7532797E-05 -0.7689524E-05  0.5226307E-06  0.1063012E-04  0.1865891E-04
 -0.6306224E-05 -0.8024550E+00  0.1867427E-04 -0.4021828E-05  0.6960436E-05  0.1916698E-05 -0.7562116E-06 -0.8021141E-07
 -0.2151135E-05  0.2661330E+00  0.3612158E-05 -0.3319259E-05  0.3399986E-05  0.5943261E-05 -0.2661538E+00  0.6849052E-05
  eigenvectors of nos in ao-basis, column   13
 -0.1273523E+00 -0.5389414E+00  0.8851536E+00  0.1331547E-05  0.3247494E-05  0.9822728E-01 -0.2957593E-05 -0.8131022E-05
 -0.5117223E+00  0.1105934E-04 -0.1540075E-04 -0.2108145E+00 -0.1019570E-04 -0.2134217E+00 -0.4684831E+00  0.2699833E+00
 -0.1641633E+00  0.1657313E-05  0.9233556E-01 -0.4684858E+00  0.2699945E+00  0.1641695E+00  0.6008405E-05  0.9233317E-01
  eigenvectors of nos in ao-basis, column   14
  0.3422321E-06  0.1532122E-05 -0.4845597E-06  0.5125605E-06 -0.8448140E-01  0.4877648E-05 -0.6958105E-06  0.3036611E+00
 -0.1215690E-04  0.2573249E-04  0.8901890E+00 -0.3646161E-05  0.2146948E-05 -0.4636813E-05 -0.5148428E-05  0.5353849E-05
 -0.2025872E-05 -0.1992575E+00  0.7454189E-06 -0.4223397E-05  0.3844336E-05  0.2475653E-05 -0.1992549E+00  0.1464613E-05
  eigenvectors of nos in ao-basis, column   15
  0.7542792E-03  0.2879827E-02 -0.5585544E-02  0.5002648E+00  0.1777305E-05 -0.3246007E-03 -0.1294920E+01 -0.1907020E-05
  0.2441521E-02 -0.6462439E-05 -0.1808287E-05  0.4605218E-04  0.8072237E+00 -0.8253904E-03 -0.1401363E+01  0.7250237E+00
 -0.7381862E-01 -0.8161294E-05  0.2557638E+00  0.1410479E+01 -0.7301478E+00 -0.7410063E-01  0.5099048E-05 -0.2552453E+00
  eigenvectors of nos in ao-basis, column   16
 -0.3568780E+00 -0.1353145E+01  0.2704083E+01  0.1012863E-02  0.3485668E-05  0.1228031E+00 -0.2613785E-02 -0.5465228E-05
 -0.1152654E+01  0.1567113E-05  0.1711623E-05 -0.3012804E-01  0.1635509E-02  0.4146429E+00 -0.2255680E+01  0.1269512E+01
 -0.7472182E-01  0.9296108E-06 -0.1595866E+00 -0.2250020E+01  0.1266626E+01  0.7439290E-01 -0.2841913E-06 -0.1606560E+00
  eigenvectors of nos in ao-basis, column   17
  0.4077101E-05  0.1936872E-04 -0.7327483E-05 -0.2273055E-04 -0.1093013E-04 -0.9704543E-05 -0.1882726E-04  0.9682069E-04
 -0.3742367E-05  0.7228200E+00 -0.7465672E-04  0.2659340E-05  0.2632423E-04  0.7932943E-06 -0.9305153E-04  0.7665679E-04
 -0.2036482E-04  0.7383539E+00 -0.1993028E-04  0.5094482E-04 -0.4441152E-04  0.3787238E-05 -0.7385516E+00  0.2154301E-04
  eigenvectors of nos in ao-basis, column   18
  0.1049376E-03  0.4767564E-03 -0.1830952E-03  0.8972900E+00 -0.4592218E-05 -0.2766743E-03  0.1199655E+00  0.2533257E-05
  0.1075680E-03  0.2183279E-04  0.3880116E-05  0.8989164E-05 -0.6613143E+00  0.4792699E-04  0.1762260E+01 -0.1626662E+01
  0.3895112E+00  0.2909115E-04  0.6927694E+00 -0.1762953E+01  0.1627205E+01  0.3899593E+00 -0.1855253E-04 -0.6930608E+00
  eigenvectors of nos in ao-basis, column   19
 -0.2161317E+00 -0.1054029E+01 -0.1498804E+00  0.7471859E-04 -0.6298170E-04  0.7011003E+00  0.3601812E-03  0.7796607E-03
  0.3388259E+00  0.9481851E-05 -0.5638154E-03 -0.1263593E+00 -0.1273876E-03 -0.1179235E+00  0.1590040E+01 -0.1110076E+01
  0.9022239E+00 -0.8248052E-03  0.1550432E+00  0.1588974E+01 -0.1109304E+01 -0.9023930E+00 -0.8471714E-03  0.1544865E+00
  eigenvectors of nos in ao-basis, column   20
 -0.2118335E-03 -0.1033775E-02 -0.1482611E-03 -0.1663514E-04  0.5882196E-01  0.6830183E-03  0.2051135E-04 -0.7813145E+00
  0.3493956E-03  0.8072607E-04  0.5703622E+00 -0.1267321E-03  0.7956078E-05 -0.1145771E-03  0.1563020E-02 -0.1086566E-02
  0.8751353E-03  0.8453948E+00  0.1464367E-03  0.1580033E-02 -0.1112367E-02 -0.9067983E-03  0.8452154E+00  0.1406712E-03
  eigenvectors of nos in ao-basis, column   21
 -0.3014494E-03 -0.1295320E-02  0.9681288E-03  0.6125914E+00  0.5606761E-06  0.5930634E-03 -0.1257735E+01 -0.1358127E-04
 -0.7777661E-03 -0.2552850E-05  0.9769160E-05  0.1105108E-03 -0.1443829E+00 -0.1526168E-03 -0.3216081E+00 -0.9964585E-01
  0.7440686E+00  0.1085286E-04 -0.3574639E+00  0.3219847E+00  0.9910524E-01  0.7437030E+00  0.1938638E-04  0.3588277E+00
  eigenvectors of nos in ao-basis, column   22
  0.3001095E+00  0.1250868E+01 -0.1219857E+01  0.3463324E-03  0.3458850E-05 -0.4956180E+00 -0.9150862E-03  0.2019468E-05
  0.1036258E+01  0.1483619E-05 -0.6567556E-05 -0.1768844E+00 -0.9166562E-04  0.1592636E+00  0.2288999E+00  0.1940381E-01
  0.6752900E-01 -0.6722713E-05 -0.8046513E+00  0.2296188E+00  0.1922784E-01 -0.6641823E-01 -0.1121587E-04 -0.8038546E+00
  eigenvectors of nos in ao-basis, column   23
 -0.3251686E+00 -0.1123997E+01  0.3947515E+01  0.3460178E-04 -0.7143118E-05 -0.3606495E+00  0.1383594E-03  0.1287346E-04
 -0.9546939E+00  0.5107271E-06 -0.9692057E-06  0.2209133E-01 -0.5561076E-04  0.1876300E+00 -0.1195942E+01 -0.4587098E+00
 -0.7604201E+00 -0.7541014E-06 -0.5056795E+00 -0.1196125E+01 -0.4589045E+00  0.7605591E+00 -0.3552013E-05 -0.5057881E+00
  eigenvectors of nos in ao-basis, column   24
 -0.2516650E-04 -0.8766289E-04  0.3158416E-03 -0.3612111E+00 -0.3561741E-05 -0.3221514E-04 -0.1588442E+01  0.5401051E-05
 -0.6287385E-04  0.7568897E-06 -0.8624451E-07  0.2645580E-07  0.5574118E+00  0.1410230E-04 -0.9425276E+00 -0.1209157E+01
 -0.7218314E+00  0.1347383E-05 -0.6512112E+00  0.9423792E+00  0.1209043E+01 -0.7217115E+00 -0.8841262E-06  0.6511150E+00


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***


 cosurf_and_phi
  2.466315385316 -0.061539892486  2.837440149000 -0.037142361826
  0.006359863210 -2.199287675687  3.114544749440 -0.066437878980
 -0.480526931872  1.061509344774  3.755560521090 -0.068203883268
  1.262739748330  2.872870034186  1.567866969040 -0.044394321175
  1.897949994968 -2.634366215369  0.570962659525 -0.015548699052
 -2.790166444607 -0.824331206483  2.170430257018 -0.021168840407
 -2.061296060778  2.493528161233  1.034251636268 -0.021565606418
 -0.002991703587  2.420846177436 -1.447622679829  0.014653403960
 -1.114435211431 -2.949559801670 -0.067898604227 -0.015631771100
  1.955709175590 -0.984462672186  3.123501329999 -0.049647146832
  1.000900154469 -1.706149424342  3.300403738319 -0.063097606104
  1.652947872482  0.360731428865  3.496571379926 -0.056462240189
  0.594940552015  0.670555958619  3.845537291721 -0.067916339528
  2.390078334690  1.202722009900  2.566708449184 -0.036404383077
  1.989334673621  2.229216060677  2.001028520754 -0.038565769817
  2.919143888229  0.378144896597  2.099775822683 -0.016083462427
  2.780367109803 -0.921046401167  2.130496470151 -0.020891591184
  2.449941849009 -2.011800733381  1.439000205618 -0.019111271301
 -0.231540571447 -1.263621120083  3.706953994699 -0.068817109059
 -0.352106389130 -0.107618694495  3.950679056227 -0.069130865165
  0.105380882100  1.878884201281  3.371423292662 -0.067718772978
  0.783266721831  2.590884380442  2.520834408126 -0.059503023755
  2.011402526412  2.551496219550  0.814855178005 -0.018233662175
  1.440220538007 -2.843720084632  1.356653310131 -0.038953327172
  0.871209499702 -2.660587439486  2.372618950166 -0.057521650531
 -2.947488997374  0.298617382934  2.058332654596 -0.014484547823
 -2.674954162172  1.563027715274  1.704200253745 -0.017247559451
 -1.353448939749  2.910920816403  0.212056013565 -0.018509317438
 -0.743422400795  2.810699746106 -0.731960510989 -0.002886251329
  0.099626804209  1.855073908563 -1.945822518898  0.033428670637
  0.019900458911 -1.968682238442 -1.864952523830  0.030023416043
 -0.601526683996 -2.669374282549 -1.032942810695  0.004562411273
 -1.976701730993 -2.578422698078  0.816296596419 -0.019109503748
 -2.525639241426 -1.850752473194  1.593331825886 -0.019742735978
 -1.124962231191 -1.881571875050  3.121019991266 -0.060973421733
 -2.117398618237 -1.513942036836  2.667866282109 -0.043181800748
 -2.336046769995 -0.152318885910  2.976109389266 -0.041183774968
 -1.478775916646  0.469153703894  3.577445396081 -0.059172823284
 -1.328587214463  1.668007282102  3.174271253825 -0.059094912284
 -1.771761195387  2.287385908353  2.202255939279 -0.044867944420
 -1.026730149698  3.017318998523  1.358608629268 -0.044044469012
  0.119020599620  3.173161356543  1.415159765853 -0.049837846736
  0.915237473358  3.108118624501  0.463299650838 -0.029780898815
  0.462814589486  2.837210699514 -0.795516582628 -0.003719889837
  1.026812912753 -3.017319139618  0.083991013862 -0.020426343877
 -0.070059393274 -3.176418651270  0.035610954337 -0.025653012976
 -0.970268948715 -3.083313212042  1.062474740253 -0.040023902912
 -0.534203029846 -2.828286137107  2.231267851728 -0.058280743349
  0.871897873699 -0.575451888002  3.799144800425 -0.066056408520
  1.408719892640  1.640288870679  3.148120355694 -0.057980218278
  2.650053602975  1.633766196698  1.655441764425 -0.017146273152
  1.964286464189 -2.001027831890  2.365093852582 -0.043203418444
 -1.342877128538 -0.760711330777  3.581800927521 -0.060855070488
 -0.531365927285  2.661712102822  2.509437292848 -0.060872218486
  1.142904167226  2.885766749848 -0.243475252462 -0.010748656545
  0.342129526197 -3.189261637688  1.246854200824 -0.046937975477
 -2.346459445268  1.135413320199  2.662806558936 -0.038320597243
 -0.258565605057  3.205542092831  0.249849913336 -0.029788741788
  0.450566961335 -2.699565911226 -1.032013446782  0.003144882200
 -1.684533476150 -2.430522251364  2.070179818920 -0.044804373076
 -3.368784049130  0.036613212616 -1.854972615293  0.060490500075
 -3.278396927799  1.580834404538 -0.078603229040  0.033512746701
 -3.656110378332 -0.713812229513  0.361831391088  0.034257920855
 -2.408052717333 -2.075622947728 -1.226189024363  0.038752249316
 -1.295829484895 -0.567704086865 -2.747607986898  0.058493085739
 -1.846557165647  1.681693338816 -2.099716493181  0.048807864536
 -3.831862607217  0.357468890277 -0.654813288033  0.050566360289
 -3.527736967644 -1.045671231370 -1.064852892071  0.051738950298
 -2.622906831544 -1.024738705101 -2.241124222290  0.058228207103
 -2.402781990555  0.378472303440 -2.579763034114  0.061498634958
 -3.126606927219  1.313275381963 -1.541857021673  0.053286266133
 -3.633182667875  0.531520753705  0.561856665829  0.031184619214
 -3.086813000261 -1.794874586082 -0.179700355757  0.031587847834
 -1.615880090830 -1.674393887320 -2.147504235692  0.047990348057
 -1.240258025012  0.765881087348 -2.687977655831  0.057254264080
 -2.430292517958  2.154437082790 -0.969924007583  0.034305219506
  3.368776503197 -0.036639040867 -1.854966842961  0.060480564860
  3.278392620256 -1.580850766330 -0.078589062660  0.033506642872
  3.656106873706  0.713798214804  0.361832640492  0.034251854531
  2.408046317685  2.075600470298 -1.226192756897  0.038742940680
  1.295820311657  0.567673501678 -2.747601655947  0.058483536026
  1.846549173548 -1.681720471299 -2.099699178989  0.048798966670
  3.831857249217 -0.357488322766 -0.654806650060  0.050558570194
  3.527730862121  1.045649613724 -1.064853177123  0.051730170827
  2.622898581638  1.024710819001 -2.241122746235  0.058217606343
  2.402773123306 -0.378501994157 -2.579753678917  0.061487930036
  3.126599952113 -1.313299541573 -1.541844004398  0.053277140776
  3.633179527908 -0.531533702443  0.561864593522  0.031179403715
  3.086808508398  1.794857685487 -0.179703829578  0.031580375689
  1.615872011596  1.674366500124 -2.147504385867  0.047980448166
  1.240248960489 -0.765911354740 -2.687964116774  0.057245477099
  2.430286585511 -2.154458194501 -0.969905238283  0.034298005185
  0.000006852884  1.035694667087 -2.361676372823  0.051844269729
 -0.298806015181  0.969607486526 -2.408809074493  0.053313689583
  0.298815704890  0.969607820141 -2.408807386530  0.053310480089
  0.000007656756 -1.035723195601 -2.361670853435  0.051844973423
 -0.298805262603 -0.969636498141 -2.408803907288  0.053314413540
  0.298816457468 -0.969636367899 -2.408802219325  0.053311020383
 -0.667712940797 -1.245186997899 -2.338574529312  0.050855901453
 -1.218112188165 -2.025138759161 -1.616566947615  0.036095877469
 -2.084175601108 -2.294008802021 -0.480477682822  0.020556442981
 -2.876631896395 -1.658047550445  0.559052047065  0.017332468283
 -3.282909221331 -0.368099862162  1.091996180628  0.019506325549
 -3.142757910518  1.067034798172  0.908143333087  0.018464781108
 -2.511458431963  2.081290260960  0.080011352455  0.017578615715
 -1.638016880752  2.274609499719 -1.065756198713  0.026870801608
 -0.866948462969  1.570740798135 -2.077229430584  0.046726607828
 -0.467915446486 -1.694563900014 -1.941501402531  0.039573353078
 -1.299753513329 -2.453901457341 -0.850306853788  0.012659458359
 -2.242033801688 -2.245340287831  0.385760998463  0.000978439875
 -2.923088754321 -1.151144046328  1.279154738758  0.003023151714
 -3.074287016292  0.397098864814  1.477489335582  0.004645288786
 -2.635990824421  1.788708515315  0.902534843950  0.001107599115
 -1.781079179779  2.474786487342 -0.218927030025  0.004524427805
 -0.846758459860  2.184720175398 -1.444553386477  0.024920170816
 -0.255852300976  1.360631011730 -2.219692209228  0.046386688833
  0.667723897920  1.245157743773 -2.338577856151  0.050848901563
  1.218123388571  2.025110412626 -1.616576841774  0.036086941706
  2.084186643139  2.293984793080 -0.480493513306  0.020548540235
  2.876642520254  1.658030225134  0.559035126479  0.017326921098
  3.282919570196  0.368088739237  1.091983758387  0.019502534583
  3.142768358070 -1.067043033853  0.908139383421  0.018461420594
  2.511469270857 -2.081300494445  0.080016452498  0.017574243851
  1.638028059662 -2.274626132712 -1.065745290088  0.026864713235
  0.866959534092 -1.570765766224 -2.077218589767  0.046720135468
  0.467932162408  1.694535407938 -1.941510815499  0.039567965759
  1.299770347024  2.453875604722 -0.850324284820  0.012652337625
  2.242050270258  2.245320705184  0.385739526123  0.000972922838
  2.923104801922  1.151131828431  1.279135167181  0.003019845857
  3.074302957696 -0.397105856761  1.477477125466  0.004643348905
  2.636007061705 -1.788714946455  0.902532611959  0.001105755208
  1.781095866662 -2.474797662024 -0.218920775640  0.004521355217
  0.846775315928 -2.184739733041 -1.444543821640  0.024916373180
  0.255868904327 -1.360657816862 -2.219684436601  0.046385039308
  0.427045418766 -0.580992742084 -2.585105089006  0.056902829124
  0.427044967836  0.580963354323 -2.585108185092  0.056902553311
 -0.427035838172 -0.000014913339 -2.543076305315  0.062149828563
 end_of_phi


 nsubv after electrostatic potential =  5

  Confirmation of dielectric energy for ground state
  edielnew =  -0.0109648001

 =========== Executing IN-CORE method ==========
 norm multnew:  3.42253746E-07
 threx: total number of loops= 14
 threx: total number of loops= 24
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331914E+01-0.309165E+01-0.410226E+01-0.407883E+01-0.475210E+01-0.534847E+01-0.433670E+01-0.358739E+01-0.342330E+01-0.317821E+01
 -0.403664E+01-0.263325E+01-0.362338E+01-0.366481E+01-0.366999E+01-0.363955E+01-0.338751E+01-0.303510E+01-0.289433E+01-0.330517E+02
 -0.787312E+01-0.671622E+01-0.695563E+01-0.711691E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         2        65         3         2        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         3       129         4         3        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         4       193         5         4        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         5       257         6         5        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         6       321         7         6        12        64       128         6       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331914E+01-0.309165E+01-0.410226E+01-0.407883E+01-0.475210E+01-0.534847E+01-0.433670E+01-0.358739E+01-0.342330E+01-0.317821E+01
 -0.403664E+01-0.263325E+01-0.362338E+01-0.366481E+01-0.366999E+01-0.363955E+01-0.338751E+01-0.303510E+01-0.289433E+01-0.330517E+02
 -0.787312E+01-0.671622E+01-0.695563E+01-0.711691E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         2        65         3         2        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         3       129         4         3        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         4       193         5         4        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         5       257         6         5        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         6       321         7         6        12        64       128         6       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331914E+01-0.309165E+01-0.410226E+01-0.407883E+01-0.475210E+01-0.534847E+01-0.433670E+01-0.358739E+01-0.342330E+01-0.317821E+01
 -0.403664E+01-0.263325E+01-0.362338E+01-0.366481E+01-0.366999E+01-0.363955E+01-0.338751E+01-0.303510E+01-0.289433E+01-0.330517E+02
 -0.787312E+01-0.671622E+01-0.695563E+01-0.711691E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         2        65         3         2        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         3       129         4         3        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         4       193         5         4        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         5       257         6         5        12        64       128         6       380       380
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         6       321         7         6        12        64       128         6       380       380


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        40 xx:       135 ww:       255
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        20 yw:        25

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        39
task #   9:       127    task #  10:       247    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 matrix-vector product formed for vector   6
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference-subspace vector overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     1.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.97556855    -0.06381076    -0.00587878    -0.09791415     0.08942842    -0.16301843
 subspace has dimension  6

          (h-repnuc*1) matrix in the subspace basis

                x:   1         x:   2         x:   3         x:   4         x:   5         x:   6
   x:   1   -85.40090838
   x:   2    -0.25187978    -6.45464988
   x:   3    -0.00054635     0.22809435    -0.18307051
   x:   4     0.00011205    -0.04711533     0.00582665    -0.00958797
   x:   5    -0.00000093     0.00038994    -0.00326741     0.00090299    -0.00086133
   x:   6     0.00000079    -0.00033129     0.00007718    -0.00017983     0.00003662    -0.00002845

          overlap matrix in the subspace basis

                x:   1         x:   2         x:   3         x:   4         x:   5         x:   6
   x:   1     1.00000000
   x:   2     0.00000000     0.07914284
   x:   3     0.00000000    -0.00275501     0.00222409
   x:   4     0.00000000     0.00054875    -0.00007138     0.00011520
   x:   5     0.00000000    -0.00000454     0.00003817    -0.00001078     0.00001033
   x:   6     0.00000000     0.00000386    -0.00000090     0.00000210    -0.00000044     0.00000034

          eigenvectors and eigenvalues in the subspace basis

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6

   energy   -85.60538156   -83.87443829   -83.59722390   -82.22804176   -81.80935498   -80.53979418

   x:   1     0.97556855    -0.06381076    -0.00587878    -0.09791415     0.08942842    -0.16301843
   x:   2     0.79036371     0.40730393     0.02383086     1.24257277    -1.31340364     3.10273155
   x:   3     0.95004286     4.87124896     6.81692167     6.06933913    17.50199501     9.48862142
   x:   4     1.05453763    64.19152680    -4.65307799    60.23515372    -1.51717623   -55.65956388
   x:   5     0.94000781   104.81604779   257.61935522   -48.41081205  -161.20609607  -104.05044341
   x:   6     1.00869674   953.31218273  -147.11168998 -1480.61096566   -63.56626244   492.61575447

          eigenvectors and eigenvalues in the schmidt orthonormalized basis

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6

   energy   -85.60538156   -83.87443829   -83.59722390   -82.22804176   -81.80935498   -80.53979418

  zx:   1     0.97556855    -0.06381076    -0.00587878    -0.09791415     0.08942842    -0.16301843
  zx:   2     0.21509962     0.20347590    -0.07530398     0.38809567    -0.54211968     0.67981396
  zx:   3     0.04339028     0.22248235     0.53444583     0.19647109     0.67736840     0.40688783
  zx:   4     0.01038448     0.76230249    -0.31848641     0.38735897     0.12234165    -0.39030635
  zx:   5     0.00270232     0.23280201     0.77511657    -0.02274598    -0.47234961    -0.34836576
  zx:   6     0.00054952     0.51935103    -0.08014437    -0.80661597    -0.03463000     0.26837012

          <ref|baseci> overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     1.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

 trial vector basis is being transformed.  new dimension:   3

          transformed tciref transformation matrix  block   1

               ref   1        ref   2        ref   3
  ci:   1     0.97556855    -0.06381076    -0.00587878

  write the transformed vectors back to cosmofile

 rtflw =  0
 freezing =  F
 resid(rootcalc)   0.000314830969
 *******************************************
 ** Charges frozen from cosmo calculation **
 *******************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  6  1    -76.2524187801 -9.3530E+00  3.1868E-08  3.1483E-04  1.0000E-03
 mr-sdci #  6  2    -74.5214755096 -9.0880E+00  0.0000E+00  8.4093E-01  1.0000E-04
 mr-sdci #  6  3    -74.2442611213 -9.1447E+00  0.0000E+00  1.2960E+00  1.0000E-04
 mr-sdci #  6  4    -72.8750789756 -8.9350E+00  0.0000E+00  1.8591E+00  1.0000E-04
 mr-sdci #  6  5    -72.4563921953 -8.2251E+00  0.0000E+00  1.2543E+00  1.0000E-04
 mr-sdci #  6  6    -71.1868314024  7.1187E+01  0.0000E+00  2.1935E+00  1.0000E-04


 total energy in cosmo calc 
e(rootcalc) + repnuc=     -76.2524187801
dielectric energy =      -0.0104563943
deltaediel =       0.0104563943
e(rootcalc) + repnuc - ediel =     -76.2419623859
e(rootcalc) + repnuc - edielnew =     -76.2524187801
deltaelast =      76.2419623859


 mr-sdci  convergence criteria satisfied after  6 iterations.

 *************************************************
 ***Final Calc of density matrix for cosmo calc***
 *************************************************

 entering drivercid: firstcall,firstnonref= F T
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=      10  D0X=      30  D0W=      50
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================

          ci-one-electron density block   1

                mo   1         mo   2         mo   3         mo   4         mo   5         mo   6         mo   7         mo   8
   mo   1     1.99991404
   mo   2    -0.00021064     1.98510661
   mo   3     0.00000000    -0.00000004     1.96965876
   mo   4    -0.00015604     0.00416185    -0.00000004     1.97325737
   mo   5     0.00000000    -0.00000007    -0.00000001    -0.00000007     1.97621175
   mo   6     0.00012065    -0.00415549    -0.00001597    -0.01965435    -0.00000768     0.00700164
   mo   7     0.00000000     0.00000670    -0.00981642    -0.00002015    -0.00000474    -0.00000008     0.00754298
   mo   8    -0.00000001     0.00001411    -0.01189457    -0.00001854     0.00000222    -0.00000022     0.00721401     0.01063697
   mo   9     0.00005756     0.00295044    -0.00000982    -0.00013238     0.00000171     0.00713781    -0.00000022    -0.00000032
   mo  10    -0.00021473     0.01030164    -0.00001285     0.00324923     0.00000015     0.00264746    -0.00000019    -0.00000012
   mo  11     0.00000004    -0.00000158     0.00000280    -0.00000043    -0.01153500     0.00000020     0.00000009    -0.00000009
   mo  12    -0.00000001    -0.00000289    -0.00600085    -0.00000577    -0.00000233     0.00000006     0.00613512     0.00410623
   mo  13     0.00020188    -0.00114346    -0.00001160    -0.00746018    -0.00000288     0.00195963    -0.00000006    -0.00000019
   mo  14     0.00000000     0.00000203    -0.00000287    -0.00000107    -0.00000081    -0.00000003    -0.00000003    -0.00000002
   mo  15     0.00000000     0.00000097    -0.00000178    -0.00000448    -0.00298356     0.00000002    -0.00000001    -0.00000001
   mo  16    -0.00038703    -0.00026300    -0.00000435     0.00284697     0.00000152    -0.00195367    -0.00000010    -0.00000004
   mo  17    -0.00000001    -0.00000809     0.00094487     0.00001960     0.00000045     0.00000000     0.00027505     0.00024696
   mo  18     0.00000003    -0.00000045     0.01165625    -0.00000056     0.00000087    -0.00000010    -0.00286912    -0.00439979
   mo  19    -0.00010843    -0.00448197    -0.00000019     0.00759335     0.00000041    -0.00312249    -0.00000012    -0.00000007
   mo  20     0.00000000     0.00000019    -0.00000045     0.00000129     0.00190374    -0.00000003     0.00000000     0.00000000
   mo  21     0.00000000    -0.00000022     0.00000056    -0.00000056    -0.00000129     0.00000001    -0.00000001    -0.00000002
   mo  22     0.00001080     0.00012865     0.00000232    -0.00110883     0.00000027    -0.00006546     0.00000003     0.00000003
   mo  23     0.00005142     0.00172134    -0.00000045     0.00029035    -0.00000045     0.00064386     0.00000000    -0.00000002
   mo  24    -0.00000001    -0.00000060    -0.00025118     0.00000177     0.00000009     0.00000000    -0.00083647     0.00033813

                mo   9         mo  10         mo  11         mo  12         mo  13         mo  14         mo  15         mo  16
   mo   9     0.01014361
   mo  10     0.00335955     0.01010219
   mo  11    -0.00000005    -0.00000002     0.01451278
   mo  12     0.00000004     0.00000003     0.00000002     0.00653210
   mo  13     0.00054166    -0.00063933     0.00000006    -0.00000002     0.00372655
   mo  14    -0.00000003     0.00000000     0.00000006    -0.00000002     0.00000000     0.00297208
   mo  15    -0.00000003    -0.00000006    -0.00139325    -0.00000001     0.00000002    -0.00000005     0.00224105
   mo  16     0.00002222     0.00264944    -0.00000003    -0.00000007    -0.00125313     0.00000001    -0.00000002     0.00439067
   mo  17     0.00000019     0.00000025    -0.00000001     0.00035879    -0.00000002     0.00000000     0.00000000     0.00000006
   mo  18    -0.00000013    -0.00000016     0.00000001    -0.00184547     0.00000001     0.00000000     0.00000000    -0.00000003
   mo  19    -0.00438184    -0.00082804     0.00000000    -0.00000012    -0.00084078     0.00000000    -0.00000001    -0.00005817
   mo  20    -0.00000001     0.00000000     0.00004145     0.00000000    -0.00000001     0.00000000    -0.00182346     0.00000000
   mo  21     0.00000000    -0.00000001     0.00000002     0.00000000     0.00000000    -0.00202199     0.00000000    -0.00000001
   mo  22    -0.00056971     0.00029399    -0.00000001     0.00000002     0.00172039     0.00000000     0.00000000     0.00070203
   mo  23     0.00024704     0.00177886     0.00000000     0.00000003     0.00025137     0.00000000     0.00000000    -0.00068746
   mo  24     0.00000001     0.00000000     0.00000001    -0.00169038     0.00000001     0.00000000     0.00000000     0.00000002

                mo  17         mo  18         mo  19         mo  20         mo  21         mo  22         mo  23         mo  24
   mo  17     0.00051302
   mo  18    -0.00020367     0.00262042
   mo  19     0.00000001     0.00000005     0.00288190
   mo  20     0.00000000     0.00000000     0.00000001     0.00254997
   mo  21     0.00000000     0.00000001     0.00000000     0.00000001     0.00242271
   mo  22    -0.00000001     0.00000001    -0.00015927     0.00000000     0.00000000     0.00197256
   mo  23     0.00000003    -0.00000002     0.00061123     0.00000000     0.00000000    -0.00008866     0.00163927
   mo  24     0.00000313    -0.00057333     0.00000002     0.00000000     0.00000000    -0.00000001    -0.00000001     0.00144901


*****   symmetry block SYM1   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       1.99991404
       2      -0.00021064     1.98510661
       3       0.00000000    -0.00000004     1.96965876
       4      -0.00015604     0.00416185    -0.00000004     1.97325737
       5       0.00000000    -0.00000007    -0.00000001    -0.00000007
       6       0.00012065    -0.00415549    -0.00001597    -0.01965435
       7       0.00000000     0.00000670    -0.00981642    -0.00002015
       8      -0.00000001     0.00001411    -0.01189457    -0.00001854
       9       0.00005756     0.00295044    -0.00000982    -0.00013238
      10      -0.00021473     0.01030164    -0.00001285     0.00324923
      11       0.00000004    -0.00000158     0.00000280    -0.00000043
      12      -0.00000001    -0.00000289    -0.00600085    -0.00000577
      13       0.00020188    -0.00114346    -0.00001160    -0.00746018
      14       0.00000000     0.00000203    -0.00000287    -0.00000107
      15       0.00000000     0.00000097    -0.00000178    -0.00000448
      16      -0.00038703    -0.00026300    -0.00000435     0.00284697
      17      -0.00000001    -0.00000809     0.00094487     0.00001960
      18       0.00000003    -0.00000045     0.01165625    -0.00000056
      19      -0.00010843    -0.00448197    -0.00000019     0.00759335
      20       0.00000000     0.00000019    -0.00000045     0.00000129
      21       0.00000000    -0.00000022     0.00000056    -0.00000056
      22       0.00001080     0.00012865     0.00000232    -0.00110883
      23       0.00005142     0.00172134    -0.00000045     0.00029035
      24      -0.00000001    -0.00000060    -0.00025118     0.00000177

               Column   5     Column   6     Column   7     Column   8
       5       1.97621175
       6      -0.00000768     0.00700164
       7      -0.00000474    -0.00000008     0.00754298
       8       0.00000222    -0.00000022     0.00721401     0.01063697
       9       0.00000171     0.00713781    -0.00000022    -0.00000032
      10       0.00000015     0.00264746    -0.00000019    -0.00000012
      11      -0.01153500     0.00000020     0.00000009    -0.00000009
      12      -0.00000233     0.00000006     0.00613512     0.00410623
      13      -0.00000288     0.00195963    -0.00000006    -0.00000019
      14      -0.00000081    -0.00000003    -0.00000003    -0.00000002
      15      -0.00298356     0.00000002    -0.00000001    -0.00000001
      16       0.00000152    -0.00195367    -0.00000010    -0.00000004
      17       0.00000045     0.00000000     0.00027505     0.00024696
      18       0.00000087    -0.00000010    -0.00286912    -0.00439979
      19       0.00000041    -0.00312249    -0.00000012    -0.00000007
      20       0.00190374    -0.00000003     0.00000000     0.00000000
      21      -0.00000129     0.00000001    -0.00000001    -0.00000002
      22       0.00000027    -0.00006546     0.00000003     0.00000003
      23      -0.00000045     0.00064386     0.00000000    -0.00000002
      24       0.00000009     0.00000000    -0.00083647     0.00033813

               Column   9     Column  10     Column  11     Column  12
       9       0.01014361
      10       0.00335955     0.01010219
      11      -0.00000005    -0.00000002     0.01451278
      12       0.00000004     0.00000003     0.00000002     0.00653210
      13       0.00054166    -0.00063933     0.00000006    -0.00000002
      14      -0.00000003     0.00000000     0.00000006    -0.00000002
      15      -0.00000003    -0.00000006    -0.00139325    -0.00000001
      16       0.00002222     0.00264944    -0.00000003    -0.00000007
      17       0.00000019     0.00000025    -0.00000001     0.00035879
      18      -0.00000013    -0.00000016     0.00000001    -0.00184547
      19      -0.00438184    -0.00082804     0.00000000    -0.00000012
      20      -0.00000001     0.00000000     0.00004145     0.00000000
      21       0.00000000    -0.00000001     0.00000002     0.00000000
      22      -0.00056971     0.00029399    -0.00000001     0.00000002
      23       0.00024704     0.00177886     0.00000000     0.00000003
      24       0.00000001     0.00000000     0.00000001    -0.00169038

               Column  13     Column  14     Column  15     Column  16
      13       0.00372655
      14       0.00000000     0.00297208
      15       0.00000002    -0.00000005     0.00224105
      16      -0.00125313     0.00000001    -0.00000002     0.00439067
      17      -0.00000002     0.00000000     0.00000000     0.00000006
      18       0.00000001     0.00000000     0.00000000    -0.00000003
      19      -0.00084078     0.00000000    -0.00000001    -0.00005817
      20      -0.00000001     0.00000000    -0.00182346     0.00000000
      21       0.00000000    -0.00202199     0.00000000    -0.00000001
      22       0.00172039     0.00000000     0.00000000     0.00070203
      23       0.00025137     0.00000000     0.00000000    -0.00068746
      24       0.00000001     0.00000000     0.00000000     0.00000002

               Column  17     Column  18     Column  19     Column  20
      17       0.00051302
      18      -0.00020367     0.00262042
      19       0.00000001     0.00000005     0.00288190
      20       0.00000000     0.00000000     0.00000001     0.00254997
      21       0.00000000     0.00000001     0.00000000     0.00000001
      22      -0.00000001     0.00000001    -0.00015927     0.00000000
      23       0.00000003    -0.00000002     0.00061123     0.00000000
      24       0.00000313    -0.00057333     0.00000002     0.00000000

               Column  21     Column  22     Column  23     Column  24
      21       0.00242271
      22       0.00000000     0.00197256
      23       0.00000000    -0.00008866     0.00163927
      24       0.00000000    -0.00000001    -0.00000001     0.00144901

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7        eno   8
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    8   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno   9        eno  10        eno  11        eno  12        eno  13        eno  14        eno  15        eno  16
 emo    9   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   10   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   11   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   12   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   13   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   14   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   15   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   16   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno  17        eno  18        eno  19        eno  20        eno  21        eno  22        eno  23        eno  24
 emo   17   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   18   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   19   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   20   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   21   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   22   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   23   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   24   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   1.9999191      1.9865397      1.9762859      1.9721629      1.9698693
   0.0216017      0.0198394      0.0146115      0.0103122      0.0054171
   0.0051611      0.0047380      0.0042170      0.0041266      0.0010148
   0.0010105      0.0006568      0.0005694      0.0004933      0.0004915
   0.0004478      0.0004309      0.0000503      0.0000331

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7         no   8
  mo    1   0.999820E+00   0.187194E-01  -0.529038E-07   0.311985E-02  -0.564122E-06   0.225100E-07  -0.350653E-04  -0.215385E-07
  mo    2  -0.168540E-01   0.951437E+00  -0.929856E-06  -0.307318E+00   0.608092E-04  -0.492746E-05  -0.268091E-02   0.959633E-06
  mo    3   0.775370E-07  -0.457807E-05   0.386584E-06   0.183417E-03   0.999946E+00   0.971480E-02   0.146023E-04  -0.155668E-05
  mo    4  -0.871870E-02   0.307175E+00   0.117477E-04   0.951543E+00  -0.173344E-03   0.105149E-04   0.622229E-02   0.172804E-07
  mo    5   0.141005E-06  -0.275262E-05   0.999981E+00  -0.115104E-04  -0.389000E-06   0.131272E-05   0.175299E-05   0.564057E-02
  mo    6   0.181817E-03  -0.503386E-02  -0.401571E-05  -0.888069E-02  -0.655611E-05  -0.210305E-03   0.536377E+00   0.491807E-05
  mo    7   0.312451E-07   0.124600E-06  -0.240798E-05  -0.117784E-04  -0.504286E-02   0.570245E+00   0.209870E-03   0.377308E-05
  mo    8  -0.422068E-07   0.393682E-05   0.111262E-05  -0.123679E-04  -0.610907E-02   0.636986E+00   0.224559E-03  -0.763729E-05
  mo    9   0.495603E-05   0.139324E-02   0.851682E-06  -0.569225E-03  -0.494212E-05  -0.249732E-03   0.672591E+00  -0.128745E-04
  mo   10  -0.209426E-03   0.545985E-02   0.886168E-07  -0.500705E-04  -0.654142E-05  -0.152428E-03   0.405592E+00  -0.126887E-04
  mo   11   0.332790E-07  -0.813174E-06  -0.587868E-02   0.109641E-06   0.143310E-05   0.261860E-05   0.114301E-04   0.992942E+00
  mo   12   0.453798E-07  -0.226043E-05  -0.119132E-05  -0.296284E-05  -0.309022E-02   0.440238E+00   0.184069E-03   0.818854E-06
  mo   13   0.143755E-03  -0.170863E-02  -0.150982E-05  -0.343968E-02  -0.527746E-05  -0.494449E-04   0.848040E-01   0.511813E-05
  mo   14  -0.126875E-07   0.805658E-06  -0.411737E-06  -0.833746E-06  -0.145829E-05  -0.330194E-05  -0.282962E-05   0.507796E-05
  mo   15   0.104940E-07  -0.227009E-06  -0.150811E-02  -0.230006E-05  -0.906483E-06  -0.200975E-05  -0.465489E-05  -0.116421E+00
  mo   16  -0.204674E-03   0.324342E-03   0.791440E-06   0.142780E-02  -0.246933E-05  -0.200825E-05  -0.484259E-02  -0.662205E-05
  mo   17  -0.238756E-07  -0.844000E-06   0.227423E-06   0.108028E-04   0.477110E-03   0.254206E-01   0.302065E-04  -0.601032E-06
  mo   18   0.222521E-07  -0.337706E-06   0.444308E-06   0.930113E-06   0.594874E-02  -0.269677E+00  -0.114621E-03   0.173465E-05
  mo   19  -0.498733E-04  -0.971336E-03   0.261786E-06   0.438512E-02  -0.875489E-06   0.975277E-04  -0.290864E+00   0.382664E-05
  mo   20  -0.609090E-08   0.290212E-06   0.965790E-03   0.586592E-06  -0.229482E-06  -0.400913E-07  -0.214225E-06   0.219029E-01
  mo   21   0.551405E-08  -0.192774E-06  -0.652947E-06  -0.234972E-06   0.288683E-06  -0.295635E-06   0.169115E-06   0.473122E-06
  mo   22   0.917369E-05  -0.110599E-03   0.127557E-06  -0.557971E-03   0.127390E-05   0.347481E-05  -0.686912E-02  -0.634935E-07
  mo   23   0.988877E-05   0.873331E-03  -0.229713E-06  -0.130740E-03  -0.209489E-06  -0.212717E-04   0.592116E-01  -0.133121E-05
  mo   24  -0.847150E-08  -0.108312E-07   0.467720E-07   0.931425E-06  -0.125582E-03  -0.423527E-01  -0.183290E-04  -0.143861E-06

                no   9         no  10         no  11         no  12         no  13         no  14         no  15         no  16
  mo    1   0.224751E-03  -0.296196E-07  -0.974198E-04  -0.115126E-08   0.434493E-04   0.150398E-09  -0.513523E-07   0.199563E-04
  mo    2  -0.411338E-02   0.399111E-05  -0.290217E-03  -0.838367E-06   0.357476E-03   0.197173E-06  -0.646146E-05   0.105047E-02
  mo    3   0.165409E-05  -0.200325E-02   0.498857E-05   0.128736E-05   0.150980E-05  -0.303098E-06  -0.188366E-02  -0.299364E-05
  mo    4  -0.605975E-02   0.537047E-07   0.357343E-02   0.330068E-06   0.130862E-02  -0.197391E-05   0.110324E-04  -0.270873E-02
  mo    5  -0.159878E-05   0.175519E-05   0.214205E-05  -0.201167E-06  -0.321700E-06  -0.225915E-02  -0.873085E-06  -0.921643E-06
  mo    6  -0.255525E+00   0.376600E-04   0.190264E+00   0.278969E-05  -0.124569E+00  -0.308656E-05   0.218403E-03  -0.134620E+00
  mo    7  -0.106108E-04   0.219612E+00  -0.579077E-04   0.297307E-05   0.509378E-05  -0.613578E-06   0.421513E-01   0.824803E-04
  mo    8   0.706001E-05  -0.545394E+00   0.159861E-03  -0.390366E-05   0.453378E-05  -0.905353E-06   0.400210E+00   0.657174E-03
  mo    9  -0.167436E+00  -0.116501E-03  -0.366178E+00  -0.291403E-05   0.459723E-01   0.576925E-06  -0.653066E-03   0.401136E+00
  mo   10   0.767845E+00   0.937117E-04   0.319457E+00   0.187081E-05  -0.143566E+00  -0.600250E-05   0.487779E-03  -0.285006E+00
  mo   11   0.121673E-04  -0.591117E-05  -0.741378E-05  -0.627799E-05   0.306730E-05  -0.932044E-01   0.177463E-05   0.300531E-05
  mo   12  -0.708699E-05   0.638896E+00  -0.170865E-03   0.796547E-05  -0.180389E-05   0.327842E-06  -0.293338E+00  -0.487357E-03
  mo   13  -0.254905E+00   0.171881E-03   0.603524E+00   0.180177E-04   0.449849E+00   0.824773E-05  -0.119839E-03   0.574649E-01
  mo   14   0.319079E-05  -0.780244E-05  -0.131638E-04   0.753197E+00  -0.826333E-05  -0.138638E-04  -0.585982E-05   0.756057E-07
  mo   15  -0.602907E-05   0.147752E-05   0.223059E-06  -0.207043E-04   0.113940E-04  -0.653763E+00  -0.956261E-06  -0.407367E-06
  mo   16   0.468386E+00  -0.929439E-04  -0.363478E+00  -0.101494E-05   0.556404E+00   0.670641E-05  -0.493476E-03   0.282879E+00
  mo   17   0.114217E-04   0.191298E-01   0.957809E-05   0.208685E-05   0.151029E-05   0.389022E-06  -0.247984E+00  -0.434498E-03
  mo   18  -0.661245E-05   0.284597E+00  -0.916462E-04   0.257662E-05  -0.222397E-05   0.119426E-05   0.619169E+00   0.101829E-02
  mo   19   0.149601E+00   0.552068E-04   0.187106E+00  -0.127753E-05  -0.277937E+00  -0.584046E-05  -0.751348E-03   0.427955E+00
  mo   20   0.184397E-05  -0.142831E-05   0.274572E-07   0.141873E-04  -0.124924E-04   0.750934E+00  -0.187467E-05   0.153546E-05
  mo   21  -0.180886E-05   0.723349E-05   0.111807E-04  -0.657795E+00   0.922439E-05   0.217896E-04  -0.668630E-05   0.145414E-05
  mo   22   0.241938E-01   0.894150E-04   0.317382E+00   0.109729E-04   0.523513E+00   0.887437E-05  -0.172276E-03   0.136023E+00
  mo   23   0.985128E-01   0.881474E-04   0.309113E+00   0.190537E-05  -0.313834E+00  -0.679885E-05  -0.108370E-02   0.674357E+00
  mo   24   0.297177E-05  -0.405909E+00   0.118883E-03  -0.489850E-05   0.688746E-05  -0.141641E-05  -0.554198E+00  -0.911212E-03

                no  17         no  18         no  19         no  20         no  21         no  22         no  23         no  24
  mo    1  -0.168638E-08  -0.223968E-07   0.450501E-04   0.202305E-07   0.393818E-07  -0.357238E-04   0.259224E-04   0.372580E-08
  mo    2  -0.629752E-06  -0.315885E-05   0.273258E-02   0.906113E-06   0.489183E-05  -0.196298E-02   0.179206E-02  -0.682787E-07
  mo    3   0.763245E-06  -0.795563E-03   0.128896E-05   0.770495E-06  -0.175298E-02  -0.696134E-05   0.679760E-06   0.151229E-02
  mo    4   0.561171E-06   0.632872E-05   0.156117E-02   0.203962E-05  -0.547243E-05  -0.147512E-02   0.571883E-02   0.853476E-06
  mo    5   0.649784E-06   0.126021E-05   0.624979E-06   0.919919E-03  -0.125119E-06  -0.117358E-05   0.215292E-05   0.115234E-05
  mo    6  -0.575316E-05  -0.144441E-03   0.473914E+00   0.228200E-03   0.265773E-03  -0.201507E+00   0.558446E+00   0.497935E-04
  mo    7  -0.160655E-04   0.564689E+00   0.107123E-03  -0.542657E-05   0.181762E+00   0.500060E-04  -0.423627E-04   0.522377E+00
  mo    8   0.119261E-04  -0.187951E+00  -0.574359E-04   0.149133E-05   0.531694E-01   0.261405E-04   0.246539E-04  -0.313745E+00
  mo    9   0.150926E-05   0.682814E-05  -0.123193E-01  -0.605740E-05  -0.141538E-04   0.269184E-01  -0.470742E+00  -0.444505E-04
  mo   10   0.185494E-05   0.404105E-04  -0.556648E-01  -0.237732E-04  -0.382770E-04   0.984911E-02  -0.196952E+00  -0.138318E-04
  mo   11  -0.116088E-04  -0.389646E-05  -0.391516E-04   0.731011E-01   0.107609E-05   0.283266E-05  -0.640904E-05  -0.321303E-05
  mo   12   0.297384E-05  -0.145861E+00  -0.108587E-05   0.325248E-05  -0.124285E+00  -0.568859E-04   0.462061E-04  -0.524617E+00
  mo   13   0.609347E-05   0.229839E-04  -0.324598E+00  -0.161741E-03   0.284754E-03  -0.468034E+00  -0.183002E+00  -0.219481E-04
  mo   14   0.657795E+00   0.219206E-04   0.106448E-04   0.824677E-04  -0.447444E-05   0.235107E-05   0.163603E-05   0.114600E-05
  mo   15  -0.829056E-04   0.414568E-05  -0.359013E-03   0.747688E+00   0.136708E-04  -0.767122E-05  -0.219502E-05   0.309531E-06
  mo   16  -0.181462E-05  -0.610355E-04   0.365208E-01   0.156341E-04   0.271645E-03  -0.330927E+00   0.384733E+00   0.290204E-04
  mo   17   0.140599E-04  -0.371145E+00  -0.323872E-03  -0.144512E-04   0.891101E+00   0.772209E-03   0.671831E-06   0.753696E-01
  mo   18  -0.500364E-05   0.385681E+00   0.200468E-04  -0.789340E-05   0.370123E+00   0.197840E-03   0.359591E-04  -0.420869E+00
  mo   19  -0.765597E-05  -0.217490E-03   0.587397E+00   0.276404E-03   0.467181E-03  -0.391904E+00  -0.314595E+00  -0.290813E-04
  mo   20  -0.833276E-04   0.410264E-05  -0.316504E-03   0.660013E+00   0.115024E-04  -0.746169E-05  -0.120608E-05   0.359733E-08
  mo   21   0.753197E+00   0.230039E-04   0.101051E-04   0.844308E-04  -0.283805E-05   0.167501E-05   0.652521E-06   0.852004E-06
  mo   22  -0.890477E-05  -0.289314E-04   0.429537E+00   0.212563E-03  -0.403018E-03   0.645208E+00  -0.726117E-01   0.138964E-06
  mo   23   0.391915E-05   0.124479E-03  -0.368659E+00  -0.173324E-03  -0.277322E-03   0.245185E+00   0.376778E+00   0.272806E-04
  mo   24  -0.221064E-04   0.581400E+00   0.115528E-03  -0.670898E-05   0.132788E+00   0.730247E-04   0.408441E-04  -0.413097E+00
  eigenvectors of nos in ao-basis, column    1
  0.1000601E+01 -0.1377804E-01 -0.1662908E-02  0.3245223E-07  0.1757237E-06 -0.7407348E-02  0.9790173E-07 -0.5020448E-07
  0.1598599E-03  0.7548227E-08 -0.1077275E-07  0.4259922E-04  0.1643250E-07  0.9087801E-04 -0.3267292E-02  0.2149286E-02
 -0.1015572E-02  0.3638948E-08 -0.9375905E-03 -0.3267423E-02  0.2149422E-02  0.1015587E-02  0.1684873E-07 -0.9376378E-03
  eigenvectors of nos in ao-basis, column    2
  0.9613130E-02  0.9133742E+00 -0.7093028E-01 -0.2908949E-05 -0.3658150E-05  0.1262520E+00 -0.4966643E-05  0.1213515E-05
  0.7713904E-01 -0.3105693E-06  0.3936961E-06 -0.1217897E-02 -0.2181361E-06  0.1108058E-02  0.2160614E+00 -0.1057495E+00
  0.2689749E-01  0.3311061E-06  0.2160456E-01  0.2160712E+00 -0.1057561E+00 -0.2689936E-01 -0.6276678E-06  0.2160309E-01
  eigenvectors of nos in ao-basis, column    3
 -0.7331143E-06 -0.1883919E-05  0.2920083E-05  0.2678192E-05  0.9106445E+00  0.1005922E-04 -0.2276159E-05  0.8254139E-01
  0.1744943E-05 -0.6470619E-06 -0.1702301E-01 -0.8810187E-07 -0.1361925E-06 -0.2351638E-06 -0.7127024E-05  0.9753982E-05
  0.7198153E-06  0.3073906E-01 -0.2843523E-06 -0.2066202E-05  0.4911864E-06 -0.2607988E-06  0.3074012E-01 -0.4873248E-06
  eigenvectors of nos in ao-basis, column    4
  0.1178538E-02 -0.1864664E-02  0.2170848E+00  0.1460466E-03 -0.1026354E-04  0.8017902E+00 -0.2761290E-04  0.1649640E-06
 -0.2079127E-01 -0.1444721E-06  0.1191195E-05 -0.4737852E-02 -0.5147203E-05 -0.2415996E-02 -0.4304921E+00  0.1834880E+00
 -0.3819656E-01 -0.2558966E-05  0.2404615E-02 -0.4303120E+00  0.1834173E+00  0.3819765E-01 -0.1235601E-05  0.2434169E-02
  eigenvectors of nos in ao-basis, column    5
  0.3741588E-06  0.3319236E-05 -0.4569826E-04  0.7308666E+00  0.1721572E-05 -0.1353268E-03 -0.1189211E+00 -0.1829870E-05
  0.2965081E-06  0.4975460E-06 -0.9424214E-07  0.7572541E-06 -0.2547355E-01  0.7173900E-06 -0.5528036E+00  0.1931091E+00
 -0.2036866E-01 -0.1687338E-05 -0.3117050E-01  0.5529811E+00 -0.1931803E+00 -0.2038531E-01  0.9402308E-07  0.3115901E-01
  eigenvectors of nos in ao-basis, column    6
 -0.3987785E-04 -0.3116654E-03  0.5918306E-04 -0.1305462E+01  0.5031380E-05  0.4516094E-03  0.5780743E+00 -0.3109995E-05
 -0.2599085E-03  0.1086689E-06  0.2438134E-06  0.1617908E-05 -0.6475753E-01  0.1105048E-04 -0.1005886E+01  0.3579602E+00
 -0.3956548E-01 -0.3888626E-05 -0.1744853E-01  0.1006606E+01 -0.3582574E+00 -0.3959898E-01  0.8684168E-06  0.1742185E-01
  eigenvectors of nos in ao-basis, column    7
  0.1149264E+00  0.8560644E+00 -0.1686224E+00 -0.5129268E-03  0.1803192E-04 -0.1208613E+01  0.2492702E-03 -0.1629883E-04
  0.7517546E+00  0.5461214E-06  0.4378608E-06 -0.3863127E-02 -0.2332773E-04 -0.3138391E-01 -0.9755524E+00  0.4145034E+00
 -0.4937316E-01 -0.5435492E-05 -0.9063568E-02 -0.9748047E+00  0.4142297E+00  0.4934973E-01 -0.1680934E-05 -0.9006312E-02
  eigenvectors of nos in ao-basis, column    8
  0.3737341E-05  0.1832317E-04 -0.2027662E-04  0.1504243E-05  0.1412736E+01  0.2533978E-04  0.2270965E-05 -0.1610862E+01
 -0.2528490E-04 -0.1473108E-06  0.3706120E-01  0.1504865E-06  0.1413979E-05  0.7042561E-06  0.3228163E-04 -0.3330823E-04
 -0.6805850E-07 -0.7523598E-01  0.1767009E-05  0.2979214E-05 -0.2736996E-06  0.1055284E-05 -0.7524331E-01  0.1274739E-05
  eigenvectors of nos in ao-basis, column    9
 -0.4410238E+00 -0.2256837E+01  0.2145985E+01  0.4337682E-05  0.1609808E-04 -0.7487930E+00 -0.1868001E-04 -0.1762974E-04
  0.1099422E+01 -0.2350947E-05  0.2867895E-05 -0.1287070E-01 -0.6008526E-05  0.6081314E-01  0.3713037E+00 -0.2066762E+00
 -0.1335664E-01 -0.2382206E-05  0.7540104E-01  0.3713482E+00 -0.2067219E+00  0.1335889E-01 -0.5382582E-05  0.7542152E-01
  eigenvectors of nos in ao-basis, column   10
  0.7375887E-04  0.3239235E-03 -0.4626132E-03 -0.4937617E+00 -0.6828551E-05 -0.6086699E-05  0.1037075E+01  0.9671029E-05
  0.5839482E-04  0.8762179E-05 -0.1735999E-05 -0.2795255E-04  0.6167754E+00  0.7569200E-04  0.6564946E+00 -0.4996328E+00
  0.1118291E+00 -0.2016034E-05 -0.2188411E-01 -0.6561118E+00  0.4994150E+00  0.1118793E+00  0.3178680E-05  0.2201377E-01
  eigenvectors of nos in ao-basis, column   11
  0.2876428E+00  0.1283661E+01 -0.1770091E+01  0.1177979E-03 -0.8496659E-05 -0.4133801E-01 -0.2811657E-03  0.1250764E-04
  0.2212994E+00  0.1368590E-04 -0.1367715E-07 -0.9798344E-01 -0.1830947E-03  0.2649137E+00  0.6446735E+00 -0.3816389E+00
 -0.6707795E-01 -0.4561915E-05  0.2206464E+00  0.6450719E+00 -0.3819382E+00  0.6702442E-01  0.4982149E-05  0.2206582E+00
  eigenvectors of nos in ao-basis, column   12
  0.5290661E-05  0.2521168E-04 -0.2734151E-04 -0.7501439E-05 -0.7546455E-05  0.5113657E-06  0.1059902E-04  0.1830351E-04
 -0.6215555E-05 -0.8024389E+00  0.1817632E-04 -0.4000631E-05  0.6917651E-05  0.1942333E-05 -0.6215976E-06 -0.1715772E-06
 -0.2157068E-05  0.2661496E+00  0.3614664E-05 -0.3218691E-05  0.3355405E-05  0.5893906E-05 -0.2661701E+00  0.6826872E-05
  eigenvectors of nos in ao-basis, column   13
 -0.1275294E+00 -0.5396780E+00  0.8866376E+00  0.1414866E-05  0.3175377E-05  0.9764750E-01 -0.2865321E-05 -0.7937840E-05
 -0.5118038E+00  0.1095640E-04 -0.1489218E-04 -0.2107402E+00 -0.9881930E-05 -0.2133337E+00 -0.4694329E+00  0.2704496E+00
 -0.1645389E+00  0.1556282E-05  0.9249592E-01 -0.4694359E+00  0.2704610E+00  0.1645450E+00  0.5866010E-05  0.9249328E-01
  eigenvectors of nos in ao-basis, column   14
  0.4813761E-06  0.2142111E-05 -0.1389908E-05  0.5076899E-06 -0.8428742E-01  0.4765284E-05 -0.6817661E-06  0.3034071E+00
 -0.1180277E-04  0.2515184E-04  0.8902048E+00 -0.3537824E-05  0.2128065E-05 -0.4462676E-05 -0.4866416E-05  0.5255815E-05
 -0.1952128E-05 -0.1992543E+00  0.7539208E-06 -0.3926948E-05  0.3698739E-05  0.2408722E-05 -0.1992519E+00  0.1453220E-05
  eigenvectors of nos in ao-basis, column   15
  0.6188134E-03  0.2366564E-02 -0.4557494E-02  0.5040674E+00  0.1749601E-05 -0.2786298E-03 -0.1296589E+01 -0.1879479E-05
  0.2003991E-02 -0.6414156E-05 -0.1799324E-05  0.3445005E-04  0.8053439E+00 -0.6676649E-03 -0.1396966E+01  0.7198505E+00
 -0.7170155E-01 -0.8086872E-05  0.2571439E+00  0.1404369E+01 -0.7240104E+00 -0.7192678E-01  0.5051876E-05 -0.2567491E+00
  eigenvectors of nos in ao-basis, column   16
 -0.3571128E+00 -0.1354257E+01  0.2704752E+01  0.8290310E-03  0.3427676E-05  0.1232872E+00 -0.2124721E-02 -0.5384878E-05
 -0.1153531E+01  0.1549495E-05  0.1674466E-05 -0.2990618E-01  0.1325623E-02  0.4145520E+00 -0.2255195E+01  0.1269380E+01
 -0.7484304E-01  0.9038143E-06 -0.1588119E+00 -0.2250620E+01  0.1267065E+01  0.7457765E-01 -0.3083441E-06 -0.1596914E+00
  eigenvectors of nos in ao-basis, column   17
  0.4090124E-05  0.1941206E-04 -0.7380156E-05 -0.2280305E-04 -0.1082434E-04 -0.9733137E-05 -0.1863037E-04  0.9584658E-04
 -0.3597775E-05  0.7228379E+00 -0.7384673E-04  0.2623752E-05  0.2635737E-04  0.8130741E-06 -0.9283713E-04  0.7651847E-04
 -0.2042693E-04  0.7383490E+00 -0.1996836E-04  0.5095484E-04 -0.4444166E-04  0.3619135E-05 -0.7385447E+00  0.2135183E-04
  eigenvectors of nos in ao-basis, column   18
  0.1060826E-03  0.4822070E-03 -0.1840012E-03  0.8965959E+00 -0.4581934E-05 -0.2800467E-03  0.1230875E+00  0.2866341E-05
  0.1069446E-03  0.2184067E-04  0.3621567E-05  0.9452036E-05 -0.6640312E+00  0.4856575E-04  0.1766668E+01 -0.1628705E+01
  0.3906253E+00  0.2869274E-04  0.6918313E+00 -0.1767374E+01  0.1629258E+01  0.3910805E+00 -0.1892628E-04 -0.6921246E+00
  eigenvectors of nos in ao-basis, column   19
 -0.2196939E+00 -0.1068699E+01 -0.1324105E+00  0.8436784E-04 -0.3289083E-04  0.7069132E+00  0.3517183E-03  0.3818382E-03
  0.3258734E+00  0.9463224E-05 -0.2734306E-03 -0.1245591E+00 -0.1319318E-03 -0.1193583E+00  0.1586139E+01 -0.1110483E+01
  0.9006815E+00 -0.3945570E-03  0.1627552E+00  0.1585058E+01 -0.1109691E+01 -0.9008358E+00 -0.4168689E-03  0.1621946E+00
  eigenvectors of nos in ao-basis, column   20
 -0.1035610E-03 -0.5044547E-03 -0.6355670E-04 -0.1627575E-04  0.5865434E-01  0.3290261E-03  0.2054109E-04 -0.7811216E+00
  0.1707063E-03  0.7990197E-04  0.5703422E+00 -0.6156398E-04  0.7727449E-05 -0.5524208E-04  0.7527387E-03 -0.5224413E-03
  0.4153654E-03  0.8454071E+00  0.7163536E-04  0.7685272E-03 -0.5472066E-03 -0.4467028E-03  0.8452295E+00  0.6534577E-04
  eigenvectors of nos in ao-basis, column   21
 -0.2950653E-03 -0.1266494E-02  0.9586206E-03  0.6111257E+00  0.5707486E-06  0.5772464E-03 -0.1255329E+01 -0.1367074E-04
 -0.7713902E-03 -0.2466357E-05  0.9840792E-05  0.1104890E-03 -0.1449342E+00 -0.1493331E-03 -0.3207282E+00 -0.9896146E-01
  0.7440339E+00  0.1106799E-04 -0.3583075E+00  0.3210659E+00  0.9845015E-01  0.7436946E+00  0.1937213E-04  0.3596485E+00
  eigenvectors of nos in ao-basis, column   22
  0.2980500E+00  0.1240714E+01 -0.1224172E+01  0.3389143E-03  0.3373528E-05 -0.4879877E+00 -0.8979729E-03  0.2115089E-05
  0.1039693E+01  0.1492988E-05 -0.6567599E-05 -0.1783150E+00 -0.9023981E-04  0.1581932E+00  0.2442247E+00  0.9954293E-02
  0.7764334E-01 -0.6718885E-05 -0.8024652E+00  0.2449310E+00  0.9779916E-02 -0.7655233E-01 -0.1122233E-04 -0.8016799E+00
  eigenvectors of nos in ao-basis, column   23
 -0.3242777E+00 -0.1119857E+01  0.3944605E+01  0.3402078E-04 -0.6917624E-05 -0.3605357E+00  0.1363526E-03  0.1265210E-04
 -0.9552461E+00  0.4887754E-06 -0.9217093E-06  0.2200646E-01 -0.5498293E-04  0.1880228E+00 -0.1197333E+01 -0.4574463E+00
 -0.7612254E+00 -0.7786997E-06 -0.5069418E+00 -0.1197514E+01 -0.4576379E+00  0.7613628E+00 -0.3521416E-05 -0.5070484E+00
  eigenvectors of nos in ao-basis, column   24
 -0.2484138E-04 -0.8654846E-04  0.3110885E-03 -0.3608410E+00 -0.3490808E-05 -0.3145826E-04 -0.1588442E+01  0.5312810E-05
 -0.6206637E-04  0.7658489E-06 -0.5273084E-07  0.4647656E-07  0.5570679E+00  0.1381970E-04 -0.9416745E+00 -0.1209860E+01
 -0.7214827E+00  0.1369986E-05 -0.6511868E+00  0.9415291E+00  0.1209747E+01 -0.7213649E+00 -0.8522474E-06  0.6510924E+00


 total number of electrons =   10.0000000000


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***


 cosurf_and_pot_on_the_outer_cavity
  4.050775607358 -0.101071808985  4.196955442697 -0.015625825168
  0.010441682970 -3.612198255232  4.652083638187 -0.024631010925
 -0.789241570581  1.743471269654  5.704914774762 -0.026889792489
  2.073972598936  4.718525890344  2.111755307006 -0.014050301086
  3.117268498212 -4.326789461396  0.474397938180 -0.002868574512
 -4.582690188881 -1.353912193893  3.101430474760 -0.009096743654
 -3.385562959628  4.095478918779  1.235323147076 -0.005877846081
 -0.004917721800  3.976102986090 -2.841011122095  0.011588996907
 -1.830399064625 -4.844476601686 -0.574894544435 -0.001144760165
  3.212134594290 -1.616918816342  4.666794301023 -0.019985566998
  1.643916289252 -2.802247348226  4.957346221664 -0.024224163755
  2.714866756337  0.592484285675  5.279540167909 -0.022961111273
  0.977151244524  1.101353062742  5.852696389317 -0.027063390242
  3.925560684092  1.975404862085  3.752294365964 -0.014597630251
  3.267362519836  3.661361660599  2.823197971886 -0.013548059029
  4.794520096155  0.621084894014  2.985384674181 -0.007470030035
  4.566587306007 -1.512761278186  3.035841551564 -0.008898802917
  4.023883025923 -3.304261852898  1.900099140339 -0.006342895225
 -0.380295949534 -2.075420336871  5.625081381127 -0.026920066925
 -0.578318295438 -0.176753562285  6.025385624624 -0.027681841690
  0.173077949460  3.085961949671  5.073991710630 -0.025692144757
  1.286466029251  4.255380848585  3.676948920622 -0.021049547516
  3.303607685031  4.190688084329  0.874977219987 -0.004312129144
  2.365474477330 -4.670641019924  1.764849152111 -0.011863119944
  1.430906928951 -4.369856297518  3.433513648100 -0.020076322674
 -4.841083335141  0.490465576515  2.917316680086 -0.006902528023
 -4.393460713370  2.567186035154  2.335674800780 -0.006601765194
 -2.222965217239  4.781022087881 -0.115085361143 -0.002738238055
 -1.221032093667  4.616414806724 -1.665577618041  0.004399223596
  0.163627211063  3.046854928202 -3.659275392660  0.018398209432
  0.032681324165 -3.233442231849 -3.526451127503  0.017208699506
 -0.987976628576 -4.384288176387 -2.159923546337  0.007416873277
 -3.246621691231 -4.234905486775  0.877344665928 -0.004564423389
 -4.148219637146 -3.039749448812  2.153579853861 -0.006970661944
 -1.847689082544 -3.090368524535  4.662718846419 -0.023131407103
 -3.477708148477 -2.486557889095  3.918440225273 -0.016819932512
 -3.836825025639 -0.250171027837  4.424711607668 -0.017179595194
 -2.428807490341  0.770561568790  5.412371037466 -0.023935626635
 -2.182131278907  2.739608870207  4.750180949167 -0.022760465554
 -2.910018776645  3.756902253439  3.153702307544 -0.015947842943
 -1.686348599859  4.955774916073  1.768060650114 -0.013390938250
  0.195480392480  5.211736928518  1.860942604097 -0.015280860701
  1.503220315915  5.104908022693  0.297567705890 -0.006770359824
  0.760142032813  4.659957506362 -1.769964770589  0.004345526935
  1.686476488177 -4.955767374770 -0.325424677516 -0.002969954851
 -0.115072502826 -5.217079072686 -0.404886112491 -0.004442182531
 -1.593614359873 -5.064158801861  1.281677954493 -0.011410073217
 -0.877401440800 -4.645291658435  3.201352687003 -0.019913059882
  1.432037543220 -0.945142383016  5.776499419601 -0.026432927219
  2.313736498748  2.694082990980  4.707229619388 -0.022376735701
  4.352554947409  2.683369878192  2.255591816868 -0.006430110132
  3.226222292135 -3.286567988530  3.421154112824 -0.015938151558
 -2.205601631966 -1.249420246876  5.419524744627 -0.024406883193
 -0.872741664792  4.371711264704  3.658229821401 -0.021409632134
  1.877149624159  4.739707996010 -0.863268981216  0.000755407615
  0.561923832933 -5.238172931193  1.584510498744 -0.013977571792
 -3.853927239386  1.864854252547  3.910129924132 -0.015428567959
 -0.424682996015  5.264920405270 -0.053011078089 -0.006245001221
  0.740026015645 -4.433876171161 -2.158397120143  0.007020026807
 -2.766752319178 -3.991987601911  2.936774958699 -0.015554418789
 -5.025900900119  0.067742504061 -3.125151150316  0.019755262332
 -4.858684725656  2.924551709117  0.161132214251  0.006124583212
 -5.557454609143 -1.320544563879  0.975936261489  0.005447435234
 -3.248547936294 -3.839894392576 -1.961901507096  0.011041547496
 -1.190934956284 -1.050244499978 -4.776526587786  0.023954795046
 -2.209781165676  3.111140737530 -3.577927324410  0.017492000806
 -5.882596232580  0.661325507734 -0.904856394886  0.013801346995
 -5.319963799369 -1.934483717314 -1.663429662357  0.015020927678
 -3.646028047584 -1.895758543717 -3.839531623260  0.020165139085
 -3.238797091754  0.700181822085 -4.466013425136  0.022287913597
 -4.577873224583  2.429567517352 -2.545887302119  0.016622264741
 -5.515038344798  0.983321455076  1.345983019759  0.003762904606
 -4.504254459710 -3.320509923530 -0.025897470176  0.005634674328
 -1.783028577264 -3.097620630820 -3.666334648055  0.017775930639
 -1.088127755501  1.416888072316 -4.666210475313  0.023540857103
 -3.289691567450  3.985716663883 -1.487811226052  0.008775835252
  5.025891038603 -0.067775101219 -3.125148233055  0.019751746932
  4.858680855164 -2.924566793325  0.161150660501  0.006122412079
  5.557452224045  1.320533821773  0.975930811333  0.005444775273
  3.248540195407  3.839867994436 -1.961916173837  0.011038382401
  1.190922084255  1.050203102491 -4.776522637080  0.023952996030
  2.209770478753 -3.111175747518 -3.577903054707  0.017490002253
  5.882590418741 -0.661346272731 -0.904851876187  0.013798298154
  5.319956602612  1.934458909776 -1.663437951255  0.015017517698
  3.646016883720  1.895722139538 -3.839536654111  0.020161721960
  3.238784785806 -0.700221564805 -4.466003879574  0.022284821159
  4.577864419098 -2.429597027524 -2.545870981713  0.016619263051
  5.515036634319 -0.983330225135  1.345989924439  0.003760699398
  4.504250248226  3.320493842536 -0.025911658296  0.005631652359
  1.783017729142  3.097585149616 -3.666342687430  0.017773398264
  1.088115084594 -1.416928881883 -4.666193189608  0.023539511363
  3.289684690884 -3.985740535441 -1.487784264400  0.008773866791
 end_of_cosurf_and_pot_on_the_outer_cavity


 **************************************************
 ***Final cosmo calculation for ground state*******
 **************************************************

  ediel before cosmo(4 =  -0.0104563943
  elast before cosmo(4 =  -76.2419624


 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  6  1    -76.2524187801 -9.3530E+00  3.1868E-08  3.1483E-04  1.0000E-03
 mr-sdci #  6  2    -74.5214755096 -9.0880E+00  0.0000E+00  8.4093E-01  1.0000E-04
 mr-sdci #  6  3    -74.2442611213 -9.1447E+00  0.0000E+00  1.2960E+00  1.0000E-04

####################CIUDGINFO####################

   ci vector at position   1 energy=  -76.252418780138

################END OF CIUDGINFO################


    1 of the   4 expansion vectors are transformed.

          transformed tciref transformation matrix  block   1

               ref   1        ref   2        ref   3
  ci:   1     0.97556855    -0.06381076    -0.00587878
    1 of the   3 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
 maximum overlap with reference  1(overlap=  0.975568549)
 ============== writing to civout ======================
 1 32768 4656
 6 5 6
 #cirefvfl= 1 #civfl= 1 method= 0 last= 1 overlap= 98%(  0.975568549)
 Hermit Integral Program : SIFS version  hochtor2        Thu Oct  7 15:41:48 2004
  cidrt_title                                                                    
  title                                                                          
 mofmt: formatted orbitals label=morbl   hochtor2        Thu Oct  7 15:41:49 2004
 SIFS file created by program tran.      hochtor2        Thu Oct  7 15:41:49 2004
 energy computed by program ciudg.       hochtor2        Thu Oct  7 15:42:15 2004
 energy of type -1=  9.31791392
 energy of type -1026= -76.2524188
 energy of type -2055=  0.000314830969
 energy of type -2056= -9.35296196
 energy of type -2057=  3.18679698E-08

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.97556855    -0.06381076    -0.00587878    -0.09791415     0.08942842    -0.16301843     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.97556855    -0.06381076    -0.00587878     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 ==================================================

information on vector: 1from unit 11 written to unit 16filename civout                                                      


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -76.2524187801

                                                       internal orbitals

                                          level       1    2    3    4    5

                                          orbital     1    2    3    4    5

                                         symmetry   a    a    a    a    a  

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1  0.975569                        +-   +-   +-   +-   +- 
 y   1  1      21  0.014580              1( a  )   +-   +-   +-    -   +- 
 x   1  1     107  0.012350    1( a  )   6( a  )   +-   +-   +-    -    - 
 x   1  1     110  0.017587    4( a  )   6( a  )   +-   +-   +-    -    - 
 x   1  1     111  0.032603    5( a  )   6( a  )   +-   +-   +-    -    - 
 x   1  1     127 -0.014594    3( a  )   9( a  )   +-   +-   +-    -    - 
 x   1  1     140 -0.011073    8( a  )  10( a  )   +-   +-   +-    -    - 
 x   1  1     151  0.011944   10( a  )  11( a  )   +-   +-   +-    -    - 
 x   1  1     195  0.010383    8( a  )  15( a  )   +-   +-   +-    -    - 
 x   1  1     198  0.010741   11( a  )  15( a  )   +-   +-   +-    -    - 
 x   1  1     231 -0.014296   15( a  )  17( a  )   +-   +-   +-    -    - 
 x   1  1     279 -0.025205    2( a  )   6( a  )   +-   +-    -   +-    - 
 x   1  1     280 -0.014437    3( a  )   6( a  )   +-   +-    -   +-    - 
 x   1  1     288  0.027265    6( a  )   7( a  )   +-   +-    -   +-    - 
 x   1  1     299  0.020242    4( a  )   9( a  )   +-   +-    -   +-    - 
 x   1  1     306  0.013893    3( a  )  10( a  )   +-   +-    -   +-    - 
 x   1  1     321 -0.011413    9( a  )  11( a  )   +-   +-    -   +-    - 
 x   1  1     339 -0.010132    6( a  )  13( a  )   +-   +-    -   +-    - 
 x   1  1     354  0.011088    9( a  )  14( a  )   +-   +-    -   +-    - 
 x   1  1     376 -0.010381    4( a  )  16( a  )   +-   +-    -   +-    - 
 x   1  1     386  0.011305   14( a  )  16( a  )   +-   +-    -   +-    - 
 x   1  1     419 -0.012158   16( a  )  18( a  )   +-   +-    -   +-    - 
 x   1  1     446 -0.023557    2( a  )   5( a  )   +-   +-    -    -   +- 
 x   1  1     447 -0.021910    3( a  )   5( a  )   +-   +-    -    -   +- 
 x   1  1     457  0.013359    4( a  )   7( a  )   +-   +-    -    -   +- 
 x   1  1     458  0.018911    5( a  )   7( a  )   +-   +-    -    -   +- 
 x   1  1     462 -0.019978    3( a  )   8( a  )   +-   +-    -    -   +- 
 x   1  1     509 -0.012365    5( a  )  13( a  )   +-   +-    -    -   +- 
 x   1  1     561 -0.011553    3( a  )  17( a  )   +-   +-    -    -   +- 
 x   1  1     620 -0.015075    1( a  )   6( a  )   +-    -   +-   +-    - 
 x   1  1     660 -0.015049    6( a  )  11( a  )   +-    -   +-   +-    - 
 x   1  1     787 -0.011149    1( a  )   5( a  )   +-    -   +-    -   +- 
 x   1  1     952  0.010333    1( a  )   2( a  )   +-    -    -   +-   +- 
 x   1  1     967  0.010335    1( a  )   7( a  )   +-    -    -   +-   +- 
 w   1  1    1827 -0.047707    6( a  )   6( a  )   +-   +-   +-   +-      
 w   1  1    1872 -0.012878   11( a  )  11( a  )   +-   +-   +-   +-      
 w   1  1    1901  0.010618    4( a  )  14( a  )   +-   +-   +-   +-      
 w   1  1    1926 -0.012413   15( a  )  15( a  )   +-   +-   +-   +-      
 w   1  1    1935  0.010878    9( a  )  16( a  )   +-   +-   +-   +-      
 w   1  1    1942 -0.012036   16( a  )  16( a  )   +-   +-   +-   +-      
 w   1  1    2015 -0.013396    4( a  )   6( a  )   +-   +-   +-   +     - 
 w   1  1    2016 -0.034829    5( a  )   6( a  )   +-   +-   +-   +     - 
 w   1  1    2035  0.012000    3( a  )   9( a  )   +-   +-   +-   +     - 
 w   1  1    2057 -0.010209    6( a  )  11( a  )   +-   +-   +-   +     - 
 w   1  1    2119 -0.010039    3( a  )  16( a  )   +-   +-   +-   +     - 
 w   1  1    2187 -0.011905    1( a  )   1( a  )   +-   +-   +-        +- 
 w   1  1    2191 -0.015346    2( a  )   3( a  )   +-   +-   +-        +- 
 w   1  1    2192 -0.023059    3( a  )   3( a  )   +-   +-   +-        +- 
 w   1  1    2193 -0.013320    1( a  )   4( a  )   +-   +-   +-        +- 
 w   1  1    2196 -0.013805    4( a  )   4( a  )   +-   +-   +-        +- 
 w   1  1    2200 -0.016693    4( a  )   5( a  )   +-   +-   +-        +- 
 w   1  1    2201 -0.033340    5( a  )   5( a  )   +-   +-   +-        +- 
 w   1  1    2222 -0.014469    8( a  )   8( a  )   +-   +-   +-        +- 
 w   1  1    2246 -0.016538    5( a  )  11( a  )   +-   +-   +-        +- 
 w   1  1    2267  0.012660    3( a  )  13( a  )   +-   +-   +-        +- 
 w   1  1    2306 -0.010504   15( a  )  15( a  )   +-   +-   +-        +- 
 w   1  1    2339 -0.011560   17( a  )  17( a  )   +-   +-   +-        +- 
 w   1  1    2393  0.023833    2( a  )   6( a  )   +-   +-   +    +-    - 
 w   1  1    2394  0.014574    3( a  )   6( a  )   +-   +-   +    +-    - 
 w   1  1    2403  0.027329    6( a  )   7( a  )   +-   +-   +    +-    - 
 w   1  1    2424 -0.010825    3( a  )  10( a  )   +-   +-   +    +-    - 
 w   1  1    2460 -0.011283    6( a  )  13( a  )   +-   +-   +    +-    - 
 w   1  1    2568  0.015671    1( a  )   2( a  )   +-   +-   +     -   +- 
 w   1  1    2570  0.015536    1( a  )   3( a  )   +-   +-   +     -   +- 
 w   1  1    2574  0.019877    2( a  )   4( a  )   +-   +-   +     -   +- 
 w   1  1    2575  0.033070    3( a  )   4( a  )   +-   +-   +     -   +- 
 w   1  1    2578  0.018212    2( a  )   5( a  )   +-   +-   +     -   +- 
 w   1  1    2591  0.011124    4( a  )   7( a  )   +-   +-   +     -   +- 
 w   1  1    2592  0.020434    5( a  )   7( a  )   +-   +-   +     -   +- 
 w   1  1    2620  0.013019    9( a  )  10( a  )   +-   +-   +     -   +- 
 w   1  1    2624  0.010026    3( a  )  11( a  )   +-   +-   +     -   +- 
 w   1  1    2660 -0.012335    3( a  )  14( a  )   +-   +-   +     -   +- 
 w   1  1    2757 -0.012210    1( a  )   1( a  )   +-   +-        +-   +- 
 w   1  1    2759 -0.023794    2( a  )   2( a  )   +-   +-        +-   +- 
 w   1  1    2761 -0.025134    2( a  )   3( a  )   +-   +-        +-   +- 
 w   1  1    2762 -0.032788    3( a  )   3( a  )   +-   +-        +-   +- 
 w   1  1    2763 -0.017244    1( a  )   4( a  )   +-   +-        +-   +- 
 w   1  1    2766 -0.025950    4( a  )   4( a  )   +-   +-        +-   +- 
 w   1  1    2779 -0.024177    2( a  )   7( a  )   +-   +-        +-   +- 
 w   1  1    2780 -0.011677    3( a  )   7( a  )   +-   +-        +-   +- 
 w   1  1    2784 -0.021170    7( a  )   7( a  )   +-   +-        +-   +- 
 w   1  1    2801 -0.014358    9( a  )   9( a  )   +-   +-        +-   +- 
 w   1  1    2837  0.014767    3( a  )  13( a  )   +-   +-        +-   +- 
 w   1  1    2847 -0.010253   13( a  )  13( a  )   +-   +-        +-   +- 
 w   1  1    2962  0.025029    1( a  )   6( a  )   +-   +    +-   +-    - 
 w   1  1    2965  0.017678    4( a  )   6( a  )   +-   +    +-   +-    - 
 w   1  1    2980  0.014173    6( a  )   8( a  )   +-   +    +-   +-    - 
 w   1  1    3007 -0.020295    6( a  )  11( a  )   +-   +    +-   +-    - 
 w   1  1    3043 -0.016729    6( a  )  14( a  )   +-   +    +-   +-    - 
 w   1  1    3055 -0.010607    4( a  )  15( a  )   +-   +    +-   +-    - 
 w   1  1    3068 -0.010402    2( a  )  16( a  )   +-   +    +-   +-    - 
 w   1  1    3137  0.013385    1( a  )   1( a  )   +-   +    +-    -   +- 
 w   1  1    3143  0.019463    1( a  )   4( a  )   +-   +    +-    -   +- 
 w   1  1    3146  0.017891    4( a  )   4( a  )   +-   +    +-    -   +- 
 w   1  1    3147  0.016052    1( a  )   5( a  )   +-   +    +-    -   +- 
 w   1  1    3169  0.012256    5( a  )   8( a  )   +-   +    +-    -   +- 
 w   1  1    3196 -0.019306    5( a  )  11( a  )   +-   +    +-    -   +- 
 w   1  1    3328 -0.018893    1( a  )   2( a  )   +-   +     -   +-   +- 
 w   1  1    3330 -0.011921    1( a  )   3( a  )   +-   +     -   +-   +- 
 w   1  1    3334 -0.010814    2( a  )   4( a  )   +-   +     -   +-   +- 
 w   1  1    3335 -0.019810    3( a  )   4( a  )   +-   +     -   +-   +- 
 w   1  1    3348 -0.020636    1( a  )   7( a  )   +-   +     -   +-   +- 
 w   1  1    3351 -0.014028    4( a  )   7( a  )   +-   +     -   +-   +- 
 w   1  1    3356 -0.012169    2( a  )   8( a  )   +-   +     -   +-   +- 
 w   1  1    3357 -0.013855    3( a  )   8( a  )   +-   +     -   +-   +- 
 w   1  1    3380 -0.012068    9( a  )  10( a  )   +-   +     -   +-   +- 
 w   1  1    3383  0.011151    2( a  )  11( a  )   +-   +     -   +-   +- 
 w   1  1    3388  0.017218    7( a  )  11( a  )   +-   +     -   +-   +- 
 w   1  1    3482 -0.010330    3( a  )  18( a  )   +-   +     -   +-   +- 
 w   1  1    3517 -0.011293    1( a  )   1( a  )   +-        +-   +-   +- 
 w   1  1    3523 -0.013862    1( a  )   4( a  )   +-        +-   +-   +- 
 w   1  1    3526 -0.013224    4( a  )   4( a  )   +-        +-   +-   +- 

 ci coefficient statistics:
           rq > 0.1                1
      0.1> rq > 0.01             111
     0.01> rq > 0.001            591
    0.001> rq > 0.0001           432
   0.0001> rq > 0.00001          139
  0.00001> rq > 0.000001          67
 0.000001> rq                   3315
           all                  4656
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        15 2x:         0 4x:         0
All internal counts: zz :         0 yy:         0 xx:         0 ww:         0
One-external counts: yz :         0 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         0    task #   2:         0    task #   3:         0    task #   4:         0
task #   5:         0    task #   6:         0    task #   7:        -1    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:        14    task #  12:         0
task #  13:         0    task #  14:         0    task #
  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.975568549009    -74.189983940947

 number of reference csfs (nref) is     1.  root number (iroot) is  1.

 pople ci energy extrapolation is computed with 10 correlated electrons.

 eref      =    -76.047945596779   "relaxed" cnot**2         =   0.951733993815
 eci       =    -76.252418780138   deltae = eci - eref       =  -0.204473183359
 eci+dv1   =    -76.262287884070   dv1 = (1-cnot**2)*deltae  =  -0.009869103933
 eci+dv2   =    -76.262788383406   dv2 = dv1 / cnot**2       =  -0.010369603268
 eci+dv3   =    -76.263342359133   dv3 = dv1 / (2*cnot**2-1) =  -0.010923578995
 eci+pople =    -76.260971065732   ( 10e- scaled deltae )    =  -0.213025468954
NO coefficients and occupation numbers are written to nocoef_ci.1
 entering drivercid: firstcall,firstnonref= F T
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=      10  D0X=      30  D0W=      50
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================

          ci-one-electron density block   1

                mo   1         mo   2         mo   3         mo   4         mo   5         mo   6         mo   7         mo   8
   mo   1     1.99991404
   mo   2    -0.00021064     1.98510661
   mo   3     0.00000000    -0.00000004     1.96965876
   mo   4    -0.00015604     0.00416185    -0.00000004     1.97325737
   mo   5     0.00000000    -0.00000007    -0.00000001    -0.00000007     1.97621175
   mo   6     0.00012065    -0.00415549    -0.00001597    -0.01965435    -0.00000768     0.00700164
   mo   7     0.00000000     0.00000670    -0.00981642    -0.00002015    -0.00000474    -0.00000008     0.00754298
   mo   8    -0.00000001     0.00001411    -0.01189457    -0.00001854     0.00000222    -0.00000022     0.00721401     0.01063697
   mo   9     0.00005756     0.00295044    -0.00000982    -0.00013238     0.00000171     0.00713781    -0.00000022    -0.00000032
   mo  10    -0.00021473     0.01030164    -0.00001285     0.00324923     0.00000015     0.00264746    -0.00000019    -0.00000012
   mo  11     0.00000004    -0.00000158     0.00000280    -0.00000043    -0.01153500     0.00000020     0.00000009    -0.00000009
   mo  12    -0.00000001    -0.00000289    -0.00600085    -0.00000577    -0.00000233     0.00000006     0.00613512     0.00410623
   mo  13     0.00020188    -0.00114346    -0.00001160    -0.00746018    -0.00000288     0.00195963    -0.00000006    -0.00000019
   mo  14     0.00000000     0.00000203    -0.00000287    -0.00000107    -0.00000081    -0.00000003    -0.00000003    -0.00000002
   mo  15     0.00000000     0.00000097    -0.00000178    -0.00000448    -0.00298356     0.00000002    -0.00000001    -0.00000001
   mo  16    -0.00038703    -0.00026300    -0.00000435     0.00284697     0.00000152    -0.00195367    -0.00000010    -0.00000004
   mo  17    -0.00000001    -0.00000809     0.00094487     0.00001960     0.00000045     0.00000000     0.00027505     0.00024696
   mo  18     0.00000003    -0.00000045     0.01165625    -0.00000056     0.00000087    -0.00000010    -0.00286912    -0.00439979
   mo  19    -0.00010843    -0.00448197    -0.00000019     0.00759335     0.00000041    -0.00312249    -0.00000012    -0.00000007
   mo  20     0.00000000     0.00000019    -0.00000045     0.00000129     0.00190374    -0.00000003     0.00000000     0.00000000
   mo  21     0.00000000    -0.00000022     0.00000056    -0.00000056    -0.00000129     0.00000001    -0.00000001    -0.00000002
   mo  22     0.00001080     0.00012865     0.00000232    -0.00110883     0.00000027    -0.00006546     0.00000003     0.00000003
   mo  23     0.00005142     0.00172134    -0.00000045     0.00029035    -0.00000045     0.00064386     0.00000000    -0.00000002
   mo  24    -0.00000001    -0.00000060    -0.00025118     0.00000177     0.00000009     0.00000000    -0.00083647     0.00033813

                mo   9         mo  10         mo  11         mo  12         mo  13         mo  14         mo  15         mo  16
   mo   9     0.01014361
   mo  10     0.00335955     0.01010219
   mo  11    -0.00000005    -0.00000002     0.01451278
   mo  12     0.00000004     0.00000003     0.00000002     0.00653210
   mo  13     0.00054166    -0.00063933     0.00000006    -0.00000002     0.00372655
   mo  14    -0.00000003     0.00000000     0.00000006    -0.00000002     0.00000000     0.00297208
   mo  15    -0.00000003    -0.00000006    -0.00139325    -0.00000001     0.00000002    -0.00000005     0.00224105
   mo  16     0.00002222     0.00264944    -0.00000003    -0.00000007    -0.00125313     0.00000001    -0.00000002     0.00439067
   mo  17     0.00000019     0.00000025    -0.00000001     0.00035879    -0.00000002     0.00000000     0.00000000     0.00000006
   mo  18    -0.00000013    -0.00000016     0.00000001    -0.00184547     0.00000001     0.00000000     0.00000000    -0.00000003
   mo  19    -0.00438184    -0.00082804     0.00000000    -0.00000012    -0.00084078     0.00000000    -0.00000001    -0.00005817
   mo  20    -0.00000001     0.00000000     0.00004145     0.00000000    -0.00000001     0.00000000    -0.00182346     0.00000000
   mo  21     0.00000000    -0.00000001     0.00000002     0.00000000     0.00000000    -0.00202199     0.00000000    -0.00000001
   mo  22    -0.00056971     0.00029399    -0.00000001     0.00000002     0.00172039     0.00000000     0.00000000     0.00070203
   mo  23     0.00024704     0.00177886     0.00000000     0.00000003     0.00025137     0.00000000     0.00000000    -0.00068746
   mo  24     0.00000001     0.00000000     0.00000001    -0.00169038     0.00000001     0.00000000     0.00000000     0.00000002

                mo  17         mo  18         mo  19         mo  20         mo  21         mo  22         mo  23         mo  24
   mo  17     0.00051302
   mo  18    -0.00020367     0.00262042
   mo  19     0.00000001     0.00000005     0.00288190
   mo  20     0.00000000     0.00000000     0.00000001     0.00254997
   mo  21     0.00000000     0.00000001     0.00000000     0.00000001     0.00242271
   mo  22    -0.00000001     0.00000001    -0.00015927     0.00000000     0.00000000     0.00197256
   mo  23     0.00000003    -0.00000002     0.00061123     0.00000000     0.00000000    -0.00008866     0.00163927
   mo  24     0.00000313    -0.00057333     0.00000002     0.00000000     0.00000000    -0.00000001    -0.00000001     0.00144901


*****   symmetry block SYM1   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       1.99991404
       2      -0.00021064     1.98510661
       3       0.00000000    -0.00000004     1.96965876
       4      -0.00015604     0.00416185    -0.00000004     1.97325737
       5       0.00000000    -0.00000007    -0.00000001    -0.00000007
       6       0.00012065    -0.00415549    -0.00001597    -0.01965435
       7       0.00000000     0.00000670    -0.00981642    -0.00002015
       8      -0.00000001     0.00001411    -0.01189457    -0.00001854
       9       0.00005756     0.00295044    -0.00000982    -0.00013238
      10      -0.00021473     0.01030164    -0.00001285     0.00324923
      11       0.00000004    -0.00000158     0.00000280    -0.00000043
      12      -0.00000001    -0.00000289    -0.00600085    -0.00000577
      13       0.00020188    -0.00114346    -0.00001160    -0.00746018
      14       0.00000000     0.00000203    -0.00000287    -0.00000107
      15       0.00000000     0.00000097    -0.00000178    -0.00000448
      16      -0.00038703    -0.00026300    -0.00000435     0.00284697
      17      -0.00000001    -0.00000809     0.00094487     0.00001960
      18       0.00000003    -0.00000045     0.01165625    -0.00000056
      19      -0.00010843    -0.00448197    -0.00000019     0.00759335
      20       0.00000000     0.00000019    -0.00000045     0.00000129
      21       0.00000000    -0.00000022     0.00000056    -0.00000056
      22       0.00001080     0.00012865     0.00000232    -0.00110883
      23       0.00005142     0.00172134    -0.00000045     0.00029035
      24      -0.00000001    -0.00000060    -0.00025118     0.00000177

               Column   5     Column   6     Column   7     Column   8
       5       1.97621175
       6      -0.00000768     0.00700164
       7      -0.00000474    -0.00000008     0.00754298
       8       0.00000222    -0.00000022     0.00721401     0.01063697
       9       0.00000171     0.00713781    -0.00000022    -0.00000032
      10       0.00000015     0.00264746    -0.00000019    -0.00000012
      11      -0.01153500     0.00000020     0.00000009    -0.00000009
      12      -0.00000233     0.00000006     0.00613512     0.00410623
      13      -0.00000288     0.00195963    -0.00000006    -0.00000019
      14      -0.00000081    -0.00000003    -0.00000003    -0.00000002
      15      -0.00298356     0.00000002    -0.00000001    -0.00000001
      16       0.00000152    -0.00195367    -0.00000010    -0.00000004
      17       0.00000045     0.00000000     0.00027505     0.00024696
      18       0.00000087    -0.00000010    -0.00286912    -0.00439979
      19       0.00000041    -0.00312249    -0.00000012    -0.00000007
      20       0.00190374    -0.00000003     0.00000000     0.00000000
      21      -0.00000129     0.00000001    -0.00000001    -0.00000002
      22       0.00000027    -0.00006546     0.00000003     0.00000003
      23      -0.00000045     0.00064386     0.00000000    -0.00000002
      24       0.00000009     0.00000000    -0.00083647     0.00033813

               Column   9     Column  10     Column  11     Column  12
       9       0.01014361
      10       0.00335955     0.01010219
      11      -0.00000005    -0.00000002     0.01451278
      12       0.00000004     0.00000003     0.00000002     0.00653210
      13       0.00054166    -0.00063933     0.00000006    -0.00000002
      14      -0.00000003     0.00000000     0.00000006    -0.00000002
      15      -0.00000003    -0.00000006    -0.00139325    -0.00000001
      16       0.00002222     0.00264944    -0.00000003    -0.00000007
      17       0.00000019     0.00000025    -0.00000001     0.00035879
      18      -0.00000013    -0.00000016     0.00000001    -0.00184547
      19      -0.00438184    -0.00082804     0.00000000    -0.00000012
      20      -0.00000001     0.00000000     0.00004145     0.00000000
      21       0.00000000    -0.00000001     0.00000002     0.00000000
      22      -0.00056971     0.00029399    -0.00000001     0.00000002
      23       0.00024704     0.00177886     0.00000000     0.00000003
      24       0.00000001     0.00000000     0.00000001    -0.00169038

               Column  13     Column  14     Column  15     Column  16
      13       0.00372655
      14       0.00000000     0.00297208
      15       0.00000002    -0.00000005     0.00224105
      16      -0.00125313     0.00000001    -0.00000002     0.00439067
      17      -0.00000002     0.00000000     0.00000000     0.00000006
      18       0.00000001     0.00000000     0.00000000    -0.00000003
      19      -0.00084078     0.00000000    -0.00000001    -0.00005817
      20      -0.00000001     0.00000000    -0.00182346     0.00000000
      21       0.00000000    -0.00202199     0.00000000    -0.00000001
      22       0.00172039     0.00000000     0.00000000     0.00070203
      23       0.00025137     0.00000000     0.00000000    -0.00068746
      24       0.00000001     0.00000000     0.00000000     0.00000002

               Column  17     Column  18     Column  19     Column  20
      17       0.00051302
      18      -0.00020367     0.00262042
      19       0.00000001     0.00000005     0.00288190
      20       0.00000000     0.00000000     0.00000001     0.00254997
      21       0.00000000     0.00000001     0.00000000     0.00000001
      22      -0.00000001     0.00000001    -0.00015927     0.00000000
      23       0.00000003    -0.00000002     0.00061123     0.00000000
      24       0.00000313    -0.00057333     0.00000002     0.00000000

               Column  21     Column  22     Column  23     Column  24
      21       0.00242271
      22       0.00000000     0.00197256
      23       0.00000000    -0.00008866     0.00163927
      24       0.00000000    -0.00000001    -0.00000001     0.00144901

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7        eno   8
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    8   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno   9        eno  10        eno  11        eno  12        eno  13        eno  14        eno  15        eno  16
 emo    9   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   10   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   11   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   12   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   13   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   14   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   15   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   16   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno  17        eno  18        eno  19        eno  20        eno  21        eno  22        eno  23        eno  24
 emo   17   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   18   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   19   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   20   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   21   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   22   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   23   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   24   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   1.9999191      1.9865397      1.9762859      1.9721629      1.9698693
   0.0216017      0.0198394      0.0146115      0.0103122      0.0054171
   0.0051611      0.0047380      0.0042170      0.0041266      0.0010148
   0.0010105      0.0006568      0.0005694      0.0004933      0.0004915
   0.0004478      0.0004309      0.0000503      0.0000331

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7         no   8
  mo    1   0.999820E+00   0.187194E-01  -0.529038E-07   0.311985E-02  -0.564122E-06   0.225100E-07  -0.350653E-04  -0.215385E-07
  mo    2  -0.168540E-01   0.951437E+00  -0.929856E-06  -0.307318E+00   0.608092E-04  -0.492746E-05  -0.268091E-02   0.959633E-06
  mo    3   0.775370E-07  -0.457807E-05   0.386584E-06   0.183417E-03   0.999946E+00   0.971480E-02   0.146023E-04  -0.155668E-05
  mo    4  -0.871870E-02   0.307175E+00   0.117477E-04   0.951543E+00  -0.173344E-03   0.105149E-04   0.622229E-02   0.172804E-07
  mo    5   0.141005E-06  -0.275262E-05   0.999981E+00  -0.115104E-04  -0.389000E-06   0.131272E-05   0.175299E-05   0.564057E-02
  mo    6   0.181817E-03  -0.503386E-02  -0.401571E-05  -0.888069E-02  -0.655611E-05  -0.210305E-03   0.536377E+00   0.491807E-05
  mo    7   0.312451E-07   0.124600E-06  -0.240798E-05  -0.117784E-04  -0.504286E-02   0.570245E+00   0.209870E-03   0.377308E-05
  mo    8  -0.422068E-07   0.393682E-05   0.111262E-05  -0.123679E-04  -0.610907E-02   0.636986E+00   0.224559E-03  -0.763729E-05
  mo    9   0.495603E-05   0.139324E-02   0.851682E-06  -0.569225E-03  -0.494212E-05  -0.249732E-03   0.672591E+00  -0.128745E-04
  mo   10  -0.209426E-03   0.545985E-02   0.886168E-07  -0.500705E-04  -0.654142E-05  -0.152428E-03   0.405592E+00  -0.126887E-04
  mo   11   0.332790E-07  -0.813174E-06  -0.587868E-02   0.109641E-06   0.143310E-05   0.261860E-05   0.114301E-04   0.992942E+00
  mo   12   0.453798E-07  -0.226043E-05  -0.119132E-05  -0.296284E-05  -0.309022E-02   0.440238E+00   0.184069E-03   0.818854E-06
  mo   13   0.143755E-03  -0.170863E-02  -0.150982E-05  -0.343968E-02  -0.527746E-05  -0.494449E-04   0.848040E-01   0.511813E-05
  mo   14  -0.126875E-07   0.805658E-06  -0.411737E-06  -0.833746E-06  -0.145829E-05  -0.330194E-05  -0.282962E-05   0.507796E-05
  mo   15   0.104940E-07  -0.227009E-06  -0.150811E-02  -0.230006E-05  -0.906483E-06  -0.200975E-05  -0.465489E-05  -0.116421E+00
  mo   16  -0.204674E-03   0.324342E-03   0.791440E-06   0.142780E-02  -0.246933E-05  -0.200825E-05  -0.484259E-02  -0.662205E-05
  mo   17  -0.238756E-07  -0.844000E-06   0.227423E-06   0.108028E-04   0.477110E-03   0.254206E-01   0.302065E-04  -0.601032E-06
  mo   18   0.222521E-07  -0.337706E-06   0.444308E-06   0.930113E-06   0.594874E-02  -0.269677E+00  -0.114621E-03   0.173465E-05
  mo   19  -0.498733E-04  -0.971336E-03   0.261786E-06   0.438512E-02  -0.875489E-06   0.975277E-04  -0.290864E+00   0.382664E-05
  mo   20  -0.609090E-08   0.290212E-06   0.965790E-03   0.586592E-06  -0.229482E-06  -0.400913E-07  -0.214225E-06   0.219029E-01
  mo   21   0.551405E-08  -0.192774E-06  -0.652947E-06  -0.234972E-06   0.288683E-06  -0.295635E-06   0.169115E-06   0.473122E-06
  mo   22   0.917369E-05  -0.110599E-03   0.127557E-06  -0.557971E-03   0.127390E-05   0.347481E-05  -0.686912E-02  -0.634935E-07
  mo   23   0.988877E-05   0.873331E-03  -0.229713E-06  -0.130740E-03  -0.209489E-06  -0.212717E-04   0.592116E-01  -0.133121E-05
  mo   24  -0.847150E-08  -0.108312E-07   0.467720E-07   0.931425E-06  -0.125582E-03  -0.423527E-01  -0.183290E-04  -0.143861E-06

                no   9         no  10         no  11         no  12         no  13         no  14         no  15         no  16
  mo    1   0.224751E-03  -0.296196E-07  -0.974198E-04  -0.115126E-08   0.434493E-04   0.150398E-09  -0.513523E-07   0.199563E-04
  mo    2  -0.411338E-02   0.399111E-05  -0.290217E-03  -0.838367E-06   0.357476E-03   0.197173E-06  -0.646146E-05   0.105047E-02
  mo    3   0.165409E-05  -0.200325E-02   0.498857E-05   0.128736E-05   0.150980E-05  -0.303098E-06  -0.188366E-02  -0.299364E-05
  mo    4  -0.605975E-02   0.537047E-07   0.357343E-02   0.330068E-06   0.130862E-02  -0.197391E-05   0.110324E-04  -0.270873E-02
  mo    5  -0.159878E-05   0.175519E-05   0.214205E-05  -0.201167E-06  -0.321700E-06  -0.225915E-02  -0.873085E-06  -0.921643E-06
  mo    6  -0.255525E+00   0.376600E-04   0.190264E+00   0.278969E-05  -0.124569E+00  -0.308656E-05   0.218403E-03  -0.134620E+00
  mo    7  -0.106108E-04   0.219612E+00  -0.579077E-04   0.297307E-05   0.509378E-05  -0.613578E-06   0.421513E-01   0.824803E-04
  mo    8   0.706001E-05  -0.545394E+00   0.159861E-03  -0.390366E-05   0.453378E-05  -0.905353E-06   0.400210E+00   0.657174E-03
  mo    9  -0.167436E+00  -0.116501E-03  -0.366178E+00  -0.291403E-05   0.459723E-01   0.576925E-06  -0.653066E-03   0.401136E+00
  mo   10   0.767845E+00   0.937117E-04   0.319457E+00   0.187081E-05  -0.143566E+00  -0.600250E-05   0.487779E-03  -0.285006E+00
  mo   11   0.121673E-04  -0.591117E-05  -0.741378E-05  -0.627799E-05   0.306730E-05  -0.932044E-01   0.177463E-05   0.300531E-05
  mo   12  -0.708699E-05   0.638896E+00  -0.170865E-03   0.796547E-05  -0.180389E-05   0.327842E-06  -0.293338E+00  -0.487357E-03
  mo   13  -0.254905E+00   0.171881E-03   0.603524E+00   0.180177E-04   0.449849E+00   0.824773E-05  -0.119839E-03   0.574649E-01
  mo   14   0.319079E-05  -0.780244E-05  -0.131638E-04   0.753197E+00  -0.826333E-05  -0.138638E-04  -0.585982E-05   0.756057E-07
  mo   15  -0.602907E-05   0.147752E-05   0.223059E-06  -0.207043E-04   0.113940E-04  -0.653763E+00  -0.956261E-06  -0.407367E-06
  mo   16   0.468386E+00  -0.929439E-04  -0.363478E+00  -0.101494E-05   0.556404E+00   0.670641E-05  -0.493476E-03   0.282879E+00
  mo   17   0.114217E-04   0.191298E-01   0.957809E-05   0.208685E-05   0.151029E-05   0.389022E-06  -0.247984E+00  -0.434498E-03
  mo   18  -0.661245E-05   0.284597E+00  -0.916462E-04   0.257662E-05  -0.222397E-05   0.119426E-05   0.619169E+00   0.101829E-02
  mo   19   0.149601E+00   0.552068E-04   0.187106E+00  -0.127753E-05  -0.277937E+00  -0.584046E-05  -0.751348E-03   0.427955E+00
  mo   20   0.184397E-05  -0.142831E-05   0.274572E-07   0.141873E-04  -0.124924E-04   0.750934E+00  -0.187467E-05   0.153546E-05
  mo   21  -0.180886E-05   0.723349E-05   0.111807E-04  -0.657795E+00   0.922439E-05   0.217896E-04  -0.668630E-05   0.145414E-05
  mo   22   0.241938E-01   0.894150E-04   0.317382E+00   0.109729E-04   0.523513E+00   0.887437E-05  -0.172276E-03   0.136023E+00
  mo   23   0.985128E-01   0.881474E-04   0.309113E+00   0.190537E-05  -0.313834E+00  -0.679885E-05  -0.108370E-02   0.674357E+00
  mo   24   0.297177E-05  -0.405909E+00   0.118883E-03  -0.489850E-05   0.688746E-05  -0.141641E-05  -0.554198E+00  -0.911212E-03

                no  17         no  18         no  19         no  20         no  21         no  22         no  23         no  24
  mo    1  -0.168638E-08  -0.223968E-07   0.450501E-04   0.202305E-07   0.393818E-07  -0.357238E-04   0.259224E-04   0.372580E-08
  mo    2  -0.629752E-06  -0.315885E-05   0.273258E-02   0.906113E-06   0.489183E-05  -0.196298E-02   0.179206E-02  -0.682787E-07
  mo    3   0.763245E-06  -0.795563E-03   0.128896E-05   0.770495E-06  -0.175298E-02  -0.696134E-05   0.679760E-06   0.151229E-02
  mo    4   0.561171E-06   0.632872E-05   0.156117E-02   0.203962E-05  -0.547243E-05  -0.147512E-02   0.571883E-02   0.853476E-06
  mo    5   0.649784E-06   0.126021E-05   0.624979E-06   0.919919E-03  -0.125119E-06  -0.117358E-05   0.215292E-05   0.115234E-05
  mo    6  -0.575316E-05  -0.144441E-03   0.473914E+00   0.228200E-03   0.265773E-03  -0.201507E+00   0.558446E+00   0.497935E-04
  mo    7  -0.160655E-04   0.564689E+00   0.107123E-03  -0.542657E-05   0.181762E+00   0.500060E-04  -0.423627E-04   0.522377E+00
  mo    8   0.119261E-04  -0.187951E+00  -0.574359E-04   0.149133E-05   0.531694E-01   0.261405E-04   0.246539E-04  -0.313745E+00
  mo    9   0.150926E-05   0.682814E-05  -0.123193E-01  -0.605740E-05  -0.141538E-04   0.269184E-01  -0.470742E+00  -0.444505E-04
  mo   10   0.185494E-05   0.404105E-04  -0.556648E-01  -0.237732E-04  -0.382770E-04   0.984911E-02  -0.196952E+00  -0.138318E-04
  mo   11  -0.116088E-04  -0.389646E-05  -0.391516E-04   0.731011E-01   0.107609E-05   0.283266E-05  -0.640904E-05  -0.321303E-05
  mo   12   0.297384E-05  -0.145861E+00  -0.108587E-05   0.325248E-05  -0.124285E+00  -0.568859E-04   0.462061E-04  -0.524617E+00
  mo   13   0.609347E-05   0.229839E-04  -0.324598E+00  -0.161741E-03   0.284754E-03  -0.468034E+00  -0.183002E+00  -0.219481E-04
  mo   14   0.657795E+00   0.219206E-04   0.106448E-04   0.824677E-04  -0.447444E-05   0.235107E-05   0.163603E-05   0.114600E-05
  mo   15  -0.829056E-04   0.414568E-05  -0.359013E-03   0.747688E+00   0.136708E-04  -0.767122E-05  -0.219502E-05   0.309531E-06
  mo   16  -0.181462E-05  -0.610355E-04   0.365208E-01   0.156341E-04   0.271645E-03  -0.330927E+00   0.384733E+00   0.290204E-04
  mo   17   0.140599E-04  -0.371145E+00  -0.323872E-03  -0.144512E-04   0.891101E+00   0.772209E-03   0.671831E-06   0.753696E-01
  mo   18  -0.500364E-05   0.385681E+00   0.200468E-04  -0.789340E-05   0.370123E+00   0.197840E-03   0.359591E-04  -0.420869E+00
  mo   19  -0.765597E-05  -0.217490E-03   0.587397E+00   0.276404E-03   0.467181E-03  -0.391904E+00  -0.314595E+00  -0.290813E-04
  mo   20  -0.833276E-04   0.410264E-05  -0.316504E-03   0.660013E+00   0.115024E-04  -0.746169E-05  -0.120608E-05   0.359733E-08
  mo   21   0.753197E+00   0.230039E-04   0.101051E-04   0.844308E-04  -0.283805E-05   0.167501E-05   0.652521E-06   0.852004E-06
  mo   22  -0.890477E-05  -0.289314E-04   0.429537E+00   0.212563E-03  -0.403018E-03   0.645208E+00  -0.726117E-01   0.138964E-06
  mo   23   0.391915E-05   0.124479E-03  -0.368659E+00  -0.173324E-03  -0.277322E-03   0.245185E+00   0.376778E+00   0.272806E-04
  mo   24  -0.221064E-04   0.581400E+00   0.115528E-03  -0.670898E-05   0.132788E+00   0.730247E-04   0.408441E-04  -0.413097E+00
  eigenvectors of nos in ao-basis, column    1
  0.1000601E+01 -0.1377804E-01 -0.1662908E-02  0.3245223E-07  0.1757237E-06 -0.7407348E-02  0.9790173E-07 -0.5020448E-07
  0.1598599E-03  0.7548227E-08 -0.1077275E-07  0.4259922E-04  0.1643250E-07  0.9087801E-04 -0.3267292E-02  0.2149286E-02
 -0.1015572E-02  0.3638948E-08 -0.9375905E-03 -0.3267423E-02  0.2149422E-02  0.1015587E-02  0.1684873E-07 -0.9376378E-03
  eigenvectors of nos in ao-basis, column    2
  0.9613130E-02  0.9133742E+00 -0.7093028E-01 -0.2908949E-05 -0.3658150E-05  0.1262520E+00 -0.4966643E-05  0.1213515E-05
  0.7713904E-01 -0.3105693E-06  0.3936961E-06 -0.1217897E-02 -0.2181361E-06  0.1108058E-02  0.2160614E+00 -0.1057495E+00
  0.2689749E-01  0.3311061E-06  0.2160456E-01  0.2160712E+00 -0.1057561E+00 -0.2689936E-01 -0.6276678E-06  0.2160309E-01
  eigenvectors of nos in ao-basis, column    3
 -0.7331143E-06 -0.1883919E-05  0.2920083E-05  0.2678192E-05  0.9106445E+00  0.1005922E-04 -0.2276159E-05  0.8254139E-01
  0.1744943E-05 -0.6470619E-06 -0.1702301E-01 -0.8810187E-07 -0.1361925E-06 -0.2351638E-06 -0.7127024E-05  0.9753982E-05
  0.7198153E-06  0.3073906E-01 -0.2843523E-06 -0.2066202E-05  0.4911864E-06 -0.2607988E-06  0.3074012E-01 -0.4873248E-06
  eigenvectors of nos in ao-basis, column    4
  0.1178538E-02 -0.1864664E-02  0.2170848E+00  0.1460466E-03 -0.1026354E-04  0.8017902E+00 -0.2761290E-04  0.1649640E-06
 -0.2079127E-01 -0.1444721E-06  0.1191195E-05 -0.4737852E-02 -0.5147203E-05 -0.2415996E-02 -0.4304921E+00  0.1834880E+00
 -0.3819656E-01 -0.2558966E-05  0.2404615E-02 -0.4303120E+00  0.1834173E+00  0.3819765E-01 -0.1235601E-05  0.2434169E-02
  eigenvectors of nos in ao-basis, column    5
  0.3741588E-06  0.3319236E-05 -0.4569826E-04  0.7308666E+00  0.1721572E-05 -0.1353268E-03 -0.1189211E+00 -0.1829870E-05
  0.2965081E-06  0.4975460E-06 -0.9424214E-07  0.7572541E-06 -0.2547355E-01  0.7173900E-06 -0.5528036E+00  0.1931091E+00
 -0.2036866E-01 -0.1687338E-05 -0.3117050E-01  0.5529811E+00 -0.1931803E+00 -0.2038531E-01  0.9402308E-07  0.3115901E-01
  eigenvectors of nos in ao-basis, column    6
 -0.3987785E-04 -0.3116654E-03  0.5918306E-04 -0.1305462E+01  0.5031380E-05  0.4516094E-03  0.5780743E+00 -0.3109995E-05
 -0.2599085E-03  0.1086689E-06  0.2438134E-06  0.1617908E-05 -0.6475753E-01  0.1105048E-04 -0.1005886E+01  0.3579602E+00
 -0.3956548E-01 -0.3888626E-05 -0.1744853E-01  0.1006606E+01 -0.3582574E+00 -0.3959898E-01  0.8684168E-06  0.1742185E-01
  eigenvectors of nos in ao-basis, column    7
  0.1149264E+00  0.8560644E+00 -0.1686224E+00 -0.5129268E-03  0.1803192E-04 -0.1208613E+01  0.2492702E-03 -0.1629883E-04
  0.7517546E+00  0.5461214E-06  0.4378608E-06 -0.3863127E-02 -0.2332773E-04 -0.3138391E-01 -0.9755524E+00  0.4145034E+00
 -0.4937316E-01 -0.5435492E-05 -0.9063568E-02 -0.9748047E+00  0.4142297E+00  0.4934973E-01 -0.1680934E-05 -0.9006312E-02
  eigenvectors of nos in ao-basis, column    8
  0.3737341E-05  0.1832317E-04 -0.2027662E-04  0.1504243E-05  0.1412736E+01  0.2533978E-04  0.2270965E-05 -0.1610862E+01
 -0.2528490E-04 -0.1473108E-06  0.3706120E-01  0.1504865E-06  0.1413979E-05  0.7042561E-06  0.3228163E-04 -0.3330823E-04
 -0.6805850E-07 -0.7523598E-01  0.1767009E-05  0.2979214E-05 -0.2736996E-06  0.1055284E-05 -0.7524331E-01  0.1274739E-05
  eigenvectors of nos in ao-basis, column    9
 -0.4410238E+00 -0.2256837E+01  0.2145985E+01  0.4337682E-05  0.1609808E-04 -0.7487930E+00 -0.1868001E-04 -0.1762974E-04
  0.1099422E+01 -0.2350947E-05  0.2867895E-05 -0.1287070E-01 -0.6008526E-05  0.6081314E-01  0.3713037E+00 -0.2066762E+00
 -0.1335664E-01 -0.2382206E-05  0.7540104E-01  0.3713482E+00 -0.2067219E+00  0.1335889E-01 -0.5382582E-05  0.7542152E-01
  eigenvectors of nos in ao-basis, column   10
  0.7375887E-04  0.3239235E-03 -0.4626132E-03 -0.4937617E+00 -0.6828551E-05 -0.6086699E-05  0.1037075E+01  0.9671029E-05
  0.5839482E-04  0.8762179E-05 -0.1735999E-05 -0.2795255E-04  0.6167754E+00  0.7569200E-04  0.6564946E+00 -0.4996328E+00
  0.1118291E+00 -0.2016034E-05 -0.2188411E-01 -0.6561118E+00  0.4994150E+00  0.1118793E+00  0.3178680E-05  0.2201377E-01
  eigenvectors of nos in ao-basis, column   11
  0.2876428E+00  0.1283661E+01 -0.1770091E+01  0.1177979E-03 -0.8496659E-05 -0.4133801E-01 -0.2811657E-03  0.1250764E-04
  0.2212994E+00  0.1368590E-04 -0.1367715E-07 -0.9798344E-01 -0.1830947E-03  0.2649137E+00  0.6446735E+00 -0.3816389E+00
 -0.6707795E-01 -0.4561915E-05  0.2206464E+00  0.6450719E+00 -0.3819382E+00  0.6702442E-01  0.4982149E-05  0.2206582E+00
  eigenvectors of nos in ao-basis, column   12
  0.5290661E-05  0.2521168E-04 -0.2734151E-04 -0.7501439E-05 -0.7546455E-05  0.5113657E-06  0.1059902E-04  0.1830351E-04
 -0.6215555E-05 -0.8024389E+00  0.1817632E-04 -0.4000631E-05  0.6917651E-05  0.1942333E-05 -0.6215976E-06 -0.1715772E-06
 -0.2157068E-05  0.2661496E+00  0.3614664E-05 -0.3218691E-05  0.3355405E-05  0.5893906E-05 -0.2661701E+00  0.6826872E-05
  eigenvectors of nos in ao-basis, column   13
 -0.1275294E+00 -0.5396780E+00  0.8866376E+00  0.1414866E-05  0.3175377E-05  0.9764750E-01 -0.2865321E-05 -0.7937840E-05
 -0.5118038E+00  0.1095640E-04 -0.1489218E-04 -0.2107402E+00 -0.9881930E-05 -0.2133337E+00 -0.4694329E+00  0.2704496E+00
 -0.1645389E+00  0.1556282E-05  0.9249592E-01 -0.4694359E+00  0.2704610E+00  0.1645450E+00  0.5866010E-05  0.9249328E-01
  eigenvectors of nos in ao-basis, column   14
  0.4813761E-06  0.2142111E-05 -0.1389908E-05  0.5076899E-06 -0.8428742E-01  0.4765284E-05 -0.6817661E-06  0.3034071E+00
 -0.1180277E-04  0.2515184E-04  0.8902048E+00 -0.3537824E-05  0.2128065E-05 -0.4462676E-05 -0.4866416E-05  0.5255815E-05
 -0.1952128E-05 -0.1992543E+00  0.7539208E-06 -0.3926948E-05  0.3698739E-05  0.2408722E-05 -0.1992519E+00  0.1453220E-05
  eigenvectors of nos in ao-basis, column   15
  0.6188134E-03  0.2366564E-02 -0.4557494E-02  0.5040674E+00  0.1749601E-05 -0.2786298E-03 -0.1296589E+01 -0.1879479E-05
  0.2003991E-02 -0.6414156E-05 -0.1799324E-05  0.3445005E-04  0.8053439E+00 -0.6676649E-03 -0.1396966E+01  0.7198505E+00
 -0.7170155E-01 -0.8086872E-05  0.2571439E+00  0.1404369E+01 -0.7240104E+00 -0.7192678E-01  0.5051876E-05 -0.2567491E+00
  eigenvectors of nos in ao-basis, column   16
 -0.3571128E+00 -0.1354257E+01  0.2704752E+01  0.8290310E-03  0.3427676E-05  0.1232872E+00 -0.2124721E-02 -0.5384878E-05
 -0.1153531E+01  0.1549495E-05  0.1674466E-05 -0.2990618E-01  0.1325623E-02  0.4145520E+00 -0.2255195E+01  0.1269380E+01
 -0.7484304E-01  0.9038143E-06 -0.1588119E+00 -0.2250620E+01  0.1267065E+01  0.7457765E-01 -0.3083441E-06 -0.1596914E+00
  eigenvectors of nos in ao-basis, column   17
  0.4090124E-05  0.1941206E-04 -0.7380156E-05 -0.2280305E-04 -0.1082434E-04 -0.9733137E-05 -0.1863037E-04  0.9584658E-04
 -0.3597775E-05  0.7228379E+00 -0.7384673E-04  0.2623752E-05  0.2635737E-04  0.8130741E-06 -0.9283713E-04  0.7651847E-04
 -0.2042693E-04  0.7383490E+00 -0.1996836E-04  0.5095484E-04 -0.4444166E-04  0.3619135E-05 -0.7385447E+00  0.2135183E-04
  eigenvectors of nos in ao-basis, column   18
  0.1060826E-03  0.4822070E-03 -0.1840012E-03  0.8965959E+00 -0.4581934E-05 -0.2800467E-03  0.1230875E+00  0.2866341E-05
  0.1069446E-03  0.2184067E-04  0.3621567E-05  0.9452036E-05 -0.6640312E+00  0.4856575E-04  0.1766668E+01 -0.1628705E+01
  0.3906253E+00  0.2869274E-04  0.6918313E+00 -0.1767374E+01  0.1629258E+01  0.3910805E+00 -0.1892628E-04 -0.6921246E+00
  eigenvectors of nos in ao-basis, column   19
 -0.2196939E+00 -0.1068699E+01 -0.1324105E+00  0.8436784E-04 -0.3289083E-04  0.7069132E+00  0.3517183E-03  0.3818382E-03
  0.3258734E+00  0.9463224E-05 -0.2734306E-03 -0.1245591E+00 -0.1319318E-03 -0.1193583E+00  0.1586139E+01 -0.1110483E+01
  0.9006815E+00 -0.3945570E-03  0.1627552E+00  0.1585058E+01 -0.1109691E+01 -0.9008358E+00 -0.4168689E-03  0.1621946E+00
  eigenvectors of nos in ao-basis, column   20
 -0.1035610E-03 -0.5044547E-03 -0.6355670E-04 -0.1627575E-04  0.5865434E-01  0.3290261E-03  0.2054109E-04 -0.7811216E+00
  0.1707063E-03  0.7990197E-04  0.5703422E+00 -0.6156398E-04  0.7727449E-05 -0.5524208E-04  0.7527387E-03 -0.5224413E-03
  0.4153654E-03  0.8454071E+00  0.7163536E-04  0.7685272E-03 -0.5472066E-03 -0.4467028E-03  0.8452295E+00  0.6534577E-04
  eigenvectors of nos in ao-basis, column   21
 -0.2950653E-03 -0.1266494E-02  0.9586206E-03  0.6111257E+00  0.5707486E-06  0.5772464E-03 -0.1255329E+01 -0.1367074E-04
 -0.7713902E-03 -0.2466357E-05  0.9840792E-05  0.1104890E-03 -0.1449342E+00 -0.1493331E-03 -0.3207282E+00 -0.9896146E-01
  0.7440339E+00  0.1106799E-04 -0.3583075E+00  0.3210659E+00  0.9845015E-01  0.7436946E+00  0.1937213E-04  0.3596485E+00
  eigenvectors of nos in ao-basis, column   22
  0.2980500E+00  0.1240714E+01 -0.1224172E+01  0.3389143E-03  0.3373528E-05 -0.4879877E+00 -0.8979729E-03  0.2115089E-05
  0.1039693E+01  0.1492988E-05 -0.6567599E-05 -0.1783150E+00 -0.9023981E-04  0.1581932E+00  0.2442247E+00  0.9954293E-02
  0.7764334E-01 -0.6718885E-05 -0.8024652E+00  0.2449310E+00  0.9779916E-02 -0.7655233E-01 -0.1122233E-04 -0.8016799E+00
  eigenvectors of nos in ao-basis, column   23
 -0.3242777E+00 -0.1119857E+01  0.3944605E+01  0.3402078E-04 -0.6917624E-05 -0.3605357E+00  0.1363526E-03  0.1265210E-04
 -0.9552461E+00  0.4887754E-06 -0.9217093E-06  0.2200646E-01 -0.5498293E-04  0.1880228E+00 -0.1197333E+01 -0.4574463E+00
 -0.7612254E+00 -0.7786997E-06 -0.5069418E+00 -0.1197514E+01 -0.4576379E+00  0.7613628E+00 -0.3521416E-05 -0.5070484E+00
  eigenvectors of nos in ao-basis, column   24
 -0.2484138E-04 -0.8654846E-04  0.3110885E-03 -0.3608410E+00 -0.3490808E-05 -0.3145826E-04 -0.1588442E+01  0.5312810E-05
 -0.6206637E-04  0.7658489E-06 -0.5273084E-07  0.4647656E-07  0.5570679E+00  0.1381970E-04 -0.9416745E+00 -0.1209860E+01
 -0.7214827E+00  0.1369986E-05 -0.6511868E+00  0.9415291E+00  0.1209747E+01 -0.7213649E+00 -0.8522474E-06  0.6510924E+00


 total number of electrons =   10.0000000000



          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!


                        A   partial gross atomic populations
   ao class       1A         2A         3A         4A         5A         6A  
    O1_ s       2.000675   1.664319   0.000000  -0.044679   0.000000   0.000000
    O1_ p       0.000085   0.037044   1.931170   1.494859   1.155245   0.011302
    O1_ d       0.000000   0.000191   0.000982   0.001835   0.008187   0.000440
    H1_ s      -0.000124   0.115035   0.000000   0.239192   0.388662   0.005213
    H1_ p      -0.000297   0.027456   0.022066   0.020980   0.014433  -0.000286
    H2_ s      -0.000124   0.115038   0.000000   0.238989   0.388914   0.005220
    H2_ p      -0.000297   0.027456   0.022067   0.020987   0.014429  -0.000287

   ao class       7A         8A         9A        10A        11A        12A  
    O1_ s       0.001580   0.000000   0.005210   0.000000   0.001125   0.000000
    O1_ p       0.009170   0.013978   0.004036   0.001908   0.000061   0.000000
    O1_ d       0.000275   0.000036   0.000292   0.002660   0.002719   0.003577
    H1_ s       0.004715   0.000000   0.000236   0.000249   0.000117   0.000000
    H1_ p      -0.000304   0.000299   0.000152   0.000176   0.000511   0.000581
    H2_ s       0.004708   0.000000   0.000236   0.000248   0.000117   0.000000
    H2_ p      -0.000304   0.000299   0.000152   0.000176   0.000511   0.000581

   ao class      13A        14A        15A        16A        17A        18A  
    O1_ s       0.000201   0.000000   0.000000   0.000205   0.000000   0.000000
    O1_ p       0.000243   0.000085   0.000264   0.000168   0.000000   0.000004
    O1_ d       0.003382   0.003560   0.000383   0.000378   0.000161   0.000054
    H1_ s       0.000012   0.000000   0.000079   0.000155   0.000000   0.000103
    H1_ p       0.000183   0.000241   0.000104  -0.000025   0.000248   0.000153
    H2_ s       0.000012   0.000000   0.000080   0.000155   0.000000   0.000103
    H2_ p       0.000183   0.000241   0.000104  -0.000025   0.000248   0.000153

   ao class      19A        20A        21A        22A        23A        24A  
    O1_ s       0.000008   0.000000   0.000000   0.000011   0.000004   0.000000
    O1_ p      -0.000007   0.000022   0.000058   0.000037   0.000003   0.000005
    O1_ d       0.000035   0.000066   0.000000   0.000075   0.000000   0.000000
    H1_ s       0.000032   0.000000   0.000007   0.000007   0.000015   0.000008
    H1_ p       0.000196   0.000202   0.000188   0.000147   0.000006   0.000006
    H2_ s       0.000032   0.000000   0.000007   0.000007   0.000015   0.000008
    H2_ p       0.000196   0.000202   0.000189   0.000147   0.000006   0.000006


                        gross atomic populations
     ao           O1_        H1_        H2_
      s         3.628659   0.753713   0.753765
      p         4.659741   0.087414   0.087419
      d         0.029288   0.000000   0.000000
    total       8.317689   0.841127   0.841184


 Total number of electrons:   10.00000000

