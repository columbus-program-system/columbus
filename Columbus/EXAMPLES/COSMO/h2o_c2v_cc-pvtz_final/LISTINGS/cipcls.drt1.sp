
 program cipc      

 print the csf info for mrsdci wave functions

 written by: ron shepard

 version date: 06-jun-96

 This Version of Program cipc is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 workspace allocation parameters: lencor=  10000000 mem1=********** ifirst= -11229020

 drt header information:
  cidrt_title                                                                    
 nmot  =    48 niot  =     5 nfct  =     0 nfvt  =     0
 nrow  =    20 nsym  =     4 ssym  =     1 lenbuf=  1600
 spnorb=     F spnodd=     F lxyzir(1:3)= 0 0 0
 nwalk,xbar:         31        1        5       10       15
 nvalwt,nvalw:       31        1        5       10       15
 ncsft:            6339
 map(*)=    44 45 46  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17
            47 18 19 20 21 22 23 24 25 26 27 28 29 30 48 31 32 33 34 35
            36 37 38 39 40 41 42 43
 mu(*)=      0  0  0  0  0
 syml(*) =   1  1  1  2  3
 rmo(*)=     1  2  3  1  1

 indx01:    31 indices saved in indxv(*)
===================================ROOT # 1===================================

 rdhciv: CI vector file information:
 Hermit Integral Program : SIFS version  ronja.itc.univieFri Sep  2 17:00:43 2005
  cidrt_title                                                                    
  title                                                                          
 mofmt: formatted orbitals label=morbl   ronja.itc.univieFri Sep  2 17:00:45 2005
 SIFS file created by program tran.      ronja.itc.univieFri Sep  2 17:00:45 2005
 energy computed by program ciudg.       ronja.itc.univieFri Sep  2 17:01:03 2005

 lenrec =   32768 lenci =      6339 ninfo =  6 nenrgy =  5 ntitle =  6

 Max. overlap with ref vector #        1
 Valid ci vector #        1
 Method:        0       97% overlap
 energy( 1)=  9.317913916904E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -7.633804685955E+01, ietype=-1026,   total energy of type: MRSDCI  
 energy( 3)=  7.947729672584E-05, ietype=-2055, cnvginf energy of type: CI-Resid
 energy( 4)=  2.515277248705E-07, ietype=-2056, cnvginf energy of type: CI-D.E. 
 energy( 5)=  1.568602155410E-09, ietype=-2057, cnvginf energy of type: CI-ApxDE
==================================================================================

 space is available for   4983280 coefficients.

 updated histogram parameters:
 csfmn = 0.0000E+00 csfmx = 1.0000E+00 fhist = 5.0000E-01 nhist =  20

 this program will print the csfs generated from
 the drt according to the following print options :

 1) run in batch mode: all valid roots are automatically
    analysed and csf info is printed by default contribution
    threshold 0.01 
 2) run in interactive mode

 input menu number [  0]:

================================================================================
===================================VECTOR # 1===================================
================================================================================


 rdcivnew:     116 coefficients were selected.
 workspace: ncsfmx=    6339
 ncsfmx= 6339

 histogram parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =    6339 ncsft =    6339 ncsf =     116
 nhist =  20 fhist = 0.50000

    cmin                cmax        num  '*'=     2 csfs.
 ----------          ----------   ----- ---------|---------|---------|---------|
 5.0000E-01 <= |c| < 1.0000E+00       1 *
 2.5000E-01 <= |c| < 5.0000E-01       0
 1.2500E-01 <= |c| < 2.5000E-01       0
 6.2500E-02 <= |c| < 1.2500E-01       0
 3.1250E-02 <= |c| < 6.2500E-02       0
 1.5625E-02 <= |c| < 3.1250E-02      37 *******************
 7.8125E-03 <= |c| < 1.5625E-02      78 ***************************************
 3.9062E-03 <= |c| < 7.8125E-03       0
 1.9531E-03 <= |c| < 3.9062E-03       0
 9.7656E-04 <= |c| < 1.9531E-03       0
 4.8828E-04 <= |c| < 9.7656E-04       0
 2.4414E-04 <= |c| < 4.8828E-04       0
 1.2207E-04 <= |c| < 2.4414E-04       0
 6.1035E-05 <= |c| < 1.2207E-04       0
 3.0518E-05 <= |c| < 6.1035E-05       0
 1.5259E-05 <= |c| < 3.0518E-05       0
 7.6294E-06 <= |c| < 1.5259E-05       0
 3.8147E-06 <= |c| < 7.6294E-06       0
 0.0000E+00 <= |c| < 3.8147E-06       0
                                  ----- ---------|---------|---------|---------|
                  total read =      116 total stored =     116

 from the selected csfs,
 min(|csfvec(:)|) = 1.0019E-02    max(|csfvec(:)|) = 9.7232E-01
 norm=  1.
 csfs will be printed based on coefficient magnitudes.

 current csfvec(*) selection parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =    6339 ncsft =    6339 ncsf =     116

 i:slabel(i) =  1: a1   2: b1   3: b2   4: a2 

 internal level =    1    2    3    4    5
 syml(*)        =    1    1    1    2    3
 label          =  a1   a1   a1   b1   b2 
 rmo(*)         =    1    2    3    1    1

 printing selected csfs in sorted order from cmin = 0.01000 to cmax = 1.00000

   indcsf     c     c**2   v  lab:rmo  lab:rmo   step(*)
  ------- -------- ------- - ---- --- ---- --- ------------
        1  0.97232 0.94540 z*                    33333
     3056 -0.03067 0.00094 w           b1 :  6  333303
     3457 -0.03047 0.00093 w  a1 :  8  b1 :  6 1233123
     2651 -0.03032 0.00092 w           b2 :  3  333330
     3053 -0.02987 0.00089 w  b1 :  3  b1 :  6 1233303
     2650 -0.02600 0.00068 w  b2 :  2  b2 :  3 1233330
      538 -0.02403 0.00058 x  a1 : 10  b1 :  6 1133223
     3406 -0.02271 0.00052 w  a1 :  8  b1 :  3 1233123
     2685 -0.02090 0.00044 w           a2 :  1  333330
      285  0.02041 0.00042 x  a1 :  9  b2 :  3 1133232
     3044 -0.01961 0.00038 w           b1 :  3  333303
     3403  0.01943 0.00038 w  a1 :  5  b1 :  3 1233123
     4214 -0.01890 0.00036 w  a1 :  8  b1 :  6 1231323
     3454  0.01875 0.00035 w  a1 :  5  b1 :  6 1233123
     4414  0.01874 0.00035 w  a1 :  5  a1 :  8 1231233
     3657  0.01849 0.00034 w  a1 :  5  a1 :  8 1233033
      185  0.01843 0.00034 x  a1 : 11  a2 :  1 1133322
      304  0.01831 0.00034 x  a1 : 11  b2 :  4 1133232
     3206 -0.01818 0.00033 w  a1 :  9  b2 :  3 1233132
      182  0.01815 0.00033 x  a1 :  8  a2 :  1 1133322
     2903 -0.01808 0.00033 w           a1 :  8  333303
     3813 -0.01805 0.00033 w           b1 :  6  333033
      534 -0.01793 0.00032 x  a1 :  6  b1 :  6 1133223
     3666 -0.01792 0.00032 w           a1 :  9  333033
     2649 -0.01777 0.00032 w           b2 :  2  333330
       92 -0.01707 0.00029 x  b1 :  7  b2 :  3 1133322
     3810 -0.01695 0.00029 w  b1 :  3  b1 :  6 1233033
     3660 -0.01694 0.00029 w           a1 :  8  333033
       75 -0.01691 0.00029 x  b1 :  3  b2 :  2 1133322
     4417 -0.01686 0.00028 w           a1 :  8  331233
     2718  0.01655 0.00027 w  b1 :  7  b2 :  3 1233312
      104  0.01649 0.00027 x  b1 :  6  b2 :  4 1133322
     3169 -0.01632 0.00027 w           a2 :  1  333303
      483 -0.01616 0.00026 x  a1 :  6  b1 :  3 1133223
     2701  0.01604 0.00026 w  b1 :  3  b2 :  2 1233312
      267 -0.01587 0.00025 x  a1 :  8  b2 :  2 1133232
      403  0.01575 0.00025 x  b1 :  6  a2 :  1 1133232
     2440 -0.01572 0.00025 w           a1 : 11  333330
       78 -0.01540 0.00024 x  b1 :  6  b2 :  2 1133322
     3959 -0.01537 0.00024 w  a1 :  5  b2 :  3 1231332
     3962  0.01531 0.00023 w  a1 :  8  b2 :  3 1231332
     2704  0.01529 0.00023 w  b1 :  6  b2 :  2 1233312
     3205  0.01523 0.00023 w  a1 :  8  b2 :  3 1233132
     3324 -0.01512 0.00023 w  b1 :  6  a2 :  1 1233132
     2900  0.01500 0.00022 w  a1 :  5  a1 :  8 1233303
      264  0.01462 0.00021 x  a1 :  5  b2 :  2 1133232
     3673 -0.01460 0.00021 w           a1 : 10  333033
     2654 -0.01456 0.00021 w           b2 :  4  333330
     2730 -0.01436 0.00021 w  b1 :  6  b2 :  4 1233312
      303 -0.01430 0.00020 x  a1 : 10  b2 :  4 1133232
      487 -0.01419 0.00020 x  a1 : 10  b1 :  3 1133223
      284 -0.01417 0.00020 x  a1 :  8  b2 :  3 1133232
     2660 -0.01391 0.00019 w  b2 :  3  b2 :  6 1233330
     3189 -0.01386 0.00019 w  a1 :  9  b2 :  2 1233132
     2714  0.01382 0.00019 w  b1 :  3  b2 :  3 1233312
       88 -0.01379 0.00019 x  b1 :  3  b2 :  3 1133322
     3966 -0.01373 0.00019 w  a1 : 12  b2 :  3 1231332
     3608 -0.01370 0.00019 w  b2 :  4  a2 :  1 1233123
     3062 -0.01367 0.00019 w           b1 :  7  333303
     4220 -0.01338 0.00018 w  a1 : 14  b1 :  6 1231323
      183  0.01328 0.00018 x  a1 :  9  a2 :  1 1133322
     3961 -0.01326 0.00018 w  a1 :  7  b2 :  3 1231332
     3648 -0.01323 0.00018 w           a1 :  5  333033
     4712 -0.01322 0.00017 w           a1 :  8  330333
     4709  0.01321 0.00017 w  a1 :  5  a1 :  8 1230333
      186 -0.01321 0.00017 x  a1 : 12  a2 :  1 1133322
      687  0.01314 0.00017 x  b2 :  4  a2 :  1 1133223
      589  0.01307 0.00017 x  a1 : 10  b1 :  9 1133223
      553  0.01304 0.00017 x  a1 :  8  b1 :  7 1133223
     3475  0.01299 0.00017 w  a1 :  9  b1 :  7 1233123
      301  0.01292 0.00017 x  a1 :  8  b2 :  4 1133232
     3665  0.01272 0.00016 w  a1 :  8  a1 :  9 1233033
     3895 -0.01254 0.00016 w           b2 :  4  333033
     2717  0.01246 0.00016 w  b1 :  6  b2 :  3 1233312
       11  0.01236 0.00015 y           b1 :  3  133323
     2810 -0.01232 0.00015 w  a1 : 10  a2 :  1 1233312
     2916 -0.01228 0.00015 w           a1 : 10  333303
     4228  0.01217 0.00015 w  a1 :  5  b1 :  7 1231323
     4405 -0.01207 0.00015 w           a1 :  5  331233
     2652  0.01193 0.00014 w  b2 :  2  b2 :  4 1233330
     3831  0.01192 0.00014 w  b1 :  6  b1 :  9 1233033
     3460 -0.01191 0.00014 w  a1 : 11  b1 :  6 1233123
       14  0.01190 0.00014 y           b1 :  6  133323
     4163 -0.01187 0.00014 w  a1 :  8  b1 :  3 1231323
      281  0.01186 0.00014 x  a1 :  5  b2 :  3 1133232
     4160  0.01186 0.00014 w  a1 :  5  b1 :  3 1231323
       26 -0.01178 0.00014 y           a1 :  7  133233
      749  0.01166 0.00014 x  a1 : 11  b2 :  3 1132332
     2663 -0.01165 0.00014 w           b2 :  6  333330
     3058 -0.01161 0.00013 w  b1 :  3  b1 :  7 1233303
     4215 -0.01160 0.00013 w  a1 :  9  b1 :  6 1231323
     4211  0.01160 0.00013 w  a1 :  5  b1 :  6 1231323
     4282  0.01154 0.00013 w  a1 :  8  b1 : 10 1231323
     3059  0.01133 0.00013 w  b1 :  4  b1 :  7 1233303
      184  0.01126 0.00013 x  a1 : 10  a2 :  1 1133322
     3945  0.01124 0.00013 w  a1 :  8  b2 :  2 1231332
     3074  0.01122 0.00013 w  b1 :  6  b1 :  9 1233303
       89  0.01121 0.00013 x  b1 :  4  b2 :  3 1133322
     3224  0.01120 0.00013 w  a1 : 10  b2 :  4 1233132
      486 -0.01113 0.00012 x  a1 :  9  b1 :  3 1133223
     3185 -0.01113 0.00012 w  a1 :  5  b2 :  2 1233132
     4436 -0.01112 0.00012 w  a1 :  9  a1 : 11 1231233
     3664  0.01098 0.00012 w  a1 :  7  a1 :  9 1233033
     4234  0.01095 0.00012 w  a1 : 11  b1 :  7 1231323
     4016 -0.01093 0.00012 w  a1 : 11  b2 :  6 1231332
     3669 -0.01078 0.00012 w  a1 :  6  a1 : 10 1233033
     3204  0.01072 0.00011 w  a1 :  7  b2 :  3 1233132
     3942 -0.01072 0.00011 w  a1 :  5  b2 :  2 1231332
       91 -0.01065 0.00011 x  b1 :  6  b2 :  3 1133322
      554 -0.01041 0.00011 x  a1 :  9  b1 :  7 1133223
     2901 -0.01036 0.00011 w  a1 :  6  a1 :  8 1233303
     2715 -0.01031 0.00011 w  b1 :  4  b2 :  3 1233312
     3188  0.01029 0.00011 w  a1 :  8  b2 :  2 1233132
     3949 -0.01018 0.00010 w  a1 : 12  b2 :  2 1231332
     4421 -0.01009 0.00010 w  a1 :  7  a1 :  9 1231233
      537 -0.01002 0.00010 x  a1 :  9  b1 :  6 1133223
          116 csfs were printed in this range.
