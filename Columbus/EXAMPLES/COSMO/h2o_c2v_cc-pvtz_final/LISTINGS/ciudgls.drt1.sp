1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      09/20/00    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 using llenci= -1
================================================================================
four external integ    1.25 MB location: local disk    
three external inte    0.50 MB location: local disk    
four external integ    1.25 MB location: local disk    
three external inte    0.50 MB location: local disk    
diagonal integrals     0.12 MB location: local disk    
off-diagonal integr    0.19 MB location: local disk    
computed file size in DP units
fil3w:       65534
fil3x:       65534
fil4w:      163835
fil4x:      163835
ofdgint:       16380
diagint:       24570
computed file size in DP units
fil3w:      524272
fil3x:      524272
fil4w:     1310680
fil4x:     1310680
ofdgint:      131040
diagint:      196560
 nsubmx= 2 lenci= 6339
global arrays:        31695   (              0.24 MB)
vdisk:                    0   (              0.00 MB)
drt:                 252658   (              0.96 MB)
================================================================================
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core            9999999 DP per process
 CIUDG version 5.9.7 ( 5-Oct-2004)
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 0,
  GSET = 0,
   DAVCOR =10,
  NCOREL = 10
  NROOT = 1
  IVMODE = 3
  NBKITR = 0
  NVBKMN = 1
  NVBKMX = 6
  RTOLBK = 1e-3,
  NITER = 20
  NVCIMN = 1
  NVCIMX = 2
  RTOLCI = 1e-4,
  IDEN  = 1
  CSFPRN = 10,
  frcsub = 2
  cosmocalc = 1
  /end
 ------------------------------------------------------------------------

 ** list of control variables **
 nrfitr =   30      nvrfmx =    2      nvrfmn =    1
 lvlprt =    0      nroot  =    1      noldv  =    0      noldhv =    0
 nunitv =    1      ntype  =    0      nbkitr =    0      niter  =   20
 ivmode =    3      vout   =    0      istrt  =    0      iortls =    0
 nvbkmx =    2      ibktv  =   -1      ibkthv =   -1      frcsub =    2
 nvcimx =    2      icitv  =   -1      icithv =   -1      maxseg =    4
 iden   =    1      itran  =    0      froot  =    0      rtmode =    0
 ftcalc =    1      lrtshift=1.0000    ncouple=    1      skipso  =   F
 ncorel =   10      csfprn  =  10      ctol   = 1.00E-02  davcor  =  10


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-03    1.000E-04

 units and filenames:
    1: ( 6)    ciudgls                                                     
    2: ( 5)    ciudgin                                                     
    3: ( 7)    ciudgsm                                                     
    4: ( 4)    cihdiag                                                     
    5: ( 3)    cihpseu                                                     
    6: (10)    cihvfl                                                      
    7: (11)    civfl                                                       
    8: (12)    diagint                                                     
    9: (13)    ofdgint                                                     
   10: (17)    cidrtfl                                                     
   11: (20)    ciftdfl                                                     
   12: (21)    ciftofl                                                     
   13: (24)    ciftifl                                                     
   14: ( 8)    ciflind                                                     
   15: (15)    civin                                                       
   16: (16)    civout                                                      
   18: (18)    d1fl                                                        
   20: (53)    cifvfl                                                      
   21: (25)    ciscr4                                                      
   22: (41)    ciftotd                                                     
   23: (42)    flacpfd                                                     
   26: (26)    ciscr5                                                      
   27: (47)    restart                                                     
   28: (48)    civout                                                      
   29: (49)    cirefv                                                      
   31: (31)    fil4w                                                       
   32: (32)    fil4x                                                       
   33: (33)    fil3w                                                       
   34: (34)    fil3x                                                       
   36: (36)    aoints                                                      
   37: (37)    aoints2                                                     
   38: (38)    drtfil                                                      
   39: (50)    cisrtif                                                     
   42: (50)    mocoef                                                      
   45: (52)    nocoef_ci                                                   
   46: (53)    civfl_restart                                               
 ------------------------------------------------------------------------

 workspace allocation information: lcore=   9999999 mem1=********** ifirst= -11267050

 integral file titles:
 Hermit Integral Program : SIFS version  ronja.itc.univieFri Sep  2 17:00:43 2005
  cidrt_title                                                                    
  title                                                                          
 mofmt: formatted orbitals label=morbl   ronja.itc.univieFri Sep  2 17:00:45 2005
 SIFS file created by program tran.      ronja.itc.univieFri Sep  2 17:00:45 2005

 core energy values from the integral file:
 energy( 1)=  9.317913916904E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total core repulsion energy =  9.317913916904E+00

 drt header information:
  cidrt_title                                                                    
 spnorb, spnodd, lxyzir,hmult F F 0 0 0 0
 nmot  =    48 niot  =     5 nfct  =     0 nfvt  =     0
 nrow  =    20 nsym  =     4 ssym  =     1 lenbuf=  1600
 nwalk,xbar:         31        1        5       10       15
 nvalwt,nvalw:       31        1        5       10       15
 ncsft:            6339
 total number of valid internal walks:      31
 nvalz,nvaly,nvalx,nvalw =        1       5      10      15

 cisrt info file parameters:
 file number  12 blocksize   4095
 mxbld   4095
 nd4ext,nd2ext,nd0ext  1892   430    30
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int   127551    56163    11805     1365       66        0        0        0
 minbl4,minbl3,maxbl2   882   837   885
 maxbuf 32767
 number of external orbitals per symmetry block:  17  13   8   5
 nmsym   4 number of internal orbitals   5

 formula file title:
 Hermit Integral Program : SIFS version  ronja.itc.univieFri Sep  2 17:00:43 2005
  cidrt_title                                                                    
  title                                                                          
 mofmt: formatted orbitals label=morbl   ronja.itc.univieFri Sep  2 17:00:45 2005
 SIFS file created by program tran.      ronja.itc.univieFri Sep  2 17:00:45 2005
  cidrt_title                                                                    
 file nmb.  24 block size     0
 pthz,pthy,pthx,pthw:     1     5    10    15 total internal walks:      31
 maxlp3,n3xtlp,n2xtlp,n1xtlp,n0xtlp,n2lp,n1lp,n0lp     0     0     0     0
 orbsym(*)= 1 1 1 2 3

 setref:        1 references kept,
                0 references were marked as invalid, out of
                1 total.
 limcnvrt: found 1 valid internal walksout of  1
  walks (skipping trailing invalids)
  ... adding  1 segmentation marks segtype= 1
 limcnvrt: found 5 valid internal walksout of  5
  walks (skipping trailing invalids)
  ... adding  5 segmentation marks segtype= 2
 limcnvrt: found 10 valid internal walksout of  10
  walks (skipping trailing invalids)
  ... adding  10 segmentation marks segtype= 3
 limcnvrt: found 15 valid internal walksout of  15
  walks (skipping trailing invalids)
  ... adding  15 segmentation marks segtype= 4

 number of external paths / symmetry
 vertex x     252     261     201     189
 vertex w     295     261     201     189

 lprune: l(*,*,*) pruned with nwalk=       1 nvalwt=       1 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=       5 nvalwt=       4 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=      10 nvalwt=      10 nprune=      14

 lprune: l(*,*,*) pruned with nwalk=      15 nvalwt=      15 nprune=       7



                   segmentation summary for type diagonal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1           1|         1|         0|         1|         0|         1|
 -------------------------------------------------------------------------------
  Y 2           5|        72|         1|         5|         1|         2|
 -------------------------------------------------------------------------------
  X 3          10|      2331|        73|        10|         6|         3|
 -------------------------------------------------------------------------------
  W 4          15|      3935|      2404|        15|        16|         4|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>      6339

                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      10       1       2331          1      10       1
     2  4   1    25      two-ext wz   2X  4 1      15       1       3935          1      15       1
     3  4   3    26      two-ext wx   2X  4 3      15      10       3935       2331      15      10
     4  2   1    11      one-ext yz   1X  2 1       5       1         72          1       5       1
     5  3   2    15      1ex3ex  yx   3X  3 2      10       5       2331         72      10       5
     6  4   2    16      1ex3ex  yw   3X  4 2      15       5       3935         72      15       5
     7  1   1     1      allint zz    OX  1 1       1       1          1          1       1       1
     8  2   2     5      0ex2ex yy    OX  2 2       5       5         72         72       5       5
     9  3   3     6      0ex2ex xx    OX  3 3      10      10       2331       2331      10      10
    10  4   4     7      0ex2ex ww    OX  4 4      15      15       3935       3935      15      15
    11  1   1    75      dg-024ext z  DG  1 1       1       1          1          1       1       1
    12  2   2    45      4exdg024 y   DG  2 2       5       5         72         72       5       5
    13  3   3    46      4exdg024 x   DG  3 3      10      10       2331       2331      10      10
    14  4   4    47      4exdg024 w   DG  4 4      15      15       3935       3935      15      15
----------------------------------------------------------------------------------------------------
 DIAGEL COUNTS: DG0X,DG2X,DG4X= 440 145 30
 diagonal elements written to file   4

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        15 2x:         0 4x:         0
All internal counts: zz :         0 yy:         0 xx:         0 ww:         0
One-external counts: yz :         0 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         0    task #   2:         0    task #   3:         0    task #   4:         0
task #   5:         0    task #   6:         0    task #   7:        -1    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:        14    task #  12:         0
task #  13:         0    task #  14:         0    task #
 reference space has dimension       1

    root           eigenvalues
    ----           ------------
       1         -76.0536656576

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt= 1

         vector  1 from unit 11 written to unit 49 filename cirefv                                                      
 ufvoutnew: ... writing  recamt= 1

         vector  1 from unit 11 written to unit 49 filename cirefv                                                      

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              6339
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               2
 number of roots to converge:                             1
 number of iterations:                                   20
 residual norm convergence criteria:               0.000100

 elast before the main iterative loop 
 elast =  -76.0536657


          starting ci iteration   1

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.31791392

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= T F

################################################################################
 one electron density matrix calculation 
################################################################################


 workspace allocation information: mxcore= 9999997

 space required:

    space required for calls in multd2:
       onex          391
       allin           0
       diagon       1475
    max.      ---------
       maxnex       1475

    total core space usage:
       maxnex       1475
    max k-seg       3935
    max k-ind         15
              ---------
       totmax       9375
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=       3  D0X=       9  D0W=      15
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   2.0000000      2.0000000      2.0000000      0.0000000      0.0000000
   0.0000000      0.0000000      0.0000000      0.0000000      0.0000000
   0.0000000      0.0000000      0.0000000      0.0000000      0.0000000
   0.0000000      0.0000000      0.0000000      0.0000000      0.0000000


*****   symmetry block SYM2   *****

 occupation numbers of nos
   2.0000000      0.0000000      0.0000000      0.0000000      0.0000000
   0.0000000      0.0000000      0.0000000      0.0000000      0.0000000
   0.0000000      0.0000000      0.0000000      0.0000000


*****   symmetry block SYM3   *****

 occupation numbers of nos
   2.0000000      0.0000000      0.0000000      0.0000000      0.0000000
   0.0000000      0.0000000      0.0000000      0.0000000


*****   symmetry block SYM4   *****

 occupation numbers of nos
   0.0000000      0.0000000      0.0000000      0.0000000      0.0000000


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***

  Confirmation of dielectric energy for ground state
  edielnew =   0.


 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************

 *** qcos is copied to qcosdalton ***

 fepsi =   1.
 *** qcosdalton is mult by fepsi ***

 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 Warning! Large contrib. to nucrep.The distance is   1.37057673E-05
 ** Total nuclear repulsion energy ** =   9.34367465521637
 Warning! Large contrib. to nucrep.The distance is   1.37057673E-05
 screening nuclear repulsion energy   0.00759477565

 Total-screening nuclear repulsion energy   9.33607988


 Adding T+Vsolv ...
 maxdens  375
 *** End of DALTON-COSMO calculation ***

 DIAGEL COUNTS: DG0X,DG2X,DG4X= 440 145 30
 =========== Executing IN-CORE method ==========
 norm multnew:  1.


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        12 xx:        36 ww:        79
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        11
task #   9:        28    task #  10:        71    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 rtolcosmo = eo,e,r,ro   0. -85.3715796 -85.4087518  9.33607988  9.31791392
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -76.0726719487  1.9006E-02  3.1915E-01  1.4431E+00  1.0000E-04


dielectric energy =      -0.0095031456
delta diel. energy =       0.0095031456
total Cosmo energy: e(nroot) + repnuc - ediel =     -76.0631688032
delta Cosmo energy =      76.0631688032

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.00   0.00   0.00   0.00         0.    0.0000
    2   25    0   0.00   0.00   0.00   0.00         0.    0.0000
    3   26    0   0.01   0.00   0.00   0.01         0.    0.0000
    4   11    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   15    0   0.01   0.00   0.00   0.01         0.    0.0000
    6   16    0   0.00   0.00   0.00   0.00         0.    0.0000
    7    1    0   0.00   0.00   0.00   0.00         0.    0.0000
    8    5    0   0.01   0.00   0.00   0.01         0.    0.0000
    9    6    0   0.00   0.00   0.00   0.00         0.    0.0000
   10    7    0   0.01   0.00   0.00   0.01         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.01   0.00   0.00   0.01         0.    0.0000
   14   47    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0600s 
time spent in multnx:                   0.0600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.4600s 

          starting ci iteration   2

 =========== Executing IN-CORE method ==========
 norm multnew:  0.0868834472


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        12 xx:        36 ww:        79
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        11
task #   9:        28    task #  10:        71    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97358879    -0.22830870

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1    -76.3265816350  2.5391E-01  1.0861E-02  2.7085E-01  1.0000E-04
 mr-sdci #  2  2    -71.4553998143  8.0791E+01  0.0000E+00  3.6593E+00  1.0000E-04


dielectric energy =      -0.0095031456
delta diel. energy =       0.0000000000
total Cosmo energy: e(nroot) + repnuc - ediel =     -76.3170784894
delta Cosmo energy =       0.2539096862

 root number  1 is used to define the new expansion vector.
 diagon: frcsub=2, new matrix-vector products will be computed.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.00   0.00   0.00   0.00         0.    0.0000
    2   25    0   0.00   0.00   0.00   0.00         0.    0.0000
    3   26    0   0.01   0.00   0.00   0.01         0.    0.0000
    4   11    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   15    0   0.00   0.00   0.00   0.00         0.    0.0000
    6   16    0   0.01   0.00   0.00   0.00         0.    0.0000
    7    1    0   0.00   0.00   0.00   0.00         0.    0.0000
    8    5    0   0.01   0.00   0.00   0.01         0.    0.0000
    9    6    0   0.00   0.00   0.00   0.00         0.    0.0000
   10    7    0   0.01   0.00   0.00   0.01         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.01   0.00   0.00   0.01         0.    0.0000
   14   47    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0600s 
time spent in multnx:                   0.0500s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.0800s 

          starting ci iteration   3

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.33607988

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=       3  D0X=       9  D0W=      15
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   1.9997094      1.9870606      1.9701232      0.0194494      0.0097882
   0.0053362      0.0045485      0.0008814      0.0007081      0.0004377
   0.0003373      0.0002621      0.0002549      0.0001274      0.0000940
   0.0000594      0.0000472      0.0000293      0.0000106      0.0000007


*****   symmetry block SYM2   *****

 occupation numbers of nos
   1.9698795      0.0211600      0.0052597      0.0008179      0.0006564
   0.0004791      0.0002884      0.0001317      0.0000880      0.0000808
   0.0000174      0.0000129      0.0000047      0.0000003


*****   symmetry block SYM3   *****

 occupation numbers of nos
   1.9705970      0.0185096      0.0046057      0.0006590      0.0004704
   0.0002894      0.0002618      0.0000960      0.0000558


*****   symmetry block SYM4   *****

 occupation numbers of nos
   0.0051211      0.0007495      0.0003663      0.0000631      0.0000129


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***

  Confirmation of dielectric energy for ground state
  edielnew =  -0.00256506366


 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************

 *** qcos is copied to qcosdalton ***

 fepsi =   1.
 *** qcosdalton is mult by fepsi ***

 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 Warning! Large contrib. to nucrep.The distance is   1.37057673E-05
 ** Total nuclear repulsion energy ** =   9.34766629197819
 Warning! Large contrib. to nucrep.The distance is   1.37057673E-05
 screening nuclear repulsion energy   0.0096464719

 Total-screening nuclear repulsion energy   9.33801982


 Adding T+Vsolv ...
 maxdens  375
 *** End of DALTON-COSMO calculation ***

 DIAGEL COUNTS: DG0X,DG2X,DG4X= 440 145 30
 =========== Executing IN-CORE method ==========
 norm multnew:  1.


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        12 xx:        36 ww:        79
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        11
task #   9:        28    task #  10:        71    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 =========== Executing IN-CORE method ==========
 norm multnew:  0.00311503223


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        12 xx:        36 ww:        79
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        11
task #   9:        28    task #  10:        71    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 rtolcosmo = eo,e,r,ro   0. -85.6626615 -85.6754286  9.33801982  9.33607988
 rtolcosmo = eo,e,r,ro   0. -80.7914797 -81.5397976  9.33801982  9.33607988
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97468795     0.00040615

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1    -76.3374087744  1.0827E-02  1.1296E-03  6.9779E-02  1.0000E-04
 mr-sdci #  3  2    -72.2017778209  7.4638E-01  0.0000E+00  3.8489E+00  1.0000E-04


dielectric energy =      -0.0110936974
delta diel. energy =       0.0015905518
total Cosmo energy: e(nroot) + repnuc - ediel =     -76.3263150770
delta Cosmo energy =       0.0092365876

 root number  1 is used to define the new expansion vector.
 diagon: frcsub=2, new matrix-vector products will be computed.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.01   0.00   0.00   0.01         0.    0.0000
    2   25    0   0.00   0.00   0.00   0.00         0.    0.0000
    3   26    0   0.00   0.00   0.00   0.00         0.    0.0000
    4   11    0   0.01   0.00   0.00   0.01         0.    0.0000
    5   15    0   0.00   0.00   0.00   0.00         0.    0.0000
    6   16    0   0.01   0.00   0.00   0.01         0.    0.0000
    7    1    0   0.00   0.00   0.00   0.00         0.    0.0000
    8    5    0   0.00   0.00   0.00   0.00         0.    0.0000
    9    6    0   0.01   0.00   0.00   0.01         0.    0.0000
   10    7    0   0.01   0.00   0.00   0.01         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.01   0.00   0.00   0.01         0.    0.0000
   14   47    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0700s 
time spent in multnx:                   0.0700s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.5300s 

          starting ci iteration   4

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.33801982

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=       3  D0X=       9  D0W=      15
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   1.9995868      1.9855166      1.9716263      0.0174333      0.0099855
   0.0058073      0.0051871      0.0010749      0.0007936      0.0005465
   0.0004427      0.0003508      0.0003345      0.0001544      0.0001032
   0.0000675      0.0000496      0.0000292      0.0000081      0.0000003


*****   symmetry block SYM2   *****

 occupation numbers of nos
   1.9712549      0.0190984      0.0056624      0.0010074      0.0007805
   0.0006186      0.0003722      0.0001574      0.0000976      0.0000901
   0.0000175      0.0000099      0.0000047      0.0000002


*****   symmetry block SYM3   *****

 occupation numbers of nos
   1.9736942      0.0145179      0.0052467      0.0007517      0.0005905
   0.0003786      0.0003487      0.0001170      0.0000522


*****   symmetry block SYM4   *****

 occupation numbers of nos
   0.0048898      0.0006745      0.0003550      0.0000894      0.0000236


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***

  Confirmation of dielectric energy for ground state
  edielnew =  -0.00271872687


 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************

 *** qcos is copied to qcosdalton ***

 fepsi =   1.
 *** qcosdalton is mult by fepsi ***

 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 Warning! Large contrib. to nucrep.The distance is   1.37057673E-05
 ** Total nuclear repulsion energy ** =   9.34410201483261
 Warning! Large contrib. to nucrep.The distance is   1.37057673E-05
 screening nuclear repulsion energy   0.00922625325

 Total-screening nuclear repulsion energy   9.33487576


 Adding T+Vsolv ...
 maxdens  375
 *** End of DALTON-COSMO calculation ***

 DIAGEL COUNTS: DG0X,DG2X,DG4X= 440 145 30
 =========== Executing IN-CORE method ==========
 norm multnew:  1.


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        12 xx:        36 ww:        79
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        11
task #   9:        28    task #  10:        71    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 =========== Executing IN-CORE method ==========
 norm multnew:  0.00124222967


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        12 xx:        36 ww:        79
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        11
task #   9:        28    task #  10:        71    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 rtolcosmo = eo,e,r,ro   0. -85.6754286 -85.6725466  9.33487576  9.33801982
 rtolcosmo = eo,e,r,ro   0. -81.5397976 -81.4045608  9.33487576  9.33801982
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97234967    -0.19043111

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1    -76.3376708111  2.6204E-04  1.5000E-04  2.6144E-02  1.0000E-04
 mr-sdci #  4  2    -72.0696850435 -1.3209E-01  0.0000E+00  3.9300E+00  1.0000E-04


dielectric energy =      -0.0106622734
delta diel. energy =      -0.0004314240
total Cosmo energy: e(nroot) + repnuc - ediel =     -76.3270085377
delta Cosmo energy =       0.0006934607

 root number  1 is used to define the new expansion vector.
 diagon: frcsub=2, new matrix-vector products will be computed.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.00   0.00   0.00   0.00         0.    0.0000
    2   25    0   0.01   0.00   0.00   0.01         0.    0.0000
    3   26    0   0.00   0.00   0.00   0.00         0.    0.0000
    4   11    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   15    0   0.01   0.00   0.00   0.00         0.    0.0000
    6   16    0   0.01   0.00   0.00   0.01         0.    0.0000
    7    1    0   0.00   0.00   0.00   0.00         0.    0.0000
    8    5    0   0.00   0.00   0.00   0.00         0.    0.0000
    9    6    0   0.01   0.00   0.00   0.01         0.    0.0000
   10    7    0   0.00   0.00   0.00   0.00         0.    0.0000
   11   75    0   0.01   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.00   0.00   0.00   0.00         0.    0.0000
   14   47    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0600s 
time spent in multnx:                   0.0400s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.5300s 

          starting ci iteration   5

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.33487576

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=       3  D0X=       9  D0W=      15
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   1.9995548      1.9842134      1.9692035      0.0191974      0.0109390
   0.0062096      0.0054526      0.0011524      0.0008574      0.0005833
   0.0004757      0.0003734      0.0003587      0.0001629      0.0001116
   0.0000712      0.0000562      0.0000322      0.0000086      0.0000004


*****   symmetry block SYM2   *****

 occupation numbers of nos
   1.9686186      0.0209758      0.0059863      0.0010828      0.0008412
   0.0006581      0.0003964      0.0001684      0.0001026      0.0000974
   0.0000186      0.0000106      0.0000052      0.0000002


*****   symmetry block SYM3   *****

 occupation numbers of nos
   1.9713152      0.0158365      0.0055100      0.0008103      0.0006327
   0.0004026      0.0003707      0.0001244      0.0000592


*****   symmetry block SYM4   *****

 occupation numbers of nos
   0.0056810      0.0007555      0.0003956      0.0001005      0.0000293


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***

  Confirmation of dielectric energy for ground state
  edielnew =  -0.00268973671


 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************

 *** qcos is copied to qcosdalton ***

 fepsi =   1.
 *** qcosdalton is mult by fepsi ***

 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 Warning! Large contrib. to nucrep.The distance is   1.37057673E-05
 ** Total nuclear repulsion energy ** =   9.34467310114936
 Warning! Large contrib. to nucrep.The distance is   1.37057673E-05
 screening nuclear repulsion energy   0.00952634727

 Total-screening nuclear repulsion energy   9.33514675


 Adding T+Vsolv ...
 maxdens  375
 *** End of DALTON-COSMO calculation ***

 DIAGEL COUNTS: DG0X,DG2X,DG4X= 440 145 30
 =========== Executing IN-CORE method ==========
 norm multnew:  1.


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        12 xx:        36 ww:        79
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        11
task #   9:        28    task #  10:        71    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 =========== Executing IN-CORE method ==========
 norm multnew:  8.87806737E-05


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        12 xx:        36 ww:        79
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        11
task #   9:        28    task #  10:        71    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 rtolcosmo = eo,e,r,ro   0. -85.6725466 -85.6731352  9.33514675  9.33487576
 rtolcosmo = eo,e,r,ro   0. -81.4045608 -81.6950514  9.33514675  9.33487576
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97292412     0.10104557

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1    -76.3379884919  3.1768E-04  4.9765E-05  1.2464E-02  1.0000E-04
 mr-sdci #  5  2    -72.3599046092  2.9022E-01  0.0000E+00  3.3130E+00  1.0000E-04


dielectric energy =      -0.0108567959
delta diel. energy =       0.0001945225
total Cosmo energy: e(nroot) + repnuc - ediel =     -76.3271316960
delta Cosmo energy =       0.0001231583

 root number  1 is used to define the new expansion vector.
 diagon: frcsub=2, new matrix-vector products will be computed.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.00   0.00   0.00   0.00         0.    0.0000
    2   25    0   0.01   0.00   0.00   0.00         0.    0.0000
    3   26    0   0.00   0.00   0.00   0.00         0.    0.0000
    4   11    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   15    0   0.01   0.00   0.00   0.00         0.    0.0000
    6   16    0   0.01   0.00   0.00   0.01         0.    0.0000
    7    1    0   0.00   0.00   0.00   0.00         0.    0.0000
    8    5    0   0.00   0.00   0.00   0.00         0.    0.0000
    9    6    0   0.01   0.00   0.00   0.01         0.    0.0000
   10    7    0   0.01   0.00   0.00   0.01         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.00   0.00   0.00   0.00         0.    0.0000
   14   47    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0600s 
time spent in multnx:                   0.0400s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.5300s 

          starting ci iteration   6

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.33514675

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=       3  D0X=       9  D0W=      15
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   1.9995643      1.9844521      1.9699307      0.0189462      0.0107646
   0.0060077      0.0052486      0.0011222      0.0008338      0.0005669
   0.0004661      0.0003633      0.0003510      0.0001563      0.0001080
   0.0000684      0.0000552      0.0000313      0.0000080      0.0000004


*****   symmetry block SYM2   *****

 occupation numbers of nos
   1.9692517      0.0206159      0.0057680      0.0010596      0.0008177
   0.0006391      0.0003865      0.0001629      0.0000983      0.0000941
   0.0000180      0.0000099      0.0000051      0.0000002


*****   symmetry block SYM3   *****

 occupation numbers of nos
   1.9720553      0.0153846      0.0052992      0.0007890      0.0006177
   0.0003913      0.0003600      0.0001203      0.0000577


*****   symmetry block SYM4   *****

 occupation numbers of nos
   0.0056925      0.0007429      0.0003829      0.0001012      0.0000333


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***

  Confirmation of dielectric energy for ground state
  edielnew =  -0.00271254912


 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************

 *** qcos is copied to qcosdalton ***

 fepsi =   1.
 *** qcosdalton is mult by fepsi ***

 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 Warning! Large contrib. to nucrep.The distance is   1.37057673E-05
 ** Total nuclear repulsion energy ** =   9.34452633523615
 Warning! Large contrib. to nucrep.The distance is   1.37057673E-05
 screening nuclear repulsion energy   0.00953481113

 Total-screening nuclear repulsion energy   9.33499152


 Adding T+Vsolv ...
 maxdens  375
 *** End of DALTON-COSMO calculation ***

 DIAGEL COUNTS: DG0X,DG2X,DG4X= 440 145 30
 =========== Executing IN-CORE method ==========
 norm multnew:  1.


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        12 xx:        36 ww:        79
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        11
task #   9:        28    task #  10:        71    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 =========== Executing IN-CORE method ==========
 norm multnew:  7.33149676E-05


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        12 xx:        36 ww:        79
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        11
task #   9:        28    task #  10:        71    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 rtolcosmo = eo,e,r,ro   0. -85.6731352 -85.6729974  9.33499152  9.33514675
 rtolcosmo = eo,e,r,ro   0. -81.6950514 -81.9822419  9.33499152  9.33514675
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97232367    -0.21011400

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  6  1    -76.3380058574  1.7365E-05  9.8339E-06  6.6400E-03  1.0000E-04
 mr-sdci #  6  2    -72.6472503953  2.8735E-01  0.0000E+00  3.3329E+00  1.0000E-04


dielectric energy =      -0.0108436059
delta diel. energy =      -0.0000131900
total Cosmo energy: e(nroot) + repnuc - ediel =     -76.3271622515
delta Cosmo energy =       0.0000305555

 root number  1 is used to define the new expansion vector.
 diagon: frcsub=2, new matrix-vector products will be computed.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.00   0.00   0.00   0.00         0.    0.0000
    2   25    0   0.00   0.00   0.00   0.00         0.    0.0000
    3   26    0   0.01   0.00   0.00   0.00         0.    0.0000
    4   11    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   15    0   0.01   0.00   0.00   0.01         0.    0.0000
    6   16    0   0.00   0.00   0.00   0.00         0.    0.0000
    7    1    0   0.01   0.00   0.00   0.01         0.    0.0000
    8    5    0   0.00   0.00   0.00   0.00         0.    0.0000
    9    6    0   0.01   0.00   0.00   0.01         0.    0.0000
   10    7    0   0.00   0.00   0.00   0.00         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.01   0.00   0.00   0.01         0.    0.0000
   14   47    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0600s 
time spent in multnx:                   0.0500s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.5400s 

          starting ci iteration   7

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.33499152

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=       3  D0X=       9  D0W=      15
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   1.9995575      1.9841298      1.9693000      0.0194068      0.0110120
   0.0061044      0.0053279      0.0011389      0.0008471      0.0005752
   0.0004742      0.0003687      0.0003569      0.0001581      0.0001096
   0.0000694      0.0000562      0.0000319      0.0000082      0.0000004


*****   symmetry block SYM2   *****

 occupation numbers of nos
   1.9685738      0.0210998      0.0058608      0.0010769      0.0008294
   0.0006479      0.0003927      0.0001651      0.0000996      0.0000954
   0.0000182      0.0000101      0.0000052      0.0000002


*****   symmetry block SYM3   *****

 occupation numbers of nos
   1.9714403      0.0157411      0.0053780      0.0008023      0.0006277
   0.0003970      0.0003651      0.0001218      0.0000586


*****   symmetry block SYM4   *****

 occupation numbers of nos
   0.0058592      0.0007668      0.0003940      0.0001038      0.0000359


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***

  Confirmation of dielectric energy for ground state
  edielnew =  -0.00271360875


 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************

 *** qcos is copied to qcosdalton ***

 fepsi =   1.
 *** qcosdalton is mult by fepsi ***

 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 Warning! Large contrib. to nucrep.The distance is   1.37057673E-05
 ** Total nuclear repulsion energy ** =   9.34451482033493
 Warning! Large contrib. to nucrep.The distance is   1.37057673E-05
 screening nuclear repulsion energy   0.00957915011

 Total-screening nuclear repulsion energy   9.33493567


 Adding T+Vsolv ...
 maxdens  375
 *** End of DALTON-COSMO calculation ***

 DIAGEL COUNTS: DG0X,DG2X,DG4X= 440 145 30
 =========== Executing IN-CORE method ==========
 norm multnew:  1.


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        12 xx:        36 ww:        79
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        11
task #   9:        28    task #  10:        71    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 =========== Executing IN-CORE method ==========
 norm multnew:  6.4914095E-06


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        12 xx:        36 ww:        79
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        11
task #   9:        28    task #  10:        71    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 rtolcosmo = eo,e,r,ro   0. -85.6729974 -85.6729715  9.33493567  9.33499152
 rtolcosmo = eo,e,r,ro   0. -81.9822419 -82.0564003  9.33493567  9.33499152
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97248138     0.10328312

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  7  1    -76.3380358601  3.0003E-05  3.9040E-06  3.4764E-03  1.0000E-04
 mr-sdci #  7  2    -72.7214645965  7.4214E-02  0.0000E+00  3.4602E+00  1.0000E-04


dielectric energy =      -0.0108652797
delta diel. energy =       0.0000216739
total Cosmo energy: e(nroot) + repnuc - ediel =     -76.3271705804
delta Cosmo energy =       0.0000083288

 root number  1 is used to define the new expansion vector.
 diagon: frcsub=2, new matrix-vector products will be computed.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.01   0.00   0.00   0.00         0.    0.0000
    2   25    0   0.00   0.00   0.00   0.00         0.    0.0000
    3   26    0   0.01   0.00   0.00   0.01         0.    0.0000
    4   11    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   15    0   0.00   0.00   0.00   0.00         0.    0.0000
    6   16    0   0.01   0.00   0.00   0.01         0.    0.0000
    7    1    0   0.00   0.00   0.00   0.00         0.    0.0000
    8    5    0   0.01   0.00   0.00   0.01         0.    0.0000
    9    6    0   0.00   0.00   0.00   0.00         0.    0.0000
   10    7    0   0.01   0.00   0.00   0.01         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.01   0.00   0.00   0.01         0.    0.0000
   14   47    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0700s 
time spent in multnx:                   0.0600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.5400s 

          starting ci iteration   8

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.33493567

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=       3  D0X=       9  D0W=      15
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   1.9995612      1.9842147      1.9694997      0.0193308      0.0109572
   0.0060473      0.0052780      0.0011282      0.0008393      0.0005698
   0.0004711      0.0003655      0.0003543      0.0001562      0.0001085
   0.0000688      0.0000558      0.0000317      0.0000081      0.0000004


*****   symmetry block SYM2   *****

 occupation numbers of nos
   1.9687533      0.0209936      0.0058055      0.0010684      0.0008210
   0.0006419      0.0003895      0.0001633      0.0000984      0.0000943
   0.0000180      0.0000099      0.0000052      0.0000002


*****   symmetry block SYM3   *****

 occupation numbers of nos
   1.9716287      0.0156290      0.0053262      0.0007959      0.0006227
   0.0003935      0.0003617      0.0001205      0.0000579


*****   symmetry block SYM4   *****

 occupation numbers of nos
   0.0058557      0.0007659      0.0003918      0.0001036      0.0000378


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***

  Confirmation of dielectric energy for ground state
  edielnew =  -0.00271622529


 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************

 *** qcos is copied to qcosdalton ***

 fepsi =   1.
 *** qcosdalton is mult by fepsi ***

 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 Warning! Large contrib. to nucrep.The distance is   1.37057673E-05
 ** Total nuclear repulsion energy ** =   9.34446943090033
 Warning! Large contrib. to nucrep.The distance is   1.37057673E-05
 screening nuclear repulsion energy   0.00958710707

 Total-screening nuclear repulsion energy   9.33488232


 Adding T+Vsolv ...
 maxdens  375
 *** End of DALTON-COSMO calculation ***

 DIAGEL COUNTS: DG0X,DG2X,DG4X= 440 145 30
 =========== Executing IN-CORE method ==========
 norm multnew:  1.


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        12 xx:        36 ww:        79
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        11
task #   9:        28    task #  10:        71    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 =========== Executing IN-CORE method ==========
 norm multnew:  5.61138324E-06


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        12 xx:        36 ww:        79
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        11
task #   9:        28    task #  10:        71    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 rtolcosmo = eo,e,r,ro   0. -85.6729715 -85.6729199  9.33488232  9.33493567
 rtolcosmo = eo,e,r,ro   0. -82.0564003 -82.0297998  9.33488232  9.33493567
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97231212    -0.20761390

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  8  1    -76.3380375348  1.6748E-06  8.7755E-07  1.9770E-03  1.0000E-04
 mr-sdci #  8  2    -72.6949174595 -2.6547E-02  0.0000E+00  3.2884E+00  1.0000E-04


dielectric energy =      -0.0108645232
delta diel. energy =      -0.0000007565
total Cosmo energy: e(nroot) + repnuc - ediel =     -76.3271730117
delta Cosmo energy =       0.0000024313

 root number  1 is used to define the new expansion vector.
 diagon: frcsub=2, new matrix-vector products will be computed.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.00   0.00   0.00   0.00         0.    0.0000
    2   25    0   0.01   0.00   0.00   0.01         0.    0.0000
    3   26    0   0.00   0.00   0.00   0.00         0.    0.0000
    4   11    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   15    0   0.01   0.00   0.00   0.00         0.    0.0000
    6   16    0   0.01   0.00   0.00   0.01         0.    0.0000
    7    1    0   0.00   0.00   0.00   0.00         0.    0.0000
    8    5    0   0.00   0.00   0.00   0.00         0.    0.0000
    9    6    0   0.01   0.00   0.00   0.01         0.    0.0000
   10    7    0   0.00   0.00   0.00   0.00         0.    0.0000
   11   75    0   0.01   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.00   0.00   0.00   0.00         0.    0.0000
   14   47    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0600s 
time spent in multnx:                   0.0400s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.5300s 

          starting ci iteration   9

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.33488232

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=       3  D0X=       9  D0W=      15
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   1.9995594      1.9841273      1.9693219      0.0194573      0.0110257
   0.0060750      0.0053016      0.0011326      0.0008430      0.0005721
   0.0004733      0.0003671      0.0003559      0.0001568      0.0001089
   0.0000691      0.0000561      0.0000319      0.0000081      0.0000004


*****   symmetry block SYM2   *****

 occupation numbers of nos
   1.9685640      0.0211273      0.0058326      0.0010729      0.0008241
   0.0006443      0.0003911      0.0001639      0.0000988      0.0000947
   0.0000181      0.0000100      0.0000052      0.0000002


*****   symmetry block SYM3   *****

 occupation numbers of nos
   1.9714508      0.0157332      0.0053496      0.0007998      0.0006254
   0.0003951      0.0003631      0.0001209      0.0000582


*****   symmetry block SYM4   *****

 occupation numbers of nos
   0.0059014      0.0007734      0.0003952      0.0001042      0.0000389


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***

  Confirmation of dielectric energy for ground state
  edielnew =  -0.00271684925


 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************

 *** qcos is copied to qcosdalton ***

 fepsi =   1.
 *** qcosdalton is mult by fepsi ***

 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 Warning! Large contrib. to nucrep.The distance is   1.37057673E-05
 ** Total nuclear repulsion energy ** =   9.34446625576033
 Warning! Large contrib. to nucrep.The distance is   1.37057673E-05
 screening nuclear repulsion energy   0.00959891643

 Total-screening nuclear repulsion energy   9.33486734


 Adding T+Vsolv ...
 maxdens  375
 *** End of DALTON-COSMO calculation ***

 DIAGEL COUNTS: DG0X,DG2X,DG4X= 440 145 30
 =========== Executing IN-CORE method ==========
 norm multnew:  1.


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        12 xx:        36 ww:        79
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        11
task #   9:        28    task #  10:        71    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 =========== Executing IN-CORE method ==========
 norm multnew:  6.13047852E-07


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        12 xx:        36 ww:        79
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        11
task #   9:        28    task #  10:        71    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 rtolcosmo = eo,e,r,ro   0. -85.6729199 -85.6729114  9.33486734  9.33488232
 rtolcosmo = eo,e,r,ro   0. -82.0297998 -82.1643125  9.33486734  9.33488232
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97236478     0.11264360

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  9  1    -76.3380440483  6.5135E-06  4.0051E-07  1.1550E-03  1.0000E-04
 mr-sdci #  9  2    -72.8294451913  1.3453E-01  0.0000E+00  3.4550E+00  1.0000E-04


dielectric energy =      -0.0108702719
delta diel. energy =       0.0000057487
total Cosmo energy: e(nroot) + repnuc - ediel =     -76.3271737764
delta Cosmo energy =       0.0000007648

 root number  1 is used to define the new expansion vector.
 diagon: frcsub=2, new matrix-vector products will be computed.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.00   0.00   0.00   0.00         0.    0.0000
    2   25    0   0.00   0.00   0.00   0.00         0.    0.0000
    3   26    0   0.01   0.00   0.00   0.01         0.    0.0000
    4   11    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   15    0   0.01   0.00   0.00   0.01         0.    0.0000
    6   16    0   0.00   0.00   0.00   0.00         0.    0.0000
    7    1    0   0.00   0.00   0.00   0.00         0.    0.0000
    8    5    0   0.01   0.00   0.00   0.01         0.    0.0000
    9    6    0   0.00   0.00   0.00   0.00         0.    0.0000
   10    7    0   0.01   0.00   0.00   0.01         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.01   0.00   0.00   0.01         0.    0.0000
   14   47    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0600s 
time spent in multnx:                   0.0600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.5500s 

          starting ci iteration  10

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.33486734

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=       3  D0X=       9  D0W=      15
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   1.9995605      1.9841579      1.9693880      0.0194269      0.0110058
   0.0060576      0.0052870      0.0011292      0.0008406      0.0005705
   0.0004723      0.0003661      0.0003551      0.0001562      0.0001086
   0.0000689      0.0000560      0.0000319      0.0000081      0.0000004


*****   symmetry block SYM2   *****

 occupation numbers of nos
   1.9686256      0.0210885      0.0058159      0.0010701      0.0008214
   0.0006425      0.0003901      0.0001633      0.0000985      0.0000944
   0.0000181      0.0000099      0.0000052      0.0000002


*****   symmetry block SYM3   *****

 occupation numbers of nos
   1.9715092      0.0156987      0.0053344      0.0007978      0.0006238
   0.0003940      0.0003621      0.0001205      0.0000580


*****   symmetry block SYM4   *****

 occupation numbers of nos
   0.0058981      0.0007735      0.0003948      0.0001040      0.0000398


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***

  Confirmation of dielectric energy for ground state
  edielnew =  -0.00271754481


 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************

 *** qcos is copied to qcosdalton ***

 fepsi =   1.
 *** qcosdalton is mult by fepsi ***

 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 Warning! Large contrib. to nucrep.The distance is   1.37057673E-05
 ** Total nuclear repulsion energy ** =   9.34445355388799
 Warning! Large contrib. to nucrep.The distance is   1.37057673E-05
 screening nuclear repulsion energy   0.009601115

 Total-screening nuclear repulsion energy   9.33485244


 Adding T+Vsolv ...
 maxdens  375
 *** End of DALTON-COSMO calculation ***

 DIAGEL COUNTS: DG0X,DG2X,DG4X= 440 145 30
 =========== Executing IN-CORE method ==========
 norm multnew:  1.


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        12 xx:        36 ww:        79
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        11
task #   9:        28    task #  10:        71    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 =========== Executing IN-CORE method ==========
 norm multnew:  5.53955572E-07


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        12 xx:        36 ww:        79
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        11
task #   9:        28    task #  10:        71    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 rtolcosmo = eo,e,r,ro   0. -85.6729114 -85.6728966  9.33485244  9.33486734
 rtolcosmo = eo,e,r,ro   0. -82.1643125 -81.9340749  9.33485244  9.33486734
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97231120    -0.20572582

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 10  1    -76.3380441171  6.8769E-08  1.0168E-07  6.7380E-04  1.0000E-04
 mr-sdci # 10  2    -72.5992224466 -2.3022E-01  0.0000E+00  3.3770E+00  1.0000E-04


dielectric energy =      -0.0108700867
delta diel. energy =      -0.0000001852
total Cosmo energy: e(nroot) + repnuc - ediel =     -76.3271740304
delta Cosmo energy =       0.0000002540

 root number  1 is used to define the new expansion vector.
 diagon: frcsub=2, new matrix-vector products will be computed.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.01   0.00   0.00   0.01         0.    0.0000
    2   25    0   0.00   0.00   0.00   0.00         0.    0.0000
    3   26    0   0.00   0.00   0.00   0.00         0.    0.0000
    4   11    0   0.01   0.00   0.00   0.00         0.    0.0000
    5   15    0   0.00   0.00   0.00   0.00         0.    0.0000
    6   16    0   0.01   0.00   0.00   0.01         0.    0.0000
    7    1    0   0.00   0.00   0.00   0.00         0.    0.0000
    8    5    0   0.00   0.00   0.00   0.00         0.    0.0000
    9    6    0   0.01   0.00   0.00   0.00         0.    0.0000
   10    7    0   0.01   0.00   0.00   0.01         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.01   0.00   0.00   0.01         0.    0.0000
   14   47    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0700s 
time spent in multnx:                   0.0500s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.5300s 

          starting ci iteration  11

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.33485244

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=       3  D0X=       9  D0W=      15
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   1.9995599      1.9841306      1.9693320      0.0194655      0.0110270
   0.0060668      0.0052950      0.0011307      0.0008418      0.0005712
   0.0004730      0.0003666      0.0003556      0.0001564      0.0001088
   0.0000690      0.0000561      0.0000320      0.0000081      0.0000004


*****   symmetry block SYM2   *****

 occupation numbers of nos
   1.9685663      0.0211298      0.0058249      0.0010716      0.0008225
   0.0006433      0.0003906      0.0001635      0.0000986      0.0000946
   0.0000181      0.0000100      0.0000052      0.0000002


*****   symmetry block SYM3   *****

 occupation numbers of nos
   1.9714517      0.0157322      0.0053422      0.0007991      0.0006247
   0.0003945      0.0003626      0.0001207      0.0000581


*****   symmetry block SYM4   *****

 occupation numbers of nos
   0.0059123      0.0007761      0.0003959      0.0001042      0.0000402


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***

  Confirmation of dielectric energy for ground state
  edielnew =  -0.00271775313


 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************

 *** qcos is copied to qcosdalton ***

 fepsi =   1.
 *** qcosdalton is mult by fepsi ***

 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 Warning! Large contrib. to nucrep.The distance is   1.37057673E-05
 ** Total nuclear repulsion energy ** =   9.34445297643702
 Warning! Large contrib. to nucrep.The distance is   1.37057673E-05
 screening nuclear repulsion energy   0.00960476882

 Total-screening nuclear repulsion energy   9.33484821


 Adding T+Vsolv ...
 maxdens  375
 *** End of DALTON-COSMO calculation ***

 DIAGEL COUNTS: DG0X,DG2X,DG4X= 440 145 30
 =========== Executing IN-CORE method ==========
 norm multnew:  1.


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        12 xx:        36 ww:        79
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        11
task #   9:        28    task #  10:        71    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 =========== Executing IN-CORE method ==========
 norm multnew:  7.33821396E-08


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        12 xx:        36 ww:        79
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        11
task #   9:        28    task #  10:        71    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 rtolcosmo = eo,e,r,ro   0. -85.6728966 -85.6728943  9.33484821  9.33485244
 rtolcosmo = eo,e,r,ro   0. -81.9340749 -82.1525371  9.33484821  9.33485244
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97233147     0.12604816

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 11  1    -76.3380460599  1.9428E-06  5.2925E-08  4.3784E-04  1.0000E-04
 mr-sdci # 11  2    -72.8176889270  2.1847E-01  0.0000E+00  3.4014E+00  1.0000E-04


dielectric energy =      -0.0108719385
delta diel. energy =       0.0000018518
total Cosmo energy: e(nroot) + repnuc - ediel =     -76.3271741214
delta Cosmo energy =       0.0000000910

 root number  1 is used to define the new expansion vector.
 diagon: frcsub=2, new matrix-vector products will be computed.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.00   0.00   0.00   0.00         0.    0.0000
    2   25    0   0.01   0.00   0.00   0.01         0.    0.0000
    3   26    0   0.00   0.00   0.00   0.00         0.    0.0000
    4   11    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   15    0   0.01   0.00   0.00   0.01         0.    0.0000
    6   16    0   0.00   0.00   0.00   0.00         0.    0.0000
    7    1    0   0.01   0.00   0.00   0.00         0.    0.0000
    8    5    0   0.00   0.00   0.00   0.00         0.    0.0000
    9    6    0   0.01   0.00   0.00   0.01         0.    0.0000
   10    7    0   0.00   0.00   0.00   0.00         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.01   0.00   0.00   0.01         0.    0.0000
   13   46    0   0.00   0.00   0.00   0.00         0.    0.0000
   14   47    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0600s 
time spent in multnx:                   0.0500s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.5300s 

          starting ci iteration  12

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.33484821

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=       3  D0X=       9  D0W=      15
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   1.9995603      1.9841424      1.9693572      0.0194521      0.0110190
   0.0060608      0.0052900      0.0011296      0.0008410      0.0005707
   0.0004726      0.0003663      0.0003553      0.0001563      0.0001086
   0.0000690      0.0000560      0.0000319      0.0000081      0.0000004


*****   symmetry block SYM2   *****

 occupation numbers of nos
   1.9685902      0.0211138      0.0058192      0.0010706      0.0008216
   0.0006427      0.0003902      0.0001633      0.0000985      0.0000945
   0.0000181      0.0000099      0.0000052      0.0000002


*****   symmetry block SYM3   *****

 occupation numbers of nos
   1.9714731      0.0157194      0.0053371      0.0007985      0.0006241
   0.0003942      0.0003622      0.0001205      0.0000580


*****   symmetry block SYM4   *****

 occupation numbers of nos
   0.0059104      0.0007762      0.0003958      0.0001041      0.0000406


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***

  Confirmation of dielectric energy for ground state
  edielnew =  -0.00271797313


 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************

 *** qcos is copied to qcosdalton ***

 fepsi =   1.
 *** qcosdalton is mult by fepsi ***

 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 Warning! Large contrib. to nucrep.The distance is   1.37057673E-05
 ** Total nuclear repulsion energy ** =   9.34444922319918
 Warning! Large contrib. to nucrep.The distance is   1.37057673E-05
 screening nuclear repulsion energy   0.00960529898

 Total-screening nuclear repulsion energy   9.33484392


 Adding T+Vsolv ...
 maxdens  375
 *** End of DALTON-COSMO calculation ***

 DIAGEL COUNTS: DG0X,DG2X,DG4X= 440 145 30
 =========== Executing IN-CORE method ==========
 norm multnew:  1.


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        12 xx:        36 ww:        79
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        11
task #   9:        28    task #  10:        71    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 =========== Executing IN-CORE method ==========
 norm multnew:  7.01418273E-08


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        12 xx:        36 ww:        79
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        11
task #   9:        28    task #  10:        71    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 rtolcosmo = eo,e,r,ro   0. -85.6728943 -85.6728899  9.33484392  9.33484821
 rtolcosmo = eo,e,r,ro   0. -82.1525371 -81.7650139  9.33484392  9.33484821
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97231235    -0.20468759

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 12  1    -76.3380460021 -5.7817E-08  1.4877E-08  2.5991E-04  1.0000E-04
 mr-sdci # 12  2    -72.4301699710 -3.8752E-01  0.0000E+00  3.4694E+00  1.0000E-04


dielectric energy =      -0.0108718466
delta diel. energy =      -0.0000000919
total Cosmo energy: e(nroot) + repnuc - ediel =     -76.3271741556
delta Cosmo energy =       0.0000000341

 root number  1 is used to define the new expansion vector.
 diagon: frcsub=2, new matrix-vector products will be computed.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.00   0.00   0.00   0.00         0.    0.0000
    2   25    0   0.00   0.00   0.00   0.00         0.    0.0000
    3   26    0   0.01   0.00   0.00   0.01         0.    0.0000
    4   11    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   15    0   0.00   0.00   0.00   0.00         0.    0.0000
    6   16    0   0.01   0.00   0.00   0.00         0.    0.0000
    7    1    0   0.00   0.00   0.00   0.00         0.    0.0000
    8    5    0   0.01   0.00   0.00   0.01         0.    0.0000
    9    6    0   0.00   0.00   0.00   0.00         0.    0.0000
   10    7    0   0.01   0.00   0.00   0.01         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.01   0.00   0.00   0.01         0.    0.0000
   14   47    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0600s 
time spent in multnx:                   0.0500s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.5400s 

          starting ci iteration  13

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.33484392

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=       3  D0X=       9  D0W=      15
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   1.9995601      1.9841327      1.9693372      0.0194654      0.0110265
   0.0060643      0.0052930      0.0011301      0.0008414      0.0005710
   0.0004729      0.0003665      0.0003555      0.0001563      0.0001087
   0.0000690      0.0000561      0.0000320      0.0000081      0.0000004


*****   symmetry block SYM2   *****

 occupation numbers of nos
   1.9685691      0.0211283      0.0058226      0.0010711      0.0008220
   0.0006430      0.0003904      0.0001634      0.0000985      0.0000945
   0.0000181      0.0000099      0.0000052      0.0000002


*****   symmetry block SYM3   *****

 occupation numbers of nos
   1.9714523      0.0157314      0.0053401      0.0007989      0.0006244
   0.0003944      0.0003624      0.0001206      0.0000580


*****   symmetry block SYM4   *****

 occupation numbers of nos
   0.0059154      0.0007771      0.0003963      0.0001042      0.0000408


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***

  Confirmation of dielectric energy for ground state
  edielnew =  -0.0027180439


 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************

 *** qcos is copied to qcosdalton ***

 fepsi =   1.
 *** qcosdalton is mult by fepsi ***

 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 Warning! Large contrib. to nucrep.The distance is   1.37057673E-05
 ** Total nuclear repulsion energy ** =   9.34444928537247
 Warning! Large contrib. to nucrep.The distance is   1.37057673E-05
 screening nuclear repulsion energy   0.00960653235

 Total-screening nuclear repulsion energy   9.33484275


 Adding T+Vsolv ...
 maxdens  375
 *** End of DALTON-COSMO calculation ***

 DIAGEL COUNTS: DG0X,DG2X,DG4X= 440 145 30
 =========== Executing IN-CORE method ==========
 norm multnew:  1.


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        12 xx:        36 ww:        79
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        11
task #   9:        28    task #  10:        71    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 =========== Executing IN-CORE method ==========
 norm multnew:  1.09739846E-08


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        12 xx:        36 ww:        79
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        11
task #   9:        28    task #  10:        71    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 rtolcosmo = eo,e,r,ro   0. -85.6728899 -85.6728894  9.33484275  9.33484392
 rtolcosmo = eo,e,r,ro   0. -81.7650139 -82.0730494  9.33484275  9.33484392
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97232095     0.14001909

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 13  1    -76.3380466738  6.7171E-07  8.5514E-09  1.8174E-04  1.0000E-04
 mr-sdci # 13  2    -72.7382066297  3.0804E-01  0.0000E+00  3.3584E+00  1.0000E-04


dielectric energy =      -0.0108725047
delta diel. energy =       0.0000006581
total Cosmo energy: e(nroot) + repnuc - ediel =     -76.3271741692
delta Cosmo energy =       0.0000000136

 root number  1 is used to define the new expansion vector.
 diagon: frcsub=2, new matrix-vector products will be computed.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.00   0.00   0.00   0.00         0.    0.0000
    2   25    0   0.00   0.00   0.00   0.00         0.    0.0000
    3   26    0   0.01   0.00   0.00   0.01         0.    0.0000
    4   11    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   15    0   0.00   0.00   0.00   0.00         0.    0.0000
    6   16    0   0.01   0.00   0.00   0.00         0.    0.0000
    7    1    0   0.00   0.00   0.00   0.00         0.    0.0000
    8    5    0   0.01   0.00   0.00   0.01         0.    0.0000
    9    6    0   0.00   0.00   0.00   0.00         0.    0.0000
   10    7    0   0.01   0.00   0.00   0.01         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.01   0.00   0.00   0.01         0.    0.0000
   14   47    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0600s 
time spent in multnx:                   0.0500s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.5400s 

          starting ci iteration  14

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.33484275

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=       3  D0X=       9  D0W=      15
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   1.9995602      1.9841376      1.9693477      0.0194593      0.0110230
   0.0060620      0.0052911      0.0011297      0.0008411      0.0005708
   0.0004728      0.0003663      0.0003554      0.0001563      0.0001087
   0.0000690      0.0000560      0.0000320      0.0000081      0.0000004


*****   symmetry block SYM2   *****

 occupation numbers of nos
   1.9685793      0.0211213      0.0058204      0.0010708      0.0008217
   0.0006428      0.0003903      0.0001634      0.0000985      0.0000945
   0.0000181      0.0000099      0.0000052      0.0000002


*****   symmetry block SYM3   *****

 occupation numbers of nos
   1.9714611      0.0157259      0.0053381      0.0007987      0.0006242
   0.0003942      0.0003623      0.0001206      0.0000580


*****   symmetry block SYM4   *****

 occupation numbers of nos
   0.0059144      0.0007772      0.0003962      0.0001041      0.0000409


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***

  Confirmation of dielectric energy for ground state
  edielnew =  -0.00271811725


 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************

 *** qcos is copied to qcosdalton ***

 fepsi =   1.
 *** qcosdalton is mult by fepsi ***

 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 Warning! Large contrib. to nucrep.The distance is   1.37057673E-05
 ** Total nuclear repulsion energy ** =   9.34444809695174
 Warning! Large contrib. to nucrep.The distance is   1.37057673E-05
 screening nuclear repulsion energy   0.00960660794

 Total-screening nuclear repulsion energy   9.33484149


 Adding T+Vsolv ...
 maxdens  375
 *** End of DALTON-COSMO calculation ***

 DIAGEL COUNTS: DG0X,DG2X,DG4X= 440 145 30
 =========== Executing IN-CORE method ==========
 norm multnew:  1.


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        12 xx:        36 ww:        79
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        11
task #   9:        28    task #  10:        71    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 =========== Executing IN-CORE method ==========
 norm multnew:  1.09454685E-08


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        12 xx:        36 ww:        79
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        11
task #   9:        28    task #  10:        71    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 rtolcosmo = eo,e,r,ro   0. -85.6728894 -85.6728881  9.33484149  9.33484275
 rtolcosmo = eo,e,r,ro   0. -82.0730494 -81.5962373  9.33484149  9.33484275
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97231340    -0.20406492

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 14  1    -76.3380466080 -6.5819E-08  2.5685E-09  1.0900E-04  1.0000E-04
 mr-sdci # 14  2    -72.2613958169 -4.7681E-01  0.0000E+00  3.5328E+00  1.0000E-04


dielectric energy =      -0.0108724333
delta diel. energy =      -0.0000000714
total Cosmo energy: e(nroot) + repnuc - ediel =     -76.3271741747
delta Cosmo energy =       0.0000000056

 root number  1 is used to define the new expansion vector.
 diagon: frcsub=2, new matrix-vector products will be computed.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.01   0.00   0.00   0.01         0.    0.0000
    2   25    0   0.00   0.00   0.00   0.00         0.    0.0000
    3   26    0   0.00   0.00   0.00   0.00         0.    0.0000
    4   11    0   0.01   0.00   0.00   0.01         0.    0.0000
    5   15    0   0.00   0.00   0.00   0.00         0.    0.0000
    6   16    0   0.01   0.00   0.00   0.01         0.    0.0000
    7    1    0   0.00   0.00   0.00   0.00         0.    0.0000
    8    5    0   0.00   0.00   0.00   0.00         0.    0.0000
    9    6    0   0.01   0.00   0.00   0.01         0.    0.0000
   10    7    0   0.01   0.00   0.00   0.01         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.01   0.00   0.00   0.01         0.    0.0000
   14   47    0   0.00   0.00   0.00   0.00         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0600s 
time spent in multnx:                   0.0600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.5300s 

          starting ci iteration  15

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.33484149

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=       3  D0X=       9  D0W=      15
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   1.9995601      1.9841338      1.9693397      0.0194644      0.0110259
   0.0060634      0.0052924      0.0011300      0.0008413      0.0005709
   0.0004729      0.0003664      0.0003555      0.0001563      0.0001087
   0.0000690      0.0000561      0.0000320      0.0000081      0.0000004


*****   symmetry block SYM2   *****

 occupation numbers of nos
   1.9685709      0.0211270      0.0058218      0.0010710      0.0008218
   0.0006429      0.0003904      0.0001634      0.0000985      0.0000945
   0.0000181      0.0000099      0.0000052      0.0000002


*****   symmetry block SYM3   *****

 occupation numbers of nos
   1.9714529      0.0157307      0.0053393      0.0007989      0.0006244
   0.0003943      0.0003623      0.0001206      0.0000580


*****   symmetry block SYM4   *****

 occupation numbers of nos
   0.0059164      0.0007775      0.0003964      0.0001042      0.0000410


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***

  Confirmation of dielectric energy for ground state
  edielnew =  -0.00271813947


 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************

 *** qcos is copied to qcosdalton ***

 fepsi =   1.
 *** qcosdalton is mult by fepsi ***

 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 Warning! Large contrib. to nucrep.The distance is   1.37057673E-05
 ** Total nuclear repulsion energy ** =   9.34444821392199
 Warning! Large contrib. to nucrep.The distance is   1.37057673E-05
 screening nuclear repulsion energy   0.00960705569

 Total-screening nuclear repulsion energy   9.33484116


 Adding T+Vsolv ...
 maxdens  375
 *** End of DALTON-COSMO calculation ***

 DIAGEL COUNTS: DG0X,DG2X,DG4X= 440 145 30
 =========== Executing IN-CORE method ==========
 norm multnew:  1.


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        12 xx:        36 ww:        79
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        11
task #   9:        28    task #  10:        71    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 =========== Executing IN-CORE method ==========
 norm multnew:  1.92395265E-09


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        12 xx:        36 ww:        79
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        11
task #   9:        28    task #  10:        71    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 rtolcosmo = eo,e,r,ro   0. -85.6728881 -85.672888  9.33484116  9.33484149
 rtolcosmo = eo,e,r,ro   0. -81.5962373 -81.9978127  9.33484116  9.33484149
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97231723     0.15084847

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 15  1    -76.3380468595  2.5153E-07  1.5686E-09  7.9477E-05  1.0000E-04
 mr-sdci # 15  2    -72.6629715350  4.0158E-01  0.0000E+00  3.3286E+00  1.0000E-04


dielectric energy =      -0.0108726824
delta diel. energy =       0.0000002492
total Cosmo energy: e(nroot) + repnuc - ediel =     -76.3271741771
delta Cosmo energy =       0.0000000024


 mr-sdci  convergence criteria satisfied after 15 iterations.

 *************************************************
 ***Final Calc of density matrix for cosmo calc***
 *************************************************

 entering drivercid: firstcall,firstnonref= F T
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=       3  D0X=       9  D0W=      15
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   1.9995602      1.9841359      1.9693443      0.0194617      0.0110244
   0.0060625      0.0052916      0.0011298      0.0008412      0.0005708
   0.0004728      0.0003664      0.0003554      0.0001563      0.0001087
   0.0000690      0.0000561      0.0000320      0.0000081      0.0000004


*****   symmetry block SYM2   *****

 occupation numbers of nos
   1.9685754      0.0211239      0.0058209      0.0010708      0.0008217
   0.0006428      0.0003903      0.0001634      0.0000985      0.0000945
   0.0000181      0.0000099      0.0000052      0.0000002


*****   symmetry block SYM3   *****

 occupation numbers of nos
   1.9714568      0.0157282      0.0053385      0.0007988      0.0006243
   0.0003943      0.0003623      0.0001206      0.0000580


*****   symmetry block SYM4   *****

 occupation numbers of nos
   0.0059159      0.0007775      0.0003964      0.0001042      0.0000411


 total number of electrons =   10.0000000000


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***


 **************************************************
 ***Final cosmo calculation for ground state*******
 **************************************************

  ediel before cosmo(4 =  -0.00271817061
  elast before cosmo(4 =  -76.3271742


 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 15  1    -76.3380468595  2.5153E-07  1.5686E-09  7.9477E-05  1.0000E-04

####################CIUDGINFO####################

   ci vector at position   1 energy=  -76.338046859547

################END OF CIUDGINFO################


    1 of the   2 expansion vectors are transformed.
    1 of the   1 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
 maximum overlap with reference  1(overlap=  0.972317234)

information on vector: 1from unit 11 written to unit 16filename civout                                                      


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -76.3380468595

                                                       internal orbitals

                                          level       1    2    3    4    5

                                          orbital     1    2    3   21   35

                                         symmetry   a1   a1   a1   b1   b2 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1  0.972317                        +-   +-   +-   +-   +- 
 y   1  1      11  0.012357              2( b1 )   +-   +-   +-    -   +- 
 y   1  1      14  0.011899              5( b1 )   +-   +-   +-    -   +- 
 y   1  1      26 -0.011776              4( a1 )   +-   +-    -   +-   +- 
 x   1  1      75 -0.016914    2( b1 )   1( b2 )   +-   +-   +-    -    - 
 x   1  1      78 -0.015403    5( b1 )   1( b2 )   +-   +-   +-    -    - 
 x   1  1      88 -0.013791    2( b1 )   2( b2 )   +-   +-   +-    -    - 
 x   1  1      89  0.011209    3( b1 )   2( b2 )   +-   +-   +-    -    - 
 x   1  1      91 -0.010653    5( b1 )   2( b2 )   +-   +-   +-    -    - 
 x   1  1      92 -0.017073    6( b1 )   2( b2 )   +-   +-   +-    -    - 
 x   1  1     104  0.016488    5( b1 )   3( b2 )   +-   +-   +-    -    - 
 x   1  1     182  0.018145    5( a1 )   1( a2 )   +-   +-   +-    -    - 
 x   1  1     183  0.013281    6( a1 )   1( a2 )   +-   +-   +-    -    - 
 x   1  1     184  0.011265    7( a1 )   1( a2 )   +-   +-   +-    -    - 
 x   1  1     185  0.018425    8( a1 )   1( a2 )   +-   +-   +-    -    - 
 x   1  1     186 -0.013214    9( a1 )   1( a2 )   +-   +-   +-    -    - 
 x   1  1     264  0.014625    2( a1 )   1( b2 )   +-   +-    -   +-    - 
 x   1  1     267 -0.015866    5( a1 )   1( b2 )   +-   +-    -   +-    - 
 x   1  1     281  0.011865    2( a1 )   2( b2 )   +-   +-    -   +-    - 
 x   1  1     284 -0.014174    5( a1 )   2( b2 )   +-   +-    -   +-    - 
 x   1  1     285  0.020412    6( a1 )   2( b2 )   +-   +-    -   +-    - 
 x   1  1     301  0.012918    5( a1 )   3( b2 )   +-   +-    -   +-    - 
 x   1  1     303 -0.014296    7( a1 )   3( b2 )   +-   +-    -   +-    - 
 x   1  1     304  0.018312    8( a1 )   3( b2 )   +-   +-    -   +-    - 
 x   1  1     403  0.015747    5( b1 )   1( a2 )   +-   +-    -   +-    - 
 x   1  1     483 -0.016159    3( a1 )   2( b1 )   +-   +-    -    -   +- 
 x   1  1     486 -0.011133    6( a1 )   2( b1 )   +-   +-    -    -   +- 
 x   1  1     487 -0.014192    7( a1 )   2( b1 )   +-   +-    -    -   +- 
 x   1  1     534 -0.017935    3( a1 )   5( b1 )   +-   +-    -    -   +- 
 x   1  1     537 -0.010019    6( a1 )   5( b1 )   +-   +-    -    -   +- 
 x   1  1     538 -0.024030    7( a1 )   5( b1 )   +-   +-    -    -   +- 
 x   1  1     553  0.013039    5( a1 )   6( b1 )   +-   +-    -    -   +- 
 x   1  1     554 -0.010413    6( a1 )   6( b1 )   +-   +-    -    -   +- 
 x   1  1     589  0.013067    7( a1 )   8( b1 )   +-   +-    -    -   +- 
 x   1  1     687  0.013143    3( b2 )   1( a2 )   +-   +-    -    -   +- 
 x   1  1     749  0.011661    8( a1 )   2( b2 )   +-    -   +-   +-    - 
 w   1  1    2440 -0.015721    8( a1 )   8( a1 )   +-   +-   +-   +-      
 w   1  1    2649 -0.017767    1( b2 )   1( b2 )   +-   +-   +-   +-      
 w   1  1    2650 -0.025997    1( b2 )   2( b2 )   +-   +-   +-   +-      
 w   1  1    2651 -0.030325    2( b2 )   2( b2 )   +-   +-   +-   +-      
 w   1  1    2652  0.011934    1( b2 )   3( b2 )   +-   +-   +-   +-      
 w   1  1    2654 -0.014565    3( b2 )   3( b2 )   +-   +-   +-   +-      
 w   1  1    2660 -0.013911    2( b2 )   5( b2 )   +-   +-   +-   +-      
 w   1  1    2663 -0.011647    5( b2 )   5( b2 )   +-   +-   +-   +-      
 w   1  1    2685 -0.020897    1( a2 )   1( a2 )   +-   +-   +-   +-      
 w   1  1    2701  0.016038    2( b1 )   1( b2 )   +-   +-   +-   +     - 
 w   1  1    2704  0.015293    5( b1 )   1( b2 )   +-   +-   +-   +     - 
 w   1  1    2714  0.013820    2( b1 )   2( b2 )   +-   +-   +-   +     - 
 w   1  1    2715 -0.010309    3( b1 )   2( b2 )   +-   +-   +-   +     - 
 w   1  1    2717  0.012464    5( b1 )   2( b2 )   +-   +-   +-   +     - 
 w   1  1    2718  0.016553    6( b1 )   2( b2 )   +-   +-   +-   +     - 
 w   1  1    2730 -0.014359    5( b1 )   3( b2 )   +-   +-   +-   +     - 
 w   1  1    2810 -0.012316    7( a1 )   1( a2 )   +-   +-   +-   +     - 
 w   1  1    2900  0.014999    2( a1 )   5( a1 )   +-   +-   +-        +- 
 w   1  1    2901 -0.010356    3( a1 )   5( a1 )   +-   +-   +-        +- 
 w   1  1    2903 -0.018084    5( a1 )   5( a1 )   +-   +-   +-        +- 
 w   1  1    2916 -0.012281    7( a1 )   7( a1 )   +-   +-   +-        +- 
 w   1  1    3044 -0.019605    2( b1 )   2( b1 )   +-   +-   +-        +- 
 w   1  1    3053 -0.029874    2( b1 )   5( b1 )   +-   +-   +-        +- 
 w   1  1    3056 -0.030670    5( b1 )   5( b1 )   +-   +-   +-        +- 
 w   1  1    3058 -0.011608    2( b1 )   6( b1 )   +-   +-   +-        +- 
 w   1  1    3059  0.011333    3( b1 )   6( b1 )   +-   +-   +-        +- 
 w   1  1    3062 -0.013668    6( b1 )   6( b1 )   +-   +-   +-        +- 
 w   1  1    3074  0.011224    5( b1 )   8( b1 )   +-   +-   +-        +- 
 w   1  1    3169 -0.016320    1( a2 )   1( a2 )   +-   +-   +-        +- 
 w   1  1    3185 -0.011131    2( a1 )   1( b2 )   +-   +-   +    +-    - 
 w   1  1    3188  0.010290    5( a1 )   1( b2 )   +-   +-   +    +-    - 
 w   1  1    3189 -0.013860    6( a1 )   1( b2 )   +-   +-   +    +-    - 
 w   1  1    3204  0.010721    4( a1 )   2( b2 )   +-   +-   +    +-    - 
 w   1  1    3205  0.015235    5( a1 )   2( b2 )   +-   +-   +    +-    - 
 w   1  1    3206 -0.018184    6( a1 )   2( b2 )   +-   +-   +    +-    - 
 w   1  1    3224  0.011196    7( a1 )   3( b2 )   +-   +-   +    +-    - 
 w   1  1    3324 -0.015123    5( b1 )   1( a2 )   +-   +-   +    +-    - 
 w   1  1    3403  0.019427    2( a1 )   2( b1 )   +-   +-   +     -   +- 
 w   1  1    3406 -0.022705    5( a1 )   2( b1 )   +-   +-   +     -   +- 
 w   1  1    3454  0.018753    2( a1 )   5( b1 )   +-   +-   +     -   +- 
 w   1  1    3457 -0.030471    5( a1 )   5( b1 )   +-   +-   +     -   +- 
 w   1  1    3460 -0.011905    8( a1 )   5( b1 )   +-   +-   +     -   +- 
 w   1  1    3475  0.012992    6( a1 )   6( b1 )   +-   +-   +     -   +- 
 w   1  1    3608 -0.013695    3( b2 )   1( a2 )   +-   +-   +     -   +- 
 w   1  1    3648 -0.013233    2( a1 )   2( a1 )   +-   +-        +-   +- 
 w   1  1    3657  0.018494    2( a1 )   5( a1 )   +-   +-        +-   +- 
 w   1  1    3660 -0.016941    5( a1 )   5( a1 )   +-   +-        +-   +- 
 w   1  1    3664  0.010978    4( a1 )   6( a1 )   +-   +-        +-   +- 
 w   1  1    3665  0.012719    5( a1 )   6( a1 )   +-   +-        +-   +- 
 w   1  1    3666 -0.017915    6( a1 )   6( a1 )   +-   +-        +-   +- 
 w   1  1    3669 -0.010779    3( a1 )   7( a1 )   +-   +-        +-   +- 
 w   1  1    3673 -0.014598    7( a1 )   7( a1 )   +-   +-        +-   +- 
 w   1  1    3810 -0.016951    2( b1 )   5( b1 )   +-   +-        +-   +- 
 w   1  1    3813 -0.018049    5( b1 )   5( b1 )   +-   +-        +-   +- 
 w   1  1    3831  0.011919    5( b1 )   8( b1 )   +-   +-        +-   +- 
 w   1  1    3895 -0.012541    3( b2 )   3( b2 )   +-   +-        +-   +- 
 w   1  1    3942 -0.010718    2( a1 )   1( b2 )   +-   +    +-   +-    - 
 w   1  1    3945  0.011245    5( a1 )   1( b2 )   +-   +    +-   +-    - 
 w   1  1    3949 -0.010177    9( a1 )   1( b2 )   +-   +    +-   +-    - 
 w   1  1    3959 -0.015367    2( a1 )   2( b2 )   +-   +    +-   +-    - 
 w   1  1    3961 -0.013261    4( a1 )   2( b2 )   +-   +    +-   +-    - 
 w   1  1    3962  0.015307    5( a1 )   2( b2 )   +-   +    +-   +-    - 
 w   1  1    3966 -0.013728    9( a1 )   2( b2 )   +-   +    +-   +-    - 
 w   1  1    4016 -0.010933    8( a1 )   5( b2 )   +-   +    +-   +-    - 
 w   1  1    4160  0.011856    2( a1 )   2( b1 )   +-   +    +-    -   +- 
 w   1  1    4163 -0.011873    5( a1 )   2( b1 )   +-   +    +-    -   +- 
 w   1  1    4211  0.011596    2( a1 )   5( b1 )   +-   +    +-    -   +- 
 w   1  1    4214 -0.018903    5( a1 )   5( b1 )   +-   +    +-    -   +- 
 w   1  1    4215 -0.011596    6( a1 )   5( b1 )   +-   +    +-    -   +- 
 w   1  1    4220 -0.013385   11( a1 )   5( b1 )   +-   +    +-    -   +- 
 w   1  1    4228  0.012175    2( a1 )   6( b1 )   +-   +    +-    -   +- 
 w   1  1    4234  0.010952    8( a1 )   6( b1 )   +-   +    +-    -   +- 
 w   1  1    4282  0.011543    5( a1 )   9( b1 )   +-   +    +-    -   +- 
 w   1  1    4405 -0.012067    2( a1 )   2( a1 )   +-   +     -   +-   +- 
 w   1  1    4414  0.018738    2( a1 )   5( a1 )   +-   +     -   +-   +- 
 w   1  1    4417 -0.016865    5( a1 )   5( a1 )   +-   +     -   +-   +- 
 w   1  1    4421 -0.010088    4( a1 )   6( a1 )   +-   +     -   +-   +- 
 w   1  1    4436 -0.011125    6( a1 )   8( a1 )   +-   +     -   +-   +- 
 w   1  1    4709  0.013214    2( a1 )   5( a1 )   +-        +-   +-   +- 
 w   1  1    4712 -0.013216    5( a1 )   5( a1 )   +-        +-   +-   +- 

 ci coefficient statistics:
           rq > 0.1                1
      0.1> rq > 0.01             115
     0.01> rq > 0.001           2127
    0.001> rq > 0.0001          2805
   0.0001> rq > 0.00001         1104
  0.00001> rq > 0.000001         169
 0.000001> rq                     18
           all                  6339
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        15 2x:         0 4x:         0
All internal counts: zz :         0 yy:         0 xx:         0 ww:         0
One-external counts: yz :         0 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         0    task #   2:         0    task #   3:         0    task #   4:         0
task #   5:         0    task #   6:         0    task #   7:        -1    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:        14    task #  12:         0
task #  13:         0    task #  14:         0    task #
  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.972317234146    -73.968046256030

 number of reference csfs (nref) is     1.  root number (iroot) is  1.

 pople ci energy extrapolation is computed with 10 correlated electrons.

 eref      =    -76.073984558112   "relaxed" cnot**2         =   0.945400803818
 eci       =    -76.338046859547   deltae = eci - eref       =  -0.264062301435
 eci+dv1   =    -76.352464448947   dv1 = (1-cnot**2)*deltae  =  -0.014417589400
 eci+dv2   =    -76.353297099807   dv2 = dv1 / cnot**2       =  -0.015250240260
 eci+dv3   =    -76.354231820684   dv3 = dv1 / (2*cnot**2-1) =  -0.016184961137
 eci+pople =    -76.350677752120   ( 10e- scaled deltae )    =  -0.276693194009
NO coefficients and occupation numbers are written to nocoef_ci.1
 entering drivercid: firstcall,firstnonref= F T
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=       3  D0X=       9  D0W=      15
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   1.9995602      1.9841359      1.9693443      0.0194617      0.0110244
   0.0060625      0.0052916      0.0011298      0.0008412      0.0005708
   0.0004728      0.0003664      0.0003554      0.0001563      0.0001087
   0.0000690      0.0000561      0.0000320      0.0000081      0.0000004


*****   symmetry block SYM2   *****

 occupation numbers of nos
   1.9685754      0.0211239      0.0058209      0.0010708      0.0008217
   0.0006428      0.0003903      0.0001634      0.0000985      0.0000945
   0.0000181      0.0000099      0.0000052      0.0000002


*****   symmetry block SYM3   *****

 occupation numbers of nos
   1.9714568      0.0157282      0.0053385      0.0007988      0.0006243
   0.0003943      0.0003623      0.0001206      0.0000580


*****   symmetry block SYM4   *****

 occupation numbers of nos
   0.0059159      0.0007775      0.0003964      0.0001042      0.0000411


 total number of electrons =   10.0000000000



          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!


                        A1  partial gross atomic populations
   ao class       1A1        2A1        3A1        4A1        5A1        6A1 
    O1_ s       1.999741   1.553253  -0.003879   0.001102   0.005822   0.001246
    O1_ p       0.000038   0.000647   1.508024   0.009593   0.003198   0.000050
    O1_ d       0.000000  -0.000400   0.001806   0.000433   0.000464   0.004087
    O1_ f       0.000000   0.000281   0.000512  -0.000018   0.000006   0.000036
    H1_ s      -0.000184   0.333377   0.388685   0.008614   0.000859   0.000067
    H1_ p      -0.000035   0.096979   0.074196  -0.000263   0.000675   0.000576

   ao class       7A1        8A1        9A1       10A1       11A1       12A1 
    O1_ s       0.000081   0.000267   0.000026   0.000009   0.000204   0.000050
    O1_ p       0.000314   0.000179   0.000596   0.000033   0.000055   0.000006
    O1_ d       0.004696   0.000369   0.000011   0.000249   0.000028   0.000050
    O1_ f       0.000009   0.000103   0.000006   0.000076   0.000122   0.000241
    H1_ s       0.000024   0.000261   0.000027   0.000001   0.000011   0.000006
    H1_ p       0.000167  -0.000050   0.000175   0.000203   0.000053   0.000014

   ao class      13A1       14A1       15A1       16A1       17A1       18A1 
    O1_ s       0.000059   0.000020   0.000017   0.000000   0.000003   0.000002
    O1_ p       0.000003   0.000009   0.000015   0.000000   0.000004   0.000000
    O1_ d       0.000115   0.000067   0.000005   0.000029   0.000010  -0.000001
    O1_ f       0.000158   0.000040   0.000002   0.000004   0.000000   0.000002
    H1_ s       0.000017   0.000010   0.000015   0.000011   0.000003   0.000001
    H1_ p       0.000004   0.000011   0.000056   0.000024   0.000036   0.000027

   ao class      19A1       20A1 
    O1_ s       0.000000   0.000000
    O1_ p       0.000000   0.000000
    O1_ d       0.000000   0.000000
    O1_ f       0.000000   0.000000
    H1_ s       0.000003   0.000000
    H1_ p       0.000004   0.000000

                        B1  partial gross atomic populations
   ao class       1B1        2B1        3B1        4B1        5B1        6B1 
    O1_ p       1.143705   0.010884   0.001956   0.000368   0.000495   0.000100
    O1_ d       0.006179   0.000758   0.003479   0.000362   0.000053   0.000010
    O1_ f       0.000536  -0.000006   0.000031   0.000058   0.000042   0.000301
    H1_ s       0.807786   0.010411   0.000023   0.000158   0.000047   0.000010
    H1_ p       0.010369  -0.000923   0.000332   0.000125   0.000184   0.000221

   ao class       7B1        8B1        9B1       10B1       11B1       12B1 
    O1_ p       0.000030   0.000020   0.000015   0.000018  -0.000001   0.000001
    O1_ d       0.000053   0.000002   0.000068   0.000004   0.000000   0.000001
    O1_ f       0.000298   0.000065   0.000004   0.000018   0.000000   0.000000
    H1_ s      -0.000008   0.000006   0.000004   0.000035   0.000007   0.000008
    H1_ p       0.000017   0.000070   0.000007   0.000019   0.000012   0.000000

   ao class      13B1       14B1 
    O1_ p      -0.000001   0.000000
    O1_ d       0.000000   0.000000
    O1_ f       0.000000   0.000000
    H1_ s      -0.000003   0.000000
    H1_ p       0.000009   0.000000

                        B2  partial gross atomic populations
   ao class       1B2        2B2        3B2        4B2        5B2        6B2 
    O1_ p       1.857021   0.013448   0.000206   0.000686   0.000041   0.000009
    O1_ d       0.001960   0.000172   0.004908   0.000016   0.000166   0.000066
    O1_ f       0.000164   0.000001   0.000010   0.000001   0.000165   0.000313
    H1_ p       0.112311   0.002107   0.000215   0.000096   0.000253   0.000007

   ao class       7B2        8B2        9B2 
    O1_ p       0.000011   0.000001   0.000010
    O1_ d       0.000085   0.000027   0.000009
    O1_ f       0.000257   0.000028   0.000000
    H1_ p       0.000009   0.000064   0.000039

                        A2  partial gross atomic populations
   ao class       1A2        2A2        3A2        4A2        5A2 
    O1_ d       0.005097   0.000255   0.000109   0.000045   0.000004
    O1_ f       0.000021   0.000127   0.000244   0.000019   0.000001
    H1_ p       0.000798   0.000395   0.000044   0.000039   0.000036


                        gross atomic populations
     ao           O1_        H1_
      s         3.558024   1.550291
      p         4.551791   0.299708
      d         0.035909   0.000000
      f         0.004278   0.000000
    total       8.150001   1.849999


 Total number of electrons:   10.00000000

