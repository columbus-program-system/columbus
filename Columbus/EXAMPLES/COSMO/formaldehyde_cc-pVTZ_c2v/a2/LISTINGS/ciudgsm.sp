1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 integral file titles:
 Hermit Integral Program : SIFS version  hochtor2        Thu Oct  7 14:54:38 2004
  cidrt_title                                                                    
  title                                                                          
 mofmt: formatted orbitals label=morbl   hochtor2        Thu Oct  7 14:55:07 2004
 SIFS file created by program tran.      hochtor2        Thu Oct  7 14:55:07 2004

 formula file title:
 Hermit Integral Program : SIFS version  hochtor2        Thu Oct  7 14:54:38 2004
  cidrt_title                                                                    
  title                                                                          
 mofmt: formatted orbitals label=morbl   hochtor2        Thu Oct  7 14:55:07 2004
 SIFS file created by program tran.      hochtor2        Thu Oct  7 14:55:07 2004
  cidrt_title                                                                    

 297 dimension of the ci-matrix ->>>    124515


 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -113.7683785828 -3.1161E+01  5.2535E-01  1.7332E+00  1.0000E-03
 mr-sdci #  2  1   -114.1317887604 -3.0865E+01  5.1671E-02  5.1937E-01  1.0000E-03
 mr-sdci #  3  1   -114.1705077695 -3.1189E+01  2.2893E-03  1.1022E-01  1.0000E-03
 mr-sdci #  4  1   -114.1726274978 -3.1226E+01  2.5739E-04  3.5047E-02  1.0000E-03
 mr-sdci #  5  1   -114.1728871497 -3.1228E+01  4.5601E-05  1.6462E-02  1.0000E-03
 mr-sdci #  6  1   -114.1729354743 -3.1228E+01  7.4920E-06  6.4957E-03  1.0000E-03
 mr-sdci #  7  1   -114.1737542231 -3.1227E+01  1.2329E-05  4.8289E-03  1.0000E-03
 mr-sdci #  8  1   -114.1737676364 -3.1238E+01  2.0995E-06  2.9435E-03  1.0000E-03
 mr-sdci #  9  1   -114.1737694312 -3.1238E+01  2.5383E-07  1.0617E-03  1.0000E-03
 mr-sdci # 10  1   -114.1738107485 -3.1238E+01  6.4286E-08  4.9961E-04  1.0000E-03

 mr-sdci  convergence criteria satisfied after 10 iterations.

 final mr-sdci  convergence information:
 mr-sdci # 10  1   -114.1738107485 -3.1238E+01  6.4286E-08  4.9961E-04  1.0000E-03

 number of reference csfs (nref) is     1.  root number (iroot) is  1.

 eref      =   -113.768598986570   "relaxed" cnot**2         =   0.899655444366
 eci       =   -114.173810748506   deltae = eci - eref       =  -0.405211761937
 eci+dv1   =   -114.214471542695   dv1 = (1-cnot**2)*deltae  =  -0.040660794189
 eci+dv2   =   -114.219006711521   dv2 = dv1 / cnot**2       =  -0.045195963014
 eci+dv3   =   -114.224680559943   dv3 = dv1 / (2*cnot**2-1) =  -0.050869811437
 eci+pople =   -114.216897668398   ( 16e- scaled deltae )    =  -0.448298681828
