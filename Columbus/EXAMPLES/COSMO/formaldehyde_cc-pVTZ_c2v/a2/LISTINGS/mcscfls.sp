

     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 This program allows the csf mixing coefficient and orbital expansion coefficient
 optimization using the graphical unitary group approach and the exponential
 operator mcscf method.
 references:  r. shepard and j. simons,                    ' int. j. quantum chem. symp. 14, 211 (1980).
              r. shepard, i. shavitt, and j. simons, j. chem. phys. 76, 543 (1982).
              r. shepard in "ab initio methods in quantum chemistry ii" advances in chemical
                  physics 69, edited by k. p. lawley (wiley, new york, 1987) pp. 63-200.
 Original autor: Ron Shepard, ANL
 Later revisions: Michal Dallos, University Vienna

 This Version of Program MCSCF is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.4.0.2     **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 Workspace allocation information:
        10000000 of real*8 words (   76.29 MB) of work space has been allocated.

 user input information:

 ======== echo of the mcscf input ========
 ------------------------------------------------------------------------
  &input
   niter=60,
   nmiter=30,
   nciitr=30,
   tol(3)=1.e-4,
   tol(2)=1.e-4,
   tol(1)=1.e-8,
   NSTATE=0,
   npath=1,3,9,10,13,17,19,21,-11,12, 2,
   ncoupl=5,
   FCIORB=  2,2,40,3,2,40
   NAVST(1) = 1,
   WAVST(1,1)=1 ,
   NAVST(2) = 1,
   WAVST(2,1)=1 ,
  &end
 ------------------------------------------------------------------------


 ***  Integral file informations  ***


 input integral file : aoints                                                      

 Integral file header information:
 Hermit Integral Program : SIFS version  hochtor2        Thu Oct  7 14:54:38 2004

 Core type energy values:
 energy( 1)=  3.116530982443E+01, ietype=   -1,    core energy of type: Nuc.Rep.
 total ao core energy =   31.165309824


   ******  Basis set informations:  ******

 Number of irreps:                  4
 Total number of basis functions:  88

 irrep no.              1    2    3    4
 irrep label           A1   B1   B2   A2 
 no. of bas.fcions.    36   18   24   10


 ***  MCSCF optimization procedure parmeters:  ***


 maximum number of mcscf iterations:        niter=    60

 maximum number of psci micro-iterations:   nmiter=   30
 maximum r,s subspace dimension allowed:    nvrsmx=   30

 tol(1)=  1.0000E-08. . . . delta-emc convergence criterion.
 tol(2)=  1.0000E-04. . . . wnorm convergence criterion.
 tol(3)=  1.0000E-04. . . . knorm convergence criterion.
 tol(4)=  1.0000E-08. . . . apxde convergence criterion.
 tol(5)=  1.0000E-04. . . . small diagonal matrix element tolerance.
 tol(6)=  1.0000E-06. . . . minimum ci-psci residual norm.
 tol(7)=  1.0000E-05. . . . maximum ci-psci residual norm.
 tol(8)=  1.0000E+00. . . . maximum abs(k(xy)) allowed.
 tol(9)=  1.0000E+00. . . . wnorm coupling tolerance.
 tol(10)= 0.0000E+00. . . . maximum psci emergency shift parameter.
 tol(11)= 0.0000E+00. . . . minimum psci emergency shift parameter.
 tol(12)= 0.0000E+00. . . . increment of psci emergency shift parameter.


 *** State averaging informations: ***


 MCSCF calculation performed for  2 DRTs.

 DRT  first state   no.of aver.states   weights
  1   ground state          1             0.500
  2   ground state          1             0.500

 The number of hmc(*) eigenvalues and eigenvectors calculated each iteration per symmetry:
 Symm.   no.of eigenv.(=ncol)
  A1         2
  B1         2

 Orbitals included in invariant subspaces:
   symmetry   orbital   mask
       2       2( 38)    40
       3       2( 56)    40

 npath(*) options:
  2:  orbital-state coupling terms will be included beginning on iteration ncoupl=  5
  3:  print intermediate timing information.
  9:  suppress the drt listing.
 10:  suppress the hmc(*) eigenvector listing.
 12:  diagonalize the hmc(*) matrix iteratively.
        nunitv= 1 nciitr=30 mxvadd=20 nvcimx=20
       rtolci(*),wnorm=     1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 0.0000E+00
   noldv =   0  0
 13:  get initial orbitals from the formatted file, mocoef.
 17:  print the final natural orbitals and occupations.
 19:  transform the virtual orbitals to diagonalize qvv(*).
 21:  write out the one- and two- electron density for further use (files:mcd1fl, mcd2fl).


   ******  DRT info section  ******


 Informations for the DRT no.  1

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a1 
 Total number of electrons:   16
 Spin multiplicity:            1
 Number of active orbitals:    2
 Number of active electrons:   2
 Total number of CSFs:         2

 Informations for the DRT no.  2

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a2 
 Total number of electrons:   16
 Spin multiplicity:            1
 Number of active orbitals:    2
 Number of active electrons:   2
 Total number of CSFs:         1

 !timer: initialization                  user+sys=     0.000 walltime=     0.000

 faar:   0 active-active rotations allowed out of:   0 possible.


 Number of active-double rotations:      2
 Number of active-active rotations:      0
 Number of double-virtual rotations:   193
 Number of active-virtual rotations:    38

 Size of orbital-Hessian matrix B:                    30288
 Size of the orbital-state Hessian matrix C:            699
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:          30987



   ****** Integral transformation section ******


 number of blocks to be transformed in-core is   0
 number of blocks to be transformed out of core is   19

 in-core ao list requires    0 segments.
 out of core ao list requires   19 segments.
 each segment has length   9981505 working precision words.

               ao integral sort statistics
 length of records:      4096
 number of buckets:  19
 scratch file used is da1:
 amount of core needed for in-core arrays: 11666

 twoao processed    1858387 two electron ao integrals.

    3945907 ao integrals were written into 1453 records

 Source of the initial MO coeficients:

 Input MO coefficient file: mocoef                                                      


               starting mcscf iteration...   1
 !timer:                                 user+sys=     2.440 walltime=     2.000

 orbital-state coupling will not be calculated this iteration.

 number of transformed integrals put on file:     95172
 number of records written:    35
 !timer: 2-e transformation              user+sys=     2.240 walltime=     3.000

 Size of orbital-Hessian matrix B:                    30288
 Total size of the state Hessian matrix M:                0
 Total size of HESSIAN-matrix for linear con          30288

 !timer: mosrt1                          user+sys=     0.060 walltime=     0.000
 !timer: mosrt2                          user+sys=     0.020 walltime=     0.000
 !timer: mosort                          user+sys=     0.080 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000

 trial vectors are generated internally.

 trial vector  1 is unit matrix column     2
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 ciiter=   2 noldhv=  2 noldv=  2
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -113.9110939890     -145.0764038134        0.0000000000        0.0000010000
    2      -113.4050655433     -144.5703753677        0.0000000000        0.0100000000
 !timer: hmcvec                          user+sys=     0.000 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000

 trial vectors are generated internally.

 trial vector  1 is unit matrix column     1
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 ciiter=   1 noldhv=  1 noldv=  1
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -113.7150716059     -144.8803814303        0.0000000000        0.0000010000
 !timer: hmcvec                          user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: mqva                            user+sys=     0.000 walltime=     0.000
 !timer: mfva                            user+sys=     0.000 walltime=     0.000
 !timer: mfad                            user+sys=     0.000 walltime=     0.000
 !timer: mqad                            user+sys=     0.000 walltime=     0.000
 !timer: mfvd                            user+sys=     0.000 walltime=     0.000
 !timer: bvaad                           user+sys=     0.000 walltime=     0.000
 !timer: bvaadf                          user+sys=     0.000 walltime=     0.000
 !timer: mqaa                            user+sys=     0.000 walltime=     0.000
 !timer: mfaa                            user+sys=     0.000 walltime=     0.000
 !timer: badad                           user+sys=     0.000 walltime=     0.000
 !timer: badadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvava                           user+sys=     0.000 walltime=     0.000
 !timer: bvavaf                          user+sys=     0.000 walltime=     0.000
 !timer: bvavd                           user+sys=     0.000 walltime=     0.000
 !timer: bvavdf                          user+sys=     0.000 walltime=     0.000
 !timer: bvdad                           user+sys=     0.000 walltime=     0.000
 !timer: bvdadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvdvd                           user+sys=     0.000 walltime=     0.000
 !timer: hbcon                           user+sys=     0.010 walltime=     0.000

  tol(10)=  0. eshsci=  0.0391125977
 Total number of micro iterations:    9

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.96406663 pnorm= 0.0000E+00 rznorm= 3.9787E-06 rpnorm= 0.0000E+00 noldr=  9 nnewr=  9 nolds=  0 nnews=  0

 !timer: Direct Bxr time contribution    user+sys=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  user+sys=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    user+sys=     0.000 walltime=     0.000
 !timer: solvek total                    user+sys=     0.010 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
   -41.413352  -22.623350   -2.908070   -1.743556   -1.381829

 qvv(*) eigenvalues. symmetry block  1
     0.298524    0.523631    0.882308    1.183006    1.329916    1.445338    2.027657    2.375460    2.616463    3.266197
     3.494650    3.968233    4.586324    4.955520    5.693005    6.357751    6.898770    7.184257    7.349331    8.241439
     8.766135    9.447810   10.002670   10.650045   11.109397   12.429207   13.104403   13.603077   14.881921   21.811229
    26.629457

 fdd(*) eigenvalues. symmetry block  2
    -1.174762
 i,qaaresolved 1  0.0526824831

 qvv(*) eigenvalues. symmetry block  2
     0.901070    1.657946    1.886066    2.428408    4.283307    4.728289    6.038748    6.474981    6.959812    7.988628
     8.462852    8.908530    9.766930   11.191667   13.107905   14.440792

 fdd(*) eigenvalues. symmetry block  3
    -1.373330
 i,qaaresolved 1 -0.758777511

 qvv(*) eigenvalues. symmetry block  3
     0.417989    0.906427    1.230802    1.738095    2.114272    2.647258    2.919902    4.535429    5.278389    5.737564
     6.225683    6.812215    7.160200    7.759998    8.309099    9.420834    9.993925   10.729382   11.214877   12.046492
    14.618096   14.874311

 qvv(*) eigenvalues. symmetry block  4
   -13.142920  -11.686235   -8.965552   -8.302360   -7.033990   -6.070660   -5.689625   -3.726331   -2.615852   -1.263187
 !timer: motran                          user+sys=     0.010 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 user+sys=     2.360 walltime=     3.000

 all mcscf convergence criteria are not satisfied.
 iter=    1 emc= -113.8130827974 demc= 1.1381E+02 wnorm= 3.1290E-01 knorm= 2.6566E-01 apxde= 1.7740E-02    *not converged* 

               starting mcscf iteration...   2
 !timer:                                 user+sys=     4.800 walltime=     5.000

 orbital-state coupling will not be calculated this iteration.

 number of transformed integrals put on file:     95172
 number of records written:    35
 !timer: 2-e transformation              user+sys=     2.720 walltime=     2.000

 Size of orbital-Hessian matrix B:                    30288
 Total size of the state Hessian matrix M:                0
 Total size of HESSIAN-matrix for linear con          30288

 !timer: mosrt1                          user+sys=     0.060 walltime=     1.000
 !timer: mosrt2                          user+sys=     0.020 walltime=     0.000
 !timer: mosort                          user+sys=     0.080 walltime=     1.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000

   2 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 ciiter=   1 noldhv=  2 noldv=  2
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -113.9006084660     -145.0659182904        0.0000000000        0.0000100000
    2      -113.4243240473     -144.5896338717        0.0000000000        0.0100000000
 !timer: hmcvec                          user+sys=     0.000 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000

   1 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 ciiter=   1 noldhv=  1 noldv=  1
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -113.7635247995     -144.9288346239        0.0000000000        0.0000100000
 !timer: hmcvec                          user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: mqva                            user+sys=     0.000 walltime=     0.000
 !timer: mfva                            user+sys=     0.000 walltime=     0.000
 !timer: mfad                            user+sys=     0.000 walltime=     0.000
 !timer: mqad                            user+sys=     0.000 walltime=     0.000
 !timer: mfvd                            user+sys=     0.000 walltime=     0.000
 !timer: bvaad                           user+sys=     0.000 walltime=     0.000
 !timer: bvaadf                          user+sys=     0.000 walltime=     0.000
 !timer: mqaa                            user+sys=     0.000 walltime=     0.000
 !timer: mfaa                            user+sys=     0.000 walltime=     0.000
 !timer: badad                           user+sys=     0.010 walltime=     0.000
 !timer: badadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvava                           user+sys=     0.000 walltime=     0.000
 !timer: bvavaf                          user+sys=     0.000 walltime=     0.000
 !timer: bvavd                           user+sys=     0.000 walltime=     0.000
 !timer: bvavdf                          user+sys=     0.000 walltime=     0.000
 !timer: bvdad                           user+sys=     0.000 walltime=     0.000
 !timer: bvdadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvdvd                           user+sys=     0.000 walltime=     0.000
 !timer: hbcon                           user+sys=     0.010 walltime=     0.000

  tol(10)=  0. eshsci=  0.00272458554
 Total number of micro iterations:    8

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.99947780 pnorm= 0.0000E+00 rznorm= 3.0384E-06 rpnorm= 0.0000E+00 noldr=  8 nnewr=  8 nolds=  0 nnews=  0

 !timer: Direct Bxr time contribution    user+sys=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  user+sys=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    user+sys=     0.000 walltime=     0.000
 !timer: solvek total                    user+sys=     0.010 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
   -41.224572  -22.627824   -2.843859   -1.720304   -1.332372

 qvv(*) eigenvalues. symmetry block  1
     0.300696    0.531748    0.884779    1.186419    1.353507    1.470131    2.037952    2.396397    2.649438    3.277694
     3.531784    3.989771    4.621077    4.977466    5.705397    6.364600    6.921969    7.201051    7.373666    8.262330
     8.788674    9.482126   10.039691   10.670765   11.160301   12.490759   13.173668   13.703043   14.977537   21.871803
    26.672614

 fdd(*) eigenvalues. symmetry block  2
    -1.149417
 i,qaaresolved 1  0.0731542175

 qvv(*) eigenvalues. symmetry block  2
     0.892945    1.678674    1.894585    2.433923    4.319118    4.729297    6.050311    6.478145    6.993387    8.008460
     8.482814    8.978829    9.783830   11.259089   13.183887   14.537391

 fdd(*) eigenvalues. symmetry block  3
    -1.328115
 i,qaaresolved 1 -0.700895197

 qvv(*) eigenvalues. symmetry block  3
     0.419876    0.918114    1.241608    1.765841    2.122305    2.672019    2.929283    4.572849    5.289567    5.749210
     6.237897    6.842109    7.174176    7.785006    8.345325    9.481716   10.014272   10.765167   11.280969   12.060505
    14.703346   14.933853

 qvv(*) eigenvalues. symmetry block  4
   -13.142920  -11.686235   -8.965552   -8.302360   -7.033990   -6.070660   -5.689625   -3.726331   -2.615852   -1.263187
 !timer: motran                          user+sys=     0.010 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 user+sys=     2.850 walltime=     3.000

 all mcscf convergence criteria are not satisfied.
 iter=    2 emc= -113.8320666327 demc= 1.8984E-02 wnorm= 2.1797E-02 knorm= 3.2313E-02 apxde= 2.0695E-04    *not converged* 

               starting mcscf iteration...   3
 !timer:                                 user+sys=     7.650 walltime=     8.000

 orbital-state coupling will not be calculated this iteration.

 number of transformed integrals put on file:     95172
 number of records written:    35
 !timer: 2-e transformation              user+sys=     2.160 walltime=     2.000

 Size of orbital-Hessian matrix B:                    30288
 Total size of the state Hessian matrix M:                0
 Total size of HESSIAN-matrix for linear con          30288

 !timer: mosrt1                          user+sys=     0.060 walltime=     0.000
 !timer: mosrt2                          user+sys=     0.020 walltime=     0.000
 !timer: mosort                          user+sys=     0.080 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000

   2 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 ciiter=   1 noldhv=  2 noldv=  2
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -113.9003762193     -145.0656860437        0.0000000000        0.0000010000
    2      -113.4152730983     -144.5805829227        0.0000000000        0.0100000000
 !timer: hmcvec                          user+sys=     0.000 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000

   1 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 ciiter=   1 noldhv=  1 noldv=  1
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -113.7641735787     -144.9294834031        0.0000000000        0.0000010000
 !timer: hmcvec                          user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: mqva                            user+sys=     0.000 walltime=     0.000
 !timer: mfva                            user+sys=     0.000 walltime=     0.000
 !timer: mfad                            user+sys=     0.000 walltime=     0.000
 !timer: mqad                            user+sys=     0.000 walltime=     0.000
 !timer: mfvd                            user+sys=     0.000 walltime=     0.000
 !timer: bvaad                           user+sys=     0.000 walltime=     0.000
 !timer: bvaadf                          user+sys=     0.000 walltime=     0.000
 !timer: mqaa                            user+sys=     0.000 walltime=     0.000
 !timer: mfaa                            user+sys=     0.000 walltime=     0.000
 !timer: badad                           user+sys=     0.010 walltime=     0.000
 !timer: badadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvava                           user+sys=     0.000 walltime=     0.000
 !timer: bvavaf                          user+sys=     0.000 walltime=     0.000
 !timer: bvavd                           user+sys=     0.000 walltime=     0.000
 !timer: bvavdf                          user+sys=     0.000 walltime=     0.000
 !timer: bvdad                           user+sys=     0.000 walltime=     0.000
 !timer: bvdadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvdvd                           user+sys=     0.000 walltime=     0.000
 !timer: hbcon                           user+sys=     0.010 walltime=     0.000

  tol(10)=  0. eshsci=  2.92812683E-05
 Total number of micro iterations:    6

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.99999997 pnorm= 0.0000E+00 rznorm= 2.5603E-07 rpnorm= 0.0000E+00 noldr=  6 nnewr=  6 nolds=  0 nnews=  0

 !timer: Direct Bxr time contribution    user+sys=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  user+sys=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    user+sys=     0.000 walltime=     0.000
 !timer: solvek total                    user+sys=     0.020 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
   -41.226484  -22.620278   -2.843156   -1.717449   -1.331363

 qvv(*) eigenvalues. symmetry block  1
     0.302174    0.532459    0.886955    1.189058    1.354181    1.471665    2.039858    2.398059    2.650624    3.281282
     3.532506    3.991606    4.622234    4.980221    5.708695    6.368688    6.925487    7.205190    7.377011    8.267385
     8.792364    9.485620   10.043109   10.675138   11.162966   12.491464   13.174281   13.701887   14.977077   21.873686
    26.675770

 fdd(*) eigenvalues. symmetry block  2
    -1.149683
 i,qaaresolved 1  0.0790799102

 qvv(*) eigenvalues. symmetry block  2
     0.889369    1.676837    1.896579    2.435992    4.319974    4.732643    6.054271    6.482193    6.995514    8.012673
     8.487274    8.979199    9.787957   11.258603   13.184635   14.536857

 fdd(*) eigenvalues. symmetry block  3
    -1.321059
 i,qaaresolved 1 -0.702925938

 qvv(*) eigenvalues. symmetry block  3
     0.421557    0.918651    1.244941    1.766075    2.124736    2.673695    2.932295    4.574060    5.293123    5.753070
     6.242277    6.845516    7.178542    7.788564    8.348850    9.482690   10.018830   10.767820   11.280902   12.065994
    14.703285   14.934289

 qvv(*) eigenvalues. symmetry block  4
   -13.142920  -11.686235   -8.965552   -8.302360   -7.033990   -6.070660   -5.689625   -3.726331   -2.615852   -1.263187
 !timer: motran                          user+sys=     0.010 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 user+sys=     2.280 walltime=     2.000

 all mcscf convergence criteria are not satisfied.
 iter=    3 emc= -113.8322748990 demc= 2.0827E-04 wnorm= 2.3425E-04 knorm= 2.2604E-04 apxde= 1.3918E-08    *not converged* 

               starting mcscf iteration...   4
 !timer:                                 user+sys=     9.930 walltime=    10.000

 orbital-state coupling will not be calculated this iteration.

 number of transformed integrals put on file:     95172
 number of records written:    35
 !timer: 2-e transformation              user+sys=     2.220 walltime=     2.000

 Size of orbital-Hessian matrix B:                    30288
 Total size of the state Hessian matrix M:                0
 Total size of HESSIAN-matrix for linear con          30288

 !timer: mosrt1                          user+sys=     0.060 walltime=     0.000
 !timer: mosrt2                          user+sys=     0.020 walltime=     0.000
 !timer: mosort                          user+sys=     0.080 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000

   2 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 ciiter=   1 noldhv=  2 noldv=  2
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -113.9003715235     -145.0656813479        0.0000000000        0.0000010000
    2      -113.4151688632     -144.5804786876        0.0000000000        0.0100000000
 !timer: hmcvec                          user+sys=     0.000 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000

   1 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 ciiter=   1 noldhv=  1 noldv=  1
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -113.7641783024     -144.9294881268        0.0000000000        0.0000010000
 !timer: hmcvec                          user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: mqva                            user+sys=     0.000 walltime=     0.000
 !timer: mfva                            user+sys=     0.000 walltime=     0.000
 !timer: mfad                            user+sys=     0.000 walltime=     0.000
 !timer: mqad                            user+sys=     0.000 walltime=     0.000
 !timer: mfvd                            user+sys=     0.000 walltime=     0.000
 !timer: bvaad                           user+sys=     0.000 walltime=     0.000
 !timer: bvaadf                          user+sys=     0.000 walltime=     0.000
 !timer: mqaa                            user+sys=     0.000 walltime=     0.000
 !timer: mfaa                            user+sys=     0.000 walltime=     0.000
 !timer: badad                           user+sys=     0.000 walltime=     0.000
 !timer: badadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvava                           user+sys=     0.000 walltime=     0.000
 !timer: bvavaf                          user+sys=     0.000 walltime=     0.000
 !timer: bvavd                           user+sys=     0.000 walltime=     0.000
 !timer: bvavdf                          user+sys=     0.000 walltime=     0.000
 !timer: bvdad                           user+sys=     0.000 walltime=     0.000
 !timer: bvdadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvdvd                           user+sys=     0.010 walltime=     0.000
 !timer: hbcon                           user+sys=     0.010 walltime=     0.000

  tol(10)=  0. eshsci=  9.72033572E-08
 Total number of micro iterations:    2

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 1.00000000 pnorm= 0.0000E+00 rznorm= 2.6821E-07 rpnorm= 0.0000E+00 noldr=  2 nnewr=  2 nolds=  0 nnews=  0

 !timer: Direct Bxr time contribution    user+sys=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  user+sys=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    user+sys=     0.000 walltime=     0.000
 !timer: solvek total                    user+sys=     0.000 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
   -41.226466  -22.620163   -2.843124   -1.717413   -1.331333

 qvv(*) eigenvalues. symmetry block  1
     0.302185    0.532471    0.886976    1.189083    1.354198    1.471692    2.039880    2.398079    2.650649    3.281317
     3.532527    3.991636    4.622259    4.980255    5.708741    6.368739    6.925535    7.205229    7.377049    8.267429
     8.792412    9.485662   10.043150   10.675183   11.163008   12.491492   13.174314   13.701904   14.977099   21.873728
    26.675825

 fdd(*) eigenvalues. symmetry block  2
    -1.149683
 i,qaaresolved 1  0.0791339978

 qvv(*) eigenvalues. symmetry block  2
     0.889354    1.676844    1.896602    2.436012    4.320002    4.732692    6.054314    6.482243    6.995550    8.012712
     8.487309    8.979226    9.787998   11.258623   13.184670   14.536879

 fdd(*) eigenvalues. symmetry block  3
    -1.320991
 i,qaaresolved 1 -0.702916681

 qvv(*) eigenvalues. symmetry block  3
     0.421570    0.918660    1.244975    1.766088    2.124756    2.673719    2.932325    4.574084    5.293159    5.753114
     6.242329    6.845559    7.178588    7.788598    8.348882    9.482714   10.018874   10.767857   11.280926   12.066053
    14.703307   14.934313

 qvv(*) eigenvalues. symmetry block  4
   -13.142920  -11.686235   -8.965552   -8.302360   -7.033990   -6.070660   -5.689625   -3.726331   -2.615852   -1.263187
 !timer: motran                          user+sys=     0.000 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 user+sys=     2.320 walltime=     2.000

 all mcscf convergence criteria are not satisfied.
 iter=    4 emc= -113.8322749129 demc= 1.3947E-08 wnorm= 7.7763E-07 knorm= 6.2053E-07 apxde= 1.4439E-13    *not converged* 

               starting mcscf iteration...   5
 !timer:                                 user+sys=    12.250 walltime=    12.000

 orbital-state coupling will be calculated this iteration.

 number of transformed integrals put on file:     95172
 number of records written:    35
 !timer: 2-e transformation              user+sys=     2.240 walltime=     3.000

 Size of orbital-Hessian matrix B:                    30288
 Size of the orbital-state Hessian matrix C:            699
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:          30987

 !timer: mosrt1                          user+sys=     0.070 walltime=     0.000
 !timer: mosrt2                          user+sys=     0.010 walltime=     0.000
 !timer: mosort                          user+sys=     0.080 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000

   2 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 ciiter=   1 noldhv=  2 noldv=  2
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -113.9003714916     -145.0656813160        0.0000000000        0.0000010000
    2      -113.4151685057     -144.5804783302        0.0000000000        0.0100000000
 !timer: hmcvec                          user+sys=     0.000 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000

   1 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 ciiter=   1 noldhv=  1 noldv=  1
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -113.7641783343     -144.9294881587        0.0000000000        0.0000010000
 !timer: hmcvec                          user+sys=     0.000 walltime=     0.000
 !timer: cadu                            user+sys=     0.000 walltime=     0.000
 !timer: cvdu                            user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: cadu                            user+sys=     0.000 walltime=     0.000
 !timer: cvdu                            user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: mqva                            user+sys=     0.000 walltime=     0.000
 !timer: mfva                            user+sys=     0.000 walltime=     0.000
 !timer: mfvd                            user+sys=     0.000 walltime=     0.000
 !timer: bvaad                           user+sys=     0.000 walltime=     0.000
 !timer: bvaadf                          user+sys=     0.000 walltime=     0.000
 !timer: mfad                            user+sys=     0.000 walltime=     0.000
 !timer: mqad                            user+sys=     0.000 walltime=     0.000
 !timer: mqaa                            user+sys=     0.000 walltime=     0.000
 !timer: mfaa                            user+sys=     0.000 walltime=     0.000
 !timer: badad                           user+sys=     0.000 walltime=     0.000
 !timer: badadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvava                           user+sys=     0.000 walltime=     0.000
 !timer: bvavaf                          user+sys=     0.000 walltime=     0.000
 !timer: bvavd                           user+sys=     0.010 walltime=     0.000
 !timer: bvavdf                          user+sys=     0.000 walltime=     0.000
 !timer: bvdad                           user+sys=     0.000 walltime=     0.000
 !timer: bvdadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvdvd                           user+sys=     0.000 walltime=     0.000
 !timer: hbcon                           user+sys=     0.010 walltime=     0.000

  tol(10)=  0. eshsci=  3.35708878E-08
 performing all-state projection
 Total number of micro iterations:    1

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 1.00000000 pnorm= 0.0000E+00 rznorm= 3.2926E-07 rpnorm= 5.4144E-09 noldr=  1 nnewr=  1 nolds=  0 nnews=  0

 !timer: Direct Bxr time contribution    user+sys=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  user+sys=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    user+sys=     0.000 walltime=     0.000
 !timer: solvek total                    user+sys=     0.000 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
   -41.226466  -22.620163   -2.843124   -1.717413   -1.331333

 qvv(*) eigenvalues. symmetry block  1
     0.302185    0.532471    0.886976    1.189083    1.354198    1.471692    2.039880    2.398079    2.650649    3.281317
     3.532527    3.991636    4.622259    4.980255    5.708741    6.368739    6.925535    7.205229    7.377049    8.267429
     8.792412    9.485662   10.043150   10.675183   11.163008   12.491492   13.174314   13.701904   14.977099   21.873728
    26.675825

 fdd(*) eigenvalues. symmetry block  2
    -1.149683
 i,qaaresolved 1  0.0791339613

 qvv(*) eigenvalues. symmetry block  2
     0.889354    1.676844    1.896602    2.436012    4.320002    4.732692    6.054314    6.482243    6.995550    8.012712
     8.487309    8.979226    9.787998   11.258623   13.184670   14.536879

 fdd(*) eigenvalues. symmetry block  3
    -1.320991
 i,qaaresolved 1 -0.702916739

 qvv(*) eigenvalues. symmetry block  3
     0.421570    0.918660    1.244975    1.766088    2.124757    2.673719    2.932325    4.574084    5.293159    5.753114
     6.242329    6.845559    7.178588    7.788598    8.348882    9.482714   10.018874   10.767857   11.280926   12.066053
    14.703307   14.934313

 qvv(*) eigenvalues. symmetry block  4
   -13.142920  -11.686235   -8.965552   -8.302360   -7.033990   -6.070660   -5.689625   -3.726331   -2.615852   -1.263187
 !timer: motran                          user+sys=     0.010 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 user+sys=     2.360 walltime=     3.000

 all mcscf convergence criteria are satisfied.

 final mcscf convergence values:
 iter=    5 emc= -113.8322749129 demc= 1.8474E-13 wnorm= 2.6857E-07 knorm= 1.9389E-08 apxde= 2.9199E-15    *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 weight 0.500000 total energy= -113.900371492
   DRT #2 state # 1 weight 0.500000 total energy= -113.764178334
   ------------------------------------------------------------



          mcscf orbitals of the final iteration,  A1  block   1

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
   1O1s       0.99878885    -0.00055376    -0.00786053    -0.00231184    -0.00384736     0.00133586     0.03763129    -0.01457443
   2O1s      -0.00283975    -0.00034364     0.82303615    -0.39608746    -0.25602104     0.01262053    -0.17589892     0.07020516
   3O1s       0.00129043     0.00035760    -0.00692612    -0.00136538    -0.00422699     0.00116362    -0.00967775    -0.00276335
   4O1s       0.00066018     0.00000029    -0.07999916    -0.06235020    -0.07695169    -0.09257168    -2.51414168     0.79878966
   5O1pz      0.00495217     0.00109098     0.19796933     0.29818082     0.73243697    -0.08000587    -0.03997540    -0.01940698
   6O1pz     -0.00458431    -0.00090026     0.00043420    -0.00940545    -0.00706026    -0.00322063    -0.20476477     0.03423651
   7O1pz     -0.00162821    -0.00015241    -0.05347464    -0.02750786    -0.02890778    -0.11810397    -0.79008989     0.59642124
   8O1d0      0.00005057    -0.00000371     0.00235872     0.00122380     0.00330212     0.00010306     0.00049520    -0.00117637
   9O1d2+     0.00000682     0.00001208    -0.00000638    -0.00005184     0.00012073    -0.00014365     0.00087453     0.00115048
  10O1d0     -0.00019034    -0.00010314     0.00507432     0.00233026     0.00882914    -0.00019435    -0.03205697     0.02332390
  11O1d2+    -0.00002305    -0.00003573     0.00034870     0.00048711     0.00028354     0.00481602    -0.00507436    -0.01918580
  12O1f0      0.00004028     0.00001857    -0.00187903    -0.00050688    -0.00224653    -0.00006525     0.00453570    -0.00384102
  13O1f2+     0.00000559    -0.00002259    -0.00005268    -0.00010916    -0.00007488     0.00054851    -0.00020788    -0.00071027
  14C1s       0.00045736     0.99931713    -0.01703136    -0.00833298    -0.00232389     0.01689054     0.02540113    -0.01987684
  15C1s       0.00181076     0.00175782     0.39700206     0.69035641     0.00559357     0.40195909     0.17517191    -0.11291369
  16C1s      -0.00019372     0.00154135    -0.00708943    -0.01007847     0.00504499    -0.01564592    -0.03087268     0.06514628
  17C1s      -0.00178471     0.00011275    -0.10001115    -0.10917495    -0.09907281     2.68304008     1.44989345    -3.49307580
  18C1pz     -0.00060747    -0.00161480    -0.27698838     0.20451499    -0.58588452     0.23211660     0.04681630    -0.03601780
  19C1pz     -0.00225207     0.00026961     0.06140216    -0.01400462     0.07813316     0.10544114    -0.58870261     0.05049604
  20C1pz      0.00051629     0.00096247     0.09248673    -0.02319999     0.17984494     0.63823893    -2.06390495    -0.74072139
  21C1d0      0.00014187     0.00020332     0.00570786    -0.00001846     0.00506904    -0.00098289    -0.00284445    -0.00605755
  22C1d2+    -0.00000212    -0.00022401    -0.00156701    -0.00556213     0.00298227     0.00057204     0.00091480    -0.00629387
  23C1d0      0.00032530     0.00004906     0.00515984    -0.00102524     0.00227853     0.00602969     0.10998276     0.00005429
  24C1d2+     0.00030035     0.00000660    -0.00412273    -0.00637387     0.00202140    -0.06923327     0.07281360     0.09136052
  25C1f0      0.00003851     0.00028414     0.00375866     0.00151062     0.00189880     0.00317780     0.01467328    -0.01850544
  26C1f2+     0.00007041    -0.00022231    -0.00221352    -0.00218946    -0.00004247    -0.00745474     0.01092846     0.02787668
  27H1s       0.00033239     0.00027146     0.05571909     0.41319598    -0.28479494    -0.05549949    -0.05440899     0.24399219
  28H1s       0.00073410     0.00009764    -0.03576154    -0.07862503     0.03284156    -0.19333357     0.33739494     1.30846197
  29H1s      -0.00010312    -0.00076204    -0.02506183    -0.14094977     0.07032634    -1.72859498     0.16310822     0.35824645
  30H1py     -0.00008247     0.00026402     0.00043640    -0.00900247     0.00706617    -0.01920917    -0.00138488    -0.02113729
  31H1pz     -0.00000375     0.00018847    -0.00035067    -0.00409454     0.00247802    -0.00570471    -0.01226596    -0.00920629
  32H1py     -0.00013905    -0.00003434     0.00963557    -0.00873093     0.01568219     0.16895659    -0.07024432    -0.09940216
  33H1pz     -0.00039187    -0.00007742     0.00380961    -0.00369044     0.00058348     0.08714388    -0.11207654    -0.19914661
  34H1d1-     0.00003071    -0.00023682    -0.00112256     0.00142254    -0.00152292    -0.00745221     0.01387247     0.02116288
  35H1d0     -0.00001342     0.00001864     0.00024538    -0.00012301     0.00053140    -0.00017591     0.00056140     0.00172382
  36H1d2+     0.00000850     0.00005358     0.00066386    -0.00076158     0.00104578     0.00435448    -0.00109073    -0.00683452

               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
   1O1s      -0.02475773    -0.00811124     0.00852783     0.05503410    -0.17926677    -0.14658076    -0.05766807    -0.38255529
   2O1s      -0.07753567    -0.06381177     0.32296284     0.24679995    -1.06545122    -0.68211753    -0.33862329    -1.68812850
   3O1s      -0.00152804     0.03670167    -0.00243145    -0.01443020     0.00523192     0.00556327    -0.03521839     0.01713185
   4O1s       0.96915543    -2.37841134    -0.64474816    -0.19111820     3.41645449     5.16153284     0.45531767     1.84676229
   5O1pz     -0.13263689     0.58486033     0.33148236    -0.03213266    -0.36861441     0.50998496    -0.17352237    -0.16034089
   6O1pz      0.05395032    -0.09533753    -0.22511856    -0.01784822     0.40512372     0.88430391     0.20907703    -0.09345402
   7O1pz      0.57495069    -2.03791802     0.92259832     0.19239392     1.40027480     0.58591850     0.32080192     0.44908660
   8O1d0     -0.00003887     0.00870500    -0.00961850     0.00108184    -0.00207789    -0.00288835    -0.00011374    -0.00114571
   9O1d2+     0.00281434     0.00047330    -0.00040701     0.00320373    -0.00413436    -0.00097485    -0.00064705    -0.00191684
  10O1d0      0.02005202    -0.06764438    -0.01158059     0.00544690     0.05439680     0.00310268     0.09528261     0.00403521
  11O1d2+    -0.01521326    -0.00283268     0.00368583     0.06939379     0.01096218     0.01104113    -0.07497539     0.33644165
  12O1f0     -0.00265991     0.00774095    -0.00155513    -0.00052115    -0.00994583    -0.00357011    -0.01458713    -0.01135802
  13O1f2+    -0.00051187     0.00118950    -0.00047307     0.00227532     0.00338149     0.00078908     0.00190964     0.00023933
  14C1s      -0.02683405     0.11955358    -0.09864021    -0.01528034    -0.00052568     0.00278663     0.35212520    -0.14533763
  15C1s       0.54581307     0.39729312    -0.77027313    -0.10693443     0.18095904    -0.21693351     2.01634694    -0.53143795
  16C1s       0.10119563     0.02584424    -0.08213080     0.09938810     0.04373085     0.08495091     0.23313715    -0.05270677
  17C1s      -6.41962153    -0.00695026     0.89182331    -2.27162411    -1.50241690    -1.68382148     2.60910478    -0.19340919
  18C1pz      0.41442652     0.22460057     0.70751146    -0.09101658    -0.56028528     0.76948503     0.10261977    -0.40159666
  19C1pz     -0.33753970    -1.51688918     0.57274424    -0.28127903     1.33034069     1.63414731     1.81426104     0.32461939
  20C1pz     -1.39933656    -1.30595029    -1.27630507    -0.96491352     0.28887503     1.61652719     0.74533403     0.69555391
  21C1d0     -0.00244305     0.02330225     0.00131706     0.00360391    -0.02335973    -0.00070947    -0.02608057    -0.03326550
  22C1d2+     0.01749811     0.01856740    -0.01052552    -0.00998059     0.00368195    -0.00875807     0.02417185    -0.00247164
  23C1d0     -0.02198818     0.21039055     0.14956367    -0.03096105    -0.37311124    -0.44826516     0.17785699     0.35433164
  24C1d2+     0.37140672     0.12929933    -0.06899799     0.68120099    -0.45488024    -0.12092895    -0.28938123     0.12641443
  25C1f0     -0.02737931     0.00994452     0.03014814    -0.01925789    -0.03801962    -0.05331400    -0.04610436    -0.03850870
  26C1f2+     0.04452328     0.03469562    -0.01946231     0.03315548    -0.02291172    -0.01561534     0.01867287    -0.03608420
  27H1s      -0.44707531    -0.01598905     0.02047385    -0.00802679     0.03348843     0.12226637    -0.93292902    -0.02580259
  28H1s       1.24935433     0.99759094    -0.35262424     1.54918327    -0.70930959    -0.88263919    -1.22742915     0.21235790
  29H1s       2.25274850     0.26485180     0.08819513     0.31901376     0.12983488    -0.23367744    -0.77980620    -0.03309438
  30H1py      0.00502505    -0.00377207     0.01844123    -0.04192228     0.05035161    -0.02627320    -0.12193091     0.00118809
  31H1pz     -0.00322486    -0.02274503    -0.00026945    -0.04292077    -0.06709577     0.00103613    -0.02817574     0.02818117
  32H1py     -0.79851186    -0.50304813     0.12420260    -0.06227463    -0.21446046     0.48193392     1.65402143     0.23486598
  33H1pz     -0.44531306    -0.05434874     0.22889687     0.12494317     1.21531263     0.21379629     0.45524062    -0.73653917
  34H1d1-     0.04183425     0.03724507    -0.02384093     0.02197785    -0.03800016    -0.02528366     0.00245019    -0.01511614
  35H1d0     -0.00002830    -0.00249692     0.00256436     0.00164012    -0.00264638    -0.00682980     0.00611846     0.01277822
  36H1d2+    -0.02138464    -0.01978560     0.01239939    -0.00180849    -0.00235240     0.00538002     0.00684103     0.00428969

               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
   1O1s       0.32149371     0.01895193     0.11180500     0.01563549    -0.02526629    -0.28383199    -0.15637149    -0.23966168
   2O1s       1.42653332     0.15500021     0.50448064     0.09439302    -0.02919725    -0.97053185    -0.67932977    -1.18186421
   3O1s       0.01596367     0.09077584    -0.00353292    -0.00729799     0.04545855    -0.02865500    -0.06352405    -0.11947914
   4O1s      -2.15908395    -4.63487119     1.33397672     1.23801538    -0.34588558    -0.18300312    -0.05573022    -1.13908337
   5O1pz      0.09187424    -0.32632909     0.32707170     0.07977554     0.05922845     1.03820570     0.39423569    -0.38979213
   6O1pz      0.02790317    -0.67915546     0.25041815     0.16227424    -0.20090157    -1.11953223    -0.64241486    -0.54475404
   7O1pz     -0.59091697    -2.04994091     0.56330895     0.57993761    -0.17780379     0.11717424     0.06798942    -0.22521705
   8O1d0      0.00106331     0.00299652    -0.00137995    -0.00184995    -0.00259462    -0.02085642    -0.00093389     0.00847829
   9O1d2+    -0.00647109    -0.00069072    -0.00104533    -0.00014071     0.00944644    -0.00382834     0.00025668    -0.00531756
  10O1d0      0.00533126    -0.40155311     0.19715315    -0.04737932    -0.02596196     0.16285184     0.04395657     0.00067163
  11O1d2+     0.35129505     0.00442954     0.15854466     0.15772619     0.04408188    -0.04099890    -0.11286525     0.04933161
  12O1f0      0.01105381     0.02860381     0.00469159    -0.03575663     0.00547529     0.00635336     0.00426596     0.05985355
  13O1f2+    -0.00069807     0.00170046    -0.00392694    -0.01431130     0.02088180     0.00752299     0.03855360    -0.03465187
  14C1s       0.35774245     0.19310264    -0.40319508     0.05154709     0.07969837    -0.06299433    -0.07542622    -0.14866754
  15C1s       1.85953024     0.76440715    -2.16374064     0.55404581     0.52857850    -0.45148138     0.00758187    -0.68487014
  16C1s       0.25691027    -0.01341896    -0.35055332     0.28185623    -0.04086660    -0.30676165     0.19712666    -0.24075008
  17C1s       3.29714773    -0.19770420    -1.75788319     0.02172134     0.92954007     0.20845154     0.79841982     0.25432031
  18C1pz      1.00739962     1.13782003     0.60045883    -2.46950359     0.17078152     0.97884073     0.64778709     1.63005093
  19C1pz     -0.07963085    -4.95550345    -0.20012661     3.44693165    -0.22427272    -0.89280054    -0.54940808    -3.37823744
  20C1pz      0.02038328    -1.45802383    -0.62232842     0.98366645     0.19180188    -0.97980904    -0.31387436    -1.06572394
  21C1d0      0.02258935     0.00140991     0.01957741    -0.02893113    -0.00975213    -0.25508821    -0.06009246     0.03281367
  22C1d2+    -0.03635925     0.06695054     0.08193434    -0.02083867     0.48235131     0.02040992    -0.12207785    -0.01739031
  23C1d0     -0.27874896     0.30939698    -0.30453149    -0.13865403    -0.06933391     0.45833111     0.24572092     0.37066998
  24C1d2+    -0.79709988     0.37243873    -0.04694677    -0.32904091    -0.60780108     0.27201529     0.06012583     0.25394540
  25C1f0      0.04941470     0.00549795    -0.11049987     0.17227010     0.11762327     0.14377455     0.09302403     0.10836185
  26C1f2+    -0.07573218     0.15786731     0.19155998     0.16601662    -0.17742412     0.05716126    -0.31491953     0.25418045
  27H1s       0.60556444    -1.62067074    -2.70585647    -0.68394687    -0.54978131     0.40440592    -2.21049054     1.20171667
  28H1s      -2.65814299     3.37647266     3.05288402    -0.62966876    -0.43194634     0.17124710     1.46718870     0.39864675
  29H1s      -0.91114749     0.70130308     1.35694203     0.12958815     0.08110180    -0.02088577     0.51124895    -0.24761713
  30H1py     -0.01426138    -0.05393264    -0.05289619    -0.01130016     0.26414942     0.18175073    -0.19217257     0.10387865
  31H1pz     -0.06672676    -0.08804979    -0.02503077    -0.04150029     0.24504482    -0.19151250    -0.12924683     0.07371547
  32H1py      0.55057089    -0.92786338    -0.77897650     0.43713218    -0.21937933     0.02308046     0.35548432    -0.55344116
  33H1pz      1.13055037    -0.49427184     0.09209513     0.42982845     0.13208355    -0.29339284    -0.03607105    -0.52400019
  34H1d1-    -0.01066627     0.12259416     0.00978561    -0.00285912    -0.07295795     0.05778348    -0.04923955     0.35543713
  35H1d0     -0.00489839    -0.00109792     0.00007245    -0.01425082    -0.03586691     0.07424302    -0.04681692    -0.10912793
  36H1d2+     0.00047785    -0.04285944    -0.01667886     0.08216084     0.02672280    -0.02619655     0.21159683     0.05909136

               MO   25        MO   26        MO   27        MO   28        MO   29        MO   30        MO   31        MO   32
   1O1s      -0.01504650    -0.28592363     0.06890025    -0.10127154     0.06572015    -0.23658240     0.21618124     0.45462620
   2O1s      -0.04064193    -1.54929618     0.03227616    -0.25576018     0.56098910    -0.36544200     0.52410799     0.89658423
   3O1s       0.07352746    -0.21310247    -0.12989801     0.07113285     0.15793596     0.31585456    -0.18180426    -0.56556788
   4O1s       0.01119441    -1.22284414    -0.71313028     1.25025065     2.09977698     3.55525163    -1.89476405    -2.00047562
   5O1pz     -0.40936076    -0.17574440     1.46397848    -1.43826992    -0.69312306    -1.77586462     0.89920509     0.99694974
   6O1pz      0.18215041    -1.30672357    -1.67717062     1.70965622     1.82713301     2.52848381    -1.19164400    -1.59437641
   7O1pz      0.21646915    -0.50019846    -0.48510589     0.65562410     0.98969053     1.55646282    -0.77708889    -0.78172863
   8O1d0      0.02133119     0.03507543     0.00776750     0.01599059    -0.02843973    -0.01166462    -0.03797956    -0.12658224
   9O1d2+    -0.00595452    -0.01206294     0.00006423     0.01657086    -0.02619908     0.04252680     0.14805105    -0.02257588
  10O1d0     -0.03851825    -0.17113797    -0.09189182     0.07352134     0.19334291     0.17225547    -0.06352147    -0.14782868
  11O1d2+    -0.01350753    -0.03527960    -0.06136304    -0.02866567     0.00755766    -0.01176992     0.03355494    -0.00129494
  12O1f0     -0.00450331     0.03866417    -0.08640555     0.02798185     0.00025539     0.09032210     0.01021636     0.37871669
  13O1f2+     0.02147182    -0.00217200     0.05969457     0.14148554    -0.08773454     0.16460142     0.43815785    -0.05229084
  14C1s      -0.05222506    -0.04883133    -0.56366172     0.16400996     0.19455447    -0.32657041     0.39839397    -0.14742877
  15C1s      -0.01445505     0.41336131    -1.15571888    -0.18232176     1.34035789    -0.68197607     1.36272872     0.15689919
  16C1s       0.13579802     0.38452270     0.60968355    -0.64345971     0.73356374     0.34532519    -0.02065076     0.52611177
  17C1s      -0.81485571    -0.32228440    -0.52109889     0.08961548     0.42760899    -1.02705588     0.66286355     0.15142282
  18C1pz      0.10453176     1.81559655     0.00870988    -0.74601765     0.29102872    -0.38331416     0.40160268    -1.52904137
  19C1pz     -0.45284067    -3.96128584    -0.64612241     2.10812148     3.76528491     2.93151645    -1.38767283    -0.20660098
  20C1pz     -0.60535426    -1.00462998    -0.34678884     0.65898409     0.69624137     0.73945717    -0.36627138     0.13743411
  21C1d0      0.12210636     0.19001414     0.11348677     0.06483849    -0.10580317    -0.16112632     0.04721488     0.22509175
  22C1d2+    -0.00010171    -0.05603192    -0.18362546     0.21516748    -0.13892988    -0.35341024     0.03673850     0.05063728
  23C1d0     -0.20667717     0.30074730     0.15572453     0.13392324    -0.04207621    -0.31087632     0.00689735    -0.03820885
  24C1d2+    -0.09262208     0.43530840     0.30880865     0.03297331    -0.07591646    -0.23018768    -0.07520274     0.00676565
  25C1f0      0.04556528     0.38461625    -0.02961292    -0.04784373    -0.30109718     0.12262372    -0.07983129     0.09819444
  26C1f2+    -0.03193975     0.09998900    -0.15931740    -0.06562931     0.05170351    -0.20764759     0.39864779    -0.05687960
  27H1s      -0.61653593    -0.26103753    -1.14919335    -0.15174821     2.51245420    -1.68527868     2.19773279    -0.66461228
  28H1s       0.64476939     1.47727226     1.54851969    -0.20874089    -3.85636935    -0.06106280    -1.49228991     0.81482814
  29H1s       0.48255825     0.14361790     0.79953402    -0.04691680    -1.30603671     0.79401927    -1.02004457     0.33736803
  30H1py      0.52088015    -0.16010272     0.56157795    -0.09329213    -0.02735364     0.52635100    -0.35152326     0.07948921
  31H1pz     -0.69676788     0.15519447     0.38169102    -0.06397000    -0.08741083     0.43535368    -0.23203268    -0.02407957
  32H1py     -0.61328382    -0.24001602    -0.53461125     0.44907243     1.66978594     0.10418680     0.20372913    -0.36149884
  33H1pz      0.74549321    -0.47851540    -0.76683934    -0.52076607     0.45835164     0.27247166     0.43662927    -0.04588892
  34H1d1-    -0.16039019    -0.34525917     0.22442066     0.59926956    -0.35663043    -0.38584283    -0.13111723     0.07868039
  35H1d0     -0.02938449     0.09822447     0.06583029     0.14581341     0.08431481    -0.02439697    -0.04999544     0.02609653
  36H1d2+    -0.02712041    -0.00542152     0.11139320     0.09844847     0.41060775    -0.01397606     0.01310521    -0.03302994

               MO   33        MO   34        MO   35        MO   36
   1O1s       0.07405095    -0.06615857    -2.67859045    -2.54963034
   2O1s       0.09664200     0.12570815    -6.64710131    -6.63262919
   3O1s      -0.15714430     0.39890519     3.10817266     2.90632829
   4O1s      -0.22000079    -1.60578793     4.66373130    -0.59526738
   5O1pz     -0.03623860     1.03388937     0.39945522     1.29963857
   6O1pz     -0.02290058    -1.20576416    -1.12129178    -3.73712436
   7O1pz     -0.02803586    -1.27108000     0.57299867    -1.65882705
   8O1d0      0.01324270     0.32729292    -0.05260666    -0.06150476
   9O1d2+     0.56633982    -0.00802308     0.05649386    -0.00409761
  10O1d0     -0.01341986    -0.38321945     0.01177485    -0.32979047
  11O1d2+    -0.36338191     0.01675018    -0.09870654    -0.00953444
  12O1f0      0.00770558     0.20253557     0.02474322     0.13860089
  13O1f2+    -0.16290334     0.03059311     0.00765442    -0.04098108
  14C1s       0.10831006     0.32708523    -3.75306687     5.25293267
  15C1s       0.34015162     1.43491909   -11.24333116    16.19604110
  16C1s      -0.10803800     0.35877816     3.20822622    -4.78844046
  17C1s      -0.44730843    -0.09191425     0.15830717    -2.99781101
  18C1pz      0.02528029    -1.21827341     0.73550634     0.08004020
  19C1pz      0.03141365    -0.24694811    -4.50072070    -1.62468535
  20C1pz     -0.14384581     0.39556530    -0.63891794     0.22616345
  21C1d0      0.00414186     0.07416733     0.10014500     0.26197656
  22C1d2+    -0.03814457     0.02478796     0.23939740    -0.28663859
  23C1d0      0.08557056    -0.02090114     0.33523762     0.53101705
  24C1d2+     0.19651654     0.01767239     1.07251973    -0.45354212
  25C1f0      0.01431215    -0.11269342     0.02068866     0.48639245
  26C1f2+    -0.13970056     0.06759618     0.31022947    -0.28331176
  27H1s      -0.40335907     0.50792880    -0.02267948    -0.37586901
  28H1s       0.42937319    -0.20481429     5.03468043    -2.94298231
  29H1s       0.22357535    -0.25025282     0.62862463    -0.37288857
  30H1py      0.08791089    -0.08357967    -0.37603150     0.35437205
  31H1pz      0.02081649    -0.03015812    -0.17859083     0.29351640
  32H1py      0.05322673    -0.01123856    -1.93935289     1.63596598
  33H1pz     -0.18369368     0.00312318    -1.62988473     0.63103499
  34H1d1-     0.00831752     0.00713705     0.45466724    -0.42453508
  35H1d0      0.02977530    -0.00610385    -0.00230136     0.01966283
  36H1d2+     0.03173320     0.01257667    -0.17500898     0.17211332

          mcscf orbitals of the final iteration,  B1  block   2

               MO   37        MO   38        MO   39        MO   40        MO   41        MO   42        MO   43        MO   44
  37O1px      0.76240658    -0.61811801    -0.03217093    -0.58438152    -0.12407507    -0.22558533    -0.05150032     0.04535982
  38O1px      0.00651888     0.01269902     0.01197561    -0.40998277    -0.04658179    -0.24980480     0.03556282     0.03258250
  39O1px     -0.00470065    -0.14282454    -0.11500709     1.85209993    -0.80322890     0.03482082     0.83432373     0.34395654
  40O1d1+     0.00974729     0.00534087    -0.00648311    -0.02194533     0.02544345     0.01622349    -0.00252875     0.00014399
  41O1d1+     0.02883669     0.00995495    -0.02911723     0.08657723    -0.02966961    -0.05638877     1.15989998     0.48571651
  42O1f1+     0.00093592     0.00013771    -0.00002799     0.00248633    -0.00190452    -0.00175790     0.00059306     0.00611414
  43O1f3+     0.00013358     0.00237829    -0.00015894    -0.00035588    -0.00078166     0.00186419    -0.00077876     0.00121260
  44C1px      0.45766085     0.76288974    -0.59993396     0.18646275     0.14409062    -0.21663261    -1.35604026     3.30288600
  45C1px     -0.01288228     0.02598795    -0.52510808    -0.08528560     0.19568373    -0.26818428     0.69544735    -3.31428735
  46C1px     -0.03330417     0.22823506     1.55701000    -0.66884408     0.73066500    -0.37778932     0.04231746    -0.79136342
  47C1d1+    -0.02091008     0.01247219     0.00762546    -0.00026043     0.00652857     0.03164028    -0.13572233    -0.11096907
  48C1d1+    -0.03302484     0.01527888     0.03403787     0.40005565    -0.86218936    -1.26060309     0.94275515     0.33419162
  49C1f1+     0.00154679    -0.00032232    -0.00007943    -0.00991570     0.00522288     0.00172235    -0.00027964     0.01416843
  50C1f3+     0.00063362     0.00045968     0.00526437     0.01133895     0.01024690    -0.02998712    -0.01291915     0.03324836
  51H1px      0.00300484     0.00713024     0.01214311     0.01597979     0.04259910    -0.06751588    -0.00837043    -0.02651665
  52H1px      0.00802870     0.02490134    -0.09706747    -0.20622266    -0.52564759     1.02338250    -0.12588687     0.14588551
  53H1d2-    -0.00102177    -0.00293305     0.00043766     0.00992488    -0.00190928    -0.02387271     0.00186991    -0.01308395
  54H1d1+    -0.00076042    -0.00145944     0.00040743    -0.00243675    -0.00668248    -0.03239283     0.07686903     0.03083940

               MO   45        MO   46        MO   47        MO   48        MO   49        MO   50        MO   51        MO   52
  37O1px      0.34620395    -0.12156122    -1.29991846     0.99509255    -0.64878054     2.61876654     0.08369154    -0.01539975
  38O1px     -0.25203786    -0.20476247     0.79774219    -0.74707502     0.53505316    -2.92486782    -0.11010018     0.01909152
  39O1px     -0.07348341    -0.54992164    -0.25875549    -0.18243626     0.03889933    -0.65838641    -0.02126850    -0.00099667
  40O1d1+    -0.00518418    -0.00631305     0.04814065    -0.04567361     0.04258996     0.06287828    -0.03313398     0.02498485
  41O1d1+     0.23990211    -0.35689645    -0.87603319     0.15784743    -0.11089026    -0.68475216    -0.05473170     0.00809737
  42O1f1+    -0.00724311    -0.02296628     0.00438206     0.00791751    -0.00174581     0.03140628    -0.01315846     0.00638348
  43O1f3+     0.01789014    -0.00995615     0.01350414     0.00279592    -0.03102707    -0.00836500     0.15763605     0.59224803
  44C1px     -0.16235634     0.06638744    -0.64802565     0.11742523     0.01440305    -0.48091395    -0.45218247     0.15585479
  45C1px      0.10254119    -0.02117090     1.11940515     0.02275179     0.34694292     0.83839250     1.37825520    -0.44825597
  46C1px      0.30043657     0.15081367     0.49834629     0.09608533     0.26216190     0.03558503     0.12496657    -0.05108495
  47C1d1+     0.35695615     0.84160320    -0.52745705    -0.07790179     0.06980998    -0.17060342     0.40072367    -0.08846523
  48C1d1+     0.13213198    -1.31038902    -0.32536843     0.09879471     0.26289667    -0.53393292     0.35362282    -0.14298170
  49C1f1+     0.01052427     0.09269336     0.12015681    -0.08084064     0.05752299     0.12581625     0.02473417    -0.00496123
  50C1f3+     0.46639709    -0.22731498     0.25243428     0.11176516    -0.16625572    -0.05475719     0.55899423    -0.25300396
  51H1px      0.20913428     0.05500865     0.07159276     0.55677219     0.65813788    -0.05284532    -0.31020824     0.12587597
  52H1px     -0.42798750     0.29438603    -0.24364867    -0.45493866    -0.61748387     0.09145670    -0.51726971     0.17349226
  53H1d2-    -0.02965282    -0.23504018    -0.07375295    -0.10682848     0.49948872     0.09261913     0.76236380    -0.21109299
  54H1d1+    -0.18061635     0.14782076     0.21264978     0.51815251    -0.21662023    -0.11338440     0.52901181    -0.18533240

               MO   53        MO   54
  37O1px     -0.68282505     0.53788285
  38O1px      0.99585899    -0.82658903
  39O1px      0.25004671    -0.68184715
  40O1d1+     0.63473237     1.04080221
  41O1d1+     0.25516147    -1.32273318
  42O1f1+     0.14569786    -0.11639388
  43O1f3+    -0.01215199    -0.00073276
  44C1px      0.03928561    -0.10338480
  45C1px     -0.43771047     0.81614456
  46C1px      0.04590862     0.13676443
  47C1d1+     0.55473863    -0.40686199
  48C1d1+     0.14322725    -0.61017150
  49C1f1+    -0.10460199     0.07905986
  50C1f3+     0.04510808     0.00306935
  51H1px     -0.02449984     0.00086546
  52H1px      0.00704753     0.00820048
  53H1d2-    -0.02681451     0.00387185
  54H1d1+     0.11409582     0.01602995

          mcscf orbitals of the final iteration,  B2  block   3

               MO   55        MO   56        MO   57        MO   58        MO   59        MO   60        MO   61        MO   62
  55O1py      0.30704679     0.87560432     0.11639805    -0.34021106    -0.16898235     0.44597196    -0.13160178    -0.46601750
  56O1py      0.00290700     0.00517524    -0.03075526     0.05269476    -0.02087775     0.31445674    -0.20315590    -0.36318718
  57O1py     -0.03236107     0.05397163     0.30599221    -0.19705872    -0.23981981    -2.16586502     0.98550756     0.26659895
  58O1d1-     0.00705060     0.00466343    -0.00698366    -0.00653255     0.01402898     0.03098578    -0.00887564     0.02292408
  59O1d1-     0.01617525     0.01905445     0.00994542     0.04385115     0.02919487    -0.12209307    -0.00893607     0.04294264
  60O1f3-    -0.00001684    -0.00029076    -0.00000090     0.00023721    -0.00003519    -0.00008416    -0.00064127    -0.00003849
  61O1f1-     0.00064202     0.00044981    -0.00017171    -0.00031536     0.00034641    -0.00275752    -0.00134292     0.00047939
  62C1py      0.65753688    -0.16213722    -0.43660635    -0.27507479     0.82434956    -0.34361009    -0.15622598    -0.03362720
  63C1py     -0.04370033     0.02868319    -0.05896954     0.79680760    -0.45411051    -0.46138719    -0.13815852    -1.04810218
  64C1py     -0.13282747     0.06186506    -1.46970615     3.78685900    -1.55391981     2.08250869    -2.32445159    -0.82653866
  65C1d1-     0.00742962    -0.03259340    -0.00525703     0.00228051    -0.04002010     0.00460672    -0.00026917    -0.01894813
  66C1d1-     0.00368419    -0.02712256     0.00447269     0.91742766    -0.62381193    -1.08461249    -0.12462851    -2.16710312
  67C1f3-    -0.00034283    -0.00000415     0.00044337    -0.01381212     0.00706401     0.00608635     0.00641336     0.01158388
  68C1f1-     0.00089379     0.00142979     0.00017026     0.00220538     0.00137557     0.00677920     0.01079567    -0.01459595
  69H1s       0.52645836    -0.33824581    -0.01473886     0.00560032    -0.38833547    -0.03753766    -0.00879851    -0.53831817
  70H1s      -0.05739942    -0.02099309    -0.11710698    -1.88066995     0.20949393     0.76075632     0.82526079     2.49386286
  71H1s      -0.12367609     0.00337407     2.36650933    -1.81304972     1.76147116    -1.20721343     0.87400386     0.07747687
  72H1py     -0.00819662     0.00500272     0.01827186     0.02077022     0.00679960    -0.01687033    -0.04605157    -0.03200598
  73H1pz     -0.00830114     0.00220822     0.01946201     0.00866887     0.01222591    -0.01415053     0.03978353    -0.06124905
  74H1py     -0.01249641     0.01755353    -0.07173013     0.52859256    -0.44864537    -0.12551509     0.60511182    -0.58031314
  75H1pz     -0.01090386     0.01090296    -0.08966734     0.23434964    -0.43502964    -0.02955819    -0.70558889     0.36757189
  76H1d1-     0.00404709    -0.00125974     0.00377028    -0.03574661     0.02820981     0.01053574     0.00731335     0.01607165
  77H1d0      0.00022362     0.00034843     0.00053039     0.00387868     0.00008948    -0.00490933     0.00747983    -0.00536993
  78H1d2+    -0.00080346     0.00083251    -0.00120680     0.01157798    -0.00736983    -0.00316548     0.00013337    -0.02027179

               MO   63        MO   64        MO   65        MO   66        MO   67        MO   68        MO   69        MO   70
  55O1py     -0.06154928     0.13718170     0.06429115    -0.18147936    -0.21570523    -0.83271668    -0.13722864     1.12589068
  56O1py     -0.17775451     0.01805332     0.09655811     0.09867636    -0.09366303     0.44389750     0.16121645    -0.88226750
  57O1py      0.16865067     0.73720300     1.00443320    -0.59310661    -0.26449023    -0.10175208    -0.09618897     0.08536389
  58O1d1-    -0.00362569     0.00174561     0.00136986     0.00541713    -0.00409266     0.04934526    -0.04320331    -0.02915338
  59O1d1-    -0.07777782     1.15774201     0.69552913    -0.55377366    -0.21391729    -0.69475421     0.22056303     0.29462995
  60O1f3-    -0.00104244    -0.00028817     0.00237590     0.00403819     0.00355435     0.00163169     0.00185154     0.00944013
  61O1f1-     0.00036422     0.00074540     0.00555763    -0.00287864    -0.01785164     0.00561935    -0.00430449     0.00252500
  62C1py     -0.67926560    -0.69103376    -1.06935193    -2.92526620    -0.32598932     0.37996839     2.29532025     0.36325322
  63C1py     -4.28016259     1.50886914     0.72060586     6.58273969     0.49587552    -0.61734228    -6.00307292    -1.59843797
  64C1py     -2.30733973     0.32456610    -0.93561593     2.37218439    -0.16467123    -0.33718311    -1.68783555    -0.87900330
  65C1d1-    -0.06085837     0.00604444    -0.24598408     0.16795601     0.74302486    -0.56928823     0.04515171    -0.10990639
  66C1d1-    -2.51491234     1.79005708     1.33163851     0.93856384    -1.16049675    -0.28639672    -2.31067838    -0.18299091
  67C1f3-     0.03043402    -0.05014454     0.10537632     0.00277095     0.08173130     0.05456722     0.08275979     0.12963170
  68C1f1-    -0.00987594     0.00785612    -0.03060867    -0.00833885     0.05450734     0.11793415    -0.06692997    -0.06184080
  69H1s       0.04960948     1.88176345    -2.26816401     2.68292228     0.58111527     0.84833669     1.76321161     0.88439886
  70H1s       4.59653762    -3.18874203     1.45307882    -5.55874160    -0.04792974    -0.11859269     2.68061619     0.45980800
  71H1s       1.57619910    -0.39748313     1.29934238    -1.36113168    -0.23333564     0.00770670    -0.13606147     0.17306912
  72H1py      0.03092237     0.06674509     0.01221837     0.11784219    -0.35413673     0.09257895     0.17980582     0.15526310
  73H1pz      0.02674748     0.09724108     0.01182011    -0.03331153     0.05934129     0.39862494    -0.02222947     0.08723320
  74H1py     -2.52479170     0.67401434     0.14991137     1.48188758     0.37883205    -0.24644649    -1.86549195    -0.63532190
  75H1pz     -1.54037530     0.10568169    -0.34627758     1.29600667     0.09665483    -0.62282903    -0.88841817    -0.41859218
  76H1d1-     0.12096449    -0.06536071     0.04830150    -0.07196979    -0.02259712     0.06447057     0.48071772    -0.10048409
  77H1d0     -0.00526138    -0.00220107    -0.03524378     0.00316508    -0.03152352     0.00875332    -0.07887792     0.08070266
  78H1d2+    -0.04609455     0.00797187     0.05436750     0.09884435    -0.04114263     0.00122652    -0.00103387    -0.26215383

               MO   71        MO   72        MO   73        MO   74        MO   75        MO   76        MO   77        MO   78
  55O1py      1.53278063    -2.34689393     0.50735281     0.12771319    -0.05348361     0.10977229    -0.08744240    -1.00436467
  56O1py     -1.53615345     2.60336989    -0.65934468    -0.44478370     0.19042971    -0.35461942     0.29362230     1.70811070
  57O1py      0.06141157     0.72151845    -0.33346153    -0.00815329     0.08322968    -0.27833211    -0.48377393     0.81359030
  58O1d1-     0.00096776     0.03471529     0.00862109     0.25892510    -0.12255281     0.08474900     1.13896981    -0.39353607
  59O1d1-    -0.02484368     0.54244096    -0.18124867    -0.37372720     0.16007626    -0.41040606    -0.57317054     1.51272054
  60O1f3-    -0.00473244     0.00837760     0.04907309     0.06812457     0.18579954     0.00669453     0.00595092     0.00621658
  61O1f1-    -0.00291020    -0.03573147     0.00718574     0.04887887    -0.02397804    -0.00180369     0.03363152     0.18983241
  62C1py      0.09119205     0.43295665     2.09637737    -0.08148801    -0.56786649    -0.11981985     0.15303397     0.39055523
  63C1py     -0.72885274    -0.80257448    -1.09073559    -3.15604354     1.34910459     5.37726755     0.30549862    -1.47744383
  64C1py     -1.13944492     0.14742247    -0.38977593    -0.61318034     0.25144346     0.99641884     0.38567017    -0.33252094
  65C1d1-     0.10623323     0.08609067     0.15120710    -0.19397034    -0.03961813     1.39152195     0.09973722     0.54642632
  66C1d1-    -0.12650474     1.24538046    -0.87738997    -1.06671577     0.56994876     1.37653683    -0.67739743     0.57951509
  67C1f3-     0.01522826     0.06313256     0.09832689     0.11130612    -0.09668149    -0.08036389    -0.05363627    -0.03534206
  68C1f1-     0.04524332    -0.01693342     0.01089052     0.15796152    -0.05510356     0.14093475    -0.14201124    -0.25637847
  69H1s      -0.37912443    -0.03384893     2.66163530    -0.47805388    -0.80017436    -0.69042086     0.49610410     0.11888223
  70H1s       0.89902458    -0.42097148    -1.76737016     2.97585057    -0.29910704    -3.89946805    -0.29587959     0.20329703
  71H1s       0.63244229    -0.10367536    -1.40522542     0.40997851     0.36793955     0.04779389    -0.40420578     0.03571810
  72H1py     -0.29807339    -0.14220880    -0.42085352     0.07351315     0.07256628     0.92625534    -0.13836204    -0.22875826
  73H1pz      0.49277180     0.08851265    -0.38975906    -0.53079208     0.30711629     0.24407997     0.24097104     0.34675527
  74H1py      0.15888648     0.36588925     0.73679775    -1.13000458     0.13297911     1.72786657    -0.11437311    -0.16965586
  75H1pz     -0.71861992    -0.41874202     0.61116536    -0.91921282     0.11954848     1.07869698     0.36504332    -0.06962435
  76H1d1-     0.32472543     0.06092757    -0.50696341     0.67527491    -0.07659432    -0.55678528    -0.23615989    -0.23768541
  77H1d0      0.10941594     0.15842219     0.02950859     0.13610813    -0.07067358     0.08533814    -0.10877801    -0.07584075
  78H1d2+     0.16305611     0.03519707     0.23367672    -0.03938009    -0.04281930     0.34490312    -0.06476872    -0.09888595

          mcscf orbitals of the final iteration,  A2  block   4

               MO   79        MO   80        MO   81        MO   82        MO   83        MO   84        MO   85        MO   86
  79O1d2-    -0.37173501     0.25583661    -0.22656857    -0.23072327     0.49877345    -0.80972979    -0.38461612    -0.10260161
  80O1d2-    -0.58011904     0.36629159    -0.15892207     0.04984600    -0.48101819     0.83821808     0.42956576     0.15639562
  81O1f2-    -0.14515653    -0.01407676     0.27056438    -0.04752219     0.58779835     0.00748779     0.75474095     0.12783989
  82C1d2-    -0.21941852    -0.65351884    -0.25242105    -0.88382981    -0.15731873     0.17192496    -0.04881580    -0.09347774
  83C1d2-    -0.11029662    -0.37155838     0.22049806     1.28498686    -0.59734902    -1.29674786     0.36892207    -0.21746058
  84C1f2-     0.17384208     0.21461759    -0.74110833     0.14140696    -0.27058280    -0.34822566     0.77656189     0.06887300
  85H1px     -0.01934795    -0.04005811    -0.02515524    -0.00792440     0.07479840     0.09224890     0.05344418     0.34801781
  86H1px      0.00004164     0.02828402    -0.24537208    -0.17297596     0.71162217     0.89632435    -0.55608715    -0.18530708
  87H1d2-     0.02614714     0.03406823    -0.00727470     0.05473628    -0.09866518    -0.09064063    -0.06476358     0.21934729
  88H1d1+     0.00571187     0.01602307     0.02987122     0.02195944    -0.06292940    -0.02735644     0.12144095    -0.62821823

               MO   87        MO   88
  79O1d2-     0.11970191    -0.05525921
  80O1d2-    -0.26673040     0.04977287
  81O1f2-    -0.28212952    -0.11382300
  82C1d2-    -0.29062988    -0.40462083
  83C1d2-     0.19521463    -0.92680377
  84C1f2-    -0.63691685    -0.70730155
  85H1px      0.83234206    -0.27023543
  86H1px     -0.47995374     0.92634784
  87H1d2-    -0.43045440    -0.84340392
  88H1d1+     0.10401894    -0.50879351

          natural orbitals of the final iteration, block  1

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8

  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     0.00000000     0.00000000     0.00000000

   1O1s       0.99878885    -0.00055376    -0.00786053    -0.00231184    -0.00384736     0.00133586     0.03763129    -0.01457443
   2O1s      -0.00283975    -0.00034364     0.82303615    -0.39608746    -0.25602105     0.01262053    -0.17589892     0.07020514
   3O1s       0.00129043     0.00035760    -0.00692612    -0.00136538    -0.00422699     0.00116362    -0.00967775    -0.00276335
   4O1s       0.00066018     0.00000029    -0.07999916    -0.06235020    -0.07695169    -0.09257162    -2.51414168     0.79878962
   5O1pz      0.00495217     0.00109098     0.19796934     0.29818082     0.73243696    -0.08000587    -0.03997541    -0.01940698
   6O1pz     -0.00458431    -0.00090026     0.00043420    -0.00940545    -0.00706026    -0.00322063    -0.20476477     0.03423651
   7O1pz     -0.00162821    -0.00015241    -0.05347464    -0.02750786    -0.02890778    -0.11810396    -0.79008988     0.59642121
   8O1d0      0.00005057    -0.00000371     0.00235872     0.00122380     0.00330212     0.00010306     0.00049520    -0.00117637
   9O1d2+     0.00000682     0.00001208    -0.00000638    -0.00005184     0.00012073    -0.00014365     0.00087453     0.00115048
  10O1d0     -0.00019034    -0.00010314     0.00507432     0.00233026     0.00882913    -0.00019435    -0.03205697     0.02332390
  11O1d2+    -0.00002305    -0.00003573     0.00034870     0.00048711     0.00028354     0.00481602    -0.00507436    -0.01918579
  12O1f0      0.00004028     0.00001857    -0.00187903    -0.00050688    -0.00224653    -0.00006525     0.00453570    -0.00384102
  13O1f2+     0.00000559    -0.00002259    -0.00005268    -0.00010916    -0.00007488     0.00054851    -0.00020788    -0.00071027
  14C1s       0.00045736     0.99931713    -0.01703136    -0.00833298    -0.00232389     0.01689054     0.02540113    -0.01987684
  15C1s       0.00181076     0.00175782     0.39700206     0.69035641     0.00559357     0.40195909     0.17517191    -0.11291368
  16C1s      -0.00019372     0.00154135    -0.00708943    -0.01007847     0.00504499    -0.01564592    -0.03087269     0.06514628
  17C1s      -0.00178471     0.00011275    -0.10001115    -0.10917495    -0.09907281     2.68304003     1.44989358    -3.49307583
  18C1pz     -0.00060747    -0.00161480    -0.27698839     0.20451499    -0.58588451     0.23211660     0.04681630    -0.03601780
  19C1pz     -0.00225207     0.00026961     0.06140216    -0.01400462     0.07813316     0.10544115    -0.58870259     0.05049601
  20C1pz      0.00051629     0.00096247     0.09248674    -0.02319999     0.17984494     0.63823898    -2.06390492    -0.74072142
  21C1d0      0.00014187     0.00020332     0.00570786    -0.00001846     0.00506904    -0.00098289    -0.00284445    -0.00605755
  22C1d2+    -0.00000212    -0.00022401    -0.00156701    -0.00556213     0.00298227     0.00057204     0.00091480    -0.00629387
  23C1d0      0.00032530     0.00004906     0.00515984    -0.00102524     0.00227853     0.00602968     0.10998276     0.00005429
  24C1d2+     0.00030035     0.00000660    -0.00412273    -0.00637387     0.00202140    -0.06923327     0.07281359     0.09136052
  25C1f0      0.00003851     0.00028414     0.00375866     0.00151062     0.00189880     0.00317780     0.01467328    -0.01850544
  26C1f2+     0.00007041    -0.00022231    -0.00221352    -0.00218946    -0.00004247    -0.00745474     0.01092846     0.02787668
  27H1s       0.00033239     0.00027146     0.05571908     0.41319598    -0.28479494    -0.05549949    -0.05440899     0.24399217
  28H1s       0.00073410     0.00009764    -0.03576154    -0.07862503     0.03284156    -0.19333357     0.33739491     1.30846200
  29H1s      -0.00010312    -0.00076204    -0.02506183    -0.14094977     0.07032634    -1.72859498     0.16310817     0.35824647
  30H1py     -0.00008247     0.00026402     0.00043640    -0.00900247     0.00706617    -0.01920917    -0.00138488    -0.02113729
  31H1pz     -0.00000375     0.00018847    -0.00035066    -0.00409454     0.00247802    -0.00570471    -0.01226596    -0.00920629
  32H1py     -0.00013905    -0.00003434     0.00963557    -0.00873093     0.01568219     0.16895659    -0.07024431    -0.09940217
  33H1pz     -0.00039187    -0.00007742     0.00380961    -0.00369044     0.00058348     0.08714389    -0.11207653    -0.19914662
  34H1d1-     0.00003071    -0.00023682    -0.00112256     0.00142254    -0.00152292    -0.00745221     0.01387247     0.02116288
  35H1d0     -0.00001342     0.00001864     0.00024538    -0.00012301     0.00053140    -0.00017591     0.00056140     0.00172382
  36H1d2+     0.00000850     0.00005358     0.00066386    -0.00076158     0.00104578     0.00435448    -0.00109073    -0.00683452

               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16

  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

   1O1s      -0.02475773    -0.00811124     0.00852782     0.05503410    -0.17926677    -0.14658077    -0.05766808    -0.38255529
   2O1s      -0.07753569    -0.06381174     0.32296284     0.24679997    -1.06545122    -0.68211756    -0.33862331    -1.68812847
   3O1s      -0.00152804     0.03670167    -0.00243145    -0.01443020     0.00523192     0.00556327    -0.03521839     0.01713185
   4O1s       0.96915533    -2.37841145    -0.64474794    -0.19111826     3.41645438     5.16153289     0.45531774     1.84676221
   5O1pz     -0.13263688     0.58486035     0.33148234    -0.03213265    -0.36861441     0.50998495    -0.17352237    -0.16034089
   6O1pz      0.05395032    -0.09533755    -0.22511854    -0.01784823     0.40512369     0.88430391     0.20907704    -0.09345402
   7O1pz      0.57495060    -2.03791800     0.92259844     0.19239390     1.40027480     0.58591850     0.32080193     0.44908657
   8O1d0     -0.00003887     0.00870500    -0.00961850     0.00108184    -0.00207789    -0.00288835    -0.00011374    -0.00114571
   9O1d2+     0.00281434     0.00047330    -0.00040701     0.00320373    -0.00413435    -0.00097485    -0.00064705    -0.00191684
  10O1d0      0.02005202    -0.06764439    -0.01158058     0.00544690     0.05439680     0.00310268     0.09528260     0.00403520
  11O1d2+    -0.01521326    -0.00283268     0.00368583     0.06939379     0.01096219     0.01104112    -0.07497539     0.33644166
  12O1f0     -0.00265991     0.00774095    -0.00155514    -0.00052115    -0.00994583    -0.00357011    -0.01458713    -0.01135802
  13O1f2+    -0.00051187     0.00118950    -0.00047307     0.00227532     0.00338149     0.00078908     0.00190964     0.00023933
  14C1s      -0.02683405     0.11955358    -0.09864022    -0.01528034    -0.00052568     0.00278663     0.35212520    -0.14533762
  15C1s       0.54581310     0.39729308    -0.77027316    -0.10693445     0.18095904    -0.21693354     2.01634695    -0.53143791
  16C1s       0.10119564     0.02584423    -0.08213080     0.09938810     0.04373085     0.08495090     0.23313715    -0.05270677
  17C1s      -6.41962153    -0.00694995     0.89182315    -2.27162406    -1.50241689    -1.68382161     2.60910476    -0.19340910
  18C1pz      0.41442651     0.22460059     0.70751149    -0.09101657    -0.56028530     0.76948502     0.10261978    -0.40159662
  19C1pz     -0.33753977    -1.51688915     0.57274432    -0.28127905     1.33034064     1.63414726     1.81426108     0.32461935
  20C1pz     -1.39933660    -1.30595032    -1.27630500    -0.96491351     0.28887498     1.61652718     0.74533406     0.69555391
  21C1d0     -0.00244305     0.02330225     0.00131706     0.00360391    -0.02335973    -0.00070947    -0.02608057    -0.03326550
  22C1d2+     0.01749811     0.01856740    -0.01052552    -0.00998059     0.00368195    -0.00875807     0.02417185    -0.00247164
  23C1d0     -0.02198818     0.21039056     0.14956365    -0.03096104    -0.37311123    -0.44826517     0.17785699     0.35433164
  24C1d2+     0.37140673     0.12929932    -0.06899799     0.68120101    -0.45488021    -0.12092895    -0.28938123     0.12641442
  25C1f0     -0.02737931     0.00994452     0.03014814    -0.01925789    -0.03801962    -0.05331400    -0.04610436    -0.03850869
  26C1f2+     0.04452328     0.03469562    -0.01946231     0.03315548    -0.02291172    -0.01561534     0.01867287    -0.03608421
  27H1s      -0.44707531    -0.01598903     0.02047385    -0.00802677     0.03348843     0.12226637    -0.93292898    -0.02580258
  28H1s       1.24935437     0.99759086    -0.35262428     1.54918328    -0.70930954    -0.88263914    -1.22742919     0.21235785
  29H1s       2.25274850     0.26485173     0.08819515     0.31901375     0.12983488    -0.23367741    -0.77980622    -0.03309440
  30H1py      0.00502505    -0.00377207     0.01844123    -0.04192229     0.05035161    -0.02627320    -0.12193091     0.00118809
  31H1pz     -0.00322487    -0.02274503    -0.00026945    -0.04292077    -0.06709577     0.00103613    -0.02817574     0.02818117
  32H1py     -0.79851188    -0.50304809     0.12420261    -0.06227462    -0.21446046     0.48193387     1.65402145     0.23486599
  33H1pz     -0.44531306    -0.05434871     0.22889686     0.12494314     1.21531262     0.21379629     0.45524061    -0.73653916
  34H1d1-     0.04183425     0.03724506    -0.02384093     0.02197785    -0.03800016    -0.02528366     0.00245019    -0.01511614
  35H1d0     -0.00002830    -0.00249692     0.00256436     0.00164012    -0.00264639    -0.00682980     0.00611846     0.01277822
  36H1d2+    -0.02138464    -0.01978560     0.01239939    -0.00180849    -0.00235240     0.00538002     0.00684103     0.00428969

               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24

  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

   1O1s       0.32149372     0.01895194     0.11180500     0.01563549    -0.02526629    -0.28383198    -0.15637148    -0.23966169
   2O1s       1.42653336     0.15500023     0.50448061     0.09439302    -0.02919723    -0.97053183    -0.67932971    -1.18186426
   3O1s       0.01596367     0.09077584    -0.00353292    -0.00729798     0.04545855    -0.02865500    -0.06352405    -0.11947915
   4O1s      -2.15908388    -4.63487126     1.33397672     1.23801536    -0.34588556    -0.18300309    -0.05573017    -1.13908339
   5O1pz      0.09187424    -0.32632909     0.32707169     0.07977554     0.05922846     1.03820571     0.39423572    -0.38979212
   6O1pz      0.02790319    -0.67915546     0.25041815     0.16227424    -0.20090157    -1.11953221    -0.64241484    -0.54475408
   7O1pz     -0.59091694    -2.04994093     0.56330895     0.57993760    -0.17780379     0.11717425     0.06798944    -0.22521705
   8O1d0      0.00106331     0.00299651    -0.00137995    -0.00184995    -0.00259462    -0.02085642    -0.00093389     0.00847829
   9O1d2+    -0.00647109    -0.00069072    -0.00104533    -0.00014071     0.00944644    -0.00382834     0.00025668    -0.00531756
  10O1d0      0.00533127    -0.40155311     0.19715315    -0.04737933    -0.02596196     0.16285184     0.04395657     0.00067163
  11O1d2+     0.35129504     0.00442954     0.15854466     0.15772619     0.04408187    -0.04099890    -0.11286525     0.04933161
  12O1f0      0.01105381     0.02860381     0.00469159    -0.03575663     0.00547529     0.00635335     0.00426596     0.05985355
  13O1f2+    -0.00069807     0.00170046    -0.00392694    -0.01431130     0.02088180     0.00752299     0.03855360    -0.03465187
  14C1s       0.35774244     0.19310264    -0.40319508     0.05154709     0.07969837    -0.06299432    -0.07542622    -0.14866754
  15C1s       1.85953023     0.76440716    -2.16374064     0.55404578     0.52857849    -0.45148137     0.00758187    -0.68487011
  16C1s       0.25691027    -0.01341897    -0.35055332     0.28185623    -0.04086660    -0.30676166     0.19712667    -0.24075007
  17C1s       3.29714771    -0.19770414    -1.75788320     0.02172129     0.92954008     0.20845155     0.79841979     0.25432038
  18C1pz      1.00739961     1.13782007     0.60045884    -2.46950358     0.17078149     0.97884068     0.64778701     1.63005098
  19C1pz     -0.07963077    -4.95550349    -0.20012664     3.44693159    -0.22427267    -0.89280044    -0.54940792    -3.37823750
  20C1pz      0.02038330    -1.45802385    -0.62232843     0.98366644     0.19180189    -0.97980900    -0.31387432    -1.06572395
  21C1d0      0.02258935     0.00140991     0.01957741    -0.02893113    -0.00975213    -0.25508821    -0.06009246     0.03281367
  22C1d2+    -0.03635925     0.06695054     0.08193434    -0.02083867     0.48235131     0.02040992    -0.12207785    -0.01739031
  23C1d0     -0.27874898     0.30939698    -0.30453149    -0.13865403    -0.06933391     0.45833110     0.24572091     0.37067000
  24C1d2+    -0.79709989     0.37243872    -0.04694676    -0.32904090    -0.60780108     0.27201528     0.06012582     0.25394540
  25C1f0      0.04941470     0.00549795    -0.11049987     0.17227010     0.11762327     0.14377455     0.09302403     0.10836186
  26C1f2+    -0.07573218     0.15786731     0.19155998     0.16601662    -0.17742413     0.05716126    -0.31491954     0.25418044
  27H1s       0.60556443    -1.62067072    -2.70585644    -0.68394690    -0.54978135     0.40440594    -2.21049061     1.20171661
  28H1s      -2.65814302     3.37647263     3.05288403    -0.62966869    -0.43194632     0.17124705     1.46718871     0.39864676
  29H1s      -0.91114748     0.70130306     1.35694202     0.12958817     0.08110182    -0.02088578     0.51124898    -0.24761712
  30H1py     -0.01426138    -0.05393264    -0.05289619    -0.01130016     0.26414941     0.18175073    -0.19217257     0.10387864
  31H1pz     -0.06672676    -0.08804979    -0.02503077    -0.04150029     0.24504482    -0.19151250    -0.12924683     0.07371546
  32H1py      0.55057090    -0.92786338    -0.77897650     0.43713215    -0.21937932     0.02308048     0.35548434    -0.55344114
  33H1pz      1.13055040    -0.49427183     0.09209512     0.42982844     0.13208356    -0.29339283    -0.03607103    -0.52400019
  34H1d1-    -0.01066627     0.12259416     0.00978562    -0.00285912    -0.07295795     0.05778348    -0.04923955     0.35543712
  35H1d0     -0.00489839    -0.00109792     0.00007245    -0.01425082    -0.03586691     0.07424302    -0.04681691    -0.10912793
  36H1d2+     0.00047785    -0.04285944    -0.01667886     0.08216084     0.02672280    -0.02619655     0.21159683     0.05909136

               MO   25        MO   26        MO   27        MO   28        MO   29        MO   30        MO   31        MO   32

  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

   1O1s      -0.01504652    -0.28592362     0.06890025    -0.10127154     0.06572015    -0.23658240     0.21618123     0.45462620
   2O1s      -0.04064200    -1.54929617     0.03227615    -0.25576017     0.56098908    -0.36544200     0.52410797     0.89658422
   3O1s       0.07352745    -0.21310247    -0.12989801     0.07113285     0.15793596     0.31585455    -0.18180425    -0.56556788
   4O1s       0.01119436    -1.22284414    -0.71313035     1.25025063     2.09977702     3.55525161    -1.89476401    -2.00047564
   5O1pz     -0.40936076    -0.17574439     1.46397852    -1.43826989    -0.69312308    -1.77586461     0.89920507     0.99694975
   6O1pz      0.18215035    -1.30672357    -1.67717069     1.70965619     1.82713303     2.52848379    -1.19164396    -1.59437643
   7O1pz      0.21646912    -0.50019846    -0.48510593     0.65562409     0.98969054     1.55646281    -0.77708888    -0.78172864
   8O1d0      0.02133119     0.03507543     0.00776750     0.01599059    -0.02843973    -0.01166462    -0.03797956    -0.12658225
   9O1d2+    -0.00595452    -0.01206294     0.00006423     0.01657086    -0.02619908     0.04252681     0.14805105    -0.02257588
  10O1d0     -0.03851825    -0.17113797    -0.09189182     0.07352133     0.19334291     0.17225547    -0.06352146    -0.14782868
  11O1d2+    -0.01350753    -0.03527960    -0.06136304    -0.02866567     0.00755765    -0.01176992     0.03355494    -0.00129494
  12O1f0     -0.00450331     0.03866417    -0.08640555     0.02798185     0.00025539     0.09032210     0.01021636     0.37871669
  13O1f2+     0.02147182    -0.00217200     0.05969457     0.14148554    -0.08773454     0.16460142     0.43815785    -0.05229083
  14C1s      -0.05222507    -0.04883133    -0.56366173     0.16400995     0.19455446    -0.32657041     0.39839398    -0.14742877
  15C1s      -0.01445504     0.41336130    -1.15571889    -0.18232181     1.34035786    -0.68197608     1.36272874     0.15689918
  16C1s       0.13579803     0.38452269     0.60968355    -0.64345972     0.73356375     0.34532519    -0.02065076     0.52611177
  17C1s      -0.81485572    -0.32228436    -0.52109889     0.08961546     0.42760897    -1.02705588     0.66286355     0.15142283
  18C1pz      0.10453186     1.81559655     0.00870991    -0.74601767     0.29102870    -0.38331416     0.40160271    -1.52904135
  19C1pz     -0.45284085    -3.96128581    -0.64612255     2.10812146     3.76528495     2.93151643    -1.38767281    -0.20660101
  20C1pz     -0.60535432    -1.00462996    -0.34678887     0.65898408     0.69624138     0.73945716    -0.36627138     0.13743410
  21C1d0      0.12210636     0.19001414     0.11348677     0.06483849    -0.10580317    -0.16112632     0.04721487     0.22509175
  22C1d2+    -0.00010171    -0.05603192    -0.18362546     0.21516748    -0.13892987    -0.35341024     0.03673850     0.05063728
  23C1d0     -0.20667715     0.30074730     0.15572454     0.13392324    -0.04207621    -0.31087632     0.00689735    -0.03820885
  24C1d2+    -0.09262206     0.43530840     0.30880865     0.03297331    -0.07591646    -0.23018768    -0.07520274     0.00676565
  25C1f0      0.04556530     0.38461625    -0.02961291    -0.04784373    -0.30109718     0.12262372    -0.07983130     0.09819444
  26C1f2+    -0.03193974     0.09998900    -0.15931739    -0.06562932     0.05170350    -0.20764759     0.39864779    -0.05687960
  27H1s      -0.61653596    -0.26103754    -1.14919334    -0.15174827     2.51245416    -1.68527869     2.19773282    -0.66461227
  28H1s       0.64476947     1.47727226     1.54851974    -0.20874082    -3.85636932    -0.06106278    -1.49228995     0.81482814
  29H1s       0.48255826     0.14361789     0.79953402    -0.04691677    -1.30603669     0.79401928    -1.02004458     0.33736803
  30H1py      0.52088015    -0.16010274     0.56157796    -0.09329211    -0.02735363     0.52635100    -0.35152327     0.07948921
  31H1pz     -0.69676787     0.15519449     0.38169103    -0.06396999    -0.08741082     0.43535368    -0.23203268    -0.02407958
  32H1py     -0.61328384    -0.24001600    -0.53461128     0.44907240     1.66978594     0.10418679     0.20372915    -0.36149885
  33H1pz      0.74549319    -0.47851543    -0.76683934    -0.52076608     0.45835163     0.27247166     0.43662927    -0.04588891
  34H1d1-    -0.16039020    -0.34525916     0.22442066     0.59926957    -0.35663042    -0.38584283    -0.13111724     0.07868040
  35H1d0     -0.02938448     0.09822447     0.06583029     0.14581340     0.08431482    -0.02439697    -0.04999544     0.02609653
  36H1d2+    -0.02712042    -0.00542152     0.11139319     0.09844846     0.41060776    -0.01397607     0.01310521    -0.03302994

               MO   33        MO   34        MO   35        MO   36

  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000

   1O1s       0.07405096    -0.06615857    -2.67859046    -2.54963034
   2O1s       0.09664202     0.12570814    -6.64710133    -6.63262917
   3O1s      -0.15714430     0.39890519     3.10817267     2.90632828
   4O1s      -0.22000082    -1.60578793     4.66373130    -0.59526739
   5O1pz     -0.03623859     1.03388937     0.39945521     1.29963857
   6O1pz     -0.02290060    -1.20576416    -1.12129179    -3.73712435
   7O1pz     -0.02803588    -1.27108000     0.57299867    -1.65882705
   8O1d0      0.01324271     0.32729292    -0.05260666    -0.06150476
   9O1d2+     0.56633982    -0.00802309     0.05649386    -0.00409761
  10O1d0     -0.01341987    -0.38321945     0.01177485    -0.32979047
  11O1d2+    -0.36338191     0.01675019    -0.09870654    -0.00953444
  12O1f0      0.00770558     0.20253557     0.02474322     0.13860089
  13O1f2+    -0.16290334     0.03059311     0.00765442    -0.04098108
  14C1s       0.10831006     0.32708522    -3.75306685     5.25293268
  15C1s       0.34015163     1.43491907   -11.24333111    16.19604114
  16C1s      -0.10803799     0.35877816     3.20822621    -4.78844047
  17C1s      -0.44730842    -0.09191424     0.15830716    -2.99781101
  18C1pz      0.02528026    -1.21827341     0.73550635     0.08004020
  19C1pz      0.03141366    -0.24694812    -4.50072071    -1.62468533
  20C1pz     -0.14384580     0.39556530    -0.63891794     0.22616345
  21C1d0      0.00414187     0.07416733     0.10014500     0.26197656
  22C1d2+    -0.03814457     0.02478796     0.23939740    -0.28663859
  23C1d0      0.08557056    -0.02090114     0.33523763     0.53101704
  24C1d2+     0.19651654     0.01767239     1.07251973    -0.45354212
  25C1f0      0.01431215    -0.11269342     0.02068866     0.48639245
  26C1f2+    -0.13970056     0.06759618     0.31022947    -0.28331177
  27H1s      -0.40335908     0.50792881    -0.02267948    -0.37586901
  28H1s       0.42937319    -0.20481429     5.03468043    -2.94298233
  29H1s       0.22357536    -0.25025282     0.62862463    -0.37288857
  30H1py      0.08791089    -0.08357968    -0.37603150     0.35437205
  31H1pz      0.02081649    -0.03015812    -0.17859083     0.29351640
  32H1py      0.05322673    -0.01123857    -1.93935289     1.63596598
  33H1pz     -0.18369368     0.00312318    -1.62988473     0.63103500
  34H1d1-     0.00831752     0.00713705     0.45466724    -0.42453508
  35H1d0      0.02977530    -0.00610385    -0.00230136     0.01966283
  36H1d2+     0.03173320     0.01257667    -0.17500898     0.17211332

          natural orbitals of the final iteration, block  2

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8

  occ(*)=     2.00000000     0.50057239     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

  37O1px      0.76240658    -0.61811801    -0.03217094    -0.58438150    -0.12407509    -0.22558534    -0.05150033     0.04535981
  38O1px      0.00651888     0.01269902     0.01197561    -0.40998276    -0.04658181    -0.24980482     0.03556283     0.03258250
  39O1px     -0.00470065    -0.14282454    -0.11500707     1.85209996    -0.80322882     0.03482087     0.83432373     0.34395656
  40O1d1+     0.00974729     0.00534087    -0.00648311    -0.02194534     0.02544345     0.01622349    -0.00252875     0.00014399
  41O1d1+     0.02883669     0.00995495    -0.02911723     0.08657723    -0.02966961    -0.05638877     1.15989997     0.48571652
  42O1f1+     0.00093592     0.00013771    -0.00002799     0.00248633    -0.00190452    -0.00175790     0.00059306     0.00611414
  43O1f3+     0.00013358     0.00237829    -0.00015894    -0.00035588    -0.00078166     0.00186419    -0.00077876     0.00121260
  44C1px      0.45766085     0.76288974    -0.59993396     0.18646275     0.14409062    -0.21663261    -1.35604031     3.30288597
  45C1px     -0.01288228     0.02598795    -0.52510808    -0.08528559     0.19568373    -0.26818426     0.69544741    -3.31428733
  46C1px     -0.03330417     0.22823506     1.55700999    -0.66884412     0.73066497    -0.37778935     0.04231747    -0.79136342
  47C1d1+    -0.02091008     0.01247219     0.00762546    -0.00026043     0.00652857     0.03164028    -0.13572233    -0.11096908
  48C1d1+    -0.03302484     0.01527888     0.03403786     0.40005572    -0.86218935    -1.26060308     0.94275515     0.33419163
  49C1f1+     0.00154679    -0.00032232    -0.00007943    -0.00991570     0.00522288     0.00172235    -0.00027964     0.01416843
  50C1f3+     0.00063362     0.00045968     0.00526438     0.01133895     0.01024690    -0.02998712    -0.01291915     0.03324837
  51H1px      0.00300484     0.00713024     0.01214311     0.01597979     0.04259910    -0.06751588    -0.00837043    -0.02651665
  52H1px      0.00802870     0.02490134    -0.09706747    -0.20622266    -0.52564760     1.02338250    -0.12588688     0.14588550
  53H1d2-    -0.00102177    -0.00293305     0.00043766     0.00992488    -0.00190928    -0.02387271     0.00186991    -0.01308395
  54H1d1+    -0.00076042    -0.00145944     0.00040743    -0.00243675    -0.00668248    -0.03239283     0.07686902     0.03083939

               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16

  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

  37O1px      0.34620394    -0.12156118    -1.29991846     0.99509257    -0.64878051     2.61876654     0.08369157    -0.01539972
  38O1px     -0.25203784    -0.20476249     0.79774218    -0.74707504     0.53505314    -2.92486782    -0.11010022     0.01909149
  39O1px     -0.07348340    -0.54992163    -0.25875552    -0.18243627     0.03889933    -0.65838641    -0.02126851    -0.00099668
  40O1d1+    -0.00518418    -0.00631305     0.04814065    -0.04567361     0.04258996     0.06287829    -0.03313398     0.02498485
  41O1d1+     0.23990211    -0.35689642    -0.87603322     0.15784742    -0.11089026    -0.68475216    -0.05473171     0.00809736
  42O1f1+    -0.00724311    -0.02296628     0.00438206     0.00791751    -0.00174581     0.03140628    -0.01315846     0.00638348
  43O1f3+     0.01789014    -0.00995615     0.01350414     0.00279592    -0.03102707    -0.00836500     0.15763605     0.59224803
  44C1px     -0.16235636     0.06638747    -0.64802567     0.11742522     0.01440306    -0.48091394    -0.45218247     0.15585478
  45C1px      0.10254122    -0.02117094     1.11940518     0.02275180     0.34694291     0.83839249     1.37825520    -0.44825596
  46C1px      0.30043657     0.15081365     0.49834629     0.09608533     0.26216190     0.03558502     0.12496657    -0.05108494
  47C1d1+     0.35695613     0.84160323    -0.52745703    -0.07790179     0.06980997    -0.17060342     0.40072367    -0.08846524
  48C1d1+     0.13213200    -1.31038900    -0.32536849     0.09879470     0.26289667    -0.53393292     0.35362281    -0.14298171
  49C1f1+     0.01052427     0.09269336     0.12015681    -0.08084064     0.05752299     0.12581625     0.02473417    -0.00496122
  50C1f3+     0.46639710    -0.22731498     0.25243427     0.11176517    -0.16625572    -0.05475720     0.55899422    -0.25300396
  51H1px      0.20913428     0.05500865     0.07159275     0.55677217     0.65813790    -0.05284532    -0.31020824     0.12587597
  52H1px     -0.42798751     0.29438603    -0.24364865    -0.45493864    -0.61748387     0.09145671    -0.51726971     0.17349226
  53H1d2-    -0.02965281    -0.23504018    -0.07375295    -0.10682849     0.49948871     0.09261912     0.76236381    -0.21109299
  54H1d1+    -0.18061635     0.14782075     0.21264978     0.51815252    -0.21662022    -0.11338440     0.52901181    -0.18533240

               MO   17        MO   18

  occ(*)=     0.00000000     0.00000000

  37O1px     -0.68282505     0.53788284
  38O1px      0.99585899    -0.82658902
  39O1px      0.25004670    -0.68184715
  40O1d1+     0.63473237     1.04080221
  41O1d1+     0.25516146    -1.32273318
  42O1f1+     0.14569786    -0.11639388
  43O1f3+    -0.01215198    -0.00073276
  44C1px      0.03928562    -0.10338481
  45C1px     -0.43771046     0.81614456
  46C1px      0.04590862     0.13676443
  47C1d1+     0.55473863    -0.40686200
  48C1d1+     0.14322725    -0.61017149
  49C1f1+    -0.10460199     0.07905986
  50C1f3+     0.04510808     0.00306935
  51H1px     -0.02449984     0.00086546
  52H1px      0.00704753     0.00820048
  53H1d2-    -0.02681451     0.00387185
  54H1d1+     0.11409582     0.01602995

          natural orbitals of the final iteration, block  3

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8

  occ(*)=     2.00000000     1.49942761     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

  55O1py      0.30704679     0.87560432     0.11639804    -0.34021106    -0.16898236     0.44597196    -0.13160179    -0.46601750
  56O1py      0.00290700     0.00517524    -0.03075526     0.05269476    -0.02087775     0.31445673    -0.20315591    -0.36318718
  57O1py     -0.03236107     0.05397163     0.30599220    -0.19705874    -0.23981980    -2.16586502     0.98550757     0.26659894
  58O1d1-     0.00705060     0.00466343    -0.00698366    -0.00653255     0.01402898     0.03098578    -0.00887564     0.02292408
  59O1d1-     0.01617525     0.01905445     0.00994542     0.04385115     0.02919487    -0.12209307    -0.00893607     0.04294264
  60O1f3-    -0.00001684    -0.00029076    -0.00000090     0.00023721    -0.00003519    -0.00008416    -0.00064127    -0.00003849
  61O1f1-     0.00064202     0.00044981    -0.00017172    -0.00031536     0.00034641    -0.00275752    -0.00134292     0.00047939
  62C1py      0.65753688    -0.16213722    -0.43660637    -0.27507480     0.82434956    -0.34361010    -0.15622599    -0.03362725
  63C1py     -0.04370033     0.02868319    -0.05896953     0.79680759    -0.45411048    -0.46138721    -0.13815860    -1.04810230
  64C1py     -0.13282747     0.06186506    -1.46970607     3.78685906    -1.55391978     2.08250864    -2.32445163    -0.82653869
  65C1d1-     0.00742962    -0.03259340    -0.00525703     0.00228052    -0.04002010     0.00460672    -0.00026917    -0.01894813
  66C1d1-     0.00368419    -0.02712256     0.00447271     0.91742765    -0.62381192    -1.08461251    -0.12462858    -2.16710321
  67C1f3-    -0.00034283    -0.00000415     0.00044337    -0.01381212     0.00706401     0.00608635     0.00641336     0.01158388
  68C1f1-     0.00089379     0.00142979     0.00017026     0.00220538     0.00137557     0.00677920     0.01079567    -0.01459595
  69H1s       0.52645836    -0.33824581    -0.01473885     0.00560033    -0.38833547    -0.03753766    -0.00879853    -0.53831814
  70H1s      -0.05739942    -0.02099309    -0.11710701    -1.88066993     0.20949389     0.76075636     0.82526089     2.49386299
  71H1s      -0.12367609     0.00337407     2.36650929    -1.81304978     1.76147116    -1.20721340     0.87400389     0.07747690
  72H1py     -0.00819662     0.00500272     0.01827186     0.02077022     0.00679960    -0.01687033    -0.04605157    -0.03200598
  73H1pz     -0.00830114     0.00220822     0.01946201     0.00866886     0.01222591    -0.01415053     0.03978353    -0.06124905
  74H1py     -0.01249641     0.01755353    -0.07173013     0.52859255    -0.44864536    -0.12551511     0.60511177    -0.58031323
  75H1pz     -0.01090386     0.01090296    -0.08966733     0.23434964    -0.43502962    -0.02955820    -0.70558891     0.36757186
  76H1d1-     0.00404709    -0.00125974     0.00377028    -0.03574661     0.02820981     0.01053574     0.00731335     0.01607165
  77H1d0      0.00022362     0.00034843     0.00053039     0.00387868     0.00008948    -0.00490933     0.00747983    -0.00536993
  78H1d2+    -0.00080346     0.00083251    -0.00120680     0.01157798    -0.00736983    -0.00316548     0.00013337    -0.02027179

               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16

  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

  55O1py     -0.06154927     0.13718169     0.06429114    -0.18147935    -0.21570522    -0.83271670    -0.13722864     1.12589071
  56O1py     -0.17775449     0.01805333     0.09655811     0.09867635    -0.09366303     0.44389751     0.16121645    -0.88226753
  57O1py      0.16865066     0.73720302     1.00443317    -0.59310664    -0.26449022    -0.10175210    -0.09618897     0.08536389
  58O1d1-    -0.00362569     0.00174561     0.00136986     0.00541713    -0.00409266     0.04934526    -0.04320331    -0.02915337
  59O1d1-    -0.07777782     1.15774202     0.69552910    -0.55377368    -0.21391726    -0.69475423     0.22056303     0.29462994
  60O1f3-    -0.00104244    -0.00028817     0.00237590     0.00403819     0.00355435     0.00163169     0.00185154     0.00944013
  61O1f1-     0.00036422     0.00074540     0.00555763    -0.00287864    -0.01785164     0.00561935    -0.00430449     0.00252500
  62C1py     -0.67926559    -0.69103375    -1.06935202    -2.92526621    -0.32598920     0.37996834     2.29532024     0.36325322
  63C1py     -4.28016254     1.50886911     0.72060607     6.58273978     0.49587527    -0.61734218    -6.00307288    -1.59843795
  64C1py     -2.30733970     0.32456608    -0.93561586     2.37218444    -0.16467130    -0.33718306    -1.68783554    -0.87900330
  65C1d1-    -0.06085836     0.00604443    -0.24598408     0.16795604     0.74302488    -0.56928822     0.04515169    -0.10990640
  66C1d1-    -2.51491226     1.79005709     1.33163851     0.93856383    -1.16049680    -0.28639672    -2.31067835    -0.18299090
  67C1f3-     0.03043402    -0.05014454     0.10537632     0.00277094     0.08173130     0.05456722     0.08275979     0.12963170
  68C1f1-    -0.00987594     0.00785612    -0.03060867    -0.00833884     0.05450734     0.11793415    -0.06692998    -0.06184080
  69H1s       0.04960949     1.88176341    -2.26816390     2.68292237     0.58111528     0.84833670     1.76321168     0.88439887
  70H1s       4.59653752    -3.18874199     1.45307864    -5.55874173    -0.04792962    -0.11859275     2.68061610     0.45980797
  71H1s       1.57619910    -0.39748311     1.29934233    -1.36113174    -0.23333563     0.00770667    -0.13606149     0.17306911
  72H1py      0.03092237     0.06674509     0.01221837     0.11784219    -0.35413673     0.09257894     0.17980583     0.15526310
  73H1pz      0.02674748     0.09724108     0.01182011    -0.03331152     0.05934128     0.39862493    -0.02222947     0.08723320
  74H1py     -2.52479169     0.67401434     0.14991141     1.48188762     0.37883200    -0.24644646    -1.86549194    -0.63532188
  75H1pz     -1.54037531     0.10568167    -0.34627755     1.29600670     0.09665480    -0.62282900    -0.88841817    -0.41859218
  76H1d1-     0.12096449    -0.06536071     0.04830149    -0.07196980    -0.02259711     0.06447056     0.48071771    -0.10048409
  77H1d0     -0.00526138    -0.00220107    -0.03524378     0.00316508    -0.03152352     0.00875332    -0.07887792     0.08070266
  78H1d2+    -0.04609455     0.00797187     0.05436750     0.09884434    -0.04114263     0.00122652    -0.00103387    -0.26215383

               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24

  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

  55O1py      1.53278066    -2.34689390     0.50735277     0.12771316    -0.05348362     0.10977228    -0.08744243    -1.00436466
  56O1py     -1.53615350     2.60336986    -0.65934464    -0.44478367     0.19042972    -0.35461941     0.29362235     1.70811069
  57O1py      0.06141155     0.72151846    -0.33346152    -0.00815328     0.08322968    -0.27833210    -0.48377391     0.81359031
  58O1d1-     0.00096776     0.03471529     0.00862110     0.25892511    -0.12255281     0.08474900     1.13896980    -0.39353610
  59O1d1-    -0.02484371     0.54244096    -0.18124867    -0.37372719     0.16007627    -0.41040605    -0.57317050     1.51272056
  60O1f3-    -0.00473244     0.00837760     0.04907309     0.06812456     0.18579954     0.00669453     0.00595092     0.00621658
  61O1f1-    -0.00291020    -0.03573147     0.00718574     0.04887887    -0.02397804    -0.00180369     0.03363153     0.18983241
  62C1py      0.09119204     0.43295662     2.09637735    -0.08148805    -0.56786648    -0.11981984     0.15303398     0.39055523
  63C1py     -0.72885271    -0.80257447    -1.09073562    -3.15604353     1.34910459     5.37726753     0.30549859    -1.47744384
  64C1py     -1.13944491     0.14742246    -0.38977592    -0.61318033     0.25144346     0.99641884     0.38567016    -0.33252095
  65C1d1-     0.10623322     0.08609066     0.15120710    -0.19397034    -0.03961813     1.39152195     0.09973722     0.54642631
  66C1d1-    -0.12650477     1.24538048    -0.87738996    -1.06671575     0.56994877     1.37653683    -0.67739742     0.57951510
  67C1f3-     0.01522826     0.06313256     0.09832689     0.11130612    -0.09668149    -0.08036389    -0.05363627    -0.03534206
  68C1f1-     0.04524333    -0.01693342     0.01089052     0.15796152    -0.05510356     0.14093475    -0.14201125    -0.25637847
  69H1s      -0.37912442    -0.03384896     2.66163528    -0.47805393    -0.80017437    -0.69042086     0.49610411     0.11888223
  70H1s       0.89902457    -0.42097145    -1.76737013     2.97585061    -0.29910704    -3.89946804    -0.29587959     0.20329704
  71H1s       0.63244229    -0.10367534    -1.40522542     0.40997853     0.36793956     0.04779389    -0.40420579     0.03571811
  72H1py     -0.29807339    -0.14220880    -0.42085352     0.07351316     0.07256628     0.92625534    -0.13836205    -0.22875826
  73H1pz      0.49277180     0.08851266    -0.38975907    -0.53079207     0.30711630     0.24407998     0.24097105     0.34675527
  74H1py      0.15888648     0.36588924     0.73679775    -1.13000460     0.13297912     1.72786656    -0.11437312    -0.16965586
  75H1pz     -0.71861991    -0.41874204     0.61116534    -0.91921283     0.11954847     1.07869698     0.36504332    -0.06962436
  76H1d1-     0.32472543     0.06092758    -0.50696340     0.67527491    -0.07659432    -0.55678528    -0.23615989    -0.23768541
  77H1d0      0.10941594     0.15842219     0.02950859     0.13610813    -0.07067358     0.08533814    -0.10877801    -0.07584075
  78H1d2+     0.16305611     0.03519707     0.23367672    -0.03938010    -0.04281930     0.34490312    -0.06476872    -0.09888595

          natural orbitals of the final iteration, block  4

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8

  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

  79O1d2-    -0.37173501     0.25583661    -0.22656857    -0.23072327     0.49877345    -0.80972979    -0.38461612    -0.10260161
  80O1d2-    -0.58011904     0.36629159    -0.15892207     0.04984600    -0.48101819     0.83821808     0.42956576     0.15639562
  81O1f2-    -0.14515653    -0.01407676     0.27056438    -0.04752219     0.58779835     0.00748779     0.75474095     0.12783989
  82C1d2-    -0.21941852    -0.65351884    -0.25242105    -0.88382981    -0.15731873     0.17192496    -0.04881580    -0.09347774
  83C1d2-    -0.11029662    -0.37155838     0.22049806     1.28498686    -0.59734902    -1.29674786     0.36892207    -0.21746058
  84C1f2-     0.17384208     0.21461759    -0.74110833     0.14140696    -0.27058280    -0.34822566     0.77656189     0.06887300
  85H1px     -0.01934795    -0.04005811    -0.02515524    -0.00792440     0.07479840     0.09224890     0.05344418     0.34801781
  86H1px      0.00004164     0.02828402    -0.24537208    -0.17297596     0.71162217     0.89632435    -0.55608715    -0.18530708
  87H1d2-     0.02614714     0.03406823    -0.00727470     0.05473628    -0.09866518    -0.09064063    -0.06476358     0.21934729
  88H1d1+     0.00571187     0.01602307     0.02987122     0.02195944    -0.06292940    -0.02735644     0.12144095    -0.62821823

               MO    9        MO   10

  occ(*)=     0.00000000     0.00000000

  79O1d2-     0.11970191    -0.05525921
  80O1d2-    -0.26673040     0.04977287
  81O1f2-    -0.28212952    -0.11382300
  82C1d2-    -0.29062988    -0.40462083
  83C1d2-     0.19521463    -0.92680377
  84C1f2-    -0.63691685    -0.70730155
  85H1px      0.83234206    -0.27023543
  86H1px     -0.47995374     0.92634784
  87H1d2-    -0.43045440    -0.84340392
  88H1d1+     0.10401894    -0.50879351
 d1(*), fmc(*), and qmc(*) written to the 1-particle density matrix file.
         81 d2(*) elements written to the 2-particle density matrix file.
 !timer: writing the mc density files requser+sys=     0.000 walltime=     0.000


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!


                        A1  partial gross atomic populations
   ao class       1A1        2A1        3A1        4A1        5A1        6A1 
    O1_ s       1.999224  -0.000028   1.370470   0.243793   0.169710   0.000000
    O1_ p       0.000002   0.000022   0.097065   0.256169   1.057970   0.000000
    O1_ d       0.000000  -0.000102   0.005216   0.002340   0.007218   0.000000
    O1_ f       0.000000  -0.000004   0.000393   0.000040   0.000548   0.000000
    C1_ s      -0.000037   2.000343   0.364520   0.832509   0.055370   0.000000
    C1_ p       0.000420   0.000001   0.150237   0.153310   0.445996   0.000000
    C1_ d       0.000351   0.000001   0.022056   0.006829   0.003741   0.000000
    C1_ f       0.000023   0.000001   0.003834   0.000714   0.000100   0.000000
    H1_ s       0.000010  -0.000157  -0.006306   0.483772   0.247037   0.000000
    H1_ p       0.000008   0.000016  -0.007371   0.019630   0.011339   0.000000
    H1_ d       0.000000  -0.000094  -0.000114   0.000895   0.000971   0.000000

   ao class       7A1        8A1        9A1       10A1       11A1       12A1 

   ao class      13A1       14A1       15A1       16A1       17A1       18A1 

   ao class      19A1       20A1       21A1       22A1       23A1       24A1 

   ao class      25A1       26A1       27A1       28A1       29A1       30A1 

   ao class      31A1       32A1       33A1       34A1       35A1       36A1 

                        B1  partial gross atomic populations
   ao class       1B1        2B1        3B1        4B1        5B1        6B1 
    O1_ p       1.388373   0.127808   0.000000   0.000000   0.000000   0.000000
    O1_ d       0.011858   0.001571   0.000000   0.000000   0.000000   0.000000
    O1_ f       0.000686   0.000042   0.000000   0.000000   0.000000   0.000000
    C1_ p       0.552822   0.354790   0.000000   0.000000   0.000000   0.000000
    C1_ d       0.033498   0.003847   0.000000   0.000000   0.000000   0.000000
    C1_ f       0.004661   0.000192   0.000000   0.000000   0.000000   0.000000
    H1_ p       0.007607   0.011745   0.000000   0.000000   0.000000   0.000000
    H1_ d       0.000495   0.000577   0.000000   0.000000   0.000000   0.000000

   ao class       7B1        8B1        9B1       10B1       11B1       12B1 

   ao class      13B1       14B1       15B1       16B1       17B1       18B1 

                        B2  partial gross atomic populations
   ao class       1B2        2B2        3B2        4B2        5B2        6B2 
    O1_ p       0.258162   1.193195   0.000000   0.000000   0.000000   0.000000
    O1_ d       0.008019  -0.000740   0.000000   0.000000   0.000000   0.000000
    O1_ f       0.000562  -0.000023   0.000000   0.000000   0.000000   0.000000
    C1_ p       0.937383   0.017147   0.000000   0.000000   0.000000   0.000000
    C1_ d       0.001656   0.041650   0.000000   0.000000   0.000000   0.000000
    C1_ f       0.001582   0.003373   0.000000   0.000000   0.000000   0.000000
    H1_ s       0.776552   0.249101   0.000000   0.000000   0.000000   0.000000
    H1_ p       0.014413  -0.004395   0.000000   0.000000   0.000000   0.000000
    H1_ d       0.001671   0.000119   0.000000   0.000000   0.000000   0.000000

   ao class       7B2        8B2        9B2       10B2       11B2       12B2 

   ao class      13B2       14B2       15B2       16B2       17B2       18B2 

   ao class      19B2       20B2       21B2       22B2       23B2       24B2 

                        A2  partial gross atomic populations
   ao class       1A2        2A2        3A2        4A2        5A2        6A2 

   ao class       7A2        8A2        9A2       10A2 


                        gross atomic populations
     ao           O1_        C1_        H1_
      s         3.783168   3.252705   1.750009
      p         4.378768   2.612106   0.052991
      d         0.035380   0.113629   0.004519
      f         0.002244   0.014480   0.000000
    total       8.199560   5.992920   1.807519


 Total number of electrons:   16.00000000

 !timer: mcscf                           user+sys=    14.690 walltime=    15.000
