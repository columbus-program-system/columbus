
 program cipc      

 print the csf info for mrsdci wave functions

 written by: ron shepard

 version date: 06-jun-96

 This Version of Program cipc is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 workspace allocation parameters: lencor=  10000000 mem1=1108566024 ifirst=-264081912

 drt header information:
  cidrt_title                                                                    
 nmot  =    88 niot  =     9 nfct  =     0 nfvt  =     0
 nrow  =    62 nsym  =     4 ssym  =     1 lenbuf=  1600
 spnorb=     F spnodd=     F lxyzir(1:3)= 0 0 0
 nwalk,xbar:        278       23      128       56       71
 nvalwt,nvalw:      276       21      128       56       71
 ncsft:          108301
 map(*)=    80 81 82 83 84  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15
            16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 85 87 32 33
            34 35 36 37 38 39 40 41 42 43 44 45 46 47 86 88 48 49 50 51
            52 53 54 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71
            72 73 74 75 76 77 78 79
 mu(*)=      0  0  0  0  0  0  0  0  0
 syml(*) =   1  1  1  1  1  2  3  2  3
 rmo(*)=     1  2  3  4  5  1  1  2  2

 indx01:   276 indices saved in indxv(*)
===================================ROOT # 1===================================

 rdhciv: CI vector file information:
 Hermit Integral Program : SIFS version  hochtor2        Thu Oct  7 14:50:04 2004
  cidrt_title                                                                    
  title                                                                          
 mofmt: formatted orbitals label=morbl   hochtor2        Thu Oct  7 14:50:31 2004
 SIFS file created by program tran.      hochtor2        Thu Oct  7 14:50:31 2004
 energy computed by program ciudg.       hochtor2        Thu Oct  7 14:52:53 2004

 lenrec =   32768 lenci =    108301 ninfo =  6 nenrgy =  5 ntitle =  6

 Max. overlap with ref vector #        1
 Valid ci vector #        1
 Method:        0       95% overlap
 energy( 1)=  3.116530982443E+01, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -1.143293137546E+02, ietype=-1026,   total energy of type: MRSDCI  
 energy( 3)=  9.756972998255E-04, ietype=-2055, cnvginf energy of type: CI-Resid
 energy( 4)= -3.122452603349E+01, ietype=-2056, cnvginf energy of type: CI-D.E. 
 energy( 5)=  2.189170246575E-07, ietype=-2057, cnvginf energy of type: CI-ApxDE
==================================================================================

 space is available for   4982377 coefficients.

 updated histogram parameters:
 csfmn = 0.0000E+00 csfmx = 1.0000E+00 fhist = 5.0000E-01 nhist =  20

 this program will print the csfs generated from
 the drt according to the following print options :

 1) run in batch mode: all valid roots are automatically
    analysed and csf info is printed by default contribution
    threshold 0.01 
 2) run in interactive mode

 input menu number [  0]:
================================================================================
===================================VECTOR # 1===================================
================================================================================


 rdcivnew:     139 coefficients were selected.
 workspace: ncsfmx=  108301
 ncsfmx= 108301

 histogram parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =  108301 ncsft =  108301 ncsf =     139
 nhist =  20 fhist = 0.50000

    cmin                cmax        num  '*'=     3 csfs.
 ----------          ----------   ----- ---------|---------|---------|---------|
 5.0000E-01 <= |c| < 1.0000E+00       1 *
 2.5000E-01 <= |c| < 5.0000E-01       0
 1.2500E-01 <= |c| < 2.5000E-01       0
 6.2500E-02 <= |c| < 1.2500E-01       1 *
 3.1250E-02 <= |c| < 6.2500E-02       3 *
 1.5625E-02 <= |c| < 3.1250E-02      31 ***********
 7.8125E-03 <= |c| < 1.5625E-02     103 ***********************************
 3.9062E-03 <= |c| < 7.8125E-03       0
 1.9531E-03 <= |c| < 3.9062E-03       0
 9.7656E-04 <= |c| < 1.9531E-03       0
 4.8828E-04 <= |c| < 9.7656E-04       0
 2.4414E-04 <= |c| < 4.8828E-04       0
 1.2207E-04 <= |c| < 2.4414E-04       0
 6.1035E-05 <= |c| < 1.2207E-04       0
 3.0518E-05 <= |c| < 6.1035E-05       0
 1.5259E-05 <= |c| < 3.0518E-05       0
 7.6294E-06 <= |c| < 1.5259E-05       0
 3.8147E-06 <= |c| < 7.6294E-06       0
 0.0000E+00 <= |c| < 3.8147E-06       0
                                  ----- ---------|---------|---------|---------|
                  total read =      139 total stored =     139

 from the selected csfs,
 min(|csfvec(:)|) = 1.0136E-02    max(|csfvec(:)|) = 9.5202E-01
 norm=  1.
 csfs will be printed based on coefficient magnitudes.

 current csfvec(*) selection parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =  108301 ncsft =  108301 ncsf =     139

 i:slabel(i) =  1: a1   2: b1   3: b2   4: a2 

 internal level =    1    2    3    4    5    6    7    8    9
 syml(*)        =    1    1    1    1    1    2    3    2    3
 label          =  a1   a1   a1   a1   a1   b1   b2   b1   b2 
 rmo(*)         =    1    2    3    4    5    1    1    2    2

 printing selected csfs in sorted order from cmin = 0.01000 to cmax = 1.00000

   indcsf     c     c**2   v  lab:rmo  lab:rmo   step(*)
  ------- -------- ------- - ---- --- ---- --- ------------
        2  0.95202 0.90635 z*                    333333303
        6 -0.08845 0.00782 z                     333330333
        5 -0.05037 0.00254 z                     333331323
      492 -0.03581 0.00128 y           a1 : 11  1333321323
      491 -0.03176 0.00101 y           a1 : 10  1333321323
      349 -0.03119 0.00097 y           b1 :  4  1333330323
      267 -0.02981 0.00089 y           b2 :  4  1333331322
      495 -0.02868 0.00082 y           a1 : 14  1333321323
        9 -0.02805 0.00079 z                     333033333
      271 -0.02761 0.00076 y           b2 :  8  1333331322
      268 -0.02572 0.00066 y           b2 :  5  1333331322
    51623 -0.02557 0.00065 w           b2 :  5  3333333003
     1136 -0.02188 0.00048 y           a1 : 11  1332331323
    65751  0.02186 0.00048 w  a1 :  9  b2 :  5 12333133203
      956  0.02022 0.00041 y           b1 :  5  1333123323
      882 -0.01963 0.00039 y           a2 :  2  1333133223
      409 -0.01843 0.00034 y           a1 : 10  1333323303
    51621  0.01841 0.00034 w  b2 :  3  b2 :  5 12333333003
    48145 -0.01825 0.00033 w           b2 :  6  3333333300
    62360 -0.01785 0.00032 w           a1 : 10  3333303303
      212  0.01753 0.00031 y           b1 :  3  1333332303
     1132  0.01752 0.00031 y           a1 :  7  1332331323
     1304 -0.01719 0.00030 y           a1 : 11  1331332323
      907 -0.01709 0.00029 y           a1 : 12  1333132323
    62390 -0.01707 0.00029 w           a1 : 14  3333303303
    56290 -0.01701 0.00029 w           b1 :  4  3333330303
     5290 -0.01698 0.00029 x  a1 : 16  a2 :  1 11333332302
    62366 -0.01696 0.00029 w           a1 : 11  3333303303
     1138  0.01696 0.00029 y           a1 : 13  1332331323
        1 -0.01684 0.00028 z*                    333333330
      269  0.01651 0.00027 y           b2 :  6  1333331322
      903  0.01639 0.00027 y           a1 :  8  1333132323
      348  0.01636 0.00027 y           b1 :  3  1333330323
      304 -0.01602 0.00026 y           b2 :  3  1333331223
    48143  0.01585 0.00025 w  b2 :  4  b2 :  6 12333333300
    50991 -0.01569 0.00025 w           a1 :  8  3333333003
     1238  0.01540 0.00024 y           a2 :  1  1331333322
      771 -0.01525 0.00023 y           a1 :  6  1333231323
     4977 -0.01514 0.00023 x  b1 :  4  b2 :  6 11333332302
    49111 -0.01495 0.00022 w           a1 :  8  3333333102
      692 -0.01490 0.00022 y           a1 :  9  1333233303
    52915  0.01489 0.00022 w  b1 :  4  b2 :  6 12333331302
    70786 -0.01488 0.00022 w  a1 :  6  a1 :  9 12333033303
      307 -0.01466 0.00022 y           b2 :  6  1333331223
    70789 -0.01462 0.00021 w           a1 :  9  3333033303
    10907  0.01456 0.00021 x  a1 : 10  b1 :  4 11333322303
      881 -0.01450 0.00021 y           a2 :  1  1333133223
    49114  0.01447 0.00021 w  a1 :  8  a1 :  9 12333333102
    56677 -0.01445 0.00021 w           a2 :  1  3333330303
       98 -0.01430 0.00020 y           b2 :  3  1333333203
    62386 -0.01423 0.00020 w  a1 : 10  a1 : 14 12333303303
      774 -0.01418 0.00020 y           a1 :  9  1333231323
      905  0.01413 0.00020 y           a1 : 10  1333132323
    60725 -0.01398 0.00020 w  a1 : 10  b1 :  4 12333312303
    68940  0.01394 0.00019 w  a1 : 10  a1 : 14 12333123303
      883 -0.01393 0.00019 y           a2 :  3  1333133223
      689 -0.01393 0.00019 y           a1 :  6  1333233303
      192 -0.01390 0.00019 y           b2 :  5  1333332312
      266  0.01381 0.00019 y           b2 :  3  1333331322
       39 -0.01373 0.00019 y           b2 :  4  1333333302
    49743 -0.01373 0.00019 w           b2 :  5  3333333102
    65748  0.01372 0.00019 w  a1 :  6  b2 :  5 12333133203
    59197  0.01371 0.00019 w  a1 :  9  b2 :  5 12333313203
    50994  0.01369 0.00019 w  a1 :  8  a1 :  9 12333333003
     7727 -0.01368 0.00019 x  a1 : 10  b2 :  6 11333323302
    48141 -0.01360 0.00018 w           b2 :  5  3333333300
      350 -0.01352 0.00018 y           b1 :  5  1333330323
    53223  0.01340 0.00018 w  a1 : 11  a2 :  1 12333331302
    68906 -0.01339 0.00018 w  a1 :  6  a1 :  9 12333123303
    48389 -0.01316 0.00017 w           a2 :  1  3333333300
      100  0.01316 0.00017 y           b2 :  5  1333333203
     7697  0.01304 0.00017 x  a1 : 11  b2 :  5 11333323302
        7 -0.01285 0.00017 z                     333303333
     6610  0.01282 0.00016 x  a1 : 12  a2 :  1 11333332203
    57483 -0.01274 0.00016 w  a1 : 10  b2 :  4 12333313302
    57545  0.01266 0.00016 w  a1 : 10  b2 :  6 12333313302
    68909 -0.01260 0.00016 w           a1 :  9  3333123303
     7665  0.01260 0.00016 x  a1 : 10  b2 :  4 11333323302
    62387 -0.01257 0.00016 w  a1 : 11  a1 : 14 12333303303
    75710  0.01247 0.00016 w  a1 :  7  b1 :  4 12331332303
     7793  0.01240 0.00015 x  a1 : 14  b2 :  8 11333323302
     5291 -0.01233 0.00015 x  a1 : 17  a2 :  1 11333332302
    67247  0.01232 0.00015 w  a1 :  9  b1 :  3 12333132303
    51618 -0.01225 0.00015 w           b2 :  3  3333333003
    51641  0.01222 0.00015 w  b2 :  5  b2 :  9 12333333003
     6252 -0.01218 0.00015 x  b1 :  3  b2 :  3 11333332203
     7790  0.01217 0.00015 x  a1 : 11  b2 :  8 11333323302
    48152 -0.01216 0.00015 w  b2 :  4  b2 :  8 12333333300
      213 -0.01209 0.00015 y           b1 :  4  1333332303
    65689 -0.01206 0.00015 w  a1 :  9  b2 :  3 12333133203
     6284  0.01197 0.00014 x  b1 :  3  b2 :  5 11333332203
     5286 -0.01196 0.00014 x  a1 : 12  a2 :  1 11333332302
    75716  0.01195 0.00014 w  a1 : 13  b1 :  4 12331332303
      840  0.01191 0.00014 y           a2 :  1  1333133322
      904 -0.01191 0.00014 y           a1 :  9  1333132323
      992  0.01188 0.00014 y           b1 :  3  1333033323
      413 -0.01182 0.00014 y           a1 : 14  1333323303
      841 -0.01181 0.00014 y           a2 :  2  1333133322
    65686 -0.01176 0.00014 w  a1 :  6  b2 :  3 12333133203
     6637 -0.01175 0.00014 x  a1 :  8  a2 :  2 11333332203
      191 -0.01171 0.00014 y           b2 :  4  1333332312
    10877 -0.01169 0.00014 x  a1 : 11  b1 :  3 11333322303
    52883 -0.01166 0.00014 w  b1 :  4  b2 :  4 12333331302
      410 -0.01165 0.00014 y           a1 : 11  1333323303
      306  0.01164 0.00014 y           b2 :  5  1333331223
    48156 -0.01154 0.00013 w           b2 :  8  3333333300
      494  0.01151 0.00013 y           a1 : 13  1333321323
    54222 -0.01149 0.00013 w  b1 :  3  b2 :  5 12333331203
    48140 -0.01143 0.00013 w  b2 :  4  b2 :  5 12333333300
    50995 -0.01142 0.00013 w           a1 :  9  3333333003
      772  0.01133 0.00013 y           a1 :  7  1333231323
    64067  0.01126 0.00013 w  a1 :  9  b2 :  5 12333133302
    48138 -0.01121 0.00013 w           b2 :  4  3333333300
    59196 -0.01101 0.00012 w  a1 :  8  b2 :  5 12333313203
     1050  0.01097 0.00012 y           a1 :  7  1332333303
    50992 -0.01093 0.00012 w  a1 :  6  a1 :  9 12333333003
     1300  0.01092 0.00012 y           a1 :  7  1331332323
      901 -0.01079 0.00012 y           a1 :  6  1333132323
      195 -0.01070 0.00011 y           b2 :  8  1333332312
     4945  0.01069 0.00011 x  b1 :  4  b2 :  4 11333332302
     1310 -0.01068 0.00011 y           a1 : 17  1331332323
      779  0.01064 0.00011 y           a1 : 14  1333231323
    75714 -0.01062 0.00011 w  a1 : 11  b1 :  4 12331332303
    60729 -0.01050 0.00011 w  a1 : 14  b1 :  4 12333312303
    72530 -0.01046 0.00011 w  a1 :  7  b2 :  6 12331333302
    54190  0.01033 0.00011 w  b1 :  3  b2 :  3 12333331203
    64066 -0.01026 0.00011 w  a1 :  8  b2 :  5 12333133302
    68918 -0.01025 0.00010 w  a1 :  9  a1 : 11 12333123303
       41  0.01024 0.00010 y           b2 :  6  1333333302
    49745 -0.01024 0.00010 w  b2 :  4  b2 :  6 12333333102
    65757  0.01024 0.00010 w  a1 : 15  b2 :  5 12333133203
    71417 -0.01023 0.00010 w           b2 :  5  3333033303
    67244  0.01021 0.00010 w  a1 :  6  b1 :  3 12333132303
    57513  0.01020 0.00010 w  a1 :  9  b2 :  5 12333313302
    65753 -0.01019 0.00010 w  a1 : 11  b2 :  5 12333133203
    56288 -0.01019 0.00010 w           b1 :  3  3333330303
    77375 -0.01018 0.00010 w  a1 : 11  a1 : 14 12331323303
    48154  0.01015 0.00010 w  b2 :  6  b2 :  8 12333333300
    68911  0.01014 0.00010 w  a1 :  7  a1 : 10 12333123303
          139 csfs were printed in this range.
