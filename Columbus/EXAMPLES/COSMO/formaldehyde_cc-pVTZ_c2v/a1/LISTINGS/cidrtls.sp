 
 program cidrt 5.9  

 distinct row table construction, reference csf selection, and internal
 walk selection for multireference single- and double-excitation
configuration interaction.

 references:  r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).
              h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. symp. 15, 91 (1981).

 based on the initial version by  Ron Shepard

 extended for spin-orbit CI calculations ( Russ Pitzer, OSU)

 and large active spaces (Thomas Müller, FZ Juelich)

 version date: 16-jul-04


 This Version of Program CIDRT is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIDRT       **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 workspace allocation parameters: lencor=  10000000 mem1=1108566024 ifirst=-264081416
 expanded "keystrokes" are being written to file:
 cidrtky                                                                         
 Spin-Orbit CI Calculation?(y,[n]) Spin-Free Calculation

 input the spin multiplicity [  0]: spin multiplicity, smult            :   1    singlet 
 input the total number of electrons [  0]: total number of electrons, nelt     :    16
 input the number of irreps (1:8) [  0]: point group dimension, nsym         :     4
 enter symmetry labels:(y,[n]) enter 4 labels (a4):
 enter symmetry label, default=   1
 enter symmetry label, default=   2
 enter symmetry label, default=   3
 enter symmetry label, default=   4
 symmetry labels: (symmetry, slabel)
 ( 1,  a1 ) ( 2,  b1 ) ( 3,  b2 ) ( 4,  a2 ) 
 input nmpsy(*):
 nmpsy(*)=        36  18  24  10

   symmetry block summary
 block(*)=         1   2   3   4
 slabel(*)=      a1  b1  b2  a2 
 nmpsy(*)=        36  18  24  10

 total molecular orbitals            :    88
 input the molecular spatial symmetry (irrep 1:nsym) [  0]: state spatial symmetry label        :  a1 

 input the frozen core orbitals (sym(i),rmo(i),i=1,nfct):
 total frozen core orbitals, nfct    :     0
 no frozen core orbitals entered

 number of frozen core orbitals      :     0
 number of frozen core electrons     :     0
 number of internal electrons        :    16

 input the frozen virtual orbitals (sym(i),rmo(i),i=1,nfvt):
 total frozen virtual orbitals, nfvt :     0

 no frozen virtual orbitals entered

 input the internal orbitals (sym(i),rmo(i),i=1,niot):
 niot                                :     9

 modrt(*)=         1   2   3   4   5  37  55  38  56
 slabel(*)=      a1  a1  a1  a1  a1  b1  b2  b1  b2 

 total number of orbitals            :    88
 number of frozen core orbitals      :     0
 number of frozen virtual orbitals   :     0
 number of internal orbitals         :     9
 number of external orbitals         :    79

 orbital-to-level mapping vector
 map(*)=          80  81  82  83  84   1   2   3   4   5   6   7   8   9  10
                  11  12  13  14  15  16  17  18  19  20  21  22  23  24  25
                  26  27  28  29  30  31  85  87  32  33  34  35  36  37  38
                  39  40  41  42  43  44  45  46  47  86  88  48  49  50  51
                  52  53  54  55  56  57  58  59  60  61  62  63  64  65  66
                  67  68  69  70  71  72  73  74  75  76  77  78  79

 input the number of ref-csf doubly-occupied orbitals [  0]: (ref) doubly-occupied orbitals      :     7

 no. of internal orbitals            :     9
 no. of doubly-occ. (ref) orbitals   :     7
 no. active (ref) orbitals           :     2
 no. of active electrons             :     2

 input the active-orbital, active-electron occmnr(*):
  38 56
 input the active-orbital, active-electron occmxr(*):
  38 56

 actmo(*) =       38  56
 occmnr(*)=        0   2
 occmxr(*)=        2   2
 reference csf cumulative electron occupations:
 modrt(*)=         1   2   3   4   5  37  55  38  56
 occmnr(*)=        2   4   6   8  10  12  14  14  16
 occmxr(*)=        2   4   6   8  10  12  14  16  16

 input the active-orbital bminr(*):
  38 56
 input the active-orbital bmaxr(*):
  38 56
 reference csf b-value constraints:
 modrt(*)=         1   2   3   4   5  37  55  38  56
 bminr(*)=         0   0   0   0   0   0   0   0   0
 bmaxr(*)=         0   0   0   0   0   0   0   2   2
 input the active orbital smaskr(*):
  38 56
 modrt:smaskr=
   1:1000   2:1000   3:1000   4:1000   5:1000  37:1000  55:1000  38:1111
  56:1111

 input the maximum excitation level from the reference csfs [  2]: maximum excitation from ref. csfs:  :     2
 number of internal electrons:       :    16

 input the internal-orbital mrsdci occmin(*):
   1  2  3  4  5 37 55 38 56
 input the internal-orbital mrsdci occmax(*):
   1  2  3  4  5 37 55 38 56
 mrsdci csf cumulative electron occupations:
 modrt(*)=         1   2   3   4   5  37  55  38  56
 occmin(*)=        0   0   0   0   0   0   0   0  14
 occmax(*)=       16  16  16  16  16  16  16  16  16

 input the internal-orbital mrsdci bmin(*):
   1  2  3  4  5 37 55 38 56
 input the internal-orbital mrsdci bmax(*):
   1  2  3  4  5 37 55 38 56
 mrsdci b-value constraints:
 modrt(*)=         1   2   3   4   5  37  55  38  56
 bmin(*)=          0   0   0   0   0   0   0   0   0
 bmax(*)=         16  16  16  16  16  16  16  16  16

 input the internal-orbital smask(*):
   1  2  3  4  5 37 55 38 56
 modrt:smask=
   1:1111   2:1111   3:1111   4:1111   5:1111  37:1111  55:1111  38:1111
  56:1111

 internal orbital summary:
 block(*)=         1   1   1   1   1   2   3   2   3
 slabel(*)=      a1  a1  a1  a1  a1  b1  b2  b1  b2 
 rmo(*)=           1   2   3   4   5   1   1   2   2
 modrt(*)=         1   2   3   4   5  37  55  38  56

 reference csf info:
 occmnr(*)=        2   4   6   8  10  12  14  14  16
 occmxr(*)=        2   4   6   8  10  12  14  16  16

 bminr(*)=         0   0   0   0   0   0   0   0   0
 bmaxr(*)=         0   0   0   0   0   0   0   2   2


 mrsdci csf info:
 occmin(*)=        0   0   0   0   0   0   0   0  14
 occmax(*)=       16  16  16  16  16  16  16  16  16

 bmin(*)=          0   0   0   0   0   0   0   0   0
 bmax(*)=         16  16  16  16  16  16  16  16  16


 a priori removal of distinct rows:

 input the level, a, and b values for the vertices 
 to be removed (-1/ to end).

 input level, a, and b (-1/ to end):
 no vertices marked for removal

 impose generalized interacting space restrictions?(y,[n]) generalized interacting space restrictions will not be imposed.
 multp 0 0 0 0 0 0 0 0 0
 spnir
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  hmult 0
 lxyzir 0 0 0
 spnir
  1  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  1  0  0  0  1  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  1  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  1  0  0  0  1  0  0  0  1  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  1  0  0  0  1  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0

 number of rows in the drt :  62

 manual arc removal step:


 input the level, a, b, and step values 
 for the arcs to be removed (-1/ to end).

 input the level, a, b, and step (-1/ to end):
 remarc:   0 arcs removed out of   0 specified.

 xbarz=      45
 xbary=     240
 xbarx=     630
 xbarw=     540
        --------
 nwalk=    1455
 input the range of drt levels to print (l1,l2):
 levprt(*)        -1   0

 reference-csf selection step 1:
 total number of z-walks in the drt, nzwalk=      45

 input the list of allowed reference symmetries:
 allowed reference symmetries:             1
 allowed reference symmetry labels:      a1 
 keep all of the z-walks as references?(y,[n]) all z-walks are initially deleted.

 generate walks while applying reference drt restrictions?([y],n) reference drt restrictions will be imposed on the z-walks.

 impose additional orbital-group occupation restrictions?(y,[n])
 apply primary reference occupation restrictions?(y,[n])
 manually select individual walks?(y,[n])
 step 1 reference csf selection complete.
        2 csfs initially selected from      45 total walks.

 beginning step-vector based selection.
 enter [internal_orbital_step_vector/disposition] pairs:

 enter internal orbital step vector, (-1/ to end):
   1  2  3  4  5 37 55 38 56

 step 2 reference csf selection complete.
        2 csfs currently selected from      45 total walks.

 beginning numerical walk based selection.
 enter positive walk numbers to add walks,
 negative walk numbers to delete walks, and zero to end:

 input reference walk number (0 to end) [  0]:
 numerical walk-number based selection complete.
        2 reference csfs selected from      45 total z-walks.

 input the reference occupations, mu(*):
 reference occupations:
 mu(*)=            0   0   0   0   0   0   0   0   0

 number of step vectors saved:      2

 exlimw: beginning excitation-based walk selection...

  number of valid internal walks of each symmetry:

       a1      b1      b2      a2 
      ----    ----    ----    ----
 z      21       0       0       0
 y      30      34      34      30
 x      22      15      15       4
 w      37      15      15       4

 csfs grouped by internal walk symmetry:

       a1      b1      b2      a2 
      ----    ----    ----    ----
 z      21       0       0       0
 y     930     544     748     300
 x   18942   10740   12630    2648
 w   34780   10740   12630    2648

 total csf counts:
 z-vertex:       21
 y-vertex:     2522
 x-vertex:    44960
 w-vertex:    60798
           --------
 total:      108301

 this is an obsolete prompt.(y,[n])
 final mrsdci walk selection step:

 nvalw(*)=      21     128      56      71 nvalwt=     276

 enter positive walk numbers to add walks,
 negative walk numbers to delete walks, and zero to end.

 input mrsdci walk number (0 to end) [  0]:
 end of manual mrsdci walk selection.
 number added=   0 number removed=   0

 nvalw(*)=      21     128      56      71 nvalwt=     276


 lprune: l(*,*,*) pruned with nwalk=    1455 nvalwt=     276
 lprune:  z-drt, nprune=   108
 lprune:  y-drt, nprune=    81
 lprune: wx-drt, nprune=    90

 xbarz=      23
 xbary=     128
 xbarx=      56
 xbarw=      71
        --------
 nwalk=     278
 levprt(*)        -1   0

 beginning the reference csf index recomputation...

     iref   iwalk  step-vector
   ------  ------  ------------
        1       1  333333330
        2       2  333333303
 indx01:     2 elements set in vec01(*)

 beginning the valid upper walk index recomputation...
 indx01:   276 elements set in vec01(*)

 beginning the final csym(*) computation...

  number of valid internal walks of each symmetry:

       a1      b1      b2      a2 
      ----    ----    ----    ----
 z      21       0       0       0
 y      30      34      34      30
 x      22      15      15       4
 w      37      15      15       4

 csfs grouped by internal walk symmetry:

       a1      b1      b2      a2 
      ----    ----    ----    ----
 z      21       0       0       0
 y     930     544     748     300
 x   18942   10740   12630    2648
 w   34780   10740   12630    2648

 total csf counts:
 z-vertex:       21
 y-vertex:     2522
 x-vertex:    44960
 w-vertex:    60798
           --------
 total:      108301

 input a title card, default=cidrt_title
 title card:
  cidrt_title                                                                    

 input a drt file name, default=cidrtfl
 drt and indexing arrays will be written to file:
 cidrtfl                                                                         

 write the drt file?([y],n) drt file is being written...
 !timer: cidrt required                  user+sys=     0.010 walltime=     0.000
