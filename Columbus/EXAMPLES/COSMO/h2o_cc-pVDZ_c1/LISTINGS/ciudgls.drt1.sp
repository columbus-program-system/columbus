1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      09/20/00    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 using llenci= -1
================================================================================
 nsubmx= 2 lenci= 4656
global arrays:        23280   (              0.18 MB)
vdisk:                    0   (              0.00 MB)
drt:                 252658   (              0.96 MB)
================================================================================
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core            9999999 DP per process
 CIUDG version 5.9.4 (29-Aug-2003)
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 0,
  GSET = 0,
   DAVCOR =10,
  NCOREL = 10
  NROOT = 1
  IVMODE = 3
  NBKITR = 0
  NVBKMN = 1
  NVBKMX = 2
  RTOLBK = 1e-3,
  NITER = 20
  NVCIMN = 1
  NVCIMX = 2
  RTOLCI = 1e-3,
  IDEN  = 1
  CSFPRN = 10,
  lvlprt = 5
  frcsub = 2
  cosmocalc = 1
  /end
 ------------------------------------------------------------------------

 ** list of control variables **
 nrfitr =   30      nvrfmx =    2      nvrfmn =    1
 lvlprt =    5      nroot  =    1      noldv  =    0      noldhv =    0
 nunitv =    1      ntype  =    0      nbkitr =    0      niter  =   20
 ivmode =    3      vout   =    0      istrt  =    0      iortls =    0
 nvbkmx =    2      ibktv  =   -1      ibkthv =   -1      frcsub =    2
 nvcimx =    2      icitv  =   -1      icithv =   -1      maxseg =    4
 iden   =    1      itran  =    0      froot  =    0      rtmode =    0
 ftcalc =    1      lrtshift=1.0000    ncouple=    1      skipso  =   F
 ncorel =   10      csfprn  =  10      ctol   = 1.00E-02  davcor  =  10


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-03    1.000E-03

 units and filenames:
    1: ( 6)    ciudgls                                                     
    2: ( 5)    ciudgin                                                     
    3: ( 7)    ciudgsm                                                     
    4: ( 4)    cihdiag                                                     
    5: ( 3)    cihpseu                                                     
    6: (10)    cihvfl                                                      
    7: (11)    civfl                                                       
    8: (12)    diagint                                                     
    9: (13)    ofdgint                                                     
   10: (17)    cidrtfl                                                     
   11: (20)    ciftdfl                                                     
   12: (21)    ciftofl                                                     
   13: (24)    ciftifl                                                     
   14: ( 8)    ciflind                                                     
   15: (15)    civin                                                       
   16: (16)    civout                                                      
   18: (18)    d1fl                                                        
   20: (53)    cifvfl                                                      
   21: (25)    ciscr4                                                      
   22: (41)    ciftotd                                                     
   23: (42)    flacpfd                                                     
   26: (26)    ciscr5                                                      
   27: (47)    restart                                                     
   28: (48)    civout                                                      
   29: (49)    cirefv                                                      
   31: (31)    fil4w                                                       
   32: (32)    fil4x                                                       
   33: (33)    fil3w                                                       
   34: (34)    fil3x                                                       
   36: (36)    aoints                                                      
   37: (37)    aoints2                                                     
   38: (38)    drtfil                                                      
   39: (50)    cisrtif                                                     
   42: (50)    mocoef                                                      
   45: (52)    nocoef_ci                                                   
   46: (53)    civfl_restart                                               
   54: (54)    civcosmo                                                    
 ------------------------------------------------------------------------

 workspace allocation information: lcore=   9999999 mem1=1108566024 ifirst=-264081384

 integral file titles:
 Hermit Integral Program : SIFS version  hochtor2        Thu Oct  7 14:40:51 2004
  cidrt_title                                                                    
  title                                                                          
 mofmt: formatted orbitals label=morbl   hochtor2        Thu Oct  7 14:40:52 2004
 SIFS file created by program tran.      hochtor2        Thu Oct  7 14:40:52 2004

 core energy values from the integral file:
 energy( 1)=  9.317913916904E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total core repulsion energy =  9.317913916904E+00

 drt header information:
  cidrt_title                                                                    
 spnorb, spnodd, lxyzir,hmult F F 0 0 0 0
 nmot  =    24 niot  =     5 nfct  =     0 nfvt  =     0
 nrow  =    20 nsym  =     1 ssym  =     1 lenbuf=  1600
 nwalk,xbar:         31        1        5       10       15
 nvalwt,nvalw:       31        1        5       10       15
 ncsft:            4656
 total number of valid internal walks:      31
 nvalz,nvaly,nvalx,nvalw =        1       5      10      15

 cisrt info file parameters:
 file number  12 blocksize   4095
 mxbld   4095
 nd4ext,nd2ext,nd0ext   380   190    30
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int    21375    19665     8550     1995      165        0        0        0
 minbl4,minbl3,maxbl2   567   567   570
 maxbuf 32767
 number of external orbitals per symmetry block:  19
 nmsym   1 number of internal orbitals   5

 formula file title:
 Hermit Integral Program : SIFS version  hochtor2        Thu Oct  7 14:40:51 2004
  cidrt_title                                                                    
  title                                                                          
 mofmt: formatted orbitals label=morbl   hochtor2        Thu Oct  7 14:40:52 2004
 SIFS file created by program tran.      hochtor2        Thu Oct  7 14:40:52 2004
  cidrt_title                                                                    
 file nmb.  24 block size     0
 pthz,pthy,pthx,pthw:     1     5    10    15 total internal walks:      31
 maxlp3,n3xtlp,n2xtlp,n1xtlp,n0xtlp,n2lp,n1lp,n0lp     0     0     0     0
 orbsym(*)= 1 1 1 1 1

 setref:        1 references kept,
                0 references were marked as invalid, out of
                1 total.
 limcnvrt: found 1 valid internal walksout of  1
  walks (skipping trailing invalids)
  ... adding  1 segmentation marks segtype= 1
 limcnvrt: found 5 valid internal walksout of  5
  walks (skipping trailing invalids)
  ... adding  5 segmentation marks segtype= 2
 limcnvrt: found 10 valid internal walksout of  10
  walks (skipping trailing invalids)
  ... adding  10 segmentation marks segtype= 3
 limcnvrt: found 15 valid internal walksout of  15
  walks (skipping trailing invalids)
  ... adding  15 segmentation marks segtype= 4
 nmb.of records 4-ext     1
 nmb.of records 3-ext     3
 nmb.of records 2-ext     1
 nmb.of records 1-ext     1
 nmb.of records 0-ext     0
 nmb.of records 2-int     0
 nmb.of records 1-int     0
 nmb.of records 0-int
 mxorb=  19

 < n-ex core usage >
     routines:
    fourex       70180
    threx        33149
    twoex         7990
    onex          4894
    allin         4095
    diagon        4649
               =======
   maximum       70180

  __ static summary __ 
   reflst            1
   hrfspc            1
               -------
   static->          1

  __ core required  __ 
   totstc            1
   max n-ex      70180
               -------
   totnec->      70181

  __ core available __ 
   totspc      9999999
   totnec -      70181
               -------
   totvec->    9929818

 number of external paths / symmetry
 vertex x     171
 vertex w     190
================ pruneseg on segment    1(   1)================
 original DRT
iwalk=   1step: 3 3 3 3 3
 level  0 through level  4 of the drt:

 row lev  l0  l1  l2  l3     y0      y1      y2    xbar 

  17  4   0   0   0  20       1       1       1       1
  18  4   0   0   0   0       0       0       0       0
  19  4   0   0   0   0       0       0       0       0
 ....................................

  13  3   0   0   0  17       1       1       1       1
  14  3   0   0   0   0       0       0       0       0
  15  3   0   0   0   0       0       0       0       0
  16  3   0   0   0   0       0       0       0       0
 ....................................

   9  2   0   0   0  13       1       1       1       1
  10  2   0   0   0   0       0       0       0       0
  11  2   0   0   0   0       0       0       0       0
  12  2   0   0   0   0       0       0       0       0
 ....................................

   5  1   0   0   0   9       1       1       1       1
   6  1   0   0   0   0       0       0       0       0
   7  1   0   0   0   0       0       0       0       0
   8  1   0   0   0   0       0       0       0       0
 ....................................

   1  0   0   0   0   5       1       1       1       1
   2  0   0   0   0   0       0       0       0       0
   3  0   0   0   0   0       0       0       0       0
   4  0   0   0   0   0       0       0       0       0
 ....................................


 lprune: l(*,*,*) pruned with nwalk=       1 nvalwt=       1 nprune=       0
 ynew( 1):
  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0
  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0
  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
 xbarnew( 1):
  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  1
 limn array= 1
  1
 limvec array= 1
  2
 reduced DRT,invalid= 0
iwalk=   1step: 3 3 3 3 3
 level  0 through level  4 of the drt:

 row lev  l0  l1  l2  l3     y0      y1      y2    xbar 

  17  4   0   0   0  20       1       1       1       1
  18  4   0   0   0   0       0       0       0       0
  19  4   0   0   0   0       0       0       0       0
 ....................................

  13  3   0   0   0  17       1       1       1       1
  14  3   0   0   0   0       0       0       0       0
  15  3   0   0   0   0       0       0       0       0
  16  3   0   0   0   0       0       0       0       0
 ....................................

   9  2   0   0   0  13       1       1       1       1
  10  2   0   0   0   0       0       0       0       0
  11  2   0   0   0   0       0       0       0       0
  12  2   0   0   0   0       0       0       0       0
 ....................................

   5  1   0   0   0   9       1       1       1       1
   6  1   0   0   0   0       0       0       0       0
   7  1   0   0   0   0       0       0       0       0
   8  1   0   0   0   0       0       0       0       0
 ....................................

   1  0   0   0   0   5       1       1       1       1
   2  0   0   0   0   0       0       0       0       0
   3  0   0   0   0   0       0       0       0       0
   4  0   0   0   0   0       0       0       0       0
 ....................................

================ pruneseg on segment    2(   2)================
 original DRT
iwalk=   2step: 3 3 3 3 2
iwalk=   3step: 3 3 3 2 3
iwalk=   4step: 3 3 2 3 3
iwalk=   5step: 3 2 3 3 3
iwalk=   6step: 2 3 3 3 3
 level  0 through level  4 of the drt:

 row lev  l0  l1  l2  l3     y0      y1      y2    xbar 

  17  4   0   0   0  20       1       1       1       1
  18  4   0   0  20   0       1       1       0       1
  19  4   0   0   0   0       0       0       0       0
 ....................................

  13  3   0   0   0  17       1       1       1       1
  14  3   0   0  17  18       2       2       1       2
  15  3   0   0   0   0       0       0       0       0
  16  3   0   0   0   0       0       0       0       0
 ....................................

   9  2   0   0   0  13       1       1       1       1
  10  2   0   0  13  14       3       3       2       3
  11  2   0   0   0   0       0       0       0       0
  12  2   0   0   0   0       0       0       0       0
 ....................................

   5  1   0   0   0   9       1       1       1       1
   6  1   0   0   9  10       4       4       3       4
   7  1   0   0   0   0       0       0       0       0
   8  1   0   0   0   0       0       0       0       0
 ....................................

   1  0   0   0   0   0       0       0       0       0
   2  0   0   0   5   6       6       6       5       5
   3  0   0   0   0   0       0       0       0       0
   4  0   0   0   0   0       0       0       0       0
 ....................................


 lprune: l(*,*,*) pruned with nwalk=       5 nvalwt=       4 nprune=       0
 ynew( 2):
  0  5  0  0  1  4  0  0  1  3  0  0  1  2  0  0  1  1  0  0
  0  5  0  0  1  4  0  0  1  3  0  0  1  2  0  0  1  1  0  0
  0  4  0  0  1  3  0  0  1  2  0  0  1  1  0  0  1  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
 xbarnew( 2):
  0  5  0  0  1  4  0  0  1  3  0  0  1  2  0  0  1  1  0  1
 limn array= 5
  1  2  3  4  5
 limvec array= 5
  0  2  2  2  2
 reduced DRT,invalid= 0
iwalk=   1step: 3 3 3 3 2
iwalk=   2step: 3 3 3 2 3
iwalk=   3step: 3 3 2 3 3
iwalk=   4step: 3 2 3 3 3
iwalk=   5step: 2 3 3 3 3
 level  0 through level  4 of the drt:

 row lev  l0  l1  l2  l3     y0      y1      y2    xbar 

  17  4   0   0   0  20       1       1       1       1
  18  4   0   0  20   0       1       1       0       1
  19  4   0   0   0   0       0       0       0       0
 ....................................

  13  3   0   0   0  17       1       1       1       1
  14  3   0   0  17  18       2       2       1       2
  15  3   0   0   0   0       0       0       0       0
  16  3   0   0   0   0       0       0       0       0
 ....................................

   9  2   0   0   0  13       1       1       1       1
  10  2   0   0  13  14       3       3       2       3
  11  2   0   0   0   0       0       0       0       0
  12  2   0   0   0   0       0       0       0       0
 ....................................

   5  1   0   0   0   9       1       1       1       1
   6  1   0   0   9  10       4       4       3       4
   7  1   0   0   0   0       0       0       0       0
   8  1   0   0   0   0       0       0       0       0
 ....................................

   1  0   0   0   0   0       0       0       0       0
   2  0   0   0   5   6       5       5       4       5
   3  0   0   0   0   0       0       0       0       0
   4  0   0   0   0   0       0       0       0       0
 ....................................

================ pruneseg on segment    3(   3)================
 original DRT
iwalk=   7step: 3 3 3 2 2
iwalk=   8step: 3 3 2 3 2
iwalk=   9step: 3 3 2 2 3
iwalk=  10step: 3 2 3 3 2
iwalk=  11step: 3 2 3 2 3
iwalk=  12step: 3 2 2 3 3
iwalk=  13step: 2 3 3 3 2
iwalk=  14step: 2 3 3 2 3
iwalk=  15step: 2 3 2 3 3
iwalk=  16step: 2 2 3 3 3
 level  0 through level  4 of the drt:

 row lev  l0  l1  l2  l3     y0      y1      y2    xbar 

  17  4   0   0   0  20       1       1       1       1
  18  4   0   0  20   0       1       1       0       1
  19  4  20   0   0   0       0       0       0       1
 ....................................

  13  3   0   0   0  17       1       1       1       1
  14  3   0   0  17  18       2       2       1       2
  15  3   0   0  18   0       1       1       0       1
  16  3  17  18   0  19       2       1       1       3
 ....................................

   9  2   0   0   0  13       1       1       1       1
  10  2   0   0  13  14       3       3       2       3
  11  2   0   0  14  15       3       3       1       3
  12  2  13  14   0  16       5       3       3       6
 ....................................

   5  1   0   0   0   9       1       1       1       1
   6  1   0   0   9  10       4       4       3       4
   7  1   0   0  10  11       6       6       3       6
   8  1   9  10   0  12       9       6       6      10
 ....................................

   1  0   0   0   0   0       0       0       0       0
   2  0   0   0   0   0       0       0       0       0
   3  0   0   0   6   7      16      16      12      10
   4  0   5   6   0   8      30      26      26      15
 ....................................


 lprune: l(*,*,*) pruned with nwalk=      10 nvalwt=      10 nprune=      14
 ynew( 3):
  0  0 10  0  0  4  6  0  1  3  3  0  1  2  1  0  1  1  0  0
  0  0 10  0  0  4  6  0  1  3  3  0  1  2  1  0  1  1  0  0
  0  0  6  0  0  3  3  0  1  2  1  0  1  1  0  0  1  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
 xbarnew( 3):
  0  0 10  0  0  4  6  0  1  3  3  0  1  2  1  0  1  1  0  1
 limn array= 10
  1  2  3  4  5  6  7  8  9 10
 limvec array= 10
  2  2  2  2  2  2  2  2  2  2
 reduced DRT,invalid= 0
iwalk=   1step: 3 3 3 2 2
iwalk=   2step: 3 3 2 3 2
iwalk=   3step: 3 3 2 2 3
iwalk=   4step: 3 2 3 3 2
iwalk=   5step: 3 2 3 2 3
iwalk=   6step: 3 2 2 3 3
iwalk=   7step: 2 3 3 3 2
iwalk=   8step: 2 3 3 2 3
iwalk=   9step: 2 3 2 3 3
iwalk=  10step: 2 2 3 3 3
 level  0 through level  4 of the drt:

 row lev  l0  l1  l2  l3     y0      y1      y2    xbar 

  17  4   0   0   0  20       1       1       1       1
  18  4   0   0  20   0       1       1       0       1
  19  4   0   0   0   0       0       0       0       0
 ....................................

  13  3   0   0   0  17       1       1       1       1
  14  3   0   0  17  18       2       2       1       2
  15  3   0   0  18   0       1       1       0       1
  16  3   0   0   0   0       0       0       0       0
 ....................................

   9  2   0   0   0  13       1       1       1       1
  10  2   0   0  13  14       3       3       2       3
  11  2   0   0  14  15       3       3       1       3
  12  2   0   0   0   0       0       0       0       0
 ....................................

   5  1   0   0   0   0       0       0       0       0
   6  1   0   0   9  10       4       4       3       4
   7  1   0   0  10  11       6       6       3       6
   8  1   0   0   0   0       0       0       0       0
 ....................................

   1  0   0   0   0   0       0       0       0       0
   2  0   0   0   0   0       0       0       0       0
   3  0   0   0   6   7      10      10       6      10
   4  0   0   0   0   0       0       0       0       0
 ....................................

================ pruneseg on segment    4(   3)================
 original DRT
iwalk=  17step: 3 3 3 3 0
iwalk=  18step: 3 3 3 1 2
iwalk=  19step: 3 3 3 0 3
iwalk=  20step: 3 3 1 3 2
iwalk=  21step: 3 3 1 2 3
iwalk=  22step: 3 3 0 3 3
iwalk=  23step: 3 1 3 3 2
iwalk=  24step: 3 1 3 2 3
iwalk=  25step: 3 1 2 3 3
iwalk=  26step: 3 0 3 3 3
iwalk=  27step: 1 3 3 3 2
iwalk=  28step: 1 3 3 2 3
iwalk=  29step: 1 3 2 3 3
iwalk=  30step: 1 2 3 3 3
iwalk=  31step: 0 3 3 3 3
 level  0 through level  4 of the drt:

 row lev  l0  l1  l2  l3     y0      y1      y2    xbar 

  17  4   0   0   0  20       1       1       1       1
  18  4   0   0  20   0       1       1       0       1
  19  4  20   0   0   0       0       0       0       1
 ....................................

  13  3   0   0   0  17       1       1       1       1
  14  3   0   0  17  18       2       2       1       2
  15  3   0   0  18   0       1       1       0       1
  16  3  17  18   0  19       2       1       1       3
 ....................................

   9  2   0   0   0  13       1       1       1       1
  10  2   0   0  13  14       3       3       2       3
  11  2   0   0  14  15       3       3       1       3
  12  2  13  14   0  16       5       3       3       6
 ....................................

   5  1   0   0   0   9       1       1       1       1
   6  1   0   0   9  10       4       4       3       4
   7  1   0   0  10  11       6       6       3       6
   8  1   9  10   0  12       9       6       6      10
 ....................................

   1  0   0   0   0   0       0       0       0       0
   2  0   0   0   0   0       0       0       0       0
   3  0   0   0   6   7      16      16      12      10
   4  0   5   6   0   8      30      26      26      15
 ....................................


 lprune: l(*,*,*) pruned with nwalk=      15 nvalwt=      15 nprune=       7
 ynew( 3):
  0  0  0 14  1  4  0  9  1  3  0  5  1  2  0  2  1  1  0  0
  0  0  0 10  1  4  0  6  1  3  0  3  1  2  0  1  1  1  0  0
  0  0  0 10  1  3  0  6  1  2  0  3  1  1  0  1  1  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
 xbarnew( 3):
  0  0  0 15  1  4  0 10  1  3  0  6  1  2  0  3  1  1  1  1
 limn array= 15
  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15
 limvec array= 15
  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2
 reduced DRT,invalid= 0
iwalk=   1step: 3 3 3 3 0
iwalk=   2step: 3 3 3 1 2
iwalk=   3step: 3 3 3 0 3
iwalk=   4step: 3 3 1 3 2
iwalk=   5step: 3 3 1 2 3
iwalk=   6step: 3 3 0 3 3
iwalk=   7step: 3 1 3 3 2
iwalk=   8step: 3 1 3 2 3
iwalk=   9step: 3 1 2 3 3
iwalk=  10step: 3 0 3 3 3
iwalk=  11step: 1 3 3 3 2
iwalk=  12step: 1 3 3 2 3
iwalk=  13step: 1 3 2 3 3
iwalk=  14step: 1 2 3 3 3
iwalk=  15step: 0 3 3 3 3
 level  0 through level  4 of the drt:

 row lev  l0  l1  l2  l3     y0      y1      y2    xbar 

  17  4   0   0   0  20       1       1       1       1
  18  4   0   0  20   0       1       1       0       1
  19  4  20   0   0   0       0       0       0       1
 ....................................

  13  3   0   0   0  17       1       1       1       1
  14  3   0   0  17  18       2       2       1       2
  15  3   0   0   0   0       0       0       0       0
  16  3  17  18   0  19       2       1       1       3
 ....................................

   9  2   0   0   0  13       1       1       1       1
  10  2   0   0  13  14       3       3       2       3
  11  2   0   0   0   0       0       0       0       0
  12  2  13  14   0  16       5       3       3       6
 ....................................

   5  1   0   0   0   9       1       1       1       1
   6  1   0   0   9  10       4       4       3       4
   7  1   0   0   0   0       0       0       0       0
   8  1   9  10   0  12       9       6       6      10
 ....................................

   1  0   0   0   0   0       0       0       0       0
   2  0   0   0   0   0       0       0       0       0
   3  0   0   0   0   0       0       0       0       0
   4  0   5   6   0   8      14      10      10      15
 ....................................




                   segmentation summary for type diagonal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1           1|         1|         0|         1|         0|         1|
 -------------------------------------------------------------------------------
  Y 2           5|        95|         1|         5|         1|         2|
 -------------------------------------------------------------------------------
  X 3          10|      1710|        96|        10|         6|         3|
 -------------------------------------------------------------------------------
  W 4          15|      2850|      1806|        15|        16|         4|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>      4656


 297 dimension of the ci-matrix ->>>      4656


 297 dimension of the ci-matrix ->>>         7

                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      10       1       1710          1      10       1
     2  4   1    25      two-ext wz   2X  4 1      15       1       2850          1      15       1
     3  4   3    26      two-ext wx   2X  4 3      15      10       2850       1710      15      10
     4  2   1    11      one-ext yz   1X  2 1       5       1         95          1       5       1
     5  3   2    15      1ex3ex  yx   3X  3 2      10       5       1710         95      10       5
     6  4   2    16      1ex3ex  yw   3X  4 2      15       5       2850         95      15       5
     7  1   1     1      allint zz    OX  1 1       1       1          1          1       1       1
     8  2   2     5      0ex2ex yy    OX  2 2       5       5         95         95       5       5
     9  3   3     6      0ex2ex xx    OX  3 3      10      10       1710       1710      10      10
    10  4   4     7      0ex2ex ww    OX  4 4      15      15       2850       2850      15      15
    11  1   1    75      dg-024ext z  DG  1 1       1       1          1          1       1       1
    12  2   2    45      4exdg024 y   DG  2 2       5       5         95         95       5       5
    13  3   3    46      4exdg024 x   DG  3 3      10      10       1710       1710      10      10
    14  4   4    47      4exdg024 w   DG  4 4      15      15       2850       2850      15      15
----------------------------------------------------------------------------------------------------
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.333640E+01-0.311405E+01-0.411071E+01-0.409434E+01-0.474346E+01-0.533548E+01-0.433126E+01-0.358815E+01-0.343744E+01-0.318602E+01
 -0.403221E+01-0.264879E+01-0.363589E+01-0.367752E+01-0.366332E+01-0.363400E+01-0.338294E+01-0.303138E+01-0.288889E+01-0.330431E+02
 -0.787128E+01-0.671895E+01-0.694549E+01-0.710686E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.333640E+01-0.311405E+01-0.411071E+01-0.409434E+01-0.474346E+01-0.533548E+01-0.433126E+01-0.358815E+01-0.343744E+01-0.318602E+01
 -0.403221E+01-0.264879E+01-0.363589E+01-0.367752E+01-0.366332E+01-0.363400E+01-0.338294E+01-0.303138E+01-0.288889E+01-0.330431E+02
 -0.787128E+01-0.671895E+01-0.694549E+01-0.710686E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.333640E+01-0.311405E+01-0.411071E+01-0.409434E+01-0.474346E+01-0.533548E+01-0.433126E+01-0.358815E+01-0.343744E+01-0.318602E+01
 -0.403221E+01-0.264879E+01-0.363589E+01-0.367752E+01-0.366332E+01-0.363400E+01-0.338294E+01-0.303138E+01-0.288889E+01-0.330431E+02
 -0.787128E+01-0.671895E+01-0.694549E+01-0.710686E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380
 DIAGEL COUNTS: DG0X,DG2X,DG4X= 440 145 30
 diagonal elements written to file   4

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        15 2x:         0 4x:         0
All internal counts: zz :         0 yy:         0 xx:         0 ww:         0
One-external counts: yz :         0 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         0    task #   2:         0    task #   3:         0    task #   4:         0
task #   5:         0    task #   6:         0    task #   7:        -1    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:        14    task #  12:         0
task #  13:         0    task #  14:         0    task #
 reference space has dimension       1

          hamiltonian matrix in the reference space 

              rcsf   1
 rcsf   1   -76.02703281

    root           eigenvalues
    ----           ------------
       1         -76.0270328082

          eigenvectors in the subspace basis

                v:   1
 rcsf   1     1.00000000

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt= 1

         vector  1 from unit 11 written to unit 49 filename cirefv                                                      
 ufvoutnew: ... writing  recamt= 1

         vector  1 from unit 11 written to unit 49 filename cirefv                                                      

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              4656
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               2
 number of roots to converge:                             1
 number of iterations:                                   20
 residual norm convergence criteria:               0.001000

 elast before the main iterative loop 
 elast =  -76.0270328

 rtflw before main loop =  0

          starting ci iteration   1

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.31791392

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= T F

################################################################################
 one electron density matrix calculation 
################################################################################


 workspace allocation information: mxcore= 9999997

 space required:

    space required for calls in multd2:
       onex          475
       allin           0
       diagon        950
    max.      ---------
       maxnex        950

    total core space usage:
       maxnex        950
    max k-seg       2850
    max k-ind         15
              ---------
       totmax       6680

 total core array space available  9999997
 total used                           6680
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=      10  D0X=      30  D0W=      50
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================

          ci-one-electron density block   1

                mo   1         mo   2         mo   3         mo   4         mo   5         mo   6         mo   7         mo   8
   mo   1     2.00000000
   mo   2     0.00000000     2.00000000
   mo   3     0.00000000     0.00000000     2.00000000
   mo   4     0.00000000     0.00000000     0.00000000     2.00000000
   mo   5     0.00000000     0.00000000     0.00000000     0.00000000     2.00000000

                mo   9         mo  10         mo  11         mo  12         mo  13         mo  14         mo  15         mo  16

                mo  17         mo  18         mo  19         mo  20         mo  21         mo  22         mo  23         mo  24


*****   symmetry block SYM1   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       2.00000000
       2       0.00000000     2.00000000
       3       0.00000000     0.00000000     2.00000000
       4       0.00000000     0.00000000     0.00000000     2.00000000

               Column   5     Column   6     Column   7     Column   8
       5       2.00000000

               Column   9     Column  10     Column  11     Column  12

               Column  13     Column  14     Column  15     Column  16

               Column  17     Column  18     Column  19     Column  20

               Column  21     Column  22     Column  23     Column  24

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7        eno   8
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    8   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno   9        eno  10        eno  11        eno  12        eno  13        eno  14        eno  15        eno  16
 emo    9   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   10   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   11   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   12   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   13   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   14   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   15   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   16   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno  17        eno  18        eno  19        eno  20        eno  21        eno  22        eno  23        eno  24
 emo   17   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   18   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   19   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   20   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   21   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   22   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   23   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   24   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   2.0000000      2.0000000      2.0000000      2.0000000      2.0000000
   0.0000000      0.0000000      0.0000000      0.0000000      0.0000000
   0.0000000      0.0000000      0.0000000      0.0000000      0.0000000
   0.0000000      0.0000000      0.0000000      0.0000000      0.0000000
   0.0000000      0.0000000      0.0000000      0.0000000

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7         no   8
  mo    1   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo    2   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo    3   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
  mo    5   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
  mo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01
  mo   24   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00

                no   9         no  10         no  11         no  12         no  13         no  14         no  15         no  16
  mo    8   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo    9   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo   10   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo   11   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo   12   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
  mo   13   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
  mo   14   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
  mo   15   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

                no  17         no  18         no  19         no  20         no  21         no  22         no  23         no  24
  mo   16   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo   17   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo   18   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo   19   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo   20   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
  mo   21   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
  mo   22   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
  mo   23   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01
  eigenvectors of nos in ao-basis, column    1
  0.0000000E+00  0.0000000E+00  0.0000000E+00  0.0000000E+00  0.9188403E+00  0.0000000E+00  0.0000000E+00  0.7204964E-01
  0.0000000E+00  0.1416552E-14 -0.1830008E-01  0.0000000E+00  0.0000000E+00  0.0000000E+00  0.0000000E+00  0.0000000E+00
  0.0000000E+00  0.3154239E-01  0.0000000E+00  0.0000000E+00  0.0000000E+00  0.0000000E+00  0.3154239E-01  0.0000000E+00
  eigenvectors of nos in ao-basis, column    2
  0.1000431E+01  0.2482130E-02 -0.1419915E-02  0.7781734E-14  0.0000000E+00 -0.2616439E-02 -0.3398887E-13  0.0000000E+00
  0.1645654E-02  0.0000000E+00  0.0000000E+00  0.3376021E-05  0.3967485E-14  0.8587611E-04 -0.5438131E-03  0.6927337E-03
 -0.6163335E-03  0.0000000E+00 -0.5060470E-03 -0.5438131E-03  0.6927337E-03  0.6163335E-03  0.0000000E+00 -0.5060470E-03
  eigenvectors of nos in ao-basis, column    3
 -0.8844768E-02  0.8674603E+00 -0.1297648E+00  0.6089053E-13  0.0000000E+00 -0.1174189E+00  0.6568151E-13  0.0000000E+00
  0.6892078E-01  0.0000000E+00  0.0000000E+00  0.3306414E-03 -0.5614132E-14  0.1611783E-02  0.3379710E+00 -0.1596253E+00
  0.3836313E-01  0.0000000E+00  0.2045257E-01  0.3379710E+00 -0.1596253E+00 -0.3836313E-01  0.0000000E+00  0.2045257E-01
  eigenvectors of nos in ao-basis, column    4
  0.4616762E-13  0.1371997E-12 -0.1998070E-12  0.7158542E+00  0.0000000E+00  0.1612520E-12 -0.1132335E+00  0.0000000E+00
  0.1762196E-13  0.0000000E+00  0.0000000E+00 -0.4966912E-15 -0.2722905E-01 -0.6927103E-15 -0.5635816E+00  0.1958934E+00
 -0.2355515E-01  0.0000000E+00 -0.3267843E-01  0.5635816E+00 -0.1958934E+00 -0.2355515E-01  0.0000000E+00  0.3267843E-01
  eigenvectors of nos in ao-basis, column    5
 -0.2070810E-02  0.2955600E+00  0.1824047E+00 -0.1630789E-12  0.0000000E+00  0.7982083E+00  0.8010964E-13  0.0000000E+00
 -0.1316750E-02  0.0000000E+00  0.0000000E+00 -0.5179269E-02  0.7124025E-14 -0.2323002E-02 -0.3484059E+00  0.1370807E+00
 -0.3161476E-01  0.0000000E+00  0.8311562E-02 -0.3484059E+00  0.1370807E+00  0.3161476E-01  0.0000000E+00  0.8311562E-02
  eigenvectors of nos in ao-basis, column    6
 -0.1687550E-13 -0.6976727E-13  0.1630899E-12  0.7277607E+00 -0.2063855E-20 -0.2635885E-13  0.8341926E+00 -0.6919644E-19
 -0.4339350E-13  0.8446766E-19  0.9486414E-19  0.5717816E-14 -0.1329366E+01  0.1531364E-13  0.1926243E+01 -0.6729536E+00
  0.6201121E+00  0.1166532E-18  0.4908839E+00 -0.1926243E+01  0.6729536E+00  0.6201121E+00  0.5660713E-19 -0.4908839E+00
  eigenvectors of nos in ao-basis, column    7
 -0.5211847E-01  0.1431756E+00  0.9350625E+00 -0.3212687E-12 -0.2651782E-16 -0.2693111E+00  0.2294226E-12  0.6097966E-16
 -0.2052835E+00 -0.1917530E-15 -0.1461129E-16  0.2090738E-02 -0.2192932E-13  0.5372106E-02 -0.9729573E-01 -0.7866691E+00
 -0.1885770E-01 -0.7385777E-16 -0.1685441E-01 -0.9729573E-01 -0.7866691E+00  0.1885770E-01  0.4913963E-17 -0.1685441E-01
  eigenvectors of nos in ao-basis, column    8
 -0.7525888E-13 -0.3615981E-12  0.3645833E-12 -0.4064254E+00  0.6143126E-16  0.1928467E-12 -0.4850854E+00 -0.4125148E-16
 -0.2481429E-12 -0.2448182E-16 -0.2575400E-16 -0.2153392E-14  0.2228350E-01  0.5706246E-14 -0.3830673E-01 -0.1445818E+01
 -0.2186979E-01 -0.2822300E-16 -0.1841129E-01  0.3830673E-01  0.1445818E+01 -0.2186979E-01 -0.4770756E-16  0.1841129E-01
  eigenvectors of nos in ao-basis, column    9
  0.8133027E-13  0.3949953E-12 -0.7847117E-12 -0.3878087E+00  0.9167769E-16 -0.4204844E-12 -0.3070854E+00 -0.1259703E-15
  0.3890379E-12  0.1296804E-15  0.1898080E-15 -0.1720719E-14 -0.1128502E+00 -0.4338076E-13 -0.1609845E+01  0.1468783E+01
  0.7759868E-01  0.1210108E-15  0.1590354E+00  0.1609845E+01 -0.1468783E+01  0.7759868E-01 -0.6645719E-16 -0.1590354E+00
  eigenvectors of nos in ao-basis, column   10
  0.6009755E-01  0.4905569E+00 -0.5872575E+00  0.1274610E-12 -0.4148646E-16 -0.4708518E+00  0.3521379E-12  0.3190314E-16
  0.2280175E+00  0.1095879E-15 -0.7270697E-16  0.1275336E-03  0.9135192E-13 -0.5458997E-01 -0.1328241E+01  0.1203349E+01
  0.3054664E+00  0.3029621E-16  0.5596935E-01 -0.1328241E+01  0.1203349E+01 -0.3054664E+00  0.4008870E-17  0.5596935E-01
  eigenvectors of nos in ao-basis, column   11
 -0.1171771E-02 -0.2149045E+00 -0.6662232E+00  0.4008957E-12  0.1272162E-15 -0.1100021E+01 -0.9162349E-12 -0.3235579E-15
  0.1802690E+01  0.4183721E-17 -0.8150565E-16 -0.3135894E-02  0.4076522E-13  0.2248781E-02  0.9562774E+00 -0.3614770E+00
  0.9392462E-01  0.3124207E-15  0.2398654E+00  0.9562774E+00 -0.3614770E+00 -0.9392462E-01  0.7696705E-16  0.2398654E+00
  eigenvectors of nos in ao-basis, column   12
 -0.1865057E-16 -0.1137790E-16  0.4372914E-15  0.2311638E-15  0.1409556E+01 -0.1502495E-16 -0.2137865E-17 -0.1685359E+01
 -0.2644028E-15  0.2619468E-14 -0.4378665E-02 -0.2227742E-16  0.2920819E-16  0.1910002E-16 -0.2112112E-15  0.1351552E-15
 -0.2569473E-18  0.5475679E-02  0.6083716E-17 -0.6046545E-15  0.1695000E-15  0.2264732E-15  0.5475679E-02 -0.1656291E-15
  eigenvectors of nos in ao-basis, column   13
  0.6769483E-13  0.1301094E-12 -0.7410199E-12 -0.1057726E+01 -0.1337786E-16 -0.2734863E-12  0.2269170E+01 -0.3776348E-16
  0.4949036E-12  0.2616358E-18  0.1265581E-15 -0.5399661E-14 -0.4799202E-01  0.1756388E-14  0.6650249E+00  0.5106979E+00
  0.3041767E+00  0.4995213E-16  0.1883073E+00 -0.6650249E+00 -0.5106979E+00  0.3041767E+00  0.8462649E-16 -0.1883073E+00
  eigenvectors of nos in ao-basis, column   14
  0.2091554E+00  0.1071580E+01 -0.1181893E+01  0.2281662E-12  0.3799142E-16  0.1763751E+00 -0.4822797E-12  0.1095098E-14
 -0.7970975E+00 -0.1337744E-15  0.5119991E-16 -0.3282162E-01  0.3109784E-13 -0.1255569E-03 -0.5377055E+00  0.4909475E+00
 -0.3086257E+00 -0.2410793E-17  0.5610314E+00 -0.5377055E+00  0.4909475E+00  0.3086257E+00 -0.2622459E-14  0.5610314E+00
  eigenvectors of nos in ao-basis, column   15
 -0.2948458E-15 -0.1543599E-14  0.1780871E-14  0.9962454E-16 -0.1039696E-13 -0.2101302E-15  0.1481156E-15  0.4657262E-13
  0.1318351E-14 -0.1289159E+00 -0.1874740E-13  0.5522893E-16  0.1935210E-16  0.1444038E-17  0.1106991E-14 -0.8594874E-15
  0.5663967E-15  0.6862173E+00 -0.1023255E-14  0.6499074E-15 -0.7245662E-15 -0.5506128E-15 -0.6862173E+00 -0.1231443E-14
  eigenvectors of nos in ao-basis, column   16
  0.1124007E-14  0.5190615E-14 -0.6901306E-14 -0.8661637E-16 -0.6688636E-01  0.7454745E-15  0.3311698E-16 -0.5949782E+00
 -0.7786919E-15  0.3482762E-13 -0.1598336E+00 -0.3942046E-16 -0.9962211E-17  0.5635439E-16  0.4204281E-15  0.4277037E-15
 -0.2403051E-15  0.7710122E+00  0.7735285E-15  0.5615269E-15  0.4375273E-15  0.5413873E-16  0.7710122E+00  0.6927030E-15
  eigenvectors of nos in ao-basis, column   17
 -0.7152723E+00 -0.3091336E+01  0.4825988E+01 -0.3740154E-13 -0.1754659E-15 -0.1908556E+00 -0.1542837E-11 -0.5249664E-15
 -0.8798870E+00 -0.3829584E-16 -0.1404789E-15 -0.3319167E-01 -0.4320577E-13 -0.5345980E-01 -0.1438511E+01  0.3295761E+00
 -0.3798899E+00  0.8949531E-15  0.3789287E-01 -0.1438511E+01  0.3295761E+00  0.3798899E+00  0.7857435E-15  0.3789287E-01
  eigenvectors of nos in ao-basis, column   18
  0.1254386E-11  0.5561242E-11 -0.8613367E-11  0.1732794E-01 -0.1896560E-17  0.4631480E-12 -0.9280177E+00 -0.1893239E-16
  0.1418057E-11 -0.2085640E-15 -0.1692849E-15  0.4726852E-13 -0.3028578E-01  0.7661613E-13 -0.6786853E+00  0.2460613E+00
  0.4823549E+00  0.1561555E-16 -0.6903832E+00  0.6786853E+00 -0.2460613E+00  0.4823549E+00  0.1887728E-16  0.6903832E+00
  eigenvectors of nos in ao-basis, column   19
  0.2629732E-12  0.1389613E-11  0.1945139E-12  0.1251840E+01 -0.2292075E-15 -0.1022185E-11 -0.4128890E+00  0.2467346E-15
  0.2119138E-12  0.1032580E-15 -0.4718214E-16 -0.2675803E-13  0.1472896E+00 -0.1247686E-12  0.5466282E+00  0.5377930E-01
  0.7276124E+00  0.1203127E-16  0.5654252E+00 -0.5466282E+00 -0.5377930E-01  0.7276124E+00  0.4511230E-16 -0.5654252E+00
  eigenvectors of nos in ao-basis, column   20
 -0.3068618E+00 -0.1538593E+01  0.1120258E+00  0.1321128E-11  0.1044481E-15  0.9807091E+00 -0.4518524E-12 -0.6503741E-16
 -0.2798825E+00 -0.1478313E-16 -0.3504715E-15  0.1641279E-01  0.1596852E-12  0.1132260E+00  0.8363142E+00 -0.2663892E+00
  0.7517673E+00 -0.3931772E-16  0.5305955E+00  0.8363142E+00 -0.2663892E+00 -0.7517673E+00 -0.1275737E-15  0.5305955E+00
  eigenvectors of nos in ao-basis, column   21
  0.2164986E-16  0.1198196E-16 -0.1021751E-14  0.2353443E-15  0.7240906E-02  0.3682210E-15 -0.3947190E-15 -0.3229147E+00
  0.2052502E-15  0.1822167E-12  0.1045714E+01 -0.1759278E-15  0.2730361E-16  0.2510648E-16  0.7780825E-15 -0.4884518E-15
  0.5190693E-15  0.4066771E+00  0.3040973E-16  0.5472104E-15  0.1565809E-15 -0.4277323E-15  0.4066771E+00  0.2941472E-15
  eigenvectors of nos in ao-basis, column   22
  0.1050887E-15  0.2357625E-15 -0.1977831E-14 -0.6737873E-16 -0.4970292E-14  0.5004176E-15 -0.9749280E-16  0.8383453E-13
  0.8871752E-15  0.1072280E+01 -0.1747192E-12 -0.2994074E-15 -0.3474570E-17 -0.3470492E-15  0.2216376E-14 -0.1323823E-14
  0.7823238E-15  0.3811176E+00 -0.1835534E-15  0.2143708E-14 -0.9715350E-15 -0.7529339E-15 -0.3811176E+00  0.2083587E-15
  eigenvectors of nos in ao-basis, column   23
  0.8598546E-01  0.3028774E+00 -0.8098987E+00 -0.2434517E-13 -0.8213752E-16  0.5946500E-01  0.7280752E-13  0.3190450E-15
  0.5469864E+00 -0.9636920E-15 -0.5635709E-15 -0.3159243E+00 -0.2776325E-13  0.6761892E-01  0.5941891E+00 -0.2522491E+00
  0.3743832E+00 -0.6404347E-15 -0.3122271E+00  0.5941891E+00 -0.2522491E+00 -0.3743832E+00  0.8279087E-16 -0.3122271E+00
  eigenvectors of nos in ao-basis, column   24
 -0.1166195E+00 -0.2416777E+00  0.2434799E+01 -0.1887791E-13 -0.7061382E-16 -0.6217072E+00 -0.4086266E-13  0.5771820E-16
 -0.6211116E+00  0.7576342E-15  0.8430511E-16  0.2467611E-01  0.3836063E-13  0.5861624E+00 -0.2169264E+01  0.8956706E+00
 -0.6236886E+00  0.2976308E-15 -0.5088806E+00 -0.2169264E+01  0.8956706E+00  0.6236886E+00 -0.2294555E-15 -0.5088806E+00


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***


 cosurf_and_phi
  2.466315385316 -0.061539892486  2.837440149000 -0.034370604262
  0.006359863210 -2.199287675687  3.114544749440 -0.062933376365
 -0.480526931872  1.061509344774  3.755560521090 -0.064148417365
  1.262739748330  2.872870034186  1.567866969040 -0.042635124542
  1.897949994968 -2.634366215369  0.570962659525 -0.015380963593
 -2.790166444607 -0.824331206483  2.170430257018 -0.019321468883
 -2.061296060778  2.493528161233  1.034251636268 -0.020819237183
 -0.002991703587  2.420846177436 -1.447622679829  0.011948642117
 -1.114435211431 -2.949559801670 -0.067898604227 -0.016077390874
  1.955709175590 -0.984462672186  3.123501329999 -0.046406945930
  1.000900154469 -1.706149424342  3.300403738319 -0.059492387183
  1.652947872482  0.360731428865  3.496571379926 -0.052788977835
  0.594940552015  0.670555958619  3.845537291721 -0.063791583883
  2.390078334690  1.202722009900  2.566708449184 -0.033900743429
  1.989334673621  2.229216060677  2.001028520754 -0.036548139281
  2.919143888229  0.378144896597  2.099775822683 -0.014391344019
  2.780367109803 -0.921046401167  2.130496470151 -0.019086870536
  2.449941849009 -2.011800733381  1.439000205618 -0.018021977970
 -0.231540571447 -1.263621120083  3.706953994699 -0.064793919300
 -0.352106389130 -0.107618694495  3.950679056227 -0.064905503156
  0.105380882100  1.878884201281  3.371423292662 -0.063982836097
  0.783266721831  2.590884380442  2.520834408126 -0.056610004193
  2.011402526412  2.551496219550  0.814855178005 -0.017769143502
  1.440220538007 -2.843720084632  1.356653310131 -0.037509340940
  0.871209499702 -2.660587439486  2.372618950166 -0.054793133390
 -2.947488997374  0.298617382934  2.058332654596 -0.012862348920
 -2.674954162172  1.563027715274  1.704200253745 -0.015931714437
 -1.353448939749  2.910920816403  0.212056013565 -0.018613926005
 -0.743422400795  2.810699746106 -0.731960510989 -0.004323943353
  0.099626804209  1.855073908563 -1.945822518898  0.029601871749
  0.019900458911 -1.968682238442 -1.864952523830  0.026388425644
 -0.601526683996 -2.669374282549 -1.032942810695  0.002601915334
 -1.976701730993 -2.578422698078  0.816296596419 -0.018629331230
 -2.525639241426 -1.850752473194  1.593331825886 -0.018492509989
 -1.124962231191 -1.881571875050  3.121019991266 -0.057552898647
 -2.117398618237 -1.513942036836  2.667866282109 -0.040465303694
 -2.336046769995 -0.152318885910  2.976109389266 -0.038215813431
 -1.478775916646  0.469153703894  3.577445396081 -0.055391145912
 -1.328587214463  1.668007282102  3.174271253825 -0.055657843358
 -1.771761195387  2.287385908353  2.202255939279 -0.042540089613
 -1.026730149698  3.017318998523  1.358608629268 -0.042473317530
  0.119020599620  3.173161356543  1.415159765853 -0.048074223322
  0.915237473358  3.108118624501  0.463299650838 -0.029353214118
  0.462814589486  2.837210699514 -0.795516582628 -0.005197620869
  1.026812912753 -3.017319139618  0.083991013862 -0.020597445718
 -0.070059393274 -3.176418651270  0.035610954337 -0.025715677406
 -0.970268948715 -3.083313212042  1.062474740253 -0.038809338274
 -0.534203029846 -2.828286137107  2.231267851728 -0.055654832963
  0.871897873699 -0.575451888002  3.799144800425 -0.061994639249
  1.408719892640  1.640288870679  3.148120355694 -0.054583395021
  2.650053602975  1.633766196698  1.655441764425 -0.015879240488
  1.964286464189 -2.001027831890  2.365093852582 -0.040762147359
 -1.342877128538 -0.760711330777  3.581800927521 -0.057046417880
 -0.531365927285  2.661712102822  2.509437292848 -0.057962221475
  1.142904167226  2.885766749848 -0.243475252462 -0.011486698850
  0.342129526197 -3.189261637688  1.246854200824 -0.045388289319
 -2.346459445268  1.135413320199  2.662806558936 -0.035693232547
 -0.258565605057  3.205542092831  0.249849913336 -0.029543194065
  0.450566961335 -2.699565911226 -1.032013446782  0.001225636567
 -1.684533476150 -2.430522251364  2.070179818920 -0.042598308929
 -3.368784049130  0.036613212616 -1.854972615293  0.058700004553
 -3.278396927799  1.580834404538 -0.078603229040  0.032623887513
 -3.656110378332 -0.713812229513  0.361831391088  0.033722006190
 -2.408052717333 -2.075622947728 -1.226189024363  0.036814251624
 -1.295829484895 -0.567704086865 -2.747607986898  0.054759631720
 -1.846557165647  1.681693338816 -2.099716493181  0.045977223701
 -3.831862607217  0.357468890277 -0.654813288033  0.049463690965
 -3.527736967644 -1.045671231370 -1.064852892071  0.050344088398
 -2.622906831544 -1.024738705101 -2.241124222290  0.055870497229
 -2.402781990555  0.378472303440 -2.579763034114  0.058853455544
 -3.126606927219  1.313275381963 -1.541857021673  0.051509509531
 -3.633182667875  0.531520753705  0.561856665829  0.030800813749
 -3.086813000261 -1.794874586082 -0.179700355757  0.030594537327
 -1.615880090830 -1.674393887320 -2.147504235692  0.044936561186
 -1.240258025012  0.765881087348 -2.687977655831  0.053488967638
 -2.430292517958  2.154437082790 -0.969924007583  0.032557462001
  3.368776503197 -0.036639040867 -1.854966842961  0.058700404880
  3.278392620256 -1.580850766330 -0.078589062660  0.032623186712
  3.656106873706  0.713798214804  0.361832640492  0.033722266754
  2.408046317685  2.075600470298 -1.226192756897  0.036815137409
  1.295820311657  0.567673501678 -2.747601655947  0.054760163553
  1.846549173548 -1.681720471299 -2.099699178989  0.045976594295
  3.831857249217 -0.357488322766 -0.654806650060  0.049463717170
  3.527730862121  1.045649613724 -1.064853177123  0.050344831858
  2.622898581638  1.024710819001 -2.241122746235  0.055871358329
  2.402773123306 -0.378501994157 -2.579753678917  0.058853673796
  3.126599952113 -1.313299541573 -1.541844004398  0.051509134312
  3.633179527908 -0.531533702443  0.561864593522  0.030800511732
  3.086808508398  1.794857685487 -0.179703829578  0.030595206142
  1.615872011596  1.674366500124 -2.147504385867  0.044937411492
  1.240248960489 -0.765911354740 -2.687964116774  0.053488827352
  2.430286585511 -2.154458194501 -0.969905238283  0.032556543067
  0.000006852884  1.035694667087 -2.361676372823  0.046979034298
 -0.298806015181  0.969607486526 -2.408809074493  0.048501242400
  0.298815704890  0.969607820141 -2.408807386530  0.048501318921
  0.000007656756 -1.035723195601 -2.361670853435  0.046978535694
 -0.298805262603 -0.969636498141 -2.408803907288  0.048500758230
  0.298816457468 -0.969636367899 -2.408802219325  0.048500852478
 -0.667712940797 -1.245186997899 -2.338574529312  0.046522342270
 -1.218112188165 -2.025138759161 -1.616566947615  0.032936789143
 -2.084175601108 -2.294008802021 -0.480477682822  0.019038748523
 -2.876631896395 -1.658047550445  0.559052047065  0.016971188367
 -3.282909221331 -0.368099862162  1.091996180628  0.019662927091
 -3.142757910518  1.067034798172  0.908143333087  0.018448454351
 -2.511458431963  2.081290260960  0.080011352455  0.016716562473
 -1.638016880752  2.274609499719 -1.065756198713  0.024575829877
 -0.866948462969  1.570740798135 -2.077229430584  0.042722811879
 -0.467915446486 -1.694563900014 -1.941501402531  0.035437689916
 -1.299753513329 -2.453901457341 -0.850306853788  0.010627894760
 -2.242033801688 -2.245340287831  0.385760998463  0.000604800965
 -2.923088754321 -1.151144046328  1.279154738758  0.003603462701
 -3.074287016292  0.397098864814  1.477489335582  0.005422302861
 -2.635990824421  1.788708515315  0.902534843950  0.001304837506
 -1.781079179779  2.474786487342 -0.218927030025  0.003408856753
 -0.846758459860  2.184720175398 -1.444553386477  0.021850296046
 -0.255852300976  1.360631011730 -2.219692209228  0.041864758849
  0.667723897920  1.245157743773 -2.338577856151  0.046523113973
  1.218123388571  2.025110412626 -1.616576841774  0.032937948849
  2.084186643139  2.293984793080 -0.480493513306  0.019040012419
  2.876642520254  1.658030225134  0.559035126479  0.016972220142
  3.282919570196  0.368088739237  1.091983758387  0.019663385996
  3.142768358070 -1.067043033853  0.908139383421  0.018448361562
  2.511469270857 -2.081300494445  0.080016452498  0.016716167673
  1.638028059662 -2.274626132712 -1.065745290088  0.024575234678
  0.866959534092 -1.570765766224 -2.077218589767  0.042722258984
  0.467932162408  1.694535407938 -1.941510815499  0.035438633029
  1.299770347024  2.453875604722 -0.850324284820  0.010629136127
  2.242050270258  2.245320705184  0.385739526123  0.000606082864
  2.923104801922  1.151131828431  1.279135167181  0.003604423008
  3.074302957696 -0.397105856761  1.477477125466  0.005422740189
  2.636007061705 -1.788714946455  0.902532611959  0.001304993646
  1.781095866662 -2.474797662024 -0.218920775640  0.003408775380
  0.846775315928 -2.184739733041 -1.444543821640  0.021849845672
  0.255868904327 -1.360657816862 -2.219684436601  0.041864253161
  0.427045418766 -0.580992742084 -2.585105089006  0.052119483206
  0.427044967836  0.580963354323 -2.585108185092  0.052119763747
 -0.427035838172 -0.000014913339 -2.543076305315  0.056973507105
 end_of_phi


 nsubv after electrostatic potential =  0

  Confirmation of dielectric energy for ground state
  edielnew =   0.


 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************


 cosurf_and_qcos_from cosmo(3
  2.466315385316 -0.061539892486  2.837440149000  0.002077140923
  0.006359863210 -2.199287675687  3.114544749440  0.005886102891
 -0.480526931872  1.061509344774  3.755560521090  0.005343431650
  1.262739748330  2.872870034186  1.567866969040  0.004810827954
  1.897949994968 -2.634366215369  0.570962659525  0.001824604073
 -2.790166444607 -0.824331206483  2.170430257018  0.000891852477
 -2.061296060778  2.493528161233  1.034251636268  0.002780625884
 -0.002991703587  2.420846177436 -1.447622679829  0.000942404166
 -1.114435211431 -2.949559801670 -0.067898604227  0.002957344011
  1.955709175590 -0.984462672186  3.123501329999  0.004303493256
  1.000900154469 -1.706149424342  3.300403738319  0.005709657447
  1.652947872482  0.360731428865  3.496571379926  0.004824755304
  0.594940552015  0.670555958619  3.845537291721  0.005213072630
  2.390078334690  1.202722009900  2.566708449184  0.003136366764
  1.989334673621  2.229216060677  2.001028520754  0.003276569962
  2.919143888229  0.378144896597  2.099775822683  0.000362185917
  2.780367109803 -0.921046401167  2.130496470151  0.001299817186
  2.449941849009 -2.011800733381  1.439000205618  0.001784061155
 -0.231540571447 -1.263621120083  3.706953994699  0.006080026411
 -0.352106389130 -0.107618694495  3.950679056227  0.005825527335
  0.105380882100  1.878884201281  3.371423292662  0.007583514818
  0.783266721831  2.590884380442  2.520834408126  0.006804894924
  2.011402526412  2.551496219550  0.814855178005  0.002321750227
  1.440220538007 -2.843720084632  1.356653310131  0.004418526225
  0.871209499702 -2.660587439486  2.372618950166  0.007350481653
 -2.947488997374  0.298617382934  2.058332654596  0.000344890719
 -2.674954162172  1.563027715274  1.704200253745  0.001057208535
 -1.353448939749  2.910920816403  0.212056013565  0.003277208180
 -0.743422400795  2.810699746106 -0.731960510989  0.002320944059
  0.099626804209  1.855073908563 -1.945822518898 -0.000034664968
  0.019900458911 -1.968682238442 -1.864952523830 -0.000164829984
 -0.601526683996 -2.669374282549 -1.032942810695  0.001438213030
 -1.976701730993 -2.578422698078  0.816296596419  0.002478056129
 -2.525639241426 -1.850752473194  1.593331825886  0.001586154715
 -1.124962231191 -1.881571875050  3.121019991266  0.005627637494
 -2.117398618237 -1.513942036836  2.667866282109  0.003675603509
 -2.336046769995 -0.152318885910  2.976109389266  0.003739117315
 -1.478775916646  0.469153703894  3.577445396081  0.004767281819
 -1.328587214463  1.668007282102  3.174271253825  0.005749609935
 -1.771761195387  2.287385908353  2.202255939279  0.005523478927
 -1.026730149698  3.017318998523  1.358608629268  0.005533754659
  0.119020599620  3.173161356543  1.415159765853  0.006805939132
  0.915237473358  3.108118624501  0.463299650838  0.004775203766
  0.462814589486  2.837210699514 -0.795516582628  0.002413969479
  1.026812912753 -3.017319139618  0.083991013862  0.004040918572
 -0.070059393274 -3.176418651270  0.035610954337  0.004601054555
 -0.970268948715 -3.083313212042  1.062474740253  0.006367477508
 -0.534203029846 -2.828286137107  2.231267851728  0.007644016918
  0.871897873699 -0.575451888002  3.799144800425  0.006059799258
  1.408719892640  1.640288870679  3.148120355694  0.006574562377
  2.650053602975  1.633766196698  1.655441764425  0.000994969243
  1.964286464189 -2.001027831890  2.365093852582  0.004450768811
 -1.342877128538 -0.760711330777  3.581800927521  0.004977287367
 -0.531365927285  2.661712102822  2.509437292848  0.007677209227
  1.142904167226  2.885766749848 -0.243475252462  0.001414745224
  0.342129526197 -3.189261637688  1.246854200824  0.006634039931
 -2.346459445268  1.135413320199  2.662806558936  0.003234585825
 -0.258565605057  3.205542092831  0.249849913336  0.004410828882
  0.450566961335 -2.699565911226 -1.032013446782  0.002504483837
 -1.684533476150 -2.430522251364  2.070179818920  0.004745537968
 -3.368784049130  0.036613212616 -1.854972615293 -0.007557421010
 -3.278396927799  1.580834404538 -0.078603229040 -0.005581037803
 -3.656110378332 -0.713812229513  0.361831391088 -0.006306411298
 -2.408052717333 -2.075622947728 -1.226189024363 -0.005148558275
 -1.295829484895 -0.567704086865 -2.747607986898 -0.005527781711
 -1.846557165647  1.681693338816 -2.099716493181 -0.005735737583
 -3.831862607217  0.357468890277 -0.654813288033 -0.008196962323
 -3.527736967644 -1.045671231370 -1.064852892071 -0.008080312575
 -2.622906831544 -1.024738705101 -2.241124222290 -0.007916206506
 -2.402781990555  0.378472303440 -2.579763034114 -0.007684768672
 -3.126606927219  1.313275381963 -1.541857021673 -0.008184080278
 -3.633182667875  0.531520753705  0.561856665829 -0.004431846792
 -3.086813000261 -1.794874586082 -0.179700355757 -0.004352372974
 -1.615880090830 -1.674393887320 -2.147504235692 -0.004339147011
 -1.240258025012  0.765881087348 -2.687977655831 -0.004539252871
 -2.430292517958  2.154437082790 -0.969924007583 -0.003880542196
  3.368776503197 -0.036639040867 -1.854966842961 -0.007563511268
  3.278392620256 -1.580850766330 -0.078589062660 -0.005567179534
  3.656106873706  0.713798214804  0.361832640492 -0.006301657824
  2.408046317685  2.075600470298 -1.226192756897 -0.005156567393
  1.295820311657  0.567673501678 -2.747601655947 -0.005915185537
  1.846549173548 -1.681720471299 -2.099699178989 -0.005693548432
  3.831857249217 -0.357488322766 -0.654806650060 -0.008196882697
  3.527730862121  1.045649613724 -1.064853177123 -0.008083759097
  2.622898581638  1.024710819001 -2.241122746235 -0.007904117440
  2.402773123306 -0.378501994157 -2.579753678917 -0.007676144450
  3.126599952113 -1.313299541573 -1.541844004398 -0.008188508547
  3.633179527908 -0.531533702443  0.561864593522 -0.004427815261
  3.086808508398  1.794857685487 -0.179703829578 -0.004350159929
  1.615872011596  1.674366500124 -2.147504385867 -0.004309960377
  1.240248960489 -0.765911354740 -2.687964116774 -0.004778346333
  2.430286585511 -2.154458194501 -0.969905238283 -0.003866470910
  0.000006852884  1.035694667087 -2.361676372823 -0.000239007612
 -0.298806015181  0.969607486526 -2.408809074493 -0.000293708784
  0.298815704890  0.969607820141 -2.408807386530 -0.000214463779
  0.000007656756 -1.035723195601 -2.361670853435 -0.000233904303
 -0.298805262603 -0.969636498141 -2.408803907288 -0.000279855259
  0.298816457468 -0.969636367899 -2.408802219325 -0.000212921335
 -0.667712940797 -1.245186997899 -2.338574529312 -0.000825090617
 -1.218112188165 -2.025138759161 -1.616566947615 -0.001238820868
 -2.084175601108 -2.294008802021 -0.480477682822 -0.000844228118
 -2.876631896395 -1.658047550445  0.559052047065 -0.001317432647
 -3.282909221331 -0.368099862162  1.091996180628 -0.001790315553
 -3.142757910518  1.067034798172  0.908143333087 -0.001592383423
 -2.511458431963  2.081290260960  0.080011352455 -0.001097655629
 -1.638016880752  2.274609499719 -1.065756198713 -0.000994588871
 -0.866948462969  1.570740798135 -2.077229430584 -0.001724722506
 -0.467915446486 -1.694563900014 -1.941501402531 -0.001171879374
 -1.299753513329 -2.453901457341 -0.850306853788 -0.000117484767
 -2.242033801688 -2.245340287831  0.385760998463 -0.000257669475
 -2.923088754321 -1.151144046328  1.279154738758 -0.000890784280
 -3.074287016292  0.397098864814  1.477489335582 -0.001169906361
 -2.635990824421  1.788708515315  0.902534843950 -0.000502851595
 -1.781079179779  2.474786487342 -0.218927030025 -0.000093859691
 -0.846758459860  2.184720175398 -1.444553386477 -0.000652386112
 -0.255852300976  1.360631011730 -2.219692209228 -0.000759990947
  0.667723897920  1.245157743773 -2.338577856151 -0.000766674937
  1.218123388571  2.025110412626 -1.616576841774 -0.001250815474
  2.084186643139  2.293984793080 -0.480493513306 -0.000822902715
  2.876642520254  1.658030225134  0.559035126479 -0.001312713855
  3.282919570196  0.368088739237  1.091983758387 -0.001778970947
  3.142768358070 -1.067043033853  0.908139383421 -0.001586275692
  2.511469270857 -2.081300494445  0.080016452498 -0.001040080399
  1.638028059662 -2.274626132712 -1.065745290088 -0.000951834357
  0.866959534092 -1.570765766224 -2.077218589767 -0.001680426803
  0.467932162408  1.694535407938 -1.941510815499 -0.001228745758
  1.299770347024  2.453875604722 -0.850324284820 -0.000121322384
  2.242050270258  2.245320705184  0.385739526123 -0.000190285053
  2.923104801922  1.151131828431  1.279135167181 -0.000915127196
  3.074302957696 -0.397105856761  1.477477125466 -0.001199228329
  2.636007061705 -1.788714946455  0.902532611959 -0.000545964278
  1.781095866662 -2.474797662024 -0.218920775640 -0.000145564759
  0.846775315928 -2.184739733041 -1.444543821640 -0.000708902932
  0.255868904327 -1.360657816862 -2.219684436601 -0.000707982396
  0.427045418766 -0.580992742084 -2.585105089006 -0.001930768153
  0.427044967836  0.580963354323 -2.585108185092 -0.001810762163
 -0.427035838172 -0.000014913339 -2.543076305315 -0.005292540966
 end_of_qcos

 sum of the negative charges =  -0.240145551

 sum of the positive charges =   0.235255592

 total sum =  -0.00488995917

 *** qcos is copied to qcosdalton ***

 fepsi =   1.
 *** qcosdalton is mult by fepsi ***
 cosurf and qcosdalton
  2.466315385316 -0.061539892486  2.837440149000  0.002077140923
  0.006359863210 -2.199287675687  3.114544749440  0.005886102891
 -0.480526931872  1.061509344774  3.755560521090  0.005343431650
  1.262739748330  2.872870034186  1.567866969040  0.004810827954
  1.897949994968 -2.634366215369  0.570962659525  0.001824604073
 -2.790166444607 -0.824331206483  2.170430257018  0.000891852477
 -2.061296060778  2.493528161233  1.034251636268  0.002780625884
 -0.002991703587  2.420846177436 -1.447622679829  0.000942404166
 -1.114435211431 -2.949559801670 -0.067898604227  0.002957344011
  1.955709175590 -0.984462672186  3.123501329999  0.004303493256
  1.000900154469 -1.706149424342  3.300403738319  0.005709657447
  1.652947872482  0.360731428865  3.496571379926  0.004824755304
  0.594940552015  0.670555958619  3.845537291721  0.005213072630
  2.390078334690  1.202722009900  2.566708449184  0.003136366764
  1.989334673621  2.229216060677  2.001028520754  0.003276569962
  2.919143888229  0.378144896597  2.099775822683  0.000362185917
  2.780367109803 -0.921046401167  2.130496470151  0.001299817186
  2.449941849009 -2.011800733381  1.439000205618  0.001784061155
 -0.231540571447 -1.263621120083  3.706953994699  0.006080026411
 -0.352106389130 -0.107618694495  3.950679056227  0.005825527335
  0.105380882100  1.878884201281  3.371423292662  0.007583514818
  0.783266721831  2.590884380442  2.520834408126  0.006804894924
  2.011402526412  2.551496219550  0.814855178005  0.002321750227
  1.440220538007 -2.843720084632  1.356653310131  0.004418526225
  0.871209499702 -2.660587439486  2.372618950166  0.007350481653
 -2.947488997374  0.298617382934  2.058332654596  0.000344890719
 -2.674954162172  1.563027715274  1.704200253745  0.001057208535
 -1.353448939749  2.910920816403  0.212056013565  0.003277208180
 -0.743422400795  2.810699746106 -0.731960510989  0.002320944059
  0.099626804209  1.855073908563 -1.945822518898 -0.000034664968
  0.019900458911 -1.968682238442 -1.864952523830 -0.000164829984
 -0.601526683996 -2.669374282549 -1.032942810695  0.001438213030
 -1.976701730993 -2.578422698078  0.816296596419  0.002478056129
 -2.525639241426 -1.850752473194  1.593331825886  0.001586154715
 -1.124962231191 -1.881571875050  3.121019991266  0.005627637494
 -2.117398618237 -1.513942036836  2.667866282109  0.003675603509
 -2.336046769995 -0.152318885910  2.976109389266  0.003739117315
 -1.478775916646  0.469153703894  3.577445396081  0.004767281819
 -1.328587214463  1.668007282102  3.174271253825  0.005749609935
 -1.771761195387  2.287385908353  2.202255939279  0.005523478927
 -1.026730149698  3.017318998523  1.358608629268  0.005533754659
  0.119020599620  3.173161356543  1.415159765853  0.006805939132
  0.915237473358  3.108118624501  0.463299650838  0.004775203766
  0.462814589486  2.837210699514 -0.795516582628  0.002413969479
  1.026812912753 -3.017319139618  0.083991013862  0.004040918572
 -0.070059393274 -3.176418651270  0.035610954337  0.004601054555
 -0.970268948715 -3.083313212042  1.062474740253  0.006367477508
 -0.534203029846 -2.828286137107  2.231267851728  0.007644016918
  0.871897873699 -0.575451888002  3.799144800425  0.006059799258
  1.408719892640  1.640288870679  3.148120355694  0.006574562377
  2.650053602975  1.633766196698  1.655441764425  0.000994969243
  1.964286464189 -2.001027831890  2.365093852582  0.004450768811
 -1.342877128538 -0.760711330777  3.581800927521  0.004977287367
 -0.531365927285  2.661712102822  2.509437292848  0.007677209227
  1.142904167226  2.885766749848 -0.243475252462  0.001414745224
  0.342129526197 -3.189261637688  1.246854200824  0.006634039931
 -2.346459445268  1.135413320199  2.662806558936  0.003234585825
 -0.258565605057  3.205542092831  0.249849913336  0.004410828882
  0.450566961335 -2.699565911226 -1.032013446782  0.002504483837
 -1.684533476150 -2.430522251364  2.070179818920  0.004745537968
 -3.368784049130  0.036613212616 -1.854972615293 -0.007557421010
 -3.278396927799  1.580834404538 -0.078603229040 -0.005581037803
 -3.656110378332 -0.713812229513  0.361831391088 -0.006306411298
 -2.408052717333 -2.075622947728 -1.226189024363 -0.005148558275
 -1.295829484895 -0.567704086865 -2.747607986898 -0.005527781711
 -1.846557165647  1.681693338816 -2.099716493181 -0.005735737583
 -3.831862607217  0.357468890277 -0.654813288033 -0.008196962323
 -3.527736967644 -1.045671231370 -1.064852892071 -0.008080312575
 -2.622906831544 -1.024738705101 -2.241124222290 -0.007916206506
 -2.402781990555  0.378472303440 -2.579763034114 -0.007684768672
 -3.126606927219  1.313275381963 -1.541857021673 -0.008184080278
 -3.633182667875  0.531520753705  0.561856665829 -0.004431846792
 -3.086813000261 -1.794874586082 -0.179700355757 -0.004352372974
 -1.615880090830 -1.674393887320 -2.147504235692 -0.004339147011
 -1.240258025012  0.765881087348 -2.687977655831 -0.004539252871
 -2.430292517958  2.154437082790 -0.969924007583 -0.003880542196
  3.368776503197 -0.036639040867 -1.854966842961 -0.007563511268
  3.278392620256 -1.580850766330 -0.078589062660 -0.005567179534
  3.656106873706  0.713798214804  0.361832640492 -0.006301657824
  2.408046317685  2.075600470298 -1.226192756897 -0.005156567393
  1.295820311657  0.567673501678 -2.747601655947 -0.005915185537
  1.846549173548 -1.681720471299 -2.099699178989 -0.005693548432
  3.831857249217 -0.357488322766 -0.654806650060 -0.008196882697
  3.527730862121  1.045649613724 -1.064853177123 -0.008083759097
  2.622898581638  1.024710819001 -2.241122746235 -0.007904117440
  2.402773123306 -0.378501994157 -2.579753678917 -0.007676144450
  3.126599952113 -1.313299541573 -1.541844004398 -0.008188508547
  3.633179527908 -0.531533702443  0.561864593522 -0.004427815261
  3.086808508398  1.794857685487 -0.179703829578 -0.004350159929
  1.615872011596  1.674366500124 -2.147504385867 -0.004309960377
  1.240248960489 -0.765911354740 -2.687964116774 -0.004778346333
  2.430286585511 -2.154458194501 -0.969905238283 -0.003866470910
  0.000006852884  1.035694667087 -2.361676372823 -0.000239007612
 -0.298806015181  0.969607486526 -2.408809074493 -0.000293708784
  0.298815704890  0.969607820141 -2.408807386530 -0.000214463779
  0.000007656756 -1.035723195601 -2.361670853435 -0.000233904303
 -0.298805262603 -0.969636498141 -2.408803907288 -0.000279855259
  0.298816457468 -0.969636367899 -2.408802219325 -0.000212921335
 -0.667712940797 -1.245186997899 -2.338574529312 -0.000825090617
 -1.218112188165 -2.025138759161 -1.616566947615 -0.001238820868
 -2.084175601108 -2.294008802021 -0.480477682822 -0.000844228118
 -2.876631896395 -1.658047550445  0.559052047065 -0.001317432647
 -3.282909221331 -0.368099862162  1.091996180628 -0.001790315553
 -3.142757910518  1.067034798172  0.908143333087 -0.001592383423
 -2.511458431963  2.081290260960  0.080011352455 -0.001097655629
 -1.638016880752  2.274609499719 -1.065756198713 -0.000994588871
 -0.866948462969  1.570740798135 -2.077229430584 -0.001724722506
 -0.467915446486 -1.694563900014 -1.941501402531 -0.001171879374
 -1.299753513329 -2.453901457341 -0.850306853788 -0.000117484767
 -2.242033801688 -2.245340287831  0.385760998463 -0.000257669475
 -2.923088754321 -1.151144046328  1.279154738758 -0.000890784280
 -3.074287016292  0.397098864814  1.477489335582 -0.001169906361
 -2.635990824421  1.788708515315  0.902534843950 -0.000502851595
 -1.781079179779  2.474786487342 -0.218927030025 -0.000093859691
 -0.846758459860  2.184720175398 -1.444553386477 -0.000652386112
 -0.255852300976  1.360631011730 -2.219692209228 -0.000759990947
  0.667723897920  1.245157743773 -2.338577856151 -0.000766674937
  1.218123388571  2.025110412626 -1.616576841774 -0.001250815474
  2.084186643139  2.293984793080 -0.480493513306 -0.000822902715
  2.876642520254  1.658030225134  0.559035126479 -0.001312713855
  3.282919570196  0.368088739237  1.091983758387 -0.001778970947
  3.142768358070 -1.067043033853  0.908139383421 -0.001586275692
  2.511469270857 -2.081300494445  0.080016452498 -0.001040080399
  1.638028059662 -2.274626132712 -1.065745290088 -0.000951834357
  0.866959534092 -1.570765766224 -2.077218589767 -0.001680426803
  0.467932162408  1.694535407938 -1.941510815499 -0.001228745758
  1.299770347024  2.453875604722 -0.850324284820 -0.000121322384
  2.242050270258  2.245320705184  0.385739526123 -0.000190285053
  2.923104801922  1.151131828431  1.279135167181 -0.000915127196
  3.074302957696 -0.397105856761  1.477477125466 -0.001199228329
  2.636007061705 -1.788714946455  0.902532611959 -0.000545964278
  1.781095866662 -2.474797662024 -0.218920775640 -0.000145564759
  0.846775315928 -2.184739733041 -1.444543821640 -0.000708902932
  0.255868904327 -1.360657816862 -2.219684436601 -0.000707982396
  0.427045418766 -0.580992742084 -2.585105089006 -0.001930768153
  0.427044967836  0.580963354323 -2.585108185092 -0.001810762163
 -0.427035838172 -0.000014913339 -2.543076305315 -0.005292540966
 end of cosurf and qcosdalton

 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 ** Total nuclear repulsion energy ** =   9.35969258317765
 screening nuclear repulsion energy   0.00672980141

 Total-screening nuclear repulsion energy   9.35296278


 Adding T+Vsolv ...
 maxdens  300
 *** End of DALTON-COSMO calculation ***

  original map vector  20 21 22 23 24 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17
 18 19 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0


          sorted Vsolv matrix  block   1

               nbf   1        nbf   2        nbf   3        nbf   4        nbf   5        nbf   6        nbf   7        nbf   8
  nbf   1    -3.31914420
  nbf   2     0.00001149    -3.09165276
  nbf   3     0.00001644    -0.98129809    -4.10225551
  nbf   4    -1.01838902     0.00000395     0.00001321    -4.07882880
  nbf   5    -0.42307970     0.00001113     0.00000877    -0.44094083    -4.75210498
  nbf   6    -0.00000456    -0.00000321     0.00000198     0.00000123     0.00000154    -5.34846983
  nbf   7    -0.00000445    -1.17036607    -0.38197374     0.00000625     0.00000665    -0.00000251    -4.33669666
  nbf   8    -0.41249016     0.00000856     0.00002461     0.03978337     0.09581695    -0.00000399    -0.00000434    -3.58739254
  nbf   9     0.00000251     0.00000076     0.00000160     0.00000112    -0.00000001    -0.00000179     0.00000072    -0.00000215
  nbf  10    -0.00000006     0.00000028     0.00000111     0.00000059     0.00000556     0.24713661     0.00000112    -0.00000059
  nbf  11     0.71247225     0.00000234     0.00000542    -0.06900573    -0.45060694     0.00000213     0.00000216     0.37418689
  nbf  12    -0.00001917    -0.02232513    -0.02659577    -0.00000043    -0.00003655     0.00000083    -0.10346762    -0.00003324
  nbf  13     0.00000918     0.51184494     1.29172158     0.00000746     0.00000411     0.00000168     1.13369988     0.00000376
  nbf  14     0.60219843     0.00000583     0.00000220     1.39132613     0.61428354     0.00000208     0.00000621     0.29798366
  nbf  15     0.00000094     0.00000000    -0.00000012     0.00000142     0.00000174     0.06480194     0.00000112     0.00000067
  nbf  16     0.00000136     0.00000164     0.00000250     0.00000043     0.00000033    -0.00000158    -0.00000024    -0.00000069
  nbf  17     0.01612412    -0.00000581    -0.00001223     0.28459188    -0.03476642     0.00000096     0.00000081    -0.91900258
  nbf  18    -0.11618280    -0.00000552    -0.00000171    -0.29344839    -0.90081959     0.00000035    -0.00000072    -0.23500848
  nbf  19    -0.00000575     0.16688253    -0.06182044     0.00000062    -0.00000076    -0.00000003     1.19156765    -0.00000660
  nbf  20     0.18301633     0.00000000    -0.00000004     0.16556699    -0.11845848    -0.00000041     0.00000003     0.20495675
  nbf  21    -0.99280126    -0.00000429    -0.00001407    -0.72366601     0.04776843     0.00000154     0.00000375    -0.60117844
  nbf  22     0.00000609     1.23621911     0.65592338     0.00000826     0.00000888    -0.00000222     1.45021768     0.00001087
  nbf  23     0.54070615     0.00000627     0.00001113     0.73124756     1.71697654     0.00000018     0.00000304    -0.42066359
  nbf  24     0.00000178     0.00000130    -0.00000138    -0.00000151     0.00000006    -2.13414477     0.00000145     0.00000213

               nbf   9        nbf  10        nbf  11        nbf  12        nbf  13        nbf  14        nbf  15        nbf  16
  nbf   9    -3.42330119
  nbf  10     0.00001544    -3.17820739
  nbf  11    -0.00000285    -0.00000176    -4.03664046
  nbf  12    -0.00000380     0.00000151    -0.00000703    -2.63324986
  nbf  13    -0.00000179    -0.00000166     0.00000077     0.09049476    -3.62338375
  nbf  14    -0.00000167    -0.00000147    -0.18644350     0.00000088     0.00001730    -3.66481183
  nbf  15     0.00000477     0.90716980    -0.00000062     0.00000062    -0.00000001     0.00000029    -3.66998914
  nbf  16     0.90196966     0.00000546    -0.00000034    -0.00000081    -0.00000062    -0.00000024     0.00000529    -3.63954921
  nbf  17     0.00000114     0.00000039    -0.42317798     0.00002009    -0.00000454     0.04521866    -0.00000006     0.00000153
  nbf  18     0.00000055    -0.00000140     0.56827758     0.00000849    -0.00000750    -0.46485052    -0.00000078     0.00000045
  nbf  19    -0.00000106    -0.00000056    -0.00000037    -0.08859802     0.44902125     0.00000677    -0.00000098    -0.00000003
  nbf  20    -0.00000001     0.00000003    -0.38282204     0.00000000     0.00000007    -0.26333522    -0.00000001     0.00000005
  nbf  21    -0.00000264    -0.00000146     1.08742287     0.00001202     0.00000194     0.59818017    -0.00000032     0.00000060
  nbf  22     0.00000336     0.00000201     0.00000545     0.01884590    -0.54187387     0.00000049     0.00000088    -0.00000140
  nbf  23     0.00000105     0.00000493     0.54848474    -0.00002353     0.00000186    -0.34225951    -0.00000260     0.00000110
  nbf  24     0.00000232     0.07788137    -0.00000187    -0.00000053    -0.00000112    -0.00000018     0.08127911     0.00000222

               nbf  17        nbf  18        nbf  19        nbf  20        nbf  21        nbf  22        nbf  23        nbf  24
  nbf  17    -3.38750818
  nbf  18    -0.05583230    -3.03509862
  nbf  19     0.00000544    -0.00000649    -2.89433126
  nbf  20     0.00966744     0.05945761    -0.00000019   -33.05174917
  nbf  21    -0.01750684    -0.18419566     0.00000123     0.58204752    -7.87312296
  nbf  22    -0.00000520     0.00000112    -0.45843066    -0.00000003    -0.00000711    -6.71622218
  nbf  23    -0.12262671     0.30572537    -0.00000404     0.18758237    -0.25069844     0.00001242    -6.95563276
  nbf  24    -0.00000081     0.00000128    -0.00000009    -0.00000037    -0.00000420     0.00000224    -0.00000106    -7.11691032
 insert_onel_diag: nmin2=   380

 onel.diag.
 -0.331914E+01-0.309165E+01-0.410226E+01-0.407883E+01-0.475210E+01-0.534847E+01-0.433670E+01-0.358739E+01-0.342330E+01-0.317821E+01
 -0.403664E+01-0.263325E+01-0.362338E+01-0.366481E+01-0.366999E+01-0.363955E+01-0.338751E+01-0.303510E+01-0.289433E+01-0.330517E+02
 -0.787312E+01-0.671622E+01-0.695563E+01-0.711691E+01

 i,start,strti,fin,filind,bufszi,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095         1       380       380

 4 external diag. modified integrals

  0.299711954 -3.3191442  0.362025844  0.213403987  0.294044049 -3.09165276
  0.345182252  0.318068021  0.34837146  0.293493931  0.497019376 -4.10225551
  0.382867967  0.309072536  0.341195502  0.316765031  0.62103433  0.362850338
  0.531836961 -4.0788288  0.349706612  0.299527054  0.308820027  0.298754537
  0.450260207  0.419223806  0.448476864  0.401601457  0.518535861 -4.75210498
  0.349531774  0.304524501  0.31278089  0.302655019  0.429312411  0.416826853
  0.436921903  0.413400849  0.530775438  0.48280831  0.60679891 -5.34846983
  0.33535564  0.293407704  0.341034277  0.276576801  0.455225842  0.389302194
  0.443351075  0.383054684  0.486303633  0.428587076  0.499742288  0.459023099
  0.475434104 -4.33669666  0.342526573  0.315362897  0.319917749  0.300731659
  0.504389096  0.410870033  0.47482656  0.425010382  0.519270202  0.400100848
  0.453053263  0.422413645  0.446074502  0.406547185  0.476405093 -3.58739254
  0.361060191  0.337659006  0.340604687  0.320198588  0.504515292  0.452245513
  0.527488651  0.469039301  0.434901697  0.420706511  0.454829088  0.417262113
  0.426620519  0.400804422  0.469386808  0.441599006  0.555764969 -3.42330119
  0.348856386  0.329501947  0.326880472  0.312634683  0.490451682  0.436629022
  0.509180653  0.444435002  0.4400708  0.419071921  0.459403052  0.417691971
  0.415311401  0.398901525  0.466192986  0.43088403  0.694750225  0.346946891
  0.501631969 -3.17820739  0.343227876  0.31280524  0.321132402  0.300850073
  0.455432102  0.418963752  0.451109274  0.419591702  0.534502331  0.438245352
  0.597905727  0.464004009  0.508095073  0.408470727  0.460615989  0.424227034
  0.469521607  0.441123033  0.462583019  0.438238584  0.525774157 -4.03664046
  0.356976431  0.333496982  0.334773679  0.315431831  0.522936179  0.430785245
  0.55704189  0.430105919  0.46546176  0.385379923  0.398185515  0.390667038
  0.424333725  0.380522726  0.592974188  0.354449455  0.516100041  0.471421051
  0.491982029  0.453400696  0.428233148  0.409923576  0.53092711 -2.63324986
  0.370351482  0.34231438  0.377526483  0.320334123  0.57663332  0.44254372
  0.563518308  0.477512966  0.511232086  0.469131823  0.508472502  0.490097443
  0.529535469  0.421869959  0.499545867  0.467575397  0.539123293  0.488956188
  0.512741012  0.475743005  0.515621907  0.477676678  0.512163042  0.474196449
  0.592144122 -3.62338375  0.388203856  0.340302294  0.365601016  0.328125926
  0.536368018  0.473948976  0.615526601  0.455378076  0.520967947  0.463416877
  0.546457699  0.489981129  0.48581785  0.455063687  0.509165453  0.46499119
  0.535052608  0.504064949  0.518029026  0.481875024  0.516561248  0.489811629
  0.517714869  0.482829445  0.717425907  0.445100321  0.607835731 -3.66481183
  0.361030445  0.336028491  0.330453775  0.322428351  0.486741558  0.469418774
  0.514189842  0.450314389  0.575178104  0.539226259  0.630438312  0.577562728
  0.510895782  0.499926663  0.503939385  0.490371469  0.52041881  0.467839603
  0.554043059  0.445274854  0.591832537  0.558697254  0.453957255  0.445032703
  0.568738744  0.548069735  0.596778585  0.560916765  0.750165186 -3.66998914
  0.353015389  0.338054232  0.358115278  0.317291888  0.522721553  0.462375435
  0.489534774  0.469144866  0.543456123  0.532600344  0.632201055  0.577477753
  0.547273656  0.513624874  0.507616587  0.483570727  0.564197636  0.455577875
  0.524876517  0.46718588  0.585160178  0.562784851  0.458775281  0.447244069
  0.587371194  0.556188564  0.58998874  0.568912261  0.721594827  0.645098881
  0.755127757 -3.63954921  0.366037972  0.335416332  0.343455146  0.320873443
  0.50940609  0.476326958  0.527676719  0.456821536  0.589439367  0.537772005
  0.579789349  0.559672288  0.53269644  0.49854365  0.567546289  0.469810852
  0.495610134  0.480892777  0.493939617  0.479062411  0.584343602  0.533457424
  0.502525388  0.442000941  0.582221799  0.560044775  0.596954899  0.56140811
  0.713528596  0.656785472  0.69947109  0.625487345  0.729807063 -3.38750818
  0.353987843  0.337616982  0.351789554  0.321182817  0.550334788  0.456688986
  0.522995786  0.46803517  0.566057852  0.507622395  0.615570104  0.560439732
  0.539018372  0.503813066  0.510463413  0.481771744  0.500103129  0.48786777
  0.496692123  0.477703001  0.585735173  0.540040646  0.469784387  0.453592071
  0.609978757  0.53850005  0.643846451  0.534124473  0.686162365  0.635949394
  0.700906311  0.666564985  0.679632027  0.617528828  0.696112735 -3.03509862
  0.358856446  0.335907395  0.354506012  0.319503646  0.548011068  0.469120392
  0.531509325  0.460743131  0.577501451  0.534350115  0.577624788  0.562928162
  0.587296462  0.4866605  0.514004213  0.4958057  0.493523583  0.479933652
  0.487740555  0.474497517  0.570979461  0.537487177  0.471960345  0.452931079
  0.634007692  0.54138526  0.615254278  0.550385293  0.691778305  0.63576614
  0.699509207  0.640665078  0.699234432  0.657364716  0.711993958  0.616407541
  0.721280492 -2.89433126
 i,start,strti,fin,filind,bufszi,nmbuf,nmin2, nd4ext = 
         1         1         2         2        12      4095         1       380       380

 all internall diag. modified integrals

  4.73971342 -33.0517492  1.04569563  0.0636788604  0.755179462 -7.87312296
  0.878477817  0.0193493095  0.682719812  0.155759544  0.668673763 -6.71622218
  0.986988872  0.0301374391  0.681192227  0.125736446  0.608457144
  0.0402117459  0.732528817 -6.95563276  1.03831833  0.0301199817  0.713926708
  0.128455235  0.624391224  0.0304166993  0.665957609  0.0433645062
  0.760228983 -7.11691032
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331914E+01-0.309165E+01-0.410226E+01-0.407883E+01-0.475210E+01-0.534847E+01-0.433670E+01-0.358739E+01-0.342330E+01-0.317821E+01
 -0.403664E+01-0.263325E+01-0.362338E+01-0.366481E+01-0.366999E+01-0.363955E+01-0.338751E+01-0.303510E+01-0.289433E+01-0.330517E+02
 -0.787312E+01-0.671622E+01-0.695563E+01-0.711691E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331914E+01-0.309165E+01-0.410226E+01-0.407883E+01-0.475210E+01-0.534847E+01-0.433670E+01-0.358739E+01-0.342330E+01-0.317821E+01
 -0.403664E+01-0.263325E+01-0.362338E+01-0.366481E+01-0.366999E+01-0.363955E+01-0.338751E+01-0.303510E+01-0.289433E+01-0.330517E+02
 -0.787312E+01-0.671622E+01-0.695563E+01-0.711691E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331914E+01-0.309165E+01-0.410226E+01-0.407883E+01-0.475210E+01-0.534847E+01-0.433670E+01-0.358739E+01-0.342330E+01-0.317821E+01
 -0.403664E+01-0.263325E+01-0.362338E+01-0.366481E+01-0.366999E+01-0.363955E+01-0.338751E+01-0.303510E+01-0.289433E+01-0.330517E+02
 -0.787312E+01-0.671622E+01-0.695563E+01-0.711691E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380
 DIAGEL COUNTS: DG0X,DG2X,DG4X= 440 145 30
 =========== Executing IN-CORE method ==========
 norm multnew:  1.
 threx: total number of loops= 14
 threx: total number of loops= 24
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331914E+01-0.309165E+01-0.410226E+01-0.407883E+01-0.475210E+01-0.534847E+01-0.433670E+01-0.358739E+01-0.342330E+01-0.317821E+01
 -0.403664E+01-0.263325E+01-0.362338E+01-0.366481E+01-0.366999E+01-0.363955E+01-0.338751E+01-0.303510E+01-0.289433E+01-0.330517E+02
 -0.787312E+01-0.671622E+01-0.695563E+01-0.711691E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331914E+01-0.309165E+01-0.410226E+01-0.407883E+01-0.475210E+01-0.534847E+01-0.433670E+01-0.358739E+01-0.342330E+01-0.317821E+01
 -0.403664E+01-0.263325E+01-0.362338E+01-0.366481E+01-0.366999E+01-0.363955E+01-0.338751E+01-0.303510E+01-0.289433E+01-0.330517E+02
 -0.787312E+01-0.671622E+01-0.695563E+01-0.711691E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331914E+01-0.309165E+01-0.410226E+01-0.407883E+01-0.475210E+01-0.534847E+01-0.433670E+01-0.358739E+01-0.342330E+01-0.317821E+01
 -0.403664E+01-0.263325E+01-0.362338E+01-0.366481E+01-0.366999E+01-0.363955E+01-0.338751E+01-0.303510E+01-0.289433E+01-0.330517E+02
 -0.787312E+01-0.671622E+01-0.695563E+01-0.711691E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        40 xx:       135 ww:       255
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        20 yw:        25

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        39
task #   9:       127    task #  10:       247    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 matrix-vector product formed for vector   1
 rtolcosmo =   0.001
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference-subspace vector overlap matrix  block   1

                ci   1
 ref:   1     1.00000000

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000
 subspace has dimension  1

          (h-repnuc*1) matrix in the subspace basis

                x:   1
   x:   1   -85.40090838

          overlap matrix in the subspace basis

                x:   1
   x:   1     1.00000000

          eigenvectors and eigenvalues in the subspace basis

                v:   1

   energy   -85.40090838

   x:   1     1.00000000

          eigenvectors and eigenvalues in the schmidt orthonormalized basis

                v:   1

   energy   -85.40090838

  zx:   1     1.00000000

          <ref|baseci> overlap matrix  block   1

                ci   1
 ref:   1     1.00000000

  write the transformed vectors back to cosmofile

 rtflw =  0
 freezing =  F

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -76.0479455968 -9.2970E+00  2.5188E-01  1.0015E+00  1.0000E-03


 total energy in cosmo calc 
e(rootcalc) + repnuc=     -76.0479455968
dielectric energy =      -0.0104563943
deltaediel =       0.0104563943
e(rootcalc) + repnuc - ediel =     -76.0374892025
e(rootcalc) + repnuc - edielnew =     -76.0479455968
deltaelast =      76.0374892025

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.01   0.00   0.00   0.01         0.    0.0000
    2   25    0   0.01   0.00   0.00   0.01         0.    0.0000
    3   26    0   0.02   0.00   0.00   0.02         0.    0.0000
    4   11    0   0.01   0.00   0.00   0.01         0.    0.0000
    5   15    0   0.02   0.00   0.00   0.02         0.    0.0000
    6   16    0   0.02   0.00   0.00   0.02         0.    0.0000
    7    1    0   0.01   0.00   0.00   0.01         0.    0.0000
    8    5    0   0.02   0.00   0.00   0.02         0.    0.0000
    9    6    0   0.02   0.00   0.00   0.02         0.    0.0000
   10    7    0   0.03   0.00   0.00   0.03         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.01   0.00   0.00   0.01         0.    0.0000
   13   46    0   0.01   0.00   0.00   0.01         0.    0.0000
   14   47    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.2000s 
time spent in multnx:                   0.2000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0300s 
total time per CI iteration:            3.4400s 

          starting ci iteration   2

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.35296278

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=      10  D0X=      30  D0W=      50
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================

          ci-one-electron density block   1

                mo   1         mo   2         mo   3         mo   4         mo   5         mo   6         mo   7         mo   8
   mo   1     2.00000000
   mo   2     0.00000000     2.00000000
   mo   3     0.00000000     0.00000000     2.00000000
   mo   4     0.00000000     0.00000000     0.00000000     2.00000000
   mo   5     0.00000000     0.00000000     0.00000000     0.00000000     2.00000000

                mo   9         mo  10         mo  11         mo  12         mo  13         mo  14         mo  15         mo  16

                mo  17         mo  18         mo  19         mo  20         mo  21         mo  22         mo  23         mo  24


*****   symmetry block SYM1   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       2.00000000
       2       0.00000000     2.00000000
       3       0.00000000     0.00000000     2.00000000
       4       0.00000000     0.00000000     0.00000000     2.00000000

               Column   5     Column   6     Column   7     Column   8
       5       2.00000000

               Column   9     Column  10     Column  11     Column  12

               Column  13     Column  14     Column  15     Column  16

               Column  17     Column  18     Column  19     Column  20

               Column  21     Column  22     Column  23     Column  24

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7        eno   8
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    8   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno   9        eno  10        eno  11        eno  12        eno  13        eno  14        eno  15        eno  16
 emo    9   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   10   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   11   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   12   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   13   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   14   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   15   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   16   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno  17        eno  18        eno  19        eno  20        eno  21        eno  22        eno  23        eno  24
 emo   17   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   18   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   19   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   20   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   21   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   22   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   23   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   24   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   2.0000000      2.0000000      2.0000000      2.0000000      2.0000000
   0.0000000      0.0000000      0.0000000      0.0000000      0.0000000
   0.0000000      0.0000000      0.0000000      0.0000000      0.0000000
   0.0000000      0.0000000      0.0000000      0.0000000      0.0000000
   0.0000000      0.0000000      0.0000000      0.0000000

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7         no   8
  mo    1   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo    2   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo    3   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
  mo    5   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
  mo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01
  mo   24   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00

                no   9         no  10         no  11         no  12         no  13         no  14         no  15         no  16
  mo    8   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo    9   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo   10   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo   11   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo   12   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
  mo   13   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
  mo   14   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
  mo   15   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

                no  17         no  18         no  19         no  20         no  21         no  22         no  23         no  24
  mo   16   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo   17   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo   18   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo   19   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo   20   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
  mo   21   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
  mo   22   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
  mo   23   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01
  eigenvectors of nos in ao-basis, column    1
  0.0000000E+00  0.0000000E+00  0.0000000E+00  0.0000000E+00  0.9188403E+00  0.0000000E+00  0.0000000E+00  0.7204964E-01
  0.0000000E+00  0.1416552E-14 -0.1830008E-01  0.0000000E+00  0.0000000E+00  0.0000000E+00  0.0000000E+00  0.0000000E+00
  0.0000000E+00  0.3154239E-01  0.0000000E+00  0.0000000E+00  0.0000000E+00  0.0000000E+00  0.3154239E-01  0.0000000E+00
  eigenvectors of nos in ao-basis, column    2
  0.1000431E+01  0.2482130E-02 -0.1419915E-02  0.7781734E-14  0.0000000E+00 -0.2616439E-02 -0.3398887E-13  0.0000000E+00
  0.1645654E-02  0.0000000E+00  0.0000000E+00  0.3376021E-05  0.3967485E-14  0.8587611E-04 -0.5438131E-03  0.6927337E-03
 -0.6163335E-03  0.0000000E+00 -0.5060470E-03 -0.5438131E-03  0.6927337E-03  0.6163335E-03  0.0000000E+00 -0.5060470E-03
  eigenvectors of nos in ao-basis, column    3
 -0.8844768E-02  0.8674603E+00 -0.1297648E+00  0.6089053E-13  0.0000000E+00 -0.1174189E+00  0.6568151E-13  0.0000000E+00
  0.6892078E-01  0.0000000E+00  0.0000000E+00  0.3306414E-03 -0.5614132E-14  0.1611783E-02  0.3379710E+00 -0.1596253E+00
  0.3836313E-01  0.0000000E+00  0.2045257E-01  0.3379710E+00 -0.1596253E+00 -0.3836313E-01  0.0000000E+00  0.2045257E-01
  eigenvectors of nos in ao-basis, column    4
  0.4616762E-13  0.1371997E-12 -0.1998070E-12  0.7158542E+00  0.0000000E+00  0.1612520E-12 -0.1132335E+00  0.0000000E+00
  0.1762196E-13  0.0000000E+00  0.0000000E+00 -0.4966912E-15 -0.2722905E-01 -0.6927103E-15 -0.5635816E+00  0.1958934E+00
 -0.2355515E-01  0.0000000E+00 -0.3267843E-01  0.5635816E+00 -0.1958934E+00 -0.2355515E-01  0.0000000E+00  0.3267843E-01
  eigenvectors of nos in ao-basis, column    5
 -0.2070810E-02  0.2955600E+00  0.1824047E+00 -0.1630789E-12  0.0000000E+00  0.7982083E+00  0.8010964E-13  0.0000000E+00
 -0.1316750E-02  0.0000000E+00  0.0000000E+00 -0.5179269E-02  0.7124025E-14 -0.2323002E-02 -0.3484059E+00  0.1370807E+00
 -0.3161476E-01  0.0000000E+00  0.8311562E-02 -0.3484059E+00  0.1370807E+00  0.3161476E-01  0.0000000E+00  0.8311562E-02
  eigenvectors of nos in ao-basis, column    6
 -0.1687550E-13 -0.6976727E-13  0.1630899E-12  0.7277607E+00 -0.2063855E-20 -0.2635885E-13  0.8341926E+00 -0.6919644E-19
 -0.4339350E-13  0.8446766E-19  0.9486414E-19  0.5717816E-14 -0.1329366E+01  0.1531364E-13  0.1926243E+01 -0.6729536E+00
  0.6201121E+00  0.1166532E-18  0.4908839E+00 -0.1926243E+01  0.6729536E+00  0.6201121E+00  0.5660713E-19 -0.4908839E+00
  eigenvectors of nos in ao-basis, column    7
 -0.5211847E-01  0.1431756E+00  0.9350625E+00 -0.3212687E-12 -0.2651782E-16 -0.2693111E+00  0.2294226E-12  0.6097966E-16
 -0.2052835E+00 -0.1917530E-15 -0.1461129E-16  0.2090738E-02 -0.2192932E-13  0.5372106E-02 -0.9729573E-01 -0.7866691E+00
 -0.1885770E-01 -0.7385777E-16 -0.1685441E-01 -0.9729573E-01 -0.7866691E+00  0.1885770E-01  0.4913963E-17 -0.1685441E-01
  eigenvectors of nos in ao-basis, column    8
 -0.7525888E-13 -0.3615981E-12  0.3645833E-12 -0.4064254E+00  0.6143126E-16  0.1928467E-12 -0.4850854E+00 -0.4125148E-16
 -0.2481429E-12 -0.2448182E-16 -0.2575400E-16 -0.2153392E-14  0.2228350E-01  0.5706246E-14 -0.3830673E-01 -0.1445818E+01
 -0.2186979E-01 -0.2822300E-16 -0.1841129E-01  0.3830673E-01  0.1445818E+01 -0.2186979E-01 -0.4770756E-16  0.1841129E-01
  eigenvectors of nos in ao-basis, column    9
  0.8133027E-13  0.3949953E-12 -0.7847117E-12 -0.3878087E+00  0.9167769E-16 -0.4204844E-12 -0.3070854E+00 -0.1259703E-15
  0.3890379E-12  0.1296804E-15  0.1898080E-15 -0.1720719E-14 -0.1128502E+00 -0.4338076E-13 -0.1609845E+01  0.1468783E+01
  0.7759868E-01  0.1210108E-15  0.1590354E+00  0.1609845E+01 -0.1468783E+01  0.7759868E-01 -0.6645719E-16 -0.1590354E+00
  eigenvectors of nos in ao-basis, column   10
  0.6009755E-01  0.4905569E+00 -0.5872575E+00  0.1274610E-12 -0.4148646E-16 -0.4708518E+00  0.3521379E-12  0.3190314E-16
  0.2280175E+00  0.1095879E-15 -0.7270697E-16  0.1275336E-03  0.9135192E-13 -0.5458997E-01 -0.1328241E+01  0.1203349E+01
  0.3054664E+00  0.3029621E-16  0.5596935E-01 -0.1328241E+01  0.1203349E+01 -0.3054664E+00  0.4008870E-17  0.5596935E-01
  eigenvectors of nos in ao-basis, column   11
 -0.1171771E-02 -0.2149045E+00 -0.6662232E+00  0.4008957E-12  0.1272162E-15 -0.1100021E+01 -0.9162349E-12 -0.3235579E-15
  0.1802690E+01  0.4183721E-17 -0.8150565E-16 -0.3135894E-02  0.4076522E-13  0.2248781E-02  0.9562774E+00 -0.3614770E+00
  0.9392462E-01  0.3124207E-15  0.2398654E+00  0.9562774E+00 -0.3614770E+00 -0.9392462E-01  0.7696705E-16  0.2398654E+00
  eigenvectors of nos in ao-basis, column   12
 -0.1865057E-16 -0.1137790E-16  0.4372914E-15  0.2311638E-15  0.1409556E+01 -0.1502495E-16 -0.2137865E-17 -0.1685359E+01
 -0.2644028E-15  0.2619468E-14 -0.4378665E-02 -0.2227742E-16  0.2920819E-16  0.1910002E-16 -0.2112112E-15  0.1351552E-15
 -0.2569473E-18  0.5475679E-02  0.6083716E-17 -0.6046545E-15  0.1695000E-15  0.2264732E-15  0.5475679E-02 -0.1656291E-15
  eigenvectors of nos in ao-basis, column   13
  0.6769483E-13  0.1301094E-12 -0.7410199E-12 -0.1057726E+01 -0.1337786E-16 -0.2734863E-12  0.2269170E+01 -0.3776348E-16
  0.4949036E-12  0.2616358E-18  0.1265581E-15 -0.5399661E-14 -0.4799202E-01  0.1756388E-14  0.6650249E+00  0.5106979E+00
  0.3041767E+00  0.4995213E-16  0.1883073E+00 -0.6650249E+00 -0.5106979E+00  0.3041767E+00  0.8462649E-16 -0.1883073E+00
  eigenvectors of nos in ao-basis, column   14
  0.2091554E+00  0.1071580E+01 -0.1181893E+01  0.2281662E-12  0.3799142E-16  0.1763751E+00 -0.4822797E-12  0.1095098E-14
 -0.7970975E+00 -0.1337744E-15  0.5119991E-16 -0.3282162E-01  0.3109784E-13 -0.1255569E-03 -0.5377055E+00  0.4909475E+00
 -0.3086257E+00 -0.2410793E-17  0.5610314E+00 -0.5377055E+00  0.4909475E+00  0.3086257E+00 -0.2622459E-14  0.5610314E+00
  eigenvectors of nos in ao-basis, column   15
 -0.2948458E-15 -0.1543599E-14  0.1780871E-14  0.9962454E-16 -0.1039696E-13 -0.2101302E-15  0.1481156E-15  0.4657262E-13
  0.1318351E-14 -0.1289159E+00 -0.1874740E-13  0.5522893E-16  0.1935210E-16  0.1444038E-17  0.1106991E-14 -0.8594874E-15
  0.5663967E-15  0.6862173E+00 -0.1023255E-14  0.6499074E-15 -0.7245662E-15 -0.5506128E-15 -0.6862173E+00 -0.1231443E-14
  eigenvectors of nos in ao-basis, column   16
  0.1124007E-14  0.5190615E-14 -0.6901306E-14 -0.8661637E-16 -0.6688636E-01  0.7454745E-15  0.3311698E-16 -0.5949782E+00
 -0.7786919E-15  0.3482762E-13 -0.1598336E+00 -0.3942046E-16 -0.9962211E-17  0.5635439E-16  0.4204281E-15  0.4277037E-15
 -0.2403051E-15  0.7710122E+00  0.7735285E-15  0.5615269E-15  0.4375273E-15  0.5413873E-16  0.7710122E+00  0.6927030E-15
  eigenvectors of nos in ao-basis, column   17
 -0.7152723E+00 -0.3091336E+01  0.4825988E+01 -0.3740154E-13 -0.1754659E-15 -0.1908556E+00 -0.1542837E-11 -0.5249664E-15
 -0.8798870E+00 -0.3829584E-16 -0.1404789E-15 -0.3319167E-01 -0.4320577E-13 -0.5345980E-01 -0.1438511E+01  0.3295761E+00
 -0.3798899E+00  0.8949531E-15  0.3789287E-01 -0.1438511E+01  0.3295761E+00  0.3798899E+00  0.7857435E-15  0.3789287E-01
  eigenvectors of nos in ao-basis, column   18
  0.1254386E-11  0.5561242E-11 -0.8613367E-11  0.1732794E-01 -0.1896560E-17  0.4631480E-12 -0.9280177E+00 -0.1893239E-16
  0.1418057E-11 -0.2085640E-15 -0.1692849E-15  0.4726852E-13 -0.3028578E-01  0.7661613E-13 -0.6786853E+00  0.2460613E+00
  0.4823549E+00  0.1561555E-16 -0.6903832E+00  0.6786853E+00 -0.2460613E+00  0.4823549E+00  0.1887728E-16  0.6903832E+00
  eigenvectors of nos in ao-basis, column   19
  0.2629732E-12  0.1389613E-11  0.1945139E-12  0.1251840E+01 -0.2292075E-15 -0.1022185E-11 -0.4128890E+00  0.2467346E-15
  0.2119138E-12  0.1032580E-15 -0.4718214E-16 -0.2675803E-13  0.1472896E+00 -0.1247686E-12  0.5466282E+00  0.5377930E-01
  0.7276124E+00  0.1203127E-16  0.5654252E+00 -0.5466282E+00 -0.5377930E-01  0.7276124E+00  0.4511230E-16 -0.5654252E+00
  eigenvectors of nos in ao-basis, column   20
 -0.3068618E+00 -0.1538593E+01  0.1120258E+00  0.1321128E-11  0.1044481E-15  0.9807091E+00 -0.4518524E-12 -0.6503741E-16
 -0.2798825E+00 -0.1478313E-16 -0.3504715E-15  0.1641279E-01  0.1596852E-12  0.1132260E+00  0.8363142E+00 -0.2663892E+00
  0.7517673E+00 -0.3931772E-16  0.5305955E+00  0.8363142E+00 -0.2663892E+00 -0.7517673E+00 -0.1275737E-15  0.5305955E+00
  eigenvectors of nos in ao-basis, column   21
  0.2164986E-16  0.1198196E-16 -0.1021751E-14  0.2353443E-15  0.7240906E-02  0.3682210E-15 -0.3947190E-15 -0.3229147E+00
  0.2052502E-15  0.1822167E-12  0.1045714E+01 -0.1759278E-15  0.2730361E-16  0.2510648E-16  0.7780825E-15 -0.4884518E-15
  0.5190693E-15  0.4066771E+00  0.3040973E-16  0.5472104E-15  0.1565809E-15 -0.4277323E-15  0.4066771E+00  0.2941472E-15
  eigenvectors of nos in ao-basis, column   22
  0.1050887E-15  0.2357625E-15 -0.1977831E-14 -0.6737873E-16 -0.4970292E-14  0.5004176E-15 -0.9749280E-16  0.8383453E-13
  0.8871752E-15  0.1072280E+01 -0.1747192E-12 -0.2994074E-15 -0.3474570E-17 -0.3470492E-15  0.2216376E-14 -0.1323823E-14
  0.7823238E-15  0.3811176E+00 -0.1835534E-15  0.2143708E-14 -0.9715350E-15 -0.7529339E-15 -0.3811176E+00  0.2083587E-15
  eigenvectors of nos in ao-basis, column   23
  0.8598546E-01  0.3028774E+00 -0.8098987E+00 -0.2434517E-13 -0.8213752E-16  0.5946500E-01  0.7280752E-13  0.3190450E-15
  0.5469864E+00 -0.9636920E-15 -0.5635709E-15 -0.3159243E+00 -0.2776325E-13  0.6761892E-01  0.5941891E+00 -0.2522491E+00
  0.3743832E+00 -0.6404347E-15 -0.3122271E+00  0.5941891E+00 -0.2522491E+00 -0.3743832E+00  0.8279087E-16 -0.3122271E+00
  eigenvectors of nos in ao-basis, column   24
 -0.1166195E+00 -0.2416777E+00  0.2434799E+01 -0.1887791E-13 -0.7061382E-16 -0.6217072E+00 -0.4086266E-13  0.5771820E-16
 -0.6211116E+00  0.7576342E-15  0.8430511E-16  0.2467611E-01  0.3836063E-13  0.5861624E+00 -0.2169264E+01  0.8956706E+00
 -0.6236886E+00  0.2976308E-15 -0.5088806E+00 -0.2169264E+01  0.8956706E+00  0.6236886E+00 -0.2294555E-15 -0.5088806E+00


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***


 cosurf_and_phi
  2.466315385316 -0.061539892486  2.837440149000 -0.034370604262
  0.006359863210 -2.199287675687  3.114544749440 -0.062933376365
 -0.480526931872  1.061509344774  3.755560521090 -0.064148417365
  1.262739748330  2.872870034186  1.567866969040 -0.042635124542
  1.897949994968 -2.634366215369  0.570962659525 -0.015380963593
 -2.790166444607 -0.824331206483  2.170430257018 -0.019321468883
 -2.061296060778  2.493528161233  1.034251636268 -0.020819237183
 -0.002991703587  2.420846177436 -1.447622679829  0.011948642117
 -1.114435211431 -2.949559801670 -0.067898604227 -0.016077390874
  1.955709175590 -0.984462672186  3.123501329999 -0.046406945930
  1.000900154469 -1.706149424342  3.300403738319 -0.059492387183
  1.652947872482  0.360731428865  3.496571379926 -0.052788977835
  0.594940552015  0.670555958619  3.845537291721 -0.063791583883
  2.390078334690  1.202722009900  2.566708449184 -0.033900743429
  1.989334673621  2.229216060677  2.001028520754 -0.036548139281
  2.919143888229  0.378144896597  2.099775822683 -0.014391344019
  2.780367109803 -0.921046401167  2.130496470151 -0.019086870536
  2.449941849009 -2.011800733381  1.439000205618 -0.018021977970
 -0.231540571447 -1.263621120083  3.706953994699 -0.064793919300
 -0.352106389130 -0.107618694495  3.950679056227 -0.064905503156
  0.105380882100  1.878884201281  3.371423292662 -0.063982836097
  0.783266721831  2.590884380442  2.520834408126 -0.056610004193
  2.011402526412  2.551496219550  0.814855178005 -0.017769143502
  1.440220538007 -2.843720084632  1.356653310131 -0.037509340940
  0.871209499702 -2.660587439486  2.372618950166 -0.054793133390
 -2.947488997374  0.298617382934  2.058332654596 -0.012862348920
 -2.674954162172  1.563027715274  1.704200253745 -0.015931714437
 -1.353448939749  2.910920816403  0.212056013565 -0.018613926005
 -0.743422400795  2.810699746106 -0.731960510989 -0.004323943353
  0.099626804209  1.855073908563 -1.945822518898  0.029601871749
  0.019900458911 -1.968682238442 -1.864952523830  0.026388425644
 -0.601526683996 -2.669374282549 -1.032942810695  0.002601915334
 -1.976701730993 -2.578422698078  0.816296596419 -0.018629331230
 -2.525639241426 -1.850752473194  1.593331825886 -0.018492509989
 -1.124962231191 -1.881571875050  3.121019991266 -0.057552898647
 -2.117398618237 -1.513942036836  2.667866282109 -0.040465303694
 -2.336046769995 -0.152318885910  2.976109389266 -0.038215813431
 -1.478775916646  0.469153703894  3.577445396081 -0.055391145912
 -1.328587214463  1.668007282102  3.174271253825 -0.055657843358
 -1.771761195387  2.287385908353  2.202255939279 -0.042540089613
 -1.026730149698  3.017318998523  1.358608629268 -0.042473317530
  0.119020599620  3.173161356543  1.415159765853 -0.048074223322
  0.915237473358  3.108118624501  0.463299650838 -0.029353214118
  0.462814589486  2.837210699514 -0.795516582628 -0.005197620869
  1.026812912753 -3.017319139618  0.083991013862 -0.020597445718
 -0.070059393274 -3.176418651270  0.035610954337 -0.025715677406
 -0.970268948715 -3.083313212042  1.062474740253 -0.038809338274
 -0.534203029846 -2.828286137107  2.231267851728 -0.055654832963
  0.871897873699 -0.575451888002  3.799144800425 -0.061994639249
  1.408719892640  1.640288870679  3.148120355694 -0.054583395021
  2.650053602975  1.633766196698  1.655441764425 -0.015879240488
  1.964286464189 -2.001027831890  2.365093852582 -0.040762147359
 -1.342877128538 -0.760711330777  3.581800927521 -0.057046417880
 -0.531365927285  2.661712102822  2.509437292848 -0.057962221475
  1.142904167226  2.885766749848 -0.243475252462 -0.011486698850
  0.342129526197 -3.189261637688  1.246854200824 -0.045388289319
 -2.346459445268  1.135413320199  2.662806558936 -0.035693232547
 -0.258565605057  3.205542092831  0.249849913336 -0.029543194065
  0.450566961335 -2.699565911226 -1.032013446782  0.001225636567
 -1.684533476150 -2.430522251364  2.070179818920 -0.042598308929
 -3.368784049130  0.036613212616 -1.854972615293  0.058700004553
 -3.278396927799  1.580834404538 -0.078603229040  0.032623887513
 -3.656110378332 -0.713812229513  0.361831391088  0.033722006190
 -2.408052717333 -2.075622947728 -1.226189024363  0.036814251624
 -1.295829484895 -0.567704086865 -2.747607986898  0.054759631720
 -1.846557165647  1.681693338816 -2.099716493181  0.045977223701
 -3.831862607217  0.357468890277 -0.654813288033  0.049463690965
 -3.527736967644 -1.045671231370 -1.064852892071  0.050344088398
 -2.622906831544 -1.024738705101 -2.241124222290  0.055870497229
 -2.402781990555  0.378472303440 -2.579763034114  0.058853455544
 -3.126606927219  1.313275381963 -1.541857021673  0.051509509531
 -3.633182667875  0.531520753705  0.561856665829  0.030800813749
 -3.086813000261 -1.794874586082 -0.179700355757  0.030594537327
 -1.615880090830 -1.674393887320 -2.147504235692  0.044936561186
 -1.240258025012  0.765881087348 -2.687977655831  0.053488967638
 -2.430292517958  2.154437082790 -0.969924007583  0.032557462001
  3.368776503197 -0.036639040867 -1.854966842961  0.058700404880
  3.278392620256 -1.580850766330 -0.078589062660  0.032623186712
  3.656106873706  0.713798214804  0.361832640492  0.033722266754
  2.408046317685  2.075600470298 -1.226192756897  0.036815137409
  1.295820311657  0.567673501678 -2.747601655947  0.054760163553
  1.846549173548 -1.681720471299 -2.099699178989  0.045976594295
  3.831857249217 -0.357488322766 -0.654806650060  0.049463717170
  3.527730862121  1.045649613724 -1.064853177123  0.050344831858
  2.622898581638  1.024710819001 -2.241122746235  0.055871358329
  2.402773123306 -0.378501994157 -2.579753678917  0.058853673796
  3.126599952113 -1.313299541573 -1.541844004398  0.051509134312
  3.633179527908 -0.531533702443  0.561864593522  0.030800511732
  3.086808508398  1.794857685487 -0.179703829578  0.030595206142
  1.615872011596  1.674366500124 -2.147504385867  0.044937411492
  1.240248960489 -0.765911354740 -2.687964116774  0.053488827352
  2.430286585511 -2.154458194501 -0.969905238283  0.032556543067
  0.000006852884  1.035694667087 -2.361676372823  0.046979034298
 -0.298806015181  0.969607486526 -2.408809074493  0.048501242400
  0.298815704890  0.969607820141 -2.408807386530  0.048501318921
  0.000007656756 -1.035723195601 -2.361670853435  0.046978535694
 -0.298805262603 -0.969636498141 -2.408803907288  0.048500758230
  0.298816457468 -0.969636367899 -2.408802219325  0.048500852478
 -0.667712940797 -1.245186997899 -2.338574529312  0.046522342270
 -1.218112188165 -2.025138759161 -1.616566947615  0.032936789143
 -2.084175601108 -2.294008802021 -0.480477682822  0.019038748523
 -2.876631896395 -1.658047550445  0.559052047065  0.016971188367
 -3.282909221331 -0.368099862162  1.091996180628  0.019662927091
 -3.142757910518  1.067034798172  0.908143333087  0.018448454351
 -2.511458431963  2.081290260960  0.080011352455  0.016716562473
 -1.638016880752  2.274609499719 -1.065756198713  0.024575829877
 -0.866948462969  1.570740798135 -2.077229430584  0.042722811879
 -0.467915446486 -1.694563900014 -1.941501402531  0.035437689916
 -1.299753513329 -2.453901457341 -0.850306853788  0.010627894760
 -2.242033801688 -2.245340287831  0.385760998463  0.000604800965
 -2.923088754321 -1.151144046328  1.279154738758  0.003603462701
 -3.074287016292  0.397098864814  1.477489335582  0.005422302861
 -2.635990824421  1.788708515315  0.902534843950  0.001304837506
 -1.781079179779  2.474786487342 -0.218927030025  0.003408856753
 -0.846758459860  2.184720175398 -1.444553386477  0.021850296046
 -0.255852300976  1.360631011730 -2.219692209228  0.041864758849
  0.667723897920  1.245157743773 -2.338577856151  0.046523113973
  1.218123388571  2.025110412626 -1.616576841774  0.032937948849
  2.084186643139  2.293984793080 -0.480493513306  0.019040012419
  2.876642520254  1.658030225134  0.559035126479  0.016972220142
  3.282919570196  0.368088739237  1.091983758387  0.019663385996
  3.142768358070 -1.067043033853  0.908139383421  0.018448361562
  2.511469270857 -2.081300494445  0.080016452498  0.016716167673
  1.638028059662 -2.274626132712 -1.065745290088  0.024575234678
  0.866959534092 -1.570765766224 -2.077218589767  0.042722258984
  0.467932162408  1.694535407938 -1.941510815499  0.035438633029
  1.299770347024  2.453875604722 -0.850324284820  0.010629136127
  2.242050270258  2.245320705184  0.385739526123  0.000606082864
  2.923104801922  1.151131828431  1.279135167181  0.003604423008
  3.074302957696 -0.397105856761  1.477477125466  0.005422740189
  2.636007061705 -1.788714946455  0.902532611959  0.001304993646
  1.781095866662 -2.474797662024 -0.218920775640  0.003408775380
  0.846775315928 -2.184739733041 -1.444543821640  0.021849845672
  0.255868904327 -1.360657816862 -2.219684436601  0.041864253161
  0.427045418766 -0.580992742084 -2.585105089006  0.052119483206
  0.427044967836  0.580963354323 -2.585108185092  0.052119763747
 -0.427035838172 -0.000014913339 -2.543076305315  0.056973507105
 end_of_phi


 nsubv after electrostatic potential =  1

  Confirmation of dielectric energy for ground state
  edielnew =  -0.0104563943

 =========== Executing IN-CORE method ==========
 norm multnew:  0.0791428405
 threx: total number of loops= 14
 threx: total number of loops= 24
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331914E+01-0.309165E+01-0.410226E+01-0.407883E+01-0.475210E+01-0.534847E+01-0.433670E+01-0.358739E+01-0.342330E+01-0.317821E+01
 -0.403664E+01-0.263325E+01-0.362338E+01-0.366481E+01-0.366999E+01-0.363955E+01-0.338751E+01-0.303510E+01-0.289433E+01-0.330517E+02
 -0.787312E+01-0.671622E+01-0.695563E+01-0.711691E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331914E+01-0.309165E+01-0.410226E+01-0.407883E+01-0.475210E+01-0.534847E+01-0.433670E+01-0.358739E+01-0.342330E+01-0.317821E+01
 -0.403664E+01-0.263325E+01-0.362338E+01-0.366481E+01-0.366999E+01-0.363955E+01-0.338751E+01-0.303510E+01-0.289433E+01-0.330517E+02
 -0.787312E+01-0.671622E+01-0.695563E+01-0.711691E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331914E+01-0.309165E+01-0.410226E+01-0.407883E+01-0.475210E+01-0.534847E+01-0.433670E+01-0.358739E+01-0.342330E+01-0.317821E+01
 -0.403664E+01-0.263325E+01-0.362338E+01-0.366481E+01-0.366999E+01-0.363955E+01-0.338751E+01-0.303510E+01-0.289433E+01-0.330517E+02
 -0.787312E+01-0.671622E+01-0.695563E+01-0.711691E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        40 xx:       135 ww:       255
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        20 yw:        25

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        39
task #   9:       127    task #  10:       247    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 matrix-vector product formed for vector   2
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference-subspace vector overlap matrix  block   1

                ci   1         ci   2
 ref:   1     1.00000000     0.00000000

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97633721    -0.21625367
 subspace has dimension  2

          (h-repnuc*1) matrix in the subspace basis

                x:   1         x:   2
   x:   1   -85.40090838
   x:   2    -0.25187978    -6.45464988

          overlap matrix in the subspace basis

                x:   1         x:   2
   x:   1     1.00000000
   x:   2     0.00000000     0.07914284

          eigenvectors and eigenvalues in the subspace basis

                v:   1         v:   2

   energy   -85.59922135   -81.35865206

   x:   1     0.97633721    -0.21625367
   x:   2     0.76870139     3.47051580

          eigenvectors and eigenvalues in the schmidt orthonormalized basis

                v:   1         v:   2

   energy   -85.59922135   -81.35865206

  zx:   1     0.97633721    -0.21625367
  zx:   2     0.21625367     0.97633721

          <ref|baseci> overlap matrix  block   1

                ci   1         ci   2
 ref:   1     1.00000000     0.00000000

 trial vector basis is being transformed.  new dimension:   1

          transformed tciref transformation matrix  block   1

               ref   1
  ci:   1     0.97633721

  write the transformed vectors back to cosmofile

 rtflw =  0
 freezing =  F

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1    -76.2462585708 -9.1546E+00  6.4774E-03  1.5760E-01  1.0000E-03
 mr-sdci #  2  2    -72.0056892796  7.2006E+01  0.0000E+00  1.7220E+00  1.0000E-04


 total energy in cosmo calc 
e(rootcalc) + repnuc=     -76.2462585708
dielectric energy =      -0.0104563943
deltaediel =       0.0104563943
e(rootcalc) + repnuc - ediel =     -76.2358021765
e(rootcalc) + repnuc - edielnew =     -76.2462585708
deltaelast =      76.2358021765

 root number  1 is used to define the new expansion vector.
 diagon: frcsub=2, new matrix-vector products will be computed.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.01   0.00   0.00   0.01         0.    0.0000
    2   25    0   0.01   0.00   0.00   0.01         0.    0.0000
    3   26    0   0.03   0.00   0.00   0.02         0.    0.0000
    4   11    0   0.01   0.00   0.00   0.01         0.    0.0000
    5   15    0   0.02   0.00   0.00   0.02         0.    0.0000
    6   16    0   0.02   0.00   0.00   0.02         0.    0.0000
    7    1    0   0.01   0.00   0.00   0.01         0.    0.0000
    8    5    0   0.02   0.00   0.00   0.02         0.    0.0000
    9    6    0   0.02   0.00   0.00   0.02         0.    0.0000
   10    7    0   0.03   0.00   0.00   0.02         0.    0.0000
   11   75    0   0.01   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.01   0.00   0.00   0.01         0.    0.0000
   14   47    0   0.02   0.00   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.2200s 
time spent in multnx:                   0.1900s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0200s 
total time per CI iteration:            3.1900s 

          starting ci iteration   3

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.35296278

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=      10  D0X=      30  D0W=      50
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================

          ci-one-electron density block   1

                mo   1         mo   2         mo   3         mo   4         mo   5         mo   6         mo   7         mo   8
   mo   1     1.99994006
   mo   2    -0.00004030     1.98830072
   mo   3     0.00000000     0.00000020     1.97100432
   mo   4    -0.00012930     0.00350194    -0.00000060     1.97339477
   mo   5     0.00000000     0.00000001    -0.00000002    -0.00000013     1.97467402
   mo   6    -0.00001189     0.00192612    -0.00001525    -0.02689911    -0.00000690     0.00630079
   mo   7     0.00000000     0.00000504    -0.01662831    -0.00001686    -0.00000418     0.00000040     0.00818021
   mo   8    -0.00000001     0.00001189    -0.01417521    -0.00001592     0.00000235     0.00000038     0.00692009     0.00925194
   mo   9    -0.00002918     0.00258691    -0.00000951    -0.00335291     0.00000228     0.00608637     0.00000014     0.00000014
   mo  10    -0.00006957     0.00246380    -0.00001024     0.00066687    -0.00000003     0.00388105     0.00000010     0.00000010
   mo  11     0.00000003    -0.00000124     0.00000250    -0.00000020    -0.00400926     0.00000001    -0.00000002    -0.00000002
   mo  12     0.00000000    -0.00000249     0.00137104    -0.00000365    -0.00000176     0.00000004     0.00668743     0.00407073
   mo  13     0.00001498    -0.00001587    -0.00000951    -0.01104214    -0.00000215     0.00152012     0.00000019     0.00000018
   mo  14     0.00000000     0.00000163    -0.00000268    -0.00000095    -0.00000160     0.00000002     0.00000003     0.00000002
   mo  15     0.00000000     0.00000082    -0.00000152    -0.00000388    -0.00797777     0.00000009     0.00000003     0.00000000
   mo  16    -0.00002849     0.00136012    -0.00000393    -0.00064908     0.00000136    -0.00110549     0.00000005     0.00000005
   mo  17     0.00000000    -0.00000628    -0.00053377     0.00001637     0.00000038    -0.00000025     0.00021545     0.00014327
   mo  18     0.00000001    -0.00000069    -0.00008945    -0.00000099     0.00000069     0.00000001    -0.00258026    -0.00352668
   mo  19     0.00003756    -0.00039170    -0.00000029    -0.00042707     0.00000016    -0.00249837     0.00000000     0.00000000
   mo  20     0.00000000     0.00000013    -0.00000039     0.00000112     0.00171200    -0.00000002     0.00000000     0.00000001
   mo  21     0.00000000    -0.00000020     0.00000054    -0.00000049    -0.00000110     0.00000001    -0.00000001     0.00000000
   mo  22     0.00000204     0.00010035     0.00000198    -0.00134052     0.00000032     0.00000661    -0.00000001     0.00000000
   mo  23    -0.00002234     0.00009865    -0.00000038     0.00023759    -0.00000045     0.00060767     0.00000000     0.00000000
   mo  24     0.00000000    -0.00000041    -0.00058949     0.00000144     0.00000004    -0.00000002    -0.00088160     0.00025176

                mo   9         mo  10         mo  11         mo  12         mo  13         mo  14         mo  15         mo  16
   mo   9     0.00833775
   mo  10     0.00387436     0.01191314
   mo  11     0.00000000     0.00000000     0.01780675
   mo  12     0.00000000     0.00000000     0.00000000     0.00690250
   mo  13     0.00020213    -0.00056106     0.00000000     0.00000002     0.00320540
   mo  14     0.00000000     0.00000000     0.00000000     0.00000000     0.00000001     0.00258371
   mo  15     0.00000000     0.00000000    -0.00185918     0.00000001     0.00000003     0.00000001     0.00195723
   mo  16     0.00044404     0.00302291     0.00000000     0.00000000    -0.00106342     0.00000000     0.00000000     0.00401611
   mo  17    -0.00000004    -0.00000001     0.00000000     0.00030353    -0.00000010     0.00000000     0.00000000    -0.00000001
   mo  18     0.00000000     0.00000000     0.00000000    -0.00176074     0.00000000     0.00000000     0.00000000     0.00000000
   mo  19    -0.00343191    -0.00093181     0.00000000     0.00000000    -0.00063753     0.00000000     0.00000000    -0.00007361
   mo  20     0.00000000     0.00000000     0.00018381     0.00000000    -0.00000001     0.00000000    -0.00150923     0.00000000
   mo  21     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00170347     0.00000000     0.00000000
   mo  22    -0.00043171     0.00028722     0.00000000     0.00000000     0.00142108     0.00000000     0.00000000     0.00056615
   mo  23     0.00016517     0.00173439     0.00000000     0.00000000     0.00018478     0.00000000     0.00000000    -0.00050628
   mo  24     0.00000000     0.00000000     0.00000000    -0.00155879    -0.00000001     0.00000000     0.00000000     0.00000000

                mo  17         mo  18         mo  19         mo  20         mo  21         mo  22         mo  23         mo  24
   mo  17     0.00038600
   mo  18    -0.00013617     0.00198159
   mo  19     0.00000000     0.00000000     0.00220692
   mo  20     0.00000000     0.00000000     0.00000000     0.00199551
   mo  21     0.00000000     0.00000000     0.00000000     0.00000000     0.00188124
   mo  22    -0.00000002     0.00000000    -0.00013384     0.00000000     0.00000000     0.00149683
   mo  23     0.00000000     0.00000000     0.00043775     0.00000000     0.00000000    -0.00007162     0.00121496
   mo  24    -0.00000985    -0.00035779     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00106754


*****   symmetry block SYM1   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       1.99994006
       2      -0.00004030     1.98830072
       3       0.00000000     0.00000020     1.97100432
       4      -0.00012930     0.00350194    -0.00000060     1.97339477
       5       0.00000000     0.00000001    -0.00000002    -0.00000013
       6      -0.00001189     0.00192612    -0.00001525    -0.02689911
       7       0.00000000     0.00000504    -0.01662831    -0.00001686
       8      -0.00000001     0.00001189    -0.01417521    -0.00001592
       9      -0.00002918     0.00258691    -0.00000951    -0.00335291
      10      -0.00006957     0.00246380    -0.00001024     0.00066687
      11       0.00000003    -0.00000124     0.00000250    -0.00000020
      12       0.00000000    -0.00000249     0.00137104    -0.00000365
      13       0.00001498    -0.00001587    -0.00000951    -0.01104214
      14       0.00000000     0.00000163    -0.00000268    -0.00000095
      15       0.00000000     0.00000082    -0.00000152    -0.00000388
      16      -0.00002849     0.00136012    -0.00000393    -0.00064908
      17       0.00000000    -0.00000628    -0.00053377     0.00001637
      18       0.00000001    -0.00000069    -0.00008945    -0.00000099
      19       0.00003756    -0.00039170    -0.00000029    -0.00042707
      20       0.00000000     0.00000013    -0.00000039     0.00000112
      21       0.00000000    -0.00000020     0.00000054    -0.00000049
      22       0.00000204     0.00010035     0.00000198    -0.00134052
      23      -0.00002234     0.00009865    -0.00000038     0.00023759
      24       0.00000000    -0.00000041    -0.00058949     0.00000144

               Column   5     Column   6     Column   7     Column   8
       5       1.97467402
       6      -0.00000690     0.00630079
       7      -0.00000418     0.00000040     0.00818021
       8       0.00000235     0.00000038     0.00692009     0.00925194
       9       0.00000228     0.00608637     0.00000014     0.00000014
      10      -0.00000003     0.00388105     0.00000010     0.00000010
      11      -0.00400926     0.00000001    -0.00000002    -0.00000002
      12      -0.00000176     0.00000004     0.00668743     0.00407073
      13      -0.00000215     0.00152012     0.00000019     0.00000018
      14      -0.00000160     0.00000002     0.00000003     0.00000002
      15      -0.00797777     0.00000009     0.00000003     0.00000000
      16       0.00000136    -0.00110549     0.00000005     0.00000005
      17       0.00000038    -0.00000025     0.00021545     0.00014327
      18       0.00000069     0.00000001    -0.00258026    -0.00352668
      19       0.00000016    -0.00249837     0.00000000     0.00000000
      20       0.00171200    -0.00000002     0.00000000     0.00000001
      21      -0.00000110     0.00000001    -0.00000001     0.00000000
      22       0.00000032     0.00000661    -0.00000001     0.00000000
      23      -0.00000045     0.00060767     0.00000000     0.00000000
      24       0.00000004    -0.00000002    -0.00088160     0.00025176

               Column   9     Column  10     Column  11     Column  12
       9       0.00833775
      10       0.00387436     0.01191314
      11       0.00000000     0.00000000     0.01780675
      12       0.00000000     0.00000000     0.00000000     0.00690250
      13       0.00020213    -0.00056106     0.00000000     0.00000002
      14       0.00000000     0.00000000     0.00000000     0.00000000
      15       0.00000000     0.00000000    -0.00185918     0.00000001
      16       0.00044404     0.00302291     0.00000000     0.00000000
      17      -0.00000004    -0.00000001     0.00000000     0.00030353
      18       0.00000000     0.00000000     0.00000000    -0.00176074
      19      -0.00343191    -0.00093181     0.00000000     0.00000000
      20       0.00000000     0.00000000     0.00018381     0.00000000
      21       0.00000000     0.00000000     0.00000000     0.00000000
      22      -0.00043171     0.00028722     0.00000000     0.00000000
      23       0.00016517     0.00173439     0.00000000     0.00000000
      24       0.00000000     0.00000000     0.00000000    -0.00155879

               Column  13     Column  14     Column  15     Column  16
      13       0.00320540
      14       0.00000001     0.00258371
      15       0.00000003     0.00000001     0.00195723
      16      -0.00106342     0.00000000     0.00000000     0.00401611
      17      -0.00000010     0.00000000     0.00000000    -0.00000001
      18       0.00000000     0.00000000     0.00000000     0.00000000
      19      -0.00063753     0.00000000     0.00000000    -0.00007361
      20      -0.00000001     0.00000000    -0.00150923     0.00000000
      21       0.00000000    -0.00170347     0.00000000     0.00000000
      22       0.00142108     0.00000000     0.00000000     0.00056615
      23       0.00018478     0.00000000     0.00000000    -0.00050628
      24      -0.00000001     0.00000000     0.00000000     0.00000000

               Column  17     Column  18     Column  19     Column  20
      17       0.00038600
      18      -0.00013617     0.00198159
      19       0.00000000     0.00000000     0.00220692
      20       0.00000000     0.00000000     0.00000000     0.00199551
      21       0.00000000     0.00000000     0.00000000     0.00000000
      22      -0.00000002     0.00000000    -0.00013384     0.00000000
      23       0.00000000     0.00000000     0.00043775     0.00000000
      24      -0.00000985    -0.00035779     0.00000000     0.00000000

               Column  21     Column  22     Column  23     Column  24
      21       0.00188124
      22       0.00000000     0.00149683
      23       0.00000000    -0.00007162     0.00121496
      24       0.00000000     0.00000000     0.00000000     0.00106754

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7        eno   8
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    8   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno   9        eno  10        eno  11        eno  12        eno  13        eno  14        eno  15        eno  16
 emo    9   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   10   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   11   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   12   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   13   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   14   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   15   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   16   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno  17        eno  18        eno  19        eno  20        eno  21        eno  22        eno  23        eno  24
 emo   17   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   18   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   19   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   20   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   21   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   22   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   23   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   24   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   1.9999410      1.9890990      1.9747160      1.9730424      1.9712496
   0.0210628      0.0192653      0.0180253      0.0094479      0.0048692
   0.0045091      0.0039718      0.0034594      0.0033229      0.0007692
   0.0007606      0.0004932      0.0004507      0.0004079      0.0003694
   0.0003434      0.0003426      0.0000437      0.0000379

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7         no   8
  mo    1   0.999971E+00   0.628062E-02  -0.231027E-07   0.434950E-02   0.169086E-06   0.712731E-08   0.398934E-04  -0.146606E-07
  mo    2  -0.516014E-02   0.975104E+00   0.108970E-05  -0.221677E+00  -0.981532E-05  -0.456183E-05  -0.211976E-02   0.667634E-06
  mo    3   0.965835E-08   0.598362E-06  -0.140839E-05  -0.420382E-04   0.999937E+00   0.901510E-02   0.979709E-05  -0.133982E-05
  mo    4  -0.563303E-02   0.221640E+00  -0.545421E-05   0.974998E+00   0.405880E-04   0.109048E-04   0.742616E-02  -0.137139E-06
  mo    5  -0.164470E-08   0.135114E-06   0.999989E+00   0.550134E-05   0.139955E-05   0.111229E-05   0.102528E-05   0.153432E-02
  mo    6   0.649410E-04  -0.205532E-02  -0.342617E-05  -0.135621E-01  -0.835543E-05   0.159214E-04   0.475201E+00   0.506602E-06
  mo    7   0.330343E-07   0.598513E-06  -0.211265E-05  -0.861668E-05  -0.849325E-02   0.604866E+00  -0.186927E-04   0.872148E-06
  mo    8   0.104155E-07   0.406897E-05   0.119741E-05  -0.898026E-05  -0.725305E-02   0.580847E+00  -0.172294E-04   0.121939E-05
  mo    9  -0.117951E-04   0.894888E-03   0.115940E-05  -0.199808E-02  -0.496289E-05   0.176998E-04   0.559149E+00   0.124329E-05
  mo   10  -0.432302E-04   0.128878E-02  -0.178661E-07   0.230122E-04  -0.525386E-05   0.186329E-04   0.630725E+00   0.144241E-05
  mo   11   0.185051E-07  -0.635624E-06  -0.204483E-02   0.300015E-07   0.127535E-05  -0.169159E-05  -0.191942E-05   0.992783E+00
  mo   12   0.152162E-07  -0.162111E-05  -0.898997E-06  -0.160678E-05   0.654185E-03   0.488835E+00  -0.165151E-04   0.800313E-06
  mo   13   0.387561E-04  -0.124228E-02  -0.106520E-05  -0.547450E-02  -0.506359E-05   0.233567E-05   0.277569E-01  -0.178551E-06
  mo   14  -0.185062E-08   0.696256E-06  -0.810988E-06  -0.654111E-06  -0.136356E-05   0.280634E-06   0.104665E-06  -0.778171E-07
  mo   15   0.765332E-08  -0.297163E-07  -0.404266E-02  -0.203457E-05  -0.777801E-06   0.336512E-06   0.590816E-06  -0.117759E+00
  mo   16  -0.160810E-04   0.599487E-03   0.697105E-06  -0.464653E-03  -0.202022E-05   0.306121E-05   0.103125E+00   0.434098E-06
  mo   17  -0.300753E-07  -0.125379E-05   0.192685E-06   0.880754E-05  -0.272167E-03   0.188333E-01  -0.182862E-05   0.563207E-07
  mo   18   0.957506E-08  -0.457044E-06   0.351890E-06  -0.379714E-06  -0.218165E-04  -0.233428E+00   0.780500E-05  -0.417152E-06
  mo   19   0.209589E-04  -0.238920E-03   0.856351E-07  -0.144617E-03  -0.130574E-06  -0.649316E-05  -0.216291E+00  -0.396294E-06
  mo   20  -0.297141E-08   0.189372E-06   0.870729E-03   0.547907E-06  -0.194658E-06  -0.344962E-07  -0.191659E-06   0.226352E-01
  mo   21   0.165124E-08  -0.155090E-06  -0.554803E-06  -0.218368E-06   0.274732E-06  -0.929391E-07   0.213732E-07  -0.634116E-07
  mo   22   0.455651E-05  -0.100964E-03   0.165203E-06  -0.677892E-03   0.975343E-06   0.135499E-06   0.305135E-02   0.885239E-08
  mo   23  -0.121091E-04   0.750649E-04  -0.230433E-06   0.101613E-03  -0.196047E-06   0.219323E-05   0.739390E-01   0.123957E-06
  mo   24  -0.199231E-08  -0.376588E-07   0.227048E-07   0.774277E-06  -0.296828E-03  -0.535620E-01   0.182352E-05  -0.865540E-07

                no   9         no  10         no  11         no  12         no  13         no  14         no  15         no  16
  mo    1   0.179864E-04  -0.209106E-08  -0.566190E-05   0.830807E-10   0.118161E-05   0.121340E-09  -0.227167E-05  -0.130864E-08
  mo    2  -0.171115E-03   0.414353E-05   0.287078E-03  -0.703959E-06  -0.246508E-03   0.170381E-06  -0.252080E-03  -0.333345E-05
  mo    3  -0.164728E-05  -0.346385E-02   0.282021E-05   0.122570E-05   0.157191E-05  -0.250170E-06   0.205851E-05   0.347166E-02
  mo    4  -0.696237E-02  -0.223009E-05   0.531635E-02   0.214014E-06   0.184829E-02  -0.172726E-05   0.172715E-03   0.632163E-05
  mo    5  -0.137861E-05   0.159423E-05   0.212117E-05   0.291730E-06  -0.429381E-06  -0.353320E-02  -0.784943E-06  -0.601834E-06
  mo    6  -0.362214E+00  -0.203084E-05   0.192209E+00  -0.272679E-06  -0.111643E+00  -0.405859E-06  -0.913831E-01   0.420854E-04
  mo    7   0.950762E-06   0.185788E+00   0.261752E-05   0.347044E-06   0.830297E-06  -0.182254E-06   0.391234E-04   0.111314E+00
  mo    8  -0.812384E-06  -0.627412E+00  -0.921194E-05  -0.102652E-05  -0.169343E-05  -0.162675E-06   0.107266E-03   0.351978E+00
  mo    9  -0.390442E+00   0.757727E-05  -0.400015E+00   0.253580E-06   0.501217E-01  -0.398946E-06   0.401163E+00  -0.130376E-03
  mo   10   0.634560E+00  -0.398899E-05   0.273067E+00  -0.258306E-06  -0.130966E+00   0.843408E-06  -0.233712E+00   0.696269E-04
  mo   11  -0.423421E-06   0.262512E-06   0.714266E-06   0.386217E-06   0.252201E-06  -0.958759E-01  -0.112197E-06  -0.484321E-08
  mo   12   0.274425E-05   0.606354E+00   0.797115E-05   0.502921E-06   0.603477E-06   0.683711E-06  -0.836167E-04  -0.261909E+00
  mo   13  -0.236072E+00  -0.994672E-05   0.609218E+00  -0.298962E-06   0.486428E+00   0.387254E-05   0.803019E-01  -0.239150E-04
  mo   14  -0.159420E-06  -0.117750E-05   0.765363E-06   0.775223E+00  -0.292656E-06   0.227590E-05   0.122759E-06   0.832361E-06
  mo   15  -0.253919E-06   0.479628E-06   0.282905E-05   0.264355E-05   0.209487E-05  -0.666177E+00  -0.320588E-06   0.359039E-06
  mo   16   0.434124E+00   0.289283E-05  -0.426234E+00   0.731489E-06   0.570674E+00  -0.486476E-06   0.247331E+00  -0.683111E-04
  mo   17   0.668339E-06   0.228076E-01  -0.216330E-05   0.235660E-07  -0.204325E-05   0.980434E-07  -0.822832E-04  -0.262113E+00
  mo   18  -0.505756E-06   0.273990E+00   0.550946E-05   0.758071E-06   0.226528E-05   0.268087E-06   0.221881E-03   0.704763E+00
  mo   19   0.249461E+00  -0.360841E-05   0.157214E+00  -0.180865E-06  -0.252131E+00  -0.697178E-06   0.509064E+00  -0.144473E-03
  mo   20   0.152986E-06  -0.508213E-06  -0.233704E-05  -0.231069E-05  -0.220299E-05   0.739597E+00  -0.623759E-06   0.145831E-06
  mo   21   0.400274E-07   0.796893E-06  -0.398967E-06  -0.631688E+00   0.418394E-06  -0.276051E-05   0.179502E-06   0.156541E-05
  mo   22   0.287920E-01  -0.481718E-05   0.275227E+00  -0.801402E-08   0.512471E+00   0.290342E-05   0.140325E+00  -0.540679E-04
  mo   23   0.799267E-01  -0.338817E-05   0.274155E+00  -0.355216E-06  -0.281763E+00   0.475137E-06   0.655468E+00  -0.213199E-03
  mo   24  -0.592062E-06  -0.358565E+00  -0.581841E-05  -0.554245E-06  -0.144085E-05  -0.465520E-06  -0.146372E-03  -0.479289E+00

                no  17         no  18         no  19         no  20         no  21         no  22         no  23         no  24
  mo    1   0.349632E-09  -0.368376E-08  -0.147203E-04  -0.152701E-09   0.168087E-07   0.497656E-05   0.625177E-05   0.148146E-08
  mo    2  -0.441943E-06  -0.790486E-06  -0.265041E-03  -0.301578E-06   0.279788E-05   0.142182E-03  -0.247708E-04  -0.161349E-06
  mo    3   0.669566E-06   0.321521E-02   0.192453E-05   0.586272E-06   0.181737E-02  -0.111701E-04   0.885813E-06   0.252376E-02
  mo    4   0.532018E-06   0.498446E-05   0.624535E-02   0.101294E-05  -0.201865E-04  -0.367339E-02   0.588356E-02   0.429014E-06
  mo    5   0.964392E-06   0.126688E-05   0.148333E-05   0.253869E-02  -0.218627E-06  -0.682642E-06   0.212867E-05   0.107221E-05
  mo    6   0.254146E-05   0.447988E-04   0.511841E+00  -0.119934E-04  -0.327573E-03  -0.897244E-01   0.561254E+00   0.100879E-04
  mo    7   0.447996E-05   0.556995E+00  -0.498893E-04  -0.403754E-05   0.115841E+00  -0.429371E-03  -0.123057E-04   0.513316E+00
  mo    8  -0.267084E-05  -0.241988E+00   0.707466E-05   0.177234E-05   0.669534E-01  -0.260447E-03   0.434676E-05  -0.286336E+00
  mo    9  -0.721459E-06  -0.105674E-04  -0.582151E-01  -0.816496E-06   0.123286E-03   0.314222E-01  -0.455051E+00  -0.961404E-05
  mo   10  -0.421580E-06  -0.518912E-05  -0.654218E-01   0.570447E-06   0.369327E-04   0.953387E-02  -0.220926E+00  -0.365786E-05
  mo   11   0.718242E-06   0.778588E-06   0.250538E-05   0.720109E-01  -0.216046E-06   0.255875E-05  -0.177451E-06  -0.361816E-07
  mo   12  -0.119897E-05  -0.166991E+00   0.216765E-04   0.358177E-06  -0.862484E-01   0.325890E-03   0.124518E-04  -0.538009E+00
  mo   13   0.115302E-06   0.135733E-05  -0.186975E+00   0.234616E-04  -0.198519E-02  -0.509696E+00  -0.185761E+00  -0.514432E-05
  mo   14   0.631688E+00  -0.505536E-05  -0.249266E-05  -0.637818E-05  -0.101186E-05   0.151847E-05  -0.473316E-06  -0.429911E-06
  mo   15   0.657205E-05   0.655232E-05   0.243797E-04   0.736427E+00  -0.208073E-05   0.268814E-04  -0.391890E-05  -0.153710E-05
  mo   16   0.118492E-05   0.159242E-04   0.723908E-01   0.991014E-05  -0.111377E-02  -0.286642E+00   0.380761E+00   0.593410E-05
  mo   17  -0.203814E-06  -0.263699E+00  -0.217294E-04   0.550394E-05   0.921932E+00  -0.359455E-02   0.107517E-04   0.104468E+00
  mo   18   0.158036E-05   0.319544E+00  -0.543770E-04  -0.291917E-05   0.334997E+00  -0.127693E-02   0.515551E-05  -0.399271E+00
  mo   19   0.275118E-05   0.522989E-04   0.637043E+00  -0.144961E-04  -0.844307E-03  -0.226316E+00  -0.294129E+00  -0.790377E-05
  mo   20   0.696701E-05   0.658115E-05   0.235674E-04   0.672669E+00  -0.195630E-05   0.236839E-04  -0.227857E-05  -0.969211E-06
  mo   21   0.775223E+00  -0.583433E-05  -0.302346E-05  -0.716099E-05  -0.101391E-05   0.178550E-05  -0.523947E-06  -0.291796E-06
  mo   22  -0.973564E-06  -0.980556E-05   0.252562E+00  -0.361461E-04   0.295514E-02   0.757204E+00  -0.627613E-01   0.606441E-06
  mo   23  -0.207409E-05  -0.433452E-04  -0.469573E+00   0.124294E-04   0.576980E-03   0.155924E+00   0.398928E+00   0.769344E-05
  mo   24   0.580375E-05   0.657006E+00  -0.475637E-04  -0.637807E-05   0.111623E+00  -0.416373E-03   0.957385E-05  -0.441279E+00
  eigenvectors of nos in ao-basis, column    1
  0.1000471E+01 -0.3582950E-02 -0.1835547E-02 -0.1663038E-07  0.2403930E-07 -0.6432917E-02  0.3650658E-07 -0.3490024E-07
  0.1190835E-02  0.2009167E-08 -0.4381429E-08  0.2898196E-04  0.3537162E-08  0.8797579E-04 -0.3083809E-03  0.6906893E-03
 -0.6259178E-03  0.4101242E-08 -0.6334907E-03 -0.3083978E-03  0.6907460E-03  0.6259092E-03  0.5382459E-08 -0.6335483E-03
  eigenvectors of nos in ao-basis, column    2
 -0.3273569E-02  0.9083896E+00 -0.8482182E-01 -0.2994812E-06 -0.7684407E-06  0.6049653E-01 -0.3965348E-05  0.1037519E-05
  0.7030041E-01 -0.2560579E-06  0.2030889E-06 -0.7830217E-03 -0.3636233E-06  0.9785590E-03  0.2519522E+00 -0.1232884E+00
  0.3072051E-01  0.4735587E-06  0.2136945E-01  0.2519671E+00 -0.1232966E+00 -0.3072284E-01 -0.3637930E-06  0.2136765E-01
  eigenvectors of nos in ao-basis, column    3
 -0.4798033E-06 -0.3907247E-05 -0.1070986E-05  0.7973359E-06  0.9162250E+00 -0.4168885E-05 -0.1528557E-05  0.7761927E-01
  0.1462956E-05 -0.4903551E-06 -0.1673425E-01 -0.2299923E-07 -0.8490052E-07 -0.2186505E-06 -0.2610191E-06  0.6740477E-05
  0.1227116E-05  0.2876725E-01 -0.1816078E-06  0.2830015E-05 -0.1518118E-05 -0.7033092E-06  0.2876879E-01 -0.5479084E-06
  eigenvectors of nos in ao-basis, column    4
  0.4041325E-02  0.8852085E-01  0.2000888E+00 -0.2116826E-04  0.5237198E-05  0.8077152E+00  0.6807229E-06  0.1379407E-05
 -0.9805768E-02 -0.1498260E-06  0.7973406E-06 -0.4742341E-02  0.6912177E-06 -0.2562698E-02 -0.4077219E+00  0.1747494E+00
 -0.3823903E-01 -0.1704257E-05  0.6780832E-03 -0.4077873E+00  0.1747657E+00  0.3824792E-01 -0.6400849E-06  0.6903103E-03
  eigenvectors of nos in ao-basis, column    5
  0.8480219E-06  0.2343239E-05  0.2231942E-05  0.7211340E+00  0.3134245E-05  0.4346041E-04 -0.1053807E+00 -0.1522946E-05
 -0.3111575E-05  0.4703732E-06 -0.1104344E-06 -0.2969532E-06 -0.2622986E-01  0.1490198E-06 -0.5515171E+00  0.1979795E+00
 -0.2406378E-01 -0.1458720E-05 -0.3352491E-01  0.5515000E+00 -0.1979673E+00 -0.2406220E-01  0.2032616E-06  0.3351600E-01
  eigenvectors of nos in ao-basis, column    6
  0.2842789E-06  0.8764838E-05  0.1117633E-04 -0.1312560E+01 -0.1385132E-05 -0.3177412E-04  0.6706686E+00  0.2741998E-05
  0.3000195E-04 -0.1358350E-06 -0.1028070E-06 -0.3543988E-06 -0.3952429E-01 -0.4758763E-06 -0.8818200E+00  0.2581596E+00
 -0.1365262E-01  0.4284028E-06  0.1716320E-02  0.8817633E+00 -0.2581394E+00 -0.1364912E-01  0.1140919E-06 -0.1712297E-02
  eigenvectors of nos in ao-basis, column    7
 -0.1805200E-02  0.2339292E+00  0.3156053E+00  0.4982669E-04 -0.1804366E-05 -0.1351579E+01 -0.2423095E-04  0.3019147E-05
  0.1070207E+01  0.9425016E-08 -0.3052110E-06 -0.7975547E-02  0.8344811E-06 -0.1303310E-01 -0.6917930E+00  0.2430703E+00
 -0.3455144E-01  0.4793810E-06  0.4073257E-01 -0.6918349E+00  0.2430824E+00  0.3455093E-01  0.3194434E-06  0.4073108E-01
  eigenvectors of nos in ao-basis, column    8
 -0.2135929E-06 -0.3978050E-07  0.1227198E-05 -0.3217205E-05  0.1408833E+01 -0.3075876E-05  0.1218002E-05 -0.1610330E+01
  0.2625022E-05 -0.5796307E-07  0.3811673E-01 -0.1716485E-07 -0.6818656E-07 -0.5531031E-07 -0.2313506E-05  0.1446558E-05
  0.7445885E-07 -0.7610407E-01  0.1148348E-06 -0.2932603E-07 -0.5402666E-08 -0.3343056E-07 -0.7610392E-01  0.4598731E-08
  eigenvectors of nos in ao-basis, column    9
 -0.4485840E+00 -0.2371377E+01  0.2039913E+01 -0.5205663E-05 -0.1845466E-05 -0.3500014E+00  0.5296664E-05  0.7159633E-06
  0.8317190E+00  0.6347232E-07  0.2276469E-06 -0.1245151E-01  0.7183490E-06  0.7467507E-01  0.7178265E+00 -0.3901529E+00
  0.3782897E-02 -0.2735034E-06  0.1031056E+00  0.7178222E+00 -0.3901510E+00 -0.3782143E-02 -0.8522014E-07  0.1031068E+00
  eigenvectors of nos in ao-basis, column   10
 -0.2529600E-05 -0.7470931E-05  0.1632875E-04 -0.3935922E+00  0.1799106E-05 -0.4924739E-05  0.1045453E+01 -0.4488213E-06
  0.1104816E-05  0.1006291E-05 -0.6384301E-06  0.1631631E-05  0.5622678E+00 -0.3295382E-05  0.8517104E+00 -0.6195106E+00
  0.1197817E+00 -0.2894700E-06 -0.2575103E-01 -0.8517281E+00  0.6195234E+00  0.1197782E+00  0.7191568E-06  0.2574185E-01
  eigenvectors of nos in ao-basis, column   11
  0.3413489E+00  0.1520100E+01 -0.2081167E+01 -0.1278727E-05  0.2749672E-05  0.2931802E-01  0.1420680E-04 -0.1979523E-05
  0.1872946E+00 -0.5264720E-06 -0.2937996E-05 -0.8398665E-01  0.9250356E-05  0.2432921E+00  0.7578644E+00 -0.4377312E+00
 -0.7618084E-01  0.1674781E-05  0.2235338E+00  0.7578410E+00 -0.4377131E+00  0.7618272E-01  0.9284766E-06  0.2235305E+00
  eigenvectors of nos in ao-basis, column   12
 -0.4538864E-06 -0.2626477E-05  0.2903434E-05  0.1148547E-05  0.6188989E-06  0.3423137E-06  0.3520917E-06 -0.1456593E-05
 -0.5391063E-06 -0.7772842E+00 -0.2845879E-05 -0.2473785E-07  0.9138021E-06 -0.2858226E-06 -0.5330430E-06 -0.5119222E-06
  0.1776404E-06  0.2912254E+00 -0.8319948E-07 -0.1760446E-05  0.1674090E-05  0.3345878E-06 -0.2912231E+00 -0.1337999E-06
  eigenvectors of nos in ao-basis, column   13
 -0.1431711E+00 -0.5945797E+00  0.1003625E+01  0.2557986E-05 -0.1951112E-06  0.8729469E-01  0.1067584E-05 -0.9910080E-06
 -0.5657352E+00  0.4863629E-06 -0.2631772E-05 -0.2077259E+00  0.2448789E-05 -0.1932580E+00 -0.5592914E+00  0.3081913E+00
 -0.1838222E+00  0.6657311E-06  0.1174076E+00 -0.5592955E+00  0.3081962E+00  0.1838217E+00  0.7484676E-06  0.1174041E+00
  eigenvectors of nos in ao-basis, column   14
  0.1564484E-05  0.6693040E-05 -0.9241655E-05 -0.7665911E-06 -0.8847544E-01 -0.2059944E-05  0.1128138E-05  0.3188646E+00
  0.3561703E-06 -0.3253437E-05  0.8803688E+00 -0.1022477E-05  0.6436598E-06  0.4471972E-06  0.8120653E-06  0.1566695E-05
 -0.5587639E-06 -0.2134891E+00  0.7816783E-06  0.7163896E-06  0.2135951E-06  0.8765738E-06 -0.2134901E+00  0.8421045E-06
  eigenvectors of nos in ao-basis, column   15
 -0.3515548E+00 -0.1343918E+01  0.2472753E+01  0.2022274E-03 -0.8624582E-06  0.1600157E+00 -0.3792459E-03  0.5247006E-06
 -0.1065569E+01  0.1766510E-06 -0.5861767E-06 -0.3005556E-01  0.2224786E-03  0.4151894E+00 -0.2059657E+01  0.1176337E+01
  0.9994802E-02 -0.3735680E-06 -0.8479665E-01 -0.2058986E+01  0.1176039E+01 -0.9968859E-02 -0.6788691E-06 -0.8501852E-01
  eigenvectors of nos in ao-basis, column   16
  0.9831112E-04  0.3690621E-03 -0.7217802E-03  0.6266732E+00 -0.5827747E-06 -0.2463355E-04 -0.1204357E+01 -0.2959109E-06
  0.3093602E-03  0.1571247E-05  0.1061457E-06  0.1232069E-04  0.7241281E+00 -0.1338487E-03 -0.1106466E+01  0.5185211E+00
  0.3428305E-01  0.1484904E-05  0.3487096E+00  0.1107772E+01 -0.5192948E+00  0.3427697E-01 -0.8506641E-06 -0.3486268E+00
  eigenvectors of nos in ao-basis, column   17
 -0.1681653E-05 -0.7691549E-05  0.4864187E-05  0.7161063E-05  0.1509391E-05  0.4319437E-05  0.2286037E-06 -0.7300989E-05
 -0.2626155E-05  0.7498214E+00  0.6214270E-05  0.2620747E-06 -0.7035822E-05 -0.9832986E-06  0.1956319E-04 -0.1922153E-04
  0.6134797E-05  0.7289340E+00  0.5868191E-05 -0.1070673E-04  0.1028260E-04  0.1794904E-05 -0.7289181E+00 -0.3885182E-06
  eigenvectors of nos in ao-basis, column   18
 -0.2591390E-04 -0.1176009E-03  0.3607372E-04  0.9199918E+00  0.1870915E-05  0.7754562E-04  0.8567638E-01 -0.7244561E-05
 -0.2919373E-04 -0.5604320E-05  0.5808129E-05  0.2396006E-05 -0.7707031E+00 -0.2020722E-04  0.1874666E+01 -0.1735319E+01
  0.4309466E+00  0.2079906E-05  0.6050061E+00 -0.1874444E+01  0.1735140E+01  0.4308437E+00  0.1346519E-04 -0.6049028E+00
  eigenvectors of nos in ao-basis, column   19
 -0.2400159E+00 -0.1153912E+01 -0.1486091E+00 -0.1070803E-03  0.3434389E-05  0.8514639E+00  0.7393734E-04 -0.2623123E-04
  0.1005413E+00 -0.2920652E-05  0.2070992E-04 -0.7595250E-01  0.5287577E-04 -0.1841192E+00  0.1660463E+01 -0.1169967E+01
  0.8626770E+00  0.2557906E-04  0.3683927E+00  0.1660668E+01 -0.1170203E+01 -0.8628197E+00  0.3130466E-04  0.3684588E+00
  eigenvectors of nos in ao-basis, column   20
 -0.1714824E-05  0.6547565E-06  0.6711972E-04 -0.7206211E-05  0.5944980E-01 -0.1801583E-04 -0.7062333E-05 -0.7765539E+00
 -0.4757319E-04 -0.6856336E-05  0.5853513E+00  0.1035692E-04  0.7559010E-05  0.2646005E-05 -0.1055004E-03  0.6155769E-04
 -0.4630445E-04  0.8418205E+00  0.2926101E-05 -0.6468517E-04  0.3310176E-04  0.4009781E-04  0.8418347E+00  0.1927971E-04
  eigenvectors of nos in ao-basis, column   21
  0.8518064E-03  0.3372499E-02 -0.4519235E-02  0.5360538E+00 -0.3804059E-06 -0.1175472E-02 -0.1173442E+01  0.2218068E-05
  0.4218898E-02 -0.9567515E-06 -0.1708211E-05 -0.8317761E-03 -0.1278517E+00  0.4938656E-03 -0.3957955E+00  0.5595867E-01
  0.7352447E+00 -0.3488701E-05 -0.4028617E+00  0.4005539E+00 -0.5787057E-01  0.7328534E+00 -0.1327151E-05  0.3972635E+00
  eigenvectors of nos in ao-basis, column   22
  0.2213585E+00  0.8793471E+00 -0.1149283E+01 -0.2041004E-02  0.1352952E-05 -0.3131199E+00  0.4544729E-02 -0.2800330E-04
  0.1081951E+01  0.1718806E-05  0.2047133E-04 -0.2130375E+00  0.4787855E-03  0.1301939E+00  0.5878515E+00 -0.2316114E+00
  0.2917722E+00  0.3207253E-04 -0.7255519E+00  0.5846550E+00 -0.2310704E+00 -0.2974374E+00  0.2862756E-04 -0.7287187E+00
  eigenvectors of nos in ao-basis, column   23
 -0.3292088E+00 -0.1132668E+01  0.3986587E+01  0.4386833E-05  0.1951402E-05 -0.3348054E+00  0.2866945E-04  0.3519884E-05
 -0.1003990E+01 -0.5007993E-06 -0.1794538E-05  0.2008095E-01 -0.1367988E-04  0.2033036E+00 -0.1260001E+01 -0.4226252E+00
 -0.7512414E+00 -0.4406476E-05 -0.5171330E+00 -0.1260031E+01 -0.4226796E+00  0.7512798E+00 -0.3357511E-05 -0.5171399E+00
  eigenvectors of nos in ao-basis, column   24
 -0.4837392E-05 -0.1587077E-04  0.6968820E-04 -0.3458672E+00  0.1029979E-05 -0.8343078E-05 -0.1682396E+01  0.1365746E-05
 -0.1422418E-04 -0.2574643E-06 -0.7873000E-06 -0.1305258E-06  0.5941520E+00  0.3908650E-05 -0.1057102E+01 -0.1135811E+01
 -0.7109357E+00 -0.1951880E-05 -0.6708918E+00  0.1057061E+01  0.1135791E+01 -0.7109064E+00 -0.1139438E-05  0.6708667E+00


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***


 cosurf_and_phi
  2.466315385316 -0.061539892486  2.837440149000 -0.038332470060
  0.006359863210 -2.199287675687  3.114544749440 -0.067617497374
 -0.480526931872  1.061509344774  3.755560521090 -0.069523268200
  1.262739748330  2.872870034186  1.567866969040 -0.045257550728
  1.897949994968 -2.634366215369  0.570962659525 -0.016047130201
 -2.790166444607 -0.824331206483  2.170430257018 -0.022154451817
 -2.061296060778  2.493528161233  1.034251636268 -0.022215256056
 -0.002991703587  2.420846177436 -1.447622679829  0.014577083741
 -1.114435211431 -2.949559801670 -0.067898604227 -0.016060998894
  1.955709175590 -0.984462672186  3.123501329999 -0.050884590991
  1.000900154469 -1.706149424342  3.300403738319 -0.064332634068
  1.652947872482  0.360731428865  3.496571379926 -0.057769027365
  0.594940552015  0.670555958619  3.845537291721 -0.069257816264
  2.390078334690  1.202722009900  2.566708449184 -0.037518823473
  1.989334673621  2.229216060677  2.001028520754 -0.039525326616
  2.919143888229  0.378144896597  2.099775822683 -0.017033335319
  2.780367109803 -0.921046401167  2.130496470151 -0.021863692123
  2.449941849009 -2.011800733381  1.439000205618 -0.019855625667
 -0.231540571447 -1.263621120083  3.706953994699 -0.070122809698
 -0.352106389130 -0.107618694495  3.950679056227 -0.070490957959
  0.105380882100  1.878884201281  3.371423292662 -0.068952008771
  0.783266721831  2.590884380442  2.520834408126 -0.060568431039
  2.011402526412  2.551496219550  0.814855178005 -0.018806951909
  1.440220538007 -2.843720084632  1.356653310131 -0.039760375091
  0.871209499702 -2.660587439486  2.372618950166 -0.058557995048
 -2.947488997374  0.298617382934  2.058332654596 -0.015414664856
 -2.674954162172  1.563027715274  1.704200253745 -0.018065953242
 -1.353448939749  2.910920816403  0.212056013565 -0.018989573022
 -0.743422400795  2.810699746106 -0.731960510989 -0.003151680637
  0.099626804209  1.855073908563 -1.945822518898  0.033616324505
  0.019900458911 -1.968682238442 -1.864952523830  0.030158807585
 -0.601526683996 -2.669374282549 -1.032942810695  0.004385590648
 -1.976701730993 -2.578422698078  0.816296596419 -0.019690858055
 -2.525639241426 -1.850752473194  1.593331825886 -0.020537317191
 -1.124962231191 -1.881571875050  3.121019991266 -0.062173884163
 -2.117398618237 -1.513942036836  2.667866282109 -0.044315548439
 -2.336046769995 -0.152318885910  2.976109389266 -0.042404083185
 -1.478775916646  0.469153703894  3.577445396081 -0.060488261581
 -1.328587214463  1.668007282102  3.174271253825 -0.060314233863
 -1.771761195387  2.287385908353  2.202255939279 -0.045879272107
 -1.026730149698  3.017318998523  1.358608629268 -0.044864515423
  0.119020599620  3.173161356543  1.415159765853 -0.050678049654
  0.915237473358  3.108118624501  0.463299650838 -0.030392549470
  0.462814589486  2.837210699514 -0.795516582628 -0.004010712509
  1.026812912753 -3.017319139618  0.083991013862 -0.020919231729
 -0.070059393274 -3.176418651270  0.035610954337 -0.026200645133
 -0.970268948715 -3.083313212042  1.062474740253 -0.040778499357
 -0.534203029846 -2.828286137107  2.231267851728 -0.059284253752
  0.871897873699 -0.575451888002  3.799144800425 -0.067395470115
  1.408719892640  1.640288870679  3.148120355694 -0.059197330483
  2.650053602975  1.633766196698  1.655441764425 -0.017948505734
  1.964286464189 -2.001027831890  2.365093852582 -0.044257709527
 -1.342877128538 -0.760711330777  3.581800927521 -0.062164915867
 -0.531365927285  2.661712102822  2.509437292848 -0.061931647178
  1.142904167226  2.885766749848 -0.243475252462 -0.011107835988
  0.342129526197 -3.189261637688  1.246854200824 -0.047743917847
 -2.346459445268  1.135413320199  2.662806558936 -0.039459644898
 -0.258565605057  3.205542092831  0.249849913336 -0.030383126660
  0.450566961335 -2.699565911226 -1.032013446782  0.002939030014
 -1.684533476150 -2.430522251364  2.070179818920 -0.045783378824
 -3.368784049130  0.036613212616 -1.854972615293  0.062123213526
 -3.278396927799  1.580834404538 -0.078603229040  0.034127502718
 -3.656110378332 -0.713812229513  0.361831391088  0.034788986269
 -2.408052717333 -2.075622947728 -1.226189024363  0.039600217307
 -1.295829484895 -0.567704086865 -2.747607986898  0.059655519128
 -1.846557165647  1.681693338816 -2.099716493181  0.049862895531
 -3.831862607217  0.357468890277 -0.654813288033  0.051792349907
 -3.527736967644 -1.045671231370 -1.064852892071  0.053043935786
 -2.622906831544 -1.024738705101 -2.241124222290  0.059720836867
 -2.402781990555  0.378472303440 -2.579763034114  0.063061182986
 -3.126606927219  1.313275381963 -1.541857021673  0.054655087813
 -3.633182667875  0.531520753705  0.561856665829  0.031569056507
 -3.086813000261 -1.794874586082 -0.179700355757  0.032164660479
 -1.615880090830 -1.674393887320 -2.147504235692  0.048959422178
 -1.240258025012  0.765881087348 -2.687977655831  0.058358894457
 -2.430292517958  2.154437082790 -0.969924007583  0.035016168993
  3.368776503197 -0.036639040867 -1.854966842961  0.062114665731
  3.278392620256 -1.580850766330 -0.078589062660  0.034121781811
  3.656106873706  0.713798214804  0.361832640492  0.034783282027
  2.408046317685  2.075600470298 -1.226192756897  0.039592157839
  1.295820311657  0.567673501678 -2.747601655947  0.059647549150
  1.846549173548 -1.681720471299 -2.099699178989  0.049855244832
  3.831857249217 -0.357488322766 -0.654806650060  0.051785339995
  3.527730862121  1.045649613724 -1.064853177123  0.053036259816
  2.622898581638  1.024710819001 -2.241122746235  0.059711880960
  2.402773123306 -0.378501994157 -2.579753678917  0.063052131628
  3.126599952113 -1.313299541573 -1.541844004398  0.054647087156
  3.633179527908 -0.531533702443  0.561864593522  0.031563987962
  3.086808508398  1.794857685487 -0.179703829578  0.032157916727
  1.615872011596  1.674366500124 -2.147504385867  0.048951058875
  1.240248960489 -0.765911354740 -2.687964116774  0.058351506804
  2.430286585511 -2.154458194501 -0.969905238283  0.035009705803
  0.000006852884  1.035694667087 -2.361676372823  0.052324127158
 -0.298806015181  0.969607486526 -2.408809074493  0.053873525320
  0.298815704890  0.969607820141 -2.408807386530  0.053870845225
  0.000007656756 -1.035723195601 -2.361670853435  0.052324715726
 -0.298805262603 -0.969636498141 -2.408803907288  0.053874122858
  0.298816457468 -0.969636367899 -2.408802219325  0.053871294884
 -0.667712940797 -1.245186997899 -2.338574529312  0.051505909611
 -1.218112188165 -2.025138759161 -1.616566947615  0.036578758618
 -2.084175601108 -2.294008802021 -0.480477682822  0.020802048527
 -2.876631896395 -1.658047550445  0.559052047065  0.017301972205
 -3.282909221331 -0.368099862162  1.091996180628  0.019297794099
 -3.142757910518  1.067034798172  0.908143333087  0.018320827018
 -2.511458431963  2.081290260960  0.080011352455  0.017686628386
 -1.638016880752  2.274609499719 -1.065756198713  0.027240804813
 -0.866948462969  1.570740798135 -2.077229430584  0.047324859025
 -0.467915446486 -1.694563900014 -1.941501402531  0.039855718821
 -1.299753513329 -2.453901457341 -0.850306853788  0.012656582461
 -2.242033801688 -2.245340287831  0.385760998463  0.000687829478
 -2.923088754321 -1.151144046328  1.279154738758  0.002448398349
 -3.074287016292  0.397098864814  1.477489335582  0.003997178181
 -2.635990824421  1.788708515315  0.902534843950  0.000662174693
 -1.781079179779  2.474786487342 -0.218927030025  0.004386440156
 -0.846758459860  2.184720175398 -1.444553386477  0.025047995435
 -0.255852300976  1.360631011730 -2.219692209228  0.046790645135
  0.667723897920  1.245157743773 -2.338577856151  0.051500064896
  1.218123388571  2.025110412626 -1.616576841774  0.036571175668
  2.084186643139  2.293984793080 -0.480493513306  0.020795098346
  2.876642520254  1.658030225134  0.559035126479  0.017296773054
  3.282919570196  0.368088739237  1.091983758387  0.019293922688
  3.142768358070 -1.067043033853  0.908139383421  0.018317343461
  2.511469270857 -2.081300494445  0.080016452498  0.017682500289
  1.638028059662 -2.274626132712 -1.065745290088  0.027235486567
  0.866959534092 -1.570765766224 -2.077218589767  0.047319393983
  0.467932162408  1.694535407938 -1.941510815499  0.039851184628
  1.299770347024  2.453875604722 -0.850324284820  0.012650403443
  2.242050270258  2.245320705184  0.385739526123  0.000682781149
  2.923104801922  1.151131828431  1.279135167181  0.002445030957
  3.074302957696 -0.397105856761  1.477477125466  0.003994913814
  2.636007061705 -1.788714946455  0.902532611959  0.000660166042
  1.781095866662 -2.474797662024 -0.218920775640  0.004383674763
  0.846775315928 -2.184739733041 -1.444543821640  0.025044802957
  0.255868904327 -1.360657816862 -2.219684436601  0.046789278145
  0.427045418766 -0.580992742084 -2.585105089006  0.057622419044
  0.427044967836  0.580963354323 -2.585108185092  0.057622194048
 -0.427035838172 -0.000014913339 -2.543076305315  0.062898569735
 end_of_phi


 nsubv after electrostatic potential =  0

  Confirmation of dielectric energy for ground state
  edielnew =  -0.0111934298


 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************


 cosurf_and_qcos_from cosmo(3
  2.466315385316 -0.061539892486  2.837440149000  0.002388357824
  0.006359863210 -2.199287675687  3.114544749440  0.006272348564
 -0.480526931872  1.061509344774  3.755560521090  0.005773772852
  1.262739748330  2.872870034186  1.567866969040  0.005069247440
  1.897949994968 -2.634366215369  0.570962659525  0.001889798738
 -2.790166444607 -0.824331206483  2.170430257018  0.001102533070
 -2.061296060778  2.493528161233  1.034251636268  0.002924467735
 -0.002991703587  2.420846177436 -1.447622679829  0.000809485971
 -1.114435211431 -2.949559801670 -0.067898604227  0.003003543760
  1.955709175590 -0.984462672186  3.123501329999  0.004762800944
  1.000900154469 -1.706149424342  3.300403738319  0.006153402316
  1.652947872482  0.360731428865  3.496571379926  0.005322542139
  0.594940552015  0.670555958619  3.845537291721  0.005653139871
  2.390078334690  1.202722009900  2.566708449184  0.003525036366
  1.989334673621  2.229216060677  2.001028520754  0.003528779535
  2.919143888229  0.378144896597  2.099775822683  0.000557613790
  2.780367109803 -0.921046401167  2.130496470151  0.001580023545
  2.449941849009 -2.011800733381  1.439000205618  0.001941478445
 -0.231540571447 -1.263621120083  3.706953994699  0.006550600319
 -0.352106389130 -0.107618694495  3.950679056227  0.006315490741
  0.105380882100  1.878884201281  3.371423292662  0.008115299485
  0.783266721831  2.590884380442  2.520834408126  0.007217783631
  2.011402526412  2.551496219550  0.814855178005  0.002422881019
  1.440220538007 -2.843720084632  1.356653310131  0.004647108836
  0.871209499702 -2.660587439486  2.372618950166  0.007787278288
 -2.947488997374  0.298617382934  2.058332654596  0.000521272814
 -2.674954162172  1.563027715274  1.704200253745  0.001233346277
 -1.353448939749  2.910920816403  0.212056013565  0.003360458128
 -0.743422400795  2.810699746106 -0.731960510989  0.002296228450
  0.099626804209  1.855073908563 -1.945822518898 -0.000118377392
  0.019900458911 -1.968682238442 -1.864952523830 -0.000341245350
 -0.601526683996 -2.669374282549 -1.032942810695  0.001363944242
 -1.976701730993 -2.578422698078  0.816296596419  0.002587005185
 -2.525639241426 -1.850752473194  1.593331825886  0.001758135529
 -1.124962231191 -1.881571875050  3.121019991266  0.006052288720
 -2.117398618237 -1.513942036836  2.667866282109  0.004051542727
 -2.336046769995 -0.152318885910  2.976109389266  0.004257612398
 -1.478775916646  0.469153703894  3.577445396081  0.005229451848
 -1.328587214463  1.668007282102  3.174271253825  0.006216518378
 -1.771761195387  2.287385908353  2.202255939279  0.005924707926
 -1.026730149698  3.017318998523  1.358608629268  0.005803583915
  0.119020599620  3.173161356543  1.415159765853  0.007122408221
  0.915237473358  3.108118624501  0.463299650838  0.004939411289
  0.462814589486  2.837210699514 -0.795516582628  0.002400243182
  1.026812912753 -3.017319139618  0.083991013862  0.004137314137
 -0.070059393274 -3.176418651270  0.035610954337  0.004739417786
 -0.970268948715 -3.083313212042  1.062474740253  0.006650512133
 -0.534203029846 -2.828286137107  2.231267851728  0.008068238065
  0.871897873699 -0.575451888002  3.799144800425  0.006583288798
  1.408719892640  1.640288870679  3.148120355694  0.007120708735
  2.650053602975  1.633766196698  1.655441764425  0.001121259710
  1.964286464189 -2.001027831890  2.365093852582  0.004828850277
 -1.342877128538 -0.760711330777  3.581800927521  0.005438594270
 -0.531365927285  2.661712102822  2.509437292848  0.008127417225
  1.142904167226  2.885766749848 -0.243475252462  0.001421515906
  0.342129526197 -3.189261637688  1.246854200824  0.006934094388
 -2.346459445268  1.135413320199  2.662806558936  0.003635722018
 -0.258565605057  3.205542092831  0.249849913336  0.004557244500
  0.450566961335 -2.699565911226 -1.032013446782  0.002441544166
 -1.684533476150 -2.430522251364  2.070179818920  0.005068964160
 -3.368784049130  0.036613212616 -1.854972615293 -0.007912120013
 -3.278396927799  1.580834404538 -0.078603229040 -0.005844741254
 -3.656110378332 -0.713812229513  0.361831391088 -0.006545687711
 -2.408052717333 -2.075622947728 -1.226189024363 -0.005504058629
 -1.295829484895 -0.567704086865 -2.747607986898 -0.006014618727
 -1.846557165647  1.681693338816 -2.099716493181 -0.006189888122
 -3.831862607217  0.357468890277 -0.654813288033 -0.008531242293
 -3.527736967644 -1.045671231370 -1.064852892071 -0.008443755984
 -2.622906831544 -1.024738705101 -2.241124222290 -0.008364360848
 -2.402781990555  0.378472303440 -2.579763034114 -0.008134722829
 -3.126606927219  1.313275381963 -1.541857021673 -0.008599547866
 -3.633182667875  0.531520753705  0.561856665829 -0.004584201205
 -3.086813000261 -1.794874586082 -0.179700355757 -0.004581194480
 -1.615880090830 -1.674393887320 -2.147504235692 -0.004721801890
 -1.240258025012  0.765881087348 -2.687977655831 -0.004962136742
 -2.430292517958  2.154437082790 -0.969924007583 -0.004153649759
  3.368776503197 -0.036639040867 -1.854966842961 -0.007917794159
  3.278392620256 -1.580850766330 -0.078589062660 -0.005829250315
  3.656106873706  0.713798214804  0.361832640492 -0.006540167754
  2.408046317685  2.075600470298 -1.226192756897 -0.005512072103
  1.295820311657  0.567673501678 -2.747601655947 -0.006436402768
  1.846549173548 -1.681720471299 -2.099699178989 -0.006142630962
  3.831857249217 -0.357488322766 -0.654806650060 -0.008530508733
  3.527730862121  1.045649613724 -1.064853177123 -0.008446593111
  2.622898581638  1.024710819001 -2.241122746235 -0.008350046837
  2.402773123306 -0.378501994157 -2.579753678917 -0.008125157590
  3.126599952113 -1.313299541573 -1.541844004398 -0.008603512680
  3.633179527908 -0.531533702443  0.561864593522 -0.004579705598
  3.086808508398  1.794857685487 -0.179703829578 -0.004578209029
  1.615872011596  1.674366500124 -2.147504385867 -0.004687475241
  1.240248960489 -0.765911354740 -2.687964116774 -0.005220447655
  2.430286585511 -2.154458194501 -0.969905238283 -0.004139129027
  0.000006852884  1.035694667087 -2.361676372823 -0.000280151011
 -0.298806015181  0.969607486526 -2.408809074493 -0.000339317566
  0.298815704890  0.969607820141 -2.408807386530 -0.000248828713
  0.000007656756 -1.035723195601 -2.361670853435 -0.000273579019
 -0.298805262603 -0.969636498141 -2.408803907288 -0.000324530735
  0.298816457468 -0.969636367899 -2.408802219325 -0.000245983615
 -0.667712940797 -1.245186997899 -2.338574529312 -0.000942466965
 -1.218112188165 -2.025138759161 -1.616566947615 -0.001409125406
 -2.084175601108 -2.294008802021 -0.480477682822 -0.000935663129
 -2.876631896395 -1.658047550445  0.559052047065 -0.001348168485
 -3.282909221331 -0.368099862162  1.091996180628 -0.001787611150
 -3.142757910518  1.067034798172  0.908143333087 -0.001604678566
 -2.511458431963  2.081290260960  0.080011352455 -0.001161881902
 -1.638016880752  2.274609499719 -1.065756198713 -0.001127708490
 -0.866948462969  1.570740798135 -2.077229430584 -0.001954264170
 -0.467915446486 -1.694563900014 -1.941501402531 -0.001392283819
 -1.299753513329 -2.453901457341 -0.850306853788 -0.000218506158
 -2.242033801688 -2.245340287831  0.385760998463 -0.000283894613
 -2.923088754321 -1.151144046328  1.279154738758 -0.000845819781
 -3.074287016292  0.397098864814  1.477489335582 -0.001120079261
 -2.635990824421  1.788708515315  0.902534843950 -0.000490477729
 -1.781079179779  2.474786487342 -0.218927030025 -0.000157630632
 -0.846758459860  2.184720175398 -1.444553386477 -0.000819731585
 -0.255852300976  1.360631011730 -2.219692209228 -0.000897851804
  0.667723897920  1.245157743773 -2.338577856151 -0.000875692080
  1.218123388571  2.025110412626 -1.616576841774 -0.001421386210
  2.084186643139  2.293984793080 -0.480493513306 -0.000912883429
  2.876642520254  1.658030225134  0.559035126479 -0.001341666622
  3.282919570196  0.368088739237  1.091983758387 -0.001776734385
  3.142768358070 -1.067043033853  0.908139383421 -0.001595560572
  2.511469270857 -2.081300494445  0.080016452498 -0.001101265626
  1.638028059662 -2.274626132712 -1.065745290088 -0.001085642625
  0.866959534092 -1.570765766224 -2.077218589767 -0.001903324343
  0.467932162408  1.694535407938 -1.941510815499 -0.001448220741
  1.299770347024  2.453875604722 -0.850324284820 -0.000228458300
  2.242050270258  2.245320705184  0.385739526123 -0.000212688464
  2.923104801922  1.151131828431  1.279135167181 -0.000874724446
  3.074302957696 -0.397105856761  1.477477125466 -0.001147707174
  2.636007061705 -1.788714946455  0.902532611959 -0.000535717314
  1.781095866662 -2.474797662024 -0.218920775640 -0.000214388154
  0.846775315928 -2.184739733041 -1.444543821640 -0.000872572647
  0.255868904327 -1.360657816862 -2.219684436601 -0.000831932933
  0.427045418766 -0.580992742084 -2.585105089006 -0.002212147030
  0.427044967836  0.580963354323 -2.585108185092 -0.002082327326
 -0.427035838172 -0.000014913339 -2.543076305315 -0.005970597047
 end_of_qcos

 sum of the negative charges =  -0.256042326

 sum of the positive charges =   0.251307661

 total sum =  -0.0047346657

 *** qcos is copied to qcosdalton ***

 fepsi =   1.
 *** qcosdalton is mult by fepsi ***
 cosurf and qcosdalton
  2.466315385316 -0.061539892486  2.837440149000  0.002388357824
  0.006359863210 -2.199287675687  3.114544749440  0.006272348564
 -0.480526931872  1.061509344774  3.755560521090  0.005773772852
  1.262739748330  2.872870034186  1.567866969040  0.005069247440
  1.897949994968 -2.634366215369  0.570962659525  0.001889798738
 -2.790166444607 -0.824331206483  2.170430257018  0.001102533070
 -2.061296060778  2.493528161233  1.034251636268  0.002924467735
 -0.002991703587  2.420846177436 -1.447622679829  0.000809485971
 -1.114435211431 -2.949559801670 -0.067898604227  0.003003543760
  1.955709175590 -0.984462672186  3.123501329999  0.004762800944
  1.000900154469 -1.706149424342  3.300403738319  0.006153402316
  1.652947872482  0.360731428865  3.496571379926  0.005322542139
  0.594940552015  0.670555958619  3.845537291721  0.005653139871
  2.390078334690  1.202722009900  2.566708449184  0.003525036366
  1.989334673621  2.229216060677  2.001028520754  0.003528779535
  2.919143888229  0.378144896597  2.099775822683  0.000557613790
  2.780367109803 -0.921046401167  2.130496470151  0.001580023545
  2.449941849009 -2.011800733381  1.439000205618  0.001941478445
 -0.231540571447 -1.263621120083  3.706953994699  0.006550600319
 -0.352106389130 -0.107618694495  3.950679056227  0.006315490741
  0.105380882100  1.878884201281  3.371423292662  0.008115299485
  0.783266721831  2.590884380442  2.520834408126  0.007217783631
  2.011402526412  2.551496219550  0.814855178005  0.002422881019
  1.440220538007 -2.843720084632  1.356653310131  0.004647108836
  0.871209499702 -2.660587439486  2.372618950166  0.007787278288
 -2.947488997374  0.298617382934  2.058332654596  0.000521272814
 -2.674954162172  1.563027715274  1.704200253745  0.001233346277
 -1.353448939749  2.910920816403  0.212056013565  0.003360458128
 -0.743422400795  2.810699746106 -0.731960510989  0.002296228450
  0.099626804209  1.855073908563 -1.945822518898 -0.000118377392
  0.019900458911 -1.968682238442 -1.864952523830 -0.000341245350
 -0.601526683996 -2.669374282549 -1.032942810695  0.001363944242
 -1.976701730993 -2.578422698078  0.816296596419  0.002587005185
 -2.525639241426 -1.850752473194  1.593331825886  0.001758135529
 -1.124962231191 -1.881571875050  3.121019991266  0.006052288720
 -2.117398618237 -1.513942036836  2.667866282109  0.004051542727
 -2.336046769995 -0.152318885910  2.976109389266  0.004257612398
 -1.478775916646  0.469153703894  3.577445396081  0.005229451848
 -1.328587214463  1.668007282102  3.174271253825  0.006216518378
 -1.771761195387  2.287385908353  2.202255939279  0.005924707926
 -1.026730149698  3.017318998523  1.358608629268  0.005803583915
  0.119020599620  3.173161356543  1.415159765853  0.007122408221
  0.915237473358  3.108118624501  0.463299650838  0.004939411289
  0.462814589486  2.837210699514 -0.795516582628  0.002400243182
  1.026812912753 -3.017319139618  0.083991013862  0.004137314137
 -0.070059393274 -3.176418651270  0.035610954337  0.004739417786
 -0.970268948715 -3.083313212042  1.062474740253  0.006650512133
 -0.534203029846 -2.828286137107  2.231267851728  0.008068238065
  0.871897873699 -0.575451888002  3.799144800425  0.006583288798
  1.408719892640  1.640288870679  3.148120355694  0.007120708735
  2.650053602975  1.633766196698  1.655441764425  0.001121259710
  1.964286464189 -2.001027831890  2.365093852582  0.004828850277
 -1.342877128538 -0.760711330777  3.581800927521  0.005438594270
 -0.531365927285  2.661712102822  2.509437292848  0.008127417225
  1.142904167226  2.885766749848 -0.243475252462  0.001421515906
  0.342129526197 -3.189261637688  1.246854200824  0.006934094388
 -2.346459445268  1.135413320199  2.662806558936  0.003635722018
 -0.258565605057  3.205542092831  0.249849913336  0.004557244500
  0.450566961335 -2.699565911226 -1.032013446782  0.002441544166
 -1.684533476150 -2.430522251364  2.070179818920  0.005068964160
 -3.368784049130  0.036613212616 -1.854972615293 -0.007912120013
 -3.278396927799  1.580834404538 -0.078603229040 -0.005844741254
 -3.656110378332 -0.713812229513  0.361831391088 -0.006545687711
 -2.408052717333 -2.075622947728 -1.226189024363 -0.005504058629
 -1.295829484895 -0.567704086865 -2.747607986898 -0.006014618727
 -1.846557165647  1.681693338816 -2.099716493181 -0.006189888122
 -3.831862607217  0.357468890277 -0.654813288033 -0.008531242293
 -3.527736967644 -1.045671231370 -1.064852892071 -0.008443755984
 -2.622906831544 -1.024738705101 -2.241124222290 -0.008364360848
 -2.402781990555  0.378472303440 -2.579763034114 -0.008134722829
 -3.126606927219  1.313275381963 -1.541857021673 -0.008599547866
 -3.633182667875  0.531520753705  0.561856665829 -0.004584201205
 -3.086813000261 -1.794874586082 -0.179700355757 -0.004581194480
 -1.615880090830 -1.674393887320 -2.147504235692 -0.004721801890
 -1.240258025012  0.765881087348 -2.687977655831 -0.004962136742
 -2.430292517958  2.154437082790 -0.969924007583 -0.004153649759
  3.368776503197 -0.036639040867 -1.854966842961 -0.007917794159
  3.278392620256 -1.580850766330 -0.078589062660 -0.005829250315
  3.656106873706  0.713798214804  0.361832640492 -0.006540167754
  2.408046317685  2.075600470298 -1.226192756897 -0.005512072103
  1.295820311657  0.567673501678 -2.747601655947 -0.006436402768
  1.846549173548 -1.681720471299 -2.099699178989 -0.006142630962
  3.831857249217 -0.357488322766 -0.654806650060 -0.008530508733
  3.527730862121  1.045649613724 -1.064853177123 -0.008446593111
  2.622898581638  1.024710819001 -2.241122746235 -0.008350046837
  2.402773123306 -0.378501994157 -2.579753678917 -0.008125157590
  3.126599952113 -1.313299541573 -1.541844004398 -0.008603512680
  3.633179527908 -0.531533702443  0.561864593522 -0.004579705598
  3.086808508398  1.794857685487 -0.179703829578 -0.004578209029
  1.615872011596  1.674366500124 -2.147504385867 -0.004687475241
  1.240248960489 -0.765911354740 -2.687964116774 -0.005220447655
  2.430286585511 -2.154458194501 -0.969905238283 -0.004139129027
  0.000006852884  1.035694667087 -2.361676372823 -0.000280151011
 -0.298806015181  0.969607486526 -2.408809074493 -0.000339317566
  0.298815704890  0.969607820141 -2.408807386530 -0.000248828713
  0.000007656756 -1.035723195601 -2.361670853435 -0.000273579019
 -0.298805262603 -0.969636498141 -2.408803907288 -0.000324530735
  0.298816457468 -0.969636367899 -2.408802219325 -0.000245983615
 -0.667712940797 -1.245186997899 -2.338574529312 -0.000942466965
 -1.218112188165 -2.025138759161 -1.616566947615 -0.001409125406
 -2.084175601108 -2.294008802021 -0.480477682822 -0.000935663129
 -2.876631896395 -1.658047550445  0.559052047065 -0.001348168485
 -3.282909221331 -0.368099862162  1.091996180628 -0.001787611150
 -3.142757910518  1.067034798172  0.908143333087 -0.001604678566
 -2.511458431963  2.081290260960  0.080011352455 -0.001161881902
 -1.638016880752  2.274609499719 -1.065756198713 -0.001127708490
 -0.866948462969  1.570740798135 -2.077229430584 -0.001954264170
 -0.467915446486 -1.694563900014 -1.941501402531 -0.001392283819
 -1.299753513329 -2.453901457341 -0.850306853788 -0.000218506158
 -2.242033801688 -2.245340287831  0.385760998463 -0.000283894613
 -2.923088754321 -1.151144046328  1.279154738758 -0.000845819781
 -3.074287016292  0.397098864814  1.477489335582 -0.001120079261
 -2.635990824421  1.788708515315  0.902534843950 -0.000490477729
 -1.781079179779  2.474786487342 -0.218927030025 -0.000157630632
 -0.846758459860  2.184720175398 -1.444553386477 -0.000819731585
 -0.255852300976  1.360631011730 -2.219692209228 -0.000897851804
  0.667723897920  1.245157743773 -2.338577856151 -0.000875692080
  1.218123388571  2.025110412626 -1.616576841774 -0.001421386210
  2.084186643139  2.293984793080 -0.480493513306 -0.000912883429
  2.876642520254  1.658030225134  0.559035126479 -0.001341666622
  3.282919570196  0.368088739237  1.091983758387 -0.001776734385
  3.142768358070 -1.067043033853  0.908139383421 -0.001595560572
  2.511469270857 -2.081300494445  0.080016452498 -0.001101265626
  1.638028059662 -2.274626132712 -1.065745290088 -0.001085642625
  0.866959534092 -1.570765766224 -2.077218589767 -0.001903324343
  0.467932162408  1.694535407938 -1.941510815499 -0.001448220741
  1.299770347024  2.453875604722 -0.850324284820 -0.000228458300
  2.242050270258  2.245320705184  0.385739526123 -0.000212688464
  2.923104801922  1.151131828431  1.279135167181 -0.000874724446
  3.074302957696 -0.397105856761  1.477477125466 -0.001147707174
  2.636007061705 -1.788714946455  0.902532611959 -0.000535717314
  1.781095866662 -2.474797662024 -0.218920775640 -0.000214388154
  0.846775315928 -2.184739733041 -1.444543821640 -0.000872572647
  0.255868904327 -1.360657816862 -2.219684436601 -0.000831932933
  0.427045418766 -0.580992742084 -2.585105089006 -0.002212147030
  0.427044967836  0.580963354323 -2.585108185092 -0.002082327326
 -0.427035838172 -0.000014913339 -2.543076305315 -0.005970597047
 end of cosurf and qcosdalton

 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 ** Total nuclear repulsion energy ** =   9.36307563494871
 screening nuclear repulsion energy   0.00777274227

 Total-screening nuclear repulsion energy   9.35530289


 Adding T+Vsolv ...
 maxdens  300
 *** End of DALTON-COSMO calculation ***

  original map vector  20 21 22 23 24 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17
 18 19 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0


          sorted Vsolv matrix  block   1

               nbf   1        nbf   2        nbf   3        nbf   4        nbf   5        nbf   6        nbf   7        nbf   8
  nbf   1    -3.31783523
  nbf   2     0.00001502    -3.09027961
  nbf   3     0.00001799    -0.98133131    -4.10173313
  nbf   4    -1.01839929     0.00000428     0.00001625    -4.07768858
  nbf   5    -0.42205095     0.00001249     0.00001022    -0.44131139    -4.75273267
  nbf   6    -0.00000519    -0.00000347     0.00000218     0.00000140     0.00000165    -5.34921753
  nbf   7    -0.00000464    -1.17073464    -0.38111646     0.00000771     0.00000694    -0.00000280    -4.33740815
  nbf   8    -0.41296041     0.00000931     0.00002806     0.04024917     0.09728632    -0.00000440    -0.00000468    -3.58738610
  nbf   9     0.00000278     0.00000098     0.00000184     0.00000128    -0.00000003    -0.00000281     0.00000087    -0.00000233
  nbf  10     0.00000003     0.00000038     0.00000128     0.00000082     0.00000613     0.24642680     0.00000117    -0.00000065
  nbf  11     0.71287433     0.00000310     0.00000629    -0.06904471    -0.45113185     0.00000256     0.00000145     0.37452315
  nbf  12    -0.00002156    -0.02255655    -0.02751166     0.00000023    -0.00004132     0.00000094    -0.10288857    -0.00003852
  nbf  13     0.00001055     0.51240181     1.29231609     0.00000911     0.00000496     0.00000178     1.13425660     0.00000452
  nbf  14     0.60256766     0.00000707     0.00000303     1.39201132     0.61424323     0.00000228     0.00000719     0.29791626
  nbf  15     0.00000111     0.00000004    -0.00000011     0.00000168     0.00000192     0.06446989     0.00000120     0.00000074
  nbf  16     0.00000150     0.00000192     0.00000283     0.00000050     0.00000035    -0.00000216    -0.00000022    -0.00000075
  nbf  17     0.01595267    -0.00000632    -0.00001369     0.28424853    -0.03532223     0.00000102     0.00000121    -0.91950394
  nbf  18    -0.11637898    -0.00000600    -0.00000168    -0.29335497    -0.90115630     0.00000050    -0.00000096    -0.23512709
  nbf  19    -0.00000635     0.16657455    -0.06195356     0.00000049    -0.00000107    -0.00000011     1.19139578    -0.00000732
  nbf  20     0.18304332     0.00000003    -0.00000001     0.16561291    -0.11837129    -0.00000046     0.00000010     0.20494770
  nbf  21    -0.99291543    -0.00000491    -0.00001583    -0.72382631     0.04741422     0.00000175     0.00000393    -0.60132151
  nbf  22     0.00000757     1.23682410     0.65697011     0.00001007     0.00000965    -0.00000241     1.45002714     0.00001191
  nbf  23     0.54144476     0.00000751     0.00001327     0.73138916     1.71708287     0.00000019     0.00000317    -0.41951915
  nbf  24     0.00000202     0.00000140    -0.00000153    -0.00000172     0.00000012    -2.13410608     0.00000162     0.00000232

               nbf   9        nbf  10        nbf  11        nbf  12        nbf  13        nbf  14        nbf  15        nbf  16
  nbf   9    -3.42214043
  nbf  10     0.00001913    -3.17751132
  nbf  11    -0.00000304    -0.00000184    -4.03682147
  nbf  12    -0.00000420     0.00000171    -0.00000802    -2.63210346
  nbf  13    -0.00000194    -0.00000172     0.00000116     0.09060710    -3.62250728
  nbf  14    -0.00000173    -0.00000162    -0.18626614     0.00000096     0.00002092    -3.66387581
  nbf  15     0.00000603     0.90779934    -0.00000060     0.00000070     0.00000003     0.00000033    -3.67040619
  nbf  16     0.90267086     0.00000685    -0.00000032    -0.00000090    -0.00000065    -0.00000023     0.00000620    -3.63995681
  nbf  17     0.00000123     0.00000044    -0.42349952     0.00002309    -0.00000509     0.04526781    -0.00000008     0.00000167
  nbf  18     0.00000059    -0.00000151     0.56835657     0.00000973    -0.00000910    -0.46551639    -0.00000084     0.00000049
  nbf  19    -0.00000117    -0.00000062    -0.00000049    -0.08837563     0.44971948     0.00000823    -0.00000108    -0.00000003
  nbf  20    -0.00000001     0.00000003    -0.38280841     0.00000000     0.00000001    -0.26339631    -0.00000001     0.00000005
  nbf  21    -0.00000287    -0.00000167     1.08734710     0.00001348     0.00000234     0.59831923    -0.00000035     0.00000066
  nbf  22     0.00000388     0.00000219     0.00000596     0.01873137    -0.54199706     0.00000039     0.00000098    -0.00000154
  nbf  23     0.00000115     0.00000556     0.54858468    -0.00002625     0.00000195    -0.34232141    -0.00000289     0.00000123
  nbf  24     0.00000345     0.07868767    -0.00000224    -0.00000059    -0.00000118    -0.00000012     0.08079665     0.00000213

               nbf  17        nbf  18        nbf  19        nbf  20        nbf  21        nbf  22        nbf  23        nbf  24
  nbf  17    -3.38786977
  nbf  18    -0.05577853    -3.03537088
  nbf  19     0.00000611    -0.00000748    -2.89484436
  nbf  20     0.00966245     0.05949613    -0.00000024   -33.05237215
  nbf  21    -0.01754409    -0.18427221     0.00000142     0.58206335    -7.87317451
  nbf  22    -0.00000599     0.00000109    -0.45828765    -0.00000010    -0.00000912    -6.71607144
  nbf  23    -0.12227558     0.30564486    -0.00000431     0.18749390    -0.25189514     0.00001417    -6.95640844
  nbf  24    -0.00000091     0.00000144    -0.00000007    -0.00000042    -0.00000478     0.00000244    -0.00000118    -7.11751794
 insert_onel_diag: nmin2=   380

 onel.diag.
 -0.331784E+01-0.309028E+01-0.410173E+01-0.407769E+01-0.475273E+01-0.534922E+01-0.433741E+01-0.358739E+01-0.342214E+01-0.317751E+01
 -0.403682E+01-0.263210E+01-0.362251E+01-0.366388E+01-0.367041E+01-0.363996E+01-0.338787E+01-0.303537E+01-0.289484E+01-0.330524E+02
 -0.787317E+01-0.671607E+01-0.695641E+01-0.711752E+01

 i,start,strti,fin,filind,bufszi,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095         1       380       380

 4 external diag. modified integrals

  0.299711954 -3.31783523  0.362025844  0.213403987  0.294044049 -3.09027961
  0.345182252  0.318068021  0.34837146  0.293493931  0.497019376 -4.10173313
  0.382867967  0.309072536  0.341195502  0.316765031  0.62103433  0.362850338
  0.531836961 -4.07768858  0.349706612  0.299527054  0.308820027  0.298754537
  0.450260207  0.419223806  0.448476864  0.401601457  0.518535861 -4.75273267
  0.349531774  0.304524501  0.31278089  0.302655019  0.429312411  0.416826853
  0.436921903  0.413400849  0.530775438  0.48280831  0.60679891 -5.34921753
  0.33535564  0.293407704  0.341034277  0.276576801  0.455225842  0.389302194
  0.443351075  0.383054684  0.486303633  0.428587076  0.499742288  0.459023099
  0.475434104 -4.33740815  0.342526573  0.315362897  0.319917749  0.300731659
  0.504389096  0.410870033  0.47482656  0.425010382  0.519270202  0.400100848
  0.453053263  0.422413645  0.446074502  0.406547185  0.476405093 -3.5873861
  0.361060191  0.337659006  0.340604687  0.320198588  0.504515292  0.452245513
  0.527488651  0.469039301  0.434901697  0.420706511  0.454829088  0.417262113
  0.426620519  0.400804422  0.469386808  0.441599006  0.555764969 -3.42214043
  0.348856386  0.329501947  0.326880472  0.312634683  0.490451682  0.436629022
  0.509180653  0.444435002  0.4400708  0.419071921  0.459403052  0.417691971
  0.415311401  0.398901525  0.466192986  0.43088403  0.694750225  0.346946891
  0.501631969 -3.17751132  0.343227876  0.31280524  0.321132402  0.300850073
  0.455432102  0.418963752  0.451109274  0.419591702  0.534502331  0.438245352
  0.597905727  0.464004009  0.508095073  0.408470727  0.460615989  0.424227034
  0.469521607  0.441123033  0.462583019  0.438238584  0.525774157 -4.03682147
  0.356976431  0.333496982  0.334773679  0.315431831  0.522936179  0.430785245
  0.55704189  0.430105919  0.46546176  0.385379923  0.398185515  0.390667038
  0.424333725  0.380522726  0.592974188  0.354449455  0.516100041  0.471421051
  0.491982029  0.453400696  0.428233148  0.409923576  0.53092711 -2.63210346
  0.370351482  0.34231438  0.377526483  0.320334123  0.57663332  0.44254372
  0.563518308  0.477512966  0.511232086  0.469131823  0.508472502  0.490097443
  0.529535469  0.421869959  0.499545867  0.467575397  0.539123293  0.488956188
  0.512741012  0.475743005  0.515621907  0.477676678  0.512163042  0.474196449
  0.592144122 -3.62250728  0.388203856  0.340302294  0.365601016  0.328125926
  0.536368018  0.473948976  0.615526601  0.455378076  0.520967947  0.463416877
  0.546457699  0.489981129  0.48581785  0.455063687  0.509165453  0.46499119
  0.535052608  0.504064949  0.518029026  0.481875024  0.516561248  0.489811629
  0.517714869  0.482829445  0.717425907  0.445100321  0.607835731 -3.66387581
  0.361030445  0.336028491  0.330453775  0.322428351  0.486741558  0.469418774
  0.514189842  0.450314389  0.575178104  0.539226259  0.630438312  0.577562728
  0.510895782  0.499926663  0.503939385  0.490371469  0.52041881  0.467839603
  0.554043059  0.445274854  0.591832537  0.558697254  0.453957255  0.445032703
  0.568738744  0.548069735  0.596778585  0.560916765  0.750165186 -3.67040619
  0.353015389  0.338054232  0.358115278  0.317291888  0.522721553  0.462375435
  0.489534774  0.469144866  0.543456123  0.532600344  0.632201055  0.577477753
  0.547273656  0.513624874  0.507616587  0.483570727  0.564197636  0.455577875
  0.524876517  0.46718588  0.585160178  0.562784851  0.458775281  0.447244069
  0.587371194  0.556188564  0.58998874  0.568912261  0.721594827  0.645098881
  0.755127757 -3.63995681  0.366037972  0.335416332  0.343455146  0.320873443
  0.50940609  0.476326958  0.527676719  0.456821536  0.589439367  0.537772005
  0.579789349  0.559672288  0.53269644  0.49854365  0.567546289  0.469810852
  0.495610134  0.480892777  0.493939617  0.479062411  0.584343602  0.533457424
  0.502525388  0.442000941  0.582221799  0.560044775  0.596954899  0.56140811
  0.713528596  0.656785472  0.69947109  0.625487345  0.729807063 -3.38786977
  0.353987843  0.337616982  0.351789554  0.321182817  0.550334788  0.456688986
  0.522995786  0.46803517  0.566057852  0.507622395  0.615570104  0.560439732
  0.539018372  0.503813066  0.510463413  0.481771744  0.500103129  0.48786777
  0.496692123  0.477703001  0.585735173  0.540040646  0.469784387  0.453592071
  0.609978757  0.53850005  0.643846451  0.534124473  0.686162365  0.635949394
  0.700906311  0.666564985  0.679632027  0.617528828  0.696112735 -3.03537088
  0.358856446  0.335907395  0.354506012  0.319503646  0.548011068  0.469120392
  0.531509325  0.460743131  0.577501451  0.534350115  0.577624788  0.562928162
  0.587296462  0.4866605  0.514004213  0.4958057  0.493523583  0.479933652
  0.487740555  0.474497517  0.570979461  0.537487177  0.471960345  0.452931079
  0.634007692  0.54138526  0.615254278  0.550385293  0.691778305  0.63576614
  0.699509207  0.640665078  0.699234432  0.657364716  0.711993958  0.616407541
  0.721280492 -2.89484436
 i,start,strti,fin,filind,bufszi,nmbuf,nmin2, nd4ext = 
         1         1         2         2        12      4095         1       380       380

 all internall diag. modified integrals

  4.73971342 -33.0523722  1.04569563  0.0636788604  0.755179462 -7.87317451
  0.878477817  0.0193493095  0.682719812  0.155759544  0.668673763 -6.71607144
  0.986988872  0.0301374391  0.681192227  0.125736446  0.608457144
  0.0402117459  0.732528817 -6.95640844  1.03831833  0.0301199817  0.713926708
  0.128455235  0.624391224  0.0304166993  0.665957609  0.0433645062
  0.760228983 -7.11751794
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331784E+01-0.309028E+01-0.410173E+01-0.407769E+01-0.475273E+01-0.534922E+01-0.433741E+01-0.358739E+01-0.342214E+01-0.317751E+01
 -0.403682E+01-0.263210E+01-0.362251E+01-0.366388E+01-0.367041E+01-0.363996E+01-0.338787E+01-0.303537E+01-0.289484E+01-0.330524E+02
 -0.787317E+01-0.671607E+01-0.695641E+01-0.711752E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331784E+01-0.309028E+01-0.410173E+01-0.407769E+01-0.475273E+01-0.534922E+01-0.433741E+01-0.358739E+01-0.342214E+01-0.317751E+01
 -0.403682E+01-0.263210E+01-0.362251E+01-0.366388E+01-0.367041E+01-0.363996E+01-0.338787E+01-0.303537E+01-0.289484E+01-0.330524E+02
 -0.787317E+01-0.671607E+01-0.695641E+01-0.711752E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331784E+01-0.309028E+01-0.410173E+01-0.407769E+01-0.475273E+01-0.534922E+01-0.433741E+01-0.358739E+01-0.342214E+01-0.317751E+01
 -0.403682E+01-0.263210E+01-0.362251E+01-0.366388E+01-0.367041E+01-0.363996E+01-0.338787E+01-0.303537E+01-0.289484E+01-0.330524E+02
 -0.787317E+01-0.671607E+01-0.695641E+01-0.711752E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380
 DIAGEL COUNTS: DG0X,DG2X,DG4X= 440 145 30
 =========== Executing IN-CORE method ==========
 norm multnew:  1.
 threx: total number of loops= 14
 threx: total number of loops= 24
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331784E+01-0.309028E+01-0.410173E+01-0.407769E+01-0.475273E+01-0.534922E+01-0.433741E+01-0.358739E+01-0.342214E+01-0.317751E+01
 -0.403682E+01-0.263210E+01-0.362251E+01-0.366388E+01-0.367041E+01-0.363996E+01-0.338787E+01-0.303537E+01-0.289484E+01-0.330524E+02
 -0.787317E+01-0.671607E+01-0.695641E+01-0.711752E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331784E+01-0.309028E+01-0.410173E+01-0.407769E+01-0.475273E+01-0.534922E+01-0.433741E+01-0.358739E+01-0.342214E+01-0.317751E+01
 -0.403682E+01-0.263210E+01-0.362251E+01-0.366388E+01-0.367041E+01-0.363996E+01-0.338787E+01-0.303537E+01-0.289484E+01-0.330524E+02
 -0.787317E+01-0.671607E+01-0.695641E+01-0.711752E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331784E+01-0.309028E+01-0.410173E+01-0.407769E+01-0.475273E+01-0.534922E+01-0.433741E+01-0.358739E+01-0.342214E+01-0.317751E+01
 -0.403682E+01-0.263210E+01-0.362251E+01-0.366388E+01-0.367041E+01-0.363996E+01-0.338787E+01-0.303537E+01-0.289484E+01-0.330524E+02
 -0.787317E+01-0.671607E+01-0.695641E+01-0.711752E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        40 xx:       135 ww:       255
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        20 yw:        25

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        39
task #   9:       127    task #  10:       247    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 matrix-vector product formed for vector   1
 =========== Executing IN-CORE method ==========
 norm multnew:  0.00222408662
 threx: total number of loops= 14
 threx: total number of loops= 24
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331784E+01-0.309028E+01-0.410173E+01-0.407769E+01-0.475273E+01-0.534922E+01-0.433741E+01-0.358739E+01-0.342214E+01-0.317751E+01
 -0.403682E+01-0.263210E+01-0.362251E+01-0.366388E+01-0.367041E+01-0.363996E+01-0.338787E+01-0.303537E+01-0.289484E+01-0.330524E+02
 -0.787317E+01-0.671607E+01-0.695641E+01-0.711752E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331784E+01-0.309028E+01-0.410173E+01-0.407769E+01-0.475273E+01-0.534922E+01-0.433741E+01-0.358739E+01-0.342214E+01-0.317751E+01
 -0.403682E+01-0.263210E+01-0.362251E+01-0.366388E+01-0.367041E+01-0.363996E+01-0.338787E+01-0.303537E+01-0.289484E+01-0.330524E+02
 -0.787317E+01-0.671607E+01-0.695641E+01-0.711752E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331784E+01-0.309028E+01-0.410173E+01-0.407769E+01-0.475273E+01-0.534922E+01-0.433741E+01-0.358739E+01-0.342214E+01-0.317751E+01
 -0.403682E+01-0.263210E+01-0.362251E+01-0.366388E+01-0.367041E+01-0.363996E+01-0.338787E+01-0.303537E+01-0.289484E+01-0.330524E+02
 -0.787317E+01-0.671607E+01-0.695641E+01-0.711752E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        40 xx:       135 ww:       255
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        20 yw:        25

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        39
task #   9:       127    task #  10:       247    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 matrix-vector product formed for vector   2
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference-subspace vector overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97633721     0.00000000

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97731793     0.00318598
 subspace has dimension  2

          (h-repnuc*1) matrix in the subspace basis

                x:   1         x:   2
   x:   1   -85.60315580
   x:   2     0.17483087    -0.18307548

          overlap matrix in the subspace basis

                x:   1         x:   2
   x:   1     1.00000000
   x:   2    -0.00211778     0.00222409

          eigenvectors and eigenvalues in the subspace basis

                v:   1         v:   2

   energy   -85.60886977   -82.31487067

   x:   1     1.00100449     0.00326320
   x:   2     0.88403512    21.20730485

          eigenvectors and eigenvalues in the schmidt orthonormalized basis

                v:   1         v:   2

   energy   -85.60886977   -82.31487067

  zx:   1     0.99913229    -0.04164924
  zx:   2     0.04164924     0.99913229

          <ref|baseci> overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97633721     0.00000000

 trial vector basis is being transformed.  new dimension:   1

          transformed tciref transformation matrix  block   1

               ref   1
  ci:   1     0.97731793

  write the transformed vectors back to cosmofile

 rtflw =  0
 freezing =  F

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1    -76.2535668738 -9.3457E+00  5.4631E-04  3.4900E-02  1.0000E-03
 mr-sdci #  3  2    -72.9595677784 -8.3991E+00  0.0000E+00  1.6856E+00  1.0000E-04


 total energy in cosmo calc 
e(rootcalc) + repnuc=     -76.2535668738
dielectric energy =      -0.0119905990
deltaediel =       0.0119905990
e(rootcalc) + repnuc - ediel =     -76.2415762748
e(rootcalc) + repnuc - edielnew =     -76.2535668738
deltaelast =      76.2415762748

 root number  1 is used to define the new expansion vector.
 diagon: frcsub=2, new matrix-vector products will be computed.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.01   0.00   0.00   0.01         0.    0.0000
    2   25    0   0.01   0.00   0.00   0.01         0.    0.0000
    3   26    0   0.03   0.00   0.00   0.02         0.    0.0000
    4   11    0   0.01   0.00   0.00   0.01         0.    0.0000
    5   15    0   0.02   0.00   0.00   0.02         0.    0.0000
    6   16    0   0.02   0.00   0.00   0.02         0.    0.0000
    7    1    0   0.01   0.00   0.00   0.01         0.    0.0000
    8    5    0   0.01   0.00   0.00   0.01         0.    0.0000
    9    6    0   0.03   0.00   0.00   0.02         0.    0.0000
   10    7    0   0.03   0.00   0.00   0.03         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.02   0.00   0.00   0.02         0.    0.0000
   14   47    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.2100s 
time spent in multnx:                   0.1900s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0300s 
total time per CI iteration:            3.7900s 

          starting ci iteration   4

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.35530289

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=      10  D0X=      30  D0W=      50
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================

          ci-one-electron density block   1

                mo   1         mo   2         mo   3         mo   4         mo   5         mo   6         mo   7         mo   8
   mo   1     1.99991680
   mo   2    -0.00019348     1.98638684
   mo   3     0.00000000     0.00000004     1.97201181
   mo   4    -0.00015463     0.00355029    -0.00000001     1.97514761
   mo   5     0.00000000    -0.00000005     0.00000001    -0.00000004     1.97737611
   mo   6     0.00014514    -0.00152270    -0.00001477    -0.01461264    -0.00000619     0.00589196
   mo   7     0.00000000     0.00000651    -0.00712919    -0.00001731    -0.00000396    -0.00000009     0.00657119
   mo   8    -0.00000001     0.00001347    -0.01236716    -0.00001674     0.00000198    -0.00000015     0.00631080     0.00974512
   mo   9     0.00008233     0.00419381    -0.00000944    -0.00138175     0.00000159     0.00619772    -0.00000017    -0.00000022
   mo  10    -0.00019994     0.00919769    -0.00001221     0.00522273     0.00000020     0.00227253    -0.00000018    -0.00000011
   mo  11     0.00000003    -0.00000155     0.00000274    -0.00000044    -0.01227318     0.00000018     0.00000009    -0.00000009
   mo  12    -0.00000001    -0.00000264    -0.00416445    -0.00000522    -0.00000218     0.00000002     0.00549153     0.00351404
   mo  13     0.00018821    -0.00077797    -0.00001101    -0.00765451    -0.00000268     0.00168784    -0.00000002    -0.00000011
   mo  14     0.00000000     0.00000193    -0.00000278    -0.00000104    -0.00000081    -0.00000002    -0.00000003    -0.00000001
   mo  15     0.00000000     0.00000098    -0.00000172    -0.00000426    -0.00329553     0.00000001    -0.00000001     0.00000000
   mo  16    -0.00034910     0.00044275    -0.00000419     0.00260790     0.00000149    -0.00176260    -0.00000010    -0.00000004
   mo  17    -0.00000001    -0.00000774     0.00075787     0.00001827     0.00000043     0.00000000     0.00023202     0.00020891
   mo  18     0.00000003    -0.00000052     0.01053551    -0.00000045     0.00000081    -0.00000008    -0.00259817    -0.00413708
   mo  19    -0.00010130    -0.00336987    -0.00000002     0.00744323     0.00000037    -0.00278235    -0.00000011    -0.00000007
   mo  20     0.00000000     0.00000019    -0.00000044     0.00000128     0.00215932    -0.00000002     0.00000000     0.00000000
   mo  21     0.00000000    -0.00000022     0.00000056    -0.00000054    -0.00000125     0.00000001    -0.00000001    -0.00000001
   mo  22     0.00000852     0.00010707     0.00000218    -0.00146425     0.00000026    -0.00003896     0.00000004     0.00000003
   mo  23     0.00004083     0.00136190    -0.00000051    -0.00060303    -0.00000045     0.00053454     0.00000001     0.00000000
   mo  24    -0.00000001    -0.00000060     0.00004976     0.00000168     0.00000009     0.00000000    -0.00072383     0.00044179

                mo   9         mo  10         mo  11         mo  12         mo  13         mo  14         mo  15         mo  16
   mo   9     0.00916958
   mo  10     0.00293585     0.00949153
   mo  11    -0.00000007    -0.00000001     0.01360762
   mo  12     0.00000001     0.00000003     0.00000002     0.00611617
   mo  13     0.00034296    -0.00059918     0.00000005    -0.00000001     0.00361310
   mo  14    -0.00000002     0.00000000     0.00000008    -0.00000002     0.00000000     0.00293390
   mo  15    -0.00000002    -0.00000006    -0.00125658    -0.00000001     0.00000002    -0.00000005     0.00219573
   mo  16     0.00011980     0.00242412    -0.00000003    -0.00000006    -0.00111817     0.00000001    -0.00000002     0.00416992
   mo  17     0.00000012     0.00000025    -0.00000001     0.00032371    -0.00000003     0.00000000     0.00000000     0.00000006
   mo  18    -0.00000010    -0.00000014     0.00000001    -0.00166850     0.00000001     0.00000000     0.00000000    -0.00000003
   mo  19    -0.00407809    -0.00074521     0.00000000    -0.00000010    -0.00074862     0.00000000    -0.00000001    -0.00013901
   mo  20    -0.00000001     0.00000000     0.00006539     0.00000000    -0.00000001     0.00000000    -0.00182076     0.00000000
   mo  21     0.00000000    -0.00000001     0.00000002     0.00000000     0.00000000    -0.00201895     0.00000000    -0.00000001
   mo  22    -0.00052545     0.00032551    -0.00000001     0.00000003     0.00171525     0.00000000     0.00000000     0.00071134
   mo  23     0.00013098     0.00173031     0.00000000     0.00000003     0.00022343     0.00000000     0.00000000    -0.00066426
   mo  24     0.00000001     0.00000000     0.00000000    -0.00160634     0.00000000     0.00000000     0.00000000     0.00000002

                mo  17         mo  18         mo  19         mo  20         mo  21         mo  22         mo  23         mo  24
   mo  17     0.00050009
   mo  18    -0.00018952     0.00251517
   mo  19     0.00000002     0.00000004     0.00275667
   mo  20     0.00000000     0.00000000     0.00000001     0.00253624
   mo  21     0.00000000     0.00000001     0.00000000     0.00000001     0.00240220
   mo  22     0.00000000     0.00000000    -0.00017189     0.00000000     0.00000000     0.00195072
   mo  23     0.00000002    -0.00000001     0.00063139     0.00000000     0.00000000    -0.00008785     0.00159579
   mo  24     0.00000895    -0.00058367     0.00000002     0.00000000     0.00000000     0.00000000    -0.00000001     0.00139812


*****   symmetry block SYM1   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       1.99991680
       2      -0.00019348     1.98638684
       3       0.00000000     0.00000004     1.97201181
       4      -0.00015463     0.00355029    -0.00000001     1.97514761
       5       0.00000000    -0.00000005     0.00000001    -0.00000004
       6       0.00014514    -0.00152270    -0.00001477    -0.01461264
       7       0.00000000     0.00000651    -0.00712919    -0.00001731
       8      -0.00000001     0.00001347    -0.01236716    -0.00001674
       9       0.00008233     0.00419381    -0.00000944    -0.00138175
      10      -0.00019994     0.00919769    -0.00001221     0.00522273
      11       0.00000003    -0.00000155     0.00000274    -0.00000044
      12      -0.00000001    -0.00000264    -0.00416445    -0.00000522
      13       0.00018821    -0.00077797    -0.00001101    -0.00765451
      14       0.00000000     0.00000193    -0.00000278    -0.00000104
      15       0.00000000     0.00000098    -0.00000172    -0.00000426
      16      -0.00034910     0.00044275    -0.00000419     0.00260790
      17      -0.00000001    -0.00000774     0.00075787     0.00001827
      18       0.00000003    -0.00000052     0.01053551    -0.00000045
      19      -0.00010130    -0.00336987    -0.00000002     0.00744323
      20       0.00000000     0.00000019    -0.00000044     0.00000128
      21       0.00000000    -0.00000022     0.00000056    -0.00000054
      22       0.00000852     0.00010707     0.00000218    -0.00146425
      23       0.00004083     0.00136190    -0.00000051    -0.00060303
      24      -0.00000001    -0.00000060     0.00004976     0.00000168

               Column   5     Column   6     Column   7     Column   8
       5       1.97737611
       6      -0.00000619     0.00589196
       7      -0.00000396    -0.00000009     0.00657119
       8       0.00000198    -0.00000015     0.00631080     0.00974512
       9       0.00000159     0.00619772    -0.00000017    -0.00000022
      10       0.00000020     0.00227253    -0.00000018    -0.00000011
      11      -0.01227318     0.00000018     0.00000009    -0.00000009
      12      -0.00000218     0.00000002     0.00549153     0.00351404
      13      -0.00000268     0.00168784    -0.00000002    -0.00000011
      14      -0.00000081    -0.00000002    -0.00000003    -0.00000001
      15      -0.00329553     0.00000001    -0.00000001     0.00000000
      16       0.00000149    -0.00176260    -0.00000010    -0.00000004
      17       0.00000043     0.00000000     0.00023202     0.00020891
      18       0.00000081    -0.00000008    -0.00259817    -0.00413708
      19       0.00000037    -0.00278235    -0.00000011    -0.00000007
      20       0.00215932    -0.00000002     0.00000000     0.00000000
      21      -0.00000125     0.00000001    -0.00000001    -0.00000001
      22       0.00000026    -0.00003896     0.00000004     0.00000003
      23      -0.00000045     0.00053454     0.00000001     0.00000000
      24       0.00000009     0.00000000    -0.00072383     0.00044179

               Column   9     Column  10     Column  11     Column  12
       9       0.00916958
      10       0.00293585     0.00949153
      11      -0.00000007    -0.00000001     0.01360762
      12       0.00000001     0.00000003     0.00000002     0.00611617
      13       0.00034296    -0.00059918     0.00000005    -0.00000001
      14      -0.00000002     0.00000000     0.00000008    -0.00000002
      15      -0.00000002    -0.00000006    -0.00125658    -0.00000001
      16       0.00011980     0.00242412    -0.00000003    -0.00000006
      17       0.00000012     0.00000025    -0.00000001     0.00032371
      18      -0.00000010    -0.00000014     0.00000001    -0.00166850
      19      -0.00407809    -0.00074521     0.00000000    -0.00000010
      20      -0.00000001     0.00000000     0.00006539     0.00000000
      21       0.00000000    -0.00000001     0.00000002     0.00000000
      22      -0.00052545     0.00032551    -0.00000001     0.00000003
      23       0.00013098     0.00173031     0.00000000     0.00000003
      24       0.00000001     0.00000000     0.00000000    -0.00160634

               Column  13     Column  14     Column  15     Column  16
      13       0.00361310
      14       0.00000000     0.00293390
      15       0.00000002    -0.00000005     0.00219573
      16      -0.00111817     0.00000001    -0.00000002     0.00416992
      17      -0.00000003     0.00000000     0.00000000     0.00000006
      18       0.00000001     0.00000000     0.00000000    -0.00000003
      19      -0.00074862     0.00000000    -0.00000001    -0.00013901
      20      -0.00000001     0.00000000    -0.00182076     0.00000000
      21       0.00000000    -0.00201895     0.00000000    -0.00000001
      22       0.00171525     0.00000000     0.00000000     0.00071134
      23       0.00022343     0.00000000     0.00000000    -0.00066426
      24       0.00000000     0.00000000     0.00000000     0.00000002

               Column  17     Column  18     Column  19     Column  20
      17       0.00050009
      18      -0.00018952     0.00251517
      19       0.00000002     0.00000004     0.00275667
      20       0.00000000     0.00000000     0.00000001     0.00253624
      21       0.00000000     0.00000001     0.00000000     0.00000001
      22       0.00000000     0.00000000    -0.00017189     0.00000000
      23       0.00000002    -0.00000001     0.00063139     0.00000000
      24       0.00000895    -0.00058367     0.00000002     0.00000000

               Column  21     Column  22     Column  23     Column  24
      21       0.00240220
      22       0.00000000     0.00195072
      23       0.00000000    -0.00008785     0.00159579
      24       0.00000000     0.00000000    -0.00000001     0.00139812

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7        eno   8
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    8   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno   9        eno  10        eno  11        eno  12        eno  13        eno  14        eno  15        eno  16
 emo    9   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   10   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   11   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   12   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   13   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   14   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   15   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   16   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno  17        eno  18        eno  19        eno  20        eno  21        eno  22        eno  23        eno  24
 emo   17   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   18   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   19   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   20   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   21   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   22   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   23   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   24   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   1.9999216      1.9874921      1.9774607      1.9742837      1.9721820
   0.0193376      0.0176129      0.0136801      0.0096172      0.0053594
   0.0050795      0.0047044      0.0041897      0.0041001      0.0009708
   0.0009686      0.0006317      0.0005415      0.0004748      0.0004721
   0.0004351      0.0004088      0.0000421      0.0000335

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7         no   8
  mo    1   0.999820E+00   0.185803E-01  -0.457609E-07   0.377413E-02  -0.631454E-06   0.219303E-07  -0.449254E-04  -0.187216E-07
  mo    2  -0.167635E-01   0.959314E+00  -0.101147E-05  -0.281791E+00   0.457670E-04  -0.463103E-05  -0.355237E-02   0.924254E-06
  mo    3   0.984228E-07  -0.249926E-05   0.101221E-05   0.153582E-03   0.999956E+00   0.855120E-02   0.128030E-04  -0.152144E-05
  mo    4  -0.885423E-02   0.281657E+00   0.106617E-04   0.959425E+00  -0.146808E-03   0.991665E-05   0.464279E-02   0.264610E-07
  mo    5   0.124268E-06  -0.205066E-05   0.999978E+00  -0.105465E-04  -0.100884E-05   0.110240E-05   0.126918E-05   0.598647E-02
  mo    6   0.150633E-03  -0.280187E-02  -0.321962E-05  -0.691700E-02  -0.647643E-05  -0.162109E-03   0.516474E+00   0.568827E-05
  mo    7   0.210941E-07   0.707536E-06  -0.201375E-05  -0.997387E-05  -0.366017E-02   0.561074E+00   0.163172E-03   0.434230E-05
  mo    8  -0.425621E-07   0.416275E-05   0.990871E-06  -0.111102E-04  -0.632855E-02   0.639861E+00   0.177596E-03  -0.781119E-05
  mo    9   0.124776E-04   0.183767E-02   0.785245E-06  -0.130476E-02  -0.463922E-05  -0.199431E-03   0.673745E+00  -0.132022E-04
  mo   10  -0.201181E-03   0.520398E-02   0.122931E-06   0.122133E-02  -0.641257E-05  -0.125901E-03   0.423988E+00  -0.102091E-04
  mo   11   0.309432E-07  -0.802499E-06  -0.624830E-02   0.744235E-07   0.140449E-05   0.226929E-05   0.107578E-04   0.993079E+00
  mo   12   0.414560E-07  -0.200680E-05  -0.111423E-05  -0.254134E-05  -0.214412E-02   0.441049E+00   0.148030E-03   0.114498E-05
  mo   13   0.135071E-03  -0.146498E-02  -0.140285E-05  -0.362448E-02  -0.503890E-05  -0.362371E-04   0.740486E-01   0.441334E-05
  mo   14  -0.117238E-07   0.784433E-06  -0.408616E-06  -0.779337E-06  -0.141299E-05  -0.320600E-05  -0.240697E-05   0.740501E-05
  mo   15   0.966886E-08  -0.126858E-06  -0.166540E-02  -0.219418E-05  -0.872600E-06  -0.177288E-05  -0.454826E-05  -0.114449E+00
  mo   16  -0.190628E-03   0.590816E-03   0.773353E-06   0.121531E-02  -0.231095E-05  -0.716381E-05   0.946010E-02  -0.587895E-05
  mo   17  -0.206648E-07  -0.114543E-05   0.220135E-06   0.100397E-04   0.382388E-03   0.247434E-01   0.259398E-04  -0.555245E-06
  mo   18   0.198400E-07  -0.337141E-06   0.414882E-06   0.715819E-06   0.536854E-02  -0.281393E+00  -0.960870E-04   0.140053E-05
  mo   19  -0.556200E-04  -0.574576E-03   0.233373E-06   0.411684E-02  -0.619183E-06   0.786487E-04  -0.301165E+00   0.378043E-05
  mo   20  -0.625756E-08   0.270140E-06   0.109467E-02   0.586119E-06  -0.222405E-06  -0.438945E-07  -0.367865E-07   0.256863E-01
  mo   21   0.509033E-08  -0.183128E-06  -0.631373E-06  -0.229843E-06   0.287564E-06  -0.236200E-06   0.957161E-07  -0.253871E-06
  mo   22   0.986618E-05  -0.156507E-03   0.123094E-06  -0.729918E-03   0.121482E-05   0.252440E-05  -0.400941E-02  -0.115162E-06
  mo   23   0.116053E-04   0.576102E-03  -0.232411E-06  -0.488109E-03  -0.187777E-06  -0.158430E-04   0.568625E-01  -0.953243E-06
  mo   24  -0.680954E-08  -0.490911E-07   0.462073E-07   0.908170E-06   0.253320E-04  -0.371820E-01  -0.132108E-04  -0.335074E-06

                no   9         no  10         no  11         no  12         no  13         no  14         no  15         no  16
  mo    1   0.212172E-03  -0.236957E-07  -0.908795E-04  -0.114277E-08   0.422292E-04   0.567932E-09   0.153644E-04   0.970941E-08
  mo    2  -0.333846E-02   0.382525E-05  -0.122450E-03  -0.801683E-06   0.258238E-03   0.204307E-06   0.601751E-03  -0.353089E-05
  mo    3   0.143781E-05  -0.271845E-02   0.483492E-05   0.124034E-05   0.132621E-05  -0.283045E-06   0.189150E-05  -0.107458E-02
  mo    4  -0.624179E-02  -0.108435E-06   0.299620E-02   0.309708E-06   0.170245E-02  -0.187230E-05  -0.136024E-02   0.400374E-05
  mo    5  -0.139143E-05   0.156612E-05   0.192840E-05  -0.215530E-06  -0.301578E-06  -0.250076E-02  -0.745316E-06  -0.788558E-06
  mo    6  -0.264003E+00   0.336079E-04   0.195256E+00   0.236236E-05  -0.132210E+00  -0.361814E-05  -0.125212E+00  -0.198120E-03
  mo    7  -0.949605E-05   0.220100E+00  -0.495313E-04   0.282198E-05   0.408571E-05  -0.444061E-06  -0.592755E-04   0.430807E-01
  mo    8   0.372684E-05  -0.540879E+00   0.134093E-03  -0.427374E-05   0.315958E-05  -0.746011E-06  -0.624239E-03   0.404263E+00
  mo    9  -0.195312E+00  -0.956539E-04  -0.349941E+00  -0.269502E-05   0.471149E-01   0.107473E-05   0.401845E+00   0.625767E-03
  mo   10   0.765033E+00   0.729561E-04   0.302139E+00   0.166504E-05  -0.150172E+00  -0.725317E-05  -0.285608E+00  -0.422502E-03
  mo   11   0.938644E-05  -0.633455E-05  -0.802408E-05  -0.863044E-05   0.417684E-05  -0.946103E-01   0.350587E-05   0.207367E-05
  mo   12  -0.263333E-05   0.646892E+00  -0.148607E-03   0.871844E-05  -0.110114E-05   0.819374E-07   0.435203E-03  -0.282427E+00
  mo   13  -0.245279E+00   0.151739E-03   0.623504E+00   0.174037E-04   0.433461E+00   0.909927E-05   0.578154E-01   0.710511E-04
  mo   14   0.256163E-05  -0.862113E-05  -0.127037E-04   0.751848E+00  -0.831637E-05  -0.156441E-04   0.544274E-06  -0.386678E-05
  mo   15  -0.553966E-05   0.115178E-05  -0.539265E-06  -0.212721E-04   0.143997E-04  -0.652282E+00  -0.726147E-07  -0.763142E-06
  mo   16   0.454876E+00  -0.830262E-04  -0.365588E+00  -0.387612E-06   0.571519E+00   0.105679E-04   0.282826E+00   0.415647E-03
  mo   17   0.129991E-04   0.183493E-01   0.932175E-05   0.190077E-05   0.129921E-05   0.312695E-06   0.365554E-03  -0.254331E+00
  mo   18  -0.630299E-05   0.277763E+00  -0.757462E-04   0.246335E-05  -0.157153E-05   0.907353E-06  -0.952912E-03   0.614038E+00
  mo   19   0.162024E+00   0.424248E-04   0.171269E+00  -0.142411E-05  -0.275244E+00  -0.694871E-05   0.430788E+00   0.623319E-03
  mo   20   0.171961E-05  -0.110268E-05   0.970688E-06   0.156672E-04  -0.161321E-04   0.752044E+00   0.192725E-05  -0.125752E-05
  mo   21  -0.161639E-05   0.750737E-05   0.108099E-04  -0.659336E+00   0.895229E-05   0.223151E-04   0.199662E-05  -0.452136E-05
  mo   22   0.307682E-01   0.778377E-04   0.327261E+00   0.108973E-04   0.515599E+00   0.107381E-04   0.132780E+00   0.243588E-03
  mo   23   0.112063E+00   0.722296E-04   0.298789E+00   0.153091E-05  -0.318783E+00  -0.900509E-05   0.674332E+00   0.106445E-02
  mo   24   0.164442E-05  -0.403783E+00   0.101336E-03  -0.529816E-05   0.472725E-05  -0.911146E-06   0.860035E-03  -0.559661E+00

                no  17         no  18         no  19         no  20         no  21         no  22         no  23         no  24
  mo    1  -0.119888E-08  -0.148605E-07  -0.902721E-08   0.267703E-04   0.152870E-07   0.320962E-04   0.201989E-04   0.480598E-08
  mo    2  -0.583062E-06  -0.280742E-05  -0.834429E-06   0.142570E-02   0.317234E-05   0.144289E-02   0.141691E-02  -0.252281E-07
  mo    3   0.760802E-06  -0.123866E-02   0.740585E-06   0.176090E-06  -0.165201E-02   0.579030E-05   0.663562E-06   0.103998E-02
  mo    4   0.554486E-06   0.632837E-05   0.116991E-05   0.126851E-03  -0.583646E-05   0.859884E-03   0.442688E-02   0.840796E-06
  mo    5   0.632487E-06   0.104312E-05   0.959994E-03   0.842484E-06  -0.101302E-06   0.109350E-05   0.177128E-05   0.946197E-06
  mo    6  -0.499612E-05  -0.133112E-03  -0.126859E-03   0.424542E+00   0.188244E-03   0.280458E+00   0.577344E+00   0.921940E-04
  mo    7  -0.158081E-04   0.553260E+00  -0.372020E-05   0.595515E-04   0.217674E+00   0.490252E-04  -0.818548E-04   0.530476E+00
  mo    8   0.101692E-04  -0.184198E+00   0.725424E-06  -0.520183E-04   0.493398E-01  -0.109629E-04   0.475744E-04  -0.313363E+00
  mo    9   0.879286E-06   0.455230E-05  -0.187451E-05   0.485444E-02   0.677409E-05  -0.180504E-01  -0.470540E+00  -0.781393E-04
  mo   10   0.191431E-05   0.326295E-04   0.155142E-04  -0.416468E-01  -0.308373E-04  -0.622382E-02  -0.194284E+00  -0.281966E-04
  mo   11  -0.112459E-04  -0.410886E-05   0.693117E-01   0.172853E-04   0.650977E-06  -0.334259E-05  -0.717182E-05  -0.366482E-05
  mo   12   0.385656E-05  -0.130768E+00   0.249080E-05   0.141031E-04  -0.127094E+00  -0.137517E-05   0.851398E-04  -0.523434E+00
  mo   13   0.541115E-05   0.163187E-04   0.113388E-03  -0.396365E+00   0.376623E-06   0.404805E+00  -0.182700E+00  -0.346099E-04
  mo   14   0.659336E+00   0.217303E-04   0.731049E-04   0.969140E-05  -0.260287E-05  -0.552019E-06   0.161978E-05   0.132455E-05
  mo   15  -0.730167E-04   0.193245E-05   0.749284E+00   0.223964E-03   0.117439E-04   0.744607E-05  -0.166842E-05   0.230945E-06
  mo   16  -0.152977E-05  -0.513030E-04   0.819481E-05  -0.336514E-01   0.860279E-04   0.324355E+00   0.382594E+00   0.576188E-04
  mo   17   0.140316E-04  -0.426901E+00  -0.123976E-04  -0.330905E-03   0.863764E+00  -0.330538E-03  -0.106840E-04   0.776758E-01
  mo   18  -0.658839E-05   0.366267E+00  -0.655469E-05  -0.273193E-04   0.401217E+00  -0.166665E-04   0.652021E-04  -0.414085E+00
  mo   19  -0.698996E-05  -0.198555E-03  -0.159455E-03   0.512812E+00   0.289797E-03   0.489863E+00  -0.299028E+00  -0.503637E-04
  mo   20  -0.733371E-04   0.174816E-05   0.658611E+00   0.196469E-03   0.981416E-05   0.667806E-05  -0.131395E-05  -0.228251E-06
  mo   21   0.751848E+00   0.221565E-04   0.739313E-04   0.902833E-05  -0.137114E-05  -0.118797E-06   0.641457E-06   0.871817E-06
  mo   22  -0.805066E-05  -0.202141E-04  -0.155608E-03   0.538268E+00  -0.932442E-05  -0.560228E+00  -0.698285E-01  -0.606547E-05
  mo   23   0.284524E-05   0.120133E-03   0.101323E-03  -0.327197E+00  -0.165438E-03  -0.313179E+00   0.365303E+00   0.551855E-04
  mo   24  -0.200581E-04   0.571381E+00  -0.482659E-05   0.746595E-04   0.164209E+00   0.120685E-04   0.699884E-04  -0.410992E+00
  eigenvectors of nos in ao-basis, column    1
  0.1000593E+01 -0.1378620E-01 -0.1657423E-02  0.5406281E-07  0.1571067E-06 -0.7541251E-02  0.9106878E-07 -0.4692902E-07
  0.1847436E-03  0.6969632E-08 -0.1049863E-07  0.4278913E-04  0.1320413E-07  0.9043170E-04 -0.3212213E-02  0.2150134E-02
 -0.1011942E-02  0.2894119E-08 -0.9424243E-03 -0.3212316E-02  0.2150238E-02  0.1011955E-02  0.1510418E-07 -0.9424635E-03
  eigenvectors of nos in ao-basis, column    2
  0.9137229E-02  0.9121422E+00 -0.7425433E-01 -0.2045997E-05 -0.3004957E-05  0.1049933E+00 -0.4731070E-05  0.1192995E-05
  0.7651699E-01 -0.2974902E-06  0.3438061E-06 -0.1080765E-02 -0.2393452E-06  0.1020492E-02  0.2269959E+00 -0.1117864E+00
  0.2836604E-01  0.4114714E-06  0.2200262E-01  0.2270082E+00 -0.1117930E+00 -0.2836819E-01 -0.5255245E-06  0.2200076E-01
  eigenvectors of nos in ao-basis, column    3
 -0.7245623E-06 -0.1988122E-05  0.3273265E-05  0.2894128E-05  0.9101326E+00  0.8976887E-05 -0.2307485E-05  0.8321611E-01
  0.1561881E-05 -0.6243315E-06 -0.1686143E-01 -0.8298896E-07 -0.1377662E-06 -0.2289378E-06 -0.6917344E-05  0.8362482E-05
  0.6368073E-06  0.3066811E-01 -0.3013713E-06 -0.1591195E-05  0.2794407E-06 -0.2470582E-06  0.3066915E-01 -0.4154790E-06
  eigenvectors of nos in ao-basis, column    4
  0.1665510E-02  0.2316401E-01  0.2150622E+00  0.1227237E-03 -0.9434685E-05  0.8034587E+00 -0.2376243E-04  0.2309186E-06
 -0.1678281E-01 -0.1459873E-06  0.1156294E-05 -0.4715991E-02 -0.4434310E-05 -0.2579642E-02 -0.4217403E+00  0.1772321E+00
 -0.3751421E-01 -0.2408028E-05  0.3205688E-02 -0.4215910E+00  0.1771745E+00  0.3751600E-01 -0.1163244E-05  0.3232011E-02
  eigenvectors of nos in ao-basis, column    5
  0.2491424E-06 -0.2405738E-05 -0.3848343E-04  0.7287784E+00  0.1109497E-05 -0.1124392E-03 -0.1169254E+00 -0.1848754E-05
 -0.9373301E-06  0.4905065E-06 -0.8078874E-07  0.6249680E-06 -0.2574687E-01  0.6449352E-06 -0.5518569E+00  0.1911232E+00
 -0.2050394E-01 -0.1647388E-05 -0.3124138E-01  0.5520048E+00 -0.1911815E+00 -0.2051802E-01  0.7266231E-07  0.3123039E-01
  eigenvectors of nos in ao-basis, column    6
 -0.2787127E-04 -0.2281718E-03  0.2827081E-04 -0.1315456E+01  0.4329881E-05  0.3666207E-03  0.5933916E+00 -0.2676132E-05
 -0.2150918E-03  0.1600324E-06  0.2073547E-06  0.1507079E-05 -0.7387260E-01  0.9879154E-05 -0.1005026E+01  0.3713800E+00
 -0.4451624E-01 -0.3627602E-05 -0.2024780E-01  0.1005599E+01 -0.3716215E+00 -0.4454360E-01  0.9524661E-06  0.2022732E-01
  eigenvectors of nos in ao-basis, column    7
  0.1072138E+00  0.8101486E+00 -0.1277950E+00 -0.4120503E-03  0.1663382E-04 -0.1238290E+01  0.2053456E-03 -0.1532128E-04
  0.7911020E+00  0.4129309E-06  0.6181664E-06 -0.5206600E-02 -0.2123480E-04 -0.3462345E-01 -0.9738361E+00  0.4241514E+00
 -0.5422241E-01 -0.5038013E-05 -0.1493486E-01 -0.9732431E+00  0.4239251E+00  0.5420110E-01 -0.1807564E-05 -0.1488281E-01
  eigenvectors of nos in ao-basis, column    8
  0.2964441E-05  0.1462352E-04 -0.1563361E-04  0.4639891E-06  0.1413142E+01  0.2202093E-04  0.2720242E-05 -0.1613463E+01
 -0.2139225E-04 -0.1226844E-05  0.4069540E-01  0.1675790E-06  0.1633269E-05  0.9049674E-06  0.3329191E-04 -0.3571808E-04
 -0.5731968E-06 -0.7216417E-01  0.1469576E-05  0.4243235E-05 -0.1117603E-05  0.1026088E-05 -0.7217414E-01  0.1561841E-05
  eigenvectors of nos in ao-basis, column    9
 -0.4354240E+00 -0.2238824E+01  0.2108655E+01 -0.2395905E-06  0.1233519E-04 -0.7221005E+00 -0.1076560E-04 -0.1317908E-04
  0.1085705E+01 -0.2063462E-05  0.2668007E-05 -0.1428823E-01 -0.4053036E-05  0.7279971E-01  0.4059527E+00 -0.2252427E+00
 -0.1642884E-01 -0.2422510E-05  0.7585443E-01  0.4059873E+00 -0.2252825E+00  0.1643357E-01 -0.4706112E-05  0.7587744E-01
  eigenvectors of nos in ao-basis, column   10
  0.6873138E-04  0.3056024E-03 -0.4239685E-03 -0.5117017E+00 -0.7574905E-05 -0.8627320E-06  0.1058998E+01  0.1045960E-04
  0.4101370E-04  0.9161401E-05 -0.1338101E-05 -0.2450559E-04  0.6121022E+00  0.6239743E-04  0.6557850E+00 -0.4917317E+00
  0.1105906E+00 -0.2600455E-05 -0.2193696E-01 -0.6554695E+00  0.4915564E+00  0.1106349E+00  0.3509095E-05  0.2204714E-01
  eigenvectors of nos in ao-basis, column   11
  0.3009860E+00  0.1353823E+01 -0.1832254E+01  0.1078627E-03 -0.9495407E-05 -0.3635597E-01 -0.2477545E-03  0.1366980E-04
  0.1949475E+00  0.1322895E-04  0.1101100E-05 -0.1021350E+00 -0.1553876E-03  0.2569505E+00  0.6136613E+00 -0.3583059E+00
 -0.7094224E-01 -0.4601766E-05  0.2222120E+00  0.6139974E+00 -0.3585532E+00  0.7089903E-01  0.4593496E-05  0.2222228E+00
  eigenvectors of nos in ao-basis, column   12
  0.4831074E-05  0.2302383E-04 -0.2485484E-04 -0.8562481E-05 -0.1082687E-04  0.5856560E-06  0.1238595E-04  0.2212711E-04
 -0.6276588E-05 -0.8039177E+00  0.1982515E-04 -0.3989158E-05  0.7441440E-05  0.1653059E-05 -0.5523311E-06  0.3891899E-06
 -0.2268439E-05  0.2646368E+00  0.3356527E-05 -0.3996101E-05  0.3381596E-05  0.5574718E-05 -0.2646570E+00  0.6657760E-05
  eigenvectors of nos in ao-basis, column   13
 -0.1422241E+00 -0.6083904E+00  0.9702798E+00  0.7237320E-06  0.4530427E-05  0.1062315E+00 -0.2214408E-05 -0.1041942E-04
 -0.5241345E+00  0.1067147E-04 -0.1918387E-04 -0.2082789E+00 -0.6803843E-05 -0.2173903E+00 -0.4813405E+00  0.2740762E+00
 -0.1632193E+00  0.2260205E-05  0.8890128E-01 -0.4813418E+00  0.2740852E+00  0.1632237E+00  0.6850125E-05  0.8889986E-01
  eigenvectors of nos in ao-basis, column   14
 -0.1285677E-05 -0.5606626E-05  0.9295220E-05  0.6586833E-06 -0.8658198E-01  0.5938884E-05 -0.7624171E-06  0.3045195E+00
 -0.1521084E-04  0.2594481E-04  0.8911397E+00 -0.4353021E-05  0.1413488E-05 -0.5994958E-05 -0.7320664E-05  0.5770721E-05
 -0.2398442E-05 -0.1976775E+00  0.1182330E-05 -0.7241976E-05  0.5227520E-05  0.2857304E-05 -0.1976731E+00  0.1654119E-05
  eigenvectors of nos in ao-basis, column   15
 -0.3585975E+00 -0.1357220E+01  0.2716053E+01 -0.7534544E-03  0.4275708E-05  0.1248839E+00  0.1979426E-02 -0.6541485E-05
 -0.1159203E+01  0.2070765E-05  0.2025246E-05 -0.2883079E-01 -0.1246541E-02  0.4146486E+00 -0.2253395E+01  0.1261196E+01
 -0.7384336E-01  0.1857905E-05 -0.1571750E+00 -0.2257761E+01  0.1263493E+01  0.7404633E-01 -0.4109662E-06 -0.1564045E+00
  eigenvectors of nos in ao-basis, column   16
 -0.5284427E-03 -0.1983751E-02  0.4116125E-02  0.4806461E+00  0.2240336E-05  0.1242914E-03 -0.1270166E+01 -0.2691573E-05
 -0.1703248E-02 -0.4349670E-05 -0.1187679E-05 -0.5561789E-04  0.8110612E+00  0.6525680E-03 -0.1412946E+01  0.7360698E+00
 -0.7853198E-01 -0.5489937E-05  0.2581059E+00  0.1405951E+01 -0.7321488E+00 -0.7827401E-01  0.3263309E-05 -0.2586947E+00
  eigenvectors of nos in ao-basis, column   17
  0.3660673E-05  0.1711894E-04 -0.7399901E-05 -0.2365544E-04 -0.1091774E-04 -0.8519627E-05 -0.1382308E-04  0.8612389E-04
 -0.2545526E-05  0.7211929E+00 -0.6498141E-04  0.2352655E-05  0.2356359E-04  0.3402914E-06 -0.8214254E-04  0.6956619E-04
 -0.1880347E-04  0.7389042E+00 -0.1972890E-04  0.4863865E-04 -0.4344795E-04  0.2455660E-05 -0.7390765E+00  0.2156898E-04
  eigenvectors of nos in ao-basis, column   18
  0.9245951E-04  0.4220189E-03 -0.1276065E-03  0.8509435E+00 -0.4949812E-05 -0.2547522E-03  0.2131786E+00  0.5285795E-05
  0.8901484E-04  0.2095658E-04  0.1518103E-05  0.6845194E-05 -0.6532742E+00  0.4840041E-04  0.1779295E+01 -0.1607088E+01
  0.3485518E+00  0.2556718E-04  0.7180970E+00 -0.1779972E+01  0.1607601E+01  0.3489731E+00 -0.2114463E-04 -0.7183823E+00
  eigenvectors of nos in ao-basis, column   19
  0.4806526E-04  0.2470949E-03  0.1328666E-03 -0.1280662E-04  0.5323288E-01 -0.1911774E-03  0.1733525E-04 -0.7752287E+00
 -0.1474891E-03  0.6985060E-04  0.5686368E+00  0.4472944E-04  0.5521871E-05  0.2981487E-04 -0.4936432E-03  0.3307821E-03
 -0.2890883E-03  0.8460372E+00 -0.1479816E-04 -0.4851385E-03  0.3153694E-03  0.2633593E-03  0.8458805E+00 -0.2102273E-04
  eigenvectors of nos in ao-basis, column   20
 -0.1535258E+00 -0.7942236E+00 -0.4473856E+00 -0.4420302E-05  0.1158117E-04  0.6039884E+00  0.3997152E-03 -0.2257680E-03
  0.5386522E+00  0.8431520E-05  0.1695625E-03 -0.1545642E+00 -0.8673626E-04 -0.9355674E-01  0.1633319E+01 -0.1084707E+01
  0.9156968E+00  0.2627914E-03  0.3028937E-01  0.1632430E+01 -0.1084130E+01 -0.9159653E+00  0.2426088E-03  0.2980350E-01
  eigenvectors of nos in ao-basis, column   21
 -0.1412592E-03 -0.6363301E-03  0.2430435E-03  0.6623765E+00  0.1100682E-06  0.3451583E-03 -0.1239217E+01 -0.1126091E-04
 -0.1519117E-03 -0.1134691E-05  0.8384739E-05  0.1274474E-05 -0.1799310E+00 -0.6879959E-04 -0.2215409E+00 -0.1841907E+00
  0.7711280E+00  0.1073752E-04 -0.3086680E+00  0.2223770E+00  0.1835415E+00  0.7705666E+00  0.1535493E-04  0.3091363E+00
  eigenvectors of nos in ao-basis, column   22
 -0.3249810E+00 -0.1382458E+01  0.1109938E+01 -0.2788246E-04 -0.4156502E-05  0.5916289E+00  0.2995030E-03 -0.8744407E-06
 -0.9298975E+00 -0.5621935E-07  0.5787830E-05  0.1538491E+00 -0.6249686E-05 -0.1809031E+00  0.6308568E-01 -0.2044327E+00
  0.9417080E-01  0.8048936E-05  0.8266355E+00  0.6258556E-01 -0.2040789E+00 -0.9450391E-01  0.8897096E-05  0.8261923E+00
  eigenvectors of nos in ao-basis, column   23
 -0.3268598E+00 -0.1131733E+01  0.3921063E+01  0.5761072E-04 -0.8379485E-05 -0.3466143E+00  0.2595962E-03  0.1363168E-04
 -0.9483588E+00  0.4790068E-06 -0.1108359E-05  0.2119832E-01 -0.9440995E-04  0.1834602E+00 -0.1154013E+01 -0.4887466E+00
 -0.7405733E+00 -0.4481347E-06 -0.4932580E+00 -0.1154334E+01 -0.4891178E+00  0.7408074E+00 -0.3160112E-05 -0.4934654E+00
  eigenvectors of nos in ao-basis, column   24
 -0.4941919E-04 -0.1717369E-03  0.6036459E-03 -0.3558051E+00 -0.4313466E-05 -0.5750925E-04 -0.1592936E+01  0.6181010E-05
 -0.1341843E-03  0.7640761E-06 -0.2768668E-06  0.1941720E-05  0.5552923E+00  0.2785461E-04 -0.9354380E+00 -0.1221015E+01
 -0.7139602E+00  0.1336210E-05 -0.6477955E+00  0.9351137E+00  0.1220835E+01 -0.7137321E+00 -0.1146181E-05  0.6476299E+00


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***


 cosurf_and_phi
  2.466315385316 -0.061539892486  2.837440149000 -0.036948237104
  0.006359863210 -2.199287675687  3.114544749440 -0.066236355905
 -0.480526931872  1.061509344774  3.755560521090 -0.067963854720
  1.262739748330  2.872870034186  1.567866969040 -0.044305667163
  1.897949994968 -2.634366215369  0.570962659525 -0.015545871663
 -2.790166444607 -0.824331206483  2.170430257018 -0.021023337280
 -2.061296060778  2.493528161233  1.034251636268 -0.021520329024
 -0.002991703587  2.420846177436 -1.447622679829  0.014401971874
 -1.114435211431 -2.949559801670 -0.067898604227 -0.015702841261
  1.955709175590 -0.984462672186  3.123501329999 -0.049436724368
  1.000900154469 -1.706149424342  3.300403738319 -0.062882115127
  1.652947872482  0.360731428865  3.496571379926 -0.056229660901
  0.594940552015  0.670555958619  3.845537291721 -0.067670848184
  2.390078334690  1.202722009900  2.566708449184 -0.036231440406
  1.989334673621  2.229216060677  2.001028520754 -0.038439572076
  2.919143888229  0.378144896597  2.099775822683 -0.015941249473
  2.780367109803 -0.921046401167  2.130496470151 -0.020749024370
  2.449941849009 -2.011800733381  1.439000205618 -0.019027250043
 -0.231540571447 -1.263621120083  3.706953994699 -0.068580476987
 -0.352106389130 -0.107618694495  3.950679056227 -0.068880587296
  0.105380882100  1.878884201281  3.371423292662 -0.067501485324
  0.783266721831  2.590884380442  2.520834408126 -0.059339968489
  2.011402526412  2.551496219550  0.814855178005 -0.018207628618
  1.440220538007 -2.843720084632  1.356653310131 -0.038883028133
  0.871209499702 -2.660587439486  2.372618950166 -0.057369306058
 -2.947488997374  0.298617382934  2.058332654596 -0.014345261496
 -2.674954162172  1.563027715274  1.704200253745 -0.017139560428
 -1.353448939749  2.910920816403  0.212056013565 -0.018548417684
 -0.743422400795  2.810699746106 -0.731960510989 -0.003037541308
  0.099626804209  1.855073908563 -1.945822518898  0.033107518575
  0.019900458911 -1.968682238442 -1.864952523830  0.029713256139
 -0.601526683996 -2.669374282549 -1.032942810695  0.004371509535
 -1.976701730993 -2.578422698078  0.816296596419 -0.019084332124
 -2.525639241426 -1.850752473194  1.593331825886 -0.019645760148
 -1.124962231191 -1.881571875050  3.121019991266 -0.060768801300
 -2.117398618237 -1.513942036836  2.667866282109 -0.043003317594
 -2.336046769995 -0.152318885910  2.976109389266 -0.040980711235
 -1.478775916646  0.469153703894  3.577445396081 -0.058936882954
 -1.328587214463  1.668007282102  3.174271253825 -0.058885620819
 -1.771761195387  2.287385908353  2.202255939279 -0.044726600500
 -1.026730149698  3.017318998523  1.358608629268 -0.043974249605
  0.119020599620  3.173161356543  1.415159765853 -0.049762372392
  0.915237473358  3.108118624501  0.463299650838 -0.029796566923
  0.462814589486  2.837210699514 -0.795516582628 -0.003882058798
  1.026812912753 -3.017319139618  0.083991013862 -0.020481597538
 -0.070059393274 -3.176418651270  0.035610954337 -0.025716343545
 -0.970268948715 -3.083313212042  1.062474740253 -0.039980899750
 -0.534203029846 -2.828286137107  2.231267851728 -0.058139276805
  0.871897873699 -0.575451888002  3.799144800425 -0.065812045379
  1.408719892640  1.640288870679  3.148120355694 -0.057772071185
  2.650053602975  1.633766196698  1.655441764425 -0.017042167936
  1.964286464189 -2.001027831890  2.365093852582 -0.043048615085
 -1.342877128538 -0.760711330777  3.581800927521 -0.060620077541
 -0.531365927285  2.661712102822  2.509437292848 -0.060710347123
  1.142904167226  2.885766749848 -0.243475252462 -0.010837856290
  0.342129526197 -3.189261637688  1.246854200824 -0.046877711936
 -2.346459445268  1.135413320199  2.662806558936 -0.038140809107
 -0.258565605057  3.205542092831  0.249849913336 -0.029827963155
  0.450566961335 -2.699565911226 -1.032013446782  0.002952228383
 -1.684533476150 -2.430522251364  2.070179818920 -0.044673896725
 -3.368784049130  0.036613212616 -1.854972615293  0.060469437038
 -3.278396927799  1.580834404538 -0.078603229040  0.033530891322
 -3.656110378332 -0.713812229513  0.361831391088  0.034309010530
 -2.408052717333 -2.075622947728 -1.226189024363  0.038674467501
 -1.295829484895 -0.567704086865 -2.747607986898  0.058256761361
 -1.846557165647  1.681693338816 -2.099716493181  0.048652315991
 -3.831862607217  0.357468890277 -0.654813288033  0.050595141979
 -3.527736967644 -1.045671231370 -1.064852892071  0.051742115747
 -2.622906831544 -1.024738705101 -2.241124222290  0.058143983723
 -2.402781990555  0.378472303440 -2.579763034114  0.061389417685
 -3.126606927219  1.313275381963 -1.541857021673  0.053254012269
 -3.633182667875  0.531520753705  0.561856665829  0.031243449681
 -3.086813000261 -1.794874586082 -0.179700355757  0.031592547216
 -1.615880090830 -1.674393887320 -2.147504235692  0.047807648701
 -1.240258025012  0.765881087348 -2.687977655831  0.057011537311
 -2.430292517958  2.154437082790 -0.969924007583  0.034238402396
  3.368776503197 -0.036639040867 -1.854966842961  0.060460385728
  3.278392620256 -1.580850766330 -0.078589062660  0.033525320738
  3.656106873706  0.713798214804  0.361832640492  0.034303555870
  2.408046317685  2.075600470298 -1.226192756897  0.038665996771
  1.295820311657  0.567673501678 -2.747601655947  0.058248004567
  1.846549173548 -1.681720471299 -2.099699178989  0.048644102081
  3.831857249217 -0.357488322766 -0.654806650060  0.050588068459
  3.527730862121  1.045649613724 -1.064853177123  0.051734164369
  2.622898581638  1.024710819001 -2.241122746235  0.058134324206
  2.402773123306 -0.378501994157 -2.579753678917  0.061379610178
  3.126599952113 -1.313299541573 -1.541844004398  0.053245649541
  3.633179527908 -0.531533702443  0.561864593522  0.031238749280
  3.086808508398  1.794857685487 -0.179703829578  0.031585792683
  1.615872011596  1.674366500124 -2.147504385867  0.047798599043
  1.240248960489 -0.765911354740 -2.687964116774  0.057003437720
  2.430286585511 -2.154458194501 -0.969905238283  0.034231753116
  0.000006852884  1.035694667087 -2.361676372823  0.051464075797
 -0.298806015181  0.969607486526 -2.408809074493  0.052944151006
  0.298815704890  0.969607820141 -2.408807386530  0.052941195544
  0.000007656756 -1.035723195601 -2.361670853435  0.051464695601
 -0.298805262603 -0.969636498141 -2.408803907288  0.052944791408
  0.298816457468 -0.969636367899 -2.408802219325  0.052941664316
 -0.667712940797 -1.245186997899 -2.338574529312  0.050534978928
 -1.218112188165 -2.025138759161 -1.616566947615  0.035878481275
 -2.084175601108 -2.294008802021 -0.480477682822  0.020489712718
 -2.876631896395 -1.658047550445  0.559052047065  0.017373745329
 -3.282909221331 -0.368099862162  1.091996180628  0.019595240312
 -3.142757910518  1.067034798172  0.908143333087  0.018537804582
 -2.511458431963  2.081290260960  0.080011352455  0.017573255524
 -1.638016880752  2.274609499719 -1.065756198713  0.026731481143
 -0.866948462969  1.570740798135 -2.077229430584  0.046437516380
 -0.467915446486 -1.694563900014 -1.941501402531  0.039244810793
 -1.299753513329 -2.453901457341 -0.850306853788  0.012505416282
 -2.242033801688 -2.245340287831  0.385760998463  0.000979468854
 -2.923088754321 -1.151144046328  1.279154738758  0.003115323119
 -3.074287016292  0.397098864814  1.477489335582  0.004756434730
 -2.635990824421  1.788708515315  0.902534843950  0.001162956350
 -1.781079179779  2.474786487342 -0.218927030025  0.004455143962
 -0.846758459860  2.184720175398 -1.444553386477  0.024675227585
 -0.255852300976  1.360631011730 -2.219692209228  0.046029321911
  0.667723897920  1.245157743773 -2.338577856151  0.050528572124
  1.218123388571  2.025110412626 -1.616576841774  0.035870328088
  2.084186643139  2.293984793080 -0.480493513306  0.020482565101
  2.876642520254  1.658030225134  0.559035126479  0.017368814577
  3.282919570196  0.368088739237  1.091983758387  0.019591930013
  3.142768358070 -1.067043033853  0.908139383421  0.018534849597
  2.511469270857 -2.081300494445  0.080016452498  0.017569304645
  1.638028059662 -2.274626132712 -1.065745290088  0.026725877071
  0.866959534092 -1.570765766224 -2.077218589767  0.046431519999
  0.467932162408  1.694535407938 -1.941510815499  0.039239897238
  1.299770347024  2.453875604722 -0.850324284820  0.012498951219
  2.242050270258  2.245320705184  0.385739526123  0.000974548956
  2.923104801922  1.151131828431  1.279135167181  0.003112485368
  3.074302957696 -0.397105856761  1.477477125466  0.004754845637
  2.636007061705 -1.788714946455  0.902532611959  0.001161409715
  1.781095866662 -2.474797662024 -0.218920775640  0.004452389147
  0.846775315928 -2.184739733041 -1.444543821640  0.024671722130
  0.255868904327 -1.360657816862 -2.219684436601  0.046027771976
  0.427045418766 -0.580992742084 -2.585105089006  0.056544848255
  0.427044967836  0.580963354323 -2.585108185092  0.056544612589
 -0.427035838172 -0.000014913339 -2.543076305315  0.061769354995
 end_of_phi


 nsubv after electrostatic potential =  0

  Confirmation of dielectric energy for ground state
  edielnew =  -0.0117192087


 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************


 cosurf_and_qcos_from cosmo(3
  2.466315385316 -0.061539892486  2.837440149000  0.002277933067
  0.006359863210 -2.199287675687  3.114544749440  0.006179955964
 -0.480526931872  1.061509344774  3.755560521090  0.005667648171
  1.262739748330  2.872870034186  1.567866969040  0.004988895245
  1.897949994968 -2.634366215369  0.570962659525  0.001842224483
 -2.790166444607 -0.824331206483  2.170430257018  0.000999659057
 -2.061296060778  2.493528161233  1.034251636268  0.002842228267
 -0.002991703587  2.420846177436 -1.447622679829  0.000771574711
 -1.114435211431 -2.949559801670 -0.067898604227  0.002951319597
  1.955709175590 -0.984462672186  3.123501329999  0.004622933246
  1.000900154469 -1.706149424342  3.300403738319  0.006039967955
  1.652947872482  0.360731428865  3.496571379926  0.005182531817
  0.594940552015  0.670555958619  3.845537291721  0.005543612574
  2.390078334690  1.202722009900  2.566708449184  0.003382987163
  1.989334673621  2.229216060677  2.001028520754  0.003437495380
  2.919143888229  0.378144896597  2.099775822683  0.000452956016
  2.780367109803 -0.921046401167  2.130496470151  0.001444661009
  2.449941849009 -2.011800733381  1.439000205618  0.001857823424
 -0.231540571447 -1.263621120083  3.706953994699  0.006436320987
 -0.352106389130 -0.107618694495  3.950679056227  0.006195015259
  0.105380882100  1.878884201281  3.371423292662  0.007988063210
  0.783266721831  2.590884380442  2.520834408126  0.007114450017
  2.011402526412  2.551496219550  0.814855178005  0.002356694110
  1.440220538007 -2.843720084632  1.356653310131  0.004567990427
  0.871209499702 -2.660587439486  2.372618950166  0.007675736580
 -2.947488997374  0.298617382934  2.058332654596  0.000426791242
 -2.674954162172  1.563027715274  1.704200253745  0.001135730851
 -1.353448939749  2.910920816403  0.212056013565  0.003302313633
 -0.743422400795  2.810699746106 -0.731960510989  0.002250321225
  0.099626804209  1.855073908563 -1.945822518898 -0.000127965682
  0.019900458911 -1.968682238442 -1.864952523830 -0.000360402348
 -0.601526683996 -2.669374282549 -1.032942810695  0.001327461553
 -1.976701730993 -2.578422698078  0.816296596419  0.002521396996
 -2.525639241426 -1.850752473194  1.593331825886  0.001665296846
 -1.124962231191 -1.881571875050  3.121019991266  0.005942029678
 -2.117398618237 -1.513942036836  2.667866282109  0.003927662493
 -2.336046769995 -0.152318885910  2.976109389266  0.004081961542
 -1.478775916646  0.469153703894  3.577445396081  0.005104072891
 -1.328587214463  1.668007282102  3.174271253825  0.006092086074
 -1.771761195387  2.287385908353  2.202255939279  0.005797738913
 -1.026730149698  3.017318998523  1.358608629268  0.005722365865
  0.119020599620  3.173161356543  1.415159765853  0.007039182962
  0.915237473358  3.108118624501  0.463299650838  0.004868792242
  0.462814589486  2.837210699514 -0.795516582628  0.002352018570
  1.026812912753 -3.017319139618  0.083991013862  0.004070832048
 -0.070059393274 -3.176418651270  0.035610954337  0.004674766969
 -0.970268948715 -3.083313212042  1.062474740253  0.006557735775
 -0.534203029846 -2.828286137107  2.231267851728  0.007963544080
  0.871897873699 -0.575451888002  3.799144800425  0.006450969162
  1.408719892640  1.640288870679  3.148120355694  0.006972641430
  2.650053602975  1.633766196698  1.655441764425  0.001050848468
  1.964286464189 -2.001027831890  2.365093852582  0.004703002238
 -1.342877128538 -0.760711330777  3.581800927521  0.005315342812
 -0.531365927285  2.661712102822  2.509437292848  0.008018175852
  1.142904167226  2.885766749848 -0.243475252462  0.001394526664
  0.342129526197 -3.189261637688  1.246854200824  0.006852600373
 -2.346459445268  1.135413320199  2.662806558936  0.003492363027
 -0.258565605057  3.205542092831  0.249849913336  0.004497834846
  0.450566961335 -2.699565911226 -1.032013446782  0.002384867382
 -1.684533476150 -2.430522251364  2.070179818920  0.004966588906
 -3.368784049130  0.036613212616 -1.854972615293 -0.007659529193
 -3.278396927799  1.580834404538 -0.078603229040 -0.005713701588
 -3.656110378332 -0.713812229513  0.361831391088 -0.006419086643
 -2.408052717333 -2.075622947728 -1.226189024363 -0.005342852394
 -1.295829484895 -0.567704086865 -2.747607986898 -0.005836323014
 -1.846557165647  1.681693338816 -2.099716493181 -0.006001435325
 -3.831862607217  0.357468890277 -0.654813288033 -0.008291397181
 -3.527736967644 -1.045671231370 -1.064852892071 -0.008193206708
 -2.622906831544 -1.024738705101 -2.241124222290 -0.008094011900
 -2.402781990555  0.378472303440 -2.579763034114 -0.007869164312
 -3.126606927219  1.313275381963 -1.541857021673 -0.008332340113
 -3.633182667875  0.531520753705  0.561856665829 -0.004509728200
 -3.086813000261 -1.794874586082 -0.179700355757 -0.004473606298
 -1.615880090830 -1.674393887320 -2.147504235692 -0.004583087670
 -1.240258025012  0.765881087348 -2.687977655831 -0.004821076561
 -2.430292517958  2.154437082790 -0.969924007583 -0.004034215471
  3.368776503197 -0.036639040867 -1.854966842961 -0.007665126118
  3.278392620256 -1.580850766330 -0.078589062660 -0.005699040367
  3.656106873706  0.713798214804  0.361832640492 -0.006413850063
  2.408046317685  2.075600470298 -1.226192756897 -0.005350751025
  1.295820311657  0.567673501678 -2.747601655947 -0.006255201658
  1.846549173548 -1.681720471299 -2.099699178989 -0.005954472342
  3.831857249217 -0.357488322766 -0.654806650060 -0.008290716382
  3.527730862121  1.045649613724 -1.064853177123 -0.008196007757
  2.622898581638  1.024710819001 -2.241122746235 -0.008079656765
  2.402773123306 -0.378501994157 -2.579753678917 -0.007859446149
  3.126599952113 -1.313299541573 -1.541844004398 -0.008336259618
  3.633179527908 -0.531533702443  0.561864593522 -0.004505471508
  3.086808508398  1.794857685487 -0.179703829578 -0.004470782830
  1.615872011596  1.674366500124 -2.147504385867 -0.004548771308
  1.240248960489 -0.765911354740 -2.687964116774 -0.005078441660
  2.430286585511 -2.154458194501 -0.969905238283 -0.004020115100
  0.000006852884  1.035694667087 -2.361676372823 -0.000280526300
 -0.298806015181  0.969607486526 -2.408809074493 -0.000337791613
  0.298815704890  0.969607820141 -2.408807386530 -0.000248517439
  0.000007656756 -1.035723195601 -2.361670853435 -0.000273941662
 -0.298805262603 -0.969636498141 -2.408803907288 -0.000323620399
  0.298816457468 -0.969636367899 -2.408802219325 -0.000245352617
 -0.667712940797 -1.245186997899 -2.338574529312 -0.000933732050
 -1.218112188165 -2.025138759161 -1.616566947615 -0.001395818023
 -2.084175601108 -2.294008802021 -0.480477682822 -0.000934273582
 -2.876631896395 -1.658047550445  0.559052047065 -0.001364257518
 -3.282909221331 -0.368099862162  1.091996180628 -0.001818765564
 -3.142757910518  1.067034798172  0.908143333087 -0.001627814272
 -2.511458431963  2.081290260960  0.080011352455 -0.001165599802
 -1.638016880752  2.274609499719 -1.065756198713 -0.001117698036
 -0.866948462969  1.570740798135 -2.077229430584 -0.001936575231
 -0.467915446486 -1.694563900014 -1.941501402531 -0.001399092909
 -1.299753513329 -2.453901457341 -0.850306853788 -0.000230478803
 -2.242033801688 -2.245340287831  0.385760998463 -0.000309422472
 -2.923088754321 -1.151144046328  1.279154738758 -0.000899967648
 -3.074287016292  0.397098864814  1.477489335582 -0.001174742132
 -2.635990824421  1.788708515315  0.902534843950 -0.000529435176
 -1.781079179779  2.474786487342 -0.218927030025 -0.000174503735
 -0.846758459860  2.184720175398 -1.444553386477 -0.000826813903
 -0.255852300976  1.360631011730 -2.219692209228 -0.000898901149
  0.667723897920  1.245157743773 -2.338577856151 -0.000867583644
  1.218123388571  2.025110412626 -1.616576841774 -0.001407697394
  2.084186643139  2.293984793080 -0.480493513306 -0.000911875309
  2.876642520254  1.658030225134  0.559035126479 -0.001358750433
  3.282919570196  0.368088739237  1.091983758387 -0.001807479335
  3.142768358070 -1.067043033853  0.908139383421 -0.001620763083
  2.511469270857 -2.081300494445  0.080016452498 -0.001106516474
  1.638028059662 -2.274626132712 -1.065745290088 -0.001076556059
  0.866959534092 -1.570765766224 -2.077218589767 -0.001886007283
  0.467932162408  1.694535407938 -1.941510815499 -0.001453641672
  1.299770347024  2.453875604722 -0.850324284820 -0.000240904760
  2.242050270258  2.245320705184  0.385739526123 -0.000239896448
  2.923104801922  1.151131828431  1.279135167181 -0.000926139925
  3.074302957696 -0.397105856761  1.477477125466 -0.001204394798
  2.636007061705 -1.788714946455  0.902532611959 -0.000574338679
  1.781095866662 -2.474797662024 -0.218920775640 -0.000229117164
  0.846775315928 -2.184739733041 -1.444543821640 -0.000878295075
  0.255868904327 -1.360657816862 -2.219684436601 -0.000832589754
  0.427045418766 -0.580992742084 -2.585105089006 -0.002192877009
  0.427044967836  0.580963354323 -2.585108185092 -0.002066857721
 -0.427035838172 -0.000014913339 -2.543076305315 -0.005928065501
 end_of_qcos

 sum of the negative charges =  -0.250645231

 sum of the positive charges =   0.245744541

 total sum =  -0.00490068946

 *** qcos is copied to qcosdalton ***

 fepsi =   1.
 *** qcosdalton is mult by fepsi ***
 cosurf and qcosdalton
  2.466315385316 -0.061539892486  2.837440149000  0.002277933067
  0.006359863210 -2.199287675687  3.114544749440  0.006179955964
 -0.480526931872  1.061509344774  3.755560521090  0.005667648171
  1.262739748330  2.872870034186  1.567866969040  0.004988895245
  1.897949994968 -2.634366215369  0.570962659525  0.001842224483
 -2.790166444607 -0.824331206483  2.170430257018  0.000999659057
 -2.061296060778  2.493528161233  1.034251636268  0.002842228267
 -0.002991703587  2.420846177436 -1.447622679829  0.000771574711
 -1.114435211431 -2.949559801670 -0.067898604227  0.002951319597
  1.955709175590 -0.984462672186  3.123501329999  0.004622933246
  1.000900154469 -1.706149424342  3.300403738319  0.006039967955
  1.652947872482  0.360731428865  3.496571379926  0.005182531817
  0.594940552015  0.670555958619  3.845537291721  0.005543612574
  2.390078334690  1.202722009900  2.566708449184  0.003382987163
  1.989334673621  2.229216060677  2.001028520754  0.003437495380
  2.919143888229  0.378144896597  2.099775822683  0.000452956016
  2.780367109803 -0.921046401167  2.130496470151  0.001444661009
  2.449941849009 -2.011800733381  1.439000205618  0.001857823424
 -0.231540571447 -1.263621120083  3.706953994699  0.006436320987
 -0.352106389130 -0.107618694495  3.950679056227  0.006195015259
  0.105380882100  1.878884201281  3.371423292662  0.007988063210
  0.783266721831  2.590884380442  2.520834408126  0.007114450017
  2.011402526412  2.551496219550  0.814855178005  0.002356694110
  1.440220538007 -2.843720084632  1.356653310131  0.004567990427
  0.871209499702 -2.660587439486  2.372618950166  0.007675736580
 -2.947488997374  0.298617382934  2.058332654596  0.000426791242
 -2.674954162172  1.563027715274  1.704200253745  0.001135730851
 -1.353448939749  2.910920816403  0.212056013565  0.003302313633
 -0.743422400795  2.810699746106 -0.731960510989  0.002250321225
  0.099626804209  1.855073908563 -1.945822518898 -0.000127965682
  0.019900458911 -1.968682238442 -1.864952523830 -0.000360402348
 -0.601526683996 -2.669374282549 -1.032942810695  0.001327461553
 -1.976701730993 -2.578422698078  0.816296596419  0.002521396996
 -2.525639241426 -1.850752473194  1.593331825886  0.001665296846
 -1.124962231191 -1.881571875050  3.121019991266  0.005942029678
 -2.117398618237 -1.513942036836  2.667866282109  0.003927662493
 -2.336046769995 -0.152318885910  2.976109389266  0.004081961542
 -1.478775916646  0.469153703894  3.577445396081  0.005104072891
 -1.328587214463  1.668007282102  3.174271253825  0.006092086074
 -1.771761195387  2.287385908353  2.202255939279  0.005797738913
 -1.026730149698  3.017318998523  1.358608629268  0.005722365865
  0.119020599620  3.173161356543  1.415159765853  0.007039182962
  0.915237473358  3.108118624501  0.463299650838  0.004868792242
  0.462814589486  2.837210699514 -0.795516582628  0.002352018570
  1.026812912753 -3.017319139618  0.083991013862  0.004070832048
 -0.070059393274 -3.176418651270  0.035610954337  0.004674766969
 -0.970268948715 -3.083313212042  1.062474740253  0.006557735775
 -0.534203029846 -2.828286137107  2.231267851728  0.007963544080
  0.871897873699 -0.575451888002  3.799144800425  0.006450969162
  1.408719892640  1.640288870679  3.148120355694  0.006972641430
  2.650053602975  1.633766196698  1.655441764425  0.001050848468
  1.964286464189 -2.001027831890  2.365093852582  0.004703002238
 -1.342877128538 -0.760711330777  3.581800927521  0.005315342812
 -0.531365927285  2.661712102822  2.509437292848  0.008018175852
  1.142904167226  2.885766749848 -0.243475252462  0.001394526664
  0.342129526197 -3.189261637688  1.246854200824  0.006852600373
 -2.346459445268  1.135413320199  2.662806558936  0.003492363027
 -0.258565605057  3.205542092831  0.249849913336  0.004497834846
  0.450566961335 -2.699565911226 -1.032013446782  0.002384867382
 -1.684533476150 -2.430522251364  2.070179818920  0.004966588906
 -3.368784049130  0.036613212616 -1.854972615293 -0.007659529193
 -3.278396927799  1.580834404538 -0.078603229040 -0.005713701588
 -3.656110378332 -0.713812229513  0.361831391088 -0.006419086643
 -2.408052717333 -2.075622947728 -1.226189024363 -0.005342852394
 -1.295829484895 -0.567704086865 -2.747607986898 -0.005836323014
 -1.846557165647  1.681693338816 -2.099716493181 -0.006001435325
 -3.831862607217  0.357468890277 -0.654813288033 -0.008291397181
 -3.527736967644 -1.045671231370 -1.064852892071 -0.008193206708
 -2.622906831544 -1.024738705101 -2.241124222290 -0.008094011900
 -2.402781990555  0.378472303440 -2.579763034114 -0.007869164312
 -3.126606927219  1.313275381963 -1.541857021673 -0.008332340113
 -3.633182667875  0.531520753705  0.561856665829 -0.004509728200
 -3.086813000261 -1.794874586082 -0.179700355757 -0.004473606298
 -1.615880090830 -1.674393887320 -2.147504235692 -0.004583087670
 -1.240258025012  0.765881087348 -2.687977655831 -0.004821076561
 -2.430292517958  2.154437082790 -0.969924007583 -0.004034215471
  3.368776503197 -0.036639040867 -1.854966842961 -0.007665126118
  3.278392620256 -1.580850766330 -0.078589062660 -0.005699040367
  3.656106873706  0.713798214804  0.361832640492 -0.006413850063
  2.408046317685  2.075600470298 -1.226192756897 -0.005350751025
  1.295820311657  0.567673501678 -2.747601655947 -0.006255201658
  1.846549173548 -1.681720471299 -2.099699178989 -0.005954472342
  3.831857249217 -0.357488322766 -0.654806650060 -0.008290716382
  3.527730862121  1.045649613724 -1.064853177123 -0.008196007757
  2.622898581638  1.024710819001 -2.241122746235 -0.008079656765
  2.402773123306 -0.378501994157 -2.579753678917 -0.007859446149
  3.126599952113 -1.313299541573 -1.541844004398 -0.008336259618
  3.633179527908 -0.531533702443  0.561864593522 -0.004505471508
  3.086808508398  1.794857685487 -0.179703829578 -0.004470782830
  1.615872011596  1.674366500124 -2.147504385867 -0.004548771308
  1.240248960489 -0.765911354740 -2.687964116774 -0.005078441660
  2.430286585511 -2.154458194501 -0.969905238283 -0.004020115100
  0.000006852884  1.035694667087 -2.361676372823 -0.000280526300
 -0.298806015181  0.969607486526 -2.408809074493 -0.000337791613
  0.298815704890  0.969607820141 -2.408807386530 -0.000248517439
  0.000007656756 -1.035723195601 -2.361670853435 -0.000273941662
 -0.298805262603 -0.969636498141 -2.408803907288 -0.000323620399
  0.298816457468 -0.969636367899 -2.408802219325 -0.000245352617
 -0.667712940797 -1.245186997899 -2.338574529312 -0.000933732050
 -1.218112188165 -2.025138759161 -1.616566947615 -0.001395818023
 -2.084175601108 -2.294008802021 -0.480477682822 -0.000934273582
 -2.876631896395 -1.658047550445  0.559052047065 -0.001364257518
 -3.282909221331 -0.368099862162  1.091996180628 -0.001818765564
 -3.142757910518  1.067034798172  0.908143333087 -0.001627814272
 -2.511458431963  2.081290260960  0.080011352455 -0.001165599802
 -1.638016880752  2.274609499719 -1.065756198713 -0.001117698036
 -0.866948462969  1.570740798135 -2.077229430584 -0.001936575231
 -0.467915446486 -1.694563900014 -1.941501402531 -0.001399092909
 -1.299753513329 -2.453901457341 -0.850306853788 -0.000230478803
 -2.242033801688 -2.245340287831  0.385760998463 -0.000309422472
 -2.923088754321 -1.151144046328  1.279154738758 -0.000899967648
 -3.074287016292  0.397098864814  1.477489335582 -0.001174742132
 -2.635990824421  1.788708515315  0.902534843950 -0.000529435176
 -1.781079179779  2.474786487342 -0.218927030025 -0.000174503735
 -0.846758459860  2.184720175398 -1.444553386477 -0.000826813903
 -0.255852300976  1.360631011730 -2.219692209228 -0.000898901149
  0.667723897920  1.245157743773 -2.338577856151 -0.000867583644
  1.218123388571  2.025110412626 -1.616576841774 -0.001407697394
  2.084186643139  2.293984793080 -0.480493513306 -0.000911875309
  2.876642520254  1.658030225134  0.559035126479 -0.001358750433
  3.282919570196  0.368088739237  1.091983758387 -0.001807479335
  3.142768358070 -1.067043033853  0.908139383421 -0.001620763083
  2.511469270857 -2.081300494445  0.080016452498 -0.001106516474
  1.638028059662 -2.274626132712 -1.065745290088 -0.001076556059
  0.866959534092 -1.570765766224 -2.077218589767 -0.001886007283
  0.467932162408  1.694535407938 -1.941510815499 -0.001453641672
  1.299770347024  2.453875604722 -0.850324284820 -0.000240904760
  2.242050270258  2.245320705184  0.385739526123 -0.000239896448
  2.923104801922  1.151131828431  1.279135167181 -0.000926139925
  3.074302957696 -0.397105856761  1.477477125466 -0.001204394798
  2.636007061705 -1.788714946455  0.902532611959 -0.000574338679
  1.781095866662 -2.474797662024 -0.218920775640 -0.000229117164
  0.846775315928 -2.184739733041 -1.444543821640 -0.000878295075
  0.255868904327 -1.360657816862 -2.219684436601 -0.000832589754
  0.427045418766 -0.580992742084 -2.585105089006 -0.002192877009
  0.427044967836  0.580963354323 -2.585108185092 -0.002066857721
 -0.427035838172 -0.000014913339 -2.543076305315 -0.005928065501
 end of cosurf and qcosdalton

 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 ** Total nuclear repulsion energy ** =   9.36042967970580
 screening nuclear repulsion energy   0.00742371076

 Total-screening nuclear repulsion energy   9.35300597


 Adding T+Vsolv ...
 maxdens  300
 *** End of DALTON-COSMO calculation ***

  original map vector  20 21 22 23 24 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17
 18 19 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0


          sorted Vsolv matrix  block   1

               nbf   1        nbf   2        nbf   3        nbf   4        nbf   5        nbf   6        nbf   7        nbf   8
  nbf   1    -3.31816074
  nbf   2     0.00001513    -3.09074408
  nbf   3     0.00001778    -0.98129387    -4.10179203
  nbf   4    -1.01841551     0.00000421     0.00001630    -4.07794671
  nbf   5    -0.42231787     0.00001242     0.00001024    -0.44122065    -4.75237624
  nbf   6    -0.00000504    -0.00000336     0.00000209     0.00000134     0.00000160    -5.34879814
  nbf   7    -0.00000449    -1.17055949    -0.38142116     0.00000770     0.00000677    -0.00000268    -4.33708165
  nbf   8    -0.41281053     0.00000924     0.00002792     0.04011393     0.09685568    -0.00000430    -0.00000459    -3.58726451
  nbf   9     0.00000270     0.00000099     0.00000184     0.00000123    -0.00000001    -0.00000292     0.00000083    -0.00000226
  nbf  10     0.00000008     0.00000038     0.00000122     0.00000078     0.00000603     0.24664809     0.00000115    -0.00000061
  nbf  11     0.71269320     0.00000310     0.00000632    -0.06900652    -0.45100076     0.00000242     0.00000126     0.37442281
  nbf  12    -0.00002151    -0.02252053    -0.02724858     0.00000032    -0.00004108     0.00000091    -0.10307534    -0.00003844
  nbf  13     0.00001047     0.51219400     1.29207974     0.00000909     0.00000498     0.00000173     1.13404876     0.00000453
  nbf  14     0.60239948     0.00000705     0.00000306     1.39173986     0.61422958     0.00000223     0.00000714     0.29792144
  nbf  15     0.00000111     0.00000004    -0.00000011     0.00000163     0.00000189     0.06457667     0.00000117     0.00000073
  nbf  16     0.00000146     0.00000188     0.00000276     0.00000048     0.00000035    -0.00000219    -0.00000021    -0.00000074
  nbf  17     0.01600175    -0.00000626    -0.00001360     0.28432484    -0.03515408     0.00000102     0.00000123    -0.91933207
  nbf  18    -0.11633606    -0.00000593    -0.00000161    -0.29335364    -0.90105133     0.00000045    -0.00000097    -0.23508015
  nbf  19    -0.00000628     0.16666475    -0.06194073     0.00000042    -0.00000113    -0.00000007     1.19142092    -0.00000725
  nbf  20     0.18303597     0.00000004    -0.00000001     0.16560027    -0.11839551    -0.00000045     0.00000011     0.20495020
  nbf  21    -0.99286516    -0.00000486    -0.00001569    -0.72374334     0.04750086     0.00000170     0.00000385    -0.60128100
  nbf  22     0.00000761     1.23659607     0.65663483     0.00001007     0.00000954    -0.00000233     1.45007495     0.00001180
  nbf  23     0.54121181     0.00000752     0.00001332     0.73131399     1.71705710     0.00000016     0.00000308    -0.41984013
  nbf  24     0.00000197     0.00000136    -0.00000146    -0.00000166     0.00000012    -2.13412925     0.00000155     0.00000229

               nbf   9        nbf  10        nbf  11        nbf  12        nbf  13        nbf  14        nbf  15        nbf  16
  nbf   9    -3.42235660
  nbf  10     0.00001921    -3.17759296
  nbf  11    -0.00000296    -0.00000184    -4.03659624
  nbf  12    -0.00000415     0.00000165    -0.00000797    -2.63232715
  nbf  13    -0.00000188    -0.00000169     0.00000121     0.09057297    -3.62269916
  nbf  14    -0.00000169    -0.00000157    -0.18631690     0.00000094     0.00002092    -3.66407334
  nbf  15     0.00000606     0.90758551    -0.00000061     0.00000068     0.00000002     0.00000034    -3.67011610
  nbf  16     0.90243535     0.00000689    -0.00000033    -0.00000090    -0.00000062    -0.00000023     0.00000617    -3.63967547
  nbf  17     0.00000121     0.00000043    -0.42339687     0.00002300    -0.00000507     0.04524160    -0.00000008     0.00000162
  nbf  18     0.00000057    -0.00000150     0.56833138     0.00000971    -0.00000911    -0.46527200    -0.00000084     0.00000048
  nbf  19    -0.00000115    -0.00000059    -0.00000051    -0.08844198     0.44946676     0.00000824    -0.00000104    -0.00000004
  nbf  20    -0.00000001     0.00000003    -0.38281207     0.00000000     0.00000000    -0.26337941    -0.00000001     0.00000005
  nbf  21    -0.00000278    -0.00000166     1.08738629     0.00001342     0.00000236     0.59829283    -0.00000035     0.00000063
  nbf  22     0.00000379     0.00000214     0.00000586     0.01873530    -0.54199890     0.00000033     0.00000095    -0.00000147
  nbf  23     0.00000113     0.00000544     0.54854104    -0.00002605     0.00000189    -0.34233151    -0.00000279     0.00000119
  nbf  24     0.00000358     0.07844552    -0.00000212    -0.00000057    -0.00000115    -0.00000015     0.08091675     0.00000202

               nbf  17        nbf  18        nbf  19        nbf  20        nbf  21        nbf  22        nbf  23        nbf  24
  nbf  17    -3.38761017
  nbf  18    -0.05579875    -3.03514049
  nbf  19     0.00000606    -0.00000743    -2.89455285
  nbf  20     0.00966385     0.05948512    -0.00000025   -33.05201287
  nbf  21    -0.01753458    -0.18424932     0.00000142     0.58205892    -7.87298465
  nbf  22    -0.00000600     0.00000104    -0.45832865    -0.00000011    -0.00000925    -6.71597488
  nbf  23    -0.12236643     0.30566614    -0.00000425     0.18751851    -0.25155154     0.00001408    -6.95602161
  nbf  24    -0.00000088     0.00000140    -0.00000008    -0.00000041    -0.00000462     0.00000236    -0.00000112    -7.11714938
 insert_onel_diag: nmin2=   380

 onel.diag.
 -0.331816E+01-0.309074E+01-0.410179E+01-0.407795E+01-0.475238E+01-0.534880E+01-0.433708E+01-0.358726E+01-0.342236E+01-0.317759E+01
 -0.403660E+01-0.263233E+01-0.362270E+01-0.366407E+01-0.367012E+01-0.363968E+01-0.338761E+01-0.303514E+01-0.289455E+01-0.330520E+02
 -0.787298E+01-0.671597E+01-0.695602E+01-0.711715E+01

 i,start,strti,fin,filind,bufszi,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095         1       380       380

 4 external diag. modified integrals

  0.299711954 -3.31816074  0.362025844  0.213403987  0.294044049 -3.09074408
  0.345182252  0.318068021  0.34837146  0.293493931  0.497019376 -4.10179203
  0.382867967  0.309072536  0.341195502  0.316765031  0.62103433  0.362850338
  0.531836961 -4.07794671  0.349706612  0.299527054  0.308820027  0.298754537
  0.450260207  0.419223806  0.448476864  0.401601457  0.518535861 -4.75237624
  0.349531774  0.304524501  0.31278089  0.302655019  0.429312411  0.416826853
  0.436921903  0.413400849  0.530775438  0.48280831  0.60679891 -5.34879814
  0.33535564  0.293407704  0.341034277  0.276576801  0.455225842  0.389302194
  0.443351075  0.383054684  0.486303633  0.428587076  0.499742288  0.459023099
  0.475434104 -4.33708165  0.342526573  0.315362897  0.319917749  0.300731659
  0.504389096  0.410870033  0.47482656  0.425010382  0.519270202  0.400100848
  0.453053263  0.422413645  0.446074502  0.406547185  0.476405093 -3.58726451
  0.361060191  0.337659006  0.340604687  0.320198588  0.504515292  0.452245513
  0.527488651  0.469039301  0.434901697  0.420706511  0.454829088  0.417262113
  0.426620519  0.400804422  0.469386808  0.441599006  0.555764969 -3.4223566
  0.348856386  0.329501947  0.326880472  0.312634683  0.490451682  0.436629022
  0.509180653  0.444435002  0.4400708  0.419071921  0.459403052  0.417691971
  0.415311401  0.398901525  0.466192986  0.43088403  0.694750225  0.346946891
  0.501631969 -3.17759296  0.343227876  0.31280524  0.321132402  0.300850073
  0.455432102  0.418963752  0.451109274  0.419591702  0.534502331  0.438245352
  0.597905727  0.464004009  0.508095073  0.408470727  0.460615989  0.424227034
  0.469521607  0.441123033  0.462583019  0.438238584  0.525774157 -4.03659624
  0.356976431  0.333496982  0.334773679  0.315431831  0.522936179  0.430785245
  0.55704189  0.430105919  0.46546176  0.385379923  0.398185515  0.390667038
  0.424333725  0.380522726  0.592974188  0.354449455  0.516100041  0.471421051
  0.491982029  0.453400696  0.428233148  0.409923576  0.53092711 -2.63232715
  0.370351482  0.34231438  0.377526483  0.320334123  0.57663332  0.44254372
  0.563518308  0.477512966  0.511232086  0.469131823  0.508472502  0.490097443
  0.529535469  0.421869959  0.499545867  0.467575397  0.539123293  0.488956188
  0.512741012  0.475743005  0.515621907  0.477676678  0.512163042  0.474196449
  0.592144122 -3.62269916  0.388203856  0.340302294  0.365601016  0.328125926
  0.536368018  0.473948976  0.615526601  0.455378076  0.520967947  0.463416877
  0.546457699  0.489981129  0.48581785  0.455063687  0.509165453  0.46499119
  0.535052608  0.504064949  0.518029026  0.481875024  0.516561248  0.489811629
  0.517714869  0.482829445  0.717425907  0.445100321  0.607835731 -3.66407334
  0.361030445  0.336028491  0.330453775  0.322428351  0.486741558  0.469418774
  0.514189842  0.450314389  0.575178104  0.539226259  0.630438312  0.577562728
  0.510895782  0.499926663  0.503939385  0.490371469  0.52041881  0.467839603
  0.554043059  0.445274854  0.591832537  0.558697254  0.453957255  0.445032703
  0.568738744  0.548069735  0.596778585  0.560916765  0.750165186 -3.6701161
  0.353015389  0.338054232  0.358115278  0.317291888  0.522721553  0.462375435
  0.489534774  0.469144866  0.543456123  0.532600344  0.632201055  0.577477753
  0.547273656  0.513624874  0.507616587  0.483570727  0.564197636  0.455577875
  0.524876517  0.46718588  0.585160178  0.562784851  0.458775281  0.447244069
  0.587371194  0.556188564  0.58998874  0.568912261  0.721594827  0.645098881
  0.755127757 -3.63967547  0.366037972  0.335416332  0.343455146  0.320873443
  0.50940609  0.476326958  0.527676719  0.456821536  0.589439367  0.537772005
  0.579789349  0.559672288  0.53269644  0.49854365  0.567546289  0.469810852
  0.495610134  0.480892777  0.493939617  0.479062411  0.584343602  0.533457424
  0.502525388  0.442000941  0.582221799  0.560044775  0.596954899  0.56140811
  0.713528596  0.656785472  0.69947109  0.625487345  0.729807063 -3.38761017
  0.353987843  0.337616982  0.351789554  0.321182817  0.550334788  0.456688986
  0.522995786  0.46803517  0.566057852  0.507622395  0.615570104  0.560439732
  0.539018372  0.503813066  0.510463413  0.481771744  0.500103129  0.48786777
  0.496692123  0.477703001  0.585735173  0.540040646  0.469784387  0.453592071
  0.609978757  0.53850005  0.643846451  0.534124473  0.686162365  0.635949394
  0.700906311  0.666564985  0.679632027  0.617528828  0.696112735 -3.03514049
  0.358856446  0.335907395  0.354506012  0.319503646  0.548011068  0.469120392
  0.531509325  0.460743131  0.577501451  0.534350115  0.577624788  0.562928162
  0.587296462  0.4866605  0.514004213  0.4958057  0.493523583  0.479933652
  0.487740555  0.474497517  0.570979461  0.537487177  0.471960345  0.452931079
  0.634007692  0.54138526  0.615254278  0.550385293  0.691778305  0.63576614
  0.699509207  0.640665078  0.699234432  0.657364716  0.711993958  0.616407541
  0.721280492 -2.89455285
 i,start,strti,fin,filind,bufszi,nmbuf,nmin2, nd4ext = 
         1         1         2         2        12      4095         1       380       380

 all internall diag. modified integrals

  4.73971342 -33.0520129  1.04569563  0.0636788604  0.755179462 -7.87298465
  0.878477817  0.0193493095  0.682719812  0.155759544  0.668673763 -6.71597488
  0.986988872  0.0301374391  0.681192227  0.125736446  0.608457144
  0.0402117459  0.732528817 -6.95602161  1.03831833  0.0301199817  0.713926708
  0.128455235  0.624391224  0.0304166993  0.665957609  0.0433645062
  0.760228983 -7.11714938
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331816E+01-0.309074E+01-0.410179E+01-0.407795E+01-0.475238E+01-0.534880E+01-0.433708E+01-0.358726E+01-0.342236E+01-0.317759E+01
 -0.403660E+01-0.263233E+01-0.362270E+01-0.366407E+01-0.367012E+01-0.363968E+01-0.338761E+01-0.303514E+01-0.289455E+01-0.330520E+02
 -0.787298E+01-0.671597E+01-0.695602E+01-0.711715E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331816E+01-0.309074E+01-0.410179E+01-0.407795E+01-0.475238E+01-0.534880E+01-0.433708E+01-0.358726E+01-0.342236E+01-0.317759E+01
 -0.403660E+01-0.263233E+01-0.362270E+01-0.366407E+01-0.367012E+01-0.363968E+01-0.338761E+01-0.303514E+01-0.289455E+01-0.330520E+02
 -0.787298E+01-0.671597E+01-0.695602E+01-0.711715E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331816E+01-0.309074E+01-0.410179E+01-0.407795E+01-0.475238E+01-0.534880E+01-0.433708E+01-0.358726E+01-0.342236E+01-0.317759E+01
 -0.403660E+01-0.263233E+01-0.362270E+01-0.366407E+01-0.367012E+01-0.363968E+01-0.338761E+01-0.303514E+01-0.289455E+01-0.330520E+02
 -0.787298E+01-0.671597E+01-0.695602E+01-0.711715E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380
 DIAGEL COUNTS: DG0X,DG2X,DG4X= 440 145 30
 =========== Executing IN-CORE method ==========
 norm multnew:  1.
 threx: total number of loops= 14
 threx: total number of loops= 24
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331816E+01-0.309074E+01-0.410179E+01-0.407795E+01-0.475238E+01-0.534880E+01-0.433708E+01-0.358726E+01-0.342236E+01-0.317759E+01
 -0.403660E+01-0.263233E+01-0.362270E+01-0.366407E+01-0.367012E+01-0.363968E+01-0.338761E+01-0.303514E+01-0.289455E+01-0.330520E+02
 -0.787298E+01-0.671597E+01-0.695602E+01-0.711715E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331816E+01-0.309074E+01-0.410179E+01-0.407795E+01-0.475238E+01-0.534880E+01-0.433708E+01-0.358726E+01-0.342236E+01-0.317759E+01
 -0.403660E+01-0.263233E+01-0.362270E+01-0.366407E+01-0.367012E+01-0.363968E+01-0.338761E+01-0.303514E+01-0.289455E+01-0.330520E+02
 -0.787298E+01-0.671597E+01-0.695602E+01-0.711715E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331816E+01-0.309074E+01-0.410179E+01-0.407795E+01-0.475238E+01-0.534880E+01-0.433708E+01-0.358726E+01-0.342236E+01-0.317759E+01
 -0.403660E+01-0.263233E+01-0.362270E+01-0.366407E+01-0.367012E+01-0.363968E+01-0.338761E+01-0.303514E+01-0.289455E+01-0.330520E+02
 -0.787298E+01-0.671597E+01-0.695602E+01-0.711715E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        40 xx:       135 ww:       255
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        20 yw:        25

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        39
task #   9:       127    task #  10:       247    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 matrix-vector product formed for vector   1
 =========== Executing IN-CORE method ==========
 norm multnew:  0.000841908461
 threx: total number of loops= 14
 threx: total number of loops= 24
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331816E+01-0.309074E+01-0.410179E+01-0.407795E+01-0.475238E+01-0.534880E+01-0.433708E+01-0.358726E+01-0.342236E+01-0.317759E+01
 -0.403660E+01-0.263233E+01-0.362270E+01-0.366407E+01-0.367012E+01-0.363968E+01-0.338761E+01-0.303514E+01-0.289455E+01-0.330520E+02
 -0.787298E+01-0.671597E+01-0.695602E+01-0.711715E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331816E+01-0.309074E+01-0.410179E+01-0.407795E+01-0.475238E+01-0.534880E+01-0.433708E+01-0.358726E+01-0.342236E+01-0.317759E+01
 -0.403660E+01-0.263233E+01-0.362270E+01-0.366407E+01-0.367012E+01-0.363968E+01-0.338761E+01-0.303514E+01-0.289455E+01-0.330520E+02
 -0.787298E+01-0.671597E+01-0.695602E+01-0.711715E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331816E+01-0.309074E+01-0.410179E+01-0.407795E+01-0.475238E+01-0.534880E+01-0.433708E+01-0.358726E+01-0.342236E+01-0.317759E+01
 -0.403660E+01-0.263233E+01-0.362270E+01-0.366407E+01-0.367012E+01-0.363968E+01-0.338761E+01-0.303514E+01-0.289455E+01-0.330520E+02
 -0.787298E+01-0.671597E+01-0.695602E+01-0.711715E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        40 xx:       135 ww:       255
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        20 yw:        25

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        39
task #   9:       127    task #  10:       247    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 matrix-vector product formed for vector   2
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference-subspace vector overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97731793    -0.02531908

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97547070    -0.17309724
 subspace has dimension  2

          (h-repnuc*1) matrix in the subspace basis

                x:   1         x:   2
   x:   1   -85.60604387
   x:   2     1.96493956    -0.07117396

          overlap matrix in the subspace basis

                x:   1         x:   2
   x:   1     1.00000000
   x:   2    -0.02295962     0.00084191

          eigenvectors and eigenvalues in the subspace basis

                v:   1         v:   2

   energy   -85.60638059   -82.83033412

   x:   1     1.01419194     1.28301988
   x:   2     0.62076759    56.36127567

          eigenvectors and eigenvalues in the schmidt orthonormalized basis

                v:   1         v:   2

   energy   -85.60638059   -82.83033412

  zx:   1     0.99993935    -0.01101341
  zx:   2     0.01101341     0.99993935

          <ref|baseci> overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97731793    -0.02531908

 trial vector basis is being transformed.  new dimension:   1

          transformed tciref transformation matrix  block   1

               ref   1
  ci:   1     0.97547070

  write the transformed vectors back to cosmofile

 rtflw =  0
 freezing =  F

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1    -76.2533746201 -9.3555E+00  7.9397E-05  1.6304E-02  1.0000E-03
 mr-sdci #  4  2    -73.4773281484 -8.8375E+00  0.0000E+00  1.6272E+00  1.0000E-04


 total energy in cosmo calc 
e(rootcalc) + repnuc=     -76.2533746201
dielectric energy =      -0.0114547213
deltaediel =       0.0114547213
e(rootcalc) + repnuc - ediel =     -76.2419198988
e(rootcalc) + repnuc - edielnew =     -76.2533746201
deltaelast =      76.2419198988

 root number  1 is used to define the new expansion vector.
 diagon: frcsub=2, new matrix-vector products will be computed.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.03   0.00   0.00   0.02         0.    0.0000
    2   25    0   0.03   0.00   0.00   0.02         0.    0.0000
    3   26    0   0.04   0.00   0.00   0.03         0.    0.0000
    4   11    0   0.03   0.00   0.00   0.02         0.    0.0000
    5   15    0   0.03   0.00   0.00   0.03         0.    0.0000
    6   16    0   0.04   0.00   0.00   0.03         0.    0.0000
    7    1    0   0.01   0.00   0.00   0.01         0.    0.0000
    8    5    0   0.02   0.00   0.00   0.02         0.    0.0000
    9    6    0   0.04   0.00   0.00   0.03         0.    0.0000
   10    7    0   0.03   0.00   0.00   0.03         0.    0.0000
   11   75    0   0.01   0.00   0.00   0.01         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.01   0.00   0.00   0.01         0.    0.0000
   14   47    0   0.03   0.00   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.009999
time for cinew                         0.030000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3500s 
time spent in multnx:                   0.2800s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0200s 
total time per CI iteration:            4.0100s 

          starting ci iteration   5

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.35300597

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=      10  D0X=      30  D0W=      50
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================

          ci-one-electron density block   1

                mo   1         mo   2         mo   3         mo   4         mo   5         mo   6         mo   7         mo   8
   mo   1     1.99991239
   mo   2    -0.00020629     1.98526772
   mo   3     0.00000000     0.00000004     1.96968856
   mo   4    -0.00016185     0.00397469    -0.00000017     1.97310418
   mo   5     0.00000000    -0.00000005     0.00000000    -0.00000008     1.97583332
   mo   6     0.00013460    -0.00252173    -0.00001773    -0.02045677    -0.00000768     0.00674806
   mo   7     0.00000000     0.00000707    -0.01045781    -0.00002085    -0.00000466     0.00000001     0.00741799
   mo   8    -0.00000001     0.00001490    -0.01346605    -0.00001982     0.00000237    -0.00000010     0.00705154     0.01056003
   mo   9     0.00006791     0.00386000    -0.00001112    -0.00109349     0.00000197     0.00690952    -0.00000016    -0.00000024
   mo  10    -0.00021649     0.00982326    -0.00001335     0.00383812     0.00000014     0.00265363    -0.00000015    -0.00000009
   mo  11     0.00000004    -0.00000170     0.00000296    -0.00000043    -0.01162658     0.00000019     0.00000008    -0.00000009
   mo  12    -0.00000001    -0.00000289    -0.00489920    -0.00000560    -0.00000240     0.00000006     0.00608150     0.00396622
   mo  13     0.00019911    -0.00089006    -0.00001210    -0.00840979    -0.00000295     0.00189728    -0.00000001    -0.00000012
   mo  14     0.00000000     0.00000210    -0.00000314    -0.00000113    -0.00000151    -0.00000002    -0.00000002    -0.00000001
   mo  15     0.00000000     0.00000106    -0.00000187    -0.00000478    -0.00379591     0.00000003    -0.00000001    -0.00000001
   mo  16    -0.00037414     0.00002464    -0.00000460     0.00247575     0.00000168    -0.00188643    -0.00000009    -0.00000003
   mo  17    -0.00000001    -0.00000855     0.00090945     0.00002067     0.00000048    -0.00000005     0.00025999     0.00023069
   mo  18     0.00000003    -0.00000062     0.01093646    -0.00000063     0.00000088    -0.00000010    -0.00284306    -0.00440737
   mo  19    -0.00010099    -0.00410698    -0.00000013     0.00725005     0.00000039    -0.00306353    -0.00000013    -0.00000008
   mo  20     0.00000000     0.00000020    -0.00000049     0.00000138     0.00212525    -0.00000003     0.00000000     0.00000000
   mo  21     0.00000000    -0.00000024     0.00000060    -0.00000060    -0.00000130     0.00000001    -0.00000001    -0.00000002
   mo  22     0.00001004     0.00013357     0.00000249    -0.00131279     0.00000030    -0.00004484     0.00000003     0.00000003
   mo  23     0.00004500     0.00163440    -0.00000046     0.00001876    -0.00000049     0.00061555     0.00000000    -0.00000001
   mo  24    -0.00000001    -0.00000066    -0.00019168     0.00000182     0.00000008    -0.00000001    -0.00081069     0.00040157

                mo   9         mo  10         mo  11         mo  12         mo  13         mo  14         mo  15         mo  16
   mo   9     0.00996114
   mo  10     0.00328245     0.01026106
   mo  11    -0.00000006    -0.00000002     0.01472532
   mo  12     0.00000003     0.00000003     0.00000002     0.00656967
   mo  13     0.00046046    -0.00063716     0.00000006    -0.00000001     0.00380320
   mo  14    -0.00000002     0.00000000     0.00000007    -0.00000002     0.00000000     0.00305743
   mo  15    -0.00000003    -0.00000006    -0.00140050    -0.00000001     0.00000003    -0.00000005     0.00230260
   mo  16     0.00008402     0.00266590    -0.00000003    -0.00000006    -0.00123081     0.00000001    -0.00000002     0.00444544
   mo  17     0.00000015     0.00000025    -0.00000001     0.00035405    -0.00000004     0.00000000     0.00000000     0.00000006
   mo  18    -0.00000013    -0.00000015     0.00000001    -0.00181887     0.00000001     0.00000000    -0.00000001    -0.00000003
   mo  19    -0.00435845    -0.00081208     0.00000000    -0.00000011    -0.00083036     0.00000000    -0.00000001    -0.00009134
   mo  20    -0.00000001     0.00000000     0.00006298     0.00000000    -0.00000001     0.00000000    -0.00188025     0.00000000
   mo  21     0.00000000    -0.00000001     0.00000002     0.00000000     0.00000000    -0.00208295     0.00000000    -0.00000001
   mo  22    -0.00055927     0.00032255    -0.00000001     0.00000002     0.00177242     0.00000000     0.00000000     0.00072740
   mo  23     0.00019380     0.00181376     0.00000000     0.00000003     0.00024603     0.00000000     0.00000000    -0.00069679
   mo  24     0.00000001     0.00000000     0.00000001    -0.00170257     0.00000001     0.00000000     0.00000000     0.00000002

                mo  17         mo  18         mo  19         mo  20         mo  21         mo  22         mo  23         mo  24
   mo  17     0.00052640
   mo  18    -0.00020194     0.00264297
   mo  19     0.00000002     0.00000005     0.00290728
   mo  20     0.00000000     0.00000000     0.00000001     0.00262078
   mo  21     0.00000000     0.00000001     0.00000000     0.00000001     0.00248639
   mo  22    -0.00000001     0.00000001    -0.00017223     0.00000000     0.00000000     0.00202171
   mo  23     0.00000003    -0.00000002     0.00064089     0.00000000     0.00000000    -0.00009091     0.00166763
   mo  24     0.00000551    -0.00059594     0.00000002     0.00000000     0.00000000     0.00000000    -0.00000001     0.00146874


*****   symmetry block SYM1   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       1.99991239
       2      -0.00020629     1.98526772
       3       0.00000000     0.00000004     1.96968856
       4      -0.00016185     0.00397469    -0.00000017     1.97310418
       5       0.00000000    -0.00000005     0.00000000    -0.00000008
       6       0.00013460    -0.00252173    -0.00001773    -0.02045677
       7       0.00000000     0.00000707    -0.01045781    -0.00002085
       8      -0.00000001     0.00001490    -0.01346605    -0.00001982
       9       0.00006791     0.00386000    -0.00001112    -0.00109349
      10      -0.00021649     0.00982326    -0.00001335     0.00383812
      11       0.00000004    -0.00000170     0.00000296    -0.00000043
      12      -0.00000001    -0.00000289    -0.00489920    -0.00000560
      13       0.00019911    -0.00089006    -0.00001210    -0.00840979
      14       0.00000000     0.00000210    -0.00000314    -0.00000113
      15       0.00000000     0.00000106    -0.00000187    -0.00000478
      16      -0.00037414     0.00002464    -0.00000460     0.00247575
      17      -0.00000001    -0.00000855     0.00090945     0.00002067
      18       0.00000003    -0.00000062     0.01093646    -0.00000063
      19      -0.00010099    -0.00410698    -0.00000013     0.00725005
      20       0.00000000     0.00000020    -0.00000049     0.00000138
      21       0.00000000    -0.00000024     0.00000060    -0.00000060
      22       0.00001004     0.00013357     0.00000249    -0.00131279
      23       0.00004500     0.00163440    -0.00000046     0.00001876
      24      -0.00000001    -0.00000066    -0.00019168     0.00000182

               Column   5     Column   6     Column   7     Column   8
       5       1.97583332
       6      -0.00000768     0.00674806
       7      -0.00000466     0.00000001     0.00741799
       8       0.00000237    -0.00000010     0.00705154     0.01056003
       9       0.00000197     0.00690952    -0.00000016    -0.00000024
      10       0.00000014     0.00265363    -0.00000015    -0.00000009
      11      -0.01162658     0.00000019     0.00000008    -0.00000009
      12      -0.00000240     0.00000006     0.00608150     0.00396622
      13      -0.00000295     0.00189728    -0.00000001    -0.00000012
      14      -0.00000151    -0.00000002    -0.00000002    -0.00000001
      15      -0.00379591     0.00000003    -0.00000001    -0.00000001
      16       0.00000168    -0.00188643    -0.00000009    -0.00000003
      17       0.00000048    -0.00000005     0.00025999     0.00023069
      18       0.00000088    -0.00000010    -0.00284306    -0.00440737
      19       0.00000039    -0.00306353    -0.00000013    -0.00000008
      20       0.00212525    -0.00000003     0.00000000     0.00000000
      21      -0.00000130     0.00000001    -0.00000001    -0.00000002
      22       0.00000030    -0.00004484     0.00000003     0.00000003
      23      -0.00000049     0.00061555     0.00000000    -0.00000001
      24       0.00000008    -0.00000001    -0.00081069     0.00040157

               Column   9     Column  10     Column  11     Column  12
       9       0.00996114
      10       0.00328245     0.01026106
      11      -0.00000006    -0.00000002     0.01472532
      12       0.00000003     0.00000003     0.00000002     0.00656967
      13       0.00046046    -0.00063716     0.00000006    -0.00000001
      14      -0.00000002     0.00000000     0.00000007    -0.00000002
      15      -0.00000003    -0.00000006    -0.00140050    -0.00000001
      16       0.00008402     0.00266590    -0.00000003    -0.00000006
      17       0.00000015     0.00000025    -0.00000001     0.00035405
      18      -0.00000013    -0.00000015     0.00000001    -0.00181887
      19      -0.00435845    -0.00081208     0.00000000    -0.00000011
      20      -0.00000001     0.00000000     0.00006298     0.00000000
      21       0.00000000    -0.00000001     0.00000002     0.00000000
      22      -0.00055927     0.00032255    -0.00000001     0.00000002
      23       0.00019380     0.00181376     0.00000000     0.00000003
      24       0.00000001     0.00000000     0.00000001    -0.00170257

               Column  13     Column  14     Column  15     Column  16
      13       0.00380320
      14       0.00000000     0.00305743
      15       0.00000003    -0.00000005     0.00230260
      16      -0.00123081     0.00000001    -0.00000002     0.00444544
      17      -0.00000004     0.00000000     0.00000000     0.00000006
      18       0.00000001     0.00000000    -0.00000001    -0.00000003
      19      -0.00083036     0.00000000    -0.00000001    -0.00009134
      20      -0.00000001     0.00000000    -0.00188025     0.00000000
      21       0.00000000    -0.00208295     0.00000000    -0.00000001
      22       0.00177242     0.00000000     0.00000000     0.00072740
      23       0.00024603     0.00000000     0.00000000    -0.00069679
      24       0.00000001     0.00000000     0.00000000     0.00000002

               Column  17     Column  18     Column  19     Column  20
      17       0.00052640
      18      -0.00020194     0.00264297
      19       0.00000002     0.00000005     0.00290728
      20       0.00000000     0.00000000     0.00000001     0.00262078
      21       0.00000000     0.00000001     0.00000000     0.00000001
      22      -0.00000001     0.00000001    -0.00017223     0.00000000
      23       0.00000003    -0.00000002     0.00064089     0.00000000
      24       0.00000551    -0.00059594     0.00000002     0.00000000

               Column  21     Column  22     Column  23     Column  24
      21       0.00248639
      22       0.00000000     0.00202171
      23       0.00000000    -0.00009091     0.00166763
      24       0.00000000     0.00000000    -0.00000001     0.00146874

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7        eno   8
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    8   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno   9        eno  10        eno  11        eno  12        eno  13        eno  14        eno  15        eno  16
 emo    9   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   10   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   11   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   12   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   13   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   14   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   15   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   16   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno  17        eno  18        eno  19        eno  20        eno  21        eno  22        eno  23        eno  24
 emo   17   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   18   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   19   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   20   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   21   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   22   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   23   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   24   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   1.9999174      1.9865520      1.9759118      1.9721728      1.9699117
   0.0212846      0.0194435      0.0148250      0.0103597      0.0055751
   0.0052977      0.0048743      0.0043363      0.0042431      0.0010296
   0.0010269      0.0006695      0.0005822      0.0005034      0.0005022
   0.0004585      0.0004387      0.0000488      0.0000354

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7         no   8
  mo    1   0.999824E+00   0.184397E-01  -0.578700E-07   0.353027E-02  -0.553840E-06   0.243448E-07  -0.351483E-04  -0.218103E-07
  mo    2  -0.166133E-01   0.956543E+00  -0.894555E-06  -0.291055E+00   0.474679E-04  -0.524413E-05  -0.339357E-02   0.100752E-05
  mo    3   0.915311E-07  -0.373070E-05   0.150169E-06   0.150458E-03   0.999943E+00   0.100762E-01   0.150929E-04  -0.164213E-05
  mo    4  -0.874130E-02   0.290914E+00   0.938145E-05   0.956633E+00  -0.143100E-03   0.113943E-04   0.647878E-02  -0.715878E-08
  mo    5   0.126374E-06  -0.189979E-05   0.999980E+00  -0.928523E-05  -0.151622E-06   0.125779E-05   0.160563E-05   0.563550E-02
  mo    6   0.178403E-03  -0.421028E-02  -0.399751E-05  -0.959827E-02  -0.762998E-05  -0.170348E-03   0.527038E+00   0.444986E-05
  mo    7   0.303269E-07   0.379896E-06  -0.237016E-05  -0.120543E-04  -0.536919E-02   0.568495E+00   0.171299E-03   0.357361E-05
  mo    8  -0.435071E-07   0.431987E-05   0.119058E-05  -0.129590E-04  -0.690929E-02   0.635938E+00   0.182041E-03  -0.702887E-05
  mo    9   0.710466E-05   0.170356E-02   0.979226E-06  -0.114859E-02  -0.553619E-05  -0.203963E-03   0.668575E+00  -0.116348E-04
  mo   10  -0.207708E-03   0.531683E-02   0.837443E-07   0.399764E-03  -0.687378E-05  -0.129831E-03   0.424099E+00  -0.105684E-04
  mo   11   0.348951E-07  -0.877023E-06  -0.592682E-02   0.965419E-07   0.151427E-05   0.217084E-05   0.101811E-04   0.992900E+00
  mo   12   0.419814E-07  -0.219926E-05  -0.122490E-05  -0.273597E-05  -0.253079E-02   0.442032E+00   0.152963E-03   0.107286E-05
  mo   13   0.144355E-03  -0.166666E-02  -0.154191E-05  -0.396799E-02  -0.556488E-05  -0.399612E-04   0.784896E-01   0.457472E-05
  mo   14  -0.128125E-07   0.846686E-06  -0.763325E-06  -0.859794E-06  -0.159787E-05  -0.314397E-05  -0.256129E-05   0.586738E-05
  mo   15   0.110075E-07  -0.185628E-06  -0.192012E-02  -0.246168E-05  -0.951675E-06  -0.183844E-05  -0.441036E-05  -0.116362E+00
  mo   16  -0.199043E-03   0.383760E-03   0.868918E-06   0.121112E-02  -0.251909E-05  -0.548512E-05   0.628681E-02  -0.585730E-05
  mo   17  -0.249613E-07  -0.108919E-05   0.244525E-06   0.113584E-04   0.459218E-03   0.248159E-01   0.268293E-04  -0.566745E-06
  mo   18   0.257917E-07  -0.422137E-06   0.450606E-06   0.668132E-06   0.558445E-02  -0.273255E+00  -0.963553E-04   0.143227E-05
  mo   19  -0.483854E-04  -0.916591E-03   0.237289E-06   0.414767E-02  -0.663301E-06   0.792806E-04  -0.292649E+00   0.329886E-05
  mo   20  -0.662605E-08   0.297778E-06   0.107863E-02   0.633376E-06  -0.245860E-06  -0.466933E-07  -0.213429E-06   0.240331E-01
  mo   21   0.565938E-08  -0.203664E-06  -0.657480E-06  -0.255133E-06   0.306665E-06  -0.323110E-06   0.117322E-06   0.289246E-06
  mo   22   0.967615E-05  -0.128797E-03   0.142530E-06  -0.660023E-03   0.136039E-05   0.256038E-05  -0.464753E-02  -0.101893E-06
  mo   23   0.878197E-05   0.793897E-03  -0.248216E-06  -0.234509E-03  -0.207591E-06  -0.169995E-04   0.588198E-01  -0.108164E-05
  mo   24  -0.653729E-08  -0.455015E-07   0.437153E-07   0.972638E-06  -0.960707E-04  -0.402226E-01  -0.145806E-04  -0.191071E-06

                no   9         no  10         no  11         no  12         no  13         no  14         no  15         no  16
  mo    1   0.223877E-03  -0.265661E-07  -0.965532E-04  -0.991977E-09   0.434422E-04   0.320632E-09   0.170313E-04   0.195455E-07
  mo    2  -0.360688E-02   0.425024E-05  -0.203636E-03  -0.874821E-06   0.310376E-03   0.218050E-06   0.795170E-03  -0.318935E-05
  mo    3   0.129609E-05  -0.259909E-02   0.516774E-05   0.139956E-05   0.147424E-05  -0.309857E-06   0.266070E-05  -0.111042E-02
  mo    4  -0.653513E-02  -0.278887E-06   0.383627E-02   0.340955E-06   0.152697E-02  -0.209986E-05  -0.219381E-02   0.239994E-05
  mo    5  -0.161485E-05   0.181513E-05   0.227293E-05   0.464147E-07  -0.395539E-06  -0.262693E-02  -0.965560E-06  -0.914505E-06
  mo    6  -0.259926E+00   0.352016E-04   0.192566E+00   0.238997E-05  -0.128210E+00  -0.323044E-05  -0.131813E+00  -0.284919E-03
  mo    7  -0.954781E-05   0.220245E+00  -0.532982E-04   0.282622E-05   0.460779E-05  -0.512389E-06  -0.776603E-04   0.411913E-01
  mo    8   0.550429E-05  -0.546832E+00   0.146206E-03  -0.379261E-05   0.388779E-05  -0.956618E-06  -0.863672E-03   0.401937E+00
  mo    9  -0.191945E+00  -0.105757E-03  -0.361570E+00  -0.246440E-05   0.467115E-01   0.738019E-06   0.402676E+00   0.869388E-03
  mo   10   0.762864E+00   0.819132E-04   0.307624E+00   0.135299E-05  -0.145609E+00  -0.609126E-05  -0.281790E+00  -0.587360E-03
  mo   11   0.980893E-05  -0.563396E-05  -0.701228E-05  -0.702218E-05   0.326202E-05  -0.947806E-01   0.294421E-05   0.181266E-05
  mo   12  -0.618776E-05   0.640212E+00  -0.157520E-03   0.760714E-05  -0.148447E-05   0.437573E-06   0.610750E-03  -0.286049E+00
  mo   13  -0.249899E+00   0.160162E-03   0.613349E+00   0.169396E-04   0.443434E+00   0.885622E-05   0.588599E-01   0.104190E-03
  mo   14   0.290843E-05  -0.752778E-05  -0.119705E-04   0.753593E+00  -0.829858E-05  -0.143576E-04   0.148089E-06  -0.496114E-05
  mo   15  -0.555124E-05   0.150394E-05   0.417429E-06  -0.205691E-04   0.123616E-04  -0.653832E+00  -0.322865E-06  -0.859699E-06
  mo   16   0.461989E+00  -0.874011E-04  -0.367571E+00  -0.259623E-06   0.563938E+00   0.774080E-05   0.280460E+00   0.578341E-03
  mo   17   0.114577E-04   0.190551E-01   0.919129E-05   0.197685E-05   0.139097E-05   0.386706E-06   0.516613E-03  -0.252921E+00
  mo   18  -0.558407E-05   0.282214E+00  -0.827379E-04   0.238066E-05  -0.183086E-05   0.114225E-05  -0.132722E-02   0.617314E+00
  mo   19   0.162042E+00   0.482454E-04   0.179014E+00  -0.156902E-05  -0.275728E+00  -0.606795E-05   0.429540E+00   0.878964E-03
  mo   20   0.176640E-05  -0.146949E-05  -0.775397E-07   0.144367E-04  -0.136122E-04   0.750675E+00   0.141565E-05  -0.160851E-05
  mo   21  -0.168379E-05   0.682279E-05   0.102078E-04  -0.657341E+00   0.908170E-05   0.214987E-04   0.148839E-05  -0.571287E-05
  mo   22   0.274628E-01   0.822146E-04   0.320254E+00   0.105012E-04   0.519736E+00   0.971880E-05   0.135314E+00   0.335329E-03
  mo   23   0.103360E+00   0.789058E-04   0.302695E+00   0.131130E-05  -0.315222E+00  -0.716747E-05   0.675370E+00   0.147322E-02
  mo   24   0.252539E-05  -0.403214E+00   0.108386E-03  -0.466572E-05   0.583263E-05  -0.132941E-05   0.119345E-02  -0.556663E+00

                no  17         no  18         no  19         no  20         no  21         no  22         no  23         no  24
  mo    1  -0.131784E-08  -0.183915E-07   0.345503E-04   0.169829E-07   0.243122E-07  -0.319129E-04   0.231286E-04   0.435699E-08
  mo    2  -0.637023E-06  -0.300941E-05   0.212941E-02   0.682156E-06   0.425149E-05  -0.173251E-02   0.158398E-02  -0.625283E-07
  mo    3   0.841826E-06  -0.520063E-03   0.124689E-05   0.804850E-06  -0.144056E-02  -0.694458E-05   0.849844E-06   0.158534E-02
  mo    4   0.598348E-06   0.680515E-05   0.159921E-02   0.221672E-05  -0.567887E-05  -0.190817E-02   0.581462E-02   0.974629E-06
  mo    5   0.865640E-06   0.125964E-05   0.377988E-06   0.114797E-02  -0.131840E-06  -0.123699E-05   0.220912E-05   0.113844E-05
  mo    6  -0.498522E-05  -0.124480E-03   0.458911E+00   0.241530E-03   0.232359E-03  -0.229575E+00   0.566005E+00   0.623033E-04
  mo    7  -0.149970E-04   0.560739E+00   0.777616E-04  -0.573717E-05   0.194377E+00   0.155332E-04  -0.558155E-04   0.523803E+00
  mo    8   0.108446E-04  -0.187115E+00  -0.524772E-04   0.141184E-05   0.536578E-01   0.182489E-04   0.315419E-04  -0.311552E+00
  mo    9   0.118160E-05   0.393416E-05  -0.523485E-02  -0.335980E-05  -0.323371E-05   0.233898E-01  -0.469598E+00  -0.552284E-04
  mo   10   0.172563E-05   0.352641E-04  -0.534296E-01  -0.252616E-04  -0.363870E-04   0.115084E-01  -0.199806E+00  -0.193420E-04
  mo   11  -0.112929E-04  -0.340954E-05  -0.406713E-04   0.716314E-01   0.103596E-05   0.298897E-05  -0.593708E-05  -0.295601E-05
  mo   12   0.309354E-05  -0.143908E+00   0.650467E-05   0.308579E-05  -0.125976E+00  -0.313371E-04   0.592883E-04  -0.525667E+00
  mo   13   0.575623E-05   0.175516E-04  -0.347832E+00  -0.185225E-03   0.155397E-03  -0.448201E+00  -0.182222E+00  -0.262468E-04
  mo   14   0.657341E+00   0.207869E-04   0.983956E-05   0.771241E-04  -0.403530E-05   0.205557E-05   0.137327E-05   0.949449E-06
  mo   15  -0.773756E-04   0.471399E-05  -0.390499E-03   0.747636E+00   0.128878E-04  -0.380995E-05  -0.270890E-05  -0.168514E-07
  mo   16  -0.127532E-05  -0.506683E-04   0.991888E-02   0.522812E-05   0.185112E-03  -0.328251E+00   0.383256E+00   0.374587E-04
  mo   17   0.138615E-04  -0.392187E+00  -0.312472E-03  -0.128776E-04   0.880423E+00   0.554135E-03  -0.123068E-05   0.780768E-01
  mo   18  -0.511522E-05   0.377707E+00  -0.674923E-05  -0.831784E-05   0.384203E+00   0.125186E-03   0.443968E-04  -0.417495E+00
  mo   19  -0.686825E-05  -0.190206E-03   0.564920E+00   0.291114E-03   0.389582E-03  -0.426489E+00  -0.307369E+00  -0.372980E-04
  mo   20  -0.778263E-04   0.455723E-05  -0.344602E-03   0.660233E+00   0.108469E-04  -0.410179E-05  -0.160965E-05  -0.255252E-06
  mo   21   0.753593E+00   0.217453E-04   0.931294E-05   0.787565E-04  -0.254364E-05   0.149301E-05   0.479232E-06   0.723092E-06
  mo   22  -0.863967E-05  -0.231628E-04   0.468184E+00   0.247386E-03  -0.223435E-03   0.619662E+00  -0.710481E-01  -0.136053E-05
  mo   23   0.339246E-05   0.110505E-03  -0.356503E+00  -0.183468E-03  -0.229195E-03   0.268517E+00   0.373556E+00   0.372848E-04
  mo   24  -0.204380E-04   0.577388E+00   0.880553E-04  -0.720639E-05   0.143907E+00   0.449598E-04   0.502682E-04  -0.414535E+00
  eigenvectors of nos in ao-basis, column    1
  0.1000599E+01 -0.1359437E-01 -0.1680333E-02  0.5276191E-07  0.1645195E-06 -0.7454434E-02  0.9060972E-07 -0.5411516E-07
  0.1758842E-03  0.7720170E-08 -0.1115376E-07  0.4241540E-04  0.1432377E-07  0.9043954E-04 -0.3183437E-02  0.2113058E-02
 -0.1005070E-02  0.3334210E-08 -0.9305198E-03 -0.3183565E-02  0.2113196E-02  0.1005089E-02  0.1660476E-07 -0.9305719E-03
  eigenvectors of nos in ao-basis, column    2
  0.9254926E-02  0.9130893E+00 -0.7381074E-01 -0.2754547E-05 -0.2967242E-05  0.1125616E+00 -0.4931769E-05  0.1355508E-05
  0.7709416E-01 -0.3275355E-06  0.3796668E-06 -0.1128446E-02 -0.2406039E-06  0.1096424E-02  0.2229219E+00 -0.1090572E+00
  0.2772165E-01  0.4166428E-06  0.2164428E-01  0.2229337E+00 -0.1090645E+00 -0.2772388E-01 -0.5901385E-06  0.2164252E-01
  eigenvectors of nos in ao-basis, column    3
 -0.7779386E-06 -0.2713656E-05  0.2781160E-05  0.2504821E-05  0.9106040E+00  0.8074639E-05 -0.2388894E-05  0.8283113E-01
  0.1750986E-05 -0.6065978E-06 -0.1683893E-01 -0.8287278E-07 -0.1316254E-06 -0.2532308E-06 -0.6562258E-05  0.9563825E-05
  0.8248522E-06  0.3046675E-01 -0.3175956E-06 -0.1441247E-05  0.2950996E-06 -0.3428090E-06  0.3046829E-01 -0.5272838E-06
  eigenvectors of nos in ao-basis, column    4
  0.1557478E-02  0.1372772E-01  0.2146540E+00  0.1222660E-03 -0.8226321E-05  0.8036874E+00 -0.2342375E-04  0.4284116E-06
 -0.1816348E-01 -0.1627333E-06  0.1225286E-05 -0.4711506E-02 -0.4310302E-05 -0.2455750E-02 -0.4249189E+00  0.1809533E+00
 -0.3776076E-01 -0.2619993E-05  0.2521489E-02 -0.4247774E+00  0.1808960E+00  0.3776366E-01 -0.1245510E-05  0.2550003E-02
  eigenvectors of nos in ao-basis, column    5
  0.3780529E-06 -0.1437316E-06 -0.3882643E-04  0.7302807E+00  0.2057002E-05 -0.1085144E-03 -0.1170556E+00 -0.1917390E-05
 -0.9173270E-06  0.5348217E-06 -0.1088459E-06  0.5822165E-06 -0.2550962E-01  0.6852281E-06 -0.5512732E+00  0.1926531E+00
 -0.2051012E-01 -0.1809842E-05 -0.3136564E-01  0.5514226E+00 -0.1927116E+00 -0.2052406E-01  0.1493833E-06  0.3135376E-01
  eigenvectors of nos in ao-basis, column    6
 -0.2974040E-04 -0.2406871E-03  0.3592364E-04 -0.1308923E+01  0.4338251E-05  0.3769113E-03  0.5870902E+00 -0.2459101E-05
 -0.2195095E-03  0.5884358E-07  0.2124942E-06  0.1530724E-05 -0.6811463E-01  0.9375863E-05 -0.1000653E+01  0.3581908E+00
 -0.4064869E-01 -0.3665484E-05 -0.1781608E-01  0.1001237E+01 -0.3584318E+00 -0.4067615E-01  0.8957020E-06  0.1779126E-01
  eigenvectors of nos in ao-basis, column    7
  0.1066591E+00  0.8105805E+00 -0.1289669E+00 -0.4219741E-03  0.1611967E-04 -0.1228890E+01  0.2091170E-03 -0.1435022E-04
  0.7832675E+00  0.4559925E-06  0.4077741E-06 -0.4845737E-02 -0.2009995E-04 -0.3205070E-01 -0.9638111E+00  0.4105841E+00
 -0.5126392E-01 -0.5093728E-05 -0.9263887E-02 -0.9632004E+00  0.4103601E+00  0.5124461E-01 -0.1667954E-05 -0.9211780E-02
  eigenvectors of nos in ao-basis, column    8
  0.3302006E-05  0.1623663E-04 -0.1795320E-04  0.6072272E-06  0.1412683E+01  0.2160775E-04  0.2820585E-05 -0.1611514E+01
 -0.2134849E-04 -0.4462458E-06  0.3927961E-01  0.1452386E-06  0.1348193E-05  0.6820841E-06  0.2988511E-04 -0.3054749E-04
 -0.2702829E-06 -0.7432443E-01  0.1615955E-05  0.2651180E-05 -0.1508734E-06  0.1053701E-05 -0.7433270E-01  0.1257212E-05
  eigenvectors of nos in ao-basis, column    9
 -0.4407458E+00 -0.2262307E+01  0.2133193E+01  0.4264690E-05  0.1272654E-04 -0.7195410E+00 -0.1746735E-04 -0.1391544E-04
  0.1082725E+01 -0.2180434E-05  0.2721028E-05 -0.1352570E-01 -0.5098903E-05  0.6693044E-01  0.4082040E+00 -0.2305037E+00
 -0.1275443E-01 -0.2204851E-05  0.7859540E-01  0.4082426E+00 -0.2305433E+00  0.1275794E-01 -0.4913041E-05  0.7861537E-01
  eigenvectors of nos in ao-basis, column   10
  0.7072693E-04  0.3125744E-03 -0.4403043E-03 -0.4963027E+00 -0.6384804E-05 -0.2435414E-05  0.1043566E+01  0.9205734E-05
  0.4831311E-04  0.8286392E-05 -0.1785599E-05 -0.2578434E-04  0.6129723E+00  0.6807991E-04  0.6639204E+00 -0.5040547E+00
  0.1120210E+00 -0.1977061E-05 -0.2183520E-01 -0.6635713E+00  0.5038580E+00  0.1120680E+00  0.3153758E-05  0.2195428E-01
  eigenvectors of nos in ao-basis, column   11
  0.2962738E+00  0.1326996E+01 -0.1812946E+01  0.1107379E-03 -0.7824216E-05 -0.3217078E-01 -0.2610223E-03  0.1175864E-04
  0.2041421E+00  0.1248880E-04 -0.1586939E-06 -0.9932759E-01 -0.1668169E-03  0.2603810E+00  0.6364333E+00 -0.3744613E+00
 -0.6930966E-01 -0.4000374E-05  0.2214645E+00  0.6368007E+00 -0.3747358E+00  0.6926150E-01  0.4647586E-05  0.2214756E+00
  eigenvectors of nos in ao-basis, column   12
  0.4692009E-05  0.2241691E-04 -0.2380539E-04 -0.7103293E-05 -0.8375174E-05  0.7110132E-06  0.1018750E-04  0.1941459E-04
 -0.6579685E-05 -0.8020038E+00  0.1841424E-04 -0.3859964E-05  0.6581006E-05  0.1461025E-05 -0.1492176E-05  0.2827838E-06
 -0.2193101E-05  0.2665941E+00  0.3269055E-05 -0.3971746E-05  0.3769444E-05  0.5627726E-05 -0.2666141E+00  0.6424005E-05
  eigenvectors of nos in ao-basis, column   13
 -0.1348639E+00 -0.5737435E+00  0.9280728E+00  0.1221983E-05  0.3309175E-05  0.1009421E+00 -0.2633867E-05 -0.8485513E-05
 -0.5179121E+00  0.1080794E-04 -0.1621728E-04 -0.2095867E+00 -0.8370458E-05 -0.2146196E+00 -0.4768812E+00  0.2734344E+00
 -0.1642157E+00  0.1767174E-05  0.9185300E-01 -0.4768833E+00  0.2734446E+00  0.1642210E+00  0.6234036E-05  0.9185086E-01
  eigenvectors of nos in ao-basis, column   14
  0.7170573E-07  0.3497373E-06  0.1080324E-05  0.3637087E-06 -0.8684429E-01  0.4689022E-05 -0.3691476E-06  0.3061617E+00
 -0.1253557E-04  0.2490359E-04  0.8899589E+00 -0.3871078E-05  0.2007777E-05 -0.4712220E-05 -0.5508226E-05  0.5502629E-05
 -0.2067156E-05 -0.1994335E+00  0.9530146E-06 -0.5161081E-05  0.4403149E-05  0.2608420E-05 -0.1994302E+00  0.1600783E-05
  eigenvectors of nos in ao-basis, column   15
 -0.3558159E+00 -0.1347787E+01  0.2694356E+01 -0.1061562E-02  0.3294673E-05  0.1203000E+00  0.2752623E-02 -0.5296648E-05
 -0.1148423E+01  0.1576880E-05  0.1536748E-05 -0.2960318E-01 -0.1731315E-02  0.4153432E+00 -0.2247961E+01  0.1265777E+01
 -0.7309094E-01  0.9813179E-06 -0.1577467E+00 -0.2254002E+01  0.1268959E+01  0.7337886E-01 -0.3564284E-06 -0.1566622E+00
  eigenvectors of nos in ao-basis, column   16
 -0.7367439E-03 -0.2773187E-02  0.5696997E-02  0.4924297E+00  0.1760620E-05  0.1898108E-03 -0.1276910E+01 -0.2089962E-05
 -0.2371871E-02 -0.5486227E-05 -0.1535833E-05 -0.7643105E-04  0.8079103E+00  0.9044898E-03 -0.1406252E+01  0.7328004E+00
 -0.7488850E-01 -0.6917597E-05  0.2593405E+00  0.1396572E+01 -0.7273484E+00 -0.7455090E-01  0.4245793E-05 -0.2601307E+00
  eigenvectors of nos in ao-basis, column   17
  0.3417126E-05  0.1636079E-04 -0.4783373E-05 -0.2181717E-04 -0.1051079E-04 -0.8659569E-05 -0.1693184E-04  0.9026307E-04
 -0.4017839E-05  0.7233206E+00 -0.6898316E-04  0.2534877E-05  0.2426700E-04  0.6042978E-06 -0.8622846E-04  0.7060239E-04
 -0.1885103E-04  0.7381952E+00 -0.1887482E-04  0.4751776E-04 -0.4188894E-04  0.3613125E-05 -0.7383779E+00  0.2100291E-04
  eigenvectors of nos in ao-basis, column   18
  0.9007777E-04  0.4102974E-03 -0.1393293E-03  0.8827425E+00 -0.3930831E-05 -0.2451838E-03  0.1486195E+00  0.1560736E-05
  0.9232634E-04  0.2063723E-04  0.4003986E-05  0.7621845E-05 -0.6595190E+00  0.4355294E-04  0.1768852E+01 -0.1623659E+01
  0.3729517E+00  0.2806077E-04  0.7004600E+00 -0.1769475E+01  0.1624136E+01  0.3733522E+00 -0.1704292E-04 -0.7007224E+00
  eigenvectors of nos in ao-basis, column   19
 -0.1955232E+00 -0.9676745E+00 -0.2571451E+00  0.3297900E-04 -0.3335731E-04  0.6789368E+00  0.3592347E-03  0.4121886E-03
  0.3963628E+00  0.8717602E-05 -0.2977695E-03 -0.1352300E+00 -0.1012797E-03 -0.1112030E+00  0.1608588E+01 -0.1103835E+01
  0.9105525E+00 -0.4311309E-03  0.1196732E+00  0.1607662E+01 -0.1103190E+01 -0.9107622E+00 -0.4517337E-03  0.1191801E+00
  eigenvectors of nos in ao-basis, column   20
 -0.1018988E-03 -0.5036753E-03 -0.1253409E-03 -0.1678382E-04  0.5679730E-01  0.3466185E-03  0.1863401E-04 -0.7786680E+00
  0.2149920E-03  0.7450645E-04  0.5705829E+00 -0.7142628E-04  0.8287628E-05 -0.5668820E-04  0.8266439E-03 -0.5648769E-03
  0.4574979E-03  0.8454495E+00  0.5811655E-04  0.8469293E-03 -0.5915483E-03 -0.4886535E-03  0.8452836E+00  0.5504690E-04
  eigenvectors of nos in ao-basis, column   21
 -0.2241981E-03 -0.9759020E-03  0.6180672E-03  0.6333552E+00  0.5556246E-06  0.4772911E-03 -0.1252100E+01 -0.1292608E-04
 -0.4893720E-03 -0.2207279E-05  0.9280744E-05  0.6071282E-04 -0.1570191E+00 -0.1138955E-03 -0.2868150E+00 -0.1266726E+00
  0.7553192E+00  0.1061087E-04 -0.3381951E+00  0.2873964E+00  0.1260918E+00  0.7548683E+00  0.1808790E-04  0.3391442E+00
  eigenvectors of nos in ao-basis, column   22
  0.3072308E+00  0.1287496E+01 -0.1186460E+01  0.2138197E-03  0.3301649E-05 -0.5279206E+00 -0.6118919E-03 -0.1535239E-05
  0.1010709E+01  0.1335930E-05 -0.3670796E-05 -0.1710383E+00 -0.5813200E-04  0.1661276E+00  0.1443123E+00  0.7427945E-01
  0.1985187E-01 -0.2648695E-05 -0.8127100E+00  0.1448482E+00  0.7407983E-01 -0.1909594E-01 -0.6607852E-05 -0.8121245E+00
  eigenvectors of nos in ao-basis, column   23
 -0.3250884E+00 -0.1122217E+01  0.3936595E+01  0.4048983E-04 -0.6169276E-05 -0.3502672E+00  0.1765727E-03  0.1229679E-04
 -0.9601830E+00  0.3368335E-06 -0.1264692E-05  0.2159934E-01 -0.6792021E-04  0.1871081E+00 -0.1186730E+01 -0.4662177E+00
 -0.7529335E+00 -0.1581024E-05 -0.5023114E+00 -0.1186955E+01 -0.4664692E+00  0.7531026E+00 -0.3831040E-05 -0.5024470E+00
  eigenvectors of nos in ao-basis, column   24
 -0.3184332E-04 -0.1097414E-03  0.4032637E-03 -0.3578842E+00 -0.3121334E-05 -0.4033081E-04 -0.1597303E+01  0.5156405E-05
 -0.8575752E-04  0.6529575E-06 -0.2721172E-06  0.5346483E-06  0.5592273E+00  0.1884437E-04 -0.9487947E+00 -0.1207415E+01
 -0.7188146E+00  0.8300363E-06 -0.6517447E+00  0.9485863E+00  0.1207285E+01 -0.7186589E+00 -0.1024186E-05  0.6516239E+00


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***


 cosurf_and_phi
  2.466315385316 -0.061539892486  2.837440149000 -0.037496079718
  0.006359863210 -2.199287675687  3.114544749440 -0.066830494620
 -0.480526931872  1.061509344774  3.755560521090 -0.068654880380
  1.262739748330  2.872870034186  1.567866969040 -0.044634545820
  1.897949994968 -2.634366215369  0.570962659525 -0.015637560864
 -2.790166444607 -0.824331206483  2.170430257018 -0.021434120503
 -2.061296060778  2.493528161233  1.034251636268 -0.021711867359
 -0.002991703587  2.420846177436 -1.447622679829  0.014783289400
 -1.114435211431 -2.949559801670 -0.067898604227 -0.015679755469
  1.955709175590 -0.984462672186  3.123501329999 -0.050035653917
  1.000900154469 -1.706149424342  3.300403738319 -0.063506375000
  1.652947872482  0.360731428865  3.496571379926 -0.056888336516
  0.594940552015  0.670555958619  3.845537291721 -0.068375702040
  2.390078334690  1.202722009900  2.566708449184 -0.036729505883
  1.989334673621  2.229216060677  2.001028520754 -0.038836481059
  2.919143888229  0.378144896597  2.099775822683 -0.016334022673
  2.780367109803 -0.921046401167  2.130496470151 -0.021152433254
  2.449941849009 -2.011800733381  1.439000205618 -0.019292295159
 -0.231540571447 -1.263621120083  3.706953994699 -0.069263595421
 -0.352106389130 -0.107618694495  3.950679056227 -0.069599817890
  0.105380882100  1.878884201281  3.371423292662 -0.068134776131
  0.783266721831  2.590884380442  2.520834408126 -0.059840554643
  2.011402526412  2.551496219550  0.814855178005 -0.018351509927
  1.440220538007 -2.843720084632  1.356653310131 -0.039166616648
  0.871209499702 -2.660587439486  2.372618950166 -0.057844847475
 -2.947488997374  0.298617382934  2.058332654596 -0.014727292309
 -2.674954162172  1.563027715274  1.704200253745 -0.017453593117
 -1.353448939749  2.910920816403  0.212056013565 -0.018583123818
 -0.743422400795  2.810699746106 -0.731960510989 -0.002855070386
  0.099626804209  1.855073908563 -1.945822518898  0.033668103178
  0.019900458911 -1.968682238442 -1.864952523830  0.030242664505
 -0.601526683996 -2.669374282549 -1.032942810695  0.004636485895
 -1.976701730993 -2.578422698078  0.816296596419 -0.019229398207
 -2.525639241426 -1.850752473194  1.593331825886 -0.019941090717
 -1.124962231191 -1.881571875050  3.121019991266 -0.061365091132
 -2.117398618237 -1.513942036836  2.667866282109 -0.043521885676
 -2.336046769995 -0.152318885910  2.976109389266 -0.041553669291
 -1.478775916646  0.469153703894  3.577445396081 -0.059606709996
 -1.328587214463  1.668007282102  3.174271253825 -0.059491063765
 -1.771761195387  2.287385908353  2.202255939279 -0.045164666266
 -1.026730149698  3.017318998523  1.358608629268 -0.044267234208
  0.119020599620  3.173161356543  1.415159765853 -0.050074833451
  0.915237473358  3.108118624501  0.463299650838 -0.029907841108
  0.462814589486  2.837210699514 -0.795516582628 -0.003692731121
  1.026812912753 -3.017319139618  0.083991013862 -0.020500692474
 -0.070059393274 -3.176418651270  0.035610954337 -0.025744997127
 -0.970268948715 -3.083313212042  1.062474740253 -0.040216467073
 -0.534203029846 -2.828286137107  2.231267851728 -0.058592359769
  0.871897873699 -0.575451888002  3.799144800425 -0.066511489600
  1.408719892640  1.640288870679  3.148120355694 -0.058373808475
  2.650053602975  1.633766196698  1.655441764425 -0.017347101702
  1.964286464189 -2.001027831890  2.365093852582 -0.043514195414
 -1.342877128538 -0.760711330777  3.581800927521 -0.061289497400
 -0.531365927285  2.661712102822  2.509437292848 -0.061209519532
  1.142904167226  2.885766749848 -0.243475252462 -0.010768860111
  0.342129526197 -3.189261637688  1.246854200824 -0.047157682677
 -2.346459445268  1.135413320199  2.662806558936 -0.038656758269
 -0.258565605057  3.205542092831  0.249849913336 -0.029904601258
  0.450566961335 -2.699565911226 -1.032013446782  0.003210596979
 -1.684533476150 -2.430522251364  2.070179818920 -0.045088641596
 -3.368784049130  0.036613212616 -1.854972615293  0.060873688136
 -3.278396927799  1.580834404538 -0.078603229040  0.033676555240
 -3.656110378332 -0.713812229513  0.361831391088  0.034386909338
 -2.408052717333 -2.075622947728 -1.226189024363  0.039013631868
 -1.295829484895 -0.567704086865 -2.747607986898  0.058904592482
 -1.846557165647  1.681693338816 -2.099716493181  0.049152242111
 -3.831862607217  0.357468890277 -0.654813288033  0.050842556716
 -3.527736967644 -1.045671231370 -1.064852892071  0.052045452943
 -2.622906831544 -1.024738705101 -2.241124222290  0.058620536059
 -2.402781990555  0.378472303440 -2.579763034114  0.061918605531
 -3.126606927219  1.313275381963 -1.541857021673  0.053625335365
 -3.633182667875  0.531520753705  0.561856665829  0.031280507821
 -3.086813000261 -1.794874586082 -0.179700355757  0.031751527425
 -1.615880090830 -1.674393887320 -2.147504235692  0.048331943000
 -1.240258025012  0.765881087348 -2.687977655831  0.057657463262
 -2.430292517958  2.154437082790 -0.969924007583  0.034533137805
  3.368776503197 -0.036639040867 -1.854966842961  0.060862864118
  3.278392620256 -1.580850766330 -0.078589062660  0.033669751157
  3.656106873706  0.713798214804  0.361832640492  0.034380028550
  2.408046317685  2.075600470298 -1.226192756897  0.039003427150
  1.295820311657  0.567673501678 -2.747601655947  0.058894324166
  1.846549173548 -1.681720471299 -2.099699178989  0.049142677020
  3.831857249217 -0.357488322766 -0.654806650060  0.050833937488
  3.527730862121  1.045649613724 -1.064853177123  0.052035772346
  2.622898581638  1.024710819001 -2.241122746235  0.058609027943
  2.402773123306 -0.378501994157 -2.579753678917  0.061907062383
  3.126599952113 -1.313299541573 -1.541844004398  0.053615410415
  3.633179527908 -0.531533702443  0.561864593522  0.031274554992
  3.086808508398  1.794857685487 -0.179703829578  0.031743181943
  1.615872011596  1.674366500124 -2.147504385867  0.048321220045
  1.240248960489 -0.765911354740 -2.687964116774  0.057648054340
  2.430286585511 -2.154458194501 -0.969905238283  0.034525275056
  0.000006852884  1.035694667087 -2.361676372823  0.052195864377
 -0.298806015181  0.969607486526 -2.408809074493  0.053677309043
  0.298815704890  0.969607820141 -2.408807386530  0.053673867370
  0.000007656756 -1.035723195601 -2.361670853435  0.052196655038
 -0.298805262603 -0.969636498141 -2.408803907288  0.053678115859
  0.298816457468 -0.969636367899 -2.408802219325  0.053674483807
 -0.667712940797 -1.245186997899 -2.338574529312  0.051210197106
 -1.218112188165 -2.025138759161 -1.616566947615  0.036360101385
 -2.084175601108 -2.294008802021 -0.480477682822  0.020694837947
 -2.876631896395 -1.658047550445  0.559052047065  0.017360982052
 -3.282909221331 -0.368099862162  1.091996180628  0.019474475197
 -3.142757910518  1.067034798172  0.908143333087  0.018454088243
 -2.511458431963  2.081290260960  0.080011352455  0.017658454050
 -1.638016880752  2.274609499719 -1.065756198713  0.027070759470
 -0.866948462969  1.570740798135 -2.077229430584  0.047055497772
 -0.467915446486 -1.694563900014 -1.941501402531  0.039850191074
 -1.299753513329 -2.453901457341 -0.850306853788  0.012776360462
 -2.242033801688 -2.245340287831  0.385760998463  0.000960794452
 -2.923088754321 -1.151144046328  1.279154738758  0.002904156178
 -3.074287016292  0.397098864814  1.477489335582  0.004502312625
 -2.635990824421  1.788708515315  0.902534843950  0.001032339970
 -1.781079179779  2.474786487342 -0.218927030025  0.004571491103
 -0.846758459860  2.184720175398 -1.444553386477  0.025112820589
 -0.255852300976  1.360631011730 -2.219692209228  0.046705423659
  0.667723897920  1.245157743773 -2.338577856151  0.051202642415
  1.218123388571  2.025110412626 -1.616576841774  0.036350367156
  2.084186643139  2.293984793080 -0.480493513306  0.020686045206
  2.876642520254  1.658030225134  0.559035126479  0.017354582578
  3.282919570196  0.368088739237  1.091983758387  0.019469921221
  3.142768358070 -1.067043033853  0.908139383421  0.018450058137
  2.511469270857 -2.081300494445  0.080016452498  0.017653483155
  1.638028059662 -2.274626132712 -1.065745290088  0.027064131994
  0.866959534092 -1.570765766224 -2.077218589767  0.047048583657
  0.467932162408  1.694535407938 -1.941510815499  0.039844333560
  1.299770347024  2.453875604722 -0.850324284820  0.012768496686
  2.242050270258  2.245320705184  0.385739526123  0.000954480120
  2.923104801922  1.151131828431  1.279135167181  0.002900110193
  3.074302957696 -0.397105856761  1.477477125466  0.004499735704
  2.636007061705 -1.788714946455  0.902532611959  0.001029969002
  1.781095866662 -2.474797662024 -0.218920775640  0.004567989223
  0.846775315928 -2.184739733041 -1.444543821640  0.025108731119
  0.255868904327 -1.360657816862 -2.219684436601  0.046703695006
  0.427045418766 -0.580992742084 -2.585105089006  0.057293295706
  0.427044967836  0.580963354323 -2.585108185092  0.057292977618
 -0.427035838172 -0.000014913339 -2.543076305315  0.062569932754
 end_of_phi


 nsubv after electrostatic potential =  0

  Confirmation of dielectric energy for ground state
  edielnew =  -0.011551886


 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************


 cosurf_and_qcos_from cosmo(3
  2.466315385316 -0.061539892486  2.837440149000  0.002321545361
  0.006359863210 -2.199287675687  3.114544749440  0.006226569995
 -0.480526931872  1.061509344774  3.755560521090  0.005720979916
  1.262739748330  2.872870034186  1.567866969040  0.005019433158
  1.897949994968 -2.634366215369  0.570962659525  0.001849753673
 -2.790166444607 -0.824331206483  2.170430257018  0.001030712540
 -2.061296060778  2.493528161233  1.034251636268  0.002860231244
 -0.002991703587  2.420846177436 -1.447622679829  0.000746450362
 -1.114435211431 -2.949559801670 -0.067898604227  0.002952615319
  1.955709175590 -0.984462672186  3.123501329999  0.004684343412
  1.000900154469 -1.706149424342  3.300403738319  0.006095508818
  1.652947872482  0.360731428865  3.496571379926  0.005247975988
  0.594940552015  0.670555958619  3.845537291721  0.005598499356
  2.390078334690  1.202722009900  2.566708449184  0.003436671352
  1.989334673621  2.229216060677  2.001028520754  0.003470250852
  2.919143888229  0.378144896597  2.099775822683  0.000482837267
  2.780367109803 -0.921046401167  2.130496470151  0.001485795483
  2.449941849009 -2.011800733381  1.439000205618  0.001879400993
 -0.231540571447 -1.263621120083  3.706953994699  0.006494185724
 -0.352106389130 -0.107618694495  3.950679056227  0.006255791679
  0.105380882100  1.878884201281  3.371423292662  0.008052737995
  0.783266721831  2.590884380442  2.520834408126  0.007164112175
  2.011402526412  2.551496219550  0.814855178005  0.002368944079
  1.440220538007 -2.843720084632  1.356653310131  0.004594896122
  0.871209499702 -2.660587439486  2.372618950166  0.007728137100
 -2.947488997374  0.298617382934  2.058332654596  0.000453567859
 -2.674954162172  1.563027715274  1.704200253745  0.001161681638
 -1.353448939749  2.910920816403  0.212056013565  0.003308938811
 -0.743422400795  2.810699746106 -0.731960510989  0.002240667744
  0.099626804209  1.855073908563 -1.945822518898 -0.000142028772
  0.019900458911 -1.968682238442 -1.864952523830 -0.000389505031
 -0.601526683996 -2.669374282549 -1.032942810695  0.001311321620
 -1.976701730993 -2.578422698078  0.816296596419  0.002534266231
 -2.525639241426 -1.850752473194  1.593331825886  0.001689737673
 -1.124962231191 -1.881571875050  3.121019991266  0.005995170911
 -2.117398618237 -1.513942036836  2.667866282109  0.003978069690
 -2.336046769995 -0.152318885910  2.976109389266  0.004153828027
 -1.478775916646  0.469153703894  3.577445396081  0.005163998461
 -1.328587214463  1.668007282102  3.174271253825  0.006151365721
 -1.771761195387  2.287385908353  2.202255939279  0.005848702060
 -1.026730149698  3.017318998523  1.358608629268  0.005753122851
  0.119020599620  3.173161356543  1.415159765853  0.007074420698
  0.915237473358  3.108118624501  0.463299650838  0.004884346738
  0.462814589486  2.837210699514 -0.795516582628  0.002342985797
  1.026812912753 -3.017319139618  0.083991013862  0.004077338614
 -0.070059393274 -3.176418651270  0.035610954337  0.004685541913
 -0.970268948715 -3.083313212042  1.062474740253  0.006588816956
 -0.534203029846 -2.828286137107  2.231267851728  0.008013506723
  0.871897873699 -0.575451888002  3.799144800425  0.006516748862
  1.408719892640  1.640288870679  3.148120355694  0.007042390851
  2.650053602975  1.633766196698  1.655441764425  0.001069050902
  1.964286464189 -2.001027831890  2.365093852582  0.004752596647
 -1.342877128538 -0.760711330777  3.581800927521  0.005374679552
 -0.531365927285  2.661712102822  2.509437292848  0.008071782517
  1.142904167226  2.885766749848 -0.243475252462  0.001392892734
  0.342129526197 -3.189261637688  1.246854200824  0.006885598512
 -2.346459445268  1.135413320199  2.662806558936  0.003547641880
 -0.258565605057  3.205542092831  0.249849913336  0.004510625293
  0.450566961335 -2.699565911226 -1.032013446782  0.002367420719
 -1.684533476150 -2.430522251364  2.070179818920  0.005007221479
 -3.368784049130  0.036613212616 -1.854972615293 -0.007700132347
 -3.278396927799  1.580834404538 -0.078603229040 -0.005741234111
 -3.656110378332 -0.713812229513  0.361831391088 -0.006442112756
 -2.408052717333 -2.075622947728 -1.226189024363 -0.005385788054
 -1.295829484895 -0.567704086865 -2.747607986898 -0.005903149374
 -1.846557165647  1.681693338816 -2.099716493181 -0.006060252366
 -3.831862607217  0.357468890277 -0.654813288033 -0.008326584933
 -3.527736967644 -1.045671231370 -1.064852892071 -0.008232901937
 -2.622906831544 -1.024738705101 -2.241124222290 -0.008147833678
 -2.402781990555  0.378472303440 -2.579763034114 -0.007924385934
 -3.126606927219  1.313275381963 -1.541857021673 -0.008379807035
 -3.633182667875  0.531520753705  0.561856665829 -0.004523485108
 -3.086813000261 -1.794874586082 -0.179700355757 -0.004498204555
 -1.615880090830 -1.674393887320 -2.147504235692 -0.004634137957
 -1.240258025012  0.765881087348 -2.687977655831 -0.004879714067
 -2.430292517958  2.154437082790 -0.969924007583 -0.004066772294
  3.368776503197 -0.036639040867 -1.854966842961 -0.007705608687
  3.278392620256 -1.580850766330 -0.078589062660 -0.005726292913
  3.656106873706  0.713798214804  0.361832640492 -0.006436711410
  2.408046317685  2.075600470298 -1.226192756897 -0.005393627650
  1.295820311657  0.567673501678 -2.747601655947 -0.006326163506
  1.846549173548 -1.681720471299 -2.099699178989 -0.006012575752
  3.831857249217 -0.357488322766 -0.654806650060 -0.008325755036
  3.527730862121  1.045649613724 -1.064853177123 -0.008235553138
  2.622898581638  1.024710819001 -2.241122746235 -0.008133141687
  2.402773123306 -0.378501994157 -2.579753678917 -0.007914553149
  3.126599952113 -1.313299541573 -1.541844004398 -0.008383596217
  3.633179527908 -0.531533702443  0.561864593522 -0.004519122390
  3.086808508398  1.794857685487 -0.179703829578 -0.004495225421
  1.615872011596  1.674366500124 -2.147504385867 -0.004599060340
  1.240248960489 -0.765911354740 -2.687964116774 -0.005139219244
  2.430286585511 -2.154458194501 -0.969905238283 -0.004052590038
  0.000006852884  1.035694667087 -2.361676372823 -0.000286501156
 -0.298806015181  0.969607486526 -2.408809074493 -0.000344375371
  0.298815704890  0.969607820141 -2.408807386530 -0.000253456991
  0.000007656756 -1.035723195601 -2.361670853435 -0.000279685617
 -0.298805262603 -0.969636498141 -2.408803907288 -0.000330079579
  0.298816457468 -0.969636367899 -2.408802219325 -0.000250086896
 -0.667712940797 -1.245186997899 -2.338574529312 -0.000950468014
 -1.218112188165 -2.025138759161 -1.616566947615 -0.001418129050
 -2.084175601108 -2.294008802021 -0.480477682822 -0.000943826452
 -2.876631896395 -1.658047550445  0.559052047065 -0.001363837032
 -3.282909221331 -0.368099862162  1.091996180628 -0.001812932597
 -3.142757910518  1.067034798172  0.908143333087 -0.001624405460
 -2.511458431963  2.081290260960  0.080011352455 -0.001170418266
 -1.638016880752  2.274609499719 -1.065756198713 -0.001134010931
 -0.866948462969  1.570740798135 -2.077229430584 -0.001967996848
 -0.467915446486 -1.694563900014 -1.941501402531 -0.001430288438
 -1.299753513329 -2.453901457341 -0.850306853788 -0.000243536426
 -2.242033801688 -2.245340287831  0.385760998463 -0.000309992412
 -2.923088754321 -1.151144046328  1.279154738758 -0.000889120740
 -3.074287016292  0.397098864814  1.477489335582 -0.001162920861
 -2.635990824421  1.788708515315  0.902534843950 -0.000523948014
 -1.781079179779  2.474786487342 -0.218927030025 -0.000181228283
 -0.846758459860  2.184720175398 -1.444553386477 -0.000850145439
 -0.255852300976  1.360631011730 -2.219692209228 -0.000919033006
  0.667723897920  1.245157743773 -2.338577856151 -0.000883107709
  1.218123388571  2.025110412626 -1.616576841774 -0.001429970442
  2.084186643139  2.293984793080 -0.480493513306 -0.000921225944
  2.876642520254  1.658030225134  0.559035126479 -0.001358024666
  3.282919570196  0.368088739237  1.091983758387 -0.001801719639
  3.142768358070 -1.067043033853  0.908139383421 -0.001616845641
  2.511469270857 -2.081300494445  0.080016452498 -0.001110949949
  1.638028059662 -2.274626132712 -1.065745290088 -0.001093034418
  0.866959534092 -1.570765766224 -2.077218589767 -0.001916464716
  0.467932162408  1.694535407938 -1.941510815499 -0.001484438433
  1.299770347024  2.453875604722 -0.850324284820 -0.000254915900
  2.242050270258  2.245320705184  0.385739526123 -0.000240007563
  2.923104801922  1.151131828431  1.279135167181 -0.000915932894
  3.074302957696 -0.397105856761  1.477477125466 -0.001192220645
  2.636007061705 -1.788714946455  0.902532611959 -0.000569042477
  1.781095866662 -2.474797662024 -0.218920775640 -0.000236594216
  0.846775315928 -2.184739733041 -1.444543821640 -0.000900793114
  0.255868904327 -1.360657816862 -2.219684436601 -0.000850569064
  0.427045418766 -0.580992742084 -2.585105089006 -0.002233629810
  0.427044967836  0.580963354323 -2.585108185092 -0.002106302088
 -0.427035838172 -0.000014913339 -2.543076305315 -0.006022283878
 end_of_qcos

 sum of the negative charges =  -0.252555324

 sum of the positive charges =   0.247718427

 total sum =  -0.00483689732

 *** qcos is copied to qcosdalton ***

 fepsi =   1.
 *** qcosdalton is mult by fepsi ***
 cosurf and qcosdalton
  2.466315385316 -0.061539892486  2.837440149000  0.002321545361
  0.006359863210 -2.199287675687  3.114544749440  0.006226569995
 -0.480526931872  1.061509344774  3.755560521090  0.005720979916
  1.262739748330  2.872870034186  1.567866969040  0.005019433158
  1.897949994968 -2.634366215369  0.570962659525  0.001849753673
 -2.790166444607 -0.824331206483  2.170430257018  0.001030712540
 -2.061296060778  2.493528161233  1.034251636268  0.002860231244
 -0.002991703587  2.420846177436 -1.447622679829  0.000746450362
 -1.114435211431 -2.949559801670 -0.067898604227  0.002952615319
  1.955709175590 -0.984462672186  3.123501329999  0.004684343412
  1.000900154469 -1.706149424342  3.300403738319  0.006095508818
  1.652947872482  0.360731428865  3.496571379926  0.005247975988
  0.594940552015  0.670555958619  3.845537291721  0.005598499356
  2.390078334690  1.202722009900  2.566708449184  0.003436671352
  1.989334673621  2.229216060677  2.001028520754  0.003470250852
  2.919143888229  0.378144896597  2.099775822683  0.000482837267
  2.780367109803 -0.921046401167  2.130496470151  0.001485795483
  2.449941849009 -2.011800733381  1.439000205618  0.001879400993
 -0.231540571447 -1.263621120083  3.706953994699  0.006494185724
 -0.352106389130 -0.107618694495  3.950679056227  0.006255791679
  0.105380882100  1.878884201281  3.371423292662  0.008052737995
  0.783266721831  2.590884380442  2.520834408126  0.007164112175
  2.011402526412  2.551496219550  0.814855178005  0.002368944079
  1.440220538007 -2.843720084632  1.356653310131  0.004594896122
  0.871209499702 -2.660587439486  2.372618950166  0.007728137100
 -2.947488997374  0.298617382934  2.058332654596  0.000453567859
 -2.674954162172  1.563027715274  1.704200253745  0.001161681638
 -1.353448939749  2.910920816403  0.212056013565  0.003308938811
 -0.743422400795  2.810699746106 -0.731960510989  0.002240667744
  0.099626804209  1.855073908563 -1.945822518898 -0.000142028772
  0.019900458911 -1.968682238442 -1.864952523830 -0.000389505031
 -0.601526683996 -2.669374282549 -1.032942810695  0.001311321620
 -1.976701730993 -2.578422698078  0.816296596419  0.002534266231
 -2.525639241426 -1.850752473194  1.593331825886  0.001689737673
 -1.124962231191 -1.881571875050  3.121019991266  0.005995170911
 -2.117398618237 -1.513942036836  2.667866282109  0.003978069690
 -2.336046769995 -0.152318885910  2.976109389266  0.004153828027
 -1.478775916646  0.469153703894  3.577445396081  0.005163998461
 -1.328587214463  1.668007282102  3.174271253825  0.006151365721
 -1.771761195387  2.287385908353  2.202255939279  0.005848702060
 -1.026730149698  3.017318998523  1.358608629268  0.005753122851
  0.119020599620  3.173161356543  1.415159765853  0.007074420698
  0.915237473358  3.108118624501  0.463299650838  0.004884346738
  0.462814589486  2.837210699514 -0.795516582628  0.002342985797
  1.026812912753 -3.017319139618  0.083991013862  0.004077338614
 -0.070059393274 -3.176418651270  0.035610954337  0.004685541913
 -0.970268948715 -3.083313212042  1.062474740253  0.006588816956
 -0.534203029846 -2.828286137107  2.231267851728  0.008013506723
  0.871897873699 -0.575451888002  3.799144800425  0.006516748862
  1.408719892640  1.640288870679  3.148120355694  0.007042390851
  2.650053602975  1.633766196698  1.655441764425  0.001069050902
  1.964286464189 -2.001027831890  2.365093852582  0.004752596647
 -1.342877128538 -0.760711330777  3.581800927521  0.005374679552
 -0.531365927285  2.661712102822  2.509437292848  0.008071782517
  1.142904167226  2.885766749848 -0.243475252462  0.001392892734
  0.342129526197 -3.189261637688  1.246854200824  0.006885598512
 -2.346459445268  1.135413320199  2.662806558936  0.003547641880
 -0.258565605057  3.205542092831  0.249849913336  0.004510625293
  0.450566961335 -2.699565911226 -1.032013446782  0.002367420719
 -1.684533476150 -2.430522251364  2.070179818920  0.005007221479
 -3.368784049130  0.036613212616 -1.854972615293 -0.007700132347
 -3.278396927799  1.580834404538 -0.078603229040 -0.005741234111
 -3.656110378332 -0.713812229513  0.361831391088 -0.006442112756
 -2.408052717333 -2.075622947728 -1.226189024363 -0.005385788054
 -1.295829484895 -0.567704086865 -2.747607986898 -0.005903149374
 -1.846557165647  1.681693338816 -2.099716493181 -0.006060252366
 -3.831862607217  0.357468890277 -0.654813288033 -0.008326584933
 -3.527736967644 -1.045671231370 -1.064852892071 -0.008232901937
 -2.622906831544 -1.024738705101 -2.241124222290 -0.008147833678
 -2.402781990555  0.378472303440 -2.579763034114 -0.007924385934
 -3.126606927219  1.313275381963 -1.541857021673 -0.008379807035
 -3.633182667875  0.531520753705  0.561856665829 -0.004523485108
 -3.086813000261 -1.794874586082 -0.179700355757 -0.004498204555
 -1.615880090830 -1.674393887320 -2.147504235692 -0.004634137957
 -1.240258025012  0.765881087348 -2.687977655831 -0.004879714067
 -2.430292517958  2.154437082790 -0.969924007583 -0.004066772294
  3.368776503197 -0.036639040867 -1.854966842961 -0.007705608687
  3.278392620256 -1.580850766330 -0.078589062660 -0.005726292913
  3.656106873706  0.713798214804  0.361832640492 -0.006436711410
  2.408046317685  2.075600470298 -1.226192756897 -0.005393627650
  1.295820311657  0.567673501678 -2.747601655947 -0.006326163506
  1.846549173548 -1.681720471299 -2.099699178989 -0.006012575752
  3.831857249217 -0.357488322766 -0.654806650060 -0.008325755036
  3.527730862121  1.045649613724 -1.064853177123 -0.008235553138
  2.622898581638  1.024710819001 -2.241122746235 -0.008133141687
  2.402773123306 -0.378501994157 -2.579753678917 -0.007914553149
  3.126599952113 -1.313299541573 -1.541844004398 -0.008383596217
  3.633179527908 -0.531533702443  0.561864593522 -0.004519122390
  3.086808508398  1.794857685487 -0.179703829578 -0.004495225421
  1.615872011596  1.674366500124 -2.147504385867 -0.004599060340
  1.240248960489 -0.765911354740 -2.687964116774 -0.005139219244
  2.430286585511 -2.154458194501 -0.969905238283 -0.004052590038
  0.000006852884  1.035694667087 -2.361676372823 -0.000286501156
 -0.298806015181  0.969607486526 -2.408809074493 -0.000344375371
  0.298815704890  0.969607820141 -2.408807386530 -0.000253456991
  0.000007656756 -1.035723195601 -2.361670853435 -0.000279685617
 -0.298805262603 -0.969636498141 -2.408803907288 -0.000330079579
  0.298816457468 -0.969636367899 -2.408802219325 -0.000250086896
 -0.667712940797 -1.245186997899 -2.338574529312 -0.000950468014
 -1.218112188165 -2.025138759161 -1.616566947615 -0.001418129050
 -2.084175601108 -2.294008802021 -0.480477682822 -0.000943826452
 -2.876631896395 -1.658047550445  0.559052047065 -0.001363837032
 -3.282909221331 -0.368099862162  1.091996180628 -0.001812932597
 -3.142757910518  1.067034798172  0.908143333087 -0.001624405460
 -2.511458431963  2.081290260960  0.080011352455 -0.001170418266
 -1.638016880752  2.274609499719 -1.065756198713 -0.001134010931
 -0.866948462969  1.570740798135 -2.077229430584 -0.001967996848
 -0.467915446486 -1.694563900014 -1.941501402531 -0.001430288438
 -1.299753513329 -2.453901457341 -0.850306853788 -0.000243536426
 -2.242033801688 -2.245340287831  0.385760998463 -0.000309992412
 -2.923088754321 -1.151144046328  1.279154738758 -0.000889120740
 -3.074287016292  0.397098864814  1.477489335582 -0.001162920861
 -2.635990824421  1.788708515315  0.902534843950 -0.000523948014
 -1.781079179779  2.474786487342 -0.218927030025 -0.000181228283
 -0.846758459860  2.184720175398 -1.444553386477 -0.000850145439
 -0.255852300976  1.360631011730 -2.219692209228 -0.000919033006
  0.667723897920  1.245157743773 -2.338577856151 -0.000883107709
  1.218123388571  2.025110412626 -1.616576841774 -0.001429970442
  2.084186643139  2.293984793080 -0.480493513306 -0.000921225944
  2.876642520254  1.658030225134  0.559035126479 -0.001358024666
  3.282919570196  0.368088739237  1.091983758387 -0.001801719639
  3.142768358070 -1.067043033853  0.908139383421 -0.001616845641
  2.511469270857 -2.081300494445  0.080016452498 -0.001110949949
  1.638028059662 -2.274626132712 -1.065745290088 -0.001093034418
  0.866959534092 -1.570765766224 -2.077218589767 -0.001916464716
  0.467932162408  1.694535407938 -1.941510815499 -0.001484438433
  1.299770347024  2.453875604722 -0.850324284820 -0.000254915900
  2.242050270258  2.245320705184  0.385739526123 -0.000240007563
  2.923104801922  1.151131828431  1.279135167181 -0.000915932894
  3.074302957696 -0.397105856761  1.477477125466 -0.001192220645
  2.636007061705 -1.788714946455  0.902532611959 -0.000569042477
  1.781095866662 -2.474797662024 -0.218920775640 -0.000236594216
  0.846775315928 -2.184739733041 -1.444543821640 -0.000900793114
  0.255868904327 -1.360657816862 -2.219684436601 -0.000850569064
  0.427045418766 -0.580992742084 -2.585105089006 -0.002233629810
  0.427044967836  0.580963354323 -2.585108185092 -0.002106302088
 -0.427035838172 -0.000014913339 -2.543076305315 -0.006022283878
 end of cosurf and qcosdalton

 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 ** Total nuclear repulsion energy ** =   9.36095227875974
 screening nuclear repulsion energy   0.00755837353

 Total-screening nuclear repulsion energy   9.35339391


 Adding T+Vsolv ...
 maxdens  300
 *** End of DALTON-COSMO calculation ***

  original map vector  20 21 22 23 24 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17
 18 19 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0


          sorted Vsolv matrix  block   1

               nbf   1        nbf   2        nbf   3        nbf   4        nbf   5        nbf   6        nbf   7        nbf   8
  nbf   1    -3.31800299
  nbf   2     0.00001575    -3.09058818
  nbf   3     0.00001796    -0.98129465    -4.10173886
  nbf   4    -1.01842104     0.00000425     0.00001681    -4.07781289
  nbf   5    -0.42217876     0.00001260     0.00001047    -0.44126884    -4.75246258
  nbf   6    -0.00000513    -0.00000339     0.00000212     0.00000136     0.00000161    -5.34889706
  nbf   7    -0.00000448    -1.17060672    -0.38130962     0.00000795     0.00000677    -0.00000272    -4.33719088
  nbf   8    -0.41286825     0.00000932     0.00002842     0.04017819     0.09704437    -0.00000436    -0.00000463    -3.58727342
  nbf   9     0.00000274     0.00000102     0.00000187     0.00000125    -0.00000001    -0.00000310     0.00000086    -0.00000228
  nbf  10     0.00000010     0.00000040     0.00000125     0.00000081     0.00000611     0.24655953     0.00000115    -0.00000061
  nbf  11     0.71274315     0.00000324     0.00000645    -0.06900810    -0.45107253     0.00000248     0.00000110     0.37446500
  nbf  12    -0.00002182    -0.02255513    -0.02737150     0.00000047    -0.00004175     0.00000092    -0.10300303    -0.00003925
  nbf  13     0.00001067     0.51226440     1.29215301     0.00000937     0.00000513     0.00000174     1.13411981     0.00000465
  nbf  14     0.60244405     0.00000726     0.00000321     1.39182514     0.61422163     0.00000226     0.00000729     0.29791186
  nbf  15     0.00000113     0.00000005    -0.00000011     0.00000167     0.00000192     0.06453545     0.00000118     0.00000074
  nbf  16     0.00000148     0.00000192     0.00000280     0.00000049     0.00000035    -0.00000229    -0.00000021    -0.00000074
  nbf  17     0.01597680    -0.00000631    -0.00001379     0.28427646    -0.03522509     0.00000102     0.00000131    -0.91939483
  nbf  18    -0.11636432    -0.00000597    -0.00000159    -0.29334023    -0.90109535     0.00000048    -0.00000102    -0.23509476
  nbf  19    -0.00000635     0.16662292    -0.06195845     0.00000039    -0.00000119    -0.00000009     1.19139590    -0.00000734
  nbf  20     0.18303953     0.00000005     0.00000000     0.16560632    -0.11838408    -0.00000046     0.00000013     0.20494899
  nbf  21    -0.99287851    -0.00000495    -0.00001593    -0.72375990     0.04745154     0.00000173     0.00000386    -0.60130280
  nbf  22     0.00000786     1.23667096     0.65677148     0.00001038     0.00000961    -0.00000236     1.45004596     0.00001191
  nbf  23     0.54130747     0.00000772     0.00001366     0.73132950     1.71707350     0.00000016     0.00000308    -0.41969022
  nbf  24     0.00000200     0.00000137    -0.00000148    -0.00000169     0.00000013    -2.13412685     0.00000157     0.00000231

               nbf   9        nbf  10        nbf  11        nbf  12        nbf  13        nbf  14        nbf  15        nbf  16
  nbf   9    -3.42221814
  nbf  10     0.00001982    -3.17751229
  nbf  11    -0.00000298    -0.00000185    -4.03662508
  nbf  12    -0.00000420     0.00000168    -0.00000812    -2.63219205
  nbf  13    -0.00000191    -0.00000169     0.00000128     0.09058670    -3.62259957
  nbf  14    -0.00000170    -0.00000158    -0.18629434     0.00000095     0.00002152    -3.66396498
  nbf  15     0.00000627     0.90766471    -0.00000061     0.00000069     0.00000003     0.00000034    -3.67017627
  nbf  16     0.90252441     0.00000713    -0.00000033    -0.00000091    -0.00000063    -0.00000023     0.00000630    -3.63973750
  nbf  17     0.00000122     0.00000043    -0.42343762     0.00002345    -0.00000514     0.04524565    -0.00000009     0.00000163
  nbf  18     0.00000058    -0.00000151     0.56834204     0.00000989    -0.00000937    -0.46535593    -0.00000085     0.00000048
  nbf  19    -0.00000117    -0.00000060    -0.00000053    -0.08841226     0.44955503     0.00000848    -0.00000105    -0.00000004
  nbf  20    -0.00000001     0.00000003    -0.38281027     0.00000000    -0.00000002    -0.26338746    -0.00000001     0.00000005
  nbf  21    -0.00000281    -0.00000169     1.08737741     0.00001361     0.00000242     0.59831052    -0.00000036     0.00000064
  nbf  22     0.00000386     0.00000216     0.00000592     0.01871432    -0.54201615     0.00000031     0.00000097    -0.00000149
  nbf  23     0.00000114     0.00000553     0.54855336    -0.00002641     0.00000190    -0.34234203    -0.00000283     0.00000120
  nbf  24     0.00000378     0.07854804    -0.00000217    -0.00000058    -0.00000116    -0.00000014     0.08085164     0.00000198

               nbf  17        nbf  18        nbf  19        nbf  20        nbf  21        nbf  22        nbf  23        nbf  24
  nbf  17    -3.38766582
  nbf  18    -0.05579364    -3.03518476
  nbf  19     0.00000616    -0.00000758    -2.89463041
  nbf  20     0.00966315     0.05949009    -0.00000026   -33.05210088
  nbf  21    -0.01753957    -0.18425984     0.00000145     0.58206100    -7.87299877
  nbf  22    -0.00000611     0.00000103    -0.45830998    -0.00000013    -0.00000960    -6.71596821
  nbf  23    -0.12232085     0.30565398    -0.00000427     0.18750690    -0.25170813     0.00001433    -6.95612872
  nbf  24    -0.00000090     0.00000142    -0.00000008    -0.00000041    -0.00000471     0.00000238    -0.00000114    -7.11723257
 insert_onel_diag: nmin2=   380

 onel.diag.
 -0.331800E+01-0.309059E+01-0.410174E+01-0.407781E+01-0.475246E+01-0.534890E+01-0.433719E+01-0.358727E+01-0.342222E+01-0.317751E+01
 -0.403663E+01-0.263219E+01-0.362260E+01-0.366396E+01-0.367018E+01-0.363974E+01-0.338767E+01-0.303518E+01-0.289463E+01-0.330521E+02
 -0.787300E+01-0.671597E+01-0.695613E+01-0.711723E+01

 i,start,strti,fin,filind,bufszi,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095         1       380       380

 4 external diag. modified integrals

  0.299711954 -3.31800299  0.362025844  0.213403987  0.294044049 -3.09058818
  0.345182252  0.318068021  0.34837146  0.293493931  0.497019376 -4.10173886
  0.382867967  0.309072536  0.341195502  0.316765031  0.62103433  0.362850338
  0.531836961 -4.07781289  0.349706612  0.299527054  0.308820027  0.298754537
  0.450260207  0.419223806  0.448476864  0.401601457  0.518535861 -4.75246258
  0.349531774  0.304524501  0.31278089  0.302655019  0.429312411  0.416826853
  0.436921903  0.413400849  0.530775438  0.48280831  0.60679891 -5.34889706
  0.33535564  0.293407704  0.341034277  0.276576801  0.455225842  0.389302194
  0.443351075  0.383054684  0.486303633  0.428587076  0.499742288  0.459023099
  0.475434104 -4.33719088  0.342526573  0.315362897  0.319917749  0.300731659
  0.504389096  0.410870033  0.47482656  0.425010382  0.519270202  0.400100848
  0.453053263  0.422413645  0.446074502  0.406547185  0.476405093 -3.58727342
  0.361060191  0.337659006  0.340604687  0.320198588  0.504515292  0.452245513
  0.527488651  0.469039301  0.434901697  0.420706511  0.454829088  0.417262113
  0.426620519  0.400804422  0.469386808  0.441599006  0.555764969 -3.42221814
  0.348856386  0.329501947  0.326880472  0.312634683  0.490451682  0.436629022
  0.509180653  0.444435002  0.4400708  0.419071921  0.459403052  0.417691971
  0.415311401  0.398901525  0.466192986  0.43088403  0.694750225  0.346946891
  0.501631969 -3.17751229  0.343227876  0.31280524  0.321132402  0.300850073
  0.455432102  0.418963752  0.451109274  0.419591702  0.534502331  0.438245352
  0.597905727  0.464004009  0.508095073  0.408470727  0.460615989  0.424227034
  0.469521607  0.441123033  0.462583019  0.438238584  0.525774157 -4.03662508
  0.356976431  0.333496982  0.334773679  0.315431831  0.522936179  0.430785245
  0.55704189  0.430105919  0.46546176  0.385379923  0.398185515  0.390667038
  0.424333725  0.380522726  0.592974188  0.354449455  0.516100041  0.471421051
  0.491982029  0.453400696  0.428233148  0.409923576  0.53092711 -2.63219205
  0.370351482  0.34231438  0.377526483  0.320334123  0.57663332  0.44254372
  0.563518308  0.477512966  0.511232086  0.469131823  0.508472502  0.490097443
  0.529535469  0.421869959  0.499545867  0.467575397  0.539123293  0.488956188
  0.512741012  0.475743005  0.515621907  0.477676678  0.512163042  0.474196449
  0.592144122 -3.62259957  0.388203856  0.340302294  0.365601016  0.328125926
  0.536368018  0.473948976  0.615526601  0.455378076  0.520967947  0.463416877
  0.546457699  0.489981129  0.48581785  0.455063687  0.509165453  0.46499119
  0.535052608  0.504064949  0.518029026  0.481875024  0.516561248  0.489811629
  0.517714869  0.482829445  0.717425907  0.445100321  0.607835731 -3.66396498
  0.361030445  0.336028491  0.330453775  0.322428351  0.486741558  0.469418774
  0.514189842  0.450314389  0.575178104  0.539226259  0.630438312  0.577562728
  0.510895782  0.499926663  0.503939385  0.490371469  0.52041881  0.467839603
  0.554043059  0.445274854  0.591832537  0.558697254  0.453957255  0.445032703
  0.568738744  0.548069735  0.596778585  0.560916765  0.750165186 -3.67017627
  0.353015389  0.338054232  0.358115278  0.317291888  0.522721553  0.462375435
  0.489534774  0.469144866  0.543456123  0.532600344  0.632201055  0.577477753
  0.547273656  0.513624874  0.507616587  0.483570727  0.564197636  0.455577875
  0.524876517  0.46718588  0.585160178  0.562784851  0.458775281  0.447244069
  0.587371194  0.556188564  0.58998874  0.568912261  0.721594827  0.645098881
  0.755127757 -3.6397375  0.366037972  0.335416332  0.343455146  0.320873443
  0.50940609  0.476326958  0.527676719  0.456821536  0.589439367  0.537772005
  0.579789349  0.559672288  0.53269644  0.49854365  0.567546289  0.469810852
  0.495610134  0.480892777  0.493939617  0.479062411  0.584343602  0.533457424
  0.502525388  0.442000941  0.582221799  0.560044775  0.596954899  0.56140811
  0.713528596  0.656785472  0.69947109  0.625487345  0.729807063 -3.38766582
  0.353987843  0.337616982  0.351789554  0.321182817  0.550334788  0.456688986
  0.522995786  0.46803517  0.566057852  0.507622395  0.615570104  0.560439732
  0.539018372  0.503813066  0.510463413  0.481771744  0.500103129  0.48786777
  0.496692123  0.477703001  0.585735173  0.540040646  0.469784387  0.453592071
  0.609978757  0.53850005  0.643846451  0.534124473  0.686162365  0.635949394
  0.700906311  0.666564985  0.679632027  0.617528828  0.696112735 -3.03518476
  0.358856446  0.335907395  0.354506012  0.319503646  0.548011068  0.469120392
  0.531509325  0.460743131  0.577501451  0.534350115  0.577624788  0.562928162
  0.587296462  0.4866605  0.514004213  0.4958057  0.493523583  0.479933652
  0.487740555  0.474497517  0.570979461  0.537487177  0.471960345  0.452931079
  0.634007692  0.54138526  0.615254278  0.550385293  0.691778305  0.63576614
  0.699509207  0.640665078  0.699234432  0.657364716  0.711993958  0.616407541
  0.721280492 -2.89463041
 i,start,strti,fin,filind,bufszi,nmbuf,nmin2, nd4ext = 
         1         1         2         2        12      4095         1       380       380

 all internall diag. modified integrals

  4.73971342 -33.0521009  1.04569563  0.0636788604  0.755179462 -7.87299877
  0.878477817  0.0193493095  0.682719812  0.155759544  0.668673763 -6.71596821
  0.986988872  0.0301374391  0.681192227  0.125736446  0.608457144
  0.0402117459  0.732528817 -6.95612872  1.03831833  0.0301199817  0.713926708
  0.128455235  0.624391224  0.0304166993  0.665957609  0.0433645062
  0.760228983 -7.11723257
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331800E+01-0.309059E+01-0.410174E+01-0.407781E+01-0.475246E+01-0.534890E+01-0.433719E+01-0.358727E+01-0.342222E+01-0.317751E+01
 -0.403663E+01-0.263219E+01-0.362260E+01-0.366396E+01-0.367018E+01-0.363974E+01-0.338767E+01-0.303518E+01-0.289463E+01-0.330521E+02
 -0.787300E+01-0.671597E+01-0.695613E+01-0.711723E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331800E+01-0.309059E+01-0.410174E+01-0.407781E+01-0.475246E+01-0.534890E+01-0.433719E+01-0.358727E+01-0.342222E+01-0.317751E+01
 -0.403663E+01-0.263219E+01-0.362260E+01-0.366396E+01-0.367018E+01-0.363974E+01-0.338767E+01-0.303518E+01-0.289463E+01-0.330521E+02
 -0.787300E+01-0.671597E+01-0.695613E+01-0.711723E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331800E+01-0.309059E+01-0.410174E+01-0.407781E+01-0.475246E+01-0.534890E+01-0.433719E+01-0.358727E+01-0.342222E+01-0.317751E+01
 -0.403663E+01-0.263219E+01-0.362260E+01-0.366396E+01-0.367018E+01-0.363974E+01-0.338767E+01-0.303518E+01-0.289463E+01-0.330521E+02
 -0.787300E+01-0.671597E+01-0.695613E+01-0.711723E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380
 DIAGEL COUNTS: DG0X,DG2X,DG4X= 440 145 30
 =========== Executing IN-CORE method ==========
 norm multnew:  1.
 threx: total number of loops= 14
 threx: total number of loops= 24
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331800E+01-0.309059E+01-0.410174E+01-0.407781E+01-0.475246E+01-0.534890E+01-0.433719E+01-0.358727E+01-0.342222E+01-0.317751E+01
 -0.403663E+01-0.263219E+01-0.362260E+01-0.366396E+01-0.367018E+01-0.363974E+01-0.338767E+01-0.303518E+01-0.289463E+01-0.330521E+02
 -0.787300E+01-0.671597E+01-0.695613E+01-0.711723E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331800E+01-0.309059E+01-0.410174E+01-0.407781E+01-0.475246E+01-0.534890E+01-0.433719E+01-0.358727E+01-0.342222E+01-0.317751E+01
 -0.403663E+01-0.263219E+01-0.362260E+01-0.366396E+01-0.367018E+01-0.363974E+01-0.338767E+01-0.303518E+01-0.289463E+01-0.330521E+02
 -0.787300E+01-0.671597E+01-0.695613E+01-0.711723E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331800E+01-0.309059E+01-0.410174E+01-0.407781E+01-0.475246E+01-0.534890E+01-0.433719E+01-0.358727E+01-0.342222E+01-0.317751E+01
 -0.403663E+01-0.263219E+01-0.362260E+01-0.366396E+01-0.367018E+01-0.363974E+01-0.338767E+01-0.303518E+01-0.289463E+01-0.330521E+02
 -0.787300E+01-0.671597E+01-0.695613E+01-0.711723E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        40 xx:       135 ww:       255
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        20 yw:        25

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        39
task #   9:       127    task #  10:       247    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 matrix-vector product formed for vector   1
 =========== Executing IN-CORE method ==========
 norm multnew:  6.93526479E-05
 threx: total number of loops= 14
 threx: total number of loops= 24
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331800E+01-0.309059E+01-0.410174E+01-0.407781E+01-0.475246E+01-0.534890E+01-0.433719E+01-0.358727E+01-0.342222E+01-0.317751E+01
 -0.403663E+01-0.263219E+01-0.362260E+01-0.366396E+01-0.367018E+01-0.363974E+01-0.338767E+01-0.303518E+01-0.289463E+01-0.330521E+02
 -0.787300E+01-0.671597E+01-0.695613E+01-0.711723E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331800E+01-0.309059E+01-0.410174E+01-0.407781E+01-0.475246E+01-0.534890E+01-0.433719E+01-0.358727E+01-0.342222E+01-0.317751E+01
 -0.403663E+01-0.263219E+01-0.362260E+01-0.366396E+01-0.367018E+01-0.363974E+01-0.338767E+01-0.303518E+01-0.289463E+01-0.330521E+02
 -0.787300E+01-0.671597E+01-0.695613E+01-0.711723E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331800E+01-0.309059E+01-0.410174E+01-0.407781E+01-0.475246E+01-0.534890E+01-0.433719E+01-0.358727E+01-0.342222E+01-0.317751E+01
 -0.403663E+01-0.263219E+01-0.362260E+01-0.366396E+01-0.367018E+01-0.363974E+01-0.338767E+01-0.303518E+01-0.289463E+01-0.330521E+02
 -0.787300E+01-0.671597E+01-0.695613E+01-0.711723E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        40 xx:       135 ww:       255
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        20 yw:        25

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        39
task #   9:       127    task #  10:       247    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 matrix-vector product formed for vector   2
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference-subspace vector overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97547070     0.00689278

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97594644     0.11671891
 subspace has dimension  2

          (h-repnuc*1) matrix in the subspace basis

                x:   1         x:   2
   x:   1   -85.60696487
   x:   2    -0.54868494    -0.00583296

          overlap matrix in the subspace basis

                x:   1         x:   2
   x:   1     1.00000000
   x:   2     0.00640842     0.00006935

          eigenvectors and eigenvalues in the subspace basis

                v:   1         v:   2

   energy   -85.60702460   -81.89014907

   x:   1     0.99516155    -1.20896568
   x:   2     0.75376039   188.02703927

          eigenvectors and eigenvalues in the schmidt orthonormalized basis

                v:   1         v:   2

   energy   -85.60702460   -81.89014907

  zx:   1     0.99999196    -0.00400875
  zx:   2     0.00400875     0.99999196

          <ref|baseci> overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97547070     0.00689278

 trial vector basis is being transformed.  new dimension:   1

          transformed tciref transformation matrix  block   1

               ref   1
  ci:   1     0.97594644

  write the transformed vectors back to cosmofile

 rtflw =  0
 freezing =  F

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1    -76.2536306957 -9.3527E+00  2.3011E-05  7.1128E-03  1.0000E-03
 mr-sdci #  5  2    -72.5367551608 -1.0294E+01  0.0000E+00  1.9566E+00  1.0000E-04


 total energy in cosmo calc 
e(rootcalc) + repnuc=     -76.2536306957
dielectric energy =      -0.0116500585
deltaediel =       0.0116500585
e(rootcalc) + repnuc - ediel =     -76.2419806372
e(rootcalc) + repnuc - edielnew =     -76.2536306957
deltaelast =      76.2419806372

 root number  1 is used to define the new expansion vector.
 diagon: frcsub=2, new matrix-vector products will be computed.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.02   0.00   0.00   0.01         0.    0.0000
    2   25    0   0.01   0.00   0.00   0.01         0.    0.0000
    3   26    0   0.02   0.00   0.00   0.02         0.    0.0000
    4   11    0   0.01   0.00   0.00   0.00         0.    0.0000
    5   15    0   0.02   0.00   0.00   0.01         0.    0.0000
    6   16    0   0.02   0.00   0.00   0.01         0.    0.0000
    7    1    0   0.01   0.00   0.00   0.00         0.    0.0000
    8    5    0   0.01   0.00   0.00   0.01         0.    0.0000
    9    6    0   0.03   0.00   0.00   0.03         0.    0.0000
   10    7    0   0.03   0.00   0.00   0.03         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.02   0.00   0.00   0.01         0.    0.0000
   14   47    0   0.02   0.00   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.009998
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.2200s 
time spent in multnx:                   0.1600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            3.7700s 

          starting ci iteration   6

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.35339391

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=      10  D0X=      30  D0W=      50
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================

          ci-one-electron density block   1

                mo   1         mo   2         mo   3         mo   4         mo   5         mo   6         mo   7         mo   8
   mo   1     1.99991452
   mo   2    -0.00020719     1.98540517
   mo   3     0.00000000     0.00000000     1.97020565
   mo   4    -0.00015564     0.00399940    -0.00000013     1.97365890
   mo   5     0.00000000    -0.00000006    -0.00000001    -0.00000008     1.97648543
   mo   6     0.00012937    -0.00325748    -0.00001887    -0.02061282    -0.00000797     0.00677798
   mo   7     0.00000000     0.00000743    -0.01045713    -0.00002221    -0.00000477    -0.00000003     0.00734855
   mo   8    -0.00000001     0.00001532    -0.01296321    -0.00002090     0.00000233    -0.00000015     0.00701349     0.01041869
   mo   9     0.00006353     0.00341447    -0.00001159    -0.00061342     0.00000190     0.00691099    -0.00000020    -0.00000031
   mo  10    -0.00022001     0.01024904    -0.00001367     0.00356963     0.00000012     0.00257582    -0.00000018    -0.00000012
   mo  11     0.00000004    -0.00000172     0.00000292    -0.00000043    -0.01170515     0.00000021     0.00000009    -0.00000009
   mo  12    -0.00000001    -0.00000286    -0.00539446    -0.00000582    -0.00000243     0.00000006     0.00599811     0.00397156
   mo  13     0.00020479    -0.00102129    -0.00001239    -0.00822664    -0.00000303     0.00191125    -0.00000003    -0.00000016
   mo  14     0.00000000     0.00000210    -0.00000318    -0.00000113    -0.00000175    -0.00000003    -0.00000002    -0.00000002
   mo  15     0.00000000     0.00000108    -0.00000187    -0.00000485    -0.00349361     0.00000002    -0.00000001    -0.00000001
   mo  16    -0.00038818    -0.00013845    -0.00000459     0.00274245     0.00000169    -0.00190666    -0.00000010    -0.00000004
   mo  17    -0.00000001    -0.00000881     0.00099980     0.00002121     0.00000048    -0.00000003     0.00026538     0.00023833
   mo  18     0.00000004    -0.00000061     0.01136489    -0.00000058     0.00000088    -0.00000011    -0.00281328    -0.00433462
   mo  19    -0.00010647    -0.00436309    -0.00000011     0.00745309     0.00000041    -0.00305067    -0.00000013    -0.00000008
   mo  20     0.00000000     0.00000021    -0.00000048     0.00000137     0.00205332    -0.00000003     0.00000000     0.00000000
   mo  21     0.00000000    -0.00000023     0.00000058    -0.00000060    -0.00000126     0.00000001    -0.00000001    -0.00000002
   mo  22     0.00001065     0.00013080     0.00000258    -0.00122111     0.00000029    -0.00005347     0.00000003     0.00000003
   mo  23     0.00004940     0.00172558    -0.00000044     0.00023881    -0.00000048     0.00061805     0.00000000    -0.00000002
   mo  24    -0.00000001    -0.00000068    -0.00026822     0.00000185     0.00000009    -0.00000001    -0.00080760     0.00036058

                mo   9         mo  10         mo  11         mo  12         mo  13         mo  14         mo  15         mo  16
   mo   9     0.00989188
   mo  10     0.00326512     0.00998607
   mo  11    -0.00000006    -0.00000002     0.01432572
   mo  12     0.00000003     0.00000004     0.00000002     0.00643048
   mo  13     0.00050070    -0.00063618     0.00000006    -0.00000002     0.00370026
   mo  14    -0.00000003     0.00000000     0.00000008    -0.00000002     0.00000000     0.00295508
   mo  15    -0.00000003    -0.00000006    -0.00136256    -0.00000001     0.00000002    -0.00000005     0.00222869
   mo  16     0.00004053     0.00260975    -0.00000003    -0.00000007    -0.00122735     0.00000001    -0.00000002     0.00433929
   mo  17     0.00000018     0.00000026    -0.00000001     0.00035187    -0.00000003     0.00000000     0.00000000     0.00000006
   mo  18    -0.00000014    -0.00000016     0.00000001    -0.00180458     0.00000001     0.00000000     0.00000000    -0.00000003
   mo  19    -0.00430294    -0.00080633     0.00000000    -0.00000012    -0.00082613     0.00000000    -0.00000001    -0.00006628
   mo  20    -0.00000001     0.00000000     0.00004869     0.00000000    -0.00000001     0.00000000    -0.00181604     0.00000000
   mo  21     0.00000000    -0.00000001     0.00000002     0.00000000     0.00000000    -0.00201138     0.00000000    -0.00000001
   mo  22    -0.00055334     0.00030110    -0.00000001     0.00000002     0.00171321     0.00000000     0.00000000     0.00070090
   mo  23     0.00022211     0.00176115     0.00000000     0.00000003     0.00024498     0.00000000     0.00000000    -0.00068045
   mo  24     0.00000001     0.00000000     0.00000001    -0.00166497     0.00000001     0.00000000     0.00000000     0.00000002

                mo  17         mo  18         mo  19         mo  20         mo  21         mo  22         mo  23         mo  24
   mo  17     0.00051153
   mo  18    -0.00020081     0.00259173
   mo  19     0.00000002     0.00000005     0.00285029
   mo  20     0.00000000     0.00000000     0.00000001     0.00253801
   mo  21     0.00000000     0.00000001     0.00000000     0.00000001     0.00241042
   mo  22    -0.00000001     0.00000001    -0.00016269     0.00000000     0.00000000     0.00196264
   mo  23     0.00000003    -0.00000002     0.00061416     0.00000000     0.00000000    -0.00008750     0.00162691
   mo  24     0.00000431    -0.00057546     0.00000002     0.00000000     0.00000000    -0.00000001    -0.00000001     0.00143611


*****   symmetry block SYM1   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       1.99991452
       2      -0.00020719     1.98540517
       3       0.00000000     0.00000000     1.97020565
       4      -0.00015564     0.00399940    -0.00000013     1.97365890
       5       0.00000000    -0.00000006    -0.00000001    -0.00000008
       6       0.00012937    -0.00325748    -0.00001887    -0.02061282
       7       0.00000000     0.00000743    -0.01045713    -0.00002221
       8      -0.00000001     0.00001532    -0.01296321    -0.00002090
       9       0.00006353     0.00341447    -0.00001159    -0.00061342
      10      -0.00022001     0.01024904    -0.00001367     0.00356963
      11       0.00000004    -0.00000172     0.00000292    -0.00000043
      12      -0.00000001    -0.00000286    -0.00539446    -0.00000582
      13       0.00020479    -0.00102129    -0.00001239    -0.00822664
      14       0.00000000     0.00000210    -0.00000318    -0.00000113
      15       0.00000000     0.00000108    -0.00000187    -0.00000485
      16      -0.00038818    -0.00013845    -0.00000459     0.00274245
      17      -0.00000001    -0.00000881     0.00099980     0.00002121
      18       0.00000004    -0.00000061     0.01136489    -0.00000058
      19      -0.00010647    -0.00436309    -0.00000011     0.00745309
      20       0.00000000     0.00000021    -0.00000048     0.00000137
      21       0.00000000    -0.00000023     0.00000058    -0.00000060
      22       0.00001065     0.00013080     0.00000258    -0.00122111
      23       0.00004940     0.00172558    -0.00000044     0.00023881
      24      -0.00000001    -0.00000068    -0.00026822     0.00000185

               Column   5     Column   6     Column   7     Column   8
       5       1.97648543
       6      -0.00000797     0.00677798
       7      -0.00000477    -0.00000003     0.00734855
       8       0.00000233    -0.00000015     0.00701349     0.01041869
       9       0.00000190     0.00691099    -0.00000020    -0.00000031
      10       0.00000012     0.00257582    -0.00000018    -0.00000012
      11      -0.01170515     0.00000021     0.00000009    -0.00000009
      12      -0.00000243     0.00000006     0.00599811     0.00397156
      13      -0.00000303     0.00191125    -0.00000003    -0.00000016
      14      -0.00000175    -0.00000003    -0.00000002    -0.00000002
      15      -0.00349361     0.00000002    -0.00000001    -0.00000001
      16       0.00000169    -0.00190666    -0.00000010    -0.00000004
      17       0.00000048    -0.00000003     0.00026538     0.00023833
      18       0.00000088    -0.00000011    -0.00281328    -0.00433462
      19       0.00000041    -0.00305067    -0.00000013    -0.00000008
      20       0.00205332    -0.00000003     0.00000000     0.00000000
      21      -0.00000126     0.00000001    -0.00000001    -0.00000002
      22       0.00000029    -0.00005347     0.00000003     0.00000003
      23      -0.00000048     0.00061805     0.00000000    -0.00000002
      24       0.00000009    -0.00000001    -0.00080760     0.00036058

               Column   9     Column  10     Column  11     Column  12
       9       0.00989188
      10       0.00326512     0.00998607
      11      -0.00000006    -0.00000002     0.01432572
      12       0.00000003     0.00000004     0.00000002     0.00643048
      13       0.00050070    -0.00063618     0.00000006    -0.00000002
      14      -0.00000003     0.00000000     0.00000008    -0.00000002
      15      -0.00000003    -0.00000006    -0.00136256    -0.00000001
      16       0.00004053     0.00260975    -0.00000003    -0.00000007
      17       0.00000018     0.00000026    -0.00000001     0.00035187
      18      -0.00000014    -0.00000016     0.00000001    -0.00180458
      19      -0.00430294    -0.00080633     0.00000000    -0.00000012
      20      -0.00000001     0.00000000     0.00004869     0.00000000
      21       0.00000000    -0.00000001     0.00000002     0.00000000
      22      -0.00055334     0.00030110    -0.00000001     0.00000002
      23       0.00022211     0.00176115     0.00000000     0.00000003
      24       0.00000001     0.00000000     0.00000001    -0.00166497

               Column  13     Column  14     Column  15     Column  16
      13       0.00370026
      14       0.00000000     0.00295508
      15       0.00000002    -0.00000005     0.00222869
      16      -0.00122735     0.00000001    -0.00000002     0.00433929
      17      -0.00000003     0.00000000     0.00000000     0.00000006
      18       0.00000001     0.00000000     0.00000000    -0.00000003
      19      -0.00082613     0.00000000    -0.00000001    -0.00006628
      20      -0.00000001     0.00000000    -0.00181604     0.00000000
      21       0.00000000    -0.00201138     0.00000000    -0.00000001
      22       0.00171321     0.00000000     0.00000000     0.00070090
      23       0.00024498     0.00000000     0.00000000    -0.00068045
      24       0.00000001     0.00000000     0.00000000     0.00000002

               Column  17     Column  18     Column  19     Column  20
      17       0.00051153
      18      -0.00020081     0.00259173
      19       0.00000002     0.00000005     0.00285029
      20       0.00000000     0.00000000     0.00000001     0.00253801
      21       0.00000000     0.00000001     0.00000000     0.00000001
      22      -0.00000001     0.00000001    -0.00016269     0.00000000
      23       0.00000003    -0.00000002     0.00061416     0.00000000
      24       0.00000431    -0.00057546     0.00000002     0.00000000

               Column  21     Column  22     Column  23     Column  24
      21       0.00241042
      22       0.00000000     0.00196264
      23       0.00000000    -0.00008750     0.00162691
      24       0.00000000    -0.00000001    -0.00000001     0.00143611

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7        eno   8
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    8   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno   9        eno  10        eno  11        eno  12        eno  13        eno  14        eno  15        eno  16
 emo    9   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   10   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   11   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   12   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   13   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   14   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   15   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   16   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno  17        eno  18        eno  19        eno  20        eno  21        eno  22        eno  23        eno  24
 emo   17   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   18   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   19   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   20   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   21   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   22   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   23   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   24   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   1.9999196      1.9867499      1.9765635      1.9726755      1.9704295
   0.0210709      0.0193068      0.0144189      0.0101687      0.0053907
   0.0051274      0.0047125      0.0041954      0.0041062      0.0010057
   0.0010045      0.0006530      0.0005668      0.0004896      0.0004892
   0.0004457      0.0004276      0.0000489      0.0000335

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7         no   8
  mo    1   0.999819E+00   0.187172E-01  -0.622560E-07   0.327590E-02  -0.624192E-06   0.283223E-07  -0.363663E-04  -0.226635E-07
  mo    2  -0.168654E-01   0.953554E+00  -0.947328E-06  -0.300684E+00   0.597030E-04  -0.537321E-05  -0.310212E-02   0.103548E-05
  mo    3   0.102806E-06  -0.439379E-05   0.158636E-06   0.184273E-03   0.999943E+00   0.100742E-01   0.163637E-04  -0.162705E-05
  mo    4  -0.874912E-02   0.300540E+00   0.110271E-04   0.953652E+00  -0.174678E-03   0.116991E-04   0.653316E-02   0.533960E-09
  mo    5   0.144187E-06  -0.243956E-05   0.999980E+00  -0.108524E-04  -0.160547E-06   0.130439E-05   0.175771E-05   0.569336E-02
  mo    6   0.183104E-03  -0.468412E-02  -0.416212E-05  -0.951537E-02  -0.790611E-05  -0.211047E-03   0.532225E+00   0.515602E-05
  mo    7   0.323310E-07   0.236777E-06  -0.242214E-05  -0.129506E-04  -0.536704E-02   0.569111E+00   0.211263E-03   0.388178E-05
  mo    8  -0.444336E-07   0.424155E-05   0.117306E-05  -0.137723E-04  -0.665110E-02   0.636644E+00   0.225692E-03  -0.768223E-05
  mo    9   0.610157E-05   0.154883E-02   0.945376E-06  -0.864464E-03  -0.579318E-05  -0.252190E-03   0.671748E+00  -0.131153E-04
  mo   10  -0.213141E-03   0.548322E-02   0.734961E-07   0.151060E-03  -0.700121E-05  -0.155384E-03   0.411715E+00  -0.124498E-04
  mo   11   0.355056E-07  -0.881675E-06  -0.596383E-02   0.122524E-06   0.149673E-05   0.245899E-05   0.114505E-04   0.992970E+00
  mo   12   0.422618E-07  -0.224004E-05  -0.123909E-05  -0.295927E-05  -0.278148E-02   0.440966E+00   0.186613E-03   0.105854E-05
  mo   13   0.147657E-03  -0.174164E-02  -0.158687E-05  -0.384089E-02  -0.560522E-05  -0.491358E-04   0.825845E-01   0.520823E-05
  mo   14  -0.130945E-07   0.840062E-06  -0.884963E-06  -0.869916E-06  -0.161821E-05  -0.353750E-05  -0.287257E-05   0.679224E-05
  mo   15   0.110749E-07  -0.209753E-06  -0.176632E-02  -0.249454E-05  -0.952211E-06  -0.204747E-05  -0.489769E-05  -0.116015E+00
  mo   16  -0.205879E-03   0.358028E-03   0.875710E-06   0.136069E-02  -0.258146E-05  -0.369646E-05  -0.139450E-02  -0.679259E-05
  mo   17  -0.249604E-07  -0.102424E-05   0.244671E-06   0.116876E-04   0.504888E-03   0.254096E-01   0.310598E-04  -0.623768E-06
  mo   18   0.273738E-07  -0.411939E-06   0.448981E-06   0.927699E-06   0.579985E-02  -0.271836E+00  -0.117040E-03   0.165534E-05
  mo   19  -0.493695E-04  -0.966434E-03   0.255773E-06   0.429222E-02  -0.819575E-06   0.984078E-04  -0.292581E+00   0.379784E-05
  mo   20  -0.657213E-08   0.304812E-06   0.104163E-02   0.623150E-06  -0.245571E-06  -0.460379E-07  -0.229683E-06   0.227865E-01
  mo   21   0.574315E-08  -0.202256E-06  -0.638365E-06  -0.253040E-06   0.297332E-06  -0.329865E-06   0.152897E-06   0.288742E-06
  mo   22   0.958999E-05  -0.122770E-03   0.138210E-06  -0.613528E-03   0.141646E-05   0.326825E-05  -0.585935E-02  -0.836012E-07
  mo   23   0.904851E-05   0.868454E-03  -0.245673E-06  -0.150140E-03  -0.204278E-06  -0.211957E-04   0.589053E-01  -0.128864E-05
  mo   24  -0.696380E-08  -0.421211E-07   0.452513E-07   0.980125E-06  -0.134573E-03  -0.412744E-01  -0.181436E-04  -0.174687E-06

                no   9         no  10         no  11         no  12         no  13         no  14         no  15         no  16
  mo    1   0.228784E-03  -0.306303E-07  -0.983117E-04  -0.117742E-08   0.441710E-04   0.311227E-09  -0.130238E-06   0.187668E-04
  mo    2  -0.393257E-02   0.426020E-05  -0.295834E-03  -0.873029E-06   0.356707E-03   0.226291E-06  -0.106996E-04   0.956399E-03
  mo    3   0.131236E-05  -0.237795E-02   0.536428E-05   0.141011E-05   0.147528E-05  -0.315183E-06  -0.144157E-02  -0.828946E-05
  mo    4  -0.643658E-02  -0.232784E-06   0.382087E-02   0.350493E-06   0.145139E-02  -0.211899E-05   0.222054E-04  -0.252429E-02
  mo    5  -0.166581E-05   0.182079E-05   0.229200E-05   0.137950E-06  -0.376354E-06  -0.249514E-02  -0.907569E-06  -0.993032E-06
  mo    6  -0.257407E+00   0.393116E-04   0.190694E+00   0.272004E-05  -0.126196E+00  -0.343784E-05   0.784545E-03  -0.133541E+00
  mo    7  -0.108688E-04   0.220000E+00  -0.607729E-04   0.316063E-05   0.507373E-05  -0.582395E-06   0.396430E-01   0.246544E-03
  mo    8   0.572581E-05  -0.545550E+00   0.166236E-03  -0.414220E-05   0.495751E-05  -0.998689E-06   0.402129E+00   0.236889E-02
  mo    9  -0.175025E+00  -0.120542E-03  -0.364076E+00  -0.281803E-05   0.463588E-01   0.743877E-06  -0.236232E-02   0.401902E+00
  mo   10   0.766221E+00   0.949065E-04   0.315737E+00   0.170925E-05  -0.144664E+00  -0.661646E-05   0.169547E-02  -0.284305E+00
  mo   11   0.119392E-04  -0.609299E-05  -0.779321E-05  -0.814927E-05   0.346118E-05  -0.935987E-01   0.189466E-05   0.324528E-05
  mo   12  -0.596389E-05   0.639605E+00  -0.179005E-03   0.845917E-05  -0.235863E-05   0.376417E-06  -0.289677E+00  -0.171137E-02
  mo   13  -0.253760E+00   0.181594E-03   0.607435E+00   0.184207E-04   0.446432E+00   0.903866E-05  -0.364229E-03   0.573513E-01
  mo   14   0.320572E-05  -0.831892E-05  -0.132102E-04   0.753051E+00  -0.877112E-05  -0.166867E-04  -0.588379E-05   0.431202E-07
  mo   15  -0.620502E-05   0.157043E-05   0.128913E-06  -0.239797E-04   0.128310E-04  -0.653746E+00  -0.987807E-06  -0.457080E-06
  mo   16   0.466059E+00  -0.979969E-04  -0.363737E+00  -0.600234E-06   0.559638E+00   0.797216E-05  -0.169040E-02   0.282111E+00
  mo   17   0.124350E-04   0.189254E-01   0.106937E-04   0.219539E-05   0.181948E-05   0.412486E-06  -0.249197E+00  -0.149806E-02
  mo   18  -0.645264E-05   0.283635E+00  -0.948338E-04   0.267919E-05  -0.260024E-05   0.123962E-05   0.616204E+00   0.362963E-02
  mo   19   0.153849E+00   0.558979E-04   0.183991E+00  -0.151866E-05  -0.277175E+00  -0.645833E-05  -0.256968E-02   0.427684E+00
  mo   20   0.192673E-05  -0.152214E-05   0.198745E-06   0.168555E-04  -0.141308E-04   0.750899E+00  -0.188057E-05   0.154016E-05
  mo   21  -0.184642E-05   0.761314E-05   0.112555E-04  -0.657963E+00   0.969263E-05   0.251233E-04  -0.672878E-05   0.147109E-05
  mo   22   0.251989E-01   0.938157E-04   0.319317E+00   0.113526E-04   0.522018E+00   0.996618E-05  -0.748338E-03   0.135983E+00
  mo   23   0.100229E+00   0.908424E-04   0.307053E+00   0.168550E-05  -0.314915E+00  -0.765271E-05  -0.395063E-02   0.674900E+00
  mo   24   0.264295E-05  -0.405053E+00   0.123866E-03  -0.519649E-05   0.745137E-05  -0.144683E-05  -0.557643E+00  -0.328719E-02

                no  17         no  18         no  19         no  20         no  21         no  22         no  23         no  24
  mo    1  -0.159338E-08  -0.235502E-07   0.405445E-04   0.691117E-07   0.356960E-07  -0.350778E-04   0.254486E-04   0.471162E-08
  mo    2  -0.649074E-06  -0.335109E-05   0.242197E-02   0.375830E-05   0.496449E-05  -0.188945E-02   0.166524E-02  -0.737445E-07
  mo    3   0.863270E-06  -0.572976E-03   0.162398E-05   0.813503E-06  -0.160117E-02  -0.745812E-05   0.992546E-06   0.160512E-02
  mo    4   0.595921E-06   0.701023E-05   0.165835E-02   0.428635E-05  -0.557003E-05  -0.181641E-02   0.586680E-02   0.107051E-05
  mo    5   0.919653E-06   0.127710E-05  -0.796815E-06   0.106399E-02  -0.131012E-06  -0.125616E-05   0.224781E-05   0.115313E-05
  mo    6  -0.566209E-05  -0.148788E-03   0.464951E+00   0.808762E-03   0.283821E-03  -0.219242E+00   0.562134E+00   0.587770E-04
  mo    7  -0.168446E-04   0.562869E+00   0.981019E-04  -0.581926E-05   0.189321E+00   0.341424E-04  -0.511142E-04   0.522923E+00
  mo    8   0.123000E-04  -0.185889E+00  -0.614056E-04   0.148016E-05   0.528839E-01   0.240725E-04   0.290605E-04  -0.312970E+00
  mo    9   0.144805E-05   0.602478E-05  -0.820125E-02  -0.145774E-04  -0.845699E-05   0.247455E-01  -0.470326E+00  -0.525623E-04
  mo   10   0.187378E-05   0.413883E-04  -0.539709E-01  -0.907347E-04  -0.417870E-04   0.107319E-01  -0.197201E+00  -0.169669E-04
  mo   11  -0.131729E-04  -0.382813E-05  -0.129343E-03   0.722141E-01   0.112863E-05   0.309195E-05  -0.662348E-05  -0.321222E-05
  mo   12   0.332359E-05  -0.145492E+00   0.595758E-05   0.339211E-05  -0.125835E+00  -0.472407E-04   0.557876E-04  -0.524908E+00
  mo   13   0.628122E-05   0.227796E-04  -0.340491E+00  -0.596690E-03   0.227178E-03  -0.455881E+00  -0.182860E+00  -0.258839E-04
  mo   14   0.657963E+00   0.231804E-04   0.107196E-04   0.899253E-04  -0.456325E-05   0.224892E-05   0.158286E-05   0.112190E-05
  mo   15  -0.901165E-04   0.466629E-05  -0.129885E-02   0.747764E+00   0.141509E-04  -0.660605E-05  -0.258024E-05   0.176005E-06
  mo   16  -0.160109E-05  -0.617736E-04   0.208327E-01   0.349986E-04   0.248548E-03  -0.330655E+00   0.384615E+00   0.347319E-04
  mo   17   0.151813E-04  -0.384036E+00  -0.364832E-03  -0.150339E-04   0.885217E+00   0.720363E-03  -0.428686E-06   0.761668E-01
  mo   18  -0.559585E-05   0.383522E+00   0.416842E-06  -0.861602E-05   0.377706E+00   0.174006E-03   0.421178E-04  -0.419704E+00
  mo   19  -0.762986E-05  -0.224754E-03   0.573172E+00   0.990145E-03   0.481599E-03  -0.414401E+00  -0.311214E+00  -0.351947E-04
  mo   20  -0.906700E-04   0.454072E-05  -0.114615E-02   0.660022E+00   0.118986E-04  -0.657443E-05  -0.152897E-05  -0.136062E-06
  mo   21   0.753051E+00   0.242568E-04   0.101377E-04   0.916789E-04  -0.287211E-05   0.159730E-05   0.602481E-06   0.833216E-06
  mo   22  -0.926755E-05  -0.288068E-04   0.453990E+00   0.793820E-03  -0.324593E-03   0.628541E+00  -0.721153E-01  -0.195841E-06
  mo   23   0.388738E-05   0.129604E-03  -0.360316E+00  -0.622188E-03  -0.283902E-03   0.259319E+00   0.374769E+00   0.337432E-04
  mo   24  -0.230401E-04   0.576960E+00   0.109327E-03  -0.723453E-05   0.138754E+00   0.630910E-04   0.483566E-04  -0.413663E+00
  eigenvectors of nos in ao-basis, column    1
  0.1000602E+01 -0.1378790E-01 -0.1676727E-02  0.6175142E-07  0.1817432E-06 -0.7425139E-02  0.8827187E-07 -0.5391817E-07
  0.1509752E-03  0.7846357E-08 -0.1143679E-07  0.4253548E-04  0.1495253E-07  0.9052291E-04 -0.3263615E-02  0.2149164E-02
 -0.1014755E-02  0.3811672E-08 -0.9362137E-03 -0.3263733E-02  0.2149304E-02  0.1014774E-02  0.1740538E-07 -0.9362661E-03
  eigenvectors of nos in ao-basis, column    2
  0.9564087E-02  0.9132222E+00 -0.7199506E-01 -0.3081191E-05 -0.3468095E-05  0.1205093E+00 -0.4917415E-05  0.1336539E-05
  0.7729122E-01 -0.3251729E-06  0.4007764E-06 -0.1178387E-02 -0.2199010E-06  0.1115398E-02  0.2188466E+00 -0.1071008E+00
  0.2723104E-01  0.3798422E-06  0.2159290E-01  0.2188575E+00 -0.1071080E+00 -0.2723318E-01 -0.6189212E-06  0.2159121E-01
  eigenvectors of nos in ao-basis, column    3
 -0.7993125E-06 -0.2410566E-05  0.3058653E-05  0.2552899E-05  0.9105414E+00  0.9473027E-05 -0.2389646E-05  0.8281397E-01
  0.1773521E-05 -0.5704203E-06 -0.1690204E-01 -0.8874833E-07 -0.1326423E-06 -0.2532647E-06 -0.7062540E-05  0.9911663E-05
  0.7796948E-06  0.3057000E-01 -0.3267224E-06 -0.1977426E-05  0.5574255E-06 -0.3074483E-06  0.3057170E-01 -0.5263234E-06
  eigenvectors of nos in ao-basis, column    4
  0.1276789E-02  0.4143091E-02  0.2161921E+00  0.1477240E-03 -0.9627543E-05  0.8026427E+00 -0.2748122E-04  0.2945656E-06
 -0.1952453E-01 -0.1591840E-06  0.1248411E-05 -0.4717633E-02 -0.5130197E-05 -0.2419039E-02 -0.4280937E+00  0.1825919E+00
 -0.3799704E-01 -0.2704930E-05  0.2349641E-02 -0.4279163E+00  0.1825212E+00  0.3799882E-01 -0.1318151E-05  0.2380829E-02
  eigenvectors of nos in ao-basis, column    5
  0.3525058E-06  0.1404544E-05 -0.4650443E-04  0.7306872E+00  0.2024119E-05 -0.1349677E-03 -0.1178682E+00 -0.1888251E-05
 -0.1048523E-06  0.5274356E-06 -0.1082171E-06  0.7327957E-06 -0.2544515E-01  0.7819344E-06 -0.5518275E+00  0.1929435E+00
 -0.2041019E-01 -0.1828026E-05 -0.3130063E-01  0.5520078E+00 -0.1930148E+00 -0.2042676E-01  0.1662206E-06  0.3128840E-01
  eigenvectors of nos in ao-basis, column    6
 -0.3900058E-04 -0.3082638E-03  0.5411325E-04 -0.1307300E+01  0.4801231E-05  0.4583966E-03  0.5821411E+00 -0.2817237E-05
 -0.2648452E-03  0.1023332E-06  0.2444741E-06  0.1746496E-05 -0.6654009E-01  0.1139096E-04 -0.1004104E+01  0.3586931E+00
 -0.4026198E-01 -0.4095950E-05 -0.1804094E-01  0.1004829E+01 -0.3589934E+00 -0.4029551E-01  0.1010476E-05  0.1801380E-01
  eigenvectors of nos in ao-basis, column    7
  0.1128059E+00  0.8434522E+00 -0.1584697E+00 -0.5182402E-03  0.1808105E-04 -0.1216019E+01  0.2541813E-03 -0.1618331E-04
  0.7633809E+00  0.5342683E-06  0.4603279E-06 -0.4289225E-02 -0.2422311E-04 -0.3183762E-01 -0.9723659E+00  0.4146286E+00
 -0.5035094E-01 -0.5664379E-05 -0.9764942E-02 -0.9716129E+00  0.4143524E+00  0.5032663E-01 -0.1838507E-05 -0.9705106E-02
  eigenvectors of nos in ao-basis, column    8
  0.3851397E-05  0.1890028E-04 -0.2089758E-04  0.1051491E-05  0.1412802E+01  0.2509651E-04  0.2812025E-05 -0.1611431E+01
 -0.2490384E-04 -0.5660157E-06  0.3791921E-01  0.1599490E-06  0.1441872E-05  0.7488159E-06  0.3308665E-04 -0.3395066E-04
 -0.1580525E-06 -0.7456093E-01  0.1815678E-05  0.3424346E-05 -0.7088144E-06  0.1107109E-05 -0.7457047E-01  0.1335388E-05
  eigenvectors of nos in ao-basis, column    9
 -0.4408908E+00 -0.2258664E+01  0.2140934E+01  0.3505710E-05  0.1572732E-04 -0.7394432E+00 -0.1683859E-04 -0.1717215E-04
  0.1094592E+01 -0.2393148E-05  0.2984786E-05 -0.1303430E-01 -0.5478324E-05  0.6289398E-01  0.3833662E+00 -0.2138750E+00
 -0.1274227E-01 -0.2491641E-05  0.7623929E-01  0.3834070E+00 -0.2139195E+00  0.1274583E-01 -0.5483869E-05  0.7626128E-01
  eigenvectors of nos in ao-basis, column   10
  0.7892357E-04  0.3477166E-03 -0.4923693E-03 -0.4954613E+00 -0.7031459E-05 -0.4261763E-05  0.1039889E+01  0.9957217E-05
  0.5655068E-04  0.9235856E-05 -0.1849371E-05 -0.2941532E-04  0.6155034E+00  0.7814973E-04  0.6586836E+00 -0.5008149E+00
  0.1117638E+00 -0.2191214E-05 -0.2174984E-01 -0.6582890E+00  0.5005922E+00  0.1118175E+00  0.3422956E-05  0.2188534E-01
  eigenvectors of nos in ao-basis, column   11
  0.2901157E+00  0.1296490E+01 -0.1781205E+01  0.1250237E-03 -0.8886168E-05 -0.3907188E-01 -0.2958081E-03  0.1315862E-04
  0.2153027E+00  0.1377204E-04  0.1794060E-06 -0.9880500E-01 -0.1906250E-03  0.2633762E+00  0.6394697E+00 -0.3777318E+00
 -0.6824300E-01 -0.4565582E-05  0.2208418E+00  0.6398854E+00 -0.3780435E+00  0.6818824E-01  0.4985263E-05  0.2208549E+00
  eigenvectors of nos in ao-basis, column   12
  0.5220433E-05  0.2494848E-04 -0.2669199E-04 -0.8006070E-05 -0.9634126E-05  0.5977553E-06  0.1129601E-04  0.2256891E-04
 -0.6747418E-05 -0.8026001E+00  0.2149195E-04 -0.4156707E-05  0.7329676E-05  0.1783522E-05 -0.1183624E-05  0.5891720E-07
 -0.2260627E-05  0.2659835E+00  0.3573490E-05 -0.3869446E-05  0.3811106E-05  0.6131184E-05 -0.2660068E+00  0.7016994E-05
  eigenvectors of nos in ao-basis, column   13
 -0.1306837E+00 -0.5544660E+00  0.9039561E+00  0.1765474E-05  0.3572381E-05  0.9933606E-01 -0.3901774E-05 -0.8931579E-05
 -0.5138422E+00  0.1152394E-04 -0.1683591E-04 -0.2102779E+00 -0.1071707E-04 -0.2141871E+00 -0.4716088E+00  0.2712050E+00
 -0.1639842E+00  0.1828414E-05  0.9190873E-01 -0.4716110E+00  0.2712164E+00  0.1639904E+00  0.6478135E-05  0.9190647E-01
  eigenvectors of nos in ao-basis, column   14
  0.1537658E-06  0.7104016E-06  0.7036398E-06  0.5062298E-06 -0.8506140E-01  0.5225177E-05 -0.6225157E-06  0.3040563E+00
 -0.1324086E-04  0.2909043E-04  0.8901712E+00 -0.3979965E-05  0.2183700E-05 -0.5039069E-05 -0.5704099E-05  0.5729277E-05
 -0.2162015E-05 -0.1992663E+00  0.8674228E-06 -0.5041527E-05  0.4434305E-05  0.2683792E-05 -0.1992625E+00  0.1589446E-05
  eigenvectors of nos in ao-basis, column   15
  0.2132902E-02  0.8103515E-02 -0.1603140E-01  0.4945457E+00  0.1889172E-05 -0.7951487E-03 -0.1288228E+01 -0.2063583E-05
  0.6897280E-02 -0.6456615E-05 -0.1800342E-05  0.1607219E-03  0.8090637E+00 -0.2429937E-02 -0.1395634E+01  0.7247231E+00
 -0.7495807E-01 -0.8146661E-05  0.2563153E+00  0.1422184E+01 -0.7396612E+00 -0.7581467E-01  0.5057363E-05 -0.2545685E+00
  eigenvectors of nos in ao-basis, column   16
 -0.3565829E+00 -0.1351380E+01  0.2702644E+01  0.2910808E-02  0.3703696E-05  0.1215443E+00 -0.7580052E-02 -0.5766402E-05
 -0.1151839E+01  0.1571859E-05  0.1687584E-05 -0.2985612E-01  0.4770379E-02  0.4148427E+00 -0.2262006E+01  0.1272627E+01
 -0.7513312E-01  0.8506275E-06 -0.1580365E+00 -0.2245430E+01  0.1264055E+01  0.7420913E-01 -0.3298672E-06 -0.1610827E+00
  eigenvectors of nos in ao-basis, column   17
  0.3932901E-05  0.1878324E-04 -0.6234763E-05 -0.2433118E-04 -0.1235194E-04 -0.9703412E-05 -0.1916002E-04  0.1051635E-03
 -0.4121391E-05  0.7226589E+00 -0.8037042E-04  0.2724695E-05  0.2739838E-04  0.7652004E-06 -0.9657748E-04  0.7933915E-04
 -0.2100176E-04  0.7383997E+00 -0.2114319E-04  0.5377297E-04 -0.4711356E-04  0.3556147E-05 -0.7386125E+00  0.2303899E-04
  eigenvectors of nos in ao-basis, column   18
  0.1083861E-03  0.4930533E-03 -0.1798543E-03  0.8901485E+00 -0.4501750E-05 -0.2892044E-03  0.1332981E+00  0.2301183E-05
  0.1091329E-03  0.2302179E-04  0.3995853E-05  0.9435249E-05 -0.6583529E+00  0.5081575E-04  0.1762539E+01 -0.1623105E+01
  0.3803815E+00  0.3061521E-04  0.6977477E+00 -0.1763274E+01  0.1623675E+01  0.3808534E+00 -0.1968782E-04 -0.6980537E+00
  eigenvectors of nos in ao-basis, column   19
 -0.2055911E+00 -0.1009822E+01 -0.2022908E+00  0.5256746E-04 -0.1044726E-03  0.6881453E+00  0.4142017E-03  0.1360831E-02
  0.3703311E+00  0.9488463E-05 -0.9903690E-03 -0.1312939E+00 -0.1254394E-03 -0.1138541E+00  0.1598685E+01 -0.1106901E+01
  0.9063381E+00 -0.1457059E-02  0.1346127E+00  0.1597572E+01 -0.1106117E+01 -0.9065641E+00 -0.1479498E-02  0.1340222E+00
  eigenvectors of nos in ao-basis, column   20
 -0.3557499E-03 -0.1747554E-02 -0.3472216E-03 -0.1752589E-04  0.5753141E-01  0.1184992E-02  0.2144767E-04 -0.7796644E+00
  0.6556960E-03  0.8671257E-04  0.5703411E+00 -0.2295140E-03  0.8321949E-05 -0.1957795E-03  0.2764818E-02 -0.1910487E-02
  0.1555491E-02  0.8454769E+00  0.2275103E-03  0.2782427E-02 -0.1936858E-02 -0.1588996E-02  0.8452836E+00  0.2216886E-03
  eigenvectors of nos in ao-basis, column   21
 -0.2880988E-03 -0.1247463E-02  0.8531242E-03  0.6236443E+00  0.6101435E-06  0.5906249E-03 -0.1255135E+01 -0.1417331E-04
 -0.6709534E-03 -0.2491429E-05  0.1017814E-04  0.8849344E-04 -0.1512990E+00 -0.1452341E-03 -0.3019197E+00 -0.1161971E+00
  0.7498218E+00  0.1152547E-04 -0.3475580E+00  0.3025064E+00  0.1155428E+00  0.7493387E+00  0.1997746E-04  0.3488040E+00
  eigenvectors of nos in ao-basis, column   22
  0.3050121E+00  0.1275211E+01 -0.1207794E+01  0.2976417E-03  0.3598314E-05 -0.5131901E+00 -0.8180321E-03  0.7518910E-06
  0.1022909E+01  0.1422827E-05 -0.5809653E-05 -0.1735162E+00 -0.7954407E-04  0.1628138E+00  0.1835327E+00  0.4984385E-01
  0.4154336E-01 -0.5637697E-05 -0.8091549E+00  0.1842118E+00  0.4963472E-01 -0.4054310E-01 -0.9941714E-05 -0.8084081E+00
  eigenvectors of nos in ao-basis, column   23
 -0.3250908E+00 -0.1123188E+01  0.3942366E+01  0.3911611E-04 -0.7109276E-05 -0.3566760E+00  0.1656964E-03  0.1335381E-04
 -0.9558009E+00  0.4419723E-06 -0.1198589E-05  0.2186252E-01 -0.6519001E-04  0.1872636E+00 -0.1190982E+01 -0.4624933E+00
 -0.7572069E+00 -0.1260760E-05 -0.5042941E+00 -0.1191199E+01 -0.4627231E+00  0.7573684E+00 -0.3892367E-05 -0.5044218E+00
  eigenvectors of nos in ao-basis, column   24
 -0.2960774E-04 -0.1026391E-03  0.3739176E-03 -0.3599290E+00 -0.3481017E-05 -0.3825295E-04 -0.1591309E+01  0.5436044E-05
 -0.7578686E-04  0.7488102E-06 -0.1774502E-06  0.1774420E-06  0.5579039E+00  0.1707165E-04 -0.9442024E+00 -0.1209010E+01
 -0.7206567E+00  0.1186574E-05 -0.6513108E+00  0.9440198E+00  0.1208880E+01 -0.7205135E+00 -0.9882686E-06  0.6511969E+00


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***


 cosurf_and_phi
  2.466315385316 -0.061539892486  2.837440149000 -0.037434438001
  0.006359863210 -2.199287675687  3.114544749440 -0.066760908953
 -0.480526931872  1.061509344774  3.755560521090 -0.068578480725
  1.262739748330  2.872870034186  1.567866969040 -0.044582458689
  1.897949994968 -2.634366215369  0.570962659525 -0.015612212567
 -2.790166444607 -0.824331206483  2.170430257018 -0.021388015069
 -2.061296060778  2.493528161233  1.034251636268 -0.021678686787
 -0.002991703587  2.420846177436 -1.447622679829  0.014798918876
 -1.114435211431 -2.949559801670 -0.067898604227 -0.015649409675
  1.955709175590 -0.984462672186  3.123501329999 -0.049967950018
  1.000900154469 -1.706149424342  3.300403738319 -0.063435241054
  1.652947872482  0.360731428865  3.496571379926 -0.056815474720
  0.594940552015  0.670555958619  3.845537291721 -0.068298318762
  2.390078334690  1.202722009900  2.566708449184 -0.036671732060
  1.989334673621  2.229216060677  2.001028520754 -0.038784374524
  2.919143888229  0.378144896597  2.099775822683 -0.016292225642
  2.780367109803 -0.921046401167  2.130496470151 -0.021107465563
  2.449941849009 -2.011800733381  1.439000205618 -0.019258150215
 -0.231540571447 -1.263621120083  3.706953994699 -0.069187738277
 -0.352106389130 -0.107618694495  3.950679056227 -0.069521143717
  0.105380882100  1.878884201281  3.371423292662 -0.068062598384
  0.783266721831  2.590884380442  2.520834408126 -0.059777160668
  2.011402526412  2.551496219550  0.814855178005 -0.018322868892
  1.440220538007 -2.843720084632  1.356653310131 -0.039118527041
  0.871209499702 -2.660587439486  2.372618950166 -0.057782991449
 -2.947488997374  0.298617382934  2.058332654596 -0.014686776311
 -2.674954162172  1.563027715274  1.704200253745 -0.017417073473
 -1.353448939749  2.910920816403  0.212056013565 -0.018552232213
 -0.743422400795  2.810699746106 -0.731960510989 -0.002831065954
  0.099626804209  1.855073908563 -1.945822518898  0.033666812724
  0.019900458911 -1.968682238442 -1.864952523830  0.030244847284
 -0.601526683996 -2.669374282549 -1.032942810695  0.004656218902
 -1.976701730993 -2.578422698078  0.816296596419 -0.019199471668
 -2.525639241426 -1.850752473194  1.593331825886 -0.019904263007
 -1.124962231191 -1.881571875050  3.121019991266 -0.061295890199
 -2.117398618237 -1.513942036836  2.667866282109 -0.043460651366
 -2.336046769995 -0.152318885910  2.976109389266 -0.041488995782
 -1.478775916646  0.469153703894  3.577445396081 -0.059532569303
 -1.328587214463  1.668007282102  3.174271253825 -0.059421503188
 -1.771761195387  2.287385908353  2.202255939279 -0.045107884210
 -1.026730149698  3.017318998523  1.358608629268 -0.044215791648
  0.119020599620  3.173161356543  1.415159765853 -0.050020101107
  0.915237473358  3.108118624501  0.463299650838 -0.029866051613
  0.462814589486  2.837210699514 -0.795516582628 -0.003665487299
  1.026812912753 -3.017319139618  0.083991013862 -0.020466105590
 -0.070059393274 -3.176418651270  0.035610954337 -0.025702865355
 -0.970268948715 -3.083313212042  1.062474740253 -0.040167808039
 -0.534203029846 -2.828286137107  2.231267851728 -0.058531172935
  0.871897873699 -0.575451888002  3.799144800425 -0.066434668037
  1.408719892640  1.640288870679  3.148120355694 -0.058304793195
  2.650053602975  1.633766196698  1.655441764425 -0.017311721109
  1.964286464189 -2.001027831890  2.365093852582 -0.043456586788
 -1.342877128538 -0.760711330777  3.581800927521 -0.061215231177
 -0.531365927285  2.661712102822  2.509437292848 -0.061145855693
  1.142904167226  2.885766749848 -0.243475252462 -0.010743199331
  0.342129526197 -3.189261637688  1.246854200824 -0.047104606395
 -2.346459445268  1.135413320199  2.662806558936 -0.038596885254
 -0.258565605057  3.205542092831  0.249849913336 -0.029860546669
  0.450566961335 -2.699565911226 -1.032013446782  0.003232892526
 -1.684533476150 -2.430522251364  2.070179818920 -0.045033006631
 -3.368784049130  0.036613212616 -1.854972615293  0.060773071810
 -3.278396927799  1.580834404538 -0.078603229040  0.033626155657
 -3.656110378332 -0.713812229513  0.361831391088  0.034338520026
 -2.408052717333 -2.075622947728 -1.226189024363  0.038957801144
 -1.295829484895 -0.567704086865 -2.747607986898  0.058840583933
 -1.846557165647  1.681693338816 -2.099716493181  0.049090129169
 -3.831862607217  0.357468890277 -0.654813288033  0.050759105416
 -3.527736967644 -1.045671231370 -1.064852892071  0.051959739800
 -2.622906831544 -1.024738705101 -2.241124222290  0.058531014964
 -2.402781990555  0.378472303440 -2.579763034114  0.061826983281
 -3.126606927219  1.313275381963 -1.541857021673  0.053538779612
 -3.633182667875  0.531520753705  0.561856665829  0.031239572871
 -3.086813000261 -1.794874586082 -0.179700355757  0.031704280842
 -1.615880090830 -1.674393887320 -2.147504235692  0.048276100765
 -1.240258025012  0.765881087348 -2.687977655831  0.057596909974
 -2.430292517958  2.154437082790 -0.969924007583  0.034484050451
  3.368776503197 -0.036639040867 -1.854966842961  0.060761566158
  3.278392620256 -1.580850766330 -0.078589062660  0.033618806788
  3.656106873706  0.713798214804  0.361832640492  0.034331065278
  2.408046317685  2.075600470298 -1.226192756897  0.038946983724
  1.295820311657  0.567673501678 -2.747601655947  0.058829782505
  1.846549173548 -1.681720471299 -2.099699178989  0.049080020001
  3.831857249217 -0.357488322766 -0.654806650060  0.050749860707
  3.527730862121  1.045649613724 -1.064853177123  0.051949401582
  2.622898581638  1.024710819001 -2.241122746235  0.058518832296
  2.402773123306 -0.378501994157 -2.579753678917  0.061814786259
  3.126599952113 -1.313299541573 -1.541844004398  0.053528224744
  3.633179527908 -0.531533702443  0.561864593522  0.031233077370
  3.086808508398  1.794857685487 -0.179703829578  0.031695342906
  1.615872011596  1.674366500124 -2.147504385867  0.048264803953
  1.240248960489 -0.765911354740 -2.687964116774  0.057587007128
  2.430286585511 -2.154458194501 -0.969905238283  0.034475657772
  0.000006852884  1.035694667087 -2.361676372823  0.052175744685
 -0.298806015181  0.969607486526 -2.408809074493  0.053652028009
  0.298815704890  0.969607820141 -2.408807386530  0.053648411479
  0.000007656756 -1.035723195601 -2.361670853435  0.052176558734
 -0.298805262603 -0.969636498141 -2.408803907288  0.053652857315
  0.298816457468 -0.969636367899 -2.408802219325  0.053649049077
 -0.667712940797 -1.245186997899 -2.338574529312  0.051178246510
 -1.218112188165 -2.025138759161 -1.616566947615  0.036332864158
 -2.084175601108 -2.294008802021 -0.480477682822  0.020670970994
 -2.876631896395 -1.658047550445  0.559052047065  0.017343710926
 -3.282909221331 -0.368099862162  1.091996180628  0.019463384746
 -3.142757910518  1.067034798172  0.908143333087  0.018440609453
 -2.511458431963  2.081290260960  0.080011352455  0.017637297840
 -1.638016880752  2.274609499719 -1.065756198713  0.027045231314
 -0.866948462969  1.570740798135 -2.077229430584  0.047024679459
 -0.467915446486 -1.694563900014 -1.941501402531  0.039840994422
 -1.299753513329 -2.453901457341 -0.850306853788  0.012776965443
 -2.242033801688 -2.245340287831  0.385760998463  0.000966011128
 -2.923088754321 -1.151144046328  1.279154738758  0.002917971825
 -3.074287016292  0.397098864814  1.477489335582  0.004518844700
 -2.635990824421  1.788708515315  0.902534843950  0.001041782895
 -1.781079179779  2.474786487342 -0.218927030025  0.004573784108
 -0.846758459860  2.184720175398 -1.444553386477  0.025110526996
 -0.255852300976  1.360631011730 -2.219692209228  0.046689872516
  0.667723897920  1.245157743773 -2.338577856151  0.051170321948
  1.218123388571  2.025110412626 -1.616576841774  0.036322614531
  2.084186643139  2.293984793080 -0.480493513306  0.020661610133
  2.876642520254  1.658030225134  0.559035126479  0.017336752825
  3.282919570196  0.368088739237  1.091983758387  0.019458298058
  3.142768358070 -1.067043033853  0.908139383421  0.018436070589
  2.511469270857 -2.081300494445  0.080016452498  0.017631832632
  1.638028059662 -2.274626132712 -1.065745290088  0.027038134819
  0.866959534092 -1.570765766224 -2.077218589767  0.047017380482
  0.467932162408  1.694535407938 -1.941510815499  0.039834860120
  1.299770347024  2.453875604722 -0.850324284820  0.012768656600
  2.242050270258  2.245320705184  0.385739526123  0.000959204068
  2.923104801922  1.151131828431  1.279135167181  0.002913434782
  3.074302957696 -0.397105856761  1.477477125466  0.004515799743
  2.636007061705 -1.788714946455  0.902532611959  0.001038976021
  1.781095866662 -2.474797662024 -0.218920775640  0.004569885225
  0.846775315928 -2.184739733041 -1.444543821640  0.025106139135
  0.255868904327 -1.360657816862 -2.219684436601  0.046688031474
  0.427045418766 -0.580992742084 -2.585105089006  0.057258188824
  0.427044967836  0.580963354323 -2.585108185092  0.057257858426
 -0.427035838172 -0.000014913339 -2.543076305315  0.062532249015
 end_of_phi


 nsubv after electrostatic potential =  0

  Confirmation of dielectric energy for ground state
  edielnew =  -0.0116351967


 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************


 cosurf_and_qcos_from cosmo(3
  2.466315385316 -0.061539892486  2.837440149000  0.002316232734
  0.006359863210 -2.199287675687  3.114544749440  0.006221715269
 -0.480526931872  1.061509344774  3.755560521090  0.005715537829
  1.262739748330  2.872870034186  1.567866969040  0.005014441791
  1.897949994968 -2.634366215369  0.570962659525  0.001846848499
 -2.790166444607 -0.824331206483  2.170430257018  0.001025872538
 -2.061296060778  2.493528161233  1.034251636268  0.002855479602
 -0.002991703587  2.420846177436 -1.447622679829  0.000740158523
 -1.114435211431 -2.949559801670 -0.067898604227  0.002947636848
  1.955709175590 -0.984462672186  3.123501329999  0.004677496164
  1.000900154469 -1.706149424342  3.300403738319  0.006089772242
  1.652947872482  0.360731428865  3.496571379926  0.005241126413
  0.594940552015  0.670555958619  3.845537291721  0.005592908528
  2.390078334690  1.202722009900  2.566708449184  0.003429673357
  1.989334673621  2.229216060677  2.001028520754  0.003465359260
  2.919143888229  0.378144896597  2.099775822683  0.000478150409
  2.780367109803 -0.921046401167  2.130496470151  0.001479408043
  2.449941849009 -2.011800733381  1.439000205618  0.001875095161
 -0.231540571447 -1.263621120083  3.706953994699  0.006488293368
 -0.352106389130 -0.107618694495  3.950679056227  0.006249574864
  0.105380882100  1.878884201281  3.371423292662  0.008046142225
  0.783266721831  2.590884380442  2.520834408126  0.007158467134
  2.011402526412  2.551496219550  0.814855178005  0.002365014305
  1.440220538007 -2.843720084632  1.356653310131  0.004589869690
  0.871209499702 -2.660587439486  2.372618950166  0.007721931416
 -2.947488997374  0.298617382934  2.058332654596  0.000449210858
 -2.674954162172  1.563027715274  1.704200253745  0.001157125495
 -1.353448939749  2.910920816403  0.212056013565  0.003304027096
 -0.743422400795  2.810699746106 -0.731960510989  0.002234891522
  0.099626804209  1.855073908563 -1.945822518898 -0.000144203379
  0.019900458911 -1.968682238442 -1.864952523830 -0.000393887886
 -0.601526683996 -2.669374282549 -1.032942810695  0.001305899723
 -1.976701730993 -2.578422698078  0.816296596419  0.002530202904
 -2.525639241426 -1.850752473194  1.593331825886  0.001685218604
 -1.124962231191 -1.881571875050  3.121019991266  0.005989549221
 -2.117398618237 -1.513942036836  2.667866282109  0.003971854219
 -2.336046769995 -0.152318885910  2.976109389266  0.004145359217
 -1.478775916646  0.469153703894  3.577445396081  0.005157793767
 -1.328587214463  1.668007282102  3.174271253825  0.006145104078
 -1.771761195387  2.287385908353  2.202255939279  0.005841805691
 -1.026730149698  3.017318998523  1.358608629268  0.005747669991
  0.119020599620  3.173161356543  1.415159765853  0.007068663308
  0.915237473358  3.108118624501  0.463299650838  0.004878247401
  0.462814589486  2.837210699514 -0.795516582628  0.002336631092
  1.026812912753 -3.017319139618  0.083991013862  0.004071077433
 -0.070059393274 -3.176418651270  0.035610954337  0.004678825981
 -0.970268948715 -3.083313212042  1.062474740253  0.006582077751
 -0.534203029846 -2.828286137107  2.231267851728  0.008007484089
  0.871897873699 -0.575451888002  3.799144800425  0.006510030742
  1.408719892640  1.640288870679  3.148120355694  0.007034961980
  2.650053602975  1.633766196698  1.655441764425  0.001065655823
  1.964286464189 -2.001027831890  2.365093852582  0.004746103145
 -1.342877128538 -0.760711330777  3.581800927521  0.005368549297
 -0.531365927285  2.661712102822  2.509437292848  0.008065764580
  1.142904167226  2.885766749848 -0.243475252462  0.001390252701
  0.342129526197 -3.189261637688  1.246854200824  0.006879732872
 -2.346459445268  1.135413320199  2.662806558936  0.003540574016
 -0.258565605057  3.205542092831  0.249849913336  0.004504778511
  0.450566961335 -2.699565911226 -1.032013446782  0.002359499755
 -1.684533476150 -2.430522251364  2.070179818920  0.005001582018
 -3.368784049130  0.036613212616 -1.854972615293 -0.007684870769
 -3.278396927799  1.580834404538 -0.078603229040 -0.005731277278
 -3.656110378332 -0.713812229513  0.361831391088 -0.006431953031
 -2.408052717333 -2.075622947728 -1.226189024363 -0.005375615883
 -1.295829484895 -0.567704086865 -2.747607986898 -0.005895027744
 -1.846557165647  1.681693338816 -2.099716493181 -0.006050016352
 -3.831862607217  0.357468890277 -0.654813288033 -0.008310730698
 -3.527736967644 -1.045671231370 -1.064852892071 -0.008216776637
 -2.622906831544 -1.024738705101 -2.241124222290 -0.008131964181
 -2.402781990555  0.378472303440 -2.579763034114 -0.007909256636
 -3.126606927219  1.313275381963 -1.541857021673 -0.008363205221
 -3.633182667875  0.531520753705  0.561856665829 -0.004516966521
 -3.086813000261 -1.794874586082 -0.179700355757 -0.004490120787
 -1.615880090830 -1.674393887320 -2.147504235692 -0.004627190918
 -1.240258025012  0.765881087348 -2.687977655831 -0.004873676031
 -2.430292517958  2.154437082790 -0.969924007583 -0.004058888833
  3.368776503197 -0.036639040867 -1.854966842961 -0.007690274645
  3.278392620256 -1.580850766330 -0.078589062660 -0.005716326504
  3.656106873706  0.713798214804  0.361832640492 -0.006426511150
  2.408046317685  2.075600470298 -1.226192756897 -0.005383382617
  1.295820311657  0.567673501678 -2.747601655947 -0.006317811953
  1.846549173548 -1.681720471299 -2.099699178989 -0.006002271153
  3.831857249217 -0.357488322766 -0.654806650060 -0.008309839129
  3.527730862121  1.045649613724 -1.064853177123 -0.008219353780
  2.622898581638  1.024710819001 -2.241122746235 -0.008117189979
  2.402773123306 -0.378501994157 -2.579753678917 -0.007899359271
  3.126599952113 -1.313299541573 -1.541844004398 -0.008366916544
  3.633179527908 -0.531533702443  0.561864593522 -0.004512578493
  3.086808508398  1.794857685487 -0.179703829578 -0.004487098489
  1.615872011596  1.674366500124 -2.147504385867 -0.004592017857
  1.240248960489 -0.765911354740 -2.687964116774 -0.005133022138
  2.430286585511 -2.154458194501 -0.969905238283 -0.004044690758
  0.000006852884  1.035694667087 -2.361676372823 -0.000286903595
 -0.298806015181  0.969607486526 -2.408809074493 -0.000344672881
  0.298815704890  0.969607820141 -2.408807386530 -0.000253730935
  0.000007656756 -1.035723195601 -2.361670853435 -0.000280065524
 -0.298805262603 -0.969636498141 -2.408803907288 -0.000330412356
  0.298816457468 -0.969636367899 -2.408802219325 -0.000250320471
 -0.667712940797 -1.245186997899 -2.338574529312 -0.000950822445
 -1.218112188165 -2.025138759161 -1.616566947615 -0.001417843618
 -2.084175601108 -2.294008802021 -0.480477682822 -0.000942824846
 -2.876631896395 -1.658047550445  0.559052047065 -0.001362905282
 -3.282909221331 -0.368099862162  1.091996180628 -0.001812533671
 -3.142757910518  1.067034798172  0.908143333087 -0.001623690693
 -2.511458431963  2.081290260960  0.080011352455 -0.001169140224
 -1.638016880752  2.274609499719 -1.065756198713 -0.001133252376
 -0.866948462969  1.570740798135 -2.077229430584 -0.001968213380
 -0.467915446486 -1.694563900014 -1.941501402531 -0.001432572575
 -1.299753513329 -2.453901457341 -0.850306853788 -0.000244514963
 -2.242033801688 -2.245340287831  0.385760998463 -0.000310280233
 -2.923088754321 -1.151144046328  1.279154738758 -0.000890291187
 -3.074287016292  0.397098864814  1.477489335582 -0.001164042948
 -2.635990824421  1.788708515315  0.902534843950 -0.000524582218
 -1.781079179779  2.474786487342 -0.218927030025 -0.000181655555
 -0.846758459860  2.184720175398 -1.444553386477 -0.000851849338
 -0.255852300976  1.360631011730 -2.219692209228 -0.000920428176
  0.667723897920  1.245157743773 -2.338577856151 -0.000883426134
  1.218123388571  2.025110412626 -1.616576841774 -0.001429612735
  2.084186643139  2.293984793080 -0.480493513306 -0.000920223299
  2.876642520254  1.658030225134  0.559035126479 -0.001357110645
  3.282919570196  0.368088739237  1.091983758387 -0.001801289867
  3.142768358070 -1.067043033853  0.908139383421 -0.001616196170
  2.511469270857 -2.081300494445  0.080016452498 -0.001109751021
  1.638028059662 -2.274626132712 -1.065745290088 -0.001092350914
  0.866959534092 -1.570765766224 -2.077218589767 -0.001916627902
  0.467932162408  1.694535407938 -1.941510815499 -0.001486510011
  1.299770347024  2.453875604722 -0.850324284820 -0.000255996287
  2.242050270258  2.245320705184  0.385739526123 -0.000240395361
  2.923104801922  1.151131828431  1.279135167181 -0.000916950561
  3.074302957696 -0.397105856761  1.477477125466 -0.001193410820
  2.636007061705 -1.788714946455  0.902532611959 -0.000569621997
  1.781095866662 -2.474797662024 -0.218920775640 -0.000236949237
  0.846775315928 -2.184739733041 -1.444543821640 -0.000902222228
  0.255868904327 -1.360657816862 -2.219684436601 -0.000851744457
  0.427045418766 -0.580992742084 -2.585105089006 -0.002234817221
  0.427044967836  0.580963354323 -2.585108185092 -0.002107679860
 -0.427035838172 -0.000014913339 -2.543076305315 -0.006024067996
 end_of_qcos

 sum of the negative charges =  -0.252218777

 sum of the positive charges =   0.247388411

 total sum =  -0.00483036637

 *** qcos is copied to qcosdalton ***

 fepsi =   1.
 *** qcosdalton is mult by fepsi ***
 cosurf and qcosdalton
  2.466315385316 -0.061539892486  2.837440149000  0.002316232734
  0.006359863210 -2.199287675687  3.114544749440  0.006221715269
 -0.480526931872  1.061509344774  3.755560521090  0.005715537829
  1.262739748330  2.872870034186  1.567866969040  0.005014441791
  1.897949994968 -2.634366215369  0.570962659525  0.001846848499
 -2.790166444607 -0.824331206483  2.170430257018  0.001025872538
 -2.061296060778  2.493528161233  1.034251636268  0.002855479602
 -0.002991703587  2.420846177436 -1.447622679829  0.000740158523
 -1.114435211431 -2.949559801670 -0.067898604227  0.002947636848
  1.955709175590 -0.984462672186  3.123501329999  0.004677496164
  1.000900154469 -1.706149424342  3.300403738319  0.006089772242
  1.652947872482  0.360731428865  3.496571379926  0.005241126413
  0.594940552015  0.670555958619  3.845537291721  0.005592908528
  2.390078334690  1.202722009900  2.566708449184  0.003429673357
  1.989334673621  2.229216060677  2.001028520754  0.003465359260
  2.919143888229  0.378144896597  2.099775822683  0.000478150409
  2.780367109803 -0.921046401167  2.130496470151  0.001479408043
  2.449941849009 -2.011800733381  1.439000205618  0.001875095161
 -0.231540571447 -1.263621120083  3.706953994699  0.006488293368
 -0.352106389130 -0.107618694495  3.950679056227  0.006249574864
  0.105380882100  1.878884201281  3.371423292662  0.008046142225
  0.783266721831  2.590884380442  2.520834408126  0.007158467134
  2.011402526412  2.551496219550  0.814855178005  0.002365014305
  1.440220538007 -2.843720084632  1.356653310131  0.004589869690
  0.871209499702 -2.660587439486  2.372618950166  0.007721931416
 -2.947488997374  0.298617382934  2.058332654596  0.000449210858
 -2.674954162172  1.563027715274  1.704200253745  0.001157125495
 -1.353448939749  2.910920816403  0.212056013565  0.003304027096
 -0.743422400795  2.810699746106 -0.731960510989  0.002234891522
  0.099626804209  1.855073908563 -1.945822518898 -0.000144203379
  0.019900458911 -1.968682238442 -1.864952523830 -0.000393887886
 -0.601526683996 -2.669374282549 -1.032942810695  0.001305899723
 -1.976701730993 -2.578422698078  0.816296596419  0.002530202904
 -2.525639241426 -1.850752473194  1.593331825886  0.001685218604
 -1.124962231191 -1.881571875050  3.121019991266  0.005989549221
 -2.117398618237 -1.513942036836  2.667866282109  0.003971854219
 -2.336046769995 -0.152318885910  2.976109389266  0.004145359217
 -1.478775916646  0.469153703894  3.577445396081  0.005157793767
 -1.328587214463  1.668007282102  3.174271253825  0.006145104078
 -1.771761195387  2.287385908353  2.202255939279  0.005841805691
 -1.026730149698  3.017318998523  1.358608629268  0.005747669991
  0.119020599620  3.173161356543  1.415159765853  0.007068663308
  0.915237473358  3.108118624501  0.463299650838  0.004878247401
  0.462814589486  2.837210699514 -0.795516582628  0.002336631092
  1.026812912753 -3.017319139618  0.083991013862  0.004071077433
 -0.070059393274 -3.176418651270  0.035610954337  0.004678825981
 -0.970268948715 -3.083313212042  1.062474740253  0.006582077751
 -0.534203029846 -2.828286137107  2.231267851728  0.008007484089
  0.871897873699 -0.575451888002  3.799144800425  0.006510030742
  1.408719892640  1.640288870679  3.148120355694  0.007034961980
  2.650053602975  1.633766196698  1.655441764425  0.001065655823
  1.964286464189 -2.001027831890  2.365093852582  0.004746103145
 -1.342877128538 -0.760711330777  3.581800927521  0.005368549297
 -0.531365927285  2.661712102822  2.509437292848  0.008065764580
  1.142904167226  2.885766749848 -0.243475252462  0.001390252701
  0.342129526197 -3.189261637688  1.246854200824  0.006879732872
 -2.346459445268  1.135413320199  2.662806558936  0.003540574016
 -0.258565605057  3.205542092831  0.249849913336  0.004504778511
  0.450566961335 -2.699565911226 -1.032013446782  0.002359499755
 -1.684533476150 -2.430522251364  2.070179818920  0.005001582018
 -3.368784049130  0.036613212616 -1.854972615293 -0.007684870769
 -3.278396927799  1.580834404538 -0.078603229040 -0.005731277278
 -3.656110378332 -0.713812229513  0.361831391088 -0.006431953031
 -2.408052717333 -2.075622947728 -1.226189024363 -0.005375615883
 -1.295829484895 -0.567704086865 -2.747607986898 -0.005895027744
 -1.846557165647  1.681693338816 -2.099716493181 -0.006050016352
 -3.831862607217  0.357468890277 -0.654813288033 -0.008310730698
 -3.527736967644 -1.045671231370 -1.064852892071 -0.008216776637
 -2.622906831544 -1.024738705101 -2.241124222290 -0.008131964181
 -2.402781990555  0.378472303440 -2.579763034114 -0.007909256636
 -3.126606927219  1.313275381963 -1.541857021673 -0.008363205221
 -3.633182667875  0.531520753705  0.561856665829 -0.004516966521
 -3.086813000261 -1.794874586082 -0.179700355757 -0.004490120787
 -1.615880090830 -1.674393887320 -2.147504235692 -0.004627190918
 -1.240258025012  0.765881087348 -2.687977655831 -0.004873676031
 -2.430292517958  2.154437082790 -0.969924007583 -0.004058888833
  3.368776503197 -0.036639040867 -1.854966842961 -0.007690274645
  3.278392620256 -1.580850766330 -0.078589062660 -0.005716326504
  3.656106873706  0.713798214804  0.361832640492 -0.006426511150
  2.408046317685  2.075600470298 -1.226192756897 -0.005383382617
  1.295820311657  0.567673501678 -2.747601655947 -0.006317811953
  1.846549173548 -1.681720471299 -2.099699178989 -0.006002271153
  3.831857249217 -0.357488322766 -0.654806650060 -0.008309839129
  3.527730862121  1.045649613724 -1.064853177123 -0.008219353780
  2.622898581638  1.024710819001 -2.241122746235 -0.008117189979
  2.402773123306 -0.378501994157 -2.579753678917 -0.007899359271
  3.126599952113 -1.313299541573 -1.541844004398 -0.008366916544
  3.633179527908 -0.531533702443  0.561864593522 -0.004512578493
  3.086808508398  1.794857685487 -0.179703829578 -0.004487098489
  1.615872011596  1.674366500124 -2.147504385867 -0.004592017857
  1.240248960489 -0.765911354740 -2.687964116774 -0.005133022138
  2.430286585511 -2.154458194501 -0.969905238283 -0.004044690758
  0.000006852884  1.035694667087 -2.361676372823 -0.000286903595
 -0.298806015181  0.969607486526 -2.408809074493 -0.000344672881
  0.298815704890  0.969607820141 -2.408807386530 -0.000253730935
  0.000007656756 -1.035723195601 -2.361670853435 -0.000280065524
 -0.298805262603 -0.969636498141 -2.408803907288 -0.000330412356
  0.298816457468 -0.969636367899 -2.408802219325 -0.000250320471
 -0.667712940797 -1.245186997899 -2.338574529312 -0.000950822445
 -1.218112188165 -2.025138759161 -1.616566947615 -0.001417843618
 -2.084175601108 -2.294008802021 -0.480477682822 -0.000942824846
 -2.876631896395 -1.658047550445  0.559052047065 -0.001362905282
 -3.282909221331 -0.368099862162  1.091996180628 -0.001812533671
 -3.142757910518  1.067034798172  0.908143333087 -0.001623690693
 -2.511458431963  2.081290260960  0.080011352455 -0.001169140224
 -1.638016880752  2.274609499719 -1.065756198713 -0.001133252376
 -0.866948462969  1.570740798135 -2.077229430584 -0.001968213380
 -0.467915446486 -1.694563900014 -1.941501402531 -0.001432572575
 -1.299753513329 -2.453901457341 -0.850306853788 -0.000244514963
 -2.242033801688 -2.245340287831  0.385760998463 -0.000310280233
 -2.923088754321 -1.151144046328  1.279154738758 -0.000890291187
 -3.074287016292  0.397098864814  1.477489335582 -0.001164042948
 -2.635990824421  1.788708515315  0.902534843950 -0.000524582218
 -1.781079179779  2.474786487342 -0.218927030025 -0.000181655555
 -0.846758459860  2.184720175398 -1.444553386477 -0.000851849338
 -0.255852300976  1.360631011730 -2.219692209228 -0.000920428176
  0.667723897920  1.245157743773 -2.338577856151 -0.000883426134
  1.218123388571  2.025110412626 -1.616576841774 -0.001429612735
  2.084186643139  2.293984793080 -0.480493513306 -0.000920223299
  2.876642520254  1.658030225134  0.559035126479 -0.001357110645
  3.282919570196  0.368088739237  1.091983758387 -0.001801289867
  3.142768358070 -1.067043033853  0.908139383421 -0.001616196170
  2.511469270857 -2.081300494445  0.080016452498 -0.001109751021
  1.638028059662 -2.274626132712 -1.065745290088 -0.001092350914
  0.866959534092 -1.570765766224 -2.077218589767 -0.001916627902
  0.467932162408  1.694535407938 -1.941510815499 -0.001486510011
  1.299770347024  2.453875604722 -0.850324284820 -0.000255996287
  2.242050270258  2.245320705184  0.385739526123 -0.000240395361
  2.923104801922  1.151131828431  1.279135167181 -0.000916950561
  3.074302957696 -0.397105856761  1.477477125466 -0.001193410820
  2.636007061705 -1.788714946455  0.902532611959 -0.000569621997
  1.781095866662 -2.474797662024 -0.218920775640 -0.000236949237
  0.846775315928 -2.184739733041 -1.444543821640 -0.000902222228
  0.255868904327 -1.360657816862 -2.219684436601 -0.000851744457
  0.427045418766 -0.580992742084 -2.585105089006 -0.002234817221
  0.427044967836  0.580963354323 -2.585108185092 -0.002107679860
 -0.427035838172 -0.000014913339 -2.543076305315 -0.006024067996
 end of cosurf and qcosdalton

 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 ** Total nuclear repulsion energy ** =   9.36083464233288
 screening nuclear repulsion energy   0.00753972592

 Total-screening nuclear repulsion energy   9.35329492


 Adding T+Vsolv ...
 maxdens  300
 *** End of DALTON-COSMO calculation ***

  original map vector  20 21 22 23 24 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17
 18 19 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0


          sorted Vsolv matrix  block   1

               nbf   1        nbf   2        nbf   3        nbf   4        nbf   5        nbf   6        nbf   7        nbf   8
  nbf   1    -3.31802272
  nbf   2     0.00001592    -3.09061980
  nbf   3     0.00001793    -0.98129122    -4.10174603
  nbf   4    -1.01842365     0.00000424     0.00001692    -4.07782990
  nbf   5    -0.42218934     0.00001260     0.00001051    -0.44126466    -4.75244506
  nbf   6    -0.00000512    -0.00000339     0.00000211     0.00000136     0.00000160    -5.34887508
  nbf   7    -0.00000446    -1.17059722    -0.38132514     0.00000801     0.00000673    -0.00000271    -4.33717963
  nbf   8    -0.41285957     0.00000930     0.00002846     0.04017304     0.09702260    -0.00000435    -0.00000462    -3.58726981
  nbf   9     0.00000273     0.00000102     0.00000188     0.00000125    -0.00000001    -0.00000316     0.00000086    -0.00000228
  nbf  10     0.00000010     0.00000040     0.00000124     0.00000081     0.00000611     0.24657181     0.00000115    -0.00000061
  nbf  11     0.71273274     0.00000328     0.00000647    -0.06900462    -0.45106778     0.00000248     0.00000102     0.37445956
  nbf  12    -0.00002183    -0.02255544    -0.02736011     0.00000053    -0.00004180     0.00000092    -0.10301354    -0.00003937
  nbf  13     0.00001070     0.51225307     1.29213935     0.00000943     0.00000516     0.00000174     1.13410868     0.00000467
  nbf  14     0.60243392     0.00000730     0.00000325     1.39180974     0.61421969     0.00000225     0.00000731     0.29791164
  nbf  15     0.00000113     0.00000005    -0.00000011     0.00000167     0.00000192     0.06454149     0.00000118     0.00000074
  nbf  16     0.00000148     0.00000192     0.00000280     0.00000049     0.00000035    -0.00000232    -0.00000021    -0.00000074
  nbf  17     0.01597818    -0.00000629    -0.00001379     0.28427846    -0.03521637     0.00000102     0.00000134    -0.91938527
  nbf  18    -0.11636346    -0.00000596    -0.00000157    -0.29333938    -0.90109029     0.00000048    -0.00000103    -0.23509202
  nbf  19    -0.00000634     0.16662668    -0.06195832     0.00000037    -0.00000121    -0.00000009     1.19139594    -0.00000734
  nbf  20     0.18303919     0.00000005     0.00000000     0.16560575    -0.11838520    -0.00000046     0.00000013     0.20494910
  nbf  21    -0.99287531    -0.00000496    -0.00001594    -0.72375385     0.04745434     0.00000173     0.00000383    -0.60130201
  nbf  22     0.00000792     1.23665788     0.65675499     0.00001045     0.00000959    -0.00000235     1.45004668     0.00001189
  nbf  23     0.54129581     0.00000776     0.00001373     0.73132436     1.71707325     0.00000016     0.00000306    -0.41970520
  nbf  24     0.00000200     0.00000137    -0.00000148    -0.00000169     0.00000013    -2.13412905     0.00000157     0.00000231

               nbf   9        nbf  10        nbf  11        nbf  12        nbf  13        nbf  14        nbf  15        nbf  16
  nbf   9    -3.42223196
  nbf  10     0.00001996    -3.17751864
  nbf  11    -0.00000297    -0.00000185    -4.03661407
  nbf  12    -0.00000420     0.00000167    -0.00000813    -2.63220682
  nbf  13    -0.00000190    -0.00000169     0.00000130     0.09058471    -3.62261342
  nbf  14    -0.00000169    -0.00000158    -0.18629694     0.00000095     0.00002165    -3.66397859
  nbf  15     0.00000633     0.90765296    -0.00000061     0.00000069     0.00000003     0.00000034    -3.67016250
  nbf  16     0.90251177     0.00000719    -0.00000033    -0.00000091    -0.00000063    -0.00000022     0.00000633    -3.63972534
  nbf  17     0.00000122     0.00000043    -0.42343216     0.00002351    -0.00000514     0.04524337    -0.00000009     0.00000163
  nbf  18     0.00000057    -0.00000151     0.56834097     0.00000991    -0.00000943    -0.46534251    -0.00000085     0.00000048
  nbf  19    -0.00000117    -0.00000060    -0.00000054    -0.08841526     0.44954127     0.00000853    -0.00000105    -0.00000004
  nbf  20    -0.00000001     0.00000003    -0.38281043     0.00000000    -0.00000002    -0.26338669    -0.00000001     0.00000005
  nbf  21    -0.00000281    -0.00000169     1.08737989     0.00001362     0.00000243     0.59830927    -0.00000036     0.00000064
  nbf  22     0.00000386     0.00000216     0.00000592     0.01871188    -0.54201712     0.00000030     0.00000097    -0.00000148
  nbf  23     0.00000114     0.00000552     0.54855080    -0.00002641     0.00000189    -0.34234373    -0.00000283     0.00000120
  nbf  24     0.00000384     0.07853537    -0.00000217    -0.00000058    -0.00000115    -0.00000014     0.08085629     0.00000195

               nbf  17        nbf  18        nbf  19        nbf  20        nbf  21        nbf  22        nbf  23        nbf  24
  nbf  17    -3.38765468
  nbf  18    -0.05579537    -3.03517508
  nbf  19     0.00000616    -0.00000760    -2.89461854
  nbf  20     0.00966319     0.05948955    -0.00000026   -33.05208400
  nbf  21    -0.01753917    -0.18425888     0.00000146     0.58206079    -7.87299029
  nbf  22    -0.00000613     0.00000102    -0.45831191    -0.00000013    -0.00000969    -6.71596668
  nbf  23    -0.12232513     0.30565438    -0.00000426     0.18750803    -0.25169190     0.00001435    -6.95611046
  nbf  24    -0.00000089     0.00000142    -0.00000008    -0.00000041    -0.00000470     0.00000238    -0.00000114    -7.11721406
 insert_onel_diag: nmin2=   380

 onel.diag.
 -0.331802E+01-0.309062E+01-0.410175E+01-0.407783E+01-0.475245E+01-0.534888E+01-0.433718E+01-0.358727E+01-0.342223E+01-0.317752E+01
 -0.403661E+01-0.263221E+01-0.362261E+01-0.366398E+01-0.367016E+01-0.363973E+01-0.338765E+01-0.303518E+01-0.289462E+01-0.330521E+02
 -0.787299E+01-0.671597E+01-0.695611E+01-0.711721E+01

 i,start,strti,fin,filind,bufszi,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095         1       380       380

 4 external diag. modified integrals

  0.299711954 -3.31802272  0.362025844  0.213403987  0.294044049 -3.0906198
  0.345182252  0.318068021  0.34837146  0.293493931  0.497019376 -4.10174603
  0.382867967  0.309072536  0.341195502  0.316765031  0.62103433  0.362850338
  0.531836961 -4.0778299  0.349706612  0.299527054  0.308820027  0.298754537
  0.450260207  0.419223806  0.448476864  0.401601457  0.518535861 -4.75244506
  0.349531774  0.304524501  0.31278089  0.302655019  0.429312411  0.416826853
  0.436921903  0.413400849  0.530775438  0.48280831  0.60679891 -5.34887508
  0.33535564  0.293407704  0.341034277  0.276576801  0.455225842  0.389302194
  0.443351075  0.383054684  0.486303633  0.428587076  0.499742288  0.459023099
  0.475434104 -4.33717963  0.342526573  0.315362897  0.319917749  0.300731659
  0.504389096  0.410870033  0.47482656  0.425010382  0.519270202  0.400100848
  0.453053263  0.422413645  0.446074502  0.406547185  0.476405093 -3.58726981
  0.361060191  0.337659006  0.340604687  0.320198588  0.504515292  0.452245513
  0.527488651  0.469039301  0.434901697  0.420706511  0.454829088  0.417262113
  0.426620519  0.400804422  0.469386808  0.441599006  0.555764969 -3.42223196
  0.348856386  0.329501947  0.326880472  0.312634683  0.490451682  0.436629022
  0.509180653  0.444435002  0.4400708  0.419071921  0.459403052  0.417691971
  0.415311401  0.398901525  0.466192986  0.43088403  0.694750225  0.346946891
  0.501631969 -3.17751864  0.343227876  0.31280524  0.321132402  0.300850073
  0.455432102  0.418963752  0.451109274  0.419591702  0.534502331  0.438245352
  0.597905727  0.464004009  0.508095073  0.408470727  0.460615989  0.424227034
  0.469521607  0.441123033  0.462583019  0.438238584  0.525774157 -4.03661407
  0.356976431  0.333496982  0.334773679  0.315431831  0.522936179  0.430785245
  0.55704189  0.430105919  0.46546176  0.385379923  0.398185515  0.390667038
  0.424333725  0.380522726  0.592974188  0.354449455  0.516100041  0.471421051
  0.491982029  0.453400696  0.428233148  0.409923576  0.53092711 -2.63220682
  0.370351482  0.34231438  0.377526483  0.320334123  0.57663332  0.44254372
  0.563518308  0.477512966  0.511232086  0.469131823  0.508472502  0.490097443
  0.529535469  0.421869959  0.499545867  0.467575397  0.539123293  0.488956188
  0.512741012  0.475743005  0.515621907  0.477676678  0.512163042  0.474196449
  0.592144122 -3.62261342  0.388203856  0.340302294  0.365601016  0.328125926
  0.536368018  0.473948976  0.615526601  0.455378076  0.520967947  0.463416877
  0.546457699  0.489981129  0.48581785  0.455063687  0.509165453  0.46499119
  0.535052608  0.504064949  0.518029026  0.481875024  0.516561248  0.489811629
  0.517714869  0.482829445  0.717425907  0.445100321  0.607835731 -3.66397859
  0.361030445  0.336028491  0.330453775  0.322428351  0.486741558  0.469418774
  0.514189842  0.450314389  0.575178104  0.539226259  0.630438312  0.577562728
  0.510895782  0.499926663  0.503939385  0.490371469  0.52041881  0.467839603
  0.554043059  0.445274854  0.591832537  0.558697254  0.453957255  0.445032703
  0.568738744  0.548069735  0.596778585  0.560916765  0.750165186 -3.6701625
  0.353015389  0.338054232  0.358115278  0.317291888  0.522721553  0.462375435
  0.489534774  0.469144866  0.543456123  0.532600344  0.632201055  0.577477753
  0.547273656  0.513624874  0.507616587  0.483570727  0.564197636  0.455577875
  0.524876517  0.46718588  0.585160178  0.562784851  0.458775281  0.447244069
  0.587371194  0.556188564  0.58998874  0.568912261  0.721594827  0.645098881
  0.755127757 -3.63972534  0.366037972  0.335416332  0.343455146  0.320873443
  0.50940609  0.476326958  0.527676719  0.456821536  0.589439367  0.537772005
  0.579789349  0.559672288  0.53269644  0.49854365  0.567546289  0.469810852
  0.495610134  0.480892777  0.493939617  0.479062411  0.584343602  0.533457424
  0.502525388  0.442000941  0.582221799  0.560044775  0.596954899  0.56140811
  0.713528596  0.656785472  0.69947109  0.625487345  0.729807063 -3.38765468
  0.353987843  0.337616982  0.351789554  0.321182817  0.550334788  0.456688986
  0.522995786  0.46803517  0.566057852  0.507622395  0.615570104  0.560439732
  0.539018372  0.503813066  0.510463413  0.481771744  0.500103129  0.48786777
  0.496692123  0.477703001  0.585735173  0.540040646  0.469784387  0.453592071
  0.609978757  0.53850005  0.643846451  0.534124473  0.686162365  0.635949394
  0.700906311  0.666564985  0.679632027  0.617528828  0.696112735 -3.03517508
  0.358856446  0.335907395  0.354506012  0.319503646  0.548011068  0.469120392
  0.531509325  0.460743131  0.577501451  0.534350115  0.577624788  0.562928162
  0.587296462  0.4866605  0.514004213  0.4958057  0.493523583  0.479933652
  0.487740555  0.474497517  0.570979461  0.537487177  0.471960345  0.452931079
  0.634007692  0.54138526  0.615254278  0.550385293  0.691778305  0.63576614
  0.699509207  0.640665078  0.699234432  0.657364716  0.711993958  0.616407541
  0.721280492 -2.89461854
 i,start,strti,fin,filind,bufszi,nmbuf,nmin2, nd4ext = 
         1         1         2         2        12      4095         1       380       380

 all internall diag. modified integrals

  4.73971342 -33.052084  1.04569563  0.0636788604  0.755179462 -7.87299029
  0.878477817  0.0193493095  0.682719812  0.155759544  0.668673763 -6.71596668
  0.986988872  0.0301374391  0.681192227  0.125736446  0.608457144
  0.0402117459  0.732528817 -6.95611046  1.03831833  0.0301199817  0.713926708
  0.128455235  0.624391224  0.0304166993  0.665957609  0.0433645062
  0.760228983 -7.11721406
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331802E+01-0.309062E+01-0.410175E+01-0.407783E+01-0.475245E+01-0.534888E+01-0.433718E+01-0.358727E+01-0.342223E+01-0.317752E+01
 -0.403661E+01-0.263221E+01-0.362261E+01-0.366398E+01-0.367016E+01-0.363973E+01-0.338765E+01-0.303518E+01-0.289462E+01-0.330521E+02
 -0.787299E+01-0.671597E+01-0.695611E+01-0.711721E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331802E+01-0.309062E+01-0.410175E+01-0.407783E+01-0.475245E+01-0.534888E+01-0.433718E+01-0.358727E+01-0.342223E+01-0.317752E+01
 -0.403661E+01-0.263221E+01-0.362261E+01-0.366398E+01-0.367016E+01-0.363973E+01-0.338765E+01-0.303518E+01-0.289462E+01-0.330521E+02
 -0.787299E+01-0.671597E+01-0.695611E+01-0.711721E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331802E+01-0.309062E+01-0.410175E+01-0.407783E+01-0.475245E+01-0.534888E+01-0.433718E+01-0.358727E+01-0.342223E+01-0.317752E+01
 -0.403661E+01-0.263221E+01-0.362261E+01-0.366398E+01-0.367016E+01-0.363973E+01-0.338765E+01-0.303518E+01-0.289462E+01-0.330521E+02
 -0.787299E+01-0.671597E+01-0.695611E+01-0.711721E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380
 DIAGEL COUNTS: DG0X,DG2X,DG4X= 440 145 30
 =========== Executing IN-CORE method ==========
 norm multnew:  1.
 threx: total number of loops= 14
 threx: total number of loops= 24
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331802E+01-0.309062E+01-0.410175E+01-0.407783E+01-0.475245E+01-0.534888E+01-0.433718E+01-0.358727E+01-0.342223E+01-0.317752E+01
 -0.403661E+01-0.263221E+01-0.362261E+01-0.366398E+01-0.367016E+01-0.363973E+01-0.338765E+01-0.303518E+01-0.289462E+01-0.330521E+02
 -0.787299E+01-0.671597E+01-0.695611E+01-0.711721E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331802E+01-0.309062E+01-0.410175E+01-0.407783E+01-0.475245E+01-0.534888E+01-0.433718E+01-0.358727E+01-0.342223E+01-0.317752E+01
 -0.403661E+01-0.263221E+01-0.362261E+01-0.366398E+01-0.367016E+01-0.363973E+01-0.338765E+01-0.303518E+01-0.289462E+01-0.330521E+02
 -0.787299E+01-0.671597E+01-0.695611E+01-0.711721E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331802E+01-0.309062E+01-0.410175E+01-0.407783E+01-0.475245E+01-0.534888E+01-0.433718E+01-0.358727E+01-0.342223E+01-0.317752E+01
 -0.403661E+01-0.263221E+01-0.362261E+01-0.366398E+01-0.367016E+01-0.363973E+01-0.338765E+01-0.303518E+01-0.289462E+01-0.330521E+02
 -0.787299E+01-0.671597E+01-0.695611E+01-0.711721E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        40 xx:       135 ww:       255
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        20 yw:        25

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        39
task #   9:       127    task #  10:       247    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 matrix-vector product formed for vector   1
 =========== Executing IN-CORE method ==========
 norm multnew:  4.24574988E-05
 threx: total number of loops= 14
 threx: total number of loops= 24
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331802E+01-0.309062E+01-0.410175E+01-0.407783E+01-0.475245E+01-0.534888E+01-0.433718E+01-0.358727E+01-0.342223E+01-0.317752E+01
 -0.403661E+01-0.263221E+01-0.362261E+01-0.366398E+01-0.367016E+01-0.363973E+01-0.338765E+01-0.303518E+01-0.289462E+01-0.330521E+02
 -0.787299E+01-0.671597E+01-0.695611E+01-0.711721E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331802E+01-0.309062E+01-0.410175E+01-0.407783E+01-0.475245E+01-0.534888E+01-0.433718E+01-0.358727E+01-0.342223E+01-0.317752E+01
 -0.403661E+01-0.263221E+01-0.362261E+01-0.366398E+01-0.367016E+01-0.363973E+01-0.338765E+01-0.303518E+01-0.289462E+01-0.330521E+02
 -0.787299E+01-0.671597E+01-0.695611E+01-0.711721E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331802E+01-0.309062E+01-0.410175E+01-0.407783E+01-0.475245E+01-0.534888E+01-0.433718E+01-0.358727E+01-0.342223E+01-0.317752E+01
 -0.403661E+01-0.263221E+01-0.362261E+01-0.366398E+01-0.367016E+01-0.363973E+01-0.338765E+01-0.303518E+01-0.289462E+01-0.330521E+02
 -0.787299E+01-0.671597E+01-0.695611E+01-0.711721E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        40 xx:       135 ww:       255
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        20 yw:        25

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        39
task #   9:       127    task #  10:       247    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 matrix-vector product formed for vector   2
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference-subspace vector overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97594644    -0.00606149

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97552724    -0.19591392
 subspace has dimension  2

          (h-repnuc*1) matrix in the subspace basis

                x:   1         x:   2
   x:   1   -85.60689593
   x:   2     0.47294205    -0.00359747

          overlap matrix in the subspace basis

                x:   1         x:   2
   x:   1     1.00000000
   x:   2    -0.00552485     0.00004246

          eigenvectors and eigenvalues in the subspace basis

                v:   1         v:   2

   energy   -85.60691026   -82.51190836

   x:   1     1.00343824     1.59716492
   x:   2     0.62274209   289.47697654

          eigenvectors and eigenvalues in the schmidt orthonormalized basis

                v:   1         v:   2

   energy   -85.60691026   -82.51190836

  zx:   1     0.99999769    -0.00215126
  zx:   2     0.00215126     0.99999769

          <ref|baseci> overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97594644    -0.00606149

 trial vector basis is being transformed.  new dimension:   1

          transformed tciref transformation matrix  block   1

               ref   1
  ci:   1     0.97552724

  write the transformed vectors back to cosmofile

 rtflw =  0
 freezing =  F

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  6  1    -76.2536153391 -9.3534E+00  4.5589E-06  3.7742E-03  1.0000E-03
 mr-sdci #  6  2    -73.1586134448 -8.7315E+00  0.0000E+00  1.7331E+00  1.0000E-04


 total energy in cosmo calc 
e(rootcalc) + repnuc=     -76.2536153391
dielectric energy =      -0.0116203567
deltaediel =       0.0116203567
e(rootcalc) + repnuc - ediel =     -76.2419949824
e(rootcalc) + repnuc - edielnew =     -76.2536153391
deltaelast =      76.2419949824

 root number  1 is used to define the new expansion vector.
 diagon: frcsub=2, new matrix-vector products will be computed.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.01   0.00   0.00   0.00         0.    0.0000
    2   25    0   0.02   0.00   0.00   0.01         0.    0.0000
    3   26    0   0.02   0.00   0.00   0.02         0.    0.0000
    4   11    0   0.01   0.00   0.00   0.01         0.    0.0000
    5   15    0   0.02   0.00   0.00   0.02         0.    0.0000
    6   16    0   0.02   0.01   0.00   0.02         0.    0.0000
    7    1    0   0.01   0.00   0.00   0.01         0.    0.0000
    8    5    0   0.02   0.00   0.00   0.02         0.    0.0000
    9    6    0   0.02   0.00   0.00   0.02         0.    0.0000
   10    7    0   0.03   0.00   0.00   0.03         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.01   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.01   0.00   0.00   0.01         0.    0.0000
   14   47    0   0.02   0.00   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.019999
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.2200s 
time spent in multnx:                   0.1900s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0100s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            3.8300s 

          starting ci iteration   7

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.35329492

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=      10  D0X=      30  D0W=      50
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================

          ci-one-electron density block   1

                mo   1         mo   2         mo   3         mo   4         mo   5         mo   6         mo   7         mo   8
   mo   1     1.99991353
   mo   2    -0.00020984     1.98513888
   mo   3     0.00000000    -0.00000001     1.96967428
   mo   4    -0.00015683     0.00410232    -0.00000012     1.97320944
   mo   5     0.00000000    -0.00000007    -0.00000001    -0.00000008     1.97612193
   mo   6     0.00012535    -0.00358657    -0.00001973    -0.02112534    -0.00000827     0.00694862
   mo   7     0.00000000     0.00000768    -0.01065703    -0.00002319    -0.00000489    -0.00000004     0.00750677
   mo   8    -0.00000001     0.00001570    -0.01300759    -0.00002169     0.00000237    -0.00000017     0.00716768     0.01060894
   mo   9     0.00005965     0.00325371    -0.00001201    -0.00045951     0.00000193     0.00706860    -0.00000023    -0.00000034
   mo  10    -0.00022129     0.01044454    -0.00001398     0.00346740     0.00000010     0.00262586    -0.00000019    -0.00000013
   mo  11     0.00000004    -0.00000175     0.00000296    -0.00000043    -0.01176106     0.00000022     0.00000009    -0.00000009
   mo  12    -0.00000002    -0.00000287    -0.00555032    -0.00000599    -0.00000248     0.00000006     0.00612131     0.00407024
   mo  13     0.00020489    -0.00106826    -0.00001268    -0.00837796    -0.00000310     0.00195785    -0.00000003    -0.00000017
   mo  14     0.00000000     0.00000214    -0.00000326    -0.00000115    -0.00000190    -0.00000003    -0.00000003    -0.00000002
   mo  15     0.00000000     0.00000111    -0.00000190    -0.00000496    -0.00354757     0.00000002    -0.00000001    -0.00000001
   mo  16    -0.00038996    -0.00015937    -0.00000465     0.00281811     0.00000173    -0.00194925    -0.00000011    -0.00000005
   mo  17    -0.00000001    -0.00000903     0.00102227     0.00002170     0.00000049    -0.00000003     0.00027288     0.00024564
   mo  18     0.00000004    -0.00000061     0.01150281    -0.00000057     0.00000089    -0.00000012    -0.00286841    -0.00440565
   mo  19    -0.00010564    -0.00443698    -0.00000012     0.00752013     0.00000041    -0.00311375    -0.00000014    -0.00000008
   mo  20     0.00000000     0.00000021    -0.00000049     0.00000139     0.00206775    -0.00000003    -0.00000001     0.00000000
   mo  21     0.00000000    -0.00000023     0.00000059    -0.00000061    -0.00000127     0.00000001    -0.00000001    -0.00000002
   mo  22     0.00001083     0.00013102     0.00000264    -0.00122089     0.00000029    -0.00005728     0.00000004     0.00000003
   mo  23     0.00004968     0.00173895    -0.00000044     0.00029677    -0.00000049     0.00063456     0.00000000    -0.00000002
   mo  24    -0.00000001    -0.00000070    -0.00029134     0.00000189     0.00000008     0.00000000    -0.00082810     0.00035423

                mo   9         mo  10         mo  11         mo  12         mo  13         mo  14         mo  15         mo  16
   mo   9     0.01008908
   mo  10     0.00334450     0.01015092
   mo  11    -0.00000006    -0.00000002     0.01457132
   mo  12     0.00000003     0.00000004     0.00000002     0.00654499
   mo  13     0.00052115    -0.00065038     0.00000006    -0.00000002     0.00374998
   mo  14    -0.00000003     0.00000000     0.00000009    -0.00000002     0.00000000     0.00298756
   mo  15    -0.00000003    -0.00000006    -0.00138701    -0.00000001     0.00000003    -0.00000006     0.00225429
   mo  16     0.00003267     0.00265683    -0.00000003    -0.00000007    -0.00125325     0.00000002    -0.00000002     0.00440429
   mo  17     0.00000019     0.00000027    -0.00000001     0.00035881    -0.00000003     0.00000000     0.00000000     0.00000007
   mo  18    -0.00000014    -0.00000017     0.00000001    -0.00184211     0.00000001     0.00000000     0.00000000    -0.00000003
   mo  19    -0.00437770    -0.00082595     0.00000000    -0.00000013    -0.00084177     0.00000000    -0.00000001    -0.00006185
   mo  20    -0.00000001     0.00000000     0.00004623     0.00000000    -0.00000001     0.00000000    -0.00183588     0.00000000
   mo  21     0.00000000    -0.00000001     0.00000003     0.00000000     0.00000001    -0.00203367     0.00000000    -0.00000001
   mo  22    -0.00056430     0.00030094    -0.00000001     0.00000002     0.00173225     0.00000000     0.00000000     0.00070773
   mo  23     0.00023597     0.00178709     0.00000000     0.00000003     0.00024963     0.00000000     0.00000000    -0.00068963
   mo  24     0.00000001     0.00000000     0.00000001    -0.00169262     0.00000001     0.00000000     0.00000000     0.00000003

                mo  17         mo  18         mo  19         mo  20         mo  21         mo  22         mo  23         mo  24
   mo  17     0.00051650
   mo  18    -0.00020437     0.00262826
   mo  19     0.00000002     0.00000006     0.00289079
   mo  20     0.00000000     0.00000000     0.00000001     0.00256588
   mo  21     0.00000000     0.00000001     0.00000000     0.00000001     0.00243717
   mo  22    -0.00000001     0.00000001    -0.00016296     0.00000000     0.00000000     0.00198461
   mo  23     0.00000004    -0.00000002     0.00061778     0.00000000     0.00000000    -0.00008852     0.00164717
   mo  24     0.00000375    -0.00057921     0.00000002     0.00000000     0.00000000    -0.00000001    -0.00000001     0.00145480


*****   symmetry block SYM1   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       1.99991353
       2      -0.00020984     1.98513888
       3       0.00000000    -0.00000001     1.96967428
       4      -0.00015683     0.00410232    -0.00000012     1.97320944
       5       0.00000000    -0.00000007    -0.00000001    -0.00000008
       6       0.00012535    -0.00358657    -0.00001973    -0.02112534
       7       0.00000000     0.00000768    -0.01065703    -0.00002319
       8      -0.00000001     0.00001570    -0.01300759    -0.00002169
       9       0.00005965     0.00325371    -0.00001201    -0.00045951
      10      -0.00022129     0.01044454    -0.00001398     0.00346740
      11       0.00000004    -0.00000175     0.00000296    -0.00000043
      12      -0.00000002    -0.00000287    -0.00555032    -0.00000599
      13       0.00020489    -0.00106826    -0.00001268    -0.00837796
      14       0.00000000     0.00000214    -0.00000326    -0.00000115
      15       0.00000000     0.00000111    -0.00000190    -0.00000496
      16      -0.00038996    -0.00015937    -0.00000465     0.00281811
      17      -0.00000001    -0.00000903     0.00102227     0.00002170
      18       0.00000004    -0.00000061     0.01150281    -0.00000057
      19      -0.00010564    -0.00443698    -0.00000012     0.00752013
      20       0.00000000     0.00000021    -0.00000049     0.00000139
      21       0.00000000    -0.00000023     0.00000059    -0.00000061
      22       0.00001083     0.00013102     0.00000264    -0.00122089
      23       0.00004968     0.00173895    -0.00000044     0.00029677
      24      -0.00000001    -0.00000070    -0.00029134     0.00000189

               Column   5     Column   6     Column   7     Column   8
       5       1.97612193
       6      -0.00000827     0.00694862
       7      -0.00000489    -0.00000004     0.00750677
       8       0.00000237    -0.00000017     0.00716768     0.01060894
       9       0.00000193     0.00706860    -0.00000023    -0.00000034
      10       0.00000010     0.00262586    -0.00000019    -0.00000013
      11      -0.01176106     0.00000022     0.00000009    -0.00000009
      12      -0.00000248     0.00000006     0.00612131     0.00407024
      13      -0.00000310     0.00195785    -0.00000003    -0.00000017
      14      -0.00000190    -0.00000003    -0.00000003    -0.00000002
      15      -0.00354757     0.00000002    -0.00000001    -0.00000001
      16       0.00000173    -0.00194925    -0.00000011    -0.00000005
      17       0.00000049    -0.00000003     0.00027288     0.00024564
      18       0.00000089    -0.00000012    -0.00286841    -0.00440565
      19       0.00000041    -0.00311375    -0.00000014    -0.00000008
      20       0.00206775    -0.00000003    -0.00000001     0.00000000
      21      -0.00000127     0.00000001    -0.00000001    -0.00000002
      22       0.00000029    -0.00005728     0.00000004     0.00000003
      23      -0.00000049     0.00063456     0.00000000    -0.00000002
      24       0.00000008     0.00000000    -0.00082810     0.00035423

               Column   9     Column  10     Column  11     Column  12
       9       0.01008908
      10       0.00334450     0.01015092
      11      -0.00000006    -0.00000002     0.01457132
      12       0.00000003     0.00000004     0.00000002     0.00654499
      13       0.00052115    -0.00065038     0.00000006    -0.00000002
      14      -0.00000003     0.00000000     0.00000009    -0.00000002
      15      -0.00000003    -0.00000006    -0.00138701    -0.00000001
      16       0.00003267     0.00265683    -0.00000003    -0.00000007
      17       0.00000019     0.00000027    -0.00000001     0.00035881
      18      -0.00000014    -0.00000017     0.00000001    -0.00184211
      19      -0.00437770    -0.00082595     0.00000000    -0.00000013
      20      -0.00000001     0.00000000     0.00004623     0.00000000
      21       0.00000000    -0.00000001     0.00000003     0.00000000
      22      -0.00056430     0.00030094    -0.00000001     0.00000002
      23       0.00023597     0.00178709     0.00000000     0.00000003
      24       0.00000001     0.00000000     0.00000001    -0.00169262

               Column  13     Column  14     Column  15     Column  16
      13       0.00374998
      14       0.00000000     0.00298756
      15       0.00000003    -0.00000006     0.00225429
      16      -0.00125325     0.00000002    -0.00000002     0.00440429
      17      -0.00000003     0.00000000     0.00000000     0.00000007
      18       0.00000001     0.00000000     0.00000000    -0.00000003
      19      -0.00084177     0.00000000    -0.00000001    -0.00006185
      20      -0.00000001     0.00000000    -0.00183588     0.00000000
      21       0.00000001    -0.00203367     0.00000000    -0.00000001
      22       0.00173225     0.00000000     0.00000000     0.00070773
      23       0.00024963     0.00000000     0.00000000    -0.00068963
      24       0.00000001     0.00000000     0.00000000     0.00000003

               Column  17     Column  18     Column  19     Column  20
      17       0.00051650
      18      -0.00020437     0.00262826
      19       0.00000002     0.00000006     0.00289079
      20       0.00000000     0.00000000     0.00000001     0.00256588
      21       0.00000000     0.00000001     0.00000000     0.00000001
      22      -0.00000001     0.00000001    -0.00016296     0.00000000
      23       0.00000004    -0.00000002     0.00061778     0.00000000
      24       0.00000375    -0.00057921     0.00000002     0.00000000

               Column  21     Column  22     Column  23     Column  24
      21       0.00243717
      22       0.00000000     0.00198461
      23       0.00000000    -0.00008852     0.00164717
      24       0.00000000    -0.00000001    -0.00000001     0.00145480

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7        eno   8
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    8   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno   9        eno  10        eno  11        eno  12        eno  13        eno  14        eno  15        eno  16
 emo    9   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   10   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   11   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   12   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   13   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   14   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   15   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   16   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno  17        eno  18        eno  19        eno  20        eno  21        eno  22        eno  23        eno  24
 emo   17   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   18   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   19   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   20   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   21   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   22   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   23   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   24   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   1.9999186      1.9865325      1.9762010      1.9721927      1.9699035
   0.0215017      0.0197159      0.0146665      0.0103436      0.0054545
   0.0051879      0.0047646      0.0042423      0.0041516      0.0010183
   0.0010158      0.0006602      0.0005726      0.0004952      0.0004944
   0.0004503      0.0004327      0.0000501      0.0000336

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7         no   8
  mo    1   0.999821E+00   0.186520E-01  -0.617638E-07   0.320680E-02  -0.656645E-06   0.289796E-07  -0.340963E-04  -0.233859E-07
  mo    2  -0.168009E-01   0.952753E+00  -0.957878E-06  -0.303215E+00   0.645623E-04  -0.553456E-05  -0.299851E-02   0.106074E-05
  mo    3   0.106983E-06  -0.459501E-05   0.237625E-06   0.198134E-03   0.999941E+00   0.102019E-01   0.171435E-04  -0.164931E-05
  mo    4  -0.870822E-02   0.303069E+00   0.116804E-04   0.952848E+00  -0.187682E-03   0.120472E-04   0.667620E-02  -0.104411E-08
  mo    5   0.148876E-06  -0.265829E-05   0.999980E+00  -0.114745E-04  -0.239838E-06   0.134289E-05   0.184683E-05   0.572088E-02
  mo    6   0.185588E-03  -0.494683E-02  -0.432352E-05  -0.970392E-02  -0.817949E-05  -0.226126E-03   0.533463E+00   0.521697E-05
  mo    7   0.342733E-07   0.179321E-06  -0.248647E-05  -0.135696E-04  -0.547207E-02   0.569634E+00   0.226042E-03   0.388369E-05
  mo    8  -0.443412E-07   0.427192E-05   0.119330E-05  -0.143326E-04  -0.667765E-02   0.636324E+00   0.241739E-03  -0.781010E-05
  mo    9   0.494834E-05   0.149213E-02   0.960496E-06  -0.771269E-03  -0.601497E-05  -0.269755E-03   0.671902E+00  -0.135771E-04
  mo   10  -0.214603E-03   0.556284E-02   0.648275E-07   0.562959E-04  -0.714300E-05  -0.165425E-03   0.410218E+00  -0.130765E-04
  mo   11   0.363015E-07  -0.897378E-06  -0.599414E-02   0.133941E-06   0.151660E-05   0.258085E-05   0.119879E-04   0.992992E+00
  mo   12   0.424803E-07  -0.227681E-05  -0.126587E-05  -0.309083E-05  -0.286298E-02   0.441233E+00   0.199218E-03   0.989113E-06
  mo   13   0.148578E-03  -0.179809E-02  -0.162393E-05  -0.390374E-02  -0.568863E-05  -0.521147E-04   0.831779E-01   0.534332E-05
  mo   14  -0.132668E-07   0.851403E-06  -0.960780E-06  -0.886115E-06  -0.165967E-05  -0.363327E-05  -0.293476E-05   0.714295E-05
  mo   15   0.112543E-07  -0.220699E-06  -0.179393E-02  -0.254953E-05  -0.967221E-06  -0.209116E-05  -0.504113E-05  -0.115904E+00
  mo   16  -0.206896E-03   0.363780E-03   0.900746E-06   0.140036E-02  -0.263701E-05  -0.329767E-05  -0.262169E-02  -0.702402E-05
  mo   17  -0.255379E-07  -0.102357E-05   0.249117E-06   0.119725E-04   0.516324E-03   0.255283E-01   0.324176E-04  -0.635466E-06
  mo   18   0.284347E-07  -0.413844E-06   0.455294E-06   0.102670E-05   0.587234E-02  -0.270951E+00  -0.124506E-03   0.173150E-05
  mo   19  -0.486199E-04  -0.979949E-03   0.261696E-06   0.434028E-02  -0.890257E-06   0.105202E-03  -0.291828E+00   0.400170E-05
  mo   20  -0.660189E-08   0.311542E-06   0.104919E-02   0.631562E-06  -0.250203E-06  -0.450527E-07  -0.234492E-06   0.223563E-01
  mo   21   0.584301E-08  -0.205708E-06  -0.643283E-06  -0.257346E-06   0.300885E-06  -0.332955E-06   0.161677E-06   0.288739E-06
  mo   22   0.965952E-05  -0.124284E-03   0.140739E-06  -0.613376E-03   0.145743E-05   0.353457E-05  -0.621844E-02  -0.778474E-07
  mo   23   0.888440E-05   0.883437E-03  -0.250870E-06  -0.126759E-03  -0.207024E-06  -0.228406E-04   0.592051E-01  -0.137096E-05
  mo   24  -0.735388E-08  -0.414947E-07   0.451816E-07   0.998432E-06  -0.146159E-03  -0.418561E-01  -0.195725E-04  -0.158038E-06

                no   9         no  10         no  11         no  12         no  13         no  14         no  15         no  16
  mo    1   0.228943E-03  -0.317000E-07  -0.983225E-04  -0.117322E-08   0.440962E-04   0.259583E-09  -0.831806E-07   0.191506E-04
  mo    2  -0.407057E-02   0.433297E-05  -0.318589E-03  -0.886517E-06   0.360425E-03   0.232058E-06  -0.849352E-05   0.101383E-02
  mo    3   0.126653E-05  -0.235270E-02   0.550051E-05   0.144422E-05   0.149410E-05  -0.321080E-06  -0.150721E-02  -0.470821E-05
  mo    4  -0.650031E-02  -0.245960E-06   0.393821E-02   0.357657E-06   0.145692E-02  -0.216376E-05   0.161779E-04  -0.264950E-02
  mo    5  -0.171776E-05   0.185949E-05   0.235325E-05   0.185739E-06  -0.390440E-06  -0.251940E-02  -0.925944E-06  -0.102275E-05
  mo    6  -0.257706E+00   0.396986E-04   0.190239E+00   0.278555E-05  -0.125294E+00  -0.344160E-05   0.432637E-03  -0.133628E+00
  mo    7  -0.114072E-04   0.219397E+00  -0.620953E-04   0.323616E-05   0.523792E-05  -0.602134E-06   0.403434E-01   0.145049E-03
  mo    8   0.558497E-05  -0.546028E+00   0.170586E-03  -0.423664E-05   0.534002E-05  -0.100926E-05   0.401609E+00   0.130711E-02
  mo    9  -0.172183E+00  -0.123649E-03  -0.365146E+00  -0.287072E-05   0.462241E-01   0.669596E-06  -0.130149E-02   0.401622E+00
  mo   10   0.766298E+00   0.974412E-04   0.317780E+00   0.178778E-05  -0.144060E+00  -0.659054E-05   0.947795E-03  -0.284634E+00
  mo   11   0.125208E-04  -0.613218E-05  -0.791859E-05  -0.854493E-05   0.346688E-05  -0.932016E-01   0.188358E-05   0.326663E-05
  mo   12  -0.578840E-05   0.639145E+00  -0.183100E-03   0.865708E-05  -0.281461E-05   0.388812E-06  -0.290776E+00  -0.951696E-03
  mo   13  -0.254789E+00   0.185966E-03   0.605526E+00   0.185915E-04   0.447918E+00   0.913921E-05  -0.213723E-03   0.571160E-01
  mo   14   0.327077E-05  -0.850143E-05  -0.133729E-04   0.753026E+00  -0.877868E-05  -0.173597E-04  -0.618282E-05   0.903430E-08
  mo   15  -0.633916E-05   0.160083E-05   0.234236E-06  -0.250471E-04   0.127755E-04  -0.653774E+00  -0.103386E-05  -0.551572E-06
  mo   16   0.467018E+00  -0.100093E-03  -0.363362E+00  -0.713676E-06   0.558389E+00   0.777565E-05  -0.949095E-03   0.282486E+00
  mo   17   0.125753E-04   0.188549E-01   0.110964E-04   0.223555E-05   0.187016E-05   0.418686E-06  -0.247933E+00  -0.837986E-03
  mo   18  -0.660337E-05   0.284015E+00  -0.973854E-04   0.274343E-05  -0.280460E-05   0.127905E-05   0.617097E+00   0.200803E-02
  mo   19   0.152329E+00   0.575051E-04   0.185429E+00  -0.149929E-05  -0.277353E+00  -0.643645E-05  -0.144524E-02   0.427944E+00
  mo   20   0.195508E-05  -0.155364E-05   0.898295E-07   0.175301E-04  -0.140503E-04   0.750924E+00  -0.197594E-05   0.148972E-05
  mo   21  -0.187206E-05   0.779630E-05   0.113924E-04  -0.657991E+00   0.974089E-05   0.262357E-04  -0.707021E-05   0.142377E-05
  mo   22   0.245840E-01   0.960796E-04   0.318480E+00   0.114226E-04   0.522706E+00   0.997293E-05  -0.387748E-03   0.136105E+00
  mo   23   0.991836E-01   0.933638E-04   0.308172E+00   0.176785E-05  -0.314378E+00  -0.757884E-05  -0.216916E-02   0.674602E+00
  mo   24   0.265025E-05  -0.405200E+00   0.126896E-03  -0.531091E-05   0.807267E-05  -0.149728E-05  -0.556991E+00  -0.181472E-02

                no  17         no  18         no  19         no  20         no  21         no  22         no  23         no  24
  mo    1  -0.166545E-08  -0.254701E-07   0.419491E-04   0.438805E-07   0.393965E-07  -0.352421E-04   0.261279E-04   0.481467E-08
  mo    2  -0.661927E-06  -0.351861E-05   0.254708E-02   0.228938E-05   0.528849E-05  -0.192586E-02   0.171610E-02  -0.790241E-07
  mo    3   0.887274E-06  -0.556655E-03   0.185452E-05   0.826745E-06  -0.162440E-02  -0.775569E-05   0.108326E-05   0.163390E-02
  mo    4   0.604282E-06   0.716883E-05   0.176747E-02   0.334725E-05  -0.552770E-05  -0.186957E-02   0.599827E-02   0.114757E-05
  mo    5   0.963395E-06   0.131156E-05  -0.477950E-07   0.108303E-02  -0.132437E-06  -0.128291E-05   0.231878E-05   0.118178E-05
  mo    6  -0.587342E-05  -0.159840E-03   0.468180E+00   0.506425E-03   0.301380E-03  -0.213372E+00   0.560731E+00   0.572837E-04
  mo    7  -0.173522E-04   0.563367E+00   0.109484E-03  -0.593994E-05   0.186786E+00   0.432638E-04  -0.489131E-04   0.522928E+00
  mo    8   0.127318E-04  -0.186149E+00  -0.652645E-04   0.155403E-05   0.525734E-01   0.265796E-04   0.277931E-04  -0.313358E+00
  mo    9   0.154347E-05   0.717711E-05  -0.978759E-02  -0.108032E-04  -0.110453E-04   0.256522E-01  -0.470504E+00  -0.512355E-04
  mo   10   0.188048E-05   0.437469E-04  -0.542611E-01  -0.555335E-04  -0.432696E-04   0.102866E-01  -0.196652E+00  -0.159141E-04
  mo   11  -0.138725E-04  -0.391079E-05  -0.822968E-04   0.724140E-01   0.116715E-05   0.311701E-05  -0.677608E-05  -0.325061E-05
  mo   12   0.335967E-05  -0.145687E+00   0.452231E-05   0.345095E-05  -0.125073E+00  -0.542742E-04   0.542959E-04  -0.524766E+00
  mo   13   0.638507E-05   0.264144E-04  -0.335181E+00  -0.367428E-03   0.261544E-03  -0.460066E+00  -0.183261E+00  -0.256491E-04
  mo   14   0.657991E+00   0.238138E-04   0.110267E-04   0.945513E-04  -0.465117E-05   0.232132E-05   0.162049E-05   0.116243E-05
  mo   15  -0.947650E-04   0.465133E-05  -0.807119E-03   0.747758E+00   0.146219E-04  -0.713591E-05  -0.262775E-05   0.203656E-06
  mo   16  -0.168177E-05  -0.657600E-04   0.261551E-01   0.268590E-04   0.275155E-03  -0.330971E+00   0.384746E+00   0.334982E-04
  mo   17   0.153941E-04  -0.379963E+00  -0.383999E-03  -0.154570E-04   0.887357E+00   0.789165E-03   0.292006E-06   0.758029E-01
  mo   18  -0.569154E-05   0.384848E+00   0.673144E-05  -0.871740E-05   0.374854E+00   0.195682E-03   0.409526E-04  -0.420060E+00
  mo   19  -0.784495E-05  -0.239577E-03   0.578038E+00   0.618265E-03   0.514998E-03  -0.406630E+00  -0.312552E+00  -0.340189E-04
  mo   20  -0.953214E-04   0.455396E-05  -0.712054E-03   0.660009E+00   0.123005E-04  -0.706401E-05  -0.152505E-05  -0.102218E-06
  mo   21   0.753026E+00   0.249524E-04   0.104473E-04   0.963848E-04  -0.292079E-05   0.165615E-05   0.620447E-06   0.860671E-06
  mo   22  -0.937826E-05  -0.332672E-04   0.445517E+00   0.486634E-03  -0.372804E-03   0.634416E+00  -0.721872E-01   0.298142E-06
  mo   23   0.410461E-05   0.138304E-03  -0.363472E+00  -0.388405E-03  -0.304191E-03   0.254298E+00   0.375475E+00   0.320793E-04
  mo   24  -0.238469E-04   0.578153E+00   0.120668E-03  -0.739082E-05   0.137051E+00   0.715722E-04   0.475501E-04  -0.413248E+00
  eigenvectors of nos in ao-basis, column    1
  0.1000604E+01 -0.1371673E-01 -0.1680030E-02  0.6471968E-07  0.1871613E-06 -0.7397380E-02  0.8709664E-07 -0.5501875E-07
  0.1520487E-03  0.7975641E-08 -0.1158589E-07  0.4234440E-04  0.1555345E-07  0.9065260E-04 -0.3254195E-02  0.2141419E-02
 -0.1013317E-02  0.4010002E-08 -0.9340746E-03 -0.3254308E-02  0.2141562E-02  0.1013338E-02  0.1776405E-07 -0.9341284E-03
  eigenvectors of nos in ao-basis, column    2
  0.9497294E-02  0.9131303E+00 -0.7156440E-01 -0.3176691E-05 -0.3690432E-05  0.1225984E+00 -0.4958833E-05  0.1351584E-05
  0.7745066E-01 -0.3303362E-06  0.4136348E-06 -0.1190269E-02 -0.2184983E-06  0.1116938E-02  0.2178490E+00 -0.1065249E+00
  0.2711065E-01  0.3736212E-06  0.2157215E-01  0.2178599E+00 -0.1065323E+00 -0.2711281E-01 -0.6380760E-06  0.2157045E-01
  eigenvectors of nos in ao-basis, column    3
 -0.8174016E-06 -0.2364651E-05  0.3175749E-05  0.2663995E-05  0.9105003E+00  0.1003949E-04 -0.2441156E-05  0.8287902E-01
  0.1803351E-05 -0.5659196E-06 -0.1688958E-01 -0.9288874E-07 -0.1363372E-06 -0.2600497E-06 -0.7396884E-05  0.1026039E-04
  0.7757177E-06  0.3055157E-01 -0.3376208E-06 -0.2127494E-05  0.6541370E-06 -0.3041389E-06  0.3055338E-01 -0.5317105E-06
  eigenvectors of nos in ao-basis, column    4
  0.1188265E-02  0.1479692E-02  0.2165340E+00  0.1583968E-03 -0.1017936E-04  0.8024236E+00 -0.2916703E-04  0.2605018E-06
 -0.1982157E-01 -0.1617124E-06  0.1277333E-05 -0.4712329E-02 -0.5470260E-05 -0.2410526E-02 -0.4289058E+00  0.1831734E+00
 -0.3801979E-01 -0.2776228E-05  0.2256006E-02 -0.4287143E+00  0.1830972E+00  0.3802123E-01 -0.1363934E-05  0.2288569E-02
  eigenvectors of nos in ao-basis, column    5
  0.3528899E-06  0.1862925E-05 -0.4975512E-04  0.7309079E+00  0.1980239E-05 -0.1456534E-03 -0.1180441E+00 -0.1917022E-05
  0.1563255E-06  0.5365900E-06 -0.1092984E-06  0.7920083E-06 -0.2541481E-01  0.8264295E-06 -0.5518182E+00  0.1930264E+00
 -0.2038310E-01 -0.1870972E-05 -0.3129097E-01  0.5520113E+00 -0.1931028E+00 -0.2040074E-01  0.1774694E-06  0.3127844E-01
  eigenvectors of nos in ao-basis, column    6
 -0.4203581E-04 -0.3313199E-03  0.5908877E-04 -0.1306893E+01  0.5011301E-05  0.4891709E-03  0.5816167E+00 -0.2994171E-05
 -0.2825780E-03  0.1113655E-06  0.2512489E-06  0.1813691E-05 -0.6560860E-01  0.1204744E-04 -0.1004195E+01  0.3580869E+00
 -0.3987888E-01 -0.4194249E-05 -0.1792301E-01  0.1004972E+01 -0.3584078E+00 -0.3991433E-01  0.1045973E-05  0.1789501E-01
  eigenvectors of nos in ao-basis, column    7
  0.1134583E+00  0.8472480E+00 -0.1619111E+00 -0.5536074E-03  0.1893004E-04 -0.1213807E+01  0.2712300E-03 -0.1699576E-04
  0.7604836E+00  0.5516995E-06  0.4742418E-06 -0.4128164E-02 -0.2557228E-04 -0.3154063E-01 -0.9729508E+00  0.4144391E+00
 -0.4994131E-01 -0.5810499E-05 -0.9488439E-02 -0.9721453E+00  0.4141435E+00  0.4991514E-01 -0.1905974E-05 -0.9426139E-02
  eigenvectors of nos in ao-basis, column    8
  0.3961588E-05  0.1940675E-04 -0.2161428E-04  0.1265054E-05  0.1412849E+01  0.2630207E-04  0.2688639E-05 -0.1611395E+01
 -0.2605637E-04 -0.6112308E-06  0.3745098E-01  0.1647439E-06  0.1449721E-05  0.7605612E-06  0.3397049E-04 -0.3471186E-04
 -0.7558198E-07 -0.7464881E-01  0.1880873E-05  0.3800620E-05 -0.9892707E-06  0.1083718E-05 -0.7465884E-01  0.1347392E-05
  eigenvectors of nos in ao-basis, column    9
 -0.4410681E+00 -0.2259127E+01  0.2142562E+01  0.3379761E-05  0.1650856E-04 -0.7420620E+00 -0.1619266E-04 -0.1808544E-04
  0.1096146E+01 -0.2429024E-05  0.3034277E-05 -0.1288906E-01 -0.5517765E-05  0.6185983E-01  0.3794748E+00 -0.2107984E+00
 -0.1263174E-01 -0.2547123E-05  0.7579556E-01  0.3795151E+00 -0.2108443E+00  0.1263533E-01 -0.5609095E-05  0.7581786E-01
  eigenvectors of nos in ao-basis, column   10
  0.8053333E-04  0.3545312E-03 -0.5026821E-03 -0.4941588E+00 -0.7053403E-05 -0.4396080E-05  0.1039066E+01  0.1001813E-04
  0.5792004E-04  0.9455780E-05 -0.1887703E-05 -0.3012327E-04  0.6158190E+00  0.8025176E-04  0.6591318E+00 -0.5007754E+00
  0.1117497E+00 -0.2235019E-05 -0.2170934E-01 -0.6587283E+00  0.5005482E+00  0.1118049E+00  0.3490028E-05  0.2184816E-01
  eigenvectors of nos in ao-basis, column   11
  0.2887613E+00  0.1289534E+01 -0.1774707E+01  0.1273209E-03 -0.9014456E-05 -0.4034094E-01 -0.3026035E-03  0.1334685E-04
  0.2184702E+00  0.1393980E-04  0.4810533E-07 -0.9844736E-01 -0.1953681E-03  0.2641793E+00  0.6415986E+00 -0.3793591E+00
 -0.6785864E-01 -0.4586915E-05  0.2206770E+00  0.6420254E+00 -0.3796791E+00  0.6780255E-01  0.5082911E-05  0.2206907E+00
  eigenvectors of nos in ao-basis, column   12
  0.5321212E-05  0.2541079E-04 -0.2725244E-04 -0.8187228E-05 -0.1007165E-04  0.5495751E-06  0.1157437E-04  0.2365639E-04
 -0.6686855E-05 -0.8026269E+00  0.2236889E-04 -0.4178481E-05  0.7491940E-05  0.1848130E-05 -0.1052477E-05 -0.3369311E-07
 -0.2245952E-05  0.2659555E+00  0.3598254E-05 -0.3836283E-05  0.3818154E-05  0.6207662E-05 -0.2659800E+00  0.7097427E-05
  eigenvectors of nos in ao-basis, column   13
 -0.1294843E+00 -0.5487220E+00  0.8974226E+00  0.2243380E-05  0.3571770E-05  0.9852864E-01 -0.4580078E-05 -0.8935148E-05
 -0.5129614E+00  0.1157667E-04 -0.1674254E-04 -0.2104924E+00 -0.1159278E-04 -0.2137662E+00 -0.4708477E+00  0.2707879E+00
 -0.1641811E+00  0.1831121E-05  0.9223537E-01 -0.4708501E+00  0.2707999E+00  0.1641876E+00  0.6454436E-05  0.9223285E-01
  eigenvectors of nos in ao-basis, column   14
  0.2962981E-06  0.1325376E-05 -0.1727998E-06  0.5137735E-06 -0.8452190E-01  0.5227421E-05 -0.6450220E-06  0.3033939E+00
 -0.1316540E-04  0.3036996E-04  0.8902004E+00 -0.3976559E-05  0.2256710E-05 -0.4978141E-05 -0.5522543E-05  0.5725440E-05
 -0.2160123E-05 -0.1992763E+00  0.8846282E-06 -0.4758985E-05  0.4318903E-05  0.2689731E-05 -0.1992725E+00  0.1617738E-05
  eigenvectors of nos in ao-basis, column   15
  0.1195357E-02  0.4552595E-02 -0.8915601E-02  0.4971930E+00  0.1859063E-05 -0.4807759E-03 -0.1291892E+01 -0.1988043E-05
  0.3867549E-02 -0.6784182E-05 -0.1892326E-05  0.8144867E-04  0.8084196E+00 -0.1335121E-02 -0.1400576E+01  0.7256395E+00
 -0.7388038E-01 -0.8556921E-05  0.2555438E+00  0.1415238E+01 -0.7338861E+00 -0.7434080E-01  0.5317762E-05 -0.2546450E+00
  eigenvectors of nos in ao-basis, column   16
 -0.3569461E+00 -0.1353146E+01  0.2704205E+01  0.1615929E-02  0.3712432E-05  0.1222892E+00 -0.4196034E-02 -0.5732009E-05
 -0.1152437E+01  0.1525510E-05  0.1650388E-05 -0.2990088E-01  0.2635096E-02  0.4147002E+00 -0.2257634E+01  0.1270153E+01
 -0.7468205E-01  0.7150127E-06 -0.1586695E+00 -0.2248496E+01  0.1265461E+01  0.7416288E-01 -0.3826304E-06 -0.1603680E+00
  eigenvectors of nos in ao-basis, column   17
  0.4060247E-05  0.1939527E-04 -0.6407319E-05 -0.2501658E-04 -0.1302059E-04 -0.1000948E-04 -0.1979821E-04  0.1106133E-03
 -0.4192126E-05  0.7226291E+00 -0.8448919E-04  0.2760274E-05  0.2838788E-04  0.8586185E-06 -0.9975709E-04  0.8190640E-04
 -0.2172104E-04  0.7384041E+00 -0.2178076E-04  0.5542522E-04 -0.4850877E-04  0.3450919E-05 -0.7386279E+00  0.2342741E-04
  eigenvectors of nos in ao-basis, column   18
  0.1157903E-03  0.5270034E-03 -0.1927867E-03  0.8928634E+00 -0.4585495E-05 -0.3080286E-03  0.1293609E+00  0.2447592E-05
  0.1128205E-03  0.2368594E-04  0.4011818E-05  0.1079755E-04 -0.6598186E+00  0.5403028E-04  0.1763031E+01 -0.1624010E+01
  0.3839424E+00  0.3130943E-04  0.6961756E+00 -0.1763822E+01  0.1624625E+01  0.3844484E+00 -0.2039308E-04 -0.6964980E+00
  eigenvectors of nos in ao-basis, column   19
 -0.2104047E+00 -0.1029990E+01 -0.1790092E+00  0.6694691E-04 -0.6721653E-04  0.6945661E+00  0.4312233E-03  0.8488474E-03
  0.3558401E+00  0.9780869E-05 -0.6152385E-03 -0.1289591E+00 -0.1382529E-03 -0.1159078E+00  0.1595618E+01 -0.1108909E+01
  0.9045624E+00 -0.9007787E-03  0.1444389E+00  0.1594419E+01 -0.1108056E+01 -0.9047856E+00 -0.9238754E-03  0.1438058E+00
  eigenvectors of nos in ao-basis, column   20
 -0.2256052E-03 -0.1104639E-02 -0.1894755E-03 -0.1780624E-04  0.5783094E-01  0.7390102E-03  0.2191967E-04 -0.7799917E+00
  0.3972464E-03  0.9116234E-04  0.5703272E+00 -0.1407934E-03  0.8513385E-05 -0.1229675E-03  0.1710322E-02 -0.1184596E-02
  0.9572317E-03  0.8454732E+00  0.1493467E-03  0.1728235E-02 -0.1211589E-02 -0.9914339E-03  0.8452700E+00  0.1431597E-03
  eigenvectors of nos in ao-basis, column   21
 -0.3130378E-03 -0.1352115E-02  0.9531870E-03  0.6191999E+00  0.6345362E-06  0.6322266E-03 -0.1254308E+01 -0.1464835E-04
 -0.7517192E-03 -0.2532292E-05  0.1052299E-04  0.1018019E-03 -0.1495774E+00 -0.1577990E-03 -0.3071076E+00 -0.1110913E+00
  0.7479841E+00  0.1197335E-04 -0.3512797E+00  0.3076682E+00  0.1104256E+00  0.7475055E+00  0.2058310E-04  0.3526509E+00
  eigenvectors of nos in ao-basis, column   22
  0.3028186E+00  0.1264076E+01 -0.1215239E+01  0.3346882E-03  0.3640960E-05 -0.5043321E+00 -0.9048774E-03  0.1181069E-05
  0.1028876E+01  0.1476605E-05 -0.6236547E-05 -0.1752067E+00 -0.8944378E-04  0.1611462E+00  0.2048969E+00  0.3626783E-01
  0.5430460E-01 -0.6173929E-05 -0.8068589E+00  0.2056308E+00  0.3606024E-01 -0.5320018E-01 -0.1062216E-04 -0.8060477E+00
  eigenvectors of nos in ao-basis, column   23
 -0.3248843E+00 -0.1122478E+01  0.3943543E+01  0.3832267E-04 -0.7255957E-05 -0.3585700E+00  0.1607621E-03  0.1364311E-04
 -0.9544609E+00  0.4563859E-06 -0.1187524E-05  0.2188415E-01 -0.6405009E-04  0.1875172E+00 -0.1192783E+01 -0.4609432E+00
 -0.7585865E+00 -0.1261725E-05 -0.5054158E+00 -0.1192996E+01 -0.4611627E+00  0.7587447E+00 -0.3958667E-05 -0.5055394E+00
  eigenvectors of nos in ao-basis, column   24
 -0.2864424E-04 -0.9960236E-04  0.3605050E-03 -0.3600590E+00 -0.3510417E-05 -0.3707771E-04 -0.1590043E+01  0.5475434E-05
 -0.7140739E-04  0.7730239E-06 -0.1468353E-06  0.2620539E-07  0.5573477E+00  0.1625051E-04 -0.9426419E+00 -0.1209897E+01
 -0.7208183E+00  0.1260625E-05 -0.6510913E+00  0.9424710E+00  0.1209768E+01 -0.7206813E+00 -0.9907691E-06  0.6509808E+00


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***


 cosurf_and_phi
  2.466315385316 -0.061539892486  2.837440149000 -0.037476236665
  0.006359863210 -2.199287675687  3.114544749440 -0.066810155092
 -0.480526931872  1.061509344774  3.755560521090 -0.068638181275
  1.262739748330  2.872870034186  1.567866969040 -0.044601812411
  1.897949994968 -2.634366215369  0.570962659525 -0.015609924768
 -2.790166444607 -0.824331206483  2.170430257018 -0.021417285053
 -2.061296060778  2.493528161233  1.034251636268 -0.021686138957
 -0.002991703587  2.420846177436 -1.447622679829  0.014856834258
 -1.114435211431 -2.949559801670 -0.067898604227 -0.015631297497
  1.955709175590 -0.984462672186  3.123501329999 -0.050015610884
  1.000900154469 -1.706149424342  3.300403738319 -0.063487171167
  1.652947872482  0.360731428865  3.496571379926 -0.056869938510
  0.594940552015  0.670555958619  3.845537291721 -0.068359415433
  2.390078334690  1.202722009900  2.566708449184 -0.036708800377
  1.989334673621  2.229216060677  2.001028520754 -0.038811426731
  2.919143888229  0.378144896597  2.099775822683 -0.016320529666
  2.780367109803 -0.921046401167  2.130496470151 -0.021136248682
  2.449941849009 -2.011800733381  1.439000205618 -0.019274034449
 -0.231540571447 -1.263621120083  3.706953994699 -0.069246724092
 -0.352106389130 -0.107618694495  3.950679056227 -0.069583940226
  0.105380882100  1.878884201281  3.371423292662 -0.068116204600
  0.783266721831  2.590884380442  2.520834408126 -0.059815633098
  2.011402526412  2.551496219550  0.814855178005 -0.018325938881
  1.440220538007 -2.843720084632  1.356653310131 -0.039133212882
  0.871209499702 -2.660587439486  2.372618950166 -0.057818578805
 -2.947488997374  0.298617382934  2.058332654596 -0.014714145682
 -2.674954162172  1.563027715274  1.704200253745 -0.017437908737
 -1.353448939749  2.910920816403  0.212056013565 -0.018541166118
 -0.743422400795  2.810699746106 -0.731960510989 -0.002795136881
  0.099626804209  1.855073908563 -1.945822518898  0.033741227728
  0.019900458911 -1.968682238442 -1.864952523830  0.030316598386
 -0.601526683996 -2.669374282549 -1.032942810695  0.004701037755
 -1.976701730993 -2.578422698078  0.816296596419 -0.019202252655
 -2.525639241426 -1.850752473194  1.593331825886 -0.019922877977
 -1.124962231191 -1.881571875050  3.121019991266 -0.061344569080
 -2.117398618237 -1.513942036836  2.667866282109 -0.043499802111
 -2.336046769995 -0.152318885910  2.976109389266 -0.041533294298
 -1.478775916646  0.469153703894  3.577445396081 -0.059588504439
 -1.328587214463  1.668007282102  3.174271253825 -0.059470892467
 -1.771761195387  2.287385908353  2.202255939279 -0.045138996535
 -1.026730149698  3.017318998523  1.358608629268 -0.044230938286
  0.119020599620  3.173161356543  1.415159765853 -0.050037083734
  0.915237473358  3.108118624501  0.463299650838 -0.029861195658
  0.462814589486  2.837210699514 -0.795516582628 -0.003627625871
  1.026812912753 -3.017319139618  0.083991013862 -0.020451924215
 -0.070059393274 -3.176418651270  0.035610954337 -0.025687506675
 -0.970268948715 -3.083313212042  1.062474740253 -0.040176509268
 -0.534203029846 -2.828286137107  2.231267851728 -0.058564304759
  0.871897873699 -0.575451888002  3.799144800425 -0.066494854247
  1.408719892640  1.640288870679  3.148120355694 -0.058353714740
  2.650053602975  1.633766196698  1.655441764425 -0.017331867664
  1.964286464189 -2.001027831890  2.365093852582 -0.043490562304
 -1.342877128538 -0.760711330777  3.581800927521 -0.061271391819
 -0.531365927285  2.661712102822  2.509437292848 -0.061184264358
  1.142904167226  2.885766749848 -0.243475252462 -0.010720912802
  0.342129526197 -3.189261637688  1.246854200824 -0.047117856756
 -2.346459445268  1.135413320199  2.662806558936 -0.038635597347
 -0.258565605057  3.205542092831  0.249849913336 -0.029850662409
  0.450566961335 -2.699565911226 -1.032013446782  0.003277739548
 -1.684533476150 -2.430522251364  2.070179818920 -0.045061648422
 -3.368784049130  0.036613212616 -1.854972615293  0.060785779797
 -3.278396927799  1.580834404538 -0.078603229040  0.033627784281
 -3.656110378332 -0.713812229513  0.361831391088  0.034332318187
 -2.408052717333 -2.075622947728 -1.226189024363  0.038981529816
 -1.295829484895 -0.567704086865 -2.747607986898  0.058899097991
 -1.846557165647  1.681693338816 -2.099716493181  0.049131128984
 -3.831862607217  0.357468890277 -0.654813288033  0.050759274833
 -3.527736967644 -1.045671231370 -1.064852892071  0.051966076538
 -2.622906831544 -1.024738705101 -2.241124222290  0.058557443538
 -2.402781990555  0.378472303440 -2.579763034114  0.061859030557
 -3.126606927219  1.313275381963 -1.541857021673  0.053553338000
 -3.633182667875  0.531520753705  0.561856665829  0.031231445513
 -3.086813000261 -1.794874586082 -0.179700355757  0.031709017015
 -1.615880090830 -1.674393887320 -2.147504235692  0.048322833085
 -1.240258025012  0.765881087348 -2.687977655831  0.057656693066
 -2.430292517958  2.154437082790 -0.969924007583  0.034505130410
  3.368776503197 -0.036639040867 -1.854966842961  0.060773774799
  3.278392620256 -1.580850766330 -0.078589062660  0.033620070727
  3.656106873706  0.713798214804  0.361832640492  0.034324455629
  2.408046317685  2.075600470298 -1.226192756897  0.038970240816
  1.295820311657  0.567673501678 -2.747601655947  0.058887887861
  1.846549173548 -1.681720471299 -2.099699178989  0.049120636006
  3.831857249217 -0.357488322766 -0.654806650060  0.050749585736
  3.527730862121  1.045649613724 -1.064853177123  0.051955252826
  2.622898581638  1.024710819001 -2.241122746235  0.058544751535
  2.402773123306 -0.378501994157 -2.579753678917  0.061846350533
  3.126599952113 -1.313299541573 -1.541844004398  0.053542336088
  3.633179527908 -0.531533702443  0.561864593522  0.031224582273
  3.086808508398  1.794857685487 -0.179703829578  0.031699637614
  1.615872011596  1.674366500124 -2.147504385867  0.048311087390
  1.240248960489 -0.765911354740 -2.687964116774  0.057646428015
  2.430286585511 -2.154458194501 -0.969905238283  0.034496381833
  0.000006852884  1.035694667087 -2.361676372823  0.052264850821
 -0.298806015181  0.969607486526 -2.408809074493  0.053738988234
  0.298815704890  0.969607820141 -2.408807386530  0.053735240227
  0.000007656756 -1.035723195601 -2.361670853435  0.052265701377
 -0.298805262603 -0.969636498141 -2.408803907288  0.053739853046
  0.298816457468 -0.969636367899 -2.408802219325  0.053735909608
 -0.667712940797 -1.245186997899 -2.338574529312  0.051254768167
 -1.218112188165 -2.025138759161 -1.616566947615  0.036387710062
 -2.084175601108 -2.294008802021 -0.480477682822  0.020692704023
 -2.876631896395 -1.658047550445  0.559052047065  0.017340741816
 -3.282909221331 -0.368099862162  1.091996180628  0.019449344815
 -3.142757910518  1.067034798172  0.908143333087  0.018430245686
 -2.511458431963  2.081290260960  0.080011352455  0.017645066346
 -1.638016880752  2.274609499719 -1.065756198713  0.027083028351
 -0.866948462969  1.570740798135 -2.077229430584  0.047095093378
 -0.467915446486 -1.694563900014 -1.941501402531  0.039919110850
 -1.299753513329 -2.453901457341 -0.850306853788  0.012816775111
 -2.242033801688 -2.245340287831  0.385760998463  0.000971395401
 -2.923088754321 -1.151144046328  1.279154738758  0.002902903101
 -3.074287016292  0.397098864814  1.477489335582  0.004499592480
 -2.635990824421  1.788708515315  0.902534843950  0.001034901557
 -1.781079179779  2.474786487342 -0.218927030025  0.004594878336
 -0.846758459860  2.184720175398 -1.444553386477  0.025170108556
 -0.255852300976  1.360631011730 -2.219692209228  0.046773696203
  0.667723897920  1.245157743773 -2.338577856151  0.051246543556
  1.218123388571  2.025110412626 -1.616576841774  0.036377044512
  2.084186643139  2.293984793080 -0.480493513306  0.020682900640
  2.876642520254  1.658030225134  0.559035126479  0.017333374702
  3.282919570196  0.368088739237  1.091983758387  0.019443896359
  3.142768358070 -1.067043033853  0.908139383421  0.018425381954
  2.511469270857 -2.081300494445  0.080016452498  0.017639290488
  1.638028059662 -2.274626132712 -1.065745290088  0.027075627813
  0.866959534092 -1.570765766224 -2.077218589767  0.047087531673
  0.467932162408  1.694535407938 -1.941510815499  0.039912736981
  1.299770347024  2.453875604722 -0.850324284820  0.012808101141
  2.242050270258  2.245320705184  0.385739526123  0.000964211319
  2.923104801922  1.151131828431  1.279135167181  0.002898023115
  3.074302957696 -0.397105856761  1.477477125466  0.004496250386
  2.636007061705 -1.788714946455  0.902532611959  0.001031835521
  1.781095866662 -2.474797662024 -0.218920775640  0.004590744355
  0.846775315928 -2.184739733041 -1.444543821640  0.025165540140
  0.255868904327 -1.360657816862 -2.219684436601  0.046771794575
  0.427045418766 -0.580992742084 -2.585105089006  0.057342417540
  0.427044967836  0.580963354323 -2.585108185092  0.057342069284
 -0.427035838172 -0.000014913339 -2.543076305315  0.062623186569
 end_of_phi


 nsubv after electrostatic potential =  0

  Confirmation of dielectric energy for ground state
  edielnew =  -0.0116271421


 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************


 cosurf_and_qcos_from cosmo(3
  2.466315385316 -0.061539892486  2.837440149000  0.002319324257
  0.006359863210 -2.199287675687  3.114544749440  0.006226231324
 -0.480526931872  1.061509344774  3.755560521090  0.005720773773
  1.262739748330  2.872870034186  1.567866969040  0.005016372810
  1.897949994968 -2.634366215369  0.570962659525  0.001846508219
 -2.790166444607 -0.824331206483  2.170430257018  0.001027465846
 -2.061296060778  2.493528161233  1.034251636268  0.002855635839
 -0.002991703587  2.420846177436 -1.447622679829  0.000734518707
 -1.114435211431 -2.949559801670 -0.067898604227  0.002945751059
  1.955709175590 -0.984462672186  3.123501329999  0.004682477534
  1.000900154469 -1.706149424342  3.300403738319  0.006094982082
  1.652947872482  0.360731428865  3.496571379926  0.005246879649
  0.594940552015  0.670555958619  3.845537291721  0.005598287774
  2.390078334690  1.202722009900  2.566708449184  0.003433306118
  1.989334673621  2.229216060677  2.001028520754  0.003467408038
  2.919143888229  0.378144896597  2.099775822683  0.000479612584
  2.780367109803 -0.921046401167  2.130496470151  0.001481543271
  2.449941849009 -2.011800733381  1.439000205618  0.001875866015
 -0.231540571447 -1.263621120083  3.706953994699  0.006494028583
 -0.352106389130 -0.107618694495  3.950679056227  0.006255597829
  0.105380882100  1.878884201281  3.371423292662  0.008052514902
  0.783266721831  2.590884380442  2.520834408126  0.007162907380
  2.011402526412  2.551496219550  0.814855178005  0.002364818821
  1.440220538007 -2.843720084632  1.356653310131  0.004591250416
  0.871209499702 -2.660587439486  2.372618950166  0.007726474751
 -2.947488997374  0.298617382934  2.058332654596  0.000450476723
 -2.674954162172  1.563027715274  1.704200253745  0.001158215910
 -1.353448939749  2.910920816403  0.212056013565  0.003302857219
 -0.743422400795  2.810699746106 -0.731960510989  0.002231356170
  0.099626804209  1.855073908563 -1.945822518898 -0.000146803014
  0.019900458911 -1.968682238442 -1.864952523830 -0.000399293693
 -0.601526683996 -2.669374282549 -1.032942810695  0.001301713629
 -1.976701730993 -2.578422698078  0.816296596419  0.002530069010
 -2.525639241426 -1.850752473194  1.593331825886  0.001686174812
 -1.124962231191 -1.881571875050  3.121019991266  0.005994414500
 -2.117398618237 -1.513942036836  2.667866282109  0.003975579784
 -2.336046769995 -0.152318885910  2.976109389266  0.004150683097
 -1.478775916646  0.469153703894  3.577445396081  0.005163216100
 -1.328587214463  1.668007282102  3.174271253825  0.006150450422
 -1.771761195387  2.287385908353  2.202255939279  0.005845472217
 -1.026730149698  3.017318998523  1.358608629268  0.005749491032
  0.119020599620  3.173161356543  1.415159765853  0.007071079352
  0.915237473358  3.108118624501  0.463299650838  0.004877815355
  0.462814589486  2.837210699514 -0.795516582628  0.002333016663
  1.026812912753 -3.017319139618  0.083991013862  0.004069386273
 -0.070059393274 -3.176418651270  0.035610954337  0.004677598381
 -0.970268948715 -3.083313212042  1.062474740253  0.006583409432
 -0.534203029846 -2.828286137107  2.231267851728  0.008011828089
  0.871897873699 -0.575451888002  3.799144800425  0.006516377766
  1.408719892640  1.640288870679  3.148120355694  0.007041165221
  2.650053602975  1.633766196698  1.655441764425  0.001066361555
  1.964286464189 -2.001027831890  2.365093852582  0.004749644295
 -1.342877128538 -0.760711330777  3.581800927521  0.005373983368
 -0.531365927285  2.661712102822  2.509437292848  0.008070652147
  1.142904167226  2.885766749848 -0.243475252462  0.001388939824
  0.342129526197 -3.189261637688  1.246854200824  0.006881787049
 -2.346459445268  1.135413320199  2.662806558936  0.003544396993
 -0.258565605057  3.205542092831  0.249849913336  0.004504151347
  0.450566961335 -2.699565911226 -1.032013446782  0.002354206213
 -1.684533476150 -2.430522251364  2.070179818920  0.005004461425
 -3.368784049130  0.036613212616 -1.854972615293 -0.007683793738
 -3.278396927799  1.580834404538 -0.078603229040 -0.005730742831
 -3.656110378332 -0.713812229513  0.361831391088 -0.006430698632
 -2.408052717333 -2.075622947728 -1.226189024363 -0.005376921412
 -1.295829484895 -0.567704086865 -2.747607986898 -0.005899829236
 -1.846557165647  1.681693338816 -2.099716493181 -0.006053180030
 -3.831862607217  0.357468890277 -0.654813288033 -0.008308725788
 -3.527736967644 -1.045671231370 -1.064852892071 -0.008215267021
 -2.622906831544 -1.024738705101 -2.241124222290 -0.008132325130
 -2.402781990555  0.378472303440 -2.579763034114 -0.007910048283
 -3.126606927219  1.313275381963 -1.541857021673 -0.008362500405
 -3.633182667875  0.531520753705  0.561856665829 -0.004516026187
 -3.086813000261 -1.794874586082 -0.179700355757 -0.004489990045
 -1.615880090830 -1.674393887320 -2.147504235692 -0.004630679827
 -1.240258025012  0.765881087348 -2.687977655831 -0.004878341158
 -2.430292517958  2.154437082790 -0.969924007583 -0.004059819096
  3.368776503197 -0.036639040867 -1.854966842961 -0.007689155204
  3.278392620256 -1.580850766330 -0.078589062660 -0.005715756709
  3.656106873706  0.713798214804  0.361832640492 -0.006425220623
  2.408046317685  2.075600470298 -1.226192756897 -0.005384653177
  1.295820311657  0.567673501678 -2.747601655947 -0.006323163211
  1.846549173548 -1.681720471299 -2.099699178989 -0.006005309901
  3.831857249217 -0.357488322766 -0.654806650060 -0.008307790617
  3.527730862121  1.045649613724 -1.064853177123 -0.008217795916
  2.622898581638  1.024710819001 -2.241122746235 -0.008117469656
  2.402773123306 -0.378501994157 -2.579753678917 -0.007900101296
  3.126599952113 -1.313299541573 -1.541844004398 -0.008366166022
  3.633179527908 -0.531533702443  0.561864593522 -0.004511615637
  3.086808508398  1.794857685487 -0.179703829578 -0.004486931047
  1.615872011596  1.674366500124 -2.147504385867 -0.004595365875
  1.240248960489 -0.765911354740 -2.687964116774 -0.005137988140
  2.430286585511 -2.154458194501 -0.969905238283 -0.004045607618
  0.000006852884  1.035694667087 -2.361676372823 -0.000287862400
 -0.298806015181  0.969607486526 -2.408809074493 -0.000345658966
  0.298815704890  0.969607820141 -2.408807386530 -0.000254501604
  0.000007656756 -1.035723195601 -2.361670853435 -0.000280986254
 -0.298805262603 -0.969636498141 -2.408803907288 -0.000331399353
  0.298816457468 -0.969636367899 -2.408802219325 -0.000251046902
 -0.667712940797 -1.245186997899 -2.338574529312 -0.000953156266
 -1.218112188165 -2.025138759161 -1.616566947615 -0.001420933247
 -2.084175601108 -2.294008802021 -0.480477682822 -0.000944090005
 -2.876631896395 -1.658047550445  0.559052047065 -0.001362931191
 -3.282909221331 -0.368099862162  1.091996180628 -0.001812085029
 -3.142757910518  1.067034798172  0.908143333087 -0.001623418908
 -2.511458431963  2.081290260960  0.080011352455 -0.001169697262
 -1.638016880752  2.274609499719 -1.065756198713 -0.001135434455
 -0.866948462969  1.570740798135 -2.077229430584 -0.001972610331
 -0.467915446486 -1.694563900014 -1.941501402531 -0.001437865359
 -1.299753513329 -2.453901457341 -0.850306853788 -0.000247025067
 -2.242033801688 -2.245340287831  0.385760998463 -0.000310974656
 -2.923088754321 -1.151144046328  1.279154738758 -0.000889928692
 -3.074287016292  0.397098864814  1.477489335582 -0.001163530904
 -2.635990824421  1.788708515315  0.902534843950 -0.000524648877
 -1.781079179779  2.474786487342 -0.218927030025 -0.000183174213
 -0.846758459860  2.184720175398 -1.444553386477 -0.000855908550
 -0.255852300976  1.360631011730 -2.219692209228 -0.000923667403
  0.667723897920  1.245157743773 -2.338577856151 -0.000885590610
  1.218123388571  2.025110412626 -1.616576841774 -0.001432673128
  2.084186643139  2.293984793080 -0.480493513306 -0.000921462278
  2.876642520254  1.658030225134  0.559035126479 -0.001357110237
  3.282919570196  0.368088739237  1.091983758387 -0.001800835391
  3.142768358070 -1.067043033853  0.908139383421 -0.001615896674
  2.511469270857 -2.081300494445  0.080016452498 -0.001110295693
  1.638028059662 -2.274626132712 -1.065745290088 -0.001094588925
  0.866959534092 -1.570765766224 -2.077218589767 -0.001920878756
  0.467932162408  1.694535407938 -1.941510815499 -0.001491682853
  1.299770347024  2.453875604722 -0.850324284820 -0.000258670578
  2.242050270258  2.245320705184  0.385739526123 -0.000241071325
  2.923104801922  1.151131828431  1.279135167181 -0.000916601918
  3.074302957696 -0.397105856761  1.477477125466 -0.001192889045
  2.636007061705 -1.788714946455  0.902532611959 -0.000569701121
  1.781095866662 -2.474797662024 -0.218920775640 -0.000238527232
  0.846775315928 -2.184739733041 -1.444543821640 -0.000906075960
  0.255868904327 -1.360657816862 -2.219684436601 -0.000854619685
  0.427045418766 -0.580992742084 -2.585105089006 -0.002240549804
  0.427044967836  0.580963354323 -2.585108185092 -0.002113327119
 -0.427035838172 -0.000014913339 -2.543076305315 -0.006037509599
 end_of_qcos

 sum of the negative charges =  -0.25233817

 sum of the positive charges =   0.247510969

 total sum =  -0.00482720104

 *** qcos is copied to qcosdalton ***

 fepsi =   1.
 *** qcosdalton is mult by fepsi ***
 cosurf and qcosdalton
  2.466315385316 -0.061539892486  2.837440149000  0.002319324257
  0.006359863210 -2.199287675687  3.114544749440  0.006226231324
 -0.480526931872  1.061509344774  3.755560521090  0.005720773773
  1.262739748330  2.872870034186  1.567866969040  0.005016372810
  1.897949994968 -2.634366215369  0.570962659525  0.001846508219
 -2.790166444607 -0.824331206483  2.170430257018  0.001027465846
 -2.061296060778  2.493528161233  1.034251636268  0.002855635839
 -0.002991703587  2.420846177436 -1.447622679829  0.000734518707
 -1.114435211431 -2.949559801670 -0.067898604227  0.002945751059
  1.955709175590 -0.984462672186  3.123501329999  0.004682477534
  1.000900154469 -1.706149424342  3.300403738319  0.006094982082
  1.652947872482  0.360731428865  3.496571379926  0.005246879649
  0.594940552015  0.670555958619  3.845537291721  0.005598287774
  2.390078334690  1.202722009900  2.566708449184  0.003433306118
  1.989334673621  2.229216060677  2.001028520754  0.003467408038
  2.919143888229  0.378144896597  2.099775822683  0.000479612584
  2.780367109803 -0.921046401167  2.130496470151  0.001481543271
  2.449941849009 -2.011800733381  1.439000205618  0.001875866015
 -0.231540571447 -1.263621120083  3.706953994699  0.006494028583
 -0.352106389130 -0.107618694495  3.950679056227  0.006255597829
  0.105380882100  1.878884201281  3.371423292662  0.008052514902
  0.783266721831  2.590884380442  2.520834408126  0.007162907380
  2.011402526412  2.551496219550  0.814855178005  0.002364818821
  1.440220538007 -2.843720084632  1.356653310131  0.004591250416
  0.871209499702 -2.660587439486  2.372618950166  0.007726474751
 -2.947488997374  0.298617382934  2.058332654596  0.000450476723
 -2.674954162172  1.563027715274  1.704200253745  0.001158215910
 -1.353448939749  2.910920816403  0.212056013565  0.003302857219
 -0.743422400795  2.810699746106 -0.731960510989  0.002231356170
  0.099626804209  1.855073908563 -1.945822518898 -0.000146803014
  0.019900458911 -1.968682238442 -1.864952523830 -0.000399293693
 -0.601526683996 -2.669374282549 -1.032942810695  0.001301713629
 -1.976701730993 -2.578422698078  0.816296596419  0.002530069010
 -2.525639241426 -1.850752473194  1.593331825886  0.001686174812
 -1.124962231191 -1.881571875050  3.121019991266  0.005994414500
 -2.117398618237 -1.513942036836  2.667866282109  0.003975579784
 -2.336046769995 -0.152318885910  2.976109389266  0.004150683097
 -1.478775916646  0.469153703894  3.577445396081  0.005163216100
 -1.328587214463  1.668007282102  3.174271253825  0.006150450422
 -1.771761195387  2.287385908353  2.202255939279  0.005845472217
 -1.026730149698  3.017318998523  1.358608629268  0.005749491032
  0.119020599620  3.173161356543  1.415159765853  0.007071079352
  0.915237473358  3.108118624501  0.463299650838  0.004877815355
  0.462814589486  2.837210699514 -0.795516582628  0.002333016663
  1.026812912753 -3.017319139618  0.083991013862  0.004069386273
 -0.070059393274 -3.176418651270  0.035610954337  0.004677598381
 -0.970268948715 -3.083313212042  1.062474740253  0.006583409432
 -0.534203029846 -2.828286137107  2.231267851728  0.008011828089
  0.871897873699 -0.575451888002  3.799144800425  0.006516377766
  1.408719892640  1.640288870679  3.148120355694  0.007041165221
  2.650053602975  1.633766196698  1.655441764425  0.001066361555
  1.964286464189 -2.001027831890  2.365093852582  0.004749644295
 -1.342877128538 -0.760711330777  3.581800927521  0.005373983368
 -0.531365927285  2.661712102822  2.509437292848  0.008070652147
  1.142904167226  2.885766749848 -0.243475252462  0.001388939824
  0.342129526197 -3.189261637688  1.246854200824  0.006881787049
 -2.346459445268  1.135413320199  2.662806558936  0.003544396993
 -0.258565605057  3.205542092831  0.249849913336  0.004504151347
  0.450566961335 -2.699565911226 -1.032013446782  0.002354206213
 -1.684533476150 -2.430522251364  2.070179818920  0.005004461425
 -3.368784049130  0.036613212616 -1.854972615293 -0.007683793738
 -3.278396927799  1.580834404538 -0.078603229040 -0.005730742831
 -3.656110378332 -0.713812229513  0.361831391088 -0.006430698632
 -2.408052717333 -2.075622947728 -1.226189024363 -0.005376921412
 -1.295829484895 -0.567704086865 -2.747607986898 -0.005899829236
 -1.846557165647  1.681693338816 -2.099716493181 -0.006053180030
 -3.831862607217  0.357468890277 -0.654813288033 -0.008308725788
 -3.527736967644 -1.045671231370 -1.064852892071 -0.008215267021
 -2.622906831544 -1.024738705101 -2.241124222290 -0.008132325130
 -2.402781990555  0.378472303440 -2.579763034114 -0.007910048283
 -3.126606927219  1.313275381963 -1.541857021673 -0.008362500405
 -3.633182667875  0.531520753705  0.561856665829 -0.004516026187
 -3.086813000261 -1.794874586082 -0.179700355757 -0.004489990045
 -1.615880090830 -1.674393887320 -2.147504235692 -0.004630679827
 -1.240258025012  0.765881087348 -2.687977655831 -0.004878341158
 -2.430292517958  2.154437082790 -0.969924007583 -0.004059819096
  3.368776503197 -0.036639040867 -1.854966842961 -0.007689155204
  3.278392620256 -1.580850766330 -0.078589062660 -0.005715756709
  3.656106873706  0.713798214804  0.361832640492 -0.006425220623
  2.408046317685  2.075600470298 -1.226192756897 -0.005384653177
  1.295820311657  0.567673501678 -2.747601655947 -0.006323163211
  1.846549173548 -1.681720471299 -2.099699178989 -0.006005309901
  3.831857249217 -0.357488322766 -0.654806650060 -0.008307790617
  3.527730862121  1.045649613724 -1.064853177123 -0.008217795916
  2.622898581638  1.024710819001 -2.241122746235 -0.008117469656
  2.402773123306 -0.378501994157 -2.579753678917 -0.007900101296
  3.126599952113 -1.313299541573 -1.541844004398 -0.008366166022
  3.633179527908 -0.531533702443  0.561864593522 -0.004511615637
  3.086808508398  1.794857685487 -0.179703829578 -0.004486931047
  1.615872011596  1.674366500124 -2.147504385867 -0.004595365875
  1.240248960489 -0.765911354740 -2.687964116774 -0.005137988140
  2.430286585511 -2.154458194501 -0.969905238283 -0.004045607618
  0.000006852884  1.035694667087 -2.361676372823 -0.000287862400
 -0.298806015181  0.969607486526 -2.408809074493 -0.000345658966
  0.298815704890  0.969607820141 -2.408807386530 -0.000254501604
  0.000007656756 -1.035723195601 -2.361670853435 -0.000280986254
 -0.298805262603 -0.969636498141 -2.408803907288 -0.000331399353
  0.298816457468 -0.969636367899 -2.408802219325 -0.000251046902
 -0.667712940797 -1.245186997899 -2.338574529312 -0.000953156266
 -1.218112188165 -2.025138759161 -1.616566947615 -0.001420933247
 -2.084175601108 -2.294008802021 -0.480477682822 -0.000944090005
 -2.876631896395 -1.658047550445  0.559052047065 -0.001362931191
 -3.282909221331 -0.368099862162  1.091996180628 -0.001812085029
 -3.142757910518  1.067034798172  0.908143333087 -0.001623418908
 -2.511458431963  2.081290260960  0.080011352455 -0.001169697262
 -1.638016880752  2.274609499719 -1.065756198713 -0.001135434455
 -0.866948462969  1.570740798135 -2.077229430584 -0.001972610331
 -0.467915446486 -1.694563900014 -1.941501402531 -0.001437865359
 -1.299753513329 -2.453901457341 -0.850306853788 -0.000247025067
 -2.242033801688 -2.245340287831  0.385760998463 -0.000310974656
 -2.923088754321 -1.151144046328  1.279154738758 -0.000889928692
 -3.074287016292  0.397098864814  1.477489335582 -0.001163530904
 -2.635990824421  1.788708515315  0.902534843950 -0.000524648877
 -1.781079179779  2.474786487342 -0.218927030025 -0.000183174213
 -0.846758459860  2.184720175398 -1.444553386477 -0.000855908550
 -0.255852300976  1.360631011730 -2.219692209228 -0.000923667403
  0.667723897920  1.245157743773 -2.338577856151 -0.000885590610
  1.218123388571  2.025110412626 -1.616576841774 -0.001432673128
  2.084186643139  2.293984793080 -0.480493513306 -0.000921462278
  2.876642520254  1.658030225134  0.559035126479 -0.001357110237
  3.282919570196  0.368088739237  1.091983758387 -0.001800835391
  3.142768358070 -1.067043033853  0.908139383421 -0.001615896674
  2.511469270857 -2.081300494445  0.080016452498 -0.001110295693
  1.638028059662 -2.274626132712 -1.065745290088 -0.001094588925
  0.866959534092 -1.570765766224 -2.077218589767 -0.001920878756
  0.467932162408  1.694535407938 -1.941510815499 -0.001491682853
  1.299770347024  2.453875604722 -0.850324284820 -0.000258670578
  2.242050270258  2.245320705184  0.385739526123 -0.000241071325
  2.923104801922  1.151131828431  1.279135167181 -0.000916601918
  3.074302957696 -0.397105856761  1.477477125466 -0.001192889045
  2.636007061705 -1.788714946455  0.902532611959 -0.000569701121
  1.781095866662 -2.474797662024 -0.218920775640 -0.000238527232
  0.846775315928 -2.184739733041 -1.444543821640 -0.000906075960
  0.255868904327 -1.360657816862 -2.219684436601 -0.000854619685
  0.427045418766 -0.580992742084 -2.585105089006 -0.002240549804
  0.427044967836  0.580963354323 -2.585108185092 -0.002113327119
 -0.427035838172 -0.000014913339 -2.543076305315 -0.006037509599
 end of cosurf and qcosdalton

 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 ** Total nuclear repulsion energy ** =   9.36082795882421
 screening nuclear repulsion energy   0.0075498163

 Total-screening nuclear repulsion energy   9.35327814


 Adding T+Vsolv ...
 maxdens  300
 *** End of DALTON-COSMO calculation ***

  original map vector  20 21 22 23 24 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17
 18 19 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0


          sorted Vsolv matrix  block   1

               nbf   1        nbf   2        nbf   3        nbf   4        nbf   5        nbf   6        nbf   7        nbf   8
  nbf   1    -3.31800791
  nbf   2     0.00001607    -3.09061008
  nbf   3     0.00001795    -0.98128995    -4.10173933
  nbf   4    -1.01842521     0.00000425     0.00001704    -4.07781680
  nbf   5    -0.42217561     0.00001262     0.00001055    -0.44126935    -4.75244681
  nbf   6    -0.00000513    -0.00000339     0.00000211     0.00000136     0.00000160    -5.34887637
  nbf   7    -0.00000445    -1.17059913    -0.38131723     0.00000807     0.00000671    -0.00000272    -4.33718635
  nbf   8    -0.41286372     0.00000930     0.00002855     0.04017910     0.09703855    -0.00000436    -0.00000463    -3.58726707
  nbf   9     0.00000274     0.00000103     0.00000188     0.00000125    -0.00000001    -0.00000320     0.00000086    -0.00000228
  nbf  10     0.00000011     0.00000040     0.00000125     0.00000082     0.00000612     0.24656515     0.00000115    -0.00000061
  nbf  11     0.71273475     0.00000331     0.00000650    -0.06900342    -0.45107513     0.00000249     0.00000097     0.37446293
  nbf  12    -0.00002188    -0.02255998    -0.02737127     0.00000058    -0.00004191     0.00000092    -0.10300830    -0.00003953
  nbf  13     0.00001074     0.51225754     1.29214325     0.00000949     0.00000519     0.00000174     1.13411322     0.00000470
  nbf  14     0.60243551     0.00000735     0.00000329     1.39181442     0.61421791     0.00000226     0.00000734     0.29791029
  nbf  15     0.00000114     0.00000005    -0.00000011     0.00000167     0.00000192     0.06453853     0.00000118     0.00000074
  nbf  16     0.00000148     0.00000192     0.00000281     0.00000049     0.00000035    -0.00000234    -0.00000020    -0.00000074
  nbf  17     0.01597567    -0.00000629    -0.00001382     0.28427305    -0.03522215     0.00000102     0.00000136    -0.91938954
  nbf  18    -0.11636675    -0.00000596    -0.00000157    -0.29333719    -0.90109388     0.00000048    -0.00000104    -0.23509280
  nbf  19    -0.00000635     0.16662287    -0.06196072     0.00000036    -0.00000122    -0.00000009     1.19139270    -0.00000735
  nbf  20     0.18303952     0.00000005     0.00000000     0.16560630    -0.11838416    -0.00000046     0.00000014     0.20494899
  nbf  21    -0.99287573    -0.00000498    -0.00001598    -0.72375360     0.04744914     0.00000173     0.00000383    -0.60130430
  nbf  22     0.00000798     1.23666226     0.65676591     0.00001052     0.00000959    -0.00000235     1.45004341     0.00001190
  nbf  23     0.54130347     0.00000780     0.00001380     0.73132430     1.71707508     0.00000016     0.00000305    -0.41969168
  nbf  24     0.00000200     0.00000137    -0.00000148    -0.00000169     0.00000013    -2.13412961     0.00000157     0.00000231

               nbf   9        nbf  10        nbf  11        nbf  12        nbf  13        nbf  14        nbf  15        nbf  16
  nbf   9    -3.42221673
  nbf  10     0.00002010    -3.17750807
  nbf  11    -0.00000297    -0.00000185    -4.03661070
  nbf  12    -0.00000421     0.00000168    -0.00000816    -2.63219252
  nbf  13    -0.00000190    -0.00000169     0.00000131     0.09058575    -3.62260352
  nbf  14    -0.00000169    -0.00000158    -0.18629501     0.00000095     0.00002178    -3.66396744
  nbf  15     0.00000638     0.90765854    -0.00000061     0.00000069     0.00000003     0.00000034    -3.67016213
  nbf  16     0.90251820     0.00000724    -0.00000033    -0.00000091    -0.00000062    -0.00000022     0.00000635    -3.63972569
  nbf  17     0.00000122     0.00000043    -0.42343527     0.00002360    -0.00000515     0.04524304    -0.00000009     0.00000163
  nbf  18     0.00000057    -0.00000152     0.56834189     0.00000995    -0.00000949    -0.46534788    -0.00000085     0.00000048
  nbf  19    -0.00000117    -0.00000060    -0.00000055    -0.08841268     0.44954708     0.00000859    -0.00000105    -0.00000004
  nbf  20    -0.00000001     0.00000003    -0.38281026     0.00000000    -0.00000003    -0.26338743    -0.00000001     0.00000005
  nbf  21    -0.00000281    -0.00000170     1.08737982     0.00001364     0.00000244     0.59831115    -0.00000036     0.00000064
  nbf  22     0.00000388     0.00000216     0.00000592     0.01870837    -0.54201991     0.00000029     0.00000097    -0.00000148
  nbf  23     0.00000114     0.00000553     0.54855134    -0.00002646     0.00000188    -0.34234580    -0.00000283     0.00000120
  nbf  24     0.00000389     0.07854368    -0.00000218    -0.00000058    -0.00000115    -0.00000014     0.08084968     0.00000193

               nbf  17        nbf  18        nbf  19        nbf  20        nbf  21        nbf  22        nbf  23        nbf  24
  nbf  17    -3.38765479
  nbf  18    -0.05579533    -3.03517422
  nbf  19     0.00000618    -0.00000763    -2.89462111
  nbf  20     0.00966313     0.05948998    -0.00000027   -33.05208589
  nbf  21    -0.01753963    -0.18425984     0.00000146     0.58206098    -7.87298595
  nbf  22    -0.00000615     0.00000101    -0.45831023    -0.00000014    -0.00000978    -6.71596230
  nbf  23    -0.12232081     0.30565305    -0.00000426     0.18750698    -0.25170571     0.00001440    -6.95611452
  nbf  24    -0.00000090     0.00000143    -0.00000008    -0.00000041    -0.00000471     0.00000238    -0.00000114    -7.11721467
 insert_onel_diag: nmin2=   380

 onel.diag.
 -0.331801E+01-0.309061E+01-0.410174E+01-0.407782E+01-0.475245E+01-0.534888E+01-0.433719E+01-0.358727E+01-0.342222E+01-0.317751E+01
 -0.403661E+01-0.263219E+01-0.362260E+01-0.366397E+01-0.367016E+01-0.363973E+01-0.338765E+01-0.303517E+01-0.289462E+01-0.330521E+02
 -0.787299E+01-0.671596E+01-0.695611E+01-0.711721E+01

 i,start,strti,fin,filind,bufszi,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095         1       380       380

 4 external diag. modified integrals

  0.299711954 -3.31800791  0.362025844  0.213403987  0.294044049 -3.09061008
  0.345182252  0.318068021  0.34837146  0.293493931  0.497019376 -4.10173933
  0.382867967  0.309072536  0.341195502  0.316765031  0.62103433  0.362850338
  0.531836961 -4.0778168  0.349706612  0.299527054  0.308820027  0.298754537
  0.450260207  0.419223806  0.448476864  0.401601457  0.518535861 -4.75244681
  0.349531774  0.304524501  0.31278089  0.302655019  0.429312411  0.416826853
  0.436921903  0.413400849  0.530775438  0.48280831  0.60679891 -5.34887637
  0.33535564  0.293407704  0.341034277  0.276576801  0.455225842  0.389302194
  0.443351075  0.383054684  0.486303633  0.428587076  0.499742288  0.459023099
  0.475434104 -4.33718635  0.342526573  0.315362897  0.319917749  0.300731659
  0.504389096  0.410870033  0.47482656  0.425010382  0.519270202  0.400100848
  0.453053263  0.422413645  0.446074502  0.406547185  0.476405093 -3.58726707
  0.361060191  0.337659006  0.340604687  0.320198588  0.504515292  0.452245513
  0.527488651  0.469039301  0.434901697  0.420706511  0.454829088  0.417262113
  0.426620519  0.400804422  0.469386808  0.441599006  0.555764969 -3.42221673
  0.348856386  0.329501947  0.326880472  0.312634683  0.490451682  0.436629022
  0.509180653  0.444435002  0.4400708  0.419071921  0.459403052  0.417691971
  0.415311401  0.398901525  0.466192986  0.43088403  0.694750225  0.346946891
  0.501631969 -3.17750807  0.343227876  0.31280524  0.321132402  0.300850073
  0.455432102  0.418963752  0.451109274  0.419591702  0.534502331  0.438245352
  0.597905727  0.464004009  0.508095073  0.408470727  0.460615989  0.424227034
  0.469521607  0.441123033  0.462583019  0.438238584  0.525774157 -4.0366107
  0.356976431  0.333496982  0.334773679  0.315431831  0.522936179  0.430785245
  0.55704189  0.430105919  0.46546176  0.385379923  0.398185515  0.390667038
  0.424333725  0.380522726  0.592974188  0.354449455  0.516100041  0.471421051
  0.491982029  0.453400696  0.428233148  0.409923576  0.53092711 -2.63219252
  0.370351482  0.34231438  0.377526483  0.320334123  0.57663332  0.44254372
  0.563518308  0.477512966  0.511232086  0.469131823  0.508472502  0.490097443
  0.529535469  0.421869959  0.499545867  0.467575397  0.539123293  0.488956188
  0.512741012  0.475743005  0.515621907  0.477676678  0.512163042  0.474196449
  0.592144122 -3.62260352  0.388203856  0.340302294  0.365601016  0.328125926
  0.536368018  0.473948976  0.615526601  0.455378076  0.520967947  0.463416877
  0.546457699  0.489981129  0.48581785  0.455063687  0.509165453  0.46499119
  0.535052608  0.504064949  0.518029026  0.481875024  0.516561248  0.489811629
  0.517714869  0.482829445  0.717425907  0.445100321  0.607835731 -3.66396744
  0.361030445  0.336028491  0.330453775  0.322428351  0.486741558  0.469418774
  0.514189842  0.450314389  0.575178104  0.539226259  0.630438312  0.577562728
  0.510895782  0.499926663  0.503939385  0.490371469  0.52041881  0.467839603
  0.554043059  0.445274854  0.591832537  0.558697254  0.453957255  0.445032703
  0.568738744  0.548069735  0.596778585  0.560916765  0.750165186 -3.67016213
  0.353015389  0.338054232  0.358115278  0.317291888  0.522721553  0.462375435
  0.489534774  0.469144866  0.543456123  0.532600344  0.632201055  0.577477753
  0.547273656  0.513624874  0.507616587  0.483570727  0.564197636  0.455577875
  0.524876517  0.46718588  0.585160178  0.562784851  0.458775281  0.447244069
  0.587371194  0.556188564  0.58998874  0.568912261  0.721594827  0.645098881
  0.755127757 -3.63972569  0.366037972  0.335416332  0.343455146  0.320873443
  0.50940609  0.476326958  0.527676719  0.456821536  0.589439367  0.537772005
  0.579789349  0.559672288  0.53269644  0.49854365  0.567546289  0.469810852
  0.495610134  0.480892777  0.493939617  0.479062411  0.584343602  0.533457424
  0.502525388  0.442000941  0.582221799  0.560044775  0.596954899  0.56140811
  0.713528596  0.656785472  0.69947109  0.625487345  0.729807063 -3.38765479
  0.353987843  0.337616982  0.351789554  0.321182817  0.550334788  0.456688986
  0.522995786  0.46803517  0.566057852  0.507622395  0.615570104  0.560439732
  0.539018372  0.503813066  0.510463413  0.481771744  0.500103129  0.48786777
  0.496692123  0.477703001  0.585735173  0.540040646  0.469784387  0.453592071
  0.609978757  0.53850005  0.643846451  0.534124473  0.686162365  0.635949394
  0.700906311  0.666564985  0.679632027  0.617528828  0.696112735 -3.03517422
  0.358856446  0.335907395  0.354506012  0.319503646  0.548011068  0.469120392
  0.531509325  0.460743131  0.577501451  0.534350115  0.577624788  0.562928162
  0.587296462  0.4866605  0.514004213  0.4958057  0.493523583  0.479933652
  0.487740555  0.474497517  0.570979461  0.537487177  0.471960345  0.452931079
  0.634007692  0.54138526  0.615254278  0.550385293  0.691778305  0.63576614
  0.699509207  0.640665078  0.699234432  0.657364716  0.711993958  0.616407541
  0.721280492 -2.89462111
 i,start,strti,fin,filind,bufszi,nmbuf,nmin2, nd4ext = 
         1         1         2         2        12      4095         1       380       380

 all internall diag. modified integrals

  4.73971342 -33.0520859  1.04569563  0.0636788604  0.755179462 -7.87298595
  0.878477817  0.0193493095  0.682719812  0.155759544  0.668673763 -6.7159623
  0.986988872  0.0301374391  0.681192227  0.125736446  0.608457144
  0.0402117459  0.732528817 -6.95611452  1.03831833  0.0301199817  0.713926708
  0.128455235  0.624391224  0.0304166993  0.665957609  0.0433645062
  0.760228983 -7.11721467
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331801E+01-0.309061E+01-0.410174E+01-0.407782E+01-0.475245E+01-0.534888E+01-0.433719E+01-0.358727E+01-0.342222E+01-0.317751E+01
 -0.403661E+01-0.263219E+01-0.362260E+01-0.366397E+01-0.367016E+01-0.363973E+01-0.338765E+01-0.303517E+01-0.289462E+01-0.330521E+02
 -0.787299E+01-0.671596E+01-0.695611E+01-0.711721E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331801E+01-0.309061E+01-0.410174E+01-0.407782E+01-0.475245E+01-0.534888E+01-0.433719E+01-0.358727E+01-0.342222E+01-0.317751E+01
 -0.403661E+01-0.263219E+01-0.362260E+01-0.366397E+01-0.367016E+01-0.363973E+01-0.338765E+01-0.303517E+01-0.289462E+01-0.330521E+02
 -0.787299E+01-0.671596E+01-0.695611E+01-0.711721E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331801E+01-0.309061E+01-0.410174E+01-0.407782E+01-0.475245E+01-0.534888E+01-0.433719E+01-0.358727E+01-0.342222E+01-0.317751E+01
 -0.403661E+01-0.263219E+01-0.362260E+01-0.366397E+01-0.367016E+01-0.363973E+01-0.338765E+01-0.303517E+01-0.289462E+01-0.330521E+02
 -0.787299E+01-0.671596E+01-0.695611E+01-0.711721E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380
 DIAGEL COUNTS: DG0X,DG2X,DG4X= 440 145 30
 =========== Executing IN-CORE method ==========
 norm multnew:  1.
 threx: total number of loops= 14
 threx: total number of loops= 24
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331801E+01-0.309061E+01-0.410174E+01-0.407782E+01-0.475245E+01-0.534888E+01-0.433719E+01-0.358727E+01-0.342222E+01-0.317751E+01
 -0.403661E+01-0.263219E+01-0.362260E+01-0.366397E+01-0.367016E+01-0.363973E+01-0.338765E+01-0.303517E+01-0.289462E+01-0.330521E+02
 -0.787299E+01-0.671596E+01-0.695611E+01-0.711721E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331801E+01-0.309061E+01-0.410174E+01-0.407782E+01-0.475245E+01-0.534888E+01-0.433719E+01-0.358727E+01-0.342222E+01-0.317751E+01
 -0.403661E+01-0.263219E+01-0.362260E+01-0.366397E+01-0.367016E+01-0.363973E+01-0.338765E+01-0.303517E+01-0.289462E+01-0.330521E+02
 -0.787299E+01-0.671596E+01-0.695611E+01-0.711721E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331801E+01-0.309061E+01-0.410174E+01-0.407782E+01-0.475245E+01-0.534888E+01-0.433719E+01-0.358727E+01-0.342222E+01-0.317751E+01
 -0.403661E+01-0.263219E+01-0.362260E+01-0.366397E+01-0.367016E+01-0.363973E+01-0.338765E+01-0.303517E+01-0.289462E+01-0.330521E+02
 -0.787299E+01-0.671596E+01-0.695611E+01-0.711721E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        40 xx:       135 ww:       255
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        20 yw:        25

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        39
task #   9:       127    task #  10:       247    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 matrix-vector product formed for vector   1
 =========== Executing IN-CORE method ==========
 norm multnew:  4.04353929E-06
 threx: total number of loops= 14
 threx: total number of loops= 24
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331801E+01-0.309061E+01-0.410174E+01-0.407782E+01-0.475245E+01-0.534888E+01-0.433719E+01-0.358727E+01-0.342222E+01-0.317751E+01
 -0.403661E+01-0.263219E+01-0.362260E+01-0.366397E+01-0.367016E+01-0.363973E+01-0.338765E+01-0.303517E+01-0.289462E+01-0.330521E+02
 -0.787299E+01-0.671596E+01-0.695611E+01-0.711721E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331801E+01-0.309061E+01-0.410174E+01-0.407782E+01-0.475245E+01-0.534888E+01-0.433719E+01-0.358727E+01-0.342222E+01-0.317751E+01
 -0.403661E+01-0.263219E+01-0.362260E+01-0.366397E+01-0.367016E+01-0.363973E+01-0.338765E+01-0.303517E+01-0.289462E+01-0.330521E+02
 -0.787299E+01-0.671596E+01-0.695611E+01-0.711721E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331801E+01-0.309061E+01-0.410174E+01-0.407782E+01-0.475245E+01-0.534888E+01-0.433719E+01-0.358727E+01-0.342222E+01-0.317751E+01
 -0.403661E+01-0.263219E+01-0.362260E+01-0.366397E+01-0.367016E+01-0.363973E+01-0.338765E+01-0.303517E+01-0.289462E+01-0.330521E+02
 -0.787299E+01-0.671596E+01-0.695611E+01-0.711721E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        40 xx:       135 ww:       255
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        20 yw:        25

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        39
task #   9:       127    task #  10:       247    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 matrix-vector product formed for vector   2
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference-subspace vector overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97552724     0.00163748

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97564481     0.11562184
 subspace has dimension  2

          (h-repnuc*1) matrix in the subspace basis

                x:   1         x:   2
   x:   1   -85.60690707
   x:   2    -0.13024513    -0.00034025

          overlap matrix in the subspace basis

                x:   1         x:   2
   x:   1     1.00000000
   x:   2     0.00152138     0.00000404

          eigenvectors and eigenvalues in the subspace basis

                v:   1         v:   2

   energy   -85.60691058   -82.18305101

   x:   1     0.99882792    -1.15804827
   x:   2     0.77006630   760.51759508

          eigenvectors and eigenvalues in the schmidt orthonormalized basis

                v:   1         v:   2

   energy   -85.60691058   -82.18305101

  zx:   1     0.99999949    -0.00101256
  zx:   2     0.00101256     0.99999949

          <ref|baseci> overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97552724     0.00163748

 trial vector basis is being transformed.  new dimension:   1

          transformed tciref transformation matrix  block   1

               ref   1
  ci:   1     0.97564481

  write the transformed vectors back to cosmofile

 rtflw =  0
 freezing =  F

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  7  1    -76.2536324369 -9.3533E+00  1.4294E-06  1.7508E-03  1.0000E-03
 mr-sdci #  7  2    -72.8297728628 -9.6821E+00  0.0000E+00  1.9427E+00  1.0000E-04


 total energy in cosmo calc 
e(rootcalc) + repnuc=     -76.2536324369
dielectric energy =      -0.0116339358
deltaediel =       0.0116339358
e(rootcalc) + repnuc - ediel =     -76.2419985011
e(rootcalc) + repnuc - edielnew =     -76.2536324369
deltaelast =      76.2419985011

 root number  1 is used to define the new expansion vector.
 diagon: frcsub=2, new matrix-vector products will be computed.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.01   0.00   0.00   0.01         0.    0.0000
    2   25    0   0.01   0.00   0.00   0.01         0.    0.0000
    3   26    0   0.02   0.00   0.00   0.02         0.    0.0000
    4   11    0   0.01   0.00   0.00   0.00         0.    0.0000
    5   15    0   0.02   0.00   0.00   0.01         0.    0.0000
    6   16    0   0.02   0.00   0.00   0.01         0.    0.0000
    7    1    0   0.01   0.00   0.00   0.01         0.    0.0000
    8    5    0   0.02   0.00   0.00   0.02         0.    0.0000
    9    6    0   0.03   0.00   0.00   0.03         0.    0.0000
   10    7    0   0.03   0.00   0.00   0.03         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.02   0.00   0.00   0.01         0.    0.0000
   14   47    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.2100s 
time spent in multnx:                   0.1700s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0300s 
total time per CI iteration:            3.8100s 

          starting ci iteration   8

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.35327814

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=      10  D0X=      30  D0W=      50
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================

          ci-one-electron density block   1

                mo   1         mo   2         mo   3         mo   4         mo   5         mo   6         mo   7         mo   8
   mo   1     1.99991410
   mo   2    -0.00020971     1.98517695
   mo   3     0.00000000    -0.00000003     1.96980222
   mo   4    -0.00015518     0.00411119    -0.00000011     1.97334600
   mo   5     0.00000000    -0.00000007    -0.00000001    -0.00000008     1.97628162
   mo   6     0.00012294    -0.00384212    -0.00002024    -0.02122024    -0.00000842     0.00695595
   mo   7     0.00000000     0.00000783    -0.01061835    -0.00002378    -0.00000494    -0.00000006     0.00748447
   mo   8    -0.00000001     0.00001584    -0.01289690    -0.00002210     0.00000237    -0.00000019     0.00715520     0.01057432
   mo   9     0.00005771     0.00312088    -0.00001221    -0.00033483     0.00000191     0.00706776    -0.00000025    -0.00000036
   mo  10    -0.00022106     0.01055122    -0.00001408     0.00340973     0.00000009     0.00260339    -0.00000021    -0.00000014
   mo  11     0.00000004    -0.00000176     0.00000296    -0.00000043    -0.01177962     0.00000022     0.00000010    -0.00000009
   mo  12    -0.00000002    -0.00000284    -0.00567566    -0.00000608    -0.00000250     0.00000005     0.00609803     0.00406761
   mo  13     0.00020461    -0.00110241    -0.00001278    -0.00837048    -0.00000312     0.00196003    -0.00000004    -0.00000018
   mo  14     0.00000000     0.00000214    -0.00000329    -0.00000115    -0.00000198    -0.00000003    -0.00000003    -0.00000002
   mo  15     0.00000000     0.00000112    -0.00000191    -0.00000499    -0.00350088     0.00000002    -0.00000001    -0.00000001
   mo  16    -0.00039043    -0.00017664    -0.00000464     0.00288844     0.00000175    -0.00195369    -0.00000011    -0.00000005
   mo  17    -0.00000001    -0.00000911     0.00103838     0.00002183     0.00000049    -0.00000003     0.00027401     0.00024765
   mo  18     0.00000004    -0.00000060     0.01160961    -0.00000056     0.00000089    -0.00000012    -0.00285990    -0.00438805
   mo  19    -0.00010571    -0.00447874    -0.00000012     0.00758632     0.00000042    -0.00310964    -0.00000014    -0.00000009
   mo  20     0.00000000     0.00000021    -0.00000049     0.00000139     0.00206189    -0.00000003    -0.00000001     0.00000000
   mo  21     0.00000000    -0.00000023     0.00000059    -0.00000061    -0.00000127     0.00000001    -0.00000001    -0.00000002
   mo  22     0.00001088     0.00012886     0.00000266    -0.00121095     0.00000029    -0.00005976     0.00000004     0.00000004
   mo  23     0.00004998     0.00174305    -0.00000044     0.00031686    -0.00000050     0.00063515     0.00000000    -0.00000002
   mo  24    -0.00000001    -0.00000070    -0.00029145     0.00000191     0.00000009     0.00000000    -0.00082782     0.00034549

                mo   9         mo  10         mo  11         mo  12         mo  13         mo  14         mo  15         mo  16
   mo   9     0.01007244
   mo  10     0.00333769     0.01007849
   mo  11    -0.00000006    -0.00000002     0.01446832
   mo  12     0.00000003     0.00000005     0.00000002     0.00651109
   mo  13     0.00052772    -0.00065069     0.00000007    -0.00000002     0.00372676
   mo  14    -0.00000003     0.00000000     0.00000009    -0.00000002     0.00000000     0.00296426
   mo  15    -0.00000003    -0.00000006    -0.00137531    -0.00000001     0.00000003    -0.00000006     0.00223703
   mo  16     0.00002390     0.00264018    -0.00000003    -0.00000007    -0.00125107     0.00000002    -0.00000002     0.00437810
   mo  17     0.00000020     0.00000028    -0.00000001     0.00035786    -0.00000003     0.00000000     0.00000000     0.00000007
   mo  18    -0.00000015    -0.00000018     0.00000001    -0.00183728     0.00000001     0.00000000     0.00000000    -0.00000003
   mo  19    -0.00436411    -0.00082436     0.00000000    -0.00000014    -0.00083927     0.00000000    -0.00000001    -0.00005848
   mo  20    -0.00000001     0.00000000     0.00004251     0.00000000    -0.00000001     0.00000000    -0.00182227     0.00000000
   mo  21     0.00000000    -0.00000001     0.00000003     0.00000000     0.00000001    -0.00201867     0.00000000    -0.00000001
   mo  22    -0.00056383     0.00029608    -0.00000001     0.00000002     0.00171963     0.00000000     0.00000000     0.00070218
   mo  23     0.00024156     0.00177569     0.00000000     0.00000004     0.00024904     0.00000000     0.00000000    -0.00068614
   mo  24     0.00000001     0.00000000     0.00000001    -0.00168447     0.00000001     0.00000000     0.00000000     0.00000003

                mo  17         mo  18         mo  19         mo  20         mo  21         mo  22         mo  23         mo  24
   mo  17     0.00051247
   mo  18    -0.00020397     0.00261562
   mo  19     0.00000002     0.00000006     0.00287655
   mo  20     0.00000000     0.00000000     0.00000001     0.00254784
   mo  21     0.00000000     0.00000001     0.00000000     0.00000001     0.00242025
   mo  22    -0.00000001     0.00000001    -0.00016073     0.00000000     0.00000000     0.00197117
   mo  23     0.00000004    -0.00000002     0.00061185     0.00000000     0.00000000    -0.00008797     0.00163743
   mo  24     0.00000352    -0.00057418     0.00000002     0.00000000     0.00000000    -0.00000001    -0.00000001     0.00144654


*****   symmetry block SYM1   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       1.99991410
       2      -0.00020971     1.98517695
       3       0.00000000    -0.00000003     1.96980222
       4      -0.00015518     0.00411119    -0.00000011     1.97334600
       5       0.00000000    -0.00000007    -0.00000001    -0.00000008
       6       0.00012294    -0.00384212    -0.00002024    -0.02122024
       7       0.00000000     0.00000783    -0.01061835    -0.00002378
       8      -0.00000001     0.00001584    -0.01289690    -0.00002210
       9       0.00005771     0.00312088    -0.00001221    -0.00033483
      10      -0.00022106     0.01055122    -0.00001408     0.00340973
      11       0.00000004    -0.00000176     0.00000296    -0.00000043
      12      -0.00000002    -0.00000284    -0.00567566    -0.00000608
      13       0.00020461    -0.00110241    -0.00001278    -0.00837048
      14       0.00000000     0.00000214    -0.00000329    -0.00000115
      15       0.00000000     0.00000112    -0.00000191    -0.00000499
      16      -0.00039043    -0.00017664    -0.00000464     0.00288844
      17      -0.00000001    -0.00000911     0.00103838     0.00002183
      18       0.00000004    -0.00000060     0.01160961    -0.00000056
      19      -0.00010571    -0.00447874    -0.00000012     0.00758632
      20       0.00000000     0.00000021    -0.00000049     0.00000139
      21       0.00000000    -0.00000023     0.00000059    -0.00000061
      22       0.00001088     0.00012886     0.00000266    -0.00121095
      23       0.00004998     0.00174305    -0.00000044     0.00031686
      24      -0.00000001    -0.00000070    -0.00029145     0.00000191

               Column   5     Column   6     Column   7     Column   8
       5       1.97628162
       6      -0.00000842     0.00695595
       7      -0.00000494    -0.00000006     0.00748447
       8       0.00000237    -0.00000019     0.00715520     0.01057432
       9       0.00000191     0.00706776    -0.00000025    -0.00000036
      10       0.00000009     0.00260339    -0.00000021    -0.00000014
      11      -0.01177962     0.00000022     0.00000010    -0.00000009
      12      -0.00000250     0.00000005     0.00609803     0.00406761
      13      -0.00000312     0.00196003    -0.00000004    -0.00000018
      14      -0.00000198    -0.00000003    -0.00000003    -0.00000002
      15      -0.00350088     0.00000002    -0.00000001    -0.00000001
      16       0.00000175    -0.00195369    -0.00000011    -0.00000005
      17       0.00000049    -0.00000003     0.00027401     0.00024765
      18       0.00000089    -0.00000012    -0.00285990    -0.00438805
      19       0.00000042    -0.00310964    -0.00000014    -0.00000009
      20       0.00206189    -0.00000003    -0.00000001     0.00000000
      21      -0.00000127     0.00000001    -0.00000001    -0.00000002
      22       0.00000029    -0.00005976     0.00000004     0.00000004
      23      -0.00000050     0.00063515     0.00000000    -0.00000002
      24       0.00000009     0.00000000    -0.00082782     0.00034549

               Column   9     Column  10     Column  11     Column  12
       9       0.01007244
      10       0.00333769     0.01007849
      11      -0.00000006    -0.00000002     0.01446832
      12       0.00000003     0.00000005     0.00000002     0.00651109
      13       0.00052772    -0.00065069     0.00000007    -0.00000002
      14      -0.00000003     0.00000000     0.00000009    -0.00000002
      15      -0.00000003    -0.00000006    -0.00137531    -0.00000001
      16       0.00002390     0.00264018    -0.00000003    -0.00000007
      17       0.00000020     0.00000028    -0.00000001     0.00035786
      18      -0.00000015    -0.00000018     0.00000001    -0.00183728
      19      -0.00436411    -0.00082436     0.00000000    -0.00000014
      20      -0.00000001     0.00000000     0.00004251     0.00000000
      21       0.00000000    -0.00000001     0.00000003     0.00000000
      22      -0.00056383     0.00029608    -0.00000001     0.00000002
      23       0.00024156     0.00177569     0.00000000     0.00000004
      24       0.00000001     0.00000000     0.00000001    -0.00168447

               Column  13     Column  14     Column  15     Column  16
      13       0.00372676
      14       0.00000000     0.00296426
      15       0.00000003    -0.00000006     0.00223703
      16      -0.00125107     0.00000002    -0.00000002     0.00437810
      17      -0.00000003     0.00000000     0.00000000     0.00000007
      18       0.00000001     0.00000000     0.00000000    -0.00000003
      19      -0.00083927     0.00000000    -0.00000001    -0.00005848
      20      -0.00000001     0.00000000    -0.00182227     0.00000000
      21       0.00000001    -0.00201867     0.00000000    -0.00000001
      22       0.00171963     0.00000000     0.00000000     0.00070218
      23       0.00024904     0.00000000     0.00000000    -0.00068614
      24       0.00000001     0.00000000     0.00000000     0.00000003

               Column  17     Column  18     Column  19     Column  20
      17       0.00051247
      18      -0.00020397     0.00261562
      19       0.00000002     0.00000006     0.00287655
      20       0.00000000     0.00000000     0.00000001     0.00254784
      21       0.00000000     0.00000001     0.00000000     0.00000001
      22      -0.00000001     0.00000001    -0.00016073     0.00000000
      23       0.00000004    -0.00000002     0.00061185     0.00000000
      24       0.00000352    -0.00057418     0.00000002     0.00000000

               Column  21     Column  22     Column  23     Column  24
      21       0.00242025
      22       0.00000000     0.00197117
      23       0.00000000    -0.00008797     0.00163743
      24       0.00000000    -0.00000001    -0.00000001     0.00144654

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7        eno   8
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    8   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno   9        eno  10        eno  11        eno  12        eno  13        eno  14        eno  15        eno  16
 emo    9   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   10   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   11   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   12   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   13   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   14   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   15   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   16   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno  17        eno  18        eno  19        eno  20        eno  21        eno  22        eno  23        eno  24
 emo   17   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   18   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   19   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   20   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   21   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   22   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   23   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   24   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   1.9999192      1.9865885      1.9763607      1.9723156      1.9700315
   0.0214414      0.0196778      0.0145615      0.0102913      0.0054137
   0.0051499      0.0047292      0.0042121      0.0041219      0.0010123
   0.0010085      0.0006553      0.0005679      0.0004914      0.0004907
   0.0004469      0.0004296      0.0000502      0.0000331

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7         no   8
  mo    1   0.999820E+00   0.187019E-01  -0.614241E-07   0.314078E-02  -0.691105E-06   0.299128E-07  -0.337172E-04  -0.237138E-07
  mo    2  -0.168452E-01   0.951937E+00  -0.979384E-06  -0.305764E+00   0.696815E-04  -0.559353E-05  -0.289255E-02   0.107180E-05
  mo    3   0.111976E-06  -0.475905E-05   0.333824E-06   0.212729E-03   0.999941E+00   0.101958E-01   0.176699E-04  -0.164893E-05
  mo    4  -0.870553E-02   0.305616E+00   0.123938E-04   0.952033E+00  -0.201360E-03   0.121655E-04   0.670423E-02   0.202312E-08
  mo    5   0.154338E-06  -0.288754E-05   0.999980E+00  -0.121541E-04  -0.336002E-06   0.136128E-05   0.190820E-05   0.573319E-02
  mo    6   0.187005E-03  -0.511019E-02  -0.440824E-05  -0.969605E-02  -0.830048E-05  -0.242708E-03   0.534674E+00   0.542818E-05
  mo    7   0.355785E-07   0.125044E-06  -0.251126E-05  -0.139554E-04  -0.545190E-02   0.569703E+00   0.242164E-03   0.396852E-05
  mo    8  -0.440581E-07   0.424094E-05   0.119064E-05  -0.146514E-04  -0.662070E-02   0.636585E+00   0.259733E-03  -0.801269E-05
  mo    9   0.448678E-05   0.144487E-02   0.951590E-06  -0.694207E-03  -0.612405E-05  -0.289421E-03   0.672758E+00  -0.141329E-04
  mo   10  -0.215358E-03   0.560486E-02   0.601156E-07  -0.302862E-05  -0.718088E-05  -0.175841E-03   0.407040E+00  -0.137141E-04
  mo   11   0.365738E-07  -0.900166E-06  -0.600283E-02   0.142599E-06   0.151563E-05   0.268859E-05   0.124919E-04   0.993023E+00
  mo   12   0.424949E-07  -0.228214E-05  -0.127408E-05  -0.319096E-05  -0.292635E-02   0.440914E+00   0.212793E-03   0.967197E-06
  mo   13   0.148702E-03  -0.182378E-02  -0.163824E-05  -0.388968E-02  -0.568429E-05  -0.554375E-04   0.839502E-01   0.552769E-05
  mo   14  -0.133384E-07   0.851247E-06  -0.100187E-05  -0.889985E-06  -0.167107E-05  -0.375532E-05  -0.300979E-05   0.756366E-05
  mo   15   0.112755E-07  -0.227308E-06  -0.177013E-02  -0.256219E-05  -0.968593E-06  -0.214373E-05  -0.520049E-05  -0.115702E+00
  mo   16  -0.207285E-03   0.370175E-03   0.909723E-06   0.143595E-02  -0.265747E-05  -0.267639E-05  -0.438884E-02  -0.732599E-05
  mo   17  -0.255711E-07  -0.100834E-05   0.249704E-06   0.120610E-04   0.524453E-03   0.256671E-01   0.338763E-04  -0.651282E-06
  mo   18   0.289202E-07  -0.407320E-06   0.455760E-06   0.113219E-05   0.592601E-02  -0.270663E+00  -0.132963E-03   0.180894E-05
  mo   19  -0.484804E-04  -0.977923E-03   0.266483E-06   0.438090E-02  -0.961659E-06   0.112985E-03  -0.291841E+00   0.421803E-05
  mo   20  -0.657782E-08   0.313913E-06   0.104611E-02   0.630543E-06  -0.250789E-06  -0.449315E-07  -0.236767E-06   0.220479E-01
  mo   21   0.587258E-08  -0.206091E-06  -0.641379E-06  -0.257393E-06   0.300185E-06  -0.335364E-06   0.171086E-06   0.273899E-06
  mo   22   0.965426E-05  -0.125398E-03   0.140248E-06  -0.607829E-03   0.147336E-05   0.383860E-05  -0.656649E-02  -0.700245E-07
  mo   23   0.887465E-05   0.888088E-03  -0.251988E-06  -0.120109E-03  -0.206640E-06  -0.245330E-04   0.591951E-01  -0.144630E-05
  mo   24  -0.765936E-08  -0.404943E-07   0.452091E-07   0.100428E-05  -0.146135E-03  -0.421033E-01  -0.210042E-04  -0.154643E-06

                no   9         no  10         no  11         no  12         no  13         no  14         no  15         no  16
  mo    1   0.228503E-03  -0.326769E-07  -0.979522E-04  -0.121411E-08   0.439691E-04   0.233551E-09  -0.670143E-07   0.194704E-04
  mo    2  -0.416871E-02   0.432916E-05  -0.334271E-03  -0.887777E-06   0.362147E-03   0.234733E-06  -0.769758E-05   0.105387E-02
  mo    3   0.123693E-05  -0.230014E-02   0.557229E-05   0.145258E-05   0.148322E-05  -0.322679E-06  -0.158520E-02  -0.350065E-05
  mo    4  -0.649202E-02  -0.224605E-06   0.396149E-02   0.360514E-06   0.144195E-02  -0.217318E-05   0.139747E-04  -0.272383E-02
  mo    5  -0.174071E-05   0.186719E-05   0.237019E-05   0.212190E-06  -0.395212E-06  -0.250009E-02  -0.927681E-06  -0.103211E-05
  mo    6  -0.257147E+00   0.404347E-04   0.189935E+00   0.287304E-05  -0.124833E+00  -0.349047E-05   0.308533E-03  -0.133827E+00
  mo    7  -0.119436E-04   0.219286E+00  -0.639043E-04   0.334749E-05   0.539899E-05  -0.621043E-06   0.406735E-01   0.109054E-03
  mo    8   0.540566E-05  -0.545647E+00   0.175500E-03  -0.438525E-05   0.568937E-05  -0.101144E-05   0.401398E+00   0.932963E-03
  mo    9  -0.168075E+00  -0.127060E-03  -0.365374E+00  -0.297170E-05   0.461672E-01   0.644807E-06  -0.926770E-03   0.401352E+00
  mo   10   0.767158E+00   0.100419E-03   0.319687E+00   0.190430E-05  -0.143813E+00  -0.670150E-05   0.685636E-03  -0.285386E+00
  mo   11   0.131992E-04  -0.626514E-05  -0.817034E-05  -0.902636E-05   0.354219E-05  -0.928276E-01   0.190399E-05   0.335424E-05
  mo   12  -0.542950E-05   0.639157E+00  -0.188260E-03   0.897338E-05  -0.324887E-05   0.370409E-06  -0.291861E+00  -0.684035E-03
  mo   13  -0.255636E+00   0.191406E-03   0.604268E+00   0.189542E-04   0.448555E+00   0.920209E-05  -0.161249E-03   0.568582E-01
  mo   14   0.333233E-05  -0.881042E-05  -0.137045E-04   0.752841E+00  -0.886914E-05  -0.182078E-04  -0.642831E-05   0.503598E-08
  mo   15  -0.652882E-05   0.161015E-05   0.203167E-06  -0.262966E-04   0.128564E-04  -0.653713E+00  -0.106468E-05  -0.604888E-06
  mo   16   0.467757E+00  -0.102669E-03  -0.362434E+00  -0.828038E-06   0.557529E+00   0.777910E-05  -0.688848E-03   0.283075E+00
  mo   17   0.129053E-04   0.187500E-01   0.116246E-04   0.229069E-05   0.196151E-05   0.421412E-06  -0.246939E+00  -0.606369E-03
  mo   18  -0.686519E-05   0.284215E+00  -0.100396E-03   0.284317E-05  -0.299545E-05   0.130312E-05   0.617357E+00   0.143444E-02
  mo   19   0.150218E+00   0.592250E-04   0.186500E+00  -0.147384E-05  -0.277704E+00  -0.651121E-05  -0.104877E-02   0.427732E+00
  mo   20   0.199699E-05  -0.156204E-05   0.117536E-06   0.183900E-04  -0.141393E-04   0.751024E+00  -0.204099E-05   0.151924E-05
  mo   21  -0.190874E-05   0.807530E-05   0.116730E-04  -0.658203E+00   0.986545E-05   0.275475E-04  -0.734291E-05   0.143451E-05
  mo   22   0.241123E-01   0.990020E-04   0.318339E+00   0.116275E-04   0.523128E+00   0.100150E-04  -0.258411E-03   0.135959E+00
  mo   23   0.987042E-01   0.963356E-04   0.309177E+00   0.188218E-05  -0.314292E+00  -0.767622E-05  -0.153968E-02   0.674347E+00
  mo   24   0.262815E-05  -0.405619E+00   0.130643E-03  -0.550385E-05   0.866018E-05  -0.152117E-05  -0.556710E+00  -0.129559E-02

                no  17         no  18         no  19         no  20         no  21         no  22         no  23         no  24
  mo    1  -0.173580E-08  -0.272704E-07   0.429912E-04   0.559890E-07   0.425490E-07  -0.356163E-04   0.265124E-04   0.490320E-08
  mo    2  -0.666395E-06  -0.365708E-05   0.262787E-02   0.304699E-05   0.550636E-05  -0.195645E-02   0.175224E-02  -0.797170E-07
  mo    3   0.896623E-06  -0.587657E-03   0.200581E-05   0.830577E-06  -0.166543E-02  -0.791868E-05   0.115457E-05   0.163017E-02
  mo    4   0.604421E-06   0.724701E-05   0.177975E-02   0.382686E-05  -0.546387E-05  -0.184944E-02   0.603388E-02   0.121585E-05
  mo    5   0.983003E-06   0.132278E-05  -0.271741E-06   0.106854E-02  -0.129988E-06  -0.129482E-05   0.234686E-05   0.119035E-05
  mo    6  -0.607506E-05  -0.171134E-03   0.469541E+00   0.628593E-03   0.317337E-03  -0.210879E+00   0.559796E+00   0.571062E-04
  mo    7  -0.180295E-04   0.563610E+00   0.118718E-03  -0.582590E-05   0.185802E+00   0.466234E-04  -0.477856E-04   0.522962E+00
  mo    8   0.131868E-04  -0.186159E+00  -0.692719E-04   0.152537E-05   0.520725E-01   0.279464E-04   0.272502E-04  -0.313843E+00
  mo    9   0.161291E-05   0.824037E-05  -0.106110E-01  -0.143063E-04  -0.126250E-04   0.262349E-01  -0.470776E+00  -0.509655E-04
  mo   10   0.192032E-05   0.462550E-04  -0.541978E-01  -0.693216E-04  -0.447264E-04   0.976704E-02  -0.195947E+00  -0.152866E-04
  mo   11  -0.146083E-04  -0.405867E-05  -0.101276E-03   0.724768E-01   0.118405E-05   0.315719E-05  -0.701336E-05  -0.333349E-05
  mo   12   0.348436E-05  -0.145474E+00   0.428178E-05   0.349693E-05  -0.124781E+00  -0.580279E-04   0.539413E-04  -0.524546E+00
  mo   13   0.649249E-05   0.295341E-04  -0.333292E+00  -0.451795E-03   0.279042E-03  -0.461793E+00  -0.183504E+00  -0.257607E-04
  mo   14   0.658203E+00   0.246653E-04   0.112656E-04   0.994072E-04  -0.470385E-05   0.232260E-05   0.167675E-05   0.122787E-05
  mo   15  -0.996072E-04   0.444751E-05  -0.999812E-03   0.747843E+00   0.150007E-04  -0.805733E-05  -0.257319E-05   0.260821E-06
  mo   16  -0.179095E-05  -0.703307E-04   0.289674E-01   0.369436E-04   0.292603E-03  -0.331460E+00   0.384901E+00   0.331769E-04
  mo   17   0.157560E-04  -0.378230E+00  -0.407381E-03  -0.161802E-04   0.888409E+00   0.834608E-03   0.368194E-06   0.753756E-01
  mo   18  -0.594792E-05   0.385602E+00   0.100399E-04  -0.869938E-05   0.373256E+00   0.206815E-03   0.408063E-04  -0.420462E+00
  mo   19  -0.808136E-05  -0.254946E-03   0.580014E+00   0.769329E-03   0.542258E-03  -0.403297E+00  -0.313560E+00  -0.334645E-04
  mo   20  -0.100160E-03   0.437537E-05  -0.881877E-03   0.659906E+00   0.126190E-04  -0.786600E-05  -0.147627E-05  -0.604941E-07
  mo   21   0.752841E+00   0.258457E-04   0.106700E-04   0.101263E-03  -0.292375E-05   0.164044E-05   0.651472E-06   0.905085E-06
  mo   22  -0.949444E-05  -0.368653E-04   0.441905E+00   0.597349E-03  -0.397842E-03   0.636692E+00  -0.723262E-01   0.537757E-06
  mo   23   0.425642E-05   0.147062E-03  -0.364487E+00  -0.483076E-03  -0.320313E-03   0.252181E+00   0.375753E+00   0.312195E-04
  mo   24  -0.247483E-04   0.578601E+00   0.130421E-03  -0.725599E-05   0.136390E+00   0.752643E-04   0.476781E-04  -0.412785E+00
  eigenvectors of nos in ao-basis, column    1
  0.1000603E+01 -0.1375310E-01 -0.1673717E-02  0.6802268E-07  0.1925631E-06 -0.7389124E-02  0.8541978E-07 -0.5510461E-07
  0.1474414E-03  0.8016576E-08 -0.1166527E-07  0.4233361E-04  0.1589256E-07  0.9063647E-04 -0.3269727E-02  0.2147349E-02
 -0.1015122E-02  0.4172072E-08 -0.9350530E-03 -0.3269834E-02  0.2147494E-02  0.1015142E-02  0.1800189E-07 -0.9351068E-03
  eigenvectors of nos in ao-basis, column    2
  0.9543578E-02  0.9130679E+00 -0.7107324E-01 -0.3245245E-05 -0.3904542E-05  0.1247407E+00 -0.4932505E-05  0.1342932E-05
  0.7750044E-01 -0.3307267E-06  0.4213788E-06 -0.1203080E-02 -0.2123191E-06  0.1114047E-02  0.2168002E+00 -0.1059955E+00
  0.2699509E-01  0.3619911E-06  0.2157167E-01  0.2168108E+00 -0.1060030E+00 -0.2699722E-01 -0.6492000E-06  0.2156999E-01
  eigenvectors of nos in ao-basis, column    3
 -0.8252582E-06 -0.2238284E-05  0.3296258E-05  0.2753259E-05  0.9104865E+00  0.1064474E-04 -0.2458545E-05  0.8288050E-01
  0.1810394E-05 -0.5585809E-06 -0.1689657E-01 -0.9637595E-07 -0.1388014E-06 -0.2623458E-06 -0.7690750E-05  0.1045978E-04
  0.7521069E-06  0.3056858E-01 -0.3415424E-06 -0.2312272E-05  0.7600202E-06 -0.2881012E-06  0.3057045E-01 -0.5260678E-06
  eigenvectors of nos in ao-basis, column    4
  0.1115457E-02 -0.1077680E-02  0.2168892E+00  0.1693692E-03 -0.1079073E-04  0.8021314E+00 -0.3088264E-04  0.2048083E-06
 -0.2014223E-01 -0.1612641E-06  0.1290688E-05 -0.4711304E-02 -0.5830397E-05 -0.2410077E-02 -0.4296868E+00  0.1835918E+00
 -0.3806361E-01 -0.2810462E-05  0.2212235E-02 -0.4294798E+00  0.1835097E+00  0.3806451E-01 -0.1392822E-05  0.2245874E-02
  eigenvectors of nos in ao-basis, column    5
  0.3400400E-06  0.2380561E-05 -0.5306213E-04  0.7310119E+00  0.1890596E-05 -0.1571119E-03 -0.1182449E+00 -0.1921306E-05
  0.5017950E-06  0.5373098E-06 -0.1079277E-06  0.8587388E-06 -0.2541012E-01  0.8659840E-06 -0.5519223E+00  0.1930507E+00
 -0.2035486E-01 -0.1883401E-05 -0.3126953E-01  0.5521287E+00 -0.1931325E+00 -0.2037360E-01  0.1812275E-06  0.3125688E-01
  eigenvectors of nos in ao-basis, column    6
 -0.4564421E-04 -0.3580735E-03  0.6553258E-04 -0.1306505E+01  0.5183582E-05  0.5224540E-03  0.5803247E+00 -0.3143179E-05
 -0.3012991E-03  0.1245164E-06  0.2589708E-06  0.1886917E-05 -0.6525407E-01  0.1288508E-04 -0.1005210E+01  0.3584092E+00
 -0.3983255E-01 -0.4318232E-05 -0.1799725E-01  0.1006044E+01 -0.3587540E+00 -0.3987026E-01  0.1091321E-05  0.1796871E-01
  eigenvectors of nos in ao-basis, column    7
  0.1148504E+00  0.8548318E+00 -0.1683574E+00 -0.5927242E-03  0.1970743E-04 -0.1210584E+01  0.2895727E-03 -0.1774514E-04
  0.7554677E+00  0.5714616E-06  0.4940040E-06 -0.3972864E-02 -0.2729601E-04 -0.3152437E-01 -0.9753166E+00  0.4155416E+00
 -0.4970000E-01 -0.5977500E-05 -0.9748960E-02 -0.9744527E+00  0.4152240E+00  0.4967148E-01 -0.1977173E-05 -0.9683885E-02
  eigenvectors of nos in ao-basis, column    8
  0.4115120E-05  0.2013066E-04 -0.2250720E-04  0.1431737E-05  0.1412887E+01  0.2755906E-04  0.2645463E-05 -0.1611466E+01
 -0.2726589E-04 -0.6813791E-06  0.3709582E-01  0.1702934E-06  0.1482888E-05  0.7875939E-06  0.3513304E-04 -0.3588711E-04
 -0.1197471E-07 -0.7461755E-01  0.1954636E-05  0.4227761E-05 -0.1297840E-05  0.1073252E-05 -0.7462814E-01  0.1384252E-05
  eigenvectors of nos in ao-basis, column    9
 -0.4408935E+00 -0.2257271E+01  0.2143660E+01  0.2928393E-05  0.1745679E-04 -0.7471652E+00 -0.1527622E-04 -0.1913123E-04
  0.1099166E+01 -0.2476288E-05  0.3105864E-05 -0.1278431E-01 -0.5545069E-05  0.6104893E-01  0.3731370E+00 -0.2065085E+00
 -0.1279491E-01 -0.2645056E-05  0.7504310E-01  0.3731770E+00 -0.2065560E+00  0.1279863E-01 -0.5763552E-05  0.7506607E-01
  eigenvectors of nos in ao-basis, column   10
  0.8264218E-04  0.3636371E-03 -0.5157659E-03 -0.4942940E+00 -0.7234422E-05 -0.4782747E-05  0.1038691E+01  0.1023994E-04
  0.5956018E-04  0.9794782E-05 -0.1897538E-05 -0.3104639E-04  0.6163609E+00  0.8271986E-04  0.6578792E+00 -0.4997740E+00
  0.1116183E+00 -0.2337433E-05 -0.2166436E-01 -0.6574661E+00  0.4995419E+00  0.1116753E+00  0.3599008E-05  0.2180709E-01
  eigenvectors of nos in ao-basis, column   11
  0.2873766E+00  0.1282811E+01 -0.1767476E+01  0.1306266E-03 -0.9351458E-05 -0.4220999E-01 -0.3110734E-03  0.1378188E-04
  0.2211020E+00  0.1428340E-04  0.8283654E-07 -0.9835661E-01 -0.2011579E-03  0.2648454E+00  0.6417093E+00 -0.3797348E+00
 -0.6758385E-01 -0.4721020E-05  0.2205573E+00  0.6421484E+00 -0.3800641E+00  0.6752623E-01  0.5189953E-05  0.2205717E+00
  eigenvectors of nos in ao-basis, column   12
  0.5464539E-05  0.2608592E-04 -0.2805352E-04 -0.8518001E-05 -0.1063614E-04  0.4996637E-06  0.1202949E-04  0.2493541E-04
 -0.6672413E-05 -0.8028305E+00  0.2346943E-04 -0.4248282E-05  0.7765290E-05  0.1944185E-05 -0.8662812E-06 -0.1485520E-06
 -0.2250026E-05  0.2657469E+00  0.3666795E-05 -0.3822240E-05  0.3837889E-05  0.6334890E-05 -0.2657725E+00  0.7251516E-05
  eigenvectors of nos in ao-basis, column   13
 -0.1286297E+00 -0.5447515E+00  0.8926444E+00  0.2684218E-05  0.3667478E-05  0.9805172E-01 -0.5265561E-05 -0.9081821E-05
 -0.5120991E+00  0.1172190E-04 -0.1684887E-04 -0.2106213E+00 -0.1241936E-04 -0.2136746E+00 -0.4699095E+00  0.2703582E+00
 -0.1642123E+00  0.1842982E-05  0.9224630E-01 -0.4699122E+00  0.2703708E+00  0.1642192E+00  0.6495493E-05  0.9224361E-01
  eigenvectors of nos in ao-basis, column   14
  0.3460525E-06  0.1537616E-05 -0.4692712E-06  0.5534233E-06 -0.8398035E-01  0.5366625E-05 -0.7091499E-06  0.3026958E+00
 -0.1330958E-04  0.3188590E-04  0.8902933E+00 -0.3995363E-05  0.2292672E-05 -0.5040165E-05 -0.5493656E-05  0.5759003E-05
 -0.2178534E-05 -0.1991854E+00  0.8861801E-06 -0.4646493E-05  0.4287533E-05  0.2705550E-05 -0.1991814E+00  0.1626101E-05
  eigenvectors of nos in ao-basis, column   15
  0.8655445E-03  0.3303786E-02 -0.6411307E-02  0.4987781E+00  0.1887821E-05 -0.3725159E-03 -0.1295236E+01 -0.1983216E-05
  0.2803925E-02 -0.7044939E-05 -0.1955478E-05  0.5288997E-04  0.8081397E+00 -0.9481338E-03 -0.1403014E+01  0.7255332E+00
 -0.7345869E-01 -0.8879465E-05  0.2547495E+00  0.1413486E+01 -0.7314198E+00 -0.7378130E-01  0.5539987E-05 -0.2541525E+00
  eigenvectors of nos in ao-basis, column   16
 -0.3573442E+00 -0.1354887E+01  0.2707282E+01  0.1157177E-02  0.3831104E-05  0.1230180E+00 -0.3001512E-02 -0.5858147E-05
 -0.1153986E+01  0.1537542E-05  0.1689570E-05 -0.2987343E-01  0.1882030E-02  0.4144980E+00 -0.2257045E+01  0.1269488E+01
 -0.7495716E-01  0.6874460E-06 -0.1591590E+00 -0.2250527E+01  0.1266163E+01  0.7457684E-01 -0.4128967E-06 -0.1603818E+00
  eigenvectors of nos in ao-basis, column   17
  0.4220260E-05  0.2013254E-04 -0.6879162E-05 -0.2601365E-04 -0.1375093E-04 -0.1032442E-04 -0.2030943E-04  0.1162984E-03
 -0.4144634E-05  0.7224030E+00 -0.8877223E-04  0.2796400E-05  0.2946482E-04  0.9140160E-06 -0.1030409E-03  0.8477483E-04
 -0.2248321E-04  0.7384733E+00 -0.2260361E-04  0.5764330E-04 -0.5043012E-04  0.3246755E-05 -0.7387085E+00  0.2406316E-04
  eigenvectors of nos in ao-basis, column   18
  0.1237460E-03  0.5631835E-03 -0.2088373E-03  0.8938200E+00 -0.4771300E-05 -0.3274765E-03  0.1281871E+00  0.2876585E-05
  0.1183212E-03  0.2453401E-04  0.3858085E-05  0.1191569E-04 -0.6603572E+00  0.5731143E-04  0.1763267E+01 -0.1624085E+01
  0.3856473E+00  0.3200397E-04  0.6956512E+00 -0.1764112E+01  0.1624744E+01  0.3861870E+00 -0.2154807E-04 -0.6959917E+00
  eigenvectors of nos in ao-basis, column   19
 -0.2129398E+00 -0.1040697E+01 -0.1652889E+00  0.7594523E-04 -0.8251567E-04  0.6966685E+00  0.4558798E-03  0.1050304E-02
  0.3496135E+00  0.9988907E-05 -0.7619394E-03 -0.1279633E+00 -0.1493575E-03 -0.1166215E+00  0.1593352E+01 -0.1109706E+01
  0.9034048E+00 -0.1118273E-02  0.1482682E+00  0.1592068E+01 -0.1108790E+01 -0.9036349E+00 -0.1141867E-02  0.1475912E+00
  eigenvectors of nos in ao-basis, column   20
 -0.2828172E-03 -0.1382699E-02 -0.2188754E-03 -0.1777920E-04  0.5789977E-01  0.9200058E-03  0.2275321E-04 -0.7801158E+00
  0.4820205E-03  0.9576747E-04  0.5702051E+00 -0.1728974E-03  0.8362171E-05 -0.1535809E-03  0.2119170E-02 -0.1471498E-02
  0.1188601E-02  0.8455017E+00  0.1911925E-03  0.2135422E-02 -0.1497590E-02 -0.1223289E-02  0.8452881E+00  0.1838504E-03
  eigenvectors of nos in ao-basis, column   21
 -0.3314201E-03 -0.1430902E-02  0.1017593E-02  0.6169932E+00  0.6375766E-06  0.6653257E-03 -0.1253877E+01 -0.1500483E-04
 -0.7985781E-03 -0.2528675E-05  0.1079542E-04  0.1086455E-03 -0.1489442E+00 -0.1666219E-03 -0.3088989E+00 -0.1096604E+00
  0.7469998E+00  0.1235777E-04 -0.3531986E+00  0.3094747E+00  0.1089647E+00  0.7465047E+00  0.2104208E-04  0.3546503E+00
  eigenvectors of nos in ao-basis, column   22
  0.3021324E+00  0.1260543E+01 -0.1219838E+01  0.3540575E-03  0.3742476E-05 -0.5001781E+00 -0.9591144E-03  0.1919689E-05
  0.1030993E+01  0.1459585E-05 -0.6927885E-05 -0.1758435E+00 -0.9398321E-04  0.1604436E+00  0.2137073E+00  0.3084069E-01
  0.5981284E-01 -0.7215762E-05 -0.8058699E+00  0.2144859E+00  0.3062411E-01 -0.5864601E-01 -0.1165378E-04 -0.8050111E+00
  eigenvectors of nos in ao-basis, column   23
 -0.3247494E+00 -0.1122153E+01  0.3944071E+01  0.3841240E-04 -0.7567916E-05 -0.3601839E+00  0.1596658E-03  0.1399682E-04
 -0.9529645E+00  0.4824007E-06 -0.1144717E-05  0.2191687E-01 -0.6414271E-04  0.1875598E+00 -0.1193274E+01 -0.4603102E+00
 -0.7595523E+00 -0.1149800E-05 -0.5060089E+00 -0.1193488E+01 -0.4605244E+00  0.7597102E+00 -0.3947606E-05 -0.5061320E+00
  eigenvectors of nos in ao-basis, column   24
 -0.2846230E-04 -0.9930954E-04  0.3561306E-03 -0.3602944E+00 -0.3622879E-05 -0.3665857E-04 -0.1588462E+01  0.5568243E-05
 -0.6929670E-04  0.8122114E-06 -0.1121348E-06 -0.4994450E-07  0.5567307E+00  0.1582824E-04 -0.9407484E+00 -0.1210987E+01
 -0.7210000E+00  0.1383319E-05 -0.6508318E+00  0.9405840E+00  0.1210856E+01 -0.7208656E+00 -0.9917426E-06  0.6507229E+00


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***


 cosurf_and_phi
  2.466315385316 -0.061539892486  2.837440149000 -0.037467527533
  0.006359863210 -2.199287675687  3.114544749440 -0.066800759181
 -0.480526931872  1.061509344774  3.755560521090 -0.068629224676
  1.262739748330  2.872870034186  1.567866969040 -0.044591616777
  1.897949994968 -2.634366215369  0.570962659525 -0.015603140116
 -2.790166444607 -0.824331206483  2.170430257018 -0.021410404707
 -2.061296060778  2.493528161233  1.034251636268 -0.021678906464
 -0.002991703587  2.420846177436 -1.447622679829  0.014868783050
 -1.114435211431 -2.949559801670 -0.067898604227 -0.015620957094
  1.955709175590 -0.984462672186  3.123501329999 -0.050006394950
  1.000900154469 -1.706149424342  3.300403738319 -0.063477919530
  1.652947872482  0.360731428865  3.496571379926 -0.056860722127
  0.594940552015  0.670555958619  3.845537291721 -0.068350511798
  2.390078334690  1.202722009900  2.566708449184 -0.036700258610
  1.989334673621  2.229216060677  2.001028520754 -0.038802590670
  2.919143888229  0.378144896597  2.099775822683 -0.016314620150
  2.780367109803 -0.921046401167  2.130496470151 -0.021129607560
  2.449941849009 -2.011800733381  1.439000205618 -0.019267961248
 -0.231540571447 -1.263621120083  3.706953994699 -0.069237760300
 -0.352106389130 -0.107618694495  3.950679056227 -0.069575092765
  0.105380882100  1.878884201281  3.371423292662 -0.068107037305
  0.783266721831  2.590884380442  2.520834408126 -0.059805768811
  2.011402526412  2.551496219550  0.814855178005 -0.018319172829
  1.440220538007 -2.843720084632  1.356653310131 -0.039123313618
  0.871209499702 -2.660587439486  2.372618950166 -0.057808591039
 -2.947488997374  0.298617382934  2.058332654596 -0.014708367571
 -2.674954162172  1.563027715274  1.704200253745 -0.017432036230
 -1.353448939749  2.910920816403  0.212056013565 -0.018531691464
 -0.743422400795  2.810699746106 -0.731960510989 -0.002783953958
  0.099626804209  1.855073908563 -1.945822518898  0.033751272498
  0.019900458911 -1.968682238442 -1.864952523830  0.030327122761
 -0.601526683996 -2.669374282549 -1.032942810695  0.004712344767
 -1.976701730993 -2.578422698078  0.816296596419 -0.019195093497
 -2.525639241426 -1.850752473194  1.593331825886 -0.019916554202
 -1.124962231191 -1.881571875050  3.121019991266 -0.061335155416
 -2.117398618237 -1.513942036836  2.667866282109 -0.043490691730
 -2.336046769995 -0.152318885910  2.976109389266 -0.041524220376
 -1.478775916646  0.469153703894  3.577445396081 -0.059579257257
 -1.328587214463  1.668007282102  3.174271253825 -0.059461522461
 -1.771761195387  2.287385908353  2.202255939279 -0.045129601676
 -1.026730149698  3.017318998523  1.358608629268 -0.044220197388
  0.119020599620  3.173161356543  1.415159765853 -0.050025739795
  0.915237473358  3.108118624501  0.463299650838 -0.029849899968
  0.462814589486  2.837210699514 -0.795516582628 -0.003615435682
  1.026812912753 -3.017319139618  0.083991013862 -0.020441103222
 -0.070059393274 -3.176418651270  0.035610954337 -0.025674626618
 -0.970268948715 -3.083313212042  1.062474740253 -0.040165470983
 -0.534203029846 -2.828286137107  2.231267851728 -0.058554024968
  0.871897873699 -0.575451888002  3.799144800425 -0.066485877900
  1.408719892640  1.640288870679  3.148120355694 -0.058344405370
  2.650053602975  1.633766196698  1.655441764425 -0.017326198679
  1.964286464189 -2.001027831890  2.365093852582 -0.043481487157
 -1.342877128538 -0.760711330777  3.581800927521 -0.061262171753
 -0.531365927285  2.661712102822  2.509437292848 -0.061174302091
  1.142904167226  2.885766749848 -0.243475252462 -0.010711160173
  0.342129526197 -3.189261637688  1.246854200824 -0.047106354624
 -2.346459445268  1.135413320199  2.662806558936 -0.038626773030
 -0.258565605057  3.205542092831  0.249849913336 -0.029838057652
  0.450566961335 -2.699565911226 -1.032013446782  0.003289641247
 -1.684533476150 -2.430522251364  2.070179818920 -0.045052116981
 -3.368784049130  0.036613212616 -1.854972615293  0.060764366795
 -3.278396927799  1.580834404538 -0.078603229040  0.033616416921
 -3.656110378332 -0.713812229513  0.361831391088  0.034320157268
 -2.408052717333 -2.075622947728 -1.226189024363  0.038972198359
 -1.295829484895 -0.567704086865 -2.747607986898  0.058892492907
 -1.846557165647  1.681693338816 -2.099716493181  0.049122682892
 -3.831862607217  0.357468890277 -0.654813288033  0.050739957038
 -3.527736967644 -1.045671231370 -1.064852892071  0.051947188891
 -2.622906831544 -1.024738705101 -2.241124222290  0.058540582138
 -2.402781990555  0.378472303440 -2.579763034114  0.061842447349
 -3.126606927219  1.313275381963 -1.541857021673  0.053535480028
 -3.633182667875  0.531520753705  0.561856665829  0.031220721084
 -3.086813000261 -1.794874586082 -0.179700355757  0.031698881777
 -1.615880090830 -1.674393887320 -2.147504235692  0.048316622606
 -1.240258025012  0.765881087348 -2.687977655831  0.057651061640
 -2.430292517958  2.154437082790 -0.969924007583  0.034497007059
  3.368776503197 -0.036639040867 -1.854966842961  0.060752078305
  3.278392620256 -1.580850766330 -0.078589062660  0.033608470116
  3.656106873706  0.713798214804  0.361832640492  0.034312040617
  2.408046317685  2.075600470298 -1.226192756897  0.038960652057
  1.295820311657  0.567673501678 -2.747601655947  0.058881072319
  1.846549173548 -1.681720471299 -2.099699178989  0.049111974332
  3.831857249217 -0.357488322766 -0.654806650060  0.050729999188
  3.527730862121  1.045649613724 -1.064853177123  0.051936085101
  2.622898581638  1.024710819001 -2.241122746235  0.058527613882
  2.402773123306 -0.378501994157 -2.579753678917  0.061829504093
  3.126599952113 -1.313299541573 -1.541844004398  0.053524217207
  3.633179527908 -0.531533702443  0.561864593522  0.031213617959
  3.086808508398  1.794857685487 -0.179703829578  0.031689244675
  1.615872011596  1.674366500124 -2.147504385867  0.048304644157
  1.240248960489 -0.765911354740 -2.687964116774  0.057640604406
  2.430286585511 -2.154458194501 -0.969905238283  0.034488041881
  0.000006852884  1.035694667087 -2.361676372823  0.052272389628
 -0.298806015181  0.969607486526 -2.408809074493  0.053745067904
  0.298815704890  0.969607820141 -2.408807386530  0.053741252852
  0.000007656756 -1.035723195601 -2.361670853435  0.052273252575
 -0.298805262603 -0.969636498141 -2.408803907288  0.053745944581
  0.298816457468 -0.969636367899 -2.408802219325  0.053741933313
 -0.667712940797 -1.245186997899 -2.338574529312  0.051258001640
 -1.218112188165 -2.025138759161 -1.616566947615  0.036389397184
 -2.084175601108 -2.294008802021 -0.480477682822  0.020690732611
 -2.876631896395 -1.658047550445  0.559052047065  0.017336575196
 -3.282909221331 -0.368099862162  1.091996180628  0.019444831214
 -3.142757910518  1.067034798172  0.908143333087  0.018425774062
 -2.511458431963  2.081290260960  0.080011352455  0.017641661967
 -1.638016880752  2.274609499719 -1.065756198713  0.027082888810
 -0.866948462969  1.570740798135 -2.077229430584  0.047097894767
 -0.467915446486 -1.694563900014 -1.941501402531  0.039927898121
 -1.299753513329 -2.453901457341 -0.850306853788  0.012823043226
 -2.242033801688 -2.245340287831  0.385760998463  0.000973883605
 -2.923088754321 -1.151144046328  1.279154738758  0.002904214213
 -3.074287016292  0.397098864814  1.477489335582  0.004500873754
 -2.635990824421  1.788708515315  0.902534843950  0.001036482536
 -1.781079179779  2.474786487342 -0.218927030025  0.004598982501
 -0.846758459860  2.184720175398 -1.444553386477  0.025178215716
 -0.255852300976  1.360631011730 -2.219692209228  0.046781641261
  0.667723897920  1.245157743773 -2.338577856151  0.051249629940
  1.218123388571  2.025110412626 -1.616576841774  0.036378520707
  2.084186643139  2.293984793080 -0.480493513306  0.020680686259
  2.876642520254  1.658030225134  0.559035126479  0.017328959778
  3.282919570196  0.368088739237  1.091983758387  0.019439142426
  3.142768358070 -1.067043033853  0.908139383421  0.018420685756
  2.511469270857 -2.081300494445  0.080016452498  0.017635679319
  1.638028059662 -2.274626132712 -1.065745290088  0.027075303459
  0.866959534092 -1.570765766224 -2.077218589767  0.047090187388
  0.467932162408  1.694535407938 -1.941510815499  0.039921410940
  1.299770347024  2.453875604722 -0.850324284820  0.012814180775
  2.242050270258  2.245320705184  0.385739526123  0.000966482134
  2.923104801922  1.151131828431  1.279135167181  0.002899111710
  3.074302957696 -0.397105856761  1.477477125466  0.004497320895
  2.636007061705 -1.788714946455  0.902532611959  0.001033229263
  1.781095866662 -2.474797662024 -0.218920775640  0.004594689979
  0.846775315928 -2.184739733041 -1.444543821640  0.025173536111
  0.255868904327 -1.360657816862 -2.219684436601  0.046779700716
  0.427045418766 -0.580992742084 -2.585105089006  0.057345833982
  0.427044967836  0.580963354323 -2.585108185092  0.057345479393
 -0.427035838172 -0.000014913339 -2.543076305315  0.062626962546
 end_of_phi


 nsubv after electrostatic potential =  0

  Confirmation of dielectric energy for ground state
  edielnew =  -0.0116314087


 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************


 cosurf_and_qcos_from cosmo(3
  2.466315385316 -0.061539892486  2.837440149000  0.002318474316
  0.006359863210 -2.199287675687  3.114544749440  0.006225770962
 -0.480526931872  1.061509344774  3.755560521090  0.005720373416
  1.262739748330  2.872870034186  1.567866969040  0.005015380390
  1.897949994968 -2.634366215369  0.570962659525  0.001845769013
 -2.790166444607 -0.824331206483  2.170430257018  0.001026554387
 -2.061296060778  2.493528161233  1.034251636268  0.002854524914
 -0.002991703587  2.420846177436 -1.447622679829  0.000732307924
 -1.114435211431 -2.949559801670 -0.067898604227  0.002944266730
  1.955709175590 -0.984462672186  3.123501329999  0.004681551018
  1.000900154469 -1.706149424342  3.300403738319  0.006094427096
  1.652947872482  0.360731428865  3.496571379926  0.005246126975
  0.594940552015  0.670555958619  3.845537291721  0.005597888705
  2.390078334690  1.202722009900  2.566708449184  0.003432125014
  1.989334673621  2.229216060677  2.001028520754  0.003466498068
  2.919143888229  0.378144896597  2.099775822683  0.000478738398
  2.780367109803 -0.921046401167  2.130496470151  0.001480349179
  2.449941849009 -2.011800733381  1.439000205618  0.001874974425
 -0.231540571447 -1.263621120083  3.706953994699  0.006493607388
 -0.352106389130 -0.107618694495  3.950679056227  0.006255184992
  0.105380882100  1.878884201281  3.371423292662  0.008051976536
  0.783266721831  2.590884380442  2.520834408126  0.007162160113
  2.011402526412  2.551496219550  0.814855178005  0.002363861859
  1.440220538007 -2.843720084632  1.356653310131  0.004590176148
  0.871209499702 -2.660587439486  2.372618950166  0.007725587584
 -2.947488997374  0.298617382934  2.058332654596  0.000449651232
 -2.674954162172  1.563027715274  1.704200253745  0.001157318503
 -1.353448939749  2.910920816403  0.212056013565  0.003301487054
 -0.743422400795  2.810699746106 -0.731960510989  0.002229483528
  0.099626804209  1.855073908563 -1.945822518898 -0.000147635966
  0.019900458911 -1.968682238442 -1.864952523830 -0.000401001724
 -0.601526683996 -2.669374282549 -1.032942810695  0.001299869041
 -1.976701730993 -2.578422698078  0.816296596419  0.002529081898
 -2.525639241426 -1.850752473194  1.593331825886  0.001685255849
 -1.124962231191 -1.881571875050  3.121019991266  0.005993802596
 -2.117398618237 -1.513942036836  2.667866282109  0.003974597505
 -2.336046769995 -0.152318885910  2.976109389266  0.004149388425
 -1.478775916646  0.469153703894  3.577445396081  0.005162587129
 -1.328587214463  1.668007282102  3.174271253825  0.006149756974
 -1.771761195387  2.287385908353  2.202255939279  0.005844289465
 -1.026730149698  3.017318998523  1.358608629268  0.005748342529
  0.119020599620  3.173161356543  1.415159765853  0.007069909239
  0.915237473358  3.108118624501  0.463299650838  0.004876206968
  0.462814589486  2.837210699514 -0.795516582628  0.002331000046
  1.026812912753 -3.017319139618  0.083991013862  0.004067598938
 -0.070059393274 -3.176418651270  0.035610954337  0.004675719120
 -0.970268948715 -3.083313212042  1.062474740253  0.006581861398
 -0.534203029846 -2.828286137107  2.231267851728  0.008010933279
  0.871897873699 -0.575451888002  3.799144800425  0.006515855470
  1.408719892640  1.640288870679  3.148120355694  0.007040315094
  2.650053602975  1.633766196698  1.655441764425  0.001065682997
  1.964286464189 -2.001027831890  2.365093852582  0.004748558590
 -1.342877128538 -0.760711330777  3.581800927521  0.005373381671
 -0.531365927285  2.661712102822  2.509437292848  0.008069874127
  1.142904167226  2.885766749848 -0.243475252462  0.001388117626
  0.342129526197 -3.189261637688  1.246854200824  0.006880533428
 -2.346459445268  1.135413320199  2.662806558936  0.003543222628
 -0.258565605057  3.205542092831  0.249849913336  0.004502562175
  0.450566961335 -2.699565911226 -1.032013446782  0.002351612282
 -1.684533476150 -2.430522251364  2.070179818920  0.005003470972
 -3.368784049130  0.036613212616 -1.854972615293 -0.007680105345
 -3.278396927799  1.580834404538 -0.078603229040 -0.005728298345
 -3.656110378332 -0.713812229513  0.361831391088 -0.006428060963
 -2.408052717333 -2.075622947728 -1.226189024363 -0.005374752375
 -1.295829484895 -0.567704086865 -2.747607986898 -0.005898614591
 -1.846557165647  1.681693338816 -2.099716493181 -0.006051266459
 -3.831862607217  0.357468890277 -0.654813288033 -0.008304733724
 -3.527736967644 -1.045671231370 -1.064852892071 -0.008211305212
 -2.622906831544 -1.024738705101 -2.241124222290 -0.008128721628
 -2.402781990555  0.378472303440 -2.579763034114 -0.007906676106
 -3.126606927219  1.313275381963 -1.541857021673 -0.008358562797
 -3.633182667875  0.531520753705  0.561856665829 -0.004514289828
 -3.086813000261 -1.794874586082 -0.179700355757 -0.004488058218
 -1.615880090830 -1.674393887320 -2.147504235692 -0.004629562713
 -1.240258025012  0.765881087348 -2.687977655831 -0.004877581845
 -2.430292517958  2.154437082790 -0.969924007583 -0.004058122179
  3.368776503197 -0.036639040867 -1.854966842961 -0.007685438467
  3.278392620256 -1.580850766330 -0.078589062660 -0.005713299097
  3.656106873706  0.713798214804  0.361832640492 -0.006422561353
  2.408046317685  2.075600470298 -1.226192756897 -0.005382456801
  1.295820311657  0.567673501678 -2.747601655947 -0.006321970035
  1.846549173548 -1.681720471299 -2.099699178989 -0.006003358861
  3.831857249217 -0.357488322766 -0.654806650060 -0.008303770943
  3.527730862121  1.045649613724 -1.064853177123 -0.008213803154
  2.622898581638  1.024710819001 -2.241122746235 -0.008113829621
  2.402773123306 -0.378501994157 -2.579753678917 -0.007896701269
  3.126599952113 -1.313299541573 -1.541844004398 -0.008362197483
  3.633179527908 -0.531533702443  0.561864593522 -0.004509865060
  3.086808508398  1.794857685487 -0.179703829578 -0.004484978560
  1.615872011596  1.674366500124 -2.147504385867 -0.004594203665
  1.240248960489 -0.765911354740 -2.687964116774 -0.005137233486
  2.430286585511 -2.154458194501 -0.969905238283 -0.004043901139
  0.000006852884  1.035694667087 -2.361676372823 -0.000288080053
 -0.298806015181  0.969607486526 -2.408809074493 -0.000345856828
  0.298815704890  0.969607820141 -2.408807386530 -0.000254665825
  0.000007656756 -1.035723195601 -2.361670853435 -0.000281193788
 -0.298805262603 -0.969636498141 -2.408803907288 -0.000331605249
  0.298816457468 -0.969636367899 -2.408802219325 -0.000251196245
 -0.667712940797 -1.245186997899 -2.338574529312 -0.000953550015
 -1.218112188165 -2.025138759161 -1.616566947615 -0.001421318729
 -2.084175601108 -2.294008802021 -0.480477682822 -0.000944070111
 -2.876631896395 -1.658047550445  0.559052047065 -0.001362726540
 -3.282909221331 -0.368099862162  1.091996180628 -0.001811917901
 -3.142757910518  1.067034798172  0.908143333087 -0.001623212836
 -2.511458431963  2.081290260960  0.080011352455 -0.001169502731
 -1.638016880752  2.274609499719 -1.065756198713 -0.001135598216
 -0.866948462969  1.570740798135 -2.077229430584 -0.001973270239
 -0.467915446486 -1.694563900014 -1.941501402531 -0.001439109577
 -1.299753513329 -2.453901457341 -0.850306853788 -0.000247634097
 -2.242033801688 -2.245340287831  0.385760998463 -0.000311162585
 -2.923088754321 -1.151144046328  1.279154738758 -0.000890149282
 -3.074287016292  0.397098864814  1.477489335582 -0.001163717354
 -2.635990824421  1.788708515315  0.902534843950 -0.000524815569
 -1.781079179779  2.474786487342 -0.218927030025 -0.000183517584
 -0.846758459860  2.184720175398 -1.444553386477 -0.000856874720
 -0.255852300976  1.360631011730 -2.219692209228 -0.000924413445
  0.667723897920  1.245157743773 -2.338577856151 -0.000885953490
  1.218123388571  2.025110412626 -1.616576841774 -0.001433036747
  2.084186643139  2.293984793080 -0.480493513306 -0.000921436717
  2.876642520254  1.658030225134  0.559035126479 -0.001356903034
  3.282919570196  0.368088739237  1.091983758387 -0.001800657448
  3.142768358070 -1.067043033853  0.908139383421 -0.001615698468
  2.511469270857 -2.081300494445  0.080016452498 -0.001110116703
  1.638028059662 -2.274626132712 -1.065745290088 -0.001094777285
  0.866959534092 -1.570765766224 -2.077218589767 -0.001921506250
  0.467932162408  1.694535407938 -1.941510815499 -0.001492861949
  1.299770347024  2.453875604722 -0.850324284820 -0.000259322816
  2.242050270258  2.245320705184  0.385739526123 -0.000241278403
  2.923104801922  1.151131828431  1.279135167181 -0.000916787778
  3.074302957696 -0.397105856761  1.477477125466 -0.001193084415
  2.636007061705 -1.788714946455  0.902532611959 -0.000569855595
  1.781095866662 -2.474797662024 -0.218920775640 -0.000238860682
  0.846775315928 -2.184739733041 -1.444543821640 -0.000906949238
  0.255868904327 -1.360657816862 -2.219684436601 -0.000855267433
  0.427045418766 -0.580992742084 -2.585105089006 -0.002241573229
  0.427044967836  0.580963354323 -2.585108185092 -0.002114382386
 -0.427035838172 -0.000014913339 -2.543076305315 -0.006039715346
 end_of_qcos

 sum of the negative charges =  -0.252276104

 sum of the positive charges =   0.247449983

 total sum =  -0.00482612062

 *** qcos is copied to qcosdalton ***

 fepsi =   1.
 *** qcosdalton is mult by fepsi ***
 cosurf and qcosdalton
  2.466315385316 -0.061539892486  2.837440149000  0.002318474316
  0.006359863210 -2.199287675687  3.114544749440  0.006225770962
 -0.480526931872  1.061509344774  3.755560521090  0.005720373416
  1.262739748330  2.872870034186  1.567866969040  0.005015380390
  1.897949994968 -2.634366215369  0.570962659525  0.001845769013
 -2.790166444607 -0.824331206483  2.170430257018  0.001026554387
 -2.061296060778  2.493528161233  1.034251636268  0.002854524914
 -0.002991703587  2.420846177436 -1.447622679829  0.000732307924
 -1.114435211431 -2.949559801670 -0.067898604227  0.002944266730
  1.955709175590 -0.984462672186  3.123501329999  0.004681551018
  1.000900154469 -1.706149424342  3.300403738319  0.006094427096
  1.652947872482  0.360731428865  3.496571379926  0.005246126975
  0.594940552015  0.670555958619  3.845537291721  0.005597888705
  2.390078334690  1.202722009900  2.566708449184  0.003432125014
  1.989334673621  2.229216060677  2.001028520754  0.003466498068
  2.919143888229  0.378144896597  2.099775822683  0.000478738398
  2.780367109803 -0.921046401167  2.130496470151  0.001480349179
  2.449941849009 -2.011800733381  1.439000205618  0.001874974425
 -0.231540571447 -1.263621120083  3.706953994699  0.006493607388
 -0.352106389130 -0.107618694495  3.950679056227  0.006255184992
  0.105380882100  1.878884201281  3.371423292662  0.008051976536
  0.783266721831  2.590884380442  2.520834408126  0.007162160113
  2.011402526412  2.551496219550  0.814855178005  0.002363861859
  1.440220538007 -2.843720084632  1.356653310131  0.004590176148
  0.871209499702 -2.660587439486  2.372618950166  0.007725587584
 -2.947488997374  0.298617382934  2.058332654596  0.000449651232
 -2.674954162172  1.563027715274  1.704200253745  0.001157318503
 -1.353448939749  2.910920816403  0.212056013565  0.003301487054
 -0.743422400795  2.810699746106 -0.731960510989  0.002229483528
  0.099626804209  1.855073908563 -1.945822518898 -0.000147635966
  0.019900458911 -1.968682238442 -1.864952523830 -0.000401001724
 -0.601526683996 -2.669374282549 -1.032942810695  0.001299869041
 -1.976701730993 -2.578422698078  0.816296596419  0.002529081898
 -2.525639241426 -1.850752473194  1.593331825886  0.001685255849
 -1.124962231191 -1.881571875050  3.121019991266  0.005993802596
 -2.117398618237 -1.513942036836  2.667866282109  0.003974597505
 -2.336046769995 -0.152318885910  2.976109389266  0.004149388425
 -1.478775916646  0.469153703894  3.577445396081  0.005162587129
 -1.328587214463  1.668007282102  3.174271253825  0.006149756974
 -1.771761195387  2.287385908353  2.202255939279  0.005844289465
 -1.026730149698  3.017318998523  1.358608629268  0.005748342529
  0.119020599620  3.173161356543  1.415159765853  0.007069909239
  0.915237473358  3.108118624501  0.463299650838  0.004876206968
  0.462814589486  2.837210699514 -0.795516582628  0.002331000046
  1.026812912753 -3.017319139618  0.083991013862  0.004067598938
 -0.070059393274 -3.176418651270  0.035610954337  0.004675719120
 -0.970268948715 -3.083313212042  1.062474740253  0.006581861398
 -0.534203029846 -2.828286137107  2.231267851728  0.008010933279
  0.871897873699 -0.575451888002  3.799144800425  0.006515855470
  1.408719892640  1.640288870679  3.148120355694  0.007040315094
  2.650053602975  1.633766196698  1.655441764425  0.001065682997
  1.964286464189 -2.001027831890  2.365093852582  0.004748558590
 -1.342877128538 -0.760711330777  3.581800927521  0.005373381671
 -0.531365927285  2.661712102822  2.509437292848  0.008069874127
  1.142904167226  2.885766749848 -0.243475252462  0.001388117626
  0.342129526197 -3.189261637688  1.246854200824  0.006880533428
 -2.346459445268  1.135413320199  2.662806558936  0.003543222628
 -0.258565605057  3.205542092831  0.249849913336  0.004502562175
  0.450566961335 -2.699565911226 -1.032013446782  0.002351612282
 -1.684533476150 -2.430522251364  2.070179818920  0.005003470972
 -3.368784049130  0.036613212616 -1.854972615293 -0.007680105345
 -3.278396927799  1.580834404538 -0.078603229040 -0.005728298345
 -3.656110378332 -0.713812229513  0.361831391088 -0.006428060963
 -2.408052717333 -2.075622947728 -1.226189024363 -0.005374752375
 -1.295829484895 -0.567704086865 -2.747607986898 -0.005898614591
 -1.846557165647  1.681693338816 -2.099716493181 -0.006051266459
 -3.831862607217  0.357468890277 -0.654813288033 -0.008304733724
 -3.527736967644 -1.045671231370 -1.064852892071 -0.008211305212
 -2.622906831544 -1.024738705101 -2.241124222290 -0.008128721628
 -2.402781990555  0.378472303440 -2.579763034114 -0.007906676106
 -3.126606927219  1.313275381963 -1.541857021673 -0.008358562797
 -3.633182667875  0.531520753705  0.561856665829 -0.004514289828
 -3.086813000261 -1.794874586082 -0.179700355757 -0.004488058218
 -1.615880090830 -1.674393887320 -2.147504235692 -0.004629562713
 -1.240258025012  0.765881087348 -2.687977655831 -0.004877581845
 -2.430292517958  2.154437082790 -0.969924007583 -0.004058122179
  3.368776503197 -0.036639040867 -1.854966842961 -0.007685438467
  3.278392620256 -1.580850766330 -0.078589062660 -0.005713299097
  3.656106873706  0.713798214804  0.361832640492 -0.006422561353
  2.408046317685  2.075600470298 -1.226192756897 -0.005382456801
  1.295820311657  0.567673501678 -2.747601655947 -0.006321970035
  1.846549173548 -1.681720471299 -2.099699178989 -0.006003358861
  3.831857249217 -0.357488322766 -0.654806650060 -0.008303770943
  3.527730862121  1.045649613724 -1.064853177123 -0.008213803154
  2.622898581638  1.024710819001 -2.241122746235 -0.008113829621
  2.402773123306 -0.378501994157 -2.579753678917 -0.007896701269
  3.126599952113 -1.313299541573 -1.541844004398 -0.008362197483
  3.633179527908 -0.531533702443  0.561864593522 -0.004509865060
  3.086808508398  1.794857685487 -0.179703829578 -0.004484978560
  1.615872011596  1.674366500124 -2.147504385867 -0.004594203665
  1.240248960489 -0.765911354740 -2.687964116774 -0.005137233486
  2.430286585511 -2.154458194501 -0.969905238283 -0.004043901139
  0.000006852884  1.035694667087 -2.361676372823 -0.000288080053
 -0.298806015181  0.969607486526 -2.408809074493 -0.000345856828
  0.298815704890  0.969607820141 -2.408807386530 -0.000254665825
  0.000007656756 -1.035723195601 -2.361670853435 -0.000281193788
 -0.298805262603 -0.969636498141 -2.408803907288 -0.000331605249
  0.298816457468 -0.969636367899 -2.408802219325 -0.000251196245
 -0.667712940797 -1.245186997899 -2.338574529312 -0.000953550015
 -1.218112188165 -2.025138759161 -1.616566947615 -0.001421318729
 -2.084175601108 -2.294008802021 -0.480477682822 -0.000944070111
 -2.876631896395 -1.658047550445  0.559052047065 -0.001362726540
 -3.282909221331 -0.368099862162  1.091996180628 -0.001811917901
 -3.142757910518  1.067034798172  0.908143333087 -0.001623212836
 -2.511458431963  2.081290260960  0.080011352455 -0.001169502731
 -1.638016880752  2.274609499719 -1.065756198713 -0.001135598216
 -0.866948462969  1.570740798135 -2.077229430584 -0.001973270239
 -0.467915446486 -1.694563900014 -1.941501402531 -0.001439109577
 -1.299753513329 -2.453901457341 -0.850306853788 -0.000247634097
 -2.242033801688 -2.245340287831  0.385760998463 -0.000311162585
 -2.923088754321 -1.151144046328  1.279154738758 -0.000890149282
 -3.074287016292  0.397098864814  1.477489335582 -0.001163717354
 -2.635990824421  1.788708515315  0.902534843950 -0.000524815569
 -1.781079179779  2.474786487342 -0.218927030025 -0.000183517584
 -0.846758459860  2.184720175398 -1.444553386477 -0.000856874720
 -0.255852300976  1.360631011730 -2.219692209228 -0.000924413445
  0.667723897920  1.245157743773 -2.338577856151 -0.000885953490
  1.218123388571  2.025110412626 -1.616576841774 -0.001433036747
  2.084186643139  2.293984793080 -0.480493513306 -0.000921436717
  2.876642520254  1.658030225134  0.559035126479 -0.001356903034
  3.282919570196  0.368088739237  1.091983758387 -0.001800657448
  3.142768358070 -1.067043033853  0.908139383421 -0.001615698468
  2.511469270857 -2.081300494445  0.080016452498 -0.001110116703
  1.638028059662 -2.274626132712 -1.065745290088 -0.001094777285
  0.866959534092 -1.570765766224 -2.077218589767 -0.001921506250
  0.467932162408  1.694535407938 -1.941510815499 -0.001492861949
  1.299770347024  2.453875604722 -0.850324284820 -0.000259322816
  2.242050270258  2.245320705184  0.385739526123 -0.000241278403
  2.923104801922  1.151131828431  1.279135167181 -0.000916787778
  3.074302957696 -0.397105856761  1.477477125466 -0.001193084415
  2.636007061705 -1.788714946455  0.902532611959 -0.000569855595
  1.781095866662 -2.474797662024 -0.218920775640 -0.000238860682
  0.846775315928 -2.184739733041 -1.444543821640 -0.000906949238
  0.255868904327 -1.360657816862 -2.219684436601 -0.000855267433
  0.427045418766 -0.580992742084 -2.585105089006 -0.002241573229
  0.427044967836  0.580963354323 -2.585108185092 -0.002114382386
 -0.427035838172 -0.000014913339 -2.543076305315 -0.006039715346
 end of cosurf and qcosdalton

 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 ** Total nuclear repulsion energy ** =   9.36079689758212
 screening nuclear repulsion energy   0.00754686887

 Total-screening nuclear repulsion energy   9.35325003


 Adding T+Vsolv ...
 maxdens  300
 *** End of DALTON-COSMO calculation ***

  original map vector  20 21 22 23 24 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17
 18 19 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0


          sorted Vsolv matrix  block   1

               nbf   1        nbf   2        nbf   3        nbf   4        nbf   5        nbf   6        nbf   7        nbf   8
  nbf   1    -3.31801021
  nbf   2     0.00001615    -3.09061588
  nbf   3     0.00001794    -0.98128897    -4.10173982
  nbf   4    -1.01842604     0.00000424     0.00001709    -4.07781868
  nbf   5    -0.42217614     0.00001263     0.00001057    -0.44126903    -4.75244279
  nbf   6    -0.00000514    -0.00000339     0.00000211     0.00000136     0.00000160    -5.34887106
  nbf   7    -0.00000444    -1.17059716    -0.38131975     0.00000810     0.00000670    -0.00000272    -4.33718442
  nbf   8    -0.41286228     0.00000930     0.00002858     0.04017876     0.09703574    -0.00000436    -0.00000462    -3.58726566
  nbf   9     0.00000274     0.00000103     0.00000188     0.00000125    -0.00000001    -0.00000322     0.00000086    -0.00000228
  nbf  10     0.00000011     0.00000040     0.00000125     0.00000082     0.00000612     0.24656712     0.00000115    -0.00000061
  nbf  11     0.71273257     0.00000333     0.00000651    -0.06900242    -0.45107507     0.00000249     0.00000094     0.37446213
  nbf  12    -0.00002189    -0.02256069    -0.02737017     0.00000060    -0.00004194     0.00000092    -0.10301002    -0.00003959
  nbf  13     0.00001075     0.51225553     1.29214061     0.00000952     0.00000520     0.00000174     1.13411125     0.00000471
  nbf  14     0.60243337     0.00000737     0.00000331     1.39181146     0.61421721     0.00000226     0.00000735     0.29791005
  nbf  15     0.00000114     0.00000005    -0.00000011     0.00000167     0.00000192     0.06453955     0.00000118     0.00000074
  nbf  16     0.00000148     0.00000192     0.00000281     0.00000049     0.00000035    -0.00000236    -0.00000020    -0.00000074
  nbf  17     0.01597565    -0.00000629    -0.00001383     0.28427275    -0.03522093     0.00000102     0.00000137    -0.91938789
  nbf  18    -0.11636703    -0.00000595    -0.00000156    -0.29333667    -0.90109318     0.00000048    -0.00000104    -0.23509227
  nbf  19    -0.00000635     0.16662321    -0.06196103     0.00000036    -0.00000123    -0.00000009     1.19139225    -0.00000735
  nbf  20     0.18303949     0.00000005     0.00000000     0.16560625    -0.11838427    -0.00000046     0.00000014     0.20494900
  nbf  21    -0.99287504    -0.00000498    -0.00001599    -0.72375214     0.04744906     0.00000173     0.00000382    -0.60130441
  nbf  22     0.00000801     1.23665981     0.65676359     0.00001055     0.00000958    -0.00000235     1.45004311     0.00001189
  nbf  23     0.54130182     0.00000782     0.00001383     0.73132309     1.71707524     0.00000016     0.00000305    -0.41969325
  nbf  24     0.00000200     0.00000137    -0.00000148    -0.00000169     0.00000013    -2.13413025     0.00000157     0.00000231

               nbf   9        nbf  10        nbf  11        nbf  12        nbf  13        nbf  14        nbf  15        nbf  16
  nbf   9    -3.42221753
  nbf  10     0.00002016    -3.17750778
  nbf  11    -0.00000297    -0.00000185    -4.03660734
  nbf  12    -0.00000421     0.00000168    -0.00000817    -2.63219374
  nbf  13    -0.00000190    -0.00000169     0.00000132     0.09058543    -3.62260512
  nbf  14    -0.00000169    -0.00000158    -0.18629536     0.00000096     0.00002183    -3.66396880
  nbf  15     0.00000640     0.90765659    -0.00000061     0.00000069     0.00000003     0.00000034    -3.67015859
  nbf  16     0.90251614     0.00000727    -0.00000033    -0.00000092    -0.00000062    -0.00000022     0.00000636    -3.63972261
  nbf  17     0.00000122     0.00000043    -0.42343443     0.00002363    -0.00000515     0.04524246    -0.00000009     0.00000163
  nbf  18     0.00000057    -0.00000152     0.56834177     0.00000996    -0.00000951    -0.46534548    -0.00000085     0.00000048
  nbf  19    -0.00000117    -0.00000060    -0.00000055    -0.08841303     0.44954468     0.00000861    -0.00000105    -0.00000004
  nbf  20    -0.00000001     0.00000003    -0.38281027     0.00000000    -0.00000003    -0.26338736    -0.00000001     0.00000005
  nbf  21    -0.00000281    -0.00000170     1.08738041     0.00001365     0.00000245     0.59831111    -0.00000036     0.00000064
  nbf  22     0.00000388     0.00000216     0.00000592     0.01870731    -0.54202055     0.00000028     0.00000097    -0.00000148
  nbf  23     0.00000114     0.00000554     0.54855083    -0.00002647     0.00000188    -0.34234649    -0.00000283     0.00000120
  nbf  24     0.00000392     0.07854186    -0.00000218    -0.00000058    -0.00000115    -0.00000014     0.08084981     0.00000192

               nbf  17        nbf  18        nbf  19        nbf  20        nbf  21        nbf  22        nbf  23        nbf  24
  nbf  17    -3.38765197
  nbf  18    -0.05579573    -3.03517156
  nbf  19     0.00000618    -0.00000764    -2.89461846
  nbf  20     0.00966313     0.05948992    -0.00000027   -33.05208194
  nbf  21    -0.01753959    -0.18425976     0.00000147     0.58206096    -7.87298309
  nbf  22    -0.00000615     0.00000101    -0.45831044    -0.00000014    -0.00000982    -6.71596107
  nbf  23    -0.12232118     0.30565296    -0.00000425     0.18750709    -0.25170387     0.00001441    -6.95611061
  nbf  24    -0.00000090     0.00000143    -0.00000008    -0.00000041    -0.00000471     0.00000238    -0.00000114    -7.11721012
 insert_onel_diag: nmin2=   380

 onel.diag.
 -0.331801E+01-0.309062E+01-0.410174E+01-0.407782E+01-0.475244E+01-0.534887E+01-0.433718E+01-0.358727E+01-0.342222E+01-0.317751E+01
 -0.403661E+01-0.263219E+01-0.362261E+01-0.366397E+01-0.367016E+01-0.363972E+01-0.338765E+01-0.303517E+01-0.289462E+01-0.330521E+02
 -0.787298E+01-0.671596E+01-0.695611E+01-0.711721E+01

 i,start,strti,fin,filind,bufszi,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095         1       380       380

 4 external diag. modified integrals

  0.299711954 -3.31801021  0.362025844  0.213403987  0.294044049 -3.09061588
  0.345182252  0.318068021  0.34837146  0.293493931  0.497019376 -4.10173982
  0.382867967  0.309072536  0.341195502  0.316765031  0.62103433  0.362850338
  0.531836961 -4.07781868  0.349706612  0.299527054  0.308820027  0.298754537
  0.450260207  0.419223806  0.448476864  0.401601457  0.518535861 -4.75244279
  0.349531774  0.304524501  0.31278089  0.302655019  0.429312411  0.416826853
  0.436921903  0.413400849  0.530775438  0.48280831  0.60679891 -5.34887106
  0.33535564  0.293407704  0.341034277  0.276576801  0.455225842  0.389302194
  0.443351075  0.383054684  0.486303633  0.428587076  0.499742288  0.459023099
  0.475434104 -4.33718442  0.342526573  0.315362897  0.319917749  0.300731659
  0.504389096  0.410870033  0.47482656  0.425010382  0.519270202  0.400100848
  0.453053263  0.422413645  0.446074502  0.406547185  0.476405093 -3.58726566
  0.361060191  0.337659006  0.340604687  0.320198588  0.504515292  0.452245513
  0.527488651  0.469039301  0.434901697  0.420706511  0.454829088  0.417262113
  0.426620519  0.400804422  0.469386808  0.441599006  0.555764969 -3.42221753
  0.348856386  0.329501947  0.326880472  0.312634683  0.490451682  0.436629022
  0.509180653  0.444435002  0.4400708  0.419071921  0.459403052  0.417691971
  0.415311401  0.398901525  0.466192986  0.43088403  0.694750225  0.346946891
  0.501631969 -3.17750778  0.343227876  0.31280524  0.321132402  0.300850073
  0.455432102  0.418963752  0.451109274  0.419591702  0.534502331  0.438245352
  0.597905727  0.464004009  0.508095073  0.408470727  0.460615989  0.424227034
  0.469521607  0.441123033  0.462583019  0.438238584  0.525774157 -4.03660734
  0.356976431  0.333496982  0.334773679  0.315431831  0.522936179  0.430785245
  0.55704189  0.430105919  0.46546176  0.385379923  0.398185515  0.390667038
  0.424333725  0.380522726  0.592974188  0.354449455  0.516100041  0.471421051
  0.491982029  0.453400696  0.428233148  0.409923576  0.53092711 -2.63219374
  0.370351482  0.34231438  0.377526483  0.320334123  0.57663332  0.44254372
  0.563518308  0.477512966  0.511232086  0.469131823  0.508472502  0.490097443
  0.529535469  0.421869959  0.499545867  0.467575397  0.539123293  0.488956188
  0.512741012  0.475743005  0.515621907  0.477676678  0.512163042  0.474196449
  0.592144122 -3.62260512  0.388203856  0.340302294  0.365601016  0.328125926
  0.536368018  0.473948976  0.615526601  0.455378076  0.520967947  0.463416877
  0.546457699  0.489981129  0.48581785  0.455063687  0.509165453  0.46499119
  0.535052608  0.504064949  0.518029026  0.481875024  0.516561248  0.489811629
  0.517714869  0.482829445  0.717425907  0.445100321  0.607835731 -3.6639688
  0.361030445  0.336028491  0.330453775  0.322428351  0.486741558  0.469418774
  0.514189842  0.450314389  0.575178104  0.539226259  0.630438312  0.577562728
  0.510895782  0.499926663  0.503939385  0.490371469  0.52041881  0.467839603
  0.554043059  0.445274854  0.591832537  0.558697254  0.453957255  0.445032703
  0.568738744  0.548069735  0.596778585  0.560916765  0.750165186 -3.67015859
  0.353015389  0.338054232  0.358115278  0.317291888  0.522721553  0.462375435
  0.489534774  0.469144866  0.543456123  0.532600344  0.632201055  0.577477753
  0.547273656  0.513624874  0.507616587  0.483570727  0.564197636  0.455577875
  0.524876517  0.46718588  0.585160178  0.562784851  0.458775281  0.447244069
  0.587371194  0.556188564  0.58998874  0.568912261  0.721594827  0.645098881
  0.755127757 -3.63972261  0.366037972  0.335416332  0.343455146  0.320873443
  0.50940609  0.476326958  0.527676719  0.456821536  0.589439367  0.537772005
  0.579789349  0.559672288  0.53269644  0.49854365  0.567546289  0.469810852
  0.495610134  0.480892777  0.493939617  0.479062411  0.584343602  0.533457424
  0.502525388  0.442000941  0.582221799  0.560044775  0.596954899  0.56140811
  0.713528596  0.656785472  0.69947109  0.625487345  0.729807063 -3.38765197
  0.353987843  0.337616982  0.351789554  0.321182817  0.550334788  0.456688986
  0.522995786  0.46803517  0.566057852  0.507622395  0.615570104  0.560439732
  0.539018372  0.503813066  0.510463413  0.481771744  0.500103129  0.48786777
  0.496692123  0.477703001  0.585735173  0.540040646  0.469784387  0.453592071
  0.609978757  0.53850005  0.643846451  0.534124473  0.686162365  0.635949394
  0.700906311  0.666564985  0.679632027  0.617528828  0.696112735 -3.03517156
  0.358856446  0.335907395  0.354506012  0.319503646  0.548011068  0.469120392
  0.531509325  0.460743131  0.577501451  0.534350115  0.577624788  0.562928162
  0.587296462  0.4866605  0.514004213  0.4958057  0.493523583  0.479933652
  0.487740555  0.474497517  0.570979461  0.537487177  0.471960345  0.452931079
  0.634007692  0.54138526  0.615254278  0.550385293  0.691778305  0.63576614
  0.699509207  0.640665078  0.699234432  0.657364716  0.711993958  0.616407541
  0.721280492 -2.89461846
 i,start,strti,fin,filind,bufszi,nmbuf,nmin2, nd4ext = 
         1         1         2         2        12      4095         1       380       380

 all internall diag. modified integrals

  4.73971342 -33.0520819  1.04569563  0.0636788604  0.755179462 -7.87298309
  0.878477817  0.0193493095  0.682719812  0.155759544  0.668673763 -6.71596107
  0.986988872  0.0301374391  0.681192227  0.125736446  0.608457144
  0.0402117459  0.732528817 -6.95611061  1.03831833  0.0301199817  0.713926708
  0.128455235  0.624391224  0.0304166993  0.665957609  0.0433645062
  0.760228983 -7.11721012
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331801E+01-0.309062E+01-0.410174E+01-0.407782E+01-0.475244E+01-0.534887E+01-0.433718E+01-0.358727E+01-0.342222E+01-0.317751E+01
 -0.403661E+01-0.263219E+01-0.362261E+01-0.366397E+01-0.367016E+01-0.363972E+01-0.338765E+01-0.303517E+01-0.289462E+01-0.330521E+02
 -0.787298E+01-0.671596E+01-0.695611E+01-0.711721E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331801E+01-0.309062E+01-0.410174E+01-0.407782E+01-0.475244E+01-0.534887E+01-0.433718E+01-0.358727E+01-0.342222E+01-0.317751E+01
 -0.403661E+01-0.263219E+01-0.362261E+01-0.366397E+01-0.367016E+01-0.363972E+01-0.338765E+01-0.303517E+01-0.289462E+01-0.330521E+02
 -0.787298E+01-0.671596E+01-0.695611E+01-0.711721E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331801E+01-0.309062E+01-0.410174E+01-0.407782E+01-0.475244E+01-0.534887E+01-0.433718E+01-0.358727E+01-0.342222E+01-0.317751E+01
 -0.403661E+01-0.263219E+01-0.362261E+01-0.366397E+01-0.367016E+01-0.363972E+01-0.338765E+01-0.303517E+01-0.289462E+01-0.330521E+02
 -0.787298E+01-0.671596E+01-0.695611E+01-0.711721E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380
 DIAGEL COUNTS: DG0X,DG2X,DG4X= 440 145 30
 =========== Executing IN-CORE method ==========
 norm multnew:  1.
 threx: total number of loops= 14
 threx: total number of loops= 24
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331801E+01-0.309062E+01-0.410174E+01-0.407782E+01-0.475244E+01-0.534887E+01-0.433718E+01-0.358727E+01-0.342222E+01-0.317751E+01
 -0.403661E+01-0.263219E+01-0.362261E+01-0.366397E+01-0.367016E+01-0.363972E+01-0.338765E+01-0.303517E+01-0.289462E+01-0.330521E+02
 -0.787298E+01-0.671596E+01-0.695611E+01-0.711721E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331801E+01-0.309062E+01-0.410174E+01-0.407782E+01-0.475244E+01-0.534887E+01-0.433718E+01-0.358727E+01-0.342222E+01-0.317751E+01
 -0.403661E+01-0.263219E+01-0.362261E+01-0.366397E+01-0.367016E+01-0.363972E+01-0.338765E+01-0.303517E+01-0.289462E+01-0.330521E+02
 -0.787298E+01-0.671596E+01-0.695611E+01-0.711721E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331801E+01-0.309062E+01-0.410174E+01-0.407782E+01-0.475244E+01-0.534887E+01-0.433718E+01-0.358727E+01-0.342222E+01-0.317751E+01
 -0.403661E+01-0.263219E+01-0.362261E+01-0.366397E+01-0.367016E+01-0.363972E+01-0.338765E+01-0.303517E+01-0.289462E+01-0.330521E+02
 -0.787298E+01-0.671596E+01-0.695611E+01-0.711721E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        40 xx:       135 ww:       255
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        20 yw:        25

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        39
task #   9:       127    task #  10:       247    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 matrix-vector product formed for vector   1
 =========== Executing IN-CORE method ==========
 norm multnew:  2.61957985E-06
 threx: total number of loops= 14
 threx: total number of loops= 24
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331801E+01-0.309062E+01-0.410174E+01-0.407782E+01-0.475244E+01-0.534887E+01-0.433718E+01-0.358727E+01-0.342222E+01-0.317751E+01
 -0.403661E+01-0.263219E+01-0.362261E+01-0.366397E+01-0.367016E+01-0.363972E+01-0.338765E+01-0.303517E+01-0.289462E+01-0.330521E+02
 -0.787298E+01-0.671596E+01-0.695611E+01-0.711721E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331801E+01-0.309062E+01-0.410174E+01-0.407782E+01-0.475244E+01-0.534887E+01-0.433718E+01-0.358727E+01-0.342222E+01-0.317751E+01
 -0.403661E+01-0.263219E+01-0.362261E+01-0.366397E+01-0.367016E+01-0.363972E+01-0.338765E+01-0.303517E+01-0.289462E+01-0.330521E+02
 -0.787298E+01-0.671596E+01-0.695611E+01-0.711721E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380
 blstr diagel,allext,nmin2=   380
 onel.diag.
 -0.331801E+01-0.309062E+01-0.410174E+01-0.407782E+01-0.475244E+01-0.534887E+01-0.433718E+01-0.358727E+01-0.342222E+01-0.317751E+01
 -0.403661E+01-0.263219E+01-0.362261E+01-0.366397E+01-0.367016E+01-0.363972E+01-0.338765E+01-0.303517E+01-0.289462E+01-0.330521E+02
 -0.787298E+01-0.671596E+01-0.695611E+01-0.711721E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1       380       380


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        40 xx:       135 ww:       255
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        20 yw:        25

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        39
task #   9:       127    task #  10:       247    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 matrix-vector product formed for vector   2
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference-subspace vector overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97564481    -0.00150012

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97554035    -0.19309127
 subspace has dimension  2

          (h-repnuc*1) matrix in the subspace basis

                x:   1         x:   2
   x:   1   -85.60687741
   x:   2     0.11696634    -0.00022196

          overlap matrix in the subspace basis

                x:   1         x:   2
   x:   1     1.00000000
   x:   2    -0.00136634     0.00000262

          eigenvectors and eigenvalues in the subspace basis

                v:   1         v:   2

   energy   -85.60687831   -82.56575469

   x:   1     1.00085303     1.57432924
   x:   2     0.62442878  1152.62338067

          eigenvectors and eigenvalues in the schmidt orthonormalized basis

                v:   1         v:   2

   energy   -85.60687831   -82.56575469

  zx:   1     0.99999985    -0.00054175
  zx:   2     0.00054175     0.99999985

          <ref|baseci> overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97564481    -0.00150012

 trial vector basis is being transformed.  new dimension:   1

          transformed tciref transformation matrix  block   1

               ref   1
  ci:   1     0.97554035

  write the transformed vectors back to cosmofile

 rtflw =  0
 freezing =  F
 resid(rootcalc)   0.00094769872
 *******************************************
 ** Charges frozen from cosmo calculation **
 *******************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  8  1    -76.2536282771 -9.3533E+00  2.9676E-07  9.4770E-04  1.0000E-03
 mr-sdci #  8  2    -73.2125046586 -8.9705E+00  0.0000E+00  1.7571E+00  1.0000E-04


 total energy in cosmo calc 
e(rootcalc) + repnuc=     -76.2536282771
dielectric energy =      -0.0116288825
deltaediel =       0.0116288825
e(rootcalc) + repnuc - ediel =     -76.2419993946
e(rootcalc) + repnuc - edielnew =     -76.2536282771
deltaelast =      76.2419993946


 mr-sdci  convergence criteria satisfied after  8 iterations.

 *************************************************
 ***Final Calc of density matrix for cosmo calc***
 *************************************************

 entering drivercid: firstcall,firstnonref= F T
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=      10  D0X=      30  D0W=      50
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================

          ci-one-electron density block   1

                mo   1         mo   2         mo   3         mo   4         mo   5         mo   6         mo   7         mo   8
   mo   1     1.99991386
   mo   2    -0.00021025     1.98511168
   mo   3     0.00000000    -0.00000003     1.96966952
   mo   4    -0.00015543     0.00413784    -0.00000010     1.97323368
   mo   5     0.00000000    -0.00000007    -0.00000001    -0.00000008     1.97619057
   mo   6     0.00012178    -0.00394438    -0.00002052    -0.02136150    -0.00000852     0.00699914
   mo   7     0.00000000     0.00000790    -0.01066388    -0.00002412    -0.00000497    -0.00000006     0.00752457
   mo   8    -0.00000001     0.00001593    -0.01289919    -0.00002234     0.00000238    -0.00000020     0.00719350     0.01062094
   mo   9     0.00005672     0.00307203    -0.00001233    -0.00030013     0.00000191     0.00710687    -0.00000025    -0.00000037
   mo  10    -0.00022101     0.01058993    -0.00001415     0.00337772     0.00000009     0.00261592    -0.00000021    -0.00000015
   mo  11     0.00000004    -0.00000177     0.00000296    -0.00000043    -0.01178414     0.00000023     0.00000010    -0.00000009
   mo  12    -0.00000002    -0.00000283    -0.00571665    -0.00000613    -0.00000251     0.00000005     0.00612917     0.00409174
   mo  13     0.00020430    -0.00111642    -0.00001285    -0.00840681    -0.00000313     0.00197126    -0.00000004    -0.00000019
   mo  14     0.00000000     0.00000215    -0.00000331    -0.00000116    -0.00000202    -0.00000003    -0.00000003    -0.00000002
   mo  15     0.00000000     0.00000112    -0.00000191    -0.00000501    -0.00351365     0.00000002    -0.00000001    -0.00000001
   mo  16    -0.00039025    -0.00017777    -0.00000464     0.00291068     0.00000176    -0.00196421    -0.00000012    -0.00000005
   mo  17    -0.00000001    -0.00000916     0.00104121     0.00002194     0.00000050    -0.00000003     0.00027582     0.00024948
   mo  18     0.00000004    -0.00000059     0.01163875    -0.00000055     0.00000089    -0.00000012    -0.00287333    -0.00440516
   mo  19    -0.00010535    -0.00449386    -0.00000012     0.00760190     0.00000042    -0.00312505    -0.00000015    -0.00000009
   mo  20     0.00000000     0.00000021    -0.00000050     0.00000140     0.00206313    -0.00000003    -0.00000001     0.00000000
   mo  21     0.00000000    -0.00000023     0.00000059    -0.00000061    -0.00000127     0.00000001    -0.00000001    -0.00000002
   mo  22     0.00001088     0.00012793     0.00000267    -0.00120961     0.00000030    -0.00006091     0.00000004     0.00000004
   mo  23     0.00004994     0.00174303    -0.00000045     0.00032500    -0.00000050     0.00063925     0.00000000    -0.00000003
   mo  24    -0.00000001    -0.00000071    -0.00029232     0.00000192     0.00000009     0.00000000    -0.00083323     0.00034401

                mo   9         mo  10         mo  11         mo  12         mo  13         mo  14         mo  15         mo  16
   mo   9     0.01012110
   mo  10     0.00335772     0.01011967
   mo  11    -0.00000006    -0.00000002     0.01452970
   mo  12     0.00000003     0.00000005     0.00000002     0.00653980
   mo  13     0.00053208    -0.00065426     0.00000007    -0.00000002     0.00373917
   mo  14    -0.00000003     0.00000000     0.00000010    -0.00000002     0.00000000     0.00297248
   mo  15    -0.00000003    -0.00000006    -0.00138116    -0.00000001     0.00000003    -0.00000006     0.00224347
   mo  16     0.00002251     0.00265158    -0.00000003    -0.00000007    -0.00125716     0.00000002    -0.00000002     0.00439413
   mo  17     0.00000020     0.00000028    -0.00000001     0.00035948    -0.00000003     0.00000000     0.00000000     0.00000007
   mo  18    -0.00000015    -0.00000018     0.00000001    -0.00184630     0.00000002     0.00000000     0.00000000    -0.00000003
   mo  19    -0.00438231    -0.00082925     0.00000000    -0.00000014    -0.00084281     0.00000000    -0.00000001    -0.00005784
   mo  20    -0.00000001     0.00000000     0.00004194     0.00000000    -0.00000001     0.00000000    -0.00182750     0.00000000
   mo  21     0.00000000    -0.00000001     0.00000003     0.00000000     0.00000001    -0.00202453     0.00000000    -0.00000001
   mo  22    -0.00056676     0.00029603    -0.00000001     0.00000002     0.00172459     0.00000000     0.00000000     0.00070401
   mo  23     0.00024472     0.00178232     0.00000000     0.00000004     0.00025013     0.00000000     0.00000000    -0.00068854
   mo  24     0.00000001     0.00000000     0.00000001    -0.00169151     0.00000001     0.00000000     0.00000000     0.00000003

                mo  17         mo  18         mo  19         mo  20         mo  21         mo  22         mo  23         mo  24
   mo  17     0.00051365
   mo  18    -0.00020483     0.00262428
   mo  19     0.00000002     0.00000006     0.00288630
   mo  20     0.00000000     0.00000000     0.00000001     0.00255506
   mo  21     0.00000000     0.00000001     0.00000000     0.00000001     0.00242704
   mo  22    -0.00000001     0.00000001    -0.00016081     0.00000000     0.00000000     0.00197672
   mo  23     0.00000004    -0.00000002     0.00061285     0.00000000     0.00000000    -0.00008831     0.00164243
   mo  24     0.00000338    -0.00057501     0.00000002     0.00000000     0.00000000    -0.00000001    -0.00000001     0.00145103


*****   symmetry block SYM1   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       1.99991386
       2      -0.00021025     1.98511168
       3       0.00000000    -0.00000003     1.96966952
       4      -0.00015543     0.00413784    -0.00000010     1.97323368
       5       0.00000000    -0.00000007    -0.00000001    -0.00000008
       6       0.00012178    -0.00394438    -0.00002052    -0.02136150
       7       0.00000000     0.00000790    -0.01066388    -0.00002412
       8      -0.00000001     0.00001593    -0.01289919    -0.00002234
       9       0.00005672     0.00307203    -0.00001233    -0.00030013
      10      -0.00022101     0.01058993    -0.00001415     0.00337772
      11       0.00000004    -0.00000177     0.00000296    -0.00000043
      12      -0.00000002    -0.00000283    -0.00571665    -0.00000613
      13       0.00020430    -0.00111642    -0.00001285    -0.00840681
      14       0.00000000     0.00000215    -0.00000331    -0.00000116
      15       0.00000000     0.00000112    -0.00000191    -0.00000501
      16      -0.00039025    -0.00017777    -0.00000464     0.00291068
      17      -0.00000001    -0.00000916     0.00104121     0.00002194
      18       0.00000004    -0.00000059     0.01163875    -0.00000055
      19      -0.00010535    -0.00449386    -0.00000012     0.00760190
      20       0.00000000     0.00000021    -0.00000050     0.00000140
      21       0.00000000    -0.00000023     0.00000059    -0.00000061
      22       0.00001088     0.00012793     0.00000267    -0.00120961
      23       0.00004994     0.00174303    -0.00000045     0.00032500
      24      -0.00000001    -0.00000071    -0.00029232     0.00000192

               Column   5     Column   6     Column   7     Column   8
       5       1.97619057
       6      -0.00000852     0.00699914
       7      -0.00000497    -0.00000006     0.00752457
       8       0.00000238    -0.00000020     0.00719350     0.01062094
       9       0.00000191     0.00710687    -0.00000025    -0.00000037
      10       0.00000009     0.00261592    -0.00000021    -0.00000015
      11      -0.01178414     0.00000023     0.00000010    -0.00000009
      12      -0.00000251     0.00000005     0.00612917     0.00409174
      13      -0.00000313     0.00197126    -0.00000004    -0.00000019
      14      -0.00000202    -0.00000003    -0.00000003    -0.00000002
      15      -0.00351365     0.00000002    -0.00000001    -0.00000001
      16       0.00000176    -0.00196421    -0.00000012    -0.00000005
      17       0.00000050    -0.00000003     0.00027582     0.00024948
      18       0.00000089    -0.00000012    -0.00287333    -0.00440516
      19       0.00000042    -0.00312505    -0.00000015    -0.00000009
      20       0.00206313    -0.00000003    -0.00000001     0.00000000
      21      -0.00000127     0.00000001    -0.00000001    -0.00000002
      22       0.00000030    -0.00006091     0.00000004     0.00000004
      23      -0.00000050     0.00063925     0.00000000    -0.00000003
      24       0.00000009     0.00000000    -0.00083323     0.00034401

               Column   9     Column  10     Column  11     Column  12
       9       0.01012110
      10       0.00335772     0.01011967
      11      -0.00000006    -0.00000002     0.01452970
      12       0.00000003     0.00000005     0.00000002     0.00653980
      13       0.00053208    -0.00065426     0.00000007    -0.00000002
      14      -0.00000003     0.00000000     0.00000010    -0.00000002
      15      -0.00000003    -0.00000006    -0.00138116    -0.00000001
      16       0.00002251     0.00265158    -0.00000003    -0.00000007
      17       0.00000020     0.00000028    -0.00000001     0.00035948
      18      -0.00000015    -0.00000018     0.00000001    -0.00184630
      19      -0.00438231    -0.00082925     0.00000000    -0.00000014
      20      -0.00000001     0.00000000     0.00004194     0.00000000
      21       0.00000000    -0.00000001     0.00000003     0.00000000
      22      -0.00056676     0.00029603    -0.00000001     0.00000002
      23       0.00024472     0.00178232     0.00000000     0.00000004
      24       0.00000001     0.00000000     0.00000001    -0.00169151

               Column  13     Column  14     Column  15     Column  16
      13       0.00373917
      14       0.00000000     0.00297248
      15       0.00000003    -0.00000006     0.00224347
      16      -0.00125716     0.00000002    -0.00000002     0.00439413
      17      -0.00000003     0.00000000     0.00000000     0.00000007
      18       0.00000002     0.00000000     0.00000000    -0.00000003
      19      -0.00084281     0.00000000    -0.00000001    -0.00005784
      20      -0.00000001     0.00000000    -0.00182750     0.00000000
      21       0.00000001    -0.00202453     0.00000000    -0.00000001
      22       0.00172459     0.00000000     0.00000000     0.00070401
      23       0.00025013     0.00000000     0.00000000    -0.00068854
      24       0.00000001     0.00000000     0.00000000     0.00000003

               Column  17     Column  18     Column  19     Column  20
      17       0.00051365
      18      -0.00020483     0.00262428
      19       0.00000002     0.00000006     0.00288630
      20       0.00000000     0.00000000     0.00000001     0.00255506
      21       0.00000000     0.00000001     0.00000000     0.00000001
      22      -0.00000001     0.00000001    -0.00016081     0.00000000
      23       0.00000004    -0.00000002     0.00061285     0.00000000
      24       0.00000338    -0.00057501     0.00000002     0.00000000

               Column  21     Column  22     Column  23     Column  24
      21       0.00242704
      22       0.00000000     0.00197672
      23       0.00000000    -0.00008831     0.00164243
      24       0.00000000    -0.00000001    -0.00000001     0.00145103

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7        eno   8
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    8   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno   9        eno  10        eno  11        eno  12        eno  13        eno  14        eno  15        eno  16
 emo    9   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   10   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   11   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   12   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   13   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   14   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   15   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   16   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno  17        eno  18        eno  19        eno  20        eno  21        eno  22        eno  23        eno  24
 emo   17   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   18   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   19   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   20   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   21   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   22   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   23   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   24   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   1.9999189      1.9865361      1.9762697      1.9721946      1.9698999
   0.0215483      0.0197793      0.0146234      0.0103342      0.0054300
   0.0051654      0.0047426      0.0042244      0.0041337      0.0010152
   0.0010110      0.0006569      0.0005692      0.0004927      0.0004919
   0.0004480      0.0004307      0.0000505      0.0000331

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7         no   8
  mo    1   0.999821E+00   0.186783E-01  -0.606739E-07   0.312340E-02  -0.706168E-06   0.300778E-07  -0.331013E-04  -0.239051E-07
  mo    2  -0.168220E-01   0.951727E+00  -0.987303E-06  -0.306417E+00   0.716091E-04  -0.563657E-05  -0.285559E-02   0.107822E-05
  mo    3   0.113551E-06  -0.480027E-05   0.385274E-06   0.218441E-03   0.999941E+00   0.102233E-01   0.179243E-04  -0.165296E-05
  mo    4  -0.869327E-02   0.306269E+00   0.127021E-04   0.951823E+00  -0.206741E-03   0.122648E-04   0.674539E-02   0.181599E-08
  mo    5   0.156023E-06  -0.298341E-05   0.999980E+00  -0.124490E-04  -0.387501E-06   0.137293E-05   0.193981E-05   0.573540E-02
  mo    6   0.187733E-03  -0.518806E-02  -0.446114E-05  -0.974585E-02  -0.838070E-05  -0.248443E-03   0.534961E+00   0.546083E-05
  mo    7   0.363927E-07   0.102519E-06  -0.252877E-05  -0.141663E-04  -0.547589E-02   0.569868E+00   0.247744E-03   0.397314E-05
  mo    8  -0.437645E-07   0.423897E-05   0.119318E-05  -0.148237E-04  -0.662283E-02   0.636475E+00   0.265901E-03  -0.806110E-05
  mo    9   0.429077E-05   0.142606E-02   0.951072E-06  -0.671303E-03  -0.618687E-05  -0.296118E-03   0.672772E+00  -0.143328E-04
  mo   10  -0.215378E-03   0.561868E-02   0.573209E-07  -0.285513E-04  -0.721319E-05  -0.179685E-03   0.406764E+00  -0.139557E-04
  mo   11   0.367547E-07  -0.903203E-06  -0.600559E-02   0.146440E-06   0.151922E-05   0.273330E-05   0.127199E-04   0.993031E+00
  mo   12   0.425154E-07  -0.228804E-05  -0.128024E-05  -0.323957E-05  -0.294775E-02   0.440993E+00   0.217563E-03   0.940549E-06
  mo   13   0.148763E-03  -0.183894E-02  -0.164768E-05  -0.390419E-02  -0.569561E-05  -0.564589E-04   0.840258E-01   0.556369E-05
  mo   14  -0.133620E-07   0.853384E-06  -0.102206E-05  -0.893222E-06  -0.168049E-05  -0.378692E-05  -0.302419E-05   0.769594E-05
  mo   15   0.113025E-07  -0.229661E-06  -0.177667E-02  -0.257370E-05  -0.971319E-06  -0.215510E-05  -0.525251E-05  -0.115654E+00
  mo   16  -0.207270E-03   0.374225E-03   0.916194E-06   0.144683E-02  -0.267004E-05  -0.256808E-05  -0.462616E-02  -0.741305E-05
  mo   17  -0.257071E-07  -0.100809E-05   0.250509E-06   0.121237E-04   0.525901E-03   0.256904E-01   0.343096E-04  -0.654697E-06
  mo   18   0.291984E-07  -0.405334E-06   0.457174E-06   0.117120E-05   0.594141E-02  -0.270417E+00  -0.135833E-03   0.183731E-05
  mo   19  -0.482501E-04  -0.979643E-03   0.268593E-06   0.439189E-02  -0.990157E-06   0.115575E-03  -0.291625E+00   0.430997E-05
  mo   20  -0.656826E-08   0.315311E-06   0.104680E-02   0.632034E-06  -0.251651E-06  -0.449104E-07  -0.236171E-06   0.219448E-01
  mo   21   0.589340E-08  -0.206712E-06  -0.643234E-06  -0.258140E-06   0.300909E-06  -0.335939E-06   0.173469E-06   0.269176E-06
  mo   22   0.965309E-05  -0.126056E-03   0.140672E-06  -0.607013E-03   0.148252E-05   0.393952E-05  -0.666655E-02  -0.680469E-07
  mo   23   0.884194E-05   0.889268E-03  -0.253333E-06  -0.116854E-03  -0.207562E-06  -0.251408E-04   0.592650E-01  -0.147801E-05
  mo   24  -0.783884E-08  -0.401055E-07   0.452542E-07   0.100964E-05  -0.146534E-03  -0.422557E-01  -0.215374E-04  -0.149623E-06

                no   9         no  10         no  11         no  12         no  13         no  14         no  15         no  16
  mo    1   0.228180E-03  -0.329513E-07  -0.977989E-04  -0.121261E-08   0.438854E-04   0.211716E-09  -0.655907E-07   0.195404E-04
  mo    2  -0.420359E-02   0.434138E-05  -0.336263E-03  -0.890340E-06   0.360766E-03   0.236109E-06  -0.765274E-05   0.106728E-02
  mo    3   0.120948E-05  -0.228980E-02   0.561047E-05   0.146025E-05   0.148238E-05  -0.323902E-06  -0.160156E-02  -0.337829E-05
  mo    4  -0.650872E-02  -0.225657E-06   0.399408E-02   0.362205E-06   0.143951E-02  -0.218274E-05   0.138279E-04  -0.275414E-02
  mo    5  -0.175636E-05   0.187559E-05   0.238604E-05   0.224095E-06  -0.399992E-06  -0.250449E-02  -0.930025E-06  -0.103981E-05
  mo    6  -0.257310E+00   0.403811E-04   0.189855E+00   0.289314E-05  -0.124610E+00  -0.349072E-05   0.294905E-03  -0.133827E+00
  mo    7  -0.121747E-04   0.219133E+00  -0.641814E-04   0.337854E-05   0.545231E-05  -0.626677E-06   0.410212E-01   0.106021E-03
  mo    8   0.529691E-05  -0.545812E+00   0.176449E-03  -0.443546E-05   0.581749E-05  -0.101156E-05   0.401202E+00   0.891968E-03
  mo    9  -0.167468E+00  -0.127660E-03  -0.365601E+00  -0.299337E-05   0.461416E-01   0.618018E-06  -0.885926E-03   0.401288E+00
  mo   10   0.767148E+00   0.100863E-03   0.320159E+00   0.193350E-05  -0.143637E+00  -0.668445E-05   0.657251E-03  -0.285466E+00
  mo   11   0.134089E-04  -0.628176E-05  -0.822314E-05  -0.917704E-05   0.355743E-05  -0.927176E-01   0.189625E-05   0.336894E-05
  mo   12  -0.529768E-05   0.639018E+00  -0.189051E-03   0.906345E-05  -0.346038E-05   0.372759E-06  -0.292181E+00  -0.655423E-03
  mo   13  -0.255860E+00   0.192405E-03   0.603807E+00   0.190101E-04   0.448947E+00   0.922442E-05  -0.155964E-03   0.568615E-01
  mo   14   0.334776E-05  -0.890119E-05  -0.137642E-04   0.752828E+00  -0.886823E-05  -0.184481E-04  -0.651019E-05  -0.593632E-08
  mo   15  -0.656861E-05   0.161692E-05   0.241572E-06  -0.266855E-04   0.128201E-04  -0.653714E+00  -0.107487E-05  -0.639795E-06
  mo   16   0.467940E+00  -0.103083E-03  -0.362388E+00  -0.869137E-06   0.557229E+00   0.770482E-05  -0.660852E-03   0.283187E+00
  mo   17   0.129360E-04   0.187121E-01   0.117739E-04   0.230004E-05   0.197899E-05   0.421859E-06  -0.246648E+00  -0.581200E-03
  mo   18  -0.690543E-05   0.284308E+00  -0.100962E-03   0.287311E-05  -0.305138E-05   0.131204E-05   0.617713E+00   0.137281E-02
  mo   19   0.149883E+00   0.595111E-04   0.186839E+00  -0.146420E-05  -0.277742E+00  -0.649719E-05  -0.100655E-02   0.427829E+00
  mo   20   0.200620E-05  -0.156773E-05   0.790775E-07   0.186278E-04  -0.140960E-04   0.751036E+00  -0.206530E-05   0.150310E-05
  mo   21  -0.191510E-05   0.815691E-05   0.117237E-04  -0.658217E+00   0.987413E-05   0.279499E-04  -0.743512E-05   0.142004E-05
  mo   22   0.239794E-01   0.994995E-04   0.318115E+00   0.116504E-04   0.523289E+00   0.100024E-04  -0.243791E-03   0.135897E+00
  mo   23   0.984786E-01   0.968816E-04   0.309451E+00   0.191299E-05  -0.314135E+00  -0.764588E-05  -0.147086E-02   0.674255E+00
  mo   24   0.261529E-05  -0.405635E+00   0.131241E-03  -0.555888E-05   0.889662E-05  -0.153264E-05  -0.556392E+00  -0.123849E-02

                no  17         no  18         no  19         no  20         no  21         no  22         no  23         no  24
  mo    1  -0.175859E-08  -0.279239E-07   0.432857E-04   0.503821E-07   0.435835E-07  -0.355767E-04   0.266532E-04   0.492574E-08
  mo    2  -0.669272E-06  -0.371486E-05   0.266233E-02   0.272298E-05   0.559675E-05  -0.196434E-02   0.176741E-02  -0.813836E-07
  mo    3   0.902348E-06  -0.584715E-03   0.208674E-05   0.833065E-06  -0.167021E-02  -0.799627E-05   0.119201E-05   0.163689E-02
  mo    4   0.605697E-06   0.729167E-05   0.180911E-02   0.362353E-05  -0.542841E-05  -0.186232E-02   0.606920E-02   0.124971E-05
  mo    5   0.994273E-06   0.133171E-05  -0.107528E-06   0.107339E-02  -0.129249E-06  -0.130284E-05   0.236829E-05   0.119770E-05
  mo    6  -0.613959E-05  -0.175528E-03   0.470317E+00   0.564716E-03   0.323019E-03  -0.209330E+00   0.559451E+00   0.567950E-04
  mo    7  -0.182664E-04   0.563623E+00   0.122696E-03  -0.581487E-05   0.185258E+00   0.485725E-04  -0.470324E-04   0.522999E+00
  mo    8   0.133383E-04  -0.186327E+00  -0.708249E-04   0.153079E-05   0.519279E-01   0.285341E-04   0.268456E-04  -0.313953E+00
  mo    9   0.163776E-05   0.868396E-05  -0.110108E-01  -0.132782E-04  -0.133088E-04   0.265060E-01  -0.470829E+00  -0.506011E-04
  mo   10   0.192368E-05   0.472124E-04  -0.542571E-01  -0.619093E-04  -0.451670E-04   0.959404E-02  -0.195796E+00  -0.149221E-04
  mo   11  -0.148623E-04  -0.409630E-05  -0.914172E-04   0.725096E-01   0.118875E-05   0.317028E-05  -0.708676E-05  -0.335089E-05
  mo   12   0.353034E-05  -0.145416E+00   0.400969E-05   0.350261E-05  -0.124560E+00  -0.597230E-04   0.535595E-04  -0.524539E+00
  mo   13   0.651106E-05   0.309894E-04  -0.331859E+00  -0.404250E-03   0.287214E-03  -0.462859E+00  -0.183623E+00  -0.257082E-04
  mo   14   0.658217E+00   0.249483E-04   0.113490E-04   0.101138E-03  -0.468960E-05   0.232724E-05   0.168897E-05   0.124800E-05
  mo   15  -0.101346E-03   0.438614E-05  -0.896675E-03   0.747849E+00   0.151239E-04  -0.827239E-05  -0.256787E-05   0.272192E-06
  mo   16  -0.182117E-05  -0.719292E-04   0.303755E-01   0.345491E-04   0.299672E-03  -0.331483E+00   0.384943E+00   0.328743E-04
  mo   17   0.158316E-04  -0.377344E+00  -0.416103E-03  -0.163296E-04   0.888872E+00   0.852866E-03   0.525270E-06   0.753141E-01
  mo   18  -0.604223E-05   0.385708E+00   0.117876E-04  -0.869453E-05   0.372604E+00   0.211947E-03   0.405397E-04  -0.420515E+00
  mo   19  -0.815141E-05  -0.260788E-03   0.581302E+00   0.690794E-03   0.552357E-03  -0.401193E+00  -0.313866E+00  -0.330281E-04
  mo   20  -0.101893E-03   0.432698E-05  -0.790830E-03   0.659895E+00   0.127250E-04  -0.805878E-05  -0.145879E-05  -0.477224E-07
  mo   21   0.752828E+00   0.261546E-04   0.107523E-04   0.103018E-03  -0.289942E-05   0.164158E-05   0.657237E-06   0.920595E-06
  mo   22  -0.951062E-05  -0.386561E-04   0.439674E+00   0.533927E-03  -0.409433E-03   0.638233E+00  -0.723160E-01   0.657804E-06
  mo   23   0.431863E-05   0.150430E-03  -0.365303E+00  -0.433699E-03  -0.326432E-03   0.250920E+00   0.375924E+00   0.306602E-04
  mo   24  -0.250685E-04   0.579056E+00   0.134532E-03  -0.724946E-05   0.136152E+00   0.772018E-04   0.475531E-04  -0.412623E+00
  eigenvectors of nos in ao-basis, column    1
  0.1000604E+01 -0.1372967E-01 -0.1673739E-02  0.6889902E-07  0.1943647E-06 -0.7381892E-02  0.8466463E-07 -0.5530717E-07
  0.1486884E-03  0.8041947E-08 -0.1169121E-07  0.4228022E-04  0.1611738E-07  0.9066606E-04 -0.3265779E-02  0.2144469E-02
 -0.1014526E-02  0.4242654E-08 -0.9343309E-03 -0.3265882E-02  0.2144614E-02  0.1014546E-02  0.1808892E-07 -0.9343850E-03
  eigenvectors of nos in ao-basis, column    2
  0.9517571E-02  0.9130288E+00 -0.7095724E-01 -0.3255820E-05 -0.3996750E-05  0.1252950E+00 -0.4930419E-05  0.1342093E-05
  0.7752960E-01 -0.3316677E-06  0.4249838E-06 -0.1206166E-02 -0.2114255E-06  0.1113069E-02  0.2165457E+00 -0.1058434E+00
  0.2696431E-01  0.3589343E-06  0.2156673E-01  0.2165562E+00 -0.1058509E+00 -0.2696644E-01 -0.6547165E-06  0.2156504E-01
  eigenvectors of nos in ao-basis, column    3
 -0.8294045E-06 -0.2194153E-05  0.3345238E-05  0.2804560E-05  0.9104830E+00  0.1090941E-04 -0.2471935E-05  0.8288881E-01
  0.1817456E-05 -0.5579667E-06 -0.1689479E-01 -0.9811492E-07 -0.1404591E-06 -0.2642029E-06 -0.7833192E-05  0.1057716E-04
  0.7446079E-06  0.3056379E-01 -0.3441801E-06 -0.2382328E-05  0.8049662E-06 -0.2827265E-06  0.3056569E-01 -0.5250027E-06
  eigenvectors of nos in ao-basis, column    4
  0.1093787E-02 -0.1763510E-02  0.2169707E+00  0.1737157E-03 -0.1105546E-04  0.8020749E+00 -0.3155429E-04  0.1834557E-06
 -0.2022021E-01 -0.1616474E-06  0.1299465E-05 -0.4710333E-02 -0.5972129E-05 -0.2409590E-02 -0.4298924E+00  0.1837406E+00
 -0.3806968E-01 -0.2830514E-05  0.2189088E-02 -0.4296794E+00  0.1836562E+00  0.3807039E-01 -0.1407862E-05  0.2223202E-02
  eigenvectors of nos in ao-basis, column    5
  0.3357731E-06  0.2500332E-05 -0.5437052E-04  0.7310639E+00  0.1848517E-05 -0.1615732E-03 -0.1182892E+00 -0.1929171E-05
  0.6192329E-06  0.5393003E-06 -0.1074662E-06  0.8845766E-06 -0.2540663E-01  0.8820419E-06 -0.5519228E+00  0.1930716E+00
 -0.2034913E-01 -0.1893640E-05 -0.3126597E-01  0.5521343E+00 -0.1931556E+00 -0.2036830E-01  0.1833530E-06  0.3125324E-01
  eigenvectors of nos in ao-basis, column    6
 -0.4674923E-04 -0.3665986E-03  0.6714589E-04 -0.1306396E+01  0.5258059E-05  0.5341878E-03  0.5802048E+00 -0.3210930E-05
 -0.3081549E-03  0.1279734E-06  0.2604002E-06  0.1911154E-05 -0.6500454E-01  0.1314905E-04 -0.1005168E+01  0.3581742E+00
 -0.3972507E-01 -0.4348275E-05 -0.1795552E-01  0.1006022E+01 -0.3585267E+00 -0.3976346E-01  0.1105082E-05  0.1792675E-01
  eigenvectors of nos in ao-basis, column    7
  0.1149395E+00  0.8554192E+00 -0.1688706E+00 -0.6062202E-03  0.2006137E-04 -0.1210115E+01  0.2961049E-03 -0.1809641E-04
  0.7549062E+00  0.5758732E-06  0.5013659E-06 -0.3929324E-02 -0.2783055E-04 -0.3145299E-01 -0.9753702E+00  0.4154267E+00
 -0.4957920E-01 -0.6024097E-05 -0.9675036E-02 -0.9744863E+00  0.4151015E+00  0.4954986E-01 -0.2005821E-05 -0.9609129E-02
  eigenvectors of nos in ao-basis, column    8
  0.4146891E-05  0.2026917E-04 -0.2273036E-04  0.1513033E-05  0.1412897E+01  0.2804219E-04  0.2593720E-05 -0.1611474E+01
 -0.2771026E-04 -0.7034964E-06  0.3698023E-01  0.1729066E-06  0.1487449E-05  0.7947625E-06  0.3550925E-04 -0.3622647E-04
  0.2571433E-07 -0.7462224E-01  0.1974471E-05  0.4424381E-05 -0.1447449E-05  0.1056036E-05 -0.7463300E-01  0.1387706E-05
  eigenvectors of nos in ao-basis, column    9
 -0.4409089E+00 -0.2257307E+01  0.2143829E+01  0.2846173E-05  0.1774068E-04 -0.7476750E+00 -0.1485117E-04 -0.1946503E-04
  0.1099496E+01 -0.2485103E-05  0.3121219E-05 -0.1275224E-01 -0.5533287E-05  0.6082606E-01  0.3723196E+00 -0.2057721E+00
 -0.1276885E-01 -0.2663170E-05  0.7493667E-01  0.3723592E+00 -0.2058200E+00  0.1277260E-01 -0.5797987E-05  0.7495971E-01
  eigenvectors of nos in ao-basis, column   10
  0.8300510E-04  0.3651820E-03 -0.5179860E-03 -0.4939093E+00 -0.7250620E-05 -0.4750241E-05  0.1038483E+01  0.1026636E-04
  0.5965666E-04  0.9893990E-05 -0.1904653E-05 -0.3120599E-04  0.6164186E+00  0.8316148E-04  0.6580988E+00 -0.4998578E+00
  0.1116058E+00 -0.2365541E-05 -0.2164276E-01 -0.6576844E+00  0.4996253E+00  0.1116633E+00  0.3633276E-05  0.2178616E-01
  eigenvectors of nos in ao-basis, column   11
  0.2870819E+00  0.1281302E+01 -0.1766069E+01  0.1309648E-03 -0.9414163E-05 -0.4251692E-01 -0.3124355E-03  0.1386158E-04
  0.2218561E+00  0.1434545E-04  0.3642268E-07 -0.9826179E-01 -0.2021170E-03  0.2650396E+00  0.6421919E+00 -0.3801086E+00
 -0.6748425E-01 -0.4728470E-05  0.2205125E+00  0.6426336E+00 -0.3804399E+00  0.6742637E-01  0.5225767E-05  0.2205271E+00
  eigenvectors of nos in ao-basis, column   12
  0.5498678E-05  0.2624176E-04 -0.2824763E-04 -0.8603342E-05 -0.1080986E-04  0.4833399E-06  0.1216642E-04  0.2534485E-04
 -0.6646736E-05 -0.8028444E+00  0.2378063E-04 -0.4255121E-05  0.7844397E-05  0.1968424E-05 -0.7898872E-06 -0.1973930E-06
 -0.2244674E-05  0.2657324E+00  0.3675921E-05 -0.3824286E-05  0.3849429E-05  0.6359156E-05 -0.2657585E+00  0.7277408E-05
  eigenvectors of nos in ao-basis, column   13
 -0.1283393E+00 -0.5433543E+00  0.8910879E+00  0.2938343E-05  0.3687311E-05  0.9780885E-01 -0.5606514E-05 -0.9100206E-05
 -0.5118817E+00  0.1173108E-04 -0.1679774E-04 -0.2106718E+00 -0.1274555E-04 -0.2135571E+00 -0.4697858E+00  0.2702920E+00
 -0.1642817E+00  0.1836430E-05  0.9234174E-01 -0.4697886E+00  0.2703049E+00  0.1642887E+00  0.6481083E-05  0.9233894E-01
  eigenvectors of nos in ao-basis, column   14
  0.3933005E-06  0.1739985E-05 -0.7662981E-06  0.5552229E-06 -0.8382919E-01  0.5365025E-05 -0.7145740E-06  0.3025066E+00
 -0.1326691E-04  0.3234839E-04  0.8903064E+00 -0.3988672E-05  0.2309029E-05 -0.5016156E-05 -0.5419538E-05  0.5749610E-05
 -0.2176080E-05 -0.1991805E+00  0.8940517E-06 -0.4542652E-05  0.4243665E-05  0.2703994E-05 -0.1991765E+00  0.1634633E-05
  eigenvectors of nos in ao-basis, column   15
  0.8300998E-03  0.3169920E-02 -0.6139838E-02  0.4997230E+00  0.1875269E-05 -0.3618982E-03 -0.1296222E+01 -0.1956430E-05
  0.2689456E-02 -0.7133257E-05 -0.1979197E-05  0.4962632E-04  0.8078061E+00 -0.9058878E-03 -0.1402534E+01  0.7245806E+00
 -0.7298976E-01 -0.8988662E-05  0.2547914E+00  0.1412549E+01 -0.7302099E+00 -0.7329657E-01  0.5613453E-05 -0.2542293E+00
  eigenvectors of nos in ao-basis, column   16
 -0.3574519E+00 -0.1355388E+01  0.2707740E+01  0.1108984E-02  0.3846965E-05  0.1232376E+00 -0.2872818E-02 -0.5857501E-05
 -0.1154248E+01  0.1523443E-05  0.1678350E-05 -0.2985782E-01  0.1799459E-02  0.4144483E+00 -0.2256796E+01  0.1269303E+01
 -0.7491087E-01  0.6407632E-06 -0.1590829E+00 -0.2250567E+01  0.1266132E+01  0.7454715E-01 -0.4334923E-06 -0.1602551E+00
  eigenvectors of nos in ao-basis, column   17
  0.4263465E-05  0.2033382E-04 -0.6966819E-05 -0.2637039E-04 -0.1399476E-04 -0.1042037E-04 -0.2043566E-04  0.1183211E-03
 -0.4135957E-05  0.7223875E+00 -0.9030543E-04  0.2802140E-05  0.2984949E-04  0.9413602E-06 -0.1041626E-03  0.8575135E-04
 -0.2276587E-04  0.7384764E+00 -0.2288064E-04  0.5837586E-04 -0.5109123E-04  0.3129692E-05 -0.7387157E+00  0.2423768E-04
  eigenvectors of nos in ao-basis, column   18
  0.1266942E-03  0.5766637E-03 -0.2142663E-03  0.8943007E+00 -0.4812380E-05 -0.3348816E-03  0.1278781E+00  0.2992786E-05
  0.1198564E-03  0.2482882E-04  0.3817297E-05  0.1246159E-04 -0.6609577E+00  0.5854239E-04  0.1763898E+01 -0.1624394E+01
  0.3864325E+00  0.3224893E-04  0.6953036E+00 -0.1764766E+01  0.1625071E+01  0.3869856E+00 -0.2192687E-04 -0.6956505E+00
  eigenvectors of nos in ao-basis, column   19
 -0.2142037E+00 -0.1046009E+01 -0.1592211E+00  0.8030516E-04 -0.7470752E-04  0.6983540E+00  0.4646032E-03  0.9429356E-03
  0.3458034E+00  0.1006638E-04 -0.6832607E-03 -0.1273496E+00 -0.1540261E-03 -0.1171543E+00  0.1592494E+01 -0.1110135E+01
  0.9029272E+00 -0.1001578E-02  0.1508805E+00  0.1591177E+01 -0.1109192E+01 -0.9031586E+00 -0.1025349E-02  0.1501862E+00
  eigenvectors of nos in ao-basis, column   20
 -0.2548925E-03 -0.1245233E-02 -0.1890997E-03 -0.1778179E-04  0.5794987E-01  0.8257999E-03  0.2290093E-04 -0.7801711E+00
  0.4295375E-03  0.9742553E-04  0.5701923E+00 -0.1545681E-03  0.8358020E-05 -0.1380793E-03  0.1898581E-02 -0.1319015E-02
  0.1063403E-02  0.8455046E+00  0.1737954E-03  0.1914613E-02 -0.1345016E-02 -0.1098216E-02  0.8452873E+00  0.1662318E-03
  eigenvectors of nos in ao-basis, column   21
 -0.3384856E-03 -0.1460906E-02  0.1043670E-02  0.6160526E+00  0.6374076E-06  0.6777318E-03 -0.1253426E+01 -0.1512027E-04
 -0.8187880E-03 -0.2504425E-05  0.1088660E-04  0.1118322E-03 -0.1487442E+00 -0.1701611E-03 -0.3096242E+00 -0.1087390E+00
  0.7466709E+00  0.1251501E-04 -0.3539581E+00  0.3102004E+00  0.1080352E+00  0.7461728E+00  0.2116122E-04  0.3554432E+00
  eigenvectors of nos in ao-basis, column   22
  0.3014960E+00  0.1257388E+01 -0.1221371E+01  0.3629261E-03  0.3766549E-05 -0.4977867E+00 -0.9815249E-03  0.2087255E-05
  0.1032332E+01  0.1460217E-05 -0.7095008E-05 -0.1762873E+00 -0.9629534E-04  0.1600416E+00  0.2190422E+00  0.2740183E-01
  0.6314641E-01 -0.7456544E-05 -0.8052541E+00  0.2198367E+00  0.2718399E-01 -0.6195312E-01 -0.1190179E-04 -0.8043773E+00
  eigenvectors of nos in ao-basis, column   23
 -0.3247146E+00 -0.1122060E+01  0.3944398E+01  0.3827179E-04 -0.7651909E-05 -0.3606409E+00  0.1584141E-03  0.1411326E-04
 -0.9525896E+00  0.4870072E-06 -0.1127355E-05  0.2191399E-01 -0.6394084E-04  0.1876252E+00 -0.1193651E+01 -0.4599689E+00
 -0.7598605E+00 -0.1127734E-05 -0.5062871E+00 -0.1193864E+01 -0.4601795E+00  0.7600177E+00 -0.3946705E-05 -0.5064092E+00
  eigenvectors of nos in ao-basis, column   24
 -0.2825559E-04 -0.9875327E-04  0.3524569E-03 -0.3602182E+00 -0.3641317E-05 -0.3627025E-04 -0.1588215E+01  0.5587201E-05
 -0.6797763E-04  0.8262489E-06 -0.1006549E-06 -0.8810983E-07  0.5565216E+00  0.1555332E-04 -0.9402437E+00 -0.1211323E+01
 -0.7209733E+00  0.1417138E-05 -0.6507556E+00  0.9400832E+00  0.1211192E+01 -0.7208408E+00 -0.9973667E-06  0.6506479E+00


 total number of electrons =   10.0000000000


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***


 cosurf_and_pot_on_the_outer_cavity
  4.050775607358 -0.101071808985  4.196955442697 -0.015748773685
  0.010441682970 -3.612198255232  4.652083638187 -0.024774732565
 -0.789241570581  1.743471269654  5.704914774762 -0.027060265037
  2.073972598936  4.718525890344  2.111755307006 -0.014119777111
  3.117268498212 -4.326789461396  0.474397938180 -0.002876541584
 -4.582690188881 -1.353912193893  3.101430474760 -0.009182394517
 -3.385562959628  4.095478918779  1.235323147076 -0.005910422581
 -0.004917721800  3.976102986090 -2.841011122095  0.011697454966
 -1.830399064625 -4.844476601686 -0.574894544435 -0.001123321996
  3.212134594290 -1.616918816342  4.666794301023 -0.020124967646
  1.643916289252 -2.802247348226  4.957346221664 -0.024374753553
  2.714866756337  0.592484285675  5.279540167909 -0.023118277701
  0.977151244524  1.101353062742  5.852696389317 -0.027237549291
  3.925560684092  1.975404862085  3.752294365964 -0.014708147244
  3.267362519836  3.661361660599  2.823197971886 -0.013633951233
  4.794520096155  0.621084894014  2.985384674181 -0.007550930823
  4.566587306007 -1.512761278186  3.035841551564 -0.008983107728
  4.023883025923 -3.304261852898  1.900099140339 -0.006393619151
 -0.380295949534 -2.075420336871  5.625081381127 -0.027088674114
 -0.578318295438 -0.176753562285  6.025385624624 -0.027860415093
  0.173077949460  3.085961949671  5.073991710630 -0.025846872169
  1.286466029251  4.255380848585  3.676948920622 -0.021166418437
  3.303607685031  4.190688084329  0.874977219987 -0.004333155720
  2.365474477330 -4.670641019924  1.764849152111 -0.011920391537
  1.430906928951 -4.369856297518  3.433513648100 -0.020186101020
 -4.841083335141  0.490465576515  2.917316680086 -0.006980079318
 -4.393460713370  2.567186035154  2.335674800780 -0.006663601747
 -2.222965217239  4.781022087881 -0.115085361143 -0.002731462482
 -1.221032093667  4.616414806724 -1.665577618041  0.004460103835
  0.163627211063  3.046854928202 -3.659275392660  0.018547204029
  0.032681324165 -3.233442231849 -3.526451127503  0.017350833174
 -0.987976628576 -4.384288176387 -2.159923546337  0.007497699893
 -3.246621691231 -4.234905486775  0.877344665928 -0.004585345647
 -4.148219637146 -3.039749448812  2.153579853861 -0.007028341010
 -1.847689082544 -3.090368524535  4.662718846419 -0.023273704268
 -3.477708148477 -2.486557889095  3.918440225273 -0.016936873809
 -3.836825025639 -0.250171027837  4.424711607668 -0.017309542543
 -2.428807490341  0.770561568790  5.412371037466 -0.024096564594
 -2.182131278907  2.739608870207  4.750180949167 -0.022904511058
 -2.910018776645  3.756902253439  3.153702307544 -0.016044971960
 -1.686348599859  4.955774916073  1.768060650114 -0.013450896331
  0.195480392480  5.211736928518  1.860942604097 -0.015346547312
  1.503220315915  5.104908022693  0.297567705890 -0.006782960863
  0.760142032813  4.659957506362 -1.769964770589  0.004408275186
  1.686476488177 -4.955767374770 -0.325424677516 -0.002959185926
 -0.115072502826 -5.217079072686 -0.404886112491 -0.004433166581
 -1.593614359873 -5.064158801861  1.281677954493 -0.011454816070
 -0.877401440800 -4.645291658435  3.201352687003 -0.020017015873
  1.432037543220 -0.945142383016  5.776499419601 -0.026604798878
  2.313736498748  2.694082990980  4.707229619388 -0.022519619566
  4.352554947409  2.683369878192  2.255591816868 -0.006490238563
  3.226222292135 -3.286567988530  3.421154112824 -0.016042127602
 -2.205601631966 -1.249420246876  5.419524744627 -0.024568395724
 -0.872741664792  4.371711264704  3.658229821401 -0.021526395502
  1.877149624159  4.739707996010 -0.863268981216  0.000787849962
  0.561923832933 -5.238172931193  1.584510498744 -0.014034432257
 -3.853927239386  1.864854252547  3.910129924132 -0.015543599629
 -0.424682996015  5.264920405270 -0.053011078089 -0.006248321361
  0.740026015645 -4.433876171161 -2.158397120143  0.007099360491
 -2.766752319178 -3.991987601911  2.936774958699 -0.015645609271
 -5.025900900119  0.067742504061 -3.125151150316  0.019862067659
 -4.858684725656  2.924551709117  0.161132214251  0.006144328233
 -5.557454609143 -1.320544563879  0.975936261489  0.005447623123
 -3.248547936294 -3.839894392576 -1.961901507096  0.011118799998
 -1.190934956284 -1.050244499978 -4.776526587786  0.024126773885
 -2.209781165676  3.111140737530 -3.577927324410  0.017618597944
 -5.882596232580  0.661325507734 -0.904856394886  0.013857379859
 -5.319963799369 -1.934483717314 -1.663429662357  0.015093904507
 -3.646028047584 -1.895758543717 -3.839531623260  0.020290804441
 -3.238797091754  0.700181822085 -4.466013425136  0.022430050786
 -4.577873224583  2.429567517352 -2.545887302119  0.016715587022
 -5.515038344798  0.983321455076  1.345983019759  0.003750406133
 -4.504254459710 -3.320509923530 -0.025897470176  0.005657728257
 -1.783028577264 -3.097620630820 -3.666334648055  0.017908767272
 -1.088127755501  1.416888072316 -4.666210475313  0.023711579807
 -3.289691567450  3.985716663883 -1.487811226052  0.008838762874
  5.025891038603 -0.067775101219 -3.125148233055  0.019857625182
  4.858680855164 -2.924566793325  0.161150660501  0.006141398167
  5.557452224045  1.320533821773  0.975930811333  0.005444065559
  3.248540195407  3.839867994436 -1.961916173837  0.011114830566
  1.190922084255  1.050203102491 -4.776522637080  0.024124558260
  2.209770478753 -3.111175747518 -3.577903054707  0.017616093038
  5.882590418741 -0.661346272731 -0.904851876187  0.013853407247
  5.319956602612  1.934458909776 -1.663437951255  0.015089539550
  3.646016883720  1.895722139538 -3.839536654111  0.020286542272
  3.238784785806 -0.700221564805 -4.466003879574  0.022426208244
  4.577864419098 -2.429597027524 -2.545870981713  0.016711765246
  5.515036634319 -0.983330225135  1.345989924439  0.003747369838
  4.504250248226  3.320493842536 -0.025911658296  0.005653823370
  1.783017729142  3.097585149616 -3.666342687430  0.017905645777
  1.088115084594 -1.416928881883 -4.666193189608  0.023709913842
  3.289684690884 -3.985740535441 -1.487784264400  0.008836192638
 end_of_cosurf_and_pot_on_the_outer_cavity


 **************************************************
 ***Final cosmo calculation for ground state*******
 **************************************************

  ediel before cosmo(4 =  -0.0116288825
  elast before cosmo(4 =  -76.2419994


 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  8  1    -76.2536282771 -9.3533E+00  2.9676E-07  9.4770E-04  1.0000E-03

####################CIUDGINFO####################

   ci vector at position   1 energy=  -76.253628277133

################END OF CIUDGINFO################


    1 of the   2 expansion vectors are transformed.

          transformed tciref transformation matrix  block   1

               ref   1
  ci:   1     0.97554035
    1 of the   1 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
 maximum overlap with reference  1(overlap=  0.975540346)
 ============== writing to civout ======================
 1 32768 4656
 6 5 6
 #cirefvfl= 1 #civfl= 1 method= 0 last= 1 overlap= 98%(  0.975540346)
 Hermit Integral Program : SIFS version  hochtor2        Thu Oct  7 14:40:51 2004
  cidrt_title                                                                    
  title                                                                          
 mofmt: formatted orbitals label=morbl   hochtor2        Thu Oct  7 14:40:52 2004
 SIFS file created by program tran.      hochtor2        Thu Oct  7 14:40:52 2004
 energy computed by program ciudg.       hochtor2        Thu Oct  7 14:41:26 2004
 energy of type -1=  9.31791392
 energy of type -1026= -76.2536283
 energy of type -2055=  0.00094769872
 energy of type -2056= -9.3532823
 energy of type -2057=  2.96763269E-07

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.97554035    -0.19309127     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.97554035     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 ==================================================

information on vector: 1from unit 11 written to unit 16filename civout                                                      


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -76.2536282771

                                                       internal orbitals

                                          level       1    2    3    4    5

                                          orbital     1    2    3    4    5

                                         symmetry   a    a    a    a    a  

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1  0.975540                        +-   +-   +-   +-   +- 
 y   1  1      21  0.015849              1( a  )   +-   +-   +-    -   +- 
 y   1  1      42 -0.010153              3( a  )   +-   +-    -   +-   +- 
 x   1  1     107  0.012308    1( a  )   6( a  )   +-   +-   +-    -    - 
 x   1  1     110  0.017639    4( a  )   6( a  )   +-   +-   +-    -    - 
 x   1  1     111  0.032670    5( a  )   6( a  )   +-   +-   +-    -    - 
 x   1  1     127 -0.014587    3( a  )   9( a  )   +-   +-   +-    -    - 
 x   1  1     140 -0.011068    8( a  )  10( a  )   +-   +-   +-    -    - 
 x   1  1     151  0.011941   10( a  )  11( a  )   +-   +-   +-    -    - 
 x   1  1     195  0.010389    8( a  )  15( a  )   +-   +-   +-    -    - 
 x   1  1     198  0.010754   11( a  )  15( a  )   +-   +-   +-    -    - 
 x   1  1     231 -0.014313   15( a  )  17( a  )   +-   +-   +-    -    - 
 x   1  1     279 -0.025255    2( a  )   6( a  )   +-   +-    -   +-    - 
 x   1  1     280 -0.014466    3( a  )   6( a  )   +-   +-    -   +-    - 
 x   1  1     288  0.027332    6( a  )   7( a  )   +-   +-    -   +-    - 
 x   1  1     299  0.020231    4( a  )   9( a  )   +-   +-    -   +-    - 
 x   1  1     306  0.013880    3( a  )  10( a  )   +-   +-    -   +-    - 
 x   1  1     321 -0.011410    9( a  )  11( a  )   +-   +-    -   +-    - 
 x   1  1     339 -0.010166    6( a  )  13( a  )   +-   +-    -   +-    - 
 x   1  1     354  0.011099    9( a  )  14( a  )   +-   +-    -   +-    - 
 x   1  1     376 -0.010392    4( a  )  16( a  )   +-   +-    -   +-    - 
 x   1  1     386  0.011320   14( a  )  16( a  )   +-   +-    -   +-    - 
 x   1  1     419 -0.012178   16( a  )  18( a  )   +-   +-    -   +-    - 
 x   1  1     446 -0.023597    2( a  )   5( a  )   +-   +-    -    -   +- 
 x   1  1     447 -0.021922    3( a  )   5( a  )   +-   +-    -    -   +- 
 x   1  1     457  0.013420    4( a  )   7( a  )   +-   +-    -    -   +- 
 x   1  1     458  0.018972    5( a  )   7( a  )   +-   +-    -    -   +- 
 x   1  1     462 -0.019966    3( a  )   8( a  )   +-   +-    -    -   +- 
 x   1  1     509 -0.012394    5( a  )  13( a  )   +-   +-    -    -   +- 
 x   1  1     561 -0.011569    3( a  )  17( a  )   +-   +-    -    -   +- 
 x   1  1     620 -0.015117    1( a  )   6( a  )   +-    -   +-   +-    - 
 x   1  1     660 -0.015072    6( a  )  11( a  )   +-    -   +-   +-    - 
 x   1  1     787 -0.011204    1( a  )   5( a  )   +-    -   +-    -   +- 
 x   1  1     952  0.010371    1( a  )   2( a  )   +-    -    -   +-   +- 
 x   1  1     967  0.010373    1( a  )   7( a  )   +-    -    -   +-   +- 
 w   1  1    1827 -0.047711    6( a  )   6( a  )   +-   +-   +-   +-      
 w   1  1    1872 -0.012887   11( a  )  11( a  )   +-   +-   +-   +-      
 w   1  1    1901  0.010615    4( a  )  14( a  )   +-   +-   +-   +-      
 w   1  1    1926 -0.012424   15( a  )  15( a  )   +-   +-   +-   +-      
 w   1  1    1935  0.010881    9( a  )  16( a  )   +-   +-   +-   +-      
 w   1  1    1942 -0.012046   16( a  )  16( a  )   +-   +-   +-   +-      
 w   1  1    2015 -0.013403    4( a  )   6( a  )   +-   +-   +-   +     - 
 w   1  1    2016 -0.034835    5( a  )   6( a  )   +-   +-   +-   +     - 
 w   1  1    2035  0.011986    3( a  )   9( a  )   +-   +-   +-   +     - 
 w   1  1    2057 -0.010205    6( a  )  11( a  )   +-   +-   +-   +     - 
 w   1  1    2119 -0.010036    3( a  )  16( a  )   +-   +-   +-   +     - 
 w   1  1    2187 -0.011784    1( a  )   1( a  )   +-   +-   +-        +- 
 w   1  1    2191 -0.015285    2( a  )   3( a  )   +-   +-   +-        +- 
 w   1  1    2192 -0.023024    3( a  )   3( a  )   +-   +-   +-        +- 
 w   1  1    2193 -0.013230    1( a  )   4( a  )   +-   +-   +-        +- 
 w   1  1    2196 -0.013777    4( a  )   4( a  )   +-   +-   +-        +- 
 w   1  1    2200 -0.016690    4( a  )   5( a  )   +-   +-   +-        +- 
 w   1  1    2201 -0.033360    5( a  )   5( a  )   +-   +-   +-        +- 
 w   1  1    2222 -0.014477    8( a  )   8( a  )   +-   +-   +-        +- 
 w   1  1    2246 -0.016531    5( a  )  11( a  )   +-   +-   +-        +- 
 w   1  1    2267  0.012650    3( a  )  13( a  )   +-   +-   +-        +- 
 w   1  1    2306 -0.010519   15( a  )  15( a  )   +-   +-   +-        +- 
 w   1  1    2339 -0.011576   17( a  )  17( a  )   +-   +-   +-        +- 
 w   1  1    2393  0.023802    2( a  )   6( a  )   +-   +-   +    +-    - 
 w   1  1    2394  0.014535    3( a  )   6( a  )   +-   +-   +    +-    - 
 w   1  1    2403  0.027337    6( a  )   7( a  )   +-   +-   +    +-    - 
 w   1  1    2424 -0.010813    3( a  )  10( a  )   +-   +-   +    +-    - 
 w   1  1    2460 -0.011291    6( a  )  13( a  )   +-   +-   +    +-    - 
 w   1  1    2568  0.015532    1( a  )   2( a  )   +-   +-   +     -   +- 
 w   1  1    2570  0.015436    1( a  )   3( a  )   +-   +-   +     -   +- 
 w   1  1    2574  0.019806    2( a  )   4( a  )   +-   +-   +     -   +- 
 w   1  1    2575  0.032991    3( a  )   4( a  )   +-   +-   +     -   +- 
 w   1  1    2578  0.018182    2( a  )   5( a  )   +-   +-   +     -   +- 
 w   1  1    2591  0.011102    4( a  )   7( a  )   +-   +-   +     -   +- 
 w   1  1    2592  0.020455    5( a  )   7( a  )   +-   +-   +     -   +- 
 w   1  1    2620  0.013022    9( a  )  10( a  )   +-   +-   +     -   +- 
 w   1  1    2624  0.010017    3( a  )  11( a  )   +-   +-   +     -   +- 
 w   1  1    2660 -0.012324    3( a  )  14( a  )   +-   +-   +     -   +- 
 w   1  1    2757 -0.012180    1( a  )   1( a  )   +-   +-        +-   +- 
 w   1  1    2759 -0.023710    2( a  )   2( a  )   +-   +-        +-   +- 
 w   1  1    2761 -0.025041    2( a  )   3( a  )   +-   +-        +-   +- 
 w   1  1    2762 -0.032739    3( a  )   3( a  )   +-   +-        +-   +- 
 w   1  1    2763 -0.017217    1( a  )   4( a  )   +-   +-        +-   +- 
 w   1  1    2766 -0.025920    4( a  )   4( a  )   +-   +-        +-   +- 
 w   1  1    2779 -0.024142    2( a  )   7( a  )   +-   +-        +-   +- 
 w   1  1    2780 -0.011620    3( a  )   7( a  )   +-   +-        +-   +- 
 w   1  1    2784 -0.021193    7( a  )   7( a  )   +-   +-        +-   +- 
 w   1  1    2801 -0.014361    9( a  )   9( a  )   +-   +-        +-   +- 
 w   1  1    2837  0.014770    3( a  )  13( a  )   +-   +-        +-   +- 
 w   1  1    2847 -0.010266   13( a  )  13( a  )   +-   +-        +-   +- 
 w   1  1    2962  0.025014    1( a  )   6( a  )   +-   +    +-   +-    - 
 w   1  1    2965  0.017658    4( a  )   6( a  )   +-   +    +-   +-    - 
 w   1  1    2980  0.014174    6( a  )   8( a  )   +-   +    +-   +-    - 
 w   1  1    3007 -0.020298    6( a  )  11( a  )   +-   +    +-   +-    - 
 w   1  1    3043 -0.016737    6( a  )  14( a  )   +-   +    +-   +-    - 
 w   1  1    3055 -0.010608    4( a  )  15( a  )   +-   +    +-   +-    - 
 w   1  1    3068 -0.010394    2( a  )  16( a  )   +-   +    +-   +-    - 
 w   1  1    3137  0.013292    1( a  )   1( a  )   +-   +    +-    -   +- 
 w   1  1    3143  0.019383    1( a  )   4( a  )   +-   +    +-    -   +- 
 w   1  1    3146  0.017853    4( a  )   4( a  )   +-   +    +-    -   +- 
 w   1  1    3147  0.016048    1( a  )   5( a  )   +-   +    +-    -   +- 
 w   1  1    3169  0.012257    5( a  )   8( a  )   +-   +    +-    -   +- 
 w   1  1    3196 -0.019315    5( a  )  11( a  )   +-   +    +-    -   +- 
 w   1  1    3328 -0.018821    1( a  )   2( a  )   +-   +     -   +-   +- 
 w   1  1    3330 -0.011881    1( a  )   3( a  )   +-   +     -   +-   +- 
 w   1  1    3334 -0.010766    2( a  )   4( a  )   +-   +     -   +-   +- 
 w   1  1    3335 -0.019775    3( a  )   4( a  )   +-   +     -   +-   +- 
 w   1  1    3348 -0.020604    1( a  )   7( a  )   +-   +     -   +-   +- 
 w   1  1    3351 -0.013995    4( a  )   7( a  )   +-   +     -   +-   +- 
 w   1  1    3356 -0.012149    2( a  )   8( a  )   +-   +     -   +-   +- 
 w   1  1    3357 -0.013843    3( a  )   8( a  )   +-   +     -   +-   +- 
 w   1  1    3380 -0.012086    9( a  )  10( a  )   +-   +     -   +-   +- 
 w   1  1    3383  0.011133    2( a  )  11( a  )   +-   +     -   +-   +- 
 w   1  1    3388  0.017236    7( a  )  11( a  )   +-   +     -   +-   +- 
 w   1  1    3482 -0.010328    3( a  )  18( a  )   +-   +     -   +-   +- 
 w   1  1    3517 -0.011267    1( a  )   1( a  )   +-        +-   +-   +- 
 w   1  1    3523 -0.013833    1( a  )   4( a  )   +-        +-   +-   +- 
 w   1  1    3526 -0.013213    4( a  )   4( a  )   +-        +-   +-   +- 

 ci coefficient statistics:
           rq > 0.1                1
      0.1> rq > 0.01             112
     0.01> rq > 0.001            592
    0.001> rq > 0.0001           428
   0.0001> rq > 0.00001          141
  0.00001> rq > 0.000001          74
 0.000001> rq                   3308
           all                  4656
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        15 2x:         0 4x:         0
All internal counts: zz :         0 yy:         0 xx:         0 ww:         0
One-external counts: yz :         0 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         0    task #   2:         0    task #   3:         0    task #   4:         0
task #   5:         0    task #   6:         0    task #   7:        -1    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:        14    task #  12:         0
task #  13:         0    task #  14:         0    task #
  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.975540345706    -74.188943073034

 number of reference csfs (nref) is     1.  root number (iroot) is  1.

 pople ci energy extrapolation is computed with 10 correlated electrons.

 eref      =    -76.049077211029   "relaxed" cnot**2         =   0.951678966099
 eci       =    -76.253628277133   deltae = eci - eref       =  -0.204551066104
 eci+dv1   =    -76.263512396132   dv1 = (1-cnot**2)*deltae  =  -0.009884119000
 eci+dv2   =    -76.264014257439   dv2 = dv1 / cnot**2       =  -0.010385980306
 eci+dv3   =    -76.264569808333   dv3 = dv1 / (2*cnot**2-1) =  -0.010941531200
 eci+pople =    -76.262194382415   ( 10e- scaled deltae )    =  -0.213117171385
NO coefficients and occupation numbers are written to nocoef_ci.1
 entering drivercid: firstcall,firstnonref= F T
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=      10  D0X=      30  D0W=      50
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================

          ci-one-electron density block   1

                mo   1         mo   2         mo   3         mo   4         mo   5         mo   6         mo   7         mo   8
   mo   1     1.99991386
   mo   2    -0.00021025     1.98511168
   mo   3     0.00000000    -0.00000003     1.96966952
   mo   4    -0.00015543     0.00413784    -0.00000010     1.97323368
   mo   5     0.00000000    -0.00000007    -0.00000001    -0.00000008     1.97619057
   mo   6     0.00012178    -0.00394438    -0.00002052    -0.02136150    -0.00000852     0.00699914
   mo   7     0.00000000     0.00000790    -0.01066388    -0.00002412    -0.00000497    -0.00000006     0.00752457
   mo   8    -0.00000001     0.00001593    -0.01289919    -0.00002234     0.00000238    -0.00000020     0.00719350     0.01062094
   mo   9     0.00005672     0.00307203    -0.00001233    -0.00030013     0.00000191     0.00710687    -0.00000025    -0.00000037
   mo  10    -0.00022101     0.01058993    -0.00001415     0.00337772     0.00000009     0.00261592    -0.00000021    -0.00000015
   mo  11     0.00000004    -0.00000177     0.00000296    -0.00000043    -0.01178414     0.00000023     0.00000010    -0.00000009
   mo  12    -0.00000002    -0.00000283    -0.00571665    -0.00000613    -0.00000251     0.00000005     0.00612917     0.00409174
   mo  13     0.00020430    -0.00111642    -0.00001285    -0.00840681    -0.00000313     0.00197126    -0.00000004    -0.00000019
   mo  14     0.00000000     0.00000215    -0.00000331    -0.00000116    -0.00000202    -0.00000003    -0.00000003    -0.00000002
   mo  15     0.00000000     0.00000112    -0.00000191    -0.00000501    -0.00351365     0.00000002    -0.00000001    -0.00000001
   mo  16    -0.00039025    -0.00017777    -0.00000464     0.00291068     0.00000176    -0.00196421    -0.00000012    -0.00000005
   mo  17    -0.00000001    -0.00000916     0.00104121     0.00002194     0.00000050    -0.00000003     0.00027582     0.00024948
   mo  18     0.00000004    -0.00000059     0.01163875    -0.00000055     0.00000089    -0.00000012    -0.00287333    -0.00440516
   mo  19    -0.00010535    -0.00449386    -0.00000012     0.00760190     0.00000042    -0.00312505    -0.00000015    -0.00000009
   mo  20     0.00000000     0.00000021    -0.00000050     0.00000140     0.00206313    -0.00000003    -0.00000001     0.00000000
   mo  21     0.00000000    -0.00000023     0.00000059    -0.00000061    -0.00000127     0.00000001    -0.00000001    -0.00000002
   mo  22     0.00001088     0.00012793     0.00000267    -0.00120961     0.00000030    -0.00006091     0.00000004     0.00000004
   mo  23     0.00004994     0.00174303    -0.00000045     0.00032500    -0.00000050     0.00063925     0.00000000    -0.00000003
   mo  24    -0.00000001    -0.00000071    -0.00029232     0.00000192     0.00000009     0.00000000    -0.00083323     0.00034401

                mo   9         mo  10         mo  11         mo  12         mo  13         mo  14         mo  15         mo  16
   mo   9     0.01012110
   mo  10     0.00335772     0.01011967
   mo  11    -0.00000006    -0.00000002     0.01452970
   mo  12     0.00000003     0.00000005     0.00000002     0.00653980
   mo  13     0.00053208    -0.00065426     0.00000007    -0.00000002     0.00373917
   mo  14    -0.00000003     0.00000000     0.00000010    -0.00000002     0.00000000     0.00297248
   mo  15    -0.00000003    -0.00000006    -0.00138116    -0.00000001     0.00000003    -0.00000006     0.00224347
   mo  16     0.00002251     0.00265158    -0.00000003    -0.00000007    -0.00125716     0.00000002    -0.00000002     0.00439413
   mo  17     0.00000020     0.00000028    -0.00000001     0.00035948    -0.00000003     0.00000000     0.00000000     0.00000007
   mo  18    -0.00000015    -0.00000018     0.00000001    -0.00184630     0.00000002     0.00000000     0.00000000    -0.00000003
   mo  19    -0.00438231    -0.00082925     0.00000000    -0.00000014    -0.00084281     0.00000000    -0.00000001    -0.00005784
   mo  20    -0.00000001     0.00000000     0.00004194     0.00000000    -0.00000001     0.00000000    -0.00182750     0.00000000
   mo  21     0.00000000    -0.00000001     0.00000003     0.00000000     0.00000001    -0.00202453     0.00000000    -0.00000001
   mo  22    -0.00056676     0.00029603    -0.00000001     0.00000002     0.00172459     0.00000000     0.00000000     0.00070401
   mo  23     0.00024472     0.00178232     0.00000000     0.00000004     0.00025013     0.00000000     0.00000000    -0.00068854
   mo  24     0.00000001     0.00000000     0.00000001    -0.00169151     0.00000001     0.00000000     0.00000000     0.00000003

                mo  17         mo  18         mo  19         mo  20         mo  21         mo  22         mo  23         mo  24
   mo  17     0.00051365
   mo  18    -0.00020483     0.00262428
   mo  19     0.00000002     0.00000006     0.00288630
   mo  20     0.00000000     0.00000000     0.00000001     0.00255506
   mo  21     0.00000000     0.00000001     0.00000000     0.00000001     0.00242704
   mo  22    -0.00000001     0.00000001    -0.00016081     0.00000000     0.00000000     0.00197672
   mo  23     0.00000004    -0.00000002     0.00061285     0.00000000     0.00000000    -0.00008831     0.00164243
   mo  24     0.00000338    -0.00057501     0.00000002     0.00000000     0.00000000    -0.00000001    -0.00000001     0.00145103


*****   symmetry block SYM1   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       1.99991386
       2      -0.00021025     1.98511168
       3       0.00000000    -0.00000003     1.96966952
       4      -0.00015543     0.00413784    -0.00000010     1.97323368
       5       0.00000000    -0.00000007    -0.00000001    -0.00000008
       6       0.00012178    -0.00394438    -0.00002052    -0.02136150
       7       0.00000000     0.00000790    -0.01066388    -0.00002412
       8      -0.00000001     0.00001593    -0.01289919    -0.00002234
       9       0.00005672     0.00307203    -0.00001233    -0.00030013
      10      -0.00022101     0.01058993    -0.00001415     0.00337772
      11       0.00000004    -0.00000177     0.00000296    -0.00000043
      12      -0.00000002    -0.00000283    -0.00571665    -0.00000613
      13       0.00020430    -0.00111642    -0.00001285    -0.00840681
      14       0.00000000     0.00000215    -0.00000331    -0.00000116
      15       0.00000000     0.00000112    -0.00000191    -0.00000501
      16      -0.00039025    -0.00017777    -0.00000464     0.00291068
      17      -0.00000001    -0.00000916     0.00104121     0.00002194
      18       0.00000004    -0.00000059     0.01163875    -0.00000055
      19      -0.00010535    -0.00449386    -0.00000012     0.00760190
      20       0.00000000     0.00000021    -0.00000050     0.00000140
      21       0.00000000    -0.00000023     0.00000059    -0.00000061
      22       0.00001088     0.00012793     0.00000267    -0.00120961
      23       0.00004994     0.00174303    -0.00000045     0.00032500
      24      -0.00000001    -0.00000071    -0.00029232     0.00000192

               Column   5     Column   6     Column   7     Column   8
       5       1.97619057
       6      -0.00000852     0.00699914
       7      -0.00000497    -0.00000006     0.00752457
       8       0.00000238    -0.00000020     0.00719350     0.01062094
       9       0.00000191     0.00710687    -0.00000025    -0.00000037
      10       0.00000009     0.00261592    -0.00000021    -0.00000015
      11      -0.01178414     0.00000023     0.00000010    -0.00000009
      12      -0.00000251     0.00000005     0.00612917     0.00409174
      13      -0.00000313     0.00197126    -0.00000004    -0.00000019
      14      -0.00000202    -0.00000003    -0.00000003    -0.00000002
      15      -0.00351365     0.00000002    -0.00000001    -0.00000001
      16       0.00000176    -0.00196421    -0.00000012    -0.00000005
      17       0.00000050    -0.00000003     0.00027582     0.00024948
      18       0.00000089    -0.00000012    -0.00287333    -0.00440516
      19       0.00000042    -0.00312505    -0.00000015    -0.00000009
      20       0.00206313    -0.00000003    -0.00000001     0.00000000
      21      -0.00000127     0.00000001    -0.00000001    -0.00000002
      22       0.00000030    -0.00006091     0.00000004     0.00000004
      23      -0.00000050     0.00063925     0.00000000    -0.00000003
      24       0.00000009     0.00000000    -0.00083323     0.00034401

               Column   9     Column  10     Column  11     Column  12
       9       0.01012110
      10       0.00335772     0.01011967
      11      -0.00000006    -0.00000002     0.01452970
      12       0.00000003     0.00000005     0.00000002     0.00653980
      13       0.00053208    -0.00065426     0.00000007    -0.00000002
      14      -0.00000003     0.00000000     0.00000010    -0.00000002
      15      -0.00000003    -0.00000006    -0.00138116    -0.00000001
      16       0.00002251     0.00265158    -0.00000003    -0.00000007
      17       0.00000020     0.00000028    -0.00000001     0.00035948
      18      -0.00000015    -0.00000018     0.00000001    -0.00184630
      19      -0.00438231    -0.00082925     0.00000000    -0.00000014
      20      -0.00000001     0.00000000     0.00004194     0.00000000
      21       0.00000000    -0.00000001     0.00000003     0.00000000
      22      -0.00056676     0.00029603    -0.00000001     0.00000002
      23       0.00024472     0.00178232     0.00000000     0.00000004
      24       0.00000001     0.00000000     0.00000001    -0.00169151

               Column  13     Column  14     Column  15     Column  16
      13       0.00373917
      14       0.00000000     0.00297248
      15       0.00000003    -0.00000006     0.00224347
      16      -0.00125716     0.00000002    -0.00000002     0.00439413
      17      -0.00000003     0.00000000     0.00000000     0.00000007
      18       0.00000002     0.00000000     0.00000000    -0.00000003
      19      -0.00084281     0.00000000    -0.00000001    -0.00005784
      20      -0.00000001     0.00000000    -0.00182750     0.00000000
      21       0.00000001    -0.00202453     0.00000000    -0.00000001
      22       0.00172459     0.00000000     0.00000000     0.00070401
      23       0.00025013     0.00000000     0.00000000    -0.00068854
      24       0.00000001     0.00000000     0.00000000     0.00000003

               Column  17     Column  18     Column  19     Column  20
      17       0.00051365
      18      -0.00020483     0.00262428
      19       0.00000002     0.00000006     0.00288630
      20       0.00000000     0.00000000     0.00000001     0.00255506
      21       0.00000000     0.00000001     0.00000000     0.00000001
      22      -0.00000001     0.00000001    -0.00016081     0.00000000
      23       0.00000004    -0.00000002     0.00061285     0.00000000
      24       0.00000338    -0.00057501     0.00000002     0.00000000

               Column  21     Column  22     Column  23     Column  24
      21       0.00242704
      22       0.00000000     0.00197672
      23       0.00000000    -0.00008831     0.00164243
      24       0.00000000    -0.00000001    -0.00000001     0.00145103

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7        eno   8
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    8   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno   9        eno  10        eno  11        eno  12        eno  13        eno  14        eno  15        eno  16
 emo    9   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   10   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   11   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   12   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   13   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   14   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   15   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   16   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno  17        eno  18        eno  19        eno  20        eno  21        eno  22        eno  23        eno  24
 emo   17   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   18   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   19   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   20   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   21   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   22   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   23   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   24   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   1.9999189      1.9865361      1.9762697      1.9721946      1.9698999
   0.0215483      0.0197793      0.0146234      0.0103342      0.0054300
   0.0051654      0.0047426      0.0042244      0.0041337      0.0010152
   0.0010110      0.0006569      0.0005692      0.0004927      0.0004919
   0.0004480      0.0004307      0.0000505      0.0000331

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7         no   8
  mo    1   0.999821E+00   0.186783E-01  -0.606739E-07   0.312340E-02  -0.706168E-06   0.300778E-07  -0.331013E-04  -0.239051E-07
  mo    2  -0.168220E-01   0.951727E+00  -0.987303E-06  -0.306417E+00   0.716091E-04  -0.563657E-05  -0.285559E-02   0.107822E-05
  mo    3   0.113551E-06  -0.480027E-05   0.385274E-06   0.218441E-03   0.999941E+00   0.102233E-01   0.179243E-04  -0.165296E-05
  mo    4  -0.869327E-02   0.306269E+00   0.127021E-04   0.951823E+00  -0.206741E-03   0.122648E-04   0.674539E-02   0.181599E-08
  mo    5   0.156023E-06  -0.298341E-05   0.999980E+00  -0.124490E-04  -0.387501E-06   0.137293E-05   0.193981E-05   0.573540E-02
  mo    6   0.187733E-03  -0.518806E-02  -0.446114E-05  -0.974585E-02  -0.838070E-05  -0.248443E-03   0.534961E+00   0.546083E-05
  mo    7   0.363927E-07   0.102519E-06  -0.252877E-05  -0.141663E-04  -0.547589E-02   0.569868E+00   0.247744E-03   0.397314E-05
  mo    8  -0.437645E-07   0.423897E-05   0.119318E-05  -0.148237E-04  -0.662283E-02   0.636475E+00   0.265901E-03  -0.806110E-05
  mo    9   0.429077E-05   0.142606E-02   0.951072E-06  -0.671303E-03  -0.618687E-05  -0.296118E-03   0.672772E+00  -0.143328E-04
  mo   10  -0.215378E-03   0.561868E-02   0.573209E-07  -0.285513E-04  -0.721319E-05  -0.179685E-03   0.406764E+00  -0.139557E-04
  mo   11   0.367547E-07  -0.903203E-06  -0.600559E-02   0.146440E-06   0.151922E-05   0.273330E-05   0.127199E-04   0.993031E+00
  mo   12   0.425154E-07  -0.228804E-05  -0.128024E-05  -0.323957E-05  -0.294775E-02   0.440993E+00   0.217563E-03   0.940549E-06
  mo   13   0.148763E-03  -0.183894E-02  -0.164768E-05  -0.390419E-02  -0.569561E-05  -0.564589E-04   0.840258E-01   0.556369E-05
  mo   14  -0.133620E-07   0.853384E-06  -0.102206E-05  -0.893222E-06  -0.168049E-05  -0.378692E-05  -0.302419E-05   0.769594E-05
  mo   15   0.113025E-07  -0.229661E-06  -0.177667E-02  -0.257370E-05  -0.971319E-06  -0.215510E-05  -0.525251E-05  -0.115654E+00
  mo   16  -0.207270E-03   0.374225E-03   0.916194E-06   0.144683E-02  -0.267004E-05  -0.256808E-05  -0.462616E-02  -0.741305E-05
  mo   17  -0.257071E-07  -0.100809E-05   0.250509E-06   0.121237E-04   0.525901E-03   0.256904E-01   0.343096E-04  -0.654697E-06
  mo   18   0.291984E-07  -0.405334E-06   0.457174E-06   0.117120E-05   0.594141E-02  -0.270417E+00  -0.135833E-03   0.183731E-05
  mo   19  -0.482501E-04  -0.979643E-03   0.268593E-06   0.439189E-02  -0.990157E-06   0.115575E-03  -0.291625E+00   0.430997E-05
  mo   20  -0.656826E-08   0.315311E-06   0.104680E-02   0.632034E-06  -0.251651E-06  -0.449104E-07  -0.236171E-06   0.219448E-01
  mo   21   0.589340E-08  -0.206712E-06  -0.643234E-06  -0.258140E-06   0.300909E-06  -0.335939E-06   0.173469E-06   0.269176E-06
  mo   22   0.965309E-05  -0.126056E-03   0.140672E-06  -0.607013E-03   0.148252E-05   0.393952E-05  -0.666655E-02  -0.680469E-07
  mo   23   0.884194E-05   0.889268E-03  -0.253333E-06  -0.116854E-03  -0.207562E-06  -0.251408E-04   0.592650E-01  -0.147801E-05
  mo   24  -0.783884E-08  -0.401055E-07   0.452542E-07   0.100964E-05  -0.146534E-03  -0.422557E-01  -0.215374E-04  -0.149623E-06

                no   9         no  10         no  11         no  12         no  13         no  14         no  15         no  16
  mo    1   0.228180E-03  -0.329513E-07  -0.977989E-04  -0.121261E-08   0.438854E-04   0.211716E-09  -0.655907E-07   0.195404E-04
  mo    2  -0.420359E-02   0.434138E-05  -0.336263E-03  -0.890340E-06   0.360766E-03   0.236109E-06  -0.765274E-05   0.106728E-02
  mo    3   0.120948E-05  -0.228980E-02   0.561047E-05   0.146025E-05   0.148238E-05  -0.323902E-06  -0.160156E-02  -0.337829E-05
  mo    4  -0.650872E-02  -0.225657E-06   0.399408E-02   0.362205E-06   0.143951E-02  -0.218274E-05   0.138279E-04  -0.275414E-02
  mo    5  -0.175636E-05   0.187559E-05   0.238604E-05   0.224095E-06  -0.399992E-06  -0.250449E-02  -0.930025E-06  -0.103981E-05
  mo    6  -0.257310E+00   0.403811E-04   0.189855E+00   0.289314E-05  -0.124610E+00  -0.349072E-05   0.294905E-03  -0.133827E+00
  mo    7  -0.121747E-04   0.219133E+00  -0.641814E-04   0.337854E-05   0.545231E-05  -0.626677E-06   0.410212E-01   0.106021E-03
  mo    8   0.529691E-05  -0.545812E+00   0.176449E-03  -0.443546E-05   0.581749E-05  -0.101156E-05   0.401202E+00   0.891968E-03
  mo    9  -0.167468E+00  -0.127660E-03  -0.365601E+00  -0.299337E-05   0.461416E-01   0.618018E-06  -0.885926E-03   0.401288E+00
  mo   10   0.767148E+00   0.100863E-03   0.320159E+00   0.193350E-05  -0.143637E+00  -0.668445E-05   0.657251E-03  -0.285466E+00
  mo   11   0.134089E-04  -0.628176E-05  -0.822314E-05  -0.917704E-05   0.355743E-05  -0.927176E-01   0.189625E-05   0.336894E-05
  mo   12  -0.529768E-05   0.639018E+00  -0.189051E-03   0.906345E-05  -0.346038E-05   0.372759E-06  -0.292181E+00  -0.655423E-03
  mo   13  -0.255860E+00   0.192405E-03   0.603807E+00   0.190101E-04   0.448947E+00   0.922442E-05  -0.155964E-03   0.568615E-01
  mo   14   0.334776E-05  -0.890119E-05  -0.137642E-04   0.752828E+00  -0.886823E-05  -0.184481E-04  -0.651019E-05  -0.593632E-08
  mo   15  -0.656861E-05   0.161692E-05   0.241572E-06  -0.266855E-04   0.128201E-04  -0.653714E+00  -0.107487E-05  -0.639795E-06
  mo   16   0.467940E+00  -0.103083E-03  -0.362388E+00  -0.869137E-06   0.557229E+00   0.770482E-05  -0.660852E-03   0.283187E+00
  mo   17   0.129360E-04   0.187121E-01   0.117739E-04   0.230004E-05   0.197899E-05   0.421859E-06  -0.246648E+00  -0.581200E-03
  mo   18  -0.690543E-05   0.284308E+00  -0.100962E-03   0.287311E-05  -0.305138E-05   0.131204E-05   0.617713E+00   0.137281E-02
  mo   19   0.149883E+00   0.595111E-04   0.186839E+00  -0.146420E-05  -0.277742E+00  -0.649719E-05  -0.100655E-02   0.427829E+00
  mo   20   0.200620E-05  -0.156773E-05   0.790775E-07   0.186278E-04  -0.140960E-04   0.751036E+00  -0.206530E-05   0.150310E-05
  mo   21  -0.191510E-05   0.815691E-05   0.117237E-04  -0.658217E+00   0.987413E-05   0.279499E-04  -0.743512E-05   0.142004E-05
  mo   22   0.239794E-01   0.994995E-04   0.318115E+00   0.116504E-04   0.523289E+00   0.100024E-04  -0.243791E-03   0.135897E+00
  mo   23   0.984786E-01   0.968816E-04   0.309451E+00   0.191299E-05  -0.314135E+00  -0.764588E-05  -0.147086E-02   0.674255E+00
  mo   24   0.261529E-05  -0.405635E+00   0.131241E-03  -0.555888E-05   0.889662E-05  -0.153264E-05  -0.556392E+00  -0.123849E-02

                no  17         no  18         no  19         no  20         no  21         no  22         no  23         no  24
  mo    1  -0.175859E-08  -0.279239E-07   0.432857E-04   0.503821E-07   0.435835E-07  -0.355767E-04   0.266532E-04   0.492574E-08
  mo    2  -0.669272E-06  -0.371486E-05   0.266233E-02   0.272298E-05   0.559675E-05  -0.196434E-02   0.176741E-02  -0.813836E-07
  mo    3   0.902348E-06  -0.584715E-03   0.208674E-05   0.833065E-06  -0.167021E-02  -0.799627E-05   0.119201E-05   0.163689E-02
  mo    4   0.605697E-06   0.729167E-05   0.180911E-02   0.362353E-05  -0.542841E-05  -0.186232E-02   0.606920E-02   0.124971E-05
  mo    5   0.994273E-06   0.133171E-05  -0.107528E-06   0.107339E-02  -0.129249E-06  -0.130284E-05   0.236829E-05   0.119770E-05
  mo    6  -0.613959E-05  -0.175528E-03   0.470317E+00   0.564716E-03   0.323019E-03  -0.209330E+00   0.559451E+00   0.567950E-04
  mo    7  -0.182664E-04   0.563623E+00   0.122696E-03  -0.581487E-05   0.185258E+00   0.485725E-04  -0.470324E-04   0.522999E+00
  mo    8   0.133383E-04  -0.186327E+00  -0.708249E-04   0.153079E-05   0.519279E-01   0.285341E-04   0.268456E-04  -0.313953E+00
  mo    9   0.163776E-05   0.868396E-05  -0.110108E-01  -0.132782E-04  -0.133088E-04   0.265060E-01  -0.470829E+00  -0.506011E-04
  mo   10   0.192368E-05   0.472124E-04  -0.542571E-01  -0.619093E-04  -0.451670E-04   0.959404E-02  -0.195796E+00  -0.149221E-04
  mo   11  -0.148623E-04  -0.409630E-05  -0.914172E-04   0.725096E-01   0.118875E-05   0.317028E-05  -0.708676E-05  -0.335089E-05
  mo   12   0.353034E-05  -0.145416E+00   0.400969E-05   0.350261E-05  -0.124560E+00  -0.597230E-04   0.535595E-04  -0.524539E+00
  mo   13   0.651106E-05   0.309894E-04  -0.331859E+00  -0.404250E-03   0.287214E-03  -0.462859E+00  -0.183623E+00  -0.257082E-04
  mo   14   0.658217E+00   0.249483E-04   0.113490E-04   0.101138E-03  -0.468960E-05   0.232724E-05   0.168897E-05   0.124800E-05
  mo   15  -0.101346E-03   0.438614E-05  -0.896675E-03   0.747849E+00   0.151239E-04  -0.827239E-05  -0.256787E-05   0.272192E-06
  mo   16  -0.182117E-05  -0.719292E-04   0.303755E-01   0.345491E-04   0.299672E-03  -0.331483E+00   0.384943E+00   0.328743E-04
  mo   17   0.158316E-04  -0.377344E+00  -0.416103E-03  -0.163296E-04   0.888872E+00   0.852866E-03   0.525270E-06   0.753141E-01
  mo   18  -0.604223E-05   0.385708E+00   0.117876E-04  -0.869453E-05   0.372604E+00   0.211947E-03   0.405397E-04  -0.420515E+00
  mo   19  -0.815141E-05  -0.260788E-03   0.581302E+00   0.690794E-03   0.552357E-03  -0.401193E+00  -0.313866E+00  -0.330281E-04
  mo   20  -0.101893E-03   0.432698E-05  -0.790830E-03   0.659895E+00   0.127250E-04  -0.805878E-05  -0.145879E-05  -0.477224E-07
  mo   21   0.752828E+00   0.261546E-04   0.107523E-04   0.103018E-03  -0.289942E-05   0.164158E-05   0.657237E-06   0.920595E-06
  mo   22  -0.951062E-05  -0.386561E-04   0.439674E+00   0.533927E-03  -0.409433E-03   0.638233E+00  -0.723160E-01   0.657804E-06
  mo   23   0.431863E-05   0.150430E-03  -0.365303E+00  -0.433699E-03  -0.326432E-03   0.250920E+00   0.375924E+00   0.306602E-04
  mo   24  -0.250685E-04   0.579056E+00   0.134532E-03  -0.724946E-05   0.136152E+00   0.772018E-04   0.475531E-04  -0.412623E+00
  eigenvectors of nos in ao-basis, column    1
  0.1000604E+01 -0.1372967E-01 -0.1673739E-02  0.6889902E-07  0.1943647E-06 -0.7381892E-02  0.8466463E-07 -0.5530717E-07
  0.1486884E-03  0.8041947E-08 -0.1169121E-07  0.4228022E-04  0.1611738E-07  0.9066606E-04 -0.3265779E-02  0.2144469E-02
 -0.1014526E-02  0.4242654E-08 -0.9343309E-03 -0.3265882E-02  0.2144614E-02  0.1014546E-02  0.1808892E-07 -0.9343850E-03
  eigenvectors of nos in ao-basis, column    2
  0.9517571E-02  0.9130288E+00 -0.7095724E-01 -0.3255820E-05 -0.3996750E-05  0.1252950E+00 -0.4930419E-05  0.1342093E-05
  0.7752960E-01 -0.3316677E-06  0.4249838E-06 -0.1206166E-02 -0.2114255E-06  0.1113069E-02  0.2165457E+00 -0.1058434E+00
  0.2696431E-01  0.3589343E-06  0.2156673E-01  0.2165562E+00 -0.1058509E+00 -0.2696644E-01 -0.6547165E-06  0.2156504E-01
  eigenvectors of nos in ao-basis, column    3
 -0.8294045E-06 -0.2194153E-05  0.3345238E-05  0.2804560E-05  0.9104830E+00  0.1090941E-04 -0.2471935E-05  0.8288881E-01
  0.1817456E-05 -0.5579667E-06 -0.1689479E-01 -0.9811492E-07 -0.1404591E-06 -0.2642029E-06 -0.7833192E-05  0.1057716E-04
  0.7446079E-06  0.3056379E-01 -0.3441801E-06 -0.2382328E-05  0.8049662E-06 -0.2827265E-06  0.3056569E-01 -0.5250027E-06
  eigenvectors of nos in ao-basis, column    4
  0.1093787E-02 -0.1763510E-02  0.2169707E+00  0.1737157E-03 -0.1105546E-04  0.8020749E+00 -0.3155429E-04  0.1834557E-06
 -0.2022021E-01 -0.1616474E-06  0.1299465E-05 -0.4710333E-02 -0.5972129E-05 -0.2409590E-02 -0.4298924E+00  0.1837406E+00
 -0.3806968E-01 -0.2830514E-05  0.2189088E-02 -0.4296794E+00  0.1836562E+00  0.3807039E-01 -0.1407862E-05  0.2223202E-02
  eigenvectors of nos in ao-basis, column    5
  0.3357731E-06  0.2500332E-05 -0.5437052E-04  0.7310639E+00  0.1848517E-05 -0.1615732E-03 -0.1182892E+00 -0.1929171E-05
  0.6192329E-06  0.5393003E-06 -0.1074662E-06  0.8845766E-06 -0.2540663E-01  0.8820419E-06 -0.5519228E+00  0.1930716E+00
 -0.2034913E-01 -0.1893640E-05 -0.3126597E-01  0.5521343E+00 -0.1931556E+00 -0.2036830E-01  0.1833530E-06  0.3125324E-01
  eigenvectors of nos in ao-basis, column    6
 -0.4674923E-04 -0.3665986E-03  0.6714589E-04 -0.1306396E+01  0.5258059E-05  0.5341878E-03  0.5802048E+00 -0.3210930E-05
 -0.3081549E-03  0.1279734E-06  0.2604002E-06  0.1911154E-05 -0.6500454E-01  0.1314905E-04 -0.1005168E+01  0.3581742E+00
 -0.3972507E-01 -0.4348275E-05 -0.1795552E-01  0.1006022E+01 -0.3585267E+00 -0.3976346E-01  0.1105082E-05  0.1792675E-01
  eigenvectors of nos in ao-basis, column    7
  0.1149395E+00  0.8554192E+00 -0.1688706E+00 -0.6062202E-03  0.2006137E-04 -0.1210115E+01  0.2961049E-03 -0.1809641E-04
  0.7549062E+00  0.5758732E-06  0.5013659E-06 -0.3929324E-02 -0.2783055E-04 -0.3145299E-01 -0.9753702E+00  0.4154267E+00
 -0.4957920E-01 -0.6024097E-05 -0.9675036E-02 -0.9744863E+00  0.4151015E+00  0.4954986E-01 -0.2005821E-05 -0.9609129E-02
  eigenvectors of nos in ao-basis, column    8
  0.4146891E-05  0.2026917E-04 -0.2273036E-04  0.1513033E-05  0.1412897E+01  0.2804219E-04  0.2593720E-05 -0.1611474E+01
 -0.2771026E-04 -0.7034964E-06  0.3698023E-01  0.1729066E-06  0.1487449E-05  0.7947625E-06  0.3550925E-04 -0.3622647E-04
  0.2571433E-07 -0.7462224E-01  0.1974471E-05  0.4424381E-05 -0.1447449E-05  0.1056036E-05 -0.7463300E-01  0.1387706E-05
  eigenvectors of nos in ao-basis, column    9
 -0.4409089E+00 -0.2257307E+01  0.2143829E+01  0.2846173E-05  0.1774068E-04 -0.7476750E+00 -0.1485117E-04 -0.1946503E-04
  0.1099496E+01 -0.2485103E-05  0.3121219E-05 -0.1275224E-01 -0.5533287E-05  0.6082606E-01  0.3723196E+00 -0.2057721E+00
 -0.1276885E-01 -0.2663170E-05  0.7493667E-01  0.3723592E+00 -0.2058200E+00  0.1277260E-01 -0.5797987E-05  0.7495971E-01
  eigenvectors of nos in ao-basis, column   10
  0.8300510E-04  0.3651820E-03 -0.5179860E-03 -0.4939093E+00 -0.7250620E-05 -0.4750241E-05  0.1038483E+01  0.1026636E-04
  0.5965666E-04  0.9893990E-05 -0.1904653E-05 -0.3120599E-04  0.6164186E+00  0.8316148E-04  0.6580988E+00 -0.4998578E+00
  0.1116058E+00 -0.2365541E-05 -0.2164276E-01 -0.6576844E+00  0.4996253E+00  0.1116633E+00  0.3633276E-05  0.2178616E-01
  eigenvectors of nos in ao-basis, column   11
  0.2870819E+00  0.1281302E+01 -0.1766069E+01  0.1309648E-03 -0.9414163E-05 -0.4251692E-01 -0.3124355E-03  0.1386158E-04
  0.2218561E+00  0.1434545E-04  0.3642268E-07 -0.9826179E-01 -0.2021170E-03  0.2650396E+00  0.6421919E+00 -0.3801086E+00
 -0.6748425E-01 -0.4728470E-05  0.2205125E+00  0.6426336E+00 -0.3804399E+00  0.6742637E-01  0.5225767E-05  0.2205271E+00
  eigenvectors of nos in ao-basis, column   12
  0.5498678E-05  0.2624176E-04 -0.2824763E-04 -0.8603342E-05 -0.1080986E-04  0.4833399E-06  0.1216642E-04  0.2534485E-04
 -0.6646736E-05 -0.8028444E+00  0.2378063E-04 -0.4255121E-05  0.7844397E-05  0.1968424E-05 -0.7898872E-06 -0.1973930E-06
 -0.2244674E-05  0.2657324E+00  0.3675921E-05 -0.3824286E-05  0.3849429E-05  0.6359156E-05 -0.2657585E+00  0.7277408E-05
  eigenvectors of nos in ao-basis, column   13
 -0.1283393E+00 -0.5433543E+00  0.8910879E+00  0.2938343E-05  0.3687311E-05  0.9780885E-01 -0.5606514E-05 -0.9100206E-05
 -0.5118817E+00  0.1173108E-04 -0.1679774E-04 -0.2106718E+00 -0.1274555E-04 -0.2135571E+00 -0.4697858E+00  0.2702920E+00
 -0.1642817E+00  0.1836430E-05  0.9234174E-01 -0.4697886E+00  0.2703049E+00  0.1642887E+00  0.6481083E-05  0.9233894E-01
  eigenvectors of nos in ao-basis, column   14
  0.3933005E-06  0.1739985E-05 -0.7662981E-06  0.5552229E-06 -0.8382919E-01  0.5365025E-05 -0.7145740E-06  0.3025066E+00
 -0.1326691E-04  0.3234839E-04  0.8903064E+00 -0.3988672E-05  0.2309029E-05 -0.5016156E-05 -0.5419538E-05  0.5749610E-05
 -0.2176080E-05 -0.1991805E+00  0.8940517E-06 -0.4542652E-05  0.4243665E-05  0.2703994E-05 -0.1991765E+00  0.1634633E-05
  eigenvectors of nos in ao-basis, column   15
  0.8300998E-03  0.3169920E-02 -0.6139838E-02  0.4997230E+00  0.1875269E-05 -0.3618982E-03 -0.1296222E+01 -0.1956430E-05
  0.2689456E-02 -0.7133257E-05 -0.1979197E-05  0.4962632E-04  0.8078061E+00 -0.9058878E-03 -0.1402534E+01  0.7245806E+00
 -0.7298976E-01 -0.8988662E-05  0.2547914E+00  0.1412549E+01 -0.7302099E+00 -0.7329657E-01  0.5613453E-05 -0.2542293E+00
  eigenvectors of nos in ao-basis, column   16
 -0.3574519E+00 -0.1355388E+01  0.2707740E+01  0.1108984E-02  0.3846965E-05  0.1232376E+00 -0.2872818E-02 -0.5857501E-05
 -0.1154248E+01  0.1523443E-05  0.1678350E-05 -0.2985782E-01  0.1799459E-02  0.4144483E+00 -0.2256796E+01  0.1269303E+01
 -0.7491087E-01  0.6407632E-06 -0.1590829E+00 -0.2250567E+01  0.1266132E+01  0.7454715E-01 -0.4334923E-06 -0.1602551E+00
  eigenvectors of nos in ao-basis, column   17
  0.4263465E-05  0.2033382E-04 -0.6966819E-05 -0.2637039E-04 -0.1399476E-04 -0.1042037E-04 -0.2043566E-04  0.1183211E-03
 -0.4135957E-05  0.7223875E+00 -0.9030543E-04  0.2802140E-05  0.2984949E-04  0.9413602E-06 -0.1041626E-03  0.8575135E-04
 -0.2276587E-04  0.7384764E+00 -0.2288064E-04  0.5837586E-04 -0.5109123E-04  0.3129692E-05 -0.7387157E+00  0.2423768E-04
  eigenvectors of nos in ao-basis, column   18
  0.1266942E-03  0.5766637E-03 -0.2142663E-03  0.8943007E+00 -0.4812380E-05 -0.3348816E-03  0.1278781E+00  0.2992786E-05
  0.1198564E-03  0.2482882E-04  0.3817297E-05  0.1246159E-04 -0.6609577E+00  0.5854239E-04  0.1763898E+01 -0.1624394E+01
  0.3864325E+00  0.3224893E-04  0.6953036E+00 -0.1764766E+01  0.1625071E+01  0.3869856E+00 -0.2192687E-04 -0.6956505E+00
  eigenvectors of nos in ao-basis, column   19
 -0.2142037E+00 -0.1046009E+01 -0.1592211E+00  0.8030516E-04 -0.7470752E-04  0.6983540E+00  0.4646032E-03  0.9429356E-03
  0.3458034E+00  0.1006638E-04 -0.6832607E-03 -0.1273496E+00 -0.1540261E-03 -0.1171543E+00  0.1592494E+01 -0.1110135E+01
  0.9029272E+00 -0.1001578E-02  0.1508805E+00  0.1591177E+01 -0.1109192E+01 -0.9031586E+00 -0.1025349E-02  0.1501862E+00
  eigenvectors of nos in ao-basis, column   20
 -0.2548925E-03 -0.1245233E-02 -0.1890997E-03 -0.1778179E-04  0.5794987E-01  0.8257999E-03  0.2290093E-04 -0.7801711E+00
  0.4295375E-03  0.9742553E-04  0.5701923E+00 -0.1545681E-03  0.8358020E-05 -0.1380793E-03  0.1898581E-02 -0.1319015E-02
  0.1063403E-02  0.8455046E+00  0.1737954E-03  0.1914613E-02 -0.1345016E-02 -0.1098216E-02  0.8452873E+00  0.1662318E-03
  eigenvectors of nos in ao-basis, column   21
 -0.3384856E-03 -0.1460906E-02  0.1043670E-02  0.6160526E+00  0.6374076E-06  0.6777318E-03 -0.1253426E+01 -0.1512027E-04
 -0.8187880E-03 -0.2504425E-05  0.1088660E-04  0.1118322E-03 -0.1487442E+00 -0.1701611E-03 -0.3096242E+00 -0.1087390E+00
  0.7466709E+00  0.1251501E-04 -0.3539581E+00  0.3102004E+00  0.1080352E+00  0.7461728E+00  0.2116122E-04  0.3554432E+00
  eigenvectors of nos in ao-basis, column   22
  0.3014960E+00  0.1257388E+01 -0.1221371E+01  0.3629261E-03  0.3766549E-05 -0.4977867E+00 -0.9815249E-03  0.2087255E-05
  0.1032332E+01  0.1460217E-05 -0.7095008E-05 -0.1762873E+00 -0.9629534E-04  0.1600416E+00  0.2190422E+00  0.2740183E-01
  0.6314641E-01 -0.7456544E-05 -0.8052541E+00  0.2198367E+00  0.2718399E-01 -0.6195312E-01 -0.1190179E-04 -0.8043773E+00
  eigenvectors of nos in ao-basis, column   23
 -0.3247146E+00 -0.1122060E+01  0.3944398E+01  0.3827179E-04 -0.7651909E-05 -0.3606409E+00  0.1584141E-03  0.1411326E-04
 -0.9525896E+00  0.4870072E-06 -0.1127355E-05  0.2191399E-01 -0.6394084E-04  0.1876252E+00 -0.1193651E+01 -0.4599689E+00
 -0.7598605E+00 -0.1127734E-05 -0.5062871E+00 -0.1193864E+01 -0.4601795E+00  0.7600177E+00 -0.3946705E-05 -0.5064092E+00
  eigenvectors of nos in ao-basis, column   24
 -0.2825559E-04 -0.9875327E-04  0.3524569E-03 -0.3602182E+00 -0.3641317E-05 -0.3627025E-04 -0.1588215E+01  0.5587201E-05
 -0.6797763E-04  0.8262489E-06 -0.1006549E-06 -0.8810983E-07  0.5565216E+00  0.1555332E-04 -0.9402437E+00 -0.1211323E+01
 -0.7209733E+00  0.1417138E-05 -0.6507556E+00  0.9400832E+00  0.1211192E+01 -0.7208408E+00 -0.9973667E-06  0.6506479E+00


 total number of electrons =   10.0000000000



          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!


                        A   partial gross atomic populations
   ao class       1A         2A         3A         4A         5A         6A  
    O1_ s       2.000675   1.663679   0.000000  -0.044234   0.000000   0.000000
    O1_ p       0.000085   0.036579   1.931434   1.496769   1.156974   0.011284
    O1_ d       0.000000   0.000191   0.000968   0.001818   0.008151   0.000441
    H1_ s      -0.000124   0.115563   0.000000   0.238185   0.387743   0.005196
    H1_ p      -0.000296   0.027480   0.021933   0.020851   0.014497  -0.000289
    H2_ s      -0.000124   0.115565   0.000000   0.237945   0.388042   0.005205
    H2_ p      -0.000296   0.027480   0.021934   0.020860   0.014493  -0.000289

   ao class       7A         8A         9A        10A        11A        12A  
    O1_ s       0.001572   0.000000   0.005224   0.000000   0.001121   0.000000
    O1_ p       0.009168   0.013995   0.004038   0.001915   0.000062   0.000000
    O1_ d       0.000275   0.000036   0.000292   0.002665   0.002726   0.003583
    H1_ s       0.004693   0.000000   0.000241   0.000249   0.000116   0.000000
    H1_ p      -0.000306   0.000296   0.000149   0.000176   0.000512   0.000580
    H2_ s       0.004684   0.000000   0.000241   0.000249   0.000116   0.000000
    H2_ p      -0.000306   0.000296   0.000149   0.000176   0.000512   0.000580

   ao class      13A        14A        15A        16A        17A        18A  
    O1_ s       0.000203   0.000000   0.000000   0.000205   0.000000   0.000000
    O1_ p       0.000244   0.000085   0.000264   0.000168   0.000000   0.000004
    O1_ d       0.003387   0.003567   0.000385   0.000378   0.000161   0.000053
    H1_ s       0.000012   0.000000   0.000080   0.000156   0.000000   0.000102
    H1_ p       0.000183   0.000241   0.000103  -0.000025   0.000248   0.000154
    H2_ s       0.000012   0.000000   0.000081   0.000155   0.000000   0.000102
    H2_ p       0.000183   0.000241   0.000102  -0.000025   0.000248   0.000154

   ao class      19A        20A        21A        22A        23A        24A  
    O1_ s       0.000008   0.000000   0.000000   0.000011   0.000004   0.000000
    O1_ p      -0.000006   0.000022   0.000057   0.000037   0.000003   0.000005
    O1_ d       0.000037   0.000066   0.000000   0.000073   0.000000   0.000000
    H1_ s       0.000032   0.000000   0.000007   0.000007   0.000015   0.000008
    H1_ p       0.000196   0.000202   0.000189   0.000148   0.000006   0.000006
    H2_ s       0.000031   0.000000   0.000007   0.000007   0.000015   0.000008
    H2_ p       0.000196   0.000202   0.000189   0.000147   0.000006   0.000006


                        gross atomic populations
     ao           O1_        H1_        H2_
      s         3.628469   0.752281   0.752344
      p         4.663183   0.087231   0.087238
      d         0.029254   0.000000   0.000000
    total       8.320906   0.839512   0.839582


 Total number of electrons:   10.00000000

