1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      09/20/00    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 using llenci= -1
================================================================================
four external integ    0.25 MB location: local disk    
three external inte    0.25 MB location: local disk    
four external integ    0.25 MB location: local disk    
three external inte    0.25 MB location: local disk    
diagonal integrals     0.12 MB location: local disk    
off-diagonal integr    0.28 MB location: local disk    
computed file size in DP units
fil3w:       32767
fil3x:       32767
fil4w:       32767
fil4x:       32767
ofdgint:       16380
diagint:       36855
computed file size in DP units
fil3w:      262136
fil3x:      262136
fil4w:      262136
fil4x:      262136
ofdgint:      131040
diagint:      294840
 nsubmx= 2 lenci= 14422
global arrays:        72110   (              0.55 MB)
vdisk:                    0   (              0.00 MB)
drt:                 684943   (              2.61 MB)
================================================================================
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core           99999999 DP per process
 CIUDG version 5.9.7 ( 5-Oct-2004)
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 0,
  GSET = 0,
   DAVCOR =10,
  NCOREL = 12
  NROOT = 1
  IVMODE = 3
  NBKITR = 0
  frcsub = 2
  NVBKMN = 1
  NVBKMX = 2
  RTOLBK = 1e-3,
  NITER = 30
  NVCIMN = 1
  NVCIMX = 2
  RTOLCI = 1e-3,
  IDEN  = 1
  CSFPRN = 10,
  cosmocalc = 1
  &end
 ------------------------------------------------------------------------

 ** list of control variables **
 nrfitr =   30      nvrfmx =    2      nvrfmn =    1
 lvlprt =    0      nroot  =    1      noldv  =    0      noldhv =    0
 nunitv =    1      ntype  =    0      nbkitr =    0      niter  =   30
 ivmode =    3      vout   =    0      istrt  =    0      iortls =    0
 nvbkmx =    2      ibktv  =   -1      ibkthv =   -1      frcsub =    2
 nvcimx =    2      icitv  =   -1      icithv =   -1      maxseg =    4
 iden   =    1      itran  =    0      froot  =    0      rtmode =    0
 ftcalc =    1      lrtshift=1.0000    ncouple=    1      skipso  =   F
 ncorel =   12      csfprn  =  10      ctol   = 1.00E-02  davcor  =  10


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-03    1.000E-03

 units and filenames:
    1: ( 6)    ciudgls                                                     
    2: ( 5)    ciudgin                                                     
    3: ( 7)    ciudgsm                                                     
    4: ( 4)    cihdiag                                                     
    5: ( 3)    cihpseu                                                     
    6: (10)    cihvfl                                                      
    7: (11)    civfl                                                       
    8: (12)    diagint                                                     
    9: (13)    ofdgint                                                     
   10: (17)    cidrtfl                                                     
   11: (20)    ciftdfl                                                     
   12: (21)    ciftofl                                                     
   13: (24)    ciftifl                                                     
   14: ( 8)    ciflind                                                     
   15: (15)    civin                                                       
   16: (16)    civout                                                      
   18: (18)    d1fl                                                        
   20: (53)    cifvfl                                                      
   21: (25)    ciscr4                                                      
   22: (41)    ciftotd                                                     
   23: (42)    flacpfd                                                     
   26: (26)    ciscr5                                                      
   27: (47)    restart                                                     
   28: (48)    civout                                                      
   29: (49)    cirefv                                                      
   31: (31)    fil4w                                                       
   32: (32)    fil4x                                                       
   33: (33)    fil3w                                                       
   34: (34)    fil3x                                                       
   36: (36)    aoints                                                      
   37: (37)    aoints2                                                     
   38: (38)    drtfil                                                      
   39: (50)    cisrtif                                                     
   42: (50)    mocoef                                                      
   45: (52)    nocoef_ci                                                   
   46: (53)    civfl_restart                                               
 ------------------------------------------------------------------------

 workspace allocation information: lcore=  99999999 mem1=********** ifirst=-101239962

 integral file titles:
 Hermit Integral Program : SIFS version  ronja.itc.univieFri Sep  2 13:50:14 2005
  cidrt_title                                                                    
  title                                                                          
 mofmt: formatted orbitals label=morbl   ronja.itc.univieFri Sep  2 13:50:15 2005
 SIFS file created by program tran.      ronja.itc.univieFri Sep  2 13:50:15 2005

 core energy values from the integral file:
 energy( 1)=  3.116530982443E+01, ietype=   -1,    core energy of type: Nuc.Rep.

 total core repulsion energy =  3.116530982443E+01

 drt header information:
  cidrt_title                                                                    
 spnorb, spnodd, lxyzir,hmult F F 0 0 0 0
 nmot  =    38 niot  =     9 nfct  =     0 nfvt  =     0
 nrow  =    54 nsym  =     4 ssym  =     1 lenbuf=  1600
 nwalk,xbar:        218       15       72       65       66
 nvalwt,nvalw:      215       12       72       65       66
 ncsft:           14422
 total number of valid internal walks:     215
 nvalz,nvaly,nvalx,nvalw =       12      72      65      66

 cisrt info file parameters:
 file number  12 blocksize   4095
 mxbld   4095
 nd4ext,nd2ext,nd0ext   870   522    90
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int    29040    32805    16512     4275      474        0        0        0
 minbl4,minbl3,maxbl2   441   423   444
 maxbuf 32767
 number of external orbitals per symmetry block:  13   5   8   3
 nmsym   4 number of internal orbitals   9

 formula file title:
 Hermit Integral Program : SIFS version  ronja.itc.univieFri Sep  2 13:50:14 2005
  cidrt_title                                                                    
  title                                                                          
 mofmt: formatted orbitals label=morbl   ronja.itc.univieFri Sep  2 13:50:15 2005
 SIFS file created by program tran.      ronja.itc.univieFri Sep  2 13:50:15 2005
  cidrt_title                                                                    
 file nmb.  24 block size     0
 pthz,pthy,pthx,pthw:    15    72    65    66 total internal walks:     218
 maxlp3,n3xtlp,n2xtlp,n1xtlp,n0xtlp,n2lp,n1lp,n0lp     0     0     0     0
 orbsym(*)= 1 1 1 1 1 2 3 2 3

 setref:        2 references kept,
                1 references were marked as invalid, out of
                3 total.
 limcnvrt: found 12 valid internal walksout of  15
  walks (skipping trailing invalids)
  ... adding  12 segmentation marks segtype= 1
 limcnvrt: found 72 valid internal walksout of  72
  walks (skipping trailing invalids)
  ... adding  72 segmentation marks segtype= 2
 limcnvrt: found 65 valid internal walksout of  65
  walks (skipping trailing invalids)
  ... adding  65 segmentation marks segtype= 3
 limcnvrt: found 66 valid internal walksout of  66
  walks (skipping trailing invalids)
  ... adding  66 segmentation marks segtype= 4

 number of external paths / symmetry
 vertex x     119      89     119      79
 vertex w     148      89     119      79

 lprune: l(*,*,*) pruned with nwalk=      15 nvalwt=      12 nprune=       1

 lprune: l(*,*,*) pruned with nwalk=      72 nvalwt=      72 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=      65 nvalwt=      65 nprune=      19

 lprune: l(*,*,*) pruned with nwalk=      66 nvalwt=      66 nprune=      28



                   segmentation summary for type diagonal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1          14|        12|         0|        12|         0|         1|
 -------------------------------------------------------------------------------
  Y 2          72|       522|        12|        72|        12|         2|
 -------------------------------------------------------------------------------
  X 3          65|      6475|       534|        65|        84|         3|
 -------------------------------------------------------------------------------
  W 4          66|      7413|      7009|        66|       149|         4|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>     14422

                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      65      14       6475         12      65      12
     2  4   1    25      two-ext wz   2X  4 1      66      14       7413         12      66      12
     3  4   3    26      two-ext wx   2X  4 3      66      65       7413       6475      66      65
     4  2   1    11      one-ext yz   1X  2 1      72      14        522         12      72      12
     5  3   2    15      1ex3ex  yx   3X  3 2      65      72       6475        522      65      72
     6  4   2    16      1ex3ex  yw   3X  4 2      66      72       7413        522      66      72
     7  1   1     1      allint zz    OX  1 1      14      14         12         12      12      12
     8  2   2     5      0ex2ex yy    OX  2 2      72      72        522        522      72      72
     9  3   3     6      0ex2ex xx    OX  3 3      65      65       6475       6475      65      65
    10  4   4     7      0ex2ex ww    OX  4 4      66      66       7413       7413      66      66
    11  1   1    75      dg-024ext z  DG  1 1      14      14         12         12      12      12
    12  2   2    45      4exdg024 y   DG  2 2      72      72        522        522      72      72
    13  3   3    46      4exdg024 x   DG  3 3      65      65       6475       6475      65      65
    14  4   4    47      4exdg024 w   DG  4 4      66      66       7413       7413      66      66
----------------------------------------------------------------------------------------------------
 DIAGEL COUNTS: DG0X,DG2X,DG4X= 8597 1713 203
 diagonal elements written to file   4

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       477 2x:         0 4x:         0
All internal counts: zz :       157 yy:         0 xx:         0 ww:         0
One-external counts: yz :         0 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         0    task #   2:         0    task #   3:         0    task #   4:         0
task #   5:         0    task #   6:         0    task #   7:       156    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:       342    task #  12:         0
task #  13:         0    task #  14:         0    task #
 reference space has dimension       2

    root           eigenvalues
    ----           ------------
       1        -113.8658135082

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt= 12

         vector  1 from unit 11 written to unit 49 filename cirefv                                                      
 ufvoutnew: ... writing  recamt= 12

         vector  1 from unit 11 written to unit 49 filename cirefv                                                      

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:             14422
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               2
 number of roots to converge:                             1
 number of iterations:                                   30
 residual norm convergence criteria:               0.001000

 elast before the main iterative loop 
 elast =  -113.865814


          starting ci iteration   1

 cosmocalc =  1
 rootcalc =  1
 repnuc =   31.1653098

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= T F

################################################################################
 one electron density matrix calculation 
################################################################################


 workspace allocation information: mxcore=99999992

 space required:

    space required for calls in multd2:
       onex          247
       allin           0
       diagon        740
    max.      ---------
       maxnex        740

    total core space usage:
       maxnex        740
    max k-seg       7413
    max k-ind         72
              ---------
       totmax      15710
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=      51  DYX=     285  DYW=     277
   D0Z=      13  D0Y=     115  D0X=      92  D0W=      90
  DDZI=     101 DDYI=     626 DDXI=     550 DDWI=     537
  DDZE=       0 DDYE=      72 DDXE=      65 DDWE=      66
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   2.0000000      2.0000000      2.0000000      2.0000000      2.0000000
   0.0000000      0.0000000      0.0000000      0.0000000      0.0000000
   0.0000000      0.0000000      0.0000000      0.0000000      0.0000000
   0.0000000      0.0000000      0.0000000


*****   symmetry block SYM2   *****

 occupation numbers of nos
   2.0000000      0.0012739      0.0000000      0.0000000      0.0000000
   0.0000000      0.0000000


*****   symmetry block SYM3   *****

 occupation numbers of nos
   2.0000000      1.9987261      0.0000000      0.0000000      0.0000000
   0.0000000      0.0000000      0.0000000      0.0000000      0.0000000


*****   symmetry block SYM4   *****

 occupation numbers of nos
   0.0000000      0.0000000      0.0000000


 total number of electrons =   16.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = "99999992"

 Work memory size (LMWORK) :    99999992 =  762.94 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***

  Confirmation of dielectric energy for ground state
  edielnew =   0.


 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************

 *** qcos is copied to qcosdalton ***

 fepsi =   1.
 *** qcosdalton is mult by fepsi ***

 root =  1


 DALTON: user specified work memory size used,
          -m = "99999992"

 Work memory size (LMWORK) :    99999992 =  762.94 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 Warning! Large contrib. to nucrep.The distance is   0.017707087
 Warning! Large contrib. to nucrep.The distance is   0.000353311222
 ** Total nuclear repulsion energy ** =  31.25040227710494
 Warning! Large contrib. to nucrep.The distance is   0.017707087
 Warning! Large contrib. to nucrep.The distance is   0.000353311222
 screening nuclear repulsion energy   0.0139330636

 Total-screening nuclear repulsion energy   31.2364692


 Adding T+Vsolv ...
 maxdens  260
 *** End of DALTON-COSMO calculation ***

 DIAGEL COUNTS: DG0X,DG2X,DG4X= 8597 1713 203
 =========== Executing IN-CORE method ==========
 norm multnew:  1.


====================================================================================================
Diagonal     counts:  0x:      8597 2x:      1713 4x:       203
All internal counts: zz :       157 yy:      1315 xx:      1079 ww:       968
One-external counts: yz :       811 yx:      4545 yw:      4311
Two-external counts: yy :      1362 ww:       922 xx:      1105 xz:        70 wz:        98 wx:       990
Three-ext.   counts: yx :       670 yw:       670

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        59    task #   2:        82    task #   3:       704    task #   4:       756
task #   5:      4069    task #   6:      3725    task #   7:       156    task #   8:       972
task #   9:       877    task #  10:       671    task #  11:       342    task #  12:         1
task #  13:         1    task #  14:         1    task #
 rtolcosmo = eo,e,r,ro   0. -145.031123 -145.126279  31.2364692  31.1653098
 calctciref: ... reading 
 cirefv                                                       recamt= 12

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -113.8898098757  2.3996E-02  4.1346E-01  1.1266E+00  1.0000E-03


dielectric energy =      -0.0119981837
delta diel. energy =       0.0119981837
total Cosmo energy: e(nroot) + repnuc - ediel =    -113.8778116920
delta Cosmo energy =     113.8778116920

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.00   0.00   0.00   0.00         0.    0.0000
    2   25    0   0.01   0.00   0.00   0.00         0.    0.0000
    3   26    0   0.01   0.00   0.00   0.01         0.    0.0000
    4   11    0   0.01   0.00   0.00   0.01         0.    0.0000
    5   15    0   0.01   0.01   0.00   0.01         0.    0.0000
    6   16    0   0.01   0.00   0.00   0.01         0.    0.0000
    7    1    0   0.01   0.01   0.00   0.01         0.    0.0000
    8    5    0   0.00   0.00   0.00   0.00         0.    0.0000
    9    6    0   0.02   0.01   0.00   0.02         0.    0.0000
   10    7    0   0.01   0.00   0.00   0.01         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.01   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.00   0.00   0.00   0.00         0.    0.0000
   14   47    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1100s 
time spent in multnx:                   0.0900s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0300s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.8400s 

          starting ci iteration   2

 =========== Executing IN-CORE method ==========
 norm multnew:  0.189362504


====================================================================================================
Diagonal     counts:  0x:      8597 2x:      1713 4x:       203
All internal counts: zz :       157 yy:      1315 xx:      1079 ww:       968
One-external counts: yz :       811 yx:      4545 yw:      4311
Two-external counts: yy :      1362 ww:       922 xx:      1105 xz:        70 wz:        98 wx:       990
Three-ext.   counts: yx :       670 yw:       670

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        59    task #   2:        82    task #   3:       704    task #   4:       756
task #   5:      4069    task #   6:      3725    task #   7:       156    task #   8:       972
task #   9:       877    task #  10:       671    task #  11:       342    task #  12:         1
task #  13:         1    task #  14:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt= 12

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.95513190    -0.29618077

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1   -114.1849872111  2.9518E-01  2.1909E-02  2.6129E-01  1.0000E-03
 mr-sdci #  2  2   -110.8201094015  1.4206E+02  0.0000E+00  1.7664E+00  1.0000E-04


dielectric energy =      -0.0119981837
delta diel. energy =       0.0000000000
total Cosmo energy: e(nroot) + repnuc - ediel =    -114.1729890274
delta Cosmo energy =       0.2951773354

 root number  1 is used to define the new expansion vector.
 diagon: frcsub=2, new matrix-vector products will be computed.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.01   0.00   0.00   0.01         0.    0.0000
    2   25    0   0.00   0.00   0.00   0.00         0.    0.0000
    3   26    0   0.01   0.00   0.00   0.01         0.    0.0000
    4   11    0   0.01   0.00   0.00   0.01         0.    0.0000
    5   15    0   0.01   0.01   0.00   0.01         0.    0.0000
    6   16    0   0.02   0.00   0.00   0.02         0.    0.0000
    7    1    0   0.00   0.00   0.00   0.00         0.    0.0000
    8    5    0   0.01   0.00   0.00   0.01         0.    0.0000
    9    6    0   0.01   0.00   0.00   0.01         0.    0.0000
   10    7    0   0.02   0.01   0.00   0.02         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.01   0.00   0.00   0.01         0.    0.0000
   14   47    0   0.00   0.00   0.00   0.00         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1100s 
time spent in multnx:                   0.1100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0200s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.1300s 

          starting ci iteration   3

 cosmocalc =  1
 rootcalc =  1
 repnuc =   31.2364692

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=      51  DYX=     285  DYW=     277
   D0Z=      13  D0Y=     115  D0X=      92  D0W=      90
  DDZI=     101 DDYI=     626 DDXI=     550 DDWI=     537
  DDZE=       0 DDYE=      72 DDXE=      65 DDWE=      66
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   2.0000000      2.0000000      1.9912795      1.9826111      1.9730628
   0.0198968      0.0098829      0.0073553      0.0043966      0.0033320
   0.0022365      0.0013600      0.0007213      0.0006276      0.0003603
   0.0001930      0.0001053      0.0000562


*****   symmetry block SYM2   *****

 occupation numbers of nos
   1.9333541      0.0624089      0.0051298      0.0030125      0.0012391
   0.0004750      0.0003224


*****   symmetry block SYM3   *****

 occupation numbers of nos
   1.9807096      1.9739528      0.0208008      0.0077629      0.0044028
   0.0016031      0.0008184      0.0005317      0.0002555      0.0001256


*****   symmetry block SYM4   *****

 occupation numbers of nos
   0.0032088      0.0020814      0.0003274


 total number of electrons =   16.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = "99999992"

 Work memory size (LMWORK) :    99999992 =  762.94 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***

  Confirmation of dielectric energy for ground state
  edielnew =  -0.00260145764


 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************

 *** qcos is copied to qcosdalton ***

 fepsi =   1.
 *** qcosdalton is mult by fepsi ***

 root =  1


 DALTON: user specified work memory size used,
          -m = "99999992"

 Work memory size (LMWORK) :    99999992 =  762.94 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 Warning! Large contrib. to nucrep.The distance is   0.017707087
 Warning! Large contrib. to nucrep.The distance is   0.000353311222
 ** Total nuclear repulsion energy ** =  31.23641347616060
 Warning! Large contrib. to nucrep.The distance is   0.017707087
 Warning! Large contrib. to nucrep.The distance is   0.000353311222
 screening nuclear repulsion energy   0.0106575954

 Total-screening nuclear repulsion energy   31.2257559


 Adding T+Vsolv ...
 maxdens  260
 *** End of DALTON-COSMO calculation ***

 DIAGEL COUNTS: DG0X,DG2X,DG4X= 8597 1713 203
 =========== Executing IN-CORE method ==========
 norm multnew:  1.


====================================================================================================
Diagonal     counts:  0x:      8597 2x:      1713 4x:       203
All internal counts: zz :       157 yy:      1315 xx:      1079 ww:       968
One-external counts: yz :       811 yx:      4545 yw:      4311
Two-external counts: yy :      1362 ww:       922 xx:      1105 xz:        70 wz:        98 wx:       990
Three-ext.   counts: yx :       670 yw:       670

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        59    task #   2:        82    task #   3:       704    task #   4:       756
task #   5:      4069    task #   6:      3725    task #   7:       156    task #   8:       972
task #   9:       877    task #  10:       671    task #  11:       342    task #  12:         1
task #  13:         1    task #  14:         1    task #
 =========== Executing IN-CORE method ==========
 norm multnew:  0.00923269529


====================================================================================================
Diagonal     counts:  0x:      8597 2x:      1713 4x:       203
All internal counts: zz :       157 yy:      1315 xx:      1079 ww:       968
One-external counts: yz :       811 yx:      4545 yw:      4311
Two-external counts: yy :      1362 ww:       922 xx:      1105 xz:        70 wz:        98 wx:       990
Three-ext.   counts: yx :       670 yw:       670

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        59    task #   2:        82    task #   3:       704    task #   4:       756
task #   5:      4069    task #   6:      3725    task #   7:       156    task #   8:       972
task #   9:       877    task #  10:       671    task #  11:       342    task #  12:         1
task #  13:         1    task #  14:         1    task #
 rtolcosmo = eo,e,r,ro   0. -145.421456 -145.425073  31.2257559  31.2364692
 rtolcosmo = eo,e,r,ro   0. -142.056579 -142.344055  31.2257559  31.2364692
 calctciref: ... reading 
 cirefv                                                       recamt= 12

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.95810256     0.00430325

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1   -114.1993172554  1.4330E-02  2.2869E-03  6.9356E-02  1.0000E-03
 mr-sdci #  3  2   -111.1182995703  2.9819E-01  0.0000E+00  1.4490E+00  1.0000E-04


dielectric energy =      -0.0090584239
delta diel. energy =      -0.0029397598
total Cosmo energy: e(nroot) + repnuc - ediel =    -114.1902588315
delta Cosmo energy =       0.0172698042

 root number  1 is used to define the new expansion vector.
 diagon: frcsub=2, new matrix-vector products will be computed.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.01   0.00   0.00   0.00         0.    0.0000
    2   25    0   0.00   0.00   0.00   0.00         0.    0.0000
    3   26    0   0.02   0.00   0.00   0.02         0.    0.0000
    4   11    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   15    0   0.01   0.00   0.00   0.01         0.    0.0000
    6   16    0   0.02   0.00   0.00   0.01         0.    0.0000
    7    1    0   0.00   0.00   0.00   0.00         0.    0.0000
    8    5    0   0.01   0.00   0.00   0.01         0.    0.0000
    9    6    0   0.01   0.00   0.00   0.01         0.    0.0000
   10    7    0   0.02   0.00   0.00   0.02         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.01   0.00   0.00   0.01         0.    0.0000
   14   47    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.9700s 

          starting ci iteration   4

 cosmocalc =  1
 rootcalc =  1
 repnuc =   31.2257559

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=      51  DYX=     285  DYW=     277
   D0Z=      13  D0Y=     115  D0X=      92  D0W=      90
  DDZI=     101 DDYI=     626 DDXI=     550 DDWI=     537
  DDZE=       0 DDYE=      72 DDXE=      65 DDWE=      66
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   2.0000000      2.0000000      1.9886948      1.9783648      1.9734226
   0.0182301      0.0132895      0.0078858      0.0052906      0.0043757
   0.0030834      0.0018874      0.0010204      0.0009056      0.0005221
   0.0002692      0.0001284      0.0000628


*****   symmetry block SYM2   *****

 occupation numbers of nos
   1.9517668      0.0425658      0.0055657      0.0041101      0.0017071
   0.0006331      0.0004825


*****   symmetry block SYM3   *****

 occupation numbers of nos
   1.9782912      1.9724933      0.0182811      0.0086958      0.0053184
   0.0023046      0.0012424      0.0007648      0.0003696      0.0001395


*****   symmetry block SYM4   *****

 occupation numbers of nos
   0.0044115      0.0029316      0.0004918


 total number of electrons =   16.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = "99999992"

 Work memory size (LMWORK) :    99999992 =  762.94 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***

  Confirmation of dielectric energy for ground state
  edielnew =  -0.00228414549


 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************

 *** qcos is copied to qcosdalton ***

 fepsi =   1.
 *** qcosdalton is mult by fepsi ***

 root =  1


 DALTON: user specified work memory size used,
          -m = "99999992"

 Work memory size (LMWORK) :    99999992 =  762.94 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 Warning! Large contrib. to nucrep.The distance is   0.017707087
 Warning! Large contrib. to nucrep.The distance is   0.000353311222
 ** Total nuclear repulsion energy ** =  31.23501448619616
 Warning! Large contrib. to nucrep.The distance is   0.017707087
 Warning! Large contrib. to nucrep.The distance is   0.000353311222
 screening nuclear repulsion energy   0.0109248655

 Total-screening nuclear repulsion energy   31.2240896


 Adding T+Vsolv ...
 maxdens  260
 *** End of DALTON-COSMO calculation ***

 DIAGEL COUNTS: DG0X,DG2X,DG4X= 8597 1713 203
 =========== Executing IN-CORE method ==========
 norm multnew:  1.


====================================================================================================
Diagonal     counts:  0x:      8597 2x:      1713 4x:       203
All internal counts: zz :       157 yy:      1315 xx:      1079 ww:       968
One-external counts: yz :       811 yx:      4545 yw:      4311
Two-external counts: yy :      1362 ww:       922 xx:      1105 xz:        70 wz:        98 wx:       990
Three-ext.   counts: yx :       670 yw:       670

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        59    task #   2:        82    task #   3:       704    task #   4:       756
task #   5:      4069    task #   6:      3725    task #   7:       156    task #   8:       972
task #   9:       877    task #  10:       671    task #  11:       342    task #  12:         1
task #  13:         1    task #  14:         1    task #
 =========== Executing IN-CORE method ==========
 norm multnew:  0.00284554263


====================================================================================================
Diagonal     counts:  0x:      8597 2x:      1713 4x:       203
All internal counts: zz :       157 yy:      1315 xx:      1079 ww:       968
One-external counts: yz :       811 yx:      4545 yw:      4311
Two-external counts: yy :      1362 ww:       922 xx:      1105 xz:        70 wz:        98 wx:       990
Three-ext.   counts: yx :       670 yw:       670

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        59    task #   2:        82    task #   3:       704    task #   4:       756
task #   5:      4069    task #   6:      3725    task #   7:       156    task #   8:       972
task #   9:       877    task #  10:       671    task #  11:       342    task #  12:         1
task #  13:         1    task #  14:         1    task #
 rtolcosmo = eo,e,r,ro   0. -145.425073 -145.42491  31.2240896  31.2257559
 rtolcosmo = eo,e,r,ro   0. -142.344055 -142.752487  31.2240896  31.2257559
 calctciref: ... reading 
 cirefv                                                       recamt= 12

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.95249685    -0.26098898

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1   -114.2008201161  1.5029E-03  3.0887E-04  3.0112E-02  1.0000E-03
 mr-sdci #  4  2   -111.5283977344  4.1010E-01  0.0000E+00  1.7946E+00  1.0000E-04


dielectric energy =      -0.0092179184
delta diel. energy =       0.0001594945
total Cosmo energy: e(nroot) + repnuc - ediel =    -114.1916021977
delta Cosmo energy =       0.0013433662

 root number  1 is used to define the new expansion vector.
 diagon: frcsub=2, new matrix-vector products will be computed.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.00   0.00   0.00   0.00         0.    0.0000
    2   25    0   0.00   0.00   0.00   0.00         0.    0.0000
    3   26    0   0.02   0.01   0.00   0.02         0.    0.0000
    4   11    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   15    0   0.02   0.00   0.00   0.02         0.    0.0000
    6   16    0   0.01   0.00   0.00   0.01         0.    0.0000
    7    1    0   0.00   0.00   0.00   0.00         0.    0.0000
    8    5    0   0.01   0.00   0.00   0.01         0.    0.0000
    9    6    0   0.02   0.01   0.00   0.02         0.    0.0000
   10    7    0   0.01   0.00   0.00   0.01         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.01   0.00   0.00   0.01         0.    0.0000
   14   47    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1100s 
time spent in multnx:                   0.1100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0200s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.9700s 

          starting ci iteration   5

 cosmocalc =  1
 rootcalc =  1
 repnuc =   31.2240896

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=      51  DYX=     285  DYW=     277
   D0Z=      13  D0Y=     115  D0X=      92  D0W=      90
  DDZI=     101 DDYI=     626 DDXI=     550 DDWI=     537
  DDZE=       0 DDYE=      72 DDXE=      65 DDWE=      66
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   2.0000000      2.0000000      1.9873611      1.9759995      1.9704216
   0.0206923      0.0150333      0.0087916      0.0056226      0.0047578
   0.0033742      0.0020920      0.0011279      0.0009908      0.0005793
   0.0002917      0.0001414      0.0000689


*****   symmetry block SYM2   *****

 occupation numbers of nos
   1.9448714      0.0492350      0.0058134      0.0044586      0.0018834
   0.0007041      0.0005366


*****   symmetry block SYM3   *****

 occupation numbers of nos
   1.9759280      1.9690263      0.0211196      0.0094425      0.0057448
   0.0025441      0.0013930      0.0008421      0.0004118      0.0001538


*****   symmetry block SYM4   *****

 occupation numbers of nos
   0.0048009      0.0032027      0.0005420


 total number of electrons =   16.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = "99999992"

 Work memory size (LMWORK) :    99999992 =  762.94 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***

  Confirmation of dielectric energy for ground state
  edielnew =  -0.00225151542


 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************

 *** qcos is copied to qcosdalton ***

 fepsi =   1.
 *** qcosdalton is mult by fepsi ***

 root =  1


 DALTON: user specified work memory size used,
          -m = "99999992"

 Work memory size (LMWORK) :    99999992 =  762.94 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 Warning! Large contrib. to nucrep.The distance is   0.017707087
 Warning! Large contrib. to nucrep.The distance is   0.000353311222
 ** Total nuclear repulsion energy ** =  31.23197275533635
 Warning! Large contrib. to nucrep.The distance is   0.017707087
 Warning! Large contrib. to nucrep.The distance is   0.000353311222
 screening nuclear repulsion energy   0.0104372225

 Total-screening nuclear repulsion energy   31.2215355


 Adding T+Vsolv ...
 maxdens  260
 *** End of DALTON-COSMO calculation ***

 DIAGEL COUNTS: DG0X,DG2X,DG4X= 8597 1713 203
 =========== Executing IN-CORE method ==========
 norm multnew:  1.


====================================================================================================
Diagonal     counts:  0x:      8597 2x:      1713 4x:       203
All internal counts: zz :       157 yy:      1315 xx:      1079 ww:       968
One-external counts: yz :       811 yx:      4545 yw:      4311
Two-external counts: yy :      1362 ww:       922 xx:      1105 xz:        70 wz:        98 wx:       990
Three-ext.   counts: yx :       670 yw:       670

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        59    task #   2:        82    task #   3:       704    task #   4:       756
task #   5:      4069    task #   6:      3725    task #   7:       156    task #   8:       972
task #   9:       877    task #  10:       671    task #  11:       342    task #  12:         1
task #  13:         1    task #  14:         1    task #
 =========== Executing IN-CORE method ==========
 norm multnew:  0.000188591004


====================================================================================================
Diagonal     counts:  0x:      8597 2x:      1713 4x:       203
All internal counts: zz :       157 yy:      1315 xx:      1079 ww:       968
One-external counts: yz :       811 yx:      4545 yw:      4311
Two-external counts: yy :      1362 ww:       922 xx:      1105 xz:        70 wz:        98 wx:       990
Three-ext.   counts: yx :       670 yw:       670

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        59    task #   2:        82    task #   3:       704    task #   4:       756
task #   5:      4069    task #   6:      3725    task #   7:       156    task #   8:       972
task #   9:       877    task #  10:       671    task #  11:       342    task #  12:         1
task #  13:         1    task #  14:         1    task #
 rtolcosmo = eo,e,r,ro   0. -145.42491 -145.422194  31.2215355  31.2240896
 rtolcosmo = eo,e,r,ro   0. -142.752487 -142.61183  31.2215355  31.2240896
 calctciref: ... reading 
 cirefv                                                       recamt= 12

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.95351164     0.10271551

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1   -114.2006585283 -1.6159E-04  1.0906E-04  1.3667E-02  1.0000E-03
 mr-sdci #  5  2   -111.3902948228 -1.3810E-01  0.0000E+00  1.8336E+00  1.0000E-04


dielectric energy =      -0.0087994142
delta diel. energy =      -0.0004185042
total Cosmo energy: e(nroot) + repnuc - ediel =    -114.1918591141
delta Cosmo energy =       0.0002569164

 root number  1 is used to define the new expansion vector.
 diagon: frcsub=2, new matrix-vector products will be computed.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.00   0.00   0.00   0.00         0.    0.0000
    2   25    0   0.01   0.00   0.00   0.00         0.    0.0000
    3   26    0   0.01   0.00   0.00   0.01         0.    0.0000
    4   11    0   0.01   0.00   0.00   0.01         0.    0.0000
    5   15    0   0.01   0.01   0.00   0.01         0.    0.0000
    6   16    0   0.01   0.00   0.00   0.01         0.    0.0000
    7    1    0   0.01   0.00   0.00   0.00         0.    0.0000
    8    5    0   0.00   0.00   0.00   0.00         0.    0.0000
    9    6    0   0.02   0.00   0.00   0.01         0.    0.0000
   10    7    0   0.02   0.00   0.00   0.02         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.01   0.00   0.00   0.01         0.    0.0000
   14   47    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.0900s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.9800s 

          starting ci iteration   6

 cosmocalc =  1
 rootcalc =  1
 repnuc =   31.2215355

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=      51  DYX=     285  DYW=     277
   D0Z=      13  D0Y=     115  D0X=      92  D0W=      90
  DDZI=     101 DDYI=     626 DDXI=     550 DDWI=     537
  DDZE=       0 DDYE=      72 DDXE=      65 DDWE=      66
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   2.0000000      2.0000000      1.9875561      1.9765130      1.9714010
   0.0201686      0.0150963      0.0085132      0.0053513      0.0046065
   0.0032680      0.0020457      0.0011047      0.0009644      0.0005705
   0.0002808      0.0001384      0.0000666


*****   symmetry block SYM2   *****

 occupation numbers of nos
   1.9465287      0.0478900      0.0054591      0.0043250      0.0018485
   0.0006988      0.0005285


*****   symmetry block SYM3   *****

 occupation numbers of nos
   1.9768671      1.9692917      0.0207298      0.0091560      0.0055058
   0.0024833      0.0013776      0.0008242      0.0004073      0.0001469


*****   symmetry block SYM4   *****

 occupation numbers of nos
   0.0046523      0.0031032      0.0005310


 total number of electrons =   16.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = "99999992"

 Work memory size (LMWORK) :    99999992 =  762.94 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***

  Confirmation of dielectric energy for ground state
  edielnew =  -0.00217376881


 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************

 *** qcos is copied to qcosdalton ***

 fepsi =   1.
 *** qcosdalton is mult by fepsi ***

 root =  1


 DALTON: user specified work memory size used,
          -m = "99999992"

 Work memory size (LMWORK) :    99999992 =  762.94 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 Warning! Large contrib. to nucrep.The distance is   0.017707087
 Warning! Large contrib. to nucrep.The distance is   0.000353311222
 ** Total nuclear repulsion energy ** =  31.23010422335247
 Warning! Large contrib. to nucrep.The distance is   0.017707087
 Warning! Large contrib. to nucrep.The distance is   0.000353311222
 screening nuclear repulsion energy   0.0102036096

 Total-screening nuclear repulsion energy   31.2199006


 Adding T+Vsolv ...
 maxdens  260
 *** End of DALTON-COSMO calculation ***

 DIAGEL COUNTS: DG0X,DG2X,DG4X= 8597 1713 203
 =========== Executing IN-CORE method ==========
 norm multnew:  1.


====================================================================================================
Diagonal     counts:  0x:      8597 2x:      1713 4x:       203
All internal counts: zz :       157 yy:      1315 xx:      1079 ww:       968
One-external counts: yz :       811 yx:      4545 yw:      4311
Two-external counts: yy :      1362 ww:       922 xx:      1105 xz:        70 wz:        98 wx:       990
Three-ext.   counts: yx :       670 yw:       670

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        59    task #   2:        82    task #   3:       704    task #   4:       756
task #   5:      4069    task #   6:      3725    task #   7:       156    task #   8:       972
task #   9:       877    task #  10:       671    task #  11:       342    task #  12:         1
task #  13:         1    task #  14:         1    task #
 =========== Executing IN-CORE method ==========
 norm multnew:  0.000158616257


====================================================================================================
Diagonal     counts:  0x:      8597 2x:      1713 4x:       203
All internal counts: zz :       157 yy:      1315 xx:      1079 ww:       968
One-external counts: yz :       811 yx:      4545 yw:      4311
Two-external counts: yy :      1362 ww:       922 xx:      1105 xz:        70 wz:        98 wx:       990
Three-ext.   counts: yx :       670 yw:       670

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        59    task #   2:        82    task #   3:       704    task #   4:       756
task #   5:      4069    task #   6:      3725    task #   7:       156    task #   8:       972
task #   9:       877    task #  10:       671    task #  11:       342    task #  12:         1
task #  13:         1    task #  14:         1    task #
 rtolcosmo = eo,e,r,ro   0. -145.422194 -145.420421  31.2199006  31.2215355
 rtolcosmo = eo,e,r,ro   0. -142.61183 -143.21994  31.2199006  31.2215355
 calctciref: ... reading 
 cirefv                                                       recamt= 12

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.95203001    -0.27020356

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  6  1   -114.2005199964 -1.3853E-04  2.4959E-05  8.0309E-03  1.0000E-03
 mr-sdci #  6  2   -112.0000390859  6.0974E-01  0.0000E+00  1.7777E+00  1.0000E-04


dielectric energy =      -0.0085920729
delta diel. energy =      -0.0002073414
total Cosmo energy: e(nroot) + repnuc - ediel =    -114.1919279235
delta Cosmo energy =       0.0000688094

 root number  1 is used to define the new expansion vector.
 diagon: frcsub=2, new matrix-vector products will be computed.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.00   0.00   0.00   0.00         0.    0.0000
    2   25    0   0.00   0.00   0.00   0.00         0.    0.0000
    3   26    0   0.02   0.00   0.00   0.02         0.    0.0000
    4   11    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   15    0   0.02   0.00   0.00   0.02         0.    0.0000
    6   16    0   0.01   0.01   0.00   0.01         0.    0.0000
    7    1    0   0.00   0.00   0.00   0.00         0.    0.0000
    8    5    0   0.01   0.00   0.00   0.01         0.    0.0000
    9    6    0   0.02   0.01   0.00   0.02         0.    0.0000
   10    7    0   0.01   0.00   0.00   0.01         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.01   0.00   0.00   0.01         0.    0.0000
   14   47    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1100s 
time spent in multnx:                   0.1100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0200s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.9600s 

          starting ci iteration   7

 cosmocalc =  1
 rootcalc =  1
 repnuc =   31.2199006

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=      51  DYX=     285  DYW=     277
   D0Z=      13  D0Y=     115  D0X=      92  D0W=      90
  DDZI=     101 DDYI=     626 DDXI=     550 DDWI=     537
  DDZE=       0 DDYE=      72 DDXE=      65 DDWE=      66
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   2.0000000      2.0000000      1.9872346      1.9759369      1.9706496
   0.0207838      0.0155635      0.0087382      0.0054529      0.0047037
   0.0033309      0.0020881      0.0011284      0.0009825      0.0005835
   0.0002857      0.0001416      0.0000681


*****   symmetry block SYM2   *****

 occupation numbers of nos
   1.9446207      0.0496625      0.0055673      0.0044035      0.0018909
   0.0007155      0.0005396


*****   symmetry block SYM3   *****

 occupation numbers of nos
   1.9762610      1.9684364      0.0214369      0.0093907      0.0056157
   0.0025315      0.0014054      0.0008416      0.0004164      0.0001498


*****   symmetry block SYM4   *****

 occupation numbers of nos
   0.0047411      0.0031602      0.0005416


 total number of electrons =   16.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = "99999992"

 Work memory size (LMWORK) :    99999992 =  762.94 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***

  Confirmation of dielectric energy for ground state
  edielnew =  -0.0021306126


 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************

 *** qcos is copied to qcosdalton ***

 fepsi =   1.
 *** qcosdalton is mult by fepsi ***

 root =  1


 DALTON: user specified work memory size used,
          -m = "99999992"

 Work memory size (LMWORK) :    99999992 =  762.94 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 Warning! Large contrib. to nucrep.The distance is   0.017707087
 Warning! Large contrib. to nucrep.The distance is   0.000353311222
 ** Total nuclear repulsion energy ** =  31.22898404008992
 Warning! Large contrib. to nucrep.The distance is   0.017707087
 Warning! Large contrib. to nucrep.The distance is   0.000353311222
 screening nuclear repulsion energy   0.0100459361

 Total-screening nuclear repulsion energy   31.2189381


 Adding T+Vsolv ...
 maxdens  260
 *** End of DALTON-COSMO calculation ***

 DIAGEL COUNTS: DG0X,DG2X,DG4X= 8597 1713 203
 =========== Executing IN-CORE method ==========
 norm multnew:  1.


====================================================================================================
Diagonal     counts:  0x:      8597 2x:      1713 4x:       203
All internal counts: zz :       157 yy:      1315 xx:      1079 ww:       968
One-external counts: yz :       811 yx:      4545 yw:      4311
Two-external counts: yy :      1362 ww:       922 xx:      1105 xz:        70 wz:        98 wx:       990
Three-ext.   counts: yx :       670 yw:       670

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        59    task #   2:        82    task #   3:       704    task #   4:       756
task #   5:      4069    task #   6:      3725    task #   7:       156    task #   8:       972
task #   9:       877    task #  10:       671    task #  11:       342    task #  12:         1
task #  13:         1    task #  14:         1    task #
 =========== Executing IN-CORE method ==========
 norm multnew:  1.96638091E-05


====================================================================================================
Diagonal     counts:  0x:      8597 2x:      1713 4x:       203
All internal counts: zz :       157 yy:      1315 xx:      1079 ww:       968
One-external counts: yz :       811 yx:      4545 yw:      4311
Two-external counts: yy :      1362 ww:       922 xx:      1105 xz:        70 wz:        98 wx:       990
Three-ext.   counts: yx :       670 yw:       670

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        59    task #   2:        82    task #   3:       704    task #   4:       756
task #   5:      4069    task #   6:      3725    task #   7:       156    task #   8:       972
task #   9:       877    task #  10:       671    task #  11:       342    task #  12:         1
task #  13:         1    task #  14:         1    task #
 rtolcosmo = eo,e,r,ro   0. -145.420421 -145.419342  31.2189381  31.2199006
 rtolcosmo = eo,e,r,ro   0. -143.21994 -143.292492  31.2189381  31.2199006
 calctciref: ... reading 
 cirefv                                                       recamt= 12

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.95232268     0.08962443

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  7  1   -114.2004038674 -1.1613E-04  1.2148E-05  4.4237E-03  1.0000E-03
 mr-sdci #  7  2   -112.0735541108  7.3515E-02  0.0000E+00  1.9491E+00  1.0000E-04


dielectric energy =      -0.0084534227
delta diel. energy =      -0.0001386501
total Cosmo energy: e(nroot) + repnuc - ediel =    -114.1919504447
delta Cosmo energy =       0.0000225212

 root number  1 is used to define the new expansion vector.
 diagon: frcsub=2, new matrix-vector products will be computed.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.00   0.00   0.00   0.00         0.    0.0000
    2   25    0   0.00   0.00   0.00   0.00         0.    0.0000
    3   26    0   0.02   0.00   0.00   0.02         0.    0.0000
    4   11    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   15    0   0.02   0.00   0.00   0.02         0.    0.0000
    6   16    0   0.01   0.01   0.00   0.01         0.    0.0000
    7    1    0   0.00   0.00   0.00   0.00         0.    0.0000
    8    5    0   0.01   0.00   0.00   0.01         0.    0.0000
    9    6    0   0.02   0.01   0.00   0.02         0.    0.0000
   10    7    0   0.01   0.00   0.00   0.01         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.01   0.00   0.00   0.01         0.    0.0000
   14   47    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1100s 
time spent in multnx:                   0.1100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0200s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.9700s 

          starting ci iteration   8

 cosmocalc =  1
 rootcalc =  1
 repnuc =   31.2189381

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=      51  DYX=     285  DYW=     277
   D0Z=      13  D0Y=     115  D0X=      92  D0W=      90
  DDZI=     101 DDYI=     626 DDXI=     550 DDWI=     537
  DDZE=       0 DDYE=      72 DDXE=      65 DDWE=      66
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   2.0000000      2.0000000      1.9873344      1.9761560      1.9709793
   0.0205938      0.0155362      0.0086343      0.0053780      0.0046534
   0.0032901      0.0020659      0.0011177      0.0009708      0.0005790
   0.0002820      0.0001403      0.0000675


*****   symmetry block SYM2   *****

 occupation numbers of nos
   1.9450141      0.0493166      0.0054856      0.0043487      0.0018766
   0.0007116      0.0005347


*****   symmetry block SYM3   *****

 occupation numbers of nos
   1.9765431      1.9686149      0.0213035      0.0092987      0.0055411
   0.0025023      0.0013919      0.0008336      0.0004132      0.0001478


*****   symmetry block SYM4   *****

 occupation numbers of nos
   0.0046858      0.0031207      0.0005365


 total number of electrons =   16.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = "99999992"

 Work memory size (LMWORK) :    99999992 =  762.94 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***

  Confirmation of dielectric energy for ground state
  edielnew =  -0.00210012744


 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************

 *** qcos is copied to qcosdalton ***

 fepsi =   1.
 *** qcosdalton is mult by fepsi ***

 root =  1


 DALTON: user specified work memory size used,
          -m = "99999992"

 Work memory size (LMWORK) :    99999992 =  762.94 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 Warning! Large contrib. to nucrep.The distance is   0.017707087
 Warning! Large contrib. to nucrep.The distance is   0.000353311222
 ** Total nuclear repulsion energy ** =  31.22805778576774
 Warning! Large contrib. to nucrep.The distance is   0.017707087
 Warning! Large contrib. to nucrep.The distance is   0.000353311222
 screening nuclear repulsion energy   0.00992535065

 Total-screening nuclear repulsion energy   31.2181324


 Adding T+Vsolv ...
 maxdens  260
 *** End of DALTON-COSMO calculation ***

 DIAGEL COUNTS: DG0X,DG2X,DG4X= 8597 1713 203
 =========== Executing IN-CORE method ==========
 norm multnew:  1.


====================================================================================================
Diagonal     counts:  0x:      8597 2x:      1713 4x:       203
All internal counts: zz :       157 yy:      1315 xx:      1079 ww:       968
One-external counts: yz :       811 yx:      4545 yw:      4311
Two-external counts: yy :      1362 ww:       922 xx:      1105 xz:        70 wz:        98 wx:       990
Three-ext.   counts: yx :       670 yw:       670

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        59    task #   2:        82    task #   3:       704    task #   4:       756
task #   5:      4069    task #   6:      3725    task #   7:       156    task #   8:       972
task #   9:       877    task #  10:       671    task #  11:       342    task #  12:         1
task #  13:         1    task #  14:         1    task #
 =========== Executing IN-CORE method ==========
 norm multnew:  1.7718725E-05


====================================================================================================
Diagonal     counts:  0x:      8597 2x:      1713 4x:       203
All internal counts: zz :       157 yy:      1315 xx:      1079 ww:       968
One-external counts: yz :       811 yx:      4545 yw:      4311
Two-external counts: yy :      1362 ww:       922 xx:      1105 xz:        70 wz:        98 wx:       990
Three-ext.   counts: yx :       670 yw:       670

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        59    task #   2:        82    task #   3:       704    task #   4:       756
task #   5:      4069    task #   6:      3725    task #   7:       156    task #   8:       972
task #   9:       877    task #  10:       671    task #  11:       342    task #  12:         1
task #  13:         1    task #  14:         1    task #
 rtolcosmo = eo,e,r,ro   0. -145.419342 -145.418439  31.2181324  31.2189381
 rtolcosmo = eo,e,r,ro   0. -143.292492 -143.424454  31.2181324  31.2189381
 calctciref: ... reading 
 cirefv                                                       recamt= 12

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.95181053    -0.25959300

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  8  1   -114.2003065576 -9.7310E-05  3.4296E-06  2.8330E-03  1.0000E-03
 mr-sdci #  8  2   -112.2063214002  1.3277E-01  0.0000E+00  1.8104E+00  1.0000E-04


dielectric energy =      -0.0083479459
delta diel. energy =      -0.0001054768
total Cosmo energy: e(nroot) + repnuc - ediel =    -114.1919586117
delta Cosmo energy =       0.0000081670

 root number  1 is used to define the new expansion vector.
 diagon: frcsub=2, new matrix-vector products will be computed.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.01   0.00   0.00   0.00         0.    0.0000
    2   25    0   0.00   0.00   0.00   0.00         0.    0.0000
    3   26    0   0.02   0.00   0.00   0.02         0.    0.0000
    4   11    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   15    0   0.01   0.00   0.00   0.01         0.    0.0000
    6   16    0   0.02   0.00   0.00   0.01         0.    0.0000
    7    1    0   0.00   0.00   0.00   0.00         0.    0.0000
    8    5    0   0.01   0.00   0.00   0.01         0.    0.0000
    9    6    0   0.01   0.00   0.00   0.01         0.    0.0000
   10    7    0   0.02   0.00   0.00   0.02         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.01   0.00   0.00   0.01         0.    0.0000
   14   47    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.9800s 

          starting ci iteration   9

 cosmocalc =  1
 rootcalc =  1
 repnuc =   31.2181324

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=      51  DYX=     285  DYW=     277
   D0Z=      13  D0Y=     115  D0X=      92  D0W=      90
  DDZI=     101 DDYI=     626 DDXI=     550 DDWI=     537
  DDZE=       0 DDYE=      72 DDXE=      65 DDWE=      66
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   2.0000000      2.0000000      1.9872328      1.9759739      1.9707327
   0.0207917      0.0156782      0.0087040      0.0054139      0.0046870
   0.0033115      0.0020798      0.0011257      0.0009767      0.0005834
   0.0002838      0.0001412      0.0000680


*****   symmetry block SYM2   *****

 occupation numbers of nos
   1.9443277      0.0499456      0.0055256      0.0043734      0.0018914
   0.0007173      0.0005384


*****   symmetry block SYM3   *****

 occupation numbers of nos
   1.9763372      1.9683544      0.0215328      0.0093734      0.0055791
   0.0025181      0.0014006      0.0008394      0.0004161      0.0001489


*****   symmetry block SYM4   *****

 occupation numbers of nos
   0.0047159      0.0031400      0.0005403


 total number of electrons =   16.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = "99999992"

 Work memory size (LMWORK) :    99999992 =  762.94 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***

  Confirmation of dielectric energy for ground state
  edielnew =  -0.00207995763


 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************

 *** qcos is copied to qcosdalton ***

 fepsi =   1.
 *** qcosdalton is mult by fepsi ***

 root =  1


 DALTON: user specified work memory size used,
          -m = "99999992"

 Work memory size (LMWORK) :    99999992 =  762.94 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 Warning! Large contrib. to nucrep.The distance is   0.017707087
 Warning! Large contrib. to nucrep.The distance is   0.000353311222
 ** Total nuclear repulsion energy ** =  31.22759188224744
 Warning! Large contrib. to nucrep.The distance is   0.017707087
 Warning! Large contrib. to nucrep.The distance is   0.000353311222
 screening nuclear repulsion energy   0.00986216672

 Total-screening nuclear repulsion energy   31.2177297


 Adding T+Vsolv ...
 maxdens  260
 *** End of DALTON-COSMO calculation ***

 DIAGEL COUNTS: DG0X,DG2X,DG4X= 8597 1713 203
 =========== Executing IN-CORE method ==========
 norm multnew:  1.


====================================================================================================
Diagonal     counts:  0x:      8597 2x:      1713 4x:       203
All internal counts: zz :       157 yy:      1315 xx:      1079 ww:       968
One-external counts: yz :       811 yx:      4545 yw:      4311
Two-external counts: yy :      1362 ww:       922 xx:      1105 xz:        70 wz:        98 wx:       990
Three-ext.   counts: yx :       670 yw:       670

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        59    task #   2:        82    task #   3:       704    task #   4:       756
task #   5:      4069    task #   6:      3725    task #   7:       156    task #   8:       972
task #   9:       877    task #  10:       671    task #  11:       342    task #  12:         1
task #  13:         1    task #  14:         1    task #
 =========== Executing IN-CORE method ==========
 norm multnew:  3.12270951E-06


====================================================================================================
Diagonal     counts:  0x:      8597 2x:      1713 4x:       203
All internal counts: zz :       157 yy:      1315 xx:      1079 ww:       968
One-external counts: yz :       811 yx:      4545 yw:      4311
Two-external counts: yy :      1362 ww:       922 xx:      1105 xz:        70 wz:        98 wx:       990
Three-ext.   counts: yx :       670 yw:       670

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        59    task #   2:        82    task #   3:       704    task #   4:       756
task #   5:      4069    task #   6:      3725    task #   7:       156    task #   8:       972
task #   9:       877    task #  10:       671    task #  11:       342    task #  12:         1
task #  13:         1    task #  14:         1    task #
 rtolcosmo = eo,e,r,ro   0. -145.418439 -145.417983  31.2177297  31.2181324
 rtolcosmo = eo,e,r,ro   0. -143.424454 -143.596036  31.2177297  31.2181324
 calctciref: ... reading 
 cirefv                                                       recamt= 12

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.95193021     0.09090614

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  9  1   -114.2002536411 -5.2916E-05  1.8970E-06  1.7600E-03  1.0000E-03
 mr-sdci #  9  2   -112.3783062319  1.7198E-01  0.0000E+00  1.8787E+00  1.0000E-04


dielectric energy =      -0.0082918149
delta diel. energy =      -0.0000561310
total Cosmo energy: e(nroot) + repnuc - ediel =    -114.1919618262
delta Cosmo energy =       0.0000032146

 root number  1 is used to define the new expansion vector.
 diagon: frcsub=2, new matrix-vector products will be computed.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.00   0.00   0.00   0.00         0.    0.0000
    2   25    0   0.00   0.00   0.00   0.00         0.    0.0000
    3   26    0   0.02   0.00   0.00   0.02         0.    0.0000
    4   11    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   15    0   0.02   0.01   0.00   0.02         0.    0.0000
    6   16    0   0.01   0.00   0.00   0.01         0.    0.0000
    7    1    0   0.00   0.00   0.00   0.00         0.    0.0000
    8    5    0   0.01   0.00   0.00   0.01         0.    0.0000
    9    6    0   0.02   0.00   0.00   0.02         0.    0.0000
   10    7    0   0.01   0.00   0.00   0.01         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.01   0.00   0.00   0.01         0.    0.0000
   14   47    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1100s 
time spent in multnx:                   0.1100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0100s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.9700s 

          starting ci iteration  10

 cosmocalc =  1
 rootcalc =  1
 repnuc =   31.2177297

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=      51  DYX=     285  DYW=     277
   D0Z=      13  D0Y=     115  D0X=      92  D0W=      90
  DDZI=     101 DDYI=     626 DDXI=     550 DDWI=     537
  DDZE=       0 DDYE=      72 DDXE=      65 DDWE=      66
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   2.0000000      2.0000000      1.9872784      1.9760716      1.9708692
   0.0207050      0.0156460      0.0086581      0.0053867      0.0046681
   0.0032962      0.0020713      0.0011217      0.0009722      0.0005817
   0.0002826      0.0001407      0.0000679


*****   symmetry block SYM2   *****

 occupation numbers of nos
   1.9444752      0.0498073      0.0054970      0.0043517      0.0018861
   0.0007157      0.0005366


*****   symmetry block SYM3   *****

 occupation numbers of nos
   1.9764485      1.9684582      0.0214618      0.0093321      0.0055516
   0.0025071      0.0013953      0.0008363      0.0004148      0.0001484


*****   symmetry block SYM4   *****

 occupation numbers of nos
   0.0046952      0.0031253      0.0005385


 total number of electrons =   16.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = "99999992"

 Work memory size (LMWORK) :    99999992 =  762.94 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***

  Confirmation of dielectric energy for ground state
  edielnew =  -0.0020670629


 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************

 *** qcos is copied to qcosdalton ***

 fepsi =   1.
 *** qcosdalton is mult by fepsi ***

 root =  1


 DALTON: user specified work memory size used,
          -m = "99999992"

 Work memory size (LMWORK) :    99999992 =  762.94 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 Warning! Large contrib. to nucrep.The distance is   0.017707087
 Warning! Large contrib. to nucrep.The distance is   0.000353311222
 ** Total nuclear repulsion energy ** =  31.22717809883000
 Warning! Large contrib. to nucrep.The distance is   0.017707087
 Warning! Large contrib. to nucrep.The distance is   0.000353311222
 screening nuclear repulsion energy   0.00980862841

 Total-screening nuclear repulsion energy   31.2173695


 Adding T+Vsolv ...
 maxdens  260
 *** End of DALTON-COSMO calculation ***

 DIAGEL COUNTS: DG0X,DG2X,DG4X= 8597 1713 203
 =========== Executing IN-CORE method ==========
 norm multnew:  1.


====================================================================================================
Diagonal     counts:  0x:      8597 2x:      1713 4x:       203
All internal counts: zz :       157 yy:      1315 xx:      1079 ww:       968
One-external counts: yz :       811 yx:      4545 yw:      4311
Two-external counts: yy :      1362 ww:       922 xx:      1105 xz:        70 wz:        98 wx:       990
Three-ext.   counts: yx :       670 yw:       670

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        59    task #   2:        82    task #   3:       704    task #   4:       756
task #   5:      4069    task #   6:      3725    task #   7:       156    task #   8:       972
task #   9:       877    task #  10:       671    task #  11:       342    task #  12:         1
task #  13:         1    task #  14:         1    task #
 =========== Executing IN-CORE method ==========
 norm multnew:  2.7399109E-06


====================================================================================================
Diagonal     counts:  0x:      8597 2x:      1713 4x:       203
All internal counts: zz :       157 yy:      1315 xx:      1079 ww:       968
One-external counts: yz :       811 yx:      4545 yw:      4311
Two-external counts: yy :      1362 ww:       922 xx:      1105 xz:        70 wz:        98 wx:       990
Three-ext.   counts: yx :       670 yw:       670

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        59    task #   2:        82    task #   3:       704    task #   4:       756
task #   5:      4069    task #   6:      3725    task #   7:       156    task #   8:       972
task #   9:       877    task #  10:       671    task #  11:       342    task #  12:         1
task #  13:         1    task #  14:         1    task #
 rtolcosmo = eo,e,r,ro   0. -145.417983 -145.417577  31.2173695  31.2177297
 rtolcosmo = eo,e,r,ro   0. -143.596036 -143.463884  31.2173695  31.2177297
 calctciref: ... reading 
 cirefv                                                       recamt= 12

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.95172789    -0.25434849

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 10  1   -114.2002078946 -4.5746E-05  5.7782E-07  1.1368E-03  1.0000E-03
 mr-sdci # 10  2   -112.2465141797 -1.3179E-01  0.0000E+00  1.8441E+00  1.0000E-04


dielectric energy =      -0.0082447584
delta diel. energy =      -0.0000470565
total Cosmo energy: e(nroot) + repnuc - ediel =    -114.1919631362
delta Cosmo energy =       0.0000013100

 root number  1 is used to define the new expansion vector.
 diagon: frcsub=2, new matrix-vector products will be computed.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.00   0.00   0.00   0.00         0.    0.0000
    2   25    0   0.00   0.00   0.00   0.00         0.    0.0000
    3   26    0   0.02   0.00   0.00   0.02         0.    0.0000
    4   11    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   15    0   0.02   0.00   0.00   0.02         0.    0.0000
    6   16    0   0.01   0.01   0.00   0.01         0.    0.0000
    7    1    0   0.00   0.00   0.00   0.00         0.    0.0000
    8    5    0   0.01   0.00   0.00   0.01         0.    0.0000
    9    6    0   0.02   0.01   0.00   0.02         0.    0.0000
   10    7    0   0.01   0.00   0.00   0.01         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.01   0.00   0.00   0.01         0.    0.0000
   14   47    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1100s 
time spent in multnx:                   0.1100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0200s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.9700s 

          starting ci iteration  11

 cosmocalc =  1
 rootcalc =  1
 repnuc =   31.2173695

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=      51  DYX=     285  DYW=     277
   D0Z=      13  D0Y=     115  D0X=      92  D0W=      90
  DDZI=     101 DDYI=     626 DDXI=     550 DDWI=     537
  DDZE=       0 DDYE=      72 DDXE=      65 DDWE=      66
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   2.0000000      2.0000000      1.9872390      1.9760017      1.9707728
   0.0207805      0.0156967      0.0086845      0.0054015      0.0046819
   0.0033050      0.0020770      0.0011250      0.0009746      0.0005835
   0.0002834      0.0001410      0.0000681


*****   symmetry block SYM2   *****

 occupation numbers of nos
   1.9442006      0.0500565      0.0055136      0.0043616      0.0018922
   0.0007180      0.0005382


*****   symmetry block SYM3   *****

 occupation numbers of nos
   1.9763669      1.9683642      0.0215474      0.0093601      0.0055672
   0.0025138      0.0013990      0.0008386      0.0004159      0.0001489


*****   symmetry block SYM4   *****

 occupation numbers of nos
   0.0047076      0.0031332      0.0005401


 total number of electrons =   16.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = "99999992"

 Work memory size (LMWORK) :    99999992 =  762.94 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***

  Confirmation of dielectric energy for ground state
  edielnew =  -0.00205827915


 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************

 *** qcos is copied to qcosdalton ***

 fepsi =   1.
 *** qcosdalton is mult by fepsi ***

 root =  1


 DALTON: user specified work memory size used,
          -m = "99999992"

 Work memory size (LMWORK) :    99999992 =  762.94 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 Warning! Large contrib. to nucrep.The distance is   0.017707087
 Warning! Large contrib. to nucrep.The distance is   0.000353311222
 ** Total nuclear repulsion energy ** =  31.22698305302564
 Warning! Large contrib. to nucrep.The distance is   0.017707087
 Warning! Large contrib. to nucrep.The distance is   0.000353311222
 screening nuclear repulsion energy   0.00978265272

 Total-screening nuclear repulsion energy   31.2172004


 Adding T+Vsolv ...
 maxdens  260
 *** End of DALTON-COSMO calculation ***

 DIAGEL COUNTS: DG0X,DG2X,DG4X= 8597 1713 203
 =========== Executing IN-CORE method ==========
 norm multnew:  1.


====================================================================================================
Diagonal     counts:  0x:      8597 2x:      1713 4x:       203
All internal counts: zz :       157 yy:      1315 xx:      1079 ww:       968
One-external counts: yz :       811 yx:      4545 yw:      4311
Two-external counts: yy :      1362 ww:       922 xx:      1105 xz:        70 wz:        98 wx:       990
Three-ext.   counts: yx :       670 yw:       670

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        59    task #   2:        82    task #   3:       704    task #   4:       756
task #   5:      4069    task #   6:      3725    task #   7:       156    task #   8:       972
task #   9:       877    task #  10:       671    task #  11:       342    task #  12:         1
task #  13:         1    task #  14:         1    task #
 =========== Executing IN-CORE method ==========
 norm multnew:  5.56877983E-07


====================================================================================================
Diagonal     counts:  0x:      8597 2x:      1713 4x:       203
All internal counts: zz :       157 yy:      1315 xx:      1079 ww:       968
One-external counts: yz :       811 yx:      4545 yw:      4311
Two-external counts: yy :      1362 ww:       922 xx:      1105 xz:        70 wz:        98 wx:       990
Three-ext.   counts: yx :       670 yw:       670

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        59    task #   2:        82    task #   3:       704    task #   4:       756
task #   5:      4069    task #   6:      3725    task #   7:       156    task #   8:       972
task #   9:       877    task #  10:       671    task #  11:       342    task #  12:         1
task #  13:         1    task #  14:         1    task #
 rtolcosmo = eo,e,r,ro   0. -145.417577 -145.417386  31.2172004  31.2173695
 rtolcosmo = eo,e,r,ro   0. -143.463884 -143.700772  31.2172004  31.2173695
 calctciref: ... reading 
 cirefv                                                       recamt= 12

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.95178063     0.09464734

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 11  1   -114.2001851758 -2.2719E-05  3.3270E-07  7.4409E-04  1.0000E-03
 mr-sdci # 11  2   -112.4835714175  2.3706E-01  0.0000E+00  1.8317E+00  1.0000E-04


dielectric energy =      -0.0082214921
delta diel. energy =      -0.0000232663
total Cosmo energy: e(nroot) + repnuc - ediel =    -114.1919636837
delta Cosmo energy =       0.0000005475


 mr-sdci  convergence criteria satisfied after 11 iterations.

 *************************************************
 ***Final Calc of density matrix for cosmo calc***
 *************************************************

 entering drivercid: firstcall,firstnonref= F T
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=      51  DYX=     285  DYW=     277
   D0Z=      13  D0Y=     115  D0X=      92  D0W=      90
  DDZI=     101 DDYI=     626 DDXI=     550 DDWI=     537
  DDZE=       0 DDYE=      72 DDXE=      65 DDWE=      66
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   2.0000000      2.0000000      1.9872589      1.9760446      1.9708311
   0.0207417      0.0156779      0.0086645      0.0053907      0.0046743
   0.0032989      0.0020737      0.0011234      0.0009729      0.0005828
   0.0002830      0.0001408      0.0000680


*****   symmetry block SYM2   *****

 occupation numbers of nos
   1.9442650      0.0499941      0.0055023      0.0043528      0.0018900
   0.0007173      0.0005375


*****   symmetry block SYM3   *****

 occupation numbers of nos
   1.9764139      1.9684162      0.0215121      0.0093415      0.0055563
   0.0025095      0.0013970      0.0008374      0.0004153      0.0001487


*****   symmetry block SYM4   *****

 occupation numbers of nos
   0.0046993      0.0031274      0.0005394


 total number of electrons =   16.0000000000


 root =  1


 DALTON: user specified work memory size used,
          -m = "99999992"

 Work memory size (LMWORK) :    99999992 =  762.94 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***


 **************************************************
 ***Final cosmo calculation for ground state*******
 **************************************************

  ediel before cosmo(4 =  -0.00205537302
  elast before cosmo(4 =  -114.191964


 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 11  1   -114.2001851758 -2.2719E-05  3.3270E-07  7.4409E-04  1.0000E-03

####################CIUDGINFO####################

   ci vector at position   1 energy= -114.200185175818

################END OF CIUDGINFO################


    1 of the   2 expansion vectors are transformed.
    1 of the   1 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
 maximum overlap with reference  1(overlap=  0.951780635)

information on vector: 1from unit 11 written to unit 16filename civout                                                      


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =      -114.2001851758

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7    8    9

                                          orbital     1    2    3    4    5   19   26   20   27

                                         symmetry   a1   a1   a1   a1   a1   b1   b2   b1   b2 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1 -0.017361                        +-   +-   +-   +-   +-   +-   +-   +-      
 z*  1  1       2  0.951646                        +-   +-   +-   +-   +-   +-   +-        +- 
 z   1  1       5 -0.072168                        +-   +-   +-   +-   +-   +    +-    -   +- 
 z   1  1       6 -0.104660                        +-   +-   +-   +-   +-        +-   +-   +- 
 z   1  1       7 -0.014070                        +-   +-   +-   +-        +-   +-   +-   +- 
 z   1  1       9 -0.029918                        +-   +-   +-        +-   +-   +-   +-   +- 
 y   1  1      19  0.012314              2( b2 )   +-   +-   +-   +-   +-   +-   +-         - 
 y   1  1      39 -0.014033              1( b2 )   +-   +-   +-   +-   +-   +-    -        +- 
 y   1  1      41  0.011631              3( b2 )   +-   +-   +-   +-   +-   +-    -        +- 
 y   1  1      70  0.011527              1( b2 )   +-   +-   +-   +-   +-    -   +-   +     - 
 y   1  1      71  0.016856              2( b2 )   +-   +-   +-   +-   +-    -   +-   +     - 
 y   1  1      78  0.019770              1( b1 )   +-   +-   +-   +-   +-    -   +-        +- 
 y   1  1      96  0.024674              1( b2 )   +-   +-   +-   +-   +-   +    +-    -    - 
 y   1  1      97  0.039391              2( b2 )   +-   +-   +-   +-   +-   +    +-    -    - 
 y   1  1      98 -0.015647              3( b2 )   +-   +-   +-   +-   +-   +    +-    -    - 
 y   1  1      99 -0.021455              4( b2 )   +-   +-   +-   +-   +-   +    +-    -    - 
 y   1  1     101 -0.019171              6( b2 )   +-   +-   +-   +-   +-   +    +-    -    - 
 y   1  1     109 -0.020586              1( b2 )   +-   +-   +-   +-   +-   +     -    -   +- 
 y   1  1     111  0.012027              3( b2 )   +-   +-   +-   +-   +-   +     -    -   +- 
 y   1  1     112  0.011906              4( b2 )   +-   +-   +-   +-   +-   +     -    -   +- 
 y   1  1     125  0.017479              1( b1 )   +-   +-   +-   +-   +-        +-    -   +- 
 y   1  1     126 -0.028962              2( b1 )   +-   +-   +-   +-   +-        +-    -   +- 
 y   1  1     127  0.010829              3( b1 )   +-   +-   +-   +-   +-        +-    -   +- 
 y   1  1     149  0.019776              4( a1 )   +-   +-   +-   +-    -   +-   +-        +- 
 y   1  1     151  0.011082              6( a1 )   +-   +-   +-   +-    -   +-   +-        +- 
 y   1  1     179 -0.020056              2( a1 )   +-   +-   +-   +-    -   +    +-    -   +- 
 y   1  1     181  0.046802              4( a1 )   +-   +-   +-   +-    -   +    +-    -   +- 
 y   1  1     182  0.023934              5( a1 )   +-   +-   +-   +-    -   +    +-    -   +- 
 y   1  1     183  0.014980              6( a1 )   +-   +-   +-   +-    -   +    +-    -   +- 
 y   1  1     185  0.010098              8( a1 )   +-   +-   +-   +-    -   +    +-    -   +- 
 y   1  1     186 -0.012799              9( a1 )   +-   +-   +-   +-    -   +    +-    -   +- 
 y   1  1     255 -0.014143              1( a1 )   +-   +-   +-    -   +-   +-   +-        +- 
 y   1  1     259 -0.013787              5( a1 )   +-   +-   +-    -   +-   +-   +-        +- 
 y   1  1     287 -0.018533              1( a1 )   +-   +-   +-    -   +-   +    +-    -   +- 
 y   1  1     288 -0.010839              2( a1 )   +-   +-   +-    -   +-   +    +-    -   +- 
 y   1  1     291 -0.017071              5( a1 )   +-   +-   +-    -   +-   +    +-    -   +- 
 y   1  1     315 -0.012738              3( a2 )   +-   +-   +-   +    +-   +-   +-    -    - 
 y   1  1     329  0.029371              1( a2 )   +-   +-   +-   +    +-   +-    -    -   +- 
 y   1  1     335 -0.015783              1( a1 )   +-   +-   +-   +    +-    -   +-    -   +- 
 y   1  1     336  0.010514              2( a1 )   +-   +-   +-   +    +-    -   +-    -   +- 
 y   1  1     337  0.022580              3( a1 )   +-   +-   +-   +    +-    -   +-    -   +- 
 y   1  1     340 -0.011701              6( a1 )   +-   +-   +-   +    +-    -   +-    -   +- 
 y   1  1     341 -0.015350              7( a1 )   +-   +-   +-   +    +-    -   +-    -   +- 
 y   1  1     358 -0.024443              3( b1 )   +-   +-   +-   +     -   +-   +-    -   +- 
 y   1  1     369  0.010345              1( b1 )   +-   +-   +-        +-   +-   +-    -   +- 
 y   1  1     391 -0.013534              2( a1 )   +-   +-    -   +-   +-   +-   +-        +- 
 y   1  1     423 -0.022781              2( a1 )   +-   +-    -   +-   +-   +    +-    -   +- 
 y   1  1     425  0.022381              4( a1 )   +-   +-    -   +-   +-   +    +-    -   +- 
 y   1  1     429  0.013770              8( a1 )   +-   +-    -   +-   +-   +    +-    -   +- 
 y   1  1     463 -0.016700              3( a2 )   +-   +-   +    +-   +-   +-   +-    -    - 
 y   1  1     484 -0.015607              2( a1 )   +-   +-   +    +-   +-    -   +-    -   +- 
 y   1  1     486  0.017460              4( a1 )   +-   +-   +    +-   +-    -   +-    -   +- 
 y   1  1     494 -0.014009             12( a1 )   +-   +-   +    +-   +-    -   +-    -   +- 
 y   1  1     508  0.012762              5( b1 )   +-   +-   +    +-    -   +-   +-    -   +- 
 x   1  1     937 -0.011228    2( b1 )   2( b2 )   +-   +-   +-   +-   +-    -   +-         - 
 x   1  1     947  0.021497    2( b1 )   4( b2 )   +-   +-   +-   +-   +-    -   +-         - 
 x   1  1    1007  0.012144   11( a1 )   3( a2 )   +-   +-   +-   +-   +-    -   +-         - 
 x   1  1    1008  0.014801   12( a1 )   3( a2 )   +-   +-   +-   +-   +-    -   +-         - 
 x   1  1    1208 -0.015858    1( b1 )   1( b2 )   +-   +-   +-   +-   +-    -    -        +- 
 x   1  1    1218  0.016406    1( b1 )   3( b2 )   +-   +-   +-   +-   +-    -    -        +- 
 x   1  1    1250  0.018587    3( a1 )   1( a2 )   +-   +-   +-   +-   +-    -    -        +- 
 x   1  1    1254 -0.012022    7( a1 )   1( a2 )   +-   +-   +-   +-   +-    -    -        +- 
 x   1  1    1709  0.015850    4( a1 )   2( b2 )   +-   +-   +-   +-    -   +-   +-         - 
 x   1  1    1737 -0.019022    6( a1 )   4( b2 )   +-   +-   +-   +-    -   +-   +-         - 
 x   1  1    1761 -0.013780    4( a1 )   6( b2 )   +-   +-   +-   +-    -   +-   +-         - 
 x   1  1    2023 -0.013572    4( a1 )   1( b2 )   +-   +-   +-   +-    -   +-    -        +- 
 x   1  1    2051 -0.010475    6( a1 )   3( b2 )   +-   +-   +-   +-    -   +-    -        +- 
 x   1  1    2439  0.014263    4( a1 )   1( b1 )   +-   +-   +-   +-    -    -   +-        +- 
 x   1  1    2454 -0.015261    6( a1 )   2( b1 )   +-   +-   +-   +-    -    -   +-        +- 
 x   1  1    3064  0.010080    6( a1 )   4( b2 )   +-   +-   +-    -   +-   +-   +-         - 
 x   1  1    3347  0.010722    1( a1 )   1( b2 )   +-   +-   +-    -   +-   +-    -        +- 
 x   1  1    3763 -0.010591    1( a1 )   1( b1 )   +-   +-   +-    -   +-    -   +-        +- 
 x   1  1    4862  0.010109    2( a1 )   4( b2 )   +-   +-    -   +-   +-   +-   +-         - 
 w   1  1    7015 -0.011672    3( a1 )   3( a1 )   +-   +-   +-   +-   +-   +-   +-           
 w   1  1    7116 -0.011611    1( b2 )   1( b2 )   +-   +-   +-   +-   +-   +-   +-           
 w   1  1    7117 -0.012186    1( b2 )   2( b2 )   +-   +-   +-   +-   +-   +-   +-           
 w   1  1    7118 -0.017193    2( b2 )   2( b2 )   +-   +-   +-   +-   +-   +-   +-           
 w   1  1    7120  0.010781    2( b2 )   3( b2 )   +-   +-   +-   +-   +-   +-   +-           
 w   1  1    7122  0.010360    1( b2 )   4( b2 )   +-   +-   +-   +-   +-   +-   +-           
 w   1  1    7123  0.018449    2( b2 )   4( b2 )   +-   +-   +-   +-   +-   +-   +-           
 w   1  1    7125 -0.026773    4( b2 )   4( b2 )   +-   +-   +-   +-   +-   +-   +-           
 w   1  1    7157 -0.012389    3( a2 )   3( a2 )   +-   +-   +-   +-   +-   +-   +-           
 w   1  1    7237 -0.011745    1( a1 )   1( a1 )   +-   +-   +-   +-   +-   +-   +          - 
 w   1  1    7240  0.013995    1( a1 )   3( a1 )   +-   +-   +-   +-   +-   +-   +          - 
 w   1  1    7242 -0.024007    3( a1 )   3( a1 )   +-   +-   +-   +-   +-   +-   +          - 
 w   1  1    7343 -0.011795    1( b2 )   1( b2 )   +-   +-   +-   +-   +-   +-   +          - 
 w   1  1    7346  0.010103    1( b2 )   3( b2 )   +-   +-   +-   +-   +-   +-   +          - 
 w   1  1    7348 -0.011379    3( b2 )   3( b2 )   +-   +-   +-   +-   +-   +-   +          - 
 w   1  1    7350 -0.012069    2( b2 )   4( b2 )   +-   +-   +-   +-   +-   +-   +          - 
 w   1  1    7352  0.010689    4( b2 )   4( b2 )   +-   +-   +-   +-   +-   +-   +          - 
 w   1  1    7612 -0.011506    1( a1 )   1( a1 )   +-   +-   +-   +-   +-   +-             +- 
 w   1  1    7615  0.012214    1( a1 )   3( a1 )   +-   +-   +-   +-   +-   +-             +- 
 w   1  1    7617 -0.025042    3( a1 )   3( a1 )   +-   +-   +-   +-   +-   +-             +- 
 w   1  1    7718 -0.018758    1( b2 )   1( b2 )   +-   +-   +-   +-   +-   +-             +- 
 w   1  1    7720 -0.013573    2( b2 )   2( b2 )   +-   +-   +-   +-   +-   +-             +- 
 w   1  1    7721  0.024550    1( b2 )   3( b2 )   +-   +-   +-   +-   +-   +-             +- 
 w   1  1    7723 -0.026651    3( b2 )   3( b2 )   +-   +-   +-   +-   +-   +-             +- 
 w   1  1    7754 -0.017834    1( a2 )   1( a2 )   +-   +-   +-   +-   +-   +-             +- 
 w   1  1    7914  0.012701    2( b1 )   2( b2 )   +-   +-   +-   +-   +-   +    +-         - 
 w   1  1    7924 -0.021003    2( b1 )   4( b2 )   +-   +-   +-   +-   +-   +    +-         - 
 w   1  1    8214  0.013699    1( b1 )   1( b2 )   +-   +-   +-   +-   +-   +     -        +- 
 w   1  1    8224 -0.015042    1( b1 )   3( b2 )   +-   +-   +-   +-   +-   +     -        +- 
 w   1  1    8255 -0.010081    2( a1 )   1( a2 )   +-   +-   +-   +-   +-   +     -        +- 
 w   1  1    8759 -0.011804    1( b1 )   1( b1 )   +-   +-   +-   +-   +-        +-        +- 
 w   1  1    8761 -0.020207    2( b1 )   2( b1 )   +-   +-   +-   +-   +-        +-        +- 
 w   1  1    8905 -0.010324    1( a1 )   1( b2 )   +-   +-   +-   +-   +    +-   +-         - 
 w   1  1    8922 -0.010520    5( a1 )   2( b2 )   +-   +-   +-   +-   +    +-   +-         - 
 w   1  1    8923 -0.010636    6( a1 )   2( b2 )   +-   +-   +-   +-   +    +-   +-         - 
 w   1  1    8933 -0.010561    3( a1 )   3( b2 )   +-   +-   +-   +-   +    +-   +-         - 
 w   1  1    8948  0.010254    5( a1 )   4( b2 )   +-   +-   +-   +-   +    +-   +-         - 
 w   1  1    8949  0.016876    6( a1 )   4( b2 )   +-   +-   +-   +-   +    +-   +-         - 
 w   1  1    9232 -0.012449    1( a1 )   1( b2 )   +-   +-   +-   +-   +    +-    -        +- 
 w   1  1    9235  0.013550    4( a1 )   1( b2 )   +-   +-   +-   +-   +    +-    -        +- 
 w   1  1    9247  0.010465    3( a1 )   2( b2 )   +-   +-   +-   +-   +    +-    -        +- 
 w   1  1    9258  0.012189    1( a1 )   3( b2 )   +-   +-   +-   +-   +    +-    -        +- 
 w   1  1    9260 -0.015616    3( a1 )   3( b2 )   +-   +-   +-   +-   +    +-    -        +- 
 w   1  1    9261 -0.015035    4( a1 )   3( b2 )   +-   +-   +-   +-   +    +-    -        +- 
 w   1  1    9338  0.012614    3( b1 )   1( a2 )   +-   +-   +-   +-   +    +-    -        +- 
 w   1  1    9651 -0.011664    4( a1 )   1( b1 )   +-   +-   +-   +-   +     -   +-        +- 
 w   1  1    9666  0.015459    6( a1 )   2( b1 )   +-   +-   +-   +-   +     -   +-        +- 
 w   1  1   10092 -0.024637    4( a1 )   4( a1 )   +-   +-   +-   +-        +-   +-        +- 
 w   1  1   10097 -0.010067    5( a1 )   5( a1 )   +-   +-   +-   +-        +-   +-        +- 
 w   1  1   10103 -0.017996    6( a1 )   6( a1 )   +-   +-   +-   +-        +-   +-        +- 
 w   1  1   10122  0.010154    4( a1 )   9( a1 )   +-   +-   +-   +-        +-   +-        +- 
 w   1  1   10127 -0.011581    9( a1 )   9( a1 )   +-   +-   +-   +-        +-   +-        +- 
 w   1  1   10320 -0.010003    1( a1 )   1( b2 )   +-   +-   +-   +    +-   +-   +-         - 
 w   1  1   10335  0.011352    3( a1 )   2( b2 )   +-   +-   +-   +    +-   +-   +-         - 
 w   1  1   10348 -0.013336    3( a1 )   3( b2 )   +-   +-   +-   +    +-   +-   +-         - 
 w   1  1   10364 -0.011371    6( a1 )   4( b2 )   +-   +-   +-   +    +-   +-   +-         - 
 w   1  1   10647 -0.017888    1( a1 )   1( b2 )   +-   +-   +-   +    +-   +-    -        +- 
 w   1  1   10651 -0.017898    5( a1 )   1( b2 )   +-   +-   +-   +    +-   +-    -        +- 
 w   1  1   10662  0.011661    3( a1 )   2( b2 )   +-   +-   +-   +    +-   +-    -        +- 
 w   1  1   10673  0.019607    1( a1 )   3( b2 )   +-   +-   +-   +    +-   +-    -        +- 
 w   1  1   10674 -0.010099    2( a1 )   3( b2 )   +-   +-   +-   +    +-   +-    -        +- 
 w   1  1   10675 -0.017502    3( a1 )   3( b2 )   +-   +-   +-   +    +-   +-    -        +- 
 w   1  1   10677  0.022738    5( a1 )   3( b2 )   +-   +-   +-   +    +-   +-    -        +- 
 w   1  1   10727 -0.010147    3( a1 )   7( b2 )   +-   +-   +-   +    +-   +-    -        +- 
 w   1  1   10753  0.012192    3( b1 )   1( a2 )   +-   +-   +-   +    +-   +-    -        +- 
 w   1  1   11063  0.013877    1( a1 )   1( b1 )   +-   +-   +-   +    +-    -   +-        +- 
 w   1  1   11067  0.014190    5( a1 )   1( b1 )   +-   +-   +-   +    +-    -   +-        +- 
 w   1  1   11498 -0.012912    1( a1 )   1( a1 )   +-   +-   +-   +     -   +-   +-        +- 
 w   1  1   11503 -0.012172    3( a1 )   3( a1 )   +-   +-   +-   +     -   +-   +-        +- 
 w   1  1   11504  0.019983    1( a1 )   4( a1 )   +-   +-   +-   +     -   +-   +-        +- 
 w   1  1   11511  0.017366    4( a1 )   5( a1 )   +-   +-   +-   +     -   +-   +-        +- 
 w   1  1   11518  0.010996    6( a1 )   6( a1 )   +-   +-   +-   +     -   +-   +-        +- 
 w   1  1   11537 -0.010735    4( a1 )   9( a1 )   +-   +-   +-   +     -   +-   +-        +- 
 w   1  1   11538 -0.010988    5( a1 )   9( a1 )   +-   +-   +-   +     -   +-   +-        +- 
 w   1  1   11539 -0.011273    6( a1 )   9( a1 )   +-   +-   +-   +     -   +-   +-        +- 
 w   1  1   11952 -0.013144    1( a1 )   1( a1 )   +-   +-   +-        +-   +-   +-        +- 
 w   1  1   11957 -0.013249    3( a1 )   3( a1 )   +-   +-   +-        +-   +-   +-        +- 
 w   1  1   11961 -0.012283    4( a1 )   4( a1 )   +-   +-   +-        +-   +-   +-        +- 
 w   1  1   11962 -0.013243    1( a1 )   5( a1 )   +-   +-   +-        +-   +-   +-        +- 
 w   1  1   11966 -0.012571    5( a1 )   5( a1 )   +-   +-   +-        +-   +-   +-        +- 
 w   1  1   12094 -0.010014    1( a2 )   1( a2 )   +-   +-   +-        +-   +-   +-        +- 
 w   1  1   12203  0.010145    2( a1 )   2( b2 )   +-   +-   +    +-   +-   +-   +-         - 
 w   1  1   12229 -0.016467    2( a1 )   4( b2 )   +-   +-   +    +-   +-   +-   +-         - 
 w   1  1   12231  0.011054    4( a1 )   4( b2 )   +-   +-   +    +-   +-   +-   +-         - 
 w   1  1   12235  0.010533    8( a1 )   4( b2 )   +-   +-   +    +-   +-   +-   +-         - 
 w   1  1   12238  0.010085   11( a1 )   4( b2 )   +-   +-   +    +-   +-   +-   +-         - 
 w   1  1   12946 -0.016581    2( a1 )   2( b1 )   +-   +-   +    +-   +-    -   +-        +- 
 w   1  1   12948  0.010515    4( a1 )   2( b1 )   +-   +-   +    +-   +-    -   +-        +- 
 w   1  1   12952  0.011364    8( a1 )   2( b1 )   +-   +-   +    +-   +-    -   +-        +- 
 w   1  1   13376 -0.011201    4( a1 )   4( a1 )   +-   +-   +    +-    -   +-   +-        +- 
 w   1  1   13383  0.010113    2( a1 )   6( a1 )   +-   +-   +    +-    -   +-   +-        +- 
 w   1  1   13398 -0.011078    4( a1 )   8( a1 )   +-   +-   +    +-    -   +-   +-        +- 
 w   1  1   13408 -0.012607    6( a1 )   9( a1 )   +-   +-   +    +-    -   +-   +-        +- 
 w   1  1   14284 -0.010847    4( a1 )   4( a1 )   +-   +-        +-   +-   +-   +-        +- 

 ci coefficient statistics:
           rq > 0.1                2
      0.1> rq > 0.01             165
     0.01> rq > 0.001           2765
    0.001> rq > 0.0001          4185
   0.0001> rq > 0.00001         5859
  0.00001> rq > 0.000001        1289
 0.000001> rq                    157
           all                 14422
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       477 2x:         0 4x:         0
All internal counts: zz :       157 yy:         0 xx:         0 ww:         0
One-external counts: yz :         0 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         0    task #   2:         0    task #   3:         0    task #   4:         0
task #   5:         0    task #   6:         0    task #   7:       156    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:       342    task #  12:         0
task #  13:         0    task #  14:         0    task #
  iref  icsf         v(icsf)             hv(icsf)
     1     1     -0.017360812897      1.979788317192
     2     2      0.951645607023   -108.378709593090

 number of reference csfs (nref) is     2.  root number (iroot) is  1.

 pople ci energy extrapolation is computed with 16 correlated electrons.

 eref      =   -113.885628197201   "relaxed" cnot**2         =   0.905930759191
 eci       =   -114.200185175818   deltae = eci - eref       =  -0.314556978617
 eci+dv1   =   -114.229775311987   dv1 = (1-cnot**2)*deltae  =  -0.029590136170
 eci+dv2   =   -114.232847866503   dv2 = dv1 / cnot**2       =  -0.032662690685
 eci+dv3   =   -114.236632446072   dv3 = dv1 / (2*cnot**2-1) =  -0.036447270254
 eci+pople =   -114.231135875882   ( 16e- scaled deltae )    =  -0.345507678682
NO coefficients and occupation numbers are written to nocoef_ci.1
 entering drivercid: firstcall,firstnonref= F T
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=      51  DYX=     285  DYW=     277
   D0Z=      13  D0Y=     115  D0X=      92  D0W=      90
  DDZI=     101 DDYI=     626 DDXI=     550 DDWI=     537
  DDZE=       0 DDYE=      72 DDXE=      65 DDWE=      66
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   2.0000000      2.0000000      1.9872589      1.9760446      1.9708311
   0.0207417      0.0156779      0.0086645      0.0053907      0.0046743
   0.0032989      0.0020737      0.0011234      0.0009729      0.0005828
   0.0002830      0.0001408      0.0000680


*****   symmetry block SYM2   *****

 occupation numbers of nos
   1.9442650      0.0499941      0.0055023      0.0043528      0.0018900
   0.0007173      0.0005375


*****   symmetry block SYM3   *****

 occupation numbers of nos
   1.9764139      1.9684162      0.0215121      0.0093415      0.0055563
   0.0025095      0.0013970      0.0008374      0.0004153      0.0001487


*****   symmetry block SYM4   *****

 occupation numbers of nos
   0.0046993      0.0031274      0.0005394


 total number of electrons =   16.0000000000



          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!


                        A1  partial gross atomic populations
   ao class       1A1        2A1        3A1        4A1        5A1        6A1 
    O1_ s      -0.000242   1.999660   1.646011   0.107650  -0.000242   0.001069
    O1_ p      -0.000420   0.000003  -0.009919   0.511637   0.871900   0.007478
    O1_ d       0.000005   0.000000   0.000692   0.003033   0.005219   0.000339
    C1_ s       2.000410   0.000255   0.219590   0.862869   0.046010   0.005935
    C1_ p       0.000002   0.000237   0.062852   0.059608   0.713674   0.003482
    C1_ d       0.000001  -0.000155   0.018506  -0.000092   0.013569   0.000594
    H1_ s       0.000410   0.000000   0.046232   0.407160   0.310884   0.001721
    H1_ p      -0.000166   0.000000   0.003295   0.024179   0.009817   0.000123

   ao class       7A1        8A1        9A1       10A1       11A1       12A1 
    O1_ s       0.000133   0.005371   0.000313  -0.000005   0.000008   0.000212
    O1_ p       0.000343   0.002394   0.001005   0.000288   0.000037   0.000159
    O1_ d       0.000125   0.000012   0.001865   0.000691   0.002577   0.000291
    C1_ s       0.001967   0.000350   0.000100   0.001059   0.000176   0.000170
    C1_ p       0.005649   0.000270   0.000529   0.000248   0.000110   0.000030
    C1_ d       0.000270   0.000033   0.001285   0.001848   0.000136   0.000713
    H1_ s       0.007158   0.000224  -0.000001   0.000189   0.000109   0.000216
    H1_ p       0.000033   0.000011   0.000295   0.000356   0.000145   0.000282

   ao class      13A1       14A1       15A1       16A1       17A1       18A1 
    O1_ s       0.000013   0.000052   0.000000   0.000009   0.000025   0.000000
    O1_ p       0.000107   0.000142   0.000001   0.000006   0.000003   0.000002
    O1_ d       0.000173   0.000359   0.000002   0.000006   0.000000   0.000000
    C1_ s       0.000201   0.000016   0.000091   0.000013   0.000010   0.000009
    C1_ p       0.000010   0.000198   0.000028   0.000002   0.000079  -0.000001
    C1_ d       0.000520   0.000142   0.000033   0.000064   0.000001   0.000001
    H1_ s       0.000118   0.000074   0.000028  -0.000003   0.000004   0.000042
    H1_ p      -0.000020  -0.000012   0.000399   0.000186   0.000017   0.000015

                        B1  partial gross atomic populations
   ao class       1B1        2B1        3B1        4B1        5B1        6B1 
    O1_ p       1.254534   0.020480   0.002098   0.001188   0.000029   0.000197
    O1_ d       0.005995   0.000254   0.001328   0.000596   0.000842   0.000086
    C1_ p       0.650492   0.028799   0.001502   0.000225   0.000610   0.000296
    C1_ d       0.028433  -0.000164   0.000525   0.001906   0.000105   0.000112
    H1_ p       0.004812   0.000625   0.000049   0.000437   0.000304   0.000025

   ao class       7B1 
    O1_ p       0.000000
    O1_ d       0.000026
    C1_ p       0.000016
    C1_ d       0.000131
    H1_ p       0.000365

                        B2  partial gross atomic populations
   ao class       1B2        2B2        3B2        4B2        5B2        6B2 
    O1_ p       1.322473   0.503407   0.006911   0.004886   0.000755   0.000051
    O1_ d       0.004054  -0.000072   0.000294   0.000022   0.001859   0.001120
    C1_ p       0.502100   0.471780   0.006587   0.003344   0.000476   0.000328
    C1_ d       0.017295   0.055161   0.001186   0.000414   0.002113   0.000024
    H1_ s       0.123617   0.932523   0.006187   0.000641   0.000200   0.000504
    H1_ p       0.006873   0.005617   0.000348   0.000034   0.000153   0.000483

   ao class       7B2        8B2        9B2       10B2 
    O1_ p       0.000008  -0.000003   0.000025   0.000002
    O1_ d       0.000004   0.000086   0.000040   0.000000
    C1_ p       0.000028   0.000332   0.000077   0.000004
    C1_ d       0.000065   0.000285   0.000024   0.000004
    H1_ s       0.000491   0.000017   0.000057   0.000054
    H1_ p       0.000802   0.000121   0.000193   0.000085

                        A2  partial gross atomic populations
   ao class       1A2        2A2        3A2 
    O1_ d       0.000974   0.002376   0.000018
    C1_ d       0.002903   0.000263   0.000161
    H1_ p       0.000822   0.000489   0.000361


                        gross atomic populations
     ao           O1_        C1_        H1_
      s         3.760039   3.139230   1.838856
      p         4.502210   2.514002   0.061953
      d         0.035293   0.148417   0.000000
    total       8.297541   5.801649   1.900810


 Total number of electrons:   16.00000000

