 
 program cidrt 5.9  

 distinct row table construction, reference csf selection, and internal
 walk selection for multireference single- and double-excitation
configuration interaction.

 references:  r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).
              h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. symp. 15, 91 (1981).

 based on the initial version by  Ron Shepard

 extended for spin-orbit CI calculations ( Russ Pitzer, OSU)

 and large active spaces (Thomas Müller, FZ Juelich)

 version date: 16-jul-04


 This Version of Program CIDRT is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIDRT       **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 workspace allocation parameters: lencor= 100000000 mem1=********** ifirst=-101234960
 expanded "keystrokes" are being written to file:
 cidrtky                                                                         
 Spin-Orbit CI Calculation?(y,[n])
 Spin-Free Calculation

 input the spin multiplicity [  0]:
 spin multiplicity, smult            :   1    singlet 
 input the total number of electrons [  0]:
 total number of electrons, nelt     :    16
 input the number of irreps (1:8) [  0]:
 point group dimension, nsym         :     4
 enter symmetry labels:(y,[n])
 enter 4 labels (a4):
 enter symmetry label, default=   1
 enter symmetry label, default=   2
 enter symmetry label, default=   3
 enter symmetry label, default=   4
 symmetry labels: (symmetry, slabel)
 ( 1,  a1 ) ( 2,  b1 ) ( 3,  b2 ) ( 4,  a2 ) 
 input nmpsy(*):
 nmpsy(*)=        18   7  10   3

   symmetry block summary
 block(*)=         1   2   3   4
 slabel(*)=      a1  b1  b2  a2 
 nmpsy(*)=        18   7  10   3

 total molecular orbitals            :    38
 input the molecular spatial symmetry (irrep 1:nsym) [  0]:
 state spatial symmetry label        :  a1 

 input the frozen core orbitals (sym(i),rmo(i),i=1,nfct):
 total frozen core orbitals, nfct    :     0
 no frozen core orbitals entered

 number of frozen core orbitals      :     0
 number of frozen core electrons     :     0
 number of internal electrons        :    16

 input the frozen virtual orbitals (sym(i),rmo(i),i=1,nfvt):
 total frozen virtual orbitals, nfvt :     0

 no frozen virtual orbitals entered

 input the internal orbitals (sym(i),rmo(i),i=1,niot):
 niot                                :     9

 modrt(*)=         1   2   3   4   5  19  26  20  27
 slabel(*)=      a1  a1  a1  a1  a1  b1  b2  b1  b2 

 total number of orbitals            :    38
 number of frozen core orbitals      :     0
 number of frozen virtual orbitals   :     0
 number of internal orbitals         :     9
 number of external orbitals         :    29

 orbital-to-level mapping vector
 map(*)=          30  31  32  33  34   1   2   3   4   5   6   7   8   9  10
                  11  12  13  35  37  14  15  16  17  18  36  38  19  20  21
                  22  23  24  25  26  27  28  29

 input the number of ref-csf doubly-occupied orbitals [  0]:
 (ref) doubly-occupied orbitals      :     7

 no. of internal orbitals            :     9
 no. of doubly-occ. (ref) orbitals   :     7
 no. active (ref) orbitals           :     2
 no. of active electrons             :     2

 input the active-orbital, active-electron occmnr(*):
  20 27
 input the active-orbital, active-electron occmxr(*):
  20 27

 actmo(*) =       20  27
 occmnr(*)=        0   2
 occmxr(*)=        2   2
 reference csf cumulative electron occupations:
 modrt(*)=         1   2   3   4   5  19  26  20  27
 occmnr(*)=        2   4   6   8  10  12  14  14  16
 occmxr(*)=        2   4   6   8  10  12  14  16  16

 input the active-orbital bminr(*):
  20 27
 input the active-orbital bmaxr(*):
  20 27
 reference csf b-value constraints:
 modrt(*)=         1   2   3   4   5  19  26  20  27
 bminr(*)=         0   0   0   0   0   0   0   0   0
 bmaxr(*)=         0   0   0   0   0   0   0   2   2
 input the active orbital smaskr(*):
  20 27
 modrt:smaskr=
   1:1000   2:1000   3:1000   4:1000   5:1000  19:1000  26:1000  20:1111
  27:1111

 input the maximum excitation level from the reference csfs [  2]:
 maximum excitation from ref. csfs:  :     2
 number of internal electrons:       :    16

 input the internal-orbital mrsdci occmin(*):
   1  2  3  4  5 19 26 20 27
 input the internal-orbital mrsdci occmax(*):
   1  2  3  4  5 19 26 20 27
 mrsdci csf cumulative electron occupations:
 modrt(*)=         1   2   3   4   5  19  26  20  27
 occmin(*)=        0   0   0   0   0   0   0   0  14
 occmax(*)=       16  16  16  16  16  16  16  16  16

 input the internal-orbital mrsdci bmin(*):
   1  2  3  4  5 19 26 20 27
 input the internal-orbital mrsdci bmax(*):
   1  2  3  4  5 19 26 20 27
 mrsdci b-value constraints:
 modrt(*)=         1   2   3   4   5  19  26  20  27
 bmin(*)=          0   0   0   0   0   0   0   0   0
 bmax(*)=         16  16  16  16  16  16  16  16  16

 input the internal-orbital smask(*):
   1  2  3  4  5 19 26 20 27
 modrt:smask=
   1:1000   2:1000   3:1111   4:1111   5:1111  19:1111  26:1111  20:1111
  27:1111

 internal orbital summary:
 block(*)=         1   1   1   1   1   2   3   2   3
 slabel(*)=      a1  a1  a1  a1  a1  b1  b2  b1  b2 
 rmo(*)=           1   2   3   4   5   1   1   2   2
 modrt(*)=         1   2   3   4   5  19  26  20  27

 reference csf info:
 occmnr(*)=        2   4   6   8  10  12  14  14  16
 occmxr(*)=        2   4   6   8  10  12  14  16  16

 bminr(*)=         0   0   0   0   0   0   0   0   0
 bmaxr(*)=         0   0   0   0   0   0   0   2   2


 mrsdci csf info:
 occmin(*)=        0   0   0   0   0   0   0   0  14
 occmax(*)=       16  16  16  16  16  16  16  16  16

 bmin(*)=          0   0   0   0   0   0   0   0   0
 bmax(*)=         16  16  16  16  16  16  16  16  16


 a priori removal of distinct rows:

 input the level, a, and b values for the vertices 
 to be removed (-1/ to end).

 input level, a, and b (-1/ to end):
 no vertices marked for removal

 impose generalized interacting space restrictions?(y,[n])
 generalized interacting space restrictions will not be imposed.
 multp 0 0 0 0 0 0 0 0 0
 spnir
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  hmult 0
 lxyzir 0 0 0
 spnir
  1  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  1  0  0  0  1  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  1  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  1  0  0  0  1  0  0  0  1  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  1  0  0  0  1  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0

 number of rows in the drt :  54

 manual arc removal step:


 input the level, a, b, and step values 
 for the arcs to be removed (-1/ to end).

 input the level, a, b, and step (-1/ to end):
 remarc:   0 arcs removed out of   0 specified.

 xbarz=      28
 xbary=     112
 xbarx=     210
 xbarw=     196
        --------
 nwalk=     546
 input the range of drt levels to print (l1,l2):
 levprt(*)        -1   0

 reference-csf selection step 1:
 total number of z-walks in the drt, nzwalk=      28

 input the list of allowed reference symmetries:
 allowed reference symmetries:             1    2    3    4
 allowed reference symmetry labels:      a1   b1   b2   a2 
 keep all of the z-walks as references?(y,[n])
 all z-walks are initially deleted.

 generate walks while applying reference drt restrictions?([y],n)
 reference drt restrictions will be imposed on the z-walks.

 impose additional orbital-group occupation restrictions?(y,[n])

 apply primary reference occupation restrictions?(y,[n])

 manually select individual walks?(y,[n])

 step 1 reference csf selection complete.
        3 csfs initially selected from      28 total walks.

 beginning step-vector based selection.
 enter [internal_orbital_step_vector/disposition] pairs:

 enter internal orbital step vector, (-1/ to end):
   1  2  3  4  5 19 26 20 27

 step 2 reference csf selection complete.
        3 csfs currently selected from      28 total walks.

 beginning numerical walk based selection.
 enter positive walk numbers to add walks,
 negative walk numbers to delete walks, and zero to end:

 input reference walk number (0 to end) [  0]:

 numerical walk-number based selection complete.
        3 reference csfs selected from      28 total z-walks.

 input the reference occupations, mu(*):
 reference occupations:
 mu(*)=            0   0   0   0   0   0   0   0   0

 number of step vectors saved:      3

 exlimw: beginning excitation-based walk selection...

  number of valid internal walks of each symmetry:

       a1      b1      b2      a2 
      ----    ----    ----    ----
 z      12       0       0       0
 y      18      18      18      18
 x      11      18      18      18
 w      21      15      15      15

 csfs grouped by internal walk symmetry:

       a1      b1      b2      a2 
      ----    ----    ----    ----
 z      12       0       0       0
 y     234      90     144      54
 x    1309    1602    2142    1422
 w    3108    1335    1785    1185

 total csf counts:
 z-vertex:       12
 y-vertex:      522
 x-vertex:     6475
 w-vertex:     7413
           --------
 total:       14422

 this is an obsolete prompt.(y,[n])

 final mrsdci walk selection step:

 nvalw(*)=      12      72      65      66 nvalwt=     215

 enter positive walk numbers to add walks,
 negative walk numbers to delete walks, and zero to end.

 input mrsdci walk number (0 to end) [  0]:

 end of manual mrsdci walk selection.
 number added=   0 number removed=   0

 nvalw(*)=      12      72      65      66 nvalwt=     215


 lprune: l(*,*,*) pruned with nwalk=     546 nvalwt=     215
 lprune:  z-drt, nprune=    83
 lprune:  y-drt, nprune=    63
 lprune: wx-drt, nprune=    42

 xbarz=      15
 xbary=      72
 xbarx=      65
 xbarw=      66
        --------
 nwalk=     218
 levprt(*)        -1   0

 beginning the reference csf index recomputation...

     iref   iwalk  step-vector
   ------  ------  ------------
        1       1  333333330
        2       2  333333312
        3       3  333333303
 indx01:     3 elements set in vec01(*)

 beginning the valid upper walk index recomputation...
 indx01:   215 elements set in vec01(*)

 beginning the final csym(*) computation...

  number of valid internal walks of each symmetry:

       a1      b1      b2      a2 
      ----    ----    ----    ----
 z      12       0       0       0
 y      18      18      18      18
 x      11      18      18      18
 w      21      15      15      15

 csfs grouped by internal walk symmetry:

       a1      b1      b2      a2 
      ----    ----    ----    ----
 z      12       0       0       0
 y     234      90     144      54
 x    1309    1602    2142    1422
 w    3108    1335    1785    1185

 total csf counts:
 z-vertex:       12
 y-vertex:      522
 x-vertex:     6475
 w-vertex:     7413
           --------
 total:       14422

 input a title card, default=cidrt_title
 title card:
  cidrt_title                                                                    

 input a drt file name, default=cidrtfl
 drt and indexing arrays will be written to file:
 cidrtfl                                                                         

 write the drt file?([y],n)
 drt file is being written...
 !timer: cidrt required                  user+sys=     0.000 walltime=     0.000
