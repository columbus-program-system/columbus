

     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 This program allows the csf mixing coefficient and orbital expansion coefficient
 optimization using the graphical unitary group approach and the exponential
 operator mcscf method.
 references:  r. shepard and j. simons,                    ' int. j. quantum chem. symp. 14, 211 (1980).
              r. shepard, i. shavitt, and j. simons, j. chem. phys. 76, 543 (1982).
              r. shepard in "ab initio methods in quantum chemistry ii" advances in chemical
                  physics 69, edited by k. p. lawley (wiley, new york, 1987) pp. 63-200.
 Original autor: Ron Shepard, ANL
 Later revisions: Michal Dallos, University Vienna

 This Version of Program MCSCF is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.4.0.2     **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 Workspace allocation information:
       100000000 of real*8 words (  762.94 MB) of work space has been allocated.

 user input information:

 ======== echo of the mcscf input ========
 ------------------------------------------------------------------------
  &input
   niter=60,
   nmiter=30,
   nciitr=30,
   tol(3)=1.e-4,
   tol(2)=1.e-4,
   tol(1)=1.e-8,
   NSTATE=0,
   npath=1,3,9,10,13,17,19,21,23,-11,12, 2,
   ncoupl=5,
  FCIORB=
   2,2,40,3,2,40
   NAVST(1) = 1,
   WAVST(1,1)=1 ,
   NAVST(2) = 1,
   WAVST(2,1)=1 ,
  &end
 ------------------------------------------------------------------------


 ***  Integral file informations  ***


 input integral file : aoints                                                      

 Integral file header information:
 Hermit Integral Program : SIFS version  ronja.itc.univieFri Sep  2 13:50:14 2005

 Core type energy values:
 energy( 1)=  3.116530982443E+01, ietype=   -1,    core energy of type: Nuc.Rep.
 total ao core energy =   31.165309824


   ******  Basis set informations:  ******

 Number of irreps:                  4
 Total number of basis functions:  38

 irrep no.              1    2    3    4
 irrep label           A1   B1   B2   A2 
 no. of bas.fcions.    18    7   10    3


 ***  MCSCF optimization procedure parmeters:  ***


 maximum number of mcscf iterations:        niter=    60

 maximum number of psci micro-iterations:   nmiter=   30
 maximum r,s subspace dimension allowed:    nvrsmx=   30

 tol(1)=  1.0000E-08. . . . delta-emc convergence criterion.
 tol(2)=  1.0000E-04. . . . wnorm convergence criterion.
 tol(3)=  1.0000E-04. . . . knorm convergence criterion.
 tol(4)=  1.0000E-08. . . . apxde convergence criterion.
 tol(5)=  1.0000E-04. . . . small diagonal matrix element tolerance.
 tol(6)=  1.0000E-06. . . . minimum ci-psci residual norm.
 tol(7)=  1.0000E-05. . . . maximum ci-psci residual norm.
 tol(8)=  1.0000E+00. . . . maximum abs(k(xy)) allowed.
 tol(9)=  1.0000E+00. . . . wnorm coupling tolerance.
 tol(10)= 0.0000E+00. . . . maximum psci emergency shift parameter.
 tol(11)= 0.0000E+00. . . . minimum psci emergency shift parameter.
 tol(12)= 0.0000E+00. . . . increment of psci emergency shift parameter.


 *** State averaging informations: ***


 MCSCF calculation performed for  2 DRTs.

 DRT  first state   no.of aver.states   weights
  1   ground state          1             0.500
  2   ground state          1             0.500

 The number of hmc(*) eigenvalues and eigenvectors calculated each iteration per symmetry:
 Symm.   no.of eigenv.(=ncol)
  A1         2
  B1         2

 Orbitals included in invariant subspaces:
   symmetry   orbital   mask
       2       2( 20)    40
       3       2( 27)    40

 npath(*) options:
  2:  orbital-state coupling terms will be included beginning on iteration ncoupl=  5
  3:  print intermediate timing information.
  9:  suppress the drt listing.
 10:  suppress the hmc(*) eigenvector listing.
 12:  diagonalize the hmc(*) matrix iteratively.
        nunitv= 1 nciitr=30 mxvadd=20 nvcimx=20
       rtolci(*),wnorm=     1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 0.0000E+00
   noldv =   0  0
 13:  get initial orbitals from the formatted file, mocoef.
 17:  print the final natural orbitals and occupations.
 19:  transform the virtual orbitals to diagonalize qvv(*).
 21:  write out the one- and two- electron density for further use (files:mcd1fl, mcd2fl).
 23:  use the old integral transformation.


   ******  DRT info section  ******


 Informations for the DRT no.  1

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a1 
 Total number of electrons:   16
 Spin multiplicity:            1
 Number of active orbitals:    2
 Number of active electrons:   2
 Total number of CSFs:         2

 Informations for the DRT no.  2

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a2 
 Total number of electrons:   16
 Spin multiplicity:            1
 Number of active orbitals:    2
 Number of active electrons:   2
 Total number of CSFs:         1

 !timer: initialization                  user+sys=     0.000 walltime=     0.000

 faar:   0 active-active rotations allowed out of:   0 possible.


 Number of active-double rotations:      2
 Number of active-active rotations:      0
 Number of double-virtual rotations:    78
 Number of active-virtual rotations:    13

 Size of orbital-Hessian matrix B:                     4837
 Size of the orbital-state Hessian matrix C:            279
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:           5116



   ****** Integral transformation section ******


 number of blocks to be transformed in-core is  19
 number of blocks to be transformed out of core is    0

 in-core ao list requires    1 segments.
 out of core ao list requires    0 segments.
 each segment has length ****** working precision words.

               ao integral sort statistics
 length of records:      1610
 number of buckets:   1
 scratch file used is da1:
 amount of core needed for in-core arrays:  2153

 twoao_o processed      70449 two electron ao integrals.

      70449 ao integrals were written into   66 records

 srtinc_o read in      74227 integrals and wrote out      74227 integrals.

 Source of the initial MO coeficients:

 Input MO coefficient file: mocoef                                                      


               starting mcscf iteration...   1
 !timer:                                 user+sys=     0.020 walltime=     0.000

 orbital-state coupling will not be calculated this iteration.

 number of transformed integrals put on file:     18548
 number of records written:     7
 !timer: 2-e transformation              user+sys=     0.030 walltime=     0.000

 Size of orbital-Hessian matrix B:                     4837
 Total size of the state Hessian matrix M:                0
 Total size of HESSIAN-matrix for linear con           4837

 !timer: mosrt1                          user+sys=     0.000 walltime=     0.000
 !timer: mosrt2                          user+sys=     0.000 walltime=     0.000
 !timer: mosort                          user+sys=     0.000 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000

 trial vectors are generated internally.

 trial vector  1 is unit matrix column     2
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 ciiter=   2 noldhv=  2 noldv=  2
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -113.8658135491     -145.0311233735        0.0000000000        0.0000010000
    2      -113.3810005949     -144.5463104193        0.0000000000        0.0100000000
 !timer: hmcvec                          user+sys=     0.000 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000

 trial vectors are generated internally.

 trial vector  1 is unit matrix column     1
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 ciiter=   1 noldhv=  1 noldv=  1
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -113.7294416606     -144.8947514851        0.0000000000        0.0000010000
 !timer: hmcvec                          user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: mqva                            user+sys=     0.000 walltime=     0.000
 !timer: mfva                            user+sys=     0.000 walltime=     0.000
 !timer: mfad                            user+sys=     0.000 walltime=     0.000
 !timer: mqad                            user+sys=     0.000 walltime=     0.000
 !timer: mfvd                            user+sys=     0.000 walltime=     0.000
 !timer: bvaad                           user+sys=     0.000 walltime=     0.000
 !timer: bvaadf                          user+sys=     0.000 walltime=     0.000
 !timer: mqaa                            user+sys=     0.000 walltime=     0.000
 !timer: mfaa                            user+sys=     0.000 walltime=     0.000
 !timer: badad                           user+sys=     0.000 walltime=     0.000
 !timer: badadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvava                           user+sys=     0.000 walltime=     0.000
 !timer: bvavaf                          user+sys=     0.000 walltime=     0.000
 !timer: bvavd                           user+sys=     0.000 walltime=     0.000
 !timer: bvavdf                          user+sys=     0.000 walltime=     0.000
 !timer: bvdad                           user+sys=     0.000 walltime=     0.000
 !timer: bvdadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvdvd                           user+sys=     0.000 walltime=     0.000
 !timer: hbcon                           user+sys=     0.000 walltime=     0.000

  tol(10)=  0. eshsci=  1.16917909E-07
 Total number of micro iterations:    2

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 1.00000000 pnorm= 0.0000E+00 rznorm= 3.4281E-07 rpnorm= 0.0000E+00 noldr=  2 nnewr=  2 nolds=  0 nnews=  0

 !timer: Direct Bxr time contribution    user+sys=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  user+sys=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    user+sys=     0.000 walltime=     0.000
 !timer: solvek total                    user+sys=     0.000 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
   -41.225802  -22.627249   -2.839652   -1.710459   -1.321217

 qvv(*) eigenvalues. symmetry block  1
     0.386823    0.715102    1.338902    1.803325    1.954445    2.190450    3.351500    3.669802    3.935799    5.095928
     5.445145    6.282911    7.615552

 fdd(*) eigenvalues. symmetry block  2
    -1.135954
 i,qaaresolved 1  0.0979938589

 qvv(*) eigenvalues. symmetry block  2
     1.352723    2.513899    2.835314    3.884158    6.803918

 fdd(*) eigenvalues. symmetry block  3
    -1.311083
 i,qaaresolved 1 -0.691110474

 qvv(*) eigenvalues. symmetry block  3
     0.525771    1.483099    1.637457    2.621501    3.363456    3.895369    5.308560    7.364420

 qvv(*) eigenvalues. symmetry block  4
     2.327518    4.012287    6.024893
 !timer: motran                          user+sys=     0.000 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 user+sys=     0.030 walltime=     0.000

 all mcscf convergence criteria are not satisfied.
 iter=    1 emc= -113.7976276049 demc= 1.1380E+02 wnorm= 9.3534E-07 knorm= 5.0332E-07 apxde= 1.6047E-13    *not converged* 

               starting mcscf iteration...   2
 !timer:                                 user+sys=     0.050 walltime=     0.000

 orbital-state coupling will not be calculated this iteration.

 number of transformed integrals put on file:     18548
 number of records written:     7
 !timer: 2-e transformation              user+sys=     0.020 walltime=     0.000

 Size of orbital-Hessian matrix B:                     4837
 Total size of the state Hessian matrix M:                0
 Total size of HESSIAN-matrix for linear con           4837

 !timer: mosrt1                          user+sys=     0.010 walltime=     0.000
 !timer: mosrt2                          user+sys=     0.000 walltime=     0.000
 !timer: mosort                          user+sys=     0.010 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000

   2 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 ciiter=   1 noldhv=  2 noldv=  2
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -113.8658135082     -145.0311233327        0.0000000000        0.0000010000
    2      -113.3810003822     -144.5463102067        0.0000000000        0.0100000000
 !timer: hmcvec                          user+sys=     0.000 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000

   1 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 ciiter=   1 noldhv=  1 noldv=  1
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -113.7294417015     -144.8947515259        0.0000000000        0.0000010000
 !timer: hmcvec                          user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: mqva                            user+sys=     0.000 walltime=     0.000
 !timer: mfva                            user+sys=     0.000 walltime=     0.000
 !timer: mfad                            user+sys=     0.000 walltime=     0.000
 !timer: mqad                            user+sys=     0.000 walltime=     0.000
 !timer: mfvd                            user+sys=     0.000 walltime=     0.000
 !timer: bvaad                           user+sys=     0.000 walltime=     0.000
 !timer: bvaadf                          user+sys=     0.000 walltime=     0.000
 !timer: mqaa                            user+sys=     0.000 walltime=     0.000
 !timer: mfaa                            user+sys=     0.000 walltime=     0.000
 !timer: badad                           user+sys=     0.000 walltime=     0.000
 !timer: badadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvava                           user+sys=     0.000 walltime=     0.000
 !timer: bvavaf                          user+sys=     0.000 walltime=     0.000
 !timer: bvavd                           user+sys=     0.000 walltime=     0.000
 !timer: bvavdf                          user+sys=     0.000 walltime=     0.000
 !timer: bvdad                           user+sys=     0.000 walltime=     0.000
 !timer: bvdadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvdvd                           user+sys=     0.000 walltime=     0.000
 !timer: hbcon                           user+sys=     0.000 walltime=     0.000

  tol(10)=  0. eshsci=  4.29960126E-08
 Total number of micro iterations:    1

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 1.00000000 pnorm= 0.0000E+00 rznorm= 3.3527E-07 rpnorm= 0.0000E+00 noldr=  1 nnewr=  1 nolds=  0 nnews=  0

 !timer: Direct Bxr time contribution    user+sys=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  user+sys=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    user+sys=     0.000 walltime=     0.000
 !timer: solvek total                    user+sys=     0.000 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
   -41.225802  -22.627250   -2.839652   -1.710459   -1.321217

 qvv(*) eigenvalues. symmetry block  1
     0.386823    0.715102    1.338902    1.803325    1.954445    2.190450    3.351500    3.669802    3.935799    5.095928
     5.445145    6.282911    7.615552

 fdd(*) eigenvalues. symmetry block  2
    -1.135954
 i,qaaresolved 1  0.0979937477

 qvv(*) eigenvalues. symmetry block  2
     1.352723    2.513899    2.835314    3.884158    6.803918

 fdd(*) eigenvalues. symmetry block  3
    -1.311083
 i,qaaresolved 1 -0.69111048

 qvv(*) eigenvalues. symmetry block  3
     0.525770    1.483099    1.637457    2.621501    3.363456    3.895369    5.308560    7.364420

 qvv(*) eigenvalues. symmetry block  4
     2.327518    4.012287    6.024893
 !timer: motran                          user+sys=     0.000 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 user+sys=     0.040 walltime=     0.000

 all mcscf convergence criteria are satisfied.

 final mcscf convergence values:
 iter=    2 emc= -113.7976276049 demc= 1.7053E-13 wnorm= 3.4397E-07 knorm= 3.3693E-08 apxde= 5.9512E-15    *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 weight 0.500000 total energy= -113.865813508
   DRT #2 state # 1 weight 0.500000 total energy= -113.729441701
   ------------------------------------------------------------



          mcscf orbitals of the final iteration,  A1  block   1

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
   1O1s       1.00069494    -0.00011263    -0.00838658    -0.00170433    -0.00033995    -0.00208014    -0.07005487     0.02537717
   2O1s       0.00318961     0.00109285     0.82612980    -0.39487868    -0.24385157    -0.00322141     0.12398016     0.19838873
   3O1s      -0.00449788    -0.00250194    -0.08206405    -0.04112803    -0.10823039     0.03918257     1.63550443    -0.06020805
   4O1pz      0.00257451     0.00049727     0.19884903     0.29623025     0.72076601    -0.08769205     0.18486994     0.06837994
   5O1pz     -0.00275225    -0.00159670    -0.05697871    -0.01570426    -0.04282272    -0.06722789     0.53695782     0.08573686
   6O1d0      0.00006204     0.00001714     0.00491813     0.00329146     0.00733510     0.00036645     0.00850467     0.00086354
   7O1d2+     0.00001541     0.00004375     0.00001194     0.00012918     0.00030753     0.00166671    -0.00007673    -0.00510051
   8C1s       0.00017480     1.00122879    -0.01635062    -0.00882728     0.01095431    -0.06321510     0.04401324     0.11571347
   9C1s       0.00038457     0.00754868     0.40275205     0.69005519     0.04768103     0.21186813    -0.01058969     0.20808155
  10C1s       0.00201616    -0.00548609    -0.16193677    -0.15728323    -0.12400111     1.80260747    -1.28144373    -0.78303036
  11C1pz      0.00018972    -0.00136768    -0.27124742     0.18386603    -0.57307544     0.26566377     0.07624506    -0.03959221
  12C1pz     -0.00139872    -0.00064478     0.11514349    -0.01343201     0.21672072     0.47472006     1.68921979    -0.28261694
  13C1d0     -0.00019616     0.00023551     0.01176871    -0.00132312     0.00921898     0.00045092    -0.00876830    -0.00080396
  14C1d2+    -0.00014165    -0.00026376     0.00354807    -0.00352595     0.00507569    -0.01068900     0.00219408    -0.13092408
  15H1s      -0.00054234    -0.00050869     0.06315322     0.37636129    -0.24895878    -0.06981397    -0.00706291     1.38218301
  16H1s       0.00027957     0.00152489    -0.03270724    -0.15965071     0.06601711    -1.33070659    -0.10956392    -0.99793740
  17H1py     -0.00002785     0.00035873    -0.00633817    -0.02327444     0.01436413     0.01464570    -0.01239892     0.12130078
  18H1pz      0.00032732     0.00025483    -0.00939995    -0.01155177    -0.00236653     0.01099221     0.01564189     0.01107740

               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
   1O1s      -0.03574376    -0.01849606     0.02797128     0.07797383    -0.34791904    -0.61149386    -0.20808184    -0.46950416
   2O1s      -0.36871738    -0.04159346     0.37566840     0.31838874    -1.64414877    -2.57873512    -0.93703492    -1.99975914
   3O1s       0.44178554     0.76210571     0.55774557    -0.56240845     1.65118098     5.06322647     0.72550073     2.25243212
   4O1pz     -0.46829293    -0.50108194    -0.78709493    -0.19983043    -0.70761370     0.73611654    -0.44668233    -0.64792018
   5O1pz     -0.17509334     0.76236948     1.84751233     0.21638228     0.89096095     0.47789291     0.26155852     0.74046834
   6O1d0      0.02794203     0.01802623    -0.00760783     0.00643829    -0.01014741    -0.03062730    -0.00698395    -0.02702818
   7O1d2+     0.00147598    -0.00466139     0.00104868     0.05302274    -0.02672517     0.00965941    -0.07433712     0.25483019
   8C1s      -0.01860517     0.65890828    -0.46402346     0.25740444    -0.07387408     0.00631730     0.23679459    -0.20240622
   9C1s       0.11233263     3.13779162    -1.95172581     1.02616229    -0.10171723    -0.07629622     1.43061719    -0.87506832
  10C1s       1.06192931    -5.47536895     1.30375388    -1.76538970    -0.27823859    -1.91878438     0.47002159    -0.45804945
  11C1pz     -1.48745613     0.24895240     0.55480464    -0.03202211    -0.27010128     0.89503381     0.81916709    -0.56293094
  12C1pz      1.79920361    -0.42199638     0.27988839    -0.80063042    -0.10392136     0.87009722     0.09173287     0.51581537
  13C1d0     -0.09069056    -0.06411698    -0.10622741    -0.01378440     0.04447982    -0.25538238     0.18013175     0.25753949
  14C1d2+    -0.03159723     0.03382342     0.00460288     0.44169687    -0.23033195     0.05027506    -0.21080823     0.29246786
  15H1s      -0.07282685    -0.01332533    -0.05258253     1.12611487    -0.06595449     0.10471951    -1.14212199     0.87990414
  16H1s      -0.53420245     1.09784527    -0.23037623    -0.34168381     0.12041833    -0.06154220     0.07485572    -0.31539981
  17H1py      0.13127379    -0.13406466     0.06034217    -0.00074303    -0.29553404     0.06960383     0.98625164    -0.15008699
  18H1pz     -0.00730651    -0.15864807    -0.05687328     0.30000617     0.50384662     0.01679823     0.16762276    -0.68987583

               MO   17        MO   18
   1O1s       0.24493339    -0.29526876
   2O1s       1.01964698    -1.08758146
   3O1s      -1.23950275     2.60580458
   4O1pz      0.30264961     0.24104936
   5O1pz     -0.40410089     0.96687641
   6O1d0      0.02495065     0.34308912
   7O1d2+     0.42569180    -0.01301711
   8C1s       0.14130903    -0.40500327
   9C1s       0.73980413    -1.85766608
  10C1s       0.73196915     0.10109539
  11C1pz      0.57354235     1.31587083
  12C1pz     -0.24838709    -0.21243201
  13C1d0     -0.12887994    -0.12628849
  14C1d2+    -0.41231162     0.04001669
  15H1s      -1.12888802     0.16784329
  16H1s       0.35553132    -0.03140312
  17H1py      0.29975212    -0.08077436
  18H1pz      0.52463659    -0.03026171

          mcscf orbitals of the final iteration,  B1  block   2

               MO   19        MO   20        MO   21        MO   22        MO   23        MO   24        MO   25
  19O1px      0.76388742    -0.61687602    -0.03075787    -1.35530244     0.11997467    -0.33983235    -0.00876028
  20O1px     -0.00365570    -0.12332628    -0.11673463     1.88440114     0.47195681     0.26433865     0.38524387
  21O1d1+     0.02810360     0.01076721    -0.03988589    -0.02660696    -0.09516782     0.08009363     1.09645853
  22C1px      0.46174391     0.78423283    -1.47989997     0.03661527    -0.02710717    -0.31677314    -0.30089769
  23C1px     -0.02836622     0.21217077     1.82591216    -0.44045027    -0.44134046    -0.18826386    -0.00973950
  24C1d1+    -0.04898927     0.02019691     0.03772520     0.12398388     0.85993738    -0.67825193     0.63064465
  25H1px      0.00949893     0.02116772    -0.03958657    -0.10141733     0.31002615     0.75620828    -0.08368951

          mcscf orbitals of the final iteration,  B2  block   3

               MO   26        MO   27        MO   28        MO   29        MO   30        MO   31        MO   32        MO   33
  26O1py      0.30697145     0.88095885     0.13648244     0.46704823    -0.05918403    -1.19761131     0.32895450    -0.53306204
  27O1py     -0.03110237     0.04872248     0.24576140     0.13745299     0.00593717     2.01573576    -0.71073520     0.36476227
  28O1d1-     0.01809400     0.01539900    -0.01039516    -0.00703863     0.03194178    -0.05081461     0.02520365     0.19672216
  29C1py      0.63329345    -0.15844456    -0.52333511     0.42905573     1.24493603     0.34306387    -0.14912273     0.28041684
  30C1py     -0.14421427     0.05476504    -1.18222205    -2.23163132    -2.18489991    -1.39961031     1.45685430    -0.30351682
  31C1d1-    -0.00178548    -0.07012527    -0.02238473     0.08792456    -0.35784380     0.42973030     0.16044676    -0.79752040
  32H1s       0.49745553    -0.32968022    -0.09603168     1.21062794    -0.58561074    -0.08524016    -0.78086653     0.71747068
  33H1s      -0.14633274     0.01570829     1.91200731    -0.02343880     1.72180549     0.63935331    -0.05301660    -0.59891318
  34H1py     -0.01707304     0.00178390    -0.01277177    -0.04282165    -0.18057127    -0.03168367    -0.49455581     0.07876153
  35H1pz     -0.01365259     0.00609398    -0.01293782     0.05769346    -0.15493158     0.03634112     0.48434826     0.48857072

               MO   34        MO   35
  26O1py      0.30446268     0.12067626
  27O1py     -0.35753663     0.36730876
  28O1d1-    -0.21079585     1.10677116
  29C1py      1.94275984     0.41424946
  30C1py      0.20563283    -0.20914701
  31C1d1-     1.09297470     1.22866579
  32H1s      -1.86710062    -0.99902948
  33H1s       0.27535589     0.53446708
  34H1py      0.93213628     0.44076468
  35H1pz      0.56815020     0.00084139

          mcscf orbitals of the final iteration,  A2  block   4

               MO   36        MO   37        MO   38
  36O1d2-     0.04146652    -0.17670257     0.99170883
  37C1d2-     0.65102034    -0.92409844    -0.34458688
  38H1px      0.34616600     0.73953720     0.17202144

          natural orbitals of the final iteration, block  1

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8

  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     0.00000000     0.00000000     0.00000000

   1O1s       1.00069494    -0.00011263    -0.00838658    -0.00170433    -0.00033995    -0.00208014    -0.07005487     0.02537717
   2O1s       0.00318961     0.00109285     0.82612979    -0.39487867    -0.24385163    -0.00322140     0.12398015     0.19838871
   3O1s      -0.00449788    -0.00250194    -0.08206405    -0.04112802    -0.10823040     0.03918256     1.63550441    -0.06020799
   4O1pz      0.00257451     0.00049727     0.19884905     0.29623017     0.72076605    -0.08769205     0.18486995     0.06837993
   5O1pz     -0.00275225    -0.00159670    -0.05697871    -0.01570425    -0.04282272    -0.06722790     0.53695780     0.08573688
   6O1d0      0.00006204     0.00001714     0.00491813     0.00329146     0.00733510     0.00036645     0.00850467     0.00086354
   7O1d2+     0.00001541     0.00004375     0.00001194     0.00012918     0.00030753     0.00166671    -0.00007673    -0.00510051
   8C1s       0.00017480     1.00122879    -0.01635062    -0.00882728     0.01095431    -0.06321508     0.04401324     0.11571347
   9C1s       0.00038457     0.00754867     0.40275206     0.69005518     0.04768110     0.21186819    -0.01058973     0.20808154
  10C1s       0.00201616    -0.00548609    -0.16193677    -0.15728322    -0.12400113     1.80260738    -1.28144366    -0.78303035
  11C1pz      0.00018972    -0.00136768    -0.27124743     0.18386610    -0.57307542     0.26566381     0.07624507    -0.03959223
  12C1pz     -0.00139872    -0.00064478     0.11514349    -0.01343204     0.21672072     0.47472001     1.68921978    -0.28261688
  13C1d0     -0.00019616     0.00023551     0.01176871    -0.00132312     0.00921898     0.00045092    -0.00876830    -0.00080396
  14C1d2+    -0.00014165    -0.00026376     0.00354807    -0.00352596     0.00507569    -0.01068900     0.00219408    -0.13092408
  15H1s      -0.00054234    -0.00050869     0.06315323     0.37636131    -0.24895874    -0.06981400    -0.00706292     1.38218300
  16H1s       0.00027957     0.00152489    -0.03270725    -0.15965072     0.06601709    -1.33070654    -0.10956392    -0.99793742
  17H1py     -0.00002785     0.00035873    -0.00633817    -0.02327444     0.01436412     0.01464570    -0.01239892     0.12130077
  18H1pz      0.00032732     0.00025483    -0.00939995    -0.01155177    -0.00236653     0.01099221     0.01564189     0.01107740

               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16

  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

   1O1s      -0.03574376    -0.01849607     0.02797128     0.07797384    -0.34791897    -0.61149389    -0.20808185    -0.46950418
   2O1s      -0.36871737    -0.04159351     0.37566840     0.31838879    -1.64414847    -2.57873526    -0.93703496    -1.99975919
   3O1s       0.44178551     0.76210570     0.55774570    -0.56240845     1.65118040     5.06322661     0.72550080     2.25243224
   4O1pz     -0.46829292    -0.50108186    -0.78709495    -0.19983038    -0.70761381     0.73611650    -0.44668234    -0.64792017
   5O1pz     -0.17509337     0.76236930     1.84751240     0.21638224     0.89096094     0.47789295     0.26155853     0.74046837
   6O1d0      0.02794203     0.01802623    -0.00760782     0.00643829    -0.01014740    -0.03062730    -0.00698395    -0.02702818
   7O1d2+     0.00147598    -0.00466139     0.00104868     0.05302274    -0.02672517     0.00965940    -0.07433711     0.25483018
   8C1s      -0.01860518     0.65890833    -0.46402339     0.25740444    -0.07387407     0.00631729     0.23679459    -0.20240623
   9C1s       0.11233258     3.13779184    -1.95172550     1.02616226    -0.10171717    -0.07629625     1.43061716    -0.87506836
  10C1s       1.06192943    -5.47536912     1.30375332    -1.76538972    -0.27823847    -1.91878434     0.47002162    -0.45804954
  11C1pz     -1.48745616     0.24895232     0.55480468    -0.03202208    -0.27010137     0.89503378     0.81916708    -0.56293095
  12C1pz      1.79920365    -0.42199636     0.27988839    -0.80063040    -0.10392150     0.87009722     0.09173290     0.51581540
  13C1d0     -0.09069056    -0.06411697    -0.10622743    -0.01378441     0.04447985    -0.25538238     0.18013175     0.25753948
  14C1d2+    -0.03159723     0.03382342     0.00460288     0.44169689    -0.23033192     0.05027502    -0.21080823     0.29246788
  15H1s      -0.07282685    -0.01332535    -0.05258254     1.12611491    -0.06595444     0.10471947    -1.14212197     0.87990420
  16H1s      -0.53420249     1.09784531    -0.23037612    -0.34168384     0.12041831    -0.06154218     0.07485570    -0.31539982
  17H1py      0.13127378    -0.13406466     0.06034216    -0.00074301    -0.29553404     0.06960379     0.98625164    -0.15008701
  18H1pz     -0.00730650    -0.15864806    -0.05687330     0.30000613     0.50384664     0.01679830     0.16762275    -0.68987586

               MO   17        MO   18

  occ(*)=     0.00000000     0.00000000

   1O1s       0.24493337    -0.29526876
   2O1s       1.01964693    -1.08758147
   3O1s      -1.23950270     2.60580458
   4O1pz      0.30264959     0.24104935
   5O1pz     -0.40410087     0.96687641
   6O1d0      0.02495065     0.34308912
   7O1d2+     0.42569181    -0.01301711
   8C1s       0.14130902    -0.40500327
   9C1s       0.73980407    -1.85766607
  10C1s       0.73196915     0.10109539
  11C1pz      0.57354232     1.31587081
  12C1pz     -0.24838708    -0.21243200
  13C1d0     -0.12887993    -0.12628849
  14C1d2+    -0.41231160     0.04001669
  15H1s      -1.12888797     0.16784330
  16H1s       0.35553130    -0.03140312
  17H1py      0.29975210    -0.08077436
  18H1pz      0.52463656    -0.03026171

          natural orbitals of the final iteration, block  2

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7

  occ(*)=     2.00000000     0.50063694     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

  19O1px      0.76388742    -0.61687602    -0.03075787    -1.35530243     0.11997471    -0.33983236    -0.00876029
  20O1px     -0.00365570    -0.12332628    -0.11673464     1.88440114     0.47195677     0.26433869     0.38524388
  21O1d1+     0.02810360     0.01076721    -0.03988589    -0.02660697    -0.09516781     0.08009362     1.09645853
  22C1px      0.46174391     0.78423283    -1.47989997     0.03661527    -0.02710718    -0.31677312    -0.30089769
  23C1px     -0.02836622     0.21217077     1.82591216    -0.44045027    -0.44134043    -0.18826390    -0.00973950
  24C1d1+    -0.04898927     0.02019691     0.03772519     0.12398390     0.85993740    -0.67825191     0.63064463
  25H1px      0.00949893     0.02116772    -0.03958657    -0.10141733     0.31002614     0.75620829    -0.08368951

          natural orbitals of the final iteration, block  3

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8

  occ(*)=     2.00000000     1.49936306     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

  26O1py      0.30697145     0.88095885     0.13648244     0.46704821    -0.05918404    -1.19761131     0.32895453    -0.53306205
  27O1py     -0.03110237     0.04872248     0.24576139     0.13745303     0.00593721     2.01573574    -0.71073524     0.36476227
  28O1d1-     0.01809400     0.01539900    -0.01039516    -0.00703863     0.03194178    -0.05081461     0.02520364     0.19672215
  29C1py      0.63329345    -0.15844456    -0.52333517     0.42905571     1.24493606     0.34306384    -0.14912273     0.28041682
  30C1py     -0.14421427     0.05476504    -1.18222195    -2.23163132    -2.18490002    -1.39961021     1.45685433    -0.30351678
  31C1d1-    -0.00178548    -0.07012527    -0.02238473     0.08792458    -0.35784378     0.42973033     0.16044678    -0.79752040
  32H1s       0.49745553    -0.32968022    -0.09603166     1.21062793    -0.58561074    -0.08524021    -0.78086656     0.71747069
  33H1s      -0.14633274     0.01570829     1.91200724    -0.02343879     1.72180557     0.63935330    -0.05301659    -0.59891320
  34H1py     -0.01707304     0.00178390    -0.01277177    -0.04282165    -0.18057126    -0.03168367    -0.49455580     0.07876151
  35H1pz     -0.01365259     0.00609398    -0.01293781     0.05769346    -0.15493156     0.03634113     0.48434825     0.48857073

               MO    9        MO   10

  occ(*)=     0.00000000     0.00000000

  26O1py      0.30446268     0.12067626
  27O1py     -0.35753664     0.36730876
  28O1d1-    -0.21079584     1.10677116
  29C1py      1.94275982     0.41424944
  30C1py      0.20563286    -0.20914702
  31C1d1-     1.09297471     1.22866577
  32H1s      -1.86710062    -0.99902945
  33H1s       0.27535588     0.53446707
  34H1py      0.93213629     0.44076468
  35H1pz      0.56815020     0.00084139

          natural orbitals of the final iteration, block  4

               MO    1        MO    2        MO    3

  occ(*)=     0.00000000     0.00000000     0.00000000

  36O1d2-     0.04146652    -0.17670256     0.99170883
  37C1d2-     0.65102036    -0.92409844    -0.34458687
  38H1px      0.34616598     0.73953721     0.17202144
 d1(*), fmc(*), and qmc(*) written to the 1-particle density matrix file.
         81 d2(*) elements written to the 2-particle density matrix file.
 !timer: writing the mc density files requser+sys=     0.000 walltime=     0.000


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!


                        A1  partial gross atomic populations
   ao class       1A1        2A1        3A1        4A1        5A1        6A1 
    O1_ s       1.999660  -0.000242   1.374922   0.243847   0.174181   0.000000
    O1_ p       0.000003  -0.000420   0.093234   0.273207   1.037891   0.000000
    O1_ d       0.000000   0.000005   0.003315   0.001409   0.005286   0.000000
    C1_ s       0.000255   2.000410   0.299292   0.772000   0.046517   0.000000
    C1_ p       0.000237   0.000002   0.181004   0.155686   0.477231   0.000000
    C1_ d      -0.000155   0.000001   0.027105   0.002809   0.005325   0.000000
    H1_ s       0.000000   0.000410   0.016536   0.524537   0.245812   0.000000
    H1_ p       0.000000  -0.000166   0.004592   0.026505   0.007757   0.000000

   ao class       7A1        8A1        9A1       10A1       11A1       12A1 

   ao class      13A1       14A1       15A1       16A1       17A1       18A1 

                        B1  partial gross atomic populations
   ao class       1B1        2B1        3B1        4B1        5B1        6B1 
    O1_ p       1.374581   0.139226   0.000000   0.000000   0.000000   0.000000
    O1_ d       0.006198   0.000758   0.000000   0.000000   0.000000   0.000000
    C1_ p       0.583326   0.351774   0.000000   0.000000   0.000000   0.000000
    C1_ d       0.030934   0.002885   0.000000   0.000000   0.000000   0.000000
    H1_ p       0.004961   0.005993   0.000000   0.000000   0.000000   0.000000

   ao class       7B1 

                        B2  partial gross atomic populations
   ao class       1B2        2B2        3B2        4B2        5B2        6B2 
    O1_ p       0.252299   1.203226   0.000000   0.000000   0.000000   0.000000
    O1_ d       0.004085   0.000171   0.000000   0.000000   0.000000   0.000000
    C1_ p       0.935736   0.019796   0.000000   0.000000   0.000000   0.000000
    C1_ d      -0.000349   0.056312   0.000000   0.000000   0.000000   0.000000
    H1_ s       0.793668   0.220124   0.000000   0.000000   0.000000   0.000000
    H1_ p       0.014560  -0.000266   0.000000   0.000000   0.000000   0.000000

   ao class       7B2        8B2        9B2       10B2 

                        A2  partial gross atomic populations
   ao class       1A2        2A2        3A2 


                        gross atomic populations
     ao           O1_        C1_        H1_
      s         3.792368   3.118474   1.801085
      p         4.373247   2.704793   0.063937
      d         0.021228   0.124868   0.000000
    total       8.186844   5.948134   1.865022


 Total number of electrons:   16.00000000

 !timer: mcscf                           user+sys=     0.090 walltime=     0.000
