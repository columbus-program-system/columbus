1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -113.8898098757  2.3996E-02  4.1346E-01  1.1266E+00  1.0000E-03
 mr-sdci #  2  1   -114.1849872111  2.9518E-01  2.1909E-02  2.6129E-01  1.0000E-03
 mr-sdci #  3  1   -114.1993172554  1.4330E-02  2.2869E-03  6.9356E-02  1.0000E-03
 mr-sdci #  4  1   -114.2008201161  1.5029E-03  3.0887E-04  3.0112E-02  1.0000E-03
 mr-sdci #  5  1   -114.2006585283 -1.6159E-04  1.0906E-04  1.3667E-02  1.0000E-03
 mr-sdci #  6  1   -114.2005199964 -1.3853E-04  2.4959E-05  8.0309E-03  1.0000E-03
 mr-sdci #  7  1   -114.2004038674 -1.1613E-04  1.2148E-05  4.4237E-03  1.0000E-03
 mr-sdci #  8  1   -114.2003065576 -9.7310E-05  3.4296E-06  2.8330E-03  1.0000E-03
 mr-sdci #  9  1   -114.2002536411 -5.2916E-05  1.8970E-06  1.7600E-03  1.0000E-03
 mr-sdci # 10  1   -114.2002078946 -4.5746E-05  5.7782E-07  1.1368E-03  1.0000E-03
 mr-sdci # 11  1   -114.2001851758 -2.2719E-05  3.3270E-07  7.4409E-04  1.0000E-03

 mr-sdci  convergence criteria satisfied after 11 iterations.

 final mr-sdci  convergence information:
 mr-sdci # 11  1   -114.2001851758 -2.2719E-05  3.3270E-07  7.4409E-04  1.0000E-03

 number of reference csfs (nref) is     2.  root number (iroot) is  1.

 eref      =   -113.885628197201   "relaxed" cnot**2         =   0.905930759191
 eci       =   -114.200185175818   deltae = eci - eref       =  -0.314556978617
 eci+dv1   =   -114.229775311987   dv1 = (1-cnot**2)*deltae  =  -0.029590136170
 eci+dv2   =   -114.232847866503   dv2 = dv1 / cnot**2       =  -0.032662690685
 eci+dv3   =   -114.236632446072   dv3 = dv1 / (2*cnot**2-1) =  -0.036447270254
 eci+pople =   -114.231135875882   ( 16e- scaled deltae )    =  -0.345507678682
