1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -113.7332219980  3.7803E-03  4.1354E-01  1.0957E+00  1.0000E-03
 mr-sdci #  2  1   -114.0145817862  2.8136E-01  3.8450E-02  3.2538E-01  1.0000E-03
 mr-sdci #  3  1   -114.0416591682  2.7077E-02  4.4436E-03  9.7739E-02  1.0000E-03
 mr-sdci #  4  1   -114.0440693049  2.4101E-03  5.8018E-04  3.9746E-02  1.0000E-03
 mr-sdci #  5  1   -114.0446380890  5.6878E-04  1.9507E-04  1.8151E-02  1.0000E-03
 mr-sdci #  6  1   -114.0447490049  1.1092E-04  4.0637E-05  1.0108E-02  1.0000E-03
 mr-sdci #  7  1   -114.0448063984  5.7394E-05  1.8923E-05  5.3968E-03  1.0000E-03
 mr-sdci #  8  1   -114.0448199614  1.3563E-05  4.5785E-06  3.2599E-03  1.0000E-03
 mr-sdci #  9  1   -114.0448316242  1.1663E-05  2.4008E-06  1.9014E-03  1.0000E-03
 mr-sdci # 10  1   -114.0448344741  2.8500E-06  6.1836E-07  1.1730E-03  1.0000E-03
 mr-sdci # 11  1   -114.0448378954  3.4213E-06  3.4169E-07  7.1792E-04  1.0000E-03

 mr-sdci  convergence criteria satisfied after 11 iterations.

 final mr-sdci  convergence information:
 mr-sdci # 11  1   -114.0448378954  3.4213E-06  3.4169E-07  7.1792E-04  1.0000E-03

 number of reference csfs (nref) is     1.  root number (iroot) is  1.

 eref      =   -113.733423979281   "relaxed" cnot**2         =   0.899306072172
 eci       =   -114.044837895443   deltae = eci - eref       =  -0.311413916162
 eci+dv1   =   -114.076195385842   dv1 = (1-cnot**2)*deltae  =  -0.031357490399
 eci+dv2   =   -114.079706436164   dv2 = dv1 / cnot**2       =  -0.034868540722
 eci+dv3   =   -114.084102876098   dv3 = dv1 / (2*cnot**2-1) =  -0.039264980655
 eci+pople =   -114.078090614649   ( 16e- scaled deltae )    =  -0.344666635368
