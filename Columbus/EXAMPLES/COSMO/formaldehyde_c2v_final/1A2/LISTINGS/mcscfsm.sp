 total ao core energy =   31.165309824
 MCSCF calculation performed for  2 DRTs.

 DRT  first state   no.of aver. states   weights
  1   ground state          1             0.500
  2   ground state          1             0.500

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a1 
 Total number of electrons:    16
 Spin multiplicity:            1
 Number of active orbitals:    2
 Number of active electrons:   2
 Total number of CSFs:         2

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a2 
 Total number of electrons:    16
 Spin multiplicity:            1
 Number of active orbitals:    2
 Number of active electrons:   2
 Total number of CSFs:         1

 Number of active-double rotations:      2
 Number of active-active rotations:      0
 Number of double-virtual rotations:    78
 Number of active-virtual rotations:    13

 iter=    1 emc= -113.7976276049 demc= 1.1380E+02 wnorm= 9.3534E-07 knorm= 5.0332E-07 apxde= 1.6047E-13    *not converged* 

 final mcscf convergence values:
 iter=    2 emc= -113.7976276049 demc= 1.7053E-13 wnorm= 3.4397E-07 knorm= 3.3693E-08 apxde= 5.9512E-15    *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 weight 0.500000 total energy= -113.865813508
   DRT #2 state # 1 weight 0.500000 total energy= -113.729441701
   ------------------------------------------------------------


