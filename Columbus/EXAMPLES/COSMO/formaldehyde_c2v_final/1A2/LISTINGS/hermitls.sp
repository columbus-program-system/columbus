
 DALTON: user specified work memory size used,
          environment variable WRKMEM = "100000000           "

 Work memory size (LMWORK) :   100000000 =  762.94 megabytes.

 Default basis set library used :
        /sphome/kedziora/dalton/basis/                              


    $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    $$$$$$$$$$$  DALTON - An electronic structure program  $$$$$$$$$$$
    $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

               This is output from DALTON (beta-version 0.9) 

                          Principal authors:

            Trygve Helgaker,     University of Oslo,        Norway 
            Hans Joergen Jensen, University of Odense,      Denmark
            Poul Joergensen,     University of Aarhus,      Denmark
            Henrik Koch,         University of Aarhus,      Denmark
            Jeppe Olsen,         University of Lund,        Sweden 
            Hans Aagren,         University of Linkoeping,  Sweden 

                          Contributors:

            Torgeir Andersen,    University of Oslo,        Norway 
            Keld L. Bak,         University of Copenhagen,  Denmark
            Vebjoern Bakken,     University of Oslo,        Norway 
            Ove Christiansen,    University of Aarhus,      Denmark
            Paal Dahle,          University of Oslo,        Norway 
            Erik K. Dalskov,     University of Odense,      Denmark
            Thomas Enevoldsen,   University of Odense,      Denmark
            Asger Halkier,       University of Aarhus,      Denmark
            Hanne Heiberg,       University of Oslo,        Norway 
            Dan Jonsson,         University of Linkoeping,  Sweden 
            Sheela Kirpekar,     University of Odense,      Denmark
            Rika Kobayashi,      University of Aarhus,      Denmark
            Alfredo S. de Meras, Valencia University,       Spain  
            Kurt Mikkelsen,      University of Aarhus,      Denmark
            Patrick Norman,      University of Linkoeping,  Sweden 
            Martin J. Packer,    University of Sheffield,   UK     
            Kenneth Ruud,        University of Oslo,        Norway 
            Trond Saue,          University of Oslo,        Norway 
            Peter Taylor,        San Diego Superc. Center,  USA    
            Olav Vahtras,        University of Linkoeping,  Sweden

                                             Release Date:  August 1996
------------------------------------------------------------------------


      
     NOTE:
      
     This is an experimental code for the evaluation of molecular
     properties using (MC)SCF/CC wave functions. The authors accept
      no responsibility for the performance of the code or for the
     correctness of the results.
      
     The code (in whole or part) is not to be reproduced for further
     distribution without the written permission of T. Helgaker,
     H. J. Aa. Jensen or P. Taylor.
      
     If results obtained with this code are published, an
     appropriate citation would be:
      
     T. Helgaker, H. J. Aa. Jensen, P.Joergensen, H. Koch,
     J. Olsen, H. Aagren, T. Andersen, K. L. Bak, V. Bakken,
     O. Christiansen, P. Dahle, E. K. Dalskov, T. Enevoldsen,
     A. Halkier, H. Heiberg, D. Jonsson, S. Kirpekar, R. Kobayashi,
     A. S. de Meras, K. V. Mikkelsen, P. Norman, M. J. Packer,
     K. Ruud, T.Saue, P. R. Taylor, and O. Vahtras:
     DALTON, an electronic structure program"



     ******************************************
     **    PROGRAM:              DALTON      **
     **    PROGRAM VERSION:      5.4.0.0     **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************



 <<<<<<<<<< OUTPUT FROM GENERAL INPUT PROCESSING >>>>>>>>>>




 Default print level:        0

    Integral sections will be executed
    Starting in Integral Section -



 *************************************************************************
 ****************** Output from HERMIT input processing ******************
 *************************************************************************



 Default print level:        2


 Calculation of one- and two-electron Hamiltonian integrals.



 Changes of defaults for READIN:
 -------------------------------


 Maximum number of primitives per integral block :    9



 *************************************************************************
 ****************** Output from READIN input processing ******************
 *************************************************************************



  Title Cards
  -----------

                                                                          
                                                                          


  Symmetry Operations
  -------------------

  Symmetry operations: 2



                      SYMGRP:Point group information
                      ------------------------------

Point group: C2v

   * The point group was generated by:

      Reflection in the yz-plane
      Reflection in the xz-plane

   * Group multiplication table

        |  E   C2z  Oxz  Oyz
   -----+--------------------
     E  |  E 
    C2z | C2z   E 
    Oxz | Oxz  Oyz   E 
    Oyz | Oyz  Oxz  C2z   E 

   * Character table

        |  E   C2z  Oxz  Oyz
   -----+--------------------
    A1  |   1    1    1    1
    B1  |   1   -1    1   -1
    B2  |   1   -1   -1    1
    A2  |   1    1   -1   -1

   * Direct product table

        | A1   B1   B2   A2 
   -----+--------------------
    A1  | A1 
    B1  | B1   A1 
    B2  | B2   A2   A1 
    A2  | A2   B2   B1   A1 


  Atoms and basis sets
  --------------------

  Number of atom types:     3
  Total number of atoms:    4

  label    atoms   charge   prim    cont     basis   
  ----------------------------------------------------------------------
  O  1        1       8      26      14      [9s4p1d|3s2p1d]                        
  C  1        1       6      26      14      [9s4p1d|3s2p1d]                        
  H  1        2       1       7       5      [4s1p|2s1p]                            
  ----------------------------------------------------------------------
  ----------------------------------------------------------------------
  total:      4      16      66      38

  Spherical harmonic basis used.
  Threshold for integrals:  1.00E-15


  Cartesian Coordinates
  ---------------------

  Total number of coordinates: 12


   1   O  1     x      0.0000000000
   2            y      0.0000000000
   3            z     -2.2827900700

   4   C  1     x      0.0000000000
   5            y      0.0000000000
   6            z      0.0000000000

   7   H  1 1   x      0.0000000000
   8            y      1.7933377400
   9            z      1.1097510600

  10   H  1 2   x      0.0000000000
  11            y     -1.7933377400
  12            z      1.1097510600



  Symmetry Coordinates
  --------------------

  Number of coordinates in each symmetry:   4  3  4  1


  Symmetry 1

   1   O  1  z    3
   2   C  1  z    6
   3   H  1  y    [ 8  - 11 ]/2
   4   H  1  z    [ 9  + 12 ]/2


  Symmetry 2

   5   O  1  x    1
   6   C  1  x    4
   7   H  1  x    [ 7  + 10 ]/2


  Symmetry 3

   8   O  1  y    2
   9   C  1  y    5
  10   H  1  y    [ 8  + 11 ]/2
  11   H  1  z    [ 9  - 12 ]/2


  Symmetry 4

  12   H  1  x    [ 7  - 10 ]/2


   Interatomic separations (in Angstroms):
   ---------------------------------------

            O  1        C  1        H  1        H  2

   O  1    0.000000
   C  1    1.208000    0.000000
   H  1    2.030647    1.116000    0.000000
   H  2    2.030647    1.116000    1.897986    0.000000




  Bond distances (angstroms):
  ---------------------------

                  atom 1     atom 2                           distance
                  ------     ------                           --------
  bond distance:    C  1       O  1                           1.208000
  bond distance:    H  1       C  1                           1.116000
  bond distance:    H  2       C  1                           1.116000


  Bond angles (degrees):
  ----------------------

                  atom 1     atom 2     atom 3                   angle
                  ------     ------     ------                   -----
  bond angle:       H  1       C  1       O  1                 121.750
  bond angle:       H  2       C  1       O  1                 121.750
  bond angle:       H  2       C  1       H  1                 116.500


  Nuclear repulsion energy :   31.165309824426


  Orbital exponents and contraction coefficients
  ----------------------------------------------


  O  1   1s    1    11720.000000    0.0007 -0.0002  0.0000
   gen. cont.  2     1759.000000    0.0055 -0.0013  0.0000
               3      400.800000    0.0278 -0.0063  0.0000
               4      113.700000    0.1048 -0.0257  0.0000
               5       37.030000    0.2831 -0.0709  0.0000
               6       13.270000    0.4487 -0.1654  0.0000
               7        5.025000    0.2710 -0.1170  0.0000
               8        1.013000    0.0155  0.5574  0.0000
               9        0.302300   -0.0026  0.5728  1.0000

  O  1   2px  10       17.700000    0.0430  0.0000
   gen. cont. 11        3.854000    0.2289  0.0000
              12        1.046000    0.5087  0.0000
              13        0.275300    0.4605  1.0000

  O  1   2py  14       17.700000    0.0430  0.0000
   gen. cont. 15        3.854000    0.2289  0.0000
              16        1.046000    0.5087  0.0000
              17        0.275300    0.4605  1.0000

  O  1   2pz  18       17.700000    0.0430  0.0000
   gen. cont. 19        3.854000    0.2289  0.0000
              20        1.046000    0.5087  0.0000
              21        0.275300    0.4605  1.0000

  O  1   3d2- 22        1.185000    1.0000

  O  1   3d1- 23        1.185000    1.0000

  O  1   3d0  24        1.185000    1.0000

  O  1   3d1+ 25        1.185000    1.0000

  O  1   3d2+ 26        1.185000    1.0000

  C  1   1s   27     6665.000000    0.0007 -0.0001  0.0000
   gen. cont. 28     1000.000000    0.0053 -0.0012  0.0000
              29      228.000000    0.0271 -0.0057  0.0000
              30       64.710000    0.1017 -0.0233  0.0000
              31       21.060000    0.2747 -0.0640  0.0000
              32        7.495000    0.4486 -0.1500  0.0000
              33        2.797000    0.2851 -0.1273  0.0000
              34        0.521500    0.0152  0.5445  0.0000
              35        0.159600   -0.0032  0.5805  1.0000

  C  1   2px  36        9.439000    0.0381  0.0000
   gen. cont. 37        2.002000    0.2095  0.0000
              38        0.545600    0.5086  0.0000
              39        0.151700    0.4688  1.0000

  C  1   2py  40        9.439000    0.0381  0.0000
   gen. cont. 41        2.002000    0.2095  0.0000
              42        0.545600    0.5086  0.0000
              43        0.151700    0.4688  1.0000

  C  1   2pz  44        9.439000    0.0381  0.0000
   gen. cont. 45        2.002000    0.2095  0.0000
              46        0.545600    0.5086  0.0000
              47        0.151700    0.4688  1.0000

  C  1   3d2- 48        0.550000    1.0000

  C  1   3d1- 49        0.550000    1.0000

  C  1   3d0  50        0.550000    1.0000

  C  1   3d1+ 51        0.550000    1.0000

  C  1   3d2+ 52        0.550000    1.0000

  H  1#1 1s   53       13.010000    0.0197  0.0000
   gen. cont. 54        1.962000    0.1380  0.0000
              55        0.444600    0.4781  0.0000
              56        0.122000    0.5012  1.0000

  H  1#2 1s   57       13.010000    0.0197  0.0000
   gen. cont. 58        1.962000    0.1380  0.0000
              59        0.444600    0.4781  0.0000
              60        0.122000    0.5012  1.0000

  H  1#1 2px  61        0.727000    1.0000

  H  1#2 2px  62        0.727000    1.0000

  H  1#1 2py  63        0.727000    1.0000

  H  1#2 2py  64        0.727000    1.0000

  H  1#1 2pz  65        0.727000    1.0000

  H  1#2 2pz  66        0.727000    1.0000


  Contracted Orbitals
  -------------------

   1  O  1    1s       1     2     3     4     5     6     7     8     9
   2  O  1    1s       1     2     3     4     5     6     7     8     9
   3  O  1    1s       9
   4  O  1    2px     10    11    12    13
   5  O  1    2py     14    15    16    17
   6  O  1    2pz     18    19    20    21
   7  O  1    2px     13
   8  O  1    2py     17
   9  O  1    2pz     21
  10  O  1    3d2-    22
  11  O  1    3d1-    23
  12  O  1    3d0     24
  13  O  1    3d1+    25
  14  O  1    3d2+    26
  15  C  1    1s      27    28    29    30    31    32    33    34    35
  16  C  1    1s      27    28    29    30    31    32    33    34    35
  17  C  1    1s      35
  18  C  1    2px     36    37    38    39
  19  C  1    2py     40    41    42    43
  20  C  1    2pz     44    45    46    47
  21  C  1    2px     39
  22  C  1    2py     43
  23  C  1    2pz     47
  24  C  1    3d2-    48
  25  C  1    3d1-    49
  26  C  1    3d0     50
  27  C  1    3d1+    51
  28  C  1    3d2+    52
  29  H  1#1  1s    53  54  55  56
  30  H  1#2  1s    57  58  59  60
  31  H  1#1  1s    56
  32  H  1#2  1s    60
  33  H  1#1  2px   61
  34  H  1#2  2px   62
  35  H  1#1  2py   63
  36  H  1#2  2py   64
  37  H  1#1  2pz   65
  38  H  1#2  2pz   66




  Symmetry Orbitals
  -----------------

  Number of orbitals in each symmetry:        18  7 10  3


  Symmetry  A1 ( 1)

    1     O  1     1s         1
    2     O  1     1s         2
    3     O  1     1s         3
    4     O  1     2pz        6
    5     O  1     2pz        9
    6     O  1     3d0       12
    7     O  1     3d2+      14
    8     C  1     1s        15
    9     C  1     1s        16
   10     C  1     1s        17
   11     C  1     2pz       20
   12     C  1     2pz       23
   13     C  1     3d0       26
   14     C  1     3d2+      28
   15     H  1     1s        29  +  30
   16     H  1     1s        31  +  32
   17     H  1     2py       35  -  36
   18     H  1     2pz       37  +  38


  Symmetry  B1 ( 2)

   19     O  1     2px        4
   20     O  1     2px        7
   21     O  1     3d1+      13
   22     C  1     2px       18
   23     C  1     2px       21
   24     C  1     3d1+      27
   25     H  1     2px       33  +  34


  Symmetry  B2 ( 3)

   26     O  1     2py        5
   27     O  1     2py        8
   28     O  1     3d1-      11
   29     C  1     2py       19
   30     C  1     2py       22
   31     C  1     3d1-      25
   32     H  1     1s        29  -  30
   33     H  1     1s        31  -  32
   34     H  1     2py       35  +  36
   35     H  1     2pz       37  -  38


  Symmetry  A2 ( 4)

   36     O  1     3d2-      10
   37     C  1     3d2-      24
   38     H  1     2px       33  -  34

  Symmetries of electric field:  B1 (2)  B2 (3)  A1 (1)

  Symmetries of magnetic field:  B2 (3)  B1 (2)  A2 (4)


 Copy of input to READIN
 -----------------------

INTGRL                                                                          
                                                                                
                                                                                
s   3    2X   Y      0.10E-14                                                   
       8.0    1    3    1    1    1                                             
O  1   0.000000000000000   0.000000000000000  -2.282790070000000       *        
H   9   3                                                                       
      11720.00000000         0.00071000        -0.00016000         0.00000000   
       1759.00000000         0.00547000        -0.00126300         0.00000000   
        400.80000000         0.02783700        -0.00626700         0.00000000   
        113.70000000         0.10480000        -0.02571600         0.00000000   
         37.03000000         0.28306200        -0.07092400         0.00000000   
         13.27000000         0.44871900        -0.16541100         0.00000000   
          5.02500000         0.27095200        -0.11695500         0.00000000   
          1.01300000         0.01545800         0.55736800         0.00000000   
          0.30230000        -0.00258500         0.57275900         1.00000000   
H   4   2                                                                       
         17.70000000         0.04301800         0.00000000                      
          3.85400000         0.22891300         0.00000000                      
          1.04600000         0.50872800         0.00000000                      
          0.27530000         0.46053100         1.00000000                      
H   1   1                                                                       
          1.18500000         1.00000000                                         
       6.0    1    3    1    1    1                                             
C  1   0.000000000000000   0.000000000000000   0.000000000000000       *        
H   9   3                                                                       
       6665.00000000         0.00069200        -0.00014600         0.00000000   
       1000.00000000         0.00532900        -0.00115400         0.00000000   
        228.00000000         0.02707700        -0.00572500         0.00000000   
         64.71000000         0.10171800        -0.02331200         0.00000000   
         21.06000000         0.27474000        -0.06395500         0.00000000   
          7.49500000         0.44856400        -0.14998100         0.00000000   
          2.79700000         0.28507400        -0.12726200         0.00000000   
          0.52150000         0.01520400         0.54452900         0.00000000   
          0.15960000        -0.00319100         0.58049600         1.00000000   
H   4   2                                                                       
          9.43900000         0.03810900         0.00000000                      
          2.00200000         0.20948000         0.00000000                      
          0.54560000         0.50855700         0.00000000                      
          0.15170000         0.46884200         1.00000000                      
H   1   1                                                                       
          0.55000000         1.00000000                                         
       1.0    1    2    1    1                                                  
H  1   0.000000000000000   1.793337740000000   1.109751060000000       *        
H   4   2                                                                       
         13.01000000         0.01968500         0.00000000                      
          1.96200000         0.13797700         0.00000000                      
          0.44460000         0.47814800         0.00000000                      
          0.12200000         0.50124000         1.00000000                      
H   1   1                                                                       
          0.72700000         1.00000000                                         


 herdrv: noofopt= 0


 ************************************************************************
 ************************** Output from HERONE **************************
 ************************************************************************

 prop, itype F 1


   200 atomic overlap integrals written in   1 buffers.
 Percentage non-zero integrals:  26.99
 prop, itype F 2


   260 one-el. Hamil. integrals written in   1 buffers.
 Percentage non-zero integrals:  35.09
 prop, itype F 3


   200 kinetic energy integrals written in   1 buffers.
 Percentage non-zero integrals:  26.99




 ************************************************************************
 ************************** Output from TWOINT **************************
 ************************************************************************

 calling sifew2:luinta,info,num,last,nrec
 calling sifew2: 11 2 4096 3272 4096 2730 2199 2 25

 Number of two-electron integrals written:     70449 (25.6%)
 Kilobytes written:                              852




 >>>> Total CPU  time used in HERMIT:   0.17 seconds
 >>>> Total wall time used in HERMIT:   0.00 seconds

- End of Integral Section
