1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      09/20/00    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 using llenci= -1
================================================================================
 nsubmx= 6 lenci= 6339
global arrays:        82407   (              0.63 MB)
vdisk:                    0   (              0.00 MB)
drt:                 252658   (              0.96 MB)
================================================================================
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core            9999999 DP per process
 CIUDG version 5.9.4 (29-Aug-2003)
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 0,
  GSET = 0,
   DAVCOR =10,
  NCOREL = 10
  NROOT = 1
  IVMODE = 3
  NBKITR = 0
  NVBKMN = 1
  NVBKMX = 6
  RTOLBK = 1e-3,
  NITER = 20
  NVCIMN = 3
  NVCIMX = 6
  RTOLCI = 1e-3,
  IDEN  = 1
  CSFPRN = 10,
  lvlprt = 5
  frcsub = 2
  cosmocalc = 1
  /end
 ------------------------------------------------------------------------

 ** list of control variables **
 nrfitr =   30      nvrfmx =    6      nvrfmn =    3
 lvlprt =    5      nroot  =    1      noldv  =    0      noldhv =    0
 nunitv =    1      ntype  =    0      nbkitr =    0      niter  =   20
 ivmode =    3      vout   =    0      istrt  =    0      iortls =    0
 nvbkmx =    6      ibktv  =   -1      ibkthv =   -1      frcsub =    2
 nvcimx =    6      icitv  =   -1      icithv =   -1      maxseg =    4
 iden   =    1      itran  =    0      froot  =    0      rtmode =    0
 ftcalc =    1      lrtshift=1.0000    ncouple=    1      skipso  =   F
 ncorel =   10      csfprn  =  10      ctol   = 1.00E-02  davcor  =  10


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-03    1.000E-03

 units and filenames:
    1: ( 6)    ciudgls                                                     
    2: ( 5)    ciudgin                                                     
    3: ( 7)    ciudgsm                                                     
    4: ( 4)    cihdiag                                                     
    5: ( 3)    cihpseu                                                     
    6: (10)    cihvfl                                                      
    7: (11)    civfl                                                       
    8: (12)    diagint                                                     
    9: (13)    ofdgint                                                     
   10: (17)    cidrtfl                                                     
   11: (20)    ciftdfl                                                     
   12: (21)    ciftofl                                                     
   13: (24)    ciftifl                                                     
   14: ( 8)    ciflind                                                     
   15: (15)    civin                                                       
   16: (16)    civout                                                      
   18: (18)    d1fl                                                        
   20: (53)    cifvfl                                                      
   21: (25)    ciscr4                                                      
   22: (41)    ciftotd                                                     
   23: (42)    flacpfd                                                     
   26: (26)    ciscr5                                                      
   27: (47)    restart                                                     
   28: (48)    civout                                                      
   29: (49)    cirefv                                                      
   31: (31)    fil4w                                                       
   32: (32)    fil4x                                                       
   33: (33)    fil3w                                                       
   34: (34)    fil3x                                                       
   36: (36)    aoints                                                      
   37: (37)    aoints2                                                     
   38: (38)    drtfil                                                      
   39: (50)    cisrtif                                                     
   42: (50)    mocoef                                                      
   45: (52)    nocoef_ci                                                   
   46: (53)    civfl_restart                                               
   54: (54)    civcosmo                                                    
 ------------------------------------------------------------------------

 workspace allocation information: lcore=   9999999 mem1=1108566024 ifirst=-264081830

 integral file titles:
 Hermit Integral Program : SIFS version  hochtor2        Thu Oct  7 15:12:41 2004
  cidrt_title                                                                    
  title                                                                          
 mofmt: formatted orbitals label=morbl   hochtor2        Thu Oct  7 15:12:44 2004
 SIFS file created by program tran.      hochtor2        Thu Oct  7 15:12:44 2004

 core energy values from the integral file:
 energy( 1)=  9.317913916904E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total core repulsion energy =  9.317913916904E+00

 drt header information:
  cidrt_title                                                                    
 spnorb, spnodd, lxyzir,hmult F F 0 0 0 0
 nmot  =    48 niot  =     5 nfct  =     0 nfvt  =     0
 nrow  =    20 nsym  =     4 ssym  =     1 lenbuf=  1600
 nwalk,xbar:         31        1        5       10       15
 nvalwt,nvalw:       31        1        5       10       15
 ncsft:            6339
 total number of valid internal walks:      31
 nvalz,nvaly,nvalx,nvalw =        1       5      10      15

 cisrt info file parameters:
 file number  12 blocksize   4095
 mxbld   4095
 nd4ext,nd2ext,nd0ext  1892   430    30
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int   127551    56163    11805     1365       66        0        0        0
 minbl4,minbl3,maxbl2   882   837   885
 maxbuf 32767
 number of external orbitals per symmetry block:  17  13   8   5
 nmsym   4 number of internal orbitals   5

 formula file title:
 Hermit Integral Program : SIFS version  hochtor2        Thu Oct  7 15:12:41 2004
  cidrt_title                                                                    
  title                                                                          
 mofmt: formatted orbitals label=morbl   hochtor2        Thu Oct  7 15:12:44 2004
 SIFS file created by program tran.      hochtor2        Thu Oct  7 15:12:44 2004
  cidrt_title                                                                    
 file nmb.  24 block size     0
 pthz,pthy,pthx,pthw:     1     5    10    15 total internal walks:      31
 maxlp3,n3xtlp,n2xtlp,n1xtlp,n0xtlp,n2lp,n1lp,n0lp     0     0     0     0
 orbsym(*)= 1 1 1 2 3

 setref:        1 references kept,
                0 references were marked as invalid, out of
                1 total.
 limcnvrt: found 1 valid internal walksout of  1
  walks (skipping trailing invalids)
  ... adding  1 segmentation marks segtype= 1
 limcnvrt: found 5 valid internal walksout of  5
  walks (skipping trailing invalids)
  ... adding  5 segmentation marks segtype= 2
 limcnvrt: found 10 valid internal walksout of  10
  walks (skipping trailing invalids)
  ... adding  10 segmentation marks segtype= 3
 limcnvrt: found 15 valid internal walksout of  15
  walks (skipping trailing invalids)
  ... adding  15 segmentation marks segtype= 4
 nmb.of records 4-ext     1
 nmb.of records 3-ext     3
 nmb.of records 2-ext     1
 nmb.of records 1-ext     1
 nmb.of records 0-ext     0
 nmb.of records 2-int     0
 nmb.of records 1-int     0
 nmb.of records 0-int
 mxorb=  17

 < n-ex core usage >
     routines:
    fourex       70213
    threx        34637
    twoex         8785
    onex          4742
    allin         4095
    diagon        4697
               =======
   maximum       70213

  __ static summary __ 
   reflst            1
   hrfspc            1
               -------
   static->          1

  __ core required  __ 
   totstc            1
   max n-ex      70213
               -------
   totnec->      70214

  __ core available __ 
   totspc      9999999
   totnec -      70214
               -------
   totvec->    9929785

 number of external paths / symmetry
 vertex x     252     261     201     189
 vertex w     295     261     201     189
================ pruneseg on segment    1(   1)================
 original DRT
iwalk=   1step: 3 3 3 3 3
 level  0 through level  4 of the drt:

 row lev  l0  l1  l2  l3     y0      y1      y2    xbar 

  17  4   0   0   0  20       1       1       1       1
  18  4   0   0   0   0       0       0       0       0
  19  4   0   0   0   0       0       0       0       0
 ....................................

  13  3   0   0   0  17       1       1       1       1
  14  3   0   0   0   0       0       0       0       0
  15  3   0   0   0   0       0       0       0       0
  16  3   0   0   0   0       0       0       0       0
 ....................................

   9  2   0   0   0  13       1       1       1       1
  10  2   0   0   0   0       0       0       0       0
  11  2   0   0   0   0       0       0       0       0
  12  2   0   0   0   0       0       0       0       0
 ....................................

   5  1   0   0   0   9       1       1       1       1
   6  1   0   0   0   0       0       0       0       0
   7  1   0   0   0   0       0       0       0       0
   8  1   0   0   0   0       0       0       0       0
 ....................................

   1  0   0   0   0   5       1       1       1       1
   2  0   0   0   0   0       0       0       0       0
   3  0   0   0   0   0       0       0       0       0
   4  0   0   0   0   0       0       0       0       0
 ....................................


 lprune: l(*,*,*) pruned with nwalk=       1 nvalwt=       1 nprune=       0
 ynew( 1):
  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0
  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0
  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
 xbarnew( 1):
  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  1
 limn array= 1
  1
 limvec array= 1
  2
 reduced DRT,invalid= 0
iwalk=   1step: 3 3 3 3 3
 level  0 through level  4 of the drt:

 row lev  l0  l1  l2  l3     y0      y1      y2    xbar 

  17  4   0   0   0  20       1       1       1       1
  18  4   0   0   0   0       0       0       0       0
  19  4   0   0   0   0       0       0       0       0
 ....................................

  13  3   0   0   0  17       1       1       1       1
  14  3   0   0   0   0       0       0       0       0
  15  3   0   0   0   0       0       0       0       0
  16  3   0   0   0   0       0       0       0       0
 ....................................

   9  2   0   0   0  13       1       1       1       1
  10  2   0   0   0   0       0       0       0       0
  11  2   0   0   0   0       0       0       0       0
  12  2   0   0   0   0       0       0       0       0
 ....................................

   5  1   0   0   0   9       1       1       1       1
   6  1   0   0   0   0       0       0       0       0
   7  1   0   0   0   0       0       0       0       0
   8  1   0   0   0   0       0       0       0       0
 ....................................

   1  0   0   0   0   5       1       1       1       1
   2  0   0   0   0   0       0       0       0       0
   3  0   0   0   0   0       0       0       0       0
   4  0   0   0   0   0       0       0       0       0
 ....................................

================ pruneseg on segment    2(   2)================
 original DRT
iwalk=   2step: 3 3 3 3 2
iwalk=   3step: 3 3 3 2 3
iwalk=   4step: 3 3 2 3 3
iwalk=   5step: 3 2 3 3 3
iwalk=   6step: 2 3 3 3 3
 level  0 through level  4 of the drt:

 row lev  l0  l1  l2  l3     y0      y1      y2    xbar 

  17  4   0   0   0  20       1       1       1       1
  18  4   0   0  20   0       1       1       0       1
  19  4   0   0   0   0       0       0       0       0
 ....................................

  13  3   0   0   0  17       1       1       1       1
  14  3   0   0  17  18       2       2       1       2
  15  3   0   0   0   0       0       0       0       0
  16  3   0   0   0   0       0       0       0       0
 ....................................

   9  2   0   0   0  13       1       1       1       1
  10  2   0   0  13  14       3       3       2       3
  11  2   0   0   0   0       0       0       0       0
  12  2   0   0   0   0       0       0       0       0
 ....................................

   5  1   0   0   0   9       1       1       1       1
   6  1   0   0   9  10       4       4       3       4
   7  1   0   0   0   0       0       0       0       0
   8  1   0   0   0   0       0       0       0       0
 ....................................

   1  0   0   0   0   0       0       0       0       0
   2  0   0   0   5   6       6       6       5       5
   3  0   0   0   0   0       0       0       0       0
   4  0   0   0   0   0       0       0       0       0
 ....................................


 lprune: l(*,*,*) pruned with nwalk=       5 nvalwt=       4 nprune=       0
 ynew( 2):
  0  5  0  0  1  4  0  0  1  3  0  0  1  2  0  0  1  1  0  0
  0  5  0  0  1  4  0  0  1  3  0  0  1  2  0  0  1  1  0  0
  0  4  0  0  1  3  0  0  1  2  0  0  1  1  0  0  1  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
 xbarnew( 2):
  0  5  0  0  1  4  0  0  1  3  0  0  1  2  0  0  1  1  0  1
 limn array= 5
  1  2  3  4  5
 limvec array= 5
  0  2  2  2  2
 reduced DRT,invalid= 0
iwalk=   1step: 3 3 3 3 2
iwalk=   2step: 3 3 3 2 3
iwalk=   3step: 3 3 2 3 3
iwalk=   4step: 3 2 3 3 3
iwalk=   5step: 2 3 3 3 3
 level  0 through level  4 of the drt:

 row lev  l0  l1  l2  l3     y0      y1      y2    xbar 

  17  4   0   0   0  20       1       1       1       1
  18  4   0   0  20   0       1       1       0       1
  19  4   0   0   0   0       0       0       0       0
 ....................................

  13  3   0   0   0  17       1       1       1       1
  14  3   0   0  17  18       2       2       1       2
  15  3   0   0   0   0       0       0       0       0
  16  3   0   0   0   0       0       0       0       0
 ....................................

   9  2   0   0   0  13       1       1       1       1
  10  2   0   0  13  14       3       3       2       3
  11  2   0   0   0   0       0       0       0       0
  12  2   0   0   0   0       0       0       0       0
 ....................................

   5  1   0   0   0   9       1       1       1       1
   6  1   0   0   9  10       4       4       3       4
   7  1   0   0   0   0       0       0       0       0
   8  1   0   0   0   0       0       0       0       0
 ....................................

   1  0   0   0   0   0       0       0       0       0
   2  0   0   0   5   6       5       5       4       5
   3  0   0   0   0   0       0       0       0       0
   4  0   0   0   0   0       0       0       0       0
 ....................................

================ pruneseg on segment    3(   3)================
 original DRT
iwalk=   7step: 3 3 3 2 2
iwalk=   8step: 3 3 2 3 2
iwalk=   9step: 3 3 2 2 3
iwalk=  10step: 3 2 3 3 2
iwalk=  11step: 3 2 3 2 3
iwalk=  12step: 3 2 2 3 3
iwalk=  13step: 2 3 3 3 2
iwalk=  14step: 2 3 3 2 3
iwalk=  15step: 2 3 2 3 3
iwalk=  16step: 2 2 3 3 3
 level  0 through level  4 of the drt:

 row lev  l0  l1  l2  l3     y0      y1      y2    xbar 

  17  4   0   0   0  20       1       1       1       1
  18  4   0   0  20   0       1       1       0       1
  19  4  20   0   0   0       0       0       0       1
 ....................................

  13  3   0   0   0  17       1       1       1       1
  14  3   0   0  17  18       2       2       1       2
  15  3   0   0  18   0       1       1       0       1
  16  3  17  18   0  19       2       1       1       3
 ....................................

   9  2   0   0   0  13       1       1       1       1
  10  2   0   0  13  14       3       3       2       3
  11  2   0   0  14  15       3       3       1       3
  12  2  13  14   0  16       5       3       3       6
 ....................................

   5  1   0   0   0   9       1       1       1       1
   6  1   0   0   9  10       4       4       3       4
   7  1   0   0  10  11       6       6       3       6
   8  1   9  10   0  12       9       6       6      10
 ....................................

   1  0   0   0   0   0       0       0       0       0
   2  0   0   0   0   0       0       0       0       0
   3  0   0   0   6   7      16      16      12      10
   4  0   5   6   0   8      30      26      26      15
 ....................................


 lprune: l(*,*,*) pruned with nwalk=      10 nvalwt=      10 nprune=      14
 ynew( 3):
  0  0 10  0  0  4  6  0  1  3  3  0  1  2  1  0  1  1  0  0
  0  0 10  0  0  4  6  0  1  3  3  0  1  2  1  0  1  1  0  0
  0  0  6  0  0  3  3  0  1  2  1  0  1  1  0  0  1  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
 xbarnew( 3):
  0  0 10  0  0  4  6  0  1  3  3  0  1  2  1  0  1  1  0  1
 limn array= 10
  1  2  3  4  5  6  7  8  9 10
 limvec array= 10
  2  2  2  2  2  2  2  2  2  2
 reduced DRT,invalid= 0
iwalk=   1step: 3 3 3 2 2
iwalk=   2step: 3 3 2 3 2
iwalk=   3step: 3 3 2 2 3
iwalk=   4step: 3 2 3 3 2
iwalk=   5step: 3 2 3 2 3
iwalk=   6step: 3 2 2 3 3
iwalk=   7step: 2 3 3 3 2
iwalk=   8step: 2 3 3 2 3
iwalk=   9step: 2 3 2 3 3
iwalk=  10step: 2 2 3 3 3
 level  0 through level  4 of the drt:

 row lev  l0  l1  l2  l3     y0      y1      y2    xbar 

  17  4   0   0   0  20       1       1       1       1
  18  4   0   0  20   0       1       1       0       1
  19  4   0   0   0   0       0       0       0       0
 ....................................

  13  3   0   0   0  17       1       1       1       1
  14  3   0   0  17  18       2       2       1       2
  15  3   0   0  18   0       1       1       0       1
  16  3   0   0   0   0       0       0       0       0
 ....................................

   9  2   0   0   0  13       1       1       1       1
  10  2   0   0  13  14       3       3       2       3
  11  2   0   0  14  15       3       3       1       3
  12  2   0   0   0   0       0       0       0       0
 ....................................

   5  1   0   0   0   0       0       0       0       0
   6  1   0   0   9  10       4       4       3       4
   7  1   0   0  10  11       6       6       3       6
   8  1   0   0   0   0       0       0       0       0
 ....................................

   1  0   0   0   0   0       0       0       0       0
   2  0   0   0   0   0       0       0       0       0
   3  0   0   0   6   7      10      10       6      10
   4  0   0   0   0   0       0       0       0       0
 ....................................

================ pruneseg on segment    4(   3)================
 original DRT
iwalk=  17step: 3 3 3 3 0
iwalk=  18step: 3 3 3 1 2
iwalk=  19step: 3 3 3 0 3
iwalk=  20step: 3 3 1 3 2
iwalk=  21step: 3 3 1 2 3
iwalk=  22step: 3 3 0 3 3
iwalk=  23step: 3 1 3 3 2
iwalk=  24step: 3 1 3 2 3
iwalk=  25step: 3 1 2 3 3
iwalk=  26step: 3 0 3 3 3
iwalk=  27step: 1 3 3 3 2
iwalk=  28step: 1 3 3 2 3
iwalk=  29step: 1 3 2 3 3
iwalk=  30step: 1 2 3 3 3
iwalk=  31step: 0 3 3 3 3
 level  0 through level  4 of the drt:

 row lev  l0  l1  l2  l3     y0      y1      y2    xbar 

  17  4   0   0   0  20       1       1       1       1
  18  4   0   0  20   0       1       1       0       1
  19  4  20   0   0   0       0       0       0       1
 ....................................

  13  3   0   0   0  17       1       1       1       1
  14  3   0   0  17  18       2       2       1       2
  15  3   0   0  18   0       1       1       0       1
  16  3  17  18   0  19       2       1       1       3
 ....................................

   9  2   0   0   0  13       1       1       1       1
  10  2   0   0  13  14       3       3       2       3
  11  2   0   0  14  15       3       3       1       3
  12  2  13  14   0  16       5       3       3       6
 ....................................

   5  1   0   0   0   9       1       1       1       1
   6  1   0   0   9  10       4       4       3       4
   7  1   0   0  10  11       6       6       3       6
   8  1   9  10   0  12       9       6       6      10
 ....................................

   1  0   0   0   0   0       0       0       0       0
   2  0   0   0   0   0       0       0       0       0
   3  0   0   0   6   7      16      16      12      10
   4  0   5   6   0   8      30      26      26      15
 ....................................


 lprune: l(*,*,*) pruned with nwalk=      15 nvalwt=      15 nprune=       7
 ynew( 3):
  0  0  0 14  1  4  0  9  1  3  0  5  1  2  0  2  1  1  0  0
  0  0  0 10  1  4  0  6  1  3  0  3  1  2  0  1  1  1  0  0
  0  0  0 10  1  3  0  6  1  2  0  3  1  1  0  1  1  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
 xbarnew( 3):
  0  0  0 15  1  4  0 10  1  3  0  6  1  2  0  3  1  1  1  1
 limn array= 15
  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15
 limvec array= 15
  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2
 reduced DRT,invalid= 0
iwalk=   1step: 3 3 3 3 0
iwalk=   2step: 3 3 3 1 2
iwalk=   3step: 3 3 3 0 3
iwalk=   4step: 3 3 1 3 2
iwalk=   5step: 3 3 1 2 3
iwalk=   6step: 3 3 0 3 3
iwalk=   7step: 3 1 3 3 2
iwalk=   8step: 3 1 3 2 3
iwalk=   9step: 3 1 2 3 3
iwalk=  10step: 3 0 3 3 3
iwalk=  11step: 1 3 3 3 2
iwalk=  12step: 1 3 3 2 3
iwalk=  13step: 1 3 2 3 3
iwalk=  14step: 1 2 3 3 3
iwalk=  15step: 0 3 3 3 3
 level  0 through level  4 of the drt:

 row lev  l0  l1  l2  l3     y0      y1      y2    xbar 

  17  4   0   0   0  20       1       1       1       1
  18  4   0   0  20   0       1       1       0       1
  19  4  20   0   0   0       0       0       0       1
 ....................................

  13  3   0   0   0  17       1       1       1       1
  14  3   0   0  17  18       2       2       1       2
  15  3   0   0   0   0       0       0       0       0
  16  3  17  18   0  19       2       1       1       3
 ....................................

   9  2   0   0   0  13       1       1       1       1
  10  2   0   0  13  14       3       3       2       3
  11  2   0   0   0   0       0       0       0       0
  12  2  13  14   0  16       5       3       3       6
 ....................................

   5  1   0   0   0   9       1       1       1       1
   6  1   0   0   9  10       4       4       3       4
   7  1   0   0   0   0       0       0       0       0
   8  1   9  10   0  12       9       6       6      10
 ....................................

   1  0   0   0   0   0       0       0       0       0
   2  0   0   0   0   0       0       0       0       0
   3  0   0   0   0   0       0       0       0       0
   4  0   5   6   0   8      14      10      10      15
 ....................................




                   segmentation summary for type diagonal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1           1|         1|         0|         1|         0|         1|
 -------------------------------------------------------------------------------
  Y 2           5|        72|         1|         5|         1|         2|
 -------------------------------------------------------------------------------
  X 3          10|      2331|        73|        10|         6|         3|
 -------------------------------------------------------------------------------
  W 4          15|      3935|      2404|        15|        16|         4|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>      6339


 297 dimension of the ci-matrix ->>>      6339


 297 dimension of the ci-matrix ->>>         7

                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      10       1       2331          1      10       1
     2  4   1    25      two-ext wz   2X  4 1      15       1       3935          1      15       1
     3  4   3    26      two-ext wx   2X  4 3      15      10       3935       2331      15      10
     4  2   1    11      one-ext yz   1X  2 1       5       1         72          1       5       1
     5  3   2    15      1ex3ex  yx   3X  3 2      10       5       2331         72      10       5
     6  4   2    16      1ex3ex  yw   3X  4 2      15       5       3935         72      15       5
     7  1   1     1      allint zz    OX  1 1       1       1          1          1       1       1
     8  2   2     5      0ex2ex yy    OX  2 2       5       5         72         72       5       5
     9  3   3     6      0ex2ex xx    OX  3 3      10      10       2331       2331      10      10
    10  4   4     7      0ex2ex ww    OX  4 4      15      15       3935       3935      15      15
    11  1   1    75      dg-024ext z  DG  1 1       1       1          1          1       1       1
    12  2   2    45      4exdg024 y   DG  2 2       5       5         72         72       5       5
    13  3   3    46      4exdg024 x   DG  3 3      10      10       2331       2331      10      10
    14  4   4    47      4exdg024 w   DG  4 4      15      15       3935       3935      15      15
----------------------------------------------------------------------------------------------------
 blstr diagel,allext,nmin2=  1892
 onel.diag.
 -0.195250E+01-0.288212E+01-0.272196E+01-0.267855E+01-0.405715E+01-0.351474E+01-0.367102E+01-0.377150E+01-0.336383E+01-0.260424E+01
 -0.288328E+01-0.356689E+01-0.151821E+01-0.144239E+01-0.151507E+01-0.742166E+00 0.956191E+00-0.156247E+01-0.295038E+01-0.241061E+01
 -0.202169E+01-0.406951E+01-0.340168E+01-0.252146E+01-0.288551E+01-0.298755E+01-0.266149E+01-0.117686E+01-0.155286E+01-0.367604E+00
 -0.279220E+01-0.386196E+01-0.376653E+01-0.283644E+01-0.442956E+01-0.159430E+01-0.132938E+01-0.167298E+01-0.458594E+01-0.282029E+01
 -0.220929E+01-0.135337E+01-0.747871E+00-0.330428E+02-0.787921E+01-0.691286E+01-0.676135E+01-0.703818E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1      1892      1892
 blstr diagel,allext,nmin2=  1892
 onel.diag.
 -0.195250E+01-0.288212E+01-0.272196E+01-0.267855E+01-0.405715E+01-0.351474E+01-0.367102E+01-0.377150E+01-0.336383E+01-0.260424E+01
 -0.288328E+01-0.356689E+01-0.151821E+01-0.144239E+01-0.151507E+01-0.742166E+00 0.956191E+00-0.156247E+01-0.295038E+01-0.241061E+01
 -0.202169E+01-0.406951E+01-0.340168E+01-0.252146E+01-0.288551E+01-0.298755E+01-0.266149E+01-0.117686E+01-0.155286E+01-0.367604E+00
 -0.279220E+01-0.386196E+01-0.376653E+01-0.283644E+01-0.442956E+01-0.159430E+01-0.132938E+01-0.167298E+01-0.458594E+01-0.282029E+01
 -0.220929E+01-0.135337E+01-0.747871E+00-0.330428E+02-0.787921E+01-0.691286E+01-0.676135E+01-0.703818E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1      1892      1892
 blstr diagel,allext,nmin2=  1892
 onel.diag.
 -0.195250E+01-0.288212E+01-0.272196E+01-0.267855E+01-0.405715E+01-0.351474E+01-0.367102E+01-0.377150E+01-0.336383E+01-0.260424E+01
 -0.288328E+01-0.356689E+01-0.151821E+01-0.144239E+01-0.151507E+01-0.742166E+00 0.956191E+00-0.156247E+01-0.295038E+01-0.241061E+01
 -0.202169E+01-0.406951E+01-0.340168E+01-0.252146E+01-0.288551E+01-0.298755E+01-0.266149E+01-0.117686E+01-0.155286E+01-0.367604E+00
 -0.279220E+01-0.386196E+01-0.376653E+01-0.283644E+01-0.442956E+01-0.159430E+01-0.132938E+01-0.167298E+01-0.458594E+01-0.282029E+01
 -0.220929E+01-0.135337E+01-0.747871E+00-0.330428E+02-0.787921E+01-0.691286E+01-0.676135E+01-0.703818E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1      1892      1892
 DIAGEL COUNTS: DG0X,DG2X,DG4X= 440 145 30
 diagonal elements written to file   4

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        15 2x:         0 4x:         0
All internal counts: zz :         0 yy:         0 xx:         0 ww:         0
One-external counts: yz :         0 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         0    task #   2:         0    task #   3:         0    task #   4:         0
task #   5:         0    task #   6:         0    task #   7:        -1    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:        14    task #  12:         0
task #  13:         0    task #  14:         0    task #
 reference space has dimension       1

          hamiltonian matrix in the reference space 

              rcsf   1
 rcsf   1   -76.05366566

    root           eigenvalues
    ----           ------------
       1         -76.0536656576

          eigenvectors in the subspace basis

                v:   1
 rcsf   1     1.00000000

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt= 1

         vector  1 from unit 11 written to unit 49 filename cirefv                                                      
 ufvoutnew: ... writing  recamt= 1

         vector  1 from unit 11 written to unit 49 filename cirefv                                                      

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              6339
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                   20
 residual norm convergence criteria:               0.001000

 elast before the main iterative loop 
 elast =  -76.0536657

 rtflw before main loop =  0

          starting ci iteration   1

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.31791392

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= T F

################################################################################
 one electron density matrix calculation 
################################################################################


 workspace allocation information: mxcore= 9999997

 space required:

    space required for calls in multd2:
       onex          391
       allin           0
       diagon       1475
    max.      ---------
       maxnex       1475

    total core space usage:
       maxnex       1475
    max k-seg       3935
    max k-ind         15
              ---------
       totmax       9375

 total core array space available  9999997
 total used                           9375
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=       3  D0X=       9  D0W=      15
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================

          ci-one-electron density block   1

                mo   1         mo   2         mo   3         mo   4         mo   5         mo   6         mo   7         mo   8
   mo   1     2.00000000
   mo   2     0.00000000     2.00000000
   mo   3     0.00000000     0.00000000     2.00000000

                mo   9         mo  10         mo  11         mo  12         mo  13         mo  14         mo  15         mo  16

                mo  17         mo  18         mo  19         mo  20

          ci-one-electron density block   2

                mo  21         mo  22         mo  23         mo  24         mo  25         mo  26         mo  27         mo  28
   mo  21     2.00000000

                mo  29         mo  30         mo  31         mo  32         mo  33         mo  34

          ci-one-electron density block   3

                mo  35         mo  36         mo  37         mo  38         mo  39         mo  40         mo  41         mo  42
   mo  35     2.00000000

                mo  43

          ci-one-electron density block   4

                mo  44         mo  45         mo  46         mo  47         mo  48


*****   symmetry block SYM1   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       2.00000000
       2       0.00000000     2.00000000
       3       0.00000000     0.00000000     2.00000000

               Column   5     Column   6     Column   7     Column   8

               Column   9     Column  10     Column  11     Column  12

               Column  13     Column  14     Column  15     Column  16

               Column  17     Column  18     Column  19     Column  20

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7        eno   8
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    8   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno   9        eno  10        eno  11        eno  12        eno  13        eno  14        eno  15        eno  16
 emo    9   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   10   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   11   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   12   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   13   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   14   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   15   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   16   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno  17        eno  18        eno  19        eno  20
 emo   17   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   18   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   19   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   20   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   2.0000000      2.0000000      2.0000000      0.0000000      0.0000000
   0.0000000      0.0000000      0.0000000      0.0000000      0.0000000
   0.0000000      0.0000000      0.0000000      0.0000000      0.0000000
   0.0000000      0.0000000      0.0000000      0.0000000      0.0000000

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7         no   8
  mo    1   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo    2   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo    3   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
  mo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
  mo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
  mo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01
  mo   20   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

                no   9         no  10         no  11         no  12         no  13         no  14         no  15         no  16
  mo    8   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo    9   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo   10   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo   11   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo   12   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
  mo   13   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
  mo   14   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
  mo   15   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

                no  17         no  18         no  19         no  20
  mo   16   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
  mo   17   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
  mo   18   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
  mo   19   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01
  eigenvectors of nos in ao-basis, column    1
  0.5515823E-02  0.3143126E+00  0.2818983E-02  0.1277576E+00  0.8098635E+00 -0.1012754E-01 -0.1718911E-01 -0.1563787E-02
 -0.2289464E-02 -0.6151497E-02  0.6842722E-03 -0.2713131E-03  0.2306609E-02 -0.3392655E+00  0.1352449E+00  0.5903180E-02
 -0.2451740E-01  0.3077675E-02 -0.1694408E-02  0.2904306E-01
  eigenvectors of nos in ao-basis, column    2
  0.9986657E+00 -0.2886031E-02  0.1478624E-02  0.2374507E-02 -0.4737345E-02  0.3961596E-02  0.1318110E-02  0.5964829E-05
  0.1153723E-04 -0.2783492E-04 -0.2824841E-03  0.1186061E-04  0.4481920E-04  0.2151009E-02 -0.1958045E-02  0.9669327E-04
  0.6393350E-03  0.4291770E-03 -0.3107908E-03 -0.2453036E-03
  eigenvectors of nos in ao-basis, column    3
 -0.6317585E-02  0.8728930E+00 -0.7920069E-02 -0.1582526E+00 -0.1221160E+00  0.1246282E-01  0.7008680E-01  0.4727738E-03
  0.2945654E-02 -0.3281494E-03 -0.2745901E-02 -0.7883363E-03 -0.2399105E-02  0.3366056E+00 -0.1483694E+00  0.1543447E-02
  0.3237704E-01  0.1937093E-01  0.1397092E-01  0.3384409E-02
  eigenvectors of nos in ao-basis, column    4
 -0.3258136E+01 -0.7842233E+01  0.4083870E+01  0.5461683E+01 -0.6278669E+00  0.1320734E+01  0.1539460E+00  0.1206279E-02
  0.1378803E-01 -0.1176279E-01 -0.2424371E+00  0.4935770E-01  0.1290585E+00  0.1600846E+01 -0.1536540E+01  0.6590637E-01
  0.6208117E+00  0.4975527E+00 -0.3587064E+00 -0.3161000E+00
  eigenvectors of nos in ao-basis, column    5
 -0.1896548E-01 -0.1960165E+00  0.3045490E-02  0.2940520E+00  0.8348312E-01  0.4467293E-02 -0.9794704E-01 -0.2041547E-03
 -0.5607408E-03  0.1881129E-03  0.5091334E-02 -0.1447916E-03 -0.4914821E-03 -0.6416044E-02 -0.3049176E+00  0.6526054E+00
 -0.2390047E-01 -0.1113291E-01 -0.9143035E-01 -0.9101806E-01
  eigenvectors of nos in ao-basis, column    6
 -0.6933849E-02  0.2056387E+00 -0.3968204E-03  0.4719356E+00 -0.3299958E+00 -0.3771450E-02 -0.3865908E+00 -0.1271028E-04
 -0.1859061E-02  0.5061668E-02  0.8431468E-02 -0.9737074E-03 -0.3876942E-02 -0.1541303E+00 -0.1034181E+01  0.5511846E+00
 -0.1993430E-01 -0.1324493E-01  0.2299308E+00  0.2468760E+00
  eigenvectors of nos in ao-basis, column    7
  0.1334365E-01 -0.1014268E+00 -0.7957062E-02 -0.5296659E+00 -0.1742761E+00  0.5368161E-01 -0.3887280E+00 -0.2410368E-02
  0.3115320E-02  0.1097053E-02  0.3505297E-02 -0.1245167E-02 -0.1079863E-02  0.1340848E+00  0.2991939E+00 -0.8008037E-01
  0.9720424E-02 -0.7078118E-02 -0.3771643E+00  0.6893968E+00
  eigenvectors of nos in ao-basis, column    8
 -0.1038740E+00 -0.5268750E-01  0.3302977E-01  0.3145348E+01  0.7134985E-01 -0.2587084E-01 -0.4243236E+00 -0.3763911E-03
  0.1497330E-02 -0.5300462E-03  0.2541430E-01 -0.1971921E-02 -0.5238674E-02 -0.1074387E+00 -0.1221210E+01  0.1618867E+00
 -0.4011088E-01 -0.2159955E-01 -0.1082452E+01 -0.6994266E+00
  eigenvectors of nos in ao-basis, column    9
 -0.6046352E-01 -0.5189847E+00 -0.2810976E-01  0.9783263E+00  0.6259861E+00  0.2478947E-01 -0.9245004E+00 -0.2746654E-02
  0.2333137E-02  0.9384163E-02  0.1212225E+00 -0.4663612E-02 -0.1607161E-01  0.7859311E+00 -0.1305086E+01  0.2776766E+00
 -0.3137467E+00 -0.1328866E+00  0.2314745E+00  0.1318239E+00
  eigenvectors of nos in ao-basis, column   10
 -0.4436934E-01 -0.3848869E+00 -0.4547481E-02  0.7229718E+00 -0.3155865E+00 -0.5361571E+00  0.1879006E+01  0.3219334E-02
  0.4487940E-02  0.1362031E-01  0.7686124E-01 -0.2243167E-02 -0.6304388E-02  0.9024371E+00 -0.7437867E+00 -0.1381213E-01
 -0.7492460E-01 -0.1354680E+00 -0.4326634E-02 -0.4772989E+00
  eigenvectors of nos in ao-basis, column   11
  0.1212447E+00  0.5248185E+00 -0.1120275E-01 -0.1541380E+01 -0.2542175E+00 -0.1101477E-01  0.5194494E+00 -0.3118431E-02
  0.7723896E-02 -0.1174546E+00  0.5068847E-01  0.3329783E-02  0.4308658E-02  0.1954633E+00  0.2744301E+00 -0.3643862E-01
 -0.2171731E+00  0.4729939E+00  0.4150133E+00 -0.1270453E+00
  eigenvectors of nos in ao-basis, column   12
  0.4747217E+00  0.2094003E+01 -0.2527890E-01 -0.5525154E+01  0.1491890E+00  0.2477071E+00  0.3893550E+00 -0.6584408E-03
  0.3464144E-02  0.9329081E-01  0.2007740E+00  0.6369915E-02  0.9886964E-02  0.7880082E+00  0.9436755E+00 -0.1370044E+00
  0.2854254E+00  0.1170632E+00  0.5472427E+00  0.5341474E+00
  eigenvectors of nos in ao-basis, column   13
  0.5598679E+00  0.2455941E+01 -0.2030198E-01 -0.4417453E+01  0.8184541E-01 -0.3468412E+00  0.9048091E+00  0.9421415E-03
  0.7189466E-02 -0.7092601E-01 -0.3216104E+00 -0.1122974E-01 -0.2225644E-01  0.1145785E+01  0.1350716E+00 -0.6200774E-01
 -0.2992456E+00 -0.3102353E+00  0.4534710E+00  0.2800140E+00
  eigenvectors of nos in ao-basis, column   14
 -0.1137147E-01 -0.8594545E-01  0.5340616E-02 -0.3231910E+00  0.3577348E+00 -0.3609437E-02  0.5113291E+00  0.3742604E-02
 -0.2887268E-02 -0.3559932E+00  0.2271254E+00  0.7261592E-03 -0.1834002E-02  0.4373582E+00 -0.8560244E-01 -0.5257139E-01
  0.7160715E+00 -0.6488710E+00 -0.1361723E+00 -0.1601128E-01
  eigenvectors of nos in ao-basis, column   15
 -0.7481917E-01 -0.6400475E+00  0.9131747E-02 -0.3193046E+01  0.1024077E+01  0.1148806E+00  0.8311160E+00 -0.1425892E-02
  0.2181597E-01  0.1421880E-01 -0.5660696E+00 -0.3005103E-02  0.3314779E-02  0.3066280E+01 -0.8167482E+00 -0.1242603E+00
  0.8682959E+00  0.8734539E+00  0.1402273E+00  0.1352925E-02
  eigenvectors of nos in ao-basis, column   16
  0.8899255E-01  0.2451066E-01 -0.1385021E+00 -0.1996855E+01 -0.2957909E+01  0.3414853E+01  0.7385109E+00 -0.2121826E-02
  0.1539053E-01 -0.4649506E-01 -0.1778044E+00 -0.2750633E-01 -0.1400864E+00  0.1499659E+01 -0.4168014E+00 -0.1268201E-01
  0.5651871E+00  0.1788439E+00  0.9442474E-01  0.2439826E+00
  eigenvectors of nos in ao-basis, column   17
 -0.4178131E-02 -0.7020462E-02  0.2488846E-02  0.9755560E-01  0.2257976E+00 -0.2794828E+00 -0.1003540E+00  0.3956452E-01
 -0.2171891E-01  0.2297642E-01 -0.2286353E-01  0.3554663E+00 -0.2028487E+00 -0.8900179E-01  0.1825039E-01  0.7261508E-02
 -0.1373799E+00  0.1367059E+00  0.3846298E-01 -0.3374215E-01
  eigenvectors of nos in ao-basis, column   18
  0.3307499E+00  0.6952940E+00 -0.3608733E+00 -0.3199790E+01 -0.9073467E+00  0.1413382E+01  0.8465276E+00 -0.2470921E-01
 -0.3521651E+00 -0.1032994E-01 -0.8226476E-01  0.1306090E+00  0.3780075E+00  0.2001685E+01 -0.4624786E+00 -0.4769118E-01
  0.4726113E+00  0.3404801E+00  0.2540434E+00  0.1597870E+00
  eigenvectors of nos in ao-basis, column   19
  0.1191641E+00  0.2807008E+00 -0.1086279E+00 -0.1095557E+01 -0.2945650E+00  0.4695848E+00  0.5404002E+00  0.3361104E+00
  0.6565018E-01 -0.2414595E+00 -0.7773214E-01 -0.1793185E-01  0.1234513E+00  0.8109871E+00 -0.2374765E+00 -0.2710490E-01
  0.2689981E+00 -0.8875936E-01  0.7033664E-01 -0.1364217E-01
  eigenvectors of nos in ao-basis, column   20
  0.3540953E+00  0.8344425E+00 -0.3279961E+00 -0.3322895E+01 -0.6812585E+00  0.1099558E+01  0.8463938E+00 -0.7105633E-01
  0.4816806E+00  0.6249602E-01 -0.5795095E+00  0.1494137E+00  0.3175916E+00  0.2222291E+01 -0.6003780E+00 -0.5356837E-01
  0.2693768E+00  0.4247536E+00  0.2229944E+00  0.1465473E+00


*****   symmetry block SYM2   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       2.00000000

               Column   5     Column   6     Column   7     Column   8

               Column   9     Column  10     Column  11     Column  12

               Column  13     Column  14

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7        eno   8
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    8   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno   9        eno  10        eno  11        eno  12        eno  13        eno  14
 emo    9   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   10   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   11   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   12   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   13   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   14   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   2.0000000      0.0000000      0.0000000      0.0000000      0.0000000
   0.0000000      0.0000000      0.0000000      0.0000000      0.0000000
   0.0000000      0.0000000      0.0000000      0.0000000

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7         no   8
  mo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo    2   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo    3   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
  mo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
  mo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
  mo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01
  mo   14   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

                no   9         no  10         no  11         no  12         no  13         no  14
  mo    8   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo    9   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo   10   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
  mo   11   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
  mo   12   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
  mo   13   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01
  eigenvectors of nos in ao-basis, column    1
  0.7339992E+00 -0.1957530E-01 -0.1266443E+00 -0.1650150E-01 -0.1230093E-01  0.3177531E-03 -0.3455797E-02 -0.5562385E+00
  0.1601369E+00 -0.7422997E-02 -0.2266358E-01 -0.3034386E-01 -0.1658476E-01 -0.2086616E-01
  eigenvectors of nos in ao-basis, column    2
 -0.1280147E+01  0.2193793E+01  0.2109494E+01  0.1061793E+01 -0.1934978E+01  0.7928737E-01 -0.2658765E+00  0.3406694E+01
 -0.4421342E+00 -0.9353217E-01  0.6667420E+00  0.4493647E+00  0.3946174E+00  0.3713557E+00
  eigenvectors of nos in ao-basis, column    3
  0.1093068E+00  0.1075461E-01 -0.1870036E+00 -0.2588802E-02  0.1082030E-01 -0.1047559E-03 -0.9794784E-04 -0.4294943E-01
 -0.5479296E+00  0.2376678E+01 -0.1446412E-01 -0.1063719E-01 -0.1141627E+00 -0.1307420E+00
  eigenvectors of nos in ao-basis, column    4
 -0.3812017E+00 -0.2606910E-01 -0.1885979E+00 -0.5722278E-02  0.1888576E-01 -0.3335429E-04  0.2851727E-02 -0.1481097E+00
 -0.4676676E+00  0.1183968E+01 -0.1647629E-01 -0.5756699E-02  0.5263456E+00  0.4535307E+00
  eigenvectors of nos in ao-basis, column    5
  0.2246490E+00 -0.1045048E+00  0.1472488E+01 -0.1836010E-02 -0.7571751E-01  0.1086397E-02 -0.2784463E-02 -0.3588376E-01
  0.2972684E+01 -0.9390370E+00  0.1679260E-01  0.5302917E-02  0.5363269E+00  0.1435720E+01
  eigenvectors of nos in ao-basis, column    6
  0.1544217E+00 -0.7351334E-01  0.1768133E+01  0.7563471E-02 -0.4736159E-01 -0.7786162E-03 -0.3350391E-02  0.7775367E-01
  0.8107401E+01 -0.1335594E-01 -0.1398208E-01  0.3720163E-01  0.4290084E+01  0.6509883E+00
  eigenvectors of nos in ao-basis, column    7
 -0.6576016E+00  0.1523096E+00  0.2460193E+00 -0.9977391E-02 -0.3488036E+00  0.2637915E-02 -0.9253229E-02 -0.1078601E+01
  0.1617104E+01 -0.5484056E+00  0.2161974E+00  0.2351352E+00  0.9001924E-01 -0.4477784E+00
  eigenvectors of nos in ao-basis, column    8
 -0.4726086E+00 -0.5619155E+00  0.4135688E+01  0.3267579E-01  0.6928461E-01  0.4394980E-03 -0.3647842E-02  0.9009467E+00
  0.4400639E+01 -0.5517123E+00  0.6925670E-01  0.2390099E-01  0.1391751E+01  0.1310313E+01
  eigenvectors of nos in ao-basis, column    9
  0.1476670E+00 -0.5035282E-01  0.1059066E+01 -0.1050263E-01  0.1904374E+00 -0.3055882E-02 -0.5375383E-02  0.2142120E+00
  0.2371163E+01  0.2211675E-01 -0.5378889E+00  0.7175725E+00  0.1248590E+01  0.7560602E-01
  eigenvectors of nos in ao-basis, column   10
  0.3532031E+00  0.9153486E+00 -0.1572021E+01  0.2189085E-01  0.3138381E+00  0.5197966E-02 -0.3043068E-01  0.2364209E-01
 -0.1926808E+01 -0.7440015E-01  0.8248592E+00  0.5472483E+00 -0.9245816E+00 -0.5633648E+00
  eigenvectors of nos in ao-basis, column   11
  0.1342051E+01 -0.2312123E+00  0.2664904E+01  0.3223863E-01 -0.1729438E+01 -0.2240352E-02  0.2488830E-01  0.3515438E+01
  0.1179648E+01 -0.3249828E+00  0.7974130E+00  0.6148220E+00  0.8353970E+00  0.6976872E+00
  eigenvectors of nos in ao-basis, column   12
 -0.2524279E+01  0.3353163E+01  0.2731226E+00  0.6476768E-01 -0.6959761E+00 -0.4718583E-01  0.2993238E+00  0.1580324E+01
 -0.1487102E+01  0.8469951E-01  0.5043800E+00  0.4763270E+00 -0.1708948E+00 -0.2377382E+00
  eigenvectors of nos in ao-basis, column   13
  0.4824277E+00 -0.6856399E+00 -0.5501167E+00 -0.1481537E-01  0.1806399E+00  0.1254268E+00  0.4331735E+00 -0.5708498E+00
 -0.2951617E+00  0.2105897E-02  0.7648452E-01 -0.3666507E+00 -0.2782562E+00 -0.1895298E-01
  eigenvectors of nos in ao-basis, column   14
 -0.1747994E+01  0.2829173E+01  0.8404603E+00 -0.6597519E+00 -0.7022573E+00  0.8604840E-01 -0.2902789E+00  0.2479199E+01
 -0.1594204E+01  0.1407320E-01  0.7836697E+00  0.5353430E+00 -0.2038212E+00  0.1634377E-01


*****   symmetry block SYM3   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       2.00000000

               Column   5     Column   6     Column   7     Column   8

               Column   9

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7        eno   8
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    8   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno   9
 emo    9   0.100000E+01

 occupation numbers of nos
   2.0000000      0.0000000      0.0000000      0.0000000      0.0000000
   0.0000000      0.0000000      0.0000000      0.0000000

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7         no   8
  mo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo    2   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo    3   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
  mo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
  mo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
  mo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01
  mo    9   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

                no   9
  mo    8   0.100000E+01
  eigenvectors of nos in ao-basis, column    1
  0.9137470E+00  0.8207613E-02  0.3452394E-01 -0.5026550E-02 -0.2193112E-01  0.1158050E-02  0.3395425E-03  0.2414960E-01
  0.3712296E-01
  eigenvectors of nos in ao-basis, column    2
  0.1840143E-01 -0.8465985E-02  0.2271669E+00  0.1168846E+01 -0.7158249E+00  0.3726690E-01  0.1296141E-01 -0.1699930E+00
 -0.7407014E-01
  eigenvectors of nos in ao-basis, column    3
 -0.3685786E+00  0.7771037E-01 -0.7276585E+00 -0.6706259E-02  0.1496248E-01 -0.4272171E-04  0.1503376E-03 -0.1681463E-01
  0.8732222E+00
  eigenvectors of nos in ao-basis, column    4
 -0.5888417E+00 -0.5235677E+00  0.2169136E+01  0.1110385E-01  0.5437990E-01  0.1132001E-02 -0.2148770E-04 -0.1086267E+00
 -0.5404860E+00
  eigenvectors of nos in ao-basis, column    5
  0.3497313E+00  0.1146727E+00 -0.5415655E+00  0.3438462E-03  0.5938959E+00  0.3539903E-02 -0.7641688E-03 -0.4729295E+00
  0.3444872E+00
  eigenvectors of nos in ao-basis, column    6
 -0.2132941E+00  0.2421387E-02 -0.4466602E+00 -0.2106667E-01  0.1016346E+01  0.3810420E-03 -0.2159145E-02  0.8144629E+00
  0.7681662E-02
  eigenvectors of nos in ao-basis, column    7
 -0.3222164E+01  0.3183987E+01  0.1547309E+00  0.2048941E-01 -0.4450805E-01 -0.1034152E-01 -0.2617833E-02 -0.1697978E+00
  0.2432818E+00
  eigenvectors of nos in ao-basis, column    8
  0.1401414E-01 -0.1683489E-01 -0.1668790E-01 -0.1445650E-01  0.3699340E-01 -0.7530972E-01  0.1469433E+00  0.2162142E-01
  0.8166381E-03
  eigenvectors of nos in ao-basis, column    9
 -0.2818450E+00  0.3392979E+00  0.1302061E+00 -0.1708410E+00 -0.8715262E-01  0.1958063E+00  0.5914437E-01 -0.2832258E+00
  0.4818875E-01


*****   symmetry block SYM4   *****


 first order reduced density matrix in mo basis



     Zero matrix.

               eno   1        eno   2        eno   3        eno   4        eno   5
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   0.0000000      0.0000000      0.0000000      0.0000000      0.0000000

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5
  mo    1   0.595930E+00  -0.396489E+00  -0.284734E+00   0.233682E+00   0.876011E+00
  mo    2  -0.938710E+00  -0.704713E+00  -0.375329E+00  -0.282795E+00  -0.769283E+00
  mo    3   0.869515E+00   0.535022E-01  -0.220130E+00  -0.356787E+00  -0.456824E+00
  mo    4  -0.691054E+00   0.372799E-01  -0.624725E+00  -0.415268E+00   0.169827E+00
  mo    5   0.160642E+00   0.734421E-02  -0.320386E+00   0.997272E+00  -0.591106E+00
  eigenvectors of nos in ao-basis, column    1
 -0.2754461E+00  0.6729100E-01  0.3836606E+00  0.1919735E-01  0.1606561E+01
  eigenvectors of nos in ao-basis, column    2
  0.4073958E+00  0.4932066E+00  0.1041821E+00  0.4045085E+00  0.2553677E+00
  eigenvectors of nos in ao-basis, column    3
 -0.5698708E+00  0.1185119E+01  0.1527356E+00  0.4305843E+00  0.2164403E+00
  eigenvectors of nos in ao-basis, column    4
  0.1350193E+00 -0.5743301E+00  0.1258899E+01 -0.4261497E+00  0.1421771E+00
  eigenvectors of nos in ao-basis, column    5
 -0.4385256E+00  0.2248173E+00 -0.2123567E+00  0.1140279E+01 -0.3980187E+00


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***


 cosurf_and_phi
  1.262739748330  2.872870034186  1.567866969040 -0.040563960056
  1.652947872482  0.360731428865  3.496571379926 -0.047744392345
  0.594940552015  0.670555958619  3.845537291721 -0.057596471692
  2.390078334690  1.202722009900  2.566708449184 -0.030282523563
  1.989334673621  2.229216060677  2.001028520754 -0.033627713369
  2.919143888229  0.378144896597  2.099775822683 -0.010812250138
  0.105380882100  1.878884201281  3.371423292662 -0.058292817904
  0.783266721831  2.590884380442  2.520834408126 -0.052559672735
  2.011402526412  2.551496219550  0.814855178005 -0.016418121584
  0.099626804209  1.855073908563 -1.945822518898  0.025971373658
  0.119020599620  3.173161356543  1.415159765853 -0.046386018763
  0.915237473358  3.108118624501  0.463299650838 -0.029665043076
  0.462814589486  2.837210699514 -0.795516582628 -0.007994178958
  1.408719892640  1.640288870679  3.148120355694 -0.049774159204
  2.650053602975  1.633766196698  1.655441764425 -0.012846014881
  1.142904167226  2.885766749848 -0.243475252462 -0.012804822376
  3.656106873706  0.713798214804  0.361832640492  0.036992904741
  2.408046317685  2.075600470298 -1.226192756897  0.036677335852
  1.295820311657  0.567673501678 -2.747601655947  0.052519534722
  3.527730862121  1.045649613724 -1.064853177123  0.052163873281
  2.622898581638  1.024710819001 -2.241122746235  0.055742482515
  3.086808508398  1.794857685487 -0.179703829578  0.032455615757
  1.615872011596  1.674366500124 -2.147504385867  0.043121867877
  0.000006852884  1.035694667087 -2.361676372823  0.043679457008
  0.298815704890  0.969607820141 -2.408807386530  0.045294705757
  0.667723897920  1.245157743773 -2.338577856151  0.043527638139
  1.218123388571  2.025110412626 -1.616576841774  0.030862927502
  2.084186643139  2.293984793080 -0.480493513306  0.019468320575
  2.876642520254  1.658030225134  0.559035126479  0.020003529333
  3.282919570196  0.368088739237  1.091983758387  0.023913389296
  0.467932162408  1.694535407938 -1.941510815499  0.032266883089
  1.299770347024  2.453875604722 -0.850324284820  0.009231499230
  2.242050270258  2.245320705184  0.385739526123  0.002433939445
  2.923104801922  1.151131828431  1.279135167181  0.007476050149
  0.427044967836  0.580963354323 -2.585108185092  0.049002947599
 end_of_phi


 nsubv after electrostatic potential =  0

  Confirmation of dielectric energy for ground state
  edielnew =   0.


 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************


 cosurf_and_qcos_from cosmo(3
  1.262739748330  2.872870034186  1.567866969040  0.004806014378
  1.652947872482  0.360731428865  3.496571379926  0.003772055180
  0.594940552015  0.670555958619  3.845537291721  0.006327013863
  2.390078334690  1.202722009900  2.566708449184  0.003527455982
  1.989334673621  2.229216060677  2.001028520754  0.003116030037
  2.919143888229  0.378144896597  2.099775822683  0.000051823211
  0.105380882100  1.878884201281  3.371423292662  0.000555205745
  0.783266721831  2.590884380442  2.520834408126  0.007979388360
  2.011402526412  2.551496219550  0.814855178005  0.002379786969
  0.099626804209  1.855073908563 -1.945822518898  0.000271883919
  0.119020599620  3.173161356543  1.415159765853  0.004264393232
  0.915237473358  3.108118624501  0.463299650838  0.005804388103
  0.462814589486  2.837210699514 -0.795516582628  0.002833974567
  1.408719892640  1.640288870679  3.148120355694  0.007372694505
  2.650053602975  1.633766196698  1.655441764425  0.000996596980
  1.142904167226  2.885766749848 -0.243475252462  0.001751491250
  3.656106873706  0.713798214804  0.361832640492 -0.007635511145
  2.408046317685  2.075600470298 -1.226192756897 -0.004751980964
  1.295820311657  0.567673501678 -2.747601655947 -0.005329829569
  3.527730862121  1.045649613724 -1.064853177123 -0.011095315813
  2.622898581638  1.024710819001 -2.241122746235 -0.010614421862
  3.086808508398  1.794857685487 -0.179703829578 -0.004255116086
  1.615872011596  1.674366500124 -2.147504385867 -0.003946610093
  0.000006852884  1.035694667087 -2.361676372823 -0.000113352452
  0.298815704890  0.969607820141 -2.408807386530 -0.000196753502
  0.667723897920  1.245157743773 -2.338577856151 -0.000732111523
  1.218123388571  2.025110412626 -1.616576841774 -0.001087408394
  2.084186643139  2.293984793080 -0.480493513306 -0.000912723538
  2.876642520254  1.658030225134  0.559035126479 -0.001507071296
  3.282919570196  0.368088739237  1.091983758387 -0.001921244861
  0.467932162408  1.694535407938 -1.941510815499 -0.001011967390
  1.299770347024  2.453875604722 -0.850324284820  0.000038819732
  2.242050270258  2.245320705184  0.385739526123 -0.000325968745
  2.923104801922  1.151131828431  1.279135167181 -0.001087725542
  0.427044967836  0.580963354323 -2.585108185092 -0.001868298743
 end_of_qcos

 sum of the negative charges =  -0.0583934115

 sum of the positive charges =   0.055849016

 total sum =  -0.00254439551

 *** qcos is copied to qcosdalton ***

 fepsi =   1.
 *** qcosdalton is mult by fepsi ***
 cosurf and qcosdalton
  1.262739748330  2.872870034186  1.567866969040  0.004806014378
  1.652947872482  0.360731428865  3.496571379926  0.003772055180
  0.594940552015  0.670555958619  3.845537291721  0.006327013863
  2.390078334690  1.202722009900  2.566708449184  0.003527455982
  1.989334673621  2.229216060677  2.001028520754  0.003116030037
  2.919143888229  0.378144896597  2.099775822683  0.000051823211
  0.105380882100  1.878884201281  3.371423292662  0.000555205745
  0.783266721831  2.590884380442  2.520834408126  0.007979388360
  2.011402526412  2.551496219550  0.814855178005  0.002379786969
  0.099626804209  1.855073908563 -1.945822518898  0.000271883919
  0.119020599620  3.173161356543  1.415159765853  0.004264393232
  0.915237473358  3.108118624501  0.463299650838  0.005804388103
  0.462814589486  2.837210699514 -0.795516582628  0.002833974567
  1.408719892640  1.640288870679  3.148120355694  0.007372694505
  2.650053602975  1.633766196698  1.655441764425  0.000996596980
  1.142904167226  2.885766749848 -0.243475252462  0.001751491250
  3.656106873706  0.713798214804  0.361832640492 -0.007635511145
  2.408046317685  2.075600470298 -1.226192756897 -0.004751980964
  1.295820311657  0.567673501678 -2.747601655947 -0.005329829569
  3.527730862121  1.045649613724 -1.064853177123 -0.011095315813
  2.622898581638  1.024710819001 -2.241122746235 -0.010614421862
  3.086808508398  1.794857685487 -0.179703829578 -0.004255116086
  1.615872011596  1.674366500124 -2.147504385867 -0.003946610093
  0.000006852884  1.035694667087 -2.361676372823 -0.000113352452
  0.298815704890  0.969607820141 -2.408807386530 -0.000196753502
  0.667723897920  1.245157743773 -2.338577856151 -0.000732111523
  1.218123388571  2.025110412626 -1.616576841774 -0.001087408394
  2.084186643139  2.293984793080 -0.480493513306 -0.000912723538
  2.876642520254  1.658030225134  0.559035126479 -0.001507071296
  3.282919570196  0.368088739237  1.091983758387 -0.001921244861
  0.467932162408  1.694535407938 -1.941510815499 -0.001011967390
  1.299770347024  2.453875604722 -0.850324284820  0.000038819732
  2.242050270258  2.245320705184  0.385739526123 -0.000325968745
  2.923104801922  1.151131828431  1.279135167181 -0.001087725542
  0.427044967836  0.580963354323 -2.585108185092 -0.001868298743
 end of cosurf and qcosdalton

 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 Warning! Large contrib. to nucrep.The distance is   1.37057673E-05
 ** Total nuclear repulsion energy ** =   9.34367465521618
 Warning! Large contrib. to nucrep.The distance is   1.37057673E-05
 screening nuclear repulsion energy   0.00759477565

 Total-screening nuclear repulsion energy   9.33607988


 Adding T+Vsolv ...
 maxdens  375
 *** End of DALTON-COSMO calculation ***

  original map vector  44 45 46 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 47 18
 19 20 21 22 23 24 25 26 27 28 29 30 48 31 32 33 34 35 36 37 38 39 40 41 42 43 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0


          sorted Vsolv matrix  block   1

               nbf   1        nbf   2        nbf   3        nbf   4        nbf   5        nbf   6        nbf   7        nbf   8
  nbf   1    -1.94545026
  nbf   2     0.68123014    -2.86130366
  nbf   3    -0.11506290    -0.00763273    -2.70402424
  nbf   4     0.43669247    -0.20535391     0.11007955    -2.67939164
  nbf   5    -0.28429564     1.38412805    -0.05636830    -0.02635192    -4.04308923
  nbf   6    -0.01701178    -0.16408465    -0.50086878     0.22010593     0.21269762    -3.53003272
  nbf   7     0.09327589    -0.26778708    -0.64502387    -0.34042521     0.16227016    -0.04632932    -3.66094018
  nbf   8     0.13404260    -0.18256773     0.31768846    -0.82907573    -0.28160104    -0.03888090     0.08679551    -3.77999189
  nbf   9     0.12540307    -0.34693853     0.27534024    -0.52376510     0.61701252     0.57999887    -0.07916162    -0.04072918
  nbf  10    -0.05882863     0.15857420    -0.00230218    -0.01686219    -0.60460450     0.32146280    -0.68979967     0.11208770
  nbf  11    -0.18168609     0.38616070     0.07035237     0.12069474    -1.36545287     0.32344348     0.56162961     0.42421437
  nbf  12     0.07220423    -0.33709769    -0.22504743     0.12492656     0.69870886    -1.06133757    -0.48957052     0.56281244
  nbf  13    -0.00035400     0.01967058    -0.00086249    -0.00671862     0.00792431     0.00941228     0.36170386    -0.11846672
  nbf  14    -0.02691927    -0.04356133    -0.05409916     0.06833746     0.22504595    -0.01190229     0.25314862     0.91478942
  nbf  15    -0.00477061    -0.04964701    -0.05181856     0.02171225     0.01012330    -0.15515333     0.51079774    -0.27791017
  nbf  16    -0.03040195    -0.03564681    -0.04566567     0.01250811    -0.10743787    -0.18833984    -0.20145800     0.00589353
  nbf  17     0.17868965    -0.24044093     0.12594056    -0.13944452     0.46286494     0.34462881    -0.28698853    -1.09329507
  nbf  44    -0.07824950     0.13072335    -0.08495048     0.16376872    -0.17409185    -0.13775153     0.06535490     0.28051949
  nbf  45     0.44096567    -0.76120686     0.24961246    -0.64646498     0.77738321     0.26125149    -0.27974344    -0.80820317
  nbf  46    -0.17178769     0.79352878     0.58164719    -0.41164389    -0.98272271     1.10029457     0.21376700    -0.46522150

               nbf   9        nbf  10        nbf  11        nbf  12        nbf  13        nbf  14        nbf  15        nbf  16
  nbf   9    -3.36132935
  nbf  10     0.07000400    -2.60062780
  nbf  11    -0.03731785    -0.21051632    -2.87414516
  nbf  12     0.03839080     0.48487203     1.10605734    -3.57359070
  nbf  13     0.01350819    -0.11247198    -0.03729521     0.08742224    -1.52440139
  nbf  14    -0.15104224     0.25455091     0.44320090    -0.26812406    -0.00945876    -1.44557955
  nbf  15     0.37170302     0.89007846     0.16437276    -0.03971887    -0.21662200     0.19892712    -1.52215995
  nbf  16     0.72977502    -0.38656901     0.71981598    -0.20489203     0.12668247     0.64925263     0.13569702    -0.74990953
  nbf  17    -1.35638137     0.15863998     1.01948333     0.06709663     0.01424992     0.24314942     0.05975908     0.19403715
  nbf  44     0.31333007    -0.02370472    -0.18736583    -0.12110615     0.00343097    -0.15396206    -0.04554550    -0.13537876
  nbf  45    -0.69037944     0.07962231     0.44806953     0.02710104     0.00318679     0.22268945     0.03314003     0.11888972
  nbf  46    -0.35786449    -0.29854919    -0.49443973     1.01914186    -0.04132496     0.21585287     0.11923362     0.17784160

               nbf  17        nbf  18        nbf  19        nbf  20        nbf  21        nbf  22        nbf  23        nbf  24
  nbf  17     0.95505383
  nbf  18     0.00000000    -1.55501657
  nbf  19     0.00000000     0.46601248    -2.92593514
  nbf  20     0.00000000    -0.36515527     0.09358796    -2.40588421
  nbf  21     0.00000000    -0.11311889     0.32490169    -0.07448634    -2.00637469
  nbf  22     0.00000000     0.15437137    -1.38419459     0.08179026     0.31689254    -4.05451448
  nbf  23     0.00000000     0.18624767    -0.48893029     0.87780990     0.26524454    -0.37560521    -3.40880036
  nbf  24     0.00000000    -0.00504521     0.08449497    -0.20966064     0.29437374     0.22894392     0.13057273    -2.50540875
  nbf  25     0.00000000    -0.05516012     0.30224571    -0.04868522    -0.29811369     1.22043150    -0.02222201    -0.17998619
  nbf  26     0.00000000    -0.14825373     0.36724295    -0.19359223    -0.10481298     0.88523000     1.31123797    -0.06078646
  nbf  27     0.00000000     0.11041545    -0.34512792     0.17743815     0.19443704    -0.46418147    -0.89951096     0.21934061
  nbf  28     0.00000000    -0.01670700     0.05098246    -0.03154452     0.00038357     0.02627334     0.13757466    -0.59998715
  nbf  29     0.00000000     0.04827518    -0.18728831     0.03119127     0.00770360    -0.55270078    -0.05699683     0.02922617
  nbf  30     0.00000000     0.02691228    -0.11560935     0.11496397     0.05676853    -0.01931282    -0.26725055    -0.11038633
  nbf  44     1.02833279     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
  nbf  45    -0.88521411     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
  nbf  46    -0.24334354     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
  nbf  47     0.00000000    -0.40143498     1.03301037    -0.60538591    -0.30371870     0.96903997     1.06089646    -0.11824386

               nbf  25        nbf  26        nbf  27        nbf  28        nbf  29        nbf  30        nbf  31        nbf  32
  nbf  25    -2.87247559
  nbf  26     0.02577859    -2.99025387
  nbf  27     0.76730489     1.04028497    -2.66613010
  nbf  28    -0.04441008    -0.27684647     0.11240222    -1.18104295
  nbf  29     1.15748432     0.64168955    -0.37668669     0.07139613    -1.55136265
  nbf  30    -0.28824314     1.29191996    -0.28953791     0.02869317     0.55967915    -0.37392670
  nbf  31     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -2.77866440
  nbf  32     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.81895847    -3.88024254
  nbf  33     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.71630300     0.13693475
  nbf  34     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.09213080    -0.47350355
  nbf  35     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.50717826    -1.61839901
  nbf  36     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00235342     0.01149007
  nbf  37     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00582749     0.02062447
  nbf  38     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.05160522    -0.08746710
  nbf  47    -0.22141273    -0.73096863     0.67004958    -0.09659415     0.21829318     0.25325830     0.00000000     0.00000000
  nbf  48     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     1.07106677     1.54921239

               nbf  33        nbf  34        nbf  35        nbf  36        nbf  37        nbf  38        nbf  39        nbf  40
  nbf  33    -3.76231952
  nbf  34    -0.60009368    -2.83288180
  nbf  35     0.76479609    -0.37709016    -4.43809966
  nbf  36     0.04445490     0.09192862     0.00536275    -1.60170333
  nbf  37     0.47170366    -0.27441490    -0.10676856    -0.00372832    -1.33510056
  nbf  38    -0.67299934    -0.83714365    -0.02825200     0.01126379     0.29049221    -1.68075141
  nbf  39     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -4.59117020
  nbf  40     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00746180    -2.80777363
  nbf  41     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00485151    -0.00310725
  nbf  42     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00241486    -0.00123530
  nbf  43     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00093858     0.01129413
  nbf  48    -0.44678930     0.27580971     1.29481892    -0.00572040     0.04624000     0.03318612     0.00000000     0.00000000

               nbf  41        nbf  42        nbf  43        nbf  44        nbf  45        nbf  46        nbf  47        nbf  48
  nbf  41    -2.20459522
  nbf  42    -0.00904874    -1.35624887
  nbf  43     0.00050563    -0.00242826    -0.74771766
  nbf  44     0.00000000     0.00000000     0.00000000   -33.04946204
  nbf  45     0.00000000     0.00000000     0.00000000     0.58276501    -7.87968877
  nbf  46     0.00000000     0.00000000     0.00000000     0.18472219    -0.22800000    -6.92041259
  nbf  47     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -6.75760999
  nbf  48     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -7.04585770
 insert_onel_diag: nmin2=  1892

 onel.diag.
 -0.194545E+01-0.286130E+01-0.270402E+01-0.267939E+01-0.404309E+01-0.353003E+01-0.366094E+01-0.377999E+01-0.336133E+01-0.260063E+01
 -0.287415E+01-0.357359E+01-0.152440E+01-0.144558E+01-0.152216E+01-0.749910E+00 0.955054E+00-0.155502E+01-0.292594E+01-0.240588E+01
 -0.200637E+01-0.405451E+01-0.340880E+01-0.250541E+01-0.287248E+01-0.299025E+01-0.266613E+01-0.118104E+01-0.155136E+01-0.373927E+00
 -0.277866E+01-0.388024E+01-0.376232E+01-0.283288E+01-0.443810E+01-0.160170E+01-0.133510E+01-0.168075E+01-0.459117E+01-0.280777E+01
 -0.220460E+01-0.135625E+01-0.747718E+00-0.330495E+02-0.787969E+01-0.692041E+01-0.675761E+01-0.704586E+01

 i,start,strti,fin,filind,bufszi,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095         1      1892      1892

 4 external diag. modified integrals

  0.162583931 -1.94545026  0.208205722  0.160361974  0.257411129 -2.86130366
  0.211866097  0.172819047  0.285377448  0.227907886  0.292636879 -2.70402424
  0.201180191  0.169627984  0.244637662  0.222077188  0.262200229  0.230622662
  0.256871848 -2.67939164  0.204782323  0.190354619  0.353861471  0.257082437
  0.307414074  0.284093738  0.284149775  0.26571056  0.454131129 -4.04308923
  0.191611185  0.182783002  0.256806383  0.245309394  0.277121536  0.257258778
  0.319642706  0.24649527  0.371309251  0.320951478  0.4062204 -3.53003272
  0.203648708  0.197431646  0.314388232  0.299157726  0.338163602  0.296808531
  0.296940337  0.284529011  0.467424556  0.415881438  0.388574144  0.36895109
  0.516577958 -3.66094018  0.195060045  0.189901517  0.275836116  0.266960473
  0.286083473  0.273691893  0.317155683  0.280108282  0.403100844  0.377135808
  0.415686909  0.370244838  0.428342355  0.414072102  0.509604969 -3.77999189
  0.199742763  0.194673788  0.313691197  0.284920235  0.310615366  0.296336636
  0.303169199  0.28522524  0.475779153  0.400994832  0.422708432  0.369283481
  0.509375063  0.440066243  0.46755657  0.440258048  0.50014168 -3.36132935
  0.1989861  0.195295106  0.314524782  0.284641855  0.320339501  0.295428393
  0.295307125  0.287869756  0.470168411  0.39676046  0.412813027  0.386068737
  0.541019267  0.41304078  0.439912758  0.421120128  0.489395609  0.45212706
  0.496984115 -2.6006278  0.203992183  0.198794343  0.337605204  0.301841751
  0.313427545  0.305483018  0.296957016  0.29114577  0.554771663  0.423748914
  0.411234724  0.374857862  0.511479912  0.475491855  0.472373425  0.428953236
  0.545207657  0.455265789  0.499940812  0.463648119  0.567827247 -2.87414516
  0.20044929  0.197929769  0.325886027  0.3032539  0.311210435  0.29967807
  0.31858978  0.309496723  0.536570158  0.464915722  0.503496053  0.440412632
  0.561027165  0.49501485  0.592424451  0.534958863  0.579581855  0.544911706
  0.596221005  0.48795483  0.644256024  0.529055581  0.878481247 -3.5735907
  0.200707487  0.198576653  0.320357122  0.304609637  0.307085941  0.302254344
  0.314914782  0.312657802  0.502720952  0.469872217  0.47137423  0.448658191
  0.579263287  0.505151876  0.602389946  0.533345136  0.584002658  0.531964393
  0.602832747  0.494226799  0.566667441  0.54846376  0.800958519  0.706574492
  0.81745201 -1.52440139  0.202971709  0.199912653  0.329392239  0.314695097
  0.310020328  0.306722632  0.31897329  0.309117686  0.539656526  0.49843245
  0.459750135  0.443794478  0.558609793  0.529707092  0.597241233  0.534606813
  0.615136892  0.554292134  0.548937937  0.526730003  0.640006687  0.58248007
  0.796255055  0.738284081  0.759554655  0.714955102  0.87067624 -1.44557955
  0.202651551  0.199135495  0.336499563  0.310425445  0.314492758  0.304907037
  0.325134017  0.314440497  0.56371812  0.484207939  0.517689992  0.455675426
  0.574985359  0.526199339  0.62918666  0.563980593  0.622384465  0.568585152
  0.604672678  0.530241703  0.637069122  0.578981353  0.910187722  0.845582462
  0.873353846  0.771341953  0.887019595  0.803244619  1.01770095 -1.52215995
  0.1980477  0.196750626  0.306052297  0.301061294  0.311283575  0.303332677
  0.320036237  0.310578659  0.489848125  0.472457914  0.480849968  0.455984077
  0.539321232  0.51245105  0.605017098  0.553865891  0.62395644  0.554322942
  0.55581959  0.525900768  0.610529032  0.549512708  0.81396413  0.772842519
  0.739088719  0.712382832  0.873089105  0.76064112  0.915222986  0.831046992
  0.917152999 -0.749909533  0.205508389  0.200450937  0.342156913  0.320377894
  0.312734907  0.306375322  0.324874543  0.307022656  0.563505817  0.508637216
  0.466752322  0.442170277  0.542293802  0.520944555  0.599442718  0.535052254
  0.629797664  0.536416528  0.543244946  0.519434725  0.680964234  0.574986706
  0.990942407  0.813738162  0.747730686  0.717903179  0.871934976  0.802985335
  0.923133504  0.883914829  0.851863211  0.811274163  1.17984261  0.955053831
  0.181100559  0.111200926  0.165161842  0.150968711  0.170528599  0.156019974
  0.170217989  0.14757216  0.168634738  0.162088706  0.155944411  0.153686935
  0.165152381  0.161592075  0.159982955  0.157027771  0.164199322  0.162076243
  0.163872813  0.162653604  0.165865886  0.16435758  0.162329136  0.161558572
  0.161265831  0.160930046  0.165143735  0.162973179  0.16300763  0.162118388
  0.164137427  0.162098534  0.165503203  0.163787595  0.206726055  0.173131144
  0.344611998  0.193580208  0.278191927  0.251527147  0.262222061  0.226749739
  0.334287937  0.296667362  0.267389149  0.256202192  0.325112783  0.307840249
  0.288140601  0.277217248  0.333680777  0.297354535  0.326461457  0.300057351
  0.347580757  0.319045094  0.326543379  0.3192572  0.315976561  0.312690014
  0.343367664  0.324710111  0.334803926  0.324945124  0.342324404  0.317594447
  0.346641191  0.33324303  0.190390755  0.165762347  0.234038042  0.207838903
  0.319730252  0.184530863  0.243042886  0.215570556  0.27206095  0.251242634
  0.262934912  0.24749127  0.273436359  0.265217826  0.268958885  0.256401113
  0.277114573  0.272276947  0.282890499  0.270790638  0.278083482  0.271317041
  0.280310393  0.276787339  0.277349215  0.27553314  0.289122186  0.281265601
  0.28349409  0.280751973  0.289414953  0.284039581  0.283878135  0.279504387
  0.196684499  0.176869595  0.250791815  0.221774435  0.270729017  0.226415807
  0.268067325  0.205289746  0.267919444  0.25425307  0.239419896  0.232018841
  0.270437643  0.261152741  0.252299247  0.246413738  0.262193148  0.257108525
  0.265710759  0.257658193  0.268843233  0.265287713  0.257411555  0.255381171
  0.258052663  0.257489899  0.26305993  0.260365906  0.258769374  0.256910331
  0.25784267  0.254762976  0.264425743  0.261347436  0.204034028  0.193828014
  0.317336532  0.288240771  0.308195299  0.289731431  0.28521734  0.271554461
  0.551694971  0.333097127  0.377222223  0.330756722  0.470432406  0.426910852
  0.407187599  0.385611117  0.482915654  0.411390477  0.464004197  0.414223559
  0.542552879  0.440597293  0.509083861  0.48446817  0.478063644  0.470098341
  0.540852497  0.500442253  0.524800709  0.502158753  0.555391843  0.482336704
  0.539964756  0.509549895  0.189298309  0.182121816  0.263175464  0.24211726
  0.286280126  0.263334223  0.279591449  0.260835822  0.362662909  0.342266096
  0.382051261  0.337087152  0.393560927  0.368787528  0.410265713  0.36189836
  0.436878477  0.37713073  0.404863705  0.384219639  0.430034693  0.379283845
  0.468536668  0.449954037  0.451151499  0.440935716  0.514442366  0.451138885
  0.49157069  0.469712256  0.506435468  0.4663034  0.48519389  0.460788436
  0.202602349  0.199529704  0.321944462  0.302500244  0.338185072  0.306452135
  0.288264533  0.280083813  0.486750444  0.405415759  0.358860779  0.34402156
  0.619932637  0.366956358  0.383568319  0.377076428  0.462607377  0.434142088
  0.542864137  0.38068661  0.50069596  0.464502941  0.452829194  0.442078082
  0.461803463  0.45124469  0.477086106  0.46978653  0.457772995  0.452643235
  0.454850909  0.443433586  0.464771662  0.456695466  0.203520114  0.198676234
  0.333977718  0.298447974  0.313647566  0.30340007  0.295391728  0.285543179
  0.49758812  0.416419838  0.38463579  0.362578249  0.486187761  0.459368947
  0.439761818  0.410214658  0.542222217  0.412432394  0.485064749  0.450604852
  0.596555442  0.428679634  0.536836281  0.512334104  0.528828687  0.509442906
  0.565482464  0.530604447  0.553851633  0.537493799  0.588637675  0.4958812
  0.563073666  0.521731907  0.196719099  0.193921448  0.30299922  0.287836202
  0.301982192  0.294539552  0.298843071  0.291084628  0.489695345  0.422008108
  0.433574242  0.406241281  0.489061688  0.466253407  0.483721816  0.457650362
  0.554487838  0.463496662  0.50617229  0.474992877  0.61863288  0.461594549
  0.650676483  0.598025459  0.591994506  0.57273645  0.711222906  0.580063407
  0.670169062  0.633841211  0.670869364  0.631723597  0.691000746  0.626842228
  0.200030289  0.198723857  0.313639944  0.306345145  0.306780659  0.30283704
  0.315061518  0.311109806  0.496190572  0.477703559  0.453156393  0.43485719
  0.520067343  0.502414173  0.609956557  0.539788568  0.573018665  0.552076142
  0.534371654  0.499995995  0.593269428  0.553610416  0.777237699  0.733189096
  0.726493916  0.685033235  0.798171909  0.744368719  0.811408996  0.787245641
  0.795404927  0.757654148  0.906665062  0.768424816  0.199110025  0.197664195
  0.311685933  0.302618316  0.31315034  0.306910613  0.312162566  0.306802203
  0.491199888  0.469594901  0.451797858  0.443181689  0.597329903  0.501391141
  0.552915491  0.52456106  0.604284717  0.538090851  0.60061827  0.493755142
  0.591344512  0.553177825  0.716024215  0.687698473  0.718370038  0.676544017
  0.795581542  0.733359328  0.798744584  0.746462827  0.816660565  0.748615343
  0.740271711  0.706961247  0.205650978  0.202098965  0.3463058  0.323472537
  0.316863455  0.312984678  0.316880123  0.310078358  0.56725533  0.513354282
  0.46895493  0.435845916  0.566740985  0.552174461  0.571580049  0.537434372
  0.601308114  0.557667114  0.55928864  0.535939654  0.667005988  0.593412559
  0.821334749  0.759704854  0.763519276  0.726998097  0.923511849  0.750999582
  0.881128568  0.830365933  0.813652819  0.772195811  0.915969785  0.800684138
  0.199718489  0.197976922  0.318502512  0.30634659  0.310896365  0.304700598
  0.318389467  0.31011476  0.522740392  0.479042146  0.488559383  0.453378141
  0.543381326  0.524237262  0.59619137  0.539089755  0.593410856  0.558399945
  0.5522044  0.540204146  0.619497378  0.572008541  0.84595457  0.768178739
  0.743778268  0.721537928  0.823528608  0.780081715  0.924424695  0.838761203
  0.908794418  0.795352855  0.866968858  0.81435082  0.147854003 -1.55501657
  0.179386255  0.1516907  0.291151891 -2.92593514  0.174191642  0.14966691
  0.242219397  0.219276556  0.258663302 -2.40588421  0.17473367  0.155852044
  0.272788043  0.226264627  0.240889386  0.21072064  0.247135925 -2.00637469
  0.170642113  0.160795236  0.375918463  0.272210101  0.266000371  0.253221783
  0.270586564  0.254514467  0.463176926 -4.05451448  0.163929292  0.156658166
  0.277666025  0.253098107  0.299700175  0.250978116  0.241105807  0.233086385
  0.37457435  0.342886792  0.391569755 -3.40880036  0.166455569  0.164959496
  0.338909353  0.310977731  0.283777781  0.265418973  0.281101819  0.26435153
  0.49041693  0.410699363  0.378817621  0.345259992  0.5216361 -2.50540875
  0.168503737  0.165275669  0.36399521  0.309497837  0.276395633  0.268171136
  0.274033174  0.266592593  0.539326171  0.399696108  0.392427873  0.36678163
  0.489447186  0.44860391  0.512221479 -2.87247559  0.164445542  0.1613885
  0.330106718  0.298899825  0.286709042  0.273915054  0.257222414  0.251772244
  0.534012377  0.418395786  0.486274173  0.387032518  0.456974473  0.431959559
  0.537109916  0.449567828  0.580011117 -2.99025387  0.16443963  0.162412782
  0.338086932  0.315392891  0.287678225  0.279885936  0.261928816  0.258727899
  0.525806673  0.470043551  0.493657849  0.441470356  0.453784817  0.444044068
  0.546353835  0.497799526  0.675859267  0.557772699  0.812559011 -2.6661301
  0.164298666  0.162672945  0.337295556  0.317233284  0.288870319  0.284429227
  0.260906337  0.257703661  0.524385327  0.481393892  0.484542647  0.46330603
  0.502408728  0.453462135  0.566721142  0.505783571  0.648865191  0.588250757
  0.727222014  0.685369342  0.796004639 -1.18104295  0.166492557  0.164385012
  0.356244286  0.332469933  0.289621881  0.281486079  0.268415064  0.264591893
  0.571309746  0.506331706  0.507323208  0.446693623  0.501530774  0.487662327
  0.623342732  0.518646408  0.717396364  0.579306693  0.815064351  0.721742713
  0.792539237  0.705999389  0.885754518 -1.55136265  0.16402837  0.162216181
  0.342068167  0.320913095  0.28721435  0.281877177  0.258953037  0.255929999
  0.558020418  0.49037548  0.503536549  0.459041534  0.465035777  0.452701305
  0.598705395  0.505772674  0.719709789  0.595125573  0.810269386  0.723911915
  0.789330972  0.725190974  0.856796727  0.770386533  0.882536543 -0.373926697
  0.216833985  0.168837654  0.267729028  0.229907828  0.273949287  0.244083154
  0.278601441  0.226593683  0.292745877  0.281211443  0.27818265  0.248488243
  0.303770996  0.295477096  0.298269685  0.284385815  0.300901785  0.287542214
  0.292457077  0.286293388  0.304368206  0.299517668  0.31313896  0.307415988
  0.315090786  0.307757997  0.320558315  0.310337291  0.322953352  0.312143993
  0.313958794  0.303849197  0.326820263  0.317794719  0.193449263  0.183426276
  0.25435577  0.245151643  0.265598219  0.255499559  0.336972462  0.246173998
  0.354857731  0.334898989  0.392904987  0.350796422  0.367507624  0.362084328
  0.471630396  0.398017007  0.416451945  0.383817325  0.383431729  0.37683257
  0.395113884  0.385930569  0.49701314  0.484236773  0.491800832  0.473300677
  0.521391259  0.461401698  0.539878027  0.492573561  0.518466619  0.477266455
  0.520263574  0.487548657  0.201344598  0.196807875  0.303705463  0.295135681
  0.305843786  0.297282758  0.301388638  0.294081963  0.452313059  0.417732805
  0.400327432  0.381677249  0.496765389  0.454366448  0.479728964  0.443039035
  0.486799308  0.45349987  0.472083889  0.44990918  0.500572065  0.475714451
  0.589198888  0.519499274  0.609880442  0.526959016  0.560729211  0.549467141
  0.579606719  0.559435299  0.557169371  0.532879633  0.562171003  0.541563349
  0.200017069  0.196988236  0.312153237  0.285203813  0.304156841  0.295619554
  0.303660007  0.290464824  0.460834957  0.398301271  0.3982042  0.373094042
  0.46610729  0.441269246  0.470879369  0.431386819  0.471007531  0.447476304
  0.474210534  0.437928167  0.483036985  0.464850739  0.560194338  0.4853262
  0.576350988  0.500332875  0.541282082  0.51646452  0.539419424  0.53006066
  0.52312009  0.509760212  0.526063593  0.510561391  0.200663018  0.198331781
  0.315405451  0.307632292  0.301605919  0.297537383  0.323099181  0.312538597
  0.507029154  0.485691393  0.467213563  0.450448069  0.504455507  0.497208428
  0.660706254  0.534651043  0.594246713  0.558697228  0.518996903  0.507360904
  0.604316103  0.558677744  0.90228294  0.811631934  0.769113507  0.722592635
  0.8260654  0.795559619  0.915357839  0.859951845  0.869700966  0.795874773
  1.07800877  0.87462372  0.199041255  0.197666509  0.311223305  0.300826039
  0.306649952  0.301455582  0.314991664  0.31204979  0.485877447  0.463701062
  0.464667911  0.453908897  0.575593353  0.497819474  0.574191424  0.54839654
  0.56658854  0.546964597  0.597103951  0.496505691  0.557093248  0.544767945
  0.79341043  0.69605458  0.780491105  0.74161129  0.750354501  0.718434814
  0.83519223  0.7699359  0.75851993  0.72669178  0.73962283  0.711115095
  0.1995279  0.198010833  0.309283899  0.301365251  0.304452057  0.302095566
  0.314796824  0.311189736  0.483815193  0.465859828  0.453249988  0.432966896
  0.528094773  0.505165575  0.620904486  0.536080953  0.618375218  0.519605881
  0.531726368  0.506301496  0.576779075  0.540832338  0.708068129  0.689762419
  0.746762894  0.700401838  0.806915825  0.7215185  0.797438687  0.749001407
  0.798194979  0.72551533  0.745874344  0.714880694  0.202518536  0.199147874
  0.332679622  0.309822161  0.311060584  0.30342353  0.327163492  0.315892767
  0.556201245  0.483159141  0.509558558  0.458800964  0.54163174  0.527192578
  0.626444193  0.582017561  0.624254849  0.573032583  0.561013976  0.54231485
  0.623148845  0.578401459  0.904484195  0.854104325  0.863311643  0.779189299
  0.886265353  0.812862522  1.00115395  0.937703894  0.925447792  0.836389825
  0.92608005  0.89084278  0.164935252  0.154187892  0.274802032  0.242014418
  0.234029201  0.214868245  0.2667466  0.230511632  0.297394324  0.284678129
  0.259392667  0.252591599  0.298353835  0.294707846  0.300613758  0.297107524
  0.290614654  0.286451114  0.316577397  0.310181051  0.304434299  0.301480299
  0.32049692  0.31635523  0.309785966  0.306277763  0.155929889  0.153868082
  0.261173275  0.25440558  0.259175407  0.249509673  0.243031821  0.234592617
  0.347482123  0.3405552  0.375625936  0.345486988  0.333895886  0.330489323
  0.367306856  0.362036022  0.422838574  0.410305568  0.496072518  0.485167042
  0.461549677  0.454880519  0.480368898  0.468522441  0.490888536  0.478699123
  0.162630868  0.161334253  0.314444764  0.304608791  0.267685054  0.26380423
  0.266301352  0.261884012  0.459079931  0.420081058  0.385314321  0.375374194
  0.462993064  0.436410756  0.492130257  0.450505092  0.48494125  0.470734719
  0.551407214  0.535736539  0.546250739  0.526052777  0.571293039  0.556428998
  0.556283423  0.539126622  0.162536412  0.162155005  0.319595554  0.301577043
  0.269719129  0.267372815  0.266301104  0.262933005  0.446857859  0.412717631
  0.384312981  0.36972566  0.452369518  0.428272711  0.470824356  0.448771756
  0.475429018  0.45645276  0.52311853  0.51163923  0.521377379  0.504315619
  0.532509673  0.523871423  0.520992432  0.51322342  0.162259384  0.161538653
  0.324515738  0.318439499  0.279580789  0.276900868  0.258005807  0.255980346
  0.49712004  0.483680727  0.469680171  0.459445976  0.428312123  0.425963461
  0.515561051  0.50533672  0.640519835  0.613341562  0.86759272  0.781553264
  0.709054426  0.693745803  0.813551025  0.787585401  0.818103956  0.794668765
  0.161468911  0.160918441  0.317527563  0.311329526  0.279121487  0.278072982
  0.256829087  0.25632856  0.482212189  0.469353365  0.45683397  0.450698198
  0.454986409  0.448584548  0.523606199  0.511125478  0.593936327  0.581604007
  0.760746462  0.675150275  0.739939938  0.691671053  0.742741622  0.717197944
  0.745746338  0.726401573  0.162897326  0.161918975  0.325579501  0.31341906
  0.282447109  0.280189317  0.259904824  0.258575504  0.4929234  0.471844969
  0.465391836  0.451836961  0.454114561  0.448642034  0.527002755  0.505491107
  0.599408243  0.578291175  0.766322034  0.720787719  0.741653214  0.694917991
  0.74829325  0.715754397  0.732710284  0.715973565  0.162486294  0.16203107
  0.329778545  0.324564247  0.281279764  0.280087181  0.258238724  0.257530675
  0.512782723  0.500557466  0.480378558  0.471344481  0.446717175  0.444261689
  0.547504106  0.532098893  0.649613573  0.636824882  0.810326982  0.800056761
  0.771776501  0.745861083  0.865795948  0.821645965  0.899701011  0.837033098
  0.287002033 -2.7786644  0.292545937  0.254951102  0.425470456 -3.88024254
  0.324717158  0.292708755  0.409485272  0.387541321  0.512709584 -3.76231952
  0.313868628  0.29505883  0.408692891  0.379444577  0.557698697  0.416771386
  0.484938377 -2.8328818  0.338545462  0.304997177  0.604284039  0.46435009
  0.586688394  0.522358559  0.554211306  0.495467396  1.00724657 -4.43809966
  0.312016993  0.305241323  0.490886353  0.472623546  0.624121165  0.512240016
  0.590493997  0.487650416  0.755261431  0.718952361  0.791328047 -1.60170333
  0.318441051  0.307047158  0.501139259  0.476710677  0.571850581  0.530455136
  0.548237368  0.50655925  0.796439812  0.730438032  0.774425531  0.681019099
  0.787602962 -1.33510056  0.333451001  0.312811354  0.580836708  0.488943429
  0.620938802  0.549741908  0.582714592  0.518048868  0.970391998  0.880883041
  0.868463283  0.768537543  0.808339582  0.76834557  1.04697639 -1.68075141
  0.199237447  0.197651865  0.306141201  0.299623712  0.305637736  0.301551532
  0.314506548  0.308167513  0.475008333  0.461190811  0.438766991  0.427349574
  0.523935826  0.493631912  0.575444517  0.531570906  0.562959964  0.534681337
  0.523253315  0.493533432  0.556332915  0.539960875  0.679568313  0.668911133
  0.702481275  0.660224324  0.746033686  0.719383718  0.764269293  0.720975882
  0.743823107  0.719467535  0.73415071  0.704017493  0.203876574  0.197017279
  0.319478305  0.29169636  0.312090565  0.298940212  0.29321323  0.286124333
  0.452398091  0.411493346  0.366781328  0.356135865  0.461280155  0.442799519
  0.422474368  0.412762941  0.458384068  0.440688021  0.450952671  0.432215597
  0.486197436  0.464454807  0.494328233  0.48587231  0.500594032  0.485211135
  0.515543387  0.507577182  0.517762043  0.503776845  0.512812863  0.499704277
  0.51478812  0.502585017  0.199951401  0.180787587  0.261923982  0.246142748
  0.271389033  0.254237078  0.270290946  0.253660343  0.337073712  0.319791078
  0.31117903  0.301386298  0.348044862  0.341502098  0.356096043  0.343681863
  0.355460526  0.348075142  0.346925017  0.340911843  0.364923063  0.354997401
  0.395051598  0.387031574  0.407881523  0.389071961  0.414552051  0.40395708
  0.410535391  0.404314119  0.401373214  0.396106137  0.404975263  0.396846337
  0.199972748  0.192808681  0.298303902  0.286405541  0.29281703  0.287680959
  0.299793209  0.293670755  0.450467132  0.426194239  0.406343071  0.399863697
  0.4679964  0.45539456  0.514918521  0.480674181  0.517392833  0.485182826
  0.471720435  0.461010053  0.512295307  0.497698185  0.639788352  0.625293446
  0.633057332  0.604124158  0.690892874  0.662614742  0.718920621  0.666584132
  0.673506799  0.657769536  0.693683775  0.669875704  0.199970469  0.198121005
  0.311234616  0.298905955  0.307695254  0.302470689  0.30918229  0.30265251
  0.469591865  0.444662379  0.42732975  0.414799009  0.494793042  0.485021683
  0.530893211  0.500497584  0.52978149  0.511203968  0.503015828  0.48757336
  0.532652778  0.518238389  0.654660561  0.637388996  0.659907514  0.617913662
  0.668386356  0.651841424  0.708544969  0.681201327  0.703563912  0.672071316
  0.676065816  0.650953848  0.163686171  0.161658437  0.330098363  0.306912425
  0.285961529  0.280551066  0.261415488  0.258726794  0.502696907  0.456341192
  0.477169067  0.441269791  0.455925767  0.445085512  0.532222682  0.49095028
  0.601302702  0.561072462  0.85208719  0.628747894  0.724188674  0.663687218
  0.74463864  0.688118571  0.733212411  0.689669236  0.166838541  0.163525701
  0.34589005  0.299318163  0.273488196  0.264183959  0.280591933  0.261274657
  0.473572189  0.410645286  0.371861952  0.359246724  0.468695069  0.434778066
  0.492356839  0.44125679  0.465816746  0.448290235  0.517830377  0.491373037
  0.509492582  0.488809868  0.538053409  0.511605212  0.51861278  0.495581065
  0.16922226  0.154464411  0.276242556  0.254295331  0.270091058  0.227311571
  0.265431992  0.226422607  0.346875398  0.315798146  0.330802484  0.301707359
  0.335801135  0.326858077  0.357709778  0.339787463  0.37410813  0.352461718
  0.409659071  0.395136656  0.404679471  0.387635689  0.416037563  0.402377801
  0.407037925  0.394008691  0.165529387  0.159827045  0.314526564  0.296189411
  0.27883128  0.267860012  0.259175473  0.248912462  0.457323994  0.42685402
  0.464939969  0.394380518  0.414141299  0.406101528  0.471223802  0.458702353
  0.576033004  0.506651596  0.678633434  0.641946765  0.636088212  0.607815148
  0.6855086  0.643467488  0.685330111  0.632986027  0.16386632  0.1627661
  0.334141544  0.313256918  0.278838678  0.274881545  0.265207828  0.261124823
  0.496530021  0.454201741  0.433895736  0.41807496  0.455532654  0.441508503
  0.54846003  0.469589539  0.562487509  0.538167125  0.686109835  0.631375042
  0.661960985  0.614265487  0.698911906  0.633203776  0.695046243  0.657090411
  0.317186254  0.304135051  0.498791854  0.458370453  0.574466026  0.508983591
  0.53631474  0.494104836  0.761133222  0.708761186  0.76664746  0.637181539
  0.80733287  0.647208425  0.778104275  0.736079544  0.338644835  0.280645374
  0.369013243  0.35894924  0.571343656  0.379955942  0.537642682  0.389356093
  0.507605946  0.486505173  0.505904671  0.486354241  0.521205846  0.484866247
  0.52882769  0.504146843  0.312832551  0.237656727  0.339666282  0.308092057
  0.388726347  0.332139308  0.363054929  0.342631258  0.407495957  0.394601942
  0.405881656  0.394581505  0.416240357  0.390387774  0.420759692  0.406479335
  0.314995193  0.285615154  0.474231553  0.418675  0.50107357  0.472182717
  0.493770737  0.442151419  0.722269766  0.653547298  0.642198577  0.612944366
  0.668823626  0.617589858  0.731680569  0.681675132  0.316765231  0.304177248
  0.4568538  0.430841271  0.561007705  0.493328978  0.551789718  0.457651991
  0.696374393  0.647010242  0.66308998  0.632991757  0.678825135  0.622181602
  0.758152322  0.67392751  0.730108994 -4.5911702  0.562163781  0.448436168
  0.478740312 -2.80777363  0.437342102  0.368854486  0.374865281  0.325155451
  0.308752165 -2.20459522  0.73625297  0.552891722  0.481070124  0.430771892
  0.450173039  0.307878085  0.600409001 -1.35624887  0.70765875  0.584130424
  0.58291261  0.418505675  0.402567706  0.365106584  0.599229834  0.552394054
  0.62268278 -0.747717661
 i,start,strti,fin,filind,bufszi,nmbuf,nmin2, nd4ext = 
         1         1         2         2        12      4095         1      1892      1892

 all internall diag. modified integrals

  4.73888533 -33.049462  1.04697806  0.0635775921  0.75633978 -7.87968877
  0.982105435  0.0304441088  0.678182812  0.124731145  0.720269923 -6.92041259
  0.888378871  0.0202631578  0.686297904  0.156106234  0.608788829  0.040066549
  0.674068355 -6.75760999  1.02562874  0.0300895777  0.707265436  0.127290671
  0.653389638  0.0413186341  0.621641696  0.0301711566  0.741234924 -7.0458577
 blstr diagel,allext,nmin2=  1892
 onel.diag.
 -0.194545E+01-0.286130E+01-0.270402E+01-0.267939E+01-0.404309E+01-0.353003E+01-0.366094E+01-0.377999E+01-0.336133E+01-0.260063E+01
 -0.287415E+01-0.357359E+01-0.152440E+01-0.144558E+01-0.152216E+01-0.749910E+00 0.955054E+00-0.155502E+01-0.292594E+01-0.240588E+01
 -0.200637E+01-0.405451E+01-0.340880E+01-0.250541E+01-0.287248E+01-0.299025E+01-0.266613E+01-0.118104E+01-0.155136E+01-0.373927E+00
 -0.277866E+01-0.388024E+01-0.376232E+01-0.283288E+01-0.443810E+01-0.160170E+01-0.133510E+01-0.168075E+01-0.459117E+01-0.280777E+01
 -0.220460E+01-0.135625E+01-0.747718E+00-0.330495E+02-0.787969E+01-0.692041E+01-0.675761E+01-0.704586E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1      1892      1892
 blstr diagel,allext,nmin2=  1892
 onel.diag.
 -0.194545E+01-0.286130E+01-0.270402E+01-0.267939E+01-0.404309E+01-0.353003E+01-0.366094E+01-0.377999E+01-0.336133E+01-0.260063E+01
 -0.287415E+01-0.357359E+01-0.152440E+01-0.144558E+01-0.152216E+01-0.749910E+00 0.955054E+00-0.155502E+01-0.292594E+01-0.240588E+01
 -0.200637E+01-0.405451E+01-0.340880E+01-0.250541E+01-0.287248E+01-0.299025E+01-0.266613E+01-0.118104E+01-0.155136E+01-0.373927E+00
 -0.277866E+01-0.388024E+01-0.376232E+01-0.283288E+01-0.443810E+01-0.160170E+01-0.133510E+01-0.168075E+01-0.459117E+01-0.280777E+01
 -0.220460E+01-0.135625E+01-0.747718E+00-0.330495E+02-0.787969E+01-0.692041E+01-0.675761E+01-0.704586E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1      1892      1892
 blstr diagel,allext,nmin2=  1892
 onel.diag.
 -0.194545E+01-0.286130E+01-0.270402E+01-0.267939E+01-0.404309E+01-0.353003E+01-0.366094E+01-0.377999E+01-0.336133E+01-0.260063E+01
 -0.287415E+01-0.357359E+01-0.152440E+01-0.144558E+01-0.152216E+01-0.749910E+00 0.955054E+00-0.155502E+01-0.292594E+01-0.240588E+01
 -0.200637E+01-0.405451E+01-0.340880E+01-0.250541E+01-0.287248E+01-0.299025E+01-0.266613E+01-0.118104E+01-0.155136E+01-0.373927E+00
 -0.277866E+01-0.388024E+01-0.376232E+01-0.283288E+01-0.443810E+01-0.160170E+01-0.133510E+01-0.168075E+01-0.459117E+01-0.280777E+01
 -0.220460E+01-0.135625E+01-0.747718E+00-0.330495E+02-0.787969E+01-0.692041E+01-0.675761E+01-0.704586E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1      1892      1892
 DIAGEL COUNTS: DG0X,DG2X,DG4X= 440 145 30
 =========== Executing IN-CORE method ==========
 norm multnew:  1.
 threx: total number of loops= 14
 threx: total number of loops= 24
 blstr diagel,allext,nmin2=  1892
 onel.diag.
 -0.194545E+01-0.286130E+01-0.270402E+01-0.267939E+01-0.404309E+01-0.353003E+01-0.366094E+01-0.377999E+01-0.336133E+01-0.260063E+01
 -0.287415E+01-0.357359E+01-0.152440E+01-0.144558E+01-0.152216E+01-0.749910E+00 0.955054E+00-0.155502E+01-0.292594E+01-0.240588E+01
 -0.200637E+01-0.405451E+01-0.340880E+01-0.250541E+01-0.287248E+01-0.299025E+01-0.266613E+01-0.118104E+01-0.155136E+01-0.373927E+00
 -0.277866E+01-0.388024E+01-0.376232E+01-0.283288E+01-0.443810E+01-0.160170E+01-0.133510E+01-0.168075E+01-0.459117E+01-0.280777E+01
 -0.220460E+01-0.135625E+01-0.747718E+00-0.330495E+02-0.787969E+01-0.692041E+01-0.675761E+01-0.704586E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1      1892      1892
 blstr diagel,allext,nmin2=  1892
 onel.diag.
 -0.194545E+01-0.286130E+01-0.270402E+01-0.267939E+01-0.404309E+01-0.353003E+01-0.366094E+01-0.377999E+01-0.336133E+01-0.260063E+01
 -0.287415E+01-0.357359E+01-0.152440E+01-0.144558E+01-0.152216E+01-0.749910E+00 0.955054E+00-0.155502E+01-0.292594E+01-0.240588E+01
 -0.200637E+01-0.405451E+01-0.340880E+01-0.250541E+01-0.287248E+01-0.299025E+01-0.266613E+01-0.118104E+01-0.155136E+01-0.373927E+00
 -0.277866E+01-0.388024E+01-0.376232E+01-0.283288E+01-0.443810E+01-0.160170E+01-0.133510E+01-0.168075E+01-0.459117E+01-0.280777E+01
 -0.220460E+01-0.135625E+01-0.747718E+00-0.330495E+02-0.787969E+01-0.692041E+01-0.675761E+01-0.704586E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1      1892      1892
 blstr diagel,allext,nmin2=  1892
 onel.diag.
 -0.194545E+01-0.286130E+01-0.270402E+01-0.267939E+01-0.404309E+01-0.353003E+01-0.366094E+01-0.377999E+01-0.336133E+01-0.260063E+01
 -0.287415E+01-0.357359E+01-0.152440E+01-0.144558E+01-0.152216E+01-0.749910E+00 0.955054E+00-0.155502E+01-0.292594E+01-0.240588E+01
 -0.200637E+01-0.405451E+01-0.340880E+01-0.250541E+01-0.287248E+01-0.299025E+01-0.266613E+01-0.118104E+01-0.155136E+01-0.373927E+00
 -0.277866E+01-0.388024E+01-0.376232E+01-0.283288E+01-0.443810E+01-0.160170E+01-0.133510E+01-0.168075E+01-0.459117E+01-0.280777E+01
 -0.220460E+01-0.135625E+01-0.747718E+00-0.330495E+02-0.787969E+01-0.692041E+01-0.675761E+01-0.704586E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1      1892      1892


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        12 xx:        36 ww:        79
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        11
task #   9:        28    task #  10:        71    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 matrix-vector product formed for vector   1
 rtolcosmo =   0.001
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference-subspace vector overlap matrix  block   1

                ci   1
 ref:   1     1.00000000

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000
 subspace has dimension  1

          (h-repnuc*1) matrix in the subspace basis

                x:   1
   x:   1   -85.40875183

          overlap matrix in the subspace basis

                x:   1
   x:   1     1.00000000

          eigenvectors and eigenvalues in the subspace basis

                v:   1

   energy   -85.40875183

   x:   1     1.00000000

          eigenvectors and eigenvalues in the schmidt orthonormalized basis

                v:   1

   energy   -85.40875183

  zx:   1     1.00000000

          <ref|baseci> overlap matrix  block   1

                ci   1
 ref:   1     1.00000000

  write the transformed vectors back to cosmofile

 rtflw =  0
 freezing =  F

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -76.0726719487 -9.2989E+00  3.1915E-01  1.4431E+00  1.0000E-03


 total energy in cosmo calc 
e(rootcalc) + repnuc=     -76.0726719487
dielectric energy =      -0.0095031456
deltaediel =       0.0095031456
e(rootcalc) + repnuc - ediel =     -76.0631688032
e(rootcalc) + repnuc - edielnew =     -76.0726719487
deltaelast =      76.0631688032

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.02   0.00   0.00   0.01         0.    0.0000
    2   25    0   0.01   0.00   0.00   0.01         0.    0.0000
    3   26    0   0.03   0.00   0.00   0.03         0.    0.0000
    4   11    0   0.01   0.00   0.00   0.01         0.    0.0000
    5   15    0   0.02   0.00   0.00   0.02         0.    0.0000
    6   16    0   0.03   0.00   0.00   0.02         0.    0.0000
    7    1    0   0.01   0.00   0.00   0.01         0.    0.0000
    8    5    0   0.01   0.00   0.00   0.01         0.    0.0000
    9    6    0   0.03   0.00   0.00   0.02         0.    0.0000
   10    7    0   0.03   0.00   0.00   0.03         0.    0.0000
   11   75    0   0.01   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.03   0.00   0.00   0.03         0.    0.0000
   14   47    0   0.03   0.00   0.00   0.03         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.2700s 
time spent in multnx:                   0.2300s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0200s 
total time per CI iteration:            1.7600s 

          starting ci iteration   2

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.33607988

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=       3  D0X=       9  D0W=      15
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================

          ci-one-electron density block   1

                mo   1         mo   2         mo   3         mo   4         mo   5         mo   6         mo   7         mo   8
   mo   1     2.00000000
   mo   2     0.00000000     2.00000000
   mo   3     0.00000000     0.00000000     2.00000000

                mo   9         mo  10         mo  11         mo  12         mo  13         mo  14         mo  15         mo  16

                mo  17         mo  18         mo  19         mo  20

          ci-one-electron density block   2

                mo  21         mo  22         mo  23         mo  24         mo  25         mo  26         mo  27         mo  28
   mo  21     2.00000000

                mo  29         mo  30         mo  31         mo  32         mo  33         mo  34

          ci-one-electron density block   3

                mo  35         mo  36         mo  37         mo  38         mo  39         mo  40         mo  41         mo  42
   mo  35     2.00000000

                mo  43

          ci-one-electron density block   4

                mo  44         mo  45         mo  46         mo  47         mo  48


*****   symmetry block SYM1   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       2.00000000
       2       0.00000000     2.00000000
       3       0.00000000     0.00000000     2.00000000

               Column   5     Column   6     Column   7     Column   8

               Column   9     Column  10     Column  11     Column  12

               Column  13     Column  14     Column  15     Column  16

               Column  17     Column  18     Column  19     Column  20

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7        eno   8
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    8   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno   9        eno  10        eno  11        eno  12        eno  13        eno  14        eno  15        eno  16
 emo    9   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   10   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   11   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   12   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   13   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   14   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   15   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   16   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno  17        eno  18        eno  19        eno  20
 emo   17   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   18   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   19   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   20   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   2.0000000      2.0000000      2.0000000      0.0000000      0.0000000
   0.0000000      0.0000000      0.0000000      0.0000000      0.0000000
   0.0000000      0.0000000      0.0000000      0.0000000      0.0000000
   0.0000000      0.0000000      0.0000000      0.0000000      0.0000000

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7         no   8
  mo    1   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo    2   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo    3   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
  mo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
  mo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
  mo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01
  mo   20   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

                no   9         no  10         no  11         no  12         no  13         no  14         no  15         no  16
  mo    8   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo    9   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo   10   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo   11   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo   12   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
  mo   13   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
  mo   14   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
  mo   15   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

                no  17         no  18         no  19         no  20
  mo   16   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
  mo   17   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
  mo   18   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
  mo   19   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01
  eigenvectors of nos in ao-basis, column    1
  0.5515823E-02  0.3143126E+00  0.2818983E-02  0.1277576E+00  0.8098635E+00 -0.1012754E-01 -0.1718911E-01 -0.1563787E-02
 -0.2289464E-02 -0.6151497E-02  0.6842722E-03 -0.2713131E-03  0.2306609E-02 -0.3392655E+00  0.1352449E+00  0.5903180E-02
 -0.2451740E-01  0.3077675E-02 -0.1694408E-02  0.2904306E-01
  eigenvectors of nos in ao-basis, column    2
  0.9986657E+00 -0.2886031E-02  0.1478624E-02  0.2374507E-02 -0.4737345E-02  0.3961596E-02  0.1318110E-02  0.5964829E-05
  0.1153723E-04 -0.2783492E-04 -0.2824841E-03  0.1186061E-04  0.4481920E-04  0.2151009E-02 -0.1958045E-02  0.9669327E-04
  0.6393350E-03  0.4291770E-03 -0.3107908E-03 -0.2453036E-03
  eigenvectors of nos in ao-basis, column    3
 -0.6317585E-02  0.8728930E+00 -0.7920069E-02 -0.1582526E+00 -0.1221160E+00  0.1246282E-01  0.7008680E-01  0.4727738E-03
  0.2945654E-02 -0.3281494E-03 -0.2745901E-02 -0.7883363E-03 -0.2399105E-02  0.3366056E+00 -0.1483694E+00  0.1543447E-02
  0.3237704E-01  0.1937093E-01  0.1397092E-01  0.3384409E-02
  eigenvectors of nos in ao-basis, column    4
 -0.3258136E+01 -0.7842233E+01  0.4083870E+01  0.5461683E+01 -0.6278669E+00  0.1320734E+01  0.1539460E+00  0.1206279E-02
  0.1378803E-01 -0.1176279E-01 -0.2424371E+00  0.4935770E-01  0.1290585E+00  0.1600846E+01 -0.1536540E+01  0.6590637E-01
  0.6208117E+00  0.4975527E+00 -0.3587064E+00 -0.3161000E+00
  eigenvectors of nos in ao-basis, column    5
 -0.1896548E-01 -0.1960165E+00  0.3045490E-02  0.2940520E+00  0.8348312E-01  0.4467293E-02 -0.9794704E-01 -0.2041547E-03
 -0.5607408E-03  0.1881129E-03  0.5091334E-02 -0.1447916E-03 -0.4914821E-03 -0.6416044E-02 -0.3049176E+00  0.6526054E+00
 -0.2390047E-01 -0.1113291E-01 -0.9143035E-01 -0.9101806E-01
  eigenvectors of nos in ao-basis, column    6
 -0.6933849E-02  0.2056387E+00 -0.3968204E-03  0.4719356E+00 -0.3299958E+00 -0.3771450E-02 -0.3865908E+00 -0.1271028E-04
 -0.1859061E-02  0.5061668E-02  0.8431468E-02 -0.9737074E-03 -0.3876942E-02 -0.1541303E+00 -0.1034181E+01  0.5511846E+00
 -0.1993430E-01 -0.1324493E-01  0.2299308E+00  0.2468760E+00
  eigenvectors of nos in ao-basis, column    7
  0.1334365E-01 -0.1014268E+00 -0.7957062E-02 -0.5296659E+00 -0.1742761E+00  0.5368161E-01 -0.3887280E+00 -0.2410368E-02
  0.3115320E-02  0.1097053E-02  0.3505297E-02 -0.1245167E-02 -0.1079863E-02  0.1340848E+00  0.2991939E+00 -0.8008037E-01
  0.9720424E-02 -0.7078118E-02 -0.3771643E+00  0.6893968E+00
  eigenvectors of nos in ao-basis, column    8
 -0.1038740E+00 -0.5268750E-01  0.3302977E-01  0.3145348E+01  0.7134985E-01 -0.2587084E-01 -0.4243236E+00 -0.3763911E-03
  0.1497330E-02 -0.5300462E-03  0.2541430E-01 -0.1971921E-02 -0.5238674E-02 -0.1074387E+00 -0.1221210E+01  0.1618867E+00
 -0.4011088E-01 -0.2159955E-01 -0.1082452E+01 -0.6994266E+00
  eigenvectors of nos in ao-basis, column    9
 -0.6046352E-01 -0.5189847E+00 -0.2810976E-01  0.9783263E+00  0.6259861E+00  0.2478947E-01 -0.9245004E+00 -0.2746654E-02
  0.2333137E-02  0.9384163E-02  0.1212225E+00 -0.4663612E-02 -0.1607161E-01  0.7859311E+00 -0.1305086E+01  0.2776766E+00
 -0.3137467E+00 -0.1328866E+00  0.2314745E+00  0.1318239E+00
  eigenvectors of nos in ao-basis, column   10
 -0.4436934E-01 -0.3848869E+00 -0.4547481E-02  0.7229718E+00 -0.3155865E+00 -0.5361571E+00  0.1879006E+01  0.3219334E-02
  0.4487940E-02  0.1362031E-01  0.7686124E-01 -0.2243167E-02 -0.6304388E-02  0.9024371E+00 -0.7437867E+00 -0.1381213E-01
 -0.7492460E-01 -0.1354680E+00 -0.4326634E-02 -0.4772989E+00
  eigenvectors of nos in ao-basis, column   11
  0.1212447E+00  0.5248185E+00 -0.1120275E-01 -0.1541380E+01 -0.2542175E+00 -0.1101477E-01  0.5194494E+00 -0.3118431E-02
  0.7723896E-02 -0.1174546E+00  0.5068847E-01  0.3329783E-02  0.4308658E-02  0.1954633E+00  0.2744301E+00 -0.3643862E-01
 -0.2171731E+00  0.4729939E+00  0.4150133E+00 -0.1270453E+00
  eigenvectors of nos in ao-basis, column   12
  0.4747217E+00  0.2094003E+01 -0.2527890E-01 -0.5525154E+01  0.1491890E+00  0.2477071E+00  0.3893550E+00 -0.6584408E-03
  0.3464144E-02  0.9329081E-01  0.2007740E+00  0.6369915E-02  0.9886964E-02  0.7880082E+00  0.9436755E+00 -0.1370044E+00
  0.2854254E+00  0.1170632E+00  0.5472427E+00  0.5341474E+00
  eigenvectors of nos in ao-basis, column   13
  0.5598679E+00  0.2455941E+01 -0.2030198E-01 -0.4417453E+01  0.8184541E-01 -0.3468412E+00  0.9048091E+00  0.9421415E-03
  0.7189466E-02 -0.7092601E-01 -0.3216104E+00 -0.1122974E-01 -0.2225644E-01  0.1145785E+01  0.1350716E+00 -0.6200774E-01
 -0.2992456E+00 -0.3102353E+00  0.4534710E+00  0.2800140E+00
  eigenvectors of nos in ao-basis, column   14
 -0.1137147E-01 -0.8594545E-01  0.5340616E-02 -0.3231910E+00  0.3577348E+00 -0.3609437E-02  0.5113291E+00  0.3742604E-02
 -0.2887268E-02 -0.3559932E+00  0.2271254E+00  0.7261592E-03 -0.1834002E-02  0.4373582E+00 -0.8560244E-01 -0.5257139E-01
  0.7160715E+00 -0.6488710E+00 -0.1361723E+00 -0.1601128E-01
  eigenvectors of nos in ao-basis, column   15
 -0.7481917E-01 -0.6400475E+00  0.9131747E-02 -0.3193046E+01  0.1024077E+01  0.1148806E+00  0.8311160E+00 -0.1425892E-02
  0.2181597E-01  0.1421880E-01 -0.5660696E+00 -0.3005103E-02  0.3314779E-02  0.3066280E+01 -0.8167482E+00 -0.1242603E+00
  0.8682959E+00  0.8734539E+00  0.1402273E+00  0.1352925E-02
  eigenvectors of nos in ao-basis, column   16
  0.8899255E-01  0.2451066E-01 -0.1385021E+00 -0.1996855E+01 -0.2957909E+01  0.3414853E+01  0.7385109E+00 -0.2121826E-02
  0.1539053E-01 -0.4649506E-01 -0.1778044E+00 -0.2750633E-01 -0.1400864E+00  0.1499659E+01 -0.4168014E+00 -0.1268201E-01
  0.5651871E+00  0.1788439E+00  0.9442474E-01  0.2439826E+00
  eigenvectors of nos in ao-basis, column   17
 -0.4178131E-02 -0.7020462E-02  0.2488846E-02  0.9755560E-01  0.2257976E+00 -0.2794828E+00 -0.1003540E+00  0.3956452E-01
 -0.2171891E-01  0.2297642E-01 -0.2286353E-01  0.3554663E+00 -0.2028487E+00 -0.8900179E-01  0.1825039E-01  0.7261508E-02
 -0.1373799E+00  0.1367059E+00  0.3846298E-01 -0.3374215E-01
  eigenvectors of nos in ao-basis, column   18
  0.3307499E+00  0.6952940E+00 -0.3608733E+00 -0.3199790E+01 -0.9073467E+00  0.1413382E+01  0.8465276E+00 -0.2470921E-01
 -0.3521651E+00 -0.1032994E-01 -0.8226476E-01  0.1306090E+00  0.3780075E+00  0.2001685E+01 -0.4624786E+00 -0.4769118E-01
  0.4726113E+00  0.3404801E+00  0.2540434E+00  0.1597870E+00
  eigenvectors of nos in ao-basis, column   19
  0.1191641E+00  0.2807008E+00 -0.1086279E+00 -0.1095557E+01 -0.2945650E+00  0.4695848E+00  0.5404002E+00  0.3361104E+00
  0.6565018E-01 -0.2414595E+00 -0.7773214E-01 -0.1793185E-01  0.1234513E+00  0.8109871E+00 -0.2374765E+00 -0.2710490E-01
  0.2689981E+00 -0.8875936E-01  0.7033664E-01 -0.1364217E-01
  eigenvectors of nos in ao-basis, column   20
  0.3540953E+00  0.8344425E+00 -0.3279961E+00 -0.3322895E+01 -0.6812585E+00  0.1099558E+01  0.8463938E+00 -0.7105633E-01
  0.4816806E+00  0.6249602E-01 -0.5795095E+00  0.1494137E+00  0.3175916E+00  0.2222291E+01 -0.6003780E+00 -0.5356837E-01
  0.2693768E+00  0.4247536E+00  0.2229944E+00  0.1465473E+00


*****   symmetry block SYM2   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       2.00000000

               Column   5     Column   6     Column   7     Column   8

               Column   9     Column  10     Column  11     Column  12

               Column  13     Column  14

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7        eno   8
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    8   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno   9        eno  10        eno  11        eno  12        eno  13        eno  14
 emo    9   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   10   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   11   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   12   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   13   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   14   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   2.0000000      0.0000000      0.0000000      0.0000000      0.0000000
   0.0000000      0.0000000      0.0000000      0.0000000      0.0000000
   0.0000000      0.0000000      0.0000000      0.0000000

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7         no   8
  mo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo    2   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo    3   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
  mo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
  mo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
  mo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01
  mo   14   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

                no   9         no  10         no  11         no  12         no  13         no  14
  mo    8   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo    9   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo   10   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
  mo   11   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
  mo   12   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
  mo   13   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01
  eigenvectors of nos in ao-basis, column    1
  0.7339992E+00 -0.1957530E-01 -0.1266443E+00 -0.1650150E-01 -0.1230093E-01  0.3177531E-03 -0.3455797E-02 -0.5562385E+00
  0.1601369E+00 -0.7422997E-02 -0.2266358E-01 -0.3034386E-01 -0.1658476E-01 -0.2086616E-01
  eigenvectors of nos in ao-basis, column    2
 -0.1280147E+01  0.2193793E+01  0.2109494E+01  0.1061793E+01 -0.1934978E+01  0.7928737E-01 -0.2658765E+00  0.3406694E+01
 -0.4421342E+00 -0.9353217E-01  0.6667420E+00  0.4493647E+00  0.3946174E+00  0.3713557E+00
  eigenvectors of nos in ao-basis, column    3
  0.1093068E+00  0.1075461E-01 -0.1870036E+00 -0.2588802E-02  0.1082030E-01 -0.1047559E-03 -0.9794784E-04 -0.4294943E-01
 -0.5479296E+00  0.2376678E+01 -0.1446412E-01 -0.1063719E-01 -0.1141627E+00 -0.1307420E+00
  eigenvectors of nos in ao-basis, column    4
 -0.3812017E+00 -0.2606910E-01 -0.1885979E+00 -0.5722278E-02  0.1888576E-01 -0.3335429E-04  0.2851727E-02 -0.1481097E+00
 -0.4676676E+00  0.1183968E+01 -0.1647629E-01 -0.5756699E-02  0.5263456E+00  0.4535307E+00
  eigenvectors of nos in ao-basis, column    5
  0.2246490E+00 -0.1045048E+00  0.1472488E+01 -0.1836010E-02 -0.7571751E-01  0.1086397E-02 -0.2784463E-02 -0.3588376E-01
  0.2972684E+01 -0.9390370E+00  0.1679260E-01  0.5302917E-02  0.5363269E+00  0.1435720E+01
  eigenvectors of nos in ao-basis, column    6
  0.1544217E+00 -0.7351334E-01  0.1768133E+01  0.7563471E-02 -0.4736159E-01 -0.7786162E-03 -0.3350391E-02  0.7775367E-01
  0.8107401E+01 -0.1335594E-01 -0.1398208E-01  0.3720163E-01  0.4290084E+01  0.6509883E+00
  eigenvectors of nos in ao-basis, column    7
 -0.6576016E+00  0.1523096E+00  0.2460193E+00 -0.9977391E-02 -0.3488036E+00  0.2637915E-02 -0.9253229E-02 -0.1078601E+01
  0.1617104E+01 -0.5484056E+00  0.2161974E+00  0.2351352E+00  0.9001924E-01 -0.4477784E+00
  eigenvectors of nos in ao-basis, column    8
 -0.4726086E+00 -0.5619155E+00  0.4135688E+01  0.3267579E-01  0.6928461E-01  0.4394980E-03 -0.3647842E-02  0.9009467E+00
  0.4400639E+01 -0.5517123E+00  0.6925670E-01  0.2390099E-01  0.1391751E+01  0.1310313E+01
  eigenvectors of nos in ao-basis, column    9
  0.1476670E+00 -0.5035282E-01  0.1059066E+01 -0.1050263E-01  0.1904374E+00 -0.3055882E-02 -0.5375383E-02  0.2142120E+00
  0.2371163E+01  0.2211675E-01 -0.5378889E+00  0.7175725E+00  0.1248590E+01  0.7560602E-01
  eigenvectors of nos in ao-basis, column   10
  0.3532031E+00  0.9153486E+00 -0.1572021E+01  0.2189085E-01  0.3138381E+00  0.5197966E-02 -0.3043068E-01  0.2364209E-01
 -0.1926808E+01 -0.7440015E-01  0.8248592E+00  0.5472483E+00 -0.9245816E+00 -0.5633648E+00
  eigenvectors of nos in ao-basis, column   11
  0.1342051E+01 -0.2312123E+00  0.2664904E+01  0.3223863E-01 -0.1729438E+01 -0.2240352E-02  0.2488830E-01  0.3515438E+01
  0.1179648E+01 -0.3249828E+00  0.7974130E+00  0.6148220E+00  0.8353970E+00  0.6976872E+00
  eigenvectors of nos in ao-basis, column   12
 -0.2524279E+01  0.3353163E+01  0.2731226E+00  0.6476768E-01 -0.6959761E+00 -0.4718583E-01  0.2993238E+00  0.1580324E+01
 -0.1487102E+01  0.8469951E-01  0.5043800E+00  0.4763270E+00 -0.1708948E+00 -0.2377382E+00
  eigenvectors of nos in ao-basis, column   13
  0.4824277E+00 -0.6856399E+00 -0.5501167E+00 -0.1481537E-01  0.1806399E+00  0.1254268E+00  0.4331735E+00 -0.5708498E+00
 -0.2951617E+00  0.2105897E-02  0.7648452E-01 -0.3666507E+00 -0.2782562E+00 -0.1895298E-01
  eigenvectors of nos in ao-basis, column   14
 -0.1747994E+01  0.2829173E+01  0.8404603E+00 -0.6597519E+00 -0.7022573E+00  0.8604840E-01 -0.2902789E+00  0.2479199E+01
 -0.1594204E+01  0.1407320E-01  0.7836697E+00  0.5353430E+00 -0.2038212E+00  0.1634377E-01


*****   symmetry block SYM3   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       2.00000000

               Column   5     Column   6     Column   7     Column   8

               Column   9

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7        eno   8
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    8   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno   9
 emo    9   0.100000E+01

 occupation numbers of nos
   2.0000000      0.0000000      0.0000000      0.0000000      0.0000000
   0.0000000      0.0000000      0.0000000      0.0000000

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7         no   8
  mo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo    2   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo    3   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
  mo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
  mo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
  mo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01
  mo    9   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00

                no   9
  mo    8   0.100000E+01
  eigenvectors of nos in ao-basis, column    1
  0.9137470E+00  0.8207613E-02  0.3452394E-01 -0.5026550E-02 -0.2193112E-01  0.1158050E-02  0.3395425E-03  0.2414960E-01
  0.3712296E-01
  eigenvectors of nos in ao-basis, column    2
  0.1840143E-01 -0.8465985E-02  0.2271669E+00  0.1168846E+01 -0.7158249E+00  0.3726690E-01  0.1296141E-01 -0.1699930E+00
 -0.7407014E-01
  eigenvectors of nos in ao-basis, column    3
 -0.3685786E+00  0.7771037E-01 -0.7276585E+00 -0.6706259E-02  0.1496248E-01 -0.4272171E-04  0.1503376E-03 -0.1681463E-01
  0.8732222E+00
  eigenvectors of nos in ao-basis, column    4
 -0.5888417E+00 -0.5235677E+00  0.2169136E+01  0.1110385E-01  0.5437990E-01  0.1132001E-02 -0.2148770E-04 -0.1086267E+00
 -0.5404860E+00
  eigenvectors of nos in ao-basis, column    5
  0.3497313E+00  0.1146727E+00 -0.5415655E+00  0.3438462E-03  0.5938959E+00  0.3539903E-02 -0.7641688E-03 -0.4729295E+00
  0.3444872E+00
  eigenvectors of nos in ao-basis, column    6
 -0.2132941E+00  0.2421387E-02 -0.4466602E+00 -0.2106667E-01  0.1016346E+01  0.3810420E-03 -0.2159145E-02  0.8144629E+00
  0.7681662E-02
  eigenvectors of nos in ao-basis, column    7
 -0.3222164E+01  0.3183987E+01  0.1547309E+00  0.2048941E-01 -0.4450805E-01 -0.1034152E-01 -0.2617833E-02 -0.1697978E+00
  0.2432818E+00
  eigenvectors of nos in ao-basis, column    8
  0.1401414E-01 -0.1683489E-01 -0.1668790E-01 -0.1445650E-01  0.3699340E-01 -0.7530972E-01  0.1469433E+00  0.2162142E-01
  0.8166381E-03
  eigenvectors of nos in ao-basis, column    9
 -0.2818450E+00  0.3392979E+00  0.1302061E+00 -0.1708410E+00 -0.8715262E-01  0.1958063E+00  0.5914437E-01 -0.2832258E+00
  0.4818875E-01


*****   symmetry block SYM4   *****


 first order reduced density matrix in mo basis



     Zero matrix.

               eno   1        eno   2        eno   3        eno   4        eno   5
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   0.0000000      0.0000000      0.0000000      0.0000000      0.0000000

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5
  mo    1   0.595930E+00  -0.396489E+00  -0.284734E+00   0.233682E+00   0.876011E+00
  mo    2  -0.938710E+00  -0.704713E+00  -0.375329E+00  -0.282795E+00  -0.769283E+00
  mo    3   0.869515E+00   0.535022E-01  -0.220130E+00  -0.356787E+00  -0.456824E+00
  mo    4  -0.691054E+00   0.372799E-01  -0.624725E+00  -0.415268E+00   0.169827E+00
  mo    5   0.160642E+00   0.734421E-02  -0.320386E+00   0.997272E+00  -0.591106E+00
  eigenvectors of nos in ao-basis, column    1
 -0.2754461E+00  0.6729100E-01  0.3836606E+00  0.1919735E-01  0.1606561E+01
  eigenvectors of nos in ao-basis, column    2
  0.4073958E+00  0.4932066E+00  0.1041821E+00  0.4045085E+00  0.2553677E+00
  eigenvectors of nos in ao-basis, column    3
 -0.5698708E+00  0.1185119E+01  0.1527356E+00  0.4305843E+00  0.2164403E+00
  eigenvectors of nos in ao-basis, column    4
  0.1350193E+00 -0.5743301E+00  0.1258899E+01 -0.4261497E+00  0.1421771E+00
  eigenvectors of nos in ao-basis, column    5
 -0.4385256E+00  0.2248173E+00 -0.2123567E+00  0.1140279E+01 -0.3980187E+00


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***


 cosurf_and_phi
  1.262739748330  2.872870034186  1.567866969040 -0.040563960056
  1.652947872482  0.360731428865  3.496571379926 -0.047744392345
  0.594940552015  0.670555958619  3.845537291721 -0.057596471692
  2.390078334690  1.202722009900  2.566708449184 -0.030282523563
  1.989334673621  2.229216060677  2.001028520754 -0.033627713369
  2.919143888229  0.378144896597  2.099775822683 -0.010812250138
  0.105380882100  1.878884201281  3.371423292662 -0.058292817904
  0.783266721831  2.590884380442  2.520834408126 -0.052559672735
  2.011402526412  2.551496219550  0.814855178005 -0.016418121584
  0.099626804209  1.855073908563 -1.945822518898  0.025971373658
  0.119020599620  3.173161356543  1.415159765853 -0.046386018763
  0.915237473358  3.108118624501  0.463299650838 -0.029665043076
  0.462814589486  2.837210699514 -0.795516582628 -0.007994178958
  1.408719892640  1.640288870679  3.148120355694 -0.049774159204
  2.650053602975  1.633766196698  1.655441764425 -0.012846014881
  1.142904167226  2.885766749848 -0.243475252462 -0.012804822376
  3.656106873706  0.713798214804  0.361832640492  0.036992904741
  2.408046317685  2.075600470298 -1.226192756897  0.036677335852
  1.295820311657  0.567673501678 -2.747601655947  0.052519534722
  3.527730862121  1.045649613724 -1.064853177123  0.052163873281
  2.622898581638  1.024710819001 -2.241122746235  0.055742482515
  3.086808508398  1.794857685487 -0.179703829578  0.032455615757
  1.615872011596  1.674366500124 -2.147504385867  0.043121867877
  0.000006852884  1.035694667087 -2.361676372823  0.043679457008
  0.298815704890  0.969607820141 -2.408807386530  0.045294705757
  0.667723897920  1.245157743773 -2.338577856151  0.043527638139
  1.218123388571  2.025110412626 -1.616576841774  0.030862927502
  2.084186643139  2.293984793080 -0.480493513306  0.019468320575
  2.876642520254  1.658030225134  0.559035126479  0.020003529333
  3.282919570196  0.368088739237  1.091983758387  0.023913389296
  0.467932162408  1.694535407938 -1.941510815499  0.032266883089
  1.299770347024  2.453875604722 -0.850324284820  0.009231499230
  2.242050270258  2.245320705184  0.385739526123  0.002433939445
  2.923104801922  1.151131828431  1.279135167181  0.007476050149
  0.427044967836  0.580963354323 -2.585108185092  0.049002947599
 end_of_phi


 nsubv after electrostatic potential =  1

  Confirmation of dielectric energy for ground state
  edielnew =  -0.00237578639

 =========== Executing IN-CORE method ==========
 norm multnew:  0.0868834472
 threx: total number of loops= 14
 threx: total number of loops= 24
 blstr diagel,allext,nmin2=  1892
 onel.diag.
 -0.194545E+01-0.286130E+01-0.270402E+01-0.267939E+01-0.404309E+01-0.353003E+01-0.366094E+01-0.377999E+01-0.336133E+01-0.260063E+01
 -0.287415E+01-0.357359E+01-0.152440E+01-0.144558E+01-0.152216E+01-0.749910E+00 0.955054E+00-0.155502E+01-0.292594E+01-0.240588E+01
 -0.200637E+01-0.405451E+01-0.340880E+01-0.250541E+01-0.287248E+01-0.299025E+01-0.266613E+01-0.118104E+01-0.155136E+01-0.373927E+00
 -0.277866E+01-0.388024E+01-0.376232E+01-0.283288E+01-0.443810E+01-0.160170E+01-0.133510E+01-0.168075E+01-0.459117E+01-0.280777E+01
 -0.220460E+01-0.135625E+01-0.747718E+00-0.330495E+02-0.787969E+01-0.692041E+01-0.675761E+01-0.704586E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1      1892      1892
 blstr diagel,allext,nmin2=  1892
 onel.diag.
 -0.194545E+01-0.286130E+01-0.270402E+01-0.267939E+01-0.404309E+01-0.353003E+01-0.366094E+01-0.377999E+01-0.336133E+01-0.260063E+01
 -0.287415E+01-0.357359E+01-0.152440E+01-0.144558E+01-0.152216E+01-0.749910E+00 0.955054E+00-0.155502E+01-0.292594E+01-0.240588E+01
 -0.200637E+01-0.405451E+01-0.340880E+01-0.250541E+01-0.287248E+01-0.299025E+01-0.266613E+01-0.118104E+01-0.155136E+01-0.373927E+00
 -0.277866E+01-0.388024E+01-0.376232E+01-0.283288E+01-0.443810E+01-0.160170E+01-0.133510E+01-0.168075E+01-0.459117E+01-0.280777E+01
 -0.220460E+01-0.135625E+01-0.747718E+00-0.330495E+02-0.787969E+01-0.692041E+01-0.675761E+01-0.704586E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1      1892      1892
 blstr diagel,allext,nmin2=  1892
 onel.diag.
 -0.194545E+01-0.286130E+01-0.270402E+01-0.267939E+01-0.404309E+01-0.353003E+01-0.366094E+01-0.377999E+01-0.336133E+01-0.260063E+01
 -0.287415E+01-0.357359E+01-0.152440E+01-0.144558E+01-0.152216E+01-0.749910E+00 0.955054E+00-0.155502E+01-0.292594E+01-0.240588E+01
 -0.200637E+01-0.405451E+01-0.340880E+01-0.250541E+01-0.287248E+01-0.299025E+01-0.266613E+01-0.118104E+01-0.155136E+01-0.373927E+00
 -0.277866E+01-0.388024E+01-0.376232E+01-0.283288E+01-0.443810E+01-0.160170E+01-0.133510E+01-0.168075E+01-0.459117E+01-0.280777E+01
 -0.220460E+01-0.135625E+01-0.747718E+00-0.330495E+02-0.787969E+01-0.692041E+01-0.675761E+01-0.704586E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1      1892      1892


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        12 xx:        36 ww:        79
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        11
task #   9:        28    task #  10:        71    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 matrix-vector product formed for vector   2
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference-subspace vector overlap matrix  block   1

                ci   1         ci   2
 ref:   1     1.00000000     0.00000000

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97358879    -0.22830870
 subspace has dimension  2

          (h-repnuc*1) matrix in the subspace basis

                x:   1         x:   2
   x:   1   -85.40875183
   x:   2    -0.31915444    -7.04150281

          overlap matrix in the subspace basis

                x:   1         x:   2
   x:   1     1.00000000
   x:   2     0.00000000     0.08688345

          eigenvectors and eigenvalues in the subspace basis

                v:   1         v:   2

   energy   -85.66266151   -80.79147969

   x:   1     0.97358879    -0.22830870
   x:   2     0.77455800     3.30298837

          eigenvectors and eigenvalues in the schmidt orthonormalized basis

                v:   1         v:   2

   energy   -85.66266151   -80.79147969

  zx:   1     0.97358879    -0.22830870
  zx:   2     0.22830870     0.97358879

          <ref|baseci> overlap matrix  block   1

                ci   1         ci   2
 ref:   1     1.00000000     0.00000000

  write the transformed vectors back to cosmofile

 rtflw =  0
 freezing =  F

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1    -76.3265816350 -9.0822E+00  1.0861E-02  2.7085E-01  1.0000E-03
 mr-sdci #  2  2    -71.4553998143  7.1455E+01  0.0000E+00  3.6593E+00  1.0000E-04


 total energy in cosmo calc 
e(rootcalc) + repnuc=     -76.3265816350
dielectric energy =      -0.0095031456
deltaediel =       0.0095031456
e(rootcalc) + repnuc - ediel =     -76.3170784894
e(rootcalc) + repnuc - edielnew =     -76.3265816350
deltaelast =      76.3170784894

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.02   0.00   0.00   0.02         0.    0.0000
    2   25    0   0.02   0.00   0.00   0.01         0.    0.0000
    3   26    0   0.03   0.00   0.00   0.03         0.    0.0000
    4   11    0   0.02   0.00   0.00   0.02         0.    0.0000
    5   15    0   0.04   0.00   0.00   0.03         0.    0.0000
    6   16    0   0.04   0.00   0.00   0.03         0.    0.0000
    7    1    0   0.02   0.00   0.00   0.01         0.    0.0000
    8    5    0   0.02   0.00   0.00   0.02         0.    0.0000
    9    6    0   0.04   0.00   0.00   0.04         0.    0.0000
   10    7    0   0.04   0.00   0.00   0.03         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.04   0.00   0.00   0.04         0.    0.0000
   14   47    0   0.04   0.00   0.00   0.04         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3700s 
time spent in multnx:                   0.3200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0200s 
total time per CI iteration:            1.6600s 

          starting ci iteration   3

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.33607988

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=       3  D0X=       9  D0W=      15
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================

          ci-one-electron density block   1

                mo   1         mo   2         mo   3         mo   4         mo   5         mo   6         mo   7         mo   8
   mo   1     1.99970893
   mo   2     0.00001754     1.98666986
   mo   3    -0.00011711     0.00253833     1.97009935
   mo   4     0.00000138    -0.00056370     0.01404698     0.00099533
   mo   5    -0.00001442     0.00242632    -0.01790481    -0.00225101     0.00629066
   mo   6    -0.00001393     0.00004488    -0.00861090    -0.00021453     0.00161951     0.00310346
   mo   7     0.00001241    -0.00112802    -0.00866593    -0.00061128     0.00000934    -0.00121807     0.00217313
   mo   8     0.00002941    -0.00257056     0.00247386     0.00238426    -0.00723832    -0.00122951     0.00053801     0.00935974
   mo   9    -0.00002939     0.00067307     0.00875631    -0.00021072     0.00216896     0.00321348    -0.00187799    -0.00208784
   mo  10    -0.00000984     0.00057930    -0.00677743    -0.00073049     0.00188133     0.00229710     0.00050002    -0.00137882
   mo  11     0.00002500    -0.00135022    -0.00026226    -0.00038715    -0.00072656    -0.00155365     0.00231184     0.00164100
   mo  12     0.00001388    -0.00024055    -0.00025564    -0.00056726     0.00073881    -0.00132233     0.00116406    -0.00115279
   mo  13     0.00001319    -0.00013604    -0.00118441     0.00019553    -0.00075406     0.00020555     0.00029862     0.00124190
   mo  14     0.00003085    -0.00015057    -0.00022064     0.00070783    -0.00196135    -0.00047844    -0.00018646     0.00253422
   mo  15    -0.00007189     0.00052915     0.00057824    -0.00022579     0.00116030     0.00107084    -0.00060384    -0.00141536
   mo  16     0.00000498    -0.00005580     0.00014069     0.00002834    -0.00009973    -0.00016773    -0.00000280     0.00007601
   mo  17    -0.00002013     0.00005534    -0.00004984     0.00005283     0.00010635     0.00001153    -0.00034723    -0.00041900
   mo  18    -0.00000547     0.00003266     0.00013497     0.00004512    -0.00009629    -0.00013892    -0.00008678     0.00009560
   mo  19    -0.00001596     0.00007464     0.00001405     0.00005187    -0.00007678     0.00032664    -0.00006775     0.00022637
   mo  20     0.00000951    -0.00007528    -0.00004112    -0.00016781     0.00018110    -0.00027637     0.00041302    -0.00019754

                mo   9         mo  10         mo  11         mo  12         mo  13         mo  14         mo  15         mo  16
   mo   9     0.00559156
   mo  10     0.00099770     0.00395233
   mo  11    -0.00099006    -0.00052500     0.00400105
   mo  12    -0.00228541    -0.00000374     0.00027182     0.00219783
   mo  13    -0.00066714     0.00101629    -0.00004181     0.00003935     0.00093427
   mo  14    -0.00087333    -0.00083188    -0.00019984    -0.00006961     0.00029349     0.00124654
   mo  15     0.00173550     0.00064882    -0.00074357    -0.00044321    -0.00027942    -0.00055339     0.00105226
   mo  16    -0.00003792    -0.00035351     0.00013030    -0.00000243    -0.00006330     0.00004127    -0.00004200     0.00028648
   mo  17    -0.00010908    -0.00023097    -0.00082287     0.00026121    -0.00012638     0.00002482     0.00007716     0.00000325
   mo  18     0.00035022    -0.00069870     0.00041109    -0.00033347    -0.00046471    -0.00001137     0.00001500     0.00007789
   mo  19     0.00045062     0.00034834     0.00015082    -0.00050376     0.00017824    -0.00017597     0.00010457    -0.00003475
   mo  20    -0.00038344     0.00007190     0.00049700     0.00042872    -0.00000690    -0.00017288    -0.00012568     0.00000467

                mo  17         mo  18         mo  19         mo  20
   mo  17     0.00047141
   mo  18    -0.00009133     0.00051198
   mo  19    -0.00018112     0.00000322     0.00032888
   mo  20    -0.00006231    -0.00001944    -0.00005710     0.00029061

          ci-one-electron density block   2

                mo  21         mo  22         mo  23         mo  24         mo  25         mo  26         mo  27         mo  28
   mo  21     1.96966652
   mo  22     0.00337546     0.00068022
   mo  23    -0.01761865    -0.00208014     0.00746743
   mo  24    -0.00308982     0.00084861    -0.00189587     0.00154531
   mo  25    -0.00000143     0.00051798    -0.00179746     0.00051101     0.00047662
   mo  26    -0.00897837    -0.00203157     0.00827903    -0.00131141    -0.00198993     0.01032463
   mo  27     0.00205194    -0.00141416     0.00347203    -0.00241309    -0.00093417     0.00277958     0.00403211
   mo  28     0.00031532     0.00015543    -0.00047323     0.00027558     0.00004677    -0.00056368    -0.00039862     0.00043957
   mo  29     0.00000104     0.00032063    -0.00187045    -0.00012995     0.00048133    -0.00294486     0.00010582     0.00017923
   mo  30    -0.00050560     0.00061603    -0.00175434     0.00095631     0.00045752    -0.00181344    -0.00184562     0.00017214
   mo  31     0.00030919    -0.00028007     0.00075732    -0.00046449    -0.00023384     0.00072070     0.00086124    -0.00011847
   mo  32     0.00008113     0.00004311    -0.00009466     0.00010281    -0.00000791    -0.00007042    -0.00015262     0.00018457
   mo  33    -0.00013642    -0.00003561     0.00034791     0.00010307    -0.00008955     0.00071219    -0.00018235    -0.00004018
   mo  34     0.00011119    -0.00005696    -0.00002319    -0.00021323    -0.00000564    -0.00023326     0.00042315     0.00004314

                mo  29         mo  30         mo  31         mo  32         mo  33         mo  34
   mo  29     0.00157232
   mo  30     0.00005598     0.00126840
   mo  31    -0.00019324    -0.00042875     0.00050751
   mo  32     0.00000111     0.00007880    -0.00002311     0.00022668
   mo  33    -0.00057711     0.00003143     0.00001071    -0.00000447     0.00040439
   mo  34     0.00033356    -0.00034049     0.00005968    -0.00000334    -0.00015385     0.00026517

          ci-one-electron density block   3

                mo  35         mo  36         mo  37         mo  38         mo  39         mo  40         mo  41         mo  42
   mo  35     1.97048408
   mo  36    -0.00941919     0.00660471
   mo  37     0.00969578     0.00722753     0.00981646
   mo  38     0.00625241    -0.00379323    -0.00258101     0.00448966
   mo  39     0.00049509     0.00039448     0.00144956     0.00101164     0.00112157
   mo  40     0.00046525     0.00262480     0.00357874    -0.00132512     0.00047620     0.00189149
   mo  41    -0.00001864     0.00003300    -0.00001556    -0.00007457    -0.00004469     0.00000079     0.00027595
   mo  42    -0.00020249     0.00019751     0.00006464    -0.00042981    -0.00004003     0.00006672     0.00000396     0.00027528
   mo  43     0.00014356    -0.00023496     0.00019795     0.00093812     0.00060230     0.00000223    -0.00000872    -0.00011657

                mo  43
   mo  43     0.00058542

          ci-one-electron density block   4

                mo  44         mo  45         mo  46         mo  47         mo  48
   mo  44     0.00494882
   mo  45     0.00033418     0.00062213
   mo  46    -0.00040391     0.00013563     0.00029945
   mo  47    -0.00064311    -0.00014782     0.00020439     0.00027367
   mo  48    -0.00031573    -0.00022115    -0.00007991     0.00004251     0.00016880


*****   symmetry block SYM1   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       1.99970893
       2       0.00001754     1.98666986
       3      -0.00011711     0.00253833     1.97009935
       4       0.00000138    -0.00056370     0.01404698     0.00099533
       5      -0.00001442     0.00242632    -0.01790481    -0.00225101
       6      -0.00001393     0.00004488    -0.00861090    -0.00021453
       7       0.00001241    -0.00112802    -0.00866593    -0.00061128
       8       0.00002941    -0.00257056     0.00247386     0.00238426
       9      -0.00002939     0.00067307     0.00875631    -0.00021072
      10      -0.00000984     0.00057930    -0.00677743    -0.00073049
      11       0.00002500    -0.00135022    -0.00026226    -0.00038715
      12       0.00001388    -0.00024055    -0.00025564    -0.00056726
      13       0.00001319    -0.00013604    -0.00118441     0.00019553
      14       0.00003085    -0.00015057    -0.00022064     0.00070783
      15      -0.00007189     0.00052915     0.00057824    -0.00022579
      16       0.00000498    -0.00005580     0.00014069     0.00002834
      17      -0.00002013     0.00005534    -0.00004984     0.00005283
      18      -0.00000547     0.00003266     0.00013497     0.00004512
      19      -0.00001596     0.00007464     0.00001405     0.00005187
      20       0.00000951    -0.00007528    -0.00004112    -0.00016781

               Column   5     Column   6     Column   7     Column   8
       5       0.00629066
       6       0.00161951     0.00310346
       7       0.00000934    -0.00121807     0.00217313
       8      -0.00723832    -0.00122951     0.00053801     0.00935974
       9       0.00216896     0.00321348    -0.00187799    -0.00208784
      10       0.00188133     0.00229710     0.00050002    -0.00137882
      11      -0.00072656    -0.00155365     0.00231184     0.00164100
      12       0.00073881    -0.00132233     0.00116406    -0.00115279
      13      -0.00075406     0.00020555     0.00029862     0.00124190
      14      -0.00196135    -0.00047844    -0.00018646     0.00253422
      15       0.00116030     0.00107084    -0.00060384    -0.00141536
      16      -0.00009973    -0.00016773    -0.00000280     0.00007601
      17       0.00010635     0.00001153    -0.00034723    -0.00041900
      18      -0.00009629    -0.00013892    -0.00008678     0.00009560
      19      -0.00007678     0.00032664    -0.00006775     0.00022637
      20       0.00018110    -0.00027637     0.00041302    -0.00019754

               Column   9     Column  10     Column  11     Column  12
       9       0.00559156
      10       0.00099770     0.00395233
      11      -0.00099006    -0.00052500     0.00400105
      12      -0.00228541    -0.00000374     0.00027182     0.00219783
      13      -0.00066714     0.00101629    -0.00004181     0.00003935
      14      -0.00087333    -0.00083188    -0.00019984    -0.00006961
      15       0.00173550     0.00064882    -0.00074357    -0.00044321
      16      -0.00003792    -0.00035351     0.00013030    -0.00000243
      17      -0.00010908    -0.00023097    -0.00082287     0.00026121
      18       0.00035022    -0.00069870     0.00041109    -0.00033347
      19       0.00045062     0.00034834     0.00015082    -0.00050376
      20      -0.00038344     0.00007190     0.00049700     0.00042872

               Column  13     Column  14     Column  15     Column  16
      13       0.00093427
      14       0.00029349     0.00124654
      15      -0.00027942    -0.00055339     0.00105226
      16      -0.00006330     0.00004127    -0.00004200     0.00028648
      17      -0.00012638     0.00002482     0.00007716     0.00000325
      18      -0.00046471    -0.00001137     0.00001500     0.00007789
      19       0.00017824    -0.00017597     0.00010457    -0.00003475
      20      -0.00000690    -0.00017288    -0.00012568     0.00000467

               Column  17     Column  18     Column  19     Column  20
      17       0.00047141
      18      -0.00009133     0.00051198
      19      -0.00018112     0.00000322     0.00032888
      20      -0.00006231    -0.00001944    -0.00005710     0.00029061

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7        eno   8
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    8   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno   9        eno  10        eno  11        eno  12        eno  13        eno  14        eno  15        eno  16
 emo    9   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   10   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   11   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   12   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   13   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   14   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   15   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   16   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno  17        eno  18        eno  19        eno  20
 emo   17   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   18   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   19   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   20   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   1.9997094      1.9870606      1.9701232      0.0194494      0.0097882
   0.0053362      0.0045485      0.0008814      0.0007081      0.0004377
   0.0003373      0.0002621      0.0002549      0.0001274      0.0000940
   0.0000594      0.0000472      0.0000293      0.0000106      0.0000007

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7         no   8
  mo    1   0.999992E+00   0.278778E-04   0.399973E-02  -0.326680E-04   0.172151E-04  -0.529501E-05  -0.195376E-05  -0.158185E-05
  mo    2   0.573182E-03   0.988656E+00  -0.150179E+00   0.193633E-02   0.534296E-04   0.509162E-03  -0.265113E-03   0.252856E-03
  mo    3  -0.395818E-02   0.150177E+00   0.988547E+00  -0.661508E-02  -0.537801E-02   0.563057E-02   0.298689E-02   0.774450E-03
  mo    4  -0.273139E-04   0.780924E-03   0.710977E-02   0.160186E+00   0.168811E+00  -0.110695E+00   0.530664E-01  -0.729923E-01
  mo    5   0.289554E-04  -0.142438E-03  -0.921378E-02  -0.531838E+00  -0.215254E+00   0.557551E-01  -0.611115E-01   0.667356E-01
  mo    6   0.100315E-04  -0.626029E-03  -0.433437E-02  -0.223683E+00   0.402114E+00   0.230174E+00   0.137791E+00   0.199528E+00
  mo    7   0.231322E-04  -0.121970E-02  -0.427144E-02   0.868165E-01  -0.333462E+00   0.381769E+00  -0.756652E-01   0.180523E+00
  mo    8   0.906290E-05  -0.109749E-02   0.148561E-02   0.642792E+00   0.316890E+00   0.226218E+00   0.113218E-01   0.226257E+00
  mo    9  -0.319608E-04   0.100132E-02   0.433798E-02  -0.308897E+00   0.521282E+00   0.923275E-01  -0.427156E+00   0.334606E+00
  mo   10   0.869079E-05  -0.224387E-03  -0.346847E-02  -0.194082E+00   0.967215E-01   0.624843E+00   0.462519E+00  -0.597018E-01
  mo   11   0.127135E-04  -0.695530E-03  -0.283507E-04   0.157248E+00  -0.278220E+00   0.471235E+00  -0.584822E+00   0.994895E-02
  mo   12   0.745207E-05  -0.140434E-03  -0.121141E-03  -0.157908E-02  -0.389140E+00  -0.430457E-01   0.256162E+00   0.612137E+00
  mo   13   0.892257E-05  -0.158580E-03  -0.583437E-03   0.709099E-01   0.358457E-01   0.219232E+00   0.275155E+00  -0.199291E+00
  mo   14   0.158220E-04  -0.927480E-04  -0.847346E-04   0.185790E+00   0.819705E-01  -0.112798E+00   0.102996E+00   0.342240E+00
  mo   15  -0.369880E-04   0.308933E-03   0.244258E-03  -0.150100E+00   0.137875E+00   0.353001E-02  -0.432206E-01   0.169074E+00
  mo   16   0.219484E-05  -0.171303E-04   0.764199E-04   0.132506E-01  -0.118508E-01  -0.438398E-01  -0.624031E-01   0.272429E-01
  mo   17  -0.996145E-05   0.244834E-04  -0.289623E-04  -0.219719E-01   0.231369E-02  -0.169679E+00   0.123117E+00   0.205740E+00
  mo   18  -0.300798E-05   0.267010E-04   0.685777E-04   0.106075E-01   0.158267E-01  -0.701902E-01  -0.234589E+00  -0.319182E-02
  mo   19  -0.799719E-05   0.382245E-04   0.184856E-05  -0.516613E-02   0.724648E-01   0.105900E+00  -0.430669E-01  -0.369230E+00
  mo   20   0.483649E-05  -0.411264E-04  -0.177773E-04   0.906339E-03  -0.961269E-01   0.646680E-01  -0.212902E-01   0.106990E+00

                no   9         no  10         no  11         no  12         no  13         no  14         no  15         no  16
  mo    1   0.291740E-04   0.148980E-05  -0.139541E-04   0.404745E-05   0.376551E-05   0.700769E-05   0.732770E-05  -0.162910E-05
  mo    2   0.250737E-03  -0.916535E-04   0.256592E-03   0.423087E-04   0.113359E-03  -0.103959E-04  -0.182605E-03  -0.340770E-04
  mo    3  -0.492450E-02  -0.252852E-02  -0.266469E-02   0.226389E-02   0.213600E-02   0.233127E-02   0.352380E-02  -0.287728E-02
  mo    4   0.148862E+00  -0.315486E-01   0.139804E+00  -0.576375E-01  -0.130588E+00  -0.114149E+00  -0.127917E+00   0.102957E+00
  mo    5  -0.388455E+00   0.553524E-01  -0.145827E+00   0.281913E-01   0.371792E-01   0.192382E+00   0.276380E+00  -0.107471E+00
  mo    6  -0.168158E+00  -0.313400E+00   0.242319E+00   0.123309E+00  -0.272375E-01   0.929710E-01   0.334528E-01  -0.161381E+00
  mo    7   0.932684E-01  -0.399547E-01  -0.190427E+00   0.136638E+00   0.317815E+00   0.728318E-01  -0.165984E+00  -0.126860E+00
  mo    8  -0.418803E-01  -0.782089E-02  -0.573288E-01   0.253372E-01   0.439189E-01   0.205108E+00   0.163518E+00  -0.133638E+00
  mo    9   0.256356E-01   0.298083E+00   0.127854E+00  -0.127765E+00   0.560286E-01   0.267927E-01  -0.254375E+00   0.140091E+00
  mo   10   0.110814E+00  -0.208805E+00  -0.854746E-01  -0.217717E-04  -0.387879E-01  -0.207004E+00  -0.758368E-01   0.620498E-01
  mo   11  -0.520825E-01   0.504774E-01   0.112286E+00  -0.399600E-01  -0.593360E-02  -0.138268E+00   0.991222E-01   0.102156E+00
  mo   12   0.149865E+00   0.100700E+00   0.182109E+00  -0.961021E-01  -0.272856E+00   0.298520E+00  -0.331966E+00   0.869425E-01
  mo   13  -0.188776E+00   0.532400E+00   0.904871E-01   0.906329E-01  -0.370778E-01   0.249604E+00   0.192077E+00   0.579861E+00
  mo   14  -0.563124E+00   0.603614E-01  -0.395824E+00  -0.433481E-01  -0.844262E-01  -0.846261E-01   0.108763E+00  -0.106047E+00
  mo   15   0.588716E+00   0.183634E+00  -0.563525E+00   0.122490E+00  -0.187290E+00   0.296298E-01   0.350734E+00  -0.444637E-02
  mo   16  -0.449801E-01   0.143176E+00   0.160413E+00   0.910277E+00  -0.216904E+00  -0.124348E+00  -0.944036E-01  -0.131531E+00
  mo   17   0.182059E+00  -0.315240E-01   0.252233E+00   0.159167E+00   0.724517E+00   0.145972E+00   0.319130E+00   0.605138E-01
  mo   18  -0.212325E-01  -0.623604E+00  -0.125805E+00   0.164224E+00  -0.179258E+00   0.416607E+00   0.596609E-01   0.502922E+00
  mo   19   0.606186E-01   0.137454E+00  -0.613826E-02  -0.367759E-01  -0.613157E-01   0.678745E+00  -0.153645E+00  -0.460662E+00
  mo   20   0.107026E+00  -0.288554E-01   0.451494E+00  -0.163852E+00  -0.383807E+00  -0.277273E-01   0.593032E+00  -0.206805E+00

                no  17         no  18         no  19         no  20
  mo    1   0.406776E-05  -0.198581E-05  -0.158458E-05  -0.143590E-07
  mo    2   0.171179E-03   0.214706E-03  -0.168110E-03  -0.312950E-04
  mo    3   0.213208E-02   0.270551E-02   0.222629E-02  -0.162585E-02
  mo    4   0.535170E-01  -0.157526E+00  -0.221209E+00   0.863548E+00
  mo    5   0.104127E+00  -0.762482E-01   0.404985E+00   0.420015E+00
  mo    6   0.522066E+00   0.262469E+00  -0.310017E+00  -0.441604E-01
  mo    7  -0.267478E+00   0.549812E+00  -0.212454E+00   0.251052E+00
  mo    8   0.749987E-01   0.605992E-02   0.525325E+00   0.890736E-01
  mo    9  -0.339569E+00   0.691050E-02   0.642926E-01  -0.462249E-02
  mo   10  -0.282437E+00  -0.372025E+00   0.948740E-01  -0.270613E-01
  mo   11   0.313719E+00  -0.371099E+00  -0.192398E+00  -0.407120E-01
  mo   12   0.146333E+00  -0.158777E+00   0.433358E-01  -0.394777E-01
  mo   13   0.469484E-01   0.189940E+00  -0.140062E+00   0.150300E-03
  mo   14  -0.223083E+00  -0.229516E+00  -0.445886E+00  -0.193404E-01
  mo   15   0.200209E+00  -0.126467E-01  -0.125984E+00   0.401038E-02
  mo   16  -0.118109E+00  -0.104397E+00   0.518795E-01   0.217232E-02
  mo   17  -0.712873E-01  -0.334249E+00  -0.146911E+00   0.367865E-02
  mo   18  -0.209720E+00  -0.257867E-01  -0.250395E-01  -0.899077E-02
  mo   19  -0.106674E+00  -0.244034E+00  -0.223278E+00  -0.175735E-01
  mo   20  -0.393966E+00   0.158162E+00  -0.935290E-01   0.138350E-01
  eigenvectors of nos in ao-basis, column    1
  0.9986156E+00 -0.3627821E-02  0.1494441E-02  0.1829202E-02 -0.7867131E-02  0.3883298E-02  0.1325943E-02  0.1222697E-04
  0.2158514E-04 -0.5156104E-05 -0.2826858E-03  0.1200912E-04  0.3285590E-04  0.3644254E-02 -0.2593466E-02  0.7507846E-04
  0.7474919E-03  0.4314569E-03 -0.3082270E-03 -0.3471971E-03
  eigenvectors of nos in ao-basis, column    2
 -0.5504177E-02  0.9088638E+00 -0.7627217E-02 -0.1368845E+00 -0.1134412E-02  0.1120195E-01  0.6996440E-01  0.2454488E-03
  0.2575783E-02 -0.1237539E-02 -0.2858670E-02 -0.8256796E-03 -0.2032082E-02  0.2813905E+00 -0.1252308E+00  0.2499039E-02
  0.2850278E-01  0.1958011E-01  0.1428287E-01  0.7118465E-02
  eigenvectors of nos in ao-basis, column    3
  0.1001816E-01  0.1726189E+00  0.3773566E-02  0.1471938E+00  0.8224366E+00 -0.1336389E-01 -0.1647898E-01 -0.1561472E-02
 -0.2699871E-02 -0.5412342E-02  0.1163980E-02 -0.1403696E-03  0.2583078E-02 -0.3806091E+00  0.1611321E+00  0.5348449E-02
 -0.2933274E-01 -0.1755839E-02 -0.1371547E-02  0.2386233E-01
  eigenvectors of nos in ao-basis, column    4
 -0.2526642E-01 -0.2667068E+00  0.2194956E-01 -0.2049792E+00  0.1483358E+01 -0.3139951E+00 -0.9140328E+00  0.3551628E-02
  0.6709328E-02  0.1850296E-01  0.1524210E-01  0.3215493E-02  0.4425478E-02  0.6841275E+00 -0.2728428E+00 -0.1200784E-01
  0.2739827E-01 -0.3042026E-01  0.4834218E-02 -0.6326369E-01
  eigenvectors of nos in ao-basis, column    5
  0.5059391E-02 -0.1186562E+01 -0.4481357E+00  0.1139199E+01 -0.3866980E+00  0.2720341E+00  0.5748196E+00 -0.1291108E-02
  0.3900364E-01 -0.1651265E-01  0.6203782E-01 -0.1508243E-02 -0.3268583E-02  0.6964157E+00 -0.4202506E+00  0.2016009E-01
  0.3681694E-01  0.8020040E-01 -0.1586998E-02  0.3962472E-01
  eigenvectors of nos in ao-basis, column    6
 -0.8499308E-02  0.5452520E+00  0.2833746E+00 -0.1116141E+01 -0.1115311E-01  0.2223421E-01  0.1145845E+00 -0.3091520E-01
  0.1132885E+00 -0.7898050E-01  0.2414513E+00 -0.1384665E-01 -0.2151053E-01  0.3869915E+00 -0.1708422E+00 -0.1051253E-01
 -0.4120903E-01  0.9161812E-01 -0.2212773E-01  0.2929321E-01
  eigenvectors of nos in ao-basis, column    7
  0.1729299E-02 -0.1658944E+00 -0.8089981E-01  0.3174977E+00  0.2734235E+00 -0.1306480E+00 -0.4335050E+00 -0.8289047E-01
 -0.7576401E-01 -0.1758724E+00 -0.1579692E+00 -0.1242945E-01  0.1318971E-01 -0.1156376E+00  0.6021621E-01  0.8332937E-02
 -0.7045915E-01  0.5709726E-01 -0.1313175E-01  0.4462980E-01
  eigenvectors of nos in ao-basis, column    8
 -0.1235616E+00  0.4245576E-01  0.4470847E+00 -0.1938461E+01 -0.1627545E+00  0.2568657E+00  0.9545347E+00  0.2037012E-01
 -0.2321758E+00  0.1453594E-01 -0.2375981E+00 -0.2857292E-01 -0.7470189E-01  0.2148039E+01 -0.1045471E+01  0.6568020E-02
  0.4127987E-01  0.1311911E+00  0.8400669E-01  0.7783395E-01
  eigenvectors of nos in ao-basis, column    9
 -0.1107490E+00 -0.4594779E-01  0.2691085E+00  0.1776549E+00 -0.2548594E+01  0.2324154E+01  0.5064240E+00 -0.1836549E-01
 -0.3449987E-01  0.5876107E-02  0.4095582E-01  0.6747279E-02  0.2264810E-01 -0.9847465E-01  0.1784112E+00 -0.3879938E-01
 -0.1957363E+00 -0.1227914E+00 -0.4432607E-01 -0.1565966E+00
  eigenvectors of nos in ao-basis, column   10
  0.1055655E+00  0.2464716E+00 -0.1065592E+00 -0.6849746E+00 -0.1230816E+00  0.1714666E+00  0.8820062E+00 -0.2089670E+00
  0.3495888E-01 -0.8044974E-02  0.1655362E-01  0.7051020E-01 -0.1085672E+00  0.7029794E+00 -0.3448137E+00 -0.1094301E-01
  0.3622389E+00 -0.3124240E+00  0.1085423E+00 -0.1690022E+00
  eigenvectors of nos in ao-basis, column   11
 -0.1262967E+01 -0.2629640E+01  0.1831524E+01  0.2180454E+01  0.8379280E+00 -0.1213245E+01 -0.1844249E-01 -0.3869212E-01
 -0.1127243E+00  0.2259576E-01  0.1822832E+00  0.1289069E+00  0.1790893E+00 -0.4952298E+00  0.2761801E+00 -0.6429399E-02
 -0.2412680E+00 -0.2549332E+00 -0.5979876E-01  0.5820499E-01
  eigenvectors of nos in ao-basis, column   12
  0.5220799E+00  0.1142065E+01 -0.7407959E+00 -0.5697324E+00 -0.2152757E+00  0.2990732E+00 -0.3006592E+00  0.8895002E-01
 -0.8511786E-01 -0.5756086E-01  0.5769326E-01  0.3252115E+00 -0.1532864E+00 -0.1227107E+00  0.2445468E-01 -0.4775135E-02
 -0.5331006E-02  0.2766549E-01 -0.1109834E+00  0.8504024E-01
  eigenvectors of nos in ao-basis, column   13
  0.1241467E+01  0.2739104E+01 -0.1749921E+01 -0.1005172E+01  0.8587671E-01 -0.1687813E+00 -0.1307512E+00 -0.8256518E-01
 -0.3031883E+00  0.7668766E-01  0.2624015E+00 -0.2991772E-04  0.2558370E+00 -0.2656435E+00 -0.6644181E-01 -0.1898352E-01
 -0.7678479E-01 -0.2076478E-01 -0.1837794E+00 -0.1228843E+00
  eigenvectors of nos in ao-basis, column   14
  0.4925850E+00  0.1304173E+01 -0.4425309E+00 -0.2990364E+01 -0.7190114E+00  0.1094014E+01  0.8497609E+00  0.8453501E-01
  0.3038565E+00 -0.1601192E+00 -0.4329566E+00  0.6068907E-01  0.3253945E+00  0.2352143E+01 -0.1157749E+01  0.4013655E-01
  0.3323806E+00 -0.2014479E+00  0.1417681E+00  0.2545763E+00
  eigenvectors of nos in ao-basis, column   15
 -0.1981007E+01 -0.5163402E+01  0.2299227E+01  0.2109289E+01 -0.1380616E+01  0.2614661E+01 -0.2465255E+00  0.1847383E-01
 -0.1690183E+00 -0.7834111E-01 -0.4309859E-01  0.7482347E-02  0.1326136E+00  0.1808144E+01 -0.1263840E+01  0.9974644E-01
  0.1053121E+01  0.4459228E+00  0.1033801E-01  0.2577616E+00
  eigenvectors of nos in ao-basis, column   16
  0.7084797E+00  0.1894796E+01 -0.7720031E+00 -0.1604074E+01  0.2624389E+00 -0.5391089E+00  0.8050727E+00  0.1982131E+00
 -0.2130057E+00 -0.3630540E+00  0.4546406E+00 -0.1253124E+00 -0.6033724E-01 -0.6698191E+00  0.8647364E+00 -0.8404361E-01
  0.2744728E+00 -0.7929911E+00  0.1765026E+00 -0.7737054E-01
  eigenvectors of nos in ao-basis, column   17
  0.1471891E+01  0.3969349E+01 -0.1576382E+01 -0.4222812E+01 -0.2376026E+00  0.1028014E+00 -0.1060823E+01 -0.6788710E-01
 -0.4522744E-01  0.6681389E-01  0.2643261E+00 -0.8812606E-01 -0.1424991E+00 -0.1339399E+01  0.1596838E+01  0.1664832E-02
 -0.2727165E+00 -0.5587546E+00  0.3126835E+00  0.1136432E+01
  eigenvectors of nos in ao-basis, column   18
 -0.1061952E+01 -0.2960052E+01  0.8819886E+00  0.8303510E+01  0.2600532E+00 -0.6219176E+00 -0.1336053E+01  0.1450725E-01
 -0.6220598E-02 -0.6007134E-01  0.2826900E+00 -0.1109615E+00 -0.1694478E+00 -0.2157491E+01 -0.7137129E+00  0.5707946E-01
 -0.1828473E+00 -0.6595767E+00 -0.1386513E+01 -0.5476607E+00
  eigenvectors of nos in ao-basis, column   19
  0.1267641E+00  0.3577537E+00 -0.2577467E+00  0.3362384E+01  0.3770109E+00 -0.1182737E+01 -0.1157283E+01  0.1289738E-01
 -0.7116016E-01  0.2068723E-01  0.4245741E+00 -0.3711924E-01 -0.1470167E+00 -0.2275388E+01 -0.2861665E+00  0.3127279E+00
 -0.1015625E+01 -0.5689140E+00  0.4266184E+00 -0.6621102E-01
  eigenvectors of nos in ao-basis, column   20
 -0.1452169E+00 -0.4437199E+00  0.7194097E-01  0.1975770E+01 -0.1595170E-01  0.1280859E-01 -0.5206957E+00 -0.2094507E-02
 -0.1203751E-01  0.5477108E-02  0.4520131E-01 -0.1865826E-02 -0.8948733E-02 -0.1896504E+00 -0.1177807E+01  0.8773213E+00
 -0.7254443E-01 -0.5132789E-01 -0.2790967E+00 -0.2017659E+00


*****   symmetry block SYM2   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       1.96966652
       2       0.00337546     0.00068022
       3      -0.01761865    -0.00208014     0.00746743
       4      -0.00308982     0.00084861    -0.00189587     0.00154531
       5      -0.00000143     0.00051798    -0.00179746     0.00051101
       6      -0.00897837    -0.00203157     0.00827903    -0.00131141
       7       0.00205194    -0.00141416     0.00347203    -0.00241309
       8       0.00031532     0.00015543    -0.00047323     0.00027558
       9       0.00000104     0.00032063    -0.00187045    -0.00012995
      10      -0.00050560     0.00061603    -0.00175434     0.00095631
      11       0.00030919    -0.00028007     0.00075732    -0.00046449
      12       0.00008113     0.00004311    -0.00009466     0.00010281
      13      -0.00013642    -0.00003561     0.00034791     0.00010307
      14       0.00011119    -0.00005696    -0.00002319    -0.00021323

               Column   5     Column   6     Column   7     Column   8
       5       0.00047662
       6      -0.00198993     0.01032463
       7      -0.00093417     0.00277958     0.00403211
       8       0.00004677    -0.00056368    -0.00039862     0.00043957
       9       0.00048133    -0.00294486     0.00010582     0.00017923
      10       0.00045752    -0.00181344    -0.00184562     0.00017214
      11      -0.00023384     0.00072070     0.00086124    -0.00011847
      12      -0.00000791    -0.00007042    -0.00015262     0.00018457
      13      -0.00008955     0.00071219    -0.00018235    -0.00004018
      14      -0.00000564    -0.00023326     0.00042315     0.00004314

               Column   9     Column  10     Column  11     Column  12
       9       0.00157232
      10       0.00005598     0.00126840
      11      -0.00019324    -0.00042875     0.00050751
      12       0.00000111     0.00007880    -0.00002311     0.00022668
      13      -0.00057711     0.00003143     0.00001071    -0.00000447
      14       0.00033356    -0.00034049     0.00005968    -0.00000334

               Column  13     Column  14
      13       0.00040439
      14      -0.00015385     0.00026517

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7        eno   8
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    8   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno   9        eno  10        eno  11        eno  12        eno  13        eno  14
 emo    9   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   10   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   11   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   12   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   13   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   14   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   1.9698795      0.0211600      0.0052597      0.0008179      0.0006564
   0.0004791      0.0002884      0.0001317      0.0000880      0.0000808
   0.0000174      0.0000129      0.0000047      0.0000003

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7         no   8
  mo    1   0.999946E+00   0.801869E-02  -0.306885E-02   0.344307E-02   0.189155E-02  -0.851769E-03   0.840854E-03  -0.190415E-02
  mo    2   0.172682E-02  -0.162879E+00  -0.123479E+00  -0.279872E-01  -0.201477E+00  -0.450762E-01   0.577043E-01   0.720091E-01
  mo    3  -0.899538E-02   0.581377E+00  -0.112663E-01   0.280123E+00   0.322381E+00  -0.298893E-02   0.708096E-01  -0.275954E+00
  mo    4  -0.155859E-02  -0.157431E+00  -0.414445E+00   0.188797E+00  -0.306175E+00  -0.293848E-01   0.182249E+00  -0.132470E+00
  mo    5   0.116504E-04  -0.144177E+00  -0.122590E-01  -0.430122E-01  -0.630835E-01  -0.211150E+00  -0.493462E-01   0.122077E+00
  mo    6  -0.461873E-02   0.667914E+00  -0.384468E+00   0.976962E-01  -0.236858E+00  -0.132648E+00   0.907161E-02   0.213954E+00
  mo    7   0.102227E-02   0.290888E+00   0.645613E+00  -0.183075E+00   0.237947E-01   0.983572E-01  -0.657206E-01   0.120153E+00
  mo    8   0.163270E-03  -0.438192E-01  -0.330030E-01   0.363499E+00  -0.693044E-01   0.726672E+00  -0.122722E+00  -0.321993E+00
  mo    9   0.164538E-04  -0.162083E+00   0.355841E+00   0.598964E+00  -0.559731E-03  -0.239293E+00   0.449213E-01  -0.422942E-01
  mo   10  -0.245747E-03  -0.157595E+00  -0.260714E+00   0.202881E-01   0.747897E+00   0.474900E-01   0.193807E+00   0.330934E-02
  mo   11   0.152450E-03   0.693261E-01   0.116543E+00  -0.347816E+00  -0.161840E+00   0.248208E+00   0.779739E+00  -0.194899E+00
  mo   12   0.416655E-04  -0.909168E-02  -0.292593E-01   0.155538E+00  -0.137666E-01   0.484154E+00   0.247539E-01   0.756040E+00
  mo   13  -0.727459E-04   0.345672E-01  -0.136849E+00  -0.409062E+00  -0.151381E-01   0.204102E+00  -0.536114E+00  -0.261473E+00
  mo   14   0.574992E-04  -0.243495E-03   0.138635E+00   0.190736E+00  -0.332142E+00  -0.133075E-02  -0.306676E-01  -0.206319E+00

                no   9         no  10         no  11         no  12         no  13         no  14
  mo    1   0.188927E-02   0.187895E-02  -0.545836E-03   0.214577E-02   0.161738E-02  -0.192977E-04
  mo    2   0.289522E-02  -0.109354E+00   0.306784E+00   0.241439E-01   0.376943E-01   0.893426E+00
  mo    3   0.170170E+00   0.277810E+00  -0.100921E+00   0.411637E+00   0.222329E+00   0.251045E+00
  mo    4   0.143907E+00  -0.101958E+00   0.481385E+00   0.469625E+00   0.133326E+00  -0.348246E+00
  mo    5   0.176056E+00   0.666601E-01  -0.126876E+00  -0.236756E+00   0.895562E+00  -0.410888E-01
  mo    6   0.111052E-01  -0.201871E+00   0.125040E+00  -0.461434E+00  -0.637930E-01  -0.587582E-01
  mo    7  -0.884726E-01  -0.257270E+00   0.553909E+00   0.104090E+00   0.190107E+00  -0.908249E-01
  mo    8  -0.133649E+00  -0.325091E+00  -0.858167E-01  -0.201174E+00   0.200534E+00   0.408352E-01
  mo    9   0.555296E+00  -0.727143E-01   0.960056E-01  -0.255106E+00  -0.198343E+00  -0.170117E-02
  mo   10  -0.931946E-01   0.761919E-01   0.436437E+00  -0.316014E+00   0.352866E-01  -0.390418E-01
  mo   11   0.286584E+00   0.850950E-01  -0.680390E-01  -0.181087E+00  -0.760570E-02  -0.271190E-02
  mo   12   0.208950E+00   0.327432E+00   0.805604E-02   0.129938E+00  -0.132469E-01  -0.835984E-02
  mo   13   0.557802E+00   0.205933E+00   0.200602E+00  -0.114939E+00  -0.898142E-01  -0.142993E-02
  mo   14  -0.373550E+00   0.717944E+00   0.270971E+00  -0.247604E+00   0.535054E-02  -0.287623E-01
  eigenvectors of nos in ao-basis, column    1
  0.7391718E+00 -0.1997043E-01 -0.1248757E+00 -0.1626213E-01 -0.1034963E-01  0.3008870E-03 -0.3376106E-02 -0.5495864E+00
  0.1557969E+00 -0.1044754E-01 -0.2370201E-01 -0.3135891E-01 -0.2131142E-01 -0.2417817E-01
  eigenvectors of nos in ao-basis, column    2
 -0.1382583E+01  0.1745019E+00  0.6412877E+00 -0.2760604E-01 -0.4530689E-01  0.1926263E-04  0.3414954E-02 -0.9150876E+00
  0.4090053E+00 -0.7184534E-02 -0.1808079E-01 -0.6337888E-02  0.3350255E-01  0.9204364E-02
  eigenvectors of nos in ao-basis, column    3
 -0.6343936E+00  0.3373774E+00  0.9053710E+00  0.2712362E+00  0.5070162E+00 -0.8576909E-02  0.1032251E-01  0.4376572E+00
 -0.1188315E+00 -0.1035212E-01  0.1019523E+00 -0.1011807E-01  0.8008509E-01  0.6343627E-01
  eigenvectors of nos in ao-basis, column    4
  0.1667081E+01 -0.1393017E+01 -0.1203740E+01  0.4483016E+00  0.3564051E+00  0.1821211E-01  0.1179120E-01 -0.1162106E+01
  0.2206248E+00  0.3998300E-01 -0.3429014E-01  0.2616901E+00 -0.8686624E-01 -0.6185136E-01
  eigenvectors of nos in ao-basis, column    5
  0.1769287E+01 -0.1498081E+01  0.6249472E+00 -0.3262590E+00 -0.4290261E+00 -0.2408107E-01  0.6140216E-01  0.1440238E+01
 -0.6052621E+00  0.8051831E-01  0.2609415E+00  0.1215230E+00  0.2118857E+00  0.2611224E+00
  eigenvectors of nos in ao-basis, column    6
 -0.6643965E+00  0.7509706E+00  0.1209136E+01 -0.1353638E+00 -0.1801164E+00  0.6273068E-01  0.2313390E+00  0.1153476E+01
 -0.1515994E+00 -0.1828947E-01 -0.2477868E+00  0.4327368E+00  0.1558947E+00  0.1707263E+00
  eigenvectors of nos in ao-basis, column    7
 -0.6837603E+00  0.1037719E+01 -0.1127518E+00  0.3762811E+00 -0.4640553E+00 -0.8185351E-01  0.4120170E+00  0.3477840E+00
 -0.6449909E+00  0.7415843E-01  0.2113424E+00  0.1158726E+00 -0.2497648E+00  0.5854730E-01
  eigenvectors of nos in ao-basis, column    8
  0.1423120E+01 -0.2409593E+01 -0.7806726E+00 -0.6344001E-01  0.7082803E+00  0.6630227E-01  0.3997686E+00 -0.2218475E+01
  0.1456609E+01 -0.2207722E+00 -0.1878050E+00 -0.7969935E+00 -0.7963627E-01 -0.2307069E+00
  eigenvectors of nos in ao-basis, column    9
 -0.1037265E+01  0.2131873E+01 -0.1491547E+01 -0.7415700E+00  0.4533601E+00  0.3457331E-01  0.9490173E-01 -0.8714960E-02
 -0.1305834E+01  0.1670986E+00  0.7975546E+00  0.3480768E+00 -0.3268128E+00 -0.3125753E+00
  eigenvectors of nos in ao-basis, column   10
 -0.1182068E+01  0.2261788E+01  0.3256485E+00  0.6243435E+00 -0.1689208E+01  0.1113369E+00 -0.7383671E-01  0.3057717E+01
 -0.2700726E+01  0.3336057E+00  0.8165266E+00  0.7494891E-01 -0.1255937E+00  0.1036401E+00
  eigenvectors of nos in ao-basis, column   11
 -0.9879322E-01  0.5923674E+00  0.4406238E+01  0.1827156E+00 -0.1391649E+01  0.4391656E-01 -0.1431582E+00  0.3165589E+01
  0.2706711E+01 -0.3940127E+00  0.8500341E+00  0.4908904E+00  0.6520098E+00  0.1556324E+01
  eigenvectors of nos in ao-basis, column   12
  0.6642484E+00 -0.1885952E+01 -0.8865918E+00 -0.2114873E+00  0.1289309E+01 -0.5175552E-02  0.1067973E+00 -0.2155848E+01
 -0.8515625E+00  0.4262313E+00 -0.7793073E+00 -0.8999708E+00 -0.8054373E+00  0.9014129E+00
  eigenvectors of nos in ao-basis, column   13
  0.2107717E+00 -0.6603237E+00  0.3060368E+01  0.7131010E-01 -0.4196547E-01 -0.1097765E-01  0.1961961E-01  0.2229037E+00
  0.9321417E+01  0.1510341E+00 -0.3301527E+00  0.3444414E-01  0.4773728E+01  0.1302055E+01
  eigenvectors of nos in ao-basis, column   14
 -0.5904160E-02  0.1947273E-01 -0.1306075E+01 -0.3717483E-01  0.1887241E+00 -0.4013507E-02  0.5100712E-02 -0.3142784E+00
 -0.2394452E+01  0.2846652E+01 -0.1181308E+00 -0.3785973E-01 -0.4529278E+00 -0.6554765E+00


*****   symmetry block SYM3   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       1.97048408
       2      -0.00941919     0.00660471
       3       0.00969578     0.00722753     0.00981646
       4       0.00625241    -0.00379323    -0.00258101     0.00448966
       5       0.00049509     0.00039448     0.00144956     0.00101164
       6       0.00046525     0.00262480     0.00357874    -0.00132512
       7      -0.00001864     0.00003300    -0.00001556    -0.00007457
       8      -0.00020249     0.00019751     0.00006464    -0.00042981
       9       0.00014356    -0.00023496     0.00019795     0.00093812

               Column   5     Column   6     Column   7     Column   8
       5       0.00112157
       6       0.00047620     0.00189149
       7      -0.00004469     0.00000079     0.00027595
       8      -0.00004003     0.00006672     0.00000396     0.00027528
       9       0.00060230     0.00000223    -0.00000872    -0.00011657

               Column   9
       9       0.00058542

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7        eno   8
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    8   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno   9
 emo    9   0.100000E+01

 occupation numbers of nos
   1.9705970      0.0185096      0.0046057      0.0006590      0.0004704
   0.0002894      0.0002618      0.0000960      0.0000558

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7         no   8
  mo    1   0.999971E+00   0.263081E-03  -0.508787E-02  -0.159808E-02   0.306659E-02  -0.119463E-02  -0.142474E-02   0.682047E-04
  mo    2  -0.478348E-02   0.583853E+00  -0.132712E+00  -0.438880E+00   0.146228E+00  -0.124660E+00  -0.134419E+00   0.132169E+00
  mo    3   0.492352E-02   0.698013E+00   0.384657E+00  -0.465969E-01  -0.301869E+00   0.139448E+00   0.155435E+00  -0.130506E-01
  mo    4   0.318280E-02  -0.308900E+00   0.754812E+00  -0.151398E+00  -0.327202E+00  -0.492562E-02   0.338845E-01   0.241275E+00
  mo    5   0.255756E-03   0.603043E-01   0.422257E+00   0.863180E-01   0.614372E+00   0.191465E+00   0.126417E+00  -0.599569E+00
  mo    6   0.236806E-03   0.268971E+00   0.816686E-01   0.868866E+00  -0.364341E-01  -0.189150E+00  -0.156006E+00   0.148529E+00
  mo    7  -0.970667E-05   0.159827E-02  -0.203334E-01  -0.114477E-01   0.135122E-01  -0.685467E+00   0.724587E+00  -0.642151E-01
  mo    8  -0.103781E-03   0.170196E-01  -0.848818E-01   0.106524E+00   0.291247E+00   0.530901E+00   0.543871E+00   0.558987E+00
  mo    9   0.755352E-04  -0.141617E-01   0.268459E+00  -0.918210E-01   0.562702E+00  -0.375274E+00  -0.308999E+00   0.475409E+00

                no   9
  mo    1   0.400198E-02
  mo    2   0.613519E+00
  mo    3  -0.477201E+00
  mo    4   0.381393E+00
  mo    5   0.145034E+00
  mo    6   0.287318E+00
  mo    7   0.159796E-01
  mo    8   0.788381E-01
  mo    9  -0.374619E+00
  eigenvectors of nos in ao-basis, column    1
  0.9129108E+00  0.6341754E-02  0.4688599E-01 -0.4832938E-02 -0.1964006E-01  0.1155935E-02  0.3285182E-03  0.2237371E-01
  0.3142917E-01
  eigenvectors of nos in ao-basis, column    2
 -0.1618572E+01  0.5069071E+00  0.1270193E+01 -0.1151536E-01 -0.7873472E-01 -0.4021287E-03  0.5325049E-03  0.6152071E-01
  0.9393381E-01
  eigenvectors of nos in ao-basis, column    3
 -0.2428909E+00  0.1051309E+00  0.3962895E+00  0.3268065E+00  0.7073241E+00 -0.2660972E-02 -0.6260783E-02 -0.8863739E-01
 -0.6483397E-01
  eigenvectors of nos in ao-basis, column    4
 -0.2715123E+01  0.2776699E+01  0.3893044E+00 -0.1069920E+00  0.6097612E-02  0.8774092E-02  0.1017280E-02 -0.8034675E-02
 -0.1863009E+00
  eigenvectors of nos in ao-basis, column    5
 -0.7296226E-01  0.1112261E+00 -0.6984197E+00  0.5896107E+00 -0.1026178E-01  0.7608872E-01  0.2555295E-01  0.5138681E+00
  0.1464637E+00
  eigenvectors of nos in ao-basis, column    6
  0.3635106E+00 -0.4902103E+00  0.2763467E+00 -0.5249484E+00  0.4028358E+00  0.1437642E+00 -0.7412567E-01  0.7591571E-01
 -0.1776924E+00
  eigenvectors of nos in ao-basis, column    7
  0.2954612E+00 -0.4094164E+00  0.3244949E+00 -0.4677761E+00  0.3626178E+00  0.4237101E-01  0.1347205E+00  0.1292398E-01
 -0.1770628E+00
  eigenvectors of nos in ao-basis, column    8
 -0.4569869E+00  0.7029550E+00  0.2174915E+00  0.4758362E+00 -0.8628240E+00  0.1310755E+00  0.3052845E-01 -0.8689797E+00
  0.2287850E+00
  eigenvectors of nos in ao-basis, column    9
 -0.7937037E+00  0.1286113E+01 -0.1783384E+01 -0.4580426E+00  0.6061488E+00 -0.1855034E-02  0.9024302E-03 -0.2771646E-01
  0.1027766E+01


*****   symmetry block SYM4   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       0.00494882
       2       0.00033418     0.00062213
       3      -0.00040391     0.00013563     0.00029945
       4      -0.00064311    -0.00014782     0.00020439     0.00027367
       5      -0.00031573    -0.00022115    -0.00007991     0.00004251

               Column   5
       5       0.00016880

               eno   1        eno   2        eno   3        eno   4        eno   5
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   0.0051211      0.0007495      0.0003663      0.0000631      0.0000129

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5
  mo    1   0.981668E+00  -0.681982E-01   0.165592E+00   0.637113E-01   0.140260E-01
  mo    2   0.780957E-01   0.863144E+00  -0.258815E+00   0.335277E+00   0.263607E+00
  mo    3  -0.847441E-01   0.358635E+00   0.661161E+00   0.112920E+00  -0.643673E+00
  mo    4  -0.136772E+00  -0.527328E-01   0.677595E+00   0.133925E+00   0.708125E+00
  mo    5  -0.658806E-01  -0.344866E+00  -0.965693E-01   0.923495E+00  -0.120658E+00
  eigenvectors of nos in ao-basis, column    1
 -0.5903343E+00 -0.5300809E+00  0.7076246E-01  0.4529910E-01 -0.3206032E-01
  eigenvectors of nos in ao-basis, column    2
 -0.3866308E+00 -0.1302732E-01 -0.5973854E+00 -0.4613336E+00  0.5638704E-01
  eigenvectors of nos in ao-basis, column    3
  0.6985718E+00 -0.6371386E+00 -0.5635715E+00  0.7511086E-01  0.3274501E+00
  eigenvectors of nos in ao-basis, column    4
  0.5733206E+00 -0.1172591E+01  0.6311283E+00 -0.8694139E+00  0.7485056E-01
  eigenvectors of nos in ao-basis, column    5
  0.3173878E+00 -0.3582819E+00 -0.2560252E+00  0.3067782E+00 -0.1164230E+01


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***


 cosurf_and_phi
  1.262739748330  2.872870034186  1.567866969040 -0.043498004279
  1.652947872482  0.360731428865  3.496571379926 -0.053464151184
  0.594940552015  0.670555958619  3.845537291721 -0.064031461795
  2.390078334690  1.202722009900  2.566708449184 -0.034301352839
  1.989334673621  2.229216060677  2.001028520754 -0.036951221687
  2.919143888229  0.378144896597  2.099775822683 -0.013683349310
  0.105380882100  1.878884201281  3.371423292662 -0.064171298367
  0.783266721831  2.590884380442  2.520834408126 -0.057175504905
  2.011402526412  2.551496219550  0.814855178005 -0.017477284371
  0.099626804209  1.855073908563 -1.945822518898  0.030926694950
  0.119020599620  3.173161356543  1.415159765853 -0.049312907999
  0.915237473358  3.108118624501  0.463299650838 -0.030640435183
  0.462814589486  2.837210699514 -0.795516582628 -0.006255048178
  1.408719892640  1.640288870679  3.148120355694 -0.055112983720
  2.650053602975  1.633766196698  1.655441764425 -0.015098507470
  1.142904167226  2.885766749848 -0.243475252462 -0.012101716064
  3.656106873706  0.713798214804  0.361832640492  0.037975374143
  2.408046317685  2.075600470298 -1.226192756897  0.039785847425
  1.295820311657  0.567673501678 -2.747601655947  0.058182537780
  3.527730862121  1.045649613724 -1.064853177123  0.054976415624
  2.622898581638  1.024710819001 -2.241122746235  0.059999095518
  3.086808508398  1.794857685487 -0.179703829578  0.034084025684
  1.615872011596  1.674366500124 -2.147504385867  0.047767638040
  0.000006852884  1.035694667087 -2.361676372823  0.050054387939
  0.298815704890  0.969607820141 -2.408807386530  0.051674410446
  0.667723897920  1.245157743773 -2.338577856151  0.049427443959
  1.218123388571  2.025110412626 -1.616576841774  0.035187939111
  2.084186643139  2.293984793080 -0.480493513306  0.021504613475
  2.876642520254  1.658030225134  0.559035126479  0.020313100330
  3.282919570196  0.368088739237  1.091983758387  0.023424030614
  0.467932162408  1.694535407938 -1.941510815499  0.037622760817
  1.299770347024  2.453875604722 -0.850324284820  0.011779216410
  2.242050270258  2.245320705184  0.385739526123  0.002599742891
  2.923104801922  1.151131828431  1.279135167181  0.006210513452
  0.427044967836  0.580963354323 -2.585108185092  0.055497394721
 end_of_phi


 nsubv after electrostatic potential =  2

  Confirmation of dielectric energy for ground state
  edielnew =  -0.00256506366

 =========== Executing IN-CORE method ==========
 norm multnew:  0.00311503223
 threx: total number of loops= 14
 threx: total number of loops= 24
 blstr diagel,allext,nmin2=  1892
 onel.diag.
 -0.194545E+01-0.286130E+01-0.270402E+01-0.267939E+01-0.404309E+01-0.353003E+01-0.366094E+01-0.377999E+01-0.336133E+01-0.260063E+01
 -0.287415E+01-0.357359E+01-0.152440E+01-0.144558E+01-0.152216E+01-0.749910E+00 0.955054E+00-0.155502E+01-0.292594E+01-0.240588E+01
 -0.200637E+01-0.405451E+01-0.340880E+01-0.250541E+01-0.287248E+01-0.299025E+01-0.266613E+01-0.118104E+01-0.155136E+01-0.373927E+00
 -0.277866E+01-0.388024E+01-0.376232E+01-0.283288E+01-0.443810E+01-0.160170E+01-0.133510E+01-0.168075E+01-0.459117E+01-0.280777E+01
 -0.220460E+01-0.135625E+01-0.747718E+00-0.330495E+02-0.787969E+01-0.692041E+01-0.675761E+01-0.704586E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1      1892      1892
 blstr diagel,allext,nmin2=  1892
 onel.diag.
 -0.194545E+01-0.286130E+01-0.270402E+01-0.267939E+01-0.404309E+01-0.353003E+01-0.366094E+01-0.377999E+01-0.336133E+01-0.260063E+01
 -0.287415E+01-0.357359E+01-0.152440E+01-0.144558E+01-0.152216E+01-0.749910E+00 0.955054E+00-0.155502E+01-0.292594E+01-0.240588E+01
 -0.200637E+01-0.405451E+01-0.340880E+01-0.250541E+01-0.287248E+01-0.299025E+01-0.266613E+01-0.118104E+01-0.155136E+01-0.373927E+00
 -0.277866E+01-0.388024E+01-0.376232E+01-0.283288E+01-0.443810E+01-0.160170E+01-0.133510E+01-0.168075E+01-0.459117E+01-0.280777E+01
 -0.220460E+01-0.135625E+01-0.747718E+00-0.330495E+02-0.787969E+01-0.692041E+01-0.675761E+01-0.704586E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1      1892      1892
 blstr diagel,allext,nmin2=  1892
 onel.diag.
 -0.194545E+01-0.286130E+01-0.270402E+01-0.267939E+01-0.404309E+01-0.353003E+01-0.366094E+01-0.377999E+01-0.336133E+01-0.260063E+01
 -0.287415E+01-0.357359E+01-0.152440E+01-0.144558E+01-0.152216E+01-0.749910E+00 0.955054E+00-0.155502E+01-0.292594E+01-0.240588E+01
 -0.200637E+01-0.405451E+01-0.340880E+01-0.250541E+01-0.287248E+01-0.299025E+01-0.266613E+01-0.118104E+01-0.155136E+01-0.373927E+00
 -0.277866E+01-0.388024E+01-0.376232E+01-0.283288E+01-0.443810E+01-0.160170E+01-0.133510E+01-0.168075E+01-0.459117E+01-0.280777E+01
 -0.220460E+01-0.135625E+01-0.747718E+00-0.330495E+02-0.787969E+01-0.692041E+01-0.675761E+01-0.704586E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1      1892      1892


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        12 xx:        36 ww:        79
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        11
task #   9:        28    task #  10:        71    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 matrix-vector product formed for vector   3
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference-subspace vector overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     1.00000000     0.00000000     0.00000000

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.97299478     0.02153534     0.22982033
 subspace has dimension  3

          (h-repnuc*1) matrix in the subspace basis

                x:   1         x:   2         x:   3
   x:   1   -85.40875183
   x:   2    -0.31915444    -7.04150281
   x:   3    -0.00086869     0.28014244    -0.25399358

          overlap matrix in the subspace basis

                x:   1         x:   2         x:   3
   x:   1     1.00000000
   x:   2     0.00000000     0.08688345
   x:   3     0.00000000    -0.00342125     0.00311503

          eigenvectors and eigenvalues in the subspace basis

                v:   1         v:   2         v:   3

   energy   -85.67213812   -81.54491440   -80.72164279

   x:   1     0.97299478     0.02153534     0.22982033
   x:   2     0.80060426    -0.30835856    -3.36063902
   x:   3     0.87152827    17.50334992    -5.32995979

          eigenvectors and eigenvalues in the schmidt orthonormalized basis

                v:   1         v:   2         v:   3

   energy   -85.67213812   -81.54491440   -80.72164279

  zx:   1     0.97299478     0.02153534     0.22982033
  zx:   2     0.22587037    -0.29405115    -0.92871766
  zx:   3     0.04757868     0.95554704    -0.29097443

          <ref|baseci> overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     1.00000000     0.00000000     0.00000000

  write the transformed vectors back to cosmofile

 rtflw =  0
 freezing =  F

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1    -76.3360582449 -9.3266E+00  5.7768E-04  5.6060E-02  1.0000E-03
 mr-sdci #  3  2    -72.2088345244 -8.5826E+00  0.0000E+00  3.6781E+00  1.0000E-04
 mr-sdci #  3  3    -71.3855629139  7.1386E+01  0.0000E+00  4.1850E+00  1.0000E-04


 total energy in cosmo calc 
e(rootcalc) + repnuc=     -76.3360582449
dielectric energy =      -0.0095031456
deltaediel =       0.0095031456
e(rootcalc) + repnuc - ediel =     -76.3265550993
e(rootcalc) + repnuc - edielnew =     -76.3360582449
deltaelast =      76.3265550993

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.01   0.00   0.00   0.01         0.    0.0000
    2   25    0   0.02   0.00   0.00   0.01         0.    0.0000
    3   26    0   0.02   0.00   0.00   0.02         0.    0.0000
    4   11    0   0.02   0.00   0.00   0.01         0.    0.0000
    5   15    0   0.02   0.00   0.00   0.02         0.    0.0000
    6   16    0   0.01   0.00   0.00   0.01         0.    0.0000
    7    1    0   0.01   0.00   0.00   0.00         0.    0.0000
    8    5    0   0.03   0.00   0.00   0.02         0.    0.0000
    9    6    0   0.02   0.00   0.00   0.02         0.    0.0000
   10    7    0   0.04   0.00   0.00   0.03         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.03   0.00   0.00   0.02         0.    0.0000
   14   47    0   0.05   0.00   0.00   0.04         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.2800s 
time spent in multnx:                   0.2100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0200s 
total time per CI iteration:            1.5200s 

          starting ci iteration   4

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.33607988

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=       3  D0X=       9  D0W=      15
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================

          ci-one-electron density block   1

                mo   1         mo   2         mo   3         mo   4         mo   5         mo   6         mo   7         mo   8
   mo   1     1.99955710
   mo   2    -0.00012014     1.98391367
   mo   3    -0.00013464     0.00284832     1.97002348
   mo   4    -0.00011937    -0.00030297     0.01007766     0.00104901
   mo   5     0.00016193     0.00513298    -0.00743134    -0.00222837     0.00603335
   mo   6    -0.00016034     0.00392138     0.00556780     0.00022413     0.00058315     0.00269460
   mo   7     0.00027873    -0.00443685    -0.01369888    -0.00087219     0.00042284    -0.00118402     0.00244103
   mo   8    -0.00019363    -0.00630640     0.00425300     0.00250503    -0.00740154    -0.00033009     0.00024018     0.01003539
   mo   9    -0.00024582     0.00491565     0.00937212     0.00014293     0.00128340     0.00280189    -0.00185617    -0.00128238
   mo  10     0.00008798     0.00163549    -0.00525595    -0.00059227     0.00146039     0.00217474     0.00067368    -0.00100959
   mo  11     0.00047582    -0.00093334     0.00024912    -0.00060374    -0.00035133    -0.00135187     0.00248960     0.00152348
   mo  12     0.00043135     0.00141209    -0.00408347    -0.00080033     0.00125119    -0.00130018     0.00123313    -0.00169048
   mo  13    -0.00000687    -0.00104408     0.00105690     0.00023512    -0.00083621     0.00039308     0.00028793     0.00142917
   mo  14    -0.00019023    -0.00322102     0.00513077     0.00080328    -0.00214183    -0.00029613    -0.00030580     0.00290655
   mo  15    -0.00022150     0.00167755    -0.00614285    -0.00012372     0.00085054     0.00088087    -0.00056356    -0.00117813
   mo  16     0.00001017    -0.00013267     0.00065094     0.00001634    -0.00006466    -0.00016652    -0.00000871     0.00004035
   mo  17    -0.00017450    -0.00000625    -0.00177657     0.00007299     0.00005276    -0.00002470    -0.00035930    -0.00041455
   mo  18    -0.00003939     0.00003224    -0.00024367     0.00004934    -0.00012261    -0.00017787    -0.00007862     0.00013542
   mo  19    -0.00010563     0.00030687    -0.00052985     0.00009156    -0.00015479     0.00033589    -0.00009608     0.00029415
   mo  20     0.00058898     0.00182554    -0.00017451    -0.00019477     0.00022512    -0.00026941     0.00044388    -0.00022842

                mo   9         mo  10         mo  11         mo  12         mo  13         mo  14         mo  15         mo  16
   mo   9     0.00536419
   mo  10     0.00065718     0.00440343
   mo  11    -0.00065987    -0.00048684     0.00447896
   mo  12    -0.00240275     0.00007727     0.00006332     0.00255476
   mo  13    -0.00067023     0.00137180    -0.00014330     0.00000763     0.00123240
   mo  14    -0.00073435    -0.00087538    -0.00030254    -0.00011851     0.00034351     0.00155898
   mo  15     0.00160718     0.00058212    -0.00075390    -0.00042394    -0.00029184    -0.00054883     0.00115876
   mo  16    -0.00002349    -0.00041435     0.00014189     0.00000414    -0.00007385     0.00004384    -0.00004177     0.00040571
   mo  17    -0.00019264    -0.00027468    -0.00096872     0.00036771    -0.00013672     0.00003674     0.00007496     0.00000742
   mo  18     0.00038650    -0.00087801     0.00054424    -0.00041599    -0.00062202    -0.00000576    -0.00000043     0.00009132
   mo  19     0.00047976     0.00039351     0.00017934    -0.00063110     0.00024158    -0.00023602     0.00010539    -0.00004517
   mo  20    -0.00040434     0.00008747     0.00055262     0.00048746    -0.00000970    -0.00021135    -0.00015190     0.00000631

                mo  17         mo  18         mo  19         mo  20
   mo  17     0.00061857
   mo  18    -0.00012941     0.00069137
   mo  19    -0.00022425     0.00000017     0.00043875
   mo  20    -0.00006930    -0.00002346    -0.00007329     0.00038515

          ci-one-electron density block   2

                mo  21         mo  22         mo  23         mo  24         mo  25         mo  26         mo  27         mo  28
   mo  21     1.96908151
   mo  22     0.00165462     0.00060151
   mo  23    -0.01368615    -0.00179979     0.00677297
   mo  24    -0.00265911     0.00074919    -0.00140917     0.00148657
   mo  25     0.00206377     0.00049748    -0.00176849     0.00044233     0.00050669
   mo  26    -0.01377915    -0.00183878     0.00809103    -0.00080221    -0.00205846     0.01078407
   mo  27    -0.00295294    -0.00125756     0.00279880    -0.00229453    -0.00085374     0.00215220     0.00390631
   mo  28     0.00143912     0.00015088    -0.00048917     0.00026744     0.00003804    -0.00064156    -0.00039086     0.00057406
   mo  29     0.00833844     0.00034386    -0.00221589    -0.00027133     0.00058266    -0.00358848     0.00027397     0.00024883
   mo  30     0.00535270     0.00057304    -0.00164712     0.00089751     0.00045892    -0.00182079    -0.00189090     0.00018132
   mo  31    -0.00479697    -0.00022786     0.00059871    -0.00040172    -0.00021540     0.00059124     0.00081171    -0.00013030
   mo  32     0.00091410     0.00003708    -0.00007909     0.00010298    -0.00002434    -0.00006288    -0.00014933     0.00026638
   mo  33    -0.00344149    -0.00000568     0.00029621     0.00016809    -0.00007984     0.00073246    -0.00030475    -0.00004734
   mo  34    -0.00125773    -0.00004127    -0.00008462    -0.00020580     0.00000714    -0.00031227     0.00045752     0.00007298

                mo  29         mo  30         mo  31         mo  32         mo  33         mo  34
   mo  29     0.00208034
   mo  30     0.00001970     0.00149254
   mo  31    -0.00022680    -0.00042752     0.00060191
   mo  32     0.00001278     0.00007853    -0.00002264     0.00033081
   mo  33    -0.00074550     0.00008578    -0.00000238    -0.00000651     0.00054600
   mo  34     0.00044003    -0.00043191     0.00004972     0.00000801    -0.00020512     0.00035209

          ci-one-electron density block   3

                mo  35         mo  36         mo  37         mo  38         mo  39         mo  40         mo  41         mo  42
   mo  35     1.97177438
   mo  36     0.01164817     0.00520288
   mo  37     0.01158345     0.00590481     0.00865229
   mo  38     0.00557144    -0.00319549    -0.00181702     0.00481072
   mo  39    -0.00046704     0.00026209     0.00151786     0.00137668     0.00144247
   mo  40    -0.00772284     0.00209415     0.00318210    -0.00117014     0.00049818     0.00196537
   mo  41    -0.00013060     0.00001602    -0.00002859    -0.00005386    -0.00003928    -0.00000360     0.00039172
   mo  42    -0.00052286     0.00019175     0.00004138    -0.00052133    -0.00004624     0.00006563     0.00000642     0.00038876
   mo  43     0.00025743    -0.00024479     0.00024832     0.00115657     0.00079650    -0.00000110     0.00000428    -0.00015021

                mo  43
   mo  43     0.00078508

          ci-one-electron density block   4

                mo  44         mo  45         mo  46         mo  47         mo  48
   mo  44     0.00512932
   mo  45    -0.00053191     0.00069884
   mo  46     0.00024845     0.00007633     0.00024453
   mo  47     0.00018734    -0.00013295     0.00015923     0.00020379
   mo  48    -0.00001131    -0.00015038    -0.00008050    -0.00001200     0.00015384


*****   symmetry block SYM1   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       1.99955710
       2      -0.00012014     1.98391367
       3      -0.00013464     0.00284832     1.97002348
       4      -0.00011937    -0.00030297     0.01007766     0.00104901
       5       0.00016193     0.00513298    -0.00743134    -0.00222837
       6      -0.00016034     0.00392138     0.00556780     0.00022413
       7       0.00027873    -0.00443685    -0.01369888    -0.00087219
       8      -0.00019363    -0.00630640     0.00425300     0.00250503
       9      -0.00024582     0.00491565     0.00937212     0.00014293
      10       0.00008798     0.00163549    -0.00525595    -0.00059227
      11       0.00047582    -0.00093334     0.00024912    -0.00060374
      12       0.00043135     0.00141209    -0.00408347    -0.00080033
      13      -0.00000687    -0.00104408     0.00105690     0.00023512
      14      -0.00019023    -0.00322102     0.00513077     0.00080328
      15      -0.00022150     0.00167755    -0.00614285    -0.00012372
      16       0.00001017    -0.00013267     0.00065094     0.00001634
      17      -0.00017450    -0.00000625    -0.00177657     0.00007299
      18      -0.00003939     0.00003224    -0.00024367     0.00004934
      19      -0.00010563     0.00030687    -0.00052985     0.00009156
      20       0.00058898     0.00182554    -0.00017451    -0.00019477

               Column   5     Column   6     Column   7     Column   8
       5       0.00603335
       6       0.00058315     0.00269460
       7       0.00042284    -0.00118402     0.00244103
       8      -0.00740154    -0.00033009     0.00024018     0.01003539
       9       0.00128340     0.00280189    -0.00185617    -0.00128238
      10       0.00146039     0.00217474     0.00067368    -0.00100959
      11      -0.00035133    -0.00135187     0.00248960     0.00152348
      12       0.00125119    -0.00130018     0.00123313    -0.00169048
      13      -0.00083621     0.00039308     0.00028793     0.00142917
      14      -0.00214183    -0.00029613    -0.00030580     0.00290655
      15       0.00085054     0.00088087    -0.00056356    -0.00117813
      16      -0.00006466    -0.00016652    -0.00000871     0.00004035
      17       0.00005276    -0.00002470    -0.00035930    -0.00041455
      18      -0.00012261    -0.00017787    -0.00007862     0.00013542
      19      -0.00015479     0.00033589    -0.00009608     0.00029415
      20       0.00022512    -0.00026941     0.00044388    -0.00022842

               Column   9     Column  10     Column  11     Column  12
       9       0.00536419
      10       0.00065718     0.00440343
      11      -0.00065987    -0.00048684     0.00447896
      12      -0.00240275     0.00007727     0.00006332     0.00255476
      13      -0.00067023     0.00137180    -0.00014330     0.00000763
      14      -0.00073435    -0.00087538    -0.00030254    -0.00011851
      15       0.00160718     0.00058212    -0.00075390    -0.00042394
      16      -0.00002349    -0.00041435     0.00014189     0.00000414
      17      -0.00019264    -0.00027468    -0.00096872     0.00036771
      18       0.00038650    -0.00087801     0.00054424    -0.00041599
      19       0.00047976     0.00039351     0.00017934    -0.00063110
      20      -0.00040434     0.00008747     0.00055262     0.00048746

               Column  13     Column  14     Column  15     Column  16
      13       0.00123240
      14       0.00034351     0.00155898
      15      -0.00029184    -0.00054883     0.00115876
      16      -0.00007385     0.00004384    -0.00004177     0.00040571
      17      -0.00013672     0.00003674     0.00007496     0.00000742
      18      -0.00062202    -0.00000576    -0.00000043     0.00009132
      19       0.00024158    -0.00023602     0.00010539    -0.00004517
      20      -0.00000970    -0.00021135    -0.00015190     0.00000631

               Column  17     Column  18     Column  19     Column  20
      17       0.00061857
      18      -0.00012941     0.00069137
      19      -0.00022425     0.00000017     0.00043875
      20      -0.00006930    -0.00002346    -0.00007329     0.00038515

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7        eno   8
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    8   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno   9        eno  10        eno  11        eno  12        eno  13        eno  14        eno  15        eno  16
 emo    9   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   10   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   11   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   12   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   13   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   14   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   15   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   16   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno  17        eno  18        eno  19        eno  20
 emo   17   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   18   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   19   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   20   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   1.9995595      1.9845610      1.9697529      0.0185842      0.0106444
   0.0061906      0.0055293      0.0011458      0.0008460      0.0005825
   0.0004721      0.0003740      0.0003566      0.0001646      0.0001100
   0.0000720      0.0000528      0.0000311      0.0000087      0.0000003

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7         no   8
  mo    1   0.999946E+00   0.967715E-02   0.383476E-02   0.105990E-03   0.390313E-03  -0.176416E-03  -0.688017E-04  -0.668063E-04
  mo    2  -0.873383E-02   0.980404E+00  -0.196706E+00   0.518596E-02  -0.237600E-02   0.147339E-03  -0.400095E-03  -0.499046E-03
  mo    3  -0.565980E-02   0.196646E+00   0.980379E+00  -0.497823E-02  -0.762289E-02   0.377091E-02  -0.167724E-02   0.293559E-03
  mo    4  -0.874924E-04   0.844715E-03   0.506340E-02   0.183286E+00   0.146622E+00  -0.108998E+00  -0.643203E-01  -0.768597E-01
  mo    5   0.803666E-04   0.181911E-02  -0.424648E-02  -0.551501E+00  -0.108216E+00   0.668945E-01   0.713493E-01   0.726160E-01
  mo    6  -0.113882E-03   0.250002E-02   0.238764E-02  -0.948128E-01   0.424754E+00   0.240375E+00  -0.924205E-01   0.197539E+00
  mo    7   0.198637E-03  -0.355725E-02  -0.639204E-02   0.206968E-01  -0.353003E+00   0.361188E+00   0.109722E+00   0.185945E+00
  mo    8  -0.821230E-04  -0.272030E-02   0.278919E-02   0.715938E+00   0.134423E+00   0.190487E+00   0.111141E-01   0.203419E+00
  mo    9  -0.172078E-03   0.337533E-02   0.418770E-02  -0.170265E+00   0.572270E+00   0.481307E-01   0.435592E+00   0.350763E+00
  mo   10   0.518402E-04   0.293824E-03  -0.279152E-02  -0.149196E+00   0.159052E+00   0.679289E+00  -0.378250E+00  -0.277523E-01
  mo   11   0.242353E-03  -0.444652E-03   0.210534E-03   0.110505E+00  -0.320353E+00   0.399363E+00   0.622657E+00   0.284391E-01
  mo   12   0.222056E-03   0.290804E-03  -0.219296E-02  -0.918095E-01  -0.370432E+00  -0.287020E-01  -0.281285E+00   0.579384E+00
  mo   13  -0.188788E-05  -0.415107E-03   0.632299E-03   0.852985E-01   0.238342E-01   0.263746E+00  -0.264478E+00  -0.198752E+00
  mo   14  -0.959192E-04  -0.109162E-02   0.288952E-02   0.218041E+00   0.197686E-01  -0.116736E+00  -0.117423E+00   0.357409E+00
  mo   15  -0.101047E-03   0.226788E-03  -0.322613E-02  -0.111308E+00   0.186014E+00  -0.120219E-02   0.417078E-01   0.200707E+00
  mo   16   0.383689E-05  -0.144026E-05   0.338041E-03   0.930247E-02  -0.191573E-01  -0.526150E-01   0.531888E-01   0.250235E-01
  mo   17  -0.823285E-04  -0.178891E-03  -0.884195E-03  -0.217818E-01   0.990313E-02  -0.156828E+00  -0.149876E+00   0.199758E+00
  mo   18  -0.192016E-04  -0.829251E-05  -0.121536E-03   0.169597E-01   0.383151E-02  -0.101416E+00   0.236264E+00  -0.113630E-01
  mo   19  -0.528026E-04   0.995161E-04  -0.292079E-03   0.101027E-01   0.773419E-01   0.101537E+00   0.574815E-01  -0.393374E+00
  mo   20   0.287383E-03   0.886274E-03  -0.272632E-03  -0.129525E-01  -0.947282E-01   0.624033E-01   0.231235E-01   0.103027E+00

                no   9         no  10         no  11         no  12         no  13         no  14         no  15         no  16
  mo    1  -0.309550E-04   0.854256E-05  -0.202330E-03   0.704808E-04   0.168544E-03   0.275003E-04  -0.628280E-04   0.297989E-04
  mo    2  -0.312127E-03   0.816637E-05  -0.151764E-02   0.462487E-03   0.122367E-02  -0.296750E-03  -0.352864E-04  -0.357178E-03
  mo    3   0.335181E-02  -0.652826E-03  -0.425007E-02   0.161706E-02   0.237190E-02   0.141259E-02   0.184248E-02  -0.174920E-02
  mo    4   0.136183E+00  -0.323539E-01   0.141854E+00  -0.466983E-01  -0.140896E+00  -0.106953E+00  -0.153655E+00   0.119621E+00
  mo    5  -0.382968E+00   0.419792E-01  -0.133684E+00   0.203812E-01   0.463039E-01   0.180645E+00   0.279512E+00  -0.573633E-01
  mo    6  -0.156445E+00  -0.305154E+00   0.256591E+00   0.130137E+00  -0.122319E-01   0.718620E-01  -0.407137E-01  -0.167331E-01
  mo    7   0.102889E+00  -0.298461E-01  -0.190446E+00   0.125554E+00   0.317767E+00   0.584838E-01  -0.734710E-01  -0.232281E+00
  mo    8  -0.197809E-01  -0.271341E-02  -0.551802E-01   0.173415E-01   0.548139E-01   0.185952E+00   0.141010E+00  -0.100042E+00
  mo    9  -0.245514E-01   0.287774E+00   0.122430E+00  -0.139994E+00   0.580912E-01   0.664716E-01  -0.192748E+00   0.191325E-01
  mo   10   0.958838E-01  -0.229959E+00  -0.788216E-01   0.276071E-01  -0.421940E-01  -0.175332E+00  -0.526435E-01  -0.395284E-01
  mo   11  -0.306017E-01   0.630376E-01   0.123826E+00  -0.504522E-01  -0.143141E-01  -0.126631E+00   0.537784E-02   0.215370E+00
  mo   12   0.140552E+00   0.931585E-01   0.176708E+00  -0.856323E-01  -0.262185E+00   0.324304E+00  -0.401145E+00   0.119921E+00
  mo   13  -0.208156E+00   0.498479E+00   0.781115E-01   0.607359E-01  -0.239813E-01   0.254566E+00   0.202104E+00   0.579808E+00
  mo   14  -0.570103E+00   0.228821E-01  -0.352210E+00  -0.299468E-01  -0.888885E-01  -0.290612E-01   0.149633E+00  -0.169191E+00
  mo   15   0.575347E+00   0.195025E+00  -0.557340E+00   0.137411E+00  -0.195698E+00   0.419200E-01   0.302962E+00   0.876944E-01
  mo   16  -0.548389E-01   0.205329E+00   0.191731E+00   0.907177E+00  -0.161423E+00  -0.107047E+00  -0.685632E-01  -0.161749E+00
  mo   17   0.207959E+00  -0.139410E-01   0.279800E+00   0.992266E-01   0.730793E+00   0.148045E+00   0.295224E+00   0.559985E-01
  mo   18  -0.101793E-01  -0.635019E+00  -0.935915E-01   0.219858E+00  -0.143725E+00   0.439499E+00   0.936835E-01   0.394836E+00
  mo   19   0.585803E-01   0.147735E+00  -0.210417E-01  -0.398255E-01  -0.357292E-01   0.679902E+00  -0.164909E+00  -0.462769E+00
  mo   20   0.140274E+00  -0.188753E-01   0.473329E+00  -0.167195E+00  -0.413453E+00   0.137020E-01   0.622574E+00  -0.289168E+00

                no  17         no  18         no  19         no  20
  mo    1  -0.339780E-04  -0.136516E-04  -0.269333E-05  -0.217022E-06
  mo    2   0.815205E-03   0.530716E-03  -0.293027E-03   0.285399E-04
  mo    3   0.105282E-02   0.187013E-02   0.151646E-02  -0.106197E-02
  mo    4  -0.614690E-01  -0.134994E+00  -0.245376E+00   0.854190E+00
  mo    5  -0.188034E+00  -0.857503E-01   0.407136E+00   0.421781E+00
  mo    6  -0.522736E+00   0.422135E+00  -0.205947E+00  -0.612715E-01
  mo    7   0.321192E+00   0.499670E+00  -0.214663E+00   0.267698E+00
  mo    8  -0.944281E-01  -0.118067E-01   0.548786E+00   0.100585E+00
  mo    9   0.400898E+00  -0.556612E-01   0.349949E-01   0.626682E-02
  mo   10   0.216255E+00  -0.438943E+00   0.338384E-01  -0.259028E-01
  mo   11  -0.355683E+00  -0.293539E+00  -0.188881E+00  -0.580273E-01
  mo   12  -0.835488E-01  -0.976742E-01   0.687072E-01  -0.356259E-01
  mo   13   0.127057E+00   0.187994E+00  -0.132869E+00  -0.363204E-03
  mo   14   0.455696E-01  -0.239089E+00  -0.475799E+00  -0.320362E-01
  mo   15  -0.245280E+00   0.155818E-01  -0.111617E+00  -0.331738E-02
  mo   16   0.610213E-01  -0.121882E+00   0.382428E-01   0.151118E-02
  mo   17  -0.658857E-01  -0.321226E+00  -0.164566E+00  -0.350525E-02
  mo   18   0.296271E+00  -0.688250E-01  -0.445782E-01  -0.481727E-02
  mo   19  -0.118483E+00  -0.186360E+00  -0.210267E+00  -0.199804E-01
  mo   20   0.215154E+00   0.684236E-01  -0.101788E+00   0.102513E-01
  eigenvectors of nos in ao-basis, column    1
  0.9978863E+00 -0.1336622E-01  0.2766673E-02  0.3664692E-02 -0.8073229E-02  0.3809548E-02  0.5792622E-03  0.1063718E-04
  0.3017979E-05  0.1056648E-04 -0.2598495E-03  0.2027378E-04  0.5150173E-04  0.1034006E-02 -0.1499110E-02  0.4852702E-04
  0.4946641E-03  0.2513324E-03 -0.4919308E-03 -0.5088689E-03
  eigenvectors of nos in ao-basis, column    2
  0.2125482E-02  0.9115066E+00 -0.3659948E-02 -0.1326553E+00  0.3298614E-01  0.1007132E-01  0.7331024E-01  0.1642558E-03
  0.2551572E-02 -0.1472515E-02 -0.2634320E-02 -0.7924964E-03 -0.1833761E-02  0.2627815E+00 -0.1156965E+00  0.2899445E-02
  0.2673441E-01  0.1930660E-01  0.1555886E-01  0.1121414E-01
  eigenvectors of nos in ao-basis, column    3
  0.9254486E-02  0.1244513E+00  0.3879391E-02  0.1525756E+00  0.8339265E+00 -0.2613518E-01 -0.2656058E-01 -0.1598197E-02
 -0.2669126E-02 -0.5397435E-02  0.1703202E-02 -0.7329431E-04  0.2660558E-02 -0.3927907E+00  0.1654873E+00  0.5841666E-02
 -0.2963229E-01 -0.1190887E-02 -0.1414606E-02  0.2973508E-01
  eigenvectors of nos in ao-basis, column    4
 -0.4241965E-01 -0.5663183E+00 -0.4875402E-01 -0.4402017E-01  0.1360591E+01 -0.2194938E+00 -0.7366025E+00  0.4109626E-02
  0.1622863E-01  0.1093816E-01  0.2759580E-01  0.2362400E-02  0.3747242E-02  0.9440706E+00 -0.4163459E+00 -0.7217393E-02
  0.5986519E-01  0.9740514E-02  0.4618308E-02 -0.3156223E-01
  eigenvectors of nos in ao-basis, column    5
  0.2160126E-01 -0.1061704E+01 -0.4488146E+00  0.1108207E+01 -0.8012377E+00  0.3954029E+00  0.8151011E+00 -0.5728064E-02
  0.3761496E-01 -0.2542989E-01  0.5413130E-01 -0.2869788E-02 -0.3875523E-02  0.4835665E+00 -0.2875115E+00  0.1814544E-01
  0.2660355E-01  0.8281374E-01  0.5321191E-02  0.3744371E-01
  eigenvectors of nos in ao-basis, column    6
 -0.1611240E-01  0.5081459E+00  0.2765050E+00 -0.9957933E+00 -0.2536352E-01  0.4116942E-02  0.8498660E-01 -0.4175342E-01
  0.1044586E+00 -0.1026679E+00  0.2348838E+00 -0.1547122E-01 -0.1973049E-01  0.2799811E+00 -0.1175416E+00 -0.1129952E-01
 -0.4054272E-01  0.8523363E-01 -0.2609377E-01  0.2933370E-01
  eigenvectors of nos in ao-basis, column    7
 -0.8546777E-03  0.1983607E+00  0.9192017E-01 -0.3418798E+00 -0.2842737E+00  0.1222335E+00  0.4133864E+00  0.8232369E-01
  0.9296351E-01  0.1682891E+00  0.1866856E+00  0.8514398E-02 -0.1564414E-01  0.1003828E+00 -0.6339393E-01 -0.6967448E-02
  0.5024518E-01 -0.3349880E-01  0.3870336E-02 -0.3519918E-01
  eigenvectors of nos in ao-basis, column    8
 -0.1261117E+00  0.1972362E-01  0.4382160E+00 -0.1961356E+01 -0.2435905E+00  0.3292427E+00  0.1002644E+01  0.1928373E-01
 -0.2413393E+00  0.1393863E-01 -0.2210549E+00 -0.3406535E-01 -0.8881482E-01  0.2141923E+01 -0.1018332E+01 -0.7043886E-03
  0.7285233E-01  0.1640358E+00  0.8265150E-01  0.6505549E-01
  eigenvectors of nos in ao-basis, column    9
 -0.2069058E+00 -0.2552489E+00  0.3963001E+00  0.2801809E+00 -0.2533142E+01  0.2400376E+01  0.3808745E+00 -0.1577087E-01
 -0.4376104E-01  0.1354071E-01  0.3888527E-01  0.8300864E-02  0.4159049E-01 -0.6582909E-01  0.1412506E+00 -0.3562414E-01
 -0.1778831E+00 -0.9454040E-01 -0.5509477E-01 -0.1243220E+00
  eigenvectors of nos in ao-basis, column   10
  0.8359755E-01  0.2072618E+00 -0.7597418E-01 -0.5792618E+00 -0.2008990E+00  0.2446133E+00  0.8153566E+00 -0.2116184E+00
  0.3106664E-01  0.1151945E-01  0.2155346E-01  0.9698433E-01 -0.1128640E+00  0.6428523E+00 -0.3313735E+00 -0.1044884E-01
  0.3270333E+00 -0.3018441E+00  0.9259461E-01 -0.1581909E+00
  eigenvectors of nos in ao-basis, column   11
 -0.1325877E+01 -0.2798056E+01  0.1911295E+01  0.2027996E+01  0.8241374E+00 -0.1120938E+01  0.2474552E-01 -0.2643338E-01
 -0.1267406E+00  0.1951458E-01  0.1559401E+00  0.1417332E+00  0.1847191E+00 -0.2763125E+00  0.1989847E+00 -0.4943727E-02
 -0.1788053E+00 -0.1860615E+00 -0.4772754E-01  0.7308343E-01
  eigenvectors of nos in ao-basis, column   12
  0.5257812E+00  0.1157462E+01 -0.7403387E+00 -0.5968623E+00 -0.2228495E+00  0.2867444E+00 -0.3090607E+00  0.1089577E+00
 -0.6100129E-01 -0.6568602E-01  0.3766694E-01  0.3141818E+00 -0.1717776E+00 -0.1636259E+00  0.7006608E-01 -0.5566483E-02
 -0.3077283E-01  0.4419960E-01 -0.9923136E-01  0.8767364E-01
  eigenvectors of nos in ao-basis, column   13
  0.1353960E+01  0.3012256E+01 -0.1884620E+01 -0.1269304E+01  0.1086327E+00 -0.2048592E+00 -0.1077634E+00 -0.7039369E-01
 -0.2925252E+00  0.6598129E-01  0.2534748E+00  0.2222101E-01  0.2562095E+00 -0.2240110E+00 -0.6558492E-01 -0.2249201E-01
 -0.9081831E-01 -0.3969764E-01 -0.1656145E+00 -0.1004085E+00
  eigenvectors of nos in ao-basis, column   14
  0.3826020E+00  0.1049813E+01 -0.2791728E+00 -0.3256038E+01 -0.7505040E+00  0.1178346E+01  0.1082003E+01  0.9289802E-01
  0.3073046E+00 -0.1714038E+00 -0.4826523E+00  0.6837150E-01  0.3295365E+00  0.2699537E+01 -0.1239400E+01  0.2226467E-01
  0.4141294E+00 -0.1257517E+00  0.1849766E+00  0.2239302E+00
  eigenvectors of nos in ao-basis, column   15
 -0.2184642E+01 -0.5798056E+01  0.2443036E+01  0.3445741E+01 -0.1231359E+01  0.2418218E+01 -0.2116562E+00  0.3277517E-01
 -0.1643167E+00 -0.9357807E-01 -0.4876930E-01  0.1404363E-01  0.1298579E+00  0.1735647E+01 -0.1552063E+01  0.1127248E+00
  0.1065916E+01  0.4837288E+00 -0.1532146E+00  0.1231420E-01
  eigenvectors of nos in ao-basis, column   16
  0.1050244E+01  0.2840220E+01 -0.1115097E+01 -0.2948022E+01  0.6516550E-01 -0.2977303E+00  0.4772221E+00  0.1602219E+00
 -0.2203980E+00 -0.3226435E+00  0.5047302E+00 -0.1413264E+00 -0.9330598E-01 -0.9090255E+00  0.1277786E+01 -0.7275714E-01
  0.2401544E+00 -0.9022820E+00  0.3081201E+00  0.3016384E+00
  eigenvectors of nos in ao-basis, column   17
 -0.9957667E+00 -0.2726598E+01  0.9680824E+00  0.5142946E+01  0.6207361E+00 -0.9647183E+00  0.8372495E+00  0.1154155E+00
 -0.1527146E-01 -0.1623654E+00  0.3438701E-01  0.6534029E-02  0.2161484E-01 -0.1099292E+00 -0.1042043E+01 -0.2876785E-01
 -0.2891897E-02 -0.2540536E-01 -0.4857197E+00 -0.1234287E+01
  eigenvectors of nos in ao-basis, column   18
 -0.6729150E+00 -0.1913290E+01  0.4879908E+00  0.6702020E+01  0.1950679E+00 -0.5367576E+00 -0.1389253E+01 -0.5537992E-02
  0.1382057E-01 -0.3640679E-01  0.2585271E+00 -0.1115505E+00 -0.1636252E+00 -0.2092084E+01 -0.3656139E+00  0.2191373E-01
 -0.1878689E+00 -0.7067615E+00 -0.1311932E+01 -0.2654049E+00
  eigenvectors of nos in ao-basis, column   19
  0.1640171E+00  0.4657466E+00 -0.2909311E+00  0.3311218E+01  0.3419392E+00 -0.1149489E+01 -0.1304348E+01  0.5140671E-02
 -0.6025009E-01  0.2817515E-01  0.4285796E+00 -0.4331882E-01 -0.1534789E+00 -0.2350572E+01 -0.2308335E+00  0.2997218E+00
 -0.1038528E+01 -0.6377940E+00  0.3817809E+00  0.4937499E-01
  eigenvectors of nos in ao-basis, column   20
 -0.1448573E+00 -0.4465231E+00  0.6176008E-01  0.2193452E+01  0.6029925E-02 -0.4165339E-01 -0.5362900E+00 -0.2911025E-03
 -0.1079141E-01  0.2970923E-02  0.5402605E-01 -0.3651016E-02 -0.1183191E-01 -0.2564115E+00 -0.1218048E+01  0.8832082E+00
 -0.1045638E+00 -0.7494022E-01 -0.2972419E+00 -0.2380980E+00


*****   symmetry block SYM2   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       1.96908151
       2       0.00165462     0.00060151
       3      -0.01368615    -0.00179979     0.00677297
       4      -0.00265911     0.00074919    -0.00140917     0.00148657
       5       0.00206377     0.00049748    -0.00176849     0.00044233
       6      -0.01377915    -0.00183878     0.00809103    -0.00080221
       7      -0.00295294    -0.00125756     0.00279880    -0.00229453
       8       0.00143912     0.00015088    -0.00048917     0.00026744
       9       0.00833844     0.00034386    -0.00221589    -0.00027133
      10       0.00535270     0.00057304    -0.00164712     0.00089751
      11      -0.00479697    -0.00022786     0.00059871    -0.00040172
      12       0.00091410     0.00003708    -0.00007909     0.00010298
      13      -0.00344149    -0.00000568     0.00029621     0.00016809
      14      -0.00125773    -0.00004127    -0.00008462    -0.00020580

               Column   5     Column   6     Column   7     Column   8
       5       0.00050669
       6      -0.00205846     0.01078407
       7      -0.00085374     0.00215220     0.00390631
       8       0.00003804    -0.00064156    -0.00039086     0.00057406
       9       0.00058266    -0.00358848     0.00027397     0.00024883
      10       0.00045892    -0.00182079    -0.00189090     0.00018132
      11      -0.00021540     0.00059124     0.00081171    -0.00013030
      12      -0.00002434    -0.00006288    -0.00014933     0.00026638
      13      -0.00007984     0.00073246    -0.00030475    -0.00004734
      14       0.00000714    -0.00031227     0.00045752     0.00007298

               Column   9     Column  10     Column  11     Column  12
       9       0.00208034
      10       0.00001970     0.00149254
      11      -0.00022680    -0.00042752     0.00060191
      12       0.00001278     0.00007853    -0.00002264     0.00033081
      13      -0.00074550     0.00008578    -0.00000238    -0.00000651
      14       0.00044003    -0.00043191     0.00004972     0.00000801

               Column  13     Column  14
      13       0.00054600
      14      -0.00020512     0.00035209

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7        eno   8
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    8   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno   9        eno  10        eno  11        eno  12        eno  13        eno  14
 emo    9   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   10   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   11   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   12   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   13   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   14   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   1.9693569      0.0203598      0.0060361      0.0010739      0.0008320
   0.0006594      0.0003968      0.0001678      0.0001041      0.0000961
   0.0000187      0.0000106      0.0000050      0.0000002

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7         no   8
  mo    1   0.999929E+00   0.109566E-01  -0.199536E-02  -0.104935E-02  -0.238703E-02   0.566407E-03   0.102122E-02  -0.142439E-02
  mo    2   0.856040E-03  -0.148333E+00  -0.132303E+00   0.210286E-01  -0.214890E+00  -0.379817E-01   0.628880E-01   0.725568E-01
  mo    3  -0.701347E-02   0.564785E+00   0.221785E-01   0.199921E+00   0.362648E+00  -0.394228E-01   0.618436E-01  -0.227653E+00
  mo    4  -0.133972E-02  -0.117789E+00  -0.415124E+00   0.252966E+00  -0.278331E+00  -0.396909E-01   0.182877E+00  -0.104700E+00
  mo    5   0.106465E-02  -0.149008E+00  -0.294846E-01  -0.303073E-01  -0.102457E+00  -0.193962E+00  -0.508900E-01   0.163962E+00
  mo    6  -0.707872E-02   0.701891E+00  -0.309904E+00   0.153429E+00  -0.189228E+00  -0.109754E+00   0.809756E-02   0.184289E+00
  mo    7  -0.152261E-02   0.238703E+00   0.665635E+00  -0.199250E+00   0.109533E-01   0.112399E+00  -0.556519E-01   0.858769E-01
  mo    8   0.736209E-03  -0.482532E-01  -0.324015E-01   0.376079E+00   0.530566E-01   0.699602E+00  -0.901332E-01  -0.402751E+00
  mo    9   0.426045E-02  -0.206674E+00   0.356243E+00   0.548234E+00   0.123101E+00  -0.245713E+00   0.238752E-01   0.554924E-02
  mo   10   0.273436E-02  -0.153230E+00  -0.292473E+00  -0.148601E+00   0.726445E+00   0.120522E-01   0.187429E+00  -0.223098E-01
  mo   11  -0.244236E-02   0.569833E-01   0.117718E+00  -0.308996E+00  -0.226200E+00   0.237126E+00   0.782687E+00  -0.166660E+00
  mo   12   0.465030E-03  -0.786659E-02  -0.277018E-01   0.186014E+00   0.431709E-01   0.518028E+00   0.416627E-01   0.770328E+00
  mo   13  -0.175294E-02   0.357739E-01  -0.145992E+00  -0.405854E+00  -0.125933E+00   0.242029E+00  -0.541521E+00  -0.180305E+00
  mo   14  -0.637019E-03  -0.875125E-02   0.142316E+00   0.269372E+00  -0.289536E+00   0.226597E-02  -0.402469E-01  -0.217478E+00

                no   9         no  10         no  11         no  12         no  13         no  14
  mo    1   0.402958E-03   0.140977E-02   0.966034E-03   0.145047E-02   0.129895E-02   0.107095E-03
  mo    2  -0.753274E-01  -0.105603E+00   0.342571E+00  -0.995676E-01  -0.196726E-01   0.871050E+00
  mo    3   0.467377E-01   0.365326E+00   0.670106E-01   0.374999E+00   0.314189E+00   0.268912E+00
  mo    4  -0.176936E+00   0.377091E-01   0.598354E+00   0.282184E+00   0.123570E+00  -0.375101E+00
  mo    5  -0.732986E-01   0.114895E+00  -0.120342E+00  -0.347305E+00   0.862139E+00  -0.381492E-01
  mo    6  -0.134140E+00  -0.149481E+00  -0.251879E-01  -0.483399E+00  -0.158788E+00  -0.773047E-01
  mo    7  -0.943050E-01  -0.262901E+00   0.566151E+00  -0.870276E-01   0.129725E+00  -0.118674E+00
  mo    8  -0.173211E+00  -0.287520E+00  -0.118731E+00  -0.184868E+00   0.173109E+00   0.410680E-01
  mo    9  -0.415107E+00   0.388247E+00   0.301048E-01  -0.270762E+00  -0.237083E+00  -0.138286E-01
  mo   10   0.164221E+00  -0.375352E-02   0.322013E+00  -0.411108E+00  -0.462116E-01  -0.597973E-01
  mo   11  -0.116236E+00   0.299238E+00  -0.111397E+00  -0.154186E+00  -0.200934E-01  -0.108536E-02
  mo   12   0.124823E+00   0.265757E+00   0.429249E-01   0.105362E+00   0.524448E-02  -0.567999E-02
  mo   13  -0.198645E+00   0.556948E+00   0.165551E+00  -0.157371E+00  -0.103006E+00  -0.776115E-02
  mo   14   0.794191E+00   0.213353E+00   0.155333E+00  -0.268001E+00  -0.308098E-01  -0.345592E-01
  eigenvectors of nos in ao-basis, column    1
  0.7575048E+00 -0.3117754E-01 -0.1359725E+00 -0.1594230E-01 -0.9012801E-02  0.2822453E-03 -0.3321380E-02 -0.5495734E+00
  0.1527712E+00 -0.9086076E-02 -0.2192812E-01 -0.2999639E-01 -0.1962194E-01 -0.2434338E-01
  eigenvectors of nos in ao-basis, column    2
 -0.1332050E+01  0.1066640E+00  0.5397491E+00 -0.4118673E-01 -0.6169983E-01  0.7006864E-04  0.3375184E-02 -0.1033208E+01
  0.4136230E+00 -0.1771362E-01 -0.5539431E-01 -0.3412000E-01 -0.8471605E-02 -0.2504564E-02
  eigenvectors of nos in ao-basis, column    3
 -0.7419638E+00  0.3305946E+00  0.8851838E+00  0.2798029E+00  0.5377133E+00 -0.8642182E-02  0.1160758E-01  0.2481172E+00
 -0.8852544E-01 -0.3286769E-01  0.8992566E-01 -0.1500860E-01  0.3552190E-01  0.3896344E-01
  eigenvectors of nos in ao-basis, column    4
  0.1254806E+01 -0.1097533E+01 -0.1328029E+01  0.5243562E+00  0.4320882E+00  0.2698579E-01  0.1115047E-01 -0.1498265E+01
  0.3649997E+00  0.3446749E-01 -0.1281778E+00  0.1970968E+00 -0.1263912E+00 -0.1191445E+00
  eigenvectors of nos in ao-basis, column    5
  0.2086264E+01 -0.1847579E+01  0.3762764E+00 -0.2135728E+00 -0.2964507E+00 -0.1956168E-01  0.8247139E-01  0.1060199E+01
 -0.5701040E+00  0.4129070E-01  0.2035493E+00  0.1817262E+00  0.1221903E+00  0.2102685E+00
  eigenvectors of nos in ao-basis, column    6
 -0.7504540E+00  0.8058683E+00  0.1198208E+01 -0.1582397E+00 -0.1534369E+00  0.7122263E-01  0.2297866E+00  0.1086344E+01
 -0.7988400E-01 -0.4470359E-01 -0.2316838E+00  0.3986272E+00  0.1543889E+00  0.1385791E+00
  eigenvectors of nos in ao-basis, column    7
 -0.6726114E+00  0.9747981E+00 -0.5510307E-01  0.3689693E+00 -0.4291412E+00 -0.8128769E-01  0.4245957E+00  0.2917993E+00
 -0.4977447E+00  0.7573613E-01  0.1641113E+00  0.1117729E+00 -0.2005060E+00  0.7252899E-01
  eigenvectors of nos in ao-basis, column    8
  0.1232137E+01 -0.2058260E+01 -0.1008530E+01 -0.1269963E+00  0.7045397E+00  0.7331722E-01  0.3924747E+00 -0.2129088E+01
  0.1172545E+01 -0.1461560E+00 -0.6354740E-01 -0.8042490E+00 -0.1062941E+00 -0.2341105E+00
  eigenvectors of nos in ao-basis, column    9
 -0.2116649E+00  0.3501359E+00  0.1506788E+01  0.9609200E+00 -0.1683884E+01  0.6449685E-01 -0.1141034E+00  0.2544990E+01
 -0.1048280E+01  0.5604864E-01  0.1693162E+00 -0.1380258E+00  0.1242573E+00  0.3180698E+00
  eigenvectors of nos in ao-basis, column   10
 -0.1685189E+01  0.3337546E+01 -0.9858550E+00 -0.1222095E+00 -0.8592585E+00  0.8640006E-01 -0.2117067E-01  0.2244785E+01
 -0.3407746E+01  0.3513797E+00  0.1165858E+01  0.4049895E+00 -0.5374042E+00 -0.2000759E+00
  eigenvectors of nos in ao-basis, column   11
  0.1163742E+00 -0.8472324E-02  0.4024532E+01  0.7560088E-01 -0.8882403E+00  0.3788782E-01 -0.9930947E-01  0.2327589E+01
  0.2895580E+01 -0.9635548E-01  0.5652880E+00  0.2211314E+00  0.7140328E+00  0.1804211E+01
  eigenvectors of nos in ao-basis, column   12
  0.5995462E+00 -0.1805456E+01 -0.2393094E+01 -0.2126006E+00  0.1509890E+01 -0.1495950E-01  0.1216691E+00 -0.2744403E+01
 -0.3096214E+01  0.4197801E+00 -0.9291626E+00 -0.9786524E+00 -0.1695112E+01  0.2488273E+00
  eigenvectors of nos in ao-basis, column   13
  0.2355879E+00 -0.8268286E+00  0.2420973E+01  0.3571627E-01  0.2052552E+00 -0.1226510E-01  0.3814022E-01 -0.2217243E+00
  0.8554124E+01  0.2494097E+00 -0.4767679E+00 -0.1175661E+00  0.4506321E+01  0.1230739E+01
  eigenvectors of nos in ao-basis, column   14
 -0.1171860E-01  0.8518208E-03 -0.1513940E+01 -0.4072416E-01  0.2424004E+00 -0.4866637E-02  0.1036957E-01 -0.4275337E+00
 -0.2590190E+01  0.2873590E+01 -0.1590156E+00 -0.6822070E-01 -0.4910478E+00 -0.7196039E+00


*****   symmetry block SYM3   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       1.97177438
       2       0.01164817     0.00520288
       3       0.01158345     0.00590481     0.00865229
       4       0.00557144    -0.00319549    -0.00181702     0.00481072
       5      -0.00046704     0.00026209     0.00151786     0.00137668
       6      -0.00772284     0.00209415     0.00318210    -0.00117014
       7      -0.00013060     0.00001602    -0.00002859    -0.00005386
       8      -0.00052286     0.00019175     0.00004138    -0.00052133
       9       0.00025743    -0.00024479     0.00024832     0.00115657

               Column   5     Column   6     Column   7     Column   8
       5       0.00144247
       6       0.00049818     0.00196537
       7      -0.00003928    -0.00000360     0.00039172
       8      -0.00004624     0.00006563     0.00000642     0.00038876
       9       0.00079650    -0.00000110     0.00000428    -0.00015021

               Column   9
       9       0.00078508

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7        eno   8
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    8   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno   9
 emo    9   0.100000E+01

 occupation numbers of nos
   1.9719581      0.0154758      0.0055930      0.0008016      0.0006295
   0.0004036      0.0003717      0.0001247      0.0000556

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7         no   8
  mo    1   0.999953E+00  -0.545912E-02  -0.322678E-02   0.680535E-02   0.211122E-02   0.561115E-03  -0.767253E-03  -0.909712E-03
  mo    2   0.593118E-02   0.559501E+00  -0.122249E+00  -0.447454E+00   0.121021E+00   0.960539E-01  -0.140817E+00   0.103008E+00
  mo    3   0.590843E-02   0.706702E+00   0.370469E+00  -0.628194E-01  -0.295921E+00  -0.106710E+00   0.172174E+00   0.206495E-01
  mo    4   0.281933E-02  -0.317037E+00   0.748631E+00  -0.118277E+00  -0.350199E+00  -0.617752E-02   0.678111E-01   0.233838E+00
  mo    5  -0.230618E-03   0.649707E-01   0.440903E+00   0.692126E-01   0.590884E+00  -0.169199E+00   0.114856E+00  -0.626515E+00
  mo    6  -0.390595E-02   0.286253E+00   0.786820E-01   0.867758E+00  -0.155574E-01   0.161989E+00  -0.172917E+00   0.143124E+00
  mo    7  -0.663425E-04   0.200151E-03  -0.133443E-01  -0.136546E-01   0.436010E-01   0.785215E+00   0.613593E+00  -0.679560E-01
  mo    8  -0.265367E-03   0.214304E-01  -0.872800E-01   0.995353E-01   0.334008E+00  -0.457310E+00   0.618305E+00   0.520310E+00
  mo    9   0.132183E-03  -0.191506E-01   0.281009E+00  -0.117961E+00   0.558831E+00   0.314298E+00  -0.379522E+00   0.495920E+00

                no   9
  mo    1  -0.850705E-03
  mo    2   0.646104E+00
  mo    3  -0.479914E+00
  mo    4   0.378265E+00
  mo    5   0.114489E+00
  mo    6   0.286324E+00
  mo    7   0.750937E-02
  mo    8   0.910919E-01
  mo    9  -0.325120E+00
  eigenvectors of nos in ao-basis, column    1
  0.9217362E+00 -0.6629097E-02  0.4099101E-01 -0.4873891E-02 -0.1998015E-01  0.1172678E-02  0.3251297E-03  0.2260022E-01
  0.3910362E-01
  eigenvectors of nos in ao-basis, column    2
 -0.1680823E+01  0.5560852E+00  0.1311024E+01 -0.1753841E-01 -0.7622528E-01  0.1794053E-03  0.4683834E-03  0.6513126E-01
  0.6977910E-01
  eigenvectors of nos in ao-basis, column    3
 -0.2322026E+00  0.1021773E+00  0.3549436E+00  0.3410917E+00  0.7135646E+00 -0.3187424E-02 -0.5238162E-02 -0.6991410E-01
 -0.5171216E-01
  eigenvectors of nos in ao-basis, column    4
 -0.2674472E+01  0.2782711E+01  0.3433651E+00 -0.1361350E+00  0.2647629E-01  0.6711575E-02 -0.4274866E-04 -0.2895735E-01
 -0.1721016E+00
  eigenvectors of nos in ao-basis, column    5
 -0.1500498E+00  0.1839577E+00 -0.6368471E+00  0.5784997E+00 -0.4859620E-01  0.8175195E-01  0.3246263E-01  0.4910209E+00
  0.1205511E+00
  eigenvectors of nos in ao-basis, column    6
 -0.3144042E+00  0.4069486E+00 -0.1986096E+00  0.4391918E+00 -0.3433005E+00 -0.1388515E+00  0.9237145E-01 -0.5932927E-01
  0.1328779E+00
  eigenvectors of nos in ao-basis, column    7
  0.3335515E+00 -0.4409331E+00  0.3451806E+00 -0.5611846E+00  0.4124587E+00  0.6298704E-01  0.1219413E+00 -0.2285336E-01
 -0.1754682E+00
  eigenvectors of nos in ao-basis, column    8
 -0.4351867E+00  0.6516739E+00  0.3266903E+00  0.5075014E+00 -0.9044191E+00  0.1246059E+00  0.2802963E-01 -0.8822983E+00
  0.1775997E+00
  eigenvectors of nos in ao-basis, column    9
 -0.8025870E+00  0.1290309E+01 -0.1784983E+01 -0.4017588E+00  0.5369235E+00  0.3004395E-02  0.1098362E-02 -0.6338493E-01
  0.1052870E+01


*****   symmetry block SYM4   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       0.00512932
       2      -0.00053191     0.00069884
       3       0.00024845     0.00007633     0.00024453
       4       0.00018734    -0.00013295     0.00015923     0.00020379
       5      -0.00001131    -0.00015038    -0.00008050    -0.00001200

               Column   5
       5       0.00015384

               eno   1        eno   2        eno   3        eno   4        eno   5
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   0.0052124      0.0007191      0.0003784      0.0000953      0.0000252

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5
  mo    1   0.991016E+00   0.104666E+00  -0.765909E-01   0.160918E-01   0.284215E-01
  mo    2  -0.117199E+00   0.924174E+00  -0.116521E+00   0.266247E+00   0.218408E+00
  mo    3   0.490929E-01   0.206332E+00   0.705184E+00   0.154380E+00  -0.658711E+00
  mo    4   0.417374E-01  -0.130256E+00   0.663842E+00   0.175269E+00   0.714063E+00
  mo    5   0.387552E-03  -0.274604E+00  -0.206366E+00   0.935041E+00  -0.877710E-01
  eigenvectors of nos in ao-basis, column    1
 -0.3112902E+00 -0.7007479E+00  0.4257514E-01  0.9659579E-01  0.6917722E-01
  eigenvectors of nos in ao-basis, column    2
 -0.5341774E+00 -0.1210016E+00 -0.4507240E+00 -0.4914902E+00 -0.5667273E-01
  eigenvectors of nos in ao-basis, column    3
  0.6868861E+00 -0.4186778E+00 -0.7127460E+00  0.3244673E-01  0.3144770E+00
  eigenvectors of nos in ao-basis, column    4
  0.6646427E+00 -0.1167492E+01  0.6201365E+00 -0.8462384E+00  0.1153794E+00
  eigenvectors of nos in ao-basis, column    5
  0.3458361E+00 -0.3826489E+00 -0.2140570E+00  0.3200783E+00 -0.1162866E+01


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***


 cosurf_and_phi
  1.262739748330  2.872870034186  1.567866969040 -0.042610741705
  1.652947872482  0.360731428865  3.496571379926 -0.051766916878
  0.594940552015  0.670555958619  3.845537291721 -0.062137710872
  2.390078334690  1.202722009900  2.566708449184 -0.032963425350
  1.989334673621  2.229216060677  2.001028520754 -0.035852489146
  2.919143888229  0.378144896597  2.099775822683 -0.012451628906
  0.105380882100  1.878884201281  3.371423292662 -0.062434746029
  0.783266721831  2.590884380442  2.520834408126 -0.055817867394
  2.011402526412  2.551496219550  0.814855178005 -0.016929013584
  0.099626804209  1.855073908563 -1.945822518898  0.029996009396
  0.119020599620  3.173161356543  1.415159765853 -0.048498949589
  0.915237473358  3.108118624501  0.463299650838 -0.030337669356
  0.462814589486  2.837210699514 -0.795516582628 -0.006633518398
  1.408719892640  1.640288870679  3.148120355694 -0.053522426094
  2.650053602975  1.633766196698  1.655441764425 -0.014081999348
  1.142904167226  2.885766749848 -0.243475252462 -0.012170480207
  3.656106873706  0.713798214804  0.361832640492  0.038230127227
  2.408046317685  2.075600470298 -1.226192756897  0.039137100058
  1.295820311657  0.567673501678 -2.747601655947  0.056875246432
  3.527730862121  1.045649613724 -1.064853177123  0.054450444439
  2.622898581638  1.024710819001 -2.241122746235  0.058934533146
  3.086808508398  1.794857685487 -0.179703829578  0.034013625250
  1.615872011596  1.674366500124 -2.147504385867  0.046688738392
  0.000006852884  1.035694667087 -2.361676372823  0.048939018722
  0.298815704890  0.969607820141 -2.408807386530  0.050525697797
  0.667723897920  1.245157743773 -2.338577856151  0.048280865624
  1.218123388571  2.025110412626 -1.616576841774  0.034358635851
  2.084186643139  2.293984793080 -0.480493513306  0.021325276035
  2.876642520254  1.658030225134  0.559035126479  0.020822166295
  3.282919570196  0.368088739237  1.091983758387  0.024302406833
  0.467932162408  1.694535407938 -1.941510815499  0.036692013529
  1.299770347024  2.453875604722 -0.850324284820  0.011413374529
  2.242050270258  2.245320705184  0.385739526123  0.003021479020
  2.923104801922  1.151131828431  1.279135167181  0.007222769621
  0.427044967836  0.580963354323 -2.585108185092  0.054263915311
 end_of_phi


 nsubv after electrostatic potential =  3

  Confirmation of dielectric energy for ground state
  edielnew =  -0.00251902677

 =========== Executing IN-CORE method ==========
 norm multnew:  0.000148944814
 threx: total number of loops= 14
 threx: total number of loops= 24
 blstr diagel,allext,nmin2=  1892
 onel.diag.
 -0.194545E+01-0.286130E+01-0.270402E+01-0.267939E+01-0.404309E+01-0.353003E+01-0.366094E+01-0.377999E+01-0.336133E+01-0.260063E+01
 -0.287415E+01-0.357359E+01-0.152440E+01-0.144558E+01-0.152216E+01-0.749910E+00 0.955054E+00-0.155502E+01-0.292594E+01-0.240588E+01
 -0.200637E+01-0.405451E+01-0.340880E+01-0.250541E+01-0.287248E+01-0.299025E+01-0.266613E+01-0.118104E+01-0.155136E+01-0.373927E+00
 -0.277866E+01-0.388024E+01-0.376232E+01-0.283288E+01-0.443810E+01-0.160170E+01-0.133510E+01-0.168075E+01-0.459117E+01-0.280777E+01
 -0.220460E+01-0.135625E+01-0.747718E+00-0.330495E+02-0.787969E+01-0.692041E+01-0.675761E+01-0.704586E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1      1892      1892
 blstr diagel,allext,nmin2=  1892
 onel.diag.
 -0.194545E+01-0.286130E+01-0.270402E+01-0.267939E+01-0.404309E+01-0.353003E+01-0.366094E+01-0.377999E+01-0.336133E+01-0.260063E+01
 -0.287415E+01-0.357359E+01-0.152440E+01-0.144558E+01-0.152216E+01-0.749910E+00 0.955054E+00-0.155502E+01-0.292594E+01-0.240588E+01
 -0.200637E+01-0.405451E+01-0.340880E+01-0.250541E+01-0.287248E+01-0.299025E+01-0.266613E+01-0.118104E+01-0.155136E+01-0.373927E+00
 -0.277866E+01-0.388024E+01-0.376232E+01-0.283288E+01-0.443810E+01-0.160170E+01-0.133510E+01-0.168075E+01-0.459117E+01-0.280777E+01
 -0.220460E+01-0.135625E+01-0.747718E+00-0.330495E+02-0.787969E+01-0.692041E+01-0.675761E+01-0.704586E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1      1892      1892
 blstr diagel,allext,nmin2=  1892
 onel.diag.
 -0.194545E+01-0.286130E+01-0.270402E+01-0.267939E+01-0.404309E+01-0.353003E+01-0.366094E+01-0.377999E+01-0.336133E+01-0.260063E+01
 -0.287415E+01-0.357359E+01-0.152440E+01-0.144558E+01-0.152216E+01-0.749910E+00 0.955054E+00-0.155502E+01-0.292594E+01-0.240588E+01
 -0.200637E+01-0.405451E+01-0.340880E+01-0.250541E+01-0.287248E+01-0.299025E+01-0.266613E+01-0.118104E+01-0.155136E+01-0.373927E+00
 -0.277866E+01-0.388024E+01-0.376232E+01-0.283288E+01-0.443810E+01-0.160170E+01-0.133510E+01-0.168075E+01-0.459117E+01-0.280777E+01
 -0.220460E+01-0.135625E+01-0.747718E+00-0.330495E+02-0.787969E+01-0.692041E+01-0.675761E+01-0.704586E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1      1892      1892


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        12 xx:        36 ww:        79
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        11
task #   9:        28    task #  10:        71    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 matrix-vector product formed for vector   4
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference-subspace vector overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     1.00000000     0.00000000     0.00000000     0.00000000

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.97261913     0.05835614     0.19276438     0.11596759
 subspace has dimension  4

          (h-repnuc*1) matrix in the subspace basis

                x:   1         x:   2         x:   3         x:   4
   x:   1   -85.40875183
   x:   2    -0.31915444    -7.04150281
   x:   3    -0.00086869     0.28014244    -0.25399358
   x:   4     0.00008128    -0.02653816    -0.00113002    -0.01206080

          overlap matrix in the subspace basis

                x:   1         x:   2         x:   3         x:   4
   x:   1     1.00000000
   x:   2     0.00000000     0.08688345
   x:   3     0.00000000    -0.00342125     0.00311503
   x:   4     0.00000000     0.00030861     0.00000546     0.00014894

          eigenvectors and eigenvalues in the subspace basis

                v:   1         v:   2         v:   3         v:   4

   energy   -85.67264029   -82.33605066   -81.01996171   -79.75069370

   x:   1     0.97261913     0.05835614     0.19276438     0.11596759
   x:   2     0.80192075    -0.53624964    -2.67280386    -2.01305062
   x:   3     0.91753922   -14.11471248     5.74135338   -10.13614643
   x:   4     0.86923163   -50.40120214   -25.18847860    59.94114082

          eigenvectors and eigenvalues in the schmidt orthonormalized basis

                v:   1         v:   2         v:   3         v:   4

   energy   -85.67264029   -82.33605066   -81.01996171   -79.75069370

  zx:   1     0.97261913     0.05835614     0.19276438     0.11596759
  zx:   2     0.22663444    -0.04700592    -0.88084658    -0.41296074
  zx:   3     0.05037090    -0.78681139     0.30530842    -0.53402002
  zx:   4     0.01056553    -0.61262782    -0.30616656     0.72858600

          <ref|baseci> overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     1.00000000     0.00000000     0.00000000     0.00000000

  write the transformed vectors back to cosmofile

 rtflw =  0
 freezing =  F

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1    -76.3365604134 -9.3356E+00  6.4519E-05  1.9998E-02  1.0000E-03
 mr-sdci #  4  2    -72.9999707775 -8.5449E+00  0.0000E+00  3.3447E+00  1.0000E-04
 mr-sdci #  4  3    -71.6838818336 -9.0378E+00  0.0000E+00  3.4157E+00  1.0000E-04
 mr-sdci #  4  4    -70.4146138195  7.0415E+01  0.0000E+00  4.7463E+00  1.0000E-04


 total energy in cosmo calc 
e(rootcalc) + repnuc=     -76.3365604134
dielectric energy =      -0.0095031456
deltaediel =       0.0095031456
e(rootcalc) + repnuc - ediel =     -76.3270572678
e(rootcalc) + repnuc - edielnew =     -76.3365604134
deltaelast =      76.3270572678

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.02   0.00   0.00   0.01         0.    0.0000
    2   25    0   0.01   0.00   0.00   0.01         0.    0.0000
    3   26    0   0.02   0.00   0.00   0.02         0.    0.0000
    4   11    0   0.02   0.01   0.00   0.01         0.    0.0000
    5   15    0   0.02   0.00   0.00   0.02         0.    0.0000
    6   16    0   0.02   0.00   0.00   0.02         0.    0.0000
    7    1    0   0.01   0.00   0.00   0.01         0.    0.0000
    8    5    0   0.02   0.00   0.00   0.02         0.    0.0000
    9    6    0   0.03   0.00   0.00   0.03         0.    0.0000
   10    7    0   0.03   0.00   0.00   0.03         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.01   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.03   0.00   0.00   0.03         0.    0.0000
   14   47    0   0.04   0.00   0.00   0.04         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.030000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.2800s 
time spent in multnx:                   0.2500s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0100s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            1.5400s 

          starting ci iteration   5

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.33607988

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=       3  D0X=       9  D0W=      15
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================

          ci-one-electron density block   1

                mo   1         mo   2         mo   3         mo   4         mo   5         mo   6         mo   7         mo   8
   mo   1     1.99954812
   mo   2    -0.00014472     1.98348619
   mo   3    -0.00013434     0.00293830     1.96985698
   mo   4    -0.00015321     0.00089573     0.01194711     0.00115136
   mo   5     0.00021244     0.00425708    -0.00989354    -0.00240169     0.00638232
   mo   6    -0.00019538     0.00440558     0.00448543     0.00024456     0.00060274     0.00281112
   mo   7     0.00035579    -0.00531893    -0.01430676    -0.00095319     0.00050021    -0.00128089     0.00257715
   mo   8    -0.00025557    -0.00678623     0.00443275     0.00265043    -0.00768175    -0.00031138     0.00015607     0.01022380
   mo   9    -0.00031654     0.00578363     0.00843043     0.00017354     0.00123878     0.00286755    -0.00190639    -0.00117955
   mo  10     0.00012700     0.00201328    -0.00483974    -0.00062246     0.00150641     0.00213254     0.00068413    -0.00103236
   mo  11     0.00063789    -0.00064747     0.00024517    -0.00063293    -0.00031670    -0.00139185     0.00254590     0.00143597
   mo  12     0.00059414     0.00196154    -0.00483428    -0.00084210     0.00130830    -0.00134698     0.00128594    -0.00176102
   mo  13    -0.00000828    -0.00127104     0.00147302     0.00025312    -0.00087908     0.00037841     0.00028263     0.00146458
   mo  14    -0.00027368    -0.00401316     0.00528022     0.00085394    -0.00223239    -0.00027746    -0.00034403     0.00297301
   mo  15    -0.00030316     0.00196290    -0.00674191    -0.00013418     0.00085967     0.00088641    -0.00055987    -0.00115200
   mo  16     0.00001272    -0.00012882     0.00057547     0.00001760    -0.00006551    -0.00016184    -0.00000848     0.00003917
   mo  17    -0.00024517     0.00005400    -0.00208170     0.00007324     0.00005002    -0.00001968    -0.00036054    -0.00040508
   mo  18    -0.00005476     0.00016165    -0.00029826     0.00005305    -0.00012588    -0.00016751    -0.00008302     0.00013658
   mo  19    -0.00014571     0.00045081    -0.00056980     0.00009364    -0.00015832     0.00033993    -0.00009831     0.00030071
   mo  20     0.00085743     0.00224259    -0.00028942    -0.00019801     0.00023195    -0.00027635     0.00044881    -0.00024636

                mo   9         mo  10         mo  11         mo  12         mo  13         mo  14         mo  15         mo  16
   mo   9     0.00526215
   mo  10     0.00064173     0.00426779
   mo  11    -0.00069000    -0.00042235     0.00441717
   mo  12    -0.00238534     0.00007679     0.00010164     0.00255226
   mo  13    -0.00064361     0.00133248    -0.00012453    -0.00001546     0.00122381
   mo  14    -0.00068881    -0.00087580    -0.00033181    -0.00015321     0.00035155     0.00158013
   mo  15     0.00155870     0.00056352    -0.00073862    -0.00040532    -0.00028928    -0.00053529     0.00113169
   mo  16    -0.00002536    -0.00040197     0.00013472     0.00000582    -0.00007048     0.00004351    -0.00004130     0.00040243
   mo  17    -0.00018959    -0.00027916    -0.00096174     0.00036987    -0.00013923     0.00004013     0.00007458     0.00000863
   mo  18     0.00037376    -0.00085866     0.00052543    -0.00040959    -0.00061449    -0.00000363    -0.00000291     0.00008924
   mo  19     0.00047492     0.00039408     0.00018280    -0.00063290     0.00024669    -0.00023762     0.00010075    -0.00004516
   mo  20    -0.00040100     0.00009015     0.00054793     0.00048839    -0.00001271    -0.00021986    -0.00014590     0.00000605

                mo  17         mo  18         mo  19         mo  20
   mo  17     0.00061937
   mo  18    -0.00012763     0.00068537
   mo  19    -0.00022646    -0.00000141     0.00044185
   mo  20    -0.00006756    -0.00002381    -0.00007265     0.00038708

          ci-one-electron density block   2

                mo  21         mo  22         mo  23         mo  24         mo  25         mo  26         mo  27         mo  28
   mo  21     1.96858463
   mo  22     0.00173237     0.00061043
   mo  23    -0.01491841    -0.00185168     0.00702963
   mo  24    -0.00216000     0.00073937    -0.00140165     0.00145355
   mo  25     0.00193563     0.00050335    -0.00181011     0.00043212     0.00051161
   mo  26    -0.01443218    -0.00189006     0.00832114    -0.00081321    -0.00209569     0.01094586
   mo  27    -0.00340258    -0.00124548     0.00282950    -0.00223194    -0.00084554     0.00221924     0.00380469
   mo  28     0.00150438     0.00015139    -0.00050118     0.00026096     0.00004002    -0.00065685    -0.00038212     0.00056889
   mo  29     0.00926166     0.00035690    -0.00226987    -0.00025821     0.00059080    -0.00361459     0.00023870     0.00025716
   mo  30     0.00546436     0.00057550    -0.00169367     0.00088010     0.00046192    -0.00187684    -0.00187179     0.00018131
   mo  31    -0.00561910    -0.00022727     0.00061979    -0.00039153    -0.00021611     0.00061951     0.00079958    -0.00013158
   mo  32     0.00093910     0.00003616    -0.00008382     0.00009743    -0.00002467    -0.00007108    -0.00014051     0.00026715
   mo  33    -0.00389404    -0.00000767     0.00029985     0.00016094    -0.00007948     0.00073015    -0.00029547    -0.00005002
   mo  34    -0.00136092    -0.00003877    -0.00008220    -0.00019877     0.00000825    -0.00030115     0.00044992     0.00007420

                mo  29         mo  30         mo  31         mo  32         mo  33         mo  34
   mo  29     0.00208276
   mo  30     0.00003706     0.00149653
   mo  31    -0.00024263    -0.00042825     0.00060363
   mo  32     0.00001819     0.00007529    -0.00002381     0.00033135
   mo  33    -0.00074767     0.00008369     0.00000469    -0.00000788     0.00054849
   mo  34     0.00043802    -0.00043464     0.00004817     0.00000958    -0.00020538     0.00035281

          ci-one-electron density block   3

                mo  35         mo  36         mo  37         mo  38         mo  39         mo  40         mo  41         mo  42
   mo  35     1.97154623
   mo  36     0.01236793     0.00545409
   mo  37     0.01248124     0.00598757     0.00845891
   mo  38     0.00454844    -0.00319318    -0.00177766     0.00464481
   mo  39    -0.00018388     0.00028702     0.00148089     0.00132367     0.00141399
   mo  40    -0.00785669     0.00208162     0.00306824    -0.00112441     0.00048328     0.00189932
   mo  41     0.00012114     0.00001920    -0.00002211    -0.00004810    -0.00003718    -0.00000363     0.00038929
   mo  42    -0.00085305     0.00018670     0.00004059    -0.00050879    -0.00004178     0.00006582     0.00000538     0.00038649
   mo  43     0.00015259    -0.00023700     0.00023766     0.00112523     0.00078509    -0.00000243     0.00000561    -0.00014765

                mo  43
   mo  43     0.00077662

          ci-one-electron density block   4

                mo  44         mo  45         mo  46         mo  47         mo  48
   mo  44     0.00571719
   mo  45    -0.00063895     0.00076709
   mo  46     0.00027164     0.00003381     0.00024352
   mo  47     0.00015096    -0.00015078     0.00016428     0.00021545
   mo  48    -0.00004628    -0.00013518    -0.00007143    -0.00001066     0.00015401


*****   symmetry block SYM1   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       1.99954812
       2      -0.00014472     1.98348619
       3      -0.00013434     0.00293830     1.96985698
       4      -0.00015321     0.00089573     0.01194711     0.00115136
       5       0.00021244     0.00425708    -0.00989354    -0.00240169
       6      -0.00019538     0.00440558     0.00448543     0.00024456
       7       0.00035579    -0.00531893    -0.01430676    -0.00095319
       8      -0.00025557    -0.00678623     0.00443275     0.00265043
       9      -0.00031654     0.00578363     0.00843043     0.00017354
      10       0.00012700     0.00201328    -0.00483974    -0.00062246
      11       0.00063789    -0.00064747     0.00024517    -0.00063293
      12       0.00059414     0.00196154    -0.00483428    -0.00084210
      13      -0.00000828    -0.00127104     0.00147302     0.00025312
      14      -0.00027368    -0.00401316     0.00528022     0.00085394
      15      -0.00030316     0.00196290    -0.00674191    -0.00013418
      16       0.00001272    -0.00012882     0.00057547     0.00001760
      17      -0.00024517     0.00005400    -0.00208170     0.00007324
      18      -0.00005476     0.00016165    -0.00029826     0.00005305
      19      -0.00014571     0.00045081    -0.00056980     0.00009364
      20       0.00085743     0.00224259    -0.00028942    -0.00019801

               Column   5     Column   6     Column   7     Column   8
       5       0.00638232
       6       0.00060274     0.00281112
       7       0.00050021    -0.00128089     0.00257715
       8      -0.00768175    -0.00031138     0.00015607     0.01022380
       9       0.00123878     0.00286755    -0.00190639    -0.00117955
      10       0.00150641     0.00213254     0.00068413    -0.00103236
      11      -0.00031670    -0.00139185     0.00254590     0.00143597
      12       0.00130830    -0.00134698     0.00128594    -0.00176102
      13      -0.00087908     0.00037841     0.00028263     0.00146458
      14      -0.00223239    -0.00027746    -0.00034403     0.00297301
      15       0.00085967     0.00088641    -0.00055987    -0.00115200
      16      -0.00006551    -0.00016184    -0.00000848     0.00003917
      17       0.00005002    -0.00001968    -0.00036054    -0.00040508
      18      -0.00012588    -0.00016751    -0.00008302     0.00013658
      19      -0.00015832     0.00033993    -0.00009831     0.00030071
      20       0.00023195    -0.00027635     0.00044881    -0.00024636

               Column   9     Column  10     Column  11     Column  12
       9       0.00526215
      10       0.00064173     0.00426779
      11      -0.00069000    -0.00042235     0.00441717
      12      -0.00238534     0.00007679     0.00010164     0.00255226
      13      -0.00064361     0.00133248    -0.00012453    -0.00001546
      14      -0.00068881    -0.00087580    -0.00033181    -0.00015321
      15       0.00155870     0.00056352    -0.00073862    -0.00040532
      16      -0.00002536    -0.00040197     0.00013472     0.00000582
      17      -0.00018959    -0.00027916    -0.00096174     0.00036987
      18       0.00037376    -0.00085866     0.00052543    -0.00040959
      19       0.00047492     0.00039408     0.00018280    -0.00063290
      20      -0.00040100     0.00009015     0.00054793     0.00048839

               Column  13     Column  14     Column  15     Column  16
      13       0.00122381
      14       0.00035155     0.00158013
      15      -0.00028928    -0.00053529     0.00113169
      16      -0.00007048     0.00004351    -0.00004130     0.00040243
      17      -0.00013923     0.00004013     0.00007458     0.00000863
      18      -0.00061449    -0.00000363    -0.00000291     0.00008924
      19       0.00024669    -0.00023762     0.00010075    -0.00004516
      20      -0.00001271    -0.00021986    -0.00014590     0.00000605

               Column  17     Column  18     Column  19     Column  20
      17       0.00061937
      18      -0.00012763     0.00068537
      19      -0.00022646    -0.00000141     0.00044185
      20      -0.00006756    -0.00002381    -0.00007265     0.00038708

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7        eno   8
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    8   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno   9        eno  10        eno  11        eno  12        eno  13        eno  14        eno  15        eno  16
 emo    9   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   10   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   11   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   12   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   13   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   14   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   15   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   16   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno  17        eno  18        eno  19        eno  20
 emo   17   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   18   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   19   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   20   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   1.9995515      1.9841985      1.9695829      0.0190836      0.0108525
   0.0061411      0.0053595      0.0011514      0.0008553      0.0005819
   0.0004773      0.0003732      0.0003595      0.0001611      0.0001109
   0.0000701      0.0000571      0.0000320      0.0000082      0.0000004

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7         no   8
  mo    1   0.999931E+00   0.110978E-01   0.366313E-02   0.163568E-03   0.515106E-03  -0.235769E-03   0.666000E-04  -0.961208E-04
  mo    2  -0.101037E-01   0.978418E+00  -0.206276E+00   0.515100E-02  -0.308961E-02   0.110635E-03   0.495672E-03  -0.531780E-03
  mo    3  -0.586869E-02   0.206205E+00   0.978400E+00  -0.642520E-02  -0.754845E-02   0.371157E-02   0.166954E-02   0.110043E-02
  mo    4  -0.117056E-03   0.167976E-02   0.586197E-02   0.192964E+00   0.143387E+00  -0.112420E+00   0.544259E-01  -0.809458E-01
  mo    5   0.114914E-03   0.108984E-02  -0.540445E-02  -0.562125E+00  -0.855562E-01   0.681141E-01  -0.631582E-01   0.781281E-01
  mo    6  -0.134217E-03   0.265083E-02   0.177399E-02  -0.841526E-01   0.434490E+00   0.240516E+00   0.108026E+00   0.209440E+00
  mo    7   0.248285E-03  -0.411858E-02  -0.656811E-02   0.713924E-02  -0.368111E+00   0.365648E+00  -0.794706E-01   0.190610E+00
  mo    8  -0.107734E-03  -0.291065E-02   0.296506E-02   0.715337E+00   0.106107E+00   0.197473E+00   0.191175E-03   0.202017E+00
  mo    9  -0.213742E-03   0.374700E-02   0.359359E-02  -0.148059E+00   0.569150E+00   0.941685E-01  -0.432149E+00   0.345801E+00
  mo   10   0.677280E-04   0.496593E-03  -0.262907E-02  -0.143312E+00   0.150195E+00   0.646469E+00   0.430015E+00  -0.315867E-01
  mo   11   0.322920E-03  -0.301653E-03   0.182881E-03   0.941755E-01  -0.332707E+00   0.438617E+00  -0.585053E+00   0.315042E-01
  mo   12   0.302671E-03   0.462616E-03  -0.262754E-02  -0.100673E+00  -0.360654E+00  -0.617704E-01   0.282384E+00   0.572517E+00
  mo   13  -0.208633E-05  -0.477665E-03   0.868339E-03   0.862394E-01   0.176697E-01   0.245507E+00   0.286114E+00  -0.198785E+00
  mo   14  -0.132582E-03  -0.143917E-02   0.306054E-02   0.217647E+00   0.172198E-01  -0.124128E+00   0.107572E+00   0.363499E+00
  mo   15  -0.142156E-03   0.273610E-03  -0.355752E-02  -0.102418E+00   0.184451E+00   0.588921E-02  -0.414117E-01   0.187663E+00
  mo   16   0.533967E-05  -0.410800E-05   0.300219E-03   0.852209E-02  -0.183867E-01  -0.485543E-01  -0.569460E-01   0.262585E-01
  mo   17  -0.116927E-03  -0.189976E-03  -0.104050E-02  -0.197854E-01   0.137376E-01  -0.168326E+00   0.138772E+00   0.200848E+00
  mo   18  -0.273956E-04   0.486131E-04  -0.162179E-03   0.164977E-01   0.357051E-02  -0.840008E-01  -0.246741E+00  -0.914230E-02
  mo   19  -0.736491E-04   0.163443E-03  -0.327925E-03   0.116621E-01   0.735645E-01   0.110074E+00  -0.506240E-01  -0.398640E+00
  mo   20   0.418733E-03   0.107922E-02  -0.381949E-03  -0.154996E-01  -0.931144E-01   0.599755E-01  -0.165554E-01   0.100492E+00

                no   9         no  10         no  11         no  12         no  13         no  14         no  15         no  16
  mo    1  -0.513633E-04   0.264625E-04  -0.309015E-03   0.140083E-03   0.221479E-03   0.314770E-04  -0.956626E-04   0.443993E-04
  mo    2  -0.989968E-03   0.132252E-03  -0.211965E-02   0.844313E-03   0.143043E-02  -0.256433E-03   0.340713E-03  -0.623359E-03
  mo    3   0.289936E-02  -0.543016E-03  -0.448289E-02   0.217162E-02   0.216319E-02   0.200464E-02   0.226085E-02  -0.201562E-02
  mo    4   0.140152E+00  -0.412891E-01   0.146894E+00  -0.677568E-01  -0.130875E+00  -0.108607E+00  -0.161645E+00   0.119943E+00
  mo    5  -0.386950E+00   0.534879E-01  -0.127370E+00   0.230498E-01   0.337355E-01   0.184789E+00   0.282374E+00  -0.360405E-01
  mo    6  -0.161514E+00  -0.314663E+00   0.254058E+00   0.128106E+00  -0.192620E-01   0.796686E-01  -0.291290E-01  -0.180119E-01
  mo    7   0.103763E+00  -0.236412E-01  -0.202117E+00   0.174260E+00   0.293556E+00   0.506954E-01  -0.709570E-01  -0.257269E+00
  mo    8  -0.289944E-01  -0.109388E-02  -0.603199E-01   0.269975E-01   0.517394E-01   0.189727E+00   0.154238E+00  -0.916363E-01
  mo    9   0.305044E-02   0.297881E+00   0.118642E+00  -0.130892E+00   0.858257E-01   0.543964E-01  -0.206300E+00   0.391125E-02
  mo   10   0.976956E-01  -0.228895E+00  -0.878975E-01   0.151684E-01  -0.525835E-01  -0.184800E+00  -0.603940E-01  -0.325948E-01
  mo   11  -0.308649E-01   0.569346E-01   0.135414E+00  -0.557543E-01  -0.151387E-01  -0.123394E+00   0.502335E-02   0.237573E+00
  mo   12   0.153437E+00   0.849799E-01   0.188923E+00  -0.122662E+00  -0.239108E+00   0.324676E+00  -0.410411E+00   0.111999E+00
  mo   13  -0.200029E+00   0.493283E+00   0.976786E-01   0.591819E-01  -0.311493E-01   0.256612E+00   0.174849E+00   0.576401E+00
  mo   14  -0.563379E+00   0.478326E-01  -0.342419E+00  -0.517822E-01  -0.102399E+00  -0.243591E-01   0.137910E+00  -0.157685E+00
  mo   15   0.574152E+00   0.193291E+00  -0.546235E+00   0.111649E+00  -0.242371E+00   0.621415E-01   0.307902E+00   0.103538E+00
  mo   16  -0.551016E-01   0.195959E+00   0.205864E+00   0.871522E+00  -0.293013E+00  -0.112669E+00  -0.651534E-01  -0.160022E+00
  mo   17   0.209923E+00  -0.188505E-01   0.251404E+00   0.209510E+00   0.712763E+00   0.138878E+00   0.290736E+00   0.817758E-01
  mo   18  -0.245157E-01  -0.633246E+00  -0.105600E+00   0.193105E+00  -0.163718E+00   0.433678E+00   0.587116E-01   0.389487E+00
  mo   19   0.542965E-01   0.141684E+00  -0.977606E-02  -0.354045E-01  -0.169049E-01   0.678298E+00  -0.165141E+00  -0.456422E+00
  mo   20   0.145477E+00  -0.382613E-01   0.485091E+00  -0.226300E+00  -0.365779E+00   0.306480E-01   0.621211E+00  -0.281425E+00

                no  17         no  18         no  19         no  20
  mo    1  -0.494748E-04  -0.209113E-04  -0.824575E-05   0.795046E-06
  mo    2   0.784221E-03   0.781404E-03  -0.323120E-04  -0.120848E-03
  mo    3   0.562846E-03   0.207835E-02   0.203058E-02  -0.122490E-02
  mo    4  -0.582072E-01  -0.130887E+00  -0.254820E+00   0.847117E+00
  mo    5  -0.177066E+00  -0.101444E+00   0.386838E+00   0.430094E+00
  mo    6  -0.509453E+00   0.412260E+00  -0.208560E+00  -0.616388E-01
  mo    7   0.301695E+00   0.486142E+00  -0.213133E+00   0.269716E+00
  mo    8  -0.989853E-01  -0.238284E-01   0.546402E+00   0.110543E+00
  mo    9   0.401688E+00  -0.532139E-01   0.429623E-01   0.625027E-02
  mo   10   0.216456E+00  -0.438709E+00   0.372896E-01  -0.288994E-01
  mo   11  -0.349855E+00  -0.289047E+00  -0.192814E+00  -0.639273E-01
  mo   12  -0.854398E-01  -0.911343E-01   0.801606E-01  -0.367783E-01
  mo   13   0.152353E+00   0.207422E+00  -0.135458E+00  -0.894257E-03
  mo   14   0.578104E-01  -0.247042E+00  -0.482351E+00  -0.399799E-01
  mo   15  -0.241252E+00   0.154317E-01  -0.118641E+00  -0.367896E-02
  mo   16   0.596494E-01  -0.127665E+00   0.409112E-01   0.762419E-03
  mo   17  -0.529115E-01  -0.331913E+00  -0.168610E+00  -0.617513E-02
  mo   18   0.326351E+00  -0.584932E-01  -0.407663E-01  -0.609447E-02
  mo   19  -0.123971E+00  -0.202817E+00  -0.208466E+00  -0.236513E-01
  mo   20   0.232391E+00   0.570028E-01  -0.103880E+00   0.909678E-02
  eigenvectors of nos in ao-basis, column    1
  0.9975145E+00 -0.1526223E-01  0.3337662E-02  0.4254081E-02 -0.8020285E-02  0.3758219E-02  0.4289347E-03  0.1020036E-04
  0.2110474E-05  0.1252992E-04 -0.2584451E-03  0.2195347E-04  0.5526734E-04  0.6455400E-03 -0.1386065E-02  0.4818499E-04
  0.4617890E-03  0.2304814E-03 -0.5347122E-03 -0.5371108E-03
  eigenvectors of nos in ao-basis, column    2
  0.3270711E-02  0.9120348E+00 -0.2882973E-02 -0.1329888E+00  0.4026591E-01  0.1021121E-01  0.7439829E-01  0.1643659E-03
  0.2563360E-02 -0.1549971E-02 -0.2562205E-02 -0.7769186E-03 -0.1761526E-02  0.2590479E+00 -0.1127694E+00  0.2951505E-02
  0.2625979E-01  0.1920322E-01  0.1592616E-01  0.1157933E-01
  eigenvectors of nos in ao-basis, column    3
  0.9156715E-02  0.1148203E+00  0.3631541E-02  0.1549635E+00  0.8357311E+00 -0.2731661E-01 -0.2829289E-01 -0.1609061E-02
 -0.2665103E-02 -0.5442271E-02  0.1913841E-02 -0.9244444E-04  0.2660277E-02 -0.3965833E+00  0.1680465E+00  0.5774128E-02
 -0.2983933E-01 -0.1289748E-02 -0.1615888E-02  0.2905821E-01
  eigenvectors of nos in ao-basis, column    4
 -0.4360921E-01 -0.6094304E+00 -0.6162982E-01 -0.7356084E-03  0.1321193E+01 -0.1999471E+00 -0.6916865E+00  0.3782486E-02
  0.1640755E-01  0.9071365E-02  0.2721096E-01  0.2209583E-02  0.3544857E-02  0.9623188E+00 -0.4207919E+00 -0.8168635E-02
  0.6088722E-01  0.1132355E-01  0.3561039E-02 -0.3736764E-01
  eigenvectors of nos in ao-basis, column    5
  0.1848063E-01 -0.1058907E+01 -0.4417447E+00  0.1096864E+01 -0.8290077E+00  0.3882788E+00  0.8226377E+00 -0.5530981E-02
  0.3421573E-01 -0.2439428E-01  0.4601725E-01 -0.2611263E-02 -0.3303202E-02  0.4461541E+00 -0.2606215E+00  0.1968632E-01
  0.2560441E-01  0.8043690E-01  0.1085356E-01  0.5487274E-01
  eigenvectors of nos in ao-basis, column    6
 -0.1232055E-01  0.5006823E+00  0.2646869E+00 -0.9705507E+00 -0.5760650E-01  0.2143823E-01  0.1271517E+00 -0.3596435E-01
  0.1135165E+00 -0.8948190E-01  0.2501501E+00 -0.1450558E-01 -0.2078360E-01  0.2988500E+00 -0.1384526E+00 -0.1170179E-01
 -0.3406263E-01  0.8148201E-01 -0.3274799E-01  0.2356555E-01
  eigenvectors of nos in ao-basis, column    7
 -0.1404340E-02 -0.1483842E+00 -0.6327134E-01  0.2344442E+00  0.2789897E+00 -0.1190024E+00 -0.4007126E+00 -0.8635618E-01
 -0.8587382E-01 -0.1756920E+00 -0.1677032E+00 -0.9473052E-02  0.1399169E-01 -0.7463810E-01  0.5759092E-01  0.5592969E-02
 -0.5039204E-01  0.3954160E-01 -0.3379587E-02  0.3852214E-01
  eigenvectors of nos in ao-basis, column    8
 -0.1235731E+00  0.2325385E-01  0.4310346E+00 -0.1932664E+01 -0.1966548E+00  0.2847792E+00  0.9718152E+00  0.2040246E-01
 -0.2443032E+00  0.1500057E-01 -0.2165500E+00 -0.3402276E-01 -0.8836563E-01  0.2117336E+01 -0.1011862E+01 -0.1279054E-02
  0.7316451E-01  0.1652121E+00  0.7126388E-01  0.7109663E-01
  eigenvectors of nos in ao-basis, column    9
 -0.2207193E+00 -0.2829779E+00  0.4200188E+00  0.2679753E+00 -0.2529395E+01  0.2375467E+01  0.4564418E+00 -0.2018205E-01
 -0.4707725E-01  0.1302102E-01  0.3620937E-01  0.8205711E-02  0.3980987E-01 -0.1999270E-01  0.1264763E+00 -0.3883600E-01
 -0.1713289E+00 -0.9715451E-01 -0.5483526E-01 -0.1431057E+00
  eigenvectors of nos in ao-basis, column   10
  0.1327243E+00  0.3036876E+00 -0.1506982E+00 -0.6222587E+00 -0.1603631E+00  0.2010132E+00  0.8275587E+00 -0.2108733E+00
  0.3040708E-01  0.1314213E-01  0.1785254E-01  0.9110438E-01 -0.1167147E+00  0.6552160E+00 -0.3512917E+00 -0.1019590E-01
  0.3286293E+00 -0.2916903E+00  0.9481773E-01 -0.1712812E+00
  eigenvectors of nos in ao-basis, column   11
 -0.1358406E+01 -0.2857575E+01  0.1965251E+01  0.1957837E+01  0.8265942E+00 -0.1102967E+01  0.4193793E-01 -0.2991576E-01
 -0.1119316E+00  0.1747383E-01  0.1432326E+00  0.1451654E+00  0.1731207E+00 -0.2313707E+00  0.2011953E+00 -0.3684617E-02
 -0.1545688E+00 -0.1896652E+00 -0.3568112E-01  0.8797631E-01
  eigenvectors of nos in ao-basis, column   12
  0.7223348E+00  0.1593300E+01 -0.1014516E+01 -0.7623309E+00 -0.2315989E+00  0.2818965E+00 -0.3225896E+00  0.9552470E-01
 -0.1006191E+00 -0.5498730E-01  0.7307394E-01  0.3150892E+00 -0.1285912E+00 -0.1915308E+00  0.5243337E-01 -0.9559775E-02
 -0.4405121E-01  0.3068632E-01 -0.1296353E+00  0.7233869E-01
  eigenvectors of nos in ao-basis, column   13
  0.1207036E+01  0.2682967E+01 -0.1682531E+01 -0.1005865E+01  0.1780836E+00 -0.3028937E+00 -0.5969644E-01 -0.8288603E-01
 -0.2757371E+00  0.7291717E-01  0.2443221E+00 -0.2034585E-01  0.2917414E+00 -0.2109644E+00 -0.8813370E-01 -0.2318599E-01
 -0.1002884E+00 -0.6071984E-01 -0.1649654E+00 -0.1204132E+00
  eigenvectors of nos in ao-basis, column   14
  0.3263710E+00  0.9113484E+00 -0.2086211E+00 -0.3216109E+01 -0.7994076E+00  0.1262368E+01  0.1062122E+01  0.9100009E-01
  0.3100418E+00 -0.1706456E+00 -0.4908524E+00  0.6530414E-01  0.3253665E+00  0.2740431E+01 -0.1258699E+01  0.2348860E-01
  0.4389892E+00 -0.1179797E+00  0.1834023E+00  0.2433672E+00
  eigenvectors of nos in ao-basis, column   15
 -0.2190822E+01 -0.5818456E+01  0.2441969E+01  0.3587193E+01 -0.1241007E+01  0.2419379E+01 -0.3112519E+00  0.2109356E-01
 -0.1654560E+00 -0.7420563E-01 -0.4256206E-01  0.1509753E-01  0.1222313E+00  0.1641474E+01 -0.1540561E+01  0.1172728E+00
  0.1027474E+01  0.4924910E+00 -0.1639012E+00  0.2807037E-01
  eigenvectors of nos in ao-basis, column   16
  0.1045287E+01  0.2832150E+01 -0.1097851E+01 -0.3239450E+01  0.4712441E-03 -0.1746122E+00  0.4935820E+00  0.1572475E+00
 -0.2262527E+00 -0.3188213E+00  0.4940758E+00 -0.1361137E+00 -0.8329684E-01 -0.7760034E+00  0.1265874E+01 -0.6735172E-01
  0.2799424E+00 -0.8616151E+00  0.3624174E+00  0.3471455E+00
  eigenvectors of nos in ao-basis, column   17
 -0.1042815E+01 -0.2847949E+01  0.1031280E+01  0.5035096E+01  0.5924970E+00 -0.8973180E+00  0.8936331E+00  0.1256140E+00
 -0.1999372E-01 -0.1786517E+00  0.2907107E-01  0.7192546E-02  0.3063388E-01  0.1250868E-01 -0.1066200E+01 -0.2969771E-01
  0.5594215E-01 -0.2059556E-01 -0.4694408E+00 -0.1211466E+01
  eigenvectors of nos in ao-basis, column   18
 -0.6357126E+00 -0.1809675E+01  0.4492458E+00  0.6628464E+01  0.2173097E+00 -0.5829395E+00 -0.1365853E+01 -0.7187517E-03
  0.1009538E-01 -0.4707590E-01  0.2770334E+00 -0.1181074E+00 -0.1717400E+00 -0.2169178E+01 -0.2833161E+00  0.1043054E-01
 -0.1904212E+00 -0.7439574E+00 -0.1300376E+01 -0.2663573E+00
  eigenvectors of nos in ao-basis, column   19
  0.1753452E+00  0.5042599E+00 -0.2982032E+00  0.3301731E+01  0.3606413E+00 -0.1189137E+01 -0.1279788E+01  0.6553539E-02
 -0.5787652E-01  0.2703998E-01  0.4282238E+00 -0.4271795E-01 -0.1540214E+00 -0.2368314E+01 -0.2019678E+00  0.2826760E+00
 -0.1056715E+01 -0.6474974E+00  0.3804620E+00  0.3737403E-01
  eigenvectors of nos in ao-basis, column   20
 -0.1474073E+00 -0.4566949E+00  0.5929617E-01  0.2295899E+01  0.7416319E-02 -0.5443215E-01 -0.5669669E+00 -0.4280898E-03
 -0.1191862E-01  0.3183593E-02  0.6153411E-01 -0.4906477E-02 -0.1438097E-01 -0.2981041E+00 -0.1234406E+01  0.8885808E+00
 -0.1191476E+00 -0.8777783E-01 -0.3016982E+00 -0.2394919E+00


*****   symmetry block SYM2   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       1.96858463
       2       0.00173237     0.00061043
       3      -0.01491841    -0.00185168     0.00702963
       4      -0.00216000     0.00073937    -0.00140165     0.00145355
       5       0.00193563     0.00050335    -0.00181011     0.00043212
       6      -0.01443218    -0.00189006     0.00832114    -0.00081321
       7      -0.00340258    -0.00124548     0.00282950    -0.00223194
       8       0.00150438     0.00015139    -0.00050118     0.00026096
       9       0.00926166     0.00035690    -0.00226987    -0.00025821
      10       0.00546436     0.00057550    -0.00169367     0.00088010
      11      -0.00561910    -0.00022727     0.00061979    -0.00039153
      12       0.00093910     0.00003616    -0.00008382     0.00009743
      13      -0.00389404    -0.00000767     0.00029985     0.00016094
      14      -0.00136092    -0.00003877    -0.00008220    -0.00019877

               Column   5     Column   6     Column   7     Column   8
       5       0.00051161
       6      -0.00209569     0.01094586
       7      -0.00084554     0.00221924     0.00380469
       8       0.00004002    -0.00065685    -0.00038212     0.00056889
       9       0.00059080    -0.00361459     0.00023870     0.00025716
      10       0.00046192    -0.00187684    -0.00187179     0.00018131
      11      -0.00021611     0.00061951     0.00079958    -0.00013158
      12      -0.00002467    -0.00007108    -0.00014051     0.00026715
      13      -0.00007948     0.00073015    -0.00029547    -0.00005002
      14       0.00000825    -0.00030115     0.00044992     0.00007420

               Column   9     Column  10     Column  11     Column  12
       9       0.00208276
      10       0.00003706     0.00149653
      11      -0.00024263    -0.00042825     0.00060363
      12       0.00001819     0.00007529    -0.00002381     0.00033135
      13      -0.00074767     0.00008369     0.00000469    -0.00000788
      14       0.00043802    -0.00043464     0.00004817     0.00000958

               Column  13     Column  14
      13       0.00054849
      14      -0.00020538     0.00035281

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7        eno   8
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    8   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno   9        eno  10        eno  11        eno  12        eno  13        eno  14
 emo    9   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   10   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   11   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   12   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   13   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   14   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   1.9689031      0.0207739      0.0058704      0.0010851      0.0008404
   0.0006570      0.0003957      0.0001676      0.0001010      0.0000966
   0.0000184      0.0000102      0.0000052      0.0000002

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7         no   8
  mo    1   0.999918E+00   0.117891E-01  -0.191397E-02  -0.150274E-02  -0.237961E-02   0.734775E-03   0.120461E-02  -0.161472E-02
  mo    2   0.897440E-03  -0.148342E+00  -0.132764E+00   0.194279E-01  -0.218933E+00  -0.382332E-01   0.643500E-01   0.749107E-01
  mo    3  -0.764782E-02   0.569054E+00   0.233601E-01   0.208359E+00   0.361861E+00  -0.402552E-01   0.658326E-01  -0.238369E+00
  mo    4  -0.108548E-02  -0.113866E+00  -0.415574E+00   0.257585E+00  -0.283682E+00  -0.427255E-01   0.189435E+00  -0.109344E+00
  mo    5   0.100143E-02  -0.148431E+00  -0.285548E-01  -0.309198E-01  -0.100761E+00  -0.192958E+00  -0.560375E-01   0.158625E+00
  mo    6  -0.741964E-02   0.701312E+00  -0.304742E+00   0.145470E+00  -0.194926E+00  -0.104737E+00   0.882088E-03   0.192872E+00
  mo    7  -0.175382E-02   0.234429E+00   0.663924E+00  -0.203730E+00   0.113446E-01   0.112273E+00  -0.519074E-01   0.886328E-01
  mo    8   0.770095E-03  -0.479179E-01  -0.308435E-01   0.370270E+00   0.459325E-01   0.699880E+00  -0.834384E-01  -0.400349E+00
  mo    9   0.473253E-02  -0.205146E+00   0.358683E+00   0.548803E+00   0.118615E+00  -0.239848E+00   0.204611E-01  -0.768793E-02
  mo   10   0.279343E-02  -0.152669E+00  -0.296268E+00  -0.136413E+00   0.724463E+00   0.129349E-01   0.187590E+00  -0.154760E-01
  mo   11  -0.286129E-02   0.565255E-01   0.117179E+00  -0.312937E+00  -0.220387E+00   0.228362E+00   0.785396E+00  -0.172018E+00
  mo   12   0.477973E-03  -0.792189E-02  -0.260382E-01   0.182833E+00   0.372602E-01   0.525777E+00   0.364908E-01   0.758056E+00
  mo   13  -0.198358E-02   0.349657E-01  -0.148103E+00  -0.408747E+00  -0.119816E+00   0.241173E+00  -0.536063E+00  -0.211143E+00
  mo   14  -0.689521E-03  -0.847045E-02   0.144644E+00   0.266288E+00  -0.295001E+00   0.165885E-02  -0.411137E-01  -0.214462E+00

                no   9         no  10         no  11         no  12         no  13         no  14
  mo    1   0.367913E-03   0.162588E-02   0.104547E-02   0.129782E-02   0.155397E-02   0.293479E-03
  mo    2  -0.637106E-01  -0.113726E+00   0.327814E+00  -0.137427E+00  -0.220543E-01   0.870068E+00
  mo    3   0.437411E-02   0.363370E+00   0.106979E+00   0.355681E+00   0.307012E+00   0.272351E+00
  mo    4  -0.183031E+00   0.165213E-01   0.619172E+00   0.204264E+00   0.134649E+00  -0.375283E+00
  mo    5  -0.904746E-01   0.116576E+00  -0.173102E+00  -0.330719E+00   0.859392E+00  -0.289421E-01
  mo    6  -0.114254E+00  -0.165925E+00  -0.834974E-01  -0.478484E+00  -0.158792E+00  -0.786904E-01
  mo    7  -0.650983E-01  -0.271312E+00   0.552454E+00  -0.149094E+00   0.141877E+00  -0.118506E+00
  mo    8  -0.131616E+00  -0.319826E+00  -0.145662E+00  -0.167896E+00   0.172031E+00   0.430886E-01
  mo    9  -0.453819E+00   0.331087E+00  -0.379852E-03  -0.284299E+00  -0.243641E+00  -0.149405E-01
  mo   10   0.170508E+00   0.703556E-02   0.273445E+00  -0.448109E+00  -0.405576E-01  -0.592995E-01
  mo   11  -0.142988E+00   0.278933E+00  -0.133740E+00  -0.145640E+00  -0.251262E-01  -0.145271E-02
  mo   12   0.800519E-01   0.303436E+00   0.588185E-01   0.100023E+00   0.664208E-02  -0.556306E-02
  mo   13  -0.256439E+00   0.522439E+00   0.152806E+00  -0.178914E+00  -0.105681E+00  -0.823458E-02
  mo   14   0.769404E+00   0.291970E+00   0.122033E+00  -0.281576E+00  -0.279360E-01  -0.346285E-01
  eigenvectors of nos in ao-basis, column    1
  0.7599090E+00 -0.3286883E-01 -0.1376117E+00 -0.1586271E-01 -0.8327638E-02  0.2811824E-03 -0.3371608E-02 -0.5505237E+00
  0.1519683E+00 -0.9749381E-02 -0.2201037E-01 -0.3011191E-01 -0.2069828E-01 -0.2460881E-01
  eigenvectors of nos in ao-basis, column    2
 -0.1326172E+01  0.1064816E+00  0.5270870E+00 -0.4051104E-01 -0.6214182E-01  0.4364486E-04  0.3357202E-02 -0.1037283E+01
  0.4087334E+00 -0.1406654E-01 -0.5500060E-01 -0.3344838E-01 -0.7814907E-02 -0.1599636E-03
  eigenvectors of nos in ao-basis, column    3
 -0.7460770E+00  0.3315031E+00  0.8699890E+00  0.2834164E+00  0.5410788E+00 -0.8377043E-02  0.1194336E-01  0.2288427E+00
 -0.8462916E-01 -0.3324573E-01  0.8878963E-01 -0.1463851E-01  0.3552389E-01  0.3198651E-01
  eigenvectors of nos in ao-basis, column    4
  0.1292496E+01 -0.1124886E+01 -0.1326861E+01  0.5231313E+00  0.4225435E+00  0.2625496E-01  0.1067494E-01 -0.1475336E+01
  0.3503221E+00  0.3894042E-01 -0.1234714E+00  0.1950921E+00 -0.1255264E+00 -0.1036230E+00
  eigenvectors of nos in ao-basis, column    5
  0.2062233E+01 -0.1822685E+01  0.3651917E+00 -0.2229275E+00 -0.2923412E+00 -0.2050290E-01  0.8151243E-01  0.1067817E+01
 -0.6019982E+00  0.4056175E-01  0.2044830E+00  0.1774468E+00  0.1175177E+00  0.2043318E+00
  eigenvectors of nos in ao-basis, column    6
 -0.7223761E+00  0.7736605E+00  0.1181180E+01 -0.1588865E+00 -0.1453920E+00  0.7252320E-01  0.2307284E+00  0.1061885E+01
 -0.6987468E-01 -0.4753169E-01 -0.2301704E+00  0.3960328E+00  0.1518711E+00  0.1311458E+00
  eigenvectors of nos in ao-basis, column    7
 -0.6879478E+00  0.9938644E+00 -0.2265600E-01  0.3646742E+00 -0.4315787E+00 -0.8170835E-01  0.4219470E+00  0.3212942E+00
 -0.5059852E+00  0.8045988E-01  0.1612825E+00  0.1187183E+00 -0.2014083E+00  0.9010857E-01
  eigenvectors of nos in ao-basis, column    8
  0.1290024E+01 -0.2161314E+01 -0.9823945E+00 -0.1036778E+00  0.7039833E+00  0.6954793E-01  0.3941756E+00 -0.2177934E+01
  0.1243849E+01 -0.1574224E+00 -0.9391545E-01 -0.8168708E+00 -0.1002272E+00 -0.2351193E+00
  eigenvectors of nos in ao-basis, column    9
 -0.4558110E-01  0.2400577E-01  0.1637070E+01  0.9714297E+00 -0.1604216E+01  0.5293499E-01 -0.1173647E+00  0.2340186E+01
 -0.6947980E+00  0.1333704E-01  0.4827934E-01 -0.1590793E+00  0.1781243E+00  0.3331712E+00
  eigenvectors of nos in ao-basis, column   10
 -0.1656250E+01  0.3269545E+01 -0.8560924E+00 -0.1843463E-01 -0.1002408E+01  0.9505500E-01 -0.1939993E-01  0.2414681E+01
 -0.3429717E+01  0.3538418E+00  0.1158331E+01  0.3465007E+00 -0.5022807E+00 -0.1628583E+00
  eigenvectors of nos in ao-basis, column   11
  0.1899805E+00 -0.2194032E+00  0.3680287E+01  0.4452394E-01 -0.7062286E+00  0.3713533E-01 -0.8587205E-01  0.2001312E+01
  0.2365714E+01 -0.4488568E-01  0.4607414E+00  0.1010401E+00  0.4400974E+00  0.1801659E+01
  eigenvectors of nos in ao-basis, column   12
  0.5927726E+00 -0.1825911E+01 -0.2824059E+01 -0.2154326E+00  0.1605674E+01 -0.1910916E-01  0.1314681E+00 -0.3006825E+01
 -0.3376915E+01  0.4265113E+00 -0.1004901E+01 -0.1007840E+01 -0.1755673E+01  0.3618933E-01
  eigenvectors of nos in ao-basis, column   13
  0.2540639E+00 -0.8608117E+00  0.2510358E+01  0.4063521E-01  0.1932570E+00 -0.1187807E-01  0.3751076E-01 -0.1964623E+00
  0.8649779E+01  0.2160780E+00 -0.4784890E+00 -0.1211980E+00  0.4525473E+01  0.1267585E+01
  eigenvectors of nos in ao-basis, column   14
 -0.8313915E-02 -0.4253481E-02 -0.1493556E+01 -0.4047131E-01  0.2424433E+00 -0.4904838E-02  0.1049260E-01 -0.4256364E+00
 -0.2509807E+01  0.2875974E+01 -0.1616826E+00 -0.6757092E-01 -0.4455511E+00 -0.7101677E+00


*****   symmetry block SYM3   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       1.97154623
       2       0.01236793     0.00545409
       3       0.01248124     0.00598757     0.00845891
       4       0.00454844    -0.00319318    -0.00177766     0.00464481
       5      -0.00018388     0.00028702     0.00148089     0.00132367
       6      -0.00785669     0.00208162     0.00306824    -0.00112441
       7       0.00012114     0.00001920    -0.00002211    -0.00004810
       8      -0.00085305     0.00018670     0.00004059    -0.00050879
       9       0.00015259    -0.00023700     0.00023766     0.00112523

               Column   5     Column   6     Column   7     Column   8
       5       0.00141399
       6       0.00048328     0.00189932
       7      -0.00003718    -0.00000363     0.00038929
       8      -0.00004178     0.00006582     0.00000538     0.00038649
       9       0.00078509    -0.00000243     0.00000561    -0.00014765

               Column   9
       9       0.00077662

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7        eno   8
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    8   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno   9
 emo    9   0.100000E+01

 occupation numbers of nos
   1.9717457      0.0154133      0.0054141      0.0008083      0.0006326
   0.0004018      0.0003701      0.0001237      0.0000600

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7         no   8
  mo    1   0.999949E+00  -0.622910E-02  -0.302347E-02   0.686074E-02   0.221821E-02   0.560805E-03  -0.857597E-03  -0.568728E-03
  mo    2   0.630105E-02   0.574215E+00  -0.120960E+00  -0.459605E+00   0.111969E+00   0.965263E-01  -0.137602E+00   0.112789E+00
  mo    3   0.636786E-02   0.699640E+00   0.370763E+00  -0.325141E-01  -0.303327E+00  -0.115599E+00   0.175302E+00   0.825581E-02
  mo    4   0.229852E-02  -0.312568E+00   0.746859E+00  -0.120856E+00  -0.354690E+00  -0.728822E-02   0.636090E-01   0.238398E+00
  mo    5  -0.870061E-04   0.648351E-01   0.442127E+00   0.592855E-01   0.593847E+00  -0.167986E+00   0.113043E+00  -0.622256E+00
  mo    6  -0.397305E-02   0.279351E+00   0.788198E-01   0.860519E+00   0.707559E-02   0.177103E+00  -0.183377E+00   0.152565E+00
  mo    7   0.613870E-04   0.427569E-03  -0.124221E-01  -0.167246E-01   0.368240E-01   0.777189E+00   0.623870E+00  -0.700315E-01
  mo    8  -0.432702E-03   0.211897E-01  -0.875582E-01   0.101169E+00   0.330209E+00  -0.463129E+00   0.615595E+00   0.520296E+00
  mo    9   0.787395E-04  -0.188143E-01   0.283877E+00  -0.136127E+00   0.553667E+00   0.314912E+00  -0.362920E+00   0.494254E+00

                no   9
  mo    1  -0.468232E-03
  mo    2   0.625187E+00
  mo    3  -0.485563E+00
  mo    4   0.378356E+00
  mo    5   0.126298E+00
  mo    6   0.294828E+00
  mo    7   0.826442E-02
  mo    8   0.919674E-01
  mo    9  -0.344695E+00
  eigenvectors of nos in ao-basis, column    1
  0.9213771E+00 -0.7172435E-02  0.4188976E-01 -0.4911555E-02 -0.2005233E-01  0.1127707E-02  0.3336170E-03  0.2297791E-01
  0.3897949E-01
  eigenvectors of nos in ao-basis, column    2
 -0.1658882E+01  0.5393669E+00  0.1281584E+01 -0.1741771E-01 -0.7375992E-01  0.2053020E-03  0.5089688E-03  0.6459631E-01
  0.8623927E-01
  eigenvectors of nos in ao-basis, column    3
 -0.2338461E+00  0.1022293E+00  0.3556863E+00  0.3444481E+00  0.7117856E+00 -0.3211186E-02 -0.5083328E-02 -0.6854042E-01
 -0.5153111E-01
  eigenvectors of nos in ao-basis, column    4
 -0.2664082E+01  0.2743289E+01  0.4187932E+00 -0.1571257E+00  0.2939059E-01  0.6682578E-02 -0.5927798E-03 -0.3512130E-01
 -0.2003943E+00
  eigenvectors of nos in ao-basis, column    5
 -0.2165031E+00  0.2575569E+00 -0.6432659E+00  0.5735887E+00 -0.4602296E-01  0.8106919E-01  0.3111175E-01  0.4944818E+00
  0.1208289E+00
  eigenvectors of nos in ao-basis, column    6
 -0.3571519E+00  0.4577926E+00 -0.2163202E+00  0.4412013E+00 -0.3441059E+00 -0.1395333E+00  0.9081484E-01 -0.5805493E-01
  0.1410658E+00
  eigenvectors of nos in ao-basis, column    7
  0.3642773E+00 -0.4773461E+00  0.3543365E+00 -0.5416289E+00  0.3975381E+00  0.6239705E-01  0.1235411E+00 -0.2279522E-01
 -0.1797127E+00
  eigenvectors of nos in ao-basis, column    8
 -0.4609722E+00  0.6895634E+00  0.2894442E+00  0.5054860E+00 -0.8972198E+00  0.1246035E+00  0.2766669E-01 -0.8811567E+00
  0.1968734E+00
  eigenvectors of nos in ao-basis, column    9
 -0.8216869E+00  0.1319211E+01 -0.1790356E+01 -0.4247988E+00  0.5619366E+00  0.2301271E-02  0.9566465E-03 -0.5118306E-01
  0.1041355E+01


*****   symmetry block SYM4   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       0.00571719
       2      -0.00063895     0.00076709
       3       0.00027164     0.00003381     0.00024352
       4       0.00015096    -0.00015078     0.00016428     0.00021545
       5      -0.00004628    -0.00013518    -0.00007143    -0.00001066

               Column   5
       5       0.00015401

               eno   1        eno   2        eno   3        eno   4        eno   5
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   0.0058167      0.0007536      0.0003911      0.0001037      0.0000322

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5
  mo    1   0.990354E+00   0.119775E+00  -0.563189E-01   0.246516E-01   0.327588E-01
  mo    2  -0.125775E+00   0.940999E+00   0.843675E-02   0.253162E+00   0.185848E+00
  mo    3   0.485095E-01   0.949573E-01   0.727478E+00   0.134571E+00  -0.664302E+00
  mo    4   0.315107E-01  -0.196521E+00   0.639588E+00   0.199842E+00   0.715106E+00
  mo    5  -0.576273E-02  -0.229211E+00  -0.241786E+00   0.936619E+00  -0.108229E+00
  eigenvectors of nos in ao-basis, column    1
 -0.3213463E+00 -0.6832574E+00  0.4395978E-01  0.1046844E+00  0.7639514E-01
  eigenvectors of nos in ao-basis, column    2
 -0.6019829E+00 -0.9810134E-01 -0.3441401E+00 -0.4978103E+00 -0.1265609E+00
  eigenvectors of nos in ao-basis, column    3
  0.6061242E+00 -0.4342618E+00 -0.7668409E+00 -0.3376163E-01  0.3054711E+00
  eigenvectors of nos in ao-basis, column    4
  0.6828120E+00 -0.1183396E+01  0.6206887E+00 -0.8264367E+00  0.8560839E-01
  eigenvectors of nos in ao-basis, column    5
  0.3408026E+00 -0.3535016E+00 -0.2229281E+00  0.3572175E+00 -0.1161882E+01


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***


 cosurf_and_phi
  1.262739748330  2.872870034186  1.567866969040 -0.042729295193
  1.652947872482  0.360731428865  3.496571379926 -0.051909485586
  0.594940552015  0.670555958619  3.845537291721 -0.062273356343
  2.390078334690  1.202722009900  2.566708449184 -0.033095969940
  1.989334673621  2.229216060677  2.001028520754 -0.035976993486
  2.919143888229  0.378144896597  2.099775822683 -0.012561581319
  0.105380882100  1.878884201281  3.371423292662 -0.062570400328
  0.783266721831  2.590884380442  2.520834408126 -0.055952327907
  2.011402526412  2.551496219550  0.814855178005 -0.017012460370
  0.099626804209  1.855073908563 -1.945822518898  0.030228374278
  0.119020599620  3.173161356543  1.415159765853 -0.048615512923
  0.915237473358  3.108118624501  0.463299650838 -0.030413536812
  0.462814589486  2.837210699514 -0.795516582628 -0.006601657385
  1.408719892640  1.640288870679  3.148120355694 -0.053662120226
  2.650053602975  1.633766196698  1.655441764425 -0.014185090588
  1.142904167226  2.885766749848 -0.243475252462 -0.012193767424
  3.656106873706  0.713798214804  0.361832640492  0.038231124436
  2.408046317685  2.075600470298 -1.226192756897  0.039204296050
  1.295820311657  0.567673501678 -2.747601655947  0.057144969613
  3.527730862121  1.045649613724 -1.064853177123  0.054517411470
  2.622898581638  1.024710819001 -2.241122746235  0.059081268956
  3.086808508398  1.794857685487 -0.179703829578  0.034018972039
  1.615872011596  1.674366500124 -2.147504385867  0.046865552837
  0.000006852884  1.035694667087 -2.361676372823  0.049282707063
  0.298815704890  0.969607820141 -2.408807386530  0.050867644272
  0.667723897920  1.245157743773 -2.338577856151  0.048573777550
  1.218123388571  2.025110412626 -1.616576841774  0.034508474986
  2.084186643139  2.293984793080 -0.480493513306  0.021332758522
  2.876642520254  1.658030225134  0.559035126479  0.020779745315
  3.282919570196  0.368088739237  1.091983758387  0.024255674080
  0.467932162408  1.694535407938 -1.941510815499  0.036941623407
  1.299770347024  2.453875604722 -0.850324284820  0.011460146768
  2.242050270258  2.245320705184  0.385739526123  0.002966063681
  2.923104801922  1.151131828431  1.279135167181  0.007145116157
  0.427044967836  0.580963354323 -2.585108185092  0.054617852895
 end_of_phi


 nsubv after electrostatic potential =  4

  Confirmation of dielectric energy for ground state
  edielnew =  -0.0025250583

 =========== Executing IN-CORE method ==========
 norm multnew:  1.53735133E-05
 threx: total number of loops= 14
 threx: total number of loops= 24
 blstr diagel,allext,nmin2=  1892
 onel.diag.
 -0.194545E+01-0.286130E+01-0.270402E+01-0.267939E+01-0.404309E+01-0.353003E+01-0.366094E+01-0.377999E+01-0.336133E+01-0.260063E+01
 -0.287415E+01-0.357359E+01-0.152440E+01-0.144558E+01-0.152216E+01-0.749910E+00 0.955054E+00-0.155502E+01-0.292594E+01-0.240588E+01
 -0.200637E+01-0.405451E+01-0.340880E+01-0.250541E+01-0.287248E+01-0.299025E+01-0.266613E+01-0.118104E+01-0.155136E+01-0.373927E+00
 -0.277866E+01-0.388024E+01-0.376232E+01-0.283288E+01-0.443810E+01-0.160170E+01-0.133510E+01-0.168075E+01-0.459117E+01-0.280777E+01
 -0.220460E+01-0.135625E+01-0.747718E+00-0.330495E+02-0.787969E+01-0.692041E+01-0.675761E+01-0.704586E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1      1892      1892
 blstr diagel,allext,nmin2=  1892
 onel.diag.
 -0.194545E+01-0.286130E+01-0.270402E+01-0.267939E+01-0.404309E+01-0.353003E+01-0.366094E+01-0.377999E+01-0.336133E+01-0.260063E+01
 -0.287415E+01-0.357359E+01-0.152440E+01-0.144558E+01-0.152216E+01-0.749910E+00 0.955054E+00-0.155502E+01-0.292594E+01-0.240588E+01
 -0.200637E+01-0.405451E+01-0.340880E+01-0.250541E+01-0.287248E+01-0.299025E+01-0.266613E+01-0.118104E+01-0.155136E+01-0.373927E+00
 -0.277866E+01-0.388024E+01-0.376232E+01-0.283288E+01-0.443810E+01-0.160170E+01-0.133510E+01-0.168075E+01-0.459117E+01-0.280777E+01
 -0.220460E+01-0.135625E+01-0.747718E+00-0.330495E+02-0.787969E+01-0.692041E+01-0.675761E+01-0.704586E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1      1892      1892
 blstr diagel,allext,nmin2=  1892
 onel.diag.
 -0.194545E+01-0.286130E+01-0.270402E+01-0.267939E+01-0.404309E+01-0.353003E+01-0.366094E+01-0.377999E+01-0.336133E+01-0.260063E+01
 -0.287415E+01-0.357359E+01-0.152440E+01-0.144558E+01-0.152216E+01-0.749910E+00 0.955054E+00-0.155502E+01-0.292594E+01-0.240588E+01
 -0.200637E+01-0.405451E+01-0.340880E+01-0.250541E+01-0.287248E+01-0.299025E+01-0.266613E+01-0.118104E+01-0.155136E+01-0.373927E+00
 -0.277866E+01-0.388024E+01-0.376232E+01-0.283288E+01-0.443810E+01-0.160170E+01-0.133510E+01-0.168075E+01-0.459117E+01-0.280777E+01
 -0.220460E+01-0.135625E+01-0.747718E+00-0.330495E+02-0.787969E+01-0.692041E+01-0.675761E+01-0.704586E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1      1892      1892


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        12 xx:        36 ww:        79
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        11
task #   9:        28    task #  10:        71    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 matrix-vector product formed for vector   5
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference-subspace vector overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     1.00000000     0.00000000     0.00000000     0.00000000     0.00000000

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     0.97236993    -0.09160917     0.11539852    -0.17767562     0.03491482
 subspace has dimension  5

          (h-repnuc*1) matrix in the subspace basis

                x:   1         x:   2         x:   3         x:   4         x:   5
   x:   1   -85.40875183
   x:   2    -0.31915444    -7.04150281
   x:   3    -0.00086869     0.28014244    -0.25399358
   x:   4     0.00008128    -0.02653816    -0.00113002    -0.01206080
   x:   5     0.00005627    -0.01833698    -0.00136609    -0.00078418    -0.00124049

          overlap matrix in the subspace basis

                x:   1         x:   2         x:   3         x:   4         x:   5
   x:   1     1.00000000
   x:   2     0.00000000     0.08688345
   x:   3     0.00000000    -0.00342125     0.00311503
   x:   4     0.00000000     0.00030861     0.00000546     0.00014894
   x:   5     0.00000000     0.00021324     0.00001595     0.00000829     0.00001537

          eigenvectors and eigenvalues in the subspace basis

                v:   1         v:   2         v:   3         v:   4         v:   5

   energy   -85.67270124   -83.34369581   -81.44787260   -80.01803570   -77.77098124

   x:   1     0.97236993    -0.09160917     0.11539852    -0.17767562     0.03491482
   x:   2     0.80207868     0.60823444    -1.48144986     2.95387083    -0.81367719
   x:   3     0.92309730     7.36074595    13.53364192    10.09277161     0.23477042
   x:   4     0.97470997    49.83717219    -2.30504623   -33.23096911   -57.87143853
   x:   5     0.94467165   129.47994830   -67.31447866   -63.78549152   211.30983726

          eigenvectors and eigenvalues in the schmidt orthonormalized basis

                v:   1         v:   2         v:   3         v:   4         v:   5

   energy   -85.67270124   -83.34369581   -81.44787260   -80.01803570   -77.77098124

  zx:   1     0.97236993    -0.09160917     0.11539852    -0.17767562     0.03491482
  zx:   2     0.22741031     0.23969535    -0.64486597     0.67260114    -0.15028733
  zx:   3     0.05112960     0.47565256     0.70807163     0.51182484     0.08837592
  zx:   4     0.01242163     0.68444723    -0.06891983    -0.44268086    -0.57503165
  zx:   5     0.00357007     0.48932661    -0.25439279    -0.24105615     0.79857559

          <ref|baseci> overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     1.00000000     0.00000000     0.00000000     0.00000000     0.00000000

  write the transformed vectors back to cosmofile

 rtflw =  0
 freezing =  F

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1    -76.3366213635 -9.3360E+00  5.1901E-06  5.4172E-03  1.0000E-03
 mr-sdci #  5  2    -74.0076159302 -8.3284E+00  0.0000E+00  1.8111E+00  1.0000E-04
 mr-sdci #  5  3    -72.1117927205 -8.9082E+00  0.0000E+00  2.7975E+00  1.0000E-04
 mr-sdci #  5  4    -70.6819558199 -9.0687E+00  0.0000E+00  4.8613E+00  1.0000E-04
 mr-sdci #  5  5    -68.4349013649  6.8435E+01  0.0000E+00  5.0207E+00  1.0000E-04


 total energy in cosmo calc 
e(rootcalc) + repnuc=     -76.3366213635
dielectric energy =      -0.0095031456
deltaediel =       0.0095031456
e(rootcalc) + repnuc - ediel =     -76.3271182179
e(rootcalc) + repnuc - edielnew =     -76.3366213635
deltaelast =      76.3271182179

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.02   0.00   0.00   0.01         0.    0.0000
    2   25    0   0.01   0.00   0.00   0.01         0.    0.0000
    3   26    0   0.03   0.00   0.00   0.03         0.    0.0000
    4   11    0   0.01   0.00   0.00   0.01         0.    0.0000
    5   15    0   0.03   0.00   0.00   0.03         0.    0.0000
    6   16    0   0.04   0.00   0.00   0.04         0.    0.0000
    7    1    0   0.01   0.00   0.00   0.01         0.    0.0000
    8    5    0   0.04   0.00   0.00   0.03         0.    0.0000
    9    6    0   0.04   0.00   0.00   0.04         0.    0.0000
   10    7    0   0.05   0.00   0.00   0.05         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.01   0.01   0.00   0.01         0.    0.0000
   13   46    0   0.03   0.00   0.00   0.03         0.    0.0000
   14   47    0   0.04   0.00   0.00   0.04         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3600s 
time spent in multnx:                   0.3400s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0100s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            1.6600s 

          starting ci iteration   6

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.33607988

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=       3  D0X=       9  D0W=      15
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================

          ci-one-electron density block   1

                mo   1         mo   2         mo   3         mo   4         mo   5         mo   6         mo   7         mo   8
   mo   1     1.99955510
   mo   2    -0.00014161     1.98335556
   mo   3    -0.00013073     0.00303677     1.96962896
   mo   4    -0.00015176     0.00131381     0.01219980     0.00119670
   mo   5     0.00020913     0.00395859    -0.01001869    -0.00247696     0.00655558
   mo   6    -0.00019888     0.00483779     0.00455983     0.00026388     0.00062090     0.00285353
   mo   7     0.00036056    -0.00573757    -0.01467223    -0.00099570     0.00053031    -0.00134001     0.00264538
   mo   8    -0.00025764    -0.00651221     0.00447849     0.00272308    -0.00786036    -0.00035804     0.00013719     0.01039066
   mo   9    -0.00032256     0.00606136     0.00795232     0.00018458     0.00127701     0.00291930    -0.00195709    -0.00124992
   mo  10     0.00013117     0.00202267    -0.00491645    -0.00063704     0.00155196     0.00211702     0.00067267    -0.00109619
   mo  11     0.00065899    -0.00067741     0.00020365    -0.00065480    -0.00030295    -0.00143008     0.00257886     0.00141872
   mo  12     0.00061878     0.00198722    -0.00498104    -0.00086283     0.00131918    -0.00136690     0.00132047    -0.00175441
   mo  13    -0.00000833    -0.00130593     0.00146597     0.00026166    -0.00090371     0.00036063     0.00028068     0.00148501
   mo  14    -0.00028620    -0.00399437     0.00500592     0.00087584    -0.00228492    -0.00028233    -0.00035156     0.00302217
   mo  15    -0.00031921     0.00201798    -0.00647677    -0.00013699     0.00088451     0.00090086    -0.00057039    -0.00118545
   mo  16     0.00001345    -0.00012960     0.00056059     0.00001779    -0.00006614    -0.00015972    -0.00000765     0.00004105
   mo  17    -0.00025833     0.00010269    -0.00205330     0.00007489     0.00004734    -0.00001267    -0.00036076    -0.00039846
   mo  18    -0.00005625     0.00021656    -0.00028927     0.00005302    -0.00012150    -0.00016203    -0.00008515     0.00012994
   mo  19    -0.00015257     0.00048340    -0.00050436     0.00009466    -0.00015659     0.00033809    -0.00010043     0.00029502
   mo  20     0.00090605     0.00224719    -0.00033452    -0.00020318     0.00023918    -0.00027969     0.00045375    -0.00025290

                mo   9         mo  10         mo  11         mo  12         mo  13         mo  14         mo  15         mo  16
   mo   9     0.00528864
   mo  10     0.00066325     0.00421248
   mo  11    -0.00074467    -0.00041970     0.00439511
   mo  12    -0.00238760     0.00007566     0.00014013     0.00253889
   mo  13    -0.00064798     0.00129895    -0.00011597    -0.00001909     0.00120874
   mo  14    -0.00070065    -0.00088497    -0.00033258    -0.00016246     0.00035756     0.00158337
   mo  15     0.00157185     0.00056955    -0.00074235    -0.00040632    -0.00029185    -0.00054027     0.00112542
   mo  16    -0.00002644    -0.00039593     0.00013349     0.00000578    -0.00006967     0.00004332    -0.00004164     0.00039409
   mo  17    -0.00017780    -0.00027513    -0.00095097     0.00036063    -0.00013893     0.00004037     0.00007552     0.00000860
   mo  18     0.00037007    -0.00084471     0.00051349    -0.00040285    -0.00060523    -0.00000435    -0.00000204     0.00008856
   mo  19     0.00047113     0.00039085     0.00017949    -0.00062548     0.00024427    -0.00023472     0.00009986    -0.00004459
   mo  20    -0.00040217     0.00009059     0.00054716     0.00048809    -0.00001369    -0.00022113    -0.00014321     0.00000589

                mo  17         mo  18         mo  19         mo  20
   mo  17     0.00061003
   mo  18    -0.00012442     0.00067387
   mo  19    -0.00022393    -0.00000216     0.00043573
   mo  20    -0.00006720    -0.00002358    -0.00007160     0.00038269

          ci-one-electron density block   2

                mo  21         mo  22         mo  23         mo  24         mo  25         mo  26         mo  27         mo  28
   mo  21     1.96823665
   mo  22     0.00184380     0.00062359
   mo  23    -0.01525017    -0.00190044     0.00719565
   mo  24    -0.00187783     0.00074830    -0.00144398     0.00145320
   mo  25     0.00187518     0.00051372    -0.00184670     0.00044050     0.00051916
   mo  26    -0.01447880    -0.00194419     0.00848348    -0.00087224    -0.00213336     0.01107931
   mo  27    -0.00351586    -0.00126628     0.00291837    -0.00223746    -0.00086397     0.00232970     0.00382126
   mo  28     0.00148225     0.00015524    -0.00051395     0.00026243     0.00004505    -0.00066849    -0.00038618     0.00055813
   mo  29     0.00917202     0.00036632    -0.00228614    -0.00023971     0.00059423    -0.00361170     0.00020823     0.00025792
   mo  30     0.00523662     0.00058970    -0.00174554     0.00088996     0.00047326    -0.00192958    -0.00188727     0.00018397
   mo  31    -0.00544004    -0.00023364     0.00064418    -0.00039660    -0.00022126     0.00064739     0.00080779    -0.00013217
   mo  32     0.00091640     0.00003692    -0.00008771     0.00009656    -0.00002319    -0.00007561    -0.00013935     0.00026231
   mo  33    -0.00389640    -0.00001025     0.00030267     0.00015380    -0.00007987     0.00072789    -0.00028494    -0.00005067
   mo  34    -0.00131676    -0.00004005    -0.00007385    -0.00019798     0.00000685    -0.00028930     0.00044836     0.00007258

                mo  29         mo  30         mo  31         mo  32         mo  33         mo  34
   mo  29     0.00206026
   mo  30     0.00004968     0.00149750
   mo  31    -0.00024686    -0.00043270     0.00060093
   mo  32     0.00001917     0.00007499    -0.00002357     0.00032524
   mo  33    -0.00073870     0.00007979     0.00000686    -0.00000840     0.00054116
   mo  34     0.00043195    -0.00043151     0.00004843     0.00000916    -0.00020287     0.00034828

          ci-one-electron density block   3

                mo  35         mo  36         mo  37         mo  38         mo  39         mo  40         mo  41         mo  42
   mo  35     1.97128952
   mo  36     0.01328158     0.00562713
   mo  37     0.01233584     0.00615105     0.00858163
   mo  38     0.00423383    -0.00323843    -0.00184867     0.00460516
   mo  39     0.00007539     0.00031431     0.00148504     0.00128531     0.00139149
   mo  40    -0.00728040     0.00212292     0.00309761    -0.00113421     0.00048269     0.00188467
   mo  41     0.00005212     0.00001953    -0.00002143    -0.00004829    -0.00003805    -0.00000323     0.00038159
   mo  42    -0.00081481     0.00018457     0.00004243    -0.00050093    -0.00003971     0.00006618     0.00000440     0.00037949
   mo  43     0.00012015    -0.00023024     0.00023457     0.00110456     0.00077175    -0.00000170     0.00000460    -0.00014504

                mo  43
   mo  43     0.00076269

          ci-one-electron density block   4

                mo  44         mo  45         mo  46         mo  47         mo  48
   mo  44     0.00577659
   mo  45    -0.00072520     0.00080983
   mo  46     0.00033119     0.00000999     0.00025339
   mo  47     0.00016651    -0.00015231     0.00016562     0.00021193
   mo  48    -0.00004933    -0.00013138    -0.00007128    -0.00001323     0.00015402


*****   symmetry block SYM1   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       1.99955510
       2      -0.00014161     1.98335556
       3      -0.00013073     0.00303677     1.96962896
       4      -0.00015176     0.00131381     0.01219980     0.00119670
       5       0.00020913     0.00395859    -0.01001869    -0.00247696
       6      -0.00019888     0.00483779     0.00455983     0.00026388
       7       0.00036056    -0.00573757    -0.01467223    -0.00099570
       8      -0.00025764    -0.00651221     0.00447849     0.00272308
       9      -0.00032256     0.00606136     0.00795232     0.00018458
      10       0.00013117     0.00202267    -0.00491645    -0.00063704
      11       0.00065899    -0.00067741     0.00020365    -0.00065480
      12       0.00061878     0.00198722    -0.00498104    -0.00086283
      13      -0.00000833    -0.00130593     0.00146597     0.00026166
      14      -0.00028620    -0.00399437     0.00500592     0.00087584
      15      -0.00031921     0.00201798    -0.00647677    -0.00013699
      16       0.00001345    -0.00012960     0.00056059     0.00001779
      17      -0.00025833     0.00010269    -0.00205330     0.00007489
      18      -0.00005625     0.00021656    -0.00028927     0.00005302
      19      -0.00015257     0.00048340    -0.00050436     0.00009466
      20       0.00090605     0.00224719    -0.00033452    -0.00020318

               Column   5     Column   6     Column   7     Column   8
       5       0.00655558
       6       0.00062090     0.00285353
       7       0.00053031    -0.00134001     0.00264538
       8      -0.00786036    -0.00035804     0.00013719     0.01039066
       9       0.00127701     0.00291930    -0.00195709    -0.00124992
      10       0.00155196     0.00211702     0.00067267    -0.00109619
      11      -0.00030295    -0.00143008     0.00257886     0.00141872
      12       0.00131918    -0.00136690     0.00132047    -0.00175441
      13      -0.00090371     0.00036063     0.00028068     0.00148501
      14      -0.00228492    -0.00028233    -0.00035156     0.00302217
      15       0.00088451     0.00090086    -0.00057039    -0.00118545
      16      -0.00006614    -0.00015972    -0.00000765     0.00004105
      17       0.00004734    -0.00001267    -0.00036076    -0.00039846
      18      -0.00012150    -0.00016203    -0.00008515     0.00012994
      19      -0.00015659     0.00033809    -0.00010043     0.00029502
      20       0.00023918    -0.00027969     0.00045375    -0.00025290

               Column   9     Column  10     Column  11     Column  12
       9       0.00528864
      10       0.00066325     0.00421248
      11      -0.00074467    -0.00041970     0.00439511
      12      -0.00238760     0.00007566     0.00014013     0.00253889
      13      -0.00064798     0.00129895    -0.00011597    -0.00001909
      14      -0.00070065    -0.00088497    -0.00033258    -0.00016246
      15       0.00157185     0.00056955    -0.00074235    -0.00040632
      16      -0.00002644    -0.00039593     0.00013349     0.00000578
      17      -0.00017780    -0.00027513    -0.00095097     0.00036063
      18       0.00037007    -0.00084471     0.00051349    -0.00040285
      19       0.00047113     0.00039085     0.00017949    -0.00062548
      20      -0.00040217     0.00009059     0.00054716     0.00048809

               Column  13     Column  14     Column  15     Column  16
      13       0.00120874
      14       0.00035756     0.00158337
      15      -0.00029185    -0.00054027     0.00112542
      16      -0.00006967     0.00004332    -0.00004164     0.00039409
      17      -0.00013893     0.00004037     0.00007552     0.00000860
      18      -0.00060523    -0.00000435    -0.00000204     0.00008856
      19       0.00024427    -0.00023472     0.00009986    -0.00004459
      20      -0.00001369    -0.00022113    -0.00014321     0.00000589

               Column  17     Column  18     Column  19     Column  20
      17       0.00061003
      18      -0.00012442     0.00067387
      19      -0.00022393    -0.00000216     0.00043573
      20      -0.00006720    -0.00002358    -0.00007160     0.00038269

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7        eno   8
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    8   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno   9        eno  10        eno  11        eno  12        eno  13        eno  14        eno  15        eno  16
 emo    9   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   10   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   11   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   12   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   13   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   14   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   15   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   16   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno  17        eno  18        eno  19        eno  20
 emo   17   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   18   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   19   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   20   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   1.9995585      1.9841115      1.9693192      0.0195020      0.0110167
   0.0060682      0.0052709      0.0011372      0.0008470      0.0005728
   0.0004725      0.0003667      0.0003555      0.0001569      0.0001093
   0.0000688      0.0000566      0.0000319      0.0000080      0.0000004

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7         no   8
  mo    1   0.999935E+00   0.108402E-01   0.351068E-02   0.164536E-03   0.532454E-03  -0.235730E-03   0.617640E-04  -0.100986E-03
  mo    2  -0.985231E-02   0.977298E+00  -0.211528E+00   0.497264E-02  -0.342027E-02   0.115629E-03   0.479206E-03  -0.604706E-03
  mo    3  -0.571924E-02   0.211455E+00   0.977279E+00  -0.651359E-02  -0.767413E-02   0.367001E-02   0.169396E-02   0.131959E-02
  mo    4  -0.118158E-03   0.194463E-02   0.593492E-02   0.194471E+00   0.148070E+00  -0.111513E+00   0.514615E-01  -0.798778E-01
  mo    5   0.114941E-03   0.899772E-03  -0.544252E-02  -0.564067E+00  -0.880927E-01   0.654673E-01  -0.593385E-01   0.737399E-01
  mo    6  -0.137476E-03   0.288218E-02   0.175042E-02  -0.856150E-01   0.433686E+00   0.243954E+00   0.111591E+00   0.211468E+00
  mo    7   0.252119E-03  -0.439996E-02  -0.668371E-02   0.538107E-02  -0.375885E+00   0.363497E+00  -0.724889E-01   0.193675E+00
  mo    8  -0.110816E-03  -0.275406E-02   0.297569E-02   0.713616E+00   0.102341E+00   0.202852E+00   0.179433E-03   0.200747E+00
  mo    9  -0.215451E-03   0.385321E-02   0.330629E-02  -0.150358E+00   0.565522E+00   0.110082E+00  -0.431889E+00   0.344761E+00
  mo   10   0.698858E-04   0.479105E-03  -0.267149E-02  -0.144471E+00   0.142175E+00   0.639289E+00   0.442026E+00  -0.316338E-01
  mo   11   0.333736E-03  -0.320567E-03   0.166564E-03   0.900698E-01  -0.338483E+00   0.443669E+00  -0.576023E+00   0.303062E-01
  mo   12   0.315219E-03   0.444778E-03  -0.270609E-02  -0.978142E-01  -0.357506E+00  -0.728742E-01   0.282383E+00   0.572663E+00
  mo   13  -0.197166E-05  -0.490907E-03   0.871466E-03   0.863328E-01   0.150509E-01   0.241157E+00   0.289962E+00  -0.201142E+00
  mo   14  -0.138282E-03  -0.144257E-02   0.293094E-02   0.217177E+00   0.195381E-01  -0.124527E+00   0.104081E+00   0.358592E+00
  mo   15  -0.151485E-03   0.310251E-03  -0.343391E-02  -0.102645E+00   0.182176E+00   0.975197E-02  -0.416336E-01   0.188314E+00
  mo   16   0.577861E-05  -0.448905E-05   0.292974E-03   0.839070E-02  -0.176099E-01  -0.477161E-01  -0.580516E-01   0.260421E-01
  mo   17  -0.123987E-03  -0.168504E-03  -0.103063E-02  -0.186407E-01   0.160411E-01  -0.170181E+00   0.135209E+00   0.204760E+00
  mo   18  -0.284380E-04   0.758203E-04  -0.163954E-03   0.154244E-01   0.419101E-02  -0.799387E-01  -0.248521E+00  -0.884955E-02
  mo   19  -0.774174E-04   0.184833E-03  -0.299902E-03   0.109221E-01   0.709264E-01   0.112837E+00  -0.488036E-01  -0.399422E+00
  mo   20   0.443427E-03   0.107463E-02  -0.410747E-03  -0.157137E-01  -0.926046E-01   0.582477E-01  -0.149212E-01   0.100997E+00

                no   9         no  10         no  11         no  12         no  13         no  14         no  15         no  16
  mo    1  -0.551141E-04   0.360508E-04  -0.335225E-03   0.180021E-03   0.205161E-03   0.316338E-04  -0.102158E-03   0.486337E-04
  mo    2  -0.105248E-02   0.236953E-03  -0.228726E-02   0.107246E-02   0.132267E-02  -0.285542E-03   0.411193E-03  -0.700058E-03
  mo    3   0.271711E-02  -0.415570E-03  -0.454593E-02   0.241603E-02   0.181569E-02   0.198996E-02   0.218610E-02  -0.208745E-02
  mo    4   0.140919E+00  -0.462344E-01   0.151964E+00  -0.864525E-01  -0.119097E+00  -0.108041E+00  -0.163045E+00   0.122482E+00
  mo    5  -0.387343E+00   0.580397E-01  -0.126908E+00   0.260314E-01   0.258762E-01   0.183207E+00   0.279668E+00  -0.266549E-01
  mo    6  -0.164781E+00  -0.319830E+00   0.248637E+00   0.125866E+00  -0.323827E-01   0.807317E-01  -0.312414E-01  -0.108989E-01
  mo    7   0.101927E+00  -0.197330E-01  -0.209081E+00   0.215457E+00   0.262649E+00   0.499613E-01  -0.687349E-01  -0.267604E+00
  mo    8  -0.325688E-01   0.569444E-03  -0.653465E-01   0.349284E-01   0.467144E-01   0.188464E+00   0.155760E+00  -0.887203E-01
  mo    9   0.501748E-02   0.300154E+00   0.119643E+00  -0.114145E+00   0.106810E+00   0.557207E-01  -0.202585E+00  -0.795795E-02
  mo   10   0.964025E-01  -0.227637E+00  -0.912236E-01   0.341100E-02  -0.560638E-01  -0.185606E+00  -0.590816E-01  -0.328308E-01
  mo   11  -0.313227E-01   0.525323E-01   0.142115E+00  -0.591911E-01  -0.108310E-01  -0.125112E+00   0.125087E-02   0.251273E+00
  mo   12   0.151662E+00   0.809634E-01   0.196887E+00  -0.155985E+00  -0.216759E+00   0.325441E+00  -0.410909E+00   0.111029E+00
  mo   13  -0.194450E+00   0.491417E+00   0.107878E+00   0.576854E-01  -0.399772E-01   0.256721E+00   0.170186E+00   0.572333E+00
  mo   14  -0.566098E+00   0.587168E-01  -0.338394E+00  -0.721441E-01  -0.102177E+00  -0.216809E-01   0.137957E+00  -0.151716E+00
  mo   15   0.572669E+00   0.196798E+00  -0.540692E+00   0.692735E-01  -0.270016E+00   0.617160E-01   0.304097E+00   0.112955E+00
  mo   16  -0.549326E-01   0.187088E+00   0.207936E+00   0.819707E+00  -0.418818E+00  -0.113196E+00  -0.633795E-01  -0.160976E+00
  mo   17   0.208786E+00  -0.207972E-01   0.233575E+00   0.316705E+00   0.675059E+00   0.135372E+00   0.290816E+00   0.936243E-01
  mo   18  -0.280691E-01  -0.632122E+00  -0.116454E+00   0.163316E+00  -0.186843E+00   0.433280E+00   0.555241E-01   0.383234E+00
  mo   19   0.584331E-01   0.139806E+00  -0.628464E-02  -0.339687E-01  -0.873388E-02   0.678808E+00  -0.162755E+00  -0.449673E+00
  mo   20   0.145455E+00  -0.485847E-01   0.489367E+00  -0.272219E+00  -0.316585E+00   0.356619E-01   0.627075E+00  -0.284569E+00

                no  17         no  18         no  19         no  20
  mo    1  -0.539933E-04  -0.214305E-04  -0.896306E-05   0.838946E-06
  mo    2   0.869531E-03   0.799464E-03  -0.193736E-04  -0.162853E-03
  mo    3   0.725205E-03   0.201542E-02   0.198096E-02  -0.122179E-02
  mo    4  -0.611644E-01  -0.127740E+00  -0.255882E+00   0.844520E+00
  mo    5  -0.178961E+00  -0.102901E+00   0.385830E+00   0.431240E+00
  mo    6  -0.501673E+00   0.420941E+00  -0.197857E+00  -0.640988E-01
  mo    7   0.296160E+00   0.474611E+00  -0.217482E+00   0.273360E+00
  mo    8  -0.975720E-01  -0.281426E-01   0.547251E+00   0.112402E+00
  mo    9   0.401994E+00  -0.610622E-01   0.398034E-01   0.745379E-02
  mo   10   0.210104E+00  -0.442936E+00   0.320726E-01  -0.292986E-01
  mo   11  -0.350965E+00  -0.278975E+00  -0.189453E+00  -0.671263E-01
  mo   12  -0.802274E-01  -0.892802E-01   0.839637E-01  -0.379543E-01
  mo   13   0.166723E+00   0.213613E+00  -0.133795E+00  -0.629685E-03
  mo   14   0.435748E-01  -0.247240E+00  -0.486006E+00  -0.411821E-01
  mo   15  -0.243452E+00   0.222380E-01  -0.115820E+00  -0.426036E-02
  mo   16   0.561093E-01  -0.131403E+00   0.393820E-01   0.444367E-03
  mo   17  -0.633875E-01  -0.333414E+00  -0.169538E+00  -0.674787E-02
  mo   18   0.336133E+00  -0.593767E-01  -0.421156E-01  -0.596170E-02
  mo   19  -0.142157E+00  -0.205427E+00  -0.208535E+00  -0.244456E-01
  mo   20   0.225042E+00   0.521695E-01  -0.105353E+00   0.967465E-02
  eigenvectors of nos in ao-basis, column    1
  0.9974447E+00 -0.1513723E-01  0.3441774E-02  0.4332073E-02 -0.7912846E-02  0.3744082E-02  0.4425081E-03  0.1026047E-04
  0.3300010E-05  0.1145629E-04 -0.2594526E-03  0.2184695E-04  0.5528152E-04  0.6826905E-03 -0.1414345E-02  0.4948046E-04
  0.4665617E-03  0.2365116E-03 -0.5357140E-03 -0.5390886E-03
  eigenvectors of nos in ao-basis, column    2
  0.3079336E-02  0.9124739E+00 -0.2915411E-02 -0.1329733E+00  0.4458886E-01  0.1029875E-01  0.7439618E-01  0.1620348E-03
  0.2554055E-02 -0.1580376E-02 -0.2556618E-02 -0.7738528E-03 -0.1735326E-02  0.2573022E+00 -0.1117060E+00  0.3028552E-02
  0.2608099E-01  0.1918525E-01  0.1607974E-01  0.1198721E-01
  eigenvectors of nos in ao-basis, column    3
  0.9124775E-02  0.1100670E+00  0.3521377E-02  0.1554939E+00  0.8350705E+00 -0.2677605E-01 -0.2921267E-01 -0.1613809E-02
 -0.2671707E-02 -0.5435024E-02  0.1965939E-02 -0.8798241E-04  0.2666487E-02 -0.3984940E+00  0.1690650E+00  0.5795249E-02
 -0.2997790E-01 -0.1445773E-02 -0.1615410E-02  0.2921466E-01
  eigenvectors of nos in ao-basis, column    4
 -0.4304202E-01 -0.6086303E+00 -0.6245291E-01  0.3263375E-02  0.1321714E+01 -0.2015488E+00 -0.6928896E+00  0.3452369E-02
  0.1554176E-01  0.8735914E-02  0.2576564E-01  0.2176412E-02  0.3589870E-02  0.9566622E+00 -0.4164024E+00 -0.8387216E-02
  0.5934801E-01  0.9536035E-02  0.3631025E-02 -0.3803715E-01
  eigenvectors of nos in ao-basis, column    5
  0.1555328E-01 -0.1071148E+01 -0.4393103E+00  0.1098674E+01 -0.8204866E+00  0.3814906E+00  0.8188100E+00 -0.5134392E-02
  0.3209073E-01 -0.2356259E-01  0.4215324E-01 -0.2415422E-02 -0.2982960E-02  0.4409788E+00 -0.2508724E+00  0.1997405E-01
  0.2605049E-01  0.7941667E-01  0.1299351E-01  0.5802489E-01
  eigenvectors of nos in ao-basis, column    6
 -0.1087252E-01  0.4867646E+00  0.2562527E+00 -0.9532402E+00 -0.7094193E-01  0.3144453E-01  0.1436938E+00 -0.3470488E-01
  0.1157667E+00 -0.8650674E-01  0.2533729E+00 -0.1422875E-01 -0.2093653E-01  0.3139512E+00 -0.1499082E+00 -0.1170929E-01
 -0.3197190E-01  0.8189980E-01 -0.3449791E-01  0.2190085E-01
  eigenvectors of nos in ao-basis, column    7
 -0.2193881E-02 -0.1350078E+00 -0.5587037E-01  0.2136975E+00  0.2752832E+00 -0.1196302E+00 -0.3991672E+00 -0.8707125E-01
 -0.8376217E-01 -0.1771111E+00 -0.1631599E+00 -0.9851852E-02  0.1353683E-01 -0.7697610E-01  0.5979040E-01  0.5381679E-02
 -0.5159764E-01  0.4084714E-01 -0.3498457E-02  0.3960331E-01
  eigenvectors of nos in ao-basis, column    8
 -0.1243671E+00  0.2221638E-01  0.4319202E+00 -0.1914137E+01 -0.2069497E+00  0.2922075E+00  0.9682744E+00  0.2044324E-01
 -0.2461102E+00  0.1540335E-01 -0.2149021E+00 -0.3369649E-01 -0.8708271E-01  0.2107183E+01 -0.1007687E+01 -0.2242917E-02
  0.6969594E-01  0.1638341E+00  0.6551998E-01  0.6945705E-01
  eigenvectors of nos in ao-basis, column    9
 -0.2210831E+00 -0.2847315E+00  0.4197008E+00  0.2777703E+00 -0.2528735E+01  0.2370517E+01  0.4632064E+00 -0.2158014E-01
 -0.4505590E-01  0.1242234E-01  0.3732747E-01  0.8883927E-02  0.4049477E-01 -0.2795615E-01  0.1313575E+00 -0.3939637E-01
 -0.1692595E+00 -0.1015665E+00 -0.5508785E-01 -0.1462141E+00
  eigenvectors of nos in ao-basis, column   10
  0.1599497E+00  0.3575769E+00 -0.1918049E+00 -0.6503006E+00 -0.1546911E+00  0.1974957E+00  0.8320296E+00 -0.2106977E+00
  0.3057251E-01  0.1313379E-01  0.1545298E-01  0.8675924E-01 -0.1179129E+00  0.6638800E+00 -0.3617513E+00 -0.1051081E-01
  0.3312126E+00 -0.2875087E+00  0.9534177E-01 -0.1766392E+00
  eigenvectors of nos in ao-basis, column   11
 -0.1370200E+01 -0.2873045E+01  0.1988181E+01  0.1922928E+01  0.8324735E+00 -0.1106822E+01  0.5603088E-01 -0.3321855E-01
 -0.1045100E+00  0.1708194E-01  0.1394052E+00  0.1443265E+00  0.1654875E+00 -0.2257337E+00  0.2140975E+00 -0.3367934E-02
 -0.1468265E+00 -0.1955935E+00 -0.2799619E-01  0.9143662E-01
  eigenvectors of nos in ao-basis, column   12
  0.8754720E+00  0.1930102E+01 -0.1230499E+01 -0.8523473E+00 -0.1995941E+00  0.2285612E+00 -0.3269629E+00  0.8080262E-01
 -0.1404221E+00 -0.4353693E-01  0.1094255E+00  0.3105391E+00 -0.8062815E-01 -0.2203782E+00  0.3096073E-01 -0.1296849E-01
 -0.5859097E-01  0.1646647E-01 -0.1578119E+00  0.5147175E-01
  eigenvectors of nos in ao-basis, column   13
  0.1048926E+01  0.2327548E+01 -0.1466073E+01 -0.7707217E+00  0.2300643E+00 -0.3701118E+00 -0.1747768E-01 -0.9504829E-01
 -0.2568734E+00  0.7979370E-01  0.2318872E+00 -0.6534820E-01  0.3126663E+00 -0.1956307E+00 -0.1004967E+00 -0.2221768E-01
 -0.9976173E-01 -0.6929696E-01 -0.1528979E+00 -0.1349615E+00
  eigenvectors of nos in ao-basis, column   14
  0.3083952E+00  0.8656448E+00 -0.1868846E+00 -0.3182930E+01 -0.7965570E+00  0.1262070E+01  0.1065877E+01  0.9090923E-01
  0.3116293E+00 -0.1706536E+00 -0.4941561E+00  0.6497554E-01  0.3249592E+00  0.2749943E+01 -0.1265417E+01  0.2277492E-01
  0.4425211E+00 -0.1151161E+00  0.1798513E+00  0.2406226E+00
  eigenvectors of nos in ao-basis, column   15
 -0.2212123E+01 -0.5873833E+01  0.2466079E+01  0.3655257E+01 -0.1235336E+01  0.2412001E+01 -0.3099147E+00  0.1992901E-01
 -0.1645295E+00 -0.7190123E-01 -0.4561646E-01  0.1651424E-01  0.1234973E+00  0.1647193E+01 -0.1556461E+01  0.1169089E+00
  0.1023258E+01  0.4990089E+00 -0.1688378E+00  0.1826177E-01
  eigenvectors of nos in ao-basis, column   16
  0.1068878E+01  0.2896091E+01 -0.1118520E+01 -0.3454628E+01 -0.3178959E-01 -0.1136149E+00  0.4924678E+00  0.1542191E+00
 -0.2273405E+00 -0.3145994E+00  0.4868287E+00 -0.1341071E+00 -0.7875773E-01 -0.7148418E+00  0.1276216E+01 -0.6521754E-01
  0.2959107E+00 -0.8432836E+00  0.3879802E+00  0.3808523E+00
  eigenvectors of nos in ao-basis, column   17
 -0.1024624E+01 -0.2796075E+01  0.1009894E+01  0.5094264E+01  0.6138079E+00 -0.9470311E+00  0.8683762E+00  0.1303891E+00
 -0.2482227E-01 -0.1869622E+00  0.5111652E-01  0.1333632E-02  0.2196068E-01 -0.8589089E-01 -0.1022418E+01 -0.3200178E-01
  0.4077552E-01 -0.6390373E-01 -0.4747912E+00 -0.1203526E+01
  eigenvectors of nos in ao-basis, column   18
 -0.6137467E+00 -0.1747963E+01  0.4295526E+00  0.6493329E+01  0.2056411E+00 -0.5637038E+00 -0.1367731E+01 -0.9600484E-03
  0.9386738E-02 -0.4839581E-01  0.2801296E+00 -0.1203472E+00 -0.1738627E+00 -0.2172400E+01 -0.2394142E+00  0.6190714E-02
 -0.1807682E+00 -0.7509736E+00 -0.1287821E+01 -0.2407383E+00
  eigenvectors of nos in ao-basis, column   19
  0.1839675E+00  0.5305659E+00 -0.3044805E+00  0.3254784E+01  0.3527089E+00 -0.1181496E+01 -0.1288038E+01  0.6052267E-02
 -0.5768369E-01  0.2720863E-01  0.4297428E+00 -0.4354605E-01 -0.1549030E+00 -0.2374003E+01 -0.1839061E+00  0.2799463E+00
 -0.1057378E+01 -0.6553280E+00  0.3822585E+00  0.5381736E-01
  eigenvectors of nos in ao-basis, column   20
 -0.1524693E+00 -0.4719734E+00  0.6236250E-01  0.2346987E+01  0.8999508E-02 -0.5854718E-01 -0.5719798E+00 -0.3147577E-03
 -0.1212776E-01  0.2875834E-02  0.6285639E-01 -0.5191776E-02 -0.1468139E-01 -0.3066932E+00 -0.1245569E+01  0.8895747E+00
 -0.1216570E+00 -0.9006043E-01 -0.3070314E+00 -0.2460635E+00


*****   symmetry block SYM2   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       1.96823665
       2       0.00184380     0.00062359
       3      -0.01525017    -0.00190044     0.00719565
       4      -0.00187783     0.00074830    -0.00144398     0.00145320
       5       0.00187518     0.00051372    -0.00184670     0.00044050
       6      -0.01447880    -0.00194419     0.00848348    -0.00087224
       7      -0.00351586    -0.00126628     0.00291837    -0.00223746
       8       0.00148225     0.00015524    -0.00051395     0.00026243
       9       0.00917202     0.00036632    -0.00228614    -0.00023971
      10       0.00523662     0.00058970    -0.00174554     0.00088996
      11      -0.00544004    -0.00023364     0.00064418    -0.00039660
      12       0.00091640     0.00003692    -0.00008771     0.00009656
      13      -0.00389640    -0.00001025     0.00030267     0.00015380
      14      -0.00131676    -0.00004005    -0.00007385    -0.00019798

               Column   5     Column   6     Column   7     Column   8
       5       0.00051916
       6      -0.00213336     0.01107931
       7      -0.00086397     0.00232970     0.00382126
       8       0.00004505    -0.00066849    -0.00038618     0.00055813
       9       0.00059423    -0.00361170     0.00020823     0.00025792
      10       0.00047326    -0.00192958    -0.00188727     0.00018397
      11      -0.00022126     0.00064739     0.00080779    -0.00013217
      12      -0.00002319    -0.00007561    -0.00013935     0.00026231
      13      -0.00007987     0.00072789    -0.00028494    -0.00005067
      14       0.00000685    -0.00028930     0.00044836     0.00007258

               Column   9     Column  10     Column  11     Column  12
       9       0.00206026
      10       0.00004968     0.00149750
      11      -0.00024686    -0.00043270     0.00060093
      12       0.00001917     0.00007499    -0.00002357     0.00032524
      13      -0.00073870     0.00007979     0.00000686    -0.00000840
      14       0.00043195    -0.00043151     0.00004843     0.00000916

               Column  13     Column  14
      13       0.00054116
      14      -0.00020287     0.00034828

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7        eno   8
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    8   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno   9        eno  10        eno  11        eno  12        eno  13        eno  14
 emo    9   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   10   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   11   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   12   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   13   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   14   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   1.9685577      0.0211687      0.0058023      0.0010761      0.0008281
   0.0006446      0.0003909      0.0001646      0.0000987      0.0000951
   0.0000181      0.0000099      0.0000052      0.0000002

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7         no   8
  mo    1   0.999918E+00   0.118971E-01  -0.191934E-02  -0.150524E-02  -0.216418E-02   0.679070E-03   0.113703E-02  -0.161736E-02
  mo    2   0.954915E-03  -0.149512E+00  -0.131845E+00   0.178598E-01  -0.219640E+00  -0.395713E-01   0.647241E-01   0.754905E-01
  mo    3  -0.781997E-02   0.570526E+00   0.192918E-01   0.213797E+00   0.359278E+00  -0.360210E-01   0.683113E-01  -0.242284E+00
  mo    4  -0.941618E-03  -0.115863E+00  -0.415072E+00   0.257713E+00  -0.286158E+00  -0.432456E-01   0.191059E+00  -0.110536E+00
  mo    5   0.971445E-03  -0.148488E+00  -0.270863E-01  -0.302466E-01  -0.979760E-01  -0.194023E+00  -0.571021E-01   0.156231E+00
  mo    6  -0.744681E-02   0.699021E+00  -0.308871E+00   0.141806E+00  -0.195620E+00  -0.103495E+00  -0.190596E-02   0.195642E+00
  mo    7  -0.181301E-02   0.238110E+00   0.662422E+00  -0.204800E+00   0.121992E-01   0.110478E+00  -0.513786E-01   0.891586E-01
  mo    8   0.759154E-03  -0.479088E-01  -0.302140E-01   0.363403E+00   0.354007E-01   0.702885E+00  -0.856621E-01  -0.399327E+00
  mo    9   0.468804E-02  -0.201391E+00   0.359892E+00   0.552552E+00   0.116513E+00  -0.233174E+00   0.206855E-01  -0.128522E-01
  mo   10   0.267889E-02  -0.154122E+00  -0.295600E+00  -0.129758E+00   0.724798E+00   0.199631E-01   0.188560E+00  -0.152822E-01
  mo   11  -0.277113E-02   0.575767E-01   0.117102E+00  -0.315511E+00  -0.220703E+00   0.224522E+00   0.785597E+00  -0.174266E+00
  mo   12   0.466567E-03  -0.806646E-02  -0.255812E-01   0.177716E+00   0.307519E-01   0.529826E+00   0.332231E-01   0.753711E+00
  mo   13  -0.198512E-02   0.341153E-01  -0.147665E+00  -0.411622E+00  -0.118193E+00   0.234012E+00  -0.534306E+00  -0.222358E+00
  mo   14  -0.667297E-03  -0.737380E-02   0.145020E+00   0.264217E+00  -0.298303E+00  -0.250226E-04  -0.408072E-01  -0.212115E+00

                no   9         no  10         no  11         no  12         no  13         no  14
  mo    1   0.723233E-04   0.167559E-02   0.105896E-02   0.123288E-02   0.159543E-02   0.326601E-03
  mo    2  -0.414019E-01  -0.122724E+00   0.320288E+00  -0.151206E+00  -0.192010E-01   0.870434E+00
  mo    3  -0.640931E-01   0.353757E+00   0.120230E+00   0.354317E+00   0.302025E+00   0.272295E+00
  mo    4  -0.183223E+00  -0.205700E-01   0.625630E+00   0.173593E+00   0.137329E+00  -0.375493E+00
  mo    5  -0.114193E+00   0.101815E+00  -0.194426E+00  -0.313336E+00   0.860978E+00  -0.268085E-01
  mo    6  -0.800632E-01  -0.183292E+00  -0.105259E+00  -0.477134E+00  -0.154158E+00  -0.777143E-01
  mo    7  -0.127973E-01  -0.276598E+00   0.544891E+00  -0.173715E+00   0.148595E+00  -0.117376E+00
  mo    8  -0.636865E-01  -0.342189E+00  -0.155592E+00  -0.159539E+00   0.172940E+00   0.437047E-01
  mo    9  -0.507523E+00   0.236392E+00  -0.136223E-01  -0.288147E+00  -0.243242E+00  -0.142644E-01
  mo   10   0.167409E+00   0.412394E-01   0.252563E+00  -0.461053E+00  -0.334048E-01  -0.574697E-01
  mo   11  -0.191697E+00   0.244018E+00  -0.142624E+00  -0.139551E+00  -0.255900E-01  -0.172127E-02
  mo   12   0.134623E-01   0.320632E+00   0.645309E-01   0.981291E-01   0.635943E-02  -0.561052E-02
  mo   13  -0.349950E+00   0.463647E+00   0.146250E+00  -0.184652E+00  -0.104680E+00  -0.770543E-02
  mo   14   0.701383E+00   0.432328E+00   0.108082E+00  -0.284949E+00  -0.240483E-01  -0.340482E-01
  eigenvectors of nos in ao-basis, column    1
  0.7596240E+00 -0.3250657E-01 -0.1378529E+00 -0.1583795E-01 -0.8259059E-02  0.2772672E-03 -0.3356788E-02 -0.5507169E+00
  0.1516840E+00 -0.9857933E-02 -0.2207942E-01 -0.3016600E-01 -0.2100310E-01 -0.2464141E-01
  eigenvectors of nos in ao-basis, column    2
 -0.1330735E+01  0.1116213E+00  0.5308295E+00 -0.3854093E-01 -0.5950941E-01  0.5995100E-05  0.3431454E-02 -0.1033302E+01
  0.4052177E+00 -0.1383655E-01 -0.5276543E-01 -0.3215153E-01 -0.7516077E-02  0.6201310E-03
  eigenvectors of nos in ao-basis, column    3
 -0.7400806E+00  0.3340858E+00  0.8681261E+00  0.2835796E+00  0.5406876E+00 -0.8257877E-02  0.1189343E-01  0.2370980E+00
 -0.8405366E-01 -0.3360534E-01  0.8961527E-01 -0.1385152E-01  0.3755566E-01  0.3156761E-01
  eigenvectors of nos in ao-basis, column    4
  0.1314242E+01 -0.1140621E+01 -0.1331650E+01  0.5230868E+00  0.4190440E+00  0.2533805E-01  0.9212513E-02 -0.1466355E+01
  0.3390171E+00  0.4145368E-01 -0.1176414E+00  0.1936128E+00 -0.1261981E+00 -0.9818855E-01
  eigenvectors of nos in ao-basis, column    5
  0.2060426E+01 -0.1823883E+01  0.3613620E+00 -0.2272800E+00 -0.2909936E+00 -0.2141211E-01  0.7912742E-01  0.1064795E+01
 -0.6017710E+00  0.3812194E-01  0.2069707E+00  0.1705294E+00  0.1172877E+00  0.2025579E+00
  eigenvectors of nos in ao-basis, column    6
 -0.6858735E+00  0.7395923E+00  0.1169448E+01 -0.1560155E+00 -0.1435901E+00  0.7247509E-01  0.2338275E+00  0.1051964E+01
 -0.6796891E-01 -0.4786097E-01 -0.2289382E+00  0.3984569E+00  0.1507960E+00  0.1303595E+00
  eigenvectors of nos in ao-basis, column    7
 -0.6916122E+00  0.1001643E+01 -0.1732185E-01  0.3639816E+00 -0.4351783E+00 -0.8195111E-01  0.4200596E+00  0.3348134E+00
 -0.5201771E+00  0.8363572E-01  0.1643012E+00  0.1195239E+00 -0.2048924E+00  0.9530416E-01
  eigenvectors of nos in ao-basis, column    8
  0.1307637E+01 -0.2196743E+01 -0.9778778E+00 -0.9399136E-01  0.7055440E+00  0.6830783E-01  0.3943813E+00 -0.2200139E+01
  0.1263276E+01 -0.1615701E+00 -0.1065567E+00 -0.8226864E+00 -0.1012624E+00 -0.2363643E+00
  eigenvectors of nos in ao-basis, column    9
  0.2601012E+00 -0.5797008E+00  0.1777693E+01  0.9582707E+00 -0.1391827E+01  0.3309308E-01 -0.1149916E+00  0.1857800E+01
 -0.5033280E-01 -0.5452664E-01 -0.1711696E+00 -0.2124713E+00  0.2645063E+00  0.3559956E+00
  eigenvectors of nos in ao-basis, column   10
 -0.1621555E+01  0.3194244E+01 -0.5154433E+00  0.1661677E+00 -0.1289518E+01  0.1043528E+00 -0.3849489E-01  0.2811238E+01
 -0.3453262E+01  0.3471012E+00  0.1146489E+01  0.3035181E+00 -0.4416928E+00 -0.9339623E-01
  eigenvectors of nos in ao-basis, column   11
  0.2204900E+00 -0.3076534E+00  0.3528512E+01  0.3225803E-01 -0.6295911E+00  0.3657231E-01 -0.8018703E-01  0.1857503E+01
  0.2146970E+01 -0.2873051E-01  0.4150924E+00  0.5167070E-01  0.3255897E+00  0.1792733E+01
  eigenvectors of nos in ao-basis, column   12
  0.5788487E+00 -0.1812923E+01 -0.2965963E+01 -0.2159812E+00  0.1633569E+01 -0.2046436E-01  0.1348853E+00 -0.3085999E+01
 -0.3411038E+01  0.4389844E+00 -0.1028660E+01 -0.1012531E+01 -0.1728332E+01 -0.3720026E-01
  eigenvectors of nos in ao-basis, column   13
  0.2551140E+00 -0.8554530E+00  0.2574904E+01  0.4450749E-01  0.1717388E+00 -0.1149726E-01  0.3599148E-01 -0.1542844E+00
  0.8715717E+01  0.2054448E+00 -0.4682941E+00 -0.1124433E+00  0.4548757E+01  0.1282968E+01
  eigenvectors of nos in ao-basis, column   14
 -0.7349008E-02 -0.2802918E-02 -0.1479154E+01 -0.4011105E-01  0.2379473E+00 -0.4807882E-02  0.1008538E-01 -0.4160513E+00
 -0.2485122E+01  0.2875087E+01 -0.1590839E+00 -0.6487009E-01 -0.4330686E+00 -0.7068797E+00


*****   symmetry block SYM3   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       1.97128952
       2       0.01328158     0.00562713
       3       0.01233584     0.00615105     0.00858163
       4       0.00423383    -0.00323843    -0.00184867     0.00460516
       5       0.00007539     0.00031431     0.00148504     0.00128531
       6      -0.00728040     0.00212292     0.00309761    -0.00113421
       7       0.00005212     0.00001953    -0.00002143    -0.00004829
       8      -0.00081481     0.00018457     0.00004243    -0.00050093
       9       0.00012015    -0.00023024     0.00023457     0.00110456

               Column   5     Column   6     Column   7     Column   8
       5       0.00139149
       6       0.00048269     0.00188467
       7      -0.00003805    -0.00000323     0.00038159
       8      -0.00003971     0.00006618     0.00000440     0.00037949
       9       0.00077175    -0.00000170     0.00000460    -0.00014504

               Column   9
       9       0.00076269

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7        eno   8
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    8   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno   9
 emo    9   0.100000E+01

 occupation numbers of nos
   1.9714933      0.0157254      0.0053196      0.0008011      0.0006258
   0.0003949      0.0003629      0.0001213      0.0000592

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7         no   8
  mo    1   0.999948E+00  -0.662495E-02  -0.289245E-02   0.675654E-02   0.201084E-02   0.570549E-03   0.733181E-03  -0.539756E-03
  mo    2   0.676792E-02   0.577538E+00  -0.121588E+00  -0.460857E+00   0.113349E+00   0.983936E-01   0.134738E+00   0.114089E+00
  mo    3   0.629755E-02   0.699115E+00   0.369951E+00  -0.257901E-01  -0.305086E+00  -0.121349E+00  -0.173058E+00   0.603288E-02
  mo    4   0.213767E-02  -0.310237E+00   0.747863E+00  -0.123448E+00  -0.354108E+00  -0.952635E-02  -0.605338E-01   0.237513E+00
  mo    5   0.446159E-04   0.655340E-01   0.441125E+00   0.609548E-01   0.596291E+00  -0.166966E+00  -0.113015E+00  -0.620566E+00
  mo    6  -0.368021E-02   0.276343E+00   0.778355E-01   0.858932E+00   0.715574E-02   0.184744E+00   0.184045E+00   0.153245E+00
  mo    7   0.263913E-04   0.492428E-03  -0.126938E-01  -0.176255E-01   0.295647E-01   0.765647E+00  -0.638298E+00  -0.702788E-01
  mo    8  -0.413259E-03   0.205476E-01  -0.875706E-01   0.105313E+00   0.326723E+00  -0.472342E+00  -0.608719E+00   0.521245E+00
  mo    9   0.621713E-04  -0.177323E-01   0.283841E+00  -0.137040E+00   0.552668E+00   0.322758E+00   0.351602E+00   0.495291E+00

                no   9
  mo    1  -0.806180E-03
  mo    2   0.620911E+00
  mo    3  -0.485676E+00
  mo    4   0.379011E+00
  mo    5   0.126821E+00
  mo    6   0.297087E+00
  mo    7   0.867142E-02
  mo    8   0.934319E-01
  mo    9  -0.348982E+00
  eigenvectors of nos in ao-basis, column    1
  0.9202112E+00 -0.6177763E-02  0.4147049E-01 -0.4934471E-02 -0.2001507E-01  0.1129883E-02  0.3285533E-03  0.2310777E-01
  0.3944411E-01
  eigenvectors of nos in ao-basis, column    2
 -0.1649601E+01  0.5303622E+00  0.1276136E+01 -0.1614596E-01 -0.7221764E-01  0.1534483E-03  0.4995090E-03  0.6456452E-01
  0.8937548E-01
  eigenvectors of nos in ao-basis, column    3
 -0.2292810E+00  0.9958624E-01  0.3541311E+00  0.3444073E+00  0.7113684E+00 -0.3181889E-02 -0.5120515E-02 -0.6955839E-01
 -0.5153440E-01
  eigenvectors of nos in ao-basis, column    4
 -0.2665021E+01  0.2735753E+01  0.4350454E+00 -0.1588729E+00  0.3022693E-01  0.7543077E-02 -0.4897849E-03 -0.3401591E-01
 -0.2061253E+00
  eigenvectors of nos in ao-basis, column    5
 -0.2158792E+00  0.2578590E+00 -0.6500475E+00  0.5730442E+00 -0.4251667E-01  0.8089588E-01  0.2982015E-01  0.4973464E+00
  0.1231167E+00
  eigenvectors of nos in ao-basis, column    6
 -0.3774872E+00  0.4820247E+00 -0.2274363E+00  0.4521702E+00 -0.3502636E+00 -0.1402687E+00  0.8865543E-01 -0.5584364E-01
  0.1458656E+00
  eigenvectors of nos in ao-basis, column    7
 -0.3680855E+00  0.4810967E+00 -0.3503993E+00  0.5274924E+00 -0.3886626E+00 -0.6037923E-01 -0.1254060E+00  0.2071605E-01
  0.1783743E+00
  eigenvectors of nos in ao-basis, column    8
 -0.4632302E+00  0.6932150E+00  0.2838704E+00  0.5064844E+00 -0.8969948E+00  0.1248346E+00  0.2769544E-01 -0.8797077E+00
  0.1990539E+00
  eigenvectors of nos in ao-basis, column    9
 -0.8280007E+00  0.1326731E+01 -0.1788530E+01 -0.4300009E+00  0.5656497E+00  0.2376438E-02  0.1039207E-02 -0.5105224E-01
  0.1038838E+01


*****   symmetry block SYM4   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       0.00577659
       2      -0.00072520     0.00080983
       3       0.00033119     0.00000999     0.00025339
       4       0.00016651    -0.00015231     0.00016562     0.00021193
       5      -0.00004933    -0.00013138    -0.00007128    -0.00001323

               Column   5
       5       0.00015402

               eno   1        eno   2        eno   3        eno   4        eno   5
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   0.0059061      0.0007673      0.0003902      0.0001049      0.0000372

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5
  mo    1   0.987609E+00   0.135990E+00  -0.643002E-01   0.252335E-01   0.369218E-01
  mo    2  -0.141293E+00   0.944492E+00   0.188685E-01   0.245026E+00   0.166065E+00
  mo    3   0.586972E-01   0.747369E-01   0.734873E+00   0.134995E+00  -0.657805E+00
  mo    4   0.343805E-01  -0.190789E+00   0.625317E+00   0.216184E+00   0.724335E+00
  mo    5  -0.604941E-02  -0.217844E+00  -0.253871E+00   0.935076E+00  -0.117007E+00
  eigenvectors of nos in ao-basis, column    1
 -0.3111157E+00 -0.6803183E+00  0.4203382E-01  0.1107314E+00  0.8976432E-01
  eigenvectors of nos in ao-basis, column    2
 -0.6023357E+00 -0.1202016E+00 -0.3295623E+00 -0.4978727E+00 -0.1492889E+00
  eigenvectors of nos in ao-basis, column    3
  0.5883433E+00 -0.4123214E+00 -0.7761911E+00 -0.3771954E-01  0.3159396E+00
  eigenvectors of nos in ao-basis, column    4
  0.6983927E+00 -0.1191995E+01  0.6135523E+00 -0.8176667E+00  0.7873494E-01
  eigenvectors of nos in ao-basis, column    5
  0.3491569E+00 -0.3497071E+00 -0.2325167E+00  0.3746672E+00 -0.1155899E+01


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***


 cosurf_and_phi
  1.262739748330  2.872870034186  1.567866969040 -0.042690763174
  1.652947872482  0.360731428865  3.496571379926 -0.051826900243
  0.594940552015  0.670555958619  3.845537291721 -0.062172020072
  2.390078334690  1.202722009900  2.566708449184 -0.033045680187
  1.989334673621  2.229216060677  2.001028520754 -0.035936958866
  2.919143888229  0.378144896597  2.099775822683 -0.012527109977
  0.105380882100  1.878884201281  3.371423292662 -0.062475727633
  0.783266721831  2.590884380442  2.520834408126 -0.055881108578
  2.011402526412  2.551496219550  0.814855178005 -0.017009633086
  0.099626804209  1.855073908563 -1.945822518898  0.030221575089
  0.119020599620  3.173161356543  1.415159765853 -0.048571336526
  0.915237473358  3.108118624501  0.463299650838 -0.030402903815
  0.462814589486  2.837210699514 -0.795516582628 -0.006613810588
  1.408719892640  1.640288870679  3.148120355694 -0.053583280764
  2.650053602975  1.633766196698  1.655441764425 -0.014163059216
  1.142904167226  2.885766749848 -0.243475252462 -0.012205184304
  3.656106873706  0.713798214804  0.361832640492  0.038220898821
  2.408046317685  2.075600470298 -1.226192756897  0.039161111639
  1.295820311657  0.567673501678 -2.747601655947  0.057125714232
  3.527730862121  1.045649613724 -1.064853177123  0.054485284785
  2.622898581638  1.024710819001 -2.241122746235  0.059042528425
  3.086808508398  1.794857685487 -0.179703829578  0.033989668069
  1.615872011596  1.674366500124 -2.147504385867  0.046831167977
  0.000006852884  1.035694667087 -2.361676372823  0.049290805588
  0.298815704890  0.969607820141 -2.408807386530  0.050873455375
  0.667723897920  1.245157743773 -2.338577856151  0.048566836025
  1.218123388571  2.025110412626 -1.616576841774  0.034478689437
  2.084186643139  2.293984793080 -0.480493513306  0.021296466585
  2.876642520254  1.658030225134  0.559035126479  0.020766288280
  3.282919570196  0.368088739237  1.091983758387  0.024263808442
  0.467932162408  1.694535407938 -1.941510815499  0.036935691295
  1.299770347024  2.453875604722 -0.850324284820  0.011433414527
  2.242050270258  2.245320705184  0.385739526123  0.002951017873
  2.923104801922  1.151131828431  1.279135167181  0.007157133857
  0.427044967836  0.580963354323 -2.585108185092  0.054622932203
 end_of_phi


 nsubv after electrostatic potential =  5

  Confirmation of dielectric energy for ground state
  edielnew =  -0.00252288786

 =========== Executing IN-CORE method ==========
 norm multnew:  1.71746648E-06
 threx: total number of loops= 14
 threx: total number of loops= 24
 blstr diagel,allext,nmin2=  1892
 onel.diag.
 -0.194545E+01-0.286130E+01-0.270402E+01-0.267939E+01-0.404309E+01-0.353003E+01-0.366094E+01-0.377999E+01-0.336133E+01-0.260063E+01
 -0.287415E+01-0.357359E+01-0.152440E+01-0.144558E+01-0.152216E+01-0.749910E+00 0.955054E+00-0.155502E+01-0.292594E+01-0.240588E+01
 -0.200637E+01-0.405451E+01-0.340880E+01-0.250541E+01-0.287248E+01-0.299025E+01-0.266613E+01-0.118104E+01-0.155136E+01-0.373927E+00
 -0.277866E+01-0.388024E+01-0.376232E+01-0.283288E+01-0.443810E+01-0.160170E+01-0.133510E+01-0.168075E+01-0.459117E+01-0.280777E+01
 -0.220460E+01-0.135625E+01-0.747718E+00-0.330495E+02-0.787969E+01-0.692041E+01-0.675761E+01-0.704586E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1      1892      1892
 blstr diagel,allext,nmin2=  1892
 onel.diag.
 -0.194545E+01-0.286130E+01-0.270402E+01-0.267939E+01-0.404309E+01-0.353003E+01-0.366094E+01-0.377999E+01-0.336133E+01-0.260063E+01
 -0.287415E+01-0.357359E+01-0.152440E+01-0.144558E+01-0.152216E+01-0.749910E+00 0.955054E+00-0.155502E+01-0.292594E+01-0.240588E+01
 -0.200637E+01-0.405451E+01-0.340880E+01-0.250541E+01-0.287248E+01-0.299025E+01-0.266613E+01-0.118104E+01-0.155136E+01-0.373927E+00
 -0.277866E+01-0.388024E+01-0.376232E+01-0.283288E+01-0.443810E+01-0.160170E+01-0.133510E+01-0.168075E+01-0.459117E+01-0.280777E+01
 -0.220460E+01-0.135625E+01-0.747718E+00-0.330495E+02-0.787969E+01-0.692041E+01-0.675761E+01-0.704586E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1      1892      1892
 blstr diagel,allext,nmin2=  1892
 onel.diag.
 -0.194545E+01-0.286130E+01-0.270402E+01-0.267939E+01-0.404309E+01-0.353003E+01-0.366094E+01-0.377999E+01-0.336133E+01-0.260063E+01
 -0.287415E+01-0.357359E+01-0.152440E+01-0.144558E+01-0.152216E+01-0.749910E+00 0.955054E+00-0.155502E+01-0.292594E+01-0.240588E+01
 -0.200637E+01-0.405451E+01-0.340880E+01-0.250541E+01-0.287248E+01-0.299025E+01-0.266613E+01-0.118104E+01-0.155136E+01-0.373927E+00
 -0.277866E+01-0.388024E+01-0.376232E+01-0.283288E+01-0.443810E+01-0.160170E+01-0.133510E+01-0.168075E+01-0.459117E+01-0.280777E+01
 -0.220460E+01-0.135625E+01-0.747718E+00-0.330495E+02-0.787969E+01-0.692041E+01-0.675761E+01-0.704586E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1      1892      1892


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        12 xx:        36 ww:        79
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        11
task #   9:        28    task #  10:        71    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 matrix-vector product formed for vector   6
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference-subspace vector overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     1.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.97234180     0.06649896    -0.09372291    -0.11116827     0.16959428    -0.01499117
 subspace has dimension  6

          (h-repnuc*1) matrix in the subspace basis

                x:   1         x:   2         x:   3         x:   4         x:   5         x:   6
   x:   1   -85.40875183
   x:   2    -0.31915444    -7.04150281
   x:   3    -0.00086869     0.28014244    -0.25399358
   x:   4     0.00008128    -0.02653816    -0.00113002    -0.01206080
   x:   5     0.00005627    -0.01833698    -0.00136609    -0.00078418    -0.00124049
   x:   6    -0.00000524     0.00170913    -0.00227837     0.00015158    -0.00008181    -0.00014129

          overlap matrix in the subspace basis

                x:   1         x:   2         x:   3         x:   4         x:   5         x:   6
   x:   1     1.00000000
   x:   2     0.00000000     0.08688345
   x:   3     0.00000000    -0.00342125     0.00311503
   x:   4     0.00000000     0.00030861     0.00000546     0.00014894
   x:   5     0.00000000     0.00021324     0.00001595     0.00000829     0.00001537
   x:   6     0.00000000    -0.00001987     0.00002659    -0.00000177     0.00000089     0.00000172

          eigenvectors and eigenvalues in the subspace basis

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6

   energy   -85.67270622   -83.87242874   -82.27013128   -81.02658902   -79.88734178   -75.49722472

   x:   1     0.97234180     0.06649896    -0.09372291    -0.11116827     0.16959428    -0.01499117
   x:   2     0.80206939    -0.32628674     0.95721790     1.55850361    -2.87974791     0.45558190
   x:   3     0.92355309    -6.20910557    -2.09711871   -11.90136825   -12.43528287    -6.95343020
   x:   4     0.98332672   -32.07854602    45.30491220   -30.77144998    16.49185920    53.00220966
   x:   5     1.02187713  -119.38631517    62.43555999    72.47156624   105.78678910  -194.30093656
   x:   6     0.95979172  -373.62919062  -442.74173349   318.30941836   148.78696359   495.61508837

          eigenvectors and eigenvalues in the schmidt orthonormalized basis

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6

   energy   -85.67270622   -83.87242874   -82.27013128   -81.02658902   -79.88734178   -75.49722472

  zx:   1     0.97234180     0.06649896    -0.09372291    -0.11116827     0.16959428    -0.01499117
  zx:   2     0.22740245    -0.11887282     0.42893809     0.59627477    -0.62073429     0.09651335
  zx:   3     0.05164541    -0.57917982    -0.28133052    -0.47685625    -0.55604153    -0.21485474
  zx:   4     0.01242710    -0.40555456     0.65604839    -0.37847011     0.24207764     0.45069987
  zx:   5     0.00407044    -0.53238164     0.13973377     0.34306026     0.43212192    -0.62658439
  zx:   6     0.00114333    -0.44507659    -0.52740521     0.37917827     0.17723881     0.59038929

          <ref|baseci> overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     1.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

 trial vector basis is being transformed.  new dimension:   3

          transformed tciref transformation matrix  block   1

               ref   1        ref   2        ref   3
  ci:   1     0.97234180     0.06649896    -0.09372291

  write the transformed vectors back to cosmofile

 rtflw =  0
 freezing =  F

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  6  1    -76.3366263449 -9.3361E+00  5.7116E-07  1.6747E-03  1.0000E-03
 mr-sdci #  6  2    -74.5363488619 -8.8073E+00  0.0000E+00  1.1755E+00  1.0000E-04
 mr-sdci #  6  3    -72.9340513976 -8.5138E+00  0.0000E+00  2.6992E+00  1.0000E-04
 mr-sdci #  6  4    -71.6905091441 -8.3275E+00  0.0000E+00  2.9454E+00  1.0000E-04
 mr-sdci #  6  5    -70.5512619009 -7.2197E+00  0.0000E+00  5.3037E+00  1.0000E-04
 mr-sdci #  6  6    -66.1611448382  6.6161E+01  0.0000E+00  3.9745E+00  1.0000E-04


 total energy in cosmo calc 
e(rootcalc) + repnuc=     -76.3366263449
dielectric energy =      -0.0095031456
deltaediel =       0.0095031456
e(rootcalc) + repnuc - ediel =     -76.3271231993
e(rootcalc) + repnuc - edielnew =     -76.3366263449
deltaelast =      76.3271231993

 root number  1 is used to define the new expansion vector.
 diagon: frcsub=2, new matrix-vector products will be computed.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.02   0.00   0.00   0.01         0.    0.0000
    2   25    0   0.01   0.00   0.00   0.01         0.    0.0000
    3   26    0   0.03   0.00   0.00   0.03         0.    0.0000
    4   11    0   0.01   0.00   0.00   0.01         0.    0.0000
    5   15    0   0.02   0.00   0.00   0.02         0.    0.0000
    6   16    0   0.02   0.00   0.00   0.02         0.    0.0000
    7    1    0   0.01   0.00   0.00   0.01         0.    0.0000
    8    5    0   0.02   0.00   0.00   0.02         0.    0.0000
    9    6    0   0.03   0.00   0.00   0.03         0.    0.0000
   10    7    0   0.03   0.00   0.00   0.03         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.01   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.03   0.00   0.00   0.03         0.    0.0000
   14   47    0   0.04   0.00   0.00   0.04         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.020000
time for cinew                         0.050000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.2800s 
time spent in multnx:                   0.2600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            1.5700s 

          starting ci iteration   7

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.33607988

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=       3  D0X=       9  D0W=      15
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================

          ci-one-electron density block   1

                mo   1         mo   2         mo   3         mo   4         mo   5         mo   6         mo   7         mo   8
   mo   1     1.99955736
   mo   2    -0.00013935     1.98335852
   mo   3    -0.00012984     0.00306309     1.96961568
   mo   4    -0.00014534     0.00137847     0.01213271     0.00120140
   mo   5     0.00019869     0.00395617    -0.00965634    -0.00247958     0.00655907
   mo   6    -0.00019410     0.00499584     0.00492763     0.00027422     0.00060765     0.00284306
   mo   7     0.00034905    -0.00580972    -0.01482014    -0.00100342     0.00053500    -0.00134524     0.00265161
   mo   8    -0.00024595    -0.00644597     0.00448562     0.00272829    -0.00787325    -0.00035665     0.00013682     0.01040960
   mo   9    -0.00031303     0.00611363     0.00790230     0.00018913     0.00127762     0.00291246    -0.00196075    -0.00126313
   mo  10     0.00012526     0.00202622    -0.00499756    -0.00063555     0.00154986     0.00211315     0.00067100    -0.00109957
   mo  11     0.00063814    -0.00071619     0.00021851    -0.00065978    -0.00029868    -0.00143055     0.00258011     0.00142140
   mo  12     0.00059764     0.00196887    -0.00497201    -0.00086579     0.00132042    -0.00136485     0.00132135    -0.00175083
   mo  13    -0.00000781    -0.00129834     0.00144741     0.00026257    -0.00090610     0.00036094     0.00028050     0.00148720
   mo  14    -0.00027426    -0.00392421     0.00506904     0.00087763    -0.00228909    -0.00028276    -0.00035048     0.00302729
   mo  15    -0.00031051     0.00202536    -0.00647680    -0.00013506     0.00088267     0.00089833    -0.00057140    -0.00118871
   mo  16     0.00001326    -0.00013521     0.00058847     0.00001755    -0.00006516    -0.00015944    -0.00000787     0.00004044
   mo  17    -0.00025022     0.00010587    -0.00203127     0.00007545     0.00004593    -0.00001329    -0.00036030    -0.00039726
   mo  18    -0.00005380     0.00020603    -0.00027979     0.00005246    -0.00011974    -0.00016256    -0.00008507     0.00012802
   mo  19    -0.00014746     0.00046395    -0.00049671     0.00009499    -0.00015696     0.00033661    -0.00010025     0.00029427
   mo  20     0.00087111     0.00221619    -0.00033270    -0.00020362     0.00023929    -0.00027896     0.00045337    -0.00025141

                mo   9         mo  10         mo  11         mo  12         mo  13         mo  14         mo  15         mo  16
   mo   9     0.00529077
   mo  10     0.00066080     0.00421742
   mo  11    -0.00074411    -0.00042678     0.00439782
   mo  12    -0.00238705     0.00007715     0.00013912     0.00253497
   mo  13    -0.00065195     0.00130002    -0.00011827    -0.00001720     0.00120841
   mo  14    -0.00070654    -0.00088581    -0.00032933    -0.00016059     0.00035851     0.00158300
   mo  15     0.00157440     0.00057022    -0.00074321    -0.00040755    -0.00029223    -0.00054201     0.00112582
   mo  16    -0.00002560    -0.00039651     0.00013420     0.00000541    -0.00007016     0.00004318    -0.00004164     0.00039312
   mo  17    -0.00017740    -0.00027420    -0.00094925     0.00035889    -0.00013827     0.00004008     0.00007544     0.00000844
   mo  18     0.00037118    -0.00084535     0.00051430    -0.00040239    -0.00060491    -0.00000483    -0.00000164     0.00008874
   mo  19     0.00046984     0.00038967     0.00017865    -0.00062328     0.00024319    -0.00023374     0.00009993    -0.00004445
   mo  20    -0.00040164     0.00009019     0.00054643     0.00048671    -0.00001342    -0.00021973    -0.00014339     0.00000590

                mo  17         mo  18         mo  19         mo  20
   mo  17     0.00060795
   mo  18    -0.00012403     0.00067247
   mo  19    -0.00022294    -0.00000215     0.00043382
   mo  20    -0.00006724    -0.00002342    -0.00007136     0.00038114

          ci-one-electron density block   2

                mo  21         mo  22         mo  23         mo  24         mo  25         mo  26         mo  27         mo  28
   mo  21     1.96820743
   mo  22     0.00183283     0.00062394
   mo  23    -0.01505895    -0.00190121     0.00719290
   mo  24    -0.00181623     0.00074839    -0.00144497     0.00145247
   mo  25     0.00190856     0.00051522    -0.00185023     0.00044232     0.00052096
   mo  26    -0.01449690    -0.00194812     0.00848965    -0.00087576    -0.00213906     0.01109290
   mo  27    -0.00362231    -0.00126881     0.00292261    -0.00224089    -0.00086800     0.00233526     0.00383118
   mo  28     0.00150564     0.00015598    -0.00051581     0.00026273     0.00004638    -0.00067009    -0.00038759     0.00055616
   mo  29     0.00918365     0.00036726    -0.00228735    -0.00023898     0.00059547    -0.00361488     0.00020747     0.00025756
   mo  30     0.00531200     0.00059198    -0.00174943     0.00089281     0.00047586    -0.00193295    -0.00189176     0.00018466
   mo  31    -0.00537495    -0.00023342     0.00064148    -0.00039702    -0.00022156     0.00064493     0.00080837    -0.00013185
   mo  32     0.00092906     0.00003688    -0.00008692     0.00009673    -0.00002307    -0.00007456    -0.00013976     0.00026135
   mo  33    -0.00389333    -0.00000954     0.00030041     0.00015469    -0.00007941     0.00072615    -0.00028607    -0.00005029
   mo  34    -0.00130664    -0.00004037    -0.00007341    -0.00019846     0.00000654    -0.00028920     0.00044871     0.00007214

                mo  29         mo  30         mo  31         mo  32         mo  33         mo  34
   mo  29     0.00205913
   mo  30     0.00005079     0.00149770
   mo  31    -0.00024549    -0.00043241     0.00059935
   mo  32     0.00001863     0.00007518    -0.00002329     0.00032409
   mo  33    -0.00073691     0.00007999     0.00000601    -0.00000833     0.00053950
   mo  34     0.00043109    -0.00043039     0.00004847     0.00000893    -0.00020235     0.00034714

          ci-one-electron density block   3

                mo  35         mo  36         mo  37         mo  38         mo  39         mo  40         mo  41         mo  42
   mo  35     1.97122645
   mo  36     0.01418242     0.00563546
   mo  37     0.01235041     0.00616343     0.00860248
   mo  38     0.00422209    -0.00324071    -0.00185395     0.00461782
   mo  39     0.00014518     0.00031779     0.00149184     0.00128695     0.00139222
   mo  40    -0.00727997     0.00212196     0.00310440    -0.00113728     0.00048407     0.00188609
   mo  41     0.00000552     0.00001926    -0.00002203    -0.00004913    -0.00003859    -0.00000306     0.00038060
   mo  42    -0.00078352     0.00018329     0.00004103    -0.00050113    -0.00003996     0.00006576     0.00000436     0.00037857
   mo  43     0.00014064    -0.00022896     0.00023690     0.00110513     0.00077126    -0.00000118     0.00000424    -0.00014487

                mo  43
   mo  43     0.00076107

          ci-one-electron density block   4

                mo  44         mo  45         mo  46         mo  47         mo  48
   mo  44     0.00578007
   mo  45    -0.00074015     0.00082439
   mo  46     0.00034799    -0.00000038     0.00026110
   mo  47     0.00015999    -0.00015230     0.00016798     0.00021160
   mo  48    -0.00005009    -0.00013262    -0.00007163    -0.00001360     0.00015422


*****   symmetry block SYM1   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       1.99955736
       2      -0.00013935     1.98335852
       3      -0.00012984     0.00306309     1.96961568
       4      -0.00014534     0.00137847     0.01213271     0.00120140
       5       0.00019869     0.00395617    -0.00965634    -0.00247958
       6      -0.00019410     0.00499584     0.00492763     0.00027422
       7       0.00034905    -0.00580972    -0.01482014    -0.00100342
       8      -0.00024595    -0.00644597     0.00448562     0.00272829
       9      -0.00031303     0.00611363     0.00790230     0.00018913
      10       0.00012526     0.00202622    -0.00499756    -0.00063555
      11       0.00063814    -0.00071619     0.00021851    -0.00065978
      12       0.00059764     0.00196887    -0.00497201    -0.00086579
      13      -0.00000781    -0.00129834     0.00144741     0.00026257
      14      -0.00027426    -0.00392421     0.00506904     0.00087763
      15      -0.00031051     0.00202536    -0.00647680    -0.00013506
      16       0.00001326    -0.00013521     0.00058847     0.00001755
      17      -0.00025022     0.00010587    -0.00203127     0.00007545
      18      -0.00005380     0.00020603    -0.00027979     0.00005246
      19      -0.00014746     0.00046395    -0.00049671     0.00009499
      20       0.00087111     0.00221619    -0.00033270    -0.00020362

               Column   5     Column   6     Column   7     Column   8
       5       0.00655907
       6       0.00060765     0.00284306
       7       0.00053500    -0.00134524     0.00265161
       8      -0.00787325    -0.00035665     0.00013682     0.01040960
       9       0.00127762     0.00291246    -0.00196075    -0.00126313
      10       0.00154986     0.00211315     0.00067100    -0.00109957
      11      -0.00029868    -0.00143055     0.00258011     0.00142140
      12       0.00132042    -0.00136485     0.00132135    -0.00175083
      13      -0.00090610     0.00036094     0.00028050     0.00148720
      14      -0.00228909    -0.00028276    -0.00035048     0.00302729
      15       0.00088267     0.00089833    -0.00057140    -0.00118871
      16      -0.00006516    -0.00015944    -0.00000787     0.00004044
      17       0.00004593    -0.00001329    -0.00036030    -0.00039726
      18      -0.00011974    -0.00016256    -0.00008507     0.00012802
      19      -0.00015696     0.00033661    -0.00010025     0.00029427
      20       0.00023929    -0.00027896     0.00045337    -0.00025141

               Column   9     Column  10     Column  11     Column  12
       9       0.00529077
      10       0.00066080     0.00421742
      11      -0.00074411    -0.00042678     0.00439782
      12      -0.00238705     0.00007715     0.00013912     0.00253497
      13      -0.00065195     0.00130002    -0.00011827    -0.00001720
      14      -0.00070654    -0.00088581    -0.00032933    -0.00016059
      15       0.00157440     0.00057022    -0.00074321    -0.00040755
      16      -0.00002560    -0.00039651     0.00013420     0.00000541
      17      -0.00017740    -0.00027420    -0.00094925     0.00035889
      18       0.00037118    -0.00084535     0.00051430    -0.00040239
      19       0.00046984     0.00038967     0.00017865    -0.00062328
      20      -0.00040164     0.00009019     0.00054643     0.00048671

               Column  13     Column  14     Column  15     Column  16
      13       0.00120841
      14       0.00035851     0.00158300
      15      -0.00029223    -0.00054201     0.00112582
      16      -0.00007016     0.00004318    -0.00004164     0.00039312
      17      -0.00013827     0.00004008     0.00007544     0.00000844
      18      -0.00060491    -0.00000483    -0.00000164     0.00008874
      19       0.00024319    -0.00023374     0.00009993    -0.00004445
      20      -0.00001342    -0.00021973    -0.00014339     0.00000590

               Column  17     Column  18     Column  19     Column  20
      17       0.00060795
      18      -0.00012403     0.00067247
      19      -0.00022294    -0.00000215     0.00043382
      20      -0.00006724    -0.00002342    -0.00007136     0.00038114

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7        eno   8
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    8   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno   9        eno  10        eno  11        eno  12        eno  13        eno  14        eno  15        eno  16
 emo    9   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   10   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   11   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   12   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   13   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   14   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   15   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   16   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno  17        eno  18        eno  19        eno  20
 emo   17   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   18   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   19   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   20   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   1.9995606      1.9841270      1.9692940      0.0195332      0.0110164
   0.0060606      0.0052826      0.0011318      0.0008435      0.0005707
   0.0004718      0.0003657      0.0003550      0.0001563      0.0001087
   0.0000688      0.0000562      0.0000319      0.0000081      0.0000004

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7         no   8
  mo    1   0.999936E+00   0.107100E-01   0.347527E-02   0.156251E-03   0.515356E-03  -0.225945E-03   0.607382E-04  -0.970155E-04
  mo    2  -0.972346E-02   0.976960E+00  -0.213087E+00   0.494309E-02  -0.351088E-02   0.111375E-03   0.459182E-03  -0.643495E-03
  mo    3  -0.567277E-02   0.213013E+00   0.976941E+00  -0.640825E-02  -0.774686E-02   0.366841E-02   0.167948E-02   0.127627E-02
  mo    4  -0.114676E-03   0.197882E-02   0.589148E-02   0.194543E+00   0.149313E+00  -0.111151E+00   0.520024E-01  -0.791497E-01
  mo    5   0.108670E-03   0.928722E-03  -0.526347E-02  -0.563942E+00  -0.889183E-01   0.650400E-01  -0.594447E-01   0.720590E-01
  mo    6  -0.136452E-03   0.300250E-02   0.191147E-02  -0.846861E-01   0.432689E+00   0.244928E+00   0.109997E+00   0.209859E+00
  mo    7   0.246380E-03  -0.446223E-02  -0.674248E-02   0.506586E-02  -0.376907E+00   0.362377E+00  -0.743007E-01   0.193697E+00
  mo    8  -0.105564E-03  -0.271584E-02   0.297586E-02   0.713891E+00   0.100761E+00   0.202924E+00  -0.158623E-02   0.200226E+00
  mo    9  -0.210188E-03   0.387916E-02   0.326999E-02  -0.150437E+00   0.565191E+00   0.108871E+00  -0.433087E+00   0.345683E+00
  mo   10   0.671419E-04   0.467962E-03  -0.271274E-02  -0.144178E+00   0.141944E+00   0.642265E+00   0.438705E+00  -0.305069E-01
  mo   11   0.323379E-03  -0.338176E-03   0.178415E-03   0.897498E-01  -0.339505E+00   0.439791E+00  -0.578350E+00   0.296767E-01
  mo   12   0.304684E-03   0.432092E-03  -0.270038E-02  -0.976739E-01  -0.356985E+00  -0.722097E-01   0.282964E+00   0.573412E+00
  mo   13  -0.174149E-05  -0.487762E-03   0.862152E-03   0.864479E-01   0.146927E-01   0.242616E+00   0.288405E+00  -0.201405E+00
  mo   14  -0.132948E-03  -0.139649E-02   0.295685E-02   0.217279E+00   0.188545E-01  -0.124000E+00   0.104342E+00   0.356877E+00
  mo   15  -0.147179E-03   0.308602E-03  -0.343510E-02  -0.102545E+00   0.182485E+00   0.100370E-01  -0.418664E-01   0.191282E+00
  mo   16   0.563775E-05  -0.380008E-05   0.307418E-03   0.829662E-02  -0.175891E-01  -0.481095E-01  -0.578724E-01   0.256857E-01
  mo   17  -0.120046E-03  -0.166128E-03  -0.101974E-02  -0.185058E-01   0.163056E-01  -0.169296E+00   0.135671E+00   0.205029E+00
  mo   18  -0.271787E-04   0.714075E-04  -0.158244E-03   0.152150E-01   0.410994E-02  -0.812172E-01  -0.247631E+00  -0.972133E-02
  mo   19  -0.747662E-04   0.175657E-03  -0.294300E-03   0.109110E-01   0.706075E-01   0.112541E+00  -0.493058E-01  -0.399032E+00
  mo   20   0.426224E-03   0.105861E-02  -0.408290E-03  -0.156651E-01  -0.924899E-01   0.579051E-01  -0.150997E-01   0.101245E+00

                no   9         no  10         no  11         no  12         no  13         no  14         no  15         no  16
  mo    1  -0.520423E-04   0.362235E-04  -0.324846E-03   0.176382E-03   0.192251E-03   0.305872E-04  -0.977742E-04   0.476365E-04
  mo    2  -0.100642E-02   0.261965E-03  -0.230851E-02   0.109149E-02   0.129492E-02  -0.287612E-03   0.412197E-03  -0.715784E-03
  mo    3   0.286725E-02  -0.349360E-03  -0.459590E-02   0.240691E-02   0.176561E-02   0.191666E-02   0.210767E-02  -0.209158E-02
  mo    4   0.140233E+00  -0.470626E-01   0.153997E+00  -0.888158E-01  -0.117426E+00  -0.107675E+00  -0.162878E+00   0.124048E+00
  mo    5  -0.386705E+00   0.580833E-01  -0.127740E+00   0.263829E-01   0.247906E-01   0.181401E+00   0.278693E+00  -0.240263E-01
  mo    6  -0.165815E+00  -0.320044E+00   0.247391E+00   0.126424E+00  -0.338301E-01   0.792721E-01  -0.349978E-01  -0.362869E-02
  mo    7   0.101858E+00  -0.185046E-01  -0.211271E+00   0.220158E+00   0.256927E+00   0.510577E-01  -0.680215E-01  -0.270167E+00
  mo    8  -0.326529E-01   0.767626E-03  -0.664770E-01   0.361060E-01   0.461493E-01   0.187290E+00   0.154732E+00  -0.879983E-01
  mo    9   0.191393E-02   0.298854E+00   0.120231E+00  -0.111245E+00   0.109904E+00   0.589459E-01  -0.198603E+00  -0.139744E-01
  mo   10   0.956334E-01  -0.227592E+00  -0.908099E-01   0.256671E-02  -0.561184E-01  -0.183772E+00  -0.569700E-01  -0.365522E-01
  mo   11  -0.312296E-01   0.514863E-01   0.143778E+00  -0.595197E-01  -0.994768E-02  -0.126508E+00  -0.122596E-02   0.256316E+00
  mo   12   0.149161E+00   0.794161E-01   0.199667E+00  -0.160273E+00  -0.213089E+00   0.326217E+00  -0.409904E+00   0.112146E+00
  mo   13  -0.193688E+00   0.491658E+00   0.108585E+00   0.564893E-01  -0.412888E-01   0.256045E+00   0.172259E+00   0.570679E+00
  mo   14  -0.567512E+00   0.588705E-01  -0.338003E+00  -0.751164E-01  -0.101866E+00  -0.212149E-01   0.140007E+00  -0.151159E+00
  mo   15   0.572851E+00   0.198044E+00  -0.538332E+00   0.606317E-01  -0.274303E+00   0.580523E-01   0.301589E+00   0.117246E+00
  mo   16  -0.551518E-01   0.187452E+00   0.207323E+00   0.811131E+00  -0.435360E+00  -0.112037E+00  -0.629134E-01  -0.162499E+00
  mo   17   0.208236E+00  -0.204823E-01   0.229092E+00   0.332329E+00   0.669392E+00   0.135217E+00   0.290597E+00   0.953533E-01
  mo   18  -0.276452E-01  -0.631863E+00  -0.118473E+00   0.160105E+00  -0.190603E+00   0.434342E+00   0.599322E-01   0.378782E+00
  mo   19   0.602236E-01   0.140735E+00  -0.735693E-02  -0.350879E-01  -0.867085E-02   0.679484E+00  -0.161448E+00  -0.446151E+00
  mo   20   0.144695E+00  -0.503977E-01   0.490968E+00  -0.276206E+00  -0.306953E+00   0.359548E-01   0.630078E+00  -0.288412E+00

                no  17         no  18         no  19         no  20
  mo    1  -0.516728E-04  -0.201397E-04  -0.825866E-05   0.684602E-06
  mo    2   0.904000E-03   0.780124E-03  -0.173130E-04  -0.172055E-03
  mo    3   0.859887E-03   0.197615E-02   0.193230E-02  -0.122561E-02
  mo    4  -0.613305E-01  -0.127734E+00  -0.255648E+00   0.843967E+00
  mo    5  -0.183210E+00  -0.102089E+00   0.387121E+00   0.430733E+00
  mo    6  -0.499604E+00   0.427183E+00  -0.193315E+00  -0.650446E-01
  mo    7   0.294489E+00   0.472402E+00  -0.218440E+00   0.275377E+00
  mo    8  -0.986528E-01  -0.286870E-01   0.547820E+00   0.112141E+00
  mo    9   0.402450E+00  -0.648061E-01   0.376244E-01   0.816900E-02
  mo   10   0.206836E+00  -0.444954E+00   0.288684E-01  -0.293526E-01
  mo   11  -0.349782E+00  -0.275275E+00  -0.188325E+00  -0.682844E-01
  mo   12  -0.767099E-01  -0.892638E-01   0.841011E-01  -0.383644E-01
  mo   13   0.173618E+00   0.212772E+00  -0.132117E+00  -0.419660E-03
  mo   14   0.348766E-01  -0.245094E+00  -0.486951E+00  -0.410847E-01
  mo   15  -0.244756E+00   0.251048E-01  -0.114023E+00  -0.462841E-02
  mo   16   0.535984E-01  -0.131980E+00   0.381530E-01   0.335074E-03
  mo   17  -0.696677E-01  -0.331344E+00  -0.170235E+00  -0.693326E-02
  mo   18   0.338172E+00  -0.613354E-01  -0.430172E-01  -0.571038E-02
  mo   19  -0.153069E+00  -0.202899E+00  -0.209215E+00  -0.243815E-01
  mo   20   0.216450E+00   0.523942E-01  -0.105650E+00   0.100466E-01
  eigenvectors of nos in ao-basis, column    1
  0.9974930E+00 -0.1492880E-01  0.3367805E-02  0.4271552E-02 -0.7891434E-02  0.3745602E-02  0.4562621E-03  0.1035103E-04
  0.3400227E-05  0.1113922E-04 -0.2592584E-03  0.2161013E-04  0.5484355E-04  0.7102880E-03 -0.1420610E-02  0.4908797E-04
  0.4681111E-03  0.2377160E-03 -0.5313482E-03 -0.5369029E-03
  eigenvectors of nos in ao-basis, column    2
  0.2992629E-02  0.9126446E+00 -0.2973975E-02 -0.1329349E+00  0.4594436E-01  0.1024304E-01  0.7432799E-01  0.1582440E-03
  0.2544856E-02 -0.1587968E-02 -0.2564464E-02 -0.7759330E-03 -0.1735969E-02  0.2567958E+00 -0.1114709E+00  0.3063202E-02
  0.2605005E-01  0.1919853E-01  0.1609660E-01  0.1214677E-01
  eigenvectors of nos in ao-basis, column    3
  0.9106914E-02  0.1086417E+00  0.3533536E-02  0.1553235E+00  0.8349296E+00 -0.2673964E-01 -0.2946696E-01 -0.1612611E-02
 -0.2676288E-02 -0.5423370E-02  0.1943959E-02 -0.7959965E-04  0.2673203E-02 -0.3988133E+00  0.1691930E+00  0.5835426E-02
 -0.2998253E-01 -0.1456553E-02 -0.1587568E-02  0.2943150E-01
  eigenvectors of nos in ao-basis, column    4
 -0.4320008E-01 -0.6094011E+00 -0.6231148E-01  0.2315242E-02  0.1321415E+01 -0.2010508E+00 -0.6931985E+00  0.3371956E-02
  0.1548555E-01  0.8665938E-02  0.2564778E-01  0.2157695E-02  0.3614463E-02  0.9575407E+00 -0.4165919E+00 -0.8311893E-02
  0.5935972E-01  0.9668824E-02  0.3721548E-02 -0.3721069E-01
  eigenvectors of nos in ao-basis, column    5
  0.1517366E-01 -0.1072073E+01 -0.4388322E+00  0.1100500E+01 -0.8227998E+00  0.3822855E+00  0.8200179E+00 -0.5137802E-02
  0.3182000E-01 -0.2359161E-01  0.4191383E-01 -0.2421752E-02 -0.3014639E-02  0.4374204E+00 -0.2477208E+00  0.2004053E-01
  0.2560040E-01  0.7901747E-01  0.1331975E-01  0.5735146E-01
  eigenvectors of nos in ao-basis, column    6
 -0.1067313E-01  0.4843070E+00  0.2547535E+00 -0.9493593E+00 -0.7175492E-01  0.3196853E-01  0.1434058E+00 -0.3515952E-01
  0.1152553E+00 -0.8753063E-01  0.2526922E+00 -0.1431794E-01 -0.2088636E-01  0.3137642E+00 -0.1499474E+00 -0.1171226E-01
 -0.3230137E-01  0.8245087E-01 -0.3426928E-01  0.2182498E-01
  eigenvectors of nos in ao-basis, column    7
 -0.2419408E-02 -0.1371966E+00 -0.5657741E-01  0.2201530E+00  0.2755324E+00 -0.1203344E+00 -0.4008650E+00 -0.8672859E-01
 -0.8415505E-01 -0.1766894E+00 -0.1645754E+00 -0.9839016E-02  0.1363604E-01 -0.8126851E-01  0.6181014E-01  0.5462981E-02
 -0.5185692E-01  0.4026311E-01 -0.3435624E-02  0.3938397E-01
  eigenvectors of nos in ao-basis, column    8
 -0.1244317E+00  0.2238270E-01  0.4323917E+00 -0.1915448E+01 -0.2181353E+00  0.3019594E+00  0.9734924E+00  0.2010300E-01
 -0.2460414E+00  0.1530906E-01 -0.2150692E+00 -0.3378320E-01 -0.8728883E-01  0.2108433E+01 -0.1007224E+01 -0.2527735E-02
  0.6934244E-01  0.1636427E+00  0.6605434E-01  0.6784255E-01
  eigenvectors of nos in ao-basis, column    9
 -0.2192906E+00 -0.2813455E+00  0.4162043E+00  0.2826825E+00 -0.2529913E+01  0.2373923E+01  0.4556119E+00 -0.2156376E-01
 -0.4405011E-01  0.1237629E-01  0.3796423E-01  0.8990788E-02  0.4089662E-01 -0.3573088E-01  0.1339095E+00 -0.3922982E-01
 -0.1687495E+00 -0.1022715E+00 -0.5555299E-01 -0.1452902E+00
  eigenvectors of nos in ao-basis, column   10
  0.1649895E+00  0.3673727E+00 -0.1997272E+00 -0.6520436E+00 -0.1575746E+00  0.2017908E+00  0.8292944E+00 -0.2106796E+00
  0.3089670E-01  0.1300390E-01  0.1528663E-01  0.8695060E-01 -0.1179257E+00  0.6625129E+00 -0.3622163E+00 -0.1073641E-01
  0.3316730E+00 -0.2871691E+00  0.9405101E-01 -0.1768651E+00
  eigenvectors of nos in ao-basis, column   11
 -0.1374718E+01 -0.2879903E+01  0.1996457E+01  0.1916733E+01  0.8303378E+00 -0.1105794E+01  0.6003641E-01 -0.3372674E-01
 -0.1034796E+00  0.1706304E-01  0.1390104E+00  0.1434034E+00  0.1631889E+00 -0.2266537E+00  0.2190945E+00 -0.3180151E-02
 -0.1466120E+00 -0.1967365E+00 -0.2539136E-01  0.9213536E-01
  eigenvectors of nos in ao-basis, column   12
  0.8890519E+00  0.1959105E+01 -0.1250359E+01 -0.8513178E+00 -0.1898537E+00  0.2149756E+00 -0.3278368E+00  0.7909297E-01
 -0.1467507E+00 -0.4193495E-01  0.1148966E+00  0.3094920E+00 -0.7302011E-01 -0.2246649E+00  0.2723496E-01 -0.1335346E-01
 -0.6133754E-01  0.1533394E-01 -0.1615038E+00  0.4820459E-01
  eigenvectors of nos in ao-basis, column   13
  0.1017827E+01  0.2256792E+01 -0.1424024E+01 -0.7262707E+00  0.2384398E+00 -0.3797197E+00 -0.1147249E-01 -0.9679579E-01
 -0.2546278E+00  0.8080337E-01  0.2299535E+00 -0.7132624E-01  0.3152336E+00 -0.1912218E+00 -0.1035604E+00 -0.2211401E-01
 -0.9896732E-01 -0.6925617E-01 -0.1505444E+00 -0.1367799E+00
  eigenvectors of nos in ao-basis, column   14
  0.3071903E+00  0.8626988E+00 -0.1853923E+00 -0.3171974E+01 -0.7875034E+00  0.1248210E+01  0.1073178E+01  0.9128547E-01
  0.3120410E+00 -0.1708038E+00 -0.4948029E+00  0.6555094E-01  0.3255430E+00  0.2750124E+01 -0.1266834E+01  0.2201254E-01
  0.4398567E+00 -0.1142922E+00  0.1787669E+00  0.2349585E+00
  eigenvectors of nos in ao-basis, column   15
 -0.2221980E+01 -0.5899281E+01  0.2477974E+01  0.3672693E+01 -0.1230595E+01  0.2407338E+01 -0.2942883E+00  0.2137583E-01
 -0.1634801E+00 -0.7401301E-01 -0.4821602E-01  0.1696024E-01  0.1249788E+00  0.1663430E+01 -0.1567897E+01  0.1164463E+00
  0.1026850E+01  0.5006698E+00 -0.1694551E+00  0.1005849E-01
  eigenvectors of nos in ao-basis, column   16
  0.1086159E+01  0.2941929E+01 -0.1136353E+01 -0.3538846E+01 -0.4313909E-01 -0.9471576E-01  0.4830128E+00  0.1523198E+00
 -0.2264979E+00 -0.3121388E+00  0.4845848E+00 -0.1341170E+00 -0.7828869E-01 -0.7052678E+00  0.1287666E+01 -0.6453661E-01
  0.2982478E+00 -0.8414436E+00  0.3933273E+00  0.3978022E+00
  eigenvectors of nos in ao-basis, column   17
 -0.9994437E+00 -0.2727735E+01  0.9804528E+00  0.5103202E+01  0.6300695E+00 -0.9840635E+00  0.8545537E+00  0.1319490E+00
 -0.2801133E-01 -0.1903242E+00  0.6543332E-01 -0.2437318E-02  0.1587134E-01 -0.1552357E+00 -0.9843504E+00 -0.3447527E-01
  0.2754835E-01 -0.9011680E-01 -0.4772720E+00 -0.1200559E+01
  eigenvectors of nos in ao-basis, column   18
 -0.6109977E+00 -0.1739881E+01  0.4285615E+00  0.6438803E+01  0.1961278E+00 -0.5451048E+00 -0.1369263E+01 -0.1904865E-02
  0.9849894E-02 -0.4706853E-01  0.2769444E+00 -0.1199206E+00 -0.1727046E+00 -0.2153617E+01 -0.2344987E+00  0.4821368E-02
 -0.1744614E+00 -0.7460114E+00 -0.1284979E+01 -0.2293459E+00
  eigenvectors of nos in ao-basis, column   19
  0.1850062E+00  0.5334593E+00 -0.3054415E+00  0.3249741E+01  0.3490282E+00 -0.1175965E+01 -0.1295815E+01  0.5759868E-02
 -0.5781812E-01  0.2713184E-01  0.4308858E+00 -0.4422732E-01 -0.1555472E+00 -0.2378047E+01 -0.1804506E+00  0.2805055E+00
 -0.1055738E+01 -0.6589203E+00  0.3808246E+00  0.6042516E-01
  eigenvectors of nos in ao-basis, column   20
 -0.1547420E+00 -0.4785998E+00  0.6406792E-01  0.2364466E+01  0.9901412E-02 -0.5998276E-01 -0.5714650E+00 -0.2275080E-03
 -0.1201518E-01  0.2687844E-02  0.6274605E-01 -0.5226971E-02 -0.1458856E-01 -0.3074903E+00 -0.1249505E+01  0.8894438E+00
 -0.1216216E+00 -0.9018576E-01 -0.3100314E+00 -0.2495335E+00


*****   symmetry block SYM2   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       1.96820743
       2       0.00183283     0.00062394
       3      -0.01505895    -0.00190121     0.00719290
       4      -0.00181623     0.00074839    -0.00144497     0.00145247
       5       0.00190856     0.00051522    -0.00185023     0.00044232
       6      -0.01449690    -0.00194812     0.00848965    -0.00087576
       7      -0.00362231    -0.00126881     0.00292261    -0.00224089
       8       0.00150564     0.00015598    -0.00051581     0.00026273
       9       0.00918365     0.00036726    -0.00228735    -0.00023898
      10       0.00531200     0.00059198    -0.00174943     0.00089281
      11      -0.00537495    -0.00023342     0.00064148    -0.00039702
      12       0.00092906     0.00003688    -0.00008692     0.00009673
      13      -0.00389333    -0.00000954     0.00030041     0.00015469
      14      -0.00130664    -0.00004037    -0.00007341    -0.00019846

               Column   5     Column   6     Column   7     Column   8
       5       0.00052096
       6      -0.00213906     0.01109290
       7      -0.00086800     0.00233526     0.00383118
       8       0.00004638    -0.00067009    -0.00038759     0.00055616
       9       0.00059547    -0.00361488     0.00020747     0.00025756
      10       0.00047586    -0.00193295    -0.00189176     0.00018466
      11      -0.00022156     0.00064493     0.00080837    -0.00013185
      12      -0.00002307    -0.00007456    -0.00013976     0.00026135
      13      -0.00007941     0.00072615    -0.00028607    -0.00005029
      14       0.00000654    -0.00028920     0.00044871     0.00007214

               Column   9     Column  10     Column  11     Column  12
       9       0.00205913
      10       0.00005079     0.00149770
      11      -0.00024549    -0.00043241     0.00059935
      12       0.00001863     0.00007518    -0.00002329     0.00032409
      13      -0.00073691     0.00007999     0.00000601    -0.00000833
      14       0.00043109    -0.00043039     0.00004847     0.00000893

               Column  13     Column  14
      13       0.00053950
      14      -0.00020235     0.00034714

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7        eno   8
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    8   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno   9        eno  10        eno  11        eno  12        eno  13        eno  14
 emo    9   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   10   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   11   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   12   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   13   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   14   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   1.9685263      0.0211914      0.0058093      0.0010724      0.0008229
   0.0006423      0.0003901      0.0001637      0.0000985      0.0000945
   0.0000180      0.0000100      0.0000052      0.0000002

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7         no   8
  mo    1   0.999918E+00   0.118704E-01  -0.187267E-02  -0.154383E-02  -0.221267E-02   0.664566E-03   0.109188E-02  -0.157349E-02
  mo    2   0.949355E-03  -0.149587E+00  -0.131681E+00   0.173766E-01  -0.219110E+00  -0.400356E-01   0.648991E-01   0.751206E-01
  mo    3  -0.772282E-02   0.570284E+00   0.188017E-01   0.214526E+00   0.358372E+00  -0.347099E-01   0.681944E-01  -0.241423E+00
  mo    4  -0.910284E-03  -0.115954E+00  -0.414803E+00   0.256795E+00  -0.286076E+00  -0.435005E-01   0.190970E+00  -0.110201E+00
  mo    5   0.988421E-03  -0.148709E+00  -0.273090E-01  -0.298143E-01  -0.974403E-01  -0.194511E+00  -0.567036E-01   0.157012E+00
  mo    6  -0.745598E-02   0.699065E+00  -0.309127E+00   0.141337E+00  -0.194949E+00  -0.103653E+00  -0.147439E-02   0.195050E+00
  mo    7  -0.186721E-02   0.238369E+00   0.662792E+00  -0.204914E+00   0.131448E-01   0.110290E+00  -0.519286E-01   0.878832E-01
  mo    8   0.771060E-03  -0.479779E-01  -0.303322E-01   0.362264E+00   0.322709E-01   0.703380E+00  -0.871661E-01  -0.400602E+00
  mo    9   0.469394E-02  -0.201301E+00   0.359528E+00   0.553392E+00   0.116254E+00  -0.232116E+00   0.209975E-01  -0.113738E-01
  mo   10   0.271728E-02  -0.154277E+00  -0.295587E+00  -0.129029E+00   0.725416E+00   0.226045E-01   0.187928E+00  -0.167517E-01
  mo   11  -0.273808E-02   0.573984E-01   0.117140E+00  -0.316279E+00  -0.220871E+00   0.224448E+00   0.785211E+00  -0.173390E+00
  mo   12   0.473001E-03  -0.799623E-02  -0.256997E-01   0.177237E+00   0.292567E-01   0.530379E+00   0.329951E-01   0.754460E+00
  mo   13  -0.198355E-02   0.339020E-01  -0.147501E+00  -0.411885E+00  -0.119358E+00   0.231833E+00  -0.534935E+00  -0.219907E+00
  mo   14  -0.662192E-03  -0.731592E-02   0.144748E+00   0.263921E+00  -0.298953E+00  -0.769563E-03  -0.399643E-01  -0.212084E+00

                no   9         no  10         no  11         no  12         no  13         no  14
  mo    1   0.100024E-03   0.161618E-02   0.106645E-02   0.121710E-02   0.154705E-02   0.310148E-03
  mo    2  -0.426071E-01  -0.121543E+00   0.319779E+00  -0.152567E+00  -0.187026E-01   0.870655E+00
  mo    3  -0.604920E-01   0.355442E+00   0.122052E+00   0.356542E+00   0.299940E+00   0.271642E+00
  mo    4  -0.183341E+00  -0.169693E-01   0.628018E+00   0.170373E+00   0.132828E+00  -0.375798E+00
  mo    5  -0.112333E+00   0.101733E+00  -0.192456E+00  -0.306762E+00   0.863815E+00  -0.276810E-01
  mo    6  -0.818403E-01  -0.182890E+00  -0.107579E+00  -0.478064E+00  -0.150733E+00  -0.770411E-01
  mo    7  -0.155893E-01  -0.274299E+00   0.545125E+00  -0.176425E+00   0.147180E+00  -0.117068E+00
  mo    8  -0.687069E-01  -0.340343E+00  -0.154743E+00  -0.157555E+00   0.174518E+00   0.435660E-01
  mo    9  -0.504501E+00   0.242861E+00  -0.168735E-01  -0.289584E+00  -0.241149E+00  -0.137723E-01
  mo   10   0.167832E+00   0.412147E-01   0.249149E+00  -0.462243E+00  -0.315549E-01  -0.568245E-01
  mo   11  -0.189612E+00   0.246310E+00  -0.144077E+00  -0.138635E+00  -0.236966E-01  -0.171945E-02
  mo   12   0.187658E-01   0.318227E+00   0.644663E-01   0.978078E-01   0.556446E-02  -0.567807E-02
  mo   13  -0.344279E+00   0.469773E+00   0.143085E+00  -0.185525E+00  -0.103782E+00  -0.735093E-02
  mo   14   0.706477E+00   0.424760E+00   0.104741E+00  -0.285030E+00  -0.226597E-01  -0.336583E-01
  eigenvectors of nos in ao-basis, column    1
  0.7595938E+00 -0.3236666E-01 -0.1378973E+00 -0.1583244E-01 -0.8356109E-02  0.2769335E-03 -0.3344797E-02 -0.5505627E+00
  0.1516254E+00 -0.9761134E-02 -0.2203553E-01 -0.3011728E-01 -0.2090223E-01 -0.2458921E-01
  eigenvectors of nos in ao-basis, column    2
 -0.1330281E+01  0.1105122E+00  0.5306830E+00 -0.3834510E-01 -0.5903664E-01  0.1079267E-04  0.3447848E-02 -0.1034288E+01
  0.4045645E+00 -0.1436300E-01 -0.5295667E-01 -0.3243558E-01 -0.8485505E-02  0.4630568E-03
  eigenvectors of nos in ao-basis, column    3
 -0.7400395E+00  0.3335840E+00  0.8697745E+00  0.2831948E+00  0.5409876E+00 -0.8283576E-02  0.1188962E-01  0.2373408E+00
 -0.8342266E-01 -0.3405886E-01  0.8932911E-01 -0.1415882E-01  0.3704516E-01  0.3226412E-01
  eigenvectors of nos in ao-basis, column    4
  0.1317762E+01 -0.1143574E+01 -0.1334239E+01  0.5229614E+00  0.4192495E+00  0.2527181E-01  0.8936933E-02 -0.1466213E+01
  0.3368290E+00  0.4197791E-01 -0.1167177E+00  0.1931389E+00 -0.1259845E+00 -0.9874196E-01
  eigenvectors of nos in ao-basis, column    5
  0.2062853E+01 -0.1828813E+01  0.3637483E+00 -0.2271104E+00 -0.2910267E+00 -0.2173489E-01  0.7896649E-01  0.1062079E+01
 -0.5951687E+00  0.3711209E-01  0.2076001E+00  0.1682755E+00  0.1177430E+00  0.2036321E+00
  eigenvectors of nos in ao-basis, column    6
 -0.6771665E+00  0.7316035E+00  0.1169408E+01 -0.1552921E+00 -0.1445053E+00  0.7229899E-01  0.2349142E+00  0.1052814E+01
 -0.6812083E-01 -0.4786822E-01 -0.2284842E+00  0.3992123E+00  0.1508914E+00  0.1311352E+00
  eigenvectors of nos in ao-basis, column    7
 -0.6916391E+00  0.1001442E+01 -0.2141979E-01  0.3652539E+00 -0.4354354E+00 -0.8194065E-01  0.4197833E+00  0.3322155E+00
 -0.5226212E+00  0.8408632E-01  0.1647688E+00  0.1182711E+00 -0.2061417E+00  0.9411596E-01
  eigenvectors of nos in ao-basis, column    8
  0.1300673E+01 -0.2185049E+01 -0.9871596E+00 -0.9556604E-01  0.7061165E+00  0.6858634E-01  0.3941824E+00 -0.2198999E+01
  0.1250788E+01 -0.1602802E+00 -0.1036132E+00 -0.8223675E+00 -0.1040981E+00 -0.2384696E+00
  eigenvectors of nos in ao-basis, column    9
  0.2431215E+00 -0.5451397E+00  0.1772591E+01  0.9600496E+00 -0.1406496E+01  0.3457435E-01 -0.1150939E+00  0.1889016E+01
 -0.8496414E-01 -0.5120862E-01 -0.1569899E+00 -0.2102114E+00  0.2607258E+00  0.3555663E+00
  eigenvectors of nos in ao-basis, column   10
 -0.1628106E+01  0.3208509E+01 -0.5183647E+00  0.1544479E+00 -0.1278985E+01  0.1039041E+00 -0.3884082E-01  0.2807333E+01
 -0.3451144E+01  0.3475854E+00  0.1151804E+01  0.3103640E+00 -0.4437828E+00 -0.9156939E-01
  eigenvectors of nos in ao-basis, column   11
  0.2298438E+00 -0.3319285E+00  0.3522502E+01  0.3054760E-01 -0.6142792E+00  0.3607873E-01 -0.7882098E-01  0.1826647E+01
  0.2179700E+01 -0.2739454E-01  0.4032905E+00  0.4407831E-01  0.3372632E+00  0.1798251E+01
  eigenvectors of nos in ao-basis, column   12
  0.5773758E+00 -0.1812258E+01 -0.2969971E+01 -0.2155507E+00  0.1635725E+01 -0.2065248E-01  0.1353106E+00 -0.3091675E+01
 -0.3374813E+01  0.4439349E+00 -0.1032713E+01 -0.1012629E+01 -0.1701495E+01 -0.3961868E-01
  eigenvectors of nos in ao-basis, column   13
  0.2487171E+00 -0.8399244E+00  0.2576537E+01  0.4557365E-01  0.1635983E+00 -0.1149572E-01  0.3552855E-01 -0.1412635E+00
  0.8722609E+01  0.2065714E+00 -0.4628625E+00 -0.1058686E+00  0.4557543E+01  0.1274281E+01
  eigenvectors of nos in ao-basis, column   14
 -0.8016112E-02 -0.5385187E-03 -0.1477668E+01 -0.3989612E-01  0.2357839E+00 -0.4751238E-02  0.9846532E-02 -0.4119625E+00
 -0.2491716E+01  0.2874319E+01 -0.1573734E+00 -0.6377955E-01 -0.4368448E+00 -0.7077955E+00


*****   symmetry block SYM3   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       1.97122645
       2       0.01418242     0.00563546
       3       0.01235041     0.00616343     0.00860248
       4       0.00422209    -0.00324071    -0.00185395     0.00461782
       5       0.00014518     0.00031779     0.00149184     0.00128695
       6      -0.00727997     0.00212196     0.00310440    -0.00113728
       7       0.00000552     0.00001926    -0.00002203    -0.00004913
       8      -0.00078352     0.00018329     0.00004103    -0.00050113
       9       0.00014064    -0.00022896     0.00023690     0.00110513

               Column   5     Column   6     Column   7     Column   8
       5       0.00139222
       6       0.00048407     0.00188609
       7      -0.00003859    -0.00000306     0.00038060
       8      -0.00003996     0.00006576     0.00000436     0.00037857
       9       0.00077126    -0.00000118     0.00000424    -0.00014487

               Column   9
       9       0.00076107

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7        eno   8
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    8   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno   9
 emo    9   0.100000E+01

 occupation numbers of nos
   1.9714430      0.0157491      0.0053302      0.0007996      0.0006243
   0.0003939      0.0003617      0.0001207      0.0000583

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7         no   8
  mo    1   0.999945E+00  -0.689833E-02  -0.285379E-02   0.696950E-02   0.190979E-02   0.553356E-03   0.666986E-03  -0.585147E-03
  mo    2   0.722641E-02   0.576912E+00  -0.121613E+00  -0.460302E+00   0.113901E+00   0.982735E-01   0.134609E+00   0.112054E+00
  mo    3   0.630668E-02   0.699510E+00   0.369821E+00  -0.270521E-01  -0.304477E+00  -0.121237E+00  -0.172923E+00   0.817235E-02
  mo    4   0.213100E-02  -0.310370E+00   0.748354E+00  -0.123649E+00  -0.353328E+00  -0.995739E-02  -0.608003E-01   0.236090E+00
  mo    5   0.801565E-04   0.658863E-01   0.440778E+00   0.621070E-01   0.596639E+00  -0.166042E+00  -0.113119E+00  -0.621059E+00
  mo    6  -0.367954E-02   0.276434E+00   0.775294E-01   0.859265E+00   0.473856E-02   0.184059E+00   0.183785E+00   0.151712E+00
  mo    7   0.274909E-05   0.490447E-03  -0.128700E-01  -0.174200E-01   0.287200E-01   0.766196E+00  -0.637687E+00  -0.702330E-01
  mo    8  -0.397358E-03   0.203946E-01  -0.875502E-01   0.105916E+00   0.327545E+00  -0.471350E+00  -0.608836E+00   0.521150E+00
  mo    9   0.725399E-04  -0.175348E-01   0.283329E+00  -0.135413E+00   0.552598E+00   0.323837E+00   0.352678E+00   0.496364E+00

                no   9
  mo    1  -0.109510E-02
  mo    2   0.622210E+00
  mo    3  -0.485564E+00
  mo    4   0.379432E+00
  mo    5   0.124330E+00
  mo    6   0.297539E+00
  mo    7   0.847629E-02
  mo    8   0.946863E-01
  mo    9  -0.346536E+00
  eigenvectors of nos in ao-basis, column    1
  0.9200172E+00 -0.6139791E-02  0.4114921E-01 -0.4928421E-02 -0.1998519E-01  0.1135134E-02  0.3261483E-03  0.2312420E-01
  0.3983754E-01
  eigenvectors of nos in ao-basis, column    2
 -0.1650221E+01  0.5303257E+00  0.1277394E+01 -0.1588455E-01 -0.7205264E-01  0.1298833E-03  0.4916372E-03  0.6486974E-01
  0.8856157E-01
  eigenvectors of nos in ao-basis, column    3
 -0.2279462E+00  0.9874782E-01  0.3535999E+00  0.3438073E+00  0.7116706E+00 -0.3179056E-02 -0.5150653E-02 -0.6992778E-01
 -0.5135385E-01
  eigenvectors of nos in ao-basis, column    4
 -0.2665817E+01  0.2737689E+01  0.4320021E+00 -0.1571131E+00  0.2998879E-01  0.7701466E-02 -0.4058278E-03 -0.3334935E-01
 -0.2050210E+00
  eigenvectors of nos in ao-basis, column    5
 -0.2087913E+00  0.2502701E+00 -0.6499778E+00  0.5727811E+00 -0.4160116E-01  0.8114639E-01  0.2974879E-01  0.4973543E+00
  0.1229928E+00
  eigenvectors of nos in ao-basis, column    6
 -0.3759162E+00  0.4800452E+00 -0.2270269E+00  0.4532224E+00 -0.3503831E+00 -0.1400697E+00  0.8880886E-01 -0.5523328E-01
  0.1453597E+00
  eigenvectors of nos in ao-basis, column    7
 -0.3673514E+00  0.4800987E+00 -0.3496450E+00  0.5287609E+00 -0.3896458E+00 -0.6040629E-01 -0.1253081E+00  0.2065074E-01
  0.1779460E+00
  eigenvectors of nos in ao-basis, column    8
 -0.4591845E+00  0.6868463E+00  0.2909748E+00  0.5077703E+00 -0.8989441E+00  0.1248657E+00  0.2771628E-01 -0.8795300E+00
  0.1951675E+00
  eigenvectors of nos in ao-basis, column    9
 -0.8298977E+00  0.1328659E+01 -0.1787565E+01 -0.4272971E+00  0.5615123E+00  0.2723521E-02  0.1120399E-02 -0.5417302E-01
  0.1040016E+01


*****   symmetry block SYM4   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       0.00578007
       2      -0.00074015     0.00082439
       3       0.00034799    -0.00000038     0.00026110
       4       0.00015999    -0.00015230     0.00016798     0.00021160
       5      -0.00005009    -0.00013262    -0.00007163    -0.00001360

               Column   5
       5       0.00015422

               eno   1        eno   2        eno   3        eno   4        eno   5
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   0.0059158      0.0007763      0.0003953      0.0001046      0.0000394

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5
  mo    1   0.987014E+00   0.139563E+00  -0.646769E-01   0.259164E-01   0.383491E-01
  mo    2  -0.144329E+00   0.945223E+00   0.284413E-01   0.245705E+00   0.156660E+00
  mo    3   0.618187E-01   0.609179E-01   0.740870E+00   0.132381E+00  -0.652729E+00
  mo    4   0.333720E-01  -0.192078E+00   0.616463E+00   0.221773E+00   0.729919E+00
  mo    5  -0.610553E-02  -0.215569E+00  -0.257086E+00   0.933943E+00  -0.123085E+00
  eigenvectors of nos in ao-basis, column    1
 -0.3102027E+00 -0.6788137E+00  0.4196833E-01  0.1111769E+00  0.9443259E-01
  eigenvectors of nos in ao-basis, column    2
 -0.6069643E+00 -0.1202292E+00 -0.3220348E+00 -0.4942486E+00 -0.1621506E+00
  eigenvectors of nos in ao-basis, column    3
  0.5774960E+00 -0.4075152E+00 -0.7792095E+00 -0.4548598E-01  0.3235677E+00
  eigenvectors of nos in ao-basis, column    4
  0.7015394E+00 -0.1195229E+01  0.6108338E+00 -0.8152475E+00  0.7242887E-01
  eigenvectors of nos in ao-basis, column    5
  0.3537252E+00 -0.3472097E+00 -0.2400169E+00  0.3836358E+00 -0.1152090E+01


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***


 cosurf_and_phi
  1.262739748330  2.872870034186  1.567866969040 -0.042648150717
  1.652947872482  0.360731428865  3.496571379926 -0.051738126393
  0.594940552015  0.670555958619  3.845537291721 -0.062076126375
  2.390078334690  1.202722009900  2.566708449184 -0.032976810762
  1.989334673621  2.229216060677  2.001028520754 -0.035883601357
  2.919143888229  0.378144896597  2.099775822683 -0.012466551947
  0.105380882100  1.878884201281  3.371423292662 -0.062388027902
  0.783266721831  2.590884380442  2.520834408126 -0.055812360905
  2.011402526412  2.551496219550  0.814855178005 -0.016991222728
  0.099626804209  1.855073908563 -1.945822518898  0.030173760503
  0.119020599620  3.173161356543  1.415159765853 -0.048530741507
  0.915237473358  3.108118624501  0.463299650838 -0.030391948239
  0.462814589486  2.837210699514 -0.795516582628 -0.006638267135
  1.408719892640  1.640288870679  3.148120355694 -0.053501413567
  2.650053602975  1.633766196698  1.655441764425 -0.014117511083
  1.142904167226  2.885766749848 -0.243475252462 -0.012216546161
  3.656106873706  0.713798214804  0.361832640492  0.038225283542
  2.408046317685  2.075600470298 -1.226192756897  0.039121651623
  1.295820311657  0.567673501678 -2.747601655947  0.057068460054
  3.527730862121  1.045649613724 -1.064853177123  0.054455836929
  2.622898581638  1.024710819001 -2.241122746235  0.058992296488
  3.086808508398  1.794857685487 -0.179703829578  0.033975769768
  1.615872011596  1.674366500124 -2.147504385867  0.046777524802
  0.000006852884  1.035694667087 -2.361676372823  0.049239542648
  0.298815704890  0.969607820141 -2.408807386530  0.050821173442
  0.667723897920  1.245157743773 -2.338577856151  0.048513045421
  1.218123388571  2.025110412626 -1.616576841774  0.034431324619
  2.084186643139  2.293984793080 -0.480493513306  0.021273479388
  2.876642520254  1.658030225134  0.559035126479  0.020777797212
  3.282919570196  0.368088739237  1.091983758387  0.024297527985
  0.467932162408  1.694535407938 -1.941510815499  0.036887464309
  1.299770347024  2.453875604722 -0.850324284820  0.011403884006
  2.242050270258  2.245320705184  0.385739526123  0.002957350480
  2.923104801922  1.151131828431  1.279135167181  0.007197766090
  0.427044967836  0.580963354323 -2.585108185092  0.054568976519
 end_of_phi


 nsubv after electrostatic potential =  0

  Confirmation of dielectric energy for ground state
  edielnew =  -0.00252052724


 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************


 cosurf_and_qcos_from cosmo(3
  1.262739748330  2.872870034186  1.567866969040  0.005047465120
  1.652947872482  0.360731428865  3.496571379926  0.004138277977
  0.594940552015  0.670555958619  3.845537291721  0.006827930350
  2.390078334690  1.202722009900  2.566708449184  0.003915529121
  1.989334673621  2.229216060677  2.001028520754  0.003318626553
  2.919143888229  0.378144896597  2.099775822683  0.000065242505
  0.105380882100  1.878884201281  3.371423292662  0.000593220098
  0.783266721831  2.590884380442  2.520834408126  0.008457025317
  2.011402526412  2.551496219550  0.814855178005  0.002460381393
  0.099626804209  1.855073908563 -1.945822518898  0.000207236653
  0.119020599620  3.173161356543  1.415159765853  0.004458519795
  0.915237473358  3.108118624501  0.463299650838  0.005992941265
  0.462814589486  2.837210699514 -0.795516582628  0.002802142144
  1.408719892640  1.640288870679  3.148120355694  0.007938208579
  2.650053602975  1.633766196698  1.655441764425  0.001084764390
  1.142904167226  2.885766749848 -0.243475252462  0.001759883077
  3.656106873706  0.713798214804  0.361832640492 -0.007868787385
  2.408046317685  2.075600470298 -1.226192756897 -0.004971645523
  1.295820311657  0.567673501678 -2.747601655947 -0.005735801265
  3.527730862121  1.045649613724 -1.064853177123 -0.011424020974
  2.622898581638  1.024710819001 -2.241122746235 -0.011012001674
  3.086808508398  1.794857685487 -0.179703829578 -0.004407764242
  1.615872011596  1.674366500124 -2.147504385867 -0.004238694556
  0.000006852884  1.035694667087 -2.361676372823 -0.000140119212
  0.298815704890  0.969607820141 -2.408807386530 -0.000238156297
  0.667723897920  1.245157743773 -2.338577856151 -0.000856789770
  1.218123388571  2.025110412626 -1.616576841774 -0.001274038265
  2.084186643139  2.293984793080 -0.480493513306 -0.001013104449
  2.876642520254  1.658030225134  0.559035126479 -0.001573774158
  3.282919570196  0.368088739237  1.091983758387 -0.001963182581
  0.467932162408  1.694535407938 -1.941510815499 -0.001288883640
  1.299770347024  2.453875604722 -0.850324284820 -0.000094645951
  2.242050270258  2.245320705184  0.385739526123 -0.000385878140
  2.923104801922  1.151131828431  1.279135167181 -0.001107652502
  0.427044967836  0.580963354323 -2.585108185092 -0.002209839450
 end_of_qcos

 sum of the negative charges =  -0.06180478

 sum of the positive charges =   0.0590673943

 total sum =  -0.0027373857

 *** qcos is copied to qcosdalton ***

 fepsi =   1.
 *** qcosdalton is mult by fepsi ***
 cosurf and qcosdalton
  1.262739748330  2.872870034186  1.567866969040  0.005047465120
  1.652947872482  0.360731428865  3.496571379926  0.004138277977
  0.594940552015  0.670555958619  3.845537291721  0.006827930350
  2.390078334690  1.202722009900  2.566708449184  0.003915529121
  1.989334673621  2.229216060677  2.001028520754  0.003318626553
  2.919143888229  0.378144896597  2.099775822683  0.000065242505
  0.105380882100  1.878884201281  3.371423292662  0.000593220098
  0.783266721831  2.590884380442  2.520834408126  0.008457025317
  2.011402526412  2.551496219550  0.814855178005  0.002460381393
  0.099626804209  1.855073908563 -1.945822518898  0.000207236653
  0.119020599620  3.173161356543  1.415159765853  0.004458519795
  0.915237473358  3.108118624501  0.463299650838  0.005992941265
  0.462814589486  2.837210699514 -0.795516582628  0.002802142144
  1.408719892640  1.640288870679  3.148120355694  0.007938208579
  2.650053602975  1.633766196698  1.655441764425  0.001084764390
  1.142904167226  2.885766749848 -0.243475252462  0.001759883077
  3.656106873706  0.713798214804  0.361832640492 -0.007868787385
  2.408046317685  2.075600470298 -1.226192756897 -0.004971645523
  1.295820311657  0.567673501678 -2.747601655947 -0.005735801265
  3.527730862121  1.045649613724 -1.064853177123 -0.011424020974
  2.622898581638  1.024710819001 -2.241122746235 -0.011012001674
  3.086808508398  1.794857685487 -0.179703829578 -0.004407764242
  1.615872011596  1.674366500124 -2.147504385867 -0.004238694556
  0.000006852884  1.035694667087 -2.361676372823 -0.000140119212
  0.298815704890  0.969607820141 -2.408807386530 -0.000238156297
  0.667723897920  1.245157743773 -2.338577856151 -0.000856789770
  1.218123388571  2.025110412626 -1.616576841774 -0.001274038265
  2.084186643139  2.293984793080 -0.480493513306 -0.001013104449
  2.876642520254  1.658030225134  0.559035126479 -0.001573774158
  3.282919570196  0.368088739237  1.091983758387 -0.001963182581
  0.467932162408  1.694535407938 -1.941510815499 -0.001288883640
  1.299770347024  2.453875604722 -0.850324284820 -0.000094645951
  2.242050270258  2.245320705184  0.385739526123 -0.000385878140
  2.923104801922  1.151131828431  1.279135167181 -0.001107652502
  0.427044967836  0.580963354323 -2.585108185092 -0.002209839450
 end of cosurf and qcosdalton

 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 Warning! Large contrib. to nucrep.The distance is   1.37057673E-05
 ** Total nuclear repulsion energy ** =   9.34395611545006
 Warning! Large contrib. to nucrep.The distance is   1.37057673E-05
 screening nuclear repulsion energy   0.00937562773

 Total-screening nuclear repulsion energy   9.33458049


 Adding T+Vsolv ...
 maxdens  375
 *** End of DALTON-COSMO calculation ***

  original map vector  44 45 46 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 47 18
 19 20 21 22 23 24 25 26 27 28 29 30 48 31 32 33 34 35 36 37 38 39 40 41 42 43 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0


          sorted Vsolv matrix  block   1

               nbf   1        nbf   2        nbf   3        nbf   4        nbf   5        nbf   6        nbf   7        nbf   8
  nbf   1    -1.94489440
  nbf   2     0.68046330    -2.85983628
  nbf   3    -0.11574538    -0.00698658    -2.70263483
  nbf   4     0.43714764    -0.20534231     0.11092180    -2.67917203
  nbf   5    -0.28439982     1.38414641    -0.05597688    -0.02690151    -4.04204749
  nbf   6    -0.01728245    -0.16389722    -0.50080547     0.22131444     0.21324297    -3.53092766
  nbf   7     0.09347666    -0.26797127    -0.64536513    -0.34053291     0.16191673    -0.04602181    -3.65989514
  nbf   8     0.13397324    -0.18263500     0.31744388    -0.82932678    -0.28182987    -0.03865039     0.08686990    -3.78019763
  nbf   9     0.12533329    -0.34730804     0.27549294    -0.52342493     0.61748594     0.58045379    -0.07906704    -0.04084541
  nbf  10    -0.05887472     0.15838278    -0.00265173    -0.01661601    -0.60433675     0.32168181    -0.69064840     0.11219869
  nbf  11    -0.18174615     0.38634640     0.07041112     0.12085694    -1.36564450     0.32352440     0.56193013     0.42442755
  nbf  12     0.07228158    -0.33720458    -0.22501480     0.12457056     0.69875130    -1.06109172    -0.48927624     0.56310659
  nbf  13    -0.00034272     0.01969898    -0.00089135    -0.00672876     0.00786999     0.00963891     0.36146200    -0.11806129
  nbf  14    -0.02692181    -0.04358362    -0.05418741     0.06821168     0.22500780    -0.01179886     0.25307676     0.91472900
  nbf  15    -0.00477612    -0.04963755    -0.05185004     0.02176699     0.00990163    -0.15468705     0.51091973    -0.27801568
  nbf  16    -0.03042136    -0.03567972    -0.04555849     0.01253847    -0.10742960    -0.18831461    -0.20147971     0.00588651
  nbf  17     0.17871602    -0.24037358     0.12595032    -0.13942808     0.46299502     0.34430576    -0.28708553    -1.09323134
  nbf  44    -0.07825712     0.13075173    -0.08493698     0.16376335    -0.17414212    -0.13771798     0.06537334     0.28050616
  nbf  45     0.44099289    -0.76131417     0.24953298    -0.64649363     0.77758110     0.26114171    -0.27994365    -0.80812401
  nbf  46    -0.17211189     0.79408109     0.58206197    -0.41121318    -0.98288238     1.09981901     0.21453551    -0.46530071

               nbf   9        nbf  10        nbf  11        nbf  12        nbf  13        nbf  14        nbf  15        nbf  16
  nbf   9    -3.36100119
  nbf  10     0.06994697    -2.60028433
  nbf  11    -0.03809049    -0.21047294    -2.87333635
  nbf  12     0.03854545     0.48559245     1.10614131    -3.57378956
  nbf  13     0.01317669    -0.11307251    -0.03734212     0.08728420    -1.52447782
  nbf  14    -0.15090964     0.25444526     0.44361395    -0.26815076    -0.00949033    -1.44552134
  nbf  15     0.37187140     0.89002047     0.16428008    -0.03953364    -0.21653953     0.19903286    -1.52235789
  nbf  16     0.72983798    -0.38674826     0.72002824    -0.20501420     0.12672421     0.64928791     0.13571823    -0.75025501
  nbf  17    -1.35665126     0.15868451     1.01971329     0.06765522     0.01419155     0.24341280     0.05977835     0.19410874
  nbf  44     0.31332730    -0.02372556    -0.18742190    -0.12100250     0.00342358    -0.15393702    -0.04553651    -0.13535865
  nbf  45    -0.69040750     0.07966004     0.44818182     0.02692446     0.00320731     0.22267131     0.03313434     0.11886597
  nbf  46    -0.35769725    -0.29835128    -0.49441745     1.01906556    -0.04136555     0.21584278     0.11915900     0.17785244

               nbf  17        nbf  18        nbf  19        nbf  20        nbf  21        nbf  22        nbf  23        nbf  24
  nbf  17     0.95526662
  nbf  18     0.00000000    -1.55461328
  nbf  19     0.00000000     0.46548705    -2.92437534
  nbf  20     0.00000000    -0.36547308     0.09372656    -2.40581988
  nbf  21     0.00000000    -0.11324887     0.32443762    -0.07517545    -2.00526506
  nbf  22     0.00000000     0.15469370    -1.38464870     0.08170561     0.31712849    -4.05335318
  nbf  23     0.00000000     0.18618564    -0.48857305     0.87814645     0.26521450    -0.37543612    -3.40935777
  nbf  24     0.00000000    -0.00501415     0.08489795    -0.20927582     0.29430381     0.22950037     0.13015516    -2.50409920
  nbf  25     0.00000000    -0.05514717     0.30261384    -0.04880437    -0.29812523     1.22106413    -0.02198511    -0.18014785
  nbf  26     0.00000000    -0.14822123     0.36725893    -0.19345762    -0.10459821     0.88498232     1.31149611    -0.06068674
  nbf  27     0.00000000     0.11042427    -0.34516032     0.17736496     0.19452591    -0.46416107    -0.89933221     0.21946571
  nbf  28     0.00000000    -0.01671803     0.05092758    -0.03163274     0.00042512     0.02636752     0.13745960    -0.60041390
  nbf  29     0.00000000     0.04829671    -0.18725804     0.03128885     0.00783721    -0.55277050    -0.05722184     0.02904929
  nbf  30     0.00000000     0.02690581    -0.11562003     0.11498720     0.05688996    -0.01926142    -0.26698631    -0.11030769
  nbf  44     1.02834784     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
  nbf  45    -0.88521871     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
  nbf  46    -0.24334834     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
  nbf  47     0.00000000    -0.40150283     1.03365603    -0.60513601    -0.30384832     0.96970449     1.06065953    -0.11814336

               nbf  25        nbf  26        nbf  27        nbf  28        nbf  29        nbf  30        nbf  31        nbf  32
  nbf  25    -2.87148212
  nbf  26     0.02649308    -2.99037240
  nbf  27     0.76755229     1.04053546    -2.66618598
  nbf  28    -0.04482214    -0.27657363     0.11236278    -1.18114524
  nbf  29     1.15765229     0.64231495    -0.37650196     0.07153975    -1.55098859
  nbf  30    -0.28847787     1.29196803    -0.28931725     0.02871425     0.55994998    -0.37415140
  nbf  31     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -2.77725224
  nbf  32     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.81924562    -3.88112226
  nbf  33     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.71650080     0.13662968
  nbf  34     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.09179356    -0.47362194
  nbf  35     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.50711443    -1.61825222
  nbf  36     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00227274     0.01134915
  nbf  37     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00590315     0.02047342
  nbf  38     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.05160534    -0.08701383
  nbf  47    -0.22157408    -0.73089132     0.67001804    -0.09659037     0.21834228     0.25320250     0.00000000     0.00000000
  nbf  48     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     1.07146470     1.54880657

               nbf  33        nbf  34        nbf  35        nbf  36        nbf  37        nbf  38        nbf  39        nbf  40
  nbf  33    -3.76163984
  nbf  34    -0.60081904    -2.83233624
  nbf  35     0.76471036    -0.37760421    -4.43834208
  nbf  36     0.04388833     0.09143317     0.00535193    -1.60190463
  nbf  37     0.47171237    -0.27482996    -0.10666542    -0.00368541    -1.33519880
  nbf  38    -0.67313409    -0.83717593    -0.02805447     0.01116441     0.29045451    -1.68096226
  nbf  39     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -4.59124369
  nbf  40     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00802710    -2.80666987
  nbf  41     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00524437    -0.00327494
  nbf  42     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00263508    -0.00137783
  nbf  43     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00107613     0.01205621
  nbf  48    -0.44748732     0.27556892     1.29483055    -0.00569054     0.04626592     0.03311918     0.00000000     0.00000000

               nbf  41        nbf  42        nbf  43        nbf  44        nbf  45        nbf  46        nbf  47        nbf  48
  nbf  41    -2.20409626
  nbf  42    -0.00962614    -1.35622929
  nbf  43     0.00052614    -0.00259973    -0.74744602
  nbf  44     0.00000000     0.00000000     0.00000000   -33.04963334
  nbf  45     0.00000000     0.00000000     0.00000000     0.58277835    -7.87938487
  nbf  46     0.00000000     0.00000000     0.00000000     0.18464674    -0.22901095    -6.92069034
  nbf  47     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -6.75716177
  nbf  48     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -7.04599005
 insert_onel_diag: nmin2=  1892

 onel.diag.
 -0.194489E+01-0.285984E+01-0.270263E+01-0.267917E+01-0.404205E+01-0.353093E+01-0.365990E+01-0.378020E+01-0.336100E+01-0.260028E+01
 -0.287334E+01-0.357379E+01-0.152448E+01-0.144552E+01-0.152236E+01-0.750255E+00 0.955267E+00-0.155461E+01-0.292438E+01-0.240582E+01
 -0.200527E+01-0.405335E+01-0.340936E+01-0.250410E+01-0.287148E+01-0.299037E+01-0.266619E+01-0.118115E+01-0.155099E+01-0.374151E+00
 -0.277725E+01-0.388112E+01-0.376164E+01-0.283234E+01-0.443834E+01-0.160190E+01-0.133520E+01-0.168096E+01-0.459124E+01-0.280667E+01
 -0.220410E+01-0.135623E+01-0.747446E+00-0.330496E+02-0.787938E+01-0.692069E+01-0.675716E+01-0.704599E+01

 i,start,strti,fin,filind,bufszi,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095         1      1892      1892

 4 external diag. modified integrals

  0.162583931 -1.9448944  0.208205722  0.160361974  0.257411129 -2.85983628
  0.211866097  0.172819047  0.285377448  0.227907886  0.292636879 -2.70263483
  0.201180191  0.169627984  0.244637662  0.222077188  0.262200229  0.230622662
  0.256871848 -2.67917203  0.204782323  0.190354619  0.353861471  0.257082437
  0.307414074  0.284093738  0.284149775  0.26571056  0.454131129 -4.04204749
  0.191611185  0.182783002  0.256806383  0.245309394  0.277121536  0.257258778
  0.319642706  0.24649527  0.371309251  0.320951478  0.4062204 -3.53092766
  0.203648708  0.197431646  0.314388232  0.299157726  0.338163602  0.296808531
  0.296940337  0.284529011  0.467424556  0.415881438  0.388574144  0.36895109
  0.516577958 -3.65989514  0.195060045  0.189901517  0.275836116  0.266960473
  0.286083473  0.273691893  0.317155683  0.280108282  0.403100844  0.377135808
  0.415686909  0.370244838  0.428342355  0.414072102  0.509604969 -3.78019763
  0.199742763  0.194673788  0.313691197  0.284920235  0.310615366  0.296336636
  0.303169199  0.28522524  0.475779153  0.400994832  0.422708432  0.369283481
  0.509375063  0.440066243  0.46755657  0.440258048  0.50014168 -3.36100119
  0.1989861  0.195295106  0.314524782  0.284641855  0.320339501  0.295428393
  0.295307125  0.287869756  0.470168411  0.39676046  0.412813027  0.386068737
  0.541019267  0.41304078  0.439912758  0.421120128  0.489395609  0.45212706
  0.496984115 -2.60028433  0.203992183  0.198794343  0.337605204  0.301841751
  0.313427545  0.305483018  0.296957016  0.29114577  0.554771663  0.423748914
  0.411234724  0.374857862  0.511479912  0.475491855  0.472373425  0.428953236
  0.545207657  0.455265789  0.499940812  0.463648119  0.567827247 -2.87333635
  0.20044929  0.197929769  0.325886027  0.3032539  0.311210435  0.29967807
  0.31858978  0.309496723  0.536570158  0.464915722  0.503496053  0.440412632
  0.561027165  0.49501485  0.592424451  0.534958863  0.579581855  0.544911706
  0.596221005  0.48795483  0.644256024  0.529055581  0.878481247 -3.57378956
  0.200707487  0.198576653  0.320357122  0.304609637  0.307085941  0.302254344
  0.314914782  0.312657802  0.502720952  0.469872217  0.47137423  0.448658191
  0.579263287  0.505151876  0.602389946  0.533345136  0.584002658  0.531964393
  0.602832747  0.494226799  0.566667441  0.54846376  0.800958519  0.706574492
  0.81745201 -1.52447782  0.202971709  0.199912653  0.329392239  0.314695097
  0.310020328  0.306722632  0.31897329  0.309117686  0.539656526  0.49843245
  0.459750135  0.443794478  0.558609793  0.529707092  0.597241233  0.534606813
  0.615136892  0.554292134  0.548937937  0.526730003  0.640006687  0.58248007
  0.796255055  0.738284081  0.759554655  0.714955102  0.87067624 -1.44552134
  0.202651551  0.199135495  0.336499563  0.310425445  0.314492758  0.304907037
  0.325134017  0.314440497  0.56371812  0.484207939  0.517689992  0.455675426
  0.574985359  0.526199339  0.62918666  0.563980593  0.622384465  0.568585152
  0.604672678  0.530241703  0.637069122  0.578981353  0.910187722  0.845582462
  0.873353846  0.771341953  0.887019595  0.803244619  1.01770095 -1.52235789
  0.1980477  0.196750626  0.306052297  0.301061294  0.311283575  0.303332677
  0.320036237  0.310578659  0.489848125  0.472457914  0.480849968  0.455984077
  0.539321232  0.51245105  0.605017098  0.553865891  0.62395644  0.554322942
  0.55581959  0.525900768  0.610529032  0.549512708  0.81396413  0.772842519
  0.739088719  0.712382832  0.873089105  0.76064112  0.915222986  0.831046992
  0.917152999 -0.750255011  0.205508389  0.200450937  0.342156913  0.320377894
  0.312734907  0.306375322  0.324874543  0.307022656  0.563505817  0.508637216
  0.466752322  0.442170277  0.542293802  0.520944555  0.599442718  0.535052254
  0.629797664  0.536416528  0.543244946  0.519434725  0.680964234  0.574986706
  0.990942407  0.813738162  0.747730686  0.717903179  0.871934976  0.802985335
  0.923133504  0.883914829  0.851863211  0.811274163  1.17984261  0.955266618
  0.181100559  0.111200926  0.165161842  0.150968711  0.170528599  0.156019974
  0.170217989  0.14757216  0.168634738  0.162088706  0.155944411  0.153686935
  0.165152381  0.161592075  0.159982955  0.157027771  0.164199322  0.162076243
  0.163872813  0.162653604  0.165865886  0.16435758  0.162329136  0.161558572
  0.161265831  0.160930046  0.165143735  0.162973179  0.16300763  0.162118388
  0.164137427  0.162098534  0.165503203  0.163787595  0.206726055  0.173131144
  0.344611998  0.193580208  0.278191927  0.251527147  0.262222061  0.226749739
  0.334287937  0.296667362  0.267389149  0.256202192  0.325112783  0.307840249
  0.288140601  0.277217248  0.333680777  0.297354535  0.326461457  0.300057351
  0.347580757  0.319045094  0.326543379  0.3192572  0.315976561  0.312690014
  0.343367664  0.324710111  0.334803926  0.324945124  0.342324404  0.317594447
  0.346641191  0.33324303  0.190390755  0.165762347  0.234038042  0.207838903
  0.319730252  0.184530863  0.243042886  0.215570556  0.27206095  0.251242634
  0.262934912  0.24749127  0.273436359  0.265217826  0.268958885  0.256401113
  0.277114573  0.272276947  0.282890499  0.270790638  0.278083482  0.271317041
  0.280310393  0.276787339  0.277349215  0.27553314  0.289122186  0.281265601
  0.28349409  0.280751973  0.289414953  0.284039581  0.283878135  0.279504387
  0.196684499  0.176869595  0.250791815  0.221774435  0.270729017  0.226415807
  0.268067325  0.205289746  0.267919444  0.25425307  0.239419896  0.232018841
  0.270437643  0.261152741  0.252299247  0.246413738  0.262193148  0.257108525
  0.265710759  0.257658193  0.268843233  0.265287713  0.257411555  0.255381171
  0.258052663  0.257489899  0.26305993  0.260365906  0.258769374  0.256910331
  0.25784267  0.254762976  0.264425743  0.261347436  0.204034028  0.193828014
  0.317336532  0.288240771  0.308195299  0.289731431  0.28521734  0.271554461
  0.551694971  0.333097127  0.377222223  0.330756722  0.470432406  0.426910852
  0.407187599  0.385611117  0.482915654  0.411390477  0.464004197  0.414223559
  0.542552879  0.440597293  0.509083861  0.48446817  0.478063644  0.470098341
  0.540852497  0.500442253  0.524800709  0.502158753  0.555391843  0.482336704
  0.539964756  0.509549895  0.189298309  0.182121816  0.263175464  0.24211726
  0.286280126  0.263334223  0.279591449  0.260835822  0.362662909  0.342266096
  0.382051261  0.337087152  0.393560927  0.368787528  0.410265713  0.36189836
  0.436878477  0.37713073  0.404863705  0.384219639  0.430034693  0.379283845
  0.468536668  0.449954037  0.451151499  0.440935716  0.514442366  0.451138885
  0.49157069  0.469712256  0.506435468  0.4663034  0.48519389  0.460788436
  0.202602349  0.199529704  0.321944462  0.302500244  0.338185072  0.306452135
  0.288264533  0.280083813  0.486750444  0.405415759  0.358860779  0.34402156
  0.619932637  0.366956358  0.383568319  0.377076428  0.462607377  0.434142088
  0.542864137  0.38068661  0.50069596  0.464502941  0.452829194  0.442078082
  0.461803463  0.45124469  0.477086106  0.46978653  0.457772995  0.452643235
  0.454850909  0.443433586  0.464771662  0.456695466  0.203520114  0.198676234
  0.333977718  0.298447974  0.313647566  0.30340007  0.295391728  0.285543179
  0.49758812  0.416419838  0.38463579  0.362578249  0.486187761  0.459368947
  0.439761818  0.410214658  0.542222217  0.412432394  0.485064749  0.450604852
  0.596555442  0.428679634  0.536836281  0.512334104  0.528828687  0.509442906
  0.565482464  0.530604447  0.553851633  0.537493799  0.588637675  0.4958812
  0.563073666  0.521731907  0.196719099  0.193921448  0.30299922  0.287836202
  0.301982192  0.294539552  0.298843071  0.291084628  0.489695345  0.422008108
  0.433574242  0.406241281  0.489061688  0.466253407  0.483721816  0.457650362
  0.554487838  0.463496662  0.50617229  0.474992877  0.61863288  0.461594549
  0.650676483  0.598025459  0.591994506  0.57273645  0.711222906  0.580063407
  0.670169062  0.633841211  0.670869364  0.631723597  0.691000746  0.626842228
  0.200030289  0.198723857  0.313639944  0.306345145  0.306780659  0.30283704
  0.315061518  0.311109806  0.496190572  0.477703559  0.453156393  0.43485719
  0.520067343  0.502414173  0.609956557  0.539788568  0.573018665  0.552076142
  0.534371654  0.499995995  0.593269428  0.553610416  0.777237699  0.733189096
  0.726493916  0.685033235  0.798171909  0.744368719  0.811408996  0.787245641
  0.795404927  0.757654148  0.906665062  0.768424816  0.199110025  0.197664195
  0.311685933  0.302618316  0.31315034  0.306910613  0.312162566  0.306802203
  0.491199888  0.469594901  0.451797858  0.443181689  0.597329903  0.501391141
  0.552915491  0.52456106  0.604284717  0.538090851  0.60061827  0.493755142
  0.591344512  0.553177825  0.716024215  0.687698473  0.718370038  0.676544017
  0.795581542  0.733359328  0.798744584  0.746462827  0.816660565  0.748615343
  0.740271711  0.706961247  0.205650978  0.202098965  0.3463058  0.323472537
  0.316863455  0.312984678  0.316880123  0.310078358  0.56725533  0.513354282
  0.46895493  0.435845916  0.566740985  0.552174461  0.571580049  0.537434372
  0.601308114  0.557667114  0.55928864  0.535939654  0.667005988  0.593412559
  0.821334749  0.759704854  0.763519276  0.726998097  0.923511849  0.750999582
  0.881128568  0.830365933  0.813652819  0.772195811  0.915969785  0.800684138
  0.199718489  0.197976922  0.318502512  0.30634659  0.310896365  0.304700598
  0.318389467  0.31011476  0.522740392  0.479042146  0.488559383  0.453378141
  0.543381326  0.524237262  0.59619137  0.539089755  0.593410856  0.558399945
  0.5522044  0.540204146  0.619497378  0.572008541  0.84595457  0.768178739
  0.743778268  0.721537928  0.823528608  0.780081715  0.924424695  0.838761203
  0.908794418  0.795352855  0.866968858  0.81435082  0.147854003 -1.55461328
  0.179386255  0.1516907  0.291151891 -2.92437534  0.174191642  0.14966691
  0.242219397  0.219276556  0.258663302 -2.40581988  0.17473367  0.155852044
  0.272788043  0.226264627  0.240889386  0.21072064  0.247135925 -2.00526506
  0.170642113  0.160795236  0.375918463  0.272210101  0.266000371  0.253221783
  0.270586564  0.254514467  0.463176926 -4.05335318  0.163929292  0.156658166
  0.277666025  0.253098107  0.299700175  0.250978116  0.241105807  0.233086385
  0.37457435  0.342886792  0.391569755 -3.40935777  0.166455569  0.164959496
  0.338909353  0.310977731  0.283777781  0.265418973  0.281101819  0.26435153
  0.49041693  0.410699363  0.378817621  0.345259992  0.5216361 -2.5040992
  0.168503737  0.165275669  0.36399521  0.309497837  0.276395633  0.268171136
  0.274033174  0.266592593  0.539326171  0.399696108  0.392427873  0.36678163
  0.489447186  0.44860391  0.512221479 -2.87148212  0.164445542  0.1613885
  0.330106718  0.298899825  0.286709042  0.273915054  0.257222414  0.251772244
  0.534012377  0.418395786  0.486274173  0.387032518  0.456974473  0.431959559
  0.537109916  0.449567828  0.580011117 -2.9903724  0.16443963  0.162412782
  0.338086932  0.315392891  0.287678225  0.279885936  0.261928816  0.258727899
  0.525806673  0.470043551  0.493657849  0.441470356  0.453784817  0.444044068
  0.546353835  0.497799526  0.675859267  0.557772699  0.812559011 -2.66618598
  0.164298666  0.162672945  0.337295556  0.317233284  0.288870319  0.284429227
  0.260906337  0.257703661  0.524385327  0.481393892  0.484542647  0.46330603
  0.502408728  0.453462135  0.566721142  0.505783571  0.648865191  0.588250757
  0.727222014  0.685369342  0.796004639 -1.18114524  0.166492557  0.164385012
  0.356244286  0.332469933  0.289621881  0.281486079  0.268415064  0.264591893
  0.571309746  0.506331706  0.507323208  0.446693623  0.501530774  0.487662327
  0.623342732  0.518646408  0.717396364  0.579306693  0.815064351  0.721742713
  0.792539237  0.705999389  0.885754518 -1.55098859  0.16402837  0.162216181
  0.342068167  0.320913095  0.28721435  0.281877177  0.258953037  0.255929999
  0.558020418  0.49037548  0.503536549  0.459041534  0.465035777  0.452701305
  0.598705395  0.505772674  0.719709789  0.595125573  0.810269386  0.723911915
  0.789330972  0.725190974  0.856796727  0.770386533  0.882536543 -0.374151402
  0.216833985  0.168837654  0.267729028  0.229907828  0.273949287  0.244083154
  0.278601441  0.226593683  0.292745877  0.281211443  0.27818265  0.248488243
  0.303770996  0.295477096  0.298269685  0.284385815  0.300901785  0.287542214
  0.292457077  0.286293388  0.304368206  0.299517668  0.31313896  0.307415988
  0.315090786  0.307757997  0.320558315  0.310337291  0.322953352  0.312143993
  0.313958794  0.303849197  0.326820263  0.317794719  0.193449263  0.183426276
  0.25435577  0.245151643  0.265598219  0.255499559  0.336972462  0.246173998
  0.354857731  0.334898989  0.392904987  0.350796422  0.367507624  0.362084328
  0.471630396  0.398017007  0.416451945  0.383817325  0.383431729  0.37683257
  0.395113884  0.385930569  0.49701314  0.484236773  0.491800832  0.473300677
  0.521391259  0.461401698  0.539878027  0.492573561  0.518466619  0.477266455
  0.520263574  0.487548657  0.201344598  0.196807875  0.303705463  0.295135681
  0.305843786  0.297282758  0.301388638  0.294081963  0.452313059  0.417732805
  0.400327432  0.381677249  0.496765389  0.454366448  0.479728964  0.443039035
  0.486799308  0.45349987  0.472083889  0.44990918  0.500572065  0.475714451
  0.589198888  0.519499274  0.609880442  0.526959016  0.560729211  0.549467141
  0.579606719  0.559435299  0.557169371  0.532879633  0.562171003  0.541563349
  0.200017069  0.196988236  0.312153237  0.285203813  0.304156841  0.295619554
  0.303660007  0.290464824  0.460834957  0.398301271  0.3982042  0.373094042
  0.46610729  0.441269246  0.470879369  0.431386819  0.471007531  0.447476304
  0.474210534  0.437928167  0.483036985  0.464850739  0.560194338  0.4853262
  0.576350988  0.500332875  0.541282082  0.51646452  0.539419424  0.53006066
  0.52312009  0.509760212  0.526063593  0.510561391  0.200663018  0.198331781
  0.315405451  0.307632292  0.301605919  0.297537383  0.323099181  0.312538597
  0.507029154  0.485691393  0.467213563  0.450448069  0.504455507  0.497208428
  0.660706254  0.534651043  0.594246713  0.558697228  0.518996903  0.507360904
  0.604316103  0.558677744  0.90228294  0.811631934  0.769113507  0.722592635
  0.8260654  0.795559619  0.915357839  0.859951845  0.869700966  0.795874773
  1.07800877  0.87462372  0.199041255  0.197666509  0.311223305  0.300826039
  0.306649952  0.301455582  0.314991664  0.31204979  0.485877447  0.463701062
  0.464667911  0.453908897  0.575593353  0.497819474  0.574191424  0.54839654
  0.56658854  0.546964597  0.597103951  0.496505691  0.557093248  0.544767945
  0.79341043  0.69605458  0.780491105  0.74161129  0.750354501  0.718434814
  0.83519223  0.7699359  0.75851993  0.72669178  0.73962283  0.711115095
  0.1995279  0.198010833  0.309283899  0.301365251  0.304452057  0.302095566
  0.314796824  0.311189736  0.483815193  0.465859828  0.453249988  0.432966896
  0.528094773  0.505165575  0.620904486  0.536080953  0.618375218  0.519605881
  0.531726368  0.506301496  0.576779075  0.540832338  0.708068129  0.689762419
  0.746762894  0.700401838  0.806915825  0.7215185  0.797438687  0.749001407
  0.798194979  0.72551533  0.745874344  0.714880694  0.202518536  0.199147874
  0.332679622  0.309822161  0.311060584  0.30342353  0.327163492  0.315892767
  0.556201245  0.483159141  0.509558558  0.458800964  0.54163174  0.527192578
  0.626444193  0.582017561  0.624254849  0.573032583  0.561013976  0.54231485
  0.623148845  0.578401459  0.904484195  0.854104325  0.863311643  0.779189299
  0.886265353  0.812862522  1.00115395  0.937703894  0.925447792  0.836389825
  0.92608005  0.89084278  0.164935252  0.154187892  0.274802032  0.242014418
  0.234029201  0.214868245  0.2667466  0.230511632  0.297394324  0.284678129
  0.259392667  0.252591599  0.298353835  0.294707846  0.300613758  0.297107524
  0.290614654  0.286451114  0.316577397  0.310181051  0.304434299  0.301480299
  0.32049692  0.31635523  0.309785966  0.306277763  0.155929889  0.153868082
  0.261173275  0.25440558  0.259175407  0.249509673  0.243031821  0.234592617
  0.347482123  0.3405552  0.375625936  0.345486988  0.333895886  0.330489323
  0.367306856  0.362036022  0.422838574  0.410305568  0.496072518  0.485167042
  0.461549677  0.454880519  0.480368898  0.468522441  0.490888536  0.478699123
  0.162630868  0.161334253  0.314444764  0.304608791  0.267685054  0.26380423
  0.266301352  0.261884012  0.459079931  0.420081058  0.385314321  0.375374194
  0.462993064  0.436410756  0.492130257  0.450505092  0.48494125  0.470734719
  0.551407214  0.535736539  0.546250739  0.526052777  0.571293039  0.556428998
  0.556283423  0.539126622  0.162536412  0.162155005  0.319595554  0.301577043
  0.269719129  0.267372815  0.266301104  0.262933005  0.446857859  0.412717631
  0.384312981  0.36972566  0.452369518  0.428272711  0.470824356  0.448771756
  0.475429018  0.45645276  0.52311853  0.51163923  0.521377379  0.504315619
  0.532509673  0.523871423  0.520992432  0.51322342  0.162259384  0.161538653
  0.324515738  0.318439499  0.279580789  0.276900868  0.258005807  0.255980346
  0.49712004  0.483680727  0.469680171  0.459445976  0.428312123  0.425963461
  0.515561051  0.50533672  0.640519835  0.613341562  0.86759272  0.781553264
  0.709054426  0.693745803  0.813551025  0.787585401  0.818103956  0.794668765
  0.161468911  0.160918441  0.317527563  0.311329526  0.279121487  0.278072982
  0.256829087  0.25632856  0.482212189  0.469353365  0.45683397  0.450698198
  0.454986409  0.448584548  0.523606199  0.511125478  0.593936327  0.581604007
  0.760746462  0.675150275  0.739939938  0.691671053  0.742741622  0.717197944
  0.745746338  0.726401573  0.162897326  0.161918975  0.325579501  0.31341906
  0.282447109  0.280189317  0.259904824  0.258575504  0.4929234  0.471844969
  0.465391836  0.451836961  0.454114561  0.448642034  0.527002755  0.505491107
  0.599408243  0.578291175  0.766322034  0.720787719  0.741653214  0.694917991
  0.74829325  0.715754397  0.732710284  0.715973565  0.162486294  0.16203107
  0.329778545  0.324564247  0.281279764  0.280087181  0.258238724  0.257530675
  0.512782723  0.500557466  0.480378558  0.471344481  0.446717175  0.444261689
  0.547504106  0.532098893  0.649613573  0.636824882  0.810326982  0.800056761
  0.771776501  0.745861083  0.865795948  0.821645965  0.899701011  0.837033098
  0.287002033 -2.77725224  0.292545937  0.254951102  0.425470456 -3.88112226
  0.324717158  0.292708755  0.409485272  0.387541321  0.512709584 -3.76163984
  0.313868628  0.29505883  0.408692891  0.379444577  0.557698697  0.416771386
  0.484938377 -2.83233624  0.338545462  0.304997177  0.604284039  0.46435009
  0.586688394  0.522358559  0.554211306  0.495467396  1.00724657 -4.43834208
  0.312016993  0.305241323  0.490886353  0.472623546  0.624121165  0.512240016
  0.590493997  0.487650416  0.755261431  0.718952361  0.791328047 -1.60190463
  0.318441051  0.307047158  0.501139259  0.476710677  0.571850581  0.530455136
  0.548237368  0.50655925  0.796439812  0.730438032  0.774425531  0.681019099
  0.787602962 -1.3351988  0.333451001  0.312811354  0.580836708  0.488943429
  0.620938802  0.549741908  0.582714592  0.518048868  0.970391998  0.880883041
  0.868463283  0.768537543  0.808339582  0.76834557  1.04697639 -1.68096226
  0.199237447  0.197651865  0.306141201  0.299623712  0.305637736  0.301551532
  0.314506548  0.308167513  0.475008333  0.461190811  0.438766991  0.427349574
  0.523935826  0.493631912  0.575444517  0.531570906  0.562959964  0.534681337
  0.523253315  0.493533432  0.556332915  0.539960875  0.679568313  0.668911133
  0.702481275  0.660224324  0.746033686  0.719383718  0.764269293  0.720975882
  0.743823107  0.719467535  0.73415071  0.704017493  0.203876574  0.197017279
  0.319478305  0.29169636  0.312090565  0.298940212  0.29321323  0.286124333
  0.452398091  0.411493346  0.366781328  0.356135865  0.461280155  0.442799519
  0.422474368  0.412762941  0.458384068  0.440688021  0.450952671  0.432215597
  0.486197436  0.464454807  0.494328233  0.48587231  0.500594032  0.485211135
  0.515543387  0.507577182  0.517762043  0.503776845  0.512812863  0.499704277
  0.51478812  0.502585017  0.199951401  0.180787587  0.261923982  0.246142748
  0.271389033  0.254237078  0.270290946  0.253660343  0.337073712  0.319791078
  0.31117903  0.301386298  0.348044862  0.341502098  0.356096043  0.343681863
  0.355460526  0.348075142  0.346925017  0.340911843  0.364923063  0.354997401
  0.395051598  0.387031574  0.407881523  0.389071961  0.414552051  0.40395708
  0.410535391  0.404314119  0.401373214  0.396106137  0.404975263  0.396846337
  0.199972748  0.192808681  0.298303902  0.286405541  0.29281703  0.287680959
  0.299793209  0.293670755  0.450467132  0.426194239  0.406343071  0.399863697
  0.4679964  0.45539456  0.514918521  0.480674181  0.517392833  0.485182826
  0.471720435  0.461010053  0.512295307  0.497698185  0.639788352  0.625293446
  0.633057332  0.604124158  0.690892874  0.662614742  0.718920621  0.666584132
  0.673506799  0.657769536  0.693683775  0.669875704  0.199970469  0.198121005
  0.311234616  0.298905955  0.307695254  0.302470689  0.30918229  0.30265251
  0.469591865  0.444662379  0.42732975  0.414799009  0.494793042  0.485021683
  0.530893211  0.500497584  0.52978149  0.511203968  0.503015828  0.48757336
  0.532652778  0.518238389  0.654660561  0.637388996  0.659907514  0.617913662
  0.668386356  0.651841424  0.708544969  0.681201327  0.703563912  0.672071316
  0.676065816  0.650953848  0.163686171  0.161658437  0.330098363  0.306912425
  0.285961529  0.280551066  0.261415488  0.258726794  0.502696907  0.456341192
  0.477169067  0.441269791  0.455925767  0.445085512  0.532222682  0.49095028
  0.601302702  0.561072462  0.85208719  0.628747894  0.724188674  0.663687218
  0.74463864  0.688118571  0.733212411  0.689669236  0.166838541  0.163525701
  0.34589005  0.299318163  0.273488196  0.264183959  0.280591933  0.261274657
  0.473572189  0.410645286  0.371861952  0.359246724  0.468695069  0.434778066
  0.492356839  0.44125679  0.465816746  0.448290235  0.517830377  0.491373037
  0.509492582  0.488809868  0.538053409  0.511605212  0.51861278  0.495581065
  0.16922226  0.154464411  0.276242556  0.254295331  0.270091058  0.227311571
  0.265431992  0.226422607  0.346875398  0.315798146  0.330802484  0.301707359
  0.335801135  0.326858077  0.357709778  0.339787463  0.37410813  0.352461718
  0.409659071  0.395136656  0.404679471  0.387635689  0.416037563  0.402377801
  0.407037925  0.394008691  0.165529387  0.159827045  0.314526564  0.296189411
  0.27883128  0.267860012  0.259175473  0.248912462  0.457323994  0.42685402
  0.464939969  0.394380518  0.414141299  0.406101528  0.471223802  0.458702353
  0.576033004  0.506651596  0.678633434  0.641946765  0.636088212  0.607815148
  0.6855086  0.643467488  0.685330111  0.632986027  0.16386632  0.1627661
  0.334141544  0.313256918  0.278838678  0.274881545  0.265207828  0.261124823
  0.496530021  0.454201741  0.433895736  0.41807496  0.455532654  0.441508503
  0.54846003  0.469589539  0.562487509  0.538167125  0.686109835  0.631375042
  0.661960985  0.614265487  0.698911906  0.633203776  0.695046243  0.657090411
  0.317186254  0.304135051  0.498791854  0.458370453  0.574466026  0.508983591
  0.53631474  0.494104836  0.761133222  0.708761186  0.76664746  0.637181539
  0.80733287  0.647208425  0.778104275  0.736079544  0.338644835  0.280645374
  0.369013243  0.35894924  0.571343656  0.379955942  0.537642682  0.389356093
  0.507605946  0.486505173  0.505904671  0.486354241  0.521205846  0.484866247
  0.52882769  0.504146843  0.312832551  0.237656727  0.339666282  0.308092057
  0.388726347  0.332139308  0.363054929  0.342631258  0.407495957  0.394601942
  0.405881656  0.394581505  0.416240357  0.390387774  0.420759692  0.406479335
  0.314995193  0.285615154  0.474231553  0.418675  0.50107357  0.472182717
  0.493770737  0.442151419  0.722269766  0.653547298  0.642198577  0.612944366
  0.668823626  0.617589858  0.731680569  0.681675132  0.316765231  0.304177248
  0.4568538  0.430841271  0.561007705  0.493328978  0.551789718  0.457651991
  0.696374393  0.647010242  0.66308998  0.632991757  0.678825135  0.622181602
  0.758152322  0.67392751  0.730108994 -4.59124369  0.562163781  0.448436168
  0.478740312 -2.80666987  0.437342102  0.368854486  0.374865281  0.325155451
  0.308752165 -2.20409626  0.73625297  0.552891722  0.481070124  0.430771892
  0.450173039  0.307878085  0.600409001 -1.35622929  0.70765875  0.584130424
  0.58291261  0.418505675  0.402567706  0.365106584  0.599229834  0.552394054
  0.62268278 -0.74744602
 i,start,strti,fin,filind,bufszi,nmbuf,nmin2, nd4ext = 
         1         1         2         2        12      4095         1      1892      1892

 all internall diag. modified integrals

  4.73888533 -33.0496333  1.04697806  0.0635775921  0.75633978 -7.87938487
  0.982105435  0.0304441088  0.678182812  0.124731145  0.720269923 -6.92069034
  0.888378871  0.0202631578  0.686297904  0.156106234  0.608788829  0.040066549
  0.674068355 -6.75716177  1.02562874  0.0300895777  0.707265436  0.127290671
  0.653389638  0.0413186341  0.621641696  0.0301711566  0.741234924 -7.04599005
 blstr diagel,allext,nmin2=  1892
 onel.diag.
 -0.194489E+01-0.285984E+01-0.270263E+01-0.267917E+01-0.404205E+01-0.353093E+01-0.365990E+01-0.378020E+01-0.336100E+01-0.260028E+01
 -0.287334E+01-0.357379E+01-0.152448E+01-0.144552E+01-0.152236E+01-0.750255E+00 0.955267E+00-0.155461E+01-0.292438E+01-0.240582E+01
 -0.200527E+01-0.405335E+01-0.340936E+01-0.250410E+01-0.287148E+01-0.299037E+01-0.266619E+01-0.118115E+01-0.155099E+01-0.374151E+00
 -0.277725E+01-0.388112E+01-0.376164E+01-0.283234E+01-0.443834E+01-0.160190E+01-0.133520E+01-0.168096E+01-0.459124E+01-0.280667E+01
 -0.220410E+01-0.135623E+01-0.747446E+00-0.330496E+02-0.787938E+01-0.692069E+01-0.675716E+01-0.704599E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1      1892      1892
 blstr diagel,allext,nmin2=  1892
 onel.diag.
 -0.194489E+01-0.285984E+01-0.270263E+01-0.267917E+01-0.404205E+01-0.353093E+01-0.365990E+01-0.378020E+01-0.336100E+01-0.260028E+01
 -0.287334E+01-0.357379E+01-0.152448E+01-0.144552E+01-0.152236E+01-0.750255E+00 0.955267E+00-0.155461E+01-0.292438E+01-0.240582E+01
 -0.200527E+01-0.405335E+01-0.340936E+01-0.250410E+01-0.287148E+01-0.299037E+01-0.266619E+01-0.118115E+01-0.155099E+01-0.374151E+00
 -0.277725E+01-0.388112E+01-0.376164E+01-0.283234E+01-0.443834E+01-0.160190E+01-0.133520E+01-0.168096E+01-0.459124E+01-0.280667E+01
 -0.220410E+01-0.135623E+01-0.747446E+00-0.330496E+02-0.787938E+01-0.692069E+01-0.675716E+01-0.704599E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1      1892      1892
 blstr diagel,allext,nmin2=  1892
 onel.diag.
 -0.194489E+01-0.285984E+01-0.270263E+01-0.267917E+01-0.404205E+01-0.353093E+01-0.365990E+01-0.378020E+01-0.336100E+01-0.260028E+01
 -0.287334E+01-0.357379E+01-0.152448E+01-0.144552E+01-0.152236E+01-0.750255E+00 0.955267E+00-0.155461E+01-0.292438E+01-0.240582E+01
 -0.200527E+01-0.405335E+01-0.340936E+01-0.250410E+01-0.287148E+01-0.299037E+01-0.266619E+01-0.118115E+01-0.155099E+01-0.374151E+00
 -0.277725E+01-0.388112E+01-0.376164E+01-0.283234E+01-0.443834E+01-0.160190E+01-0.133520E+01-0.168096E+01-0.459124E+01-0.280667E+01
 -0.220410E+01-0.135623E+01-0.747446E+00-0.330496E+02-0.787938E+01-0.692069E+01-0.675716E+01-0.704599E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1      1892      1892
 DIAGEL COUNTS: DG0X,DG2X,DG4X= 440 145 30
 =========== Executing IN-CORE method ==========
 norm multnew:  1.
 threx: total number of loops= 14
 threx: total number of loops= 24
 blstr diagel,allext,nmin2=  1892
 onel.diag.
 -0.194489E+01-0.285984E+01-0.270263E+01-0.267917E+01-0.404205E+01-0.353093E+01-0.365990E+01-0.378020E+01-0.336100E+01-0.260028E+01
 -0.287334E+01-0.357379E+01-0.152448E+01-0.144552E+01-0.152236E+01-0.750255E+00 0.955267E+00-0.155461E+01-0.292438E+01-0.240582E+01
 -0.200527E+01-0.405335E+01-0.340936E+01-0.250410E+01-0.287148E+01-0.299037E+01-0.266619E+01-0.118115E+01-0.155099E+01-0.374151E+00
 -0.277725E+01-0.388112E+01-0.376164E+01-0.283234E+01-0.443834E+01-0.160190E+01-0.133520E+01-0.168096E+01-0.459124E+01-0.280667E+01
 -0.220410E+01-0.135623E+01-0.747446E+00-0.330496E+02-0.787938E+01-0.692069E+01-0.675716E+01-0.704599E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1      1892      1892
 blstr diagel,allext,nmin2=  1892
 onel.diag.
 -0.194489E+01-0.285984E+01-0.270263E+01-0.267917E+01-0.404205E+01-0.353093E+01-0.365990E+01-0.378020E+01-0.336100E+01-0.260028E+01
 -0.287334E+01-0.357379E+01-0.152448E+01-0.144552E+01-0.152236E+01-0.750255E+00 0.955267E+00-0.155461E+01-0.292438E+01-0.240582E+01
 -0.200527E+01-0.405335E+01-0.340936E+01-0.250410E+01-0.287148E+01-0.299037E+01-0.266619E+01-0.118115E+01-0.155099E+01-0.374151E+00
 -0.277725E+01-0.388112E+01-0.376164E+01-0.283234E+01-0.443834E+01-0.160190E+01-0.133520E+01-0.168096E+01-0.459124E+01-0.280667E+01
 -0.220410E+01-0.135623E+01-0.747446E+00-0.330496E+02-0.787938E+01-0.692069E+01-0.675716E+01-0.704599E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1      1892      1892
 blstr diagel,allext,nmin2=  1892
 onel.diag.
 -0.194489E+01-0.285984E+01-0.270263E+01-0.267917E+01-0.404205E+01-0.353093E+01-0.365990E+01-0.378020E+01-0.336100E+01-0.260028E+01
 -0.287334E+01-0.357379E+01-0.152448E+01-0.144552E+01-0.152236E+01-0.750255E+00 0.955267E+00-0.155461E+01-0.292438E+01-0.240582E+01
 -0.200527E+01-0.405335E+01-0.340936E+01-0.250410E+01-0.287148E+01-0.299037E+01-0.266619E+01-0.118115E+01-0.155099E+01-0.374151E+00
 -0.277725E+01-0.388112E+01-0.376164E+01-0.283234E+01-0.443834E+01-0.160190E+01-0.133520E+01-0.168096E+01-0.459124E+01-0.280667E+01
 -0.220410E+01-0.135623E+01-0.747446E+00-0.330496E+02-0.787938E+01-0.692069E+01-0.675716E+01-0.704599E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1      1892      1892


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        12 xx:        36 ww:        79
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        11
task #   9:        28    task #  10:        71    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 matrix-vector product formed for vector   1
 =========== Executing IN-CORE method ==========
 norm multnew:  1.
 threx: total number of loops= 14
 threx: total number of loops= 24
 blstr diagel,allext,nmin2=  1892
 onel.diag.
 -0.194489E+01-0.285984E+01-0.270263E+01-0.267917E+01-0.404205E+01-0.353093E+01-0.365990E+01-0.378020E+01-0.336100E+01-0.260028E+01
 -0.287334E+01-0.357379E+01-0.152448E+01-0.144552E+01-0.152236E+01-0.750255E+00 0.955267E+00-0.155461E+01-0.292438E+01-0.240582E+01
 -0.200527E+01-0.405335E+01-0.340936E+01-0.250410E+01-0.287148E+01-0.299037E+01-0.266619E+01-0.118115E+01-0.155099E+01-0.374151E+00
 -0.277725E+01-0.388112E+01-0.376164E+01-0.283234E+01-0.443834E+01-0.160190E+01-0.133520E+01-0.168096E+01-0.459124E+01-0.280667E+01
 -0.220410E+01-0.135623E+01-0.747446E+00-0.330496E+02-0.787938E+01-0.692069E+01-0.675716E+01-0.704599E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1      1892      1892
 blstr diagel,allext,nmin2=  1892
 onel.diag.
 -0.194489E+01-0.285984E+01-0.270263E+01-0.267917E+01-0.404205E+01-0.353093E+01-0.365990E+01-0.378020E+01-0.336100E+01-0.260028E+01
 -0.287334E+01-0.357379E+01-0.152448E+01-0.144552E+01-0.152236E+01-0.750255E+00 0.955267E+00-0.155461E+01-0.292438E+01-0.240582E+01
 -0.200527E+01-0.405335E+01-0.340936E+01-0.250410E+01-0.287148E+01-0.299037E+01-0.266619E+01-0.118115E+01-0.155099E+01-0.374151E+00
 -0.277725E+01-0.388112E+01-0.376164E+01-0.283234E+01-0.443834E+01-0.160190E+01-0.133520E+01-0.168096E+01-0.459124E+01-0.280667E+01
 -0.220410E+01-0.135623E+01-0.747446E+00-0.330496E+02-0.787938E+01-0.692069E+01-0.675716E+01-0.704599E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1      1892      1892
 blstr diagel,allext,nmin2=  1892
 onel.diag.
 -0.194489E+01-0.285984E+01-0.270263E+01-0.267917E+01-0.404205E+01-0.353093E+01-0.365990E+01-0.378020E+01-0.336100E+01-0.260028E+01
 -0.287334E+01-0.357379E+01-0.152448E+01-0.144552E+01-0.152236E+01-0.750255E+00 0.955267E+00-0.155461E+01-0.292438E+01-0.240582E+01
 -0.200527E+01-0.405335E+01-0.340936E+01-0.250410E+01-0.287148E+01-0.299037E+01-0.266619E+01-0.118115E+01-0.155099E+01-0.374151E+00
 -0.277725E+01-0.388112E+01-0.376164E+01-0.283234E+01-0.443834E+01-0.160190E+01-0.133520E+01-0.168096E+01-0.459124E+01-0.280667E+01
 -0.220410E+01-0.135623E+01-0.747446E+00-0.330496E+02-0.787938E+01-0.692069E+01-0.675716E+01-0.704599E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1      1892      1892


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        12 xx:        36 ww:        79
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        11
task #   9:        28    task #  10:        71    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 matrix-vector product formed for vector   2
 =========== Executing IN-CORE method ==========
 norm multnew:  1.
 threx: total number of loops= 14
 threx: total number of loops= 24
 blstr diagel,allext,nmin2=  1892
 onel.diag.
 -0.194489E+01-0.285984E+01-0.270263E+01-0.267917E+01-0.404205E+01-0.353093E+01-0.365990E+01-0.378020E+01-0.336100E+01-0.260028E+01
 -0.287334E+01-0.357379E+01-0.152448E+01-0.144552E+01-0.152236E+01-0.750255E+00 0.955267E+00-0.155461E+01-0.292438E+01-0.240582E+01
 -0.200527E+01-0.405335E+01-0.340936E+01-0.250410E+01-0.287148E+01-0.299037E+01-0.266619E+01-0.118115E+01-0.155099E+01-0.374151E+00
 -0.277725E+01-0.388112E+01-0.376164E+01-0.283234E+01-0.443834E+01-0.160190E+01-0.133520E+01-0.168096E+01-0.459124E+01-0.280667E+01
 -0.220410E+01-0.135623E+01-0.747446E+00-0.330496E+02-0.787938E+01-0.692069E+01-0.675716E+01-0.704599E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1      1892      1892
 blstr diagel,allext,nmin2=  1892
 onel.diag.
 -0.194489E+01-0.285984E+01-0.270263E+01-0.267917E+01-0.404205E+01-0.353093E+01-0.365990E+01-0.378020E+01-0.336100E+01-0.260028E+01
 -0.287334E+01-0.357379E+01-0.152448E+01-0.144552E+01-0.152236E+01-0.750255E+00 0.955267E+00-0.155461E+01-0.292438E+01-0.240582E+01
 -0.200527E+01-0.405335E+01-0.340936E+01-0.250410E+01-0.287148E+01-0.299037E+01-0.266619E+01-0.118115E+01-0.155099E+01-0.374151E+00
 -0.277725E+01-0.388112E+01-0.376164E+01-0.283234E+01-0.443834E+01-0.160190E+01-0.133520E+01-0.168096E+01-0.459124E+01-0.280667E+01
 -0.220410E+01-0.135623E+01-0.747446E+00-0.330496E+02-0.787938E+01-0.692069E+01-0.675716E+01-0.704599E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1      1892      1892
 blstr diagel,allext,nmin2=  1892
 onel.diag.
 -0.194489E+01-0.285984E+01-0.270263E+01-0.267917E+01-0.404205E+01-0.353093E+01-0.365990E+01-0.378020E+01-0.336100E+01-0.260028E+01
 -0.287334E+01-0.357379E+01-0.152448E+01-0.144552E+01-0.152236E+01-0.750255E+00 0.955267E+00-0.155461E+01-0.292438E+01-0.240582E+01
 -0.200527E+01-0.405335E+01-0.340936E+01-0.250410E+01-0.287148E+01-0.299037E+01-0.266619E+01-0.118115E+01-0.155099E+01-0.374151E+00
 -0.277725E+01-0.388112E+01-0.376164E+01-0.283234E+01-0.443834E+01-0.160190E+01-0.133520E+01-0.168096E+01-0.459124E+01-0.280667E+01
 -0.220410E+01-0.135623E+01-0.747446E+00-0.330496E+02-0.787938E+01-0.692069E+01-0.675716E+01-0.704599E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1      1892      1892


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        12 xx:        36 ww:        79
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        11
task #   9:        28    task #  10:        71    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 matrix-vector product formed for vector   3
 =========== Executing IN-CORE method ==========
 norm multnew:  1.88360028E-07
 threx: total number of loops= 14
 threx: total number of loops= 24
 blstr diagel,allext,nmin2=  1892
 onel.diag.
 -0.194489E+01-0.285984E+01-0.270263E+01-0.267917E+01-0.404205E+01-0.353093E+01-0.365990E+01-0.378020E+01-0.336100E+01-0.260028E+01
 -0.287334E+01-0.357379E+01-0.152448E+01-0.144552E+01-0.152236E+01-0.750255E+00 0.955267E+00-0.155461E+01-0.292438E+01-0.240582E+01
 -0.200527E+01-0.405335E+01-0.340936E+01-0.250410E+01-0.287148E+01-0.299037E+01-0.266619E+01-0.118115E+01-0.155099E+01-0.374151E+00
 -0.277725E+01-0.388112E+01-0.376164E+01-0.283234E+01-0.443834E+01-0.160190E+01-0.133520E+01-0.168096E+01-0.459124E+01-0.280667E+01
 -0.220410E+01-0.135623E+01-0.747446E+00-0.330496E+02-0.787938E+01-0.692069E+01-0.675716E+01-0.704599E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1      1892      1892
 blstr diagel,allext,nmin2=  1892
 onel.diag.
 -0.194489E+01-0.285984E+01-0.270263E+01-0.267917E+01-0.404205E+01-0.353093E+01-0.365990E+01-0.378020E+01-0.336100E+01-0.260028E+01
 -0.287334E+01-0.357379E+01-0.152448E+01-0.144552E+01-0.152236E+01-0.750255E+00 0.955267E+00-0.155461E+01-0.292438E+01-0.240582E+01
 -0.200527E+01-0.405335E+01-0.340936E+01-0.250410E+01-0.287148E+01-0.299037E+01-0.266619E+01-0.118115E+01-0.155099E+01-0.374151E+00
 -0.277725E+01-0.388112E+01-0.376164E+01-0.283234E+01-0.443834E+01-0.160190E+01-0.133520E+01-0.168096E+01-0.459124E+01-0.280667E+01
 -0.220410E+01-0.135623E+01-0.747446E+00-0.330496E+02-0.787938E+01-0.692069E+01-0.675716E+01-0.704599E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1      1892      1892
 blstr diagel,allext,nmin2=  1892
 onel.diag.
 -0.194489E+01-0.285984E+01-0.270263E+01-0.267917E+01-0.404205E+01-0.353093E+01-0.365990E+01-0.378020E+01-0.336100E+01-0.260028E+01
 -0.287334E+01-0.357379E+01-0.152448E+01-0.144552E+01-0.152236E+01-0.750255E+00 0.955267E+00-0.155461E+01-0.292438E+01-0.240582E+01
 -0.200527E+01-0.405335E+01-0.340936E+01-0.250410E+01-0.287148E+01-0.299037E+01-0.266619E+01-0.118115E+01-0.155099E+01-0.374151E+00
 -0.277725E+01-0.388112E+01-0.376164E+01-0.283234E+01-0.443834E+01-0.160190E+01-0.133520E+01-0.168096E+01-0.459124E+01-0.280667E+01
 -0.220410E+01-0.135623E+01-0.747446E+00-0.330496E+02-0.787938E+01-0.692069E+01-0.675716E+01-0.704599E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1      1892      1892


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        12 xx:        36 ww:        79
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        11
task #   9:        28    task #  10:        71    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 matrix-vector product formed for vector   4
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference-subspace vector overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.97234180     0.06649896    -0.09372291     0.00000000

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.97234073    -0.06572283     0.03785464     0.08847894
 subspace has dimension  4

          (h-repnuc*1) matrix in the subspace basis

                x:   1         x:   2         x:   3         x:   4
   x:   1   -85.67245276
   x:   2    -0.00036856   -83.87106855
   x:   3    -0.00050015     0.00069489   -82.26740199
   x:   4     0.00004199    -0.00501832    -0.01027778    -0.00001555

          overlap matrix in the subspace basis

                x:   1         x:   2         x:   3         x:   4
   x:   1     1.00000000
   x:   2     0.00000000     1.00000000
   x:   3     0.00000000     0.00000000     1.00000000
   x:   4    -0.00000050     0.00006117     0.00012303     0.00000019

          eigenvectors and eigenvalues in the subspace basis

                v:   1         v:   2         v:   3         v:   4

   energy   -85.67245398   -83.92289186   -82.68162368   -81.93016899

   x:   1     1.00000051     0.00022350    -0.00030067     0.00056617
   x:   2     0.00003043    -1.00923136    -0.05918376    -0.00552657
   x:   3     0.00003822    -0.01251223    -0.44901129    -0.94209562
   x:   4     1.40896891   452.38511781 -1765.40468060  1605.94690721

          eigenvectors and eigenvalues in the schmidt orthonormalized basis

                v:   1         v:   2         v:   3         v:   4

   energy   -85.67245398   -83.92289186   -82.68162368   -81.93016899

  zx:   1     0.99999980    -0.00000269     0.00058201    -0.00023678
  zx:   2     0.00011662    -0.98155685    -0.16718178     0.09271667
  zx:   3     0.00021157     0.04314519    -0.66621090    -0.74451426
  zx:   4     0.00058004     0.18623811    -0.72678260     0.66113696

          <ref|baseci> overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.97234180     0.06649896    -0.09372291     0.00000000

  write the transformed vectors back to cosmofile

 rtflw =  0
 freezing =  F

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  7  1    -76.3378734950 -9.3348E+00  4.2867E-06  2.7501E-03  1.0000E-03
 mr-sdci #  7  2    -74.5883113734 -9.2841E+00  0.0000E+00  9.9986E-01  1.0000E-04
 mr-sdci #  7  3    -73.3470431913 -8.9231E+00  0.0000E+00  2.6940E+00  1.0000E-04
 mr-sdci #  7  4    -72.5955884997 -8.4310E+00  0.0000E+00  2.8401E+00  1.0000E-04


 total energy in cosmo calc 
e(rootcalc) + repnuc=     -76.3378734950
dielectric energy =      -0.0107050726
deltaediel =       0.0107050726
e(rootcalc) + repnuc - ediel =     -76.3271684225
e(rootcalc) + repnuc - edielnew =     -76.3378734950
deltaelast =      76.3271684225

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.02   0.00   0.00   0.01         0.    0.0000
    2   25    0   0.03   0.00   0.00   0.02         0.    0.0000
    3   26    0   0.04   0.01   0.00   0.04         0.    0.0000
    4   11    0   0.02   0.00   0.00   0.02         0.    0.0000
    5   15    0   0.04   0.01   0.00   0.04         0.    0.0000
    6   16    0   0.04   0.00   0.00   0.03         0.    0.0000
    7    1    0   0.02   0.00   0.00   0.02         0.    0.0000
    8    5    0   0.04   0.00   0.00   0.04         0.    0.0000
    9    6    0   0.03   0.00   0.00   0.02         0.    0.0000
   10    7    0   0.04   0.00   0.00   0.04         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.01   0.00   0.00   0.01         0.    0.0000
   13   46    0   0.03   0.00   0.00   0.03         0.    0.0000
   14   47    0   0.05   0.00   0.00   0.04         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.020000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.4100s 
time spent in multnx:                   0.3600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0200s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            2.9300s 

          starting ci iteration   8

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.33458049

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=       3  D0X=       9  D0W=      15
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================

          ci-one-electron density block   1

                mo   1         mo   2         mo   3         mo   4         mo   5         mo   6         mo   7         mo   8
   mo   1     1.99955680
   mo   2    -0.00013983     1.98336624
   mo   3    -0.00013040     0.00305964     1.96960684
   mo   4    -0.00014454     0.00142495     0.01231488     0.00120301
   mo   5     0.00019783     0.00390293    -0.00993813    -0.00248209     0.00656427
   mo   6    -0.00019217     0.00493620     0.00468044     0.00026922     0.00061744     0.00284903
   mo   7     0.00034636    -0.00577061    -0.01478734    -0.00100162     0.00053092    -0.00134265     0.00264890
   mo   8    -0.00024364    -0.00650169     0.00450977     0.00272699    -0.00786842    -0.00035851     0.00013930     0.01039568
   mo   9    -0.00031104     0.00606735     0.00789285     0.00018616     0.00128043     0.00291285    -0.00195567    -0.00125497
   mo  10     0.00012419     0.00205030    -0.00499435    -0.00063650     0.00154979     0.00211725     0.00067269    -0.00109333
   mo  11     0.00063294    -0.00069369     0.00022294    -0.00065743    -0.00030315    -0.00143006     0.00257866     0.00142245
   mo  12     0.00059151     0.00200460    -0.00494512    -0.00086381     0.00131801    -0.00136452     0.00131786    -0.00175184
   mo  13    -0.00000729    -0.00129635     0.00145825     0.00026251    -0.00090558     0.00036164     0.00028096     0.00148559
   mo  14    -0.00027048    -0.00395253     0.00511152     0.00087734    -0.00228670    -0.00028399    -0.00035040     0.00302138
   mo  15    -0.00030847     0.00202148    -0.00652285    -0.00013641     0.00088346     0.00089962    -0.00057023    -0.00118445
   mo  16     0.00001314    -0.00013572     0.00058681     0.00001759    -0.00006508    -0.00015988    -0.00000801     0.00003992
   mo  17    -0.00024797     0.00009530    -0.00204101     0.00007488     0.00004713    -0.00001390    -0.00036059    -0.00039837
   mo  18    -0.00005343     0.00019680    -0.00028233     0.00005247    -0.00012007    -0.00016310    -0.00008490     0.00012870
   mo  19    -0.00014600     0.00045690    -0.00051175     0.00009463    -0.00015675     0.00033734    -0.00009964     0.00029519
   mo  20     0.00086079     0.00221928    -0.00032795    -0.00020274     0.00023770    -0.00027909     0.00045268    -0.00025056

                mo   9         mo  10         mo  11         mo  12         mo  13         mo  14         mo  15         mo  16
   mo   9     0.00528555
   mo  10     0.00065832     0.00422149
   mo  11    -0.00073713    -0.00042567     0.00439882
   mo  12    -0.00238556     0.00007734     0.00013500     0.00253499
   mo  13    -0.00065117     0.00130256    -0.00011880    -0.00001697     0.00120926
   mo  14    -0.00070452    -0.00088443    -0.00032999    -0.00016003     0.00035790     0.00158174
   mo  15     0.00157188     0.00056925    -0.00074206    -0.00040690    -0.00029176    -0.00054088     0.00112563
   mo  16    -0.00002548    -0.00039694     0.00013409     0.00000551    -0.00007016     0.00004311    -0.00004158     0.00039372
   mo  17    -0.00017905    -0.00027483    -0.00095025     0.00035966    -0.00013826     0.00004010     0.00007531     0.00000848
   mo  18     0.00037140    -0.00084641     0.00051529    -0.00040278    -0.00060538    -0.00000476    -0.00000182     0.00008868
   mo  19     0.00047024     0.00039005     0.00017920    -0.00062363     0.00024328    -0.00023381     0.00009997    -0.00004451
   mo  20    -0.00040119     0.00009005     0.00054606     0.00048635    -0.00001332    -0.00021949    -0.00014354     0.00000592

                mo  17         mo  18         mo  19         mo  20
   mo  17     0.00060851
   mo  18    -0.00012427     0.00067315
   mo  19    -0.00022301    -0.00000211     0.00043413
   mo  20    -0.00006725    -0.00002341    -0.00007141     0.00038132

          ci-one-electron density block   2

                mo  21         mo  22         mo  23         mo  24         mo  25         mo  26         mo  27         mo  28
   mo  21     1.96821514
   mo  22     0.00187140     0.00062376
   mo  23    -0.01518422    -0.00190163     0.00719668
   mo  24    -0.00178222     0.00074775    -0.00144350     0.00145199
   mo  25     0.00190067     0.00051476    -0.00184928     0.00044138     0.00052032
   mo  26    -0.01457093    -0.00194644     0.00848702    -0.00087128    -0.00213640     0.01108532
   mo  27    -0.00364772    -0.00126756     0.00291886    -0.00223963    -0.00086604     0.00232625     0.00382748
   mo  28     0.00151717     0.00015552    -0.00051453     0.00026228     0.00004569    -0.00066839    -0.00038668     0.00055698
   mo  29     0.00919391     0.00036674    -0.00228713    -0.00024072     0.00059487    -0.00361427     0.00021028     0.00025710
   mo  30     0.00535379     0.00059117    -0.00174648     0.00089191     0.00047459    -0.00192787    -0.00188922     0.00018428
   mo  31    -0.00540634    -0.00023315     0.00064039    -0.00039678    -0.00022099     0.00064240     0.00080742    -0.00013172
   mo  32     0.00093906     0.00003670    -0.00008637     0.00009665    -0.00002333    -0.00007388    -0.00013965     0.00026168
   mo  33    -0.00389609    -0.00000932     0.00030068     0.00015554    -0.00007931     0.00072666    -0.00028708    -0.00005012
   mo  34    -0.00131382    -0.00004033    -0.00007402    -0.00019866     0.00000670    -0.00029040     0.00044866     0.00007220

                mo  29         mo  30         mo  31         mo  32         mo  33         mo  34
   mo  29     0.00205982
   mo  30     0.00004964     0.00149666
   mo  31    -0.00024494    -0.00043189     0.00059933
   mo  32     0.00001847     0.00007515    -0.00002337     0.00032449
   mo  33    -0.00073737     0.00008025     0.00000587    -0.00000828     0.00053992
   mo  34     0.00043131    -0.00043033     0.00004851     0.00000896    -0.00020243     0.00034733

          ci-one-electron density block   3

                mo  35         mo  36         mo  37         mo  38         mo  39         mo  40         mo  41         mo  42
   mo  35     1.97122308
   mo  36     0.01399779     0.00565246
   mo  37     0.01235269     0.00616482     0.00858481
   mo  38     0.00421222    -0.00324469    -0.00184588     0.00462001
   mo  39     0.00013335     0.00031699     0.00149074     0.00129011     0.00139356
   mo  40    -0.00736530     0.00212286     0.00309768    -0.00113483     0.00048359     0.00188538
   mo  41     0.00001419     0.00001919    -0.00002223    -0.00004899    -0.00003844    -0.00000316     0.00038112
   mo  42    -0.00078673     0.00018381     0.00004086    -0.00050172    -0.00004012     0.00006567     0.00000449     0.00037900
   mo  43     0.00014515    -0.00022982     0.00023709     0.00110677     0.00077206    -0.00000132     0.00000440    -0.00014501

                mo  43
   mo  43     0.00076190

          ci-one-electron density block   4

                mo  44         mo  45         mo  46         mo  47         mo  48
   mo  44     0.00578015
   mo  45    -0.00074324     0.00082736
   mo  46     0.00035296    -0.00000354     0.00026367
   mo  47     0.00015377    -0.00014967     0.00016722     0.00020917
   mo  48    -0.00004994    -0.00013278    -0.00007176    -0.00001370     0.00015402


*****   symmetry block SYM1   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       1.99955680
       2      -0.00013983     1.98336624
       3      -0.00013040     0.00305964     1.96960684
       4      -0.00014454     0.00142495     0.01231488     0.00120301
       5       0.00019783     0.00390293    -0.00993813    -0.00248209
       6      -0.00019217     0.00493620     0.00468044     0.00026922
       7       0.00034636    -0.00577061    -0.01478734    -0.00100162
       8      -0.00024364    -0.00650169     0.00450977     0.00272699
       9      -0.00031104     0.00606735     0.00789285     0.00018616
      10       0.00012419     0.00205030    -0.00499435    -0.00063650
      11       0.00063294    -0.00069369     0.00022294    -0.00065743
      12       0.00059151     0.00200460    -0.00494512    -0.00086381
      13      -0.00000729    -0.00129635     0.00145825     0.00026251
      14      -0.00027048    -0.00395253     0.00511152     0.00087734
      15      -0.00030847     0.00202148    -0.00652285    -0.00013641
      16       0.00001314    -0.00013572     0.00058681     0.00001759
      17      -0.00024797     0.00009530    -0.00204101     0.00007488
      18      -0.00005343     0.00019680    -0.00028233     0.00005247
      19      -0.00014600     0.00045690    -0.00051175     0.00009463
      20       0.00086079     0.00221928    -0.00032795    -0.00020274

               Column   5     Column   6     Column   7     Column   8
       5       0.00656427
       6       0.00061744     0.00284903
       7       0.00053092    -0.00134265     0.00264890
       8      -0.00786842    -0.00035851     0.00013930     0.01039568
       9       0.00128043     0.00291285    -0.00195567    -0.00125497
      10       0.00154979     0.00211725     0.00067269    -0.00109333
      11      -0.00030315    -0.00143006     0.00257866     0.00142245
      12       0.00131801    -0.00136452     0.00131786    -0.00175184
      13      -0.00090558     0.00036164     0.00028096     0.00148559
      14      -0.00228670    -0.00028399    -0.00035040     0.00302138
      15       0.00088346     0.00089962    -0.00057023    -0.00118445
      16      -0.00006508    -0.00015988    -0.00000801     0.00003992
      17       0.00004713    -0.00001390    -0.00036059    -0.00039837
      18      -0.00012007    -0.00016310    -0.00008490     0.00012870
      19      -0.00015675     0.00033734    -0.00009964     0.00029519
      20       0.00023770    -0.00027909     0.00045268    -0.00025056

               Column   9     Column  10     Column  11     Column  12
       9       0.00528555
      10       0.00065832     0.00422149
      11      -0.00073713    -0.00042567     0.00439882
      12      -0.00238556     0.00007734     0.00013500     0.00253499
      13      -0.00065117     0.00130256    -0.00011880    -0.00001697
      14      -0.00070452    -0.00088443    -0.00032999    -0.00016003
      15       0.00157188     0.00056925    -0.00074206    -0.00040690
      16      -0.00002548    -0.00039694     0.00013409     0.00000551
      17      -0.00017905    -0.00027483    -0.00095025     0.00035966
      18       0.00037140    -0.00084641     0.00051529    -0.00040278
      19       0.00047024     0.00039005     0.00017920    -0.00062363
      20      -0.00040119     0.00009005     0.00054606     0.00048635

               Column  13     Column  14     Column  15     Column  16
      13       0.00120926
      14       0.00035790     0.00158174
      15      -0.00029176    -0.00054088     0.00112563
      16      -0.00007016     0.00004311    -0.00004158     0.00039372
      17      -0.00013826     0.00004010     0.00007531     0.00000848
      18      -0.00060538    -0.00000476    -0.00000182     0.00008868
      19       0.00024328    -0.00023381     0.00009997    -0.00004451
      20      -0.00001332    -0.00021949    -0.00014354     0.00000592

               Column  17     Column  18     Column  19     Column  20
      17       0.00060851
      18      -0.00012427     0.00067315
      19      -0.00022301    -0.00000211     0.00043413
      20      -0.00006725    -0.00002341    -0.00007141     0.00038132

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7        eno   8
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    8   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno   9        eno  10        eno  11        eno  12        eno  13        eno  14        eno  15        eno  16
 emo    9   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   10   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   11   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   12   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   13   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   14   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   15   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   16   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno  17        eno  18        eno  19        eno  20
 emo   17   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   18   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   19   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   20   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   1.9995600      1.9841316      1.9692918      0.0195166      0.0110050
   0.0060696      0.0052898      0.0011325      0.0008452      0.0005713
   0.0004722      0.0003661      0.0003554      0.0001567      0.0001090
   0.0000690      0.0000566      0.0000321      0.0000082      0.0000004

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7         no   8
  mo    1   0.999936E+00   0.107456E-01   0.349034E-02   0.154411E-03   0.510747E-03  -0.224980E-03   0.600946E-04  -0.957625E-04
  mo    2  -0.975783E-02   0.977065E+00  -0.212604E+00   0.494454E-02  -0.346661E-02   0.114348E-03   0.448640E-03  -0.623879E-03
  mo    3  -0.569029E-02   0.212531E+00   0.977045E+00  -0.652990E-02  -0.769684E-02   0.372368E-02   0.167007E-02   0.130026E-02
  mo    4  -0.115142E-03   0.201833E-02   0.597806E-02   0.194622E+00   0.148910E+00  -0.111536E+00   0.520855E-01  -0.796898E-01
  mo    5   0.109311E-03   0.874773E-03  -0.539775E-02  -0.564295E+00  -0.884421E-01   0.653775E-01  -0.597025E-01   0.733899E-01
  mo    6  -0.134612E-03   0.294558E-02   0.179648E-02  -0.853489E-01   0.433560E+00   0.244277E+00   0.110162E+00   0.209904E+00
  mo    7   0.244967E-03  -0.443604E-02  -0.673259E-02   0.548694E-02  -0.376338E+00   0.363078E+00  -0.741240E-01   0.193261E+00
  mo    8  -0.104127E-03  -0.274195E-02   0.299327E-02   0.713571E+00   0.102014E+00   0.202717E+00  -0.128493E-02   0.200771E+00
  mo    9  -0.209099E-03   0.385350E-02   0.327190E-02  -0.150460E+00   0.565075E+00   0.108302E+00  -0.433017E+00   0.345379E+00
  mo   10   0.664932E-04   0.481398E-03  -0.271380E-02  -0.144087E+00   0.142473E+00   0.641748E+00   0.439162E+00  -0.310818E-01
  mo   11   0.320651E-03  -0.326622E-03   0.178160E-03   0.901522E-01  -0.338703E+00   0.440671E+00  -0.578040E+00   0.297034E-01
  mo   12   0.301361E-03   0.453960E-03  -0.269070E-02  -0.976157E-01  -0.357189E+00  -0.717621E-01   0.282822E+00   0.573315E+00
  mo   13  -0.150834E-05  -0.486013E-03   0.867141E-03   0.863769E-01   0.150678E-01   0.242318E+00   0.288576E+00  -0.200672E+00
  mo   14  -0.131014E-03  -0.140725E-02   0.298053E-02   0.217034E+00   0.190188E-01  -0.124036E+00   0.104297E+00   0.358484E+00
  mo   15  -0.145985E-03   0.303361E-03  -0.345754E-02  -0.102462E+00   0.182401E+00   0.971308E-02  -0.417983E-01   0.190386E+00
  mo   16   0.557936E-05  -0.437728E-05   0.306660E-03   0.828764E-02  -0.176473E-01  -0.480555E-01  -0.578656E-01   0.258373E-01
  mo   17  -0.118823E-03  -0.171874E-03  -0.102353E-02  -0.186025E-01   0.160164E-01  -0.169425E+00   0.135573E+00   0.204106E+00
  mo   18  -0.269426E-04   0.666606E-04  -0.158454E-03   0.152924E-01   0.404536E-02  -0.810163E-01  -0.247639E+00  -0.975735E-02
  mo   19  -0.739606E-04   0.170706E-03  -0.300924E-03   0.109374E-01   0.708279E-01   0.112373E+00  -0.492012E-01  -0.398859E+00
  mo   20   0.420993E-03   0.106081E-02  -0.405748E-03  -0.155527E-01  -0.924629E-01   0.580208E-01  -0.150627E-01   0.100898E+00

                no   9         no  10         no  11         no  12         no  13         no  14         no  15         no  16
  mo    1  -0.503572E-04   0.360708E-04  -0.321399E-03   0.172808E-03   0.190779E-03   0.300962E-04  -0.964028E-04   0.472051E-04
  mo    2  -0.104156E-02   0.262417E-03  -0.231579E-02   0.108824E-02   0.130730E-02  -0.267832E-03   0.432388E-03  -0.724717E-03
  mo    3   0.280438E-02  -0.376193E-03  -0.459936E-02   0.242833E-02   0.178892E-02   0.197071E-02   0.216080E-02  -0.210529E-02
  mo    4   0.140049E+00  -0.471316E-01   0.154101E+00  -0.880241E-01  -0.118145E+00  -0.108117E+00  -0.162469E+00   0.124454E+00
  mo    5  -0.386668E+00   0.582440E-01  -0.127746E+00   0.260742E-01   0.248805E-01   0.182082E+00   0.279163E+00  -0.239092E-01
  mo    6  -0.165823E+00  -0.319835E+00   0.247674E+00   0.126939E+00  -0.327342E-01   0.792308E-01  -0.339086E-01  -0.236593E-02
  mo    7   0.102451E+00  -0.183870E-01  -0.211427E+00   0.218292E+00   0.258337E+00   0.505946E-01  -0.697521E-01  -0.270205E+00
  mo    8  -0.328132E-01   0.557064E-03  -0.660747E-01   0.359754E-01   0.465418E-01   0.188129E+00   0.155181E+00  -0.880259E-01
  mo    9   0.384496E-02   0.298761E+00   0.120113E+00  -0.112503E+00   0.109395E+00   0.577424E-01  -0.200666E+00  -0.144408E-01
  mo   10   0.959986E-01  -0.227489E+00  -0.908279E-01   0.318428E-02  -0.562349E-01  -0.184076E+00  -0.572628E-01  -0.371989E-01
  mo   11  -0.314503E-01   0.513224E-01   0.143674E+00  -0.593927E-01  -0.102785E-01  -0.126320E+00   0.672457E-03   0.256785E+00
  mo   12   0.150386E+00   0.789282E-01   0.200109E+00  -0.158644E+00  -0.213732E+00   0.325557E+00  -0.409926E+00   0.112427E+00
  mo   13  -0.194073E+00   0.492036E+00   0.108351E+00   0.560940E-01  -0.408222E-01   0.255996E+00   0.171227E+00   0.570479E+00
  mo   14  -0.566252E+00   0.590236E-01  -0.338307E+00  -0.746560E-01  -0.102571E+00  -0.218356E-01   0.139397E+00  -0.151380E+00
  mo   15   0.573721E+00   0.197491E+00  -0.537310E+00   0.626962E-01  -0.274194E+00   0.589578E-01   0.303053E+00   0.117513E+00
  mo   16  -0.552634E-01   0.188565E+00   0.207648E+00   0.814007E+00  -0.429204E+00  -0.112231E+00  -0.630996E-01  -0.162739E+00
  mo   17   0.208080E+00  -0.205421E-01   0.228649E+00   0.327717E+00   0.672271E+00   0.135444E+00   0.290417E+00   0.953202E-01
  mo   18  -0.280626E-01  -0.631560E+00  -0.118233E+00   0.162177E+00  -0.189724E+00   0.434080E+00   0.579309E-01   0.378061E+00
  mo   19   0.590984E-01   0.140976E+00  -0.755351E-02  -0.352320E-01  -0.892189E-02   0.679375E+00  -0.162195E+00  -0.445771E+00
  mo   20   0.144452E+00  -0.507259E-01   0.491738E+00  -0.273758E+00  -0.308085E+00   0.368593E-01   0.628814E+00  -0.289162E+00

                no  17         no  18         no  19         no  20
  mo    1  -0.507927E-04  -0.201689E-04  -0.821916E-05   0.682897E-06
  mo    2   0.882072E-03   0.792168E-03   0.103086E-04  -0.182094E-03
  mo    3   0.756174E-03   0.203253E-02   0.199101E-02  -0.124964E-02
  mo    4  -0.608509E-01  -0.128614E+00  -0.256353E+00   0.843552E+00
  mo    5  -0.181999E+00  -0.102887E+00   0.385673E+00   0.431118E+00
  mo    6  -0.500459E+00   0.424632E+00  -0.195174E+00  -0.651595E-01
  mo    7   0.291866E+00   0.473869E+00  -0.218131E+00   0.275544E+00
  mo    8  -0.994018E-01  -0.288169E-01   0.547214E+00   0.112736E+00
  mo    9   0.402285E+00  -0.632852E-01   0.383086E-01   0.820360E-02
  mo   10   0.208404E+00  -0.444008E+00   0.297201E-01  -0.294530E-01
  mo   11  -0.347895E+00  -0.277039E+00  -0.188820E+00  -0.687035E-01
  mo   12  -0.777939E-01  -0.898340E-01   0.839657E-01  -0.384049E-01
  mo   13   0.174934E+00   0.213173E+00  -0.131859E+00  -0.602554E-03
  mo   14   0.378679E-01  -0.244867E+00  -0.486954E+00  -0.416713E-01
  mo   15  -0.243463E+00   0.241123E-01  -0.114682E+00  -0.475487E-02
  mo   16   0.536299E-01  -0.131684E+00   0.382585E-01   0.356002E-03
  mo   17  -0.662141E-01  -0.331573E+00  -0.170754E+00  -0.712018E-02
  mo   18   0.340106E+00  -0.600547E-01  -0.425582E-01  -0.580291E-02
  mo   19  -0.152563E+00  -0.203597E+00  -0.209961E+00  -0.245408E-01
  mo   20   0.218589E+00   0.536720E-01  -0.105765E+00   0.998620E-02
  eigenvectors of nos in ao-basis, column    1
  0.9975072E+00 -0.1493818E-01  0.3345823E-02  0.4258378E-02 -0.7901978E-02  0.3745548E-02  0.4538048E-03  0.1033748E-04
  0.3273569E-05  0.1127977E-04 -0.2590274E-03  0.2160897E-04  0.5482572E-04  0.7048815E-03 -0.1417177E-02  0.4887047E-04
  0.4674661E-03  0.2369560E-03 -0.5309802E-03 -0.5354504E-03
  eigenvectors of nos in ao-basis, column    2
  0.3031943E-02  0.9126582E+00 -0.2960827E-02 -0.1330434E+00  0.4557884E-01  0.1022241E-01  0.7435573E-01  0.1580399E-03
  0.2545385E-02 -0.1587698E-02 -0.2563438E-02 -0.7770897E-03 -0.1740185E-02  0.2569228E+00 -0.1114722E+00  0.3057166E-02
  0.2605284E-01  0.1919298E-01  0.1608591E-01  0.1207443E-01
  eigenvectors of nos in ao-basis, column    3
  0.9104524E-02  0.1090311E+00  0.3548200E-02  0.1553089E+00  0.8351370E+00 -0.2682721E-01 -0.2935815E-01 -0.1611818E-02
 -0.2676798E-02 -0.5426128E-02  0.1936177E-02 -0.8104060E-04  0.2672454E-02 -0.3986334E+00  0.1691694E+00  0.5832149E-02
 -0.2996917E-01 -0.1441039E-02 -0.1582169E-02  0.2930514E-01
  eigenvectors of nos in ao-basis, column    4
 -0.4335221E-01 -0.6090126E+00 -0.6183296E-01  0.2270252E-02  0.1320887E+01 -0.2006769E+00 -0.6926575E+00  0.3400098E-02
  0.1553641E-01  0.8683734E-02  0.2574700E-01  0.2152503E-02  0.3607816E-02  0.9570824E+00 -0.4161233E+00 -0.8443793E-02
  0.5936101E-01  0.9658760E-02  0.3567235E-02 -0.3789839E-01
  eigenvectors of nos in ao-basis, column    5
  0.1524908E-01 -0.1071495E+01 -0.4387145E+00  0.1098657E+01 -0.8216266E+00  0.3822582E+00  0.8185046E+00 -0.5177428E-02
  0.3203733E-01 -0.2364833E-01  0.4223232E-01 -0.2441815E-02 -0.3045317E-02  0.4392338E+00 -0.2493558E+00  0.2026148E-01
  0.2567094E-01  0.7914622E-01  0.1330199E-01  0.5818883E-01
  eigenvectors of nos in ao-basis, column    6
 -0.1058260E-01  0.4864594E+00  0.2553586E+00 -0.9511711E+00 -0.7041373E-01  0.3103598E-01  0.1423155E+00 -0.3507351E-01
  0.1152262E+00 -0.8736608E-01  0.2527199E+00 -0.1432962E-01 -0.2091453E-01  0.3129487E+00 -0.1496539E+00 -0.1174865E-01
 -0.3241332E-01  0.8230073E-01 -0.3433535E-01  0.2175145E-01
  eigenvectors of nos in ao-basis, column    7
 -0.2444478E-02 -0.1372082E+00 -0.5644700E-01  0.2192143E+00  0.2754997E+00 -0.1199918E+00 -0.4007246E+00 -0.8673838E-01
 -0.8406482E-01 -0.1767570E+00 -0.1644079E+00 -0.9830206E-02  0.1362726E-01 -0.8062844E-01  0.6175971E-01  0.5420333E-02
 -0.5180333E-01  0.4040123E-01 -0.3430542E-02  0.3937803E-01
  eigenvectors of nos in ao-basis, column    8
 -0.1238392E+00  0.2325919E-01  0.4313725E+00 -0.1917940E+01 -0.2125228E+00  0.2976808E+00  0.9720741E+00  0.2010866E-01
 -0.2456319E+00  0.1523390E-01 -0.2155467E+00 -0.3382318E-01 -0.8754305E-01  0.2110078E+01 -0.1008654E+01 -0.2254763E-02
  0.7013855E-01  0.1637492E+00  0.6669730E-01  0.6858918E-01
  eigenvectors of nos in ao-basis, column    9
 -0.2185179E+00 -0.2793148E+00  0.4155618E+00  0.2787200E+00 -0.2530942E+01  0.2373559E+01  0.4605107E+00 -0.2162332E-01
 -0.4450353E-01  0.1239796E-01  0.3749152E-01  0.8711468E-02  0.4026243E-01 -0.3140355E-01  0.1318863E+00 -0.3944833E-01
 -0.1686867E+00 -0.1018807E+00 -0.5555205E-01 -0.1463344E+00
  eigenvectors of nos in ao-basis, column   10
  0.1657597E+00  0.3687077E+00 -0.2010540E+00 -0.6512355E+00 -0.1556902E+00  0.1996760E+00  0.8287448E+00 -0.2105481E+00
  0.3101526E-01  0.1287418E-01  0.1538127E-01  0.8737299E-01 -0.1180129E+00  0.6614393E+00 -0.3618864E+00 -0.1076265E-01
  0.3316720E+00 -0.2871198E+00  0.9370968E-01 -0.1770318E+00
  eigenvectors of nos in ao-basis, column   11
 -0.1377101E+01 -0.2885465E+01  0.1999631E+01  0.1920131E+01  0.8272390E+00 -0.1102230E+01  0.5979878E-01 -0.3361150E-01
 -0.1033858E+00  0.1699102E-01  0.1387587E+00  0.1434310E+00  0.1628625E+00 -0.2253956E+00  0.2177951E+00 -0.2947718E-02
 -0.1464993E+00 -0.1966901E+00 -0.2542805E-01  0.9250332E-01
  eigenvectors of nos in ao-basis, column   12
  0.8811837E+00  0.1941906E+01 -0.1239250E+01 -0.8461045E+00 -0.1925726E+00  0.2190168E+00 -0.3287151E+00  0.8001910E-01
 -0.1450357E+00 -0.4250273E-01  0.1131979E+00  0.3099069E+00 -0.7512003E-01 -0.2233849E+00  0.2818001E-01 -0.1321890E-01
 -0.6097401E-01  0.1617885E-01 -0.1602945E+00  0.4959950E-01
  eigenvectors of nos in ao-basis, column   13
  0.1021862E+01  0.2265731E+01 -0.1429654E+01 -0.7307613E+00  0.2371333E+00 -0.3779956E+00 -0.1290937E-01 -0.9631484E-01
 -0.2558715E+00  0.8054512E-01  0.2305929E+00 -0.6887207E-01  0.3149363E+00 -0.1906468E+00 -0.1044387E+00 -0.2230379E-01
 -0.9919499E-01 -0.6898998E-01 -0.1515973E+00 -0.1363900E+00
  eigenvectors of nos in ao-basis, column   14
  0.3041130E+00  0.8549532E+00 -0.1818703E+00 -0.3165879E+01 -0.7907906E+00  0.1253507E+01  0.1069697E+01  0.9118610E-01
  0.3118968E+00 -0.1707035E+00 -0.4945383E+00  0.6552820E-01  0.3255983E+00  0.2749856E+01 -0.1268277E+01  0.2243208E-01
  0.4405480E+00 -0.1140218E+00  0.1790454E+00  0.2360681E+00
  eigenvectors of nos in ao-basis, column   15
 -0.2217133E+01 -0.5885802E+01  0.2473012E+01  0.3653714E+01 -0.1233014E+01  0.2410341E+01 -0.2999189E+00  0.2073535E-01
 -0.1639137E+00 -0.7308020E-01 -0.4721579E-01  0.1670865E-01  0.1241256E+00  0.1657801E+01 -0.1560483E+01  0.1166398E+00
  0.1025511E+01  0.5005423E+00 -0.1666156E+00  0.1485691E-01
  eigenvectors of nos in ao-basis, column   16
  0.1089020E+01  0.2949402E+01 -0.1139508E+01 -0.3546914E+01 -0.4367612E-01 -0.9437410E-01  0.4814277E+00  0.1520374E+00
 -0.2263559E+00 -0.3117865E+00  0.4845807E+00 -0.1341834E+00 -0.7835834E-01 -0.7063387E+00  0.1289723E+01 -0.6438972E-01
  0.2977601E+00 -0.8419362E+00  0.3933009E+00  0.3996951E+00
  eigenvectors of nos in ao-basis, column   17
 -0.1004209E+01 -0.2740247E+01  0.9873203E+00  0.5071476E+01  0.6232935E+00 -0.9692312E+00  0.8652727E+00  0.1324769E+00
 -0.2874704E-01 -0.1912390E+00  0.6320835E-01 -0.1833775E-02  0.1774462E-01 -0.1307019E+00 -0.9883885E+00 -0.3486615E-01
  0.3626422E-01 -0.8437789E-01 -0.4721337E+00 -0.1198356E+01
  eigenvectors of nos in ao-basis, column   18
 -0.6167075E+00 -0.1755386E+01  0.4342036E+00  0.6465694E+01  0.1986825E+00 -0.5485981E+00 -0.1366497E+01 -0.1394046E-02
  0.9678217E-02 -0.4774419E-01  0.2770855E+00 -0.1198860E+00 -0.1726201E+00 -0.2154156E+01 -0.2399407E+00  0.4497799E-02
 -0.1743957E+00 -0.7458253E+00 -0.1287262E+01 -0.2350292E+00
  eigenvectors of nos in ao-basis, column   19
  0.1847027E+00  0.5326529E+00 -0.3053921E+00  0.3256993E+01  0.3519175E+00 -0.1180307E+01 -0.1293829E+01  0.5992223E-02
 -0.5797602E-01  0.2677935E-01  0.4314799E+00 -0.4435896E-01 -0.1558544E+00 -0.2381752E+01 -0.1786249E+00  0.2793540E+00
 -0.1056469E+01 -0.6593762E+00  0.3804824E+00  0.5748815E-01
  eigenvectors of nos in ao-basis, column   20
 -0.1549249E+00 -0.4792321E+00  0.6396279E-01  0.2371454E+01  0.1013093E-01 -0.6115607E-01 -0.5733658E+00 -0.2421323E-03
 -0.1204925E-01  0.2749417E-02  0.6319180E-01 -0.5269864E-02 -0.1473272E-01 -0.3104436E+00 -0.1250402E+01  0.8897499E+00
 -0.1228103E+00 -0.9092377E-01 -0.3103315E+00 -0.2498210E+00


*****   symmetry block SYM2   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       1.96821514
       2       0.00187140     0.00062376
       3      -0.01518422    -0.00190163     0.00719668
       4      -0.00178222     0.00074775    -0.00144350     0.00145199
       5       0.00190067     0.00051476    -0.00184928     0.00044138
       6      -0.01457093    -0.00194644     0.00848702    -0.00087128
       7      -0.00364772    -0.00126756     0.00291886    -0.00223963
       8       0.00151717     0.00015552    -0.00051453     0.00026228
       9       0.00919391     0.00036674    -0.00228713    -0.00024072
      10       0.00535379     0.00059117    -0.00174648     0.00089191
      11      -0.00540634    -0.00023315     0.00064039    -0.00039678
      12       0.00093906     0.00003670    -0.00008637     0.00009665
      13      -0.00389609    -0.00000932     0.00030068     0.00015554
      14      -0.00131382    -0.00004033    -0.00007402    -0.00019866

               Column   5     Column   6     Column   7     Column   8
       5       0.00052032
       6      -0.00213640     0.01108532
       7      -0.00086604     0.00232625     0.00382748
       8       0.00004569    -0.00066839    -0.00038668     0.00055698
       9       0.00059487    -0.00361427     0.00021028     0.00025710
      10       0.00047459    -0.00192787    -0.00188922     0.00018428
      11      -0.00022099     0.00064240     0.00080742    -0.00013172
      12      -0.00002333    -0.00007388    -0.00013965     0.00026168
      13      -0.00007931     0.00072666    -0.00028708    -0.00005012
      14       0.00000670    -0.00029040     0.00044866     0.00007220

               Column   9     Column  10     Column  11     Column  12
       9       0.00205982
      10       0.00004964     0.00149666
      11      -0.00024494    -0.00043189     0.00059933
      12       0.00001847     0.00007515    -0.00002337     0.00032449
      13      -0.00073737     0.00008025     0.00000587    -0.00000828
      14       0.00043131    -0.00043033     0.00004851     0.00000896

               Column  13     Column  14
      13       0.00053992
      14      -0.00020243     0.00034733

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7        eno   8
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    8   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno   9        eno  10        eno  11        eno  12        eno  13        eno  14
 emo    9   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   10   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   11   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   12   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   13   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   14   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   1.9685377      0.0211736      0.0058131      0.0010725      0.0008239
   0.0006433      0.0003901      0.0001640      0.0000987      0.0000948
   0.0000181      0.0000100      0.0000052      0.0000002

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7         no   8
  mo    1   0.999917E+00   0.119475E-01  -0.185157E-02  -0.154755E-02  -0.220434E-02   0.663584E-03   0.109941E-02  -0.158697E-02
  mo    2   0.969060E-03  -0.149589E+00  -0.131793E+00   0.166982E-01  -0.218862E+00  -0.398340E-01   0.648852E-01   0.748497E-01
  mo    3  -0.778687E-02   0.570575E+00   0.193839E-01   0.215171E+00   0.358196E+00  -0.351460E-01   0.679676E-01  -0.241931E+00
  mo    4  -0.892922E-03  -0.115736E+00  -0.414902E+00   0.255960E+00  -0.286195E+00  -0.435189E-01   0.191106E+00  -0.110740E+00
  mo    5   0.984512E-03  -0.148663E+00  -0.273729E-01  -0.302502E-01  -0.973767E-01  -0.194391E+00  -0.567750E-01   0.156494E+00
  mo    6  -0.749405E-02   0.699075E+00  -0.308880E+00   0.140698E+00  -0.195845E+00  -0.103863E+00  -0.127165E-02   0.195369E+00
  mo    7  -0.188026E-02   0.237921E+00   0.662794E+00  -0.204896E+00   0.135908E-01   0.110505E+00  -0.522429E-01   0.880192E-01
  mo    8   0.776940E-03  -0.478807E-01  -0.303539E-01   0.363277E+00   0.320912E-01   0.703083E+00  -0.869271E-01  -0.399906E+00
  mo    9   0.469927E-02  -0.201401E+00   0.359496E+00   0.553388E+00   0.114716E+00  -0.233013E+00   0.209157E-01  -0.115143E-01
  mo   10   0.273859E-02  -0.153997E+00  -0.295575E+00  -0.127203E+00   0.726032E+00   0.218848E-01   0.187395E+00  -0.165020E-01
  mo   11  -0.275405E-02   0.572471E-01   0.117173E+00  -0.316900E+00  -0.219209E+00   0.225013E+00   0.785209E+00  -0.173481E+00
  mo   12   0.478081E-03  -0.794495E-02  -0.257226E-01   0.177992E+00   0.293101E-01   0.529754E+00   0.331810E-01   0.754387E+00
  mo   13  -0.198497E-02   0.339413E-01  -0.147634E+00  -0.411674E+00  -0.117983E+00   0.232651E+00  -0.535133E+00  -0.220214E+00
  mo   14  -0.665829E-03  -0.741321E-02   0.144723E+00   0.263135E+00  -0.299527E+00  -0.568830E-03  -0.395099E-01  -0.212545E+00

                no   9         no  10         no  11         no  12         no  13         no  14
  mo    1   0.997596E-04   0.163814E-02   0.105359E-02   0.122853E-02   0.155492E-02   0.312836E-03
  mo    2  -0.427885E-01  -0.120692E+00   0.318743E+00  -0.152676E+00  -0.175924E-01   0.871240E+00
  mo    3  -0.592076E-01   0.356057E+00   0.121885E+00   0.357333E+00   0.297612E+00   0.271333E+00
  mo    4  -0.183303E+00  -0.152495E-01   0.629068E+00   0.169584E+00   0.131919E+00  -0.375019E+00
  mo    5  -0.111831E+00   0.102458E+00  -0.192811E+00  -0.302144E+00   0.865449E+00  -0.278838E-01
  mo    6  -0.825280E-01  -0.183296E+00  -0.108104E+00  -0.478350E+00  -0.148056E+00  -0.767312E-01
  mo    7  -0.161656E-01  -0.273585E+00   0.545373E+00  -0.176759E+00   0.148023E+00  -0.116369E+00
  mo    8  -0.696361E-01  -0.340806E+00  -0.154755E+00  -0.156116E+00   0.175423E+00   0.434315E-01
  mo    9  -0.503961E+00   0.243577E+00  -0.177400E-01  -0.290898E+00  -0.239767E+00  -0.135507E-01
  mo   10   0.168004E+00   0.402423E-01   0.247805E+00  -0.463056E+00  -0.296946E-01  -0.564501E-01
  mo   11  -0.189352E+00   0.246332E+00  -0.144506E+00  -0.138829E+00  -0.231273E-01  -0.182540E-02
  mo   12   0.195622E-01   0.319041E+00   0.645806E-01   0.974541E-01   0.508182E-02  -0.565432E-02
  mo   13  -0.342947E+00   0.470420E+00   0.141810E+00  -0.186826E+00  -0.103400E+00  -0.722549E-02
  mo   14   0.707520E+00   0.422633E+00   0.103398E+00  -0.286009E+00  -0.219939E-01  -0.335874E-01
  eigenvectors of nos in ao-basis, column    1
  0.7597355E+00 -0.3243410E-01 -0.1378949E+00 -0.1583551E-01 -0.8359855E-02  0.2777915E-03 -0.3345478E-02 -0.5504929E+00
  0.1516003E+00 -0.9786526E-02 -0.2203650E-01 -0.3011803E-01 -0.2094684E-01 -0.2458386E-01
  eigenvectors of nos in ao-basis, column    2
 -0.1329256E+01  0.1099248E+00  0.5299340E+00 -0.3849672E-01 -0.5930785E-01  0.1861941E-04  0.3450121E-02 -0.1034291E+01
  0.4044515E+00 -0.1406988E-01 -0.5300652E-01 -0.3237104E-01 -0.8227413E-02  0.6028657E-03
  eigenvectors of nos in ao-basis, column    3
 -0.7402851E+00  0.3332861E+00  0.8694097E+00  0.2832513E+00  0.5410025E+00 -0.8300925E-02  0.1193591E-01  0.2366681E+00
 -0.8382932E-01 -0.3367746E-01  0.8926255E-01 -0.1419023E-01  0.3709198E-01  0.3225666E-01
  eigenvectors of nos in ao-basis, column    4
  0.1322764E+01 -0.1147789E+01 -0.1332439E+01  0.5219868E+00  0.4185367E+00  0.2534218E-01  0.9277134E-02 -0.1462505E+01
  0.3351986E+00  0.4171560E-01 -0.1165687E+00  0.1940073E+00 -0.1255821E+00 -0.9833658E-01
  eigenvectors of nos in ao-basis, column    5
  0.2057734E+01 -0.1822558E+01  0.3695356E+00 -0.2285077E+00 -0.2932635E+00 -0.2174495E-01  0.7930976E-01  0.1069610E+01
 -0.5957064E+00  0.3797391E-01  0.2082907E+00  0.1687343E+00  0.1192785E+00  0.2050724E+00
  eigenvectors of nos in ao-basis, column    6
 -0.6816689E+00  0.7358957E+00  0.1171261E+01 -0.1556003E+00 -0.1449894E+00  0.7227755E-01  0.2345326E+00  0.1054648E+01
 -0.6838313E-01 -0.4755967E-01 -0.2286540E+00  0.3990531E+00  0.1512675E+00  0.1312843E+00
  eigenvectors of nos in ao-basis, column    7
 -0.6923654E+00  0.1001989E+01 -0.2289864E-01  0.3658305E+00 -0.4353053E+00 -0.8189758E-01  0.4197863E+00  0.3308623E+00
 -0.5235806E+00  0.8385996E-01  0.1643394E+00  0.1181399E+00 -0.2068151E+00  0.9351524E-01
  eigenvectors of nos in ao-basis, column    8
  0.1302063E+01 -0.2187330E+01 -0.9876716E+00 -0.9585738E-01  0.7068820E+00  0.6851580E-01  0.3943402E+00 -0.2200559E+01
  0.1249550E+01 -0.1612876E+00 -0.1044148E+00 -0.8221174E+00 -0.1055049E+00 -0.2395867E+00
  eigenvectors of nos in ao-basis, column    9
  0.2397689E+00 -0.5381096E+00  0.1772355E+01  0.9602953E+00 -0.1409598E+01  0.3486219E-01 -0.1153322E+00  0.1896328E+01
 -0.9117941E-01 -0.4963867E-01 -0.1541887E+00 -0.2096246E+00  0.2610905E+00  0.3561854E+00
  eigenvectors of nos in ao-basis, column   10
 -0.1626998E+01  0.3205392E+01 -0.5205637E+00  0.1517647E+00 -0.1273327E+01  0.1038998E+00 -0.3815197E-01  0.2798698E+01
 -0.3442401E+01  0.3490023E+00  0.1150988E+01  0.3088733E+00 -0.4418183E+00 -0.8926037E-01
  eigenvectors of nos in ao-basis, column   11
  0.2331681E+00 -0.3407899E+00  0.3518228E+01  0.2988289E-01 -0.6082915E+00  0.3589589E-01 -0.7817774E-01  0.1814221E+01
  0.2184120E+01 -0.3031111E-01  0.3993522E+00  0.4112450E-01  0.3360682E+00  0.1799277E+01
  eigenvectors of nos in ao-basis, column   12
  0.5804557E+00 -0.1819714E+01 -0.2966139E+01 -0.2157849E+00  0.1639802E+01 -0.2089201E-01  0.1357420E+00 -0.3100641E+01
 -0.3334069E+01  0.4460830E+00 -0.1037190E+01 -0.1013825E+01 -0.1679822E+01 -0.3773074E-01
  eigenvectors of nos in ao-basis, column   13
  0.2475481E+00 -0.8343712E+00  0.2588351E+01  0.4617829E-01  0.1580308E+00 -0.1149205E-01  0.3516372E-01 -0.1317824E+00
  0.8741743E+01  0.2046558E+00 -0.4591105E+00 -0.1016607E+00  0.4565756E+01  0.1273458E+01
  eigenvectors of nos in ao-basis, column   14
 -0.7693397E-02 -0.6746681E-03 -0.1473241E+01 -0.3987540E-01  0.2349245E+00 -0.4724038E-02  0.9765267E-02 -0.4100266E+00
 -0.2488025E+01  0.2873903E+01 -0.1565979E+00 -0.6340308E-01 -0.4365709E+00 -0.7060682E+00


*****   symmetry block SYM3   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       1.97122308
       2       0.01399779     0.00565246
       3       0.01235269     0.00616482     0.00858481
       4       0.00421222    -0.00324469    -0.00184588     0.00462001
       5       0.00013335     0.00031699     0.00149074     0.00129011
       6      -0.00736530     0.00212286     0.00309768    -0.00113483
       7       0.00001419     0.00001919    -0.00002223    -0.00004899
       8      -0.00078673     0.00018381     0.00004086    -0.00050172
       9       0.00014515    -0.00022982     0.00023709     0.00110677

               Column   5     Column   6     Column   7     Column   8
       5       0.00139356
       6       0.00048359     0.00188538
       7      -0.00003844    -0.00000316     0.00038112
       8      -0.00004012     0.00006567     0.00000449     0.00037900
       9       0.00077206    -0.00000132     0.00000440    -0.00014501

               Column   9
       9       0.00076190

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7        eno   8
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    8   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno   9
 emo    9   0.100000E+01

 occupation numbers of nos
   1.9714376      0.0157440      0.0053374      0.0008014      0.0006246
   0.0003944      0.0003622      0.0001209      0.0000588

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7         no   8
  mo    1   0.999945E+00  -0.683773E-02  -0.285998E-02   0.695713E-02   0.193379E-02   0.564425E-03   0.693325E-03  -0.569499E-03
  mo    2   0.713254E-02   0.578058E+00  -0.121316E+00  -0.460784E+00   0.112962E+00   0.981976E-01   0.134845E+00   0.112507E+00
  mo    3   0.630748E-02   0.698689E+00   0.370230E+00  -0.255159E-01  -0.304519E+00  -0.121179E+00  -0.173711E+00   0.758832E-02
  mo    4   0.212618E-02  -0.310371E+00   0.748158E+00  -0.123844E+00  -0.353380E+00  -0.947038E-02  -0.608906E-01   0.236687E+00
  mo    5   0.741199E-04   0.656818E-01   0.440840E+00   0.612954E-01   0.596585E+00  -0.166310E+00  -0.112792E+00  -0.620972E+00
  mo    6  -0.372299E-02   0.276157E+00   0.777684E-01   0.859010E+00   0.594290E-02   0.184104E+00   0.184380E+00   0.152192E+00
  mo    7   0.714636E-05   0.474385E-03  -0.128303E-01  -0.173841E-01   0.296819E-01   0.766953E+00  -0.636727E+00  -0.703020E-01
  mo    8  -0.398997E-03   0.204336E-01  -0.875286E-01   0.105458E+00   0.328146E+00  -0.470592E+00  -0.608954E+00   0.521424E+00
  mo    9   0.748323E-04  -0.176418E-01   0.283283E+00  -0.136247E+00   0.552374E+00   0.323042E+00   0.353510E+00   0.495651E+00

                no   9
  mo    1  -0.100509E-02
  mo    2   0.620898E+00
  mo    3  -0.486234E+00
  mo    4   0.379331E+00
  mo    5   0.125253E+00
  mo    6   0.297809E+00
  mo    7   0.847428E-02
  mo    8   0.946359E-01
  mo    9  -0.347510E+00
  eigenvectors of nos in ao-basis, column    1
  0.9201919E+00 -0.6287041E-02  0.4121807E-01 -0.4925654E-02 -0.1999497E-01  0.1135002E-02  0.3268437E-03  0.2313062E-01
  0.3974263E-01
  eigenvectors of nos in ao-basis, column    2
 -0.1649180E+01  0.5299754E+00  0.1274810E+01 -0.1603458E-01 -0.7220489E-01  0.1366154E-03  0.4915739E-03  0.6482888E-01
  0.8994920E-01
  eigenvectors of nos in ao-basis, column    3
 -0.2291599E+00  0.9930234E-01  0.3543779E+00  0.3437562E+00  0.7116660E+00 -0.3182206E-02 -0.5144705E-02 -0.6987262E-01
 -0.5132032E-01
  eigenvectors of nos in ao-basis, column    4
 -0.2665512E+01  0.2735859E+01  0.4358634E+00 -0.1579784E+00  0.2977427E-01  0.7581417E-02 -0.4359876E-03 -0.3376126E-01
 -0.2063682E+00
  eigenvectors of nos in ao-basis, column    5
 -0.2124453E+00  0.2542374E+00 -0.6491352E+00  0.5724342E+00 -0.4161425E-01  0.8117055E-01  0.2991962E-01  0.4970400E+00
  0.1225173E+00
  eigenvectors of nos in ao-basis, column    6
 -0.3760487E+00  0.4804600E+00 -0.2270770E+00  0.4521615E+00 -0.3498361E+00 -0.1400066E+00  0.8895471E-01 -0.5575778E-01
  0.1455353E+00
  eigenvectors of nos in ao-basis, column    7
 -0.3689079E+00  0.4823524E+00 -0.3513717E+00  0.5297342E+00 -0.3899825E+00 -0.6047801E-01 -0.1251653E+00  0.2085421E-01
  0.1786278E+00
  eigenvectors of nos in ao-basis, column    8
 -0.4604427E+00  0.6888859E+00  0.2889660E+00  0.5068904E+00 -0.8980644E+00  0.1248943E+00  0.2771125E-01 -0.8797241E+00
  0.1962683E+00
  eigenvectors of nos in ao-basis, column    9
 -0.8300426E+00  0.1329749E+01 -0.1788604E+01 -0.4284401E+00  0.5630215E+00  0.2674126E-02  0.1101729E-02 -0.5314223E-01
  0.1039343E+01


*****   symmetry block SYM4   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       0.00578015
       2      -0.00074324     0.00082736
       3       0.00035296    -0.00000354     0.00026367
       4       0.00015377    -0.00014967     0.00016722     0.00020917
       5      -0.00004994    -0.00013278    -0.00007176    -0.00001370

               Column   5
       5       0.00015402

               eno   1        eno   2        eno   3        eno   4        eno   5
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   0.0059171      0.0007772      0.0003952      0.0001043      0.0000406

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5
  mo    1   0.986903E+00   0.140080E+00  -0.648757E-01   0.261393E-01   0.388281E-01
  mo    2  -0.144949E+00   0.945988E+00   0.273322E-01   0.245680E+00   0.151628E+00
  mo    3   0.627364E-01   0.583254E-01   0.744143E+00   0.130515E+00  -0.649522E+00
  mo    4   0.322396E-01  -0.188964E+00   0.612232E+00   0.226123E+00   0.733003E+00
  mo    5  -0.606987E-02  -0.215348E+00  -0.257812E+00   0.933163E+00  -0.127783E+00
  eigenvectors of nos in ao-basis, column    1
 -0.3107385E+00 -0.6779246E+00  0.4231983E-01  0.1109621E+00  0.9622073E-01
  eigenvectors of nos in ao-basis, column    2
 -0.6051332E+00 -0.1227506E+00 -0.3224807E+00 -0.4932546E+00 -0.1667823E+00
  eigenvectors of nos in ao-basis, column    3
  0.5745166E+00 -0.4039482E+00 -0.7788422E+00 -0.4637670E-01  0.3295704E+00
  eigenvectors of nos in ao-basis, column    4
  0.7043673E+00 -0.1197462E+01  0.6088516E+00 -0.8131710E+00  0.6788222E-01
  eigenvectors of nos in ao-basis, column    5
  0.3556189E+00 -0.3445269E+00 -0.2455219E+00  0.3892368E+00 -0.1149854E+01


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***


 cosurf_and_phi
  1.262739748330  2.872870034186  1.567866969040 -0.042673451019
  1.652947872482  0.360731428865  3.496571379926 -0.051781061601
  0.594940552015  0.670555958619  3.845537291721 -0.062124634391
  2.390078334690  1.202722009900  2.566708449184 -0.033008067787
  1.989334673621  2.229216060677  2.001028520754 -0.035910923610
  2.919143888229  0.378144896597  2.099775822683 -0.012490165175
  0.105380882100  1.878884201281  3.371423292662 -0.062432531601
  0.783266721831  2.590884380442  2.520834408126 -0.055848252158
  2.011402526412  2.551496219550  0.814855178005 -0.017004837244
  0.099626804209  1.855073908563 -1.945822518898  0.030202463146
  0.119020599620  3.173161356543  1.415159765853 -0.048556157095
  0.915237473358  3.108118624501  0.463299650838 -0.030405423065
  0.462814589486  2.837210699514 -0.795516582628 -0.006634160464
  1.408719892640  1.640288870679  3.148120355694 -0.053541860569
  2.650053602975  1.633766196698  1.655441764425 -0.014137757762
  1.142904167226  2.885766749848 -0.243475252462 -0.012219357073
  3.656106873706  0.713798214804  0.361832640492  0.038230302827
  2.408046317685  2.075600470298 -1.226192756897  0.039141440639
  1.295820311657  0.567673501678 -2.747601655947  0.057111240312
  3.527730862121  1.045649613724 -1.064853177123  0.054476267387
  2.622898581638  1.024710819001 -2.241122746235  0.059025020433
  3.086808508398  1.794857685487 -0.179703829578  0.033984381445
  1.615872011596  1.674366500124 -2.147504385867  0.046809997855
  0.000006852884  1.035694667087 -2.361676372823  0.049280885543
  0.298815704890  0.969607820141 -2.408807386530  0.050863391060
  0.667723897920  1.245157743773 -2.338577856151  0.048552374909
  1.218123388571  2.025110412626 -1.616576841774  0.034456189375
  2.084186643139  2.293984793080 -0.480493513306  0.021281232444
  2.876642520254  1.658030225134  0.559035126479  0.020774753642
  3.282919570196  0.368088739237  1.091983758387  0.024290126310
  0.467932162408  1.694535407938 -1.941510815499  0.036918439566
  1.299770347024  2.453875604722 -0.850324284820  0.011413040509
  2.242050270258  2.245320705184  0.385739526123  0.002951110865
  2.923104801922  1.151131828431  1.279135167181  0.007183655339
  0.427044967836  0.580963354323 -2.585108185092  0.054614199802
 end_of_phi


 nsubv after electrostatic potential =  4

  Confirmation of dielectric energy for ground state
  edielnew =  -0.00267781028

 =========== Executing IN-CORE method ==========
 norm multnew:  3.18067925E-06
 threx: total number of loops= 14
 threx: total number of loops= 24
 blstr diagel,allext,nmin2=  1892
 onel.diag.
 -0.194489E+01-0.285984E+01-0.270263E+01-0.267917E+01-0.404205E+01-0.353093E+01-0.365990E+01-0.378020E+01-0.336100E+01-0.260028E+01
 -0.287334E+01-0.357379E+01-0.152448E+01-0.144552E+01-0.152236E+01-0.750255E+00 0.955267E+00-0.155461E+01-0.292438E+01-0.240582E+01
 -0.200527E+01-0.405335E+01-0.340936E+01-0.250410E+01-0.287148E+01-0.299037E+01-0.266619E+01-0.118115E+01-0.155099E+01-0.374151E+00
 -0.277725E+01-0.388112E+01-0.376164E+01-0.283234E+01-0.443834E+01-0.160190E+01-0.133520E+01-0.168096E+01-0.459124E+01-0.280667E+01
 -0.220410E+01-0.135623E+01-0.747446E+00-0.330496E+02-0.787938E+01-0.692069E+01-0.675716E+01-0.704599E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1      1892      1892
 blstr diagel,allext,nmin2=  1892
 onel.diag.
 -0.194489E+01-0.285984E+01-0.270263E+01-0.267917E+01-0.404205E+01-0.353093E+01-0.365990E+01-0.378020E+01-0.336100E+01-0.260028E+01
 -0.287334E+01-0.357379E+01-0.152448E+01-0.144552E+01-0.152236E+01-0.750255E+00 0.955267E+00-0.155461E+01-0.292438E+01-0.240582E+01
 -0.200527E+01-0.405335E+01-0.340936E+01-0.250410E+01-0.287148E+01-0.299037E+01-0.266619E+01-0.118115E+01-0.155099E+01-0.374151E+00
 -0.277725E+01-0.388112E+01-0.376164E+01-0.283234E+01-0.443834E+01-0.160190E+01-0.133520E+01-0.168096E+01-0.459124E+01-0.280667E+01
 -0.220410E+01-0.135623E+01-0.747446E+00-0.330496E+02-0.787938E+01-0.692069E+01-0.675716E+01-0.704599E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1      1892      1892
 blstr diagel,allext,nmin2=  1892
 onel.diag.
 -0.194489E+01-0.285984E+01-0.270263E+01-0.267917E+01-0.404205E+01-0.353093E+01-0.365990E+01-0.378020E+01-0.336100E+01-0.260028E+01
 -0.287334E+01-0.357379E+01-0.152448E+01-0.144552E+01-0.152236E+01-0.750255E+00 0.955267E+00-0.155461E+01-0.292438E+01-0.240582E+01
 -0.200527E+01-0.405335E+01-0.340936E+01-0.250410E+01-0.287148E+01-0.299037E+01-0.266619E+01-0.118115E+01-0.155099E+01-0.374151E+00
 -0.277725E+01-0.388112E+01-0.376164E+01-0.283234E+01-0.443834E+01-0.160190E+01-0.133520E+01-0.168096E+01-0.459124E+01-0.280667E+01
 -0.220410E+01-0.135623E+01-0.747446E+00-0.330496E+02-0.787938E+01-0.692069E+01-0.675716E+01-0.704599E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1      1892      1892


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        12 xx:        36 ww:        79
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        11
task #   9:        28    task #  10:        71    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 matrix-vector product formed for vector   5
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference-subspace vector overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     0.97234180     0.06649896    -0.09372291     0.00000000    -0.00009842

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     0.97231179    -0.01524139    -0.06902297     0.03445149     0.08664664
 subspace has dimension  5

          (h-repnuc*1) matrix in the subspace basis

                x:   1         x:   2         x:   3         x:   4         x:   5
   x:   1   -85.67245276
   x:   2    -0.00036856   -83.87106855
   x:   3    -0.00050015     0.00069489   -82.26740199
   x:   4     0.00004199    -0.00501832    -0.01027778    -0.00001555
   x:   5     0.00627730    -0.01578978    -0.03017397    -0.00001614    -0.00026848

          overlap matrix in the subspace basis

                x:   1         x:   2         x:   3         x:   4         x:   5
   x:   1     1.00000000
   x:   2     0.00000000     1.00000000
   x:   3     0.00000000     0.00000000     1.00000000
   x:   4    -0.00000050     0.00006117     0.00012303     0.00000019
   x:   5    -0.00007332     0.00018428     0.00035308     0.00000019     0.00000318

          eigenvectors and eigenvalues in the subspace basis

                v:   1         v:   2         v:   3         v:   4         v:   5

   energy   -85.67245857   -84.49660693   -83.91297221   -82.19764288   -81.92835090

   x:   1     1.00007692     0.03586303     0.00480802     0.01904333    -0.00129540
   x:   2     0.00001189     0.09423506    -1.00336184    -0.11331186     0.00333630
   x:   3     0.00000221     0.05971400     0.00649551    -0.53258427    -0.90812209
   x:   4     1.56834284   357.06164765   548.92887161 -1651.24416385  1719.16667135
   x:   5     1.07070170   515.99295772    64.69122862   268.70727085   -26.13880982

          eigenvectors and eigenvalues in the schmidt orthonormalized basis

                v:   1         v:   2         v:   3         v:   4         v:   5

   energy   -85.67245857   -84.49660693   -83.91297221   -82.19764288   -81.92835090

  zx:   1     0.99999763    -0.00214799    -0.00020958     0.00016737    -0.00023847
  zx:   2     0.00030514     0.21116625    -0.95785989    -0.16480830     0.10368881
  zx:   3     0.00057321     0.28583189     0.09687217    -0.64086269    -0.70584035
  zx:   4     0.00099087     0.31336279     0.24684116    -0.59314781     0.69931959
  zx:   5     0.00182733     0.88062937     0.11040654     0.45859447    -0.04461031

          <ref|baseci> overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     0.97234180     0.06649896    -0.09372291     0.00000000    -0.00009842

  write the transformed vectors back to cosmofile

 rtflw =  0
 freezing =  F

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  8  1    -76.3378780848 -9.3346E+00  4.0750E-07  1.3031E-03  1.0000E-03
 mr-sdci #  8  2    -75.1620264416 -8.7609E+00  0.0000E+00  7.8896E-01  1.0000E-04
 mr-sdci #  8  3    -74.5783917225 -8.1032E+00  0.0000E+00  9.7288E-01  1.0000E-04
 mr-sdci #  8  4    -72.8630623917 -9.0671E+00  0.0000E+00  2.8541E+00  1.0000E-04
 mr-sdci #  8  5    -72.5937704109 -7.2936E+00  0.0000E+00  2.8574E+00  1.0000E-04


 total energy in cosmo calc 
e(rootcalc) + repnuc=     -76.3378780848
dielectric energy =      -0.0107050726
deltaediel =       0.0107050726
e(rootcalc) + repnuc - ediel =     -76.3271730123
e(rootcalc) + repnuc - edielnew =     -76.3378780848
deltaelast =      76.3271730123

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.02   0.00   0.00   0.01         0.    0.0000
    2   25    0   0.01   0.00   0.00   0.01         0.    0.0000
    3   26    0   0.02   0.00   0.00   0.02         0.    0.0000
    4   11    0   0.01   0.00   0.00   0.00         0.    0.0000
    5   15    0   0.03   0.00   0.00   0.02         0.    0.0000
    6   16    0   0.02   0.00   0.00   0.02         0.    0.0000
    7    1    0   0.01   0.00   0.00   0.01         0.    0.0000
    8    5    0   0.02   0.00   0.00   0.02         0.    0.0000
    9    6    0   0.02   0.00   0.00   0.02         0.    0.0000
   10    7    0   0.04   0.00   0.00   0.03         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.03   0.00   0.00   0.03         0.    0.0000
   14   47    0   0.04   0.00   0.00   0.04         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.030000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.2700s 
time spent in multnx:                   0.2300s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            1.5300s 

          starting ci iteration   9

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.33458049

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=       3  D0X=       9  D0W=      15
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================

          ci-one-electron density block   1

                mo   1         mo   2         mo   3         mo   4         mo   5         mo   6         mo   7         mo   8
   mo   1     1.99955691
   mo   2    -0.00013940     1.98336388
   mo   3    -0.00012943     0.00306144     1.96958842
   mo   4    -0.00014412     0.00138016     0.01304876     0.00121110
   mo   5     0.00019579     0.00406190    -0.01081412    -0.00248549     0.00655751
   mo   6    -0.00019390     0.00510268     0.00414218     0.00027246     0.00060692     0.00283433
   mo   7     0.00034754    -0.00580522    -0.01569108    -0.00101575     0.00054501    -0.00133948     0.00266433
   mo   8    -0.00024051    -0.00662061     0.00461885     0.00272391    -0.00786233    -0.00035699     0.00013905     0.01039607
   mo   9    -0.00031387     0.00622662     0.00859471     0.00019896     0.00126887     0.00290680    -0.00197055    -0.00126193
   mo  10     0.00012300     0.00214737    -0.00563313    -0.00064032     0.00155161     0.00211299     0.00067881    -0.00109586
   mo  11     0.00063525    -0.00078121     0.00023223    -0.00066016    -0.00029875    -0.00142770     0.00257904     0.00142274
   mo  12     0.00059280     0.00197653    -0.00506115    -0.00086861     0.00132417    -0.00136108     0.00132203    -0.00175210
   mo  13    -0.00000592    -0.00131268     0.00134935     0.00026182    -0.00090474     0.00036132     0.00028127     0.00148526
   mo  14    -0.00026719    -0.00396110     0.00507018     0.00087839    -0.00228892    -0.00028543    -0.00035147     0.00302430
   mo  15    -0.00031635     0.00207045    -0.00646483    -0.00013735     0.00088552     0.00090022    -0.00056869    -0.00118815
   mo  16     0.00001368    -0.00013927     0.00059598     0.00001772    -0.00006520    -0.00015982    -0.00000822     0.00004012
   mo  17    -0.00025025     0.00010386    -0.00203396     0.00007422     0.00004786    -0.00001311    -0.00035949    -0.00039841
   mo  18    -0.00005401     0.00020080    -0.00027132     0.00005232    -0.00011980    -0.00016300    -0.00008471     0.00012860
   mo  19    -0.00014758     0.00046284    -0.00050995     0.00009465    -0.00015657     0.00033696    -0.00009971     0.00029435
   mo  20     0.00086193     0.00221010    -0.00032715    -0.00020359     0.00023918    -0.00027836     0.00045312    -0.00025111

                mo   9         mo  10         mo  11         mo  12         mo  13         mo  14         mo  15         mo  16
   mo   9     0.00529924
   mo  10     0.00065288     0.00422360
   mo  11    -0.00074020    -0.00042633     0.00439894
   mo  12    -0.00238872     0.00007962     0.00013543     0.00253602
   mo  13    -0.00065280     0.00130284    -0.00011923    -0.00001673     0.00120949
   mo  14    -0.00070566    -0.00088709    -0.00032923    -0.00016025     0.00035770     0.00158289
   mo  15     0.00157227     0.00057182    -0.00074289    -0.00040667    -0.00029178    -0.00054201     0.00112643
   mo  16    -0.00002548    -0.00039695     0.00013396     0.00000561    -0.00007013     0.00004326    -0.00004167     0.00039368
   mo  17    -0.00017910    -0.00027370    -0.00095012     0.00035965    -0.00013812     0.00004001     0.00007540     0.00000851
   mo  18     0.00037138    -0.00084640     0.00051540    -0.00040278    -0.00060559    -0.00000464    -0.00000189     0.00008865
   mo  19     0.00047005     0.00039021     0.00017897    -0.00062365     0.00024331    -0.00023401     0.00010004    -0.00004455
   mo  20    -0.00040141     0.00009047     0.00054621     0.00048659    -0.00001336    -0.00021966    -0.00014347     0.00000591

                mo  17         mo  18         mo  19         mo  20
   mo  17     0.00060856
   mo  18    -0.00012431     0.00067337
   mo  19    -0.00022306    -0.00000215     0.00043426
   mo  20    -0.00006726    -0.00002342    -0.00007141     0.00038141

          ci-one-electron density block   2

                mo  21         mo  22         mo  23         mo  24         mo  25         mo  26         mo  27         mo  28
   mo  21     1.96820511
   mo  22     0.00195711     0.00062318
   mo  23    -0.01609456    -0.00189942     0.00719477
   mo  24    -0.00222351     0.00074759    -0.00143879     0.00145423
   mo  25     0.00204599     0.00051457    -0.00184837     0.00044164     0.00052019
   mo  26    -0.01516378    -0.00194595     0.00848797    -0.00086960    -0.00213646     0.01108835
   mo  27    -0.00337947    -0.00126781     0.00291632    -0.00224164    -0.00086686     0.00232664     0.00383074
   mo  28     0.00144729     0.00015594    -0.00051654     0.00026207     0.00004643    -0.00067109    -0.00038679     0.00055664
   mo  29     0.00924393     0.00036762    -0.00229254    -0.00024180     0.00059585    -0.00361915     0.00021029     0.00025786
   mo  30     0.00528883     0.00059171    -0.00174829     0.00089196     0.00047537    -0.00193002    -0.00189035     0.00018440
   mo  31    -0.00537464    -0.00023365     0.00064328    -0.00039612    -0.00022174     0.00064527     0.00080760    -0.00013168
   mo  32     0.00093779     0.00003674    -0.00008682     0.00009646    -0.00002320    -0.00007444    -0.00013959     0.00026164
   mo  33    -0.00389929    -0.00000969     0.00030311     0.00015613    -0.00007976     0.00072852    -0.00028714    -0.00005021
   mo  34    -0.00129945    -0.00004049    -0.00007308    -0.00019840     0.00000646    -0.00028953     0.00044873     0.00007235

                mo  29         mo  30         mo  31         mo  32         mo  33         mo  34
   mo  29     0.00206133
   mo  30     0.00005009     0.00149729
   mo  31    -0.00024549    -0.00043220     0.00059953
   mo  32     0.00001869     0.00007517    -0.00002338     0.00032452
   mo  33    -0.00073777     0.00008011     0.00000597    -0.00000835     0.00054010
   mo  34     0.00043140    -0.00043049     0.00004849     0.00000899    -0.00020253     0.00034745

          ci-one-electron density block   3

                mo  35         mo  36         mo  37         mo  38         mo  39         mo  40         mo  41         mo  42
   mo  35     1.97122428
   mo  36     0.01356110     0.00562364
   mo  37     0.01300125     0.00616055     0.00860822
   mo  38     0.00476775    -0.00323485    -0.00184173     0.00462141
   mo  39     0.00028285     0.00031761     0.00149333     0.00129082     0.00139380
   mo  40    -0.00732122     0.00212285     0.00309996    -0.00113768     0.00048337     0.00188672
   mo  41     0.00000402     0.00001893    -0.00002219    -0.00004862    -0.00003826    -0.00000310     0.00038111
   mo  42    -0.00079073     0.00018380     0.00004077    -0.00050193    -0.00004015     0.00006577     0.00000443     0.00037902
   mo  43     0.00015277    -0.00022964     0.00023696     0.00110694     0.00077217    -0.00000145     0.00000448    -0.00014505

                mo  43
   mo  43     0.00076212

          ci-one-electron density block   4

                mo  44         mo  45         mo  46         mo  47         mo  48
   mo  44     0.00577708
   mo  45    -0.00074430     0.00082947
   mo  46     0.00035685    -0.00000645     0.00026575
   mo  47     0.00015358    -0.00015021     0.00016810     0.00020952
   mo  48    -0.00004937    -0.00013320    -0.00007153    -0.00001352     0.00015401


*****   symmetry block SYM1   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       1.99955691
       2      -0.00013940     1.98336388
       3      -0.00012943     0.00306144     1.96958842
       4      -0.00014412     0.00138016     0.01304876     0.00121110
       5       0.00019579     0.00406190    -0.01081412    -0.00248549
       6      -0.00019390     0.00510268     0.00414218     0.00027246
       7       0.00034754    -0.00580522    -0.01569108    -0.00101575
       8      -0.00024051    -0.00662061     0.00461885     0.00272391
       9      -0.00031387     0.00622662     0.00859471     0.00019896
      10       0.00012300     0.00214737    -0.00563313    -0.00064032
      11       0.00063525    -0.00078121     0.00023223    -0.00066016
      12       0.00059280     0.00197653    -0.00506115    -0.00086861
      13      -0.00000592    -0.00131268     0.00134935     0.00026182
      14      -0.00026719    -0.00396110     0.00507018     0.00087839
      15      -0.00031635     0.00207045    -0.00646483    -0.00013735
      16       0.00001368    -0.00013927     0.00059598     0.00001772
      17      -0.00025025     0.00010386    -0.00203396     0.00007422
      18      -0.00005401     0.00020080    -0.00027132     0.00005232
      19      -0.00014758     0.00046284    -0.00050995     0.00009465
      20       0.00086193     0.00221010    -0.00032715    -0.00020359

               Column   5     Column   6     Column   7     Column   8
       5       0.00655751
       6       0.00060692     0.00283433
       7       0.00054501    -0.00133948     0.00266433
       8      -0.00786233    -0.00035699     0.00013905     0.01039607
       9       0.00126887     0.00290680    -0.00197055    -0.00126193
      10       0.00155161     0.00211299     0.00067881    -0.00109586
      11      -0.00029875    -0.00142770     0.00257904     0.00142274
      12       0.00132417    -0.00136108     0.00132203    -0.00175210
      13      -0.00090474     0.00036132     0.00028127     0.00148526
      14      -0.00228892    -0.00028543    -0.00035147     0.00302430
      15       0.00088552     0.00090022    -0.00056869    -0.00118815
      16      -0.00006520    -0.00015982    -0.00000822     0.00004012
      17       0.00004786    -0.00001311    -0.00035949    -0.00039841
      18      -0.00011980    -0.00016300    -0.00008471     0.00012860
      19      -0.00015657     0.00033696    -0.00009971     0.00029435
      20       0.00023918    -0.00027836     0.00045312    -0.00025111

               Column   9     Column  10     Column  11     Column  12
       9       0.00529924
      10       0.00065288     0.00422360
      11      -0.00074020    -0.00042633     0.00439894
      12      -0.00238872     0.00007962     0.00013543     0.00253602
      13      -0.00065280     0.00130284    -0.00011923    -0.00001673
      14      -0.00070566    -0.00088709    -0.00032923    -0.00016025
      15       0.00157227     0.00057182    -0.00074289    -0.00040667
      16      -0.00002548    -0.00039695     0.00013396     0.00000561
      17      -0.00017910    -0.00027370    -0.00095012     0.00035965
      18       0.00037138    -0.00084640     0.00051540    -0.00040278
      19       0.00047005     0.00039021     0.00017897    -0.00062365
      20      -0.00040141     0.00009047     0.00054621     0.00048659

               Column  13     Column  14     Column  15     Column  16
      13       0.00120949
      14       0.00035770     0.00158289
      15      -0.00029178    -0.00054201     0.00112643
      16      -0.00007013     0.00004326    -0.00004167     0.00039368
      17      -0.00013812     0.00004001     0.00007540     0.00000851
      18      -0.00060559    -0.00000464    -0.00000189     0.00008865
      19       0.00024331    -0.00023401     0.00010004    -0.00004455
      20      -0.00001336    -0.00021966    -0.00014347     0.00000591

               Column  17     Column  18     Column  19     Column  20
      17       0.00060856
      18      -0.00012431     0.00067337
      19      -0.00022306    -0.00000215     0.00043426
      20      -0.00006726    -0.00002342    -0.00007141     0.00038141

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7        eno   8
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    8   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno   9        eno  10        eno  11        eno  12        eno  13        eno  14        eno  15        eno  16
 emo    9   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   10   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   11   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   12   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   13   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   14   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   15   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   16   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno  17        eno  18        eno  19        eno  20
 emo   17   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   18   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   19   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   20   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   1.9995601      1.9841349      1.9693115      0.0194992      0.0110069
   0.0060653      0.0052909      0.0011317      0.0008433      0.0005712
   0.0004724      0.0003663      0.0003554      0.0001566      0.0001088
   0.0000690      0.0000562      0.0000321      0.0000081      0.0000004

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7         no   8
  mo    1   0.999936E+00   0.107224E-01   0.347463E-02   0.151529E-03   0.513616E-03  -0.224229E-03   0.605559E-04  -0.957120E-04
  mo    2  -0.973599E-02   0.976971E+00  -0.213033E+00   0.507247E-02  -0.357286E-02   0.775002E-04   0.436478E-03  -0.659204E-03
  mo    3  -0.567402E-02   0.212957E+00   0.976942E+00  -0.690250E-02  -0.804240E-02   0.418611E-02   0.194871E-02   0.139212E-02
  mo    4  -0.116689E-03   0.207780E-02   0.634731E-02   0.194305E+00   0.149563E+00  -0.111180E+00   0.522530E-01  -0.791768E-01
  mo    5   0.109960E-03   0.856735E-03  -0.585237E-02  -0.563759E+00  -0.891485E-01   0.651780E-01  -0.596488E-01   0.721859E-01
  mo    6  -0.134677E-03   0.297089E-02   0.151007E-02  -0.846455E-01   0.432305E+00   0.245012E+00   0.109886E+00   0.209125E+00
  mo    7   0.248125E-03  -0.455351E-02  -0.717644E-02   0.550088E-02  -0.376938E+00   0.362134E+00  -0.749978E-01   0.193421E+00
  mo    8  -0.102310E-03  -0.278774E-02   0.306405E-02   0.713972E+00   0.100942E+00   0.202519E+00  -0.181257E-02   0.200482E+00
  mo    9  -0.213176E-03   0.400939E-02   0.360182E-02  -0.150725E+00   0.565463E+00   0.108155E+00  -0.433048E+00   0.346022E+00
  mo   10   0.672206E-04   0.459417E-03  -0.304302E-02  -0.143931E+00   0.141679E+00   0.643064E+00   0.437804E+00  -0.300870E-01
  mo   11   0.322217E-03  -0.369015E-03   0.191946E-03   0.900442E-01  -0.339130E+00   0.438873E+00  -0.579209E+00   0.297589E-01
  mo   12   0.302465E-03   0.426191E-03  -0.274655E-02  -0.978415E-01  -0.357172E+00  -0.720040E-01   0.282764E+00   0.573461E+00
  mo   13  -0.432906E-06  -0.505475E-03   0.814900E-03   0.864664E-01   0.147000E-01   0.243030E+00   0.288099E+00  -0.200909E+00
  mo   14  -0.129206E-03  -0.141467E-02   0.296247E-02   0.217372E+00   0.188003E-01  -0.124126E+00   0.104453E+00   0.357170E+00
  mo   15  -0.150370E-03   0.332386E-03  -0.343423E-02  -0.102670E+00   0.182544E+00   0.101036E-01  -0.416652E-01   0.192117E+00
  mo   16   0.584573E-05  -0.500703E-05   0.311705E-03   0.828184E-02  -0.176077E-01  -0.482321E-01  -0.576819E-01   0.256289E-01
  mo   17  -0.120042E-03  -0.167319E-03  -0.102079E-02  -0.186285E-01   0.161843E-01  -0.169139E+00   0.135966E+00   0.204494E+00
  mo   18  -0.272823E-04   0.697819E-04  -0.153130E-03   0.152777E-01   0.412757E-02  -0.815459E-01  -0.247444E+00  -0.101870E-01
  mo   19  -0.747856E-04   0.173733E-03  -0.300681E-03   0.109356E-01   0.707297E-01   0.112517E+00  -0.493998E-01  -0.398917E+00
  mo   20   0.421633E-03   0.105612E-02  -0.405091E-03  -0.156391E-01  -0.925258E-01   0.578553E-01  -0.152682E-01   0.101215E+00

                no   9         no  10         no  11         no  12         no  13         no  14         no  15         no  16
  mo    1  -0.472912E-04   0.347972E-04  -0.323796E-03   0.173923E-03   0.189642E-03   0.307161E-04  -0.960381E-04   0.473992E-04
  mo    2  -0.100734E-02   0.266180E-03  -0.231571E-02   0.108188E-02   0.129570E-02  -0.281399E-03   0.427924E-03  -0.711485E-03
  mo    3   0.258875E-02  -0.594633E-03  -0.479950E-02   0.263016E-02   0.186567E-02   0.206721E-02   0.232598E-02  -0.225651E-02
  mo    4   0.139800E+00  -0.461073E-01   0.155113E+00  -0.888784E-01  -0.117700E+00  -0.108149E+00  -0.163035E+00   0.124963E+00
  mo    5  -0.386526E+00   0.569115E-01  -0.128578E+00   0.265301E-01   0.249196E-01   0.181755E+00   0.278771E+00  -0.234710E-01
  mo    6  -0.165425E+00  -0.319508E+00   0.247681E+00   0.127427E+00  -0.335337E-01   0.787008E-01  -0.351747E-01  -0.346785E-02
  mo    7   0.102172E+00  -0.192670E-01  -0.212169E+00   0.219357E+00   0.256530E+00   0.508151E-01  -0.681316E-01  -0.270320E+00
  mo    8  -0.322197E-01   0.241499E-03  -0.666544E-01   0.363168E-01   0.463293E-01   0.187759E+00   0.154557E+00  -0.880951E-01
  mo    9   0.993557E-03   0.298676E+00   0.119763E+00  -0.110815E+00   0.110248E+00   0.592512E-01  -0.198459E+00  -0.148007E-01
  mo   10   0.954408E-01  -0.227739E+00  -0.893064E-01   0.216488E-02  -0.563032E-01  -0.183421E+00  -0.569420E-01  -0.367506E-01
  mo   11  -0.314221E-01   0.521122E-01   0.144007E+00  -0.591823E-01  -0.969608E-02  -0.126723E+00  -0.153418E-02   0.257120E+00
  mo   12   0.148518E+00   0.800092E-01   0.200520E+00  -0.159775E+00  -0.212786E+00   0.325770E+00  -0.410414E+00   0.111687E+00
  mo   13  -0.194363E+00   0.491883E+00   0.106473E+00   0.566570E-01  -0.410850E-01   0.255861E+00   0.171424E+00   0.570895E+00
  mo   14  -0.567450E+00   0.568981E-01  -0.337844E+00  -0.758965E-01  -0.102008E+00  -0.211314E-01   0.140391E+00  -0.150194E+00
  mo   15   0.573241E+00   0.196561E+00  -0.538353E+00   0.591674E-01  -0.274749E+00   0.576012E-01   0.301137E+00   0.118511E+00
  mo   16  -0.553420E-01   0.188641E+00   0.204974E+00   0.811809E+00  -0.434655E+00  -0.111922E+00  -0.627722E-01  -0.162715E+00
  mo   17   0.208227E+00  -0.200564E-01   0.227631E+00   0.332937E+00   0.670056E+00   0.135808E+00   0.289324E+00   0.967238E-01
  mo   18  -0.270605E-01  -0.632310E+00  -0.116433E+00   0.160055E+00  -0.191323E+00   0.434241E+00   0.585783E-01   0.378755E+00
  mo   19   0.602056E-01   0.141399E+00  -0.857112E-02  -0.353455E-01  -0.890558E-02   0.679506E+00  -0.161725E+00  -0.445078E+00
  mo   20   0.144527E+00  -0.486634E-01   0.492493E+00  -0.274190E+00  -0.305910E+00   0.374554E-01   0.630753E+00  -0.287960E+00

                no  17         no  18         no  19         no  20
  mo    1  -0.514554E-04  -0.200717E-04  -0.817165E-05   0.616882E-06
  mo    2   0.894056E-03   0.774403E-03   0.911403E-06  -0.180564E-03
  mo    3   0.648387E-03   0.224083E-02   0.205538E-02  -0.127567E-02
  mo    4  -0.614882E-01  -0.129024E+00  -0.257239E+00   0.842910E+00
  mo    5  -0.183563E+00  -0.102532E+00   0.385998E+00   0.431482E+00
  mo    6  -0.500389E+00   0.427168E+00  -0.193333E+00  -0.652381E-01
  mo    7   0.293699E+00   0.471844E+00  -0.220056E+00   0.276168E+00
  mo    8  -0.988373E-01  -0.286907E-01   0.547265E+00   0.113526E+00
  mo    9   0.402334E+00  -0.639542E-01   0.375884E-01   0.827301E-02
  mo   10   0.207391E+00  -0.444961E+00   0.290320E-01  -0.297908E-01
  mo   11  -0.349190E+00  -0.275259E+00  -0.187509E+00  -0.695315E-01
  mo   12  -0.767674E-01  -0.885626E-01   0.843870E-01  -0.384707E-01
  mo   13   0.173905E+00   0.213621E+00  -0.131402E+00  -0.802924E-03
  mo   14   0.346083E-01  -0.245248E+00  -0.486917E+00  -0.425127E-01
  mo   15  -0.244225E+00   0.244198E-01  -0.114068E+00  -0.493826E-02
  mo   16   0.534585E-01  -0.132122E+00   0.378441E-01   0.364593E-03
  mo   17  -0.693549E-01  -0.331155E+00  -0.170406E+00  -0.765886E-02
  mo   18   0.338289E+00  -0.605852E-01  -0.429266E-01  -0.594139E-02
  mo   19  -0.153914E+00  -0.203235E+00  -0.209850E+00  -0.247729E-01
  mo   20   0.215667E+00   0.523278E-01  -0.106188E+00   0.100525E-01
  eigenvectors of nos in ao-basis, column    1
  0.9975053E+00 -0.1491475E-01  0.3349685E-02  0.4264178E-02 -0.7872382E-02  0.3731003E-02  0.4432601E-03  0.1029785E-04
  0.3291001E-05  0.1100599E-04 -0.2586514E-03  0.2154741E-04  0.5466556E-04  0.7025556E-03 -0.1419388E-02  0.4904660E-04
  0.4666979E-03  0.2375826E-03 -0.5327533E-03 -0.5352257E-03
  eigenvectors of nos in ao-basis, column    2
  0.3002440E-02  0.9125443E+00 -0.2986703E-02 -0.1329710E+00  0.4574853E-01  0.1023975E-01  0.7467788E-01  0.1585643E-03
  0.2544459E-02 -0.1583088E-02 -0.2566494E-02 -0.7773447E-03 -0.1739955E-02  0.2568231E+00 -0.1113568E+00  0.3061621E-02
  0.2604390E-01  0.1918409E-01  0.1614377E-01  0.1207821E-01
  eigenvectors of nos in ao-basis, column    3
  0.9049327E-02  0.1080813E+00  0.3538565E-02  0.1550157E+00  0.8352166E+00 -0.2689701E-01 -0.2862839E-01 -0.1607553E-02
 -0.2680476E-02 -0.5362522E-02  0.1954194E-02 -0.7900596E-04  0.2671802E-02 -0.3984622E+00  0.1696163E+00  0.5805506E-02
 -0.2995747E-01 -0.1603159E-02 -0.1269389E-02  0.2915978E-01
  eigenvectors of nos in ao-basis, column    4
 -0.4329330E-01 -0.6092984E+00 -0.6215486E-01  0.2528900E-02  0.1321581E+01 -0.2012662E+00 -0.6940037E+00  0.3392292E-02
  0.1554817E-01  0.8664454E-02  0.2573052E-01  0.2147252E-02  0.3611642E-02  0.9575855E+00 -0.4169218E+00 -0.8321026E-02
  0.5944476E-01  0.9912721E-02  0.3477033E-02 -0.3726206E-01
  eigenvectors of nos in ao-basis, column    5
  0.1531562E-01 -0.1071946E+01 -0.4389925E+00  0.1099895E+01 -0.8229242E+00  0.3824157E+00  0.8204378E+00 -0.5136259E-02
  0.3192142E-01 -0.2350810E-01  0.4204418E-01 -0.2426537E-02 -0.3026012E-02  0.4378160E+00 -0.2477968E+00  0.2011862E-01
  0.2569751E-01  0.7888164E-01  0.1348021E-01  0.5712039E-01
  eigenvectors of nos in ao-basis, column    6
 -0.1063202E-01  0.4844025E+00  0.2545461E+00 -0.9482338E+00 -0.7196602E-01  0.3226009E-01  0.1427831E+00 -0.3527934E-01
  0.1151646E+00 -0.8781872E-01  0.2525689E+00 -0.1434422E-01 -0.2086459E-01  0.3121166E+00 -0.1491078E+00 -0.1172567E-01
 -0.3243363E-01  0.8247651E-01 -0.3421553E-01  0.2191385E-01
  eigenvectors of nos in ao-basis, column    7
 -0.2319993E-02 -0.1383779E+00 -0.5736782E-01  0.2224676E+00  0.2750415E+00 -0.1196575E+00 -0.4008679E+00 -0.8665696E-01
 -0.8430555E-01 -0.1766002E+00 -0.1648878E+00 -0.9768216E-02  0.1365361E-01 -0.8174745E-01  0.6197312E-01  0.5487036E-02
 -0.5173565E-01  0.4015678E-01 -0.3486531E-02  0.3941225E-01
  eigenvectors of nos in ao-basis, column    8
 -0.1243705E+00  0.2239662E-01  0.4323443E+00 -0.1917778E+01 -0.2194558E+00  0.3037615E+00  0.9750484E+00  0.1994967E-01
 -0.2458065E+00  0.1518324E-01 -0.2151788E+00 -0.3387195E-01 -0.8762399E-01  0.2110143E+01 -0.1007894E+01 -0.2467540E-02
  0.6987711E-01  0.1637350E+00  0.6687905E-01  0.6758827E-01
  eigenvectors of nos in ao-basis, column    9
 -0.2191208E+00 -0.2818311E+00  0.4154220E+00  0.2850973E+00 -0.2531173E+01  0.2376034E+01  0.4526988E+00 -0.2138150E-01
 -0.4401380E-01  0.1249927E-01  0.3786633E-01  0.8893343E-02  0.4093054E-01 -0.3641643E-01  0.1333257E+00 -0.3919316E-01
 -0.1687711E+00 -0.1017371E+00 -0.5610570E-01 -0.1447735E+00
  eigenvectors of nos in ao-basis, column   10
  0.1603941E+00  0.3582100E+00 -0.1928130E+00 -0.6458284E+00 -0.1566289E+00  0.1996717E+00  0.8288397E+00 -0.2108309E+00
  0.3097485E-01  0.1313955E-01  0.1571265E-01  0.8766995E-01 -0.1174166E+00  0.6599587E+00 -0.3597119E+00 -0.1085618E-01
  0.3307106E+00 -0.2878804E+00  0.9394841E-01 -0.1765963E+00
  eigenvectors of nos in ao-basis, column   11
 -0.1379455E+01 -0.2890034E+01  0.2003316E+01  0.1920596E+01  0.8297217E+00 -0.1105574E+01  0.5979420E-01 -0.3302326E-01
 -0.1033180E+00  0.1696720E-01  0.1385609E+00  0.1422346E+00  0.1631859E+00 -0.2272261E+00  0.2206955E+00 -0.3011717E-02
 -0.1475900E+00 -0.1954118E+00 -0.2455450E-01  0.9220574E-01
  eigenvectors of nos in ao-basis, column   12
  0.8829655E+00  0.1945424E+01 -0.1242114E+01 -0.8418162E+00 -0.1876499E+00  0.2126721E+00 -0.3280872E+00  0.7911362E-01
 -0.1471139E+00 -0.4190420E-01  0.1151654E+00  0.3099105E+00 -0.7256576E-01 -0.2239764E+00  0.2612565E-01 -0.1330128E-01
 -0.6138860E-01  0.1509763E-01 -0.1615595E+00  0.4873760E-01
  eigenvectors of nos in ao-basis, column   13
  0.1014734E+01  0.2249788E+01 -0.1419820E+01 -0.7232204E+00  0.2390072E+00 -0.3799319E+00 -0.1096531E-01 -0.9700577E-01
 -0.2550299E+00  0.8093628E-01  0.2300104E+00 -0.7095071E-01  0.3153660E+00 -0.1894128E+00 -0.1045868E+00 -0.2222355E-01
 -0.9862924E-01 -0.6900085E-01 -0.1503039E+00 -0.1365198E+00
  eigenvectors of nos in ao-basis, column   14
  0.3021208E+00  0.8498465E+00 -0.1794194E+00 -0.3162038E+01 -0.7875005E+00  0.1249357E+01  0.1073572E+01  0.9124174E-01
  0.3118488E+00 -0.1707326E+00 -0.4950408E+00  0.6576286E-01  0.3259958E+00  0.2753043E+01 -0.1270346E+01  0.2215356E-01
  0.4405474E+00 -0.1130134E+00  0.1789097E+00  0.2340141E+00
  eigenvectors of nos in ao-basis, column   15
 -0.2225331E+01 -0.5907954E+01  0.2481511E+01  0.3686430E+01 -0.1227903E+01  0.2403962E+01 -0.2967215E+00  0.2097640E-01
 -0.1632495E+00 -0.7336793E-01 -0.4824900E-01  0.1687561E-01  0.1243782E+00  0.1659447E+01 -0.1568062E+01  0.1165664E+00
  0.1025791E+01  0.5015346E+00 -0.1702589E+00  0.9110536E-02
  eigenvectors of nos in ao-basis, column   16
  0.1085694E+01  0.2940319E+01 -0.1135522E+01 -0.3552859E+01 -0.4806075E-01 -0.8570649E-01  0.4850386E+00  0.1521864E+00
 -0.2264214E+00 -0.3120889E+00  0.4832547E+00 -0.1338576E+00 -0.7748371E-01 -0.6952144E+00  0.1285103E+01 -0.6400283E-01
  0.3016772E+00 -0.8391423E+00  0.3942486E+00  0.3993248E+00
  eigenvectors of nos in ao-basis, column   17
 -0.9966028E+00 -0.2720357E+01  0.9772989E+00  0.5093851E+01  0.6289208E+00 -0.9835088E+00  0.8555908E+00  0.1320350E+00
 -0.2852446E-01 -0.1905433E+00  0.6631118E-01 -0.2616399E-02  0.1560645E-01 -0.1571704E+00 -0.9806959E+00 -0.3500053E-01
  0.2747827E-01 -0.9069558E-01 -0.4755533E+00 -0.1200065E+01
  eigenvectors of nos in ao-basis, column   18
 -0.6103581E+00 -0.1737590E+01  0.4283092E+00  0.6434833E+01  0.1981533E+00 -0.5479518E+00 -0.1366401E+01 -0.1630865E-02
  0.9665784E-02 -0.4758725E-01  0.2773042E+00 -0.1200014E+00 -0.1725480E+00 -0.2152974E+01 -0.2330805E+00  0.3556053E-02
 -0.1744061E+00 -0.7473170E+00 -0.1284191E+01 -0.2293357E+00
  eigenvectors of nos in ao-basis, column   19
  0.1872821E+00  0.5399904E+00 -0.3074368E+00  0.3236433E+01  0.3502692E+00 -0.1177493E+01 -0.1293751E+01  0.5830977E-02
 -0.5805543E-01  0.2684427E-01  0.4315008E+00 -0.4447126E-01 -0.1557788E+00 -0.2379358E+01 -0.1740288E+00  0.2782605E+00
 -0.1055330E+01 -0.6597774E+00  0.3828887E+00  0.6188933E-01
  eigenvectors of nos in ao-basis, column   20
 -0.1559469E+00 -0.4821477E+00  0.6455826E-01  0.2385773E+01  0.1068059E-01 -0.6309516E-01 -0.5764898E+00 -0.2579098E-03
 -0.1200363E-01  0.2818160E-02  0.6378273E-01 -0.5369543E-02 -0.1502271E-01 -0.3152178E+00 -0.1252238E+01  0.8901505E+00
 -0.1245157E+00 -0.9215832E-01 -0.3116049E+00 -0.2506926E+00


*****   symmetry block SYM2   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       1.96820511
       2       0.00195711     0.00062318
       3      -0.01609456    -0.00189942     0.00719477
       4      -0.00222351     0.00074759    -0.00143879     0.00145423
       5       0.00204599     0.00051457    -0.00184837     0.00044164
       6      -0.01516378    -0.00194595     0.00848797    -0.00086960
       7      -0.00337947    -0.00126781     0.00291632    -0.00224164
       8       0.00144729     0.00015594    -0.00051654     0.00026207
       9       0.00924393     0.00036762    -0.00229254    -0.00024180
      10       0.00528883     0.00059171    -0.00174829     0.00089196
      11      -0.00537464    -0.00023365     0.00064328    -0.00039612
      12       0.00093779     0.00003674    -0.00008682     0.00009646
      13      -0.00389929    -0.00000969     0.00030311     0.00015613
      14      -0.00129945    -0.00004049    -0.00007308    -0.00019840

               Column   5     Column   6     Column   7     Column   8
       5       0.00052019
       6      -0.00213646     0.01108835
       7      -0.00086686     0.00232664     0.00383074
       8       0.00004643    -0.00067109    -0.00038679     0.00055664
       9       0.00059585    -0.00361915     0.00021029     0.00025786
      10       0.00047537    -0.00193002    -0.00189035     0.00018440
      11      -0.00022174     0.00064527     0.00080760    -0.00013168
      12      -0.00002320    -0.00007444    -0.00013959     0.00026164
      13      -0.00007976     0.00072852    -0.00028714    -0.00005021
      14       0.00000646    -0.00028953     0.00044873     0.00007235

               Column   9     Column  10     Column  11     Column  12
       9       0.00206133
      10       0.00005009     0.00149729
      11      -0.00024549    -0.00043220     0.00059953
      12       0.00001869     0.00007517    -0.00002338     0.00032452
      13      -0.00073777     0.00008011     0.00000597    -0.00000835
      14       0.00043140    -0.00043049     0.00004849     0.00000899

               Column  13     Column  14
      13       0.00054010
      14      -0.00020253     0.00034745

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7        eno   8
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    8   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno   9        eno  10        eno  11        eno  12        eno  13        eno  14
 emo    9   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   10   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   11   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   12   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   13   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   14   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   1.9685516      0.0211575      0.0058157      0.0010720      0.0008227
   0.0006431      0.0003903      0.0001637      0.0000986      0.0000946
   0.0000181      0.0000100      0.0000052      0.0000002

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7         no   8
  mo    1   0.999911E+00   0.123828E-01  -0.213912E-02  -0.131012E-02  -0.212444E-02   0.634557E-03   0.117123E-02  -0.169728E-02
  mo    2   0.101318E-02  -0.149556E+00  -0.131698E+00   0.173358E-01  -0.218731E+00  -0.397719E-01   0.647126E-01   0.748361E-01
  mo    3  -0.825194E-02   0.570134E+00   0.188911E-01   0.214260E+00   0.358154E+00  -0.351894E-01   0.681762E-01  -0.241460E+00
  mo    4  -0.111693E-02  -0.115994E+00  -0.414870E+00   0.256597E+00  -0.285922E+00  -0.432918E-01   0.190750E+00  -0.110052E+00
  mo    5   0.105900E-02  -0.148656E+00  -0.274478E-01  -0.297405E-01  -0.976230E-01  -0.194308E+00  -0.567170E-01   0.156897E+00
  mo    6  -0.779871E-02   0.699126E+00  -0.308928E+00   0.141742E+00  -0.194588E+00  -0.103508E+00  -0.143696E-02   0.194791E+00
  mo    7  -0.174452E-02   0.238248E+00   0.662899E+00  -0.204884E+00   0.135187E-01   0.110234E+00  -0.522589E-01   0.875515E-01
  mo    8   0.741617E-03  -0.480972E-01  -0.301518E-01   0.362424E+00   0.333175E-01   0.703237E+00  -0.868308E-01  -0.400992E+00
  mo    9   0.472586E-02  -0.201657E+00   0.359416E+00   0.552962E+00   0.116222E+00  -0.232586E+00   0.208019E-01  -0.110278E-01
  mo   10   0.270606E-02  -0.154223E+00  -0.295590E+00  -0.129601E+00   0.725647E+00   0.217365E-01   0.187616E+00  -0.175054E-01
  mo   11  -0.273809E-02   0.574043E-01   0.117153E+00  -0.316388E+00  -0.220811E+00   0.224497E+00   0.785261E+00  -0.172859E+00
  mo   12   0.477441E-03  -0.797095E-02  -0.256781E-01   0.177820E+00   0.302481E-01   0.530185E+00   0.335237E-01   0.754688E+00
  mo   13  -0.198682E-02   0.340189E-01  -0.147542E+00  -0.411745E+00  -0.119771E+00   0.232546E+00  -0.535108E+00  -0.218864E+00
  mo   14  -0.658401E-03  -0.737216E-02   0.144694E+00   0.264244E+00  -0.298916E+00  -0.473247E-03  -0.394466E-01  -0.212600E+00

                no   9         no  10         no  11         no  12         no  13         no  14
  mo    1   0.459998E-04   0.175677E-02   0.115819E-02   0.133579E-02   0.160017E-02   0.309008E-03
  mo    2  -0.437520E-01  -0.120531E+00   0.318821E+00  -0.153475E+00  -0.175292E-01   0.871104E+00
  mo    3  -0.564717E-01   0.357221E+00   0.123908E+00   0.358582E+00   0.296726E+00   0.270878E+00
  mo    4  -0.183472E+00  -0.137742E-01   0.629764E+00   0.168032E+00   0.129108E+00  -0.375638E+00
  mo    5  -0.110694E+00   0.103019E+00  -0.191166E+00  -0.299362E+00   0.866781E+00  -0.283708E-01
  mo    6  -0.839966E-01  -0.182700E+00  -0.109694E+00  -0.478880E+00  -0.146577E+00  -0.763722E-01
  mo    7  -0.184854E-01  -0.273176E+00   0.545266E+00  -0.177786E+00   0.146934E+00  -0.116667E+00
  mo    8  -0.734333E-01  -0.339407E+00  -0.154384E+00  -0.155054E+00   0.176314E+00   0.434266E-01
  mo    9  -0.501653E+00   0.248891E+00  -0.189938E-01  -0.291545E+00  -0.238890E+00  -0.133066E-01
  mo   10   0.168432E+00   0.395337E-01   0.246613E+00  -0.463391E+00  -0.289016E-01  -0.563781E-01
  mo   11  -0.187058E+00   0.248152E+00  -0.144794E+00  -0.138424E+00  -0.223485E-01  -0.171632E-02
  mo   12   0.234478E-01   0.317455E+00   0.646309E-01   0.970087E-01   0.460321E-02  -0.568137E-02
  mo   13  -0.338143E+00   0.474272E+00   0.140942E+00  -0.187228E+00  -0.103085E+00  -0.717682E-02
  mo   14   0.711538E+00   0.416002E+00   0.102385E+00  -0.285793E+00  -0.212807E-01  -0.334864E-01
  eigenvectors of nos in ao-basis, column    1
  0.7599241E+00 -0.3242748E-01 -0.1376734E+00 -0.1581435E-01 -0.8205389E-02  0.2766701E-03 -0.3342454E-02 -0.5500328E+00
  0.1516381E+00 -0.9922256E-02 -0.2206412E-01 -0.3020306E-01 -0.2093075E-01 -0.2480066E-01
  eigenvectors of nos in ao-basis, column    2
 -0.1330034E+01  0.1104618E+00  0.5307345E+00 -0.3849849E-01 -0.5927496E-01  0.1786386E-04  0.3452011E-02 -0.1034473E+01
  0.4048683E+00 -0.1438661E-01 -0.5309019E-01 -0.3265371E-01 -0.8361063E-02  0.3796891E-03
  eigenvectors of nos in ao-basis, column    3
 -0.7403918E+00  0.3332633E+00  0.8701464E+00  0.2831650E+00  0.5410728E+00 -0.8289830E-02  0.1193097E-01  0.2371025E+00
 -0.8332926E-01 -0.3407406E-01  0.8913455E-01 -0.1408792E-01  0.3693896E-01  0.3222713E-01
  eigenvectors of nos in ao-basis, column    4
  0.1316718E+01 -0.1143441E+01 -0.1334381E+01  0.5231621E+00  0.4194559E+00  0.2538715E-01  0.9023092E-02 -0.1467743E+01
  0.3379643E+00  0.4169616E-01 -0.1172146E+00  0.1927140E+00 -0.1257610E+00 -0.9924854E-01
  eigenvectors of nos in ao-basis, column    5
  0.2064089E+01 -0.1830664E+01  0.3662251E+00 -0.2268076E+00 -0.2909865E+00 -0.2164839E-01  0.7951929E-01  0.1061642E+01
 -0.5909759E+00  0.3715469E-01  0.2071072E+00  0.1686941E+00  0.1187783E+00  0.2041454E+00
  eigenvectors of nos in ao-basis, column    6
 -0.6801623E+00  0.7343785E+00  0.1169542E+01 -0.1554779E+00 -0.1444073E+00  0.7235754E-01  0.2345492E+00  0.1052565E+01
 -0.6753657E-01 -0.4775621E-01 -0.2286879E+00  0.3989708E+00  0.1510731E+00  0.1309393E+00
  eigenvectors of nos in ao-basis, column    7
 -0.6921860E+00  0.1001985E+01 -0.2259327E-01  0.3658834E+00 -0.4357369E+00 -0.8185210E-01  0.4199374E+00  0.3319383E+00
 -0.5240353E+00  0.8406725E-01  0.1644378E+00  0.1181829E+00 -0.2063404E+00  0.9344164E-01
  eigenvectors of nos in ao-basis, column    8
  0.1297571E+01 -0.2180960E+01 -0.9916643E+00 -0.9679017E-01  0.7074531E+00  0.6864269E-01  0.3942512E+00 -0.2200138E+01
  0.1243799E+01 -0.1604923E+00 -0.1030286E+00 -0.8224922E+00 -0.1069899E+00 -0.2396815E+00
  eigenvectors of nos in ao-basis, column    9
  0.2241539E+00 -0.5075392E+00  0.1768322E+01  0.9615229E+00 -0.1422026E+01  0.3598985E-01 -0.1154373E+00  0.1923330E+01
 -0.1227469E+00 -0.4696045E-01 -0.1425305E+00 -0.2071414E+00  0.2566830E+00  0.3553306E+00
  eigenvectors of nos in ao-basis, column   10
 -0.1629973E+01  0.3213522E+01 -0.5339677E+00  0.1424029E+00 -0.1261890E+01  0.1034485E+00 -0.3784613E-01  0.2786843E+01
 -0.3444520E+01  0.3495078E+00  0.1153597E+01  0.3130464E+00 -0.4444341E+00 -0.9218738E-01
  eigenvectors of nos in ao-basis, column   11
  0.2355683E+00 -0.3477908E+00  0.3517132E+01  0.2930119E-01 -0.6033195E+00  0.3575096E-01 -0.7770126E-01  0.1805354E+01
  0.2199642E+01 -0.2692919E-01  0.3952716E+00  0.3856858E-01  0.3446974E+00  0.1802404E+01
  eigenvectors of nos in ao-basis, column   12
  0.5795537E+00 -0.1818874E+01 -0.2966268E+01 -0.2153051E+00  0.1639998E+01 -0.2099602E-01  0.1357424E+00 -0.3101272E+01
 -0.3318196E+01  0.4481282E+00 -0.1038827E+01 -0.1013442E+01 -0.1667588E+01 -0.3839232E-01
  eigenvectors of nos in ao-basis, column   13
  0.2448872E+00 -0.8273458E+00  0.2586502E+01  0.4678973E-01  0.1544165E+00 -0.1150575E-01  0.3488769E-01 -0.1263369E+00
  0.8741699E+01  0.2058684E+00 -0.4569648E+00 -0.9865854E-01  0.4569289E+01  0.1268010E+01
  eigenvectors of nos in ao-basis, column   14
 -0.8167350E-02  0.6094295E-03 -0.1475947E+01 -0.3979800E-01  0.2344793E+00 -0.4718710E-02  0.9739464E-02 -0.4097131E+00
 -0.2494934E+01  0.2873555E+01 -0.1561152E+00 -0.6303541E-01 -0.4397526E+00 -0.7080894E+00


*****   symmetry block SYM3   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       1.97122428
       2       0.01356110     0.00562364
       3       0.01300125     0.00616055     0.00860822
       4       0.00476775    -0.00323485    -0.00184173     0.00462141
       5       0.00028285     0.00031761     0.00149333     0.00129082
       6      -0.00732122     0.00212285     0.00309996    -0.00113768
       7       0.00000402     0.00001893    -0.00002219    -0.00004862
       8      -0.00079073     0.00018380     0.00004077    -0.00050193
       9       0.00015277    -0.00022964     0.00023696     0.00110694

               Column   5     Column   6     Column   7     Column   8
       5       0.00139380
       6       0.00048337     0.00188672
       7      -0.00003826    -0.00000310     0.00038111
       8      -0.00004015     0.00006577     0.00000443     0.00037902
       9       0.00077217    -0.00000145     0.00000448    -0.00014505

               Column   9
       9       0.00076212

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7        eno   8
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    8   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno   9
 emo    9   0.100000E+01

 occupation numbers of nos
   1.9714432      0.0157383      0.0053383      0.0008001      0.0006246
   0.0003944      0.0003623      0.0001208      0.0000583

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7         no   8
  mo    1   0.999944E+00  -0.686126E-02  -0.325311E-02   0.688476E-02   0.209066E-02   0.633319E-03   0.785999E-03  -0.572462E-03
  mo    2   0.691088E-02   0.576942E+00  -0.121696E+00  -0.459640E+00   0.114659E+00   0.986152E-01   0.134913E+00   0.111274E+00
  mo    3   0.663704E-02   0.699607E+00   0.369653E+00  -0.278571E-01  -0.304361E+00  -0.121040E+00  -0.172894E+00   0.882873E-02
  mo    4   0.240876E-02  -0.310050E+00   0.748453E+00  -0.123766E+00  -0.353083E+00  -0.958670E-02  -0.607180E-01   0.235994E+00
  mo    5   0.150424E-03   0.658824E-01   0.440788E+00   0.628603E-01   0.596117E+00  -0.166710E+00  -0.112978E+00  -0.621451E+00
  mo    6  -0.370046E-02   0.276482E+00   0.773027E-01   0.859689E+00   0.389846E-02   0.183649E+00   0.183293E+00   0.151380E+00
  mo    7   0.197338E-05   0.460794E-03  -0.127359E-01  -0.172781E-01   0.295977E-01   0.765984E+00  -0.637915E+00  -0.701948E-01
  mo    8  -0.401111E-03   0.204245E-01  -0.875447E-01   0.105709E+00   0.327521E+00  -0.471806E+00  -0.608526E+00   0.521070E+00
  mo    9   0.789553E-04  -0.175919E-01   0.283300E+00  -0.134535E+00   0.553200E+00   0.323544E+00   0.353015E+00   0.496274E+00

                no   9
  mo    1  -0.847877E-03
  mo    2   0.622541E+00
  mo    3  -0.485623E+00
  mo    4   0.379769E+00
  mo    5   0.123699E+00
  mo    6   0.297066E+00
  mo    7   0.832874E-02
  mo    8   0.951652E-01
  mo    9  -0.345996E+00
  eigenvectors of nos in ao-basis, column    1
  0.9200891E+00 -0.6373164E-02  0.4191132E-01 -0.4916296E-02 -0.1973892E-01  0.1136309E-02  0.3255315E-03  0.2302298E-01
  0.3947391E-01
  eigenvectors of nos in ao-basis, column    2
 -0.1650307E+01  0.5304783E+00  0.1277412E+01 -0.1595407E-01 -0.7182660E-01  0.1366179E-03  0.4879579E-03  0.6469747E-01
  0.8866387E-01
  eigenvectors of nos in ao-basis, column    3
 -0.2274181E+00  0.9811523E-01  0.3531813E+00  0.3437665E+00  0.7117732E+00 -0.3187115E-02 -0.5130652E-02 -0.6991203E-01
 -0.5136936E-01
  eigenvectors of nos in ao-basis, column    4
 -0.2667156E+01  0.2739419E+01  0.4297336E+00 -0.1560748E+00  0.3002935E-01  0.7677488E-02 -0.3883836E-03 -0.3276570E-01
 -0.2040169E+00
  eigenvectors of nos in ao-basis, column    5
 -0.2060395E+00  0.2475933E+00 -0.6501806E+00  0.5734653E+00 -0.4233204E-01  0.8110765E-01  0.2988744E-01  0.4968578E+00
  0.1234292E+00
  eigenvectors of nos in ao-basis, column    6
 -0.3743740E+00  0.4785589E+00 -0.2269341E+00  0.4529667E+00 -0.3505688E+00 -0.1401483E+00  0.8874925E-01 -0.5573453E-01
  0.1455774E+00
  eigenvectors of nos in ao-basis, column    7
 -0.3658708E+00  0.4786566E+00 -0.3498624E+00  0.5290901E+00 -0.3897050E+00 -0.6031030E-01 -0.1253178E+00  0.2065489E-01
  0.1780993E+00
  eigenvectors of nos in ao-basis, column    8
 -0.4581328E+00  0.6853483E+00  0.2931109E+00  0.5076925E+00 -0.8992884E+00  0.1248474E+00  0.2771762E-01 -0.8797667E+00
  0.1940182E+00
  eigenvectors of nos in ao-basis, column    9
 -0.8281088E+00  0.1327408E+01 -0.1787711E+01 -0.4267461E+00  0.5606541E+00  0.2854591E-02  0.1136524E-02 -0.5498962E-01
  0.1040324E+01


*****   symmetry block SYM4   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       0.00577708
       2      -0.00074430     0.00082947
       3       0.00035685    -0.00000645     0.00026575
       4       0.00015358    -0.00015021     0.00016810     0.00020952
       5      -0.00004937    -0.00013320    -0.00007153    -0.00001352

               Column   5
       5       0.00015401

               eno   1        eno   2        eno   3        eno   4        eno   5
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   0.0059150      0.0007789      0.0003967      0.0001043      0.0000411

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5
  mo    1   0.986799E+00   0.140784E+00  -0.648407E-01   0.261718E-01   0.389571E-01
  mo    2  -0.145303E+00   0.945951E+00   0.318209E-01   0.246341E+00   0.149558E+00
  mo    3   0.635363E-01   0.534887E-01   0.745823E+00   0.129263E+00  -0.648183E+00
  mo    4   0.322749E-01  -0.190701E+00   0.609850E+00   0.226712E+00   0.734354E+00
  mo    5  -0.596164E-02  -0.214777E+00  -0.258098E+00   0.933019E+00  -0.129218E+00
  eigenvectors of nos in ao-basis, column    1
 -0.3103141E+00 -0.6780736E+00  0.4218479E-01  0.1107785E+00  0.9712768E-01
  eigenvectors of nos in ao-basis, column    2
 -0.6077124E+00 -0.1210654E+00 -0.3194190E+00 -0.4918864E+00 -0.1704706E+00
  eigenvectors of nos in ao-basis, column    3
  0.5713607E+00 -0.4040327E+00 -0.7795878E+00 -0.5008424E-01  0.3311692E+00
  eigenvectors of nos in ao-basis, column    4
  0.7043045E+00 -0.1197697E+01  0.6087599E+00 -0.8128630E+00  0.6604993E-01
  eigenvectors of nos in ao-basis, column    5
  0.3567980E+00 -0.3439119E+00 -0.2474014E+00  0.3911994E+00 -0.1148884E+01


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***


 cosurf_and_phi
  1.262739748330  2.872870034186  1.567866969040 -0.042887343159
  1.652947872482  0.360731428865  3.496571379926 -0.052232804076
  0.594940552015  0.670555958619  3.845537291721 -0.062628204807
  2.390078334690  1.202722009900  2.566708449184 -0.033333303920
  1.989334673621  2.229216060677  2.001028520754 -0.036170812674
  2.919143888229  0.378144896597  2.099775822683 -0.012740748852
  0.105380882100  1.878884201281  3.371423292662 -0.062885399923
  0.783266721831  2.590884380442  2.520834408126 -0.056195537142
  2.011402526412  2.551496219550  0.814855178005 -0.017087064916
  0.099626804209  1.855073908563 -1.945822518898  0.030618667056
  0.119020599620  3.173161356543  1.415159765853 -0.048759047257
  0.915237473358  3.108118624501  0.463299650838 -0.030457012708
  0.462814589486  2.837210699514 -0.795516582628 -0.006468225496
  1.408719892640  1.640288870679  3.148120355694 -0.053957635512
  2.650053602975  1.633766196698  1.655441764425 -0.014332190178
  1.142904167226  2.885766749848 -0.243475252462 -0.012143114421
  3.656106873706  0.713798214804  0.361832640492  0.038268712723
  2.408046317685  2.075600470298 -1.226192756897  0.039379694526
  1.295820311657  0.567673501678 -2.747601655947  0.057570259611
  3.527730862121  1.045649613724 -1.064853177123  0.054671434899
  2.622898581638  1.024710819001 -2.241122746235  0.059353458954
  3.086808508398  1.794857685487 -0.179703829578  0.034087975156
  1.615872011596  1.674366500124 -2.147504385867  0.047184590657
  0.000006852884  1.035694667087 -2.361676372823  0.049807691412
  0.298815704890  0.969607820141 -2.408807386530  0.051389776477
  0.667723897920  1.245157743773 -2.338577856151  0.049038577068
  1.218123388571  2.025110412626 -1.616576841774  0.034812454844
  2.084186643139  2.293984793080 -0.480493513306  0.021436900212
  2.876642520254  1.658030225134  0.559035126479  0.020768956167
  3.282919570196  0.368088739237  1.091983758387  0.024207550658
  0.467932162408  1.694535407938 -1.941510815499  0.037365095968
  1.299770347024  2.453875604722 -0.850324284820  0.011628545318
  2.242050270258  2.245320705184  0.385739526123  0.002952965634
  2.923104801922  1.151131828431  1.279135167181  0.007049778207
  0.427044967836  0.580963354323 -2.585108185092  0.055148137057
 end_of_phi


 nsubv after electrostatic potential =  5

  Confirmation of dielectric energy for ground state
  edielnew =  -0.00269315145

 =========== Executing IN-CORE method ==========
 norm multnew:  1.59814318E-07
 threx: total number of loops= 14
 threx: total number of loops= 24
 blstr diagel,allext,nmin2=  1892
 onel.diag.
 -0.194489E+01-0.285984E+01-0.270263E+01-0.267917E+01-0.404205E+01-0.353093E+01-0.365990E+01-0.378020E+01-0.336100E+01-0.260028E+01
 -0.287334E+01-0.357379E+01-0.152448E+01-0.144552E+01-0.152236E+01-0.750255E+00 0.955267E+00-0.155461E+01-0.292438E+01-0.240582E+01
 -0.200527E+01-0.405335E+01-0.340936E+01-0.250410E+01-0.287148E+01-0.299037E+01-0.266619E+01-0.118115E+01-0.155099E+01-0.374151E+00
 -0.277725E+01-0.388112E+01-0.376164E+01-0.283234E+01-0.443834E+01-0.160190E+01-0.133520E+01-0.168096E+01-0.459124E+01-0.280667E+01
 -0.220410E+01-0.135623E+01-0.747446E+00-0.330496E+02-0.787938E+01-0.692069E+01-0.675716E+01-0.704599E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1      1892      1892
 blstr diagel,allext,nmin2=  1892
 onel.diag.
 -0.194489E+01-0.285984E+01-0.270263E+01-0.267917E+01-0.404205E+01-0.353093E+01-0.365990E+01-0.378020E+01-0.336100E+01-0.260028E+01
 -0.287334E+01-0.357379E+01-0.152448E+01-0.144552E+01-0.152236E+01-0.750255E+00 0.955267E+00-0.155461E+01-0.292438E+01-0.240582E+01
 -0.200527E+01-0.405335E+01-0.340936E+01-0.250410E+01-0.287148E+01-0.299037E+01-0.266619E+01-0.118115E+01-0.155099E+01-0.374151E+00
 -0.277725E+01-0.388112E+01-0.376164E+01-0.283234E+01-0.443834E+01-0.160190E+01-0.133520E+01-0.168096E+01-0.459124E+01-0.280667E+01
 -0.220410E+01-0.135623E+01-0.747446E+00-0.330496E+02-0.787938E+01-0.692069E+01-0.675716E+01-0.704599E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1      1892      1892
 blstr diagel,allext,nmin2=  1892
 onel.diag.
 -0.194489E+01-0.285984E+01-0.270263E+01-0.267917E+01-0.404205E+01-0.353093E+01-0.365990E+01-0.378020E+01-0.336100E+01-0.260028E+01
 -0.287334E+01-0.357379E+01-0.152448E+01-0.144552E+01-0.152236E+01-0.750255E+00 0.955267E+00-0.155461E+01-0.292438E+01-0.240582E+01
 -0.200527E+01-0.405335E+01-0.340936E+01-0.250410E+01-0.287148E+01-0.299037E+01-0.266619E+01-0.118115E+01-0.155099E+01-0.374151E+00
 -0.277725E+01-0.388112E+01-0.376164E+01-0.283234E+01-0.443834E+01-0.160190E+01-0.133520E+01-0.168096E+01-0.459124E+01-0.280667E+01
 -0.220410E+01-0.135623E+01-0.747446E+00-0.330496E+02-0.787938E+01-0.692069E+01-0.675716E+01-0.704599E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1      1892      1892


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        12 xx:        36 ww:        79
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        11
task #   9:        28    task #  10:        71    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 matrix-vector product formed for vector   6
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference-subspace vector overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.97234180     0.06649896    -0.09372291     0.00000000    -0.00009842     0.00011776

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.97232334    -0.00531155    -0.06495217    -0.05954075     0.08491072     0.00115004
 subspace has dimension  6

          (h-repnuc*1) matrix in the subspace basis

                x:   1         x:   2         x:   3         x:   4         x:   5         x:   6
   x:   1   -85.67245276
   x:   2    -0.00036856   -83.87106855
   x:   3    -0.00050015     0.00069489   -82.26740199
   x:   4     0.00004199    -0.00501832    -0.01027778    -0.00001555
   x:   5     0.00627730    -0.01578978    -0.03017397    -0.00001614    -0.00026848
   x:   6    -0.00886081     0.00055702     0.00380960     0.00000088    -0.00000447    -0.00001322

          overlap matrix in the subspace basis

                x:   1         x:   2         x:   3         x:   4         x:   5         x:   6
   x:   1     1.00000000
   x:   2     0.00000000     1.00000000
   x:   3     0.00000000     0.00000000     1.00000000
   x:   4    -0.00000050     0.00006117     0.00012303     0.00000019
   x:   5    -0.00007332     0.00018428     0.00035308     0.00000019     0.00000318
   x:   6     0.00010343    -0.00000637    -0.00004511    -0.00000001     0.00000005     0.00000016

          eigenvectors and eigenvalues in the subspace basis

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6

   energy   -85.67245895   -84.65319427   -83.91938776   -82.81719784   -81.92990849   -81.42133089

   x:   1     0.99998655    -0.03486367    -0.01240261     0.17049328    -0.01120036    -0.21861529
   x:   2     0.00000518     0.04851333    -1.00665889     0.02395071     0.01395985     0.11165224
   x:   3     0.00001490     0.04627457    -0.00356303     0.35801496    -0.85955932     0.50905246
   x:   4     1.57461449   281.28713729   497.16836000  1188.83683630  1843.31344774   993.84431750
   x:   5     1.15930912   501.91789417    37.24581546   -33.59461504   -52.83314951  -301.25169470
   x:   6     0.93211874   671.69128118   147.60002701 -1670.08429890    77.38766921  1905.25092999

          eigenvectors and eigenvalues in the schmidt orthonormalized basis

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6

   energy   -85.67245895   -84.65319427   -83.91938776   -82.81719784   -81.92990849   -81.42133089

  zx:   1     0.99999717    -0.00233410    -0.00011628    -0.00036906    -0.00024434     0.00002907
  zx:   2     0.00030921     0.15393907    -0.97032073     0.10111880     0.11649505     0.10480571
  zx:   3     0.00057591     0.22779798     0.06409625     0.56776014    -0.65492020     0.43900726
  zx:   4     0.00101242     0.27071071     0.21516294     0.49579278     0.74102438     0.29239013
  zx:   5     0.00201991     0.88640641     0.07011433    -0.13142555    -0.08673554    -0.42961360
  zx:   6     0.00035490     0.25574358     0.05619808    -0.63587744     0.02946502     0.72541613

          <ref|baseci> overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.97234180     0.06649896    -0.09372291     0.00000000    -0.00009842     0.00011776

 trial vector basis is being transformed.  new dimension:   3

          transformed tciref transformation matrix  block   1

               ref   1        ref   2        ref   3
  ci:   1     0.97232334    -0.00531155    -0.06495217

  write the transformed vectors back to cosmofile

 rtflw =  0
 freezing =  F
 resid(rootcalc)   0.000399087182
 *******************************************
 ** Charges frozen from cosmo calculation **
 *******************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  9  1    -76.3378784647 -9.3346E+00  3.7153E-08  3.9909E-04  1.0000E-03
 mr-sdci #  9  2    -75.3186137815 -9.1780E+00  0.0000E+00  6.4753E-01  1.0000E-04
 mr-sdci #  9  3    -74.5848072727 -9.3282E+00  0.0000E+00  1.0026E+00  1.0000E-04
 mr-sdci #  9  4    -73.4826173497 -8.7150E+00  0.0000E+00  2.0022E+00  1.0000E-04
 mr-sdci #  9  5    -72.5953280006 -9.3330E+00  0.0000E+00  2.8820E+00  1.0000E-04
 mr-sdci #  9  6    -72.0867504023 -3.4105E+00  0.0000E+00  3.4818E+00  1.0000E-04


 total energy in cosmo calc 
e(rootcalc) + repnuc=     -76.3378784647
dielectric energy =      -0.0107050726
deltaediel =       0.0107050726
e(rootcalc) + repnuc - ediel =     -76.3271733921
e(rootcalc) + repnuc - edielnew =     -76.3378784647
deltaelast =      76.3271733921


 mr-sdci  convergence criteria satisfied after  9 iterations.

 *************************************************
 ***Final Calc of density matrix for cosmo calc***
 *************************************************

 entering drivercid: firstcall,firstnonref= F T
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=       3  D0X=       9  D0W=      15
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================

          ci-one-electron density block   1

                mo   1         mo   2         mo   3         mo   4         mo   5         mo   6         mo   7         mo   8
   mo   1     1.99955706
   mo   2    -0.00013912     1.98336771
   mo   3    -0.00012872     0.00305497     1.96960509
   mo   4    -0.00014481     0.00137815     0.01324571     0.00121120
   mo   5     0.00019696     0.00409182    -0.01098209    -0.00247990     0.00654401
   mo   6    -0.00019391     0.00513909     0.00415830     0.00027860     0.00059726     0.00282696
   mo   7     0.00034838    -0.00584070    -0.01582426    -0.00101986     0.00054433    -0.00134392     0.00267255
   mo   8    -0.00024172    -0.00665196     0.00460654     0.00271688    -0.00785165    -0.00035257     0.00014454     0.01038963
   mo   9    -0.00031420     0.00628472     0.00866476     0.00020590     0.00126511     0.00290520    -0.00198135    -0.00126729
   mo  10     0.00012371     0.00214512    -0.00561138    -0.00063751     0.00154588     0.00210722     0.00067724    -0.00109211
   mo  11     0.00063673    -0.00080085     0.00015923    -0.00066339    -0.00029474    -0.00142560     0.00258255     0.00142084
   mo  12     0.00059430     0.00195092    -0.00508164    -0.00087109     0.00132714    -0.00136082     0.00132418    -0.00175339
   mo  13    -0.00000617    -0.00132071     0.00135338     0.00026107    -0.00090348     0.00036190     0.00028187     0.00148443
   mo  14    -0.00026857    -0.00395732     0.00506379     0.00087750    -0.00228815    -0.00028486    -0.00035025     0.00302428
   mo  15    -0.00031645     0.00207076    -0.00645326    -0.00013645     0.00088493     0.00089836    -0.00057033    -0.00118938
   mo  16     0.00001372    -0.00013710     0.00059135     0.00001763    -0.00006509    -0.00015951    -0.00000804     0.00004006
   mo  17    -0.00025107     0.00010226    -0.00203499     0.00007406     0.00004801    -0.00001314    -0.00035941    -0.00039836
   mo  18    -0.00005409     0.00020313    -0.00027579     0.00005229    -0.00011995    -0.00016312    -0.00008446     0.00012872
   mo  19    -0.00014780     0.00046471    -0.00050900     0.00009474    -0.00015649     0.00033677    -0.00009990     0.00029398
   mo  20     0.00086462     0.00220817    -0.00033021    -0.00020413     0.00024018    -0.00027788     0.00045338    -0.00025191

                mo   9         mo  10         mo  11         mo  12         mo  13         mo  14         mo  15         mo  16
   mo   9     0.00530842
   mo  10     0.00065015     0.00421823
   mo  11    -0.00074255    -0.00042365     0.00439796
   mo  12    -0.00239028     0.00007962     0.00013551     0.00253596
   mo  13    -0.00065407     0.00130363    -0.00011953    -0.00001709     0.00120969
   mo  14    -0.00070777    -0.00088679    -0.00032999    -0.00016039     0.00035749     0.00158332
   mo  15     0.00157316     0.00057033    -0.00074237    -0.00040608    -0.00029212    -0.00054256     0.00112617
   mo  16    -0.00002596    -0.00039610     0.00013329     0.00000602    -0.00006984     0.00004336    -0.00004169     0.00039338
   mo  17    -0.00017889    -0.00027363    -0.00094965     0.00035947    -0.00013809     0.00004003     0.00007544     0.00000863
   mo  18     0.00037093    -0.00084614     0.00051532    -0.00040237    -0.00060550    -0.00000457    -0.00000205     0.00008847
   mo  19     0.00047014     0.00039024     0.00017913    -0.00062356     0.00024337    -0.00023421     0.00009992    -0.00004459
   mo  20    -0.00040129     0.00009083     0.00054589     0.00048641    -0.00001347    -0.00021988    -0.00014314     0.00000589

                mo  17         mo  18         mo  19         mo  20
   mo  17     0.00060826
   mo  18    -0.00012421     0.00067315
   mo  19    -0.00022301    -0.00000228     0.00043423
   mo  20    -0.00006721    -0.00002337    -0.00007133     0.00038122

          ci-one-electron density block   2

                mo  21         mo  22         mo  23         mo  24         mo  25         mo  26         mo  27         mo  28
   mo  21     1.96822575
   mo  22     0.00197378     0.00062256
   mo  23    -0.01615999    -0.00189568     0.00717806
   mo  24    -0.00224037     0.00074864    -0.00143957     0.00145756
   mo  25     0.00204421     0.00051376    -0.00184389     0.00044250     0.00051905
   mo  26    -0.01514240    -0.00194358     0.00847411    -0.00087267    -0.00213266     0.01107563
   mo  27    -0.00337115    -0.00126793     0.00291258    -0.00224536    -0.00086668     0.00232588     0.00383396
   mo  28     0.00144470     0.00015629    -0.00051847     0.00026188     0.00004706    -0.00067398    -0.00038666     0.00055603
   mo  29     0.00921810     0.00036790    -0.00229242    -0.00024061     0.00059549    -0.00361860     0.00020935     0.00025905
   mo  30     0.00528304     0.00059172    -0.00174622     0.00089370     0.00047508    -0.00192830    -0.00189188     0.00018455
   mo  31    -0.00537462    -0.00023383     0.00064350    -0.00039672    -0.00022182     0.00064611     0.00080815    -0.00013183
   mo  32     0.00093817     0.00003671    -0.00008689     0.00009635    -0.00002317    -0.00007481    -0.00013934     0.00026158
   mo  33    -0.00389632    -0.00000999     0.00030393     0.00015574    -0.00007988     0.00072897    -0.00028647    -0.00005040
   mo  34    -0.00130189    -0.00004052    -0.00007296    -0.00019845     0.00000641    -0.00028923     0.00044886     0.00007238

                mo  29         mo  30         mo  31         mo  32         mo  33         mo  34
   mo  29     0.00206077
   mo  30     0.00005015     0.00149763
   mo  31    -0.00024599    -0.00043257     0.00059961
   mo  32     0.00001904     0.00007507    -0.00002341     0.00032449
   mo  33    -0.00073743     0.00007982     0.00000622    -0.00000845     0.00053991
   mo  34     0.00043126    -0.00043061     0.00004845     0.00000903    -0.00020247     0.00034740

          ci-one-electron density block   3

                mo  35         mo  36         mo  37         mo  38         mo  39         mo  40         mo  41         mo  42
   mo  35     1.97123721
   mo  36     0.01353781     0.00560978
   mo  37     0.01309235     0.00615749     0.00861921
   mo  38     0.00471092    -0.00322724    -0.00183848     0.00461469
   mo  39     0.00027789     0.00031669     0.00149390     0.00129122     0.00139351
   mo  40    -0.00729666     0.00212022     0.00310072    -0.00113514     0.00048345     0.00188560
   mo  41     0.00000775     0.00001862    -0.00002200    -0.00004764    -0.00003778    -0.00000313     0.00038091
   mo  42    -0.00079094     0.00018381     0.00004113    -0.00050139    -0.00003994     0.00006586     0.00000430     0.00037883
   mo  43     0.00014678    -0.00023011     0.00023610     0.00110652     0.00077179    -0.00000164     0.00000468    -0.00014491

                mo  43
   mo  43     0.00076178

          ci-one-electron density block   4

                mo  44         mo  45         mo  46         mo  47         mo  48
   mo  44     0.00577868
   mo  45    -0.00074309     0.00082811
   mo  46     0.00035796    -0.00000627     0.00026586
   mo  47     0.00015354    -0.00014950     0.00016794     0.00020920
   mo  48    -0.00004957    -0.00013325    -0.00007156    -0.00001354     0.00015398


*****   symmetry block SYM1   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       1.99955706
       2      -0.00013912     1.98336771
       3      -0.00012872     0.00305497     1.96960509
       4      -0.00014481     0.00137815     0.01324571     0.00121120
       5       0.00019696     0.00409182    -0.01098209    -0.00247990
       6      -0.00019391     0.00513909     0.00415830     0.00027860
       7       0.00034838    -0.00584070    -0.01582426    -0.00101986
       8      -0.00024172    -0.00665196     0.00460654     0.00271688
       9      -0.00031420     0.00628472     0.00866476     0.00020590
      10       0.00012371     0.00214512    -0.00561138    -0.00063751
      11       0.00063673    -0.00080085     0.00015923    -0.00066339
      12       0.00059430     0.00195092    -0.00508164    -0.00087109
      13      -0.00000617    -0.00132071     0.00135338     0.00026107
      14      -0.00026857    -0.00395732     0.00506379     0.00087750
      15      -0.00031645     0.00207076    -0.00645326    -0.00013645
      16       0.00001372    -0.00013710     0.00059135     0.00001763
      17      -0.00025107     0.00010226    -0.00203499     0.00007406
      18      -0.00005409     0.00020313    -0.00027579     0.00005229
      19      -0.00014780     0.00046471    -0.00050900     0.00009474
      20       0.00086462     0.00220817    -0.00033021    -0.00020413

               Column   5     Column   6     Column   7     Column   8
       5       0.00654401
       6       0.00059726     0.00282696
       7       0.00054433    -0.00134392     0.00267255
       8      -0.00785165    -0.00035257     0.00014454     0.01038963
       9       0.00126511     0.00290520    -0.00198135    -0.00126729
      10       0.00154588     0.00210722     0.00067724    -0.00109211
      11      -0.00029474    -0.00142560     0.00258255     0.00142084
      12       0.00132714    -0.00136082     0.00132418    -0.00175339
      13      -0.00090348     0.00036190     0.00028187     0.00148443
      14      -0.00228815    -0.00028486    -0.00035025     0.00302428
      15       0.00088493     0.00089836    -0.00057033    -0.00118938
      16      -0.00006509    -0.00015951    -0.00000804     0.00004006
      17       0.00004801    -0.00001314    -0.00035941    -0.00039836
      18      -0.00011995    -0.00016312    -0.00008446     0.00012872
      19      -0.00015649     0.00033677    -0.00009990     0.00029398
      20       0.00024018    -0.00027788     0.00045338    -0.00025191

               Column   9     Column  10     Column  11     Column  12
       9       0.00530842
      10       0.00065015     0.00421823
      11      -0.00074255    -0.00042365     0.00439796
      12      -0.00239028     0.00007962     0.00013551     0.00253596
      13      -0.00065407     0.00130363    -0.00011953    -0.00001709
      14      -0.00070777    -0.00088679    -0.00032999    -0.00016039
      15       0.00157316     0.00057033    -0.00074237    -0.00040608
      16      -0.00002596    -0.00039610     0.00013329     0.00000602
      17      -0.00017889    -0.00027363    -0.00094965     0.00035947
      18       0.00037093    -0.00084614     0.00051532    -0.00040237
      19       0.00047014     0.00039024     0.00017913    -0.00062356
      20      -0.00040129     0.00009083     0.00054589     0.00048641

               Column  13     Column  14     Column  15     Column  16
      13       0.00120969
      14       0.00035749     0.00158332
      15      -0.00029212    -0.00054256     0.00112617
      16      -0.00006984     0.00004336    -0.00004169     0.00039338
      17      -0.00013809     0.00004003     0.00007544     0.00000863
      18      -0.00060550    -0.00000457    -0.00000205     0.00008847
      19       0.00024337    -0.00023421     0.00009992    -0.00004459
      20      -0.00001347    -0.00021988    -0.00014314     0.00000589

               Column  17     Column  18     Column  19     Column  20
      17       0.00060826
      18      -0.00012421     0.00067315
      19      -0.00022301    -0.00000228     0.00043423
      20      -0.00006721    -0.00002337    -0.00007133     0.00038122

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7        eno   8
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    8   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno   9        eno  10        eno  11        eno  12        eno  13        eno  14        eno  15        eno  16
 emo    9   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   10   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   11   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   12   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   13   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   14   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   15   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   16   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno  17        eno  18        eno  19        eno  20
 emo   17   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   18   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   19   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   20   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   1.9995603      1.9841384      1.9693367      0.0194738      0.0110159
   0.0060620      0.0052900      0.0011300      0.0008418      0.0005707
   0.0004725      0.0003663      0.0003554      0.0001563      0.0001087
   0.0000690      0.0000561      0.0000320      0.0000081      0.0000004

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7         no   8
  mo    1   0.999936E+00   0.106994E-01   0.346090E-02   0.152561E-03   0.514819E-03  -0.224206E-03   0.611072E-04  -0.962201E-04
  mo    2  -0.971684E-02   0.976986E+00  -0.212963E+00   0.510048E-02  -0.361148E-02   0.739312E-04   0.442877E-03  -0.665081E-03
  mo    3  -0.565494E-02   0.212887E+00   0.976955E+00  -0.694662E-02  -0.814878E-02   0.421880E-02   0.192268E-02   0.141542E-02
  mo    4  -0.117448E-03   0.209756E-02   0.644550E-02   0.193885E+00   0.150159E+00  -0.110938E+00   0.521444E-01  -0.786196E-01
  mo    5   0.110815E-03   0.853872E-03  -0.593910E-02  -0.563400E+00  -0.895619E-01   0.652847E-01  -0.593533E-01   0.712762E-01
  mo    6  -0.134819E-03   0.299056E-02   0.151452E-02  -0.840835E-01   0.431384E+00   0.245538E+00   0.110378E+00   0.208608E+00
  mo    7   0.248899E-03  -0.458495E-02  -0.723918E-02   0.599094E-02  -0.377933E+00   0.361358E+00  -0.751419E-01   0.193379E+00
  mo    8  -0.102755E-03  -0.280480E-02   0.306142E-02   0.714304E+00   0.100326E+00   0.202027E+00  -0.187115E-02   0.200210E+00
  mo    9  -0.213685E-03   0.404550E-02   0.363074E-02  -0.151088E+00   0.565830E+00   0.109209E+00  -0.432651E+00   0.346423E+00
  mo   10   0.674921E-04   0.460869E-03  -0.303194E-02  -0.143489E+00   0.140435E+00   0.643279E+00   0.438139E+00  -0.291417E-01
  mo   11   0.323255E-03  -0.386667E-03   0.157576E-03   0.899811E-01  -0.339159E+00   0.438513E+00  -0.579363E+00   0.300765E-01
  mo   12   0.303373E-03   0.411476E-03  -0.275410E-02  -0.980860E-01  -0.357068E+00  -0.730145E-01   0.282314E+00   0.573719E+00
  mo   13  -0.529243E-06  -0.509077E-03   0.817753E-03   0.865766E-01   0.143012E-01   0.243062E+00   0.288345E+00  -0.200945E+00
  mo   14  -0.129887E-03  -0.141373E-02   0.295887E-02   0.217623E+00   0.187100E-01  -0.124654E+00   0.104379E+00   0.356256E+00
  mo   15  -0.150494E-03   0.334101E-03  -0.342842E-02  -0.102821E+00   0.182400E+00   0.105297E-01  -0.414577E-01   0.193369E+00
  mo   16   0.587449E-05  -0.446247E-05   0.309160E-03   0.826557E-02  -0.175318E-01  -0.482511E-01  -0.574986E-01   0.254264E-01
  mo   17  -0.120458E-03  -0.168130E-03  -0.102111E-02  -0.186743E-01   0.162876E-01  -0.169152E+00   0.135870E+00   0.204460E+00
  mo   18  -0.273245E-04   0.704664E-04  -0.155590E-03   0.152766E-01   0.423577E-02  -0.815133E-01  -0.247429E+00  -0.107956E-01
  mo   19  -0.749087E-04   0.174795E-03  -0.300372E-03   0.109487E-01   0.705886E-01   0.112830E+00  -0.492532E-01  -0.399062E+00
  mo   20   0.423017E-03   0.105484E-02  -0.406381E-03  -0.157230E-01  -0.924685E-01   0.576765E-01  -0.153556E-01   0.101571E+00

                no   9         no  10         no  11         no  12         no  13         no  14         no  15         no  16
  mo    1  -0.470758E-04   0.346503E-04  -0.325206E-03   0.174563E-03   0.189709E-03   0.307785E-04  -0.964761E-04   0.480034E-04
  mo    2  -0.985793E-03   0.266605E-03  -0.232173E-02   0.108120E-02   0.129082E-02  -0.282589E-03   0.426264E-03  -0.713279E-03
  mo    3   0.255340E-02  -0.591636E-03  -0.484001E-02   0.265159E-02   0.188011E-02   0.209104E-02   0.236271E-02  -0.228444E-02
  mo    4   0.139380E+00  -0.458292E-01   0.155818E+00  -0.891768E-01  -0.117446E+00  -0.108059E+00  -0.163213E+00   0.125736E+00
  mo    5  -0.386534E+00   0.561621E-01  -0.129268E+00   0.268763E-01   0.249815E-01   0.181863E+00   0.278993E+00  -0.225534E-01
  mo    6  -0.165281E+00  -0.319920E+00   0.247012E+00   0.127829E+00  -0.334662E-01   0.784906E-01  -0.357841E-01  -0.137857E-02
  mo    7   0.102467E+00  -0.191644E-01  -0.212654E+00   0.219588E+00   0.255774E+00   0.505932E-01  -0.679462E-01  -0.272234E+00
  mo    8  -0.318570E-01   0.294847E-03  -0.668756E-01   0.362482E-01   0.460686E-01   0.187723E+00   0.154371E+00  -0.874769E-01
  mo    9  -0.916867E-03   0.298336E+00   0.119438E+00  -0.109651E+00   0.110733E+00   0.600084E-01  -0.197401E+00  -0.173536E-01
  mo   10   0.952019E-01  -0.227835E+00  -0.888907E-01   0.177151E-02  -0.562432E-01  -0.182948E+00  -0.567544E-01  -0.378235E-01
  mo   11  -0.319845E-01   0.521864E-01   0.144486E+00  -0.590493E-01  -0.953304E-02  -0.126447E+00  -0.211371E-02   0.259512E+00
  mo   12   0.146738E+00   0.801054E-01   0.201192E+00  -0.160057E+00  -0.212397E+00   0.326052E+00  -0.410538E+00   0.112213E+00
  mo   13  -0.194713E+00   0.491396E+00   0.106182E+00   0.570290E-01  -0.412810E-01   0.255793E+00   0.171958E+00   0.569709E+00
  mo   14  -0.567902E+00   0.559768E-01  -0.337953E+00  -0.760167E-01  -0.101620E+00  -0.205253E-01   0.140760E+00  -0.150302E+00
  mo   15   0.573250E+00   0.196778E+00  -0.537889E+00   0.575789E-01  -0.275438E+00   0.571916E-01   0.300385E+00   0.120420E+00
  mo   16  -0.554667E-01   0.188490E+00   0.204125E+00   0.811120E+00  -0.436372E+00  -0.111964E+00  -0.626387E-01  -0.163149E+00
  mo   17   0.208344E+00  -0.199746E-01   0.227166E+00   0.334765E+00   0.669376E+00   0.136559E+00   0.288980E+00   0.972765E-01
  mo   18  -0.263202E-01  -0.632662E+00  -0.116182E+00   0.159569E+00  -0.191993E+00   0.434367E+00   0.591887E-01   0.376351E+00
  mo   19   0.610203E-01   0.141894E+00  -0.874339E-02  -0.356992E-01  -0.932890E-02   0.679365E+00  -0.162073E+00  -0.443885E+00
  mo   20   0.144099E+00  -0.482528E-01   0.493045E+00  -0.274309E+00  -0.304886E+00   0.373725E-01   0.631062E+00  -0.289526E+00

                no  17         no  18         no  19         no  20
  mo    1  -0.512955E-04  -0.199920E-04  -0.830097E-05   0.629264E-06
  mo    2   0.893466E-03   0.766273E-03  -0.324711E-06  -0.177988E-03
  mo    3   0.627977E-03   0.225888E-02   0.209564E-02  -0.130227E-02
  mo    4  -0.614203E-01  -0.128760E+00  -0.257999E+00   0.842624E+00
  mo    5  -0.184854E+00  -0.101643E+00   0.385772E+00   0.431620E+00
  mo    6  -0.499088E+00   0.429953E+00  -0.192325E+00  -0.656860E-01
  mo    7   0.293768E+00   0.470365E+00  -0.220284E+00   0.276319E+00
  mo    8  -0.993974E-01  -0.286787E-01   0.547212E+00   0.113999E+00
  mo    9   0.402156E+00  -0.650127E-01   0.374825E-01   0.839434E-02
  mo   10   0.205568E+00  -0.446025E+00   0.285432E-01  -0.297236E-01
  mo   11  -0.348741E+00  -0.273617E+00  -0.187326E+00  -0.699763E-01
  mo   12  -0.757070E-01  -0.877930E-01   0.846628E-01  -0.384588E-01
  mo   13   0.177736E+00   0.213742E+00  -0.131296E+00  -0.970246E-03
  mo   14   0.318486E-01  -0.244986E+00  -0.487091E+00  -0.429850E-01
  mo   15  -0.243731E+00   0.251727E-01  -0.114080E+00  -0.507740E-02
  mo   16   0.518224E-01  -0.132470E+00   0.377384E-01   0.442012E-03
  mo   17  -0.708474E-01  -0.330454E+00  -0.170481E+00  -0.791557E-02
  mo   18   0.339893E+00  -0.612622E-01  -0.431001E-01  -0.591392E-02
  mo   19  -0.157474E+00  -0.202290E+00  -0.209800E+00  -0.249275E-01
  mo   20   0.213091E+00   0.515727E-01  -0.106327E+00   0.100447E-01
  eigenvectors of nos in ao-basis, column    1
  0.9975019E+00 -0.1489757E-01  0.3355435E-02  0.4267103E-02 -0.7860249E-02  0.3731792E-02  0.4435908E-03  0.1028471E-04
  0.3398544E-05  0.1089693E-04 -0.2586730E-03  0.2153755E-04  0.5464486E-04  0.7021598E-03 -0.1420387E-02  0.4918676E-04
  0.4668458E-03  0.2380375E-03 -0.5327697E-03 -0.5345590E-03
  eigenvectors of nos in ao-basis, column    2
  0.2969613E-02  0.9124635E+00 -0.2992721E-02 -0.1329314E+00  0.4565596E-01  0.1022795E-01  0.7474970E-01  0.1589739E-03
  0.2545509E-02 -0.1581922E-02 -0.2567220E-02 -0.7771256E-03 -0.1740193E-02  0.2568500E+00 -0.1113497E+00  0.3063855E-02
  0.2604725E-01  0.1918810E-01  0.1614976E-01  0.1207693E-01
  eigenvectors of nos in ao-basis, column    3
  0.9024926E-02  0.1080293E+00  0.3531281E-02  0.1550134E+00  0.8352126E+00 -0.2689813E-01 -0.2853116E-01 -0.1608406E-02
 -0.2680102E-02 -0.5367377E-02  0.1952258E-02 -0.8009265E-04  0.2671208E-02 -0.3984377E+00  0.1696998E+00  0.5815278E-02
 -0.2996388E-01 -0.1605104E-02 -0.1249651E-02  0.2914287E-01
  eigenvectors of nos in ao-basis, column    4
 -0.4321839E-01 -0.6092842E+00 -0.6245760E-01  0.3726176E-02  0.1322329E+01 -0.2016264E+00 -0.6954153E+00  0.3386748E-02
  0.1557537E-01  0.8598024E-02  0.2577324E-01  0.2137912E-02  0.3611720E-02  0.9576642E+00 -0.4177480E+00 -0.8297760E-02
  0.5946108E-01  0.1024784E-01  0.2998841E-02 -0.3704969E-01
  eigenvectors of nos in ao-basis, column    5
  0.1512204E-01 -0.1072727E+01 -0.4387394E+00  0.1099554E+01 -0.8227662E+00  0.3817486E+00  0.8216551E+00 -0.5080912E-02
  0.3180773E-01 -0.2326211E-01  0.4190228E-01 -0.2402230E-02 -0.2998716E-02  0.4369803E+00 -0.2463513E+00  0.2009660E-01
  0.2573982E-01  0.7847429E-01  0.1413787E-01  0.5688228E-01
  eigenvectors of nos in ao-basis, column    6
 -0.1050722E-01  0.4831073E+00  0.2536581E+00 -0.9456086E+00 -0.7482526E-01  0.3349411E-01  0.1444712E+00 -0.3528896E-01
  0.1153116E+00 -0.8782002E-01  0.2529298E+00 -0.1431512E-01 -0.2082287E-01  0.3108478E+00 -0.1484045E+00 -0.1169378E-01
 -0.3239430E-01  0.8240869E-01 -0.3416272E-01  0.2195746E-01
  eigenvectors of nos in ao-basis, column    7
 -0.2269424E-02 -0.1389396E+00 -0.5776750E-01  0.2234952E+00  0.2739877E+00 -0.1191640E+00 -0.4003571E+00 -0.8665294E-01
 -0.8420278E-01 -0.1767016E+00 -0.1647341E+00 -0.9699690E-02  0.1359481E-01 -0.8177789E-01  0.6173001E-01  0.5516705E-02
 -0.5157325E-01  0.4021031E-01 -0.3579287E-02  0.3956056E-01
  eigenvectors of nos in ao-basis, column    8
 -0.1250814E+00  0.2145633E-01  0.4337268E+00 -0.1918617E+01 -0.2240836E+00  0.3077005E+00  0.9771588E+00  0.1974741E-01
 -0.2458847E+00  0.1515246E-01 -0.2148208E+00 -0.3396912E-01 -0.8784192E-01  0.2109847E+01 -0.1006647E+01 -0.2598029E-02
  0.6965825E-01  0.1637103E+00  0.6725418E-01  0.6706192E-01
  eigenvectors of nos in ao-basis, column    9
 -0.2185228E+00 -0.2822558E+00  0.4133358E+00  0.2914244E+00 -0.2531832E+01  0.2378405E+01  0.4472726E+00 -0.2120757E-01
 -0.4364885E-01  0.1256935E-01  0.3795861E-01  0.8969964E-02  0.4133042E-01 -0.3992801E-01  0.1335784E+00 -0.3913635E-01
 -0.1687288E+00 -0.1012313E+00 -0.5716758E-01 -0.1443800E+00
  eigenvectors of nos in ao-basis, column   10
  0.1593843E+00  0.3562734E+00 -0.1913265E+00 -0.6430676E+00 -0.1584510E+00  0.2015356E+00  0.8281178E+00 -0.2109928E+00
  0.3115396E-01  0.1339964E-01  0.1567812E-01  0.8772453E-01 -0.1172184E+00  0.6588266E+00 -0.3592111E+00 -0.1088884E-01
  0.3300592E+00 -0.2878970E+00  0.9376423E-01 -0.1768889E+00
  eigenvectors of nos in ao-basis, column   11
 -0.1380668E+01 -0.2891735E+01  0.2005665E+01  0.1916824E+01  0.8283824E+00 -0.1103731E+01  0.6093206E-01 -0.3294955E-01
 -0.1031832E+00  0.1689308E-01  0.1383111E+00  0.1418573E+00  0.1631678E+00 -0.2263060E+00  0.2220855E+00 -0.3058587E-02
 -0.1474203E+00 -0.1951971E+00 -0.2361207E-01  0.9223421E-01
  eigenvectors of nos in ao-basis, column   12
  0.8834375E+00  0.1946259E+01 -0.1242867E+01 -0.8407530E+00 -0.1847853E+00  0.2087555E+00 -0.3266501E+00  0.7891160E-01
 -0.1479731E+00 -0.4180684E-01  0.1159251E+00  0.3098978E+00 -0.7170454E-01 -0.2235450E+00  0.2528426E-01 -0.1337370E-01
 -0.6132948E-01  0.1458820E-01 -0.1619438E+00  0.4835741E-01
  eigenvectors of nos in ao-basis, column   13
  0.1011232E+01  0.2241778E+01 -0.1415119E+01 -0.7181937E+00  0.2411195E+00 -0.3824901E+00 -0.1037491E-01 -0.9724830E-01
 -0.2549843E+00  0.8112012E-01  0.2299127E+00 -0.7163450E-01  0.3154683E+00 -0.1895068E+00 -0.1044701E+00 -0.2218005E-01
 -0.9858125E-01 -0.6882746E-01 -0.1500245E+00 -0.1366220E+00
  eigenvectors of nos in ao-basis, column   14
  0.3028563E+00  0.8518344E+00 -0.1799604E+00 -0.3169158E+01 -0.7865790E+00  0.1248450E+01  0.1076503E+01  0.9127510E-01
  0.3115404E+00 -0.1707669E+00 -0.4952580E+00  0.6582743E-01  0.3263033E+00  0.2756680E+01 -0.1270847E+01  0.2205642E-01
  0.4409468E+00 -0.1122995E+00  0.1799617E+00  0.2338710E+00
  eigenvectors of nos in ao-basis, column   15
 -0.2226985E+01 -0.5912622E+01  0.2483080E+01  0.3694876E+01 -0.1225439E+01  0.2400510E+01 -0.2949115E+00  0.2122842E-01
 -0.1632586E+00 -0.7373856E-01 -0.4810258E-01  0.1684755E-01  0.1243248E+00  0.1659297E+01 -0.1569979E+01  0.1166411E+00
  0.1025946E+01  0.5011686E+00 -0.1708258E+00  0.7327474E-02
  eigenvectors of nos in ao-basis, column   16
  0.1092890E+01  0.2959840E+01 -0.1142662E+01 -0.3589235E+01 -0.5286034E-01 -0.7820937E-01  0.4807847E+00  0.1512405E+00
 -0.2261854E+00 -0.3108080E+00  0.4826526E+00 -0.1338398E+00 -0.7755491E-01 -0.6930535E+00  0.1291396E+01 -0.6366481E-01
  0.3016533E+00 -0.8381771E+00  0.3980384E+00  0.4064110E+00
  eigenvectors of nos in ao-basis, column   17
 -0.9889124E+00 -0.2699725E+01  0.9682284E+00  0.5094962E+01  0.6310765E+00 -0.9904527E+00  0.8523084E+00  0.1328176E+00
 -0.2967258E-01 -0.1924032E+00  0.7095007E-01 -0.4095896E-02  0.1400983E-01 -0.1758093E+00 -0.9700534E+00 -0.3580306E-01
  0.2592699E-01 -0.1002068E+00 -0.4776418E+00 -0.1198620E+01
  eigenvectors of nos in ao-basis, column   18
 -0.6060573E+00 -0.1725645E+01  0.4245225E+00  0.6406445E+01  0.1956265E+00 -0.5436343E+00 -0.1366662E+01 -0.1965792E-02
  0.9846149E-02 -0.4722778E-01  0.2766170E+00 -0.1199398E+00 -0.1721958E+00 -0.2147804E+01 -0.2294207E+00  0.3373988E-02
 -0.1730770E+00 -0.7471651E+00 -0.1281957E+01 -0.2237591E+00
  eigenvectors of nos in ao-basis, column   19
  0.1879620E+00  0.5420219E+00 -0.3079953E+00  0.3233292E+01  0.3503414E+00 -0.1177776E+01 -0.1294195E+01  0.5766488E-02
 -0.5801862E-01  0.2690263E-01  0.4315534E+00 -0.4451747E-01 -0.1558115E+00 -0.2379844E+01 -0.1723591E+00  0.2774974E+00
 -0.1055412E+01 -0.6603372E+00  0.3827697E+00  0.6307966E-01
  eigenvectors of nos in ao-basis, column   20
 -0.1562809E+00 -0.4831171E+00  0.6468488E-01  0.2392424E+01  0.1107441E-01 -0.6437365E-01 -0.5776643E+00 -0.2276442E-03
 -0.1200013E-01  0.2821795E-02  0.6412435E-01 -0.5399455E-02 -0.1517745E-01 -0.3177303E+00 -0.1253046E+01  0.8903768E+00
 -0.1256035E+00 -0.9273800E-01 -0.3117911E+00 -0.2513822E+00


*****   symmetry block SYM2   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       1.96822575
       2       0.00197378     0.00062256
       3      -0.01615999    -0.00189568     0.00717806
       4      -0.00224037     0.00074864    -0.00143957     0.00145756
       5       0.00204421     0.00051376    -0.00184389     0.00044250
       6      -0.01514240    -0.00194358     0.00847411    -0.00087267
       7      -0.00337115    -0.00126793     0.00291258    -0.00224536
       8       0.00144470     0.00015629    -0.00051847     0.00026188
       9       0.00921810     0.00036790    -0.00229242    -0.00024061
      10       0.00528304     0.00059172    -0.00174622     0.00089370
      11      -0.00537462    -0.00023383     0.00064350    -0.00039672
      12       0.00093817     0.00003671    -0.00008689     0.00009635
      13      -0.00389632    -0.00000999     0.00030393     0.00015574
      14      -0.00130189    -0.00004052    -0.00007296    -0.00019845

               Column   5     Column   6     Column   7     Column   8
       5       0.00051905
       6      -0.00213266     0.01107563
       7      -0.00086668     0.00232588     0.00383396
       8       0.00004706    -0.00067398    -0.00038666     0.00055603
       9       0.00059549    -0.00361860     0.00020935     0.00025905
      10       0.00047508    -0.00192830    -0.00189188     0.00018455
      11      -0.00022182     0.00064611     0.00080815    -0.00013183
      12      -0.00002317    -0.00007481    -0.00013934     0.00026158
      13      -0.00007988     0.00072897    -0.00028647    -0.00005040
      14       0.00000641    -0.00028923     0.00044886     0.00007238

               Column   9     Column  10     Column  11     Column  12
       9       0.00206077
      10       0.00005015     0.00149763
      11      -0.00024599    -0.00043257     0.00059961
      12       0.00001904     0.00007507    -0.00002341     0.00032449
      13      -0.00073743     0.00007982     0.00000622    -0.00000845
      14       0.00043126    -0.00043061     0.00004845     0.00000903

               Column  13     Column  14
      13       0.00053991
      14      -0.00020247     0.00034740

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7        eno   8
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    8   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno   9        eno  10        eno  11        eno  12        eno  13        eno  14
 emo    9   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   10   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   11   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   12   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   13   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   14   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   1.9685727      0.0211315      0.0058185      0.0010710      0.0008219
   0.0006427      0.0003902      0.0001634      0.0000985      0.0000945
   0.0000181      0.0000099      0.0000052      0.0000002

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7         no   8
  mo    1   0.999911E+00   0.123891E-01  -0.214103E-02  -0.128366E-02  -0.211291E-02   0.628739E-03   0.117626E-02  -0.170852E-02
  mo    2   0.102162E-02  -0.149564E+00  -0.131611E+00   0.177356E-01  -0.218750E+00  -0.396869E-01   0.645036E-01   0.750491E-01
  mo    3  -0.828500E-02   0.569771E+00   0.182535E-01   0.213826E+00   0.357992E+00  -0.353576E-01   0.685118E-01  -0.241421E+00
  mo    4  -0.112544E-02  -0.116415E+00  -0.415068E+00   0.257018E+00  -0.286129E+00  -0.431669E-01   0.190464E+00  -0.109336E+00
  mo    5   0.105806E-02  -0.148579E+00  -0.274639E-01  -0.293503E-01  -0.976458E-01  -0.194176E+00  -0.568803E-01   0.157104E+00
  mo    6  -0.778770E-02   0.699126E+00  -0.309014E+00   0.142496E+00  -0.193700E+00  -0.102932E+00  -0.176611E-02   0.194480E+00
  mo    7  -0.174028E-02   0.238568E+00   0.662963E+00  -0.204659E+00   0.133271E-01   0.109836E+00  -0.519858E-01   0.875136E-01
  mo    8   0.740321E-03  -0.483354E-01  -0.298214E-01   0.361432E+00   0.342093E-01   0.703391E+00  -0.863690E-01  -0.401957E+00
  mo    9   0.471269E-02  -0.201893E+00   0.359171E+00   0.552702E+00   0.117280E+00  -0.232078E+00   0.206289E-01  -0.108748E-01
  mo   10   0.270308E-02  -0.154363E+00  -0.295662E+00  -0.131347E+00   0.725266E+00   0.215357E-01   0.187904E+00  -0.181392E-01
  mo   11  -0.273806E-02   0.575465E-01   0.117087E+00  -0.316270E+00  -0.222065E+00   0.223504E+00   0.785250E+00  -0.172761E+00
  mo   12   0.477629E-03  -0.800062E-02  -0.255526E-01   0.177688E+00   0.308853E-01   0.530839E+00   0.341892E-01   0.754435E+00
  mo   13  -0.198529E-02   0.341085E-01  -0.147335E+00  -0.411842E+00  -0.120919E+00   0.232637E+00  -0.535162E+00  -0.218175E+00
  mo   14  -0.659636E-03  -0.735654E-02   0.144626E+00   0.265093E+00  -0.298428E+00  -0.562505E-03  -0.393557E-01  -0.212903E+00

                no   9         no  10         no  11         no  12         no  13         no  14
  mo    1   0.467733E-04   0.177432E-02   0.117089E-02   0.135233E-02   0.160738E-02   0.307598E-03
  mo    2  -0.444181E-01  -0.120584E+00   0.318289E+00  -0.154208E+00  -0.177579E-01   0.871124E+00
  mo    3  -0.549756E-01   0.357797E+00   0.125512E+00   0.359137E+00   0.296281E+00   0.270730E+00
  mo    4  -0.183888E+00  -0.134086E-01   0.629956E+00   0.166609E+00   0.127945E+00  -0.375732E+00
  mo    5  -0.109931E+00   0.103755E+00  -0.190836E+00  -0.298268E+00   0.867245E+00  -0.283892E-01
  mo    6  -0.848450E-01  -0.182239E+00  -0.111061E+00  -0.479102E+00  -0.146043E+00  -0.762607E-01
  mo    7  -0.201553E-01  -0.273057E+00   0.545076E+00  -0.178617E+00   0.146579E+00  -0.116665E+00
  mo    8  -0.756611E-01  -0.338592E+00  -0.154392E+00  -0.154423E+00   0.176754E+00   0.434484E-01
  mo    9  -0.500243E+00   0.252176E+00  -0.198023E-01  -0.291874E+00  -0.238705E+00  -0.132697E-01
  mo   10   0.168597E+00   0.388988E-01   0.246061E+00  -0.463586E+00  -0.285852E-01  -0.563188E-01
  mo   11  -0.185570E+00   0.249343E+00  -0.144982E+00  -0.138180E+00  -0.221317E-01  -0.171626E-02
  mo   12   0.259535E-01   0.316810E+00   0.646571E-01   0.966944E-01   0.442424E-02  -0.568018E-02
  mo   13  -0.334931E+00   0.476460E+00   0.140769E+00  -0.187349E+00  -0.103013E+00  -0.718262E-02
  mo   14   0.714016E+00   0.411603E+00   0.102178E+00  -0.285624E+00  -0.210277E-01  -0.334592E-01
  eigenvectors of nos in ao-basis, column    1
  0.7599163E+00 -0.3243599E-01 -0.1376527E+00 -0.1581684E-01 -0.8206686E-02  0.2766907E-03 -0.3342332E-02 -0.5500475E+00
  0.1516692E+00 -0.9939641E-02 -0.2207328E-01 -0.3021010E-01 -0.2094361E-01 -0.2482416E-01
  eigenvectors of nos in ao-basis, column    2
 -0.1330981E+01  0.1109431E+00  0.5315507E+00 -0.3852477E-01 -0.5930766E-01  0.1582935E-04  0.3455272E-02 -0.1034143E+01
  0.4051936E+00 -0.1454683E-01 -0.5309811E-01 -0.3289573E-01 -0.8226276E-02  0.7160096E-04
  eigenvectors of nos in ao-basis, column    3
 -0.7403408E+00  0.3331537E+00  0.8706570E+00  0.2829448E+00  0.5412802E+00 -0.8261110E-02  0.1192683E-01  0.2372610E+00
 -0.8272175E-01 -0.3437159E-01  0.8878106E-01 -0.1404504E-01  0.3698537E-01  0.3185777E-01
  eigenvectors of nos in ao-basis, column    4
  0.1312639E+01 -0.1141221E+01 -0.1335427E+01  0.5240880E+00  0.4202222E+00  0.2543173E-01  0.8761818E-02 -0.1471748E+01
  0.3403145E+00  0.4168749E-01 -0.1175678E+00  0.1914178E+00 -0.1258379E+00 -0.9953969E-01
  eigenvectors of nos in ao-basis, column    5
  0.2068464E+01 -0.1836203E+01  0.3629747E+00 -0.2256360E+00 -0.2892849E+00 -0.2156367E-01  0.7956949E-01  0.1055882E+01
 -0.5881790E+00  0.3663053E-01  0.2062165E+00  0.1686555E+00  0.1185800E+00  0.2027667E+00
  eigenvectors of nos in ao-basis, column    6
 -0.6775241E+00  0.7314577E+00  0.1166558E+01 -0.1557209E+00 -0.1431992E+00  0.7249118E-01  0.2345060E+00  0.1048933E+01
 -0.6655587E-01 -0.4801060E-01 -0.2288530E+00  0.3986626E+00  0.1505955E+00  0.1301157E+00
  eigenvectors of nos in ao-basis, column    7
 -0.6916131E+00  0.1001115E+01 -0.2097544E-01  0.3660159E+00 -0.4360426E+00 -0.8176945E-01  0.4202282E+00  0.3333751E+00
 -0.5239460E+00  0.8418905E-01  0.1642815E+00  0.1182801E+00 -0.2058028E+00  0.9396551E-01
  eigenvectors of nos in ao-basis, column    8
  0.1295860E+01 -0.2178949E+01 -0.9933666E+00 -0.9756238E-01  0.7084465E+00  0.6864664E-01  0.3940371E+00 -0.2201309E+01
  0.1242435E+01 -0.1602056E+00 -0.1025844E+00 -0.8231823E+00 -0.1078835E+00 -0.2391599E+00
  eigenvectors of nos in ao-basis, column    9
  0.2139397E+00 -0.4876299E+00  0.1764606E+01  0.9621067E+00 -0.1429731E+01  0.3671664E-01 -0.1155081E+00  0.1940086E+01
 -0.1438317E+00 -0.4526800E-01 -0.1352578E+00 -0.2054587E+00  0.2536178E+00  0.3539112E+00
  eigenvectors of nos in ao-basis, column   10
 -0.1631431E+01  0.3217506E+01 -0.5443786E+00  0.1364187E+00 -0.1253780E+01  0.1031680E+00 -0.3736123E-01  0.2777113E+01
 -0.3444707E+01  0.3499158E+00  0.1154790E+01  0.3151746E+00 -0.4453211E+00 -0.9506769E-01
  eigenvectors of nos in ao-basis, column   11
  0.2360153E+00 -0.3501850E+00  0.3515815E+01  0.2914896E-01 -0.6015050E+00  0.3572522E-01 -0.7761395E-01  0.1803048E+01
  0.2200911E+01 -0.2538324E-01  0.3934738E+00  0.3717973E-01  0.3469895E+00  0.1804089E+01
  eigenvectors of nos in ao-basis, column   12
  0.5786049E+00 -0.1817621E+01 -0.2968726E+01 -0.2150614E+00  0.1639960E+01 -0.2105042E-01  0.1356781E+00 -0.3101420E+01
 -0.3315673E+01  0.4490491E+00 -0.1039620E+01 -0.1013081E+01 -0.1663388E+01 -0.4025865E-01
  eigenvectors of nos in ao-basis, column   13
  0.2441361E+00 -0.8252858E+00  0.2586167E+01  0.4702896E-01  0.1531321E+00 -0.1151359E-01  0.3478352E-01 -0.1243877E+00
  0.8742200E+01  0.2056742E+00 -0.4564001E+00 -0.9759296E-01  0.4570739E+01  0.1265962E+01
  eigenvectors of nos in ao-basis, column   14
 -0.8136527E-02  0.7017405E-03 -0.1475880E+01 -0.3976303E-01  0.2343108E+00 -0.4716706E-02  0.9733555E-02 -0.4095155E+00
 -0.2495064E+01  0.2873428E+01 -0.1560103E+00 -0.6292858E-01 -0.4398940E+00 -0.7083199E+00


*****   symmetry block SYM3   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       1.97123721
       2       0.01353781     0.00560978
       3       0.01309235     0.00615749     0.00861921
       4       0.00471092    -0.00322724    -0.00183848     0.00461469
       5       0.00027789     0.00031669     0.00149390     0.00129122
       6      -0.00729666     0.00212022     0.00310072    -0.00113514
       7       0.00000775     0.00001862    -0.00002200    -0.00004764
       8      -0.00079094     0.00018381     0.00004113    -0.00050139
       9       0.00014678    -0.00023011     0.00023610     0.00110652

               Column   5     Column   6     Column   7     Column   8
       5       0.00139351
       6       0.00048345     0.00188560
       7      -0.00003778    -0.00000313     0.00038091
       8      -0.00003994     0.00006586     0.00000430     0.00037883
       9       0.00077179    -0.00000164     0.00000468    -0.00014491

               Column   9
       9       0.00076178

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7        eno   8
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    8   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno   9
 emo    9   0.100000E+01

 occupation numbers of nos
   1.9714566      0.0157297      0.0053368      0.0007991      0.0006242
   0.0003942      0.0003622      0.0001206      0.0000580

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7         no   8
  mo    1   0.999944E+00  -0.690173E-02  -0.324252E-02   0.687207E-02   0.208043E-02   0.634998E-03   0.788028E-03  -0.566689E-03
  mo    2   0.689915E-02   0.576340E+00  -0.122299E+00  -0.458969E+00   0.116047E+00   0.988772E-01   0.134919E+00   0.111005E+00
  mo    3   0.668344E-02   0.700374E+00   0.369027E+00  -0.291578E-01  -0.304040E+00  -0.121053E+00  -0.172333E+00   0.909162E-02
  mo    4   0.237984E-02  -0.309354E+00   0.748713E+00  -0.124226E+00  -0.352872E+00  -0.979930E-02  -0.605524E-01   0.236125E+00
  mo    5   0.147918E-03   0.660385E-01   0.440800E+00   0.642848E-01   0.595726E+00  -0.166665E+00  -0.113119E+00  -0.621679E+00
  mo    6  -0.368789E-02   0.276540E+00   0.769658E-01   0.859952E+00   0.217657E-02   0.183606E+00   0.182908E+00   0.151380E+00
  mo    7   0.386473E-05   0.433229E-03  -0.125170E-01  -0.172692E-01   0.294321E-01   0.765599E+00  -0.638426E+00  -0.698771E-01
  mo    8  -0.401203E-03   0.204182E-01  -0.874894E-01   0.106160E+00   0.327430E+00  -0.472275E+00  -0.608215E+00   0.521020E+00
  mo    9   0.758974E-04  -0.175726E-01   0.283268E+00  -0.133413E+00   0.553713E+00   0.323728E+00   0.353082E+00   0.496079E+00

                no   9
  mo    1  -0.820154E-03
  mo    2   0.623222E+00
  mo    3  -0.485310E+00
  mo    4   0.379809E+00
  mo    5   0.123504E+00
  mo    6   0.296620E+00
  mo    7   0.813570E-02
  mo    8   0.949639E-01
  mo    9  -0.345676E+00
  eigenvectors of nos in ao-basis, column    1
  0.9200159E+00 -0.6361704E-02  0.4203848E-01 -0.4918987E-02 -0.1975459E-01  0.1135855E-02  0.3257562E-03  0.2302822E-01
  0.3943188E-01
  eigenvectors of nos in ao-basis, column    2
 -0.1650549E+01  0.5302938E+00  0.1279076E+01 -0.1591923E-01 -0.7123765E-01  0.1409447E-03  0.4826417E-03  0.6440928E-01
  0.8797664E-01
  eigenvectors of nos in ao-basis, column    3
 -0.2256567E+00  0.9736908E-01  0.3520598E+00  0.3437068E+00  0.7119381E+00 -0.3190197E-02 -0.5095041E-02 -0.6989500E-01
 -0.5154407E-01
  eigenvectors of nos in ao-basis, column    4
 -0.2668067E+01  0.2741084E+01  0.4263908E+00 -0.1548841E+00  0.3028959E-01  0.7801618E-02 -0.3491426E-03 -0.3162114E-01
 -0.2028736E+00
  eigenvectors of nos in ao-basis, column    5
 -0.2010118E+00  0.2420419E+00 -0.6505948E+00  0.5740503E+00 -0.4285383E-01  0.8114025E-01  0.2986981E-01  0.4966089E+00
  0.1240766E+00
  eigenvectors of nos in ao-basis, column    6
 -0.3742746E+00  0.4782679E+00 -0.2270763E+00  0.4532636E+00 -0.3507489E+00 -0.1402045E+00  0.8866757E-01 -0.5549943E-01
  0.1456928E+00
  eigenvectors of nos in ao-basis, column    7
 -0.3649686E+00  0.4772717E+00 -0.3486726E+00  0.5291233E+00 -0.3897955E+00 -0.6020324E-01 -0.1253725E+00  0.2035573E-01
  0.1777741E+00
  eigenvectors of nos in ao-basis, column    8
 -0.4580715E+00  0.6851813E+00  0.2938523E+00  0.5074782E+00 -0.8992771E+00  0.1248071E+00  0.2775917E-01 -0.8799840E+00
  0.1936966E+00
  eigenvectors of nos in ao-basis, column    9
 -0.8269653E+00  0.1325813E+01 -0.1787482E+01 -0.4263412E+00  0.5603083E+00  0.2846667E-02  0.1102059E-02 -0.5513790E-01
  0.1040622E+01


*****   symmetry block SYM4   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       0.00577868
       2      -0.00074309     0.00082811
       3       0.00035796    -0.00000627     0.00026586
       4       0.00015354    -0.00014950     0.00016794     0.00020920
       5      -0.00004957    -0.00013325    -0.00007156    -0.00001354

               Column   5
       5       0.00015398

               eno   1        eno   2        eno   3        eno   4        eno   5
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   0.0059163      0.0007778      0.0003964      0.0001042      0.0000412

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5
  mo    1   0.986835E+00   0.140364E+00  -0.651871E-01   0.261621E-01   0.389935E-01
  mo    2  -0.144989E+00   0.946040E+00   0.303707E-01   0.246545E+00   0.149262E+00
  mo    3   0.637120E-01   0.544589E-01   0.745856E+00   0.128989E+00  -0.648101E+00
  mo    4   0.322365E-01  -0.189626E+00   0.610051E+00   0.226797E+00   0.734441E+00
  mo    5  -0.600292E-02  -0.215365E+00  -0.257612E+00   0.932982E+00  -0.129467E+00
  eigenvectors of nos in ao-basis, column    1
 -0.3104350E+00 -0.6781980E+00  0.4203661E-01  0.1105327E+00  0.9721870E-01
  eigenvectors of nos in ao-basis, column    2
 -0.6067536E+00 -0.1213516E+00 -0.3208104E+00 -0.4917714E+00 -0.1702648E+00
  eigenvectors of nos in ao-basis, column    3
  0.5723840E+00 -0.4038638E+00 -0.7789689E+00 -0.4950654E-01  0.3316244E+00
  eigenvectors of nos in ao-basis, column    4
  0.7042385E+00 -0.1197721E+01  0.6087417E+00 -0.8128380E+00  0.6565548E-01
  eigenvectors of nos in ao-basis, column    5
  0.3568151E+00 -0.3436826E+00 -0.2476204E+00  0.3915389E+00 -0.1148798E+01


 total number of electrons =   10.0000000000


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***


 cosurf_and_pot_on_the_outer_cavity
  2.073972598936  4.718525890344  2.111755307006 -0.014280160732
  2.714866756337  0.592484285675  5.279540167909 -0.021944494273
  0.977151244524  1.101353062742  5.852696389317 -0.025899747041
  3.925560684092  1.975404862085  3.752294365964 -0.013878786191
  3.267362519836  3.661361660599  2.823197971886 -0.013284952271
  4.794520096155  0.621084894014  2.985384674181 -0.006589357712
  0.173077949460  3.085961949671  5.073991710630 -0.024836274113
  1.286466029251  4.255380848585  3.676948920622 -0.020739463727
  3.303607685031  4.190688084329  0.874977219987 -0.004578950578
  0.163627211063  3.046854928202 -3.659275392660  0.016666466022
  0.195480392480  5.211736928518  1.860942604097 -0.015766515637
  1.503220315915  5.104908022693  0.297567705890 -0.007729747369
  0.760142032813  4.659957506362 -1.769964770589  0.002747508712
  2.313736498748  2.694082990980  4.707229619388 -0.021595325532
  4.352554947409  2.683369878192  2.255591816868 -0.005910081754
  1.877149624159  4.739707996010 -0.863268981216 -0.000423875034
  5.557452224045  1.320533821773  0.975930811333  0.006310847202
  3.248540195407  3.839867994436 -1.961916173837  0.010440670207
  1.190922084255  1.050203102491 -4.776522637080  0.022774267663
  5.319956602612  1.934458909776 -1.663437951255  0.015492294703
  3.646016883720  1.895722139538 -3.839536654111  0.019881641863
  4.504250248226  3.320493842536 -0.025911658296  0.005814254384
  1.783017729142  3.097585149616 -3.666342687430  0.016588910564
 end_of_cosurf_and_pot_on_the_outer_cavity


 **************************************************
 ***Final cosmo calculation for ground state*******
 **************************************************

  ediel before cosmo(4 =  -0.00267626814
  elast before cosmo(4 =  -76.3271734


 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  9  1    -76.3378784647 -9.3346E+00  3.7153E-08  3.9909E-04  1.0000E-03
 mr-sdci #  9  2    -75.3186137815 -9.1780E+00  0.0000E+00  6.4753E-01  1.0000E-04
 mr-sdci #  9  3    -74.5848072727 -9.3282E+00  0.0000E+00  1.0026E+00  1.0000E-04

####################CIUDGINFO####################

   ci vector at position   1 energy=  -76.337878464675

################END OF CIUDGINFO################


    1 of the   4 expansion vectors are transformed.

          transformed tciref transformation matrix  block   1

               ref   1        ref   2        ref   3
  ci:   1     0.97232334    -0.00531155    -0.06495217
    1 of the   3 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
 maximum overlap with reference  1(overlap=  0.972323335)
 ============== writing to civout ======================
 1 32768 6339
 6 5 6
 #cirefvfl= 1 #civfl= 1 method= 0 last= 1 overlap= 97%(  0.972323335)
 Hermit Integral Program : SIFS version  hochtor2        Thu Oct  7 15:12:41 2004
  cidrt_title                                                                    
  title                                                                          
 mofmt: formatted orbitals label=morbl   hochtor2        Thu Oct  7 15:12:44 2004
 SIFS file created by program tran.      hochtor2        Thu Oct  7 15:12:44 2004
 energy computed by program ciudg.       hochtor2        Thu Oct  7 15:13:03 2004
 energy of type -1=  9.31791392
 energy of type -1026= -76.3378785
 energy of type -2055=  0.000399087182
 energy of type -2056= -9.33458011
 energy of type -2057=  3.71531965E-08

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.97232334    -0.00531155    -0.06495217    -0.05954075     0.08491072     0.00115004     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.97232334    -0.00531155    -0.06495217     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 ==================================================

information on vector: 1from unit 11 written to unit 16filename civout                                                      


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -76.3378784647

                                                       internal orbitals

                                          level       1    2    3    4    5

                                          orbital     1    2    3   21   35

                                         symmetry   a1   a1   a1   b1   b2 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1  0.972323                        +-   +-   +-   +-   +- 
 y   1  1      11  0.012243              2( b1 )   +-   +-   +-    -   +- 
 y   1  1      14  0.011847              5( b1 )   +-   +-   +-    -   +- 
 y   1  1      26 -0.011629              4( a1 )   +-   +-    -   +-   +- 
 x   1  1      75 -0.016918    2( b1 )   1( b2 )   +-   +-   +-    -    - 
 x   1  1      78 -0.015402    5( b1 )   1( b2 )   +-   +-   +-    -    - 
 x   1  1      88 -0.013790    2( b1 )   2( b2 )   +-   +-   +-    -    - 
 x   1  1      89  0.011206    3( b1 )   2( b2 )   +-   +-   +-    -    - 
 x   1  1      91 -0.010650    5( b1 )   2( b2 )   +-   +-   +-    -    - 
 x   1  1      92 -0.017070    6( b1 )   2( b2 )   +-   +-   +-    -    - 
 x   1  1     104  0.016484    5( b1 )   3( b2 )   +-   +-   +-    -    - 
 x   1  1     182  0.018149    5( a1 )   1( a2 )   +-   +-   +-    -    - 
 x   1  1     183  0.013283    6( a1 )   1( a2 )   +-   +-   +-    -    - 
 x   1  1     184  0.011264    7( a1 )   1( a2 )   +-   +-   +-    -    - 
 x   1  1     185  0.018423    8( a1 )   1( a2 )   +-   +-   +-    -    - 
 x   1  1     186 -0.013212    9( a1 )   1( a2 )   +-   +-   +-    -    - 
 x   1  1     264  0.014637    2( a1 )   1( b2 )   +-   +-    -   +-    - 
 x   1  1     267 -0.015869    5( a1 )   1( b2 )   +-   +-    -   +-    - 
 x   1  1     281  0.011866    2( a1 )   2( b2 )   +-   +-    -   +-    - 
 x   1  1     284 -0.014171    5( a1 )   2( b2 )   +-   +-    -   +-    - 
 x   1  1     285  0.020407    6( a1 )   2( b2 )   +-   +-    -   +-    - 
 x   1  1     301  0.012920    5( a1 )   3( b2 )   +-   +-    -   +-    - 
 x   1  1     303 -0.014293    7( a1 )   3( b2 )   +-   +-    -   +-    - 
 x   1  1     304  0.018309    8( a1 )   3( b2 )   +-   +-    -   +-    - 
 x   1  1     403  0.015752    5( b1 )   1( a2 )   +-   +-    -   +-    - 
 x   1  1     483 -0.016165    3( a1 )   2( b1 )   +-   +-    -    -   +- 
 x   1  1     486 -0.011135    6( a1 )   2( b1 )   +-   +-    -    -   +- 
 x   1  1     487 -0.014192    7( a1 )   2( b1 )   +-   +-    -    -   +- 
 x   1  1     534 -0.017936    3( a1 )   5( b1 )   +-   +-    -    -   +- 
 x   1  1     537 -0.010020    6( a1 )   5( b1 )   +-   +-    -    -   +- 
 x   1  1     538 -0.024027    7( a1 )   5( b1 )   +-   +-    -    -   +- 
 x   1  1     553  0.013030    5( a1 )   6( b1 )   +-   +-    -    -   +- 
 x   1  1     554 -0.010411    6( a1 )   6( b1 )   +-   +-    -    -   +- 
 x   1  1     589  0.013064    7( a1 )   8( b1 )   +-   +-    -    -   +- 
 x   1  1     687  0.013143    3( b2 )   1( a2 )   +-   +-    -    -   +- 
 x   1  1     749  0.011660    8( a1 )   2( b2 )   +-    -   +-   +-    - 
 w   1  1    2440 -0.015718    8( a1 )   8( a1 )   +-   +-   +-   +-      
 w   1  1    2649 -0.017777    1( b2 )   1( b2 )   +-   +-   +-   +-      
 w   1  1    2650 -0.026000    1( b2 )   2( b2 )   +-   +-   +-   +-      
 w   1  1    2651 -0.030319    2( b2 )   2( b2 )   +-   +-   +-   +-      
 w   1  1    2652  0.011936    1( b2 )   3( b2 )   +-   +-   +-   +-      
 w   1  1    2654 -0.014563    3( b2 )   3( b2 )   +-   +-   +-   +-      
 w   1  1    2660 -0.013910    2( b2 )   5( b2 )   +-   +-   +-   +-      
 w   1  1    2663 -0.011647    5( b2 )   5( b2 )   +-   +-   +-   +-      
 w   1  1    2685 -0.020890    1( a2 )   1( a2 )   +-   +-   +-   +-      
 w   1  1    2701  0.016048    2( b1 )   1( b2 )   +-   +-   +-   +     - 
 w   1  1    2704  0.015299    5( b1 )   1( b2 )   +-   +-   +-   +     - 
 w   1  1    2714  0.013822    2( b1 )   2( b2 )   +-   +-   +-   +     - 
 w   1  1    2715 -0.010305    3( b1 )   2( b2 )   +-   +-   +-   +     - 
 w   1  1    2717  0.012462    5( b1 )   2( b2 )   +-   +-   +-   +     - 
 w   1  1    2718  0.016550    6( b1 )   2( b2 )   +-   +-   +-   +     - 
 w   1  1    2730 -0.014357    5( b1 )   3( b2 )   +-   +-   +-   +     - 
 w   1  1    2810 -0.012315    7( a1 )   1( a2 )   +-   +-   +-   +     - 
 w   1  1    2900  0.014999    2( a1 )   5( a1 )   +-   +-   +-        +- 
 w   1  1    2901 -0.010355    3( a1 )   5( a1 )   +-   +-   +-        +- 
 w   1  1    2903 -0.018085    5( a1 )   5( a1 )   +-   +-   +-        +- 
 w   1  1    2916 -0.012279    7( a1 )   7( a1 )   +-   +-   +-        +- 
 w   1  1    3044 -0.019619    2( b1 )   2( b1 )   +-   +-   +-        +- 
 w   1  1    3053 -0.029881    2( b1 )   5( b1 )   +-   +-   +-        +- 
 w   1  1    3056 -0.030670    5( b1 )   5( b1 )   +-   +-   +-        +- 
 w   1  1    3058 -0.011613    2( b1 )   6( b1 )   +-   +-   +-        +- 
 w   1  1    3059  0.011328    3( b1 )   6( b1 )   +-   +-   +-        +- 
 w   1  1    3062 -0.013666    6( b1 )   6( b1 )   +-   +-   +-        +- 
 w   1  1    3074  0.011221    5( b1 )   8( b1 )   +-   +-   +-        +- 
 w   1  1    3169 -0.016319    1( a2 )   1( a2 )   +-   +-   +-        +- 
 w   1  1    3185 -0.011148    2( a1 )   1( b2 )   +-   +-   +    +-    - 
 w   1  1    3188  0.010295    5( a1 )   1( b2 )   +-   +-   +    +-    - 
 w   1  1    3189 -0.013863    6( a1 )   1( b2 )   +-   +-   +    +-    - 
 w   1  1    3204  0.010710    4( a1 )   2( b2 )   +-   +-   +    +-    - 
 w   1  1    3205  0.015226    5( a1 )   2( b2 )   +-   +-   +    +-    - 
 w   1  1    3206 -0.018180    6( a1 )   2( b2 )   +-   +-   +    +-    - 
 w   1  1    3224  0.011195    7( a1 )   3( b2 )   +-   +-   +    +-    - 
 w   1  1    3324 -0.015130    5( b1 )   1( a2 )   +-   +-   +    +-    - 
 w   1  1    3403  0.019455    2( a1 )   2( b1 )   +-   +-   +     -   +- 
 w   1  1    3406 -0.022716    5( a1 )   2( b1 )   +-   +-   +     -   +- 
 w   1  1    3454  0.018771    2( a1 )   5( b1 )   +-   +-   +     -   +- 
 w   1  1    3457 -0.030476    5( a1 )   5( b1 )   +-   +-   +     -   +- 
 w   1  1    3460 -0.011905    8( a1 )   5( b1 )   +-   +-   +     -   +- 
 w   1  1    3475  0.012987    6( a1 )   6( b1 )   +-   +-   +     -   +- 
 w   1  1    3608 -0.013696    3( b2 )   1( a2 )   +-   +-   +     -   +- 
 w   1  1    3648 -0.013261    2( a1 )   2( a1 )   +-   +-        +-   +- 
 w   1  1    3657  0.018513    2( a1 )   5( a1 )   +-   +-        +-   +- 
 w   1  1    3660 -0.016941    5( a1 )   5( a1 )   +-   +-        +-   +- 
 w   1  1    3664  0.010963    4( a1 )   6( a1 )   +-   +-        +-   +- 
 w   1  1    3665  0.012711    5( a1 )   6( a1 )   +-   +-        +-   +- 
 w   1  1    3666 -0.017905    6( a1 )   6( a1 )   +-   +-        +-   +- 
 w   1  1    3669 -0.010783    3( a1 )   7( a1 )   +-   +-        +-   +- 
 w   1  1    3673 -0.014598    7( a1 )   7( a1 )   +-   +-        +-   +- 
 w   1  1    3810 -0.016962    2( b1 )   5( b1 )   +-   +-        +-   +- 
 w   1  1    3813 -0.018055    5( b1 )   5( b1 )   +-   +-        +-   +- 
 w   1  1    3831  0.011921    5( b1 )   8( b1 )   +-   +-        +-   +- 
 w   1  1    3895 -0.012541    3( b2 )   3( b2 )   +-   +-        +-   +- 
 w   1  1    3942 -0.010725    2( a1 )   1( b2 )   +-   +    +-   +-    - 
 w   1  1    3945  0.011252    5( a1 )   1( b2 )   +-   +    +-   +-    - 
 w   1  1    3949 -0.010180    9( a1 )   1( b2 )   +-   +    +-   +-    - 
 w   1  1    3959 -0.015366    2( a1 )   2( b2 )   +-   +    +-   +-    - 
 w   1  1    3961 -0.013257    4( a1 )   2( b2 )   +-   +    +-   +-    - 
 w   1  1    3962  0.015305    5( a1 )   2( b2 )   +-   +    +-   +-    - 
 w   1  1    3966 -0.013724    9( a1 )   2( b2 )   +-   +    +-   +-    - 
 w   1  1    4016 -0.010932    8( a1 )   5( b2 )   +-   +    +-   +-    - 
 w   1  1    4160  0.011860    2( a1 )   2( b1 )   +-   +    +-    -   +- 
 w   1  1    4163 -0.011877    5( a1 )   2( b1 )   +-   +    +-    -   +- 
 w   1  1    4211  0.011590    2( a1 )   5( b1 )   +-   +    +-    -   +- 
 w   1  1    4214 -0.018904    5( a1 )   5( b1 )   +-   +    +-    -   +- 
 w   1  1    4215 -0.011592    6( a1 )   5( b1 )   +-   +    +-    -   +- 
 w   1  1    4220 -0.013384   11( a1 )   5( b1 )   +-   +    +-    -   +- 
 w   1  1    4228  0.012177    2( a1 )   6( b1 )   +-   +    +-    -   +- 
 w   1  1    4234  0.010950    8( a1 )   6( b1 )   +-   +    +-    -   +- 
 w   1  1    4282  0.011543    5( a1 )   9( b1 )   +-   +    +-    -   +- 
 w   1  1    4405 -0.012081    2( a1 )   2( a1 )   +-   +     -   +-   +- 
 w   1  1    4414  0.018745    2( a1 )   5( a1 )   +-   +     -   +-   +- 
 w   1  1    4417 -0.016864    5( a1 )   5( a1 )   +-   +     -   +-   +- 
 w   1  1    4421 -0.010083    4( a1 )   6( a1 )   +-   +     -   +-   +- 
 w   1  1    4436 -0.011120    6( a1 )   8( a1 )   +-   +     -   +-   +- 
 w   1  1    4709  0.013216    2( a1 )   5( a1 )   +-        +-   +-   +- 
 w   1  1    4712 -0.013215    5( a1 )   5( a1 )   +-        +-   +-   +- 

 ci coefficient statistics:
           rq > 0.1                1
      0.1> rq > 0.01             115
     0.01> rq > 0.001           2126
    0.001> rq > 0.0001          2805
   0.0001> rq > 0.00001         1106
  0.00001> rq > 0.000001         169
 0.000001> rq                     17
           all                  6339
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        15 2x:         0 4x:         0
All internal counts: zz :         0 yy:         0 xx:         0 ww:         0
One-external counts: yz :         0 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         0    task #   2:         0    task #   3:         0    task #   4:         0
task #   5:         0    task #   6:         0    task #   7:        -1    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:        14    task #  12:         0
task #  13:         0    task #  14:         0    task #
  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.972323335005    -73.968359971176

 number of reference csfs (nref) is     1.  root number (iroot) is  1.

 pople ci energy extrapolation is computed with 10 correlated electrons.

 eref      =    -76.073829875564   "relaxed" cnot**2         =   0.945412667794
 eci       =    -76.337878464675   deltae = eci - eref       =  -0.264048589111
 eci+dv1   =    -76.352292172727   dv1 = (1-cnot**2)*deltae  =  -0.014413708052
 eci+dv2   =    -76.353124408106   dv2 = dv1 / cnot**2       =  -0.015245943431
 eci+dv3   =    -76.354058637686   dv3 = dv1 / (2*cnot**2-1) =  -0.016180173011
 eci+pople =    -76.350505697694   ( 10e- scaled deltae )    =  -0.276675822130
NO coefficients and occupation numbers are written to nocoef_ci.1
 entering drivercid: firstcall,firstnonref= F T
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=       3  D0X=       9  D0W=      15
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================

          ci-one-electron density block   1

                mo   1         mo   2         mo   3         mo   4         mo   5         mo   6         mo   7         mo   8
   mo   1     1.99955706
   mo   2    -0.00013912     1.98336771
   mo   3    -0.00012872     0.00305497     1.96960509
   mo   4    -0.00014481     0.00137815     0.01324571     0.00121120
   mo   5     0.00019696     0.00409182    -0.01098209    -0.00247990     0.00654401
   mo   6    -0.00019391     0.00513909     0.00415830     0.00027860     0.00059726     0.00282696
   mo   7     0.00034838    -0.00584070    -0.01582426    -0.00101986     0.00054433    -0.00134392     0.00267255
   mo   8    -0.00024172    -0.00665196     0.00460654     0.00271688    -0.00785165    -0.00035257     0.00014454     0.01038963
   mo   9    -0.00031420     0.00628472     0.00866476     0.00020590     0.00126511     0.00290520    -0.00198135    -0.00126729
   mo  10     0.00012371     0.00214512    -0.00561138    -0.00063751     0.00154588     0.00210722     0.00067724    -0.00109211
   mo  11     0.00063673    -0.00080085     0.00015923    -0.00066339    -0.00029474    -0.00142560     0.00258255     0.00142084
   mo  12     0.00059430     0.00195092    -0.00508164    -0.00087109     0.00132714    -0.00136082     0.00132418    -0.00175339
   mo  13    -0.00000617    -0.00132071     0.00135338     0.00026107    -0.00090348     0.00036190     0.00028187     0.00148443
   mo  14    -0.00026857    -0.00395732     0.00506379     0.00087750    -0.00228815    -0.00028486    -0.00035025     0.00302428
   mo  15    -0.00031645     0.00207076    -0.00645326    -0.00013645     0.00088493     0.00089836    -0.00057033    -0.00118938
   mo  16     0.00001372    -0.00013710     0.00059135     0.00001763    -0.00006509    -0.00015951    -0.00000804     0.00004006
   mo  17    -0.00025107     0.00010226    -0.00203499     0.00007406     0.00004801    -0.00001314    -0.00035941    -0.00039836
   mo  18    -0.00005409     0.00020313    -0.00027579     0.00005229    -0.00011995    -0.00016312    -0.00008446     0.00012872
   mo  19    -0.00014780     0.00046471    -0.00050900     0.00009474    -0.00015649     0.00033677    -0.00009990     0.00029398
   mo  20     0.00086462     0.00220817    -0.00033021    -0.00020413     0.00024018    -0.00027788     0.00045338    -0.00025191

                mo   9         mo  10         mo  11         mo  12         mo  13         mo  14         mo  15         mo  16
   mo   9     0.00530842
   mo  10     0.00065015     0.00421823
   mo  11    -0.00074255    -0.00042365     0.00439796
   mo  12    -0.00239028     0.00007962     0.00013551     0.00253596
   mo  13    -0.00065407     0.00130363    -0.00011953    -0.00001709     0.00120969
   mo  14    -0.00070777    -0.00088679    -0.00032999    -0.00016039     0.00035749     0.00158332
   mo  15     0.00157316     0.00057033    -0.00074237    -0.00040608    -0.00029212    -0.00054256     0.00112617
   mo  16    -0.00002596    -0.00039610     0.00013329     0.00000602    -0.00006984     0.00004336    -0.00004169     0.00039338
   mo  17    -0.00017889    -0.00027363    -0.00094965     0.00035947    -0.00013809     0.00004003     0.00007544     0.00000863
   mo  18     0.00037093    -0.00084614     0.00051532    -0.00040237    -0.00060550    -0.00000457    -0.00000205     0.00008847
   mo  19     0.00047014     0.00039024     0.00017913    -0.00062356     0.00024337    -0.00023421     0.00009992    -0.00004459
   mo  20    -0.00040129     0.00009083     0.00054589     0.00048641    -0.00001347    -0.00021988    -0.00014314     0.00000589

                mo  17         mo  18         mo  19         mo  20
   mo  17     0.00060826
   mo  18    -0.00012421     0.00067315
   mo  19    -0.00022301    -0.00000228     0.00043423
   mo  20    -0.00006721    -0.00002337    -0.00007133     0.00038122

          ci-one-electron density block   2

                mo  21         mo  22         mo  23         mo  24         mo  25         mo  26         mo  27         mo  28
   mo  21     1.96822575
   mo  22     0.00197378     0.00062256
   mo  23    -0.01615999    -0.00189568     0.00717806
   mo  24    -0.00224037     0.00074864    -0.00143957     0.00145756
   mo  25     0.00204421     0.00051376    -0.00184389     0.00044250     0.00051905
   mo  26    -0.01514240    -0.00194358     0.00847411    -0.00087267    -0.00213266     0.01107563
   mo  27    -0.00337115    -0.00126793     0.00291258    -0.00224536    -0.00086668     0.00232588     0.00383396
   mo  28     0.00144470     0.00015629    -0.00051847     0.00026188     0.00004706    -0.00067398    -0.00038666     0.00055603
   mo  29     0.00921810     0.00036790    -0.00229242    -0.00024061     0.00059549    -0.00361860     0.00020935     0.00025905
   mo  30     0.00528304     0.00059172    -0.00174622     0.00089370     0.00047508    -0.00192830    -0.00189188     0.00018455
   mo  31    -0.00537462    -0.00023383     0.00064350    -0.00039672    -0.00022182     0.00064611     0.00080815    -0.00013183
   mo  32     0.00093817     0.00003671    -0.00008689     0.00009635    -0.00002317    -0.00007481    -0.00013934     0.00026158
   mo  33    -0.00389632    -0.00000999     0.00030393     0.00015574    -0.00007988     0.00072897    -0.00028647    -0.00005040
   mo  34    -0.00130189    -0.00004052    -0.00007296    -0.00019845     0.00000641    -0.00028923     0.00044886     0.00007238

                mo  29         mo  30         mo  31         mo  32         mo  33         mo  34
   mo  29     0.00206077
   mo  30     0.00005015     0.00149763
   mo  31    -0.00024599    -0.00043257     0.00059961
   mo  32     0.00001904     0.00007507    -0.00002341     0.00032449
   mo  33    -0.00073743     0.00007982     0.00000622    -0.00000845     0.00053991
   mo  34     0.00043126    -0.00043061     0.00004845     0.00000903    -0.00020247     0.00034740

          ci-one-electron density block   3

                mo  35         mo  36         mo  37         mo  38         mo  39         mo  40         mo  41         mo  42
   mo  35     1.97123721
   mo  36     0.01353781     0.00560978
   mo  37     0.01309235     0.00615749     0.00861921
   mo  38     0.00471092    -0.00322724    -0.00183848     0.00461469
   mo  39     0.00027789     0.00031669     0.00149390     0.00129122     0.00139351
   mo  40    -0.00729666     0.00212022     0.00310072    -0.00113514     0.00048345     0.00188560
   mo  41     0.00000775     0.00001862    -0.00002200    -0.00004764    -0.00003778    -0.00000313     0.00038091
   mo  42    -0.00079094     0.00018381     0.00004113    -0.00050139    -0.00003994     0.00006586     0.00000430     0.00037883
   mo  43     0.00014678    -0.00023011     0.00023610     0.00110652     0.00077179    -0.00000164     0.00000468    -0.00014491

                mo  43
   mo  43     0.00076178

          ci-one-electron density block   4

                mo  44         mo  45         mo  46         mo  47         mo  48
   mo  44     0.00577868
   mo  45    -0.00074309     0.00082811
   mo  46     0.00035796    -0.00000627     0.00026586
   mo  47     0.00015354    -0.00014950     0.00016794     0.00020920
   mo  48    -0.00004957    -0.00013325    -0.00007156    -0.00001354     0.00015398


*****   symmetry block SYM1   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       1.99955706
       2      -0.00013912     1.98336771
       3      -0.00012872     0.00305497     1.96960509
       4      -0.00014481     0.00137815     0.01324571     0.00121120
       5       0.00019696     0.00409182    -0.01098209    -0.00247990
       6      -0.00019391     0.00513909     0.00415830     0.00027860
       7       0.00034838    -0.00584070    -0.01582426    -0.00101986
       8      -0.00024172    -0.00665196     0.00460654     0.00271688
       9      -0.00031420     0.00628472     0.00866476     0.00020590
      10       0.00012371     0.00214512    -0.00561138    -0.00063751
      11       0.00063673    -0.00080085     0.00015923    -0.00066339
      12       0.00059430     0.00195092    -0.00508164    -0.00087109
      13      -0.00000617    -0.00132071     0.00135338     0.00026107
      14      -0.00026857    -0.00395732     0.00506379     0.00087750
      15      -0.00031645     0.00207076    -0.00645326    -0.00013645
      16       0.00001372    -0.00013710     0.00059135     0.00001763
      17      -0.00025107     0.00010226    -0.00203499     0.00007406
      18      -0.00005409     0.00020313    -0.00027579     0.00005229
      19      -0.00014780     0.00046471    -0.00050900     0.00009474
      20       0.00086462     0.00220817    -0.00033021    -0.00020413

               Column   5     Column   6     Column   7     Column   8
       5       0.00654401
       6       0.00059726     0.00282696
       7       0.00054433    -0.00134392     0.00267255
       8      -0.00785165    -0.00035257     0.00014454     0.01038963
       9       0.00126511     0.00290520    -0.00198135    -0.00126729
      10       0.00154588     0.00210722     0.00067724    -0.00109211
      11      -0.00029474    -0.00142560     0.00258255     0.00142084
      12       0.00132714    -0.00136082     0.00132418    -0.00175339
      13      -0.00090348     0.00036190     0.00028187     0.00148443
      14      -0.00228815    -0.00028486    -0.00035025     0.00302428
      15       0.00088493     0.00089836    -0.00057033    -0.00118938
      16      -0.00006509    -0.00015951    -0.00000804     0.00004006
      17       0.00004801    -0.00001314    -0.00035941    -0.00039836
      18      -0.00011995    -0.00016312    -0.00008446     0.00012872
      19      -0.00015649     0.00033677    -0.00009990     0.00029398
      20       0.00024018    -0.00027788     0.00045338    -0.00025191

               Column   9     Column  10     Column  11     Column  12
       9       0.00530842
      10       0.00065015     0.00421823
      11      -0.00074255    -0.00042365     0.00439796
      12      -0.00239028     0.00007962     0.00013551     0.00253596
      13      -0.00065407     0.00130363    -0.00011953    -0.00001709
      14      -0.00070777    -0.00088679    -0.00032999    -0.00016039
      15       0.00157316     0.00057033    -0.00074237    -0.00040608
      16      -0.00002596    -0.00039610     0.00013329     0.00000602
      17      -0.00017889    -0.00027363    -0.00094965     0.00035947
      18       0.00037093    -0.00084614     0.00051532    -0.00040237
      19       0.00047014     0.00039024     0.00017913    -0.00062356
      20      -0.00040129     0.00009083     0.00054589     0.00048641

               Column  13     Column  14     Column  15     Column  16
      13       0.00120969
      14       0.00035749     0.00158332
      15      -0.00029212    -0.00054256     0.00112617
      16      -0.00006984     0.00004336    -0.00004169     0.00039338
      17      -0.00013809     0.00004003     0.00007544     0.00000863
      18      -0.00060550    -0.00000457    -0.00000205     0.00008847
      19       0.00024337    -0.00023421     0.00009992    -0.00004459
      20      -0.00001347    -0.00021988    -0.00014314     0.00000589

               Column  17     Column  18     Column  19     Column  20
      17       0.00060826
      18      -0.00012421     0.00067315
      19      -0.00022301    -0.00000228     0.00043423
      20      -0.00006721    -0.00002337    -0.00007133     0.00038122

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7        eno   8
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    8   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno   9        eno  10        eno  11        eno  12        eno  13        eno  14        eno  15        eno  16
 emo    9   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   10   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   11   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   12   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   13   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   14   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   15   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   16   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno  17        eno  18        eno  19        eno  20
 emo   17   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   18   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   19   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   20   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   1.9995603      1.9841384      1.9693367      0.0194738      0.0110159
   0.0060620      0.0052900      0.0011300      0.0008418      0.0005707
   0.0004725      0.0003663      0.0003554      0.0001563      0.0001087
   0.0000690      0.0000561      0.0000320      0.0000081      0.0000004

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7         no   8
  mo    1   0.999936E+00   0.106994E-01   0.346090E-02   0.152561E-03   0.514819E-03  -0.224206E-03   0.611072E-04  -0.962201E-04
  mo    2  -0.971684E-02   0.976986E+00  -0.212963E+00   0.510048E-02  -0.361148E-02   0.739312E-04   0.442877E-03  -0.665081E-03
  mo    3  -0.565494E-02   0.212887E+00   0.976955E+00  -0.694662E-02  -0.814878E-02   0.421880E-02   0.192268E-02   0.141542E-02
  mo    4  -0.117448E-03   0.209756E-02   0.644550E-02   0.193885E+00   0.150159E+00  -0.110938E+00   0.521444E-01  -0.786196E-01
  mo    5   0.110815E-03   0.853872E-03  -0.593910E-02  -0.563400E+00  -0.895619E-01   0.652847E-01  -0.593533E-01   0.712762E-01
  mo    6  -0.134819E-03   0.299056E-02   0.151452E-02  -0.840835E-01   0.431384E+00   0.245538E+00   0.110378E+00   0.208608E+00
  mo    7   0.248899E-03  -0.458495E-02  -0.723918E-02   0.599094E-02  -0.377933E+00   0.361358E+00  -0.751419E-01   0.193379E+00
  mo    8  -0.102755E-03  -0.280480E-02   0.306142E-02   0.714304E+00   0.100326E+00   0.202027E+00  -0.187115E-02   0.200210E+00
  mo    9  -0.213685E-03   0.404550E-02   0.363074E-02  -0.151088E+00   0.565830E+00   0.109209E+00  -0.432651E+00   0.346423E+00
  mo   10   0.674921E-04   0.460869E-03  -0.303194E-02  -0.143489E+00   0.140435E+00   0.643279E+00   0.438139E+00  -0.291417E-01
  mo   11   0.323255E-03  -0.386667E-03   0.157576E-03   0.899811E-01  -0.339159E+00   0.438513E+00  -0.579363E+00   0.300765E-01
  mo   12   0.303373E-03   0.411476E-03  -0.275410E-02  -0.980860E-01  -0.357068E+00  -0.730145E-01   0.282314E+00   0.573719E+00
  mo   13  -0.529243E-06  -0.509077E-03   0.817753E-03   0.865766E-01   0.143012E-01   0.243062E+00   0.288345E+00  -0.200945E+00
  mo   14  -0.129887E-03  -0.141373E-02   0.295887E-02   0.217623E+00   0.187100E-01  -0.124654E+00   0.104379E+00   0.356256E+00
  mo   15  -0.150494E-03   0.334101E-03  -0.342842E-02  -0.102821E+00   0.182400E+00   0.105297E-01  -0.414577E-01   0.193369E+00
  mo   16   0.587449E-05  -0.446247E-05   0.309160E-03   0.826557E-02  -0.175318E-01  -0.482511E-01  -0.574986E-01   0.254264E-01
  mo   17  -0.120458E-03  -0.168130E-03  -0.102111E-02  -0.186743E-01   0.162876E-01  -0.169152E+00   0.135870E+00   0.204460E+00
  mo   18  -0.273245E-04   0.704664E-04  -0.155590E-03   0.152766E-01   0.423577E-02  -0.815133E-01  -0.247429E+00  -0.107956E-01
  mo   19  -0.749087E-04   0.174795E-03  -0.300372E-03   0.109487E-01   0.705886E-01   0.112830E+00  -0.492532E-01  -0.399062E+00
  mo   20   0.423017E-03   0.105484E-02  -0.406381E-03  -0.157230E-01  -0.924685E-01   0.576765E-01  -0.153556E-01   0.101571E+00

                no   9         no  10         no  11         no  12         no  13         no  14         no  15         no  16
  mo    1  -0.470758E-04   0.346503E-04  -0.325206E-03   0.174563E-03   0.189709E-03   0.307785E-04  -0.964761E-04   0.480034E-04
  mo    2  -0.985793E-03   0.266605E-03  -0.232173E-02   0.108120E-02   0.129082E-02  -0.282589E-03   0.426264E-03  -0.713279E-03
  mo    3   0.255340E-02  -0.591636E-03  -0.484001E-02   0.265159E-02   0.188011E-02   0.209104E-02   0.236271E-02  -0.228444E-02
  mo    4   0.139380E+00  -0.458292E-01   0.155818E+00  -0.891768E-01  -0.117446E+00  -0.108059E+00  -0.163213E+00   0.125736E+00
  mo    5  -0.386534E+00   0.561621E-01  -0.129268E+00   0.268763E-01   0.249815E-01   0.181863E+00   0.278993E+00  -0.225534E-01
  mo    6  -0.165281E+00  -0.319920E+00   0.247012E+00   0.127829E+00  -0.334662E-01   0.784906E-01  -0.357841E-01  -0.137857E-02
  mo    7   0.102467E+00  -0.191644E-01  -0.212654E+00   0.219588E+00   0.255774E+00   0.505932E-01  -0.679462E-01  -0.272234E+00
  mo    8  -0.318570E-01   0.294847E-03  -0.668756E-01   0.362482E-01   0.460686E-01   0.187723E+00   0.154371E+00  -0.874769E-01
  mo    9  -0.916867E-03   0.298336E+00   0.119438E+00  -0.109651E+00   0.110733E+00   0.600084E-01  -0.197401E+00  -0.173536E-01
  mo   10   0.952019E-01  -0.227835E+00  -0.888907E-01   0.177151E-02  -0.562432E-01  -0.182948E+00  -0.567544E-01  -0.378235E-01
  mo   11  -0.319845E-01   0.521864E-01   0.144486E+00  -0.590493E-01  -0.953304E-02  -0.126447E+00  -0.211371E-02   0.259512E+00
  mo   12   0.146738E+00   0.801054E-01   0.201192E+00  -0.160057E+00  -0.212397E+00   0.326052E+00  -0.410538E+00   0.112213E+00
  mo   13  -0.194713E+00   0.491396E+00   0.106182E+00   0.570290E-01  -0.412810E-01   0.255793E+00   0.171958E+00   0.569709E+00
  mo   14  -0.567902E+00   0.559768E-01  -0.337953E+00  -0.760167E-01  -0.101620E+00  -0.205253E-01   0.140760E+00  -0.150302E+00
  mo   15   0.573250E+00   0.196778E+00  -0.537889E+00   0.575789E-01  -0.275438E+00   0.571916E-01   0.300385E+00   0.120420E+00
  mo   16  -0.554667E-01   0.188490E+00   0.204125E+00   0.811120E+00  -0.436372E+00  -0.111964E+00  -0.626387E-01  -0.163149E+00
  mo   17   0.208344E+00  -0.199746E-01   0.227166E+00   0.334765E+00   0.669376E+00   0.136559E+00   0.288980E+00   0.972765E-01
  mo   18  -0.263202E-01  -0.632662E+00  -0.116182E+00   0.159569E+00  -0.191993E+00   0.434367E+00   0.591887E-01   0.376351E+00
  mo   19   0.610203E-01   0.141894E+00  -0.874339E-02  -0.356992E-01  -0.932890E-02   0.679365E+00  -0.162073E+00  -0.443885E+00
  mo   20   0.144099E+00  -0.482528E-01   0.493045E+00  -0.274309E+00  -0.304886E+00   0.373725E-01   0.631062E+00  -0.289526E+00

                no  17         no  18         no  19         no  20
  mo    1  -0.512955E-04  -0.199920E-04  -0.830097E-05   0.629264E-06
  mo    2   0.893466E-03   0.766273E-03  -0.324711E-06  -0.177988E-03
  mo    3   0.627977E-03   0.225888E-02   0.209564E-02  -0.130227E-02
  mo    4  -0.614203E-01  -0.128760E+00  -0.257999E+00   0.842624E+00
  mo    5  -0.184854E+00  -0.101643E+00   0.385772E+00   0.431620E+00
  mo    6  -0.499088E+00   0.429953E+00  -0.192325E+00  -0.656860E-01
  mo    7   0.293768E+00   0.470365E+00  -0.220284E+00   0.276319E+00
  mo    8  -0.993974E-01  -0.286787E-01   0.547212E+00   0.113999E+00
  mo    9   0.402156E+00  -0.650127E-01   0.374825E-01   0.839434E-02
  mo   10   0.205568E+00  -0.446025E+00   0.285432E-01  -0.297236E-01
  mo   11  -0.348741E+00  -0.273617E+00  -0.187326E+00  -0.699763E-01
  mo   12  -0.757070E-01  -0.877930E-01   0.846628E-01  -0.384588E-01
  mo   13   0.177736E+00   0.213742E+00  -0.131296E+00  -0.970246E-03
  mo   14   0.318486E-01  -0.244986E+00  -0.487091E+00  -0.429850E-01
  mo   15  -0.243731E+00   0.251727E-01  -0.114080E+00  -0.507740E-02
  mo   16   0.518224E-01  -0.132470E+00   0.377384E-01   0.442012E-03
  mo   17  -0.708474E-01  -0.330454E+00  -0.170481E+00  -0.791557E-02
  mo   18   0.339893E+00  -0.612622E-01  -0.431001E-01  -0.591392E-02
  mo   19  -0.157474E+00  -0.202290E+00  -0.209800E+00  -0.249275E-01
  mo   20   0.213091E+00   0.515727E-01  -0.106327E+00   0.100447E-01
  eigenvectors of nos in ao-basis, column    1
  0.9975019E+00 -0.1489757E-01  0.3355435E-02  0.4267103E-02 -0.7860249E-02  0.3731792E-02  0.4435908E-03  0.1028471E-04
  0.3398544E-05  0.1089693E-04 -0.2586730E-03  0.2153755E-04  0.5464486E-04  0.7021598E-03 -0.1420387E-02  0.4918676E-04
  0.4668458E-03  0.2380375E-03 -0.5327697E-03 -0.5345590E-03
  eigenvectors of nos in ao-basis, column    2
  0.2969613E-02  0.9124635E+00 -0.2992721E-02 -0.1329314E+00  0.4565596E-01  0.1022795E-01  0.7474970E-01  0.1589739E-03
  0.2545509E-02 -0.1581922E-02 -0.2567220E-02 -0.7771256E-03 -0.1740193E-02  0.2568500E+00 -0.1113497E+00  0.3063855E-02
  0.2604725E-01  0.1918810E-01  0.1614976E-01  0.1207693E-01
  eigenvectors of nos in ao-basis, column    3
  0.9024926E-02  0.1080293E+00  0.3531281E-02  0.1550134E+00  0.8352126E+00 -0.2689813E-01 -0.2853116E-01 -0.1608406E-02
 -0.2680102E-02 -0.5367377E-02  0.1952258E-02 -0.8009265E-04  0.2671208E-02 -0.3984377E+00  0.1696998E+00  0.5815278E-02
 -0.2996388E-01 -0.1605104E-02 -0.1249651E-02  0.2914287E-01
  eigenvectors of nos in ao-basis, column    4
 -0.4321839E-01 -0.6092842E+00 -0.6245760E-01  0.3726176E-02  0.1322329E+01 -0.2016264E+00 -0.6954153E+00  0.3386748E-02
  0.1557537E-01  0.8598024E-02  0.2577324E-01  0.2137912E-02  0.3611720E-02  0.9576642E+00 -0.4177480E+00 -0.8297760E-02
  0.5946108E-01  0.1024784E-01  0.2998841E-02 -0.3704969E-01
  eigenvectors of nos in ao-basis, column    5
  0.1512204E-01 -0.1072727E+01 -0.4387394E+00  0.1099554E+01 -0.8227662E+00  0.3817486E+00  0.8216551E+00 -0.5080912E-02
  0.3180773E-01 -0.2326211E-01  0.4190228E-01 -0.2402230E-02 -0.2998716E-02  0.4369803E+00 -0.2463513E+00  0.2009660E-01
  0.2573982E-01  0.7847429E-01  0.1413787E-01  0.5688228E-01
  eigenvectors of nos in ao-basis, column    6
 -0.1050722E-01  0.4831073E+00  0.2536581E+00 -0.9456086E+00 -0.7482526E-01  0.3349411E-01  0.1444712E+00 -0.3528896E-01
  0.1153116E+00 -0.8782002E-01  0.2529298E+00 -0.1431512E-01 -0.2082287E-01  0.3108478E+00 -0.1484045E+00 -0.1169378E-01
 -0.3239430E-01  0.8240869E-01 -0.3416272E-01  0.2195746E-01
  eigenvectors of nos in ao-basis, column    7
 -0.2269424E-02 -0.1389396E+00 -0.5776750E-01  0.2234952E+00  0.2739877E+00 -0.1191640E+00 -0.4003571E+00 -0.8665294E-01
 -0.8420278E-01 -0.1767016E+00 -0.1647341E+00 -0.9699690E-02  0.1359481E-01 -0.8177789E-01  0.6173001E-01  0.5516705E-02
 -0.5157325E-01  0.4021031E-01 -0.3579287E-02  0.3956056E-01
  eigenvectors of nos in ao-basis, column    8
 -0.1250814E+00  0.2145633E-01  0.4337268E+00 -0.1918617E+01 -0.2240836E+00  0.3077005E+00  0.9771588E+00  0.1974741E-01
 -0.2458847E+00  0.1515246E-01 -0.2148208E+00 -0.3396912E-01 -0.8784192E-01  0.2109847E+01 -0.1006647E+01 -0.2598029E-02
  0.6965825E-01  0.1637103E+00  0.6725418E-01  0.6706192E-01
  eigenvectors of nos in ao-basis, column    9
 -0.2185228E+00 -0.2822558E+00  0.4133358E+00  0.2914244E+00 -0.2531832E+01  0.2378405E+01  0.4472726E+00 -0.2120757E-01
 -0.4364885E-01  0.1256935E-01  0.3795861E-01  0.8969964E-02  0.4133042E-01 -0.3992801E-01  0.1335784E+00 -0.3913635E-01
 -0.1687288E+00 -0.1012313E+00 -0.5716758E-01 -0.1443800E+00
  eigenvectors of nos in ao-basis, column   10
  0.1593843E+00  0.3562734E+00 -0.1913265E+00 -0.6430676E+00 -0.1584510E+00  0.2015356E+00  0.8281178E+00 -0.2109928E+00
  0.3115396E-01  0.1339964E-01  0.1567812E-01  0.8772453E-01 -0.1172184E+00  0.6588266E+00 -0.3592111E+00 -0.1088884E-01
  0.3300592E+00 -0.2878970E+00  0.9376423E-01 -0.1768889E+00
  eigenvectors of nos in ao-basis, column   11
 -0.1380668E+01 -0.2891735E+01  0.2005665E+01  0.1916824E+01  0.8283824E+00 -0.1103731E+01  0.6093206E-01 -0.3294955E-01
 -0.1031832E+00  0.1689308E-01  0.1383111E+00  0.1418573E+00  0.1631678E+00 -0.2263060E+00  0.2220855E+00 -0.3058587E-02
 -0.1474203E+00 -0.1951971E+00 -0.2361207E-01  0.9223421E-01
  eigenvectors of nos in ao-basis, column   12
  0.8834375E+00  0.1946259E+01 -0.1242867E+01 -0.8407530E+00 -0.1847853E+00  0.2087555E+00 -0.3266501E+00  0.7891160E-01
 -0.1479731E+00 -0.4180684E-01  0.1159251E+00  0.3098978E+00 -0.7170454E-01 -0.2235450E+00  0.2528426E-01 -0.1337370E-01
 -0.6132948E-01  0.1458820E-01 -0.1619438E+00  0.4835741E-01
  eigenvectors of nos in ao-basis, column   13
  0.1011232E+01  0.2241778E+01 -0.1415119E+01 -0.7181937E+00  0.2411195E+00 -0.3824901E+00 -0.1037491E-01 -0.9724830E-01
 -0.2549843E+00  0.8112012E-01  0.2299127E+00 -0.7163450E-01  0.3154683E+00 -0.1895068E+00 -0.1044701E+00 -0.2218005E-01
 -0.9858125E-01 -0.6882746E-01 -0.1500245E+00 -0.1366220E+00
  eigenvectors of nos in ao-basis, column   14
  0.3028563E+00  0.8518344E+00 -0.1799604E+00 -0.3169158E+01 -0.7865790E+00  0.1248450E+01  0.1076503E+01  0.9127510E-01
  0.3115404E+00 -0.1707669E+00 -0.4952580E+00  0.6582743E-01  0.3263033E+00  0.2756680E+01 -0.1270847E+01  0.2205642E-01
  0.4409468E+00 -0.1122995E+00  0.1799617E+00  0.2338710E+00
  eigenvectors of nos in ao-basis, column   15
 -0.2226985E+01 -0.5912622E+01  0.2483080E+01  0.3694876E+01 -0.1225439E+01  0.2400510E+01 -0.2949115E+00  0.2122842E-01
 -0.1632586E+00 -0.7373856E-01 -0.4810258E-01  0.1684755E-01  0.1243248E+00  0.1659297E+01 -0.1569979E+01  0.1166411E+00
  0.1025946E+01  0.5011686E+00 -0.1708258E+00  0.7327474E-02
  eigenvectors of nos in ao-basis, column   16
  0.1092890E+01  0.2959840E+01 -0.1142662E+01 -0.3589235E+01 -0.5286034E-01 -0.7820937E-01  0.4807847E+00  0.1512405E+00
 -0.2261854E+00 -0.3108080E+00  0.4826526E+00 -0.1338398E+00 -0.7755491E-01 -0.6930535E+00  0.1291396E+01 -0.6366481E-01
  0.3016533E+00 -0.8381771E+00  0.3980384E+00  0.4064110E+00
  eigenvectors of nos in ao-basis, column   17
 -0.9889124E+00 -0.2699725E+01  0.9682284E+00  0.5094962E+01  0.6310765E+00 -0.9904527E+00  0.8523084E+00  0.1328176E+00
 -0.2967258E-01 -0.1924032E+00  0.7095007E-01 -0.4095896E-02  0.1400983E-01 -0.1758093E+00 -0.9700534E+00 -0.3580306E-01
  0.2592699E-01 -0.1002068E+00 -0.4776418E+00 -0.1198620E+01
  eigenvectors of nos in ao-basis, column   18
 -0.6060573E+00 -0.1725645E+01  0.4245225E+00  0.6406445E+01  0.1956265E+00 -0.5436343E+00 -0.1366662E+01 -0.1965792E-02
  0.9846149E-02 -0.4722778E-01  0.2766170E+00 -0.1199398E+00 -0.1721958E+00 -0.2147804E+01 -0.2294207E+00  0.3373988E-02
 -0.1730770E+00 -0.7471651E+00 -0.1281957E+01 -0.2237591E+00
  eigenvectors of nos in ao-basis, column   19
  0.1879620E+00  0.5420219E+00 -0.3079953E+00  0.3233292E+01  0.3503414E+00 -0.1177776E+01 -0.1294195E+01  0.5766488E-02
 -0.5801862E-01  0.2690263E-01  0.4315534E+00 -0.4451747E-01 -0.1558115E+00 -0.2379844E+01 -0.1723591E+00  0.2774974E+00
 -0.1055412E+01 -0.6603372E+00  0.3827697E+00  0.6307966E-01
  eigenvectors of nos in ao-basis, column   20
 -0.1562809E+00 -0.4831171E+00  0.6468488E-01  0.2392424E+01  0.1107441E-01 -0.6437365E-01 -0.5776643E+00 -0.2276442E-03
 -0.1200013E-01  0.2821795E-02  0.6412435E-01 -0.5399455E-02 -0.1517745E-01 -0.3177303E+00 -0.1253046E+01  0.8903768E+00
 -0.1256035E+00 -0.9273800E-01 -0.3117911E+00 -0.2513822E+00


*****   symmetry block SYM2   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       1.96822575
       2       0.00197378     0.00062256
       3      -0.01615999    -0.00189568     0.00717806
       4      -0.00224037     0.00074864    -0.00143957     0.00145756
       5       0.00204421     0.00051376    -0.00184389     0.00044250
       6      -0.01514240    -0.00194358     0.00847411    -0.00087267
       7      -0.00337115    -0.00126793     0.00291258    -0.00224536
       8       0.00144470     0.00015629    -0.00051847     0.00026188
       9       0.00921810     0.00036790    -0.00229242    -0.00024061
      10       0.00528304     0.00059172    -0.00174622     0.00089370
      11      -0.00537462    -0.00023383     0.00064350    -0.00039672
      12       0.00093817     0.00003671    -0.00008689     0.00009635
      13      -0.00389632    -0.00000999     0.00030393     0.00015574
      14      -0.00130189    -0.00004052    -0.00007296    -0.00019845

               Column   5     Column   6     Column   7     Column   8
       5       0.00051905
       6      -0.00213266     0.01107563
       7      -0.00086668     0.00232588     0.00383396
       8       0.00004706    -0.00067398    -0.00038666     0.00055603
       9       0.00059549    -0.00361860     0.00020935     0.00025905
      10       0.00047508    -0.00192830    -0.00189188     0.00018455
      11      -0.00022182     0.00064611     0.00080815    -0.00013183
      12      -0.00002317    -0.00007481    -0.00013934     0.00026158
      13      -0.00007988     0.00072897    -0.00028647    -0.00005040
      14       0.00000641    -0.00028923     0.00044886     0.00007238

               Column   9     Column  10     Column  11     Column  12
       9       0.00206077
      10       0.00005015     0.00149763
      11      -0.00024599    -0.00043257     0.00059961
      12       0.00001904     0.00007507    -0.00002341     0.00032449
      13      -0.00073743     0.00007982     0.00000622    -0.00000845
      14       0.00043126    -0.00043061     0.00004845     0.00000903

               Column  13     Column  14
      13       0.00053991
      14      -0.00020247     0.00034740

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7        eno   8
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    8   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno   9        eno  10        eno  11        eno  12        eno  13        eno  14
 emo    9   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   10   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo   11   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo   12   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo   13   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo   14   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   1.9685727      0.0211315      0.0058185      0.0010710      0.0008219
   0.0006427      0.0003902      0.0001634      0.0000985      0.0000945
   0.0000181      0.0000099      0.0000052      0.0000002

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7         no   8
  mo    1   0.999911E+00   0.123891E-01  -0.214103E-02  -0.128366E-02  -0.211291E-02   0.628739E-03   0.117626E-02  -0.170852E-02
  mo    2   0.102162E-02  -0.149564E+00  -0.131611E+00   0.177356E-01  -0.218750E+00  -0.396869E-01   0.645036E-01   0.750491E-01
  mo    3  -0.828500E-02   0.569771E+00   0.182535E-01   0.213826E+00   0.357992E+00  -0.353576E-01   0.685118E-01  -0.241421E+00
  mo    4  -0.112544E-02  -0.116415E+00  -0.415068E+00   0.257018E+00  -0.286129E+00  -0.431669E-01   0.190464E+00  -0.109336E+00
  mo    5   0.105806E-02  -0.148579E+00  -0.274639E-01  -0.293503E-01  -0.976458E-01  -0.194176E+00  -0.568803E-01   0.157104E+00
  mo    6  -0.778770E-02   0.699126E+00  -0.309014E+00   0.142496E+00  -0.193700E+00  -0.102932E+00  -0.176611E-02   0.194480E+00
  mo    7  -0.174028E-02   0.238568E+00   0.662963E+00  -0.204659E+00   0.133271E-01   0.109836E+00  -0.519858E-01   0.875136E-01
  mo    8   0.740321E-03  -0.483354E-01  -0.298214E-01   0.361432E+00   0.342093E-01   0.703391E+00  -0.863690E-01  -0.401957E+00
  mo    9   0.471269E-02  -0.201893E+00   0.359171E+00   0.552702E+00   0.117280E+00  -0.232078E+00   0.206289E-01  -0.108748E-01
  mo   10   0.270308E-02  -0.154363E+00  -0.295662E+00  -0.131347E+00   0.725266E+00   0.215357E-01   0.187904E+00  -0.181392E-01
  mo   11  -0.273806E-02   0.575465E-01   0.117087E+00  -0.316270E+00  -0.222065E+00   0.223504E+00   0.785250E+00  -0.172761E+00
  mo   12   0.477629E-03  -0.800062E-02  -0.255526E-01   0.177688E+00   0.308853E-01   0.530839E+00   0.341892E-01   0.754435E+00
  mo   13  -0.198529E-02   0.341085E-01  -0.147335E+00  -0.411842E+00  -0.120919E+00   0.232637E+00  -0.535162E+00  -0.218175E+00
  mo   14  -0.659636E-03  -0.735654E-02   0.144626E+00   0.265093E+00  -0.298428E+00  -0.562505E-03  -0.393557E-01  -0.212903E+00

                no   9         no  10         no  11         no  12         no  13         no  14
  mo    1   0.467733E-04   0.177432E-02   0.117089E-02   0.135233E-02   0.160738E-02   0.307598E-03
  mo    2  -0.444181E-01  -0.120584E+00   0.318289E+00  -0.154208E+00  -0.177579E-01   0.871124E+00
  mo    3  -0.549756E-01   0.357797E+00   0.125512E+00   0.359137E+00   0.296281E+00   0.270730E+00
  mo    4  -0.183888E+00  -0.134086E-01   0.629956E+00   0.166609E+00   0.127945E+00  -0.375732E+00
  mo    5  -0.109931E+00   0.103755E+00  -0.190836E+00  -0.298268E+00   0.867245E+00  -0.283892E-01
  mo    6  -0.848450E-01  -0.182239E+00  -0.111061E+00  -0.479102E+00  -0.146043E+00  -0.762607E-01
  mo    7  -0.201553E-01  -0.273057E+00   0.545076E+00  -0.178617E+00   0.146579E+00  -0.116665E+00
  mo    8  -0.756611E-01  -0.338592E+00  -0.154392E+00  -0.154423E+00   0.176754E+00   0.434484E-01
  mo    9  -0.500243E+00   0.252176E+00  -0.198023E-01  -0.291874E+00  -0.238705E+00  -0.132697E-01
  mo   10   0.168597E+00   0.388988E-01   0.246061E+00  -0.463586E+00  -0.285852E-01  -0.563188E-01
  mo   11  -0.185570E+00   0.249343E+00  -0.144982E+00  -0.138180E+00  -0.221317E-01  -0.171626E-02
  mo   12   0.259535E-01   0.316810E+00   0.646571E-01   0.966944E-01   0.442424E-02  -0.568018E-02
  mo   13  -0.334931E+00   0.476460E+00   0.140769E+00  -0.187349E+00  -0.103013E+00  -0.718262E-02
  mo   14   0.714016E+00   0.411603E+00   0.102178E+00  -0.285624E+00  -0.210277E-01  -0.334592E-01
  eigenvectors of nos in ao-basis, column    1
  0.7599163E+00 -0.3243599E-01 -0.1376527E+00 -0.1581684E-01 -0.8206686E-02  0.2766907E-03 -0.3342332E-02 -0.5500475E+00
  0.1516692E+00 -0.9939641E-02 -0.2207328E-01 -0.3021010E-01 -0.2094361E-01 -0.2482416E-01
  eigenvectors of nos in ao-basis, column    2
 -0.1330981E+01  0.1109431E+00  0.5315507E+00 -0.3852477E-01 -0.5930766E-01  0.1582935E-04  0.3455272E-02 -0.1034143E+01
  0.4051936E+00 -0.1454683E-01 -0.5309811E-01 -0.3289573E-01 -0.8226276E-02  0.7160096E-04
  eigenvectors of nos in ao-basis, column    3
 -0.7403408E+00  0.3331537E+00  0.8706570E+00  0.2829448E+00  0.5412802E+00 -0.8261110E-02  0.1192683E-01  0.2372610E+00
 -0.8272175E-01 -0.3437159E-01  0.8878106E-01 -0.1404504E-01  0.3698537E-01  0.3185777E-01
  eigenvectors of nos in ao-basis, column    4
  0.1312639E+01 -0.1141221E+01 -0.1335427E+01  0.5240880E+00  0.4202222E+00  0.2543173E-01  0.8761818E-02 -0.1471748E+01
  0.3403145E+00  0.4168749E-01 -0.1175678E+00  0.1914178E+00 -0.1258379E+00 -0.9953969E-01
  eigenvectors of nos in ao-basis, column    5
  0.2068464E+01 -0.1836203E+01  0.3629747E+00 -0.2256360E+00 -0.2892849E+00 -0.2156367E-01  0.7956949E-01  0.1055882E+01
 -0.5881790E+00  0.3663053E-01  0.2062165E+00  0.1686555E+00  0.1185800E+00  0.2027667E+00
  eigenvectors of nos in ao-basis, column    6
 -0.6775241E+00  0.7314577E+00  0.1166558E+01 -0.1557209E+00 -0.1431992E+00  0.7249118E-01  0.2345060E+00  0.1048933E+01
 -0.6655587E-01 -0.4801060E-01 -0.2288530E+00  0.3986626E+00  0.1505955E+00  0.1301157E+00
  eigenvectors of nos in ao-basis, column    7
 -0.6916131E+00  0.1001115E+01 -0.2097544E-01  0.3660159E+00 -0.4360426E+00 -0.8176945E-01  0.4202282E+00  0.3333751E+00
 -0.5239460E+00  0.8418905E-01  0.1642815E+00  0.1182801E+00 -0.2058028E+00  0.9396551E-01
  eigenvectors of nos in ao-basis, column    8
  0.1295860E+01 -0.2178949E+01 -0.9933666E+00 -0.9756238E-01  0.7084465E+00  0.6864664E-01  0.3940371E+00 -0.2201309E+01
  0.1242435E+01 -0.1602056E+00 -0.1025844E+00 -0.8231823E+00 -0.1078835E+00 -0.2391599E+00
  eigenvectors of nos in ao-basis, column    9
  0.2139397E+00 -0.4876299E+00  0.1764606E+01  0.9621067E+00 -0.1429731E+01  0.3671664E-01 -0.1155081E+00  0.1940086E+01
 -0.1438317E+00 -0.4526800E-01 -0.1352578E+00 -0.2054587E+00  0.2536178E+00  0.3539112E+00
  eigenvectors of nos in ao-basis, column   10
 -0.1631431E+01  0.3217506E+01 -0.5443786E+00  0.1364187E+00 -0.1253780E+01  0.1031680E+00 -0.3736123E-01  0.2777113E+01
 -0.3444707E+01  0.3499158E+00  0.1154790E+01  0.3151746E+00 -0.4453211E+00 -0.9506769E-01
  eigenvectors of nos in ao-basis, column   11
  0.2360153E+00 -0.3501850E+00  0.3515815E+01  0.2914896E-01 -0.6015050E+00  0.3572522E-01 -0.7761395E-01  0.1803048E+01
  0.2200911E+01 -0.2538324E-01  0.3934738E+00  0.3717973E-01  0.3469895E+00  0.1804089E+01
  eigenvectors of nos in ao-basis, column   12
  0.5786049E+00 -0.1817621E+01 -0.2968726E+01 -0.2150614E+00  0.1639960E+01 -0.2105042E-01  0.1356781E+00 -0.3101420E+01
 -0.3315673E+01  0.4490491E+00 -0.1039620E+01 -0.1013081E+01 -0.1663388E+01 -0.4025865E-01
  eigenvectors of nos in ao-basis, column   13
  0.2441361E+00 -0.8252858E+00  0.2586167E+01  0.4702896E-01  0.1531321E+00 -0.1151359E-01  0.3478352E-01 -0.1243877E+00
  0.8742200E+01  0.2056742E+00 -0.4564001E+00 -0.9759296E-01  0.4570739E+01  0.1265962E+01
  eigenvectors of nos in ao-basis, column   14
 -0.8136527E-02  0.7017405E-03 -0.1475880E+01 -0.3976303E-01  0.2343108E+00 -0.4716706E-02  0.9733555E-02 -0.4095155E+00
 -0.2495064E+01  0.2873428E+01 -0.1560103E+00 -0.6292858E-01 -0.4398940E+00 -0.7083199E+00


*****   symmetry block SYM3   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       1.97123721
       2       0.01353781     0.00560978
       3       0.01309235     0.00615749     0.00861921
       4       0.00471092    -0.00322724    -0.00183848     0.00461469
       5       0.00027789     0.00031669     0.00149390     0.00129122
       6      -0.00729666     0.00212022     0.00310072    -0.00113514
       7       0.00000775     0.00001862    -0.00002200    -0.00004764
       8      -0.00079094     0.00018381     0.00004113    -0.00050139
       9       0.00014678    -0.00023011     0.00023610     0.00110652

               Column   5     Column   6     Column   7     Column   8
       5       0.00139351
       6       0.00048345     0.00188560
       7      -0.00003778    -0.00000313     0.00038091
       8      -0.00003994     0.00006586     0.00000430     0.00037883
       9       0.00077179    -0.00000164     0.00000468    -0.00014491

               Column   9
       9       0.00076178

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7        eno   8
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    8   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

               eno   9
 emo    9   0.100000E+01

 occupation numbers of nos
   1.9714566      0.0157297      0.0053368      0.0007991      0.0006242
   0.0003942      0.0003622      0.0001206      0.0000580

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7         no   8
  mo    1   0.999944E+00  -0.690173E-02  -0.324252E-02   0.687207E-02   0.208043E-02   0.634998E-03   0.788028E-03  -0.566689E-03
  mo    2   0.689915E-02   0.576340E+00  -0.122299E+00  -0.458969E+00   0.116047E+00   0.988772E-01   0.134919E+00   0.111005E+00
  mo    3   0.668344E-02   0.700374E+00   0.369027E+00  -0.291578E-01  -0.304040E+00  -0.121053E+00  -0.172333E+00   0.909162E-02
  mo    4   0.237984E-02  -0.309354E+00   0.748713E+00  -0.124226E+00  -0.352872E+00  -0.979930E-02  -0.605524E-01   0.236125E+00
  mo    5   0.147918E-03   0.660385E-01   0.440800E+00   0.642848E-01   0.595726E+00  -0.166665E+00  -0.113119E+00  -0.621679E+00
  mo    6  -0.368789E-02   0.276540E+00   0.769658E-01   0.859952E+00   0.217657E-02   0.183606E+00   0.182908E+00   0.151380E+00
  mo    7   0.386473E-05   0.433229E-03  -0.125170E-01  -0.172692E-01   0.294321E-01   0.765599E+00  -0.638426E+00  -0.698771E-01
  mo    8  -0.401203E-03   0.204182E-01  -0.874894E-01   0.106160E+00   0.327430E+00  -0.472275E+00  -0.608215E+00   0.521020E+00
  mo    9   0.758974E-04  -0.175726E-01   0.283268E+00  -0.133413E+00   0.553713E+00   0.323728E+00   0.353082E+00   0.496079E+00

                no   9
  mo    1  -0.820154E-03
  mo    2   0.623222E+00
  mo    3  -0.485310E+00
  mo    4   0.379809E+00
  mo    5   0.123504E+00
  mo    6   0.296620E+00
  mo    7   0.813570E-02
  mo    8   0.949639E-01
  mo    9  -0.345676E+00
  eigenvectors of nos in ao-basis, column    1
  0.9200159E+00 -0.6361704E-02  0.4203848E-01 -0.4918987E-02 -0.1975459E-01  0.1135855E-02  0.3257562E-03  0.2302822E-01
  0.3943188E-01
  eigenvectors of nos in ao-basis, column    2
 -0.1650549E+01  0.5302938E+00  0.1279076E+01 -0.1591923E-01 -0.7123765E-01  0.1409447E-03  0.4826417E-03  0.6440928E-01
  0.8797664E-01
  eigenvectors of nos in ao-basis, column    3
 -0.2256567E+00  0.9736908E-01  0.3520598E+00  0.3437068E+00  0.7119381E+00 -0.3190197E-02 -0.5095041E-02 -0.6989500E-01
 -0.5154407E-01
  eigenvectors of nos in ao-basis, column    4
 -0.2668067E+01  0.2741084E+01  0.4263908E+00 -0.1548841E+00  0.3028959E-01  0.7801618E-02 -0.3491426E-03 -0.3162114E-01
 -0.2028736E+00
  eigenvectors of nos in ao-basis, column    5
 -0.2010118E+00  0.2420419E+00 -0.6505948E+00  0.5740503E+00 -0.4285383E-01  0.8114025E-01  0.2986981E-01  0.4966089E+00
  0.1240766E+00
  eigenvectors of nos in ao-basis, column    6
 -0.3742746E+00  0.4782679E+00 -0.2270763E+00  0.4532636E+00 -0.3507489E+00 -0.1402045E+00  0.8866757E-01 -0.5549943E-01
  0.1456928E+00
  eigenvectors of nos in ao-basis, column    7
 -0.3649686E+00  0.4772717E+00 -0.3486726E+00  0.5291233E+00 -0.3897955E+00 -0.6020324E-01 -0.1253725E+00  0.2035573E-01
  0.1777741E+00
  eigenvectors of nos in ao-basis, column    8
 -0.4580715E+00  0.6851813E+00  0.2938523E+00  0.5074782E+00 -0.8992771E+00  0.1248071E+00  0.2775917E-01 -0.8799840E+00
  0.1936966E+00
  eigenvectors of nos in ao-basis, column    9
 -0.8269653E+00  0.1325813E+01 -0.1787482E+01 -0.4263412E+00  0.5603083E+00  0.2846667E-02  0.1102059E-02 -0.5513790E-01
  0.1040622E+01


*****   symmetry block SYM4   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       0.00577868
       2      -0.00074309     0.00082811
       3       0.00035796    -0.00000627     0.00026586
       4       0.00015354    -0.00014950     0.00016794     0.00020920
       5      -0.00004957    -0.00013325    -0.00007156    -0.00001354

               Column   5
       5       0.00015398

               eno   1        eno   2        eno   3        eno   4        eno   5
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   0.0059163      0.0007778      0.0003964      0.0001042      0.0000412

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5
  mo    1   0.986835E+00   0.140364E+00  -0.651871E-01   0.261621E-01   0.389935E-01
  mo    2  -0.144989E+00   0.946040E+00   0.303707E-01   0.246545E+00   0.149262E+00
  mo    3   0.637120E-01   0.544589E-01   0.745856E+00   0.128989E+00  -0.648101E+00
  mo    4   0.322365E-01  -0.189626E+00   0.610051E+00   0.226797E+00   0.734441E+00
  mo    5  -0.600292E-02  -0.215365E+00  -0.257612E+00   0.932982E+00  -0.129467E+00
  eigenvectors of nos in ao-basis, column    1
 -0.3104350E+00 -0.6781980E+00  0.4203661E-01  0.1105327E+00  0.9721870E-01
  eigenvectors of nos in ao-basis, column    2
 -0.6067536E+00 -0.1213516E+00 -0.3208104E+00 -0.4917714E+00 -0.1702648E+00
  eigenvectors of nos in ao-basis, column    3
  0.5723840E+00 -0.4038638E+00 -0.7789689E+00 -0.4950654E-01  0.3316244E+00
  eigenvectors of nos in ao-basis, column    4
  0.7042385E+00 -0.1197721E+01  0.6087417E+00 -0.8128380E+00  0.6565548E-01
  eigenvectors of nos in ao-basis, column    5
  0.3568151E+00 -0.3436826E+00 -0.2476204E+00  0.3915389E+00 -0.1148798E+01


 total number of electrons =   10.0000000000



          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!


                        A1  partial gross atomic populations
   ao class       1A1        2A1        3A1        4A1        5A1        6A1 
    O1_ s       1.999741   1.553549  -0.004011   0.001102   0.005817   0.001249
    O1_ p       0.000038   0.000696   1.507644   0.009596   0.003195   0.000050
    O1_ d       0.000000  -0.000399   0.001809   0.000433   0.000465   0.004084
    O1_ f       0.000000   0.000280   0.000513  -0.000018   0.000006   0.000036
    H1_ s      -0.000183   0.333068   0.389105   0.008621   0.000857   0.000067
    H1_ p      -0.000035   0.096944   0.074277  -0.000260   0.000675   0.000576

   ao class       7A1        8A1        9A1       10A1       11A1       12A1 
    O1_ s       0.000080   0.000267   0.000026   0.000009   0.000204   0.000050
    O1_ p       0.000314   0.000179   0.000596   0.000033   0.000055   0.000006
    O1_ d       0.004695   0.000369   0.000011   0.000249   0.000028   0.000050
    O1_ f       0.000009   0.000103   0.000006   0.000076   0.000123   0.000241
    H1_ s       0.000024   0.000261   0.000027   0.000001   0.000011   0.000006
    H1_ p       0.000168  -0.000049   0.000175   0.000203   0.000053   0.000014

   ao class      13A1       14A1       15A1       16A1       17A1       18A1 
    O1_ s       0.000059   0.000020   0.000017   0.000000   0.000003   0.000002
    O1_ p       0.000003   0.000008   0.000015   0.000000   0.000004   0.000000
    O1_ d       0.000115   0.000067   0.000005   0.000029   0.000010  -0.000001
    O1_ f       0.000157   0.000040   0.000002   0.000004   0.000000   0.000002
    H1_ s       0.000017   0.000010   0.000015   0.000011   0.000003   0.000001
    H1_ p       0.000004   0.000011   0.000056   0.000024   0.000036   0.000027

   ao class      19A1       20A1 
    O1_ s       0.000000   0.000000
    O1_ p       0.000000   0.000000
    O1_ d       0.000000   0.000000
    O1_ f       0.000000   0.000000
    H1_ s       0.000003   0.000000
    H1_ p       0.000004   0.000000

                        B1  partial gross atomic populations
   ao class       1B1        2B1        3B1        4B1        5B1        6B1 
    O1_ p       1.143591   0.010887   0.001955   0.000368   0.000495   0.000100
    O1_ d       0.006185   0.000758   0.003478   0.000362   0.000053   0.000010
    O1_ f       0.000536  -0.000006   0.000031   0.000058   0.000042   0.000301
    H1_ s       0.807931   0.010415   0.000023   0.000157   0.000047   0.000010
    H1_ p       0.010330  -0.000923   0.000332   0.000125   0.000184   0.000221

   ao class       7B1        8B1        9B1       10B1       11B1       12B1 
    O1_ p       0.000030   0.000020   0.000015   0.000018  -0.000001   0.000001
    O1_ d       0.000053   0.000002   0.000068   0.000004   0.000000   0.000001
    O1_ f       0.000298   0.000065   0.000004   0.000018   0.000000   0.000000
    H1_ s      -0.000008   0.000006   0.000004   0.000035   0.000007   0.000008
    H1_ p       0.000017   0.000070   0.000007   0.000019   0.000012   0.000000

   ao class      13B1       14B1 
    O1_ p      -0.000001   0.000000
    O1_ d       0.000000   0.000000
    O1_ f       0.000000   0.000000
    H1_ s      -0.000003   0.000000
    H1_ p       0.000009   0.000000

                        B2  partial gross atomic populations
   ao class       1B2        2B2        3B2        4B2        5B2        6B2 
    O1_ p       1.856858   0.013445   0.000206   0.000686   0.000041   0.000009
    O1_ d       0.001963   0.000172   0.004906   0.000016   0.000166   0.000066
    O1_ f       0.000164   0.000001   0.000010   0.000001   0.000165   0.000313
    H1_ p       0.112472   0.002112   0.000215   0.000097   0.000253   0.000007

   ao class       7B2        8B2        9B2 
    O1_ p       0.000011   0.000001   0.000010
    O1_ d       0.000085   0.000027   0.000009
    O1_ f       0.000257   0.000028   0.000000
    H1_ p       0.000009   0.000064   0.000039

                        A2  partial gross atomic populations
   ao class       1A2        2A2        3A2        4A2        5A2 
    O1_ d       0.005096   0.000255   0.000109   0.000045   0.000004
    O1_ f       0.000021   0.000127   0.000244   0.000019   0.000001
    H1_ p       0.000799   0.000396   0.000044   0.000039   0.000036


                        gross atomic populations
     ao           O1_        H1_
      s         3.558184   1.550557
      p         4.551180   0.299886
      d         0.035915   0.000000
      f         0.004278   0.000000
    total       8.149557   1.850443


 Total number of electrons:   10.00000000

