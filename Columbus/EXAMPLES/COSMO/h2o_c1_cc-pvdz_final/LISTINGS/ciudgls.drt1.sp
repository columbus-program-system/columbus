1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      09/20/00    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 using llenci= -1
================================================================================
four external integ    0.25 MB location: local disk    
three external inte    0.25 MB location: local disk    
four external integ    0.25 MB location: local disk    
three external inte    0.25 MB location: local disk    
diagonal integrals     0.12 MB location: local disk    
off-diagonal integr    0.19 MB location: local disk    
computed file size in DP units
fil3w:       32767
fil3x:       32767
fil4w:       32767
fil4x:       32767
ofdgint:       16380
diagint:       24570
computed file size in DP units
fil3w:      262136
fil3x:      262136
fil4w:      262136
fil4x:      262136
ofdgint:      131040
diagint:      196560
 nsubmx= 2 lenci= 4656
global arrays:        23280   (              0.18 MB)
vdisk:                    0   (              0.00 MB)
drt:                 252658   (              0.96 MB)
================================================================================
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core            9999999 DP per process
 CIUDG version 5.9.7 ( 5-Oct-2004)
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 0,
  GSET = 0,
   DAVCOR =10,
  NCOREL = 10
  NROOT = 1
  IVMODE = 3
  NBKITR = 0
  NVBKMN = 1
  NVBKMX = 6
  RTOLBK = 1e-3,
  NITER = 20
  NVCIMN = 1
  NVCIMX = 2
  RTOLCI = 1e-4,
  IDEN  = 1
  CSFPRN = 10,
  frcsub = 2
  cosmocalc = 1
  /end
 ------------------------------------------------------------------------

 ** list of control variables **
 nrfitr =   30      nvrfmx =    2      nvrfmn =    1
 lvlprt =    0      nroot  =    1      noldv  =    0      noldhv =    0
 nunitv =    1      ntype  =    0      nbkitr =    0      niter  =   20
 ivmode =    3      vout   =    0      istrt  =    0      iortls =    0
 nvbkmx =    2      ibktv  =   -1      ibkthv =   -1      frcsub =    2
 nvcimx =    2      icitv  =   -1      icithv =   -1      maxseg =    4
 iden   =    1      itran  =    0      froot  =    0      rtmode =    0
 ftcalc =    1      lrtshift=1.0000    ncouple=    1      skipso  =   F
 ncorel =   10      csfprn  =  10      ctol   = 1.00E-02  davcor  =  10


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-03    1.000E-04

 units and filenames:
    1: ( 6)    ciudgls                                                     
    2: ( 5)    ciudgin                                                     
    3: ( 7)    ciudgsm                                                     
    4: ( 4)    cihdiag                                                     
    5: ( 3)    cihpseu                                                     
    6: (10)    cihvfl                                                      
    7: (11)    civfl                                                       
    8: (12)    diagint                                                     
    9: (13)    ofdgint                                                     
   10: (17)    cidrtfl                                                     
   11: (20)    ciftdfl                                                     
   12: (21)    ciftofl                                                     
   13: (24)    ciftifl                                                     
   14: ( 8)    ciflind                                                     
   15: (15)    civin                                                       
   16: (16)    civout                                                      
   18: (18)    d1fl                                                        
   20: (53)    cifvfl                                                      
   21: (25)    ciscr4                                                      
   22: (41)    ciftotd                                                     
   23: (42)    flacpfd                                                     
   26: (26)    ciscr5                                                      
   27: (47)    restart                                                     
   28: (48)    civout                                                      
   29: (49)    cirefv                                                      
   31: (31)    fil4w                                                       
   32: (32)    fil4x                                                       
   33: (33)    fil3w                                                       
   34: (34)    fil3x                                                       
   36: (36)    aoints                                                      
   37: (37)    aoints2                                                     
   38: (38)    drtfil                                                      
   39: (50)    cisrtif                                                     
   42: (50)    mocoef                                                      
   45: (52)    nocoef_ci                                                   
   46: (53)    civfl_restart                                               
 ------------------------------------------------------------------------

 workspace allocation information: lcore=   9999999 mem1=********** ifirst= -11230422

 integral file titles:
 Hermit Integral Program : SIFS version  ronja.itc.univieThu Sep  8 16:45:14 2005
  cidrt_title                                                                    
  title                                                                          
 mofmt: formatted orbitals label=morbl   ronja.itc.univieThu Sep  8 16:45:15 2005
 SIFS file created by program tran.      ronja.itc.univieThu Sep  8 16:45:15 2005

 core energy values from the integral file:
 energy( 1)=  9.317913916904E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total core repulsion energy =  9.317913916904E+00

 drt header information:
  cidrt_title                                                                    
 spnorb, spnodd, lxyzir,hmult F F 0 0 0 0
 nmot  =    24 niot  =     5 nfct  =     0 nfvt  =     0
 nrow  =    20 nsym  =     1 ssym  =     1 lenbuf=  1600
 nwalk,xbar:         31        1        5       10       15
 nvalwt,nvalw:       31        1        5       10       15
 ncsft:            4656
 total number of valid internal walks:      31
 nvalz,nvaly,nvalx,nvalw =        1       5      10      15

 cisrt info file parameters:
 file number  12 blocksize   4095
 mxbld   4095
 nd4ext,nd2ext,nd0ext   380   190    30
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int    21375    19665     8550     1995      165        0        0        0
 minbl4,minbl3,maxbl2   567   567   570
 maxbuf 32767
 number of external orbitals per symmetry block:  19
 nmsym   1 number of internal orbitals   5

 formula file title:
 Hermit Integral Program : SIFS version  ronja.itc.univieThu Sep  8 16:45:14 2005
  cidrt_title                                                                    
  title                                                                          
 mofmt: formatted orbitals label=morbl   ronja.itc.univieThu Sep  8 16:45:15 2005
 SIFS file created by program tran.      ronja.itc.univieThu Sep  8 16:45:15 2005
  cidrt_title                                                                    
 file nmb.  24 block size     0
 pthz,pthy,pthx,pthw:     1     5    10    15 total internal walks:      31
 maxlp3,n3xtlp,n2xtlp,n1xtlp,n0xtlp,n2lp,n1lp,n0lp     0     0     0     0
 orbsym(*)= 1 1 1 1 1

 setref:        1 references kept,
                0 references were marked as invalid, out of
                1 total.
 limcnvrt: found 1 valid internal walksout of  1
  walks (skipping trailing invalids)
  ... adding  1 segmentation marks segtype= 1
 limcnvrt: found 5 valid internal walksout of  5
  walks (skipping trailing invalids)
  ... adding  5 segmentation marks segtype= 2
 limcnvrt: found 10 valid internal walksout of  10
  walks (skipping trailing invalids)
  ... adding  10 segmentation marks segtype= 3
 limcnvrt: found 15 valid internal walksout of  15
  walks (skipping trailing invalids)
  ... adding  15 segmentation marks segtype= 4

 number of external paths / symmetry
 vertex x     171
 vertex w     190

 lprune: l(*,*,*) pruned with nwalk=       1 nvalwt=       1 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=       5 nvalwt=       4 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=      10 nvalwt=      10 nprune=      14

 lprune: l(*,*,*) pruned with nwalk=      15 nvalwt=      15 nprune=       7



                   segmentation summary for type diagonal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1           1|         1|         0|         1|         0|         1|
 -------------------------------------------------------------------------------
  Y 2           5|        95|         1|         5|         1|         2|
 -------------------------------------------------------------------------------
  X 3          10|      1710|        96|        10|         6|         3|
 -------------------------------------------------------------------------------
  W 4          15|      2850|      1806|        15|        16|         4|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>      4656

                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      10       1       1710          1      10       1
     2  4   1    25      two-ext wz   2X  4 1      15       1       2850          1      15       1
     3  4   3    26      two-ext wx   2X  4 3      15      10       2850       1710      15      10
     4  2   1    11      one-ext yz   1X  2 1       5       1         95          1       5       1
     5  3   2    15      1ex3ex  yx   3X  3 2      10       5       1710         95      10       5
     6  4   2    16      1ex3ex  yw   3X  4 2      15       5       2850         95      15       5
     7  1   1     1      allint zz    OX  1 1       1       1          1          1       1       1
     8  2   2     5      0ex2ex yy    OX  2 2       5       5         95         95       5       5
     9  3   3     6      0ex2ex xx    OX  3 3      10      10       1710       1710      10      10
    10  4   4     7      0ex2ex ww    OX  4 4      15      15       2850       2850      15      15
    11  1   1    75      dg-024ext z  DG  1 1       1       1          1          1       1       1
    12  2   2    45      4exdg024 y   DG  2 2       5       5         95         95       5       5
    13  3   3    46      4exdg024 x   DG  3 3      10      10       1710       1710      10      10
    14  4   4    47      4exdg024 w   DG  4 4      15      15       2850       2850      15      15
----------------------------------------------------------------------------------------------------
 DIAGEL COUNTS: DG0X,DG2X,DG4X= 440 145 30
 diagonal elements written to file   4

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        15 2x:         0 4x:         0
All internal counts: zz :         0 yy:         0 xx:         0 ww:         0
One-external counts: yz :         0 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         0    task #   2:         0    task #   3:         0    task #   4:         0
task #   5:         0    task #   6:         0    task #   7:        -1    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:        14    task #  12:         0
task #  13:         0    task #  14:         0    task #
 reference space has dimension       1

    root           eigenvalues
    ----           ------------
       1         -76.0270328082

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt= 1

         vector  1 from unit 11 written to unit 49 filename cirefv                                                      
 ufvoutnew: ... writing  recamt= 1

         vector  1 from unit 11 written to unit 49 filename cirefv                                                      

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              4656
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               2
 number of roots to converge:                             1
 number of iterations:                                   20
 residual norm convergence criteria:               0.000100

 elast before the main iterative loop 
 elast =  -76.0270328


          starting ci iteration   1

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.31791392

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= T F

################################################################################
 one electron density matrix calculation 
################################################################################


 workspace allocation information: mxcore= 9999997

 space required:

    space required for calls in multd2:
       onex          475
       allin           0
       diagon        950
    max.      ---------
       maxnex        950

    total core space usage:
       maxnex        950
    max k-seg       2850
    max k-ind         15
              ---------
       totmax       6680
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=      10  D0X=      30  D0W=      50
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   2.0000000      2.0000000      2.0000000      2.0000000      2.0000000
   0.0000000      0.0000000      0.0000000      0.0000000      0.0000000
   0.0000000      0.0000000      0.0000000      0.0000000      0.0000000
   0.0000000      0.0000000      0.0000000      0.0000000      0.0000000
   0.0000000      0.0000000      0.0000000      0.0000000


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***

  Confirmation of dielectric energy for ground state
  edielnew =   0.


 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************

 *** qcos is copied to qcosdalton ***

 fepsi =   1.
 *** qcosdalton is mult by fepsi ***

 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 ** Total nuclear repulsion energy ** =   9.35969258317764
 screening nuclear repulsion energy   0.00672980141

 Total-screening nuclear repulsion energy   9.35296278


 Adding T+Vsolv ...
 maxdens  300
 *** End of DALTON-COSMO calculation ***

 DIAGEL COUNTS: DG0X,DG2X,DG4X= 440 145 30
 =========== Executing IN-CORE method ==========
 norm multnew:  1.


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        40 xx:       135 ww:       255
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        20 yw:        25

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        39
task #   9:       127    task #  10:       247    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 rtolcosmo = eo,e,r,ro   0. -85.3449467 -85.4009084  9.35296278  9.31791392
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -76.0479455968  2.0913E-02  2.5188E-01  1.0015E+00  1.0000E-04


dielectric energy =      -0.0104563943
delta diel. energy =       0.0104563943
total Cosmo energy: e(nroot) + repnuc - ediel =     -76.0374892025
delta Cosmo energy =      76.0374892025

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.01   0.00   0.00   0.01         0.    0.0000
    2   25    0   0.00   0.00   0.00   0.00         0.    0.0000
    3   26    0   0.00   0.00   0.00   0.00         0.    0.0000
    4   11    0   0.01   0.00   0.00   0.01         0.    0.0000
    5   15    0   0.00   0.00   0.00   0.00         0.    0.0000
    6   16    0   0.01   0.00   0.00   0.01         0.    0.0000
    7    1    0   0.00   0.00   0.00   0.00         0.    0.0000
    8    5    0   0.00   0.00   0.00   0.00         0.    0.0000
    9    6    0   0.01   0.00   0.00   0.01         0.    0.0000
   10    7    0   0.00   0.00   0.00   0.00         0.    0.0000
   11   75    0   0.01   0.00   0.00   0.01         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.00   0.00   0.00   0.00         0.    0.0000
   14   47    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0600s 
time spent in multnx:                   0.0600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.9800s 

          starting ci iteration   2

 =========== Executing IN-CORE method ==========
 norm multnew:  0.0791428405


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        40 xx:       135 ww:       255
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        20 yw:        25

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        39
task #   9:       127    task #  10:       247    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97633721    -0.21625367

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1    -76.2462585708  1.9831E-01  6.4774E-03  1.5760E-01  1.0000E-04
 mr-sdci #  2  2    -72.0056892796  8.1359E+01  0.0000E+00  1.7220E+00  1.0000E-04


dielectric energy =      -0.0104563943
delta diel. energy =       0.0000000000
total Cosmo energy: e(nroot) + repnuc - ediel =     -76.2358021765
delta Cosmo energy =       0.1983129740

 root number  1 is used to define the new expansion vector.
 diagon: frcsub=2, new matrix-vector products will be computed.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.00   0.00   0.00   0.00         0.    0.0000
    2   25    0   0.00   0.00   0.00   0.00         0.    0.0000
    3   26    0   0.01   0.00   0.00   0.01         0.    0.0000
    4   11    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   15    0   0.01   0.00   0.00   0.01         0.    0.0000
    6   16    0   0.00   0.00   0.00   0.00         0.    0.0000
    7    1    0   0.00   0.00   0.00   0.00         0.    0.0000
    8    5    0   0.01   0.00   0.00   0.01         0.    0.0000
    9    6    0   0.00   0.00   0.00   0.00         0.    0.0000
   10    7    0   0.01   0.00   0.00   0.01         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.01   0.00   0.00   0.01         0.    0.0000
   14   47    0   0.00   0.00   0.00   0.00         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0500s 
time spent in multnx:                   0.0500s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.0600s 

          starting ci iteration   3

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.35296278

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=      10  D0X=      30  D0W=      50
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   1.9999410      1.9890990      1.9747160      1.9730424      1.9712496
   0.0210628      0.0192653      0.0180253      0.0094479      0.0048692
   0.0045091      0.0039718      0.0034594      0.0033229      0.0007692
   0.0007606      0.0004932      0.0004507      0.0004079      0.0003694
   0.0003434      0.0003426      0.0000437      0.0000379


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***

  Confirmation of dielectric energy for ground state
  edielnew =  -0.0111934298


 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************

 *** qcos is copied to qcosdalton ***

 fepsi =   1.
 *** qcosdalton is mult by fepsi ***

 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 ** Total nuclear repulsion energy ** =   9.36307563494872
 screening nuclear repulsion energy   0.00777274227

 Total-screening nuclear repulsion energy   9.35530289


 Adding T+Vsolv ...
 maxdens  300
 *** End of DALTON-COSMO calculation ***

 DIAGEL COUNTS: DG0X,DG2X,DG4X= 440 145 30
 =========== Executing IN-CORE method ==========
 norm multnew:  1.


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        40 xx:       135 ww:       255
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        20 yw:        25

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        39
task #   9:       127    task #  10:       247    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 =========== Executing IN-CORE method ==========
 norm multnew:  0.00222408662


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        40 xx:       135 ww:       255
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        20 yw:        25

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        39
task #   9:       127    task #  10:       247    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 rtolcosmo = eo,e,r,ro   0. -85.5992214 -85.6088698  9.35530289  9.35296278
 rtolcosmo = eo,e,r,ro   0. -81.3586521 -82.3148707  9.35530289  9.35296278
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97731793     0.00318598

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1    -76.2535668738  7.3083E-03  5.4631E-04  3.4900E-02  1.0000E-04
 mr-sdci #  3  2    -72.9595677784  9.5388E-01  0.0000E+00  1.6856E+00  1.0000E-04


dielectric energy =      -0.0119905990
delta diel. energy =       0.0015342047
total Cosmo energy: e(nroot) + repnuc - ediel =     -76.2415762748
delta Cosmo energy =       0.0057740983

 root number  1 is used to define the new expansion vector.
 diagon: frcsub=2, new matrix-vector products will be computed.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.01   0.00   0.00   0.00         0.    0.0000
    2   25    0   0.00   0.00   0.00   0.00         0.    0.0000
    3   26    0   0.01   0.00   0.00   0.01         0.    0.0000
    4   11    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   15    0   0.00   0.00   0.00   0.00         0.    0.0000
    6   16    0   0.01   0.00   0.00   0.01         0.    0.0000
    7    1    0   0.00   0.00   0.00   0.00         0.    0.0000
    8    5    0   0.00   0.00   0.00   0.00         0.    0.0000
    9    6    0   0.01   0.00   0.00   0.01         0.    0.0000
   10    7    0   0.01   0.00   0.00   0.01         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.00   0.00   0.00   0.00         0.    0.0000
   14   47    0   0.01   0.00   0.00   0.00         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0600s 
time spent in multnx:                   0.0400s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.0600s 

          starting ci iteration   4

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.35530289

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=      10  D0X=      30  D0W=      50
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   1.9999216      1.9874921      1.9774607      1.9742837      1.9721820
   0.0193376      0.0176129      0.0136801      0.0096172      0.0053594
   0.0050795      0.0047044      0.0041897      0.0041001      0.0009708
   0.0009686      0.0006317      0.0005415      0.0004748      0.0004721
   0.0004351      0.0004088      0.0000421      0.0000335


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***

  Confirmation of dielectric energy for ground state
  edielnew =  -0.0117192087


 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************

 *** qcos is copied to qcosdalton ***

 fepsi =   1.
 *** qcosdalton is mult by fepsi ***

 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 ** Total nuclear repulsion energy ** =   9.36042967970585
 screening nuclear repulsion energy   0.00742371076

 Total-screening nuclear repulsion energy   9.35300597


 Adding T+Vsolv ...
 maxdens  300
 *** End of DALTON-COSMO calculation ***

 DIAGEL COUNTS: DG0X,DG2X,DG4X= 440 145 30
 =========== Executing IN-CORE method ==========
 norm multnew:  1.


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        40 xx:       135 ww:       255
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        20 yw:        25

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        39
task #   9:       127    task #  10:       247    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 =========== Executing IN-CORE method ==========
 norm multnew:  0.000841908461


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        40 xx:       135 ww:       255
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        20 yw:        25

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        39
task #   9:       127    task #  10:       247    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 rtolcosmo = eo,e,r,ro   0. -85.6088698 -85.6063806  9.35300597  9.35530289
 rtolcosmo = eo,e,r,ro   0. -82.3148707 -82.8303341  9.35300597  9.35530289
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97547070    -0.17309724

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1    -76.2533746201 -1.9225E-04  7.9397E-05  1.6304E-02  1.0000E-04
 mr-sdci #  4  2    -73.4773281484  5.1776E-01  0.0000E+00  1.6272E+00  1.0000E-04


dielectric energy =      -0.0114547213
delta diel. energy =      -0.0005358778
total Cosmo energy: e(nroot) + repnuc - ediel =     -76.2419198988
delta Cosmo energy =       0.0003436241

 root number  1 is used to define the new expansion vector.
 diagon: frcsub=2, new matrix-vector products will be computed.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.00   0.00   0.00   0.00         0.    0.0000
    2   25    0   0.00   0.00   0.00   0.00         0.    0.0000
    3   26    0   0.01   0.00   0.00   0.00         0.    0.0000
    4   11    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   15    0   0.01   0.00   0.00   0.01         0.    0.0000
    6   16    0   0.00   0.00   0.00   0.00         0.    0.0000
    7    1    0   0.01   0.00   0.00   0.00         0.    0.0000
    8    5    0   0.00   0.00   0.00   0.00         0.    0.0000
    9    6    0   0.01   0.00   0.00   0.01         0.    0.0000
   10    7    0   0.00   0.00   0.00   0.00         0.    0.0000
   11   75    0   0.01   0.00   0.00   0.01         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.01   0.00   0.00   0.01         0.    0.0000
   14   47    0   0.00   0.00   0.00   0.00         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0600s 
time spent in multnx:                   0.0400s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1200s 

          starting ci iteration   5

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.35300597

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=      10  D0X=      30  D0W=      50
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   1.9999174      1.9865520      1.9759118      1.9721728      1.9699117
   0.0212846      0.0194435      0.0148250      0.0103597      0.0055751
   0.0052977      0.0048743      0.0043363      0.0042431      0.0010296
   0.0010269      0.0006695      0.0005822      0.0005034      0.0005022
   0.0004585      0.0004387      0.0000488      0.0000354


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***

  Confirmation of dielectric energy for ground state
  edielnew =  -0.011551886


 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************

 *** qcos is copied to qcosdalton ***

 fepsi =   1.
 *** qcosdalton is mult by fepsi ***

 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 ** Total nuclear repulsion energy ** =   9.36095227875974
 screening nuclear repulsion energy   0.00755837353

 Total-screening nuclear repulsion energy   9.35339391


 Adding T+Vsolv ...
 maxdens  300
 *** End of DALTON-COSMO calculation ***

 DIAGEL COUNTS: DG0X,DG2X,DG4X= 440 145 30
 =========== Executing IN-CORE method ==========
 norm multnew:  1.


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        40 xx:       135 ww:       255
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        20 yw:        25

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        39
task #   9:       127    task #  10:       247    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 =========== Executing IN-CORE method ==========
 norm multnew:  6.93526479E-05


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        40 xx:       135 ww:       255
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        20 yw:        25

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        39
task #   9:       127    task #  10:       247    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 rtolcosmo = eo,e,r,ro   0. -85.6063806 -85.6070246  9.35339391  9.35300597
 rtolcosmo = eo,e,r,ro   0. -82.8303341 -81.8901491  9.35339391  9.35300597
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97594644     0.11671891

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1    -76.2536306957  2.5608E-04  2.3011E-05  7.1128E-03  1.0000E-04
 mr-sdci #  5  2    -72.5367551608 -9.4057E-01  0.0000E+00  1.9566E+00  1.0000E-04


dielectric energy =      -0.0116500585
delta diel. energy =       0.0001953373
total Cosmo energy: e(nroot) + repnuc - ediel =     -76.2419806372
delta Cosmo energy =       0.0000607384

 root number  1 is used to define the new expansion vector.
 diagon: frcsub=2, new matrix-vector products will be computed.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.00   0.00   0.00   0.00         0.    0.0000
    2   25    0   0.00   0.00   0.00   0.00         0.    0.0000
    3   26    0   0.01   0.00   0.00   0.01         0.    0.0000
    4   11    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   15    0   0.00   0.00   0.00   0.00         0.    0.0000
    6   16    0   0.01   0.00   0.00   0.01         0.    0.0000
    7    1    0   0.00   0.00   0.00   0.00         0.    0.0000
    8    5    0   0.01   0.01   0.00   0.01         0.    0.0000
    9    6    0   0.00   0.00   0.00   0.00         0.    0.0000
   10    7    0   0.01   0.00   0.00   0.01         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.01   0.00   0.00   0.01         0.    0.0000
   14   47    0   0.00   0.00   0.00   0.00         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0500s 
time spent in multnx:                   0.0500s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0100s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            1.0600s 

          starting ci iteration   6

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.35339391

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=      10  D0X=      30  D0W=      50
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   1.9999196      1.9867499      1.9765635      1.9726755      1.9704295
   0.0210709      0.0193068      0.0144189      0.0101687      0.0053907
   0.0051274      0.0047125      0.0041954      0.0041062      0.0010057
   0.0010045      0.0006530      0.0005668      0.0004896      0.0004892
   0.0004457      0.0004276      0.0000489      0.0000335


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***

  Confirmation of dielectric energy for ground state
  edielnew =  -0.0116351967


 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************

 *** qcos is copied to qcosdalton ***

 fepsi =   1.
 *** qcosdalton is mult by fepsi ***

 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 ** Total nuclear repulsion energy ** =   9.36083464233283
 screening nuclear repulsion energy   0.00753972592

 Total-screening nuclear repulsion energy   9.35329492


 Adding T+Vsolv ...
 maxdens  300
 *** End of DALTON-COSMO calculation ***

 DIAGEL COUNTS: DG0X,DG2X,DG4X= 440 145 30
 =========== Executing IN-CORE method ==========
 norm multnew:  1.


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        40 xx:       135 ww:       255
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        20 yw:        25

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        39
task #   9:       127    task #  10:       247    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 =========== Executing IN-CORE method ==========
 norm multnew:  4.24574988E-05


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        40 xx:       135 ww:       255
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        20 yw:        25

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        39
task #   9:       127    task #  10:       247    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 rtolcosmo = eo,e,r,ro   0. -85.6070246 -85.6069103  9.35329492  9.35339391
 rtolcosmo = eo,e,r,ro   0. -81.8901491 -82.5119084  9.35329492  9.35339391
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97552724    -0.19591392

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  6  1    -76.2536153391 -1.5357E-05  4.5589E-06  3.7742E-03  1.0000E-04
 mr-sdci #  6  2    -73.1586134448  6.2186E-01  0.0000E+00  1.7331E+00  1.0000E-04


dielectric energy =      -0.0116203567
delta diel. energy =      -0.0000297018
total Cosmo energy: e(nroot) + repnuc - ediel =     -76.2419949824
delta Cosmo energy =       0.0000143452

 root number  1 is used to define the new expansion vector.
 diagon: frcsub=2, new matrix-vector products will be computed.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.00   0.00   0.00   0.00         0.    0.0000
    2   25    0   0.00   0.00   0.00   0.00         0.    0.0000
    3   26    0   0.01   0.00   0.00   0.01         0.    0.0000
    4   11    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   15    0   0.01   0.00   0.00   0.01         0.    0.0000
    6   16    0   0.00   0.00   0.00   0.00         0.    0.0000
    7    1    0   0.00   0.00   0.00   0.00         0.    0.0000
    8    5    0   0.01   0.00   0.00   0.01         0.    0.0000
    9    6    0   0.00   0.00   0.00   0.00         0.    0.0000
   10    7    0   0.01   0.00   0.00   0.00         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.01   0.00   0.00   0.01         0.    0.0000
   13   46    0   0.00   0.00   0.00   0.00         0.    0.0000
   14   47    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0600s 
time spent in multnx:                   0.0500s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.0900s 

          starting ci iteration   7

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.35329492

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=      10  D0X=      30  D0W=      50
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   1.9999186      1.9865325      1.9762010      1.9721927      1.9699035
   0.0215017      0.0197159      0.0146665      0.0103436      0.0054545
   0.0051879      0.0047646      0.0042423      0.0041516      0.0010183
   0.0010158      0.0006602      0.0005726      0.0004952      0.0004944
   0.0004503      0.0004327      0.0000501      0.0000336


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***

  Confirmation of dielectric energy for ground state
  edielnew =  -0.0116271421


 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************

 *** qcos is copied to qcosdalton ***

 fepsi =   1.
 *** qcosdalton is mult by fepsi ***

 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 ** Total nuclear repulsion energy ** =   9.36082795882421
 screening nuclear repulsion energy   0.0075498163

 Total-screening nuclear repulsion energy   9.35327814


 Adding T+Vsolv ...
 maxdens  300
 *** End of DALTON-COSMO calculation ***

 DIAGEL COUNTS: DG0X,DG2X,DG4X= 440 145 30
 =========== Executing IN-CORE method ==========
 norm multnew:  1.


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        40 xx:       135 ww:       255
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        20 yw:        25

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        39
task #   9:       127    task #  10:       247    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 =========== Executing IN-CORE method ==========
 norm multnew:  4.04353929E-06


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        40 xx:       135 ww:       255
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        20 yw:        25

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        39
task #   9:       127    task #  10:       247    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 rtolcosmo = eo,e,r,ro   0. -85.6069103 -85.6069106  9.35327814  9.35329492
 rtolcosmo = eo,e,r,ro   0. -82.5119084 -82.183051  9.35327814  9.35329492
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97564481     0.11562184

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  7  1    -76.2536324369  1.7098E-05  1.4294E-06  1.7508E-03  1.0000E-04
 mr-sdci #  7  2    -72.8297728629 -3.2884E-01  0.0000E+00  1.9427E+00  1.0000E-04


dielectric energy =      -0.0116339358
delta diel. energy =       0.0000135791
total Cosmo energy: e(nroot) + repnuc - ediel =     -76.2419985011
delta Cosmo energy =       0.0000035187

 root number  1 is used to define the new expansion vector.
 diagon: frcsub=2, new matrix-vector products will be computed.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.00   0.00   0.00   0.00         0.    0.0000
    2   25    0   0.01   0.00   0.00   0.00         0.    0.0000
    3   26    0   0.00   0.00   0.00   0.00         0.    0.0000
    4   11    0   0.01   0.00   0.00   0.01         0.    0.0000
    5   15    0   0.00   0.00   0.00   0.00         0.    0.0000
    6   16    0   0.00   0.00   0.00   0.00         0.    0.0000
    7    1    0   0.01   0.00   0.00   0.01         0.    0.0000
    8    5    0   0.00   0.00   0.00   0.00         0.    0.0000
    9    6    0   0.01   0.00   0.00   0.01         0.    0.0000
   10    7    0   0.00   0.00   0.00   0.00         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.01   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.00   0.00   0.00   0.00         0.    0.0000
   14   47    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0600s 
time spent in multnx:                   0.0400s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.0600s 

          starting ci iteration   8

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.35327814

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=      10  D0X=      30  D0W=      50
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   1.9999192      1.9865885      1.9763607      1.9723156      1.9700315
   0.0214414      0.0196778      0.0145615      0.0102913      0.0054137
   0.0051499      0.0047292      0.0042121      0.0041219      0.0010123
   0.0010085      0.0006553      0.0005679      0.0004914      0.0004907
   0.0004469      0.0004296      0.0000502      0.0000331


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***

  Confirmation of dielectric energy for ground state
  edielnew =  -0.0116314087


 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************

 *** qcos is copied to qcosdalton ***

 fepsi =   1.
 *** qcosdalton is mult by fepsi ***

 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 ** Total nuclear repulsion energy ** =   9.36079689758209
 screening nuclear repulsion energy   0.00754686887

 Total-screening nuclear repulsion energy   9.35325003


 Adding T+Vsolv ...
 maxdens  300
 *** End of DALTON-COSMO calculation ***

 DIAGEL COUNTS: DG0X,DG2X,DG4X= 440 145 30
 =========== Executing IN-CORE method ==========
 norm multnew:  1.


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        40 xx:       135 ww:       255
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        20 yw:        25

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        39
task #   9:       127    task #  10:       247    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 =========== Executing IN-CORE method ==========
 norm multnew:  2.61957985E-06


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        40 xx:       135 ww:       255
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        20 yw:        25

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        39
task #   9:       127    task #  10:       247    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 rtolcosmo = eo,e,r,ro   0. -85.6069106 -85.6068783  9.35325003  9.35327814
 rtolcosmo = eo,e,r,ro   0. -82.183051 -82.5657547  9.35325003  9.35327814
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97554035    -0.19309127

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  8  1    -76.2536282771 -4.1598E-06  2.9676E-07  9.4770E-04  1.0000E-04
 mr-sdci #  8  2    -73.2125046585  3.8273E-01  0.0000E+00  1.7571E+00  1.0000E-04


dielectric energy =      -0.0116288825
delta diel. energy =      -0.0000050533
total Cosmo energy: e(nroot) + repnuc - ediel =     -76.2419993946
delta Cosmo energy =       0.0000008935

 root number  1 is used to define the new expansion vector.
 diagon: frcsub=2, new matrix-vector products will be computed.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.00   0.00   0.00   0.00         0.    0.0000
    2   25    0   0.00   0.00   0.00   0.00         0.    0.0000
    3   26    0   0.01   0.00   0.00   0.00         0.    0.0000
    4   11    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   15    0   0.01   0.00   0.00   0.01         0.    0.0000
    6   16    0   0.00   0.00   0.00   0.00         0.    0.0000
    7    1    0   0.00   0.00   0.00   0.00         0.    0.0000
    8    5    0   0.01   0.00   0.00   0.00         0.    0.0000
    9    6    0   0.00   0.00   0.00   0.00         0.    0.0000
   10    7    0   0.01   0.00   0.00   0.00         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.01   0.00   0.00   0.00         0.    0.0000
   14   47    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0600s 
time spent in multnx:                   0.0200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.0600s 

          starting ci iteration   9

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.35325003

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=      10  D0X=      30  D0W=      50
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   1.9999189      1.9865361      1.9762697      1.9721946      1.9698999
   0.0215483      0.0197793      0.0146234      0.0103342      0.0054300
   0.0051654      0.0047426      0.0042244      0.0041337      0.0010152
   0.0010110      0.0006569      0.0005692      0.0004927      0.0004919
   0.0004480      0.0004307      0.0000505      0.0000331


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***

  Confirmation of dielectric energy for ground state
  edielnew =  -0.0116303856


 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************

 *** qcos is copied to qcosdalton ***

 fepsi =   1.
 *** qcosdalton is mult by fepsi ***

 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 ** Total nuclear repulsion energy ** =   9.36079441515800
 screening nuclear repulsion energy   0.00754912707

 Total-screening nuclear repulsion energy   9.35324529


 Adding T+Vsolv ...
 maxdens  300
 *** End of DALTON-COSMO calculation ***

 DIAGEL COUNTS: DG0X,DG2X,DG4X= 440 145 30
 =========== Executing IN-CORE method ==========
 norm multnew:  1.


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        40 xx:       135 ww:       255
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        20 yw:        25

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        39
task #   9:       127    task #  10:       247    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 =========== Executing IN-CORE method ==========
 norm multnew:  2.74340178E-07


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        40 xx:       135 ww:       255
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        20 yw:        25

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        39
task #   9:       127    task #  10:       247    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 rtolcosmo = eo,e,r,ro   0. -85.6068783 -85.6068768  9.35324529  9.35325003
 rtolcosmo = eo,e,r,ro   0. -82.5657547 -82.3291207  9.35324529  9.35325003
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97557140     0.11697339

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  9  1    -76.2536315146  3.2375E-06  9.6709E-08  4.5463E-04  1.0000E-04
 mr-sdci #  9  2    -72.9758754460 -2.3663E-01  0.0000E+00  1.9670E+00  1.0000E-04


dielectric energy =      -0.0116318891
delta diel. energy =       0.0000030065
total Cosmo energy: e(nroot) + repnuc - ediel =     -76.2419996256
delta Cosmo energy =       0.0000002310

 root number  1 is used to define the new expansion vector.
 diagon: frcsub=2, new matrix-vector products will be computed.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.00   0.00   0.00   0.00         0.    0.0000
    2   25    0   0.00   0.00   0.00   0.00         0.    0.0000
    3   26    0   0.01   0.00   0.00   0.01         0.    0.0000
    4   11    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   15    0   0.01   0.00   0.00   0.01         0.    0.0000
    6   16    0   0.00   0.00   0.00   0.00         0.    0.0000
    7    1    0   0.00   0.00   0.00   0.00         0.    0.0000
    8    5    0   0.01   0.00   0.00   0.01         0.    0.0000
    9    6    0   0.00   0.00   0.00   0.00         0.    0.0000
   10    7    0   0.01   0.00   0.00   0.01         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.01   0.00   0.00   0.01         0.    0.0000
   14   47    0   0.00   0.00   0.00   0.00         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0500s 
time spent in multnx:                   0.0500s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            1.0900s 

          starting ci iteration  10

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.35324529

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=      10  D0X=      30  D0W=      50
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   1.9999191      1.9865519      1.9763110      1.9722271      1.9699339
   0.0215313      0.0197680      0.0145962      0.0103199      0.0054196
   0.0051558      0.0047337      0.0042169      0.0041263      0.0010135
   0.0010090      0.0006556      0.0005679      0.0004917      0.0004910
   0.0004472      0.0004299      0.0000505      0.0000330


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***

  Confirmation of dielectric energy for ground state
  edielnew =  -0.0116313243


 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************

 *** qcos is copied to qcosdalton ***

 fepsi =   1.
 *** qcosdalton is mult by fepsi ***

 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 ** Total nuclear repulsion energy ** =   9.36078689684562
 screening nuclear repulsion energy   0.00754848485

 Total-screening nuclear repulsion energy   9.35323841


 Adding T+Vsolv ...
 maxdens  300
 *** End of DALTON-COSMO calculation ***

 DIAGEL COUNTS: DG0X,DG2X,DG4X= 440 145 30
 =========== Executing IN-CORE method ==========
 norm multnew:  1.


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        40 xx:       135 ww:       255
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        20 yw:        25

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        39
task #   9:       127    task #  10:       247    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 =========== Executing IN-CORE method ==========
 norm multnew:  1.75300294E-07


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        40 xx:       135 ww:       255
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        20 yw:        25

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        39
task #   9:       127    task #  10:       247    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 rtolcosmo = eo,e,r,ro   0. -85.6068768 -85.6068689  9.35323841  9.35324529
 rtolcosmo = eo,e,r,ro   0. -82.3291207 -82.5958351  9.35323841  9.35324529
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97554440    -0.19040904

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 10  1    -76.2536304457 -1.0689E-06  2.0629E-08  2.4627E-04  1.0000E-04
 mr-sdci # 10  2    -73.2425966413  2.6672E-01  0.0000E+00  1.7922E+00  1.0000E-04


dielectric energy =      -0.0116307595
delta diel. energy =      -0.0000011295
total Cosmo energy: e(nroot) + repnuc - ediel =     -76.2419996862
delta Cosmo energy =       0.0000000606

 root number  1 is used to define the new expansion vector.
 diagon: frcsub=2, new matrix-vector products will be computed.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.00   0.00   0.00   0.00         0.    0.0000
    2   25    0   0.01   0.00   0.00   0.01         0.    0.0000
    3   26    0   0.00   0.00   0.00   0.00         0.    0.0000
    4   11    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   15    0   0.02   0.00   0.00   0.01         0.    0.0000
    6   16    0   0.00   0.00   0.00   0.00         0.    0.0000
    7    1    0   0.00   0.00   0.00   0.00         0.    0.0000
    8    5    0   0.01   0.00   0.00   0.01         0.    0.0000
    9    6    0   0.00   0.00   0.00   0.00         0.    0.0000
   10    7    0   0.00   0.00   0.00   0.00         0.    0.0000
   11   75    0   0.01   0.00   0.00   0.01         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.00   0.00   0.00   0.00         0.    0.0000
   14   47    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0600s 
time spent in multnx:                   0.0500s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.0700s 

          starting ci iteration  11

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.35323841

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=      10  D0X=      30  D0W=      50
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   1.9999190      1.9865385      1.9762873      1.9721959      1.9698999
   0.0215587      0.0197939      0.0146123      0.0103308      0.0054239
   0.0051599      0.0047372      0.0042202      0.0041295      0.0010142
   0.0010097      0.0006560      0.0005682      0.0004921      0.0004913
   0.0004475      0.0004302      0.0000506      0.0000330


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***

  Confirmation of dielectric energy for ground state
  edielnew =  -0.0116311879


 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************

 *** qcos is copied to qcosdalton ***

 fepsi =   1.
 *** qcosdalton is mult by fepsi ***

 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 ** Total nuclear repulsion energy ** =   9.36078645320349
 screening nuclear repulsion energy   0.00754912252

 Total-screening nuclear repulsion energy   9.35323733


 Adding T+Vsolv ...
 maxdens  300
 *** End of DALTON-COSMO calculation ***

 DIAGEL COUNTS: DG0X,DG2X,DG4X= 440 145 30
 =========== Executing IN-CORE method ==========
 norm multnew:  1.


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        40 xx:       135 ww:       255
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        20 yw:        25

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        39
task #   9:       127    task #  10:       247    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 =========== Executing IN-CORE method ==========
 norm multnew:  1.97293911E-08


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        40 xx:       135 ww:       255
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        20 yw:        25

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        39
task #   9:       127    task #  10:       247    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 rtolcosmo = eo,e,r,ro   0. -85.6068689 -85.6068686  9.35323733  9.35323841
 rtolcosmo = eo,e,r,ro   0. -82.5958351 -82.446661  9.35323733  9.35323841
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97555285     0.11825532

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 11  1    -76.2536313187  8.7294E-07  6.8960E-09  1.2148E-04  1.0000E-04
 mr-sdci # 11  2    -73.0934236495 -1.4917E-01  0.0000E+00  1.9744E+00  1.0000E-04


dielectric energy =      -0.0116316163
delta diel. energy =       0.0000008568
total Cosmo energy: e(nroot) + repnuc - ediel =     -76.2419997023
delta Cosmo energy =       0.0000000161

 root number  1 is used to define the new expansion vector.
 diagon: frcsub=2, new matrix-vector products will be computed.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.01   0.00   0.00   0.01         0.    0.0000
    2   25    0   0.00   0.00   0.00   0.00         0.    0.0000
    3   26    0   0.00   0.00   0.00   0.00         0.    0.0000
    4   11    0   0.01   0.00   0.00   0.00         0.    0.0000
    5   15    0   0.00   0.00   0.00   0.00         0.    0.0000
    6   16    0   0.01   0.00   0.00   0.01         0.    0.0000
    7    1    0   0.00   0.00   0.00   0.00         0.    0.0000
    8    5    0   0.00   0.00   0.00   0.00         0.    0.0000
    9    6    0   0.01   0.00   0.00   0.01         0.    0.0000
   10    7    0   0.00   0.00   0.00   0.00         0.    0.0000
   11   75    0   0.01   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.00   0.00   0.00   0.00         0.    0.0000
   14   47    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0600s 
time spent in multnx:                   0.0400s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            1.1100s 

          starting ci iteration  12

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.35323733

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=      10  D0X=      30  D0W=      50
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   1.9999191      1.9865429      1.9762983      1.9722049      1.9699093
   0.0215538      0.0197903      0.0146050      0.0103269      0.0054212
   0.0051574      0.0047349      0.0042183      0.0041275      0.0010138
   0.0010091      0.0006557      0.0005678      0.0004918      0.0004910
   0.0004472      0.0004300      0.0000506      0.0000330


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***

  Confirmation of dielectric energy for ground state
  edielnew =  -0.0116315078


 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************

 *** qcos is copied to qcosdalton ***

 fepsi =   1.
 *** qcosdalton is mult by fepsi ***

 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 ** Total nuclear repulsion energy ** =   9.36078470193880
 screening nuclear repulsion energy   0.00754900742

 Total-screening nuclear repulsion energy   9.35323569


 Adding T+Vsolv ...
 maxdens  300
 *** End of DALTON-COSMO calculation ***

 DIAGEL COUNTS: DG0X,DG2X,DG4X= 440 145 30
 =========== Executing IN-CORE method ==========
 norm multnew:  1.


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        40 xx:       135 ww:       255
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        20 yw:        25

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        39
task #   9:       127    task #  10:       247    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 =========== Executing IN-CORE method ==========
 norm multnew:  1.23892722E-08


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        40 xx:       135 ww:       255
One-external counts: yz :        25 yx:       130 yw:       175
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        20 yw:        25

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        39
task #   9:       127    task #  10:       247    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 rtolcosmo = eo,e,r,ro   0. -85.6068686 -85.6068668  9.35323569  9.35323733
 rtolcosmo = eo,e,r,ro   0. -82.446661 -82.6238868  9.35323569  9.35323733
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97554569    -0.18785268

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 12  1    -76.2536311060 -2.1267E-07  1.5005E-09  6.5529E-05  1.0000E-04
 mr-sdci # 12  2    -73.2706510838  1.7723E-01  0.0000E+00  1.8285E+00  1.0000E-04


dielectric energy =      -0.0116313993
delta diel. energy =      -0.0000002170
total Cosmo energy: e(nroot) + repnuc - ediel =     -76.2419997067
delta Cosmo energy =       0.0000000043


 mr-sdci  convergence criteria satisfied after 12 iterations.

 *************************************************
 ***Final Calc of density matrix for cosmo calc***
 *************************************************

 entering drivercid: firstcall,firstnonref= F T
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=      10  D0X=      30  D0W=      50
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   1.9999190      1.9865394      1.9762920      1.9721967      1.9699003
   0.0215610      0.0197971      0.0146093      0.0103298      0.0054224
   0.0051585      0.0047359      0.0042192      0.0041284      0.0010140
   0.0010093      0.0006558      0.0005679      0.0004919      0.0004911
   0.0004473      0.0004301      0.0000506      0.0000330


 total number of electrons =   10.0000000000


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***


 **************************************************
 ***Final cosmo calculation for ground state*******
 **************************************************

  ediel before cosmo(4 =  -0.0116313993
  elast before cosmo(4 =  -76.2419997


 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 12  1    -76.2536311060 -2.1267E-07  1.5005E-09  6.5529E-05  1.0000E-04

####################CIUDGINFO####################

   ci vector at position   1 energy=  -76.253631106007

################END OF CIUDGINFO################


    1 of the   2 expansion vectors are transformed.
    1 of the   1 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
 maximum overlap with reference  1(overlap=  0.975545687)

information on vector: 1from unit 11 written to unit 16filename civout                                                      


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -76.2536311060

                                                       internal orbitals

                                          level       1    2    3    4    5

                                          orbital     1    2    3    4    5

                                         symmetry   a    a    a    a    a  

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1  0.975546                        +-   +-   +-   +-   +- 
 y   1  1      21  0.015924              1( a  )   +-   +-   +-    -   +- 
 y   1  1      42 -0.010128              3( a  )   +-   +-    -   +-   +- 
 x   1  1     107  0.012261    1( a  )   6( a  )   +-   +-   +-    -    - 
 x   1  1     110  0.017598    4( a  )   6( a  )   +-   +-   +-    -    - 
 x   1  1     111  0.032630    5( a  )   6( a  )   +-   +-   +-    -    - 
 x   1  1     127 -0.014575    3( a  )   9( a  )   +-   +-   +-    -    - 
 x   1  1     140 -0.011062    8( a  )  10( a  )   +-   +-   +-    -    - 
 x   1  1     151  0.011935   10( a  )  11( a  )   +-   +-   +-    -    - 
 x   1  1     195  0.010382    8( a  )  15( a  )   +-   +-   +-    -    - 
 x   1  1     198  0.010747   11( a  )  15( a  )   +-   +-   +-    -    - 
 x   1  1     231 -0.014305   15( a  )  17( a  )   +-   +-   +-    -    - 
 x   1  1     279 -0.025194    2( a  )   6( a  )   +-   +-    -   +-    - 
 x   1  1     280 -0.014425    3( a  )   6( a  )   +-   +-    -   +-    - 
 x   1  1     288  0.027289    6( a  )   7( a  )   +-   +-    -   +-    - 
 x   1  1     299  0.020216    4( a  )   9( a  )   +-   +-    -   +-    - 
 x   1  1     306  0.013869    3( a  )  10( a  )   +-   +-    -   +-    - 
 x   1  1     321 -0.011404    9( a  )  11( a  )   +-   +-    -   +-    - 
 x   1  1     339 -0.010150    6( a  )  13( a  )   +-   +-    -   +-    - 
 x   1  1     354  0.011089    9( a  )  14( a  )   +-   +-    -   +-    - 
 x   1  1     376 -0.010384    4( a  )  16( a  )   +-   +-    -   +-    - 
 x   1  1     386  0.011313   14( a  )  16( a  )   +-   +-    -   +-    - 
 x   1  1     419 -0.012171   16( a  )  18( a  )   +-   +-    -   +-    - 
 x   1  1     446 -0.023536    2( a  )   5( a  )   +-   +-    -    -   +- 
 x   1  1     447 -0.021875    3( a  )   5( a  )   +-   +-    -    -   +- 
 x   1  1     457  0.013387    4( a  )   7( a  )   +-   +-    -    -   +- 
 x   1  1     458  0.018937    5( a  )   7( a  )   +-   +-    -    -   +- 
 x   1  1     462 -0.019952    3( a  )   8( a  )   +-   +-    -    -   +- 
 x   1  1     509 -0.012376    5( a  )  13( a  )   +-   +-    -    -   +- 
 x   1  1     561 -0.011564    3( a  )  17( a  )   +-   +-    -    -   +- 
 x   1  1     620 -0.015089    1( a  )   6( a  )   +-    -   +-   +-    - 
 x   1  1     660 -0.015051    6( a  )  11( a  )   +-    -   +-   +-    - 
 x   1  1     787 -0.011184    1( a  )   5( a  )   +-    -   +-    -   +- 
 x   1  1     952  0.010326    1( a  )   2( a  )   +-    -    -   +-   +- 
 x   1  1     967  0.010343    1( a  )   7( a  )   +-    -    -   +-   +- 
 w   1  1    1827 -0.047704    6( a  )   6( a  )   +-   +-   +-   +-      
 w   1  1    1872 -0.012879   11( a  )  11( a  )   +-   +-   +-   +-      
 w   1  1    1901  0.010612    4( a  )  14( a  )   +-   +-   +-   +-      
 w   1  1    1926 -0.012415   15( a  )  15( a  )   +-   +-   +-   +-      
 w   1  1    1935  0.010876    9( a  )  16( a  )   +-   +-   +-   +-      
 w   1  1    1942 -0.012039   16( a  )  16( a  )   +-   +-   +-   +-      
 w   1  1    2015 -0.013421    4( a  )   6( a  )   +-   +-   +-   +     - 
 w   1  1    2016 -0.034833    5( a  )   6( a  )   +-   +-   +-   +     - 
 w   1  1    2035  0.011980    3( a  )   9( a  )   +-   +-   +-   +     - 
 w   1  1    2057 -0.010206    6( a  )  11( a  )   +-   +-   +-   +     - 
 w   1  1    2119 -0.010030    3( a  )  16( a  )   +-   +-   +-   +     - 
 w   1  1    2187 -0.011809    1( a  )   1( a  )   +-   +-   +-        +- 
 w   1  1    2191 -0.015272    2( a  )   3( a  )   +-   +-   +-        +- 
 w   1  1    2192 -0.023010    3( a  )   3( a  )   +-   +-   +-        +- 
 w   1  1    2193 -0.013274    1( a  )   4( a  )   +-   +-   +-        +- 
 w   1  1    2196 -0.013810    4( a  )   4( a  )   +-   +-   +-        +- 
 w   1  1    2200 -0.016711    4( a  )   5( a  )   +-   +-   +-        +- 
 w   1  1    2201 -0.033352    5( a  )   5( a  )   +-   +-   +-        +- 
 w   1  1    2222 -0.014464    8( a  )   8( a  )   +-   +-   +-        +- 
 w   1  1    2246 -0.016530    5( a  )  11( a  )   +-   +-   +-        +- 
 w   1  1    2267  0.012648    3( a  )  13( a  )   +-   +-   +-        +- 
 w   1  1    2306 -0.010511   15( a  )  15( a  )   +-   +-   +-        +- 
 w   1  1    2339 -0.011567   17( a  )  17( a  )   +-   +-   +-        +- 
 w   1  1    2393  0.023820    2( a  )   6( a  )   +-   +-   +    +-    - 
 w   1  1    2394  0.014555    3( a  )   6( a  )   +-   +-   +    +-    - 
 w   1  1    2403  0.027341    6( a  )   7( a  )   +-   +-   +    +-    - 
 w   1  1    2424 -0.010808    3( a  )  10( a  )   +-   +-   +    +-    - 
 w   1  1    2460 -0.011293    6( a  )  13( a  )   +-   +-   +    +-    - 
 w   1  1    2568  0.015553    1( a  )   2( a  )   +-   +-   +     -   +- 
 w   1  1    2570  0.015446    1( a  )   3( a  )   +-   +-   +     -   +- 
 w   1  1    2574  0.019842    2( a  )   4( a  )   +-   +-   +     -   +- 
 w   1  1    2575  0.033010    3( a  )   4( a  )   +-   +-   +     -   +- 
 w   1  1    2578  0.018203    2( a  )   5( a  )   +-   +-   +     -   +- 
 w   1  1    2591  0.011123    4( a  )   7( a  )   +-   +-   +     -   +- 
 w   1  1    2592  0.020461    5( a  )   7( a  )   +-   +-   +     -   +- 
 w   1  1    2620  0.013008    9( a  )  10( a  )   +-   +-   +     -   +- 
 w   1  1    2624  0.010012    3( a  )  11( a  )   +-   +-   +     -   +- 
 w   1  1    2660 -0.012332    3( a  )  14( a  )   +-   +-   +     -   +- 
 w   1  1    2757 -0.012175    1( a  )   1( a  )   +-   +-        +-   +- 
 w   1  1    2759 -0.023761    2( a  )   2( a  )   +-   +-        +-   +- 
 w   1  1    2761 -0.025100    2( a  )   3( a  )   +-   +-        +-   +- 
 w   1  1    2762 -0.032753    3( a  )   3( a  )   +-   +-        +-   +- 
 w   1  1    2763 -0.017199    1( a  )   4( a  )   +-   +-        +-   +- 
 w   1  1    2766 -0.025892    4( a  )   4( a  )   +-   +-        +-   +- 
 w   1  1    2779 -0.024182    2( a  )   7( a  )   +-   +-        +-   +- 
 w   1  1    2780 -0.011657    3( a  )   7( a  )   +-   +-        +-   +- 
 w   1  1    2784 -0.021197    7( a  )   7( a  )   +-   +-        +-   +- 
 w   1  1    2801 -0.014347    9( a  )   9( a  )   +-   +-        +-   +- 
 w   1  1    2837  0.014776    3( a  )  13( a  )   +-   +-        +-   +- 
 w   1  1    2847 -0.010257   13( a  )  13( a  )   +-   +-        +-   +- 
 w   1  1    2962  0.025038    1( a  )   6( a  )   +-   +    +-   +-    - 
 w   1  1    2965  0.017671    4( a  )   6( a  )   +-   +    +-   +-    - 
 w   1  1    2980  0.014176    6( a  )   8( a  )   +-   +    +-   +-    - 
 w   1  1    3007 -0.020296    6( a  )  11( a  )   +-   +    +-   +-    - 
 w   1  1    3043 -0.016732    6( a  )  14( a  )   +-   +    +-   +-    - 
 w   1  1    3055 -0.010604    4( a  )  15( a  )   +-   +    +-   +-    - 
 w   1  1    3068 -0.010386    2( a  )  16( a  )   +-   +    +-   +-    - 
 w   1  1    3137  0.013343    1( a  )   1( a  )   +-   +    +-    -   +- 
 w   1  1    3143  0.019455    1( a  )   4( a  )   +-   +    +-    -   +- 
 w   1  1    3146  0.017892    4( a  )   4( a  )   +-   +    +-    -   +- 
 w   1  1    3147  0.016073    1( a  )   5( a  )   +-   +    +-    -   +- 
 w   1  1    3169  0.012260    5( a  )   8( a  )   +-   +    +-    -   +- 
 w   1  1    3196 -0.019310    5( a  )  11( a  )   +-   +    +-    -   +- 
 w   1  1    3328 -0.018888    1( a  )   2( a  )   +-   +     -   +-   +- 
 w   1  1    3330 -0.011936    1( a  )   3( a  )   +-   +     -   +-   +- 
 w   1  1    3334 -0.010810    2( a  )   4( a  )   +-   +     -   +-   +- 
 w   1  1    3335 -0.019799    3( a  )   4( a  )   +-   +     -   +-   +- 
 w   1  1    3348 -0.020637    1( a  )   7( a  )   +-   +     -   +-   +- 
 w   1  1    3351 -0.014016    4( a  )   7( a  )   +-   +     -   +-   +- 
 w   1  1    3356 -0.012160    2( a  )   8( a  )   +-   +     -   +-   +- 
 w   1  1    3357 -0.013848    3( a  )   8( a  )   +-   +     -   +-   +- 
 w   1  1    3380 -0.012073    9( a  )  10( a  )   +-   +     -   +-   +- 
 w   1  1    3383  0.011146    2( a  )  11( a  )   +-   +     -   +-   +- 
 w   1  1    3388  0.017233    7( a  )  11( a  )   +-   +     -   +-   +- 
 w   1  1    3482 -0.010327    3( a  )  18( a  )   +-   +     -   +-   +- 
 w   1  1    3517 -0.011294    1( a  )   1( a  )   +-        +-   +-   +- 
 w   1  1    3523 -0.013862    1( a  )   4( a  )   +-        +-   +-   +- 
 w   1  1    3526 -0.013227    4( a  )   4( a  )   +-        +-   +-   +- 

 ci coefficient statistics:
           rq > 0.1                1
      0.1> rq > 0.01             112
     0.01> rq > 0.001            592
    0.001> rq > 0.0001           429
   0.0001> rq > 0.00001          141
  0.00001> rq > 0.000001          74
 0.000001> rq                   3307
           all                  4656
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        15 2x:         0 4x:         0
All internal counts: zz :         0 yy:         0 xx:         0 ww:         0
One-external counts: yz :         0 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         0    task #   2:         0    task #   3:         0    task #   4:         0
task #   5:         0    task #   6:         0    task #   7:        -1    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:        14    task #  12:         0
task #  13:         0    task #  14:         0    task #
  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.975545686563    -74.189351412555

 number of reference csfs (nref) is     1.  root number (iroot) is  1.

 pople ci energy extrapolation is computed with 10 correlated electrons.

 eref      =    -76.049079437753   "relaxed" cnot**2         =   0.951689386571
 eci       =    -76.253631106007   deltae = eci - eref       =  -0.204551668253
 eci+dv1   =    -76.263513122578   dv1 = (1-cnot**2)*deltae  =  -0.009882016571
 eci+dv2   =    -76.264014763438   dv2 = dv1 / cnot**2       =  -0.010383657431
 eci+dv3   =    -76.264570057491   dv3 = dv1 / (2*cnot**2-1) =  -0.010938951484
 eci+pople =    -76.262195236214   ( 10e- scaled deltae )    =  -0.213115798460
NO coefficients and occupation numbers are written to nocoef_ci.1
 entering drivercid: firstcall,firstnonref= F T
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=      10  D0X=      30  D0W=      50
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   1.9999190      1.9865394      1.9762920      1.9721967      1.9699003
   0.0215610      0.0197971      0.0146093      0.0103298      0.0054224
   0.0051585      0.0047359      0.0042192      0.0041284      0.0010140
   0.0010093      0.0006558      0.0005679      0.0004919      0.0004911
   0.0004473      0.0004301      0.0000506      0.0000330


 total number of electrons =   10.0000000000



          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!


                        A   partial gross atomic populations
   ao class       1A         2A         3A         4A         5A         6A  
    O1_ s       2.000676   1.663811   0.000000  -0.044539   0.000000   0.000000
    O1_ p       0.000085   0.037185   1.931452   1.496163   1.156936   0.011287
    O1_ d       0.000000   0.000191   0.000969   0.001819   0.008152   0.000440
    H1_ s      -0.000124   0.115237   0.000000   0.238622   0.387760   0.005201
    H1_ p      -0.000296   0.027438   0.021935   0.020877   0.014492  -0.000289
    H2_ s      -0.000124   0.115239   0.000000   0.238370   0.388072   0.005211
    H2_ p      -0.000296   0.027438   0.021936   0.020885   0.014488  -0.000289

   ao class       7A         8A         9A        10A        11A        12A  
    O1_ s       0.001582   0.000000   0.005220   0.000000   0.001117   0.000000
    O1_ p       0.009156   0.013982   0.004045   0.001912   0.000062   0.000000
    O1_ d       0.000275   0.000036   0.000289   0.002662   0.002725   0.003578
    H1_ s       0.004703   0.000000   0.000239   0.000249   0.000116   0.000000
    H1_ p      -0.000307   0.000296   0.000149   0.000175   0.000511   0.000579
    H2_ s       0.004694   0.000000   0.000239   0.000249   0.000117   0.000000
    H2_ p      -0.000306   0.000296   0.000149   0.000176   0.000511   0.000579

   ao class      13A        14A        15A        16A        17A        18A  
    O1_ s       0.000202   0.000000   0.000000   0.000205   0.000000   0.000000
    O1_ p       0.000243   0.000084   0.000264   0.000168   0.000000   0.000005
    O1_ d       0.003384   0.003563   0.000384   0.000377   0.000160   0.000053
    H1_ s       0.000012   0.000000   0.000080   0.000155   0.000000   0.000102
    H1_ p       0.000183   0.000241   0.000103  -0.000025   0.000248   0.000153
    H2_ s       0.000012   0.000000   0.000081   0.000154   0.000000   0.000102
    H2_ p       0.000183   0.000241   0.000102  -0.000025   0.000248   0.000154

   ao class      19A        20A        21A        22A        23A        24A  
    O1_ s       0.000008   0.000000   0.000000   0.000011   0.000005   0.000000
    O1_ p      -0.000006   0.000022   0.000057   0.000037   0.000003   0.000005
    O1_ d       0.000036   0.000066   0.000000   0.000073   0.000000   0.000000
    H1_ s       0.000032   0.000000   0.000007   0.000007   0.000015   0.000008
    H1_ p       0.000195   0.000202   0.000188   0.000147   0.000006   0.000006
    H2_ s       0.000032   0.000000   0.000007   0.000007   0.000015   0.000008
    H2_ p       0.000195   0.000201   0.000188   0.000147   0.000006   0.000006


                        gross atomic populations
     ao           O1_        H1_        H2_
      s         3.628298   0.752421   0.752485
      p         4.663147   0.087206   0.087213
      d         0.029232   0.000000   0.000000
    total       8.320676   0.839627   0.839697


 Total number of electrons:   10.00000000

