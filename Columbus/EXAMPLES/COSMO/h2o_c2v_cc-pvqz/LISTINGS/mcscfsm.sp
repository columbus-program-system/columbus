 total ao core energy =    9.317913917
 MCSCF calculation performed for  1 DRT.

 DRT  first state   no.of aver. states   weights
  1   ground state          1             1.000

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a1 
 Total number of electrons:    10
 Spin multiplicity:            1
 Number of active orbitals:    0
 Number of active electrons:   0
 Total number of CSFs:         1

 Number of active-double rotations:      0
 Number of active-active rotations:      0
 Number of double-virtual rotations:   171
 Number of active-virtual rotations:     0

 iter=    1 emc=  -76.0654569389 demc= 7.6065E+01 wnorm= 6.5458E-10 knorm= 6.7859E-11 apxde= 3.0769E-16    *not converged* 

 final mcscf convergence values:
 iter=    2 emc=  -76.0654569389 demc= 0.0000E+00 wnorm= 1.2501E-09 knorm= 7.9630E-12 apxde= 1.3245E-15    *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 weight 1.000000 total energy=  -76.065456939
   ------------------------------------------------------------


