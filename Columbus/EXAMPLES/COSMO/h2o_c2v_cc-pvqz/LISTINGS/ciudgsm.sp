1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 integral file titles:
 Hermit Integral Program : SIFS version  hochtor2        Thu Oct  7 15:07:44 2004
  cidrt_title                                                                    
  title                                                                          
 mofmt: formatted orbitals label=morbl   hochtor2        Thu Oct  7 15:08:35 2004
 SIFS file created by program tran.      hochtor2        Thu Oct  7 15:08:35 2004

 formula file title:
 Hermit Integral Program : SIFS version  hochtor2        Thu Oct  7 15:07:44 2004
  cidrt_title                                                                    
  title                                                                          
 mofmt: formatted orbitals label=morbl   hochtor2        Thu Oct  7 15:08:35 2004
 SIFS file created by program tran.      hochtor2        Thu Oct  7 15:08:35 2004
  cidrt_title                                                                    

 297 dimension of the ci-matrix ->>>     39574


 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -76.0856466705 -9.2977E+00  3.6144E-01  2.0350E+00  1.0000E-03
 mr-sdci #  2  1    -76.3773566853 -9.0461E+00  1.3653E-02  3.8800E-01  1.0000E-03
 mr-sdci #  3  1    -76.3885395731 -9.3266E+00  9.2407E-04  8.1011E-02  1.0000E-03
 mr-sdci #  4  1    -76.3892556977 -9.3371E+00  1.0977E-04  2.8778E-02  1.0000E-03
 mr-sdci #  5  1    -76.3893567585 -9.3377E+00  1.2099E-05  1.0271E-02  1.0000E-03
 mr-sdci #  6  1    -76.3893682949 -9.3378E+00  1.2512E-06  2.8875E-03  1.0000E-03
 mr-sdci #  7  1    -76.3909724344 -9.3362E+00  7.0729E-06  3.4702E-03  1.0000E-03
 mr-sdci #  8  1    -76.3909798210 -9.3366E+00  7.4976E-07  1.8727E-03  1.0000E-03
 mr-sdci #  9  1    -76.3909805130 -9.3366E+00  5.0866E-08  5.0513E-04  1.0000E-03

 mr-sdci  convergence criteria satisfied after  9 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  9  1    -76.3909805130 -9.3366E+00  5.0866E-08  5.0513E-04  1.0000E-03

 number of reference csfs (nref) is     1.  root number (iroot) is  1.

 eref      =    -76.087120666718   "relaxed" cnot**2         =   0.944706917049
 eci       =    -76.390980512973   deltae = eci - eref       =  -0.303859846255
 eci+dv1   =    -76.407781860658   dv1 = (1-cnot**2)*deltae  =  -0.016801347685
 eci+dv2   =    -76.408765232637   dv2 = dv1 / cnot**2       =  -0.017784719664
 eci+dv3   =    -76.409870873164   dv3 = dv1 / (2*cnot**2-1) =  -0.018890360191
 eci+pople =    -76.405717448139   ( 10e- scaled deltae )    =  -0.318596781421
