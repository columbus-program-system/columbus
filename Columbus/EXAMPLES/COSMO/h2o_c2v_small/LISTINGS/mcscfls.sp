

     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 This program allows the csf mixing coefficient and orbital expansion coefficient
 optimization using the graphical unitary group approach and the exponential
 operator mcscf method.
 references:  r. shepard and j. simons,                    ' int. j. quantum chem. symp. 14, 211 (1980).
              r. shepard, i. shavitt, and j. simons, j. chem. phys. 76, 543 (1982).
              r. shepard in "ab initio methods in quantum chemistry ii" advances in chemical
                  physics 69, edited by k. p. lawley (wiley, new york, 1987) pp. 63-200.
 Original autor: Ron Shepard, ANL
 Later revisions: Michal Dallos, University Vienna

 This Version of Program MCSCF is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.4.0.2     **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 Workspace allocation information:
        10000000 of real*8 words (   76.29 MB) of work space has been allocated.

 user input information:

 ======== echo of the mcscf input ========
 ------------------------------------------------------------------------
  &input
   niter=60,
   nmiter=30,
   nciitr=30,
   tol(3)=1.e-4,
   tol(2)=1.e-4,
   tol(1)=1.e-8,
   NSTATE=0,
   npath=1,3,9,10,13,17,19,21,-11,12, 2,
   ncoupl=5,
   NAVST(1) = 1,
   WAVST(1,1)=1 ,
  &end
 ------------------------------------------------------------------------


 ***  Integral file informations  ***


 input integral file : aoints                                                      

 Integral file header information:
 Hermit Integral Program : SIFS version  hochtor2        Thu Oct  7 15:18:42 2004

 Core type energy values:
 energy( 1)=  9.317913916904E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 total ao core energy =    9.317913917


   ******  Basis set informations:  ******

 Number of irreps:                  4
 Total number of basis functions:  13

 irrep no.              1    2    3    4
 irrep label           A1   B1   B2   A2 
 no. of bas.fcions.     7    4    2    0


 ***  MCSCF optimization procedure parmeters:  ***


 maximum number of mcscf iterations:        niter=    60

 maximum number of psci micro-iterations:   nmiter=   30
 maximum r,s subspace dimension allowed:    nvrsmx=   30

 tol(1)=  1.0000E-08. . . . delta-emc convergence criterion.
 tol(2)=  1.0000E-04. . . . wnorm convergence criterion.
 tol(3)=  1.0000E-04. . . . knorm convergence criterion.
 tol(4)=  1.0000E-08. . . . apxde convergence criterion.
 tol(5)=  1.0000E-04. . . . small diagonal matrix element tolerance.
 tol(6)=  1.0000E-06. . . . minimum ci-psci residual norm.
 tol(7)=  1.0000E-05. . . . maximum ci-psci residual norm.
 tol(8)=  1.0000E+00. . . . maximum abs(k(xy)) allowed.
 tol(9)=  1.0000E+00. . . . wnorm coupling tolerance.
 tol(10)= 0.0000E+00. . . . maximum psci emergency shift parameter.
 tol(11)= 0.0000E+00. . . . minimum psci emergency shift parameter.
 tol(12)= 0.0000E+00. . . . increment of psci emergency shift parameter.


 *** State averaging informations: ***


 MCSCF calculation performed for  1 DRT.

 DRT  first state   no.of aver.states   weights
  1   ground state          1             1.000

 The number of hmc(*) eigenvalues and eigenvectors calculated each iteration per symmetry:
 Symm.   no.of eigenv.(=ncol)
  A1         2

 orbital coefficients are optimized for the ground state (nstate=0).

 no fciorb(*,*) specified.

 npath(*) options:
  2:  orbital-state coupling terms will be included beginning on iteration ncoupl=  5
  3:  print intermediate timing information.
  9:  suppress the drt listing.
 10:  suppress the hmc(*) eigenvector listing.
 12:  diagonalize the hmc(*) matrix iteratively.
        nunitv= 1 nciitr=30 mxvadd=20 nvcimx=20
       rtolci(*),wnorm=     1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 0.0000E+00
   noldv =   0
 13:  get initial orbitals from the formatted file, mocoef.
 17:  print the final natural orbitals and occupations.
 19:  transform the virtual orbitals to diagonalize qvv(*).
 21:  write out the one- and two- electron density for further use (files:mcd1fl, mcd2fl).


   ******  DRT info section  ******


 Informations for the DRT no.  1

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a1 
 Total number of electrons:   10
 Spin multiplicity:            1
 Number of active orbitals:    0
 Number of active electrons:   0
 Total number of CSFs:         1

 !timer: initialization                  user+sys=     0.010 walltime=     0.000

 faar:   0 active-active rotations allowed out of:   0 possible.


 Number of active-double rotations:      0
 Number of active-active rotations:      0
 Number of double-virtual rotations:    16
 Number of active-virtual rotations:     0

 Size of orbital-Hessian matrix B:                      157
 Size of the orbital-state Hessian matrix C:             16
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:            173



   ****** Integral transformation section ******


 number of blocks to be transformed in-core is   0
 number of blocks to be transformed out of core is    9

 in-core ao list requires    0 segments.
 out of core ao list requires    9 segments.
 each segment has length   9995562 working precision words.

               ao integral sort statistics
 length of records:      4096
 number of buckets:   9
 scratch file used is da1:
 amount of core needed for in-core arrays:   269

 twoao processed       1270 two electron ao integrals.

       2681 ao integrals were written into    9 records

 Source of the initial MO coeficients:

 Input MO coefficient file: mocoef                                                      


               starting mcscf iteration...   1
 !timer:                                 user+sys=     0.020 walltime=     0.000

 orbital-state coupling will not be calculated this iteration.

 number of transformed integrals put on file:       693
 number of records written:     1
 !timer: 2-e transformation              user+sys=     0.010 walltime=     0.000

 Size of orbital-Hessian matrix B:                      157
 Total size of the state Hessian matrix M:                0
 Total size of HESSIAN-matrix for linear con            157

 !timer: mosrt1                          user+sys=     0.000 walltime=     0.000
 !timer: mosrt2                          user+sys=     0.010 walltime=     0.000
 !timer: mosort                          user+sys=     0.010 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000

 trial vectors are generated internally.

 trial vector  1 is unit matrix column     1
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 ciiter=   1 noldhv=  1 noldv=  1
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -75.9798614863      -85.2977754032        0.0000000000        0.0000010000
 !timer: hmcvec                          user+sys=     0.000 walltime=     0.000
 !timer: mfvd                            user+sys=     0.000 walltime=     0.000
 !timer: badad                           user+sys=     0.000 walltime=     0.000
 !timer: badadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvava                           user+sys=     0.000 walltime=     0.000
 !timer: bvdvd                           user+sys=     0.000 walltime=     0.000
 !timer: hbcon                           user+sys=     0.000 walltime=     0.000

  tol(10)=  0. eshsci=  1.82260421E-10
 Total number of micro iterations:    1

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 1.00000000 pnorm= 0.0000E+00 rznorm= 1.7984E-09 rpnorm= 0.0000E+00 noldr=  1 nnewr=  1 nolds=  0 nnews=  0

 !timer: Direct Bxr time contribution    user+sys=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  user+sys=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    user+sys=     0.000 walltime=     0.000
 !timer: solvek total                    user+sys=     0.000 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
   -41.105086   -2.722965   -1.123121

 qvv(*) eigenvalues. symmetry block  1
     0.354643    1.878964    2.452163    3.746149

 fdd(*) eigenvalues. symmetry block  2
    -1.433375

 qvv(*) eigenvalues. symmetry block  2
     0.508585    1.654983    2.763644

 fdd(*) eigenvalues. symmetry block  3
    -1.004494

 qvv(*) eigenvalues. symmetry block  3
     2.386452
 !timer: motran                          user+sys=     0.000 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 user+sys=     0.020 walltime=     0.000

 all mcscf convergence criteria are not satisfied.
 iter=    1 emc=  -75.9798614863 demc= 7.5980E+01 wnorm= 1.4581E-09 knorm= 2.8560E-10 apxde= 4.2010E-18    *not converged* 

               starting mcscf iteration...   2
 !timer:                                 user+sys=     0.040 walltime=     0.000

 orbital-state coupling will not be calculated this iteration.

 number of transformed integrals put on file:       693
 number of records written:     1
 !timer: 2-e transformation              user+sys=     0.000 walltime=     0.000

 Size of orbital-Hessian matrix B:                      157
 Total size of the state Hessian matrix M:                0
 Total size of HESSIAN-matrix for linear con            157

 !timer: mosrt1                          user+sys=     0.010 walltime=     0.000
 !timer: mosrt2                          user+sys=     0.000 walltime=     0.000
 !timer: mosort                          user+sys=     0.010 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000

   1 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 ciiter=   1 noldhv=  1 noldv=  1
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -75.9798614863      -85.2977754032        0.0000000000        0.0000010000
 !timer: hmcvec                          user+sys=     0.000 walltime=     0.000
 !timer: mfvd                            user+sys=     0.000 walltime=     0.000
 !timer: badad                           user+sys=     0.000 walltime=     0.000
 !timer: badadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvava                           user+sys=     0.000 walltime=     0.000
 !timer: bvdvd                           user+sys=     0.000 walltime=     0.000
 !timer: hbcon                           user+sys=     0.000 walltime=     0.000

  tol(10)=  0. eshsci=  2.24806075E-10
 Total number of micro iterations:    1

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 1.00000000 pnorm= 0.0000E+00 rznorm= 8.9654E-10 rpnorm= 0.0000E+00 noldr=  1 nnewr=  1 nolds=  0 nnews=  0

 !timer: Direct Bxr time contribution    user+sys=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  user+sys=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    user+sys=     0.000 walltime=     0.000
 !timer: solvek total                    user+sys=     0.000 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
   -41.105086   -2.722965   -1.123121

 qvv(*) eigenvalues. symmetry block  1
     0.354643    1.878964    2.452163    3.746149

 fdd(*) eigenvalues. symmetry block  2
    -1.433375

 qvv(*) eigenvalues. symmetry block  2
     0.508585    1.654983    2.763644

 fdd(*) eigenvalues. symmetry block  3
    -1.004494

 qvv(*) eigenvalues. symmetry block  3
     2.386452
 !timer: motran                          user+sys=     0.000 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 user+sys=     0.020 walltime=     0.000

 all mcscf convergence criteria are satisfied.

 final mcscf convergence values:
 iter=    2 emc=  -75.9798614863 demc= 0.0000E+00 wnorm= 1.7984E-09 knorm= 2.7265E-11 apxde=-2.2821E-15    *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 weight 1.000000 total energy=  -75.979861486
   ------------------------------------------------------------



          mcscf orbitals of the final iteration,  A1  block   1

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7
   1O1s       1.00049466    -0.00995599    -0.00032284    -0.04753886     0.04180364    -0.03063047     0.80430805
   2O1s       0.00269642     0.86483270     0.32304427     0.15744639     0.51217756    -0.28674188     3.58448357
   3O1s      -0.00333642    -0.04196472     0.13217185     0.86323908     0.12501366    -0.07288835    -4.77462253
   4O1pz     -0.00286921    -0.16596078     0.79840471    -0.28550457    -0.49303675    -1.36544217    -0.16422886
   5O1pz      0.00255904     0.04508323     0.03316137    -0.16989217    -0.31639626     1.86986020     0.73830378
   6H1s       0.00056556     0.29273447    -0.31940455    -0.09355255    -2.03133059     0.48325916     0.90926985
   7H1s       0.00049572    -0.15958251     0.13419183    -0.76635303     1.48533303    -0.15256898    -0.07203884

          mcscf orbitals of the final iteration,  B1  block   2

               MO    8        MO    9        MO   10        MO   11
   8O1px      0.72963106    -0.41074769     0.42297293    -1.47141509
   9O1px     -0.09596331    -0.44985670     0.63360954     2.23017940
  10H1s      -0.57794826    -0.02829207     1.90383722     0.24022075
  11H1s       0.23868493    -1.41598366    -1.50664804     0.61409106

          mcscf orbitals of the final iteration,  B2  block   3

               MO   12        MO   13
  12O1py      0.92492545    -1.40711321
  13O1py      0.09147553     1.68139433

          mcscf orbitals of the final iteration,  A2  block   4

          natural orbitals of the final iteration, block  1

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7

  occ(*)=     2.00000000     2.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000

   1O1s       1.00049466    -0.00995599    -0.00032284    -0.04753886     0.04180364    -0.03063047     0.80430805
   2O1s       0.00269642     0.86483270     0.32304427     0.15744639     0.51217756    -0.28674188     3.58448357
   3O1s      -0.00333642    -0.04196472     0.13217185     0.86323908     0.12501366    -0.07288835    -4.77462253
   4O1pz     -0.00286921    -0.16596078     0.79840471    -0.28550457    -0.49303675    -1.36544217    -0.16422886
   5O1pz      0.00255904     0.04508323     0.03316137    -0.16989217    -0.31639626     1.86986020     0.73830378
   6H1s       0.00056556     0.29273447    -0.31940455    -0.09355255    -2.03133059     0.48325916     0.90926985
   7H1s       0.00049572    -0.15958251     0.13419183    -0.76635303     1.48533303    -0.15256898    -0.07203884

          natural orbitals of the final iteration, block  2

               MO    1        MO    2        MO    3        MO    4

  occ(*)=     2.00000000     0.00000000     0.00000000     0.00000000

   8O1px      0.72963106    -0.41074769     0.42297293    -1.47141509
   9O1px     -0.09596331    -0.44985670     0.63360954     2.23017940
  10H1s      -0.57794826    -0.02829207     1.90383722     0.24022075
  11H1s       0.23868493    -1.41598366    -1.50664804     0.61409106

          natural orbitals of the final iteration, block  3

               MO    1        MO    2

  occ(*)=     2.00000000     0.00000000

  12O1py      0.92492545    -1.40711321
  13O1py      0.09147553     1.68139433
 d1(*), fmc(*), and qmc(*) written to the 1-particle density matrix file.
         25 d2(*) elements written to the 2-particle density matrix file.
 !timer: writing the mc density files requser+sys=     0.000 walltime=     0.000


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!


                        A1  partial gross atomic populations
   ao class       1A1        2A1        3A1        4A1        5A1        6A1 
    O1_ s       1.999705   1.585296   0.220475   0.000000   0.000000   0.000000
    O1_ p       0.000005   0.056156   1.561391   0.000000   0.000000   0.000000
    H1_ s       0.000290   0.358549   0.218134   0.000000   0.000000   0.000000

   ao class       7A1 

                        B1  partial gross atomic populations
   ao class       1B1        2B1        3B1        4B1 
    O1_ p       1.199135   0.000000   0.000000   0.000000
    H1_ s       0.800865   0.000000   0.000000   0.000000

                        B2  partial gross atomic populations
   ao class       1B2        2B2 
    O1_ p       2.000000   0.000000


                        gross atomic populations
     ao           O1_        H1_
      s         3.805475   1.377838
      p         4.816687   0.000000
    total       8.622162   1.377838


 Total number of electrons:   10.00000000

 !timer: mcscf                           user+sys=     0.070 walltime=     0.000
