                                program "scfpq"
                            columbus program system
             restricted hartree-fock scf and two-configuration mcscf

                   programmed (in part) by russell m. pitzer
                            version date: 30-sep-00

 This Version of Program SCFPQ is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              SCFPQ       **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 echo of the input file:
 ------------------------------------------------------------------------
 Default SCF Title
  -15   9   0  -1   0   8   0   0   0  40  20   1   1   0   0   0  -1   0  11   0
    3   1   1   0
    2   2   2
    2
    2
    0.90000   0.10000   0.05000   0.00000    0    0
  (4f18.12)
    1
 ------------------------------------------------------------------------

workspace parameters: lcore=  10000000
 mem1= 1108566024 ifirst= -264081226

Default SCF Title                                                               

scf flags
 -15   9   0  -1   0   8   0   0   0  40  20   1   1   0   0   0  -1   0  11   0
   1
input orbital labels, i:bfnlab(i)=
   1:  1O1s     2:  2O1s     3:  3O1s     4:  4O1pz    5:  5O1pz    6:  6H1s  
   7:  7H1s     8:  8O1px    9:  9O1px   10: 10H1s    11: 11H1s    12: 12O1py 
  13: 13O1py 
bfn_to_center map(*), i:map(i)
   1:  1   2:  1   3:  1   4:  1   5:  1   6:  2   7:  2   8:  1   9:  1  10:  2
  11:  2  12:  1  13:  1
bfn_to_orbital_type map(*), i:map(i)
   1:  1   2:  1   3:  1   4:  2   5:  2   6:  1   7:  1   8:  2   9:  2  10:  1
  11:  1  12:  2  13:  2

Hermit Integral Program : SIFS version  hochtor2        Thu Oct  7 15:18:42 2004

core potentials used

normalization threshold = 10**(-20)
one- and two-electron energy convergence criterion = 10**(- 9)

nuclear repulsion energy =     9.3179139169037

 fock matrix built from AO INTEGRALS
                        ^^^^^^^^^^^^
 in-core processing switched off

DIIS SWITCHED ON (error vector is FDS-SDF)

total number of SCF basis functions:  13

Hermit Integral Program : SIFS version  hochtor2        Thu Oct  7 15:18:42 2004
Default SCF Title                                                               

                 41 s integrals
                 41 t integrals
                 41 v integrals
                 41 c integrals
                 41 h integrals

           starting vectors from diagonalization of one-electron terms

     occupied and virtual orbitals
Hermit Integral Program : SIFS version  hochtor2        Thu Oct  7 15:18:42 2004
Default SCF Title                                                               

                         A1  molecular orbitals
      sym. orb.       1A1         2A1         3A1         4A1         5A1 
   1A1 ,  1O1s      0.985074    0.266712    0.139442   -0.366680   -0.397252
   2A1 ,  2O1s     -0.094727    1.944935    1.036248   -1.580882   -1.599793
   3A1 ,  3O1s      0.077607   -1.280835   -0.637880    1.678703    2.131145
   4A1 ,  4O1pz    -0.001094   -0.742746    1.361266    0.363255   -0.503580
   5A1 ,  5O1pz    -0.001468    0.436196   -0.752015   -0.447948    0.972453
   6A1 ,  6H1s     -0.001540    0.051441    0.008340    0.880968   -0.368168
   7A1 ,  7H1s     -0.002343    0.001171   -0.012191   -0.574683    0.236242

      orb. en.    -33.071727   -8.935156   -8.500990   -4.611391   -3.884368

      occ. no.      2.000000    2.000000    2.000000    0.000000    0.000000

      sym. orb.       6A1         7A1 
   1A1 ,  1O1s     -0.411280    0.361359
   2A1 ,  2O1s     -1.506067    1.370342
   3A1 ,  3O1s      2.787833   -2.529249
   4A1 ,  4O1pz     0.277950   -0.116107
   5A1 ,  5O1pz    -1.449263    0.413637
   6A1 ,  6H1s     -2.105374   -0.190034
   7A1 ,  7H1s      1.126639    1.099959

      orb. en.     -3.474119   -2.460798

      occ. no.      0.000000    0.000000

                         B1  molecular orbitals
      sym. orb.       1B1         2B1         3B1         4B1 
   1B1 ,  8O1px     1.566988   -0.547396   -0.477495   -0.248891
   2B1 ,  9O1px    -0.930432    0.575466    1.917415    0.844627
   3B1 , 10H1s     -0.024863   -1.260271    1.498525   -0.427374
   4B1 , 11H1s     -0.072401    0.889539   -0.596245    1.886013

      orb. en.     -8.658529   -4.522733   -3.609909   -2.292859

      occ. no.      2.000000    0.000000    0.000000    0.000000

                         B2  molecular orbitals
      sym. orb.       1B2         2B2 
   1B2 , 12O1py     1.549607   -0.658919
   2B2 , 13O1py    -0.855448    1.450401

      orb. en.     -8.538777   -3.902169

      occ. no.      2.000000    0.000000

mo coefficients will be saved after each iteration

iteration       energy           one-electron energy       two-electron energy
         ttr          tmr           tnr

    1     -69.449740190710         -135.41035769744          56.642703589824    
       0.9000       0.1000       0.5000E-01
    2     -72.809542044057         -133.59943850229          51.471982541333    
       0.9000       0.1000       0.5000E-01
    3     -70.330754571646         -135.28575938447          55.637090895918    
       0.9000       0.1000       0.5000E-01
    4     -69.903060128912         -135.37717947775          56.156205431934    
       0.9500       0.1000       0.5000E-01
    5     -69.676059357614         -135.40204261051          56.408069335996    
       0.9500       0.1000       0.5000E-01
    6     -69.554276823291         -135.40857770299          56.536386962797    
       0.9500       0.1000       0.5000E-01
    7     -72.321281279526         -134.09432639316          52.455131196729    
       0.9500       0.1000       0.5000E-01
    8     -75.158746355100         -129.05815488996          44.581494617960    
       0.9000       0.1000       0.5000E-01
    9     -75.787711742102         -126.15552200165          41.049896342648    
       0.8000       0.1000       0.5000E-01
   10     -75.962651971234         -124.08783896666          38.807273078524    
       0.7000       0.1000       0.5000E-01
   11     -75.977356131380         -123.54602041433          38.250750366047    
       0.6000       0.1000       0.5000E-01
   12     -75.979604257748         -123.31553163107          38.018013456416    
       0.5000       0.1000       0.5000E-01
   13     -75.979840182050         -123.23796102827          37.940206929318    
       0.4000       0.1000       0.5000E-01
   14     -75.979860361969         -123.21374848713          37.915974208259    
       0.3000       0.1000       0.5000E-01
   15     -75.979861455100         -123.20771383368          37.909938461680    
       0.2000       0.1000       0.5000E-01
   16     -75.979861485782         -123.20662885400          37.908853451313    
       0.1000       0.1000       0.5000E-01
   17     -75.979861486255         -123.20652070235          37.908745299196    
       0.1000       0.1000       0.5000E-01
   18     -75.979861486260         -123.20651052820          37.908735125034    
       0.1000       0.1000       0.5000E-01
   19     -75.979861486261         -123.20650952571          37.908734122546    
       0.1000       0.1000       0.5000E-01
   20     -75.979861486261         -123.20650943368          37.908734030517    
       0.1000       0.1000       0.5000E-01
   21     -75.979861486261         -123.20650942284          37.908734019671    
       0.1000       0.1000       0.5000E-01

calculation has *converged*
scf gradient information written to file vectgrd

     total energy =        -75.9798614863
     kinetic energy =       76.1186438555
     potential energy =   -152.0985053418
     virial theorem =        2.0018265678
     wavefunction norm =     1.0000000000

     occupied and virtual orbitals
Hermit Integral Program : SIFS version  hochtor2        Thu Oct  7 15:18:42 2004
Default SCF Title                                                               

                         A1  molecular orbitals
      sym. orb.       1A1         2A1         3A1         4A1         5A1 
   1A1 ,  1O1s      1.000495   -0.009956   -0.000323   -0.047539    0.041804
   2A1 ,  2O1s      0.002696    0.864833    0.323044    0.157446    0.512178
   3A1 ,  3O1s     -0.003336   -0.041965    0.132172    0.863239    0.125014
   4A1 ,  4O1pz    -0.002869   -0.165961    0.798405   -0.285505   -0.493037
   5A1 ,  5O1pz     0.002559    0.045083    0.033161   -0.169892   -0.316396
   6A1 ,  6H1s      0.000566    0.292734   -0.319405   -0.093553   -2.031331
   7A1 ,  7H1s      0.000496   -0.159583    0.134192   -0.766353    1.485333

      orb. en.    -20.552543   -1.361482   -0.561561    0.177322    0.939482

      occ. no.      2.000000    2.000000    2.000000    0.000000    0.000000

      sym. orb.       6A1         7A1 
   1A1 ,  1O1s     -0.030630    0.804308
   2A1 ,  2O1s     -0.286742    3.584484
   3A1 ,  3O1s     -0.072888   -4.774623
   4A1 ,  4O1pz    -1.365442   -0.164229
   5A1 ,  5O1pz     1.869860    0.738304
   6A1 ,  6H1s      0.483259    0.909270
   7A1 ,  7H1s     -0.152569   -0.072039

      orb. en.      1.226081    1.873074

      occ. no.      0.000000    0.000000

                         B1  molecular orbitals
      sym. orb.       1B1         2B1         3B1         4B1 
   1B1 ,  8O1px     0.729631   -0.410748    0.422973   -1.471415
   2B1 ,  9O1px    -0.095963   -0.449857    0.633610    2.230179
   3B1 , 10H1s     -0.577948   -0.028292    1.903837    0.240221
   4B1 , 11H1s      0.238685   -1.415984   -1.506648    0.614091

      orb. en.     -0.716688    0.254293    0.827491    1.381822

      occ. no.      2.000000    0.000000    0.000000    0.000000

                         B2  molecular orbitals
      sym. orb.       1B2         2B2 
   1B2 , 12O1py     0.924925   -1.407113
   2B2 , 13O1py     0.091476    1.681394

      orb. en.     -0.502247    1.193226

      occ. no.      2.000000    0.000000

     population analysis
Hermit Integral Program : SIFS version  hochtor2        Thu Oct  7 15:18:42 2004
Default SCF Title                                                               
  NOTE: For HERMIT use spherical harmonics basis sets !!!


                        A1  partial gross atomic populations
   ao class       1A1        2A1        3A1 
    O1_ s       1.999705   1.585296   0.220475
    O1_ p       0.000005   0.056156   1.561391
    H1_ s       0.000290   0.358549   0.218134

                        B1  partial gross atomic populations
   ao class       1B1 
    O1_ p       1.199135
    H1_ s       0.800865

                        B2  partial gross atomic populations
   ao class       1B2 
    O1_ p       2.000000


                        gross atomic populations
     ao           O1_        H1_
      s         3.805475   1.377838
      p         4.816687   0.000000
    total       8.622162   1.377838

 Total number of electrons:   10.00000000

 !timer: scf required                    user+sys=     0.040 walltime=     0.000
