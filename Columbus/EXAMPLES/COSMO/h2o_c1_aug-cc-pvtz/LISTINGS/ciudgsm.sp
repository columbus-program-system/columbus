1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 integral file titles:
 Hermit Integral Program : SIFS version  hochtor2        Thu Oct  7 15:21:00 2004
  cidrt_title                                                                    
  title                                                                          
 mofmt: formatted orbitals label=morbl   hochtor2        Thu Oct  7 15:21:57 2004
 SIFS file created by program tran.      hochtor2        Thu Oct  7 15:21:58 2004

 formula file title:
 Hermit Integral Program : SIFS version  hochtor2        Thu Oct  7 15:21:00 2004
  cidrt_title                                                                    
  title                                                                          
 mofmt: formatted orbitals label=morbl   hochtor2        Thu Oct  7 15:21:57 2004
 SIFS file created by program tran.      hochtor2        Thu Oct  7 15:21:58 2004
  cidrt_title                                                                    

 297 dimension of the ci-matrix ->>>     95266


 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -76.0819716288 -9.2971E+00  3.1588E-01  1.5288E+00  1.0000E-03
 mr-sdci #  2  1    -76.3504845035 -9.0648E+00  7.5128E-03  2.2372E-01  1.0000E-03
 mr-sdci #  3  1    -76.3574341390 -9.3264E+00  3.7551E-04  3.9487E-02  1.0000E-03
 mr-sdci #  4  1    -76.3578148143 -9.3329E+00  3.5721E-05  1.3594E-02  1.0000E-03
 mr-sdci #  5  1    -76.3578508113 -9.3333E+00  1.9543E-06  3.4370E-03  1.0000E-03
 mr-sdci #  6  1    -76.3578528337 -9.3333E+00  8.1965E-08  7.0933E-04  1.0000E-03

 mr-sdci  convergence criteria satisfied after  6 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  6  1    -76.3578528337 -9.3333E+00  8.1965E-08  7.0933E-04  1.0000E-03

 number of reference csfs (nref) is     1.  root number (iroot) is  1.

 eref      =    -76.081971628776   "relaxed" cnot**2         =   0.943802923190
 eci       =    -76.357852833669   deltae = eci - eref       =  -0.275881204893
 eci+dv1   =    -76.373356550931   dv1 = (1-cnot**2)*deltae  =  -0.015503717262
 eci+dv2   =    -76.374279692371   dv2 = dv1 / cnot**2       =  -0.016426858702
 eci+dv3   =    -76.375319727702   dv3 = dv1 / (2*cnot**2-1) =  -0.017466894033
 eci+pople =    -76.371472902862   ( 10e- scaled deltae )    =  -0.289501274085
