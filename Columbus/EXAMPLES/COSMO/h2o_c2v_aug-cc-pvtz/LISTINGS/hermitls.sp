
 DALTON: user specified work memory size used,
          environment variable WRKMEM = "10000000            "

 Work memory size (LMWORK) :    10000000 =   76.29 megabytes.

 Default basis set library used :
        /sphome/kedziora/dalton/basis/                              


    $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    $$$$$$$$$$$  DALTON - An electronic structure program  $$$$$$$$$$$
    $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

               This is output from DALTON (beta-version 0.9) 

                          Principal authors:

            Trygve Helgaker,     University of Oslo,        Norway 
            Hans Joergen Jensen, University of Odense,      Denmark
            Poul Joergensen,     University of Aarhus,      Denmark
            Henrik Koch,         University of Aarhus,      Denmark
            Jeppe Olsen,         University of Lund,        Sweden 
            Hans Aagren,         University of Linkoeping,  Sweden 

                          Contributors:

            Torgeir Andersen,    University of Oslo,        Norway 
            Keld L. Bak,         University of Copenhagen,  Denmark
            Vebjoern Bakken,     University of Oslo,        Norway 
            Ove Christiansen,    University of Aarhus,      Denmark
            Paal Dahle,          University of Oslo,        Norway 
            Erik K. Dalskov,     University of Odense,      Denmark
            Thomas Enevoldsen,   University of Odense,      Denmark
            Asger Halkier,       University of Aarhus,      Denmark
            Hanne Heiberg,       University of Oslo,        Norway 
            Dan Jonsson,         University of Linkoeping,  Sweden 
            Sheela Kirpekar,     University of Odense,      Denmark
            Rika Kobayashi,      University of Aarhus,      Denmark
            Alfredo S. de Meras, Valencia University,       Spain  
            Kurt Mikkelsen,      University of Aarhus,      Denmark
            Patrick Norman,      University of Linkoeping,  Sweden 
            Martin J. Packer,    University of Sheffield,   UK     
            Kenneth Ruud,        University of Oslo,        Norway 
            Trond Saue,          University of Oslo,        Norway 
            Peter Taylor,        San Diego Superc. Center,  USA    
            Olav Vahtras,        University of Linkoeping,  Sweden

                                             Release Date:  August 1996
------------------------------------------------------------------------


      
     NOTE:
      
     This is an experimental code for the evaluation of molecular
     properties using (MC)SCF/CC wave functions. The authors accept
      no responsibility for the performance of the code or for the
     correctness of the results.
      
     The code (in whole or part) is not to be reproduced for further
     distribution without the written permission of T. Helgaker,
     H. J. Aa. Jensen or P. Taylor.
      
     If results obtained with this code are published, an
     appropriate citation would be:
      
     T. Helgaker, H. J. Aa. Jensen, P.Joergensen, H. Koch,
     J. Olsen, H. Aagren, T. Andersen, K. L. Bak, V. Bakken,
     O. Christiansen, P. Dahle, E. K. Dalskov, T. Enevoldsen,
     A. Halkier, H. Heiberg, D. Jonsson, S. Kirpekar, R. Kobayashi,
     A. S. de Meras, K. V. Mikkelsen, P. Norman, M. J. Packer,
     K. Ruud, T.Saue, P. R. Taylor, and O. Vahtras:
     DALTON, an electronic structure program"



     ******************************************
     **    PROGRAM:              DALTON      **
     **    PROGRAM VERSION:      5.4.0.0     **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************



 <<<<<<<<<< OUTPUT FROM GENERAL INPUT PROCESSING >>>>>>>>>>




 Default print level:        0

    Integral sections will be executed
    Starting in Integral Section -



 *************************************************************************
 ****************** Output from HERMIT input processing ******************
 *************************************************************************



 Default print level:        2


 Calculation of one- and two-electron Hamiltonian integrals.


 The following one-electron property integrals are calculated:

          - overlap integrals
          - Cartesian multipole moment integrals of orders 4 and lower
          - electronic angular momentum around the origin


 Changes of defaults for READIN:
 -------------------------------


 Maximum number of primitives per integral block :   11



 *************************************************************************
 ****************** Output from READIN input processing ******************
 *************************************************************************



  Title Cards
  -----------

                                                                          
                                                                          


  Symmetry Operations
  -------------------

  Symmetry operations: 2



                      SYMGRP:Point group information
                      ------------------------------

Point group: C2v

   * The point group was generated by:

      Reflection in the yz-plane
      Reflection in the xz-plane

   * Group multiplication table

        |  E   C2z  Oxz  Oyz
   -----+--------------------
     E  |  E 
    C2z | C2z   E 
    Oxz | Oxz  Oyz   E 
    Oyz | Oyz  Oxz  C2z   E 

   * Character table

        |  E   C2z  Oxz  Oyz
   -----+--------------------
    A1  |   1    1    1    1
    B1  |   1   -1    1   -1
    B2  |   1   -1   -1    1
    A2  |   1    1   -1   -1

   * Direct product table

        | A1   B1   B2   A2 
   -----+--------------------
    A1  | A1 
    B1  | B1   A1 
    B2  | B2   A2   A1 
    A2  | A2   B2   B1   A1 


  Atoms and basis sets
  --------------------

  Number of atom types:     2
  Total number of atoms:    3

  label    atoms   charge   prim    cont     basis   
  ----------------------------------------------------------------------
  O  1        1       8      58      46      [11s6p3d2f|5s4p3d2f]                   
  H  1        2       1      25      23      [6s3p2d|4s3p2d]                        
  ----------------------------------------------------------------------
  ----------------------------------------------------------------------
  total:      3      10     108      92

  Spherical harmonic basis used.
  Threshold for integrals:  1.00E-15


  Cartesian Coordinates
  ---------------------

  Total number of coordinates:  9


   1   O  1     x      0.0000000000
   2            y      0.0000000000
   3            z      0.7212806600

   4   H  1 1   x     -1.4192363700
   5            y      0.0000000000
   6            z     -0.3606403300

   7   H  1 2   x      1.4192363700
   8            y      0.0000000000
   9            z     -0.3606403300



  Symmetry Coordinates
  --------------------

  Number of coordinates in each symmetry:   3  3  2  1


  Symmetry 1

   1   O  1  z    3
   2   H  1  x    [ 4  -  7 ]/2
   3   H  1  z    [ 6  +  9 ]/2


  Symmetry 2

   4   O  1  x    1
   5   H  1  x    [ 4  +  7 ]/2
   6   H  1  z    [ 6  -  9 ]/2


  Symmetry 3

   7   O  1  y    2
   8   H  1  y    [ 5  +  8 ]/2


  Symmetry 4

   9   H  1  y    [ 5  -  8 ]/2


   Interatomic separations (in Angstroms):
   ---------------------------------------

            O  1        H  1        H  2

   O  1    0.000000
   H  1    0.944368    0.000000
   H  2    0.944368    1.502055    0.000000




  Bond distances (angstroms):
  ---------------------------

                  atom 1     atom 2                           distance
                  ------     ------                           --------
  bond distance:    H  1       O  1                           0.944368
  bond distance:    H  2       O  1                           0.944368


  Bond angles (degrees):
  ----------------------

                  atom 1     atom 2     atom 3                   angle
                  ------     ------     ------                   -----
  bond angle:       H  2       O  1       H  1                 105.362


  Nuclear repulsion energy :    9.317913916904


  Orbital exponents and contraction coefficients
  ----------------------------------------------


  O  1   1s    1    15330.000000    0.0005 -0.0001  0.0000  0.0000  0.0000
   gen. cont.  2     2299.000000    0.0039 -0.0009  0.0000  0.0000  0.0000
               3      522.400000    0.0202 -0.0046  0.0000  0.0000  0.0000
               4      147.300000    0.0792 -0.0187  0.0000  0.0000  0.0000
               5       47.550000    0.2307 -0.0585  0.0000  0.0000  0.0000
               6       16.760000    0.4331 -0.1365  0.0000  0.0000  0.0000
               7        6.207000    0.3503 -0.1757  0.0000  0.0000  0.0000
               8        1.752000    0.0427  0.1609  1.0000  0.0000  0.0000
               9        0.688200   -0.0082  0.6034  0.0000  0.0000  0.0000
              10        0.238400    0.0024  0.3788  0.0000  1.0000  0.0000
              11        0.073760    0.0000  0.0000  0.0000  0.0000  1.0000

  O  1   2px  12       34.460000    0.0159  0.0000  0.0000  0.0000
   gen. cont. 13        7.749000    0.0997  0.0000  0.0000  0.0000
              14        2.280000    0.3105  0.0000  0.0000  0.0000
              15        0.715600    0.4910  1.0000  0.0000  0.0000
              16        0.214000    0.3363  0.0000  1.0000  0.0000
              17        0.059740    0.0000  0.0000  0.0000  1.0000

  O  1   2py  18       34.460000    0.0159  0.0000  0.0000  0.0000
   gen. cont. 19        7.749000    0.0997  0.0000  0.0000  0.0000
              20        2.280000    0.3105  0.0000  0.0000  0.0000
              21        0.715600    0.4910  1.0000  0.0000  0.0000
              22        0.214000    0.3363  0.0000  1.0000  0.0000
              23        0.059740    0.0000  0.0000  0.0000  1.0000

  O  1   2pz  24       34.460000    0.0159  0.0000  0.0000  0.0000
   gen. cont. 25        7.749000    0.0997  0.0000  0.0000  0.0000
              26        2.280000    0.3105  0.0000  0.0000  0.0000
              27        0.715600    0.4910  1.0000  0.0000  0.0000
              28        0.214000    0.3363  0.0000  1.0000  0.0000
              29        0.059740    0.0000  0.0000  0.0000  1.0000

  O  1   3d2- 30        2.314000    1.0000  0.0000  0.0000
   seg. cont. 31        0.645000    0.0000  1.0000  0.0000
              32        0.214000    0.0000  0.0000  1.0000

  O  1   3d1- 33        2.314000    1.0000  0.0000  0.0000
   seg. cont. 34        0.645000    0.0000  1.0000  0.0000
              35        0.214000    0.0000  0.0000  1.0000

  O  1   3d0  36        2.314000    1.0000  0.0000  0.0000
   seg. cont. 37        0.645000    0.0000  1.0000  0.0000
              38        0.214000    0.0000  0.0000  1.0000

  O  1   3d1+ 39        2.314000    1.0000  0.0000  0.0000
   seg. cont. 40        0.645000    0.0000  1.0000  0.0000
              41        0.214000    0.0000  0.0000  1.0000

  O  1   3d2+ 42        2.314000    1.0000  0.0000  0.0000
   seg. cont. 43        0.645000    0.0000  1.0000  0.0000
              44        0.214000    0.0000  0.0000  1.0000

  O  1   4f3- 45        1.428000    1.0000  0.0000
   seg. cont. 46        0.500000    0.0000  1.0000

  O  1   4f2- 47        1.428000    1.0000  0.0000
   seg. cont. 48        0.500000    0.0000  1.0000

  O  1   4f1- 49        1.428000    1.0000  0.0000
   seg. cont. 50        0.500000    0.0000  1.0000

  O  1   4f0  51        1.428000    1.0000  0.0000
   seg. cont. 52        0.500000    0.0000  1.0000

  O  1   4f1+ 53        1.428000    1.0000  0.0000
   seg. cont. 54        0.500000    0.0000  1.0000

  O  1   4f2+ 55        1.428000    1.0000  0.0000
   seg. cont. 56        0.500000    0.0000  1.0000

  O  1   4f3+ 57        1.428000    1.0000  0.0000
   seg. cont. 58        0.500000    0.0000  1.0000

  H  1#1 1s   59       33.870000    0.0061  0.0000  0.0000  0.0000
   gen. cont. 60        5.095000    0.0453  0.0000  0.0000  0.0000
              61        1.159000    0.2028  0.0000  0.0000  0.0000
              62        0.325800    0.5039  1.0000  0.0000  0.0000
              63        0.102700    0.3834  0.0000  1.0000  0.0000
              64        0.025260    0.0000  0.0000  0.0000  1.0000

  H  1#2 1s   65       33.870000    0.0061  0.0000  0.0000  0.0000
   gen. cont. 66        5.095000    0.0453  0.0000  0.0000  0.0000
              67        1.159000    0.2028  0.0000  0.0000  0.0000
              68        0.325800    0.5039  1.0000  0.0000  0.0000
              69        0.102700    0.3834  0.0000  1.0000  0.0000
              70        0.025260    0.0000  0.0000  0.0000  1.0000

  H  1#1 2px  71        1.407000    1.0000  0.0000  0.0000
   seg. cont. 72        0.388000    0.0000  1.0000  0.0000
              73        0.102000    0.0000  0.0000  1.0000

  H  1#2 2px  74        1.407000    1.0000  0.0000  0.0000
   seg. cont. 75        0.388000    0.0000  1.0000  0.0000
              76        0.102000    0.0000  0.0000  1.0000

  H  1#1 2py  77        1.407000    1.0000  0.0000  0.0000
   seg. cont. 78        0.388000    0.0000  1.0000  0.0000
              79        0.102000    0.0000  0.0000  1.0000

  H  1#2 2py  80        1.407000    1.0000  0.0000  0.0000
   seg. cont. 81        0.388000    0.0000  1.0000  0.0000
              82        0.102000    0.0000  0.0000  1.0000

  H  1#1 2pz  83        1.407000    1.0000  0.0000  0.0000
   seg. cont. 84        0.388000    0.0000  1.0000  0.0000
              85        0.102000    0.0000  0.0000  1.0000

  H  1#2 2pz  86        1.407000    1.0000  0.0000  0.0000
   seg. cont. 87        0.388000    0.0000  1.0000  0.0000
              88        0.102000    0.0000  0.0000  1.0000

  H  1#1 3d2- 89        1.057000    1.0000  0.0000
   seg. cont. 90        0.247000    0.0000  1.0000

  H  1#2 3d2- 91        1.057000    1.0000  0.0000
   seg. cont. 92        0.247000    0.0000  1.0000

  H  1#1 3d1- 93        1.057000    1.0000  0.0000
   seg. cont. 94        0.247000    0.0000  1.0000

  H  1#2 3d1- 95        1.057000    1.0000  0.0000
   seg. cont. 96        0.247000    0.0000  1.0000

  H  1#1 3d0  97        1.057000    1.0000  0.0000
   seg. cont. 98        0.247000    0.0000  1.0000

  H  1#2 3d0  99        1.057000    1.0000  0.0000
   seg. cont.100        0.247000    0.0000  1.0000

  H  1#1 3d1+101        1.057000    1.0000  0.0000
   seg. cont.102        0.247000    0.0000  1.0000

  H  1#2 3d1+103        1.057000    1.0000  0.0000
   seg. cont.104        0.247000    0.0000  1.0000

  H  1#1 3d2+105        1.057000    1.0000  0.0000
   seg. cont.106        0.247000    0.0000  1.0000

  H  1#2 3d2+107        1.057000    1.0000  0.0000
   seg. cont.108        0.247000    0.0000  1.0000


  Contracted Orbitals
  -------------------

   1  O  1    1s       1     2     3     4     5     6     7     8     9    10
   2  O  1    1s       1     2     3     4     5     6     7     8     9    10
   3  O  1    1s       8
   4  O  1    1s      10
   5  O  1    1s      11
   6  O  1    2px     12    13    14    15    16
   7  O  1    2py     18    19    20    21    22
   8  O  1    2pz     24    25    26    27    28
   9  O  1    2px     15
  10  O  1    2py     21
  11  O  1    2pz     27
  12  O  1    2px     16
  13  O  1    2py     22
  14  O  1    2pz     28
  15  O  1    2px     17
  16  O  1    2py     23
  17  O  1    2pz     29
  18  O  1    3d2-    30
  19  O  1    3d1-    33
  20  O  1    3d0     36
  21  O  1    3d1+    39
  22  O  1    3d2+    42
  23  O  1    3d2-    31
  24  O  1    3d1-    34
  25  O  1    3d0     37
  26  O  1    3d1+    40
  27  O  1    3d2+    43
  28  O  1    3d2-    32
  29  O  1    3d1-    35
  30  O  1    3d0     38
  31  O  1    3d1+    41
  32  O  1    3d2+    44
  33  O  1    4f3-    45
  34  O  1    4f2-    47
  35  O  1    4f1-    49
  36  O  1    4f0     51
  37  O  1    4f1+    53
  38  O  1    4f2+    55
  39  O  1    4f3+    57
  40  O  1    4f3-    46
  41  O  1    4f2-    48
  42  O  1    4f1-    50
  43  O  1    4f0     52
  44  O  1    4f1+    54
  45  O  1    4f2+    56
  46  O  1    4f3+    58
  47  H  1#1  1s    59  60  61  62  63
  48  H  1#2  1s    65  66  67  68  69
  49  H  1#1  1s    62
  50  H  1#2  1s    68
  51  H  1#1  1s    63
  52  H  1#2  1s    69
  53  H  1#1  1s    64
  54  H  1#2  1s    70
  55  H  1#1  2px   71
  56  H  1#2  2px   74
  57  H  1#1  2py   77
  58  H  1#2  2py   80
  59  H  1#1  2pz   83
  60  H  1#2  2pz   86
  61  H  1#1  2px   72
  62  H  1#2  2px   75
  63  H  1#1  2py   78
  64  H  1#2  2py   81
  65  H  1#1  2pz   84
  66  H  1#2  2pz   87
  67  H  1#1  2px   73
  68  H  1#2  2px   76
  69  H  1#1  2py   79
  70  H  1#2  2py   82
  71  H  1#1  2pz   85
  72  H  1#2  2pz   88
  73  H  1#1  3d2-  89
  74  H  1#2  3d2-  91
  75  H  1#1  3d1-  93
  76  H  1#2  3d1-  95
  77  H  1#1  3d0   97
  78  H  1#2  3d0   99
  79  H  1#1  3d1+ 101
  80  H  1#2  3d1+ 103
  81  H  1#1  3d2+ 105
  82  H  1#2  3d2+ 107
  83  H  1#1  3d2-  90
  84  H  1#2  3d2-  92
  85  H  1#1  3d1-  94
  86  H  1#2  3d1-  96
  87  H  1#1  3d0   98
  88  H  1#2  3d0  100
  89  H  1#1  3d1+ 102
  90  H  1#2  3d1+ 104
  91  H  1#1  3d2+ 106
  92  H  1#2  3d2+ 108




  Symmetry Orbitals
  -----------------

  Number of orbitals in each symmetry:        35 27 18 12


  Symmetry  A1 ( 1)

    1     O  1     1s         1
    2     O  1     1s         2
    3     O  1     1s         3
    4     O  1     1s         4
    5     O  1     1s         5
    6     O  1     2pz        8
    7     O  1     2pz       11
    8     O  1     2pz       14
    9     O  1     2pz       17
   10     O  1     3d0       20
   11     O  1     3d2+      22
   12     O  1     3d0       25
   13     O  1     3d2+      27
   14     O  1     3d0       30
   15     O  1     3d2+      32
   16     O  1     4f0       36
   17     O  1     4f2+      38
   18     O  1     4f0       43
   19     O  1     4f2+      45
   20     H  1     1s        47  +  48
   21     H  1     1s        49  +  50
   22     H  1     1s        51  +  52
   23     H  1     1s        53  +  54
   24     H  1     2px       55  -  56
   25     H  1     2pz       59  +  60
   26     H  1     2px       61  -  62
   27     H  1     2pz       65  +  66
   28     H  1     2px       67  -  68
   29     H  1     2pz       71  +  72
   30     H  1     3d0       77  +  78
   31     H  1     3d1+      79  -  80
   32     H  1     3d2+      81  +  82
   33     H  1     3d0       87  +  88
   34     H  1     3d1+      89  -  90
   35     H  1     3d2+      91  +  92


  Symmetry  B1 ( 2)

   36     O  1     2px        6
   37     O  1     2px        9
   38     O  1     2px       12
   39     O  1     2px       15
   40     O  1     3d1+      21
   41     O  1     3d1+      26
   42     O  1     3d1+      31
   43     O  1     4f1+      37
   44     O  1     4f3+      39
   45     O  1     4f1+      44
   46     O  1     4f3+      46
   47     H  1     1s        47  -  48
   48     H  1     1s        49  -  50
   49     H  1     1s        51  -  52
   50     H  1     1s        53  -  54
   51     H  1     2px       55  +  56
   52     H  1     2pz       59  -  60
   53     H  1     2px       61  +  62
   54     H  1     2pz       65  -  66
   55     H  1     2px       67  +  68
   56     H  1     2pz       71  -  72
   57     H  1     3d0       77  -  78
   58     H  1     3d1+      79  +  80
   59     H  1     3d2+      81  -  82
   60     H  1     3d0       87  -  88
   61     H  1     3d1+      89  +  90
   62     H  1     3d2+      91  -  92


  Symmetry  B2 ( 3)

   63     O  1     2py        7
   64     O  1     2py       10
   65     O  1     2py       13
   66     O  1     2py       16
   67     O  1     3d1-      19
   68     O  1     3d1-      24
   69     O  1     3d1-      29
   70     O  1     4f3-      33
   71     O  1     4f1-      35
   72     O  1     4f3-      40
   73     O  1     4f1-      42
   74     H  1     2py       57  +  58
   75     H  1     2py       63  +  64
   76     H  1     2py       69  +  70
   77     H  1     3d2-      73  -  74
   78     H  1     3d1-      75  +  76
   79     H  1     3d2-      83  -  84
   80     H  1     3d1-      85  +  86


  Symmetry  A2 ( 4)

   81     O  1     3d2-      18
   82     O  1     3d2-      23
   83     O  1     3d2-      28
   84     O  1     4f2-      34
   85     O  1     4f2-      41
   86     H  1     2py       57  -  58
   87     H  1     2py       63  -  64
   88     H  1     2py       69  -  70
   89     H  1     3d2-      73  +  74
   90     H  1     3d1-      75  -  76
   91     H  1     3d2-      83  +  84
   92     H  1     3d1-      85  -  86

  Symmetries of electric field:  B1 (2)  B2 (3)  A1 (1)

  Symmetries of magnetic field:  B2 (3)  B1 (2)  A2 (4)


 Copy of input to READIN
 -----------------------

INTGRL                                                                          
                                                                                
                                                                                
s   2    2X   Y      0.10E-14                                                   
       8.0    1    4    2    2    2    2                                        
O  1   0.000000000000000   0.000000000000000   0.721280660000000       *        
H  10   4                                                                       
      15330.00000000         0.00050800        -0.00011500         0.00000000   
                             0.00000000                                         
       2299.00000000         0.00392900        -0.00089500         0.00000000   
                             0.00000000                                         
        522.40000000         0.02024300        -0.00463600         0.00000000   
                             0.00000000                                         
        147.30000000         0.07918100        -0.01872400         0.00000000   
                             0.00000000                                         
         47.55000000         0.23068700        -0.05846300         0.00000000   
                             0.00000000                                         
         16.76000000         0.43311800        -0.13646300         0.00000000   
                             0.00000000                                         
          6.20700000         0.35026000        -0.17574000         0.00000000   
                             0.00000000                                         
          1.75200000         0.04272800         0.16093400         1.00000000   
                             0.00000000                                         
          0.68820000        -0.00815400         0.60341800         0.00000000   
                             0.00000000                                         
          0.23840000         0.00238100         0.37876500         0.00000000   
                             1.00000000                                         
H   1   1                                                                       
          0.07376000         1.00000000                                         
H   5   3                                                                       
         34.46000000         0.01592800         0.00000000         0.00000000   
          7.74900000         0.09974000         0.00000000         0.00000000   
          2.28000000         0.31049200         0.00000000         0.00000000   
          0.71560000         0.49102600         1.00000000         0.00000000   
          0.21400000         0.33633700         0.00000000         1.00000000   
H   1   1                                                                       
          0.05974000         1.00000000                                         
H   2   2                                                                       
          2.31400000         1.00000000         0.00000000                      
          0.64500000         0.00000000         1.00000000                      
H   1   1                                                                       
          0.21400000         1.00000000                                         
H   1   1                                                                       
          1.42800000         1.00000000                                         
H   1   1                                                                       
          0.50000000         1.00000000                                         
       1.0    1    3    2    2    2                                             
H  1  -1.419236370000000   0.000000000000000  -0.360640330000000       *        
H   5   3                                                                       
         33.87000000         0.00606800         0.00000000         0.00000000   
          5.09500000         0.04530800         0.00000000         0.00000000   
          1.15900000         0.20282200         0.00000000         0.00000000   
          0.32580000         0.50390300         1.00000000         0.00000000   
          0.10270000         0.38342100         0.00000000         1.00000000   
H   1   1                                                                       
          0.02526000         1.00000000                                         
H   2   2                                                                       
          1.40700000         1.00000000         0.00000000                      
          0.38800000         0.00000000         1.00000000                      
H   1   1                                                                       
          0.10200000         1.00000000                                         
H   1   1                                                                       
          1.05700000         1.00000000                                         
H   1   1                                                                       
          0.24700000         1.00000000                                         


 herdrv: noofopt= 5


 ************************************************************************
 ************************** Output from HERONE **************************
 ************************************************************************

 prop, itype T 1


   890 atomic overlap integrals written in   1 buffers.
 Percentage non-zero integrals:  20.80
 prop, itype T 2


  1257 one-el. Hamil. integrals written in   1 buffers.
 Percentage non-zero integrals:  29.38
 prop, itype T 3


   899 kinetic energy integrals written in   1 buffers.
 Percentage non-zero integrals:  21.01


 inttyp= 1noptyp= 1


                    +---------------------------------+
                    ! Integrals of operator: OVERLAP  !
                    +---------------------------------+



 finopt,noofopt,last1= 0 5 0
 inttyp= 8noptyp= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000000 !
                    +---------------------------------+



 finopt,noofopt,last1= 1 5 0
 inttyp= 8noptyp= 3


                    +---------------------------------+
                    ! Integrals of operator: CM010000 !
                    +---------------------------------+

 typea=  1typeb=  0last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000100 !
                    +---------------------------------+

 typea=  1typeb=  1last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000001 !
                    +---------------------------------+

 typea=  1typeb=  2last= 2
 lstflg= 1


 finopt,noofopt,last1= 2 5 0
 inttyp= 8noptyp= 6


                    +---------------------------------+
                    ! Integrals of operator: CM020000 !
                    +---------------------------------+

 typea=  1typeb=  3last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM010100 !
                    +---------------------------------+

 typea=  1typeb=  4last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM010001 !
                    +---------------------------------+

 typea=  1typeb=  5last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000200 !
                    +---------------------------------+

 typea=  1typeb=  6last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000101 !
                    +---------------------------------+

 typea=  1typeb=  7last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000002 !
                    +---------------------------------+

 typea=  1typeb=  8last= 2
 lstflg= 1


 finopt,noofopt,last1= 3 5 0
 inttyp= 8noptyp= 10


                    +---------------------------------+
                    ! Integrals of operator: CM030000 !
                    +---------------------------------+

 typea=  1typeb=  9last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM020100 !
                    +---------------------------------+

 typea=  1typeb=  10last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM020001 !
                    +---------------------------------+

 typea=  1typeb=  11last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM010200 !
                    +---------------------------------+

 typea=  1typeb=  12last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM010101 !
                    +---------------------------------+

 typea=  1typeb=  13last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM010002 !
                    +---------------------------------+

 typea=  1typeb=  14last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000300 !
                    +---------------------------------+

 typea=  1typeb=  15last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000201 !
                    +---------------------------------+

 typea=  1typeb=  16last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000102 !
                    +---------------------------------+

 typea=  1typeb=  17last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000003 !
                    +---------------------------------+

 typea=  1typeb=  18last= 2
 lstflg= 1


 finopt,noofopt,last1= 4 5 0
 inttyp= 8noptyp= 15


                    +---------------------------------+
                    ! Integrals of operator: CM040000 !
                    +---------------------------------+

 typea=  1typeb=  19last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM030100 !
                    +---------------------------------+

 typea=  1typeb=  20last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM030001 !
                    +---------------------------------+

 typea=  1typeb=  21last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM020200 !
                    +---------------------------------+

 typea=  1typeb=  22last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM020101 !
                    +---------------------------------+

 typea=  1typeb=  23last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM020002 !
                    +---------------------------------+

 typea=  1typeb=  24last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM010300 !
                    +---------------------------------+

 typea=  1typeb=  25last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM010201 !
                    +---------------------------------+

 typea=  1typeb=  26last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM010102 !
                    +---------------------------------+

 typea=  1typeb=  27last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM010003 !
                    +---------------------------------+

 typea=  1typeb=  28last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000400 !
                    +---------------------------------+

 typea=  1typeb=  29last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000301 !
                    +---------------------------------+

 typea=  1typeb=  30last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000202 !
                    +---------------------------------+

 typea=  1typeb=  31last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000103 !
                    +---------------------------------+

 typea=  1typeb=  32last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000004 !
                    +---------------------------------+

 typea=  1typeb=  33last= 2
 lstflg= 1


 finopt,noofopt,last1= 5 5 2
 inttyp= 18noptyp= 3


                    +---------------------------------+
                    ! Integrals of operator: XANGMOM  !
                    +---------------------------------+

 angular moment
 typea=  2typeb=  6last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: YANGMOM  !
                    +---------------------------------+

 angular moment
 typea=  2typeb=  7last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: ZANGMOM  !
                    +---------------------------------+

 angular moment
 typea=  2typeb=  8last= 2
 lstflg= 2




 ************************************************************************
 ************************** Output from TWOINT **************************
 ************************************************************************

 calling sifew2:luinta,info,num,last,nrec
 calling sifew2: 11 2 4096 3272 4096 2730 2206 2 794

 Number of two-electron integrals written:   2169826 (23.7%)
 Kilobytes written:                            26048




 >>>> Total CPU  time used in HERMIT:   5.58 seconds
 >>>> Total wall time used in HERMIT:   5.00 seconds

- End of Integral Section
