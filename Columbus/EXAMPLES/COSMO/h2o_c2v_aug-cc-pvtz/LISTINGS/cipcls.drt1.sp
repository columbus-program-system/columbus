
 program cipc      

 print the csf info for mrsdci wave functions

 written by: ron shepard

 version date: 06-jun-96

 This Version of Program cipc is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 workspace allocation parameters: lencor=  10000000 mem1=1108566024 ifirst=-264082040

 drt header information:
  cidrt_title                                                                    
 nmot  =    92 niot  =     5 nfct  =     0 nfvt  =     0
 nrow  =    20 nsym  =     4 ssym  =     1 lenbuf=  1600
 spnorb=     F spnodd=     F lxyzir(1:3)= 0 0 0
 nwalk,xbar:         31        1        5       10       15
 nvalwt,nvalw:       31        1        5       10       15
 ncsft:           25093
 map(*)=    88 89 90  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17
            18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 91 33 34 35 36
            37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56
            57 58 92 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75
            76 77 78 79 80 81 82 83 84 85 86 87
 mu(*)=      0  0  0  0  0
 syml(*) =   1  1  1  2  3
 rmo(*)=     1  2  3  1  1

 indx01:    31 indices saved in indxv(*)
===================================ROOT # 1===================================

 rdhciv: CI vector file information:
 Hermit Integral Program : SIFS version  hochtor2        Thu Oct  7 15:04:40 2004
  cidrt_title                                                                    
  title                                                                          
 mofmt: formatted orbitals label=morbl   hochtor2        Thu Oct  7 15:05:01 2004
 SIFS file created by program tran.      hochtor2        Thu Oct  7 15:05:01 2004
 energy computed by program ciudg.       hochtor2        Thu Oct  7 15:06:04 2004

 lenrec =   32768 lenci =     25093 ninfo =  6 nenrgy =  5 ntitle =  6

 Max. overlap with ref vector #        1
 Valid ci vector #        1
 Method:        0       97% overlap
 energy( 1)=  9.317913916904E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -7.635856601161E+01, ietype=-1026,   total energy of type: MRSDCI  
 energy( 3)=  6.094069042278E-04, ietype=-2055, cnvginf energy of type: CI-Resid
 energy( 4)= -9.326642841588E+00, ietype=-2056, cnvginf energy of type: CI-D.E. 
 energy( 5)=  7.598612979125E-08, ietype=-2057, cnvginf energy of type: CI-ApxDE
==================================================================================

 space is available for   4982356 coefficients.

 updated histogram parameters:
 csfmn = 0.0000E+00 csfmx = 1.0000E+00 fhist = 5.0000E-01 nhist =  20

 this program will print the csfs generated from
 the drt according to the following print options :

 1) run in batch mode: all valid roots are automatically
    analysed and csf info is printed by default contribution
    threshold 0.01 
 2) run in interactive mode

 input menu number [  0]:
================================================================================
===================================VECTOR # 1===================================
================================================================================


 rdcivnew:      69 coefficients were selected.
 workspace: ncsfmx=   25093
 ncsfmx= 25093

 histogram parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =   25093 ncsft =   25093 ncsf =      69
 nhist =  20 fhist = 0.50000

    cmin                cmax        num  '*'=     2 csfs.
 ----------          ----------   ----- ---------|---------|---------|---------|
 5.0000E-01 <= |c| < 1.0000E+00       1 *
 2.5000E-01 <= |c| < 5.0000E-01       0
 1.2500E-01 <= |c| < 2.5000E-01       0
 6.2500E-02 <= |c| < 1.2500E-01       0
 3.1250E-02 <= |c| < 6.2500E-02       0
 1.5625E-02 <= |c| < 3.1250E-02       9 *****
 7.8125E-03 <= |c| < 1.5625E-02      59 ******************************
 3.9062E-03 <= |c| < 7.8125E-03       0
 1.9531E-03 <= |c| < 3.9062E-03       0
 9.7656E-04 <= |c| < 1.9531E-03       0
 4.8828E-04 <= |c| < 9.7656E-04       0
 2.4414E-04 <= |c| < 4.8828E-04       0
 1.2207E-04 <= |c| < 2.4414E-04       0
 6.1035E-05 <= |c| < 1.2207E-04       0
 3.0518E-05 <= |c| < 6.1035E-05       0
 1.5259E-05 <= |c| < 3.0518E-05       0
 7.6294E-06 <= |c| < 1.5259E-05       0
 3.8147E-06 <= |c| < 7.6294E-06       0
 0.0000E+00 <= |c| < 3.8147E-06       0
                                  ----- ---------|---------|---------|---------|
                  total read =       69 total stored =      69

 from the selected csfs,
 min(|csfvec(:)|) = 1.0014E-02    max(|csfvec(:)|) = 9.7146E-01
 norm=  1.
 csfs will be printed based on coefficient magnitudes.

 current csfvec(*) selection parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =   25093 ncsft =   25093 ncsf =      69

 i:slabel(i) =  1: a1   2: b1   3: b2   4: a2 

 internal level =    1    2    3    4    5
 syml(*)        =    1    1    1    2    3
 label          =  a1   a1   a1   b1   b2 
 rmo(*)         =    1    2    3    1    1

 printing selected csfs in sorted order from cmin = 0.01000 to cmax = 1.00000

   indcsf     c     c**2   v  lab:rmo  lab:rmo   step(*)
  ------- -------- ------- - ---- --- ---- --- ------------
        1  0.97146 0.94374 z*                    33333
    10744 -0.02079 0.00043 w           a2 :  1  333330
      589  0.01999 0.00040 x  a1 : 10  a2 :  1 1133322
    12216  0.01931 0.00037 w  b1 :  6  b1 : 10 1233303
    14704 -0.01862 0.00035 w           a1 : 13  333033
    12190 -0.01746 0.00030 w           b1 :  6  333303
        2  0.01632 0.00027 y           b2 :  2  133332
    12680 -0.01632 0.00027 w           a2 :  1  333303
       47  0.01611 0.00026 y           a1 :  6  133233
    10610 -0.01597 0.00026 w  b2 :  6  b2 :  7 1233330
    14699  0.01545 0.00024 w  a1 :  8  a1 : 13 1233033
    13751 -0.01538 0.00024 w  a1 : 13  b1 :  6 1233123
     1038  0.01518 0.00023 x  a1 : 11  b2 :  4 1133232
    14764 -0.01485 0.00022 w  a1 : 13  a1 : 18 1233033
     1989  0.01467 0.00022 x  a1 : 10  b1 :  7 1133223
    12895 -0.01406 0.00020 w  a1 : 13  b2 :  6 1233132
    10611 -0.01386 0.00019 w           b2 :  7  333330
     1516 -0.01358 0.00018 x  b1 :  7  a2 :  1 1133232
    12187  0.01354 0.00018 w  b1 :  3  b1 :  6 1233303
    10601 -0.01350 0.00018 w  b2 :  2  b2 :  6 1233330
      597 -0.01349 0.00018 x  a1 : 18  a2 :  1 1133322
    12196 -0.01339 0.00018 w           b1 :  7  333303
    16749  0.01332 0.00018 w  a1 :  9  b1 :  6 1231323
    13879  0.01317 0.00017 w  a1 : 13  b1 : 10 1233123
    12217  0.01307 0.00017 w  b1 :  7  b1 : 10 1233303
    12195 -0.01294 0.00017 w  b1 :  6  b1 :  7 1233303
    10606 -0.01293 0.00017 w  b2 :  2  b2 :  7 1233330
     1960  0.01276 0.00016 x  a1 : 13  b1 :  6 1133223
    12927 -0.01260 0.00016 w  a1 : 13  b2 :  7 1233132
     1136  0.01258 0.00016 x  a1 : 13  b2 :  7 1133232
    12220 -0.01255 0.00016 w           b1 : 10  333303
    15198 -0.01245 0.00016 w           b1 :  7  333033
    14696 -0.01242 0.00015 w  a1 :  5  a1 : 13 1233033
    13307  0.01237 0.00015 w  b1 :  7  a2 :  1 1233132
    10609  0.01214 0.00015 w  b2 :  5  b2 :  7 1233330
    10605 -0.01211 0.00015 w           b2 :  6  333330
    12188  0.01204 0.00014 w  b1 :  4  b1 :  6 1233303
     1996  0.01202 0.00014 x  a1 : 17  b1 :  7 1133223
      590  0.01179 0.00014 x  a1 : 11  a2 :  1 1133322
     1104  0.01162 0.00013 x  a1 : 13  b2 :  6 1133232
      275  0.01158 0.00013 x  b1 :  6  b2 :  7 1133322
    13746  0.01154 0.00013 w  a1 :  8  b1 :  6 1233123
    13747  0.01152 0.00013 w  a1 :  9  b1 :  6 1233123
    10607 -0.01151 0.00013 w  b2 :  3  b2 :  7 1233330
    16877 -0.01127 0.00013 w  a1 :  9  b1 : 10 1231323
      976  0.01113 0.00012 x  a1 : 13  b2 :  2 1133232
      145  0.01105 0.00012 x  b1 :  6  b2 :  2 1133322
    10604  0.01104 0.00012 w  b2 :  5  b2 :  6 1233330
    10956 -0.01103 0.00012 w  b1 :  6  b2 :  7 1233312
    16755 -0.01100 0.00012 w  a1 : 15  b1 :  6 1231323
    10930 -0.01091 0.00012 w  b1 :  6  b2 :  6 1233312
      598  0.01090 0.00012 x  a1 : 19  a2 :  1 1133322
      249  0.01085 0.00012 x  b1 :  6  b2 :  6 1133322
    11675 -0.01083 0.00012 w           a1 : 10  333303
    15893  0.01078 0.00012 w  a1 :  9  b2 :  6 1231332
    13779  0.01077 0.00012 w  a1 :  9  b1 :  7 1233123
    12767 -0.01077 0.00012 w  a1 : 13  b2 :  2 1233132
    16883  0.01065 0.00011 w  a1 : 15  b1 : 10 1231323
    12262  0.01063 0.00011 w  b1 : 10  b1 : 14 1233303
    16746  0.01061 0.00011 w  a1 :  6  b1 :  6 1231323
    10826 -0.01050 0.00011 w  b1 :  6  b2 :  2 1233312
    10592 -0.01038 0.00011 w  b2 :  2  b2 :  3 1233330
    17723  0.01035 0.00011 w  a1 :  9  a1 : 15 1231233
    12193  0.01031 0.00011 w  b1 :  4  b1 :  7 1233303
     1198 -0.01023 0.00010 x  a1 : 11  b2 :  9 1133232
      171  0.01020 0.00010 x  b1 :  6  b2 :  3 1133322
    13743 -0.01019 0.00010 w  a1 :  5  b1 :  6 1233123
      600  0.01019 0.00010 x  a1 : 21  a2 :  1 1133322
    17672 -0.01001 0.00010 w           a1 :  9  331233
           69 csfs were printed in this range.
