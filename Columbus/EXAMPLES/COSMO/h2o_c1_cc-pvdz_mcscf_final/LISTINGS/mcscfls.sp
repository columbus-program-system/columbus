

     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 This program allows the csf mixing coefficient and orbital expansion coefficient
 optimization using the graphical unitary group approach and the exponential
 operator mcscf method.
 references:  r. shepard and j. simons,                    ' int. j. quantum chem. symp. 14, 211 (1980).
              r. shepard, i. shavitt, and j. simons, j. chem. phys. 76, 543 (1982).
              r. shepard in "ab initio methods in quantum chemistry ii" advances in chemical
                  physics 69, edited by k. p. lawley (wiley, new york, 1987) pp. 63-200.
 Original autor: Ron Shepard, ANL
 Later revisions: Michal Dallos, University Vienna

 This Version of Program MCSCF is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.4.0.2     **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 Workspace allocation information:
        10000000 of real*8 words (   76.29 MB) of work space has been allocated.

 user input information:

 ======== echo of the mcscf input ========
 ------------------------------------------------------------------------
  &input
   niter=60,
   nmiter=30,
   nciitr=30,
   tol(3)=1.e-4,
   tol(2)=1.e-4,
   tol(1)=1.e-8,
   NSTATE=0,
   npath=1,3,9,10,13,17,19,21,-11,12, 2,
   ncoupl=5,
   NAVST(1) = 1,
   WAVST(1,1)=1 ,
   cosmocalc=1
  &end
 ------------------------------------------------------------------------


 ***  Integral file informations  ***


 input integral file : aoints                                                      

 Integral file header information:
 Hermit Integral Program : SIFS version  ronja.itc.univieThu Sep  8 17:09:41 2005

 Core type energy values:
 energy( 1)=  9.317913916904E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 total ao core energy =    9.317913917


   ******  Basis set informations:  ******

 Number of irreps:                  1
 Total number of basis functions:  24

 irrep no.              1
 irrep label           A  
 no. of bas.fcions.    24


 ***  MCSCF optimization procedure parmeters:  ***


 maximum number of mcscf iterations:        niter=    60

 maximum number of psci micro-iterations:   nmiter=   30
 maximum r,s subspace dimension allowed:    nvrsmx=   30

 tol(1)=  1.0000E-08. . . . delta-emc convergence criterion.
 tol(2)=  1.0000E-04. . . . wnorm convergence criterion.
 tol(3)=  1.0000E-04. . . . knorm convergence criterion.
 tol(4)=  1.0000E-08. . . . apxde convergence criterion.
 tol(5)=  1.0000E-04. . . . small diagonal matrix element tolerance.
 tol(6)=  1.0000E-06. . . . minimum ci-psci residual norm.
 tol(7)=  1.0000E-05. . . . maximum ci-psci residual norm.
 tol(8)=  1.0000E+00. . . . maximum abs(k(xy)) allowed.
 tol(9)=  1.0000E+00. . . . wnorm coupling tolerance.
 tol(10)= 0.0000E+00. . . . maximum psci emergency shift parameter.
 tol(11)= 0.0000E+00. . . . minimum psci emergency shift parameter.
 tol(12)= 0.0000E+00. . . . increment of psci emergency shift parameter.


 *** State averaging informations: ***


 MCSCF calculation performed for  1 DRT.

 DRT  first state   no.of aver.states   weights
  1   ground state          1             1.000

 The number of hmc(*) eigenvalues and eigenvectors calculated each iteration per symmetry:
 Symm.   no.of eigenv.(=ncol)
  A          2

 orbital coefficients are optimized for the ground state (nstate=0).

 no fciorb(*,*) specified.

 npath(*) options:
  2:  orbital-state coupling terms will be included beginning on iteration ncoupl=  5
  3:  print intermediate timing information.
  9:  suppress the drt listing.
 10:  suppress the hmc(*) eigenvector listing.
 12:  diagonalize the hmc(*) matrix iteratively.
        nunitv= 1 nciitr=30 mxvadd=20 nvcimx=20
       rtolci(*),wnorm=     1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 0.0000E+00
   noldv =   0
 13:  get initial orbitals from the formatted file, mocoef.
 17:  print the final natural orbitals and occupations.
 19:  transform the virtual orbitals to diagonalize qvv(*).
 21:  write out the one- and two- electron density for further use (files:mcd1fl, mcd2fl).


   ******  DRT info section  ******


 Informations for the DRT no.  1

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a  
 Total number of electrons:   10
 Spin multiplicity:            1
 Number of active orbitals:    0
 Number of active electrons:   0
 Total number of CSFs:         1

 !timer: initialization                  user+sys=     0.010 walltime=     0.000

 faar:   0 active-active rotations allowed out of:   0 possible.


 Number of active-double rotations:      0
 Number of active-active rotations:      0
 Number of double-virtual rotations:    95
 Number of active-virtual rotations:     0

 Size of orbital-Hessian matrix B:                     5415
 Size of the orbital-state Hessian matrix C:             95
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:           5510



   ****** Integral transformation section ******


 number of blocks to be transformed in-core is   0
 number of blocks to be transformed out of core is    1

 in-core ao list requires    0 segments.
 out of core ao list requires    1 segments.
 each segment has length   9993880 working precision words.

               ao integral sort statistics
 length of records:      4096
 number of buckets:   1
 scratch file used is da1:
 amount of core needed for in-core arrays:  1451

 twoao processed      21643 two electron ao integrals.

      42986 ao integrals were written into   16 records

 Source of the initial MO coeficients:

 Input MO coefficient file: mocoef                                                      


               starting mcscf iteration...   1
 !timer:                                 user+sys=     0.210 walltime=     1.000

 orbital-state coupling will not be calculated this iteration.

 number of transformed integrals put on file:      2578
 number of records written:     1
 !timer: 2-e transformation              user+sys=     0.000 walltime=     0.000

 Size of orbital-Hessian matrix B:                     5415
 Total size of the state Hessian matrix M:                0
 Total size of HESSIAN-matrix for linear con           5415

 !timer: mosrt1                          user+sys=     0.000 walltime=     0.000
 !timer: mosrt2                          user+sys=     0.000 walltime=     0.000
 !timer: mosort                          user+sys=     0.000 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000

 trial vectors are generated internally.

 trial vector  1 is unit matrix column     1
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 ciiter=   1 noldhv=  1 noldv=  1
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -76.0270328082      -85.3449467251        0.0000000000        0.0000010000
 !timer: hmcvec                          user+sys=     0.000 walltime=     0.000
 !timer: mfvd                            user+sys=     0.000 walltime=     0.000
 !timer: badad                           user+sys=     0.000 walltime=     0.000
 !timer: badadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvava                           user+sys=     0.000 walltime=     0.000
 !timer: bvdvd                           user+sys=     0.000 walltime=     0.000
 !timer: hbcon                           user+sys=     0.000 walltime=     0.000

  tol(10)=  0. eshsci=  4.72005832E-10
 Total number of micro iterations:    1

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 1.00000000 pnorm= 0.0000E+00 rznorm= 4.6465E-09 rpnorm= 0.0000E+00 noldr=  1 nnewr=  1 nolds=  0 nnews=  0

 !timer: Direct Bxr time contribution    user+sys=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  user+sys=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    user+sys=     0.000 walltime=     0.000
 !timer: solvek total                    user+sys=     0.000 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
   -41.095365   -2.685328   -1.415850   -1.134430   -0.987607

 qvv(*) eigenvalues. symmetry block  1
     0.375778    0.515942    1.603511    1.725483    2.323844    2.401125    2.503516    2.900709    2.953812    3.364362
     3.733210    3.888524    4.972678    5.043343    6.584975    6.710133    7.046588    7.775299    8.314173
 !timer: motran                          user+sys=     0.010 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 user+sys=     0.010 walltime=     0.000

 all mcscf convergence criteria are not satisfied.
 iter=    1 emc=  -76.0270328082 demc= 7.6027E+01 wnorm= 3.7760E-09 knorm= 3.9102E-10 apxde= 3.0078E-16    *not converged* 

               starting mcscf iteration...   2
 !timer:                                 user+sys=     0.220 walltime=     1.000

 orbital-state coupling will not be calculated this iteration.
 ************************************************
 **Call for electrostatic potential calculation**
 ************************************************

 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************


 ediel after cosmo(3 -0.0104563943


 cosurf_and_qcos_from cosmo(3
 end_of_qcos

 Sum of negative charges =  -0.240145551
 Sum of positive charges =   0.235255592
 Total sum =  -0.00488995917

 fepsi =   1.
 *************************************************
 *Call for solvent-modified integrals calculation*
 *************************************************

 number of transformed integrals put on file:      2578
 number of records written:     1
 !timer: 2-e transformation              user+sys=     0.010 walltime=     0.000

 Size of orbital-Hessian matrix B:                     5415
 Total size of the state Hessian matrix M:                0
 Total size of HESSIAN-matrix for linear con           5415

 !timer: mosrt1                          user+sys=     0.000 walltime=     0.000
 !timer: mosrt2                          user+sys=     0.000 walltime=     0.000
 !timer: mosort                          user+sys=     0.000 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000

   1 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 ciiter=   1 noldhv=  1 noldv=  1
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -76.0479455968      -85.4009083785        0.0000000000        0.0000010000
 !timer: hmcvec                          user+sys=     0.000 walltime=     0.000
 !timer: mfvd                            user+sys=     0.000 walltime=     0.000
 !timer: badad                           user+sys=     0.000 walltime=     0.000
 !timer: badadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvava                           user+sys=     0.000 walltime=     0.000
 !timer: bvdvd                           user+sys=     0.000 walltime=     0.000
 !timer: hbcon                           user+sys=     0.010 walltime=     0.000

  tol(10)=  0. eshsci=  0.0129308926
 Total number of micro iterations:    6

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.99969865 pnorm= 0.0000E+00 rznorm= 2.9563E-06 rpnorm= 0.0000E+00 noldr=  6 nnewr=  6 nolds=  0 nnews=  0

 !timer: Direct Bxr time contribution    user+sys=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  user+sys=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    user+sys=     0.000 walltime=     0.000
 !timer: solvek total                    user+sys=     0.000 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
   -41.112720   -2.689487   -1.410387   -1.154247   -1.007700

 qvv(*) eigenvalues. symmetry block  1
     0.409935    0.560615    1.619697    1.756171    2.304670    2.374735    2.492944    2.904323    2.981999    3.380282
     3.724515    3.919909    4.997832    5.068802    6.571739    6.699133    7.037565    7.768013    8.303411
 !timer: motran                          user+sys=     0.000 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 user+sys=     0.940 walltime=     1.000

 all mcscf convergence criteria are not satisfied.
 iter=    2 emc=  -76.0479455968 demc= 2.0913E-02 wnorm= 1.0345E-01 knorm= 2.4548E-02 apxde= 1.0580E-03    *not converged* 

               starting mcscf iteration...   3
 !timer:                                 user+sys=     1.160 walltime=     2.000

 orbital-state coupling will not be calculated this iteration.
 ************************************************
 **Call for electrostatic potential calculation**
 ************************************************

 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************


 ediel after cosmo(3 -0.0104563943


 cosurf_and_qcos_from cosmo(3
 end_of_qcos

 Sum of negative charges =  -0.240145551
 Sum of positive charges =   0.235255592
 Total sum =  -0.00488995917

 fepsi =   1.
 *************************************************
 *Call for solvent-modified integrals calculation*
 *************************************************

 number of transformed integrals put on file:      8955
 number of records written:     4
 !timer: 2-e transformation              user+sys=     0.010 walltime=     0.000

 Size of orbital-Hessian matrix B:                     5415
 Total size of the state Hessian matrix M:                0
 Total size of HESSIAN-matrix for linear con           5415

 !timer: mosrt1                          user+sys=     0.000 walltime=     0.000
 !timer: mosrt2                          user+sys=     0.010 walltime=     0.000
 !timer: mosort                          user+sys=     0.010 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000

   1 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 ciiter=   1 noldhv=  1 noldv=  1
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -76.0490119620      -85.4019747438        0.0000000000        0.0000100000
 !timer: hmcvec                          user+sys=     0.000 walltime=     0.000
 !timer: mfvd                            user+sys=     0.000 walltime=     0.000
 !timer: badad                           user+sys=     0.000 walltime=     0.000
 !timer: badadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvava                           user+sys=     0.000 walltime=     0.000
 !timer: bvdvd                           user+sys=     0.000 walltime=     0.000
 !timer: hbcon                           user+sys=     0.000 walltime=     0.000

  tol(10)=  0. eshsci=  0.000158852463
 Total number of micro iterations:    6

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.99999993 pnorm= 0.0000E+00 rznorm= 7.4442E-08 rpnorm= 0.0000E+00 noldr=  6 nnewr=  6 nolds=  0 nnews=  0

 !timer: Direct Bxr time contribution    user+sys=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  user+sys=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    user+sys=     0.000 walltime=     0.000
 !timer: solvek total                    user+sys=     0.000 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
   -41.092183   -2.681890   -1.409239   -1.143630   -0.992866

 qvv(*) eigenvalues. symmetry block  1
     0.407208    0.557527    1.618024    1.750719    2.313577    2.388071    2.499625    2.904986    2.976476    3.376706
     3.731578    3.912216    4.994737    5.065949    6.582320    6.709764    7.047093    7.776594    8.313726
 !timer: motran                          user+sys=     0.000 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 user+sys=     0.910 walltime=     2.000

 all mcscf convergence criteria are not satisfied.
 iter=    3 emc=  -76.0490119620 demc= 1.0664E-03 wnorm= 1.2708E-03 knorm= 3.8113E-04 apxde= 1.8897E-07    *not converged* 

               starting mcscf iteration...   4
 !timer:                                 user+sys=     2.070 walltime=     4.000

 orbital-state coupling will not be calculated this iteration.
 ************************************************
 **Call for electrostatic potential calculation**
 ************************************************

 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************


 ediel after cosmo(3 -0.0126724697


 cosurf_and_qcos_from cosmo(3
 end_of_qcos

 Sum of negative charges =  -0.262800164
 Sum of positive charges =   0.258274637
 Total sum =  -0.00452552701

 fepsi =   1.
 *************************************************
 *Call for solvent-modified integrals calculation*
 *************************************************

 number of transformed integrals put on file:      8955
 number of records written:     4
 !timer: 2-e transformation              user+sys=     0.010 walltime=     0.000

 Size of orbital-Hessian matrix B:                     5415
 Total size of the state Hessian matrix M:                0
 Total size of HESSIAN-matrix for linear con           5415

 !timer: mosrt1                          user+sys=     0.000 walltime=     0.000
 !timer: mosrt2                          user+sys=     0.000 walltime=     0.000
 !timer: mosort                          user+sys=     0.000 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000

   1 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 ciiter=   1 noldhv=  1 noldv=  1
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -76.0513474700      -85.4088999465        0.0000000000        0.0000010000
 !timer: hmcvec                          user+sys=     0.000 walltime=     0.000
 !timer: mfvd                            user+sys=     0.000 walltime=     0.000
 !timer: badad                           user+sys=     0.000 walltime=     0.000
 !timer: badadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvava                           user+sys=     0.000 walltime=     0.000
 !timer: bvdvd                           user+sys=     0.000 walltime=     0.000
 !timer: hbcon                           user+sys=     0.000 walltime=     0.000

  tol(10)=  0. eshsci=  0.00148672268
 Total number of micro iterations:    5

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.99999633 pnorm= 0.0000E+00 rznorm= 9.0005E-06 rpnorm= 0.0000E+00 noldr=  5 nnewr=  5 nolds=  0 nnews=  0

 !timer: Direct Bxr time contribution    user+sys=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  user+sys=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    user+sys=     0.000 walltime=     0.000
 !timer: solvek total                    user+sys=     0.010 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
   -41.093503   -2.682124   -1.408947   -1.145862   -0.994589

 qvv(*) eigenvalues. symmetry block  1
     0.410715    0.561439    1.619288    1.753713    2.311419    2.385837    2.497759    2.905321    2.979481    3.378522
     3.730986    3.915254    4.997164    5.068520    6.581169    6.708659    7.046127    7.775863    8.312388
 !timer: motran                          user+sys=     0.000 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 user+sys=     0.930 walltime=     3.000

 all mcscf convergence criteria are not satisfied.
 iter=    4 emc=  -76.0513474700 demc= 2.3355E-03 wnorm= 1.1894E-02 knorm= 2.7083E-03 apxde= 1.3199E-05    *not converged* 

               starting mcscf iteration...   5
 !timer:                                 user+sys=     3.000 walltime=     7.000

 orbital-state coupling will be calculated this iteration.
 ************************************************
 **Call for electrostatic potential calculation**
 ************************************************

 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************


 ediel after cosmo(3 -0.0126959344


 cosurf_and_qcos_from cosmo(3
 end_of_qcos

 Sum of negative charges =  -0.263025589
 Sum of positive charges =   0.258505616
 Total sum =  -0.00451997314

 fepsi =   1.
 *************************************************
 *Call for solvent-modified integrals calculation*
 *************************************************

 number of transformed integrals put on file:      8955
 number of records written:     4
 !timer: 2-e transformation              user+sys=     0.010 walltime=     0.000

 Size of orbital-Hessian matrix B:                     5415
 Size of the orbital-state Hessian matrix C:             95
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:           5510

 !timer: mosrt1                          user+sys=     0.000 walltime=     0.000
 !timer: mosrt2                          user+sys=     0.000 walltime=     0.000
 !timer: mosort                          user+sys=     0.000 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000

   1 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 ciiter=   1 noldhv=  1 noldv=  1
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -76.0513844226      -85.4089934254        0.0000000000        0.0000010000
 !timer: hmcvec                          user+sys=     0.000 walltime=     0.000
 !timer: cvdu                            user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: mfvd                            user+sys=     0.000 walltime=     0.000
 !timer: badad                           user+sys=     0.000 walltime=     0.000
 !timer: badadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvava                           user+sys=     0.000 walltime=     0.000
 !timer: bvdvd                           user+sys=     0.000 walltime=     0.000
 !timer: hbcon                           user+sys=     0.010 walltime=     0.000

  tol(10)=  0. eshsci=  1.62359671E-05
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 Total number of micro iterations:    4

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 1.00000000 pnorm= 0.0000E+00 rznorm= 7.5652E-07 rpnorm= 6.6249E-09 noldr=  4 nnewr=  4 nolds=  0 nnews=  0

 !timer: Direct Bxr time contribution    user+sys=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  user+sys=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    user+sys=     0.000 walltime=     0.000
 !timer: solvek total                    user+sys=     0.000 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
   -41.091366   -2.681416   -1.408803   -1.144717   -0.993158

 qvv(*) eigenvalues. symmetry block  1
     0.410353    0.561115    1.619026    1.753048    2.312290    2.387110    2.498485    2.905296    2.978799    3.378038
     3.731676    3.914353    4.996777    5.068121    6.582181    6.709721    7.047047    7.776701    8.313427
 !timer: motran                          user+sys=     0.000 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 user+sys=     0.930 walltime=     1.000

 all mcscf convergence criteria are not satisfied.
 iter=    5 emc=  -76.0513844226 demc= 3.6953E-05 wnorm= 1.2989E-04 knorm= 3.1331E-05 apxde= 1.6562E-09    *not converged* 

               starting mcscf iteration...   6
 !timer:                                 user+sys=     3.930 walltime=     8.000

 orbital-state coupling will be calculated this iteration.
 ************************************************
 **Call for electrostatic potential calculation**
 ************************************************

 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************


 ediel after cosmo(3 -0.0129554587


 cosurf_and_qcos_from cosmo(3
 end_of_qcos

 Sum of negative charges =  -0.265492699
 Sum of positive charges =   0.261006367
 Total sum =  -0.00448633207

 fepsi =   1.
 *************************************************
 *Call for solvent-modified integrals calculation*
 *************************************************

 number of transformed integrals put on file:      8955
 number of records written:     4
 !timer: 2-e transformation              user+sys=     0.010 walltime=     0.000

 Size of orbital-Hessian matrix B:                     5415
 Size of the orbital-state Hessian matrix C:             95
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:           5510

 !timer: mosrt1                          user+sys=     0.000 walltime=     0.000
 !timer: mosrt2                          user+sys=     0.000 walltime=     0.000
 !timer: mosort                          user+sys=     0.000 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000

   1 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 ciiter=   1 noldhv=  1 noldv=  1
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -76.0516454779      -85.4096870478        0.0000000000        0.0000010000
 !timer: hmcvec                          user+sys=     0.000 walltime=     0.000
 !timer: cvdu                            user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: mfvd                            user+sys=     0.000 walltime=     0.000
 !timer: badad                           user+sys=     0.000 walltime=     0.000
 !timer: badadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvava                           user+sys=     0.000 walltime=     0.000
 !timer: bvdvd                           user+sys=     0.000 walltime=     0.000
 !timer: hbcon                           user+sys=     0.010 walltime=     0.000

  tol(10)=  0. eshsci=  0.000169843101
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 Total number of micro iterations:    6

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.99999995 pnorm= 0.0000E+00 rznorm= 2.8904E-08 rpnorm= 0.0000E+00 noldr=  6 nnewr=  6 nolds=  0 nnews=  0

 !timer: Direct Bxr time contribution    user+sys=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  user+sys=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    user+sys=     0.000 walltime=     0.000
 !timer: solvek total                    user+sys=     0.000 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
   -41.091544   -2.681444   -1.408778   -1.144964   -0.993345

 qvv(*) eigenvalues. symmetry block  1
     0.410753    0.561538    1.619164    1.753382    2.312055    2.386874    2.498264    2.905339    2.979142    3.378251
     3.731620    3.914698    4.997043    5.068404    6.582055    6.709595    7.046936    7.776617    8.313269
 !timer: motran                          user+sys=     0.000 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 user+sys=     0.920 walltime=     2.000

 all mcscf convergence criteria are not satisfied.
 iter=    6 emc=  -76.0516454779 demc= 2.6106E-04 wnorm= 1.3587E-03 knorm= 3.0581E-04 apxde= 1.6971E-07    *not converged* 

               starting mcscf iteration...   7
 !timer:                                 user+sys=     4.850 walltime=    10.000

 orbital-state coupling will be calculated this iteration.
 ************************************************
 **Call for electrostatic potential calculation**
 ************************************************

 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************


 ediel after cosmo(3 -0.0129583722


 cosurf_and_qcos_from cosmo(3
 end_of_qcos

 Sum of negative charges =  -0.26552022
 Sum of positive charges =   0.261034296
 Total sum =  -0.00448592347

 fepsi =   1.
 *************************************************
 *Call for solvent-modified integrals calculation*
 *************************************************

 number of transformed integrals put on file:      8955
 number of records written:     4
 !timer: 2-e transformation              user+sys=     0.010 walltime=     0.000

 Size of orbital-Hessian matrix B:                     5415
 Size of the orbital-state Hessian matrix C:             95
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:           5510

 !timer: mosrt1                          user+sys=     0.000 walltime=     0.000
 !timer: mosrt2                          user+sys=     0.000 walltime=     0.000
 !timer: mosort                          user+sys=     0.000 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000

   1 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 ciiter=   1 noldhv=  1 noldv=  1
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -76.0516485651      -85.4096951497        0.0000000000        0.0000010000
 !timer: hmcvec                          user+sys=     0.000 walltime=     0.000
 !timer: cvdu                            user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: mfvd                            user+sys=     0.000 walltime=     0.000
 !timer: badad                           user+sys=     0.000 walltime=     0.000
 !timer: badadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvava                           user+sys=     0.000 walltime=     0.000
 !timer: bvdvd                           user+sys=     0.000 walltime=     0.000
 !timer: hbcon                           user+sys=     0.000 walltime=     0.000

  tol(10)=  0. eshsci=  1.90815602E-06
 performing all-state projection
 performing all-state projection
 performing all-state projection
 Total number of micro iterations:    3

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 1.00000000 pnorm= 0.0000E+00 rznorm= 5.2710E-07 rpnorm= 8.6199E-11 noldr=  3 nnewr=  3 nolds=  0 nnews=  0

 !timer: Direct Bxr time contribution    user+sys=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  user+sys=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    user+sys=     0.000 walltime=     0.000
 !timer: solvek total                    user+sys=     0.000 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
   -41.091306   -2.681367   -1.408761   -1.144836   -0.993188

 qvv(*) eigenvalues. symmetry block  1
     0.410711    0.561502    1.619133    1.753306    2.312151    2.387013    2.498346    2.905335    2.979064    3.378194
     3.731696    3.914595    4.996998    5.068357    6.582166    6.709713    7.047037    7.776710    8.313384
 !timer: motran                          user+sys=     0.010 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 user+sys=     0.920 walltime=     2.000

 all mcscf convergence criteria are not satisfied.
 iter=    7 emc=  -76.0516485651 demc= 3.0872E-06 wnorm= 1.5265E-05 knorm= 3.4531E-06 apxde= 2.1550E-11    *not converged* 

               starting mcscf iteration...   8
 !timer:                                 user+sys=     5.770 walltime=    12.000

 orbital-state coupling will be calculated this iteration.
 ************************************************
 **Call for electrostatic potential calculation**
 ************************************************

 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************


 ediel after cosmo(3 -0.0129878361


 cosurf_and_qcos_from cosmo(3
 end_of_qcos

 Sum of negative charges =  -0.265797509
 Sum of positive charges =   0.261315259
 Total sum =  -0.00448225075

 fepsi =   1.
 *************************************************
 *Call for solvent-modified integrals calculation*
 *************************************************

 number of transformed integrals put on file:      8955
 number of records written:     4
 !timer: 2-e transformation              user+sys=     0.010 walltime=     0.000

 Size of orbital-Hessian matrix B:                     5415
 Size of the orbital-state Hessian matrix C:             95
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:           5510

 !timer: mosrt1                          user+sys=     0.000 walltime=     0.000
 !timer: mosrt2                          user+sys=     0.000 walltime=     0.000
 !timer: mosort                          user+sys=     0.000 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000

   1 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 ciiter=   1 noldhv=  1 noldv=  1
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -76.0516780487      -85.4097716592        0.0000000000        0.0000010000
 !timer: hmcvec                          user+sys=     0.000 walltime=     0.000
 !timer: cvdu                            user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: mfvd                            user+sys=     0.000 walltime=     0.000
 !timer: badad                           user+sys=     0.000 walltime=     0.000
 !timer: badadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvava                           user+sys=     0.000 walltime=     0.000
 !timer: bvdvd                           user+sys=     0.000 walltime=     0.000
 !timer: hbcon                           user+sys=     0.010 walltime=     0.000

  tol(10)=  0. eshsci=  1.9302562E-05
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 Total number of micro iterations:    4

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 1.00000000 pnorm= 0.0000E+00 rznorm= 8.9833E-07 rpnorm= 8.7406E-09 noldr=  4 nnewr=  4 nolds=  0 nnews=  0

 !timer: Direct Bxr time contribution    user+sys=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  user+sys=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    user+sys=     0.000 walltime=     0.000
 !timer: solvek total                    user+sys=     0.000 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
   -41.091327   -2.681370   -1.408759   -1.144864   -0.993209

 qvv(*) eigenvalues. symmetry block  1
     0.410756    0.561550    1.619149    1.753343    2.312124    2.386987    2.498320    2.905340    2.979103    3.378219
     3.731690    3.914634    4.997028    5.068389    6.582152    6.709699    7.047024    7.776700    8.313366
 !timer: motran                          user+sys=     0.000 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 user+sys=     0.910 walltime=     2.000

 all mcscf convergence criteria are not satisfied.
 iter=    8 emc=  -76.0516780487 demc= 2.9484E-05 wnorm= 1.5442E-04 knorm= 3.4682E-05 apxde= 2.1852E-09    *not converged* 

               starting mcscf iteration...   9
 !timer:                                 user+sys=     6.680 walltime=    14.000

 orbital-state coupling will be calculated this iteration.
 ************************************************
 **Call for electrostatic potential calculation**
 ************************************************

 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************


 ediel after cosmo(3 -0.0129881676


 cosurf_and_qcos_from cosmo(3
 end_of_qcos

 Sum of negative charges =  -0.265800622
 Sum of positive charges =   0.261318413
 Total sum =  -0.0044822095

 fepsi =   1.
 *************************************************
 *Call for solvent-modified integrals calculation*
 *************************************************

 number of transformed integrals put on file:      8955
 number of records written:     4
 !timer: 2-e transformation              user+sys=     0.010 walltime=     0.000

 Size of orbital-Hessian matrix B:                     5415
 Size of the orbital-state Hessian matrix C:             95
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:           5510

 !timer: mosrt1                          user+sys=     0.000 walltime=     0.000
 !timer: mosrt2                          user+sys=     0.000 walltime=     0.000
 !timer: mosort                          user+sys=     0.000 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000

   1 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 ciiter=   1 noldhv=  1 noldv=  1
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -76.0516783824      -85.4097725168        0.0000000000        0.0000010000
 !timer: hmcvec                          user+sys=     0.000 walltime=     0.000
 !timer: cvdu                            user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: mfvd                            user+sys=     0.000 walltime=     0.000
 !timer: badad                           user+sys=     0.000 walltime=     0.000
 !timer: badadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvava                           user+sys=     0.000 walltime=     0.000
 !timer: bvdvd                           user+sys=     0.000 walltime=     0.000
 !timer: hbcon                           user+sys=     0.000 walltime=     0.000

  tol(10)=  0. eshsci=  2.44921441E-07
 performing all-state projection
 performing all-state projection
 Total number of micro iterations:    2

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 1.00000000 pnorm= 0.0000E+00 rznorm= 3.2912E-07 rpnorm= 1.2640E-12 noldr=  2 nnewr=  2 nolds=  0 nnews=  0

 !timer: Direct Bxr time contribution    user+sys=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  user+sys=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    user+sys=     0.000 walltime=     0.000
 !timer: solvek total                    user+sys=     0.000 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
   -41.091300   -2.681361   -1.408757   -1.144849   -0.993191

 qvv(*) eigenvalues. symmetry block  1
     0.410751    0.561546    1.619145    1.753335    2.312135    2.387003    2.498330    2.905339    2.979094    3.378212
     3.731699    3.914622    4.997023    5.068384    6.582164    6.709712    7.047036    7.776711    8.313379
 !timer: motran                          user+sys=     0.010 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 user+sys=     0.910 walltime=     2.000

 all mcscf convergence criteria are not satisfied.
 iter=    9 emc=  -76.0516783824 demc= 3.3374E-07 wnorm= 1.9594E-06 knorm= 3.9168E-07 apxde= 3.1715E-13    *not converged* 

               starting mcscf iteration...  10
 !timer:                                 user+sys=     7.590 walltime=    16.000

 orbital-state coupling will be calculated this iteration.
 ************************************************
 **Call for electrostatic potential calculation**
 ************************************************

 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************


 ediel after cosmo(3 -0.0129915087


 cosurf_and_qcos_from cosmo(3
 end_of_qcos

 Sum of negative charges =  -0.265832013
 Sum of positive charges =   0.261350219
 Total sum =  -0.00448179411

 fepsi =   1.
 *************************************************
 *Call for solvent-modified integrals calculation*
 *************************************************

 number of transformed integrals put on file:      8955
 number of records written:     4
 !timer: 2-e transformation              user+sys=     0.010 walltime=     0.000

 Size of orbital-Hessian matrix B:                     5415
 Size of the orbital-state Hessian matrix C:             95
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:           5510

 !timer: mosrt1                          user+sys=     0.000 walltime=     0.000
 !timer: mosrt2                          user+sys=     0.000 walltime=     0.000
 !timer: mosort                          user+sys=     0.000 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000

   1 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 ciiter=   1 noldhv=  1 noldv=  1
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -76.0516817238      -85.4097811514        0.0000000000        0.0000010000
 !timer: hmcvec                          user+sys=     0.000 walltime=     0.000
 !timer: cvdu                            user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: mfvd                            user+sys=     0.000 walltime=     0.000
 !timer: badad                           user+sys=     0.000 walltime=     0.000
 !timer: badadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvava                           user+sys=     0.000 walltime=     0.000
 !timer: bvdvd                           user+sys=     0.000 walltime=     0.000
 !timer: hbcon                           user+sys=     0.010 walltime=     0.000

  tol(10)=  0. eshsci=  2.18395011E-06
 performing all-state projection
 performing all-state projection
 performing all-state projection
 Total number of micro iterations:    3

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 1.00000000 pnorm= 0.0000E+00 rznorm= 6.2095E-07 rpnorm= 1.1241E-10 noldr=  3 nnewr=  3 nolds=  0 nnews=  0

 !timer: Direct Bxr time contribution    user+sys=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  user+sys=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    user+sys=     0.000 walltime=     0.000
 !timer: solvek total                    user+sys=     0.000 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
   -41.091302   -2.681362   -1.408756   -1.144852   -0.993193

 qvv(*) eigenvalues. symmetry block  1
     0.410757    0.561551    1.619147    1.753339    2.312132    2.387000    2.498327    2.905340    2.979099    3.378215
     3.731698    3.914627    4.997026    5.068387    6.582163    6.709710    7.047034    7.776710    8.313377
 !timer: motran                          user+sys=     0.000 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 user+sys=     0.910 walltime=     2.000

 all mcscf convergence criteria are not satisfied.
 iter=   10 emc=  -76.0516817238 demc= 3.3414E-06 wnorm= 1.7472E-05 knorm= 3.9304E-06 apxde= 2.8101E-11    *not converged* 

               starting mcscf iteration...  11
 !timer:                                 user+sys=     8.500 walltime=    18.000

 orbital-state coupling will be calculated this iteration.
 ************************************************
 **Call for electrostatic potential calculation**
 ************************************************

 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************


 ediel after cosmo(3 -0.012991547


 cosurf_and_qcos_from cosmo(3
 end_of_qcos

 Sum of negative charges =  -0.265832379
 Sum of positive charges =   0.261350588
 Total sum =  -0.00448179049

 fepsi =   1.
 *************************************************
 *Call for solvent-modified integrals calculation*
 *************************************************

 number of transformed integrals put on file:      8955
 number of records written:     4
 !timer: 2-e transformation              user+sys=     0.010 walltime=     0.000

 Size of orbital-Hessian matrix B:                     5415
 Size of the orbital-state Hessian matrix C:             95
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:           5510

 !timer: mosrt1                          user+sys=     0.000 walltime=     0.000
 !timer: mosrt2                          user+sys=     0.000 walltime=     0.000
 !timer: mosort                          user+sys=     0.000 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000

   1 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 ciiter=   1 noldhv=  1 noldv=  1
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -76.0516817621      -85.4097812491        0.0000000000        0.0000010000
 !timer: hmcvec                          user+sys=     0.000 walltime=     0.000
 !timer: cvdu                            user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: mfvd                            user+sys=     0.000 walltime=     0.000
 !timer: badad                           user+sys=     0.000 walltime=     0.000
 !timer: badadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvava                           user+sys=     0.000 walltime=     0.000
 !timer: bvdvd                           user+sys=     0.000 walltime=     0.000
 !timer: hbcon                           user+sys=     0.010 walltime=     0.000

  tol(10)=  0. eshsci=  8.15747817E-08
 performing all-state projection
 Total number of micro iterations:    1

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 1.00000000 pnorm= 0.0000E+00 rznorm= 8.7060E-07 rpnorm= 6.4797E-14 noldr=  1 nnewr=  1 nolds=  0 nnews=  0

 !timer: Direct Bxr time contribution    user+sys=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  user+sys=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    user+sys=     0.000 walltime=     0.000
 !timer: solvek total                    user+sys=     0.000 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
   -41.091300   -2.681361   -1.408756   -1.144851   -0.993192

 qvv(*) eigenvalues. symmetry block  1
     0.410756    0.561551    1.619147    1.753338    2.312134    2.387001    2.498328    2.905340    2.979097    3.378214
     3.731699    3.914626    4.997026    5.068386    6.582164    6.709712    7.047035    7.776711    8.313378
 !timer: motran                          user+sys=     0.000 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 user+sys=     0.910 walltime=     1.000

 all mcscf convergence criteria are not satisfied.
 iter=   11 emc=  -76.0516817621 demc= 3.8307E-08 wnorm= 6.5260E-07 knorm= 4.9645E-08 apxde= 1.6356E-14    *not converged* 

               starting mcscf iteration...  12
 !timer:                                 user+sys=     9.410 walltime=    19.000

 orbital-state coupling will be calculated this iteration.
 ************************************************
 **Call for electrostatic potential calculation**
 ************************************************

 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************


 ediel after cosmo(3 -0.0129919252


 cosurf_and_qcos_from cosmo(3
 end_of_qcos

 Sum of negative charges =  -0.265835928
 Sum of positive charges =   0.261354183
 Total sum =  -0.00448174413

 fepsi =   1.
 *************************************************
 *Call for solvent-modified integrals calculation*
 *************************************************

 number of transformed integrals put on file:      8955
 number of records written:     4
 !timer: 2-e transformation              user+sys=     0.010 walltime=     0.000

 Size of orbital-Hessian matrix B:                     5415
 Size of the orbital-state Hessian matrix C:             95
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:           5510

 !timer: mosrt1                          user+sys=     0.000 walltime=     0.000
 !timer: mosrt2                          user+sys=     0.000 walltime=     0.000
 !timer: mosort                          user+sys=     0.000 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000

   1 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 ciiter=   1 noldhv=  1 noldv=  1
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -76.0516821403      -85.4097822182        0.0000000000        0.0000010000
 !timer: hmcvec                          user+sys=     0.000 walltime=     0.000
 !timer: cvdu                            user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: mfvd                            user+sys=     0.000 walltime=     0.000
 !timer: badad                           user+sys=     0.000 walltime=     0.000
 !timer: badadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvava                           user+sys=     0.000 walltime=     0.000
 !timer: bvdvd                           user+sys=     0.000 walltime=     0.000
 !timer: hbcon                           user+sys=     0.010 walltime=     0.000

  tol(10)=  0. eshsci=  2.72457412E-07
 performing all-state projection
 performing all-state projection
 Total number of micro iterations:    2

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 1.00000000 pnorm= 0.0000E+00 rznorm= 5.5008E-07 rpnorm= 1.6325E-12 noldr=  2 nnewr=  2 nolds=  0 nnews=  0

 !timer: Direct Bxr time contribution    user+sys=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  user+sys=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    user+sys=     0.000 walltime=     0.000
 !timer: solvek total                    user+sys=     0.000 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
   -41.091300   -2.681361   -1.408756   -1.144851   -0.993192

 qvv(*) eigenvalues. symmetry block  1
     0.410757    0.561551    1.619147    1.753339    2.312133    2.387001    2.498328    2.905340    2.979098    3.378215
     3.731699    3.914626    4.997026    5.068387    6.582164    6.709712    7.047035    7.776711    8.313378
 !timer: motran                          user+sys=     0.000 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 user+sys=     0.960 walltime=     2.000

 all mcscf convergence criteria are not satisfied.
 iter=   12 emc=  -76.0516821403 demc= 3.7819E-07 wnorm= 2.1797E-06 knorm= 4.6309E-07 apxde= 4.0824E-13    *not converged* 

               starting mcscf iteration...  13
 !timer:                                 user+sys=    10.370 walltime=    21.000

 orbital-state coupling will be calculated this iteration.
 ************************************************
 **Call for electrostatic potential calculation**
 ************************************************

 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************


 ediel after cosmo(3 -0.0129919271


 cosurf_and_qcos_from cosmo(3
 end_of_qcos

 Sum of negative charges =  -0.265835947
 Sum of positive charges =   0.261354203
 Total sum =  -0.00448174385

 fepsi =   1.
 *************************************************
 *Call for solvent-modified integrals calculation*
 *************************************************

 number of transformed integrals put on file:      8955
 number of records written:     4
 !timer: 2-e transformation              user+sys=     0.010 walltime=     0.000

 Size of orbital-Hessian matrix B:                     5415
 Size of the orbital-state Hessian matrix C:             95
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:           5510

 !timer: mosrt1                          user+sys=     0.000 walltime=     0.000
 !timer: mosrt2                          user+sys=     0.000 walltime=     0.000
 !timer: mosort                          user+sys=     0.000 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000

   1 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           user+sys=     0.000 walltime=     0.000
 ciiter=   1 noldhv=  1 noldv=  1
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -76.0516821422      -85.4097822235        0.0000000000        0.0000010000
 !timer: hmcvec                          user+sys=     0.000 walltime=     0.000
 !timer: cvdu                            user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: mfvd                            user+sys=     0.000 walltime=     0.000
 !timer: badad                           user+sys=     0.000 walltime=     0.000
 !timer: badadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvava                           user+sys=     0.000 walltime=     0.000
 !timer: bvdvd                           user+sys=     0.000 walltime=     0.000
 !timer: hbcon                           user+sys=     0.010 walltime=     0.000

  tol(10)=  0. eshsci=  6.86319719E-08
 performing all-state projection
 Total number of micro iterations:    1

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 1.00000000 pnorm= 0.0000E+00 rznorm= 6.5156E-07 rpnorm= 3.8294E-14 noldr=  1 nnewr=  1 nolds=  0 nnews=  0

 !timer: Direct Bxr time contribution    user+sys=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  user+sys=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    user+sys=     0.000 walltime=     0.000
 !timer: solvek total                    user+sys=     0.000 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
   -41.091299   -2.681361   -1.408756   -1.144851   -0.993191

 qvv(*) eigenvalues. symmetry block  1
     0.410757    0.561551    1.619147    1.753338    2.312133    2.387001    2.498328    2.905340    2.979098    3.378215
     3.731699    3.914626    4.997026    5.068387    6.582164    6.709712    7.047036    7.776711    8.313378
 !timer: motran                          user+sys=     0.000 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 user+sys=     0.910 walltime=     2.000

 all mcscf convergence criteria are satisfied.
 symf =   1.
 elast =  -76.0386902
 ediel =  -0.0129919271

 **************************************************
 ***Final cosmo calculation for ground state*******
 **************************************************



 final mcscf convergence values:
 iter=   13 emc=  -76.0516821422 demc= 1.9195E-09 wnorm= 5.4906E-07 knorm= 3.4873E-08 apxde= 9.4162E-15    *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 weight 1.000000 total energy=  -76.051682142
   ------------------------------------------------------------



          mcscf orbitals of the final iteration,  A   block   1

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
   1O1s       1.00043657    -0.00987293     0.00000101    -0.00342104    -0.00000086    -0.04809012    -0.00000776    -0.00001546
   2O1s       0.00253317     0.86708490    -0.00002874     0.26980700    -0.00001438     0.16764139     0.00002826    -0.00009857
   3O1s      -0.00150540    -0.12185970    -0.00002038     0.17945709    -0.00000131     0.92340213     0.00016778     0.00016435
   4O1px      0.00000005     0.00000029     0.72341360     0.00006811     0.00000990     0.00007676    -0.40699872    -0.36181544
   5O1py      0.00000006     0.00000218    -0.00000510     0.00001359     0.91213462     0.00001243     0.00000846    -0.00001022
   6O1pz     -0.00243732    -0.11275260    -0.00003732     0.80241037    -0.00001082    -0.24245586    -0.00001351     0.00010050
   7O1px     -0.00000003    -0.00000492    -0.10284431    -0.00001511    -0.00000397     0.00009130    -0.47329999    -0.36262019
   8O1py     -0.00000004     0.00000194    -0.00000330     0.00000329     0.08305165    -0.00000750    -0.00000564     0.00001161
   9O1pz      0.00155087     0.07266376    -0.00000727     0.00963727     0.00000186    -0.22258896    -0.00005093    -0.00005034
  10O1d2-     0.00000000    -0.00000031     0.00000068    -0.00000028    -0.00000072    -0.00000019    -0.00000045    -0.00000073
  11O1d1-     0.00000000    -0.00000003     0.00000002     0.00000116    -0.01634796    -0.00000042    -0.00000003     0.00000035
  12O1d0      0.00000372     0.00024145     0.00000022    -0.00470989     0.00000002     0.00189389    -0.00000005     0.00000015
  13O1d1+     0.00000002    -0.00000032    -0.02597928    -0.00000214    -0.00000038    -0.00000365     0.02018703    -0.11250941
  14O1d2+     0.00008529     0.00150253     0.00000052    -0.00229592    -0.00000027     0.00553349     0.00000202     0.00000986
  15H1s      -0.00049921     0.33136524    -0.54761558    -0.34250727    -0.00000429    -0.11032545    -0.04494842    -1.63682399
  16H1s       0.00067577    -0.15733239     0.19985628     0.14735378     0.00001164    -0.78056145    -1.43844153     1.46321646
  17H1px     -0.00058847     0.03831751    -0.02420735    -0.03061867     0.00000138    -0.02270193    -0.02171062     0.07123997
  18H1py      0.00000000     0.00000149    -0.00000250    -0.00000217     0.02812237    -0.00000168    -0.00000086    -0.00000352
  19H1pz     -0.00048648     0.02097186    -0.03383307     0.00447769    -0.00000097    -0.01779039    -0.01893274     0.14698982
  20H2s      -0.00049921     0.33140126     0.54768428    -0.34246060     0.00001110    -0.11041457     0.04489152     1.63723753
  21H2s       0.00067577    -0.15734456    -0.19987479     0.14731827    -0.00000457    -0.78095553     1.43818680    -1.46366631
  22H2px      0.00058848    -0.03832351    -0.02421368     0.03062470    -0.00000130     0.02271603    -0.02169828     0.07131507
  23H2py      0.00000000    -0.00000015    -0.00000001    -0.00000093     0.02812411     0.00000186     0.00000070     0.00000102
  24H2pz     -0.00048651     0.02096388     0.03381832     0.00450355    -0.00000056    -0.01776822     0.01890540    -0.14707176

               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
   1O1s       0.05837261    -0.01463794     0.00000503     0.00000870     0.21325130    -0.00002019    -0.00000883    -0.71306010
   2O1s       0.47977046    -0.28384888     0.00001375     0.00003068     1.08093107    -0.00009754    -0.00004048    -3.08024155
   3O1s      -0.57438919    -0.57659414    -0.00006745    -0.00009282    -1.24608491     0.00012168     0.00005000     4.82400208
   4O1px     -0.00004719     0.00005730    -0.00004769    -1.06890308     0.00000411    -0.00000037     0.00000034    -0.00000738
   5O1py     -0.00000840    -0.00007230     1.41271755    -0.00005634    -0.00001982     0.00000186    -0.08871657     0.00000314
   6O1pz     -0.48947635    -1.10585127    -0.00005761    -0.00007203     0.12641884    -0.00000882    -0.00001141    -0.19100291
   7O1px     -0.00009023    -0.00019771     0.00009489     2.27524437    -0.00012402    -0.00000367     0.00001031    -0.00006650
   8O1py      0.00000952     0.00009466    -1.69638794     0.00007156     0.00002590     0.00004183    -0.56197780     0.00000208
   9O1pz      0.25590076     1.83332912     0.00008910     0.00011222    -0.69801825     0.00003699     0.00001054    -0.88829029
  10O1d2-     0.00000008    -0.00000019     0.00000020     0.00000057    -0.00000810    -0.13250397    -0.00001137     0.00000130
  11O1d1-    -0.00000044     0.00000123    -0.00544083     0.00000001    -0.00000046     0.00000822    -0.16332329     0.00000237
  12O1d0     -0.00006927    -0.00230672    -0.00000041    -0.00000065    -0.03346226     0.00000211    -0.00000029    -0.03395277
  13O1d1+    -0.00001734    -0.00000114    -0.00000316    -0.05157093    -0.00000983    -0.00000102    -0.00000011    -0.00000361
  14O1d2+    -0.05503449     0.00348413    -0.00000025    -0.00000061     0.00059954    -0.00000058     0.00000004    -0.05356872
  15H1s      -1.32122262     0.99413534     0.00005701     0.63508366    -0.49221346     0.00000956    -0.00000219    -1.44397065
  16H1s       1.19628079    -0.40787994     0.00003220     0.54786309     0.48268550    -0.00002106     0.00000197     0.32860620
  17H1px      0.30439962     0.10428505     0.00001374     0.29879213    -0.29900175     0.00002488    -0.00000261    -0.38486253
  18H1py     -0.00000246    -0.00001035     0.02079210    -0.00000495     0.00004430     0.68488813     0.76959246    -0.00001389
  19H1pz      0.05238693     0.20940359     0.00002872     0.19637801     0.57166154    -0.00003959     0.00000990     0.03757260
  20H2s      -1.32066290     0.99436013     0.00002260    -0.63500678    -0.49194934     0.00002583    -0.00001147    -1.44384531
  21H2s       1.19583578    -0.40786439    -0.00004046    -0.54785647     0.48257009    -0.00003481    -0.00000128     0.32855113
  22H2px     -0.30439001    -0.10428002     0.00000920     0.29878657     0.29906457    -0.00001085     0.00000136     0.38494300
  23H2py      0.00000059    -0.00001024     0.02078727    -0.00000107    -0.00004454    -0.68498759     0.76950763    -0.00000275
  24H2pz      0.05235471     0.20949125     0.00000994    -0.19633522     0.57175889    -0.00003111    -0.00000331     0.03768685

               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
   1O1s       0.00004494     0.00012024    -0.31014125     0.00000020    -0.00000058     0.08687533    -0.11493847     0.00000138
   2O1s       0.00018869     0.00060499    -1.54958264     0.00000087    -0.00000171     0.30568004    -0.23407444    -0.00000089
   3O1s      -0.00029765    -0.00004320     0.14186275    -0.00000047     0.00000513    -0.82310456     2.43176456    -0.00005019
   4O1px      0.00727239     1.24211593     0.00047560     0.00000038    -0.00000169    -0.00001089     0.00000949     0.73382794
   5O1py      0.00000091     0.00000139     0.00000194     0.00323784    -0.00000014     0.00000060     0.00000046    -0.00000020
   6O1pz      0.00005228    -0.00038999     0.97419231    -0.00000233    -0.00000044     0.06679018    -0.62510140     0.00001439
   7O1px     -0.90398837    -0.41151295    -0.00015605     0.00000180     0.00000005    -0.00001356     0.00001321     0.83052800
   8O1py     -0.00000568    -0.00000088    -0.00000124    -0.32163539    -0.00002183    -0.00000098     0.00000077     0.00000069
   9O1pz      0.00003770     0.00011342    -0.28542644     0.00000100    -0.00000372     0.54759455    -0.61833009     0.00001697
  10O1d2-     0.00000195     0.00000114     0.00000056    -0.00007136     1.07184212     0.00001006     0.00000106    -0.00000001
  11O1d1-    -0.00000165     0.00000044    -0.00000015     1.04520183     0.00006886    -0.00000030    -0.00000138    -0.00000117
  12O1d0      0.00000945    -0.00000822     0.01673762    -0.00000003     0.00000298    -0.31587365     0.02337949    -0.00000288
  13O1d1+    -0.02874783     0.15206703     0.00006366    -0.00000160    -0.00000027     0.00001030    -0.00002516    -1.32881595
  14O1d2+     0.00000045    -0.00004234     0.11538380     0.00000091    -0.00000125     0.06552468     0.58592042    -0.00000986
  15H1s      -0.66363510     0.53318933     0.81453276     0.00000042    -0.00000497     0.60541386    -2.17044988     1.92682683
  16H1s       0.24145105     0.06120692    -0.25702866     0.00000078     0.00000147    -0.25727981     0.89563069    -0.67132350
  17H1px      0.48269839     0.72710149     0.74889218     0.00000098    -0.00000380     0.37843775    -0.62542896     0.62350277
  18H1py     -0.00000114    -0.00000258    -0.00000266     0.40917094     0.38344142     0.00000413    -0.00000059    -0.00000096
  19H1pz     -0.69131315     0.56327386     0.52892366     0.00000010     0.00000325    -0.31152560    -0.51320810     0.49237578
  20H2s       0.66387631    -0.53385512     0.81413055    -0.00000383    -0.00000201     0.60545113    -2.17051216    -1.92672940
  21H2s      -0.24152943    -0.06099392    -0.25708376     0.00000149     0.00000085    -0.25728829     0.89565196     0.67128581
  22H2px      0.48260753     0.72768848    -0.74832349     0.00000121     0.00000202    -0.37844297     0.62544943     0.62346621
  23H2py      0.00001094    -0.00000073    -0.00000054     0.40921897    -0.38338288    -0.00000402    -0.00000171    -0.00000037
  24H2pz      0.69121550    -0.56368887     0.52848761    -0.00000040     0.00000295    -0.31149226    -0.51321397    -0.49236107

          natural orbitals of the final iteration, block  1

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8

  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     0.00000000     0.00000000     0.00000000

   1O1s       1.00043657    -0.00987294     0.00000101    -0.00342104    -0.00000086    -0.04809014    -0.00000776    -0.00001546
   2O1s       0.00253317     0.86708486    -0.00002874     0.26980714    -0.00001438     0.16764128     0.00002826    -0.00009856
   3O1s      -0.00150540    -0.12185972    -0.00002038     0.17945707    -0.00000131     0.92340220     0.00016777     0.00016434
   4O1px      0.00000005     0.00000029     0.72341360     0.00006811     0.00000990     0.00007676    -0.40699865    -0.36181549
   5O1py      0.00000006     0.00000218    -0.00000510     0.00001359     0.91213462     0.00001243     0.00000846    -0.00001022
   6O1pz     -0.00243732    -0.11275272    -0.00003731     0.80241036    -0.00001082    -0.24245590    -0.00001351     0.00010049
   7O1px     -0.00000003    -0.00000492    -0.10284431    -0.00001511    -0.00000397     0.00009129    -0.47330004    -0.36262004
   8O1py     -0.00000004     0.00000194    -0.00000330     0.00000329     0.08305165    -0.00000750    -0.00000564     0.00001161
   9O1pz      0.00155087     0.07266375    -0.00000727     0.00963728     0.00000186    -0.22258887    -0.00005093    -0.00005034
  10O1d2-     0.00000000    -0.00000031     0.00000068    -0.00000028    -0.00000072    -0.00000019    -0.00000045    -0.00000073
  11O1d1-     0.00000000    -0.00000003     0.00000002     0.00000116    -0.01634796    -0.00000042    -0.00000003     0.00000035
  12O1d0      0.00000372     0.00024145     0.00000022    -0.00470989     0.00000002     0.00189389    -0.00000005     0.00000015
  13O1d1+     0.00000002    -0.00000032    -0.02597928    -0.00000214    -0.00000038    -0.00000365     0.02018704    -0.11250939
  14O1d2+     0.00008529     0.00150253     0.00000052    -0.00229592    -0.00000027     0.00553349     0.00000202     0.00000986
  15H1s      -0.00049921     0.33136530    -0.54761558    -0.34250721    -0.00000429    -0.11032538    -0.04494836    -1.63682395
  16H1s       0.00067577    -0.15733241     0.19985628     0.14735376     0.00001164    -0.78056151    -1.43844159     1.46321647
  17H1px     -0.00058847     0.03831751    -0.02420735    -0.03061866     0.00000138    -0.02270191    -0.02171061     0.07123999
  18H1py      0.00000000     0.00000149    -0.00000250    -0.00000217     0.02812237    -0.00000168    -0.00000086    -0.00000352
  19H1pz     -0.00048648     0.02097186    -0.03383307     0.00447770    -0.00000097    -0.01779038    -0.01893273     0.14698988
  20H2s      -0.00049921     0.33140132     0.54768428    -0.34246055     0.00001110    -0.11041451     0.04489147     1.63723745
  21H2s       0.00067577    -0.15734459    -0.19987479     0.14731825    -0.00000457    -0.78095556     1.43818687    -1.46366628
  22H2px      0.00058848    -0.03832351    -0.02421368     0.03062469    -0.00000130     0.02271602    -0.02169828     0.07131509
  23H2py      0.00000000    -0.00000015    -0.00000001    -0.00000093     0.02812411     0.00000186     0.00000070     0.00000102
  24H2pz     -0.00048651     0.02096388     0.03381832     0.00450355    -0.00000056    -0.01776821     0.01890540    -0.14707181

               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16

  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

   1O1s       0.05837260    -0.01463779     0.00000503     0.00000870     0.21325122    -0.00002019    -0.00000883    -0.71306013
   2O1s       0.47977045    -0.28384822     0.00001375     0.00003068     1.08093077    -0.00009754    -0.00004048    -3.08024171
   3O1s      -0.57438913    -0.57659513    -0.00006745    -0.00009282    -1.24608412     0.00012168     0.00005000     4.82400221
   4O1px     -0.00004719     0.00005730    -0.00004769    -1.06890298     0.00000411    -0.00000037     0.00000034    -0.00000738
   5O1py     -0.00000840    -0.00007230     1.41271756    -0.00005634    -0.00001982     0.00000187    -0.08871650     0.00000314
   6O1pz     -0.48947619    -1.10585123    -0.00005761    -0.00007203     0.12641916    -0.00000882    -0.00001141    -0.19100305
   7O1px     -0.00009022    -0.00019771     0.00009488     2.27524433    -0.00012402    -0.00000367     0.00001031    -0.00006649
   8O1py      0.00000952     0.00009466    -1.69638790     0.00007156     0.00002590     0.00004182    -0.56197790     0.00000208
   9O1pz      0.25590052     1.83332904     0.00008910     0.00011223    -0.69801893     0.00003699     0.00001054    -0.88829005
  10O1d2-     0.00000008    -0.00000019     0.00000020     0.00000057    -0.00000810    -0.13250392    -0.00001137     0.00000130
  11O1d1-    -0.00000044     0.00000123    -0.00544083     0.00000001    -0.00000046     0.00000822    -0.16332324     0.00000237
  12O1d0     -0.00006927    -0.00230672    -0.00000041    -0.00000065    -0.03346225     0.00000211    -0.00000029    -0.03395276
  13O1d1+    -0.00001734    -0.00000114    -0.00000316    -0.05157092    -0.00000983    -0.00000102    -0.00000011    -0.00000361
  14O1d2+    -0.05503449     0.00348412    -0.00000025    -0.00000061     0.00059953    -0.00000058     0.00000004    -0.05356872
  15H1s      -1.32122270     0.99413525     0.00005701     0.63508380    -0.49221389     0.00000956    -0.00000219    -1.44397053
  16H1s       1.19628082    -0.40787968     0.00003220     0.54786296     0.48268560    -0.00002106     0.00000197     0.32860613
  17H1px      0.30439962     0.10428505     0.00001374     0.29879219    -0.29900184     0.00002488    -0.00000261    -0.38486251
  18H1py     -0.00000246    -0.00001035     0.02079206    -0.00000495     0.00004430     0.68488815     0.76959248    -0.00001389
  19H1pz      0.05238695     0.20940378     0.00002872     0.19637801     0.57166149    -0.00003959     0.00000990     0.03757256
  20H2s      -1.32066303     0.99436003     0.00002260    -0.63500691    -0.49194978     0.00002583    -0.00001147    -1.44384520
  21H2s       1.19583585    -0.40786413    -0.00004046    -0.54785634     0.48257020    -0.00003481    -0.00000128     0.32855106
  22H2px     -0.30439001    -0.10428002     0.00000920     0.29878664     0.29906466    -0.00001085     0.00000136     0.38494298
  23H2py      0.00000059    -0.00001024     0.02078723    -0.00000107    -0.00004454    -0.68498760     0.76950766    -0.00000275
  24H2pz      0.05235473     0.20949144     0.00000994    -0.19633523     0.57175884    -0.00003111    -0.00000331     0.03768680

               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24

  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

   1O1s       0.00004493     0.00012023    -0.31014125     0.00000020    -0.00000058     0.08687533    -0.11493847     0.00000138
   2O1s       0.00018868     0.00060493    -1.54958265     0.00000087    -0.00000171     0.30568003    -0.23407449    -0.00000089
   3O1s      -0.00029764    -0.00004320     0.14186263    -0.00000047     0.00000513    -0.82310452     2.43176448    -0.00005018
   4O1px      0.00727245     1.24211605     0.00047555     0.00000038    -0.00000169    -0.00001089     0.00000949     0.73382789
   5O1py      0.00000091     0.00000139     0.00000194     0.00323785    -0.00000014     0.00000060     0.00000046    -0.00000020
   6O1pz      0.00005228    -0.00038995     0.97419238    -0.00000233    -0.00000044     0.06679016    -0.62510136     0.00001439
   7O1px     -0.90398848    -0.41151301    -0.00015604     0.00000180     0.00000005    -0.00001356     0.00001321     0.83052799
   8O1py     -0.00000568    -0.00000088    -0.00000124    -0.32163538    -0.00002183    -0.00000098     0.00000077     0.00000069
   9O1pz      0.00003770     0.00011341    -0.28542642     0.00000100    -0.00000372     0.54759453    -0.61833005     0.00001697
  10O1d2-     0.00000195     0.00000114     0.00000056    -0.00007135     1.07184213     0.00001006     0.00000106    -0.00000001
  11O1d1-    -0.00000165     0.00000044    -0.00000015     1.04520184     0.00006885    -0.00000030    -0.00000138    -0.00000117
  12O1d0      0.00000945    -0.00000821     0.01673761    -0.00000003     0.00000298    -0.31587365     0.02337949    -0.00000288
  13O1d1+    -0.02874783     0.15206698     0.00006365    -0.00000160    -0.00000027     0.00001030    -0.00002516    -1.32881596
  14O1d2+     0.00000045    -0.00004234     0.11538377     0.00000091    -0.00000125     0.06552468     0.58592043    -0.00000986
  15H1s      -0.66363517     0.53318949     0.81453293     0.00000042    -0.00000497     0.60541382    -2.17044979     1.92682678
  16H1s       0.24145107     0.06120682    -0.25702875     0.00000078     0.00000147    -0.25727979     0.89563066    -0.67132350
  17H1px      0.48269840     0.72710151     0.74889217     0.00000098    -0.00000380     0.37843773    -0.62542891     0.62350274
  18H1py     -0.00000114    -0.00000258    -0.00000266     0.40917090     0.38344139     0.00000413    -0.00000059    -0.00000096
  19H1pz     -0.69131314     0.56327390     0.52892366     0.00000010     0.00000325    -0.31152557    -0.51320807     0.49237575
  20H2s       0.66387637    -0.53385521     0.81413075    -0.00000383    -0.00000201     0.60545109    -2.17051206    -1.92672935
  21H2s      -0.24152945    -0.06099384    -0.25708385     0.00000149     0.00000085    -0.25728827     0.89565192     0.67128581
  22H2px      0.48260754     0.72768844    -0.74832354     0.00000121     0.00000202    -0.37844295     0.62544937     0.62346617
  23H2py      0.00001094    -0.00000073    -0.00000054     0.40921893    -0.38338285    -0.00000402    -0.00000171    -0.00000037
  24H2pz      0.69121549    -0.56368887     0.52848765    -0.00000040     0.00000295    -0.31149223    -0.51321394    -0.49236104
 d1(*), fmc(*), and qmc(*) written to the 1-particle density matrix file.
         25 d2(*) elements written to the 2-particle density matrix file.
 !timer: writing the mc density files requser+sys=     0.000 walltime=     0.000


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!


                        A   partial gross atomic populations
   ao class       1A         2A         3A         4A         5A         6A  
    O1_ s       2.000294   1.455290   0.000000   0.181270   0.000000   0.000000
    O1_ p       0.000003   0.017030   1.188276   1.549447   1.958487   0.000000
    O1_ d       0.000000   0.000370   0.008490   0.001591   0.000899   0.000000
    H1_ s       0.000027   0.226572   0.385162   0.121265   0.000000   0.000000
    H1_ p      -0.000176   0.037064   0.016417   0.012594   0.020306   0.000000
    H2_ s       0.000027   0.226609   0.385246   0.121227   0.000000   0.000000
    H2_ p      -0.000176   0.037064   0.016408   0.012606   0.020308   0.000000

   ao class       7A         8A         9A        10A        11A        12A  

   ao class      13A        14A        15A        16A        17A        18A  

   ao class      19A        20A        21A        22A        23A        24A  


                        gross atomic populations
     ao           O1_        H1_        H2_
      s         3.636855   0.733027   0.733109
      p         4.713244   0.086205   0.086210
      d         0.011350   0.000000   0.000000
    total       8.361449   0.819232   0.819319


 Total number of electrons:   10.00000000

 !timer: mcscf                           user+sys=    11.840 walltime=    24.000
