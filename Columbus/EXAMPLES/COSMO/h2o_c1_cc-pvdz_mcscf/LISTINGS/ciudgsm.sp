1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 integral file titles:
 Hermit Integral Program : SIFS version  hochtor2        Thu Oct  7 15:38:30 2004
  cidrt_title                                                                    
  title                                                                          
 mofmt: formatted orbitals label=morbl   hochtor2        Thu Oct  7 15:39:14 2004
 SIFS file created by program tran.      hochtor2        Thu Oct  7 15:39:14 2004

 formula file title:
 Hermit Integral Program : SIFS version  hochtor2        Thu Oct  7 15:38:30 2004
  cidrt_title                                                                    
  title                                                                          
 mofmt: formatted orbitals label=morbl   hochtor2        Thu Oct  7 15:39:14 2004
 SIFS file created by program tran.      hochtor2        Thu Oct  7 15:39:14 2004
  cidrt_title                                                                    

 297 dimension of the ci-matrix ->>>      4656


 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -76.0516821872 -9.2919E+00  2.5050E-01  1.0017E+00  1.0000E-03
 mr-sdci #  2  1    -76.2492661514 -9.1605E+00  6.0336E-03  1.5367E-01  1.0000E-03
 mr-sdci #  3  1    -76.2548937635 -9.3525E+00  2.1032E-04  2.7819E-02  1.0000E-03
 mr-sdci #  4  1    -76.2551145538 -9.3579E+00  1.1788E-05  5.9745E-03  1.0000E-03
 mr-sdci #  5  1    -76.2551250678 -9.3581E+00  6.2930E-07  1.4430E-03  1.0000E-03
 mr-sdci #  6  1    -76.2551256780 -9.3581E+00  2.1689E-08  2.6674E-04  1.0000E-03

 mr-sdci  convergence criteria satisfied after  6 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  6  1    -76.2551256780 -9.3581E+00  2.1689E-08  2.6674E-04  1.0000E-03

 number of reference csfs (nref) is     1.  root number (iroot) is  1.

 eref      =    -76.051682187224   "relaxed" cnot**2         =   0.952152081168
 eci       =    -76.255125678023   deltae = eci - eref       =  -0.203443490800
 eci+dv1   =    -76.264860025658   dv1 = (1-cnot**2)*deltae  =  -0.009734347635
 eci+dv2   =    -76.265349199903   dv2 = dv1 / cnot**2       =  -0.010223521880
 eci+dv3   =    -76.265890139849   dv3 = dv1 / (2*cnot**2-1) =  -0.010764461826
 eci+pople =    -76.263555145782   ( 10e- scaled deltae )    =  -0.211872958558
