echo of the argos input file:
 ------------------------------------------------------------------------
  Xe (6s6p3d1f)->[4s4p3d1f],C2v
   0  1  4  4  4 20  0  0  0  1  1  0  0  0  0
   8  1  S  1Dxy  1Dxz  1Dyz  1xyz  1 Pz  1 Py  1 Px
   7
   2  3  4
   2  5  6
   3  5  7
   4  5  8
   3  6  8
   4  6  7
   2  7  8
     1    1
     3    8    7    6
     5    1    1    2    3    4
     7    6    6    5    8    8    7    7
     1    1    1
     1
     3    3    2
     1    0    0
     0    1    0
     0    0    1
     5    6    3
     1    1   -4    0    0    0
     1   -1    0    0    0    0
     0    0    0    1    0    0
     0    0    0    0    1    0
     0    0    0    0    0    1
     7   10    4
     0    0    4    0   -9    0   -9    0    0    0
     0    0    0    0    1    0   -1    0    0    0
     0    0    0    0    0    0    0    0    0    1
     4    0    0    0    0   -9    0   -9    0    0
     0    0    0    0    0    1    0   -1    0    0
     0    4    0   -9    0    0    0    0   -9    0
     0    0    0    1    0    0    0    0   -1    0
     6    1    6
       7.8580150        1.0000000    .0000000    .0000000    .0000000
                         .0000000    .0000000
       3.4957720         .0000000   1.0000000    .0000000    .0000000
                         .0000000    .0000000
       1.7588690         .0000000    .0000000   1.0000000    .0000000
                         .0000000    .0000000
        .3147450         .0000000    .0000000    .0000000   1.0000000
                         .0000000    .0000000
        .1511600         .0000000    .0000000    .0000000    .0000000
                        1.0000000    .0000000
        .0712260         .0000000    .0000000    .0000000    .0000000
                         .0000000   1.0000000
     6    2    6
       3.2145230        1.0000000    .0000000    .0000000    .0000000
                         .0000000    .0000000
       1.8849440         .0000000   1.0000000    .0000000    .0000000
                         .0000000    .0000000
        .4488760         .0000000    .0000000   1.0000000    .0000000
                         .0000000    .0000000
        .2122320         .0000000    .0000000    .0000000   1.0000000
                         .0000000    .0000000
        .1001150         .0000000    .0000000    .0000000    .0000000
                        1.0000000    .0000000
        .0469790         .0000000    .0000000    .0000000    .0000000
                         .0000000   1.0000000
     3    3    3
        .4460000        1.0000000    .0000000    .0000000
        .2322000         .0000000   1.0000000    .0000000
        .1208000         .0000000    .0000000   1.0000000
     1    4    1
        .5157000        1.0000000
     4    3
     1
    2      1.8478920     -7.1058506
     3
    2      1.8478920      7.1058506
    2      3.9402630    122.7638293
    2      2.2772640      8.3088512
     3
    2      1.8478920      7.1058506
    2      3.0283730     68.8230044
    2      1.3943190      3.6467422
     3
    2      1.8478920      7.1058506
    2      2.1226050     23.6520785
    2       .7986690      3.2584411
     3
    2      1.8478920      7.1058506
    2      6.1643600    -47.7031988
    2      1.5423740     -6.5411399
     2
    2      3.02837300     -1.74609113
    2      1.39431900      2.19487257
     2
    2      2.12260500     -1.04567591
    2       .79866900       .27199291
     1
    2      6.16436000       .20772207
  Xe  4  1 8.
      .00000000     .00000000     .00000000
   1  1
   2  2
   3  3
   4  4
   1
 ------------------------------------------------------------------------
                              program "argos" 5.4
                            columbus program system

             this program computes integrals over symmetry orbitals
               of generally contracted gaussian atomic orbitals.
                        programmed by russell m. pitzer

                           version date: 03-feb-1999

references:

    symmetry analysis (equal contributions):
    r. m. pitzer, j. chem. phys. 58, 3111 (1973).

    ao integral evaluation (hondo):
    m. dupuis, j. rys, and h. f. king, j. chem. phys. 65, 111 (1976).

    general contraction of gaussian orbitals:
    r. c. raffenetti, j. chem. phys. 58, 4452 (1973).

    core potential ao integrals (meldps):
    l. e. mcmurchie and e. r. davidson, j. comput. phys. 44, 289 (1981)

    spin-orbit and core potential integrals:
    r. m. pitzer and n. w. winter, int. j. quantum chem. 40, 773 (1991)

 This Version of Program ARGOS is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at


 workspace allocation parameters: lcore=  10000000 mem1= 806696856 ifirst=    170800

 filenames and unit numbers:
 unit description                   filename
 ---- -----------                   ----------
   6  listing file:                 argosls                                                     
   4  1-e integral file:            aoints                                                      
   8  2-e integral file [fsplit=2]: aoints2                                                     
   5  input file:                   argosin                                                     

 argos input parameters and titles:
 ngen   =  0 ns     =  1 naords =  4 ncons  =  4 ngcs   =  4 itol   = 20
 icut   =  9 aoints =  4 only1e =  0 inrm   =  1 ncrs   =  1
 l1rec  =         0      l2rec  =         0      aoint2 =  8 fsplit =  2

 Xe (6s6p3d1f)->[4s4p3d1f],C2v                                                  
aoints SIFS file created by argos.      zam403          Wed Aug 20 14:51:12 2003


irrep            1       2       3       4       5       6       7       8
degeneracy       1       1       1       1       1       1       1       1
label             S     Dxy     Dxz     Dyz     xyz      Pz      Py      Px


direct product table
   (  S) (  S) =   S
   (  S) (Dxy) = Dxy
   (  S) (Dxz) = Dxz
   (  S) (Dyz) = Dyz
   (  S) (xyz) = xyz
   (  S) ( Pz) =  Pz
   (  S) ( Py) =  Py
   (  S) ( Px) =  Px
   (Dxy) (  S) = Dxy
   (Dxy) (Dxy) =   S
   (Dxy) (Dxz) = Dyz
   (Dxy) (Dyz) = Dxz
   (Dxy) (xyz) =  Pz
   (Dxy) ( Pz) = xyz
   (Dxy) ( Py) =  Px
   (Dxy) ( Px) =  Py
   (Dxz) (  S) = Dxz
   (Dxz) (Dxy) = Dyz
   (Dxz) (Dxz) =   S
   (Dxz) (Dyz) = Dxy
   (Dxz) (xyz) =  Py
   (Dxz) ( Pz) =  Px
   (Dxz) ( Py) = xyz
   (Dxz) ( Px) =  Pz
   (Dyz) (  S) = Dyz
   (Dyz) (Dxy) = Dxz
   (Dyz) (Dxz) = Dxy
   (Dyz) (Dyz) =   S
   (Dyz) (xyz) =  Px
   (Dyz) ( Pz) =  Py
   (Dyz) ( Py) =  Pz
   (Dyz) ( Px) = xyz
   (xyz) (  S) = xyz
   (xyz) (Dxy) =  Pz
   (xyz) (Dxz) =  Py
   (xyz) (Dyz) =  Px
   (xyz) (xyz) =   S
   (xyz) ( Pz) = Dxy
   (xyz) ( Py) = Dxz
   (xyz) ( Px) = Dyz
   ( Pz) (  S) =  Pz
   ( Pz) (Dxy) = xyz
   ( Pz) (Dxz) =  Px
   ( Pz) (Dyz) =  Py
   ( Pz) (xyz) = Dxy
   ( Pz) ( Pz) =   S
   ( Pz) ( Py) = Dyz
   ( Pz) ( Px) = Dxz
   ( Py) (  S) =  Py
   ( Py) (Dxy) =  Px
   ( Py) (Dxz) = xyz
   ( Py) (Dyz) =  Pz
   ( Py) (xyz) = Dxz
   ( Py) ( Pz) = Dyz
   ( Py) ( Py) =   S
   ( Py) ( Px) = Dxy
   ( Px) (  S) =  Px
   ( Px) (Dxy) =  Py
   ( Px) (Dxz) =  Pz
   ( Px) (Dyz) = xyz
   ( Px) (xyz) = Dyz
   ( Px) ( Pz) = Dxz
   ( Px) ( Py) = Dxy
   ( Px) ( Px) =   S


                     nuclear repulsion energy     .00000000


primitive ao integrals neglected if exponential factor below 10**(-20)
contracted ao and so integrals neglected if value below 10**(- 9)
symmetry orbital integrals written on units  4  8


                                     Xe atoms

                              nuclear charge   8.00

           center            x               y               z
             1              .00000000       .00000000       .00000000

                    1s orbitals

 orbital exponents  contraction coefficients
    7.858015        1.000000       0.0000000E+00   0.0000000E+00   0.0000000E+00
    3.495772       0.0000000E+00    1.000000       0.0000000E+00   0.0000000E+00
    1.758869       0.0000000E+00   0.0000000E+00    1.000000       0.0000000E+00
    .3147450       0.0000000E+00   0.0000000E+00   0.0000000E+00    1.000000    
    .1511600       0.0000000E+00   0.0000000E+00   0.0000000E+00   0.0000000E+00
   7.1226000E-02   0.0000000E+00   0.0000000E+00   0.0000000E+00   0.0000000E+00

                     symmetry orbital labels
                     1  S1           2  S1           3  S1           4  S1

                    contraction coefficients
                   0.0000000E+00   0.0000000E+00
                   0.0000000E+00   0.0000000E+00
                   0.0000000E+00   0.0000000E+00
                   0.0000000E+00   0.0000000E+00
                    1.000000       0.0000000E+00
                   0.0000000E+00    1.000000    

                     symmetry orbital labels
                     5  S1           6  S1

           symmetry orbitals
 ctr, ao     S1
  1, 000  1.000

                    2p orbitals

 orbital exponents  contraction coefficients
    3.214523        1.000000       0.0000000E+00   0.0000000E+00   0.0000000E+00
    1.884944       0.0000000E+00    1.000000       0.0000000E+00   0.0000000E+00
    .4488760       0.0000000E+00   0.0000000E+00    1.000000       0.0000000E+00
    .2122320       0.0000000E+00   0.0000000E+00   0.0000000E+00    1.000000    
    .1001150       0.0000000E+00   0.0000000E+00   0.0000000E+00   0.0000000E+00
   4.6979000E-02   0.0000000E+00   0.0000000E+00   0.0000000E+00   0.0000000E+00

                     symmetry orbital labels
                     1 Px1           2 Px1           3 Px1           4 Px1
                     1 Py1           2 Py1           3 Py1           4 Py1
                     1 Pz1           2 Pz1           3 Pz1           4 Pz1

                    contraction coefficients
                   0.0000000E+00   0.0000000E+00
                   0.0000000E+00   0.0000000E+00
                   0.0000000E+00   0.0000000E+00
                   0.0000000E+00   0.0000000E+00
                    1.000000       0.0000000E+00
                   0.0000000E+00    1.000000    

                     symmetry orbital labels
                     5 Px1           6 Px1
                     5 Py1           6 Py1
                     5 Pz1           6 Pz1

           symmetry orbitals
 ctr, ao    Px1    Py1    Pz1
  1, 100  1.000   .000   .000
  1, 010   .000  1.000   .000
  1, 001   .000   .000  1.000

                    3d orbitals

 orbital exponents  contraction coefficients
    .4460000        1.000000       0.0000000E+00   0.0000000E+00
    .2322000       0.0000000E+00    1.000000       0.0000000E+00
    .1208000       0.0000000E+00   0.0000000E+00    1.000000    

                     symmetry orbital labels
                     7  S1           9  S1          11  S1
                     8  S1          10  S1          12  S1
                     1Dxy1           2Dxy1           3Dxy1
                     1Dxz1           2Dxz1           3Dxz1
                     1Dyz1           2Dyz1           3Dyz1

           symmetry orbitals
 ctr, ao     S1     S1   Dxy1   Dxz1   Dyz1
  1, 200   .289   .500   .000   .000   .000
  1, 020   .289  -.500   .000   .000   .000
  1, 002  -.577   .000   .000   .000   .000
  1, 110   .000   .000  1.000   .000   .000
  1, 101   .000   .000   .000  1.000   .000
  1, 011   .000   .000   .000   .000  1.000

                    4f orbitals

 orbital exponents  contraction coefficients
    .5157000        1.000000    

                     symmetry orbital labels
                     7 Pz1
                     8 Pz1
                     1xyz1
                     7 Px1
                     8 Px1
                     7 Py1
                     8 Py1

           symmetry orbitals
 ctr, ao    Pz1    Pz1   xyz1    Px1    Px1    Py1    Py1
  1, 300   .000   .000   .000   .258   .000   .000   .000
  1, 030   .000   .000   .000   .000   .000   .258   .000
  1, 003   .258   .000   .000   .000   .000   .000   .000
  1, 210   .000   .000   .000   .000   .000  -.387   .500
  1, 201  -.387   .500   .000   .000   .000   .000   .000
  1, 120   .000   .000   .000  -.387   .500   .000   .000
  1, 021  -.387  -.500   .000   .000   .000   .000   .000
  1, 102   .000   .000   .000  -.387  -.500   .000   .000
  1, 012   .000   .000   .000   .000   .000  -.387  -.500
  1, 111   .000   .000  1.000   .000   .000   .000   .000


                                Xe core potential

                                   g potential
                   powers      exponentials    coefficients
                     2           1.847892       -7.105851    

                                 s - g potential
                   powers      exponentials    coefficients
                     2           1.847892        7.105851    
                     2           3.940263        122.7638    
                     2           2.277264        8.308851    

                                 p - g potential
                   powers      exponentials    coefficients
                     2           1.847892        7.105851    
                     2           3.028373        68.82300    
                     2           1.394319        3.646742    

                                 d - g potential
                   powers      exponentials    coefficients
                     2           1.847892        7.105851    
                     2           2.122605        23.65208    
                     2           .7986690        3.258441    

                                 f - g potential
                   powers      exponentials    coefficients
                     2           1.847892        7.105851    
                     2           6.164360       -47.70320    
                     2           1.542374       -6.541140    


                             Xe spin-orbit potential

                                   p potential
                   powers      exponentials    coefficients
                     2           3.028373       -1.746091    
                     2           1.394319        2.194873    

                                   d potential
                   powers      exponentials    coefficients
                     2           2.122605       -1.045676    
                     2           .7986690        .2719929    

                                   f potential
                   powers      exponentials    coefficients
                     2           6.164360        .2077221    

lx: Dyz          ly: Dxz          lz: Dxy

output SIFS file header information:
 Xe (6s6p3d1f)->[4s4p3d1f],C2v                                                  
aoints SIFS file created by argos.      zam403          Wed Aug 20 14:51:12 2003

output energy(*) values:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.

total core energy =  0.000000000000E+00

nsym = 8 nbft=  46

symmetry  =    1    2    3    4    5    6    7    8
slabel(*) =    S  Dxy  Dxz  Dyz  xyz   Pz   Py   Px
nbpsy(*)  =   12    3    3    3    1    8    8    8

info(*) =         2      4096      3272      4096      2700

output orbital labels, i:bfnlab(i)=
   1:  1_Xe1s   2:  2_Xe1s   3:  3_Xe1s   4:  4_Xe1s   5:  5_Xe1s   6:  6_Xe1s
   7:  7_Xe3d   8:  8_Xe3d   9:  9_Xe3d  10: 10_Xe3d  11: 11_Xe3d  12: 12_Xe3d
  13: 13_Xe3d  14: 14_Xe3d  15: 15_Xe3d  16: 16_Xe3d  17: 17_Xe3d  18: 18_Xe3d
  19: 19_Xe3d  20: 20_Xe3d  21: 21_Xe3d  22: 22_Xe4f  23: 23_Xe2p  24: 24_Xe2p
  25: 25_Xe2p  26: 26_Xe2p  27: 27_Xe2p  28: 28_Xe2p  29: 29_Xe4f  30: 30_Xe4f
  31: 31_Xe2p  32: 32_Xe2p  33: 33_Xe2p  34: 34_Xe2p  35: 35_Xe2p  36: 36_Xe2p
  37: 37_Xe4f  38: 38_Xe4f  39: 39_Xe2p  40: 40_Xe2p  41: 41_Xe2p  42: 42_Xe2p
  43: 43_Xe2p  44: 44_Xe2p  45: 45_Xe4f  46: 46_Xe4f

bfn_to_center map(*), i:map(i)
   1:  1   2:  1   3:  1   4:  1   5:  1   6:  1   7:  1   8:  1   9:  1  10:  1
  11:  1  12:  1  13:  1  14:  1  15:  1  16:  1  17:  1  18:  1  19:  1  20:  1
  21:  1  22:  1  23:  1  24:  1  25:  1  26:  1  27:  1  28:  1  29:  1  30:  1
  31:  1  32:  1  33:  1  34:  1  35:  1  36:  1  37:  1  38:  1  39:  1  40:  1
  41:  1  42:  1  43:  1  44:  1  45:  1  46:  1

bfn_to_orbital_type map(*), i:map(i)
   1:  1   2:  1   3:  1   4:  1   5:  1   6:  1   7:  4   8:  4   9:  4  10:  4
  11:  4  12:  4  13:  4  14:  4  15:  4  16:  4  17:  4  18:  4  19:  4  20:  4
  21:  4  22:  6  23:  2  24:  2  25:  2  26:  2  27:  2  28:  2  29:  6  30:  6
  31:  2  32:  2  33:  2  34:  2  35:  2  36:  2  37:  6  38:  6  39:  2  40:  2
  41:  2  42:  2  43:  2  44:  2  45:  6  46:  6


       46 symmetry orbitals,        S:  12   Dxy:   3   Dxz:   3   Dyz:   3
                                  xyz:   1    Pz:   8    Py:   8    Px:   8
 !timer: syminp required                     user=      .000 walltime=      .000

 socfpd: mcxu=     7875 mcxu2=     5983 left=  9992125
 !timer: socfpd required                     user=      .010 walltime=      .000
 
oneint:   121 S1(*)    integrals were written in  1 records.
oneint:   121 T1(*)    integrals were written in  1 records.
oneint:   121 V1(*)    integrals were written in  1 records.
oneint:   135 X(*)     integrals were written in  1 records.
oneint:   135 Y(*)     integrals were written in  1 records.
oneint:   111 Z(*)     integrals were written in  1 records.
oneint:   135 Im(px)   integrals were written in  1 records.
oneint:   135 Im(py)   integrals were written in  1 records.
oneint:   111 Im(pz)   integrals were written in  1 records.
oneint:    68 Im(lx)   integrals were written in  1 records.
oneint:    68 Im(ly)   integrals were written in  1 records.
oneint:    59 Im(lz)   integrals were written in  1 records.
oneint:   198 XX(*)    integrals were written in  1 records.
oneint:   107 XY(*)    integrals were written in  1 records.
oneint:   116 XZ(*)    integrals were written in  1 records.
oneint:   198 YY(*)    integrals were written in  1 records.
oneint:   116 YZ(*)    integrals were written in  1 records.
oneint:   171 ZZ(*)    integrals were written in  1 records.
oneint:   121 Veff(*)  integrals were written in  1 records.
oneint:    68 Im(SO:x) integrals were written in  1 records.
oneint:    68 Im(SO:y) integrals were written in  1 records.
oneint:    59 Im(SO:z) integrals were written in  1 records.
 
 !timer: oneint required                     user=      .010 walltime=     1.000
 !timer: seg1mn required                     user=      .020 walltime=     1.000

twoint:       67990 1/r12    integrals and       17 pk flags
                                 were written in    26 records.

 twoint: maximum mblu needed =    108383
 !timer: twoint required                     user=      .360 walltime=      .000
 
 driver: 1-e integral  workspace high-water mark =     29246
 driver: 2-e integral  workspace high-water mark =    130401
 driver: overall argos workspace high-water mark =    130401
 !timer: argos required                      user=      .380 walltime=     1.000
