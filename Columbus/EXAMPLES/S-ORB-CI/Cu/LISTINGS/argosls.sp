echo of the argos input file:
 ------------------------------------------------------------------------
 Cu atom
    0   1   4   4   4  20   9   0   0   0   1   0   0   0   0
   8  1 ag  1b1g  1b2g  1b3g  1 au  1b1u  1b2u  1b3u
   7
    2   3   4
    2   5   6
    2   7   8
    3   5   7
    3   6   8
    4   5   8
    4   6   7
     1    1
     3    6    8    7
     5    1    3    4    1    2
     7    6    8    7    6    5    8    7
     1    1    1
     1
     3    3    2
     0    0    1
     1    0    0
     0    1    0
     5    6    3
    -1   -1    4    0    0    0
     0    0    0    0    1    0
     0    0    0    0    0    1
     1   -1    0    0    0    0
     0    0    0    1    0    0
     7   10    4
     0    0    4    0   -9    0   -9    0    0    0
    -1    0    0    0    0   -1    0   16    0    0
     0   -1    0   -1    0    0    0    0   16    0
     0    0    0    0    1    0   -1    0    0    0
     0    0    0    0    0    0    0    0    0    1
    -1    0    0    0    0    9    0    0    0    0
     0   -1    0    9    0    0    0    0    0    0
     8    1    4
     560.0880000        0.0006370  -0.0001360  -0.0003330   0.0000000
      56.6486000       -0.0097350   0.0014010   0.0059300   0.0000000
      35.4258000        0.0657930  -0.0131740  -0.0325490   0.0000000
      11.0546000       -0.4150350   0.0956950   0.2110710   0.0000000
       2.3068200        0.7466110  -0.2118740  -0.7305560   0.0000000
       0.9514290        0.4621730  -0.2359440   0.1772420   0.0000000
       0.1451840        0.0159830   0.5081150   1.7148730   0.0000000
       0.0503250       -0.0027670   0.6215190  -1.6045730   1.0000000
     7    2    4
      70.9739000        0.0036820  -0.0006280  -0.0009100   0.0000000
      17.8510000       -0.0821280   0.0165630   0.0299140   0.0000000
       4.2467900        0.3753790  -0.0845720  -0.1660940   0.0000000
       1.8776000        0.5084090  -0.1412830  -0.2780000   0.0000000
       0.7933350        0.2390950  -0.0035710   0.1575840   0.0000000
       0.1934760        0.0158500   0.5190050   0.8304880   0.0000000
       0.0573930       -0.0025270   0.5924750   0.1658690   1.0000000
     6    3    3
      60.3804000        0.0175640  -0.0222860   0.0000000
      19.1121000        0.0991340  -0.1282740   0.0000000
       6.9528800        0.2711710  -0.3627970   0.0000000
       2.6099400        0.4061800  -0.3257220   0.0000000
       0.9225670        0.3814270   0.3270870   0.0000000
       0.2836420        0.2006260   0.6568090   1.0000000
     1    4    1
       2.7482000        1.0000000
     4    3
     1
    2      1.0000000      0.0000000
     2
    2     30.1105430    355.7505100
    2     13.0763100     70.9309060
     4
    2     32.6926140     77.9699310
    2     32.7703390    155.9274500
    2     13.7510670     18.0211320
    2     13.3221660     36.0943720
     4
    2     38.9965110    -12.3434100
    2     39.5397880    -18.2733620
    2     12.2875110     -0.9847050
    2     11.4593000     -1.3187470
     2
    2      6.1901020     -0.2272640
    2      8.1187800     -0.4687730
     4
    2     32.69261400   -155.93986000
    2     32.77033900    155.92745000
    2     13.75106700    -36.04226500
    2     13.32216600     36.09437200
     4
    2     38.99651100     12.34341000
    2     39.53978800    -12.18224100
    2     12.28751100      0.98470500
    2     11.45930000     -0.87916500
     2
    2      6.19010200      0.15150900
    2      8.11878000     -0.23438700
 Cu   4  119.
     0.00000000    0.00000000    0.00000000
    1   1
    2   2
    3   3
    4   4
    1
 ------------------------------------------------------------------------
                              program "argos" 5.9
                            columbus program system

             this program computes integrals over symmetry orbitals
               of generally contracted gaussian atomic orbitals.
                        programmed by russell m. pitzer

                           version date: 20-aug-2001

references:

    symmetry analysis (equal contributions):
    r. m. pitzer, j. chem. phys. 58, 3111 (1973).

    ao integral evaluation (hondo):
    m. dupuis, j. rys, and h. f. king, j. chem. phys. 65, 111 (1976).

    general contraction of gaussian orbitals:
    r. c. raffenetti, j. chem. phys. 58, 4452 (1973).

    core potential ao integrals (meldps):
    l. e. mcmurchie and e. r. davidson, j. comput. phys. 44, 289 (1981)

    spin-orbit and core potential integrals:
    r. m. pitzer and n. w. winter, int. j. quantum chem. 40, 773 (1991)

 This Version of Program ARGOS is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de

*********************** File revision status: ***********************
* argos1.f  Revision: 2.15.12.1         Date: 2013/04/11 14:37:29   * 
* argos2.f  Revision: 2.4.12.1          Date: 2013/04/11 14:37:29   * 
* argos3.f  Revision: 2.3.12.1           Date: 2013/04/11 14:37:29  * 
* argos4.f  Revision: 2.4.6.1           Date: 2013/04/11 14:37:29   * 
* argos5.f  Revision: 2.2.20.1          Date: 2013/04/11 14:37:29   * 
* argos6.f  Revision: 2.22.6.1          Date: 2013/04/11 14:37:29   * 
********************************************************************

 workspace allocation parameters: lcore= 131072000 mem1=         0 ifirst=         1

 filenames and unit numbers:
 unit description                   filename
 ---- -----------                   ----------
   6  listing file:                 argosls                                                     
   4  1-e integral file:            aoints                                                      
   8  2-e integral file [fsplit=2]: aoints2                                                     
   5  input file:                   argosin                                                     

 argos input parameters and titles:
 ngen   =  0 ns     =  1 naords =  4 ncons  =  4 ngcs   =  4 itol   = 20
 icut   =  9 aoints =  4 only1e =  0 inrm   =  0 ncrs   =  1
 l1rec  =         0      l2rec  =         0      aoint2 =  8 fsplit =  2

Cu atom                                                                         
aoints SIFS file created by argos.      zam792            14:38:58.196 03-Nov-14


irrep            1       2       3       4       5       6       7       8
degeneracy       1       1       1       1       1       1       1       1
label            ag     b1g     b2g     b3g      au     b1u     b2u     b3u


direct product table
   ( ag) ( ag) =  ag
   ( ag) (b1g) = b1g
   ( ag) (b2g) = b2g
   ( ag) (b3g) = b3g
   ( ag) ( au) =  au
   ( ag) (b1u) = b1u
   ( ag) (b2u) = b2u
   ( ag) (b3u) = b3u
   (b1g) ( ag) = b1g
   (b1g) (b1g) =  ag
   (b1g) (b2g) = b3g
   (b1g) (b3g) = b2g
   (b1g) ( au) = b1u
   (b1g) (b1u) =  au
   (b1g) (b2u) = b3u
   (b1g) (b3u) = b2u
   (b2g) ( ag) = b2g
   (b2g) (b1g) = b3g
   (b2g) (b2g) =  ag
   (b2g) (b3g) = b1g
   (b2g) ( au) = b2u
   (b2g) (b1u) = b3u
   (b2g) (b2u) =  au
   (b2g) (b3u) = b1u
   (b3g) ( ag) = b3g
   (b3g) (b1g) = b2g
   (b3g) (b2g) = b1g
   (b3g) (b3g) =  ag
   (b3g) ( au) = b3u
   (b3g) (b1u) = b2u
   (b3g) (b2u) = b1u
   (b3g) (b3u) =  au
   ( au) ( ag) =  au
   ( au) (b1g) = b1u
   ( au) (b2g) = b2u
   ( au) (b3g) = b3u
   ( au) ( au) =  ag
   ( au) (b1u) = b1g
   ( au) (b2u) = b2g
   ( au) (b3u) = b3g
   (b1u) ( ag) = b1u
   (b1u) (b1g) =  au
   (b1u) (b2g) = b3u
   (b1u) (b3g) = b2u
   (b1u) ( au) = b1g
   (b1u) (b1u) =  ag
   (b1u) (b2u) = b3g
   (b1u) (b3u) = b2g
   (b2u) ( ag) = b2u
   (b2u) (b1g) = b3u
   (b2u) (b2g) =  au
   (b2u) (b3g) = b1u
   (b2u) ( au) = b2g
   (b2u) (b1u) = b3g
   (b2u) (b2u) =  ag
   (b2u) (b3u) = b1g
   (b3u) ( ag) = b3u
   (b3u) (b1g) = b2u
   (b3u) (b2g) = b1u
   (b3u) (b3g) =  au
   (b3u) ( au) = b3g
   (b3u) (b1u) = b2g
   (b3u) (b2u) = b1g
   (b3u) (b3u) =  ag


                     nuclear repulsion energy    0.00000000


primitive ao integrals neglected if exponential factor below 10**(-20)
contracted ao and so integrals neglected if value below 10**(- 9)
symmetry orbital integrals written on units  4  8


                                    Cu  atoms

                              nuclear charge  19.00

           center            x               y               z
             1             0.00000000      0.00000000      0.00000000

                    1s orbitals

 orbital exponents  contraction coefficients
    560.0880       6.3699986E-04  -1.3600010E-04  -3.3300000E-04    0.000000    
    56.64860      -9.7349978E-03   1.4010010E-03   5.9300001E-03    0.000000    
    35.42580       6.5792985E-02  -1.3174010E-02  -3.2549000E-02    0.000000    
    11.05460      -0.4150349       9.5695070E-02   0.2110710        0.000000    
    2.306820       0.7466108      -0.2118742      -0.7305560        0.000000    
   0.9514290       0.4621729      -0.2359442       0.1772420        0.000000    
   0.1451840       1.5982996E-02   0.5081154        1.714873        0.000000    
   5.0325000E-02  -2.7669994E-03   0.6215195       -1.604573        1.000000    

                     symmetry orbital labels
                     1 ag1           2 ag1           3 ag1           4 ag1

           symmetry orbitals
 ctr, ao    ag1
  1, 000  1.000

                    2p orbitals

 orbital exponents  contraction coefficients
    70.97390       3.6820001E-03  -6.2800028E-04  -9.0999997E-04    0.000000    
    17.85100      -8.2128003E-02   1.6563007E-02   2.9913999E-02    0.000000    
    4.246790       0.3753790      -8.4572037E-02  -0.1660940        0.000000    
    1.877600       0.5084090      -0.1412831      -0.2780000        0.000000    
   0.7933350       0.2390950      -3.5710016E-03   0.1575840        0.000000    
   0.1934760       1.5850001E-02   0.5190052       0.8304880        0.000000    
   5.7393000E-02  -2.5270001E-03   0.5924753       0.1658690        1.000000    

                     symmetry orbital labels
                     1b1u1           2b1u1           3b1u1           4b1u1
                     1b3u1           2b3u1           3b3u1           4b3u1
                     1b2u1           2b2u1           3b2u1           4b2u1

           symmetry orbitals
 ctr, ao   b1u1   b3u1   b2u1
  1, 100  0.000  1.000  0.000
  1, 010  0.000  0.000  1.000
  1, 001  1.000  0.000  0.000

                    3d orbitals

 orbital exponents  contraction coefficients
    60.38040       1.7564002E-02  -2.2286004E-02    0.000000    
    19.11210       9.9134009E-02  -0.1282740        0.000000    
    6.952880       0.2711710      -0.3627971        0.000000    
    2.609940       0.4061800      -0.3257221        0.000000    
   0.9225670       0.3814270       0.3270871        0.000000    
   0.2836420       0.2006260       0.6568091        1.000000    

                     symmetry orbital labels
                     5 ag1           7 ag1           9 ag1
                     1b2g1           2b2g1           3b2g1
                     1b3g1           2b3g1           3b3g1
                     6 ag1           8 ag1          10 ag1
                     1b1g1           2b1g1           3b1g1

           symmetry orbitals
 ctr, ao    ag1   b2g1   b3g1    ag1   b1g1
  1, 200 -1.000  0.000  0.000  1.000  0.000
  1, 020 -1.000  0.000  0.000 -1.000  0.000
  1, 002  2.000  0.000  0.000  0.000  0.000
  1, 110  0.000  0.000  0.000  0.000  1.000
  1, 101  0.000  1.000  0.000  0.000  0.000
  1, 011  0.000  0.000  1.000  0.000  0.000

                    4f orbitals

 orbital exponents  contraction coefficients
    2.748200        1.000000    

                     symmetry orbital labels
                     5b1u1
                     5b3u1
                     5b2u1
                     6b1u1
                     1 au1
                     6b3u1
                     6b2u1

           symmetry orbitals
 ctr, ao   b1u1   b3u1   b2u1   b1u1    au1   b3u1   b2u1
  1, 300  0.000 -1.000  0.000  0.000  0.000 -1.000  0.000
  1, 030  0.000  0.000 -1.000  0.000  0.000  0.000 -1.000
  1, 003  2.000  0.000  0.000  0.000  0.000  0.000  0.000
  1, 210  0.000  0.000 -1.000  0.000  0.000  0.000  3.000
  1, 201 -3.000  0.000  0.000  1.000  0.000  0.000  0.000
  1, 120  0.000 -1.000  0.000  0.000  0.000  3.000  0.000
  1, 021 -3.000  0.000  0.000 -1.000  0.000  0.000  0.000
  1, 102  0.000  4.000  0.000  0.000  0.000  0.000  0.000
  1, 012  0.000  0.000  4.000  0.000  0.000  0.000  0.000
  1, 111  0.000  0.000  0.000  0.000  1.000  0.000  0.000


                               Cu  core potential

                                   g potential
                   powers      exponentials    coefficients
                     2           1.000000        0.000000    

                                 s - g potential
                   powers      exponentials    coefficients
                     2           30.11054        355.7505    
                     2           13.07631        70.93091    

                                 p - g potential
                   powers      exponentials    coefficients
                     2           32.69261        77.96993    
                     2           32.77034        155.9274    
                     2           13.75107        18.02113    
                     2           13.32217        36.09437    

                                 d - g potential
                   powers      exponentials    coefficients
                     2           38.99651       -12.34341    
                     2           39.53979       -18.27336    
                     2           12.28751      -0.9847050    
                     2           11.45930       -1.318747    

                                 f - g potential
                   powers      exponentials    coefficients
                     2           6.190102      -0.2272640    
                     2           8.118780      -0.4687730    


                            Cu  spin-orbit potential

                                   p potential
                   powers      exponentials    coefficients
                     2           32.69261       -155.9399    
                     2           32.77034        155.9274    
                     2           13.75107       -36.04227    
                     2           13.32217        36.09437    

                                   d potential
                   powers      exponentials    coefficients
                     2           38.99651        12.34341    
                     2           39.53979       -12.18224    
                     2           12.28751       0.9847050    
                     2           11.45930      -0.8791650    

                                   f potential
                   powers      exponentials    coefficients
                     2           6.190102       0.1515090    
                     2           8.118780      -0.2343870    

lx: b3g          ly: b2g          lz: b1g

output SIFS file header information:
Cu atom                                                                         
aoints SIFS file created by argos.      zam792            14:38:58.196 03-Nov-14

output energy(*) values:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.

total core energy =  0.000000000000E+00

nsym = 8 nbft=  38

symmetry  =    1    2    3    4    5    6    7    8
slabel(*) =   ag  b1g  b2g  b3g   au  b1u  b2u  b3u
nbpsy(*)  =   10    3    3    3    1    6    6    6

info(*) =         2      4096      3272      4096      2700         0

output orbital labels, i:bfnlab(i)=
   1:  1Cu_1s   2:  2Cu_1s   3:  3Cu_1s   4:  4Cu_1s   5:  5Cu_3d   6:  6Cu_3d
   7:  7Cu_3d   8:  8Cu_3d   9:  9Cu_3d  10: 10Cu_3d  11: 11Cu_3d  12: 12Cu_3d
  13: 13Cu_3d  14: 14Cu_3d  15: 15Cu_3d  16: 16Cu_3d  17: 17Cu_3d  18: 18Cu_3d
  19: 19Cu_3d  20: 20Cu_4f  21: 21Cu_2p  22: 22Cu_2p  23: 23Cu_2p  24: 24Cu_2p
  25: 25Cu_4f  26: 26Cu_4f  27: 27Cu_2p  28: 28Cu_2p  29: 29Cu_2p  30: 30Cu_2p
  31: 31Cu_4f  32: 32Cu_4f  33: 33Cu_2p  34: 34Cu_2p  35: 35Cu_2p  36: 36Cu_2p
  37: 37Cu_4f  38: 38Cu_4f

bfn_to_center map(*), i:map(i)
   1:  1   2:  1   3:  1   4:  1   5:  1   6:  1   7:  1   8:  1   9:  1  10:  1
  11:  1  12:  1  13:  1  14:  1  15:  1  16:  1  17:  1  18:  1  19:  1  20:  1
  21:  1  22:  1  23:  1  24:  1  25:  1  26:  1  27:  1  28:  1  29:  1  30:  1
  31:  1  32:  1  33:  1  34:  1  35:  1  36:  1  37:  1  38:  1

bfn_to_orbital_type map(*), i:map(i)
   1:  1   2:  1   3:  1   4:  1   5:  4   6:  4   7:  4   8:  4   9:  4  10:  4
  11:  4  12:  4  13:  4  14:  4  15:  4  16:  4  17:  4  18:  4  19:  4  20:  6
  21:  2  22:  2  23:  2  24:  2  25:  6  26:  6  27:  2  28:  2  29:  2  30:  2
  31:  6  32:  6  33:  2  34:  2  35:  2  36:  2  37:  6  38:  6


       38 symmetry orbitals,       ag:  10   b1g:   3   b2g:   3   b3g:   3
                                   au:   1   b1u:   6   b2u:   6   b3u:   6

 socfpd: mcxu=     7875 mcxu2=     5983 left=131064125
 
oneint:    77 S1(*)    integrals were written in  1 records.
oneint:    77 T1(*)    integrals were written in  1 records.
oneint:    77 V1(*)    integrals were written in  1 records.
oneint:    88 X(*)     integrals were written in  1 records.
oneint:    88 Y(*)     integrals were written in  1 records.
oneint:    67 Z(*)     integrals were written in  1 records.
oneint:    88 Im(px)   integrals were written in  1 records.
oneint:    88 Im(py)   integrals were written in  1 records.
oneint:    67 Im(pz)   integrals were written in  1 records.
oneint:    48 Im(lx)   integrals were written in  1 records.
oneint:    48 Im(ly)   integrals were written in  1 records.
oneint:    37 Im(lz)   integrals were written in  1 records.
oneint:   137 XX(*)    integrals were written in  1 records.
oneint:    70 XY(*)    integrals were written in  1 records.
oneint:    76 XZ(*)    integrals were written in  1 records.
oneint:   137 YY(*)    integrals were written in  1 records.
oneint:    76 YZ(*)    integrals were written in  1 records.
oneint:   101 ZZ(*)    integrals were written in  1 records.
oneint:    77 Veff(*)  integrals were written in  1 records.
oneint:    48 Im(SO:x) integrals were written in  1 records.
oneint:    48 Im(SO:y) integrals were written in  1 records.
oneint:    37 Im(SO:z) integrals were written in  1 records.
 

twoint:       30298 1/r12    integrals and       17 pk flags
                                 were written in    12 records.

 twoint: maximum mblu needed =     66946
 
driver: 1-e  integral workspace high-water mark =     29476
driver: 2-e  integral workspace high-water mark =     95970
driver: overall argos workspace high-water mark =     95970
