1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      2009-03.    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


================ Computing sorting integral file structure ================

                    -----z----- -----y----- -----x----- -----w----- ---total---

                CSFs         6         432       16740       14980       32158
      internal walks        24         144         360         280         808
valid internal walks         6         144         360         280         790
 getinfoarray: info=                     6 :                     1
                  8192                  6552                  8192
                  5460                     0
 icd(3)=                  5356 ci%nnlev=                   595  l2rec=
                  8192  n2max=                  5460
 lcore1,lcore2=             131030557             131007241
 lencor,maxblo             131072000                 60000
========================================
 current settings:
 minbl3         156
 minbl4         225
 locmaxbl3     1424
 locmaxbuf      712
 maxbl3       60000
 maxbl3       60000
 maxbl4       60000
 maxbuf       30006
========================================

 sorted 4-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)

 sorted 3-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)
 Orig.  diagonal integrals:  1electron:        34
                             0ext.    :        42
                             2ext.    :       336
                             4ext.    :       812


 Orig. off-diag. integrals:  4ext.    :     12615
                             3ext.    :      9360
                             2ext.    :      3708
                             1ext.    :       576
                             0ext.    :        99
                             2ext. SO :        12
                             1ext. SO :        48
                             0ext. SO :       138
                             1electron:        76


 Sorted integrals            3ext.  w :      8712 x :      8064
                             4ext.  w :     10563 x :      8841


Cycle #  1 sortfile size=    131068(       4 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core 131030557
Cycle #  2 sortfile size=    131068(       4 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core 131030557
 minimum size of srtscr:     65534 WP (     2 records)
 maximum size of srtscr:    131068 WP (     4 records)
diagi   file:      4 records  of   1536 WP each=>       6144 WP total
ofdgi   file:      9 records  of   1536 WP each=>      13824 WP total
fil3w   file:      1 records  of  30006 WP each=>      30006 WP total
fil3x   file:      1 records  of  30006 WP each=>      30006 WP total
fil4w   file:      1 records  of  30006 WP each=>      30006 WP total
fil4x   file:      1 records  of  30006 WP each=>      30006 WP total
  detected spin-orbit CI calculation ...
 compressed index vector length=                    19
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 0,
  GSET = 0,
   DAVCOR =10,
  NCOREL = 11
  NROOT = 6
  IVMODE = 3
  NBKITR = 1
  NVBKMN = 6
  RTOLBK = 1e-4,1e-4,1e-4,1e-4,1e-4,1e-4,
  NITER = 120
  NVCIMN = 8
  RTOLCI = 1e-4,1e-4,1e-4,1e-4,1e-4,1e-4,
  NVCIMX = 16
  NVRFMX = 16
  NVBKMX = 16
  update_mode=10
  IDEN  = 1
  CSFPRN = 10,
 /&end
 ------------------------------------------------------------------------
 Detected spin-orbit CI calculation ...
 Disabling tasklist usage (with_tsklst=0)!
lodens (list->root)=  6
invlodens (root->list)= -1 -1 -1 -1 -1  1
 USING SEGMENTS OF EQUAL SIZE

****************  list of control variables  ****************
 lvlprt =    0      nroot  =    6      noldv  =   0      noldhv =   0
 nunitv =    8      nbkitr =    1      niter  = 120      davcor =  10
 csfprn =   10      ivmode =    3      istrt  =   0      vout   =   0
 iortls =    0      nvbkmx =   16      ibktv  =  -1      ibkthv =  -1
 nvcimx =   16      icitv  =   -1      icithv =  -1      frcsub =   0
 nvbkmn =    6      nvcimn =    8      maxseg = 300      nrfitr =  30
 ncorel =   11      nvrfmx =   16      nvrfmn =   8      iden   =   1
 itran  =    0      froot  =    0      rtmode =   0      ncouple=   1
 skipso =    F      dalton2=    0      molcas =   0      finalv =   0
 finalw =    0      cosmocalc=   0    with_tsklst=   0
 nsegwx =    1     1     1     1
 nseg0x =    1     1     1     1
 nseg1x =    1     1     1     1
 nseg2x =    1     1     1     1
 nseg3x =    1     1     1     1
 nseg4x =    1     1     1     1
 no0ex  =      0    no1ex  =      0    no2ex  =     0    no3ex  =     0
 no4ex  =      0    nodiag =      0
 cdg4ex =    1      c3ex1ex=    1      c2ex0ex=   1
 fileloc=    0     0     0     0     0     0     0     1     1     1
 directhd=   1      noaqccshift_zyxw=      0
 critical_crit=-1.00000    critical_delta= 0.05000

 ctol   = 0.010000    lrtshift=1.000000    smalld =0.001000


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-04    1.000E-04
    2        1.000E-04    1.000E-04
    3        1.000E-04    1.000E-04
    4        1.000E-04    1.000E-04
    5        1.000E-04    1.000E-04
    6        1.000E-04    1.000E-04
 Computing density:                    .drt1.state6
 using                      1  nodes and                      1  cores.
 szdg/szodg per processor=                  6144                 13824
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core          131071999 DP per process

********** Integral sort section *************


 workspace allocation information: lencor= 131071999

 echo of the input for program cisrt:
 ------------------------------------------------------------------------
  &input
  maxbl3=60000
  maxbl4=60000
  &end
 ------------------------------------------------------------------------
 
 ( 6) listing file:                    ciudgls             
 ( 5) input file:                      cisrtin   
 (17) cidrt file:                      cidrtfl             
 (11) transformed integrals file:      moints    
 (12) diagonal integral file:          diagint             
 (13) off-diagonal integral file:      ofdgint             
 (31) 4-external w integrals file:     fil4w               
 (32) 4-external x integrals file:     fil4x               
 (33) 3-external w integrals file:     fil3w               
 (34) 3-external x integrals file:     fil3x               
 (21) scratch da sorting file:         srtscr              
 (12) 2-e integral file [fsplit=2]:    moints2   

 input integral file header information:
 Cu atom                                                                         
 aoints SIFS file created by argos.      zam792            14:38:58.196 03-Nov-14
  cidrt_title                                                                    
 MO-coefficients from mcscf.x                                                    
  with dummy occupation 1.0 for active orbitals                                  
  total ao core energy =    0.000000000                                          
 MCSCF energy =    -196.145501877                                                
 SIFS file created by program tran.      zam792            14:38:58.446 03-Nov-14

 input energy(*) values:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total core energy =   0.000000000000E+00

 nsym = 8 nmot=  34

 symmetry  =    1    2    3    4    5    6    7    8
 slabel(*) =   ag  b1g  b2g  b3g   au  b1u  b2u  b3u
 nmpsy(*)  =    9    3    3    3    1    5    5    5

 info(*) =          1      8192      6552      8192      5460         0

 orbital labels, i:molab(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012  13:tout:013  14:tout:014  15:tout:015  16:tout:016  17:tout:017  18:tout:018  19:tout:019  20:tout:020
  21:tout:021  22:tout:022  23:tout:023  24:tout:024  25:tout:025  26:tout:026  27:tout:027  28:tout:028  29:tout:029  30:tout:030
  31:tout:031  32:tout:032  33:tout:033  34:tout:034

 input parameters:
 prnopt=  0
 ldamin=    4095 ldamax=   32767 ldainc=      64
 maxbuf=   30006 maxbl3=   60000 maxbl4=   60000 intmxo=    1536
  Using 32 bit compression 

 drt information:
  cidrt_title                                                                    
 nmotd =  38 nfctd =   4 nfvtc =   0 nmot  =  34
 nlevel =  34 niot  =   6 lowinl=  29
 orbital-to-level map(*)
   -1  29  30  31   1   2   3   4   5   6  32   7   8  33   9  10  34  11  12  13
   -1  14  15  16  17  18  -1  19  20  21  22  23  -1  24  25  26  27  28
 compressed map(*)
   29  30  31   1   2   3   4   5   6  32   7   8  33   9  10  34  11  12  13  14
   15  16  17  18  19  20  21  22  23  24  25  26  27  28
 levsym(*)
    1   1   1   1   1   1   2   2   3   3   4   4   5   6   6   6   6   6   7   7
    7   7   7   8   8   8   8   8   1   1   1   2   3   4
 repartitioning mu(*)=
   0.  0.  0.  0.  0.  0.

 new core energy added to the energy(*) list.
 from the integral file: h1_core= -1.458883147958E+02

 indxdg: diagonal integral statistics.
 total number of integrals contributing to diagonal matrix elements:      1190
 number with all external indices:       812
 number with half external - half internal indices:       336
 number with all internal indices:        42

 indxof: off-diagonal integral statistics.
    4-external integrals: num=      12615 strt=          1
    3-external integrals: num=       9360 strt=      12616
    2-external integrals: num=       3708 strt=      21976
    1-external integrals: num=        576 strt=      25684
    0-external integrals: num=         99 strt=      26260

 total number of off-diagonal integrals:       26358


 indxof(2nd)  ittp=   3 numx(ittp)=        3708
 indxof(2nd)  ittp=   4 numx(ittp)=         576
 indxof(2nd)  ittp=   5 numx(ittp)=          99

 indxof: spin-orbit integral statistics.
    0-external so integrals: num=         12 strt=      26359
    1-external so integrals: num=         48 strt=      26371
    2-external so integrals: num=        138 strt=      26419

 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg= 130894995
 pro2e     1786    2381    2976    3571    4166    4187    4208    4803   48491   92179
   124946  133138  138598  160437

 pro2e:     23309 integrals read in     5 records.

 pro2e:         0 integrals 34-ext integrals skipped.
 pro1e     1786    2381    2976    3571    4166    4187    4208    4803   48491   92179
   124946  133138  138598  160437
 pro1e: eref =    0.000000000000000E+00
 total size of srtscr:                     4  records of                  32767 
 WP =               1048544 Bytes
 putdg     1786    2381    2976    3571    5107   37874   59719    4803   48491   92179
   124946  133138  138598  160437

 putf:       4 buffers of length    1536 written to file 12
 diagonal integral file completed.

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep4e:    53 blocks of linear combinations of 4-external integrals processed.
 number of sorted 4-external integrals      19404
 number of original 4-external integrals    12615


 putf34: external integral file complete. nfilw=    31 nfilx=    32 nrecw=     1 nrecx=     1 lbufp= 30006

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep3e:    43 blocks of linear combinations of 3-external integrals processed.
 number of sorted 3-external integrals      16776
 number of original 3-external integrals     9360


 putf34: external integral file complete. nfilw=    33 nfilx=    34 nrecw=     1 nrecx=     1 lbufp= 30006
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      3708         3     21976     21976      3708     26556
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd       576         4     25684     25684       576     26556
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd        99         5     26260     26260        99     26556
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd        12         6     26359     26359        12     26556
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd        48         7     26371     26371        48     26556
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd       138         8     26419     26419       138     26556

 putf:       9 buffers of length    1536 written to file 13
 off-diagonal files sort completed.
 executing brd_struct for cisrtinfo
cisrtinfo:
bufszi  1536
 diagfile 4ext:     812 2ext:     336 0ext:      42
 fil4w,fil4x  :   12615 fil3w,fil3x :    9360
 ofdgint  2ext:    3708 1ext:     576 0ext:      99so0ext:      12so1ext:      48so2ext:     138
buffer minbl4     225 minbl3     156 maxbl2     228nbas:   6   2   2   2   1   5   5   5 maxbuf 30006
 CIUDG version 5.9.7 ( 5-Oct-2004)

 workspace allocation information: lcore= 131071999

 core energy values from the integral file:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -1.458883147958E+02, ietype=    6,   fcore energy of type: H1(*)   

 total core repulsion energy = -1.458883147958E+02
 bummer (warning): calculation: setting iden=0                      0
 nmot  =    38 niot  =     7 nfct  =     4 nfvt  =     0
 nrow  =    39 nsym  =     8 ssym  =     4 lenbuf=  1600
 nwalk,xbar:        808       24      144      360      280
 nvalwt,nvalw:      790        6      144      360      280
 ncsft:           32158
 total number of valid internal walks:     790
 nvalz,nvaly,nvalx,nvalw =        6     144     360     280

 cisrt info file parameters:
 file number  12 blocksize   1536
 mxbld   1536
 nd4ext,nd2ext,nd0ext   812   336    42
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int    12615     9360     3708      576       99       12       48      138
 minbl4,minbl3,maxbl2   225   156   228
 maxbuf 30006
 number of external orbitals per symmetry block:   6   2   2   2   1   5   5   5
 nmsym   8 number of internal orbitals   6
 executing brd_struct for drt
 executing brd_struct for orbinf
 executing brd_struct for momap
 calcthrxt: niot,maxw1=                     7                    66
 block size     0
 pthz,pthy,pthx,pthw:    24   144   360   280 total internal walks:     808
 maxlp3,n2lp,n1lp,n0lp    66     0     0     0
 orbsym(*)= 1 1 1 2 3 4

 setref:        2 references kept,
               10 references were marked as invalid, out of
               12 total.
 pdinf%rmuval=  0.000000000000000E+000  0.000000000000000E+000
  0.000000000000000E+000  0.000000000000000E+000  0.000000000000000E+000
  0.000000000000000E+000
 nmb.of records onel     1
 nmb.of records 2-ext     3
 nmb.of records 1-ext     1
 nmb.of records 0-ext     1
 nmb.of records 2-int     1
 nmb.of records 1-int     1
 nmb.of records 0-int     1
 ---------memory usage in DP -----------------
 < n-ex core usage >
     routines:
    fourex            61660
    threx             60409
    twoex              2552
    onex               1633
    allin              1536
    diagon             2109
               =======
   maximum            61660
 
  __ static summary __ 
   reflst                 6
   hrfspc                 6
               -------
   static->               6
 
  __ core required  __ 
   totstc                 6
   max n-ex           61660
               -------
   totnec->           61666
 
  __ core available __ 
   totspc         131071999
   totnec -           61666
               -------
   totvec->       131010333

 number of external paths / symmetry
 vertex x      48      46      46      46      36      52      52      52
 vertex w      76      46      46      46      36      52      52      52
segment: free space=   131010333
 reducing frespc by                  2541 to              131007792 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1          24|         6|         0|         6|         0|         1|
 -------------------------------------------------------------------------------
  Y 2         144|       432|         6|       144|         6|         2|
 -------------------------------------------------------------------------------
  X 3         360|     16740|       438|       360|       150|         3|
 -------------------------------------------------------------------------------
  W 4         280|     14980|     17178|       280|       510|         4|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=          24DP  conft+indsym=        1440DP  drtbuffer=        1077 DP

dimension of the ci-matrix ->>>     32158

 executing brd_struct for civct
 gentasklist: ntask=                    20
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS   
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1     360      24      16740          6     360       6
     2  4   1    25      two-ext wz   2X  4 1     280      24      14980          6     280       6
     3  4   3    26      two-ext wx*  WX  4 3     280     360      14980      16740     280     360
     4  4   3    27      two-ext wx+  WX  4 3     280     360      14980      16740     280     360
     5  2   1    11      one-ext yz   1X  2 1     144      24        432          6     144       6
     6  3   2    15      1ex3ex yx    3X  3 2     360     144      16740        432     360     144
     7  4   2    16      1ex3ex yw    3X  4 2     280     144      14980        432     280     144
     8  1   1     1      allint zz    OX  1 1      24      24          6          6       6       6
     9  2   2     5      0ex2ex yy    OX  2 2     144     144        432        432     144     144
    10  3   3     6      0ex2ex xx*   OX  3 3     360     360      16740      16740     360     360
    11  3   3    18      0ex2ex xx+   OX  3 3     360     360      16740      16740     360     360
    12  4   4     7      0ex2ex ww*   OX  4 4     280     280      14980      14980     280     280
    13  4   4    19      0ex2ex ww+   OX  4 4     280     280      14980      14980     280     280
    14  2   2    42      four-ext y   4X  2 2     144     144        432        432     144     144
    15  3   3    43      four-ext x   4X  3 3     360     360      16740      16740     360     360
    16  4   4    44      four-ext w   4X  4 4     280     280      14980      14980     280     280
    17  1   1    75      dg-024ext z  OX  1 1      24      24          6          6       6       6
    18  2   2    76      dg-024ext y  OX  2 2     144     144        432        432     144     144
    19  3   3    77      dg-024ext x  OX  3 3     360     360      16740      16740     360     360
    20  4   4    78      dg-024ext w  OX  4 4     280     280      14980      14980     280     280
----------------------------------------------------------------------------------------------------
REDTASK #   1 TIME=  19.000 N=  1 (task/type/sgbra)=(   1/24/0) (
REDTASK #   2 TIME=  18.000 N=  1 (task/type/sgbra)=(   2/25/0) (
REDTASK #   3 TIME=  17.000 N=  1 (task/type/sgbra)=(   3/26/1) (
REDTASK #   4 TIME=  16.000 N=  1 (task/type/sgbra)=(   4/27/2) (
REDTASK #   5 TIME=  15.000 N=  1 (task/type/sgbra)=(   5/11/0) (
REDTASK #   6 TIME=  14.000 N=  1 (task/type/sgbra)=(   6/15/0) (
REDTASK #   7 TIME=  13.000 N=  1 (task/type/sgbra)=(   7/16/0) (
REDTASK #   8 TIME=  12.000 N=  1 (task/type/sgbra)=(   8/ 1/0) (
REDTASK #   9 TIME=  11.000 N=  1 (task/type/sgbra)=(   9/ 5/0) (
REDTASK #  10 TIME=  10.000 N=  1 (task/type/sgbra)=(  10/ 6/1) (
REDTASK #  11 TIME=   9.000 N=  1 (task/type/sgbra)=(  11/18/2) (
REDTASK #  12 TIME=   8.000 N=  1 (task/type/sgbra)=(  12/ 7/1) (
REDTASK #  13 TIME=   7.000 N=  1 (task/type/sgbra)=(  13/19/2) (
REDTASK #  14 TIME=   6.000 N=  1 (task/type/sgbra)=(  14/42/1) (
REDTASK #  15 TIME=   5.000 N=  1 (task/type/sgbra)=(  15/43/1) (
REDTASK #  16 TIME=   4.000 N=  1 (task/type/sgbra)=(  16/44/1) (
REDTASK #  17 TIME=   3.000 N=  1 (task/type/sgbra)=(  17/75/1) (
REDTASK #  18 TIME=   2.000 N=  1 (task/type/sgbra)=(  18/76/1) (
REDTASK #  19 TIME=   1.000 N=  1 (task/type/sgbra)=(  19/77/1) (
REDTASK #  20 TIME=   0.000 N=  1 (task/type/sgbra)=(  20/78/1) (
 initializing v-file: 1:                 32158

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      6 vectors will be written to unit 11 beginning with logical record   1

            6 vectors will be created
 bummer (warning):strefv: reducing number of start vectors to nref                      2
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:         126 2x:           0 4x:           0
All internal counts: zz :          15 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :          12 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:           0    task #     2:           0    task #     3:           0    task #     4:           0
task #     5:           0    task #     6:           0    task #     7:           0    task #     8:          29
task #     9:           0    task #    10:           0    task #    11:           0    task #    12:           0
task #    13:           0    task #    14:           0    task #    15:           0    task #    16:           0
task #    17:         105    task #    18:           0    task #    19:           0    task #    20:           0
 reference space has dimension       2
 dsyevx: computed roots 1 to    2(converged:   2)

    root           eigenvalues
    ----           ------------
       1        -196.1646625014
       2        -196.1615218388

 strefv generated    2 initial ci vector(s).
 bummer (warning):startv: num req > num generated, ngen=                       2
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=                     6

         vector  1 from unit 11 written to unit 49 filename cirefv              
 ufvoutnew: ... writing  recamt=                     6

         vector  2 from unit 11 written to unit 49 filename cirefv              
 ufvoutnew: ... writing  recamt=                     6

         vector  3 from unit 11 written to unit 49 filename cirefv              
 ufvoutnew: ... writing  recamt=                     6

         vector  4 from unit 11 written to unit 49 filename cirefv              
 ufvoutnew: ... writing  recamt=                     6

         vector  5 from unit 11 written to unit 49 filename cirefv              
 ufvoutnew: ... writing  recamt=                     6

         vector  6 from unit 11 written to unit 49 filename cirefv              

 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=     6)...
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:             32158
 number of initial trial vectors:                         2
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:              16
 number of roots to converge:                             6
 number of iterations:                                    1
 residual norm convergence criteria:               0.000100  0.000100  0.000100  0.000100  0.000100  0.000100

          starting bk iteration   1

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:           0 xx:           0 ww:           0
One-external counts: yz :         471 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:         150 wz:         160 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :          12 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :         153 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:           0    task #     4:           0
task #     5:         536    task #     6:           0    task #     7:           0    task #     8:          29
task #     9:           0    task #    10:           0    task #    11:           0    task #    12:           0
task #    13:           0    task #    14:           0    task #    15:           0    task #    16:           0
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:           0 xx:           0 ww:           0
One-external counts: yz :         471 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:         150 wz:         160 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :          12 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :         153 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:           0    task #     4:           0
task #     5:         536    task #     6:           0    task #     7:           0    task #     8:          29
task #     9:           0    task #    10:           0    task #    11:           0    task #    12:           0
task #    13:           0    task #    14:           0    task #    15:           0    task #    16:           0
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2
   ht   1   -50.27634771
   ht   2     0.00000000   -50.27320704

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2
 ref    1    1.00000      -3.636162E-12
 ref    2   3.636162E-12    1.00000    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2
 ref    1    1.00000        1.00000    

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2

          Spin-Orbit CI: perturbational estimate of Spin-Orbit CI energies and wavefunctions 

                SO   1         SO   2

   energy  -196.16466250  -196.16152184
 
   NR   1     1.00000000     0.00000000
   NR   2     0.00000000     1.00000000

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     1.00000000     0.00000000
 ref:   2     0.00000000     1.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -196.1646625014  0.0000E+00  4.0429E-01  1.7461E+00  1.0000E-04   
 mr-sdci #  1  2   -196.1615218388  0.0000E+00  0.0000E+00  1.7460E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.002000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mr-sdci  convergence not reached after  1 iterations.

 warning: *** the expansion subspace is invariant before all roots are converged.
 generate additional expansion vectors and try again.***


 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -196.1646625014  0.0000E+00  4.0429E-01  1.7461E+00  1.0000E-04   
 mr-sdci #  1  2   -196.1615218388  0.0000E+00  0.0000E+00  1.7460E+00  1.0000E-04   
 
 expansion vectors are not transformed.
 matrix-vector products are not transformed.

    3 expansion eigenvectors written to unit nvfile (= 11)
    2 matrix-vector products written to unit nhvfil (= 10)

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:             32158
 number of initial trial vectors:                         3
 number of initial matrix-vector products:                2
 maximum dimension of the subspace vectors:              16
 number of roots to converge:                             6
 number of iterations:                                  120
 residual norm convergence criteria:               0.000100  0.000100  0.000100  0.000100  0.000100  0.000100

          starting ci iteration   1

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3
   ht   1   -50.27634771
   ht   2     0.00000000   -50.27320704
   ht   3    -0.40428965     0.00042142  -105.01164411

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3
 ref    1   0.842140       1.031814E-03  -0.539258    
 ref    2  -8.626695E-04   0.999999       5.661943E-04

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3
 ref    1   0.709201        1.00000       0.290799    

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3

          Spin-Orbit CI: perturbational estimate of Spin-Orbit CI energies and wavefunctions 

                SO   1         SO   2         SO   3

   energy  -196.34333374  -196.16152184  -195.72891892
 
   NR   1     0.84214025     0.00103181    -0.53925757
   NR   2    -0.00086267     0.99999947     0.00056619
   NR   3     0.37217437    -0.00000802     0.58121207

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.84214025     0.00103181    -0.53925757
 ref:   2    -0.00086267     0.99999947     0.00056619

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -196.3433337440  1.7867E-01  0.0000E+00  1.1399E+00  1.0000E-04   
 mr-sdci #  1  2   -196.1615218422  3.3779E-09  4.0176E-01  1.7460E+00  1.0000E-04   
 mr-sdci #  1  3   -195.7289189189  4.9841E+01  0.0000E+00  2.6166E+00  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.002000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   2

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4
   ht   1   -50.27634771
   ht   2     0.00000000   -50.27320704
   ht   3    -0.40428965     0.00042142  -105.01164411
   ht   4     0.00005603    -0.40175812     0.89667707    -6.16649529

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4
 ref    1   8.253258E-03   0.842108       0.539241      -2.214497E-03
 ref    2   0.968864      -6.923988E-03  -5.032082E-03  -0.247445    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4
 ref    1   0.938766       0.709194       0.290806       6.123370E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.00825326     0.84210824     0.53924084    -0.00221450
 ref:   2     0.96886425    -0.00692399    -0.00503208    -0.24744453

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1   -196.4426933282  9.9360E-02  0.0000E+00  4.1908E-01  1.0000E-04   
 mr-sdci #  2  2   -196.3433312839  1.8181E-01  0.0000E+00  1.1400E+00  1.0000E-04   
 mr-sdci #  2  3   -195.7289386232  1.9704E-05  7.4927E-01  2.6165E+00  1.0000E-04   
 mr-sdci #  2  4   -191.8509216405  4.5963E+01  0.0000E+00  4.8018E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.003000
time for eigenvalue solver             0.001000
time for vector access                 0.000000

          starting ci iteration   3

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1   -50.27634771
   ht   2     0.00000000   -50.27320704
   ht   3    -0.40428965     0.00042142  -105.01164411
   ht   4     0.00005603    -0.40175812     0.89667707    -6.16649529
   ht   5    -0.56766398     0.00543915   -64.17026187     0.55106057   -48.71612589

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1  -9.368486E-03   0.319467      -0.935816      -5.466512E-03  -0.148567    
 ref    2  -0.968844      -7.407209E-03   7.629201E-03  -0.247367       6.212354E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1   0.938746       0.102114       0.875810       6.122018E-02   2.211065E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1    -0.00936849     0.31946653    -0.93581592    -0.00546651    -0.14856667
 ref:   2    -0.96884366    -0.00740721     0.00762920    -0.24736672     0.00621235

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1   -196.4426942097  8.8143E-07  0.0000E+00  4.1907E-01  1.0000E-04   
 mr-sdci #  3  2   -196.4180282186  7.4697E-02  0.0000E+00  6.5521E-01  1.0000E-04   
 mr-sdci #  3  3   -196.2607247886  5.3179E-01  0.0000E+00  1.4005E+00  1.0000E-04   
 mr-sdci #  3  4   -191.8511560507  2.3441E-04  2.8025E+00  4.8015E+00  1.0000E-04   
 mr-sdci #  3  5   -191.1864039528  4.5298E+01  0.0000E+00  3.8103E+00  1.0000E-04   
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.005000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   4

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1   -50.27634771
   ht   2     0.00000000   -50.27320704
   ht   3    -0.40428965     0.00042142  -105.01164411
   ht   4     0.00005603    -0.40175812     0.89667707    -6.16649529
   ht   5    -0.56766398     0.00543915   -64.17026187     0.55106057   -48.71612589
   ht   6     0.10319127     2.25393441     1.01649511    23.19174779     1.46538896 -4397.06618228

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -9.358139E-03   0.319470       0.935815       4.691654E-03   7.421868E-04   0.148590    
 ref    2  -0.968838      -7.388461E-03  -7.613867E-03   0.193602       0.154085      -4.062375E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.938735       0.102116       0.875807       3.750367E-02   2.374265E-02   2.209553E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.00935814     0.31947046     0.93581487     0.00469165     0.00074219     0.14859013
 ref:   2    -0.96883806    -0.00738846    -0.00761387     0.19360179     0.15408470    -0.00406237

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1   -196.4426948399  6.3021E-07  0.0000E+00  4.1898E-01  1.0000E-04   
 mr-sdci #  4  2   -196.4180284565  2.3793E-07  0.0000E+00  6.5521E-01  1.0000E-04   
 mr-sdci #  4  3   -196.2607264340  1.6454E-06  0.0000E+00  1.4005E+00  1.0000E-04   
 mr-sdci #  4  4   -192.0816350498  2.3048E-01  0.0000E+00  3.7560E+00  1.0000E-04   
 mr-sdci #  4  5   -191.4888515343  3.0245E-01  7.2645E-01  3.0775E+00  1.0000E-04   
 mr-sdci #  4  6   -191.1862402509  4.5298E+01  0.0000E+00  3.8092E+00  1.0000E-04   
 
 root number  5 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.005000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   5

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1   -50.27634771
   ht   2     0.00000000   -50.27320704
   ht   3    -0.40428965     0.00042142  -105.01164411
   ht   4     0.00005603    -0.40175812     0.89667707    -6.16649529
   ht   5    -0.56766398     0.00543915   -64.17026187     0.55106057   -48.71612589
   ht   6     0.10319127     2.25393441     1.01649511    23.19174779     1.46538896 -4397.06618228
   ht   7     0.02025675    -1.34765719     0.16071959   -12.58929108     0.56890320   267.28992167  -276.20018132

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1  -9.531980E-03   0.319472       0.935813       2.252884E-03   3.066519E-03   0.148541       4.750941E-03
 ref    2  -0.968671      -7.860700E-03  -7.656542E-03   0.195734      -0.107785       5.682870E-03  -0.107677    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.938414       0.102124       0.875804       3.831698E-02   1.162709E-02   2.209669E-02   1.161701E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.00953198     0.31947183     0.93581272     0.00225288     0.00306652     0.14854089     0.00475094
 ref:   2    -0.96867087    -0.00786070    -0.00765654     0.19573427    -0.10778535     0.00568287    -0.10767749

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1   -196.4427584116  6.3572E-05  0.0000E+00  4.1631E-01  1.0000E-04   
 mr-sdci #  5  2   -196.4180304623  2.0057E-06  0.0000E+00  6.5519E-01  1.0000E-04   
 mr-sdci #  5  3   -196.2607268774  4.4335E-07  0.0000E+00  1.4005E+00  1.0000E-04   
 mr-sdci #  5  4   -192.4840828604  4.0245E-01  0.0000E+00  3.6136E+00  1.0000E-04   
 mr-sdci #  5  5   -191.5347096900  4.5858E-02  0.0000E+00  2.1857E+00  1.0000E-04   
 mr-sdci #  5  6   -191.1876088985  1.3686E-03  4.2845E+00  3.8088E+00  1.0000E-04   
 mr-sdci #  5  7   -190.2009651422  4.4313E+01  0.0000E+00  2.4101E+00  1.0000E-04   
 
 root number  6 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.005000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   6

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.27634771
   ht   2     0.00000000   -50.27320704
   ht   3    -0.40428965     0.00042142  -105.01164411
   ht   4     0.00005603    -0.40175812     0.89667707    -6.16649529
   ht   5    -0.56766398     0.00543915   -64.17026187     0.55106057   -48.71612589
   ht   6     0.10319127     2.25393441     1.01649511    23.19174779     1.46538896 -4397.06618228
   ht   7     0.02025675    -1.34765719     0.16071959   -12.58929108     0.56890320   267.28992167  -276.20018132
   ht   8    -2.04546636    -0.18078623   -14.90009580    -1.58195950   -39.50647198    35.51325822   -36.10011767 -5305.24520305

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -9.551346E-03  -0.318815      -0.936085      -2.180274E-03   0.102579      -1.841758E-03  -0.107084       3.775559E-03
 ref    2  -0.968669       8.133972E-03   7.586944E-03  -0.195729       4.169342E-03   0.107841      -1.811044E-03  -0.107689    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.938411       0.101709       0.876312       3.831449E-02   1.053984E-02   1.163303E-02   1.147019E-02   1.161116E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00955135    -0.31881470    -0.93608450    -0.00218027     0.10257903    -0.00184176    -0.10708365     0.00377556
 ref:   2    -0.96866885     0.00813397     0.00758694    -0.19572873     0.00416934     0.10784079    -0.00181104    -0.10768893

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  6  1   -196.4427592960  8.8443E-07  3.5954E-02  4.1630E-01  1.0000E-04   
 mr-sdci #  6  2   -196.4180864503  5.5988E-05  0.0000E+00  6.5562E-01  1.0000E-04   
 mr-sdci #  6  3   -196.2609238282  1.9695E-04  0.0000E+00  1.4009E+00  1.0000E-04   
 mr-sdci #  6  4   -192.4840869163  4.0559E-06  0.0000E+00  3.6135E+00  1.0000E-04   
 mr-sdci #  6  5   -191.5785429063  4.3833E-02  0.0000E+00  2.6785E+00  1.0000E-04   
 mr-sdci #  6  6   -191.5347044888  3.4710E-01  0.0000E+00  2.1855E+00  1.0000E-04   
 mr-sdci #  6  7   -190.7789016564  5.7794E-01  0.0000E+00  2.7571E+00  1.0000E-04   
 mr-sdci #  6  8   -190.2007788870  4.4312E+01  0.0000E+00  2.4092E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.011000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   7

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.27634771
   ht   2     0.00000000   -50.27320704
   ht   3    -0.40428965     0.00042142  -105.01164411
   ht   4     0.00005603    -0.40175812     0.89667707    -6.16649529
   ht   5    -0.56766398     0.00543915   -64.17026187     0.55106057   -48.71612589
   ht   6     0.10319127     2.25393441     1.01649511    23.19174779     1.46538896 -4397.06618228
   ht   7     0.02025675    -1.34765719     0.16071959   -12.58929108     0.56890320   267.28992167  -276.20018132
   ht   8    -2.04546636    -0.18078623   -14.90009580    -1.58195950   -39.50647198    35.51325822   -36.10011767 -5305.24520305
   ht   9     0.00004406     0.00858157    -0.00792852    -1.50192705    -0.00714458     2.63191946    -1.41562659    -0.05887739

                ht   9
   ht   9    -1.31711120

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   8.768808E-03   0.318819       0.936091      -2.119775E-03   3.696680E-03   0.102449       9.627321E-04   0.107215    
 ref    2   0.973107      -7.720418E-03  -6.830881E-03  -7.068607E-02  -3.448239E-02  -2.234402E-03   0.135560       5.590344E-03

              v      9
 ref    1   1.191105E-03
 ref    2  -0.168399    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.947014       0.101705       0.876313       5.001013E-03   1.202700E-03   1.050080E-02   1.837742E-02   1.152634E-02

              v      9
 ref    1   2.835950E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00876881     0.31881919     0.93609082    -0.00211978     0.00369668     0.10244903     0.00096273     0.10721515
 ref:   2     0.97310706    -0.00772042    -0.00683088    -0.07068607    -0.03448239    -0.00223440     0.13555994     0.00559034

                ci   9
 ref:   1     0.00119110
 ref:   2    -0.16839859

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  7  1   -196.4645576256  2.1798E-02  0.0000E+00  1.0523E-01  1.0000E-04   
 mr-sdci #  7  2   -196.4180864600  9.6899E-09  7.4541E-02  6.5562E-01  1.0000E-04   
 mr-sdci #  7  3   -196.2609249570  1.1288E-06  0.0000E+00  1.4009E+00  1.0000E-04   
 mr-sdci #  7  4   -194.2770192472  1.7929E+00  0.0000E+00  3.1695E+00  1.0000E-04   
 mr-sdci #  7  5   -191.7842126819  2.0567E-01  0.0000E+00  1.7190E+00  1.0000E-04   
 mr-sdci #  7  6   -191.5783287872  4.3624E-02  0.0000E+00  2.6759E+00  1.0000E-04   
 mr-sdci #  7  7   -191.0094797089  2.3058E-01  0.0000E+00  3.6732E+00  1.0000E-04   
 mr-sdci #  7  8   -190.7784698302  5.7769E-01  0.0000E+00  2.7618E+00  1.0000E-04   
 mr-sdci #  7  9   -189.9027242892  4.4014E+01  0.0000E+00  4.0026E+00  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.008000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   8

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.27634771
   ht   2     0.00000000   -50.27320704
   ht   3    -0.40428965     0.00042142  -105.01164411
   ht   4     0.00005603    -0.40175812     0.89667707    -6.16649529
   ht   5    -0.56766398     0.00543915   -64.17026187     0.55106057   -48.71612589
   ht   6     0.10319127     2.25393441     1.01649511    23.19174779     1.46538896 -4397.06618228
   ht   7     0.02025675    -1.34765719     0.16071959   -12.58929108     0.56890320   267.28992167  -276.20018132
   ht   8    -2.04546636    -0.18078623   -14.90009580    -1.58195950   -39.50647198    35.51325822   -36.10011767 -5305.24520305
   ht   9     0.00004406     0.00858157    -0.00792852    -1.50192705    -0.00714458     2.63191946    -1.41562659    -0.05887739
   ht  10    -0.03952408     0.00060102     7.80514744    -0.10945250     2.90547354     0.10831149    -0.02101858    -2.02224288

                ht   9         ht  10
   ht   9    -1.31711120
   ht  10    -0.01645552    -2.02064159

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.365778       7.627658E-03  -0.914752       3.887462E-03  -0.149383       5.413924E-04  -1.530080E-02  -1.321970E-03
 ref    2  -5.142893E-03   0.973128       6.401490E-03   7.067161E-02   1.007437E-03  -3.444400E-02  -5.317214E-03  -0.135592    

              v      9       v     10
 ref    1   6.214984E-02  -5.428448E-02
 ref    2  -0.111255      -0.126418    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.133820       0.947036       0.836813       5.009589E-03   2.231643E-02   1.186682E-03   2.623874E-04   1.838694E-02

              v      9       v     10
 ref    1   1.624031E-02   1.892835E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.36577825     0.00762766    -0.91475243     0.00388746    -0.14938344     0.00054139    -0.01530080    -0.00132197
 ref:   2    -0.00514289     0.97312793     0.00640149     0.07067161     0.00100744    -0.03444400    -0.00531721    -0.13559200

                ci   9         ci  10
 ref:   1     0.06214984    -0.05428448
 ref:   2    -0.11125516    -0.12641812

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  8  1   -196.4683106623  3.7530E-03  0.0000E+00  1.0064E-01  1.0000E-04   
 mr-sdci #  8  2   -196.4645575913  4.6471E-02  0.0000E+00  1.0523E-01  1.0000E-04   
 mr-sdci #  8  3   -196.2630830179  2.1581E-03  4.8391E-01  1.3718E+00  1.0000E-04   
 mr-sdci #  8  4   -194.2772320142  2.1277E-04  0.0000E+00  3.1697E+00  1.0000E-04   
 mr-sdci #  8  5   -192.6168362588  8.3262E-01  0.0000E+00  2.6418E+00  1.0000E-04   
 mr-sdci #  8  6   -191.7840926552  2.0576E-01  0.0000E+00  1.7175E+00  1.0000E-04   
 mr-sdci #  8  7   -191.1777975378  1.6832E-01  0.0000E+00  1.6816E+00  1.0000E-04   
 mr-sdci #  8  8   -191.0094763593  2.3101E-01  0.0000E+00  3.6739E+00  1.0000E-04   
 mr-sdci #  8  9   -189.9161714325  1.3447E-02  0.0000E+00  4.7190E+00  1.0000E-04   
 mr-sdci #  8 10   -189.8913439752  4.4003E+01  0.0000E+00  4.6295E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.005000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   9

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.27634771
   ht   2     0.00000000   -50.27320704
   ht   3    -0.40428965     0.00042142  -105.01164411
   ht   4     0.00005603    -0.40175812     0.89667707    -6.16649529
   ht   5    -0.56766398     0.00543915   -64.17026187     0.55106057   -48.71612589
   ht   6     0.10319127     2.25393441     1.01649511    23.19174779     1.46538896 -4397.06618228
   ht   7     0.02025675    -1.34765719     0.16071959   -12.58929108     0.56890320   267.28992167  -276.20018132
   ht   8    -2.04546636    -0.18078623   -14.90009580    -1.58195950   -39.50647198    35.51325822   -36.10011767 -5305.24520305
   ht   9     0.00004406     0.00858157    -0.00792852    -1.50192705    -0.00714458     2.63191946    -1.41562659    -0.05887739
   ht  10    -0.03952408     0.00060102     7.80514744    -0.10945250     2.90547354     0.10831149    -0.02101858    -2.02224288
   ht  11     0.20852559    -0.00172117  -106.57411644     0.74542285   -63.35550915     0.61485475    -0.28974522    -2.07680014

                ht   9         ht  10         ht  11
   ht   9    -1.31711120
   ht  10    -0.01645552    -2.02064159
   ht  11    -0.09505302     8.23925080  -118.56938430

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.463276       4.244828E-03   0.861373      -3.784887E-03   1.497604E-02   1.307824E-03   1.773658E-02  -2.479585E-03
 ref    2  -9.472329E-03  -0.973054       1.046865E-02   7.076552E-02  -1.339968E-04  -3.446282E-02  -7.615514E-03   0.135545    

              v      9       v     10       v     11
 ref    1  -7.472212E-02  -1.711557E-02   0.192238    
 ref    2   6.200070E-03   0.167177       1.878938E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.214715       0.946853       0.742074       5.022085E-03   2.242998E-04   1.189396E-03   3.725822E-04   1.837866E-02

              v      9       v     10       v     11
 ref    1   5.621836E-03   2.824112E-02   3.730859E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.46327639     0.00424483     0.86137334    -0.00378489     0.01497604     0.00130782     0.01773658    -0.00247958
 ref:   2    -0.00947233    -0.97305450     0.01046865     0.07076552    -0.00013400    -0.03446282    -0.00761551     0.13554524

                ci   9         ci  10         ci  11
 ref:   1    -0.07472212    -0.01711557     0.19223827
 ref:   2     0.00620007     0.16717709     0.01878938

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  9  1   -196.4684645662  1.5390E-04  0.0000E+00  1.0807E-01  1.0000E-04   
 mr-sdci #  9  2   -196.4645596528  2.0616E-06  0.0000E+00  1.0522E-01  1.0000E-04   
 mr-sdci #  9  3   -196.4564780304  1.9340E-01  0.0000E+00  2.1668E-01  1.0000E-04   
 mr-sdci #  9  4   -194.2787584020  1.5264E-03  2.4343E+00  3.1695E+00  1.0000E-04   
 mr-sdci #  9  5   -193.1957243130  5.7889E-01  0.0000E+00  2.2693E+00  1.0000E-04   
 mr-sdci #  9  6   -191.7841034991  1.0844E-05  0.0000E+00  1.7178E+00  1.0000E-04   
 mr-sdci #  9  7   -191.2008457426  2.3048E-02  0.0000E+00  1.4906E+00  1.0000E-04   
 mr-sdci #  9  8   -191.0097774477  3.0109E-04  0.0000E+00  3.6736E+00  1.0000E-04   
 mr-sdci #  9  9   -190.1683463193  2.5217E-01  0.0000E+00  5.1657E+00  1.0000E-04   
 mr-sdci #  9 10   -189.9049542382  1.3610E-02  0.0000E+00  4.0099E+00  1.0000E-04   
 mr-sdci #  9 11   -189.5589327339  4.3671E+01  0.0000E+00  5.0292E+00  1.0000E-04   
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  10

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.27634771
   ht   2     0.00000000   -50.27320704
   ht   3    -0.40428965     0.00042142  -105.01164411
   ht   4     0.00005603    -0.40175812     0.89667707    -6.16649529
   ht   5    -0.56766398     0.00543915   -64.17026187     0.55106057   -48.71612589
   ht   6     0.10319127     2.25393441     1.01649511    23.19174779     1.46538896 -4397.06618228
   ht   7     0.02025675    -1.34765719     0.16071959   -12.58929108     0.56890320   267.28992167  -276.20018132
   ht   8    -2.04546636    -0.18078623   -14.90009580    -1.58195950   -39.50647198    35.51325822   -36.10011767 -5305.24520305
   ht   9     0.00004406     0.00858157    -0.00792852    -1.50192705    -0.00714458     2.63191946    -1.41562659    -0.05887739
   ht  10    -0.03952408     0.00060102     7.80514744    -0.10945250     2.90547354     0.10831149    -0.02101858    -2.02224288
   ht  11     0.20852559    -0.00172117  -106.57411644     0.74542285   -63.35550915     0.61485475    -0.28974522    -2.07680014
   ht  12     0.00388863    -0.63878625     0.12450677   -15.65654318    -0.01770401    -1.07331414     2.02236339     1.68736257

                ht   9         ht  10         ht  11         ht  12
   ht   9    -1.31711120
   ht  10    -0.01645552    -2.02064159
   ht  11    -0.09505302     8.23925080  -118.56938430
   ht  12    -8.51818658    -0.12130090    -0.48755068 -5379.39906573

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.463285       4.176087E-03   0.861368      -3.471810E-03   1.895912E-03   1.497605E-02   1.318109E-03  -1.773643E-02
 ref    2  -9.450514E-03  -0.973059       1.038395E-02   5.096117E-02  -4.929042E-02  -1.258315E-04  -3.421411E-02   7.635100E-03

              v      9       v     10       v     11       v     12
 ref    1  -2.520413E-03   7.471399E-02   1.714340E-02  -0.192238    
 ref    2   0.135678      -6.107834E-03  -0.167047      -1.879078E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.214723       0.946861       0.742063       2.609095E-03   2.433140E-03   2.242979E-04   1.172343E-03   3.728758E-04

              v      9       v     10       v     11       v     12
 ref    1   1.841492E-02   5.619486E-03   2.819875E-02   3.730857E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.46328524     0.00417609     0.86136821    -0.00347181     0.00189591     0.01497605     0.00131811    -0.01773643
 ref:   2    -0.00945051    -0.97305885     0.01038395     0.05096117    -0.04929042    -0.00012583    -0.03421411     0.00763510

                ci   9         ci  10         ci  11         ci  12
 ref:   1    -0.00252041     0.07471399     0.01714340    -0.19223807
 ref:   2     0.13567818    -0.00610783    -0.16704747    -0.01879078

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 10  1   -196.4684645805  1.4338E-08  0.0000E+00  1.0807E-01  1.0000E-04   
 mr-sdci # 10  2   -196.4645601561  5.0323E-07  0.0000E+00  1.0518E-01  1.0000E-04   
 mr-sdci # 10  3   -196.4564791295  1.0990E-06  0.0000E+00  2.1668E-01  1.0000E-04   
 mr-sdci # 10  4   -194.4917007447  2.1294E-01  0.0000E+00  2.2655E+00  1.0000E-04   
 mr-sdci # 10  5   -194.0258590276  8.3013E-01  1.4410E+00  2.2835E+00  1.0000E-04   
 mr-sdci # 10  6   -193.1957241104  1.4116E+00  0.0000E+00  2.2693E+00  1.0000E-04   
 mr-sdci # 10  7   -191.7834746386  5.8263E-01  0.0000E+00  1.7189E+00  1.0000E-04   
 mr-sdci # 10  8   -191.2008453294  1.9107E-01  0.0000E+00  1.4906E+00  1.0000E-04   
 mr-sdci # 10  9   -191.0076319739  8.3929E-01  0.0000E+00  3.6848E+00  1.0000E-04   
 mr-sdci # 10 10   -190.1683342530  2.6338E-01  0.0000E+00  5.1657E+00  1.0000E-04   
 mr-sdci # 10 11   -189.9038666643  3.4493E-01  0.0000E+00  4.0028E+00  1.0000E-04   
 mr-sdci # 10 12   -189.5589327226  4.3671E+01  0.0000E+00  5.0292E+00  1.0000E-04   
 
 root number  5 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.009000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  11

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.27634771
   ht   2     0.00000000   -50.27320704
   ht   3    -0.40428965     0.00042142  -105.01164411
   ht   4     0.00005603    -0.40175812     0.89667707    -6.16649529
   ht   5    -0.56766398     0.00543915   -64.17026187     0.55106057   -48.71612589
   ht   6     0.10319127     2.25393441     1.01649511    23.19174779     1.46538896 -4397.06618228
   ht   7     0.02025675    -1.34765719     0.16071959   -12.58929108     0.56890320   267.28992167  -276.20018132
   ht   8    -2.04546636    -0.18078623   -14.90009580    -1.58195950   -39.50647198    35.51325822   -36.10011767 -5305.24520305
   ht   9     0.00004406     0.00858157    -0.00792852    -1.50192705    -0.00714458     2.63191946    -1.41562659    -0.05887739
   ht  10    -0.03952408     0.00060102     7.80514744    -0.10945250     2.90547354     0.10831149    -0.02101858    -2.02224288
   ht  11     0.20852559    -0.00172117  -106.57411644     0.74542285   -63.35550915     0.61485475    -0.28974522    -2.07680014
   ht  12     0.00388863    -0.63878625     0.12450677   -15.65654318    -0.01770401    -1.07331414     2.02236339     1.68736257
   ht  13     0.10412573     0.38579250     2.33818197     7.97840744     2.74920370    22.07428343   -13.09045342    -1.00836386

                ht   9         ht  10         ht  11         ht  12         ht  13
   ht   9    -1.31711120
   ht  10    -0.01645552    -2.02064159
   ht  11    -0.09505302     8.23925080  -118.56938430
   ht  12    -8.51818658    -0.12130090    -0.48755068 -5379.39906573
   ht  13     7.83974554     1.00974690    -0.13567229   502.43125696 -7841.30318636

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.463436      -4.305232E-03  -0.861287       3.033174E-03   9.353283E-07  -2.374673E-03   1.497373E-02  -1.320650E-03
 ref    2   9.661879E-03   0.973060      -1.062857E-02  -5.302711E-02  -2.615087E-02   3.891502E-02  -1.070098E-04   3.423679E-02

              v      9       v     10       v     11       v     12       v     13
 ref    1  -1.774035E-02   2.507270E-03   7.467096E-02  -1.739467E-02   0.192233    
 ref    2   7.670117E-03  -0.135614      -5.657028E-03   0.167122       1.882246E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.214866       0.946864       0.741928       2.821074E-03   6.838678E-04   1.520018E-03   2.242240E-04   1.173902E-03

              v      9       v     10       v     11       v     12       v     13
 ref    1   3.735506E-04   1.839735E-02   5.607755E-03   2.823238E-02   3.730796E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.46343613    -0.00430523    -0.86128660     0.00303317     0.00000094    -0.00237467     0.01497373    -0.00132065
 ref:   2     0.00966188     0.97305981    -0.01062857    -0.05302711    -0.02615087     0.03891502    -0.00010701     0.03423679

                ci   9         ci  10         ci  11         ci  12         ci  13
 ref:   1    -0.01774035     0.00250727     0.07467096    -0.01739467     0.19223339
 ref:   2     0.00767012    -0.13561366    -0.00565703     0.16712213     0.01882246

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 11  1   -196.4684653931  8.1259E-07  0.0000E+00  1.0806E-01  1.0000E-04   
 mr-sdci # 11  2   -196.4645610097  8.5365E-07  0.0000E+00  1.0516E-01  1.0000E-04   
 mr-sdci # 11  3   -196.4564844830  5.3536E-06  0.0000E+00  2.1657E-01  1.0000E-04   
 mr-sdci # 11  4   -194.5443518556  5.2651E-02  0.0000E+00  2.2763E+00  1.0000E-04   
 mr-sdci # 11  5   -194.1213572591  9.5498E-02  0.0000E+00  1.1554E+00  1.0000E-04   
 mr-sdci # 11  6   -193.9046375944  7.0891E-01  4.2011E+00  2.0246E+00  1.0000E-04   
 mr-sdci # 11  7   -193.1957160792  1.4122E+00  0.0000E+00  2.2693E+00  1.0000E-04   
 mr-sdci # 11  8   -191.7834566378  5.8261E-01  0.0000E+00  1.7198E+00  1.0000E-04   
 mr-sdci # 11  9   -191.2008375489  1.9321E-01  0.0000E+00  1.4907E+00  1.0000E-04   
 mr-sdci # 11 10   -191.0075850914  8.3925E-01  0.0000E+00  3.6831E+00  1.0000E-04   
 mr-sdci # 11 11   -190.1682025875  2.6434E-01  0.0000E+00  5.1663E+00  1.0000E-04   
 mr-sdci # 11 12   -189.8996838338  3.4075E-01  0.0000E+00  4.0025E+00  1.0000E-04   
 mr-sdci # 11 13   -189.5589317614  4.3671E+01  0.0000E+00  5.0291E+00  1.0000E-04   
 
 root number  6 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.008000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  12

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.27634771
   ht   2     0.00000000   -50.27320704
   ht   3    -0.40428965     0.00042142  -105.01164411
   ht   4     0.00005603    -0.40175812     0.89667707    -6.16649529
   ht   5    -0.56766398     0.00543915   -64.17026187     0.55106057   -48.71612589
   ht   6     0.10319127     2.25393441     1.01649511    23.19174779     1.46538896 -4397.06618228
   ht   7     0.02025675    -1.34765719     0.16071959   -12.58929108     0.56890320   267.28992167  -276.20018132
   ht   8    -2.04546636    -0.18078623   -14.90009580    -1.58195950   -39.50647198    35.51325822   -36.10011767 -5305.24520305
   ht   9     0.00004406     0.00858157    -0.00792852    -1.50192705    -0.00714458     2.63191946    -1.41562659    -0.05887739
   ht  10    -0.03952408     0.00060102     7.80514744    -0.10945250     2.90547354     0.10831149    -0.02101858    -2.02224288
   ht  11     0.20852559    -0.00172117  -106.57411644     0.74542285   -63.35550915     0.61485475    -0.28974522    -2.07680014
   ht  12     0.00388863    -0.63878625     0.12450677   -15.65654318    -0.01770401    -1.07331414     2.02236339     1.68736257
   ht  13     0.10412573     0.38579250     2.33818197     7.97840744     2.74920370    22.07428343   -13.09045342    -1.00836386
   ht  14    -0.27085385    -0.86729421    -5.64748467   -16.91017441    -8.13397630   -50.98376812    26.11314306     1.06000228

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14
   ht   9    -1.31711120
   ht  10    -0.01645552    -2.02064159
   ht  11    -0.09505302     8.23925080  -118.56938430
   ht  12    -8.51818658    -0.12130090    -0.48755068 -5379.39906573
   ht  13     7.83974554     1.00974690    -0.13567229   502.43125696 -7841.30318636
   ht  14   -15.60854856    -2.05232610     0.57161768  -868.60628528  1998.75884454-86809.61784139

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.463443       4.342261E-03   0.861283      -3.114870E-03   2.481221E-04  -1.772902E-03   1.431386E-03  -1.497551E-02
 ref    2  -9.775160E-03  -0.973064       1.072881E-02   5.373912E-02  -2.647472E-02   1.553047E-02  -3.408562E-02   6.644806E-05

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1  -1.326406E-03  -1.773978E-02  -2.518715E-03  -7.464593E-02  -1.735929E-02   0.192245    
 ref    2   3.423149E-02   7.670867E-03   0.135631       5.536547E-03   0.167138       1.874713E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.214875       0.946873       0.741923       2.897595E-03   7.009723E-04   2.443387E-04   1.163878E-03   2.242703E-04

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1   1.173554E-03   3.735419E-04   1.840220E-02   5.602669E-03   2.823634E-02   3.730948E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.46344296     0.00434226     0.86128297    -0.00311487     0.00024812    -0.00177290     0.00143139    -0.01497551
 ref:   2    -0.00977516    -0.97306419     0.01072881     0.05373912    -0.02647472     0.01553047    -0.03408562     0.00006645

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14
 ref:   1    -0.00132641    -0.01773978    -0.00251872    -0.07464593    -0.01735929     0.19224471
 ref:   2     0.03423149     0.00767087     0.13563133     0.00553655     0.16713764     0.01874713

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 12  1   -196.4684654397  4.6559E-08  2.7160E-03  1.0806E-01  1.0000E-04   
 mr-sdci # 12  2   -196.4645653953  4.3856E-06  0.0000E+00  1.0511E-01  1.0000E-04   
 mr-sdci # 12  3   -196.4564846448  1.6176E-07  0.0000E+00  2.1657E-01  1.0000E-04   
 mr-sdci # 12  4   -194.5569857636  1.2634E-02  0.0000E+00  2.3156E+00  1.0000E-04   
 mr-sdci # 12  5   -194.1358538381  1.4497E-02  0.0000E+00  1.2103E+00  1.0000E-04   
 mr-sdci # 12  6   -193.9942266414  8.9589E-02  0.0000E+00  1.0651E+00  1.0000E-04   
 mr-sdci # 12  7   -193.8036098043  6.0789E-01  0.0000E+00  1.7314E+00  1.0000E-04   
 mr-sdci # 12  8   -193.1957027957  1.4122E+00  0.0000E+00  2.2693E+00  1.0000E-04   
 mr-sdci # 12  9   -191.7834333579  5.8260E-01  0.0000E+00  1.7201E+00  1.0000E-04   
 mr-sdci # 12 10   -191.2008374195  1.9325E-01  0.0000E+00  1.4907E+00  1.0000E-04   
 mr-sdci # 12 11   -191.0075482700  8.3935E-01  0.0000E+00  3.6837E+00  1.0000E-04   
 mr-sdci # 12 12   -190.1681561724  2.6847E-01  0.0000E+00  5.1664E+00  1.0000E-04   
 mr-sdci # 12 13   -189.8988618792  3.3993E-01  0.0000E+00  4.0018E+00  1.0000E-04   
 mr-sdci # 12 14   -189.5589040791  4.3671E+01  0.0000E+00  5.0292E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.016000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  13

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.27634771
   ht   2     0.00000000   -50.27320704
   ht   3    -0.40428965     0.00042142  -105.01164411
   ht   4     0.00005603    -0.40175812     0.89667707    -6.16649529
   ht   5    -0.56766398     0.00543915   -64.17026187     0.55106057   -48.71612589
   ht   6     0.10319127     2.25393441     1.01649511    23.19174779     1.46538896 -4397.06618228
   ht   7     0.02025675    -1.34765719     0.16071959   -12.58929108     0.56890320   267.28992167  -276.20018132
   ht   8    -2.04546636    -0.18078623   -14.90009580    -1.58195950   -39.50647198    35.51325822   -36.10011767 -5305.24520305
   ht   9     0.00004406     0.00858157    -0.00792852    -1.50192705    -0.00714458     2.63191946    -1.41562659    -0.05887739
   ht  10    -0.03952408     0.00060102     7.80514744    -0.10945250     2.90547354     0.10831149    -0.02101858    -2.02224288
   ht  11     0.20852559    -0.00172117  -106.57411644     0.74542285   -63.35550915     0.61485475    -0.28974522    -2.07680014
   ht  12     0.00388863    -0.63878625     0.12450677   -15.65654318    -0.01770401    -1.07331414     2.02236339     1.68736257
   ht  13     0.10412573     0.38579250     2.33818197     7.97840744     2.74920370    22.07428343   -13.09045342    -1.00836386
   ht  14    -0.27085385    -0.86729421    -5.64748467   -16.91017441    -8.13397630   -50.98376812    26.11314306     1.06000228
   ht  15    -0.00049698     0.00005370     0.08480212    -0.00884625     0.15167986     0.01739746    -0.01571833     0.28389498

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15
   ht   9    -1.31711120
   ht  10    -0.01645552    -2.02064159
   ht  11    -0.09505302     8.23925080  -118.56938430
   ht  12    -8.51818658    -0.12130090    -0.48755068 -5379.39906573
   ht  13     7.83974554     1.00974690    -0.13567229   502.43125696 -7841.30318636
   ht  14   -15.60854856    -2.05232610     0.57161768  -868.60628528  1998.75884454-86809.61784139
   ht  15    -0.00603926    -0.02398345    -0.01274647    -0.04252264     0.04339756    -0.04591759    -0.05323580

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.627161      -4.185245E-03  -0.750223      -1.703442E-03  -5.315801E-04  -6.534272E-03  -1.920100E-02   7.314775E-03
 ref    2  -1.430297E-02   0.972902      -1.796912E-02   5.379120E-02  -2.657722E-02  -1.527985E-02  -1.483524E-03  -3.396517E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1   1.320456E-02  -1.342950E-03   1.658112E-02  -2.446712E-03  -0.108910       0.126168      -0.122930    
 ref    2   1.071121E-03   3.423108E-02  -8.635715E-03   0.135575       0.128801       0.108165      -5.258345E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.393536       0.946556       0.563157       2.896394E-03   7.066313E-04   2.761707E-04   3.708792E-04   1.207138E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1   1.755076E-04   1.173570E-03   3.495093E-04   1.838662E-02   2.845127E-02   2.761812E-02   1.513947E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.62716111    -0.00418525    -0.75022300    -0.00170344    -0.00053158    -0.00653427    -0.01920100     0.00731478
 ref:   2    -0.01430297     0.97290192    -0.01796912     0.05379120    -0.02657722    -0.01527985    -0.00148352    -0.03396517

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15
 ref:   1     0.01320456    -0.00134295     0.01658112    -0.00244671    -0.10891038     0.12616831    -0.12293013
 ref:   2     0.00107112     0.03423108    -0.00863572     0.13557520     0.12880138     0.10816504    -0.00525835

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 13  1   -196.4713175157  2.8521E-03  0.0000E+00  5.1225E-02  1.0000E-04   
 mr-sdci # 13  2   -196.4645659281  5.3275E-07  3.6075E-03  1.0509E-01  1.0000E-04   
 mr-sdci # 13  3   -196.4586417904  2.1571E-03  0.0000E+00  1.8031E-01  1.0000E-04   
 mr-sdci # 13  4   -194.5579602280  9.7446E-04  0.0000E+00  2.3217E+00  1.0000E-04   
 mr-sdci # 13  5   -194.1360553172  2.0148E-04  0.0000E+00  1.2225E+00  1.0000E-04   
 mr-sdci # 13  6   -194.0062652617  1.2039E-02  0.0000E+00  1.5812E+00  1.0000E-04   
 mr-sdci # 13  7   -193.9190491798  1.1544E-01  0.0000E+00  3.0297E+00  1.0000E-04   
 mr-sdci # 13  8   -193.7916226853  5.9592E-01  0.0000E+00  2.0198E+00  1.0000E-04   
 mr-sdci # 13  9   -193.1354739995  1.3520E+00  0.0000E+00  2.2311E+00  1.0000E-04   
 mr-sdci # 13 10   -191.7834317709  5.8259E-01  0.0000E+00  1.7202E+00  1.0000E-04   
 mr-sdci # 13 11   -191.1766793341  1.6913E-01  0.0000E+00  1.2017E+00  1.0000E-04   
 mr-sdci # 13 12   -191.0074917595  8.3934E-01  0.0000E+00  3.6822E+00  1.0000E-04   
 mr-sdci # 13 13   -189.9180895695  1.9228E-02  0.0000E+00  4.2080E+00  1.0000E-04   
 mr-sdci # 13 14   -189.8644531570  3.0555E-01  0.0000E+00  4.3431E+00  1.0000E-04   
 mr-sdci # 13 15   -188.9709379992  4.3083E+01  0.0000E+00  5.1233E+00  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.009000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  14

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.27634771
   ht   2     0.00000000   -50.27320704
   ht   3    -0.40428965     0.00042142  -105.01164411
   ht   4     0.00005603    -0.40175812     0.89667707    -6.16649529
   ht   5    -0.56766398     0.00543915   -64.17026187     0.55106057   -48.71612589
   ht   6     0.10319127     2.25393441     1.01649511    23.19174779     1.46538896 -4397.06618228
   ht   7     0.02025675    -1.34765719     0.16071959   -12.58929108     0.56890320   267.28992167  -276.20018132
   ht   8    -2.04546636    -0.18078623   -14.90009580    -1.58195950   -39.50647198    35.51325822   -36.10011767 -5305.24520305
   ht   9     0.00004406     0.00858157    -0.00792852    -1.50192705    -0.00714458     2.63191946    -1.41562659    -0.05887739
   ht  10    -0.03952408     0.00060102     7.80514744    -0.10945250     2.90547354     0.10831149    -0.02101858    -2.02224288
   ht  11     0.20852559    -0.00172117  -106.57411644     0.74542285   -63.35550915     0.61485475    -0.28974522    -2.07680014
   ht  12     0.00388863    -0.63878625     0.12450677   -15.65654318    -0.01770401    -1.07331414     2.02236339     1.68736257
   ht  13     0.10412573     0.38579250     2.33818197     7.97840744     2.74920370    22.07428343   -13.09045342    -1.00836386
   ht  14    -0.27085385    -0.86729421    -5.64748467   -16.91017441    -8.13397630   -50.98376812    26.11314306     1.06000228
   ht  15    -0.00049698     0.00005370     0.08480212    -0.00884625     0.15167986     0.01739746    -0.01571833     0.28389498
   ht  16    -0.00000683     0.00311072     0.00102103    -0.52347424     0.00053739     1.52018905    -0.88445270    -0.04151563

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15         ht  16
   ht   9    -1.31711120
   ht  10    -0.01645552    -2.02064159
   ht  11    -0.09505302     8.23925080  -118.56938430
   ht  12    -8.51818658    -0.12130090    -0.48755068 -5379.39906573
   ht  13     7.83974554     1.00974690    -0.13567229   502.43125696 -7841.30318636
   ht  14   -15.60854856    -2.05232610     0.57161768  -868.60628528  1998.75884454-86809.61784139
   ht  15    -0.00603926    -0.02398345    -0.01274647    -0.04252264     0.04339756    -0.04591759    -0.05323580
   ht  16    -0.20459831    -0.00447914    -0.01977814    -0.67070357     0.31614012    -0.04313459    -0.00117385    -0.17182111

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.627188      -4.843037E-03  -0.750196       2.366685E-03   7.885536E-04  -9.142241E-04  -8.546992E-03  -1.883519E-02
 ref    2  -1.753885E-02   0.967952      -2.167356E-02  -0.153676       4.396124E-02  -1.473177E-02   1.572380E-02  -3.464873E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1  -5.906456E-03  -1.320513E-02   9.630325E-04  -1.645490E-02   9.391378E-04   0.166839      -0.115864       4.052582E-02
 ref    2  -1.465138E-04  -3.360150E-04  -1.187428E-02   1.905391E-03  -5.278957E-02  -5.114081E-03   5.442705E-02   0.174202    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.393672       0.936955       0.563263       2.362180E-02   1.933213E-03   2.178607E-04   3.202889E-04   3.667696E-04

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   3.490769E-05   1.744883E-04   1.419258E-04   2.743943E-04   2.787621E-03   2.786139E-02   1.638670E-02   3.198862E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15        ev   16

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.62718753    -0.00484304    -0.75019575     0.00236669     0.00078855    -0.00091422    -0.00854699    -0.01883519
 ref:   2    -0.01753885     0.96795214    -0.02167356    -0.15367561     0.04396124    -0.01473177     0.01572380    -0.00346487

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1    -0.00590646    -0.01320513     0.00096303    -0.01645490     0.00093914     0.16683895    -0.11586371     0.04052582
 ref:   2    -0.00014651    -0.00033601    -0.01187428     0.00190539    -0.05278957    -0.00511408     0.05442705     0.17420183

 trial vector basis is being transformed.  new dimension:   8

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 14  1   -196.4713176332  1.1751E-07  0.0000E+00  5.1210E-02  1.0000E-04   
 mr-sdci # 14  2   -196.4672306444  2.6647E-03  0.0000E+00  6.5008E-02  1.0000E-04   
 mr-sdci # 14  3   -196.4586420611  2.7079E-07  7.4649E-03  1.8030E-01  1.0000E-04   
 mr-sdci # 14  4   -195.1691836005  6.1122E-01  0.0000E+00  2.8090E+00  1.0000E-04   
 mr-sdci # 14  5   -194.2856289861  1.4957E-01  0.0000E+00  1.5027E+00  1.0000E-04   
 mr-sdci # 14  6   -194.0828317005  7.6566E-02  0.0000E+00  1.2304E+00  1.0000E-04   
 mr-sdci # 14  7   -193.9702286797  5.1179E-02  0.0000E+00  2.0504E+00  1.0000E-04   
 mr-sdci # 14  8   -193.9189234610  1.2730E-01  0.0000E+00  2.9716E+00  1.0000E-04   
 mr-sdci # 14  9   -193.7292063462  5.9373E-01  0.0000E+00  2.5020E+00  1.0000E-04   
 mr-sdci # 14 10   -193.1354384937  1.3520E+00  0.0000E+00  2.2309E+00  1.0000E-04   
 mr-sdci # 14 11   -191.7664074009  5.8973E-01  0.0000E+00  1.3296E+00  1.0000E-04   
 mr-sdci # 14 12   -191.1762747484  1.6878E-01  0.0000E+00  1.1852E+00  1.0000E-04   
 mr-sdci # 14 13   -190.7234902522  8.0540E-01  0.0000E+00  2.0027E+00  1.0000E-04   
 mr-sdci # 14 14   -189.8871509815  2.2698E-02  0.0000E+00  4.5436E+00  1.0000E-04   
 mr-sdci # 14 15   -188.9818615566  1.0924E-02  0.0000E+00  5.0503E+00  1.0000E-04   
 mr-sdci # 14 16   -188.8671307375  4.2979E+01  0.0000E+00  4.3660E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.013000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  15

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58300284
   ht   2     0.00000000   -50.57891585
   ht   3     0.00000000     0.00000000   -50.57032727
   ht   4     0.00000000     0.00000000     0.00000000   -49.28086880
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -48.39731419
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.19451690
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.08191388
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.03060867
   ht   9     0.06038893     0.01297269    -0.07406336     0.00687769     0.02539808    -0.00502705     0.11915072     0.19867571

                ht   9
   ht   9    -0.15890915

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.742237      -5.149311E-03  -0.632929       3.194040E-03   4.779313E-03   5.432482E-02   4.751897E-04   8.891495E-03
 ref    2  -1.383489E-02   0.967938      -2.455238E-02  -0.153612       4.446701E-02   7.056323E-03  -1.436862E-02  -1.510500E-02

              v      9
 ref    1   5.305505E-02
 ref    2   1.442424E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.551108       0.936930       0.401202       2.360672E-02   2.000157E-03   3.000978E-03   2.066831E-04   3.072196E-04

              v      9
 ref    1   2.816919E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.74223737    -0.00514931    -0.63292891     0.00319404     0.00477931     0.05432482     0.00047519     0.00889150
 ref:   2    -0.01383489     0.96793795    -0.02455238    -0.15361158     0.04446701     0.00705632    -0.01436862    -0.01510500

                ci   9
 ref:   1     0.05305505
 ref:   2     0.00144242

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 15  1   -196.4716073874  2.8975E-04  0.0000E+00  4.2656E-02  1.0000E-04   
 mr-sdci # 15  2   -196.4672306449  5.4519E-10  0.0000E+00  6.5010E-02  1.0000E-04   
 mr-sdci # 15  3   -196.4657314053  7.0893E-03  0.0000E+00  6.8919E-02  1.0000E-04   
 mr-sdci # 15  4   -195.1692433609  5.9760E-05  6.2470E+00  2.8093E+00  1.0000E-04   
 mr-sdci # 15  5   -194.2860238974  3.9491E-04  0.0000E+00  1.5229E+00  1.0000E-04   
 mr-sdci # 15  6   -194.1930070122  1.1018E-01  0.0000E+00  3.3076E+00  1.0000E-04   
 mr-sdci # 15  7   -194.0827026404  1.1247E-01  0.0000E+00  1.2346E+00  1.0000E-04   
 mr-sdci # 15  8   -193.9607326236  4.1809E-02  0.0000E+00  1.4122E+00  1.0000E-04   
 mr-sdci # 15  9   -193.8326249153  1.0342E-01  0.0000E+00  3.0434E+00  1.0000E-04   
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  16

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58300284
   ht   2     0.00000000   -50.57891585
   ht   3     0.00000000     0.00000000   -50.57032727
   ht   4     0.00000000     0.00000000     0.00000000   -49.28086880
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -48.39731419
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.19451690
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.08191388
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.03060867
   ht   9     0.06038893     0.01297269    -0.07406336     0.00687769     0.02539808    -0.00502705     0.11915072     0.19867571
   ht  10     3.86678696    -5.77773780    -1.03423260   113.87031911    14.77390004    -9.52831740     2.45265508   -13.47294181

                ht   9         ht  10
   ht   9    -0.15890915
   ht  10     0.95202689-58322.56840276

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.742357      -7.754490E-03   0.632753      -5.856980E-03   1.738212E-03   6.011111E-03   5.411085E-02  -6.067706E-04
 ref    2   1.453532E-02   0.967630       2.947390E-02   0.127554       8.789656E-02   4.535938E-02   6.176583E-03   1.509418E-02

              v      9       v     10
 ref    1   8.870485E-03   5.305843E-02
 ref    2  -1.525783E-02   1.328926E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.551305       0.936368       0.401245       1.630426E-02   7.728826E-03   2.093607E-03   2.966134E-03   2.282023E-04

              v      9       v     10
 ref    1   3.114869E-04   2.816964E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.74235680    -0.00775449     0.63275329    -0.00585698     0.00173821     0.00601111     0.05411085    -0.00060677
 ref:   2     0.01453532     0.96763011     0.02947390     0.12755374     0.08789656     0.04535938     0.00617658     0.01509418

                ci   9         ci  10
 ref:   1     0.00887048     0.05305843
 ref:   2    -0.01525783     0.00132893

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 16  1   -196.4716078581  4.7068E-07  0.0000E+00  4.2645E-02  1.0000E-04   
 mr-sdci # 16  2   -196.4672535848  2.2940E-05  0.0000E+00  6.5576E-02  1.0000E-04   
 mr-sdci # 16  3   -196.4657339156  2.5102E-06  0.0000E+00  6.8869E-02  1.0000E-04   
 mr-sdci # 16  4   -195.3352370975  1.6599E-01  0.0000E+00  2.0774E+00  1.0000E-04   
 mr-sdci # 16  5   -194.9711230085  6.8510E-01  4.2659E-01  1.9877E+00  1.0000E-04   
 mr-sdci # 16  6   -194.2817721429  8.8765E-02  0.0000E+00  1.5337E+00  1.0000E-04   
 mr-sdci # 16  7   -194.1924742621  1.0977E-01  0.0000E+00  3.3020E+00  1.0000E-04   
 mr-sdci # 16  8   -194.0813970050  1.2066E-01  0.0000E+00  1.2487E+00  1.0000E-04   
 mr-sdci # 16  9   -193.9606617464  1.2804E-01  0.0000E+00  1.4151E+00  1.0000E-04   
 mr-sdci # 16 10   -193.8325407504  6.9710E-01  0.0000E+00  3.0432E+00  1.0000E-04   
 
 root number  5 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.006000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  17

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58300284
   ht   2     0.00000000   -50.57891585
   ht   3     0.00000000     0.00000000   -50.57032727
   ht   4     0.00000000     0.00000000     0.00000000   -49.28086880
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -48.39731419
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.19451690
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.08191388
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.03060867
   ht   9     0.06038893     0.01297269    -0.07406336     0.00687769     0.02539808    -0.00502705     0.11915072     0.19867571
   ht  10     3.86678696    -5.77773780    -1.03423260   113.87031911    14.77390004    -9.52831740     2.45265508   -13.47294181
   ht  11     7.00578430    -0.49513356    -3.11324996   -20.98684041    -4.36035239     1.48331261    -1.59907200    -0.38495975

                ht   9         ht  10         ht  11
   ht   9    -0.15890915
   ht  10     0.95202689-58322.56840276
   ht  11    -0.75567042   277.61129003-11450.12171974

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.743354      -5.971475E-03   0.631528      -2.490118E-03  -1.107788E-02   1.968509E-02  -3.069136E-03  -5.148941E-02
 ref    2  -1.378699E-02   0.967729       2.584476E-02   0.127836      -7.159233E-02  -5.027429E-02  -4.542406E-02  -8.215346E-03

              v      9       v     10       v     11
 ref    1  -5.761856E-04   8.397925E-03   5.296658E-02
 ref    2   1.526298E-02  -1.509346E-02   2.049766E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.552765       0.936535       0.399495       1.634829E-02   5.248181E-03   2.915007E-03   2.072765E-03   2.718652E-03

              v      9       v     10       v     11
 ref    1   2.332904E-04   2.983377E-04   2.809660E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.74335369    -0.00597147     0.63152793    -0.00249012    -0.01107788     0.01968509    -0.00306914    -0.05148941
 ref:   2    -0.01378699     0.96772896     0.02584476     0.12783617    -0.07159233    -0.05027429    -0.04542406    -0.00821535

                ci   9         ci  10         ci  11
 ref:   1    -0.00057619     0.00839792     0.05296658
 ref:   2     0.01526298    -0.01509346     0.00204977

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 17  1   -196.4716134044  5.5463E-06  0.0000E+00  4.2613E-02  1.0000E-04   
 mr-sdci # 17  2   -196.4672555242  1.9394E-06  0.0000E+00  6.5636E-02  1.0000E-04   
 mr-sdci # 17  3   -196.4657503169  1.6401E-05  0.0000E+00  6.8576E-02  1.0000E-04   
 mr-sdci # 17  4   -195.3465806621  1.1344E-02  0.0000E+00  2.0620E+00  1.0000E-04   
 mr-sdci # 17  5   -194.9840852957  1.2962E-02  0.0000E+00  1.6666E+00  1.0000E-04   
 mr-sdci # 17  6   -194.9138551495  6.3208E-01 -1.4709E+00  1.4356E+00  1.0000E-04   
 mr-sdci # 17  7   -194.2804130631  8.7939E-02  0.0000E+00  1.5210E+00  1.0000E-04   
 mr-sdci # 17  8   -194.1854911379  1.0409E-01  0.0000E+00  3.3140E+00  1.0000E-04   
 mr-sdci # 17  9   -194.0813293787  1.2067E-01  0.0000E+00  1.2486E+00  1.0000E-04   
 mr-sdci # 17 10   -193.9604710134  1.2793E-01  0.0000E+00  1.4032E+00  1.0000E-04   
 mr-sdci # 17 11   -193.8233565251  2.0569E+00  0.0000E+00  3.0275E+00  1.0000E-04   
 
 root number  6 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.006000
time for eigenvalue solver             0.001000
time for vector access                 0.000000

          starting ci iteration  18

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58300284
   ht   2     0.00000000   -50.57891585
   ht   3     0.00000000     0.00000000   -50.57032727
   ht   4     0.00000000     0.00000000     0.00000000   -49.28086880
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -48.39731419
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.19451690
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.08191388
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.03060867
   ht   9     0.06038893     0.01297269    -0.07406336     0.00687769     0.02539808    -0.00502705     0.11915072     0.19867571
   ht  10     3.86678696    -5.77773780    -1.03423260   113.87031911    14.77390004    -9.52831740     2.45265508   -13.47294181
   ht  11     7.00578430    -0.49513356    -3.11324996   -20.98684041    -4.36035239     1.48331261    -1.59907200    -0.38495975
   ht  12    -5.77245150    -1.94523002     1.27145774    -5.96649935   -43.58069197    24.85787678   -27.27892716    -1.15059901

                ht   9         ht  10         ht  11         ht  12
   ht   9    -0.15890915
   ht  10     0.95202689-58322.56840276
   ht  11    -0.75567042   277.61129003-11450.12171974
   ht  12     2.35353483    76.49501108  -182.60073969-36543.73064462

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.743466      -5.585028E-03  -0.631388       2.526630E-03   1.800702E-02  -1.247737E-02   9.166282E-03  -3.349254E-03
 ref    2  -1.364553E-02   0.967750      -2.508572E-02  -0.127835       5.107228E-02   6.391544E-02  -3.105180E-02  -4.552066E-02

              v      9       v     10       v     11       v     12
 ref    1  -5.111556E-02   5.844989E-04   8.385646E-03   5.300860E-02
 ref    2  -7.820869E-03  -1.526810E-02  -1.510114E-02   1.908602E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.552928       0.936571       0.399280       1.634824E-02   2.932630E-03   4.240868E-03   1.048235E-03   2.083348E-03

              v      9       v     10       v     11       v     12
 ref    1   2.673967E-03   2.334565E-04   2.983635E-04   2.813555E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.74346639    -0.00558503    -0.63138823     0.00252663     0.01800702    -0.01247737     0.00916628    -0.00334925
 ref:   2    -0.01364553     0.96774998    -0.02508572    -0.12783529     0.05107228     0.06391544    -0.03105180    -0.04552066

                ci   9         ci  10         ci  11         ci  12
 ref:   1    -0.05111556     0.00058450     0.00838565     0.05300860
 ref:   2    -0.00782087    -0.01526810    -0.01510114     0.00190860

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 18  1   -196.4716139738  5.6935E-07  6.5299E-04  4.2606E-02  1.0000E-04   
 mr-sdci # 18  2   -196.4672561985  6.7425E-07  0.0000E+00  6.5587E-02  1.0000E-04   
 mr-sdci # 18  3   -196.4657523872  2.0703E-06  0.0000E+00  6.8566E-02  1.0000E-04   
 mr-sdci # 18  4   -195.3465862607  5.5986E-06  0.0000E+00  2.0625E+00  1.0000E-04   
 mr-sdci # 18  5   -194.9911253115  7.0400E-03  0.0000E+00  1.2588E+00  1.0000E-04   
 mr-sdci # 18  6   -194.9629822940  4.9127E-02  0.0000E+00  1.6307E+00  1.0000E-04   
 mr-sdci # 18  7   -194.8601671631  5.7975E-01  0.0000E+00  9.7299E-01  1.0000E-04   
 mr-sdci # 18  8   -194.2803199181  9.4829E-02  0.0000E+00  1.5231E+00  1.0000E-04   
 mr-sdci # 18  9   -194.1840575005  1.0273E-01  0.0000E+00  3.3143E+00  1.0000E-04   
 mr-sdci # 18 10   -194.0813280887  1.2086E-01  0.0000E+00  1.2489E+00  1.0000E-04   
 mr-sdci # 18 11   -193.9604688033  1.3711E-01  0.0000E+00  1.4031E+00  1.0000E-04   
 mr-sdci # 18 12   -193.8223019946  2.6460E+00  0.0000E+00  3.0265E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.015000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  19

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58300284
   ht   2     0.00000000   -50.57891585
   ht   3     0.00000000     0.00000000   -50.57032727
   ht   4     0.00000000     0.00000000     0.00000000   -49.28086880
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -48.39731419
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.19451690
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.08191388
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.03060867
   ht   9     0.06038893     0.01297269    -0.07406336     0.00687769     0.02539808    -0.00502705     0.11915072     0.19867571
   ht  10     3.86678696    -5.77773780    -1.03423260   113.87031911    14.77390004    -9.52831740     2.45265508   -13.47294181
   ht  11     7.00578430    -0.49513356    -3.11324996   -20.98684041    -4.36035239     1.48331261    -1.59907200    -0.38495975
   ht  12    -5.77245150    -1.94523002     1.27145774    -5.96649935   -43.58069197    24.85787678   -27.27892716    -1.15059901
   ht  13     0.22985735     0.00825511    -0.15963971    -0.01523073     0.00497658    -0.00921936    -0.06550796    -0.13422918

                ht   9         ht  10         ht  11         ht  12         ht  13
   ht   9    -0.15890915
   ht  10     0.95202689-58322.56840276
   ht  11    -0.75567042   277.61129003-11450.12171974
   ht  12     2.35353483    76.49501108  -182.60073969-36543.73064462
   ht  13     0.00236993    -0.02012281    -0.00268240    -0.06706765    -0.01967844

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.792765      -8.021226E-03   0.561973      -0.132937       1.446249E-02  -1.917516E-03  -7.657247E-04   2.834074E-03
 ref    2   1.453385E-02   0.967428       3.500102E-02  -8.386776E-03  -0.127602      -6.322031E-02  -5.167674E-02  -3.134474E-02

              v      9       v     10       v     11       v     12       v     13
 ref    1   3.069881E-03   5.360858E-02  -3.969566E-03   9.096111E-04   2.664779E-02
 ref    2   4.552032E-02   7.845420E-03   1.531522E-02  -1.513961E-02  -1.330917E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.628688       0.935982       0.317039       1.774250E-02   1.649137E-02   4.000485E-03   2.671071E-03   9.905246E-04

              v      9       v     10       v     11       v     12       v     13
 ref    1   2.081524E-03   2.935431E-03   2.503134E-04   2.300351E-04   7.118761E-04

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.79276519    -0.00802123     0.56197321    -0.13293669     0.01446249    -0.00191752    -0.00076572     0.00283407
 ref:   2     0.01453385     0.96742847     0.03500102    -0.00838678    -0.12760176    -0.06322031    -0.05167674    -0.03134474

                ci   9         ci  10         ci  11         ci  12         ci  13
 ref:   1     0.00306988     0.05360858    -0.00396957     0.00090961     0.02664779
 ref:   2     0.04552032     0.00784542     0.01531522    -0.01513961    -0.00133092

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 19  1   -196.4723991799  7.8521E-04  0.0000E+00  3.0391E-02  1.0000E-04   
 mr-sdci # 19  2   -196.4672565817  3.8321E-07  1.1014E-03  6.5600E-02  1.0000E-04   
 mr-sdci # 19  3   -196.4661114251  3.5904E-04  0.0000E+00  6.4816E-02  1.0000E-04   
 mr-sdci # 19  4   -195.3749599169  2.8374E-02  0.0000E+00  1.4386E+00  1.0000E-04   
 mr-sdci # 19  5   -195.3463563340  3.5523E-01  0.0000E+00  2.0556E+00  1.0000E-04   
 mr-sdci # 19  6   -194.9859534666  2.2971E-02  0.0000E+00  1.4581E+00  1.0000E-04   
 mr-sdci # 19  7   -194.9572945122  9.7127E-02  0.0000E+00  1.4451E+00  1.0000E-04   
 mr-sdci # 19  8   -194.8587340944  5.7841E-01  0.0000E+00  9.8936E-01  1.0000E-04   
 mr-sdci # 19  9   -194.2803148147  9.6257E-02  0.0000E+00  1.5245E+00  1.0000E-04   
 mr-sdci # 19 10   -194.1837191612  1.0239E-01  0.0000E+00  3.3018E+00  1.0000E-04   
 mr-sdci # 19 11   -194.0808422833  1.2037E-01  0.0000E+00  1.2650E+00  1.0000E-04   
 mr-sdci # 19 12   -193.9586346492  1.3633E-01  0.0000E+00  1.3152E+00  1.0000E-04   
 mr-sdci # 19 13   -192.6787207049  1.9552E+00  0.0000E+00  3.0516E+00  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  20

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58300284
   ht   2     0.00000000   -50.57891585
   ht   3     0.00000000     0.00000000   -50.57032727
   ht   4     0.00000000     0.00000000     0.00000000   -49.28086880
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -48.39731419
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.19451690
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.08191388
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.03060867
   ht   9     0.06038893     0.01297269    -0.07406336     0.00687769     0.02539808    -0.00502705     0.11915072     0.19867571
   ht  10     3.86678696    -5.77773780    -1.03423260   113.87031911    14.77390004    -9.52831740     2.45265508   -13.47294181
   ht  11     7.00578430    -0.49513356    -3.11324996   -20.98684041    -4.36035239     1.48331261    -1.59907200    -0.38495975
   ht  12    -5.77245150    -1.94523002     1.27145774    -5.96649935   -43.58069197    24.85787678   -27.27892716    -1.15059901
   ht  13     0.22985735     0.00825511    -0.15963971    -0.01523073     0.00497658    -0.00921936    -0.06550796    -0.13422918
   ht  14     0.00955187    -0.10095229    -0.00059698     0.13704686     0.06525504    -0.06961377     0.08200050    -0.01234126

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14
   ht   9    -0.15890915
   ht  10     0.95202689-58322.56840276
   ht  11    -0.75567042   277.61129003-11450.12171974
   ht  12     2.35353483    76.49501108  -182.60073969-36543.73064462
   ht  13     0.00236993    -0.02012281    -0.00268240    -0.06706765    -0.01967844
   ht  14     0.00401985     0.81030846    -0.42212495     0.20419902     0.00077782    -0.03676431

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.792793       1.737746E-02  -0.561721       6.522560E-03   0.133094      -1.153084E-02  -9.703625E-05  -2.184810E-03
 ref    2  -1.557386E-02  -0.962393      -5.273045E-02  -0.193582       6.644490E-03  -1.027816E-02  -8.309122E-03   1.627973E-02

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1   8.296090E-03  -4.198754E-02  -3.228575E-02   3.102357E-03  -4.817715E-03   2.653762E-02
 ref    2  -1.406929E-02   1.016778E-02  -1.395141E-02   4.799429E-03  -1.318796E-02  -8.843516E-04

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.628763       0.926502       0.318312       3.751663E-02   1.775812E-02   2.386008E-04   6.905092E-05   2.698029E-04

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1   2.667700E-04   1.866337E-03   1.237011E-03   3.265914E-05   1.971327E-04   7.050276E-04

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.79279275     0.01737746    -0.56172147     0.00652256     0.13309385    -0.01153084    -0.00009704    -0.00218481
 ref:   2    -0.01557386    -0.96239284    -0.05273045    -0.19358225     0.00664449    -0.01027816    -0.00830912     0.01627973

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14
 ref:   1     0.00829609    -0.04198754    -0.03228575     0.00310236    -0.00481771     0.02653762
 ref:   2    -0.01406929     0.01016778    -0.01395141     0.00479943    -0.01318796    -0.00088435

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 20  1   -196.4723992044  2.4458E-08  0.0000E+00  3.0400E-02  1.0000E-04   
 mr-sdci # 20  2   -196.4682998366  1.0433E-03  0.0000E+00  4.0570E-02  1.0000E-04   
 mr-sdci # 20  3   -196.4661122244  7.9931E-07  1.1404E-03  6.4629E-02  1.0000E-04   
 mr-sdci # 20  4   -195.6168833175  2.4192E-01  0.0000E+00  1.4949E+00  1.0000E-04   
 mr-sdci # 20  5   -195.3749557884  2.8599E-02  0.0000E+00  1.4366E+00  1.0000E-04   
 mr-sdci # 20  6   -195.1821817376  1.9623E-01  0.0000E+00  1.0375E+00  1.0000E-04   
 mr-sdci # 20  7   -194.9664808934  9.1864E-03  0.0000E+00  8.1497E-01  1.0000E-04   
 mr-sdci # 20  8   -194.8633026311  4.5685E-03  0.0000E+00  7.7477E-01  1.0000E-04   
 mr-sdci # 20  9   -194.6586278422  3.7831E-01  0.0000E+00  2.5175E+00  1.0000E-04   
 mr-sdci # 20 10   -194.1962399519  1.2521E-02  0.0000E+00  2.6649E+00  1.0000E-04   
 mr-sdci # 20 11   -194.1534314929  7.2589E-02  0.0000E+00  2.1784E+00  1.0000E-04   
 mr-sdci # 20 12   -194.0057076953  4.7073E-02  0.0000E+00  8.7426E-01  1.0000E-04   
 mr-sdci # 20 13   -193.3704875118  6.9177E-01  0.0000E+00  3.3166E+00  1.0000E-04   
 mr-sdci # 20 14   -192.6774410747  2.7903E+00  0.0000E+00  3.0361E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.008000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  21

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58300284
   ht   2     0.00000000   -50.57891585
   ht   3     0.00000000     0.00000000   -50.57032727
   ht   4     0.00000000     0.00000000     0.00000000   -49.28086880
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -48.39731419
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.19451690
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.08191388
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.03060867
   ht   9     0.06038893     0.01297269    -0.07406336     0.00687769     0.02539808    -0.00502705     0.11915072     0.19867571
   ht  10     3.86678696    -5.77773780    -1.03423260   113.87031911    14.77390004    -9.52831740     2.45265508   -13.47294181
   ht  11     7.00578430    -0.49513356    -3.11324996   -20.98684041    -4.36035239     1.48331261    -1.59907200    -0.38495975
   ht  12    -5.77245150    -1.94523002     1.27145774    -5.96649935   -43.58069197    24.85787678   -27.27892716    -1.15059901
   ht  13     0.22985735     0.00825511    -0.15963971    -0.01523073     0.00497658    -0.00921936    -0.06550796    -0.13422918
   ht  14     0.00955187    -0.10095229    -0.00059698     0.13704686     0.06525504    -0.06961377     0.08200050    -0.01234126
   ht  15    -0.48147592    -0.03869281     0.37663940    -0.12041326    -0.05214735     0.03917554    -0.19538993    -0.25832045

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15
   ht   9    -0.15890915
   ht  10     0.95202689-58322.56840276
   ht  11    -0.75567042   277.61129003-11450.12171974
   ht  12     2.35353483    76.49501108  -182.60073969-36543.73064462
   ht  13     0.00236993    -0.02012281    -0.00268240    -0.06706765    -0.01967844
   ht  14     0.00401985     0.81030846    -0.42212495     0.20419902     0.00077782    -0.03676431
   ht  15    -0.01437860     0.18463928    -0.06396832     0.08599109     0.00674833    -0.00174638    -0.04691122

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.801479      -8.691872E-03   0.546273      -1.616566E-02  -0.122448      -0.108281      -5.477255E-03  -3.340529E-03
 ref    2   1.355301E-02  -0.963842       4.911478E-03   0.192399      -2.227684E-02  -4.535982E-03  -1.006137E-02  -8.269029E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1   3.734874E-03  -9.314033E-03  -9.544661E-04   2.080424E-03  -1.172881E-04   1.683511E-02  -6.310019E-03
 ref    2  -1.634567E-02  -1.430963E-02  -1.761007E-02  -4.720325E-03  -1.326581E-02  -1.736004E-03   2.345659E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.642552       0.929068       0.298438       3.727860E-02   1.548976E-02   1.174525E-02   1.312315E-04   7.953598E-05

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1   2.811302E-04   2.915168E-04   3.110256E-04   2.660963E-05   1.759955E-04   2.864346E-04   4.531846E-05

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.80147895    -0.00869187     0.54627302    -0.01616566    -0.12244794    -0.10828054    -0.00547725    -0.00334053
 ref:   2     0.01355301    -0.96384249     0.00491148     0.19239874    -0.02227684    -0.00453598    -0.01006137    -0.00826903

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15
 ref:   1     0.00373487    -0.00931403    -0.00095447     0.00208042    -0.00011729     0.01683511    -0.00631002
 ref:   2    -0.01634567    -0.01430963    -0.01761007    -0.00472032    -0.01326581    -0.00173600     0.00234566

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 21  1   -196.4724069355  7.7312E-06  0.0000E+00  3.1531E-02  1.0000E-04   
 mr-sdci # 21  2   -196.4683052365  5.3999E-06  0.0000E+00  3.9756E-02  1.0000E-04   
 mr-sdci # 21  3   -196.4672100604  1.0978E-03  0.0000E+00  4.5131E-02  1.0000E-04   
 mr-sdci # 21  4   -195.6173215270  4.3821E-04  3.9494E-01  1.5251E+00  1.0000E-04   
 mr-sdci # 21  5   -195.5531627428  1.7821E-01  0.0000E+00  1.3736E+00  1.0000E-04   
 mr-sdci # 21  6   -195.3655489807  1.8337E-01  0.0000E+00  1.4585E+00  1.0000E-04   
 mr-sdci # 21  7   -195.1793548805  2.1287E-01  0.0000E+00  1.0036E+00  1.0000E-04   
 mr-sdci # 21  8   -194.9656415736  1.0234E-01  0.0000E+00  7.8943E-01  1.0000E-04   
 mr-sdci # 21  9   -194.8631095486  2.0448E-01  0.0000E+00  7.7996E-01  1.0000E-04   
 mr-sdci # 21 10   -194.6344338431  4.3819E-01  0.0000E+00  2.4577E+00  1.0000E-04   
 mr-sdci # 21 11   -194.1693207990  1.5889E-02  0.0000E+00  1.1279E+00  1.0000E-04   
 mr-sdci # 21 12   -194.0079179971  2.2103E-03  0.0000E+00  8.3464E-01  1.0000E-04   
 mr-sdci # 21 13   -193.4185923141  4.8105E-02  0.0000E+00  3.4413E+00  1.0000E-04   
 mr-sdci # 21 14   -192.9059192285  2.2848E-01  0.0000E+00  3.1700E+00  1.0000E-04   
 mr-sdci # 21 15   -192.0691281493  3.0873E+00  0.0000E+00  4.2700E+00  1.0000E-04   
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  22

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58300284
   ht   2     0.00000000   -50.57891585
   ht   3     0.00000000     0.00000000   -50.57032727
   ht   4     0.00000000     0.00000000     0.00000000   -49.28086880
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -48.39731419
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.19451690
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.08191388
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.03060867
   ht   9     0.06038893     0.01297269    -0.07406336     0.00687769     0.02539808    -0.00502705     0.11915072     0.19867571
   ht  10     3.86678696    -5.77773780    -1.03423260   113.87031911    14.77390004    -9.52831740     2.45265508   -13.47294181
   ht  11     7.00578430    -0.49513356    -3.11324996   -20.98684041    -4.36035239     1.48331261    -1.59907200    -0.38495975
   ht  12    -5.77245150    -1.94523002     1.27145774    -5.96649935   -43.58069197    24.85787678   -27.27892716    -1.15059901
   ht  13     0.22985735     0.00825511    -0.15963971    -0.01523073     0.00497658    -0.00921936    -0.06550796    -0.13422918
   ht  14     0.00955187    -0.10095229    -0.00059698     0.13704686     0.06525504    -0.06961377     0.08200050    -0.01234126
   ht  15    -0.48147592    -0.03869281     0.37663940    -0.12041326    -0.05214735     0.03917554    -0.19538993    -0.25832045
   ht  16     0.93998872    -4.03741279    -0.19886153   -17.35580916   -10.34143012     8.30591179    -9.34496518     0.28264639

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15         ht  16
   ht   9    -0.15890915
   ht  10     0.95202689-58322.56840276
   ht  11    -0.75567042   277.61129003-11450.12171974
   ht  12     2.35353483    76.49501108  -182.60073969-36543.73064462
   ht  13     0.00236993    -0.02012281    -0.00268240    -0.06706765    -0.01967844
   ht  14     0.00401985     0.81030846    -0.42212495     0.20419902     0.00077782    -0.03676431
   ht  15    -0.01437860     0.18463928    -0.06396832     0.08599109     0.00674833    -0.00174638    -0.04691122
   ht  16     0.26534956   -37.23140680    17.23477557   -14.55774301    -0.07885042     0.22682661    -0.09007027   -76.68048749

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.801211      -3.955154E-03  -0.546709      -2.324957E-02   0.118391       0.110993      -7.704258E-03   5.539268E-03
 ref    2  -1.191070E-02   0.963079      -2.497966E-02   0.169069       4.875220E-02  -7.651155E-03   2.219506E-02  -4.445542E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1  -1.677727E-03   3.499008E-03   1.305680E-02   9.323386E-04  -2.217064E-03   4.249563E-04  -1.663985E-02  -6.329579E-03
 ref    2  -3.197613E-02  -6.460971E-02  -2.318896E-02   1.770982E-02   5.341895E-03  -1.556561E-02   6.960621E-04   2.467471E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.642081       0.927537       0.299515       2.912499E-02   1.639331E-02   1.237795E-02   5.519762E-04   2.006968E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   1.025287E-03   4.186657E-03   7.082079E-04   3.145070E-04   3.345121E-05   2.424687E-04   2.773691E-04   4.615199E-05

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15        ev   16

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.80121120    -0.00395515    -0.54670910    -0.02324957     0.11839145     0.11099283    -0.00770426     0.00553927
 ref:   2    -0.01191070     0.96307911    -0.02497966     0.16906935     0.04875220    -0.00765115     0.02219506    -0.04445542

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1    -0.00167773     0.00349901     0.01305680     0.00093234    -0.00221706     0.00042496    -0.01663985    -0.00632958
 ref:   2    -0.03197613    -0.06460971    -0.02318896     0.01770982     0.00534189    -0.01556561     0.00069606     0.00246747

 trial vector basis is being transformed.  new dimension:   8

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 22  1   -196.4724077569  8.2135E-07  0.0000E+00  3.1662E-02  1.0000E-04   
 mr-sdci # 22  2   -196.4683683332  6.3097E-05  0.0000E+00  3.2802E-02  1.0000E-04   
 mr-sdci # 22  3   -196.4672185715  8.5111E-06  0.0000E+00  4.4557E-02  1.0000E-04   
 mr-sdci # 22  4   -195.7860409682  1.6872E-01  0.0000E+00  7.8180E-01  1.0000E-04   
 mr-sdci # 22  5   -195.5546987777  1.5360E-03  1.1262E+00  1.3197E+00  1.0000E-04   
 mr-sdci # 22  6   -195.3672482026  1.6992E-03  0.0000E+00  1.4571E+00  1.0000E-04   
 mr-sdci # 22  7   -195.1970567578  1.7702E-02  0.0000E+00  1.1859E+00  1.0000E-04   
 mr-sdci # 22  8   -195.0158079559  5.0166E-02  0.0000E+00  1.2935E+00  1.0000E-04   
 mr-sdci # 22  9   -194.9132248664  5.0115E-02  0.0000E+00  1.1457E+00  1.0000E-04   
 mr-sdci # 22 10   -194.7839834840  1.4955E-01  0.0000E+00  2.0236E+00  1.0000E-04   
 mr-sdci # 22 11   -194.5665412109  3.9722E-01  0.0000E+00  2.2659E+00  1.0000E-04   
 mr-sdci # 22 12   -194.1693184855  1.6140E-01  0.0000E+00  1.1272E+00  1.0000E-04   
 mr-sdci # 22 13   -194.0077851484  5.8919E-01  0.0000E+00  8.3163E-01  1.0000E-04   
 mr-sdci # 22 14   -193.4140362660  5.0812E-01  0.0000E+00  3.4205E+00  1.0000E-04   
 mr-sdci # 22 15   -192.9044789038  8.3535E-01  0.0000E+00  3.1701E+00  1.0000E-04   
 mr-sdci # 22 16   -192.0690868117  3.2020E+00  0.0000E+00  4.2702E+00  1.0000E-04   
 
 root number  5 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.011000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  23

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58409296
   ht   2     0.00000000   -50.58005354
   ht   3     0.00000000     0.00000000   -50.57890378
   ht   4     0.00000000     0.00000000     0.00000000   -49.89772617
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -49.66638398
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.47893341
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.30874196
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.12749316
   ht   9    -1.91627213    11.19840277    -5.71307214  -141.98610433   -97.38784706   -45.75185425    51.33474507   -59.31286804

                ht   9
   ht   9 -9673.84444996

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.801139       6.156120E-03  -0.546788       2.773897E-02   8.737216E-02  -6.972725E-02  -0.116458       1.107340E-02
 ref    2   1.167833E-02  -0.962920      -2.856535E-02  -0.168823       3.777135E-02  -4.345123E-02   3.157318E-03  -1.915687E-02

              v      9
 ref    1   9.045590E-03
 ref    2  -4.206872E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.641960       0.927253       0.299793       2.927054E-02   9.060570E-03   6.749899E-03   1.357247E-02   4.896057E-04

              v      9
 ref    1   1.851600E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.80113869     0.00615612    -0.54678766     0.02773897     0.08737216    -0.06972725    -0.11645817     0.01107340
 ref:   2     0.01167833    -0.96292015    -0.02856535    -0.16882267     0.03777135    -0.04345123     0.00315732    -0.01915687

                ci   9
 ref:   1     0.00904559
 ref:   2    -0.04206872

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 23  1   -196.4724079278  1.7088E-07  0.0000E+00  3.1716E-02  1.0000E-04   
 mr-sdci # 23  2   -196.4683745777  6.2445E-06  0.0000E+00  3.2902E-02  1.0000E-04   
 mr-sdci # 23  3   -196.4672214474  2.8758E-06  0.0000E+00  4.4234E-02  1.0000E-04   
 mr-sdci # 23  4   -195.7892261149  3.1851E-03  0.0000E+00  7.9377E-01  1.0000E-04   
 mr-sdci # 23  5   -195.6252993407  7.0601E-02  0.0000E+00  9.0283E-01  1.0000E-04   
 mr-sdci # 23  6   -195.4584061593  9.1158E-02  3.0354E-01  1.2431E+00  1.0000E-04   
 mr-sdci # 23  7   -195.3657804900  1.6872E-01  0.0000E+00  1.4626E+00  1.0000E-04   
 mr-sdci # 23  8   -195.1950325174  1.7922E-01  0.0000E+00  1.1757E+00  1.0000E-04   
 mr-sdci # 23  9   -195.0095012952  9.6276E-02  0.0000E+00  1.3100E+00  1.0000E-04   
 
 root number  6 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  24

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58409296
   ht   2     0.00000000   -50.58005354
   ht   3     0.00000000     0.00000000   -50.57890378
   ht   4     0.00000000     0.00000000     0.00000000   -49.89772617
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -49.66638398
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.47893341
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.30874196
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.12749316
   ht   9    -1.91627213    11.19840277    -5.71307214  -141.98610433   -97.38784706   -45.75185425    51.33474507   -59.31286804
   ht  10     4.14922092    -0.32182783    -0.40260112    21.34577857     7.97319842    10.08980014    -7.23483202     8.77873868

                ht   9         ht  10
   ht   9 -9673.84444996
   ht  10   175.82436505  -335.92984826

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.801245      -7.795538E-03   0.546534       3.379971E-02   9.367816E-02  -1.753032E-02   0.102867       7.788501E-02
 ref    2  -1.170891E-02   0.962816       3.160300E-02  -0.166467       5.073855E-02  -1.744698E-02  -1.061258E-02   3.875362E-02

              v      9       v     10
 ref    1   1.166164E-02   1.101160E-02
 ref    2  -1.875554E-02  -4.060073E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.642130       0.927075       0.299698       2.885370E-02   1.135000E-02   6.117091E-04   1.069426E-02   7.567917E-03

              v      9       v     10
 ref    1   4.877642E-04   1.769674E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.80124482    -0.00779554     0.54653424     0.03379971     0.09367816    -0.01753032     0.10286705     0.07788501
 ref:   2    -0.01170891     0.96281586     0.03160300    -0.16646706     0.05073855    -0.01744698    -0.01061258     0.03875362

                ci   9         ci  10
 ref:   1     0.01166164     0.01101160
 ref:   2    -0.01875554    -0.04060073

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 24  1   -196.4724079768  4.9038E-08  1.9253E-04  3.1678E-02  1.0000E-04   
 mr-sdci # 24  2   -196.4683751459  5.6812E-07  0.0000E+00  3.2969E-02  1.0000E-04   
 mr-sdci # 24  3   -196.4672432530  2.1806E-05  0.0000E+00  4.2220E-02  1.0000E-04   
 mr-sdci # 24  4   -195.7920067873  2.7807E-03  0.0000E+00  7.9766E-01  1.0000E-04   
 mr-sdci # 24  5   -195.6541807744  2.8881E-02  0.0000E+00  8.2121E-01  1.0000E-04   
 mr-sdci # 24  6   -195.5414140468  8.3008E-02  0.0000E+00  7.1886E-01  1.0000E-04   
 mr-sdci # 24  7   -195.3670173750  1.2369E-03  0.0000E+00  1.4500E+00  1.0000E-04   
 mr-sdci # 24  8   -195.3115460705  1.1651E-01  0.0000E+00  1.5156E+00  1.0000E-04   
 mr-sdci # 24  9   -195.1950137057  1.8551E-01  0.0000E+00  1.1754E+00  1.0000E-04   
 mr-sdci # 24 10   -195.0082865064  2.2430E-01  0.0000E+00  1.3171E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.013000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  25

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58409296
   ht   2     0.00000000   -50.58005354
   ht   3     0.00000000     0.00000000   -50.57890378
   ht   4     0.00000000     0.00000000     0.00000000   -49.89772617
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -49.66638398
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.47893341
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.30874196
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.12749316
   ht   9    -1.91627213    11.19840277    -5.71307214  -141.98610433   -97.38784706   -45.75185425    51.33474507   -59.31286804
   ht  10     4.14922092    -0.32182783    -0.40260112    21.34577857     7.97319842    10.08980014    -7.23483202     8.77873868
   ht  11    -0.45792418    -0.01722527    -0.03741476    -0.00727284    -0.01880216     0.01093477    -0.02831063     0.00263267

                ht   9         ht  10         ht  11
   ht   9 -9673.84444996
   ht  10   175.82436505  -335.92984826
   ht  11    -0.12339759     0.06954688    -0.00784069

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.806492      -7.695883E-03   0.538251      -3.705359E-02  -0.119651      -7.731271E-02   1.926712E-03  -3.402428E-02
 ref    2  -1.295661E-02   0.962716       3.399553E-02   0.165951      -5.059085E-02   9.765815E-03  -1.960501E-02  -3.875368E-02

              v      9       v     10       v     11
 ref    1  -2.793842E-03  -7.603611E-03   9.865524E-02
 ref    2  -2.063395E-02   4.084405E-02   1.281432E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.650597       0.926882       0.290870       2.891283E-02   1.687589E-02   6.072627E-03   3.880687E-04   2.659499E-03

              v      9       v     10       v     11
 ref    1   4.335656E-04   1.726051E-03   9.897064E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.80649206    -0.00769588     0.53825119    -0.03705359    -0.11965138    -0.07731271     0.00192671    -0.03402428
 ref:   2    -0.01295661     0.96271632     0.03399553     0.16595137    -0.05059085     0.00976582    -0.01960501    -0.03875368

                ci   9         ci  10         ci  11
 ref:   1    -0.00279384    -0.00760361     0.09865524
 ref:   2    -0.02063395     0.04084405     0.01281432

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 25  1   -196.4725476671  1.3969E-04  0.0000E+00  1.3496E-02  1.0000E-04   
 mr-sdci # 25  2   -196.4683754878  3.4197E-07  2.3269E-04  3.2918E-02  1.0000E-04   
 mr-sdci # 25  3   -196.4672633849  2.0132E-05  0.0000E+00  3.9244E-02  1.0000E-04   
 mr-sdci # 25  4   -195.7922329817  2.2619E-04  0.0000E+00  7.9245E-01  1.0000E-04   
 mr-sdci # 25  5   -195.6692614975  1.5081E-02  0.0000E+00  6.6713E-01  1.0000E-04   
 mr-sdci # 25  6   -195.5562360026  1.4822E-02  0.0000E+00  1.0092E+00  1.0000E-04   
 mr-sdci # 25  7   -195.5406559702  1.7364E-01  0.0000E+00  7.5847E-01  1.0000E-04   
 mr-sdci # 25  8   -195.3201886464  8.6426E-03  0.0000E+00  1.4538E+00  1.0000E-04   
 mr-sdci # 25  9   -195.1975603548  2.5466E-03  0.0000E+00  1.1900E+00  1.0000E-04   
 mr-sdci # 25 10   -195.0086511614  3.6466E-04  0.0000E+00  1.2997E+00  1.0000E-04   
 mr-sdci # 25 11   -192.2347938100 -2.3317E+00  0.0000E+00  4.3964E+00  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  26

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58409296
   ht   2     0.00000000   -50.58005354
   ht   3     0.00000000     0.00000000   -50.57890378
   ht   4     0.00000000     0.00000000     0.00000000   -49.89772617
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -49.66638398
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.47893341
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.30874196
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.12749316
   ht   9    -1.91627213    11.19840277    -5.71307214  -141.98610433   -97.38784706   -45.75185425    51.33474507   -59.31286804
   ht  10     4.14922092    -0.32182783    -0.40260112    21.34577857     7.97319842    10.08980014    -7.23483202     8.77873868
   ht  11    -0.45792418    -0.01722527    -0.03741476    -0.00727284    -0.01880216     0.01093477    -0.02831063     0.00263267
   ht  12    -0.10983652    -0.36698529     0.02772039    -0.01518053     0.02712690     0.00299610     0.15458759    -0.11335803

                ht   9         ht  10         ht  11         ht  12
   ht   9 -9673.84444996
   ht  10   175.82436505  -335.92984826
   ht  11    -0.12339759     0.06954688    -0.00784069
   ht  12    -0.11028885     0.11129722    -0.00003663    -0.01146323

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.806491      -1.220448E-02   0.538158      -2.593540E-02   0.121063      -7.691688E-02  -2.055155E-02   2.624970E-02
 ref    2  -1.291950E-02   0.961657       4.232479E-02   0.175214       3.040322E-02   3.991008E-03  -5.821044E-03   1.456124E-02

              v      9       v     10       v     11       v     12
 ref    1   1.922136E-02  -1.257078E-02  -2.448898E-03  -0.103800    
 ref    2   1.611106E-02   1.092624E-02   5.861104E-02  -2.609492E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.650594       0.924933       0.291405       3.137269E-02   1.558070E-02   5.932134E-03   4.562506E-04   9.010765E-04

              v      9       v     10       v     11       v     12
 ref    1   6.290270E-04   2.774074E-04   3.441251E-03   1.145540E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.80649086    -0.01220448     0.53815774    -0.02593540     0.12106340    -0.07691688    -0.02055155     0.02624970
 ref:   2    -0.01291950     0.96165711     0.04232479     0.17521428     0.03040322     0.00399101    -0.00582104     0.01456124

                ci   9         ci  10         ci  11         ci  12
 ref:   1     0.01922136    -0.01257078    -0.00244890    -0.10380005
 ref:   2     0.01611106     0.01092624     0.05861104    -0.02609492

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 26  1   -196.4725476672  1.4408E-10  0.0000E+00  1.3495E-02  1.0000E-04   
 mr-sdci # 26  2   -196.4685542413  1.7875E-04  0.0000E+00  2.0258E-02  1.0000E-04   
 mr-sdci # 26  3   -196.4672639804  5.9541E-07  2.6182E-04  3.9222E-02  1.0000E-04   
 mr-sdci # 26  4   -195.8510345301  5.8802E-02  0.0000E+00  9.6270E-01  1.0000E-04   
 mr-sdci # 26  5   -195.6754217814  6.1603E-03  0.0000E+00  7.3264E-01  1.0000E-04   
 mr-sdci # 26  6   -195.5598029627  3.5670E-03  0.0000E+00  1.0627E+00  1.0000E-04   
 mr-sdci # 26  7   -195.5465006540  5.8447E-03  0.0000E+00  7.3370E-01  1.0000E-04   
 mr-sdci # 26  8   -195.3714234178  5.1235E-02  0.0000E+00  1.4911E+00  1.0000E-04   
 mr-sdci # 26  9   -195.2658216112  6.8261E-02  0.0000E+00  1.4609E+00  1.0000E-04   
 mr-sdci # 26 10   -195.0665618660  5.7911E-02  0.0000E+00  1.1011E+00  1.0000E-04   
 mr-sdci # 26 11   -194.1043181313  1.8695E+00  0.0000E+00  3.5287E+00  1.0000E-04   
 mr-sdci # 26 12   -191.9944173336 -2.1749E+00  0.0000E+00  4.3200E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  27

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58409296
   ht   2     0.00000000   -50.58005354
   ht   3     0.00000000     0.00000000   -50.57890378
   ht   4     0.00000000     0.00000000     0.00000000   -49.89772617
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -49.66638398
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.47893341
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.30874196
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.12749316
   ht   9    -1.91627213    11.19840277    -5.71307214  -141.98610433   -97.38784706   -45.75185425    51.33474507   -59.31286804
   ht  10     4.14922092    -0.32182783    -0.40260112    21.34577857     7.97319842    10.08980014    -7.23483202     8.77873868
   ht  11    -0.45792418    -0.01722527    -0.03741476    -0.00727284    -0.01880216     0.01093477    -0.02831063     0.00263267
   ht  12    -0.10983652    -0.36698529     0.02772039    -0.01518053     0.02712690     0.00299610     0.15458759    -0.11335803
   ht  13    -0.06755373    -0.06753329     0.27596638     0.01994001     0.01159858    -0.02671456    -0.01481442     0.01023703

                ht   9         ht  10         ht  11         ht  12         ht  13
   ht   9 -9673.84444996
   ht  10   175.82436505  -335.92984826
   ht  11    -0.12339759     0.06954688    -0.00784069
   ht  12    -0.11028885     0.11129722    -0.00003663    -0.01146323
   ht  13    -0.35780091     0.28825116    -0.00005677    -0.00050036    -0.00635366

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.807003      -1.330296E-02   0.536874      -4.294043E-02  -9.656327E-02   9.708973E-02   4.224830E-02   2.209256E-02
 ref    2  -1.288551E-02   0.961566       4.448989E-02   0.166314      -6.444354E-02  -2.567416E-03   4.346395E-03   1.030741E-02

              v      9       v     10       v     11       v     12       v     13
 ref    1  -9.602482E-04  -7.307803E-03   5.390730E-03  -1.954224E-02  -0.107485    
 ref    2  -5.036433E-03   1.387217E-02  -5.565597E-02   1.707395E-02  -3.884614E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.651421       0.924785       0.290213       2.950420E-02   1.347744E-02   9.433008E-03   1.803810E-03   5.943239E-04

              v      9       v     10       v     11       v     12       v     13
 ref    1   2.628773E-05   2.458410E-04   3.126647E-03   6.734192E-04   1.306208E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.80700349    -0.01330296     0.53687358    -0.04294043    -0.09656327     0.09708973     0.04224830     0.02209256
 ref:   2    -0.01288551     0.96156559     0.04448989     0.16631392    -0.06444354    -0.00256742     0.00434639     0.01030741

                ci   9         ci  10         ci  11         ci  12         ci  13
 ref:   1    -0.00096025    -0.00730780     0.00539073    -0.01954224    -0.10748517
 ref:   2    -0.00503643     0.01387217    -0.05565597     0.01707395    -0.03884614

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 27  1   -196.4725477651  9.7905E-08  0.0000E+00  1.3462E-02  1.0000E-04   
 mr-sdci # 27  2   -196.4685542637  2.2369E-08  0.0000E+00  2.0249E-02  1.0000E-04   
 mr-sdci # 27  3   -196.4675195941  2.5561E-04  0.0000E+00  1.4340E-02  1.0000E-04   
 mr-sdci # 27  4   -195.8577484839  6.7140E-03 -1.1362E-01  9.1020E-01  1.0000E-04   
 mr-sdci # 27  5   -195.7569259733  8.1504E-02  0.0000E+00  7.4606E-01  1.0000E-04   
 mr-sdci # 27  6   -195.5879641763  2.8161E-02  0.0000E+00  9.5436E-01  1.0000E-04   
 mr-sdci # 27  7   -195.5485266241  2.0260E-03  0.0000E+00  7.6470E-01  1.0000E-04   
 mr-sdci # 27  8   -195.4648291995  9.3406E-02  0.0000E+00  9.9938E-01  1.0000E-04   
 mr-sdci # 27  9   -195.3038873009  3.8066E-02  0.0000E+00  1.5214E+00  1.0000E-04   
 mr-sdci # 27 10   -195.0736992328  7.1374E-03  0.0000E+00  1.0462E+00  1.0000E-04   
 mr-sdci # 27 11   -194.1187641137  1.4446E-02  0.0000E+00  3.5397E+00  1.0000E-04   
 mr-sdci # 27 12   -192.8173592551  8.2294E-01  0.0000E+00  3.7829E+00  1.0000E-04   
 mr-sdci # 27 13   -191.6891358034 -2.3186E+00  0.0000E+00  4.7148E+00  1.0000E-04   
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  28

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58409296
   ht   2     0.00000000   -50.58005354
   ht   3     0.00000000     0.00000000   -50.57890378
   ht   4     0.00000000     0.00000000     0.00000000   -49.89772617
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -49.66638398
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.47893341
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.30874196
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.12749316
   ht   9    -1.91627213    11.19840277    -5.71307214  -141.98610433   -97.38784706   -45.75185425    51.33474507   -59.31286804
   ht  10     4.14922092    -0.32182783    -0.40260112    21.34577857     7.97319842    10.08980014    -7.23483202     8.77873868
   ht  11    -0.45792418    -0.01722527    -0.03741476    -0.00727284    -0.01880216     0.01093477    -0.02831063     0.00263267
   ht  12    -0.10983652    -0.36698529     0.02772039    -0.01518053     0.02712690     0.00299610     0.15458759    -0.11335803
   ht  13    -0.06755373    -0.06753329     0.27596638     0.01994001     0.01159858    -0.02671456    -0.01481442     0.01023703
   ht  14     6.07083737     2.43566373     0.07164182    28.29418034    -6.61435816     3.66032616     8.29115159   -13.46199641

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14
   ht   9 -9673.84444996
   ht  10   175.82436505  -335.92984826
   ht  11    -0.12339759     0.06954688    -0.00784069
   ht  12    -0.11028885     0.11129722    -0.00003663    -0.01146323
   ht  13    -0.35780091     0.28825116    -0.00005677    -0.00050036    -0.00635366
   ht  14     6.23872026    -0.86104826     0.00963338    -0.10098948    -0.04196389   -56.41893991

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.806996      -1.284908E-02   0.536893      -3.423377E-02  -7.022906E-02  -9.679377E-02  -6.541325E-02   4.988820E-02
 ref    2  -1.289582E-02   0.961642       4.379112E-02   0.137018      -0.105007       4.665186E-02  -7.697980E-03   8.243655E-03

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1   2.446094E-02   1.707193E-03  -6.595919E-03   5.021290E-03   1.870659E-02  -0.112303    
 ref    2   1.348059E-02   5.616121E-03   1.511400E-02  -5.570960E-02  -1.729881E-02  -4.278045E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.651409       0.924920       0.290171       1.994601E-02   1.595851E-02   1.154543E-02   4.338152E-03   2.556790E-03

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1   7.800639E-04   3.445532E-05   2.719390E-04   3.128773E-03   6.491850E-04   1.444215E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.80699591    -0.01284908     0.53689260    -0.03423377    -0.07022906    -0.09679377    -0.06541325     0.04988820
 ref:   2    -0.01289582     0.96164158     0.04379112     0.13701847    -0.10500659     0.04665186    -0.00769798     0.00824365

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14
 ref:   1     0.02446094     0.00170719    -0.00659592     0.00502129     0.01870659    -0.11230308
 ref:   2     0.01348059     0.00561612     0.01511400    -0.05570960    -0.01729881    -0.04278045

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 28  1   -196.4725477674  2.2367E-09  0.0000E+00  1.3466E-02  1.0000E-04   
 mr-sdci # 28  2   -196.4685545804  3.1676E-07  0.0000E+00  1.9437E-02  1.0000E-04   
 mr-sdci # 28  3   -196.4675219616  2.3675E-06  0.0000E+00  1.5365E-02  1.0000E-04   
 mr-sdci # 28  4   -195.9624325842  1.0468E-01  0.0000E+00  1.6878E+00  1.0000E-04   
 mr-sdci # 28  5   -195.7656528677  8.7269E-03 -1.0090E-01  8.4279E-01  1.0000E-04   
 mr-sdci # 28  6   -195.6767231275  8.8759E-02  0.0000E+00  1.2618E+00  1.0000E-04   
 mr-sdci # 28  7   -195.5729679634  2.4441E-02  0.0000E+00  1.0584E+00  1.0000E-04   
 mr-sdci # 28  8   -195.5459052351  8.1076E-02  0.0000E+00  8.0512E-01  1.0000E-04   
 mr-sdci # 28  9   -195.4590028896  1.5512E-01  0.0000E+00  1.0127E+00  1.0000E-04   
 mr-sdci # 28 10   -195.3015900726  2.2789E-01  0.0000E+00  1.5801E+00  1.0000E-04   
 mr-sdci # 28 11   -195.0277046091  9.0894E-01  0.0000E+00  1.0524E+00  1.0000E-04   
 mr-sdci # 28 12   -194.1170775374  1.2997E+00  0.0000E+00  3.5208E+00  1.0000E-04   
 mr-sdci # 28 13   -192.8167574238  1.1276E+00  0.0000E+00  3.7905E+00  1.0000E-04   
 mr-sdci # 28 14   -191.4319017735 -1.9821E+00  0.0000E+00  4.7436E+00  1.0000E-04   
 
 root number  5 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.009000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  29

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58409296
   ht   2     0.00000000   -50.58005354
   ht   3     0.00000000     0.00000000   -50.57890378
   ht   4     0.00000000     0.00000000     0.00000000   -49.89772617
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -49.66638398
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.47893341
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.30874196
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.12749316
   ht   9    -1.91627213    11.19840277    -5.71307214  -141.98610433   -97.38784706   -45.75185425    51.33474507   -59.31286804
   ht  10     4.14922092    -0.32182783    -0.40260112    21.34577857     7.97319842    10.08980014    -7.23483202     8.77873868
   ht  11    -0.45792418    -0.01722527    -0.03741476    -0.00727284    -0.01880216     0.01093477    -0.02831063     0.00263267
   ht  12    -0.10983652    -0.36698529     0.02772039    -0.01518053     0.02712690     0.00299610     0.15458759    -0.11335803
   ht  13    -0.06755373    -0.06753329     0.27596638     0.01994001     0.01159858    -0.02671456    -0.01481442     0.01023703
   ht  14     6.07083737     2.43566373     0.07164182    28.29418034    -6.61435816     3.66032616     8.29115159   -13.46199641
   ht  15     3.21286561    -5.50259650     3.74333171    -3.42714660   -16.30753730     2.95109756    -7.88814003     5.18228486

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15
   ht   9 -9673.84444996
   ht  10   175.82436505  -335.92984826
   ht  11    -0.12339759     0.06954688    -0.00784069
   ht  12    -0.11028885     0.11129722    -0.00003663    -0.01146323
   ht  13    -0.35780091     0.28825116    -0.00005677    -0.00050036    -0.00635366
   ht  14     6.23872026    -0.86104826     0.00963338    -0.10098948    -0.04196389   -56.41893991
   ht  15    23.12876173   -18.43518859    -0.08138505     0.19615122     0.00527094     6.28077666   -36.60179931

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.807433      -1.446262E-02  -0.536167       4.867393E-02   8.947268E-03   9.431479E-03   0.125384       6.035624E-02
 ref    2   1.338604E-02   0.961474      -4.745413E-02  -6.796610E-02   0.135423      -9.348526E-02   1.101962E-02   7.685396E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1  -7.909265E-03  -3.144262E-02   1.349518E-03  -6.872568E-03   6.572152E-03  -1.685134E-02   0.117815    
 ref    2   6.298895E-03  -2.146070E-02   5.346817E-03   1.486491E-02  -5.534326E-02   2.102665E-02   4.037181E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.652127       0.924641       0.289727       6.988542E-03   1.841941E-02   8.828446E-03   1.584261E-02   3.701941E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1   1.022325E-04   1.449200E-03   3.040965E-05   2.681977E-04   3.106069E-03   7.260877E-04   1.551027E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.80743280    -0.01446262    -0.53616696     0.04867393     0.00894727     0.00943148     0.12538413     0.06035624
 ref:   2     0.01338604     0.96147356    -0.04745413    -0.06796610     0.13542288    -0.09348526     0.01101962     0.00768540

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15
 ref:   1    -0.00790926    -0.03144262     0.00134952    -0.00687257     0.00657215    -0.01685134     0.11781506
 ref:   2     0.00629889    -0.02146070     0.00534682     0.01486491    -0.05534326     0.02102665     0.04037181

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 29  1   -196.4725499842  2.2168E-06  0.0000E+00  1.2981E-02  1.0000E-04   
 mr-sdci # 29  2   -196.4685566314  2.0510E-06  0.0000E+00  1.8142E-02  1.0000E-04   
 mr-sdci # 29  3   -196.4675295288  7.5672E-06  0.0000E+00  1.9058E-02  1.0000E-04   
 mr-sdci # 29  4   -195.9922443394  2.9812E-02  0.0000E+00  1.6681E+00  1.0000E-04   
 mr-sdci # 29  5   -195.9324296843  1.6678E-01  0.0000E+00  1.7680E+00  1.0000E-04   
 mr-sdci # 29  6   -195.6995876122  2.2864E-02  5.9638E-02  1.2717E+00  1.0000E-04   
 mr-sdci # 29  7   -195.6468383889  7.3870E-02  0.0000E+00  9.1941E-01  1.0000E-04   
 mr-sdci # 29  8   -195.5475334943  1.6283E-03  0.0000E+00  9.2710E-01  1.0000E-04   
 mr-sdci # 29  9   -195.5189974627  5.9995E-02  0.0000E+00  1.1644E+00  1.0000E-04   
 mr-sdci # 29 10   -195.4339915723  1.3240E-01  0.0000E+00  1.0102E+00  1.0000E-04   
 mr-sdci # 29 11   -195.3015615755  2.7386E-01  0.0000E+00  1.5780E+00  1.0000E-04   
 mr-sdci # 29 12   -195.0276269609  9.1055E-01  0.0000E+00  1.0528E+00  1.0000E-04   
 mr-sdci # 29 13   -194.0602330123  1.2435E+00  0.0000E+00  3.5459E+00  1.0000E-04   
 mr-sdci # 29 14   -192.7778211780  1.3459E+00  0.0000E+00  3.8418E+00  1.0000E-04   
 mr-sdci # 29 15   -191.3202693788 -1.5842E+00  0.0000E+00  4.7740E+00  1.0000E-04   
 
 root number  6 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.009000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  30

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58409296
   ht   2     0.00000000   -50.58005354
   ht   3     0.00000000     0.00000000   -50.57890378
   ht   4     0.00000000     0.00000000     0.00000000   -49.89772617
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -49.66638398
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.47893341
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.30874196
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.12749316
   ht   9    -1.91627213    11.19840277    -5.71307214  -141.98610433   -97.38784706   -45.75185425    51.33474507   -59.31286804
   ht  10     4.14922092    -0.32182783    -0.40260112    21.34577857     7.97319842    10.08980014    -7.23483202     8.77873868
   ht  11    -0.45792418    -0.01722527    -0.03741476    -0.00727284    -0.01880216     0.01093477    -0.02831063     0.00263267
   ht  12    -0.10983652    -0.36698529     0.02772039    -0.01518053     0.02712690     0.00299610     0.15458759    -0.11335803
   ht  13    -0.06755373    -0.06753329     0.27596638     0.01994001     0.01159858    -0.02671456    -0.01481442     0.01023703
   ht  14     6.07083737     2.43566373     0.07164182    28.29418034    -6.61435816     3.66032616     8.29115159   -13.46199641
   ht  15     3.21286561    -5.50259650     3.74333171    -3.42714660   -16.30753730     2.95109756    -7.88814003     5.18228486
   ht  16     3.69234004    -2.48022723     1.08034364     3.97853924    -0.68718260    -3.18100986   -19.00227965     3.54062324

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15         ht  16
   ht   9 -9673.84444996
   ht  10   175.82436505  -335.92984826
   ht  11    -0.12339759     0.06954688    -0.00784069
   ht  12    -0.11028885     0.11129722    -0.00003663    -0.01146323
   ht  13    -0.35780091     0.28825116    -0.00005677    -0.00050036    -0.00635366
   ht  14     6.23872026    -0.86104826     0.00963338    -0.10098948    -0.04196389   -56.41893991
   ht  15    23.12876173   -18.43518859    -0.08138505     0.19615122     0.00527094     6.28077666   -36.60179931
   ht  16    -1.28919835    -3.17262742    -0.09752682     0.35083866    -0.04422377    -6.79475252   -10.92121171   -36.30769857

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.807353       1.461692E-02   0.536282       3.092165E-02   3.207460E-02  -2.592409E-02   0.125227       6.046285E-02
 ref    2  -1.341440E-02  -0.961443       4.767353E-02  -0.110888       9.087343E-02   0.110105       1.632617E-02   7.401451E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1  -1.531450E-03   1.190082E-02   2.984146E-02   8.977450E-03   1.268637E-02   9.761510E-03  -9.428920E-03  -0.117295    
 ref    2   6.580425E-03  -2.792924E-03   2.242124E-02  -1.455026E-02  -3.453155E-03   6.774899E-02   4.019652E-02  -3.136518E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.651998       0.924586       0.289871       1.325236E-02   9.286760E-03   1.279526E-02   1.594825E-02   3.710537E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   4.564733E-05   1.494299E-04   1.393225E-03   2.923047E-04   1.728682E-04   4.685213E-03   1.704664E-03   1.474197E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15        ev   16

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.80735275     0.01461692     0.53628150     0.03092165     0.03207460    -0.02592409     0.12522662     0.06046285
 ref:   2    -0.01341440    -0.96144299     0.04767353    -0.11088830     0.09087343     0.11010543     0.01632617     0.00740145

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1    -0.00153145     0.01190082     0.02984146     0.00897745     0.01268637     0.00976151    -0.00942892    -0.11729535
 ref:   2     0.00658042    -0.00279292     0.02242124    -0.01455026    -0.00345316     0.06774899     0.04019652    -0.03136518

 trial vector basis is being transformed.  new dimension:   8

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 30  1   -196.4725512234  1.2392E-06  3.5745E-05  1.2551E-02  1.0000E-04   
 mr-sdci # 30  2   -196.4685567060  7.4561E-08  0.0000E+00  1.8296E-02  1.0000E-04   
 mr-sdci # 30  3   -196.4675299489  4.2012E-07  0.0000E+00  1.9474E-02  1.0000E-04   
 mr-sdci # 30  4   -196.0561829396  6.3939E-02  0.0000E+00  1.6536E+00  1.0000E-04   
 mr-sdci # 30  5   -195.9496416383  1.7212E-02  0.0000E+00  1.7655E+00  1.0000E-04   
 mr-sdci # 30  6   -195.7216427688  2.2055E-02  0.0000E+00  7.4785E-01  1.0000E-04   
 mr-sdci # 30  7   -195.6481256272  1.2872E-03  0.0000E+00  8.6511E-01  1.0000E-04   
 mr-sdci # 30  8   -195.5475800906  4.6596E-05  0.0000E+00  9.2290E-01  1.0000E-04   
 mr-sdci # 30  9   -195.5306851473  1.1688E-02  0.0000E+00  1.4604E+00  1.0000E-04   
 mr-sdci # 30 10   -195.5032920942  6.9301E-02  0.0000E+00  1.6880E+00  1.0000E-04   
 mr-sdci # 30 11   -195.4310786779  1.2952E-01  0.0000E+00  1.0477E+00  1.0000E-04   
 mr-sdci # 30 12   -195.0358950067  8.2680E-03  0.0000E+00  1.1108E+00  1.0000E-04   
 mr-sdci # 30 13   -194.8155009149  7.5527E-01  0.0000E+00  2.5753E+00  1.0000E-04   
 mr-sdci # 30 14   -193.6030395427  8.2522E-01  0.0000E+00  3.6476E+00  1.0000E-04   
 mr-sdci # 30 15   -192.6939176061  1.3736E+00  0.0000E+00  3.8997E+00  1.0000E-04   
 mr-sdci # 30 16   -191.2740372093 -7.9505E-01  0.0000E+00  4.7991E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.014000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  31

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58423643
   ht   2     0.00000000   -50.58024191
   ht   3     0.00000000     0.00000000   -50.57921515
   ht   4     0.00000000     0.00000000     0.00000000   -50.16786814
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.06132684
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.83332797
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.75981083
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.65926529
   ht   9     0.17293586    -0.00248446    -0.01593332    -0.00733993    -0.00146880    -0.00392220     0.02528209     0.03776973

                ht   9
   ht   9    -0.00118094

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.806722      -1.561730E-02  -0.536788       2.524911E-02   2.962272E-02  -3.859091E-02  -0.143637      -1.936248E-02
 ref    2  -1.261920E-02   0.961428      -4.807952E-02  -0.110081       9.180943E-02   0.108831      -2.411128E-02  -2.388856E-03

              v      9
 ref    1  -5.552474E-02
 ref    2   7.973103E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.650960       0.924587       0.290453       1.275532E-02   9.306478E-03   1.333355E-02   2.121290E-02   3.806122E-04

              v      9
 ref    1   3.146567E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.80672212    -0.01561730    -0.53678837     0.02524911     0.02962272    -0.03859091    -0.14363686    -0.01936248
 ref:   2    -0.01261920     0.96142782    -0.04807952    -0.11008090     0.09180943     0.10883149    -0.02411128    -0.00238886

                ci   9
 ref:   1    -0.05552474
 ref:   2     0.00797310

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 31  1   -196.4725759020  2.4679E-05  0.0000E+00  5.6515E-03  1.0000E-04   
 mr-sdci # 31  2   -196.4685572360  5.3004E-07  1.0951E-04  1.8247E-02  1.0000E-04   
 mr-sdci # 31  3   -196.4675303076  3.5868E-07  0.0000E+00  1.9549E-02  1.0000E-04   
 mr-sdci # 31  4   -196.0590100523  2.8271E-03  0.0000E+00  1.6500E+00  1.0000E-04   
 mr-sdci # 31  5   -195.9499104721  2.6883E-04  0.0000E+00  1.7694E+00  1.0000E-04   
 mr-sdci # 31  6   -195.7224689472  8.2618E-04  0.0000E+00  7.2798E-01  1.0000E-04   
 mr-sdci # 31  7   -195.6717507180  2.3625E-02  0.0000E+00  7.2190E-01  1.0000E-04   
 mr-sdci # 31  8   -195.5895405681  4.1960E-02  0.0000E+00  8.1535E-01  1.0000E-04   
 mr-sdci # 31  9   -191.7017892643 -3.8289E+00  0.0000E+00  3.9990E+00  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  32

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58423643
   ht   2     0.00000000   -50.58024191
   ht   3     0.00000000     0.00000000   -50.57921515
   ht   4     0.00000000     0.00000000     0.00000000   -50.16786814
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.06132684
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.83332797
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.75981083
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.65926529
   ht   9     0.17293586    -0.00248446    -0.01593332    -0.00733993    -0.00146880    -0.00392220     0.02528209     0.03776973
   ht  10     0.01515303    -0.17722388    -0.01718115    -0.18136111     0.23400723    -0.10973324     0.01587644    -0.04220973

                ht   9         ht  10
   ht   9    -0.00118094
   ht  10    -0.00011799    -0.00796786

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.806704       1.969554E-02   0.536676      -5.126774E-03  -3.728008E-02   2.849450E-02  -0.146470      -1.838968E-02
 ref    2  -1.226810E-02  -0.958201       5.512039E-02   0.159306       7.540151E-03  -0.122545      -2.284494E-02  -3.347765E-03

              v      9       v     10
 ref    1   4.108639E-03   5.589591E-02
 ref    2  -1.431902E-02  -1.002377E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.650921       0.918536       0.291059       2.540453E-02   1.446658E-03   1.582918E-02   2.197525E-02   3.493879E-04

              v      9       v     10
 ref    1   2.219152E-04   3.224829E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.80670367     0.01969554     0.53667567    -0.00512677    -0.03728008     0.02849450    -0.14646964    -0.01838968
 ref:   2    -0.01226810    -0.95820064     0.05512039     0.15930551     0.00754015    -0.12254488    -0.02284494    -0.00334777

                ci   9         ci  10
 ref:   1     0.00410864     0.05589591
 ref:   2    -0.01431902    -0.01002377

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 32  1   -196.4725759266  2.4530E-08  0.0000E+00  5.6478E-03  1.0000E-04   
 mr-sdci # 32  2   -196.4686656062  1.0837E-04  0.0000E+00  2.6777E-02  1.0000E-04   
 mr-sdci # 32  3   -196.4675308532  5.4557E-07  1.0304E-04  1.9576E-02  1.0000E-04   
 mr-sdci # 32  4   -196.2836678942  2.2466E-01  0.0000E+00  1.1171E+00  1.0000E-04   
 mr-sdci # 32  5   -196.0101099763  6.0200E-02  0.0000E+00  1.7098E+00  1.0000E-04   
 mr-sdci # 32  6   -195.7285150209  6.0461E-03  0.0000E+00  6.7375E-01  1.0000E-04   
 mr-sdci # 32  7   -195.6731447903  1.3941E-03  0.0000E+00  6.7903E-01  1.0000E-04   
 mr-sdci # 32  8   -195.5905380396  9.9747E-04  0.0000E+00  8.1636E-01  1.0000E-04   
 mr-sdci # 32  9   -193.9167018066  2.2149E+00  0.0000E+00  3.7631E+00  1.0000E-04   
 mr-sdci # 32 10   -191.6933631354 -3.8099E+00  0.0000E+00  3.9980E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.005000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  33

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58423643
   ht   2     0.00000000   -50.58024191
   ht   3     0.00000000     0.00000000   -50.57921515
   ht   4     0.00000000     0.00000000     0.00000000   -50.16786814
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.06132684
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.83332797
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.75981083
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.65926529
   ht   9     0.17293586    -0.00248446    -0.01593332    -0.00733993    -0.00146880    -0.00392220     0.02528209     0.03776973
   ht  10     0.01515303    -0.17722388    -0.01718115    -0.18136111     0.23400723    -0.10973324     0.01587644    -0.04220973
   ht  11     0.00729752    -0.09720954     0.21897549     0.23926615     0.22168880     0.01709820    -0.05019697    -0.00130827

                ht   9         ht  10         ht  11
   ht   9    -0.00118094
   ht  10    -0.00011799    -0.00796786
   ht  11     0.00008051    -0.00049301    -0.00593826

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.806776      -2.512310E-02   0.535739      -2.190882E-02  -3.530847E-02   3.388535E-02  -0.146191       2.183457E-02
 ref    2  -1.227402E-02   0.957521       6.553636E-02   0.147313      -5.866871E-02  -0.122643      -2.489866E-02   4.064754E-03

              v      9       v     10       v     11
 ref    1  -5.755727E-03  -3.380835E-02   4.755296E-02
 ref    2   1.261057E-02  -2.479568E-02  -1.820176E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.651039       0.917477       0.291311       2.218097E-02   4.688706E-03   1.618944E-02   2.199185E-02   4.932706E-04

              v      9       v     10       v     11
 ref    1   1.921549E-04   1.757830E-03   2.592588E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.80677623    -0.02512310     0.53573852    -0.02190882    -0.03530847     0.03388535    -0.14619135     0.02183457
 ref:   2    -0.01227402     0.95752062     0.06553636     0.14731250    -0.05866871    -0.12264266    -0.02489866     0.00406475

                ci   9         ci  10         ci  11
 ref:   1    -0.00575573    -0.03380835     0.04755296
 ref:   2     0.01261057    -0.02479568    -0.01820176

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 33  1   -196.4725759313  4.7359E-09  0.0000E+00  5.6517E-03  1.0000E-04   
 mr-sdci # 33  2   -196.4686667706  1.1644E-06  0.0000E+00  2.7011E-02  1.0000E-04   
 mr-sdci # 33  3   -196.4676366554  1.0580E-04  0.0000E+00  2.5308E-02  1.0000E-04   
 mr-sdci # 33  4   -196.2973179787  1.3650E-02  2.3738E-01  1.0883E+00  1.0000E-04   
 mr-sdci # 33  5   -196.2186255316  2.0852E-01  0.0000E+00  1.2859E+00  1.0000E-04   
 mr-sdci # 33  6   -195.7292880318  7.7301E-04  0.0000E+00  6.6372E-01  1.0000E-04   
 mr-sdci # 33  7   -195.6771892461  4.0445E-03  0.0000E+00  5.6668E-01  1.0000E-04   
 mr-sdci # 33  8   -195.5916411500  1.1031E-03  0.0000E+00  7.8132E-01  1.0000E-04   
 mr-sdci # 33  9   -193.9203370912  3.6353E-03  0.0000E+00  3.7478E+00  1.0000E-04   
 mr-sdci # 33 10   -192.9552444873  1.2619E+00  0.0000E+00  3.6839E+00  1.0000E-04   
 mr-sdci # 33 11   -191.5559533090 -3.8751E+00  0.0000E+00  3.8577E+00  1.0000E-04   
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.005000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  34

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58423643
   ht   2     0.00000000   -50.58024191
   ht   3     0.00000000     0.00000000   -50.57921515
   ht   4     0.00000000     0.00000000     0.00000000   -50.16786814
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.06132684
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.83332797
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.75981083
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.65926529
   ht   9     0.17293586    -0.00248446    -0.01593332    -0.00733993    -0.00146880    -0.00392220     0.02528209     0.03776973
   ht  10     0.01515303    -0.17722388    -0.01718115    -0.18136111     0.23400723    -0.10973324     0.01587644    -0.04220973
   ht  11     0.00729752    -0.09720954     0.21897549     0.23926615     0.22168880     0.01709820    -0.05019697    -0.00130827
   ht  12    -2.13663658   -23.46421839   -11.84677630    -4.25774897    -6.38601550     4.55909637    -0.23946045     1.25559726

                ht   9         ht  10         ht  11         ht  12
   ht   9    -0.00118094
   ht  10    -0.00011799    -0.00796786
   ht  11     0.00008051    -0.00049301    -0.00593826
   ht  12     0.00250751    -0.03894159     0.06794181   -20.46597205

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.806645       7.217936E-02   0.530987       3.088902E-02  -3.700055E-02   3.573476E-02   0.145781       2.361164E-02
 ref    2  -1.181893E-02  -0.939152       0.154509      -0.177327      -4.659938E-02  -0.136606       2.548826E-02   6.340589E-03

              v      9       v     10       v     11       v     12
 ref    1   9.530313E-04  -3.035577E-02   4.528336E-02  -3.566146E-02
 ref    2  -2.442163E-02  -3.784371E-02  -0.118610      -1.834348E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.650815       0.887217       0.305821       3.239906E-02   3.540543E-03   1.993816E-02   2.190168E-02   5.977124E-04

              v      9       v     10       v     11       v     12
 ref    1   5.973243E-04   2.353619E-03   1.611899E-02   1.608223E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.80664472     0.07217936     0.53098747     0.03088902    -0.03700055     0.03573476     0.14578077     0.02361164
 ref:   2    -0.01181893    -0.93915247     0.15450936    -0.17732718    -0.04659938    -0.13660596     0.02548826     0.00634059

                ci   9         ci  10         ci  11         ci  12
 ref:   1     0.00095303    -0.03035577     0.04528336    -0.03566146
 ref:   2    -0.02442163    -0.03784371    -0.11861033    -0.01834348

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 34  1   -196.4725759551  2.3766E-08  0.0000E+00  5.6688E-03  1.0000E-04   
 mr-sdci # 34  2   -196.4688810622  2.1429E-04  0.0000E+00  5.1338E-02  1.0000E-04   
 mr-sdci # 34  3   -196.4676826805  4.6025E-05  0.0000E+00  2.8893E-02  1.0000E-04   
 mr-sdci # 34  4   -196.4245615378  1.2724E-01  0.0000E+00  5.5620E-01  1.0000E-04   
 mr-sdci # 34  5   -196.2193213892  6.9586E-04  4.5398E-01  1.2877E+00  1.0000E-04   
 mr-sdci # 34  6   -195.7401860337  1.0898E-02  0.0000E+00  5.0688E-01  1.0000E-04   
 mr-sdci # 34  7   -195.6772221711  3.2925E-05  0.0000E+00  5.6975E-01  1.0000E-04   
 mr-sdci # 34  8   -195.6003140529  8.6729E-03  0.0000E+00  7.2471E-01  1.0000E-04   
 mr-sdci # 34  9   -194.0905043351  1.7017E-01  0.0000E+00  3.5912E+00  1.0000E-04   
 mr-sdci # 34 10   -192.9700147717  1.4770E-02  0.0000E+00  3.7129E+00  1.0000E-04   
 mr-sdci # 34 11   -191.6487711429  9.2818E-02  0.0000E+00  4.3847E+00  1.0000E-04   
 mr-sdci # 34 12   -191.5467168660 -3.4892E+00  0.0000E+00  3.8678E+00  1.0000E-04   
 
 root number  5 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  35

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58423643
   ht   2     0.00000000   -50.58024191
   ht   3     0.00000000     0.00000000   -50.57921515
   ht   4     0.00000000     0.00000000     0.00000000   -50.16786814
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.06132684
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.83332797
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.75981083
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.65926529
   ht   9     0.17293586    -0.00248446    -0.01593332    -0.00733993    -0.00146880    -0.00392220     0.02528209     0.03776973
   ht  10     0.01515303    -0.17722388    -0.01718115    -0.18136111     0.23400723    -0.10973324     0.01587644    -0.04220973
   ht  11     0.00729752    -0.09720954     0.21897549     0.23926615     0.22168880     0.01709820    -0.05019697    -0.00130827
   ht  12    -2.13663658   -23.46421839   -11.84677630    -4.25774897    -6.38601550     4.55909637    -0.23946045     1.25559726
   ht  13    17.42973895    61.80014764   -45.34986550    13.75528929     6.06280982   -11.42741189    -6.08695926     1.56152826

                ht   9         ht  10         ht  11         ht  12         ht  13
   ht   9    -0.00118094
   ht  10    -0.00011799    -0.00796786
   ht  11     0.00008051    -0.00049301    -0.00593826
   ht  12     0.00250751    -0.03894159     0.06794181   -20.46597205
   ht  13    -0.08174430     0.07099840     0.20027024    24.44423261  -150.76339885

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.806372      -0.108671       0.522237      -6.158512E-02   3.275281E-02   2.094377E-02  -0.148917      -2.945224E-02
 ref    2  -1.175569E-02   0.924124       0.225361       0.166014       7.886995E-02  -0.142973      -1.665582E-02  -9.551212E-03

              v      9       v     10       v     11       v     12       v     13
 ref    1  -7.293891E-03   2.148304E-03   6.500307E-02   2.866601E-02   2.555113E-02
 ref    2  -3.413237E-02   1.596625E-02  -7.372487E-02   4.753471E-02   0.128797    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.650373       0.865814       0.323519       3.135353E-02   7.293216E-03   2.088004E-02   2.245372E-02   9.586599E-04

              v      9       v     10       v     11       v     12       v     13
 ref    1   1.218220E-03   2.595364E-04   9.660755E-03   3.081289E-03   1.724155E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.80637159    -0.10867089     0.52223715    -0.06158512     0.03275281     0.02094377    -0.14891710    -0.02945224
 ref:   2    -0.01175569     0.92412387     0.22536052     0.16601448     0.07886995    -0.14297341    -0.01665582    -0.00955121

                ci   9         ci  10         ci  11         ci  12         ci  13
 ref:   1    -0.00729389     0.00214830     0.06500307     0.02866601     0.02555113
 ref:   2    -0.03413237     0.01596625    -0.07372487     0.04753471     0.12879710

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 35  1   -196.4725759827  2.7611E-08  0.0000E+00  5.6120E-03  1.0000E-04   
 mr-sdci # 35  2   -196.4689063381  2.5276E-05  0.0000E+00  5.1083E-02  1.0000E-04   
 mr-sdci # 35  3   -196.4679164315  2.3375E-04  0.0000E+00  4.4769E-02  1.0000E-04   
 mr-sdci # 35  4   -196.4335585853  8.9970E-03  0.0000E+00  4.5031E-01  1.0000E-04   
 mr-sdci # 35  5   -196.3766110730  1.5729E-01  0.0000E+00  8.4506E-01  1.0000E-04   
 mr-sdci # 35  6   -195.7469817961  6.7958E-03  7.8795E-04  4.8468E-01  1.0000E-04   
 mr-sdci # 35  7   -195.6818179621  4.5958E-03  0.0000E+00  4.5983E-01  1.0000E-04   
 mr-sdci # 35  8   -195.6054734876  5.1594E-03  0.0000E+00  6.3863E-01  1.0000E-04   
 mr-sdci # 35  9   -194.4104980472  3.1999E-01  0.0000E+00  3.3796E+00  1.0000E-04   
 mr-sdci # 35 10   -193.6948451264  7.2483E-01  0.0000E+00  3.4438E+00  1.0000E-04   
 mr-sdci # 35 11   -191.7641380720  1.1537E-01  0.0000E+00  4.1897E+00  1.0000E-04   
 mr-sdci # 35 12   -191.5535031595  6.7863E-03  0.0000E+00  3.9482E+00  1.0000E-04   
 mr-sdci # 35 13   -190.5840027247 -4.2315E+00  0.0000E+00  4.4842E+00  1.0000E-04   
 
 root number  6 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.008000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  36

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58423643
   ht   2     0.00000000   -50.58024191
   ht   3     0.00000000     0.00000000   -50.57921515
   ht   4     0.00000000     0.00000000     0.00000000   -50.16786814
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.06132684
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.83332797
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.75981083
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.65926529
   ht   9     0.17293586    -0.00248446    -0.01593332    -0.00733993    -0.00146880    -0.00392220     0.02528209     0.03776973
   ht  10     0.01515303    -0.17722388    -0.01718115    -0.18136111     0.23400723    -0.10973324     0.01587644    -0.04220973
   ht  11     0.00729752    -0.09720954     0.21897549     0.23926615     0.22168880     0.01709820    -0.05019697    -0.00130827
   ht  12    -2.13663658   -23.46421839   -11.84677630    -4.25774897    -6.38601550     4.55909637    -0.23946045     1.25559726
   ht  13    17.42973895    61.80014764   -45.34986550    13.75528929     6.06280982   -11.42741189    -6.08695926     1.56152826
   ht  14    -0.56178899    -2.35082399    -0.15755498     7.23668692    -9.71888454    -0.72115456     1.35228125     0.08477600

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14
   ht   9    -0.00118094
   ht  10    -0.00011799    -0.00796786
   ht  11     0.00008051    -0.00049301    -0.00593826
   ht  12     0.00250751    -0.03894159     0.06794181   -20.46597205
   ht  13    -0.08174430     0.07099840     0.20027024    24.44423261  -150.76339885
   ht  14     0.00186301    -0.04552337     0.00046058    -0.79225499    -3.12643767   -10.47792243

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.806477      -0.111197       0.521367       6.255912E-02   3.362998E-02   2.063926E-02  -0.148728       2.746968E-02
 ref    2   1.233385E-02   0.921044       0.231806      -0.174947       7.691713E-02  -0.142966      -1.593622E-02   8.654865E-03

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1   1.398888E-02  -6.301312E-03   2.364462E-03   6.497460E-02   2.866517E-02   2.589206E-02
 ref    2   1.101977E-02  -3.082507E-02   1.725742E-02  -7.390074E-02   4.753996E-02   0.129329    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.650558       0.860686       0.325558       3.452009E-02   7.047221E-03   2.086512E-02   2.237402E-02   8.294899E-04

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1   3.171242E-04   9.898914E-04   3.034092E-04   9.683018E-03   3.081740E-03   1.739647E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.80647728    -0.11119664     0.52136688     0.06255912     0.03362998     0.02063926    -0.14872813     0.02746968
 ref:   2     0.01233385     0.92104379     0.23180635    -0.17494698     0.07691713    -0.14296553    -0.01593622     0.00865486

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14
 ref:   1     0.01398888    -0.00630131     0.00236446     0.06497460     0.02866517     0.02589206
 ref:   2     0.01101977    -0.03082507     0.01725742    -0.07390074     0.04753996     0.12932930

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 36  1   -196.4725761424  1.5968E-07  9.4805E-06  5.5765E-03  1.0000E-04   
 mr-sdci # 36  2   -196.4689517306  4.5393E-05  0.0000E+00  4.8869E-02  1.0000E-04   
 mr-sdci # 36  3   -196.4679174529  1.0214E-06  0.0000E+00  4.4332E-02  1.0000E-04   
 mr-sdci # 36  4   -196.4350681002  1.5095E-03  0.0000E+00  4.1559E-01  1.0000E-04   
 mr-sdci # 36  5   -196.3782225436  1.6115E-03  0.0000E+00  8.3054E-01  1.0000E-04   
 mr-sdci # 36  6   -195.7470065582  2.4762E-05  0.0000E+00  4.6848E-01  1.0000E-04   
 mr-sdci # 36  7   -195.6827197644  9.0180E-04  0.0000E+00  4.8160E-01  1.0000E-04   
 mr-sdci # 36  8   -195.6060255380  5.5205E-04  0.0000E+00  6.1253E-01  1.0000E-04   
 mr-sdci # 36  9   -195.5154984972  1.1050E+00  0.0000E+00  1.9777E+00  1.0000E-04   
 mr-sdci # 36 10   -194.2426128486  5.4777E-01  0.0000E+00  3.2857E+00  1.0000E-04   
 mr-sdci # 36 11   -193.6876474266  1.9235E+00  0.0000E+00  3.4576E+00  1.0000E-04   
 mr-sdci # 36 12   -191.7638113940  2.1031E-01  0.0000E+00  4.1853E+00  1.0000E-04   
 mr-sdci # 36 13   -191.5535030908  9.6950E-01  0.0000E+00  3.9482E+00  1.0000E-04   
 mr-sdci # 36 14   -190.5616097957 -3.0414E+00  0.0000E+00  4.5173E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.012000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  37

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58423643
   ht   2     0.00000000   -50.58024191
   ht   3     0.00000000     0.00000000   -50.57921515
   ht   4     0.00000000     0.00000000     0.00000000   -50.16786814
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.06132684
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.83332797
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.75981083
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.65926529
   ht   9     0.17293586    -0.00248446    -0.01593332    -0.00733993    -0.00146880    -0.00392220     0.02528209     0.03776973
   ht  10     0.01515303    -0.17722388    -0.01718115    -0.18136111     0.23400723    -0.10973324     0.01587644    -0.04220973
   ht  11     0.00729752    -0.09720954     0.21897549     0.23926615     0.22168880     0.01709820    -0.05019697    -0.00130827
   ht  12    -2.13663658   -23.46421839   -11.84677630    -4.25774897    -6.38601550     4.55909637    -0.23946045     1.25559726
   ht  13    17.42973895    61.80014764   -45.34986550    13.75528929     6.06280982   -11.42741189    -6.08695926     1.56152826
   ht  14    -0.56178899    -2.35082399    -0.15755498     7.23668692    -9.71888454    -0.72115456     1.35228125     0.08477600
   ht  15     0.11535592    -0.01049029     0.02815943    -0.02151254     0.01970282    -0.00280269     0.00731666     0.01710025

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15
   ht   9    -0.00118094
   ht  10    -0.00011799    -0.00796786
   ht  11     0.00008051    -0.00049301    -0.00593826
   ht  12     0.00250751    -0.03894159     0.06794181   -20.46597205
   ht  13    -0.08174430     0.07099840     0.20027024    24.44423261  -150.76339885
   ht  14     0.00186301    -0.04552337     0.00046058    -0.79225499    -3.12643767   -10.47792243
   ht  15    -0.00033896    -0.00012017    -0.00003832     0.00808353     0.00860677     0.01238562    -0.00051809

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.806008      -0.114039       0.521651      -6.274914E-02  -3.361085E-02  -5.501006E-02   0.118269       2.482011E-02
 ref    2   1.118383E-02   0.920165       0.234805       0.175260      -7.748956E-02   0.131637       5.957479E-02  -8.322725E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1  -4.563217E-02  -7.519128E-02  -2.274819E-02   1.962166E-02  -4.767167E-02   2.717798E-03   0.135273    
 ref    2  -1.118586E-02  -1.327670E-02   3.097695E-02   8.915934E-03   8.854333E-02   0.122081       4.558221E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.649773       0.859708       0.327253       3.465336E-02   7.134322E-03   2.035437E-02   1.753667E-02   6.853058E-04

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1   2.207418E-03   5.830000E-03   1.477052E-03   4.645036E-04   1.011251E-02   1.491118E-02   2.037665E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.80600765    -0.11403859     0.52165145    -0.06274914    -0.03361085    -0.05501006     0.11826881     0.02482011
 ref:   2     0.01118383     0.92016484     0.23480472     0.17525954    -0.07748956     0.13163686     0.05957479    -0.00832272

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15
 ref:   1    -0.04563217    -0.07519128    -0.02274819     0.01962166    -0.04767167     0.00271780     0.13527346
 ref:   2    -0.01118586    -0.01327670     0.03097695     0.00891593     0.08854333     0.12208108     0.04558221

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 37  1   -196.4725844499  8.3075E-06  0.0000E+00  2.5589E-03  1.0000E-04   
 mr-sdci # 37  2   -196.4689563121  4.5815E-06  4.7810E-04  4.8882E-02  1.0000E-04   
 mr-sdci # 37  3   -196.4679197523  2.2994E-06  0.0000E+00  4.4630E-02  1.0000E-04   
 mr-sdci # 37  4   -196.4354717972  4.0370E-04  0.0000E+00  4.1315E-01  1.0000E-04   
 mr-sdci # 37  5   -196.3783008707  7.8327E-05  0.0000E+00  8.3151E-01  1.0000E-04   
 mr-sdci # 37  6   -195.7561194707  9.1129E-03  0.0000E+00  6.1560E-01  1.0000E-04   
 mr-sdci # 37  7   -195.7135314818  3.0812E-02  0.0000E+00  4.8571E-01  1.0000E-04   
 mr-sdci # 37  8   -195.6200687913  1.4043E-02  0.0000E+00  6.4266E-01  1.0000E-04   
 mr-sdci # 37  9   -195.5320181725  1.6520E-02  0.0000E+00  1.9046E+00  1.0000E-04   
 mr-sdci # 37 10   -194.7921864694  5.4957E-01  0.0000E+00  2.0402E+00  1.0000E-04   
 mr-sdci # 37 11   -194.0270156034  3.3937E-01  0.0000E+00  3.4149E+00  1.0000E-04   
 mr-sdci # 37 12   -193.5527528478  1.7889E+00  0.0000E+00  3.6262E+00  1.0000E-04   
 mr-sdci # 37 13   -191.7137143741  1.6021E-01  0.0000E+00  4.2488E+00  1.0000E-04   
 mr-sdci # 37 14   -190.5943241904  3.2714E-02  0.0000E+00  4.4514E+00  1.0000E-04   
 mr-sdci # 37 15   -189.3671388095 -3.3268E+00  0.0000E+00  3.7324E+00  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.002000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  38

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58423643
   ht   2     0.00000000   -50.58024191
   ht   3     0.00000000     0.00000000   -50.57921515
   ht   4     0.00000000     0.00000000     0.00000000   -50.16786814
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.06132684
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.83332797
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.75981083
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.65926529
   ht   9     0.17293586    -0.00248446    -0.01593332    -0.00733993    -0.00146880    -0.00392220     0.02528209     0.03776973
   ht  10     0.01515303    -0.17722388    -0.01718115    -0.18136111     0.23400723    -0.10973324     0.01587644    -0.04220973
   ht  11     0.00729752    -0.09720954     0.21897549     0.23926615     0.22168880     0.01709820    -0.05019697    -0.00130827
   ht  12    -2.13663658   -23.46421839   -11.84677630    -4.25774897    -6.38601550     4.55909637    -0.23946045     1.25559726
   ht  13    17.42973895    61.80014764   -45.34986550    13.75528929     6.06280982   -11.42741189    -6.08695926     1.56152826
   ht  14    -0.56178899    -2.35082399    -0.15755498     7.23668692    -9.71888454    -0.72115456     1.35228125     0.08477600
   ht  15     0.11535592    -0.01049029     0.02815943    -0.02151254     0.01970282    -0.00280269     0.00731666     0.01710025
   ht  16     0.10205160    -0.47847578    -0.45032486    -0.03580240    -0.13955564     0.16636769    -0.04133524     0.09222762

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15         ht  16
   ht   9    -0.00118094
   ht  10    -0.00011799    -0.00796786
   ht  11     0.00008051    -0.00049301    -0.00593826
   ht  12     0.00250751    -0.03894159     0.06794181   -20.46597205
   ht  13    -0.08174430     0.07099840     0.20027024    24.44423261  -150.76339885
   ht  14     0.00186301    -0.04552337     0.00046058    -0.79225499    -3.12643767   -10.47792243
   ht  15    -0.00033896    -0.00012017    -0.00003832     0.00808353     0.00860677     0.01238562    -0.00051809
   ht  16    -0.00037460     0.00198634     0.00183202    -0.35759679     0.29665127     0.01250574    -0.00017519    -0.01978191

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.805544       0.183829       0.486187      -0.137937      -4.178455E-02  -5.463731E-02  -0.118444      -2.045156E-02
 ref    2  -9.596808E-03  -0.765039       0.419592       0.418443      -5.020301E-02   0.131322      -5.867242E-02   1.261399E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1  -4.726172E-02   7.210847E-02  -2.153051E-02  -2.454144E-02   4.116725E-03   2.147846E-02   0.103685      -0.103870    
 ref    2  -1.312910E-02   9.225136E-03   3.210238E-02  -2.254520E-02   3.208816E-02   9.894938E-02  -0.100077      -8.178554E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.648993       0.619078       0.412435       0.194121       4.266291E-03   2.023078E-02   1.747155E-02   5.773790E-04

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   2.406044E-03   5.284735E-03   1.494126E-03   1.110568E-03   1.046598E-03   1.025230E-02   2.076609E-02   1.747776E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15        ev   16

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.80554360     0.18382945     0.48618700    -0.13793710    -0.04178455    -0.05463731    -0.11844448    -0.02045156
 ref:   2    -0.00959681    -0.76503896     0.41959174     0.41844336    -0.05020301     0.13132230    -0.05867242     0.01261399

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1    -0.04726172     0.07210847    -0.02153051    -0.02454144     0.00411673     0.02147846     0.10368548    -0.10386958
 ref:   2    -0.01312910     0.00922514     0.03210238    -0.02254520     0.03208816     0.09894938    -0.10007702    -0.08178554

 trial vector basis is being transformed.  new dimension:   8

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 38  1   -196.4725844904  4.0557E-08  0.0000E+00  2.5464E-03  1.0000E-04   
 mr-sdci # 38  2   -196.4703057204  1.3494E-03  0.0000E+00  5.2003E-02  1.0000E-04   
 mr-sdci # 38  3   -196.4679832510  6.3499E-05  3.0951E-04  4.0482E-02  1.0000E-04   
 mr-sdci # 38  4   -196.4591759685  2.3704E-02  0.0000E+00  1.1805E-01  1.0000E-04   
 mr-sdci # 38  5   -196.3873106750  9.0098E-03  0.0000E+00  7.7118E-01  1.0000E-04   
 mr-sdci # 38  6   -195.7563647486  2.4528E-04  0.0000E+00  6.2790E-01  1.0000E-04   
 mr-sdci # 38  7   -195.7136427521  1.1127E-04  0.0000E+00  4.8519E-01  1.0000E-04   
 mr-sdci # 38  8   -195.6262000081  6.1312E-03  0.0000E+00  6.0001E-01  1.0000E-04   
 mr-sdci # 38  9   -195.5417916118  9.7734E-03  0.0000E+00  1.7919E+00  1.0000E-04   
 mr-sdci # 38 10   -194.8436955382  5.1509E-02  0.0000E+00  1.9849E+00  1.0000E-04   
 mr-sdci # 38 11   -194.0278262634  8.1066E-04  0.0000E+00  3.4192E+00  1.0000E-04   
 mr-sdci # 38 12   -193.5710836538  1.8331E-02  0.0000E+00  3.9310E+00  1.0000E-04   
 mr-sdci # 38 13   -193.4579488989  1.7442E+00  0.0000E+00  3.5991E+00  1.0000E-04   
 mr-sdci # 38 14   -190.6136546120  1.9330E-02  0.0000E+00  4.4036E+00  1.0000E-04   
 mr-sdci # 38 15   -190.1285379483  7.6140E-01  0.0000E+00  4.1118E+00  1.0000E-04   
 mr-sdci # 38 16   -189.2152700939 -2.0588E+00  0.0000E+00  3.9923E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  39

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58426969
   ht   2     0.00000000   -50.58199092
   ht   3     0.00000000     0.00000000   -50.57966846
   ht   4     0.00000000     0.00000000     0.00000000   -50.57086117
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.49899588
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.86804995
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.82532796
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.73788521
   ht   9    -0.11594754    -0.10560479     0.38005187    -0.15066042     0.08404613     0.03886074     0.00736092    -0.06597914

                ht   9
   ht   9    -0.01057282

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.804818      -0.265807       0.402414      -0.233653      -7.055689E-02  -5.658729E-02   0.117316       1.916621E-02
 ref    2  -9.311936E-03   0.629076       0.616648       0.365617      -0.171449       0.130603       6.034657E-02  -1.297627E-02

              v      9
 ref    1  -2.637846E-02
 ref    2  -2.924328E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.647819       0.466390       0.542191       0.188270       3.437303E-02   2.025914E-02   1.740479E-02   5.357272E-04

              v      9
 ref    1   1.550993E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.80481805    -0.26580696     0.40241381    -0.23365343    -0.07055689    -0.05658729     0.11731614     0.01916621
 ref:   2    -0.00931194     0.62907584     0.61664784     0.36561745    -0.17144899     0.13060253     0.06034657    -0.01297627

                ci   9
 ref:   1    -0.02637846
 ref:   2    -0.02924328

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 39  1   -196.4725845275  3.7050E-08  0.0000E+00  2.6391E-03  1.0000E-04   
 mr-sdci # 39  2   -196.4706044216  2.9870E-04  0.0000E+00  5.4491E-02  1.0000E-04   
 mr-sdci # 39  3   -196.4687046733  7.2142E-04  0.0000E+00  4.9766E-02  1.0000E-04   
 mr-sdci # 39  4   -196.4610534110  1.8774E-03  3.5658E-03  9.9412E-02  1.0000E-04   
 mr-sdci # 39  5   -196.4409415269  5.3631E-02  0.0000E+00  3.1079E-01  1.0000E-04   
 mr-sdci # 39  6   -195.7567857170  4.2097E-04  0.0000E+00  6.4768E-01  1.0000E-04   
 mr-sdci # 39  7   -195.7149339149  1.2912E-03  0.0000E+00  4.6305E-01  1.0000E-04   
 mr-sdci # 39  8   -195.6265688815  3.6887E-04  0.0000E+00  5.9636E-01  1.0000E-04   
 mr-sdci # 39  9   -193.2186099656 -2.3232E+00  0.0000E+00  4.1810E+00  1.0000E-04   
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  40

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58426969
   ht   2     0.00000000   -50.58199092
   ht   3     0.00000000     0.00000000   -50.57966846
   ht   4     0.00000000     0.00000000     0.00000000   -50.57086117
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.49899588
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.86804995
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.82532796
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.73788521
   ht   9    -0.11594754    -0.10560479     0.38005187    -0.15066042     0.08404613     0.03886074     0.00736092    -0.06597914
   ht  10     0.50252067    -0.48133736    -1.15580068    -0.12499077    -0.53045182    -0.32792970    -0.07033311    -0.17146198

                ht   9         ht  10
   ht   9    -0.01057282
   ht  10     0.00366574    -0.15579214

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.784962      -0.300290       0.350250      -0.325026      -7.170728E-02   5.891169E-02   0.117520       1.616024E-02
 ref    2   2.169710E-02   0.436475       0.715013       0.456024      -0.169263      -0.130519       6.013083E-02  -1.698800E-02

              v      9       v     10
 ref    1   8.050497E-04   3.784611E-02
 ref    2  -2.938965E-02   1.770112E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.616637       0.280685       0.633919       0.313600       3.379205E-02   2.050570E-02   1.742664E-02   5.497454E-04

              v      9       v     10
 ref    1   8.643994E-04   1.745658E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.78496236    -0.30029002     0.35025049    -0.32502582    -0.07170728     0.05891169     0.11751988     0.01616024
 ref:   2     0.02169710     0.43647547     0.71501283     0.45602384    -0.16926344    -0.13051864     0.06013083    -0.01698800

                ci   9         ci  10
 ref:   1     0.00080505     0.03784611
 ref:   2    -0.02938965     0.01770112

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 40  1   -196.4725867608  2.2333E-06  0.0000E+00  4.5897E-03  1.0000E-04   
 mr-sdci # 40  2   -196.4722574477  1.6530E-03  0.0000E+00  4.8755E-02  1.0000E-04   
 mr-sdci # 40  3   -196.4687668703  6.2197E-05  0.0000E+00  4.9972E-02  1.0000E-04   
 mr-sdci # 40  4   -196.4641959044  3.1425E-03  0.0000E+00  5.1981E-02  1.0000E-04   
 mr-sdci # 40  5   -196.4409566332  1.5106E-05  2.9376E-02  3.1015E-01  1.0000E-04   
 mr-sdci # 40  6   -195.7570662527  2.8054E-04  0.0000E+00  6.4846E-01  1.0000E-04   
 mr-sdci # 40  7   -195.7165404817  1.6066E-03  0.0000E+00  4.3744E-01  1.0000E-04   
 mr-sdci # 40  8   -195.6307676151  4.1987E-03  0.0000E+00  5.8777E-01  1.0000E-04   
 mr-sdci # 40  9   -195.2603500472  2.0417E+00  0.0000E+00  1.6850E+00  1.0000E-04   
 mr-sdci # 40 10   -192.2163279664 -2.6274E+00  0.0000E+00  4.6845E+00  1.0000E-04   
 
 root number  5 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.006000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  41

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58426969
   ht   2     0.00000000   -50.58199092
   ht   3     0.00000000     0.00000000   -50.57966846
   ht   4     0.00000000     0.00000000     0.00000000   -50.57086117
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.49899588
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.86804995
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.82532796
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.73788521
   ht   9    -0.11594754    -0.10560479     0.38005187    -0.15066042     0.08404613     0.03886074     0.00736092    -0.06597914
   ht  10     0.50252067    -0.48133736    -1.15580068    -0.12499077    -0.53045182    -0.32792970    -0.07033311    -0.17146198
   ht  11    -0.85734623     1.52435304    -0.34856564     2.59380049     0.18512456    -0.44987417    -0.41868774    -0.34868029

                ht   9         ht  10         ht  11
   ht   9    -0.01057282
   ht  10     0.00366574    -0.15579214
   ht  11     0.03360558    -0.00609543    -1.18436470

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.782503      -0.313265      -0.302712       0.331716       0.166120      -5.894349E-02   0.118260       1.556536E-02
 ref    2   2.039750E-02   0.415000      -0.653543      -0.441943       0.378895       0.130501       6.063389E-02  -1.709984E-02

              v      9       v     10       v     11
 ref    1  -2.391610E-03   2.345593E-02  -3.091533E-02
 ref    2   2.607746E-02   2.643530E-02  -1.032906E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.612728       0.270360       0.518752       0.305349       0.171157       2.050474E-02   1.766182E-02   5.346848E-04

              v      9       v     10       v     11
 ref    1   6.857536E-04   1.249006E-03   1.062447E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.78250334    -0.31326507    -0.30271169     0.33171567     0.16612035    -0.05894349     0.11825969     0.01556536
 ref:   2     0.02039750     0.41500036    -0.65354254    -0.44194299     0.37889494     0.13050058     0.06063389    -0.01709984

                ci   9         ci  10         ci  11
 ref:   1    -0.00239161     0.02345593    -0.03091533
 ref:   2     0.02607746     0.02643530    -0.01032906

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 41  1   -196.4725869411  1.8034E-07  0.0000E+00  4.6838E-03  1.0000E-04   
 mr-sdci # 41  2   -196.4722651690  7.7214E-06  0.0000E+00  4.9084E-02  1.0000E-04   
 mr-sdci # 41  3   -196.4701782755  1.4114E-03  0.0000E+00  6.1082E-02  1.0000E-04   
 mr-sdci # 41  4   -196.4641986007  2.6963E-06  0.0000E+00  5.1764E-02  1.0000E-04   
 mr-sdci # 41  5   -196.4596332183  1.8677E-02  0.0000E+00  1.3076E-01  1.0000E-04   
 mr-sdci # 41  6   -195.7570663293  7.6561E-08  7.8070E-03  6.4815E-01  1.0000E-04   
 mr-sdci # 41  7   -195.7175273610  9.8688E-04  0.0000E+00  3.9969E-01  1.0000E-04   
 mr-sdci # 41  8   -195.6314017440  6.3413E-04  0.0000E+00  5.9529E-01  1.0000E-04   
 mr-sdci # 41  9   -195.2786694276  1.8319E-02  0.0000E+00  1.7142E+00  1.0000E-04   
 mr-sdci # 41 10   -194.1708963585  1.9546E+00  0.0000E+00  3.0296E+00  1.0000E-04   
 mr-sdci # 41 11   -191.9103786266 -2.1174E+00  0.0000E+00  4.7918E+00  1.0000E-04   
 
 root number  6 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.006000
time for eigenvalue solver             0.001000
time for vector access                 0.000000

          starting ci iteration  42

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58426969
   ht   2     0.00000000   -50.58199092
   ht   3     0.00000000     0.00000000   -50.57966846
   ht   4     0.00000000     0.00000000     0.00000000   -50.57086117
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.49899588
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.86804995
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.82532796
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.73788521
   ht   9    -0.11594754    -0.10560479     0.38005187    -0.15066042     0.08404613     0.03886074     0.00736092    -0.06597914
   ht  10     0.50252067    -0.48133736    -1.15580068    -0.12499077    -0.53045182    -0.32792970    -0.07033311    -0.17146198
   ht  11    -0.85734623     1.52435304    -0.34856564     2.59380049     0.18512456    -0.44987417    -0.41868774    -0.34868029
   ht  12    -2.88400676     6.55158569    -1.10694213     5.53948079    -1.69666697     7.25274455     3.91182126    -1.55748312

                ht   9         ht  10         ht  11         ht  12
   ht   9    -0.01057282
   ht  10     0.00366574    -0.15579214
   ht  11     0.03360558    -0.00609543    -1.18436470
   ht  12    -0.05445946     0.34958034     0.98723840   -14.91845832

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.765550      -0.360919       0.272447      -0.350901      -0.161498       6.260909E-02  -0.117580      -1.777432E-02
 ref    2   3.501376E-02   0.371175       0.678033       0.410937      -0.413588      -0.127704      -6.624572E-02   2.079677E-02

              v      9       v     10       v     11       v     12
 ref    1  -1.147980E-02  -2.904939E-02   9.242477E-03  -2.518371E-02
 ref    2  -2.771872E-02  -4.704711E-04   2.264260E-02  -9.981248E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.587292       0.268033       0.533956       0.292001       0.197137       2.022829E-02   1.821357E-02   7.484321E-04

              v      9       v     10       v     11       v     12
 ref    1   9.001131E-04   8.440882E-04   5.981109E-04   7.338448E-04

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.76554955    -0.36091904     0.27244704    -0.35090111    -0.16149758     0.06260909    -0.11758006    -0.01777432
 ref:   2     0.03501376     0.37117495     0.67803295     0.41093716    -0.41358836    -0.12770431    -0.06624572     0.02079677

                ci   9         ci  10         ci  11         ci  12
 ref:   1    -0.01147980    -0.02904939     0.00924248    -0.02518371
 ref:   2    -0.02771872    -0.00047047     0.02264260    -0.00998125

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 42  1   -196.4725882117  1.2706E-06  1.2109E-05  6.7549E-03  1.0000E-04   
 mr-sdci # 42  2   -196.4723822927  1.1712E-04  0.0000E+00  4.7351E-02  1.0000E-04   
 mr-sdci # 42  3   -196.4703419426  1.6367E-04  0.0000E+00  5.0896E-02  1.0000E-04   
 mr-sdci # 42  4   -196.4643051714  1.0657E-04  0.0000E+00  4.8765E-02  1.0000E-04   
 mr-sdci # 42  5   -196.4601641149  5.3090E-04  0.0000E+00  1.0619E-01  1.0000E-04   
 mr-sdci # 42  6   -195.7576814533  6.1512E-04  0.0000E+00  5.9912E-01  1.0000E-04   
 mr-sdci # 42  7   -195.7191638658  1.6365E-03  0.0000E+00  3.7115E-01  1.0000E-04   
 mr-sdci # 42  8   -195.6387454969  7.3438E-03  0.0000E+00  5.8354E-01  1.0000E-04   
 mr-sdci # 42  9   -195.4382272237  1.5956E-01  0.0000E+00  1.6322E+00  1.0000E-04   
 mr-sdci # 42 10   -194.9552963257  7.8440E-01  0.0000E+00  2.8070E+00  1.0000E-04   
 mr-sdci # 42 11   -194.0135359564  2.1032E+00  0.0000E+00  2.7524E+00  1.0000E-04   
 mr-sdci # 42 12   -191.7250183513 -1.8461E+00  0.0000E+00  4.7683E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.014000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  43

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58426969
   ht   2     0.00000000   -50.58199092
   ht   3     0.00000000     0.00000000   -50.57966846
   ht   4     0.00000000     0.00000000     0.00000000   -50.57086117
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.49899588
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.86804995
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.82532796
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.73788521
   ht   9    -0.11594754    -0.10560479     0.38005187    -0.15066042     0.08404613     0.03886074     0.00736092    -0.06597914
   ht  10     0.50252067    -0.48133736    -1.15580068    -0.12499077    -0.53045182    -0.32792970    -0.07033311    -0.17146198
   ht  11    -0.85734623     1.52435304    -0.34856564     2.59380049     0.18512456    -0.44987417    -0.41868774    -0.34868029
   ht  12    -2.88400676     6.55158569    -1.10694213     5.53948079    -1.69666697     7.25274455     3.91182126    -1.55748312
   ht  13     0.03130919    -0.09628959    -0.05570656    -0.03760867     0.01851683    -0.01840881    -0.00646530     0.01786231

                ht   9         ht  10         ht  11         ht  12         ht  13
   ht   9    -0.01057282
   ht  10     0.00366574    -0.15579214
   ht  11     0.03360558    -0.00609543    -1.18436470
   ht  12    -0.05445946     0.34958034     0.98723840   -14.91845832
   ht  13     0.00044189    -0.00228614     0.00070219     0.04515056    -0.00064012

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.102524       0.837180       0.264782      -0.362453      -0.163924      -7.621293E-02   8.897829E-02   3.697945E-02
 ref    2   0.330214      -7.306819E-02   0.688638       0.425903      -0.410511       0.104562       9.956827E-02   2.471161E-03

              v      9       v     10       v     11       v     12       v     13
 ref    1  -5.455206E-02  -2.936208E-02  -1.284225E-02   1.284558E-03   2.932232E-02
 ref    2   1.683059E-02   4.809996E-04  -7.832616E-03  -4.844104E-02  -1.232042E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.119552       0.706210       0.544331       0.312765       0.195391       1.674160E-02   1.783098E-02   1.373586E-03

              v      9       v     10       v     11       v     12       v     13
 ref    1   3.259196E-03   8.623629E-04   2.262733E-04   2.348184E-03   1.011591E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.10252399     0.83718029     0.26478162    -0.36245255    -0.16392442    -0.07621293     0.08897829     0.03697945
 ref:   2     0.33021406    -0.07306819     0.68863760     0.42590310    -0.41051104     0.10456190     0.09956827     0.00247116

                ci   9         ci  10         ci  11         ci  12         ci  13
 ref:   1    -0.05455206    -0.02936208    -0.01284225     0.00128456     0.02932232
 ref:   2     0.01683059     0.00048100    -0.00783262    -0.04844104    -0.01232042

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 43  1   -196.4727800839  1.9187E-04  0.0000E+00  2.1637E-02  1.0000E-04   
 mr-sdci # 43  2   -196.4725787084  1.9642E-04  5.6178E-06  5.1851E-03  1.0000E-04   
 mr-sdci # 43  3   -196.4703472739  5.3313E-06  0.0000E+00  5.0940E-02  1.0000E-04   
 mr-sdci # 43  4   -196.4646784420  3.7327E-04  0.0000E+00  2.0881E-02  1.0000E-04   
 mr-sdci # 43  5   -196.4601684349  4.3200E-06  0.0000E+00  1.0575E-01  1.0000E-04   
 mr-sdci # 43  6   -195.8128671531  5.5186E-02  0.0000E+00  8.0499E-01  1.0000E-04   
 mr-sdci # 43  7   -195.7243598964  5.1960E-03  0.0000E+00  3.0927E-01  1.0000E-04   
 mr-sdci # 43  8   -195.6575202856  1.8775E-02  0.0000E+00  7.4572E-01  1.0000E-04   
 mr-sdci # 43  9   -195.6043510417  1.6612E-01  0.0000E+00  8.7734E-01  1.0000E-04   
 mr-sdci # 43 10   -194.9556163334  3.2001E-04  0.0000E+00  2.8052E+00  1.0000E-04   
 mr-sdci # 43 11   -194.1083298351  9.4794E-02  0.0000E+00  2.6488E+00  1.0000E-04   
 mr-sdci # 43 12   -192.5667031789  8.4168E-01  0.0000E+00  4.0553E+00  1.0000E-04   
 mr-sdci # 43 13   -191.4806674243 -1.9773E+00  0.0000E+00  4.7929E+00  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  44

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58426969
   ht   2     0.00000000   -50.58199092
   ht   3     0.00000000     0.00000000   -50.57966846
   ht   4     0.00000000     0.00000000     0.00000000   -50.57086117
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.49899588
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.86804995
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.82532796
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.73788521
   ht   9    -0.11594754    -0.10560479     0.38005187    -0.15066042     0.08404613     0.03886074     0.00736092    -0.06597914
   ht  10     0.50252067    -0.48133736    -1.15580068    -0.12499077    -0.53045182    -0.32792970    -0.07033311    -0.17146198
   ht  11    -0.85734623     1.52435304    -0.34856564     2.59380049     0.18512456    -0.44987417    -0.41868774    -0.34868029
   ht  12    -2.88400676     6.55158569    -1.10694213     5.53948079    -1.69666697     7.25274455     3.91182126    -1.55748312
   ht  13     0.03130919    -0.09628959    -0.05570656    -0.03760867     0.01851683    -0.01840881    -0.00646530     0.01786231
   ht  14     0.03879153    -0.00853882     0.00432465     0.01329538    -0.00768166    -0.00291629    -0.01033920    -0.00178067

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14
   ht   9    -0.01057282
   ht  10     0.00366574    -0.15579214
   ht  11     0.03360558    -0.00609543    -1.18436470
   ht  12    -0.05445946     0.34958034     0.98723840   -14.91845832
   ht  13     0.00044189    -0.00228614     0.00070219     0.04515056    -0.00064012
   ht  14     0.00007098    -0.00082570    -0.00154414     0.00770484    -0.00003030    -0.00015978

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.156288       0.827025       0.268187      -0.362725       0.166790      -6.239991E-02   0.106989      -4.462384E-02
 ref    2   0.334275      -4.971749E-02   0.684122       0.435571       0.408068       0.104608       8.850268E-02   4.481488E-02

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1  -3.562868E-02  -2.929351E-02  -9.844936E-03  -2.689820E-02  -9.371291E-03  -2.548712E-02
 ref    2   2.021746E-02   2.693513E-04  -8.416012E-03   8.905195E-03  -4.762611E-02   1.522418E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.136165       0.686443       0.539947       0.321291       0.194339       1.483649E-02   1.927928E-02   3.999661E-03

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1   1.678149E-03   8.581821E-04   1.677520E-04   8.028158E-04   2.356067E-03   8.813687E-04

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.15628820     0.82702530     0.26818677    -0.36272513     0.16678953    -0.06239991     0.10698858    -0.04462384
 ref:   2     0.33427451    -0.04971749     0.68412188     0.43557071     0.40806848     0.10460758     0.08850268     0.04481488

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14
 ref:   1    -0.03562868    -0.02929351    -0.00984494    -0.02689820    -0.00937129    -0.02548712
 ref:   2     0.02021746     0.00026935    -0.00841601     0.00890519    -0.04762611     0.01522418

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 44  1   -196.4728624868  8.2403E-05  0.0000E+00  1.3405E-02  1.0000E-04   
 mr-sdci # 44  2   -196.4725820937  3.3854E-06  0.0000E+00  1.3584E-03  1.0000E-04   
 mr-sdci # 44  3   -196.4703517876  4.5136E-06  1.1024E-03  5.0634E-02  1.0000E-04   
 mr-sdci # 44  4   -196.4647405622  6.2120E-05  0.0000E+00  1.4296E-02  1.0000E-04   
 mr-sdci # 44  5   -196.4602114887  4.3054E-05  0.0000E+00  1.0406E-01  1.0000E-04   
 mr-sdci # 44  6   -195.8193920209  6.5249E-03  0.0000E+00  8.5286E-01  1.0000E-04   
 mr-sdci # 44  7   -195.7259441759  1.5843E-03  0.0000E+00  2.8296E-01  1.0000E-04   
 mr-sdci # 44  8   -195.7085067822  5.0986E-02  0.0000E+00  5.6987E-01  1.0000E-04   
 mr-sdci # 44  9   -195.6208449606  1.6494E-02  0.0000E+00  6.6054E-01  1.0000E-04   
 mr-sdci # 44 10   -194.9577801182  2.1638E-03  0.0000E+00  2.8246E+00  1.0000E-04   
 mr-sdci # 44 11   -194.1182725093  9.9427E-03  0.0000E+00  2.5811E+00  1.0000E-04   
 mr-sdci # 44 12   -193.5371879911  9.7048E-01  0.0000E+00  3.6473E+00  1.0000E-04   
 mr-sdci # 44 13   -192.4582447855  9.7758E-01  0.0000E+00  3.8836E+00  1.0000E-04   
 mr-sdci # 44 14   -191.4490068323  8.3535E-01  0.0000E+00  4.8783E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.006000
time for eigenvalue solver             0.001000
time for vector access                 0.000000

          starting ci iteration  45

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58426969
   ht   2     0.00000000   -50.58199092
   ht   3     0.00000000     0.00000000   -50.57966846
   ht   4     0.00000000     0.00000000     0.00000000   -50.57086117
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.49899588
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.86804995
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.82532796
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.73788521
   ht   9    -0.11594754    -0.10560479     0.38005187    -0.15066042     0.08404613     0.03886074     0.00736092    -0.06597914
   ht  10     0.50252067    -0.48133736    -1.15580068    -0.12499077    -0.53045182    -0.32792970    -0.07033311    -0.17146198
   ht  11    -0.85734623     1.52435304    -0.34856564     2.59380049     0.18512456    -0.44987417    -0.41868774    -0.34868029
   ht  12    -2.88400676     6.55158569    -1.10694213     5.53948079    -1.69666697     7.25274455     3.91182126    -1.55748312
   ht  13     0.03130919    -0.09628959    -0.05570656    -0.03760867     0.01851683    -0.01840881    -0.00646530     0.01786231
   ht  14     0.03879153    -0.00853882     0.00432465     0.01329538    -0.00768166    -0.00291629    -0.01033920    -0.00178067
   ht  15     0.12977163    -0.18606326    -0.21478534    -0.61058522    -0.16498779     0.19573676    -0.06934637    -0.01617605

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15
   ht   9    -0.01057282
   ht  10     0.00366574    -0.15579214
   ht  11     0.03360558    -0.00609543    -1.18436470
   ht  12    -0.05445946     0.34958034     0.98723840   -14.91845832
   ht  13     0.00044189    -0.00228614     0.00070219     0.04515056    -0.00064012
   ht  14     0.00007098    -0.00082570    -0.00154414     0.00770484    -0.00003030    -0.00015978
   ht  15    -0.00243510    -0.00829336     0.03584449    -0.01673896    -0.00037561     0.00002407    -0.04714144

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.171908       0.827292       0.194347      -0.301385       0.311616      -6.318736E-02  -0.106853       4.488825E-02
 ref    2   0.293599      -4.884975E-02   0.545569       0.557556       0.490794       0.101238      -8.961417E-02  -4.543862E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1   3.553022E-02   1.115288E-02  -2.688874E-02  -2.432653E-02   1.528712E-02  -1.278813E-02   2.618544E-02
 ref    2  -2.120096E-02   5.145680E-02   1.100915E-02   2.163647E-02   1.360422E-02  -3.796876E-02  -4.716323E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.115752       0.686798       0.335416       0.401701       0.337983       1.424180E-02   1.944828E-02   4.079623E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1   1.711877E-03   2.772188E-03   8.442060E-04   1.059917E-03   4.187708E-04   1.605163E-03   7.079212E-04

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.17190751     0.82729194     0.19434689    -0.30138466     0.31161603    -0.06318736    -0.10685310     0.04488825
 ref:   2     0.29359884    -0.04884975     0.54556917     0.55755599     0.49079364     0.10123814    -0.08961417    -0.04543862

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15
 ref:   1     0.03553022     0.01115288    -0.02688874    -0.02432653     0.01528712    -0.01278813     0.02618544
 ref:   2    -0.02120096     0.05145680     0.01100915     0.02163647     0.01360422    -0.03796876    -0.00471632

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 45  1   -196.4728672420  4.7552E-06  0.0000E+00  1.3388E-02  1.0000E-04   
 mr-sdci # 45  2   -196.4725820945  7.4591E-10  0.0000E+00  1.3519E-03  1.0000E-04   
 mr-sdci # 45  3   -196.4723096232  1.9578E-03  0.0000E+00  3.4399E-02  1.0000E-04   
 mr-sdci # 45  4   -196.4647673018  2.6740E-05  7.3983E-05  1.7585E-02  1.0000E-04   
 mr-sdci # 45  5   -196.4642104792  3.9990E-03  0.0000E+00  3.6440E-02  1.0000E-04   
 mr-sdci # 45  6   -195.8205475231  1.1555E-03  0.0000E+00  8.6367E-01  1.0000E-04   
 mr-sdci # 45  7   -195.7259948492  5.0673E-05  0.0000E+00  2.7874E-01  1.0000E-04   
 mr-sdci # 45  8   -195.7085445082  3.7726E-05  0.0000E+00  5.6750E-01  1.0000E-04   
 mr-sdci # 45  9   -195.6208964028  5.1442E-05  0.0000E+00  6.5934E-01  1.0000E-04   
 mr-sdci # 45 10   -195.4614790522  5.0370E-01  0.0000E+00  1.0455E+00  1.0000E-04   
 mr-sdci # 45 11   -194.9323957726  8.1412E-01  0.0000E+00  2.7924E+00  1.0000E-04   
 mr-sdci # 45 12   -193.6727143427  1.3553E-01  0.0000E+00  3.2023E+00  1.0000E-04   
 mr-sdci # 45 13   -193.4274491551  9.6920E-01  0.0000E+00  3.4416E+00  1.0000E-04   
 mr-sdci # 45 14   -192.3861430324  9.3714E-01  0.0000E+00  4.0029E+00  1.0000E-04   
 mr-sdci # 45 15   -190.8053833725  6.7685E-01  0.0000E+00  4.7596E+00  1.0000E-04   
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  46

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58426969
   ht   2     0.00000000   -50.58199092
   ht   3     0.00000000     0.00000000   -50.57966846
   ht   4     0.00000000     0.00000000     0.00000000   -50.57086117
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.49899588
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.86804995
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.82532796
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.73788521
   ht   9    -0.11594754    -0.10560479     0.38005187    -0.15066042     0.08404613     0.03886074     0.00736092    -0.06597914
   ht  10     0.50252067    -0.48133736    -1.15580068    -0.12499077    -0.53045182    -0.32792970    -0.07033311    -0.17146198
   ht  11    -0.85734623     1.52435304    -0.34856564     2.59380049     0.18512456    -0.44987417    -0.41868774    -0.34868029
   ht  12    -2.88400676     6.55158569    -1.10694213     5.53948079    -1.69666697     7.25274455     3.91182126    -1.55748312
   ht  13     0.03130919    -0.09628959    -0.05570656    -0.03760867     0.01851683    -0.01840881    -0.00646530     0.01786231
   ht  14     0.03879153    -0.00853882     0.00432465     0.01329538    -0.00768166    -0.00291629    -0.01033920    -0.00178067
   ht  15     0.12977163    -0.18606326    -0.21478534    -0.61058522    -0.16498779     0.19573676    -0.06934637    -0.01617605
   ht  16    -0.11304681    -0.02175154    -0.07075906    -0.18219201     0.09290642     0.04729128     0.04869500     0.00231656

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15         ht  16
   ht   9    -0.01057282
   ht  10     0.00366574    -0.15579214
   ht  11     0.03360558    -0.00609543    -1.18436470
   ht  12    -0.05445946     0.34958034     0.98723840   -14.91845832
   ht  13     0.00044189    -0.00228614     0.00070219     0.04515056    -0.00064012
   ht  14     0.00007098    -0.00082570    -0.00154414     0.00770484    -0.00003030    -0.00015978
   ht  15    -0.00243510    -0.00829336     0.03584449    -0.01673896    -0.00037561     0.00002407    -0.04714144
   ht  16    -0.00068874     0.00180327     0.02298965    -0.05477462     0.00008809     0.00015287    -0.00301424    -0.00331266

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.159079       0.824359       0.216202       0.249033       0.355006      -4.093209E-02  -6.463502E-02   0.108166    
 ref    2   0.330921      -5.305710E-02   0.508385      -0.633831       0.406758       7.489301E-02  -0.114286      -2.628504E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   3.654163E-02   7.703791E-03  -1.907916E-02   5.006853E-03  -3.634211E-03   3.133450E-02   2.756407E-02   3.225447E-02
 ref    2  -2.220584E-02   3.039996E-02   5.051582E-02   4.244880E-02  -1.567810E-02   4.360409E-02   1.355965E-02  -3.150804E-04

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.134815       0.682382       0.305199       0.463760       0.291481       7.284399E-03   1.723890E-02   1.239075E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   1.828390E-03   9.835060E-04   2.915863E-03   1.826969E-03   2.590104E-04   2.883168E-03   9.436418E-04   1.040450E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15        ev   16

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.15907909     0.82435852     0.21620192     0.24903290     0.35500575    -0.04093209    -0.06463502     0.10816584
 ref:   2     0.33092135    -0.05305710     0.50838501    -0.63383141     0.40675806     0.07489301    -0.11428569    -0.02628504

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1     0.03654163     0.00770379    -0.01907916     0.00500685    -0.00363421     0.03133450     0.02756407     0.03225447
 ref:   2    -0.02220584     0.03039996     0.05051582     0.04244880    -0.01567810     0.04360409     0.01355965    -0.00031508

 trial vector basis is being transformed.  new dimension:   8

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 46  1   -196.4728876782  2.0436E-05  0.0000E+00  1.2058E-02  1.0000E-04   
 mr-sdci # 46  2   -196.4725821661  7.1576E-08  0.0000E+00  1.2384E-03  1.0000E-04   
 mr-sdci # 46  3   -196.4723960769  8.6454E-05  0.0000E+00  2.4806E-02  1.0000E-04   
 mr-sdci # 46  4   -196.4648505844  8.3283E-05  0.0000E+00  1.1690E-02  1.0000E-04   
 mr-sdci # 46  5   -196.4642963330  8.5854E-05  1.8295E-04  2.5716E-02  1.0000E-04   
 mr-sdci # 46  6   -195.9097924778  8.9245E-02  0.0000E+00  1.1380E+00  1.0000E-04   
 mr-sdci # 46  7   -195.7261369140  1.4206E-04  0.0000E+00  2.8600E-01  1.0000E-04   
 mr-sdci # 46  8   -195.7254602738  1.6916E-02  0.0000E+00  3.9204E-01  1.0000E-04   
 mr-sdci # 46  9   -195.6351106740  1.4214E-02  0.0000E+00  5.5397E-01  1.0000E-04   
 mr-sdci # 46 10   -195.5229308818  6.1452E-02  0.0000E+00  9.1212E-01  1.0000E-04   
 mr-sdci # 46 11   -195.1291036484  1.9671E-01  0.0000E+00  2.2118E+00  1.0000E-04   
 mr-sdci # 46 12   -194.3960323390  7.2332E-01  0.0000E+00  2.6086E+00  1.0000E-04   
 mr-sdci # 46 13   -193.4575489874  3.0100E-02  0.0000E+00  3.1775E+00  1.0000E-04   
 mr-sdci # 46 14   -192.5691206321  1.8298E-01  0.0000E+00  3.5683E+00  1.0000E-04   
 mr-sdci # 46 15   -191.4593524527  6.5397E-01  0.0000E+00  5.0703E+00  1.0000E-04   
 mr-sdci # 46 16   -190.7807279148  1.5655E+00  0.0000E+00  4.6061E+00  1.0000E-04   
 
 root number  5 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.011000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  47

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58457288
   ht   2     0.00000000   -50.58426737
   ht   3     0.00000000     0.00000000   -50.58408128
   ht   4     0.00000000     0.00000000     0.00000000   -50.57653579
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57598154
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.02147768
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.83782212
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.83714548
   ht   9    -0.15471391     0.17674945    -0.02093096     0.10395579    -0.11573405    -0.04497299     0.01730190     0.03084715

                ht   9
   ht   9    -0.00585451

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.174532       0.827302      -0.184712      -0.232816      -0.369362       4.039066E-02  -0.123672      -2.196896E-02
 ref    2   0.293466      -4.418443E-02  -0.524749       0.652075      -0.386515      -7.636679E-02  -5.503599E-02   0.103557    

              v      9
 ref    1  -2.822570E-02
 ref    2   2.995002E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.116583       0.686381       0.309480       0.479405       0.285823       7.463293E-03   1.832366E-02   1.120665E-02

              v      9
 ref    1   1.693694E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.17453152     0.82730203    -0.18471168    -0.23281619    -0.36936212     0.04039066    -0.12367174    -0.02196896
 ref:   2     0.29346575    -0.04418443    -0.52474882     0.65207465    -0.38651542    -0.07636679    -0.05503599     0.10355681

                ci   9
 ref:   1    -0.02822570
 ref:   2     0.02995002

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 47  1   -196.4728964064  8.7282E-06  0.0000E+00  1.0971E-02  1.0000E-04   
 mr-sdci # 47  2   -196.4725821910  2.4920E-08  0.0000E+00  1.2248E-03  1.0000E-04   
 mr-sdci # 47  3   -196.4725205886  1.2451E-04  0.0000E+00  1.3373E-02  1.0000E-04   
 mr-sdci # 47  4   -196.4648537073  3.1229E-06  0.0000E+00  1.1303E-02  1.0000E-04   
 mr-sdci # 47  5   -196.4644338841  1.3755E-04  0.0000E+00  1.0862E-02  1.0000E-04   
 mr-sdci # 47  6   -195.9124143626  2.6219E-03  1.6288E-02  1.1290E+00  1.0000E-04   
 mr-sdci # 47  7   -195.7276799401  1.5430E-03  0.0000E+00  2.9184E-01  1.0000E-04   
 mr-sdci # 47  8   -195.7259046024  4.4433E-04  0.0000E+00  3.6702E-01  1.0000E-04   
 mr-sdci # 47  9   -193.7387866900 -1.8963E+00  0.0000E+00  3.0486E+00  1.0000E-04   
 
 root number  6 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.005000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  48

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58457288
   ht   2     0.00000000   -50.58426737
   ht   3     0.00000000     0.00000000   -50.58408128
   ht   4     0.00000000     0.00000000     0.00000000   -50.57653579
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57598154
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.02147768
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.83782212
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.83714548
   ht   9    -0.15471391     0.17674945    -0.02093096     0.10395579    -0.11573405    -0.04497299     0.01730190     0.03084715
   ht  10    -1.23055024    -7.91993895     7.56101610    16.22918191    -2.90524491   -43.41645229     0.48841644    -4.73818847

                ht   9         ht  10
   ht   9    -0.00585451
   ht  10    -0.07991094   -80.34438674

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.174777       0.827347      -0.184243      -0.232802       0.369380      -4.065893E-02  -6.186473E-02   0.108599    
 ref    2  -0.292952      -4.399168E-02  -0.525042       0.652089       0.386509       7.605082E-02   7.759762E-02   8.754292E-02

              v      9       v     10
 ref    1   3.571925E-02   2.427453E-02
 ref    2  -1.797327E-02  -2.824884E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.116368       0.686438       0.309614       0.479417       0.285831       7.436877E-03   9.848634E-03   1.945746E-02

              v      9       v     10
 ref    1   1.598903E-03   1.387250E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.17477707     0.82734703    -0.18424275    -0.23280244     0.36938033    -0.04065893    -0.06186473     0.10859877
 ref:   2    -0.29295236    -0.04399168    -0.52504191     0.65208912     0.38650880     0.07605082     0.07759762     0.08754292

                ci   9         ci  10
 ref:   1     0.03571925     0.02427453
 ref:   2    -0.01797327    -0.02824884

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 48  1   -196.4728969141  5.0775E-07  3.5710E-05  1.0284E-02  1.0000E-04   
 mr-sdci # 48  2   -196.4725821923  1.2835E-09  0.0000E+00  1.2349E-03  1.0000E-04   
 mr-sdci # 48  3   -196.4725209008  3.1211E-07  0.0000E+00  1.3335E-02  1.0000E-04   
 mr-sdci # 48  4   -196.4648537075  2.1274E-10  0.0000E+00  1.1317E-02  1.0000E-04   
 mr-sdci # 48  5   -196.4644350033  1.1192E-06  0.0000E+00  1.0727E-02  1.0000E-04   
 mr-sdci # 48  6   -195.9127284002  3.1404E-04  0.0000E+00  1.0988E+00  1.0000E-04   
 mr-sdci # 48  7   -195.7297315622  2.0516E-03  0.0000E+00  3.6138E-01  1.0000E-04   
 mr-sdci # 48  8   -195.7275580931  1.6535E-03  0.0000E+00  2.6676E-01  1.0000E-04   
 mr-sdci # 48  9   -194.6907170939  9.5193E-01  0.0000E+00  3.1182E+00  1.0000E-04   
 mr-sdci # 48 10   -193.7251415170 -1.7978E+00  0.0000E+00  3.0490E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.014000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  49

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58457288
   ht   2     0.00000000   -50.58426737
   ht   3     0.00000000     0.00000000   -50.58408128
   ht   4     0.00000000     0.00000000     0.00000000   -50.57653579
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57598154
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.02147768
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.83782212
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.83714548
   ht   9    -0.15471391     0.17674945    -0.02093096     0.10395579    -0.11573405    -0.04497299     0.01730190     0.03084715
   ht  10    -1.23055024    -7.91993895     7.56101610    16.22918191    -2.90524491   -43.41645229     0.48841644    -4.73818847
   ht  11     0.00356068     0.06603177    -0.03051528    -0.11175855    -0.03329672     0.15375429    -0.00507839     0.01808227

                ht   9         ht  10         ht  11
   ht   9    -0.00585451
   ht  10    -0.07991094   -80.34438674
   ht  11     0.00001641     0.29189698    -0.00225415

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.177591       0.827251      -0.180941      -0.236839      -0.367297       3.227799E-02  -6.297114E-02   0.109025    
 ref    2   0.285505      -4.265326E-02  -0.528613       0.648829      -0.392502      -7.289597E-02   8.145178E-02   8.702143E-02

              v      9       v     10       v     11
 ref    1  -3.962562E-02  -2.567430E-02   5.861420E-03
 ref    2   2.670691E-02   2.876607E-02  -3.476855E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.113052       0.686164       0.312171       0.477072       0.288965       6.355691E-03   1.059976E-02   1.945923E-02

              v      9       v     10       v     11
 ref    1   2.283448E-03   1.486656E-03   4.644477E-05

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.17759092     0.82725120    -0.18094062    -0.23683917    -0.36729722     0.03227799    -0.06297114     0.10902522
 ref:   2     0.28550529    -0.04265326    -0.52861321     0.64882882    -0.39250155    -0.07289597     0.08145178     0.08702143

                ci   9         ci  10         ci  11
 ref:   1    -0.03962562    -0.02567430     0.00586142
 ref:   2     0.02670691     0.02876607    -0.00347686

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 49  1   -196.4729294125  3.2498E-05  0.0000E+00  9.8764E-03  1.0000E-04   
 mr-sdci # 49  2   -196.4725822047  1.2416E-08  5.1836E-07  1.2259E-03  1.0000E-04   
 mr-sdci # 49  3   -196.4725215090  6.0826E-07  0.0000E+00  1.3108E-02  1.0000E-04   
 mr-sdci # 49  4   -196.4648763362  2.2629E-05  0.0000E+00  1.2263E-02  1.0000E-04   
 mr-sdci # 49  5   -196.4644357087  7.0539E-07  0.0000E+00  1.0625E-02  1.0000E-04   
 mr-sdci # 49  6   -196.1267344694  2.1401E-01  0.0000E+00  9.7999E-01  1.0000E-04   
 mr-sdci # 49  7   -195.7303129638  5.8140E-04  0.0000E+00  3.6184E-01  1.0000E-04   
 mr-sdci # 49  8   -195.7275587315  6.3842E-07  0.0000E+00  2.6698E-01  1.0000E-04   
 mr-sdci # 49  9   -194.9582133192  2.6750E-01  0.0000E+00  2.7372E+00  1.0000E-04   
 mr-sdci # 49 10   -193.7321595648  7.0180E-03  0.0000E+00  3.0329E+00  1.0000E-04   
 mr-sdci # 49 11   -193.0852926184 -2.0438E+00  0.0000E+00  3.1165E+00  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.006000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  50

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58457288
   ht   2     0.00000000   -50.58426737
   ht   3     0.00000000     0.00000000   -50.58408128
   ht   4     0.00000000     0.00000000     0.00000000   -50.57653579
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57598154
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.02147768
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.83782212
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.83714548
   ht   9    -0.15471391     0.17674945    -0.02093096     0.10395579    -0.11573405    -0.04497299     0.01730190     0.03084715
   ht  10    -1.23055024    -7.91993895     7.56101610    16.22918191    -2.90524491   -43.41645229     0.48841644    -4.73818847
   ht  11     0.00356068     0.06603177    -0.03051528    -0.11175855    -0.03329672     0.15375429    -0.00507839     0.01808227
   ht  12    -0.00960914    -0.01101358    -0.00459888     0.00756614    -0.00119855     0.00174302     0.00124956    -0.00117077

                ht   9         ht  10         ht  11         ht  12
   ht   9    -0.00585451
   ht  10    -0.07991094   -80.34438674
   ht  11     0.00001641     0.29189698    -0.00225415
   ht  12     0.00000522    -0.00066329     0.00000133    -0.00001895

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.178032      -0.826498       0.184045       0.236832       0.367282       3.246144E-02  -0.121439       2.003305E-02
 ref    2   0.285663       4.441780E-02   0.528353      -0.648839       0.392519      -7.194933E-02  -1.138065E-02   0.117370    

              v      9       v     10       v     11       v     12
 ref    1  -2.848556E-02   6.526478E-02   1.183090E-02   2.148371E-02
 ref    2   3.516851E-02   3.696697E-02  -3.772445E-02   2.955992E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.113299       0.685072       0.313029       0.477081       0.288967       6.230450E-03   1.487691E-02   1.417709E-02

              v      9       v     10       v     11       v     12
 ref    1   2.048251E-03   5.626048E-03   1.563104E-03   1.335339E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.17803206    -0.82649816     0.18404466     0.23683188     0.36728170     0.03246144    -0.12143882     0.02003305
 ref:   2     0.28566322     0.04441780     0.52835288    -0.64883877     0.39251873    -0.07194933    -0.01138065     0.11737020

                ci   9         ci  10         ci  11         ci  12
 ref:   1    -0.02848556     0.06526478     0.01183090     0.02148371
 ref:   2     0.03516851     0.03696697    -0.03772445     0.02955992

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 50  1   -196.4729295146  1.0211E-07  0.0000E+00  9.9245E-03  1.0000E-04   
 mr-sdci # 50  2   -196.4725826002  3.9556E-07  0.0000E+00  7.2419E-04  1.0000E-04   
 mr-sdci # 50  3   -196.4725216379  1.2892E-07  5.4970E-05  1.3186E-02  1.0000E-04   
 mr-sdci # 50  4   -196.4648763365  3.7301E-10  0.0000E+00  1.2272E-02  1.0000E-04   
 mr-sdci # 50  5   -196.4644359646  2.5584E-07  0.0000E+00  1.0718E-02  1.0000E-04   
 mr-sdci # 50  6   -196.1276505175  9.1605E-04  0.0000E+00  9.8612E-01  1.0000E-04   
 mr-sdci # 50  7   -195.7355603491  5.2474E-03  0.0000E+00  3.1481E-01  1.0000E-04   
 mr-sdci # 50  8   -195.7294674244  1.9087E-03  0.0000E+00  3.2379E-01  1.0000E-04   
 mr-sdci # 50  9   -194.9821853979  2.3972E-02  0.0000E+00  2.6752E+00  1.0000E-04   
 mr-sdci # 50 10   -194.0193495652  2.8719E-01  0.0000E+00  2.3994E+00  1.0000E-04   
 mr-sdci # 50 11   -193.7161621065  6.3087E-01  0.0000E+00  3.1524E+00  1.0000E-04   
 mr-sdci # 50 12   -192.8290219184 -1.5670E+00  0.0000E+00  3.4174E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  51

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58457288
   ht   2     0.00000000   -50.58426737
   ht   3     0.00000000     0.00000000   -50.58408128
   ht   4     0.00000000     0.00000000     0.00000000   -50.57653579
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57598154
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.02147768
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.83782212
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.83714548
   ht   9    -0.15471391     0.17674945    -0.02093096     0.10395579    -0.11573405    -0.04497299     0.01730190     0.03084715
   ht  10    -1.23055024    -7.91993895     7.56101610    16.22918191    -2.90524491   -43.41645229     0.48841644    -4.73818847
   ht  11     0.00356068     0.06603177    -0.03051528    -0.11175855    -0.03329672     0.15375429    -0.00507839     0.01808227
   ht  12    -0.00960914    -0.01101358    -0.00459888     0.00756614    -0.00119855     0.00174302     0.00124956    -0.00117077
   ht  13    -0.10078745     0.11980222    -0.06903328     0.10388880    -0.14171334    -0.02683023     0.01334800     0.01447162

                ht   9         ht  10         ht  11         ht  12         ht  13
   ht   9    -0.00585451
   ht  10    -0.07991094   -80.34438674
   ht  11     0.00001641     0.29189698    -0.00225415
   ht  12     0.00000522    -0.00066329     0.00000133    -0.00001895
   ht  13    -0.00101129    -0.04064478     0.00005262    -0.00001009    -0.00256769

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.179799       0.824486       0.189905       0.227162      -0.374009       3.248837E-02  -0.116583      -3.882638E-02
 ref    2   0.280362      -5.018565E-02   0.528218      -0.659328      -0.378101      -7.221843E-02   4.711453E-03  -0.115567    

              v      9       v     10       v     11       v     12       v     13
 ref    1   6.439465E-03   2.740569E-02   6.283427E-02   3.139545E-02  -2.873774E-02
 ref    2   3.486929E-02  -4.901194E-02   3.981546E-02   1.258738E-03   0.103643    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.110931       0.682296       0.315078       0.486316       0.282843       6.270996E-03   1.361370E-02   1.486329E-02

              v      9       v     10       v     11       v     12       v     13
 ref    1   1.257334E-03   3.153242E-03   5.533417E-03   9.872584E-04   1.156772E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.17979857     0.82448604     0.18990501     0.22716177    -0.37400933     0.03248837    -0.11658259    -0.03882638
 ref:   2     0.28036233    -0.05018565     0.52821809    -0.65932800    -0.37810080    -0.07221843     0.00471145    -0.11556730

                ci   9         ci  10         ci  11         ci  12         ci  13
 ref:   1     0.00643946     0.02740569     0.06283427     0.03139545    -0.02873774
 ref:   2     0.03486929    -0.04901194     0.03981546     0.00125874     0.10364293

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 51  1   -196.4729298479  3.3333E-07  0.0000E+00  9.9400E-03  1.0000E-04   
 mr-sdci # 51  2   -196.4725826022  1.9987E-09  0.0000E+00  7.1580E-04  1.0000E-04   
 mr-sdci # 51  3   -196.4725694334  4.7795E-05  0.0000E+00  6.9637E-03  1.0000E-04   
 mr-sdci # 51  4   -196.4648805805  4.2440E-06  3.3396E-05  1.1360E-02  1.0000E-04   
 mr-sdci # 51  5   -196.4644631685  2.7204E-05  0.0000E+00  7.5357E-03  1.0000E-04   
 mr-sdci # 51  6   -196.1276769021  2.6385E-05  0.0000E+00  9.8553E-01  1.0000E-04   
 mr-sdci # 51  7   -195.7363336143  7.7327E-04  0.0000E+00  3.1234E-01  1.0000E-04   
 mr-sdci # 51  8   -195.7305492543  1.0818E-03  0.0000E+00  3.0244E-01  1.0000E-04   
 mr-sdci # 51  9   -195.0472865291  6.5101E-02  0.0000E+00  2.0994E+00  1.0000E-04   
 mr-sdci # 51 10   -194.9738998000  9.5455E-01  0.0000E+00  2.5812E+00  1.0000E-04   
 mr-sdci # 51 11   -194.0065674649  2.9041E-01  0.0000E+00  2.5006E+00  1.0000E-04   
 mr-sdci # 51 12   -192.8896870621  6.0665E-02  0.0000E+00  3.4048E+00  1.0000E-04   
 mr-sdci # 51 13   -192.0425964927 -1.4150E+00  0.0000E+00  3.8736E+00  1.0000E-04   
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  52

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58457288
   ht   2     0.00000000   -50.58426737
   ht   3     0.00000000     0.00000000   -50.58408128
   ht   4     0.00000000     0.00000000     0.00000000   -50.57653579
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57598154
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.02147768
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.83782212
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.83714548
   ht   9    -0.15471391     0.17674945    -0.02093096     0.10395579    -0.11573405    -0.04497299     0.01730190     0.03084715
   ht  10    -1.23055024    -7.91993895     7.56101610    16.22918191    -2.90524491   -43.41645229     0.48841644    -4.73818847
   ht  11     0.00356068     0.06603177    -0.03051528    -0.11175855    -0.03329672     0.15375429    -0.00507839     0.01808227
   ht  12    -0.00960914    -0.01101358    -0.00459888     0.00756614    -0.00119855     0.00174302     0.00124956    -0.00117077
   ht  13    -0.10078745     0.11980222    -0.06903328     0.10388880    -0.14171334    -0.02683023     0.01334800     0.01447162
   ht  14     0.05076075    -0.01571138    -0.03676918    -0.06018433    -0.03967661    -0.07470124    -0.00462990    -0.01049466

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14
   ht   9    -0.00585451
   ht  10    -0.07991094   -80.34438674
   ht  11     0.00001641     0.29189698    -0.00225415
   ht  12     0.00000522    -0.00066329     0.00000133    -0.00001895
   ht  13    -0.00101129    -0.04064478     0.00005262    -0.00001009    -0.00256769
   ht  14     0.00022793    -0.14968597     0.00055708     0.00002821     0.00000634    -0.00145951

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.175530       0.823974      -0.193455      -0.229999      -0.373686       3.086392E-03   0.127855      -1.400617E-02
 ref    2  -0.279602      -5.080676E-02  -0.524660       0.660234      -0.379060      -7.063725E-02   1.609732E-02  -0.106320    

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1  -4.357813E-02   6.949806E-03  -6.361420E-02   3.509641E-02   1.348336E-02  -3.157585E-02
 ref    2   7.089569E-02  -5.319201E-02  -4.007575E-02  -6.114194E-04  -4.377115E-03   0.104812    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.108988       0.681515       0.312692       0.488808       0.283327       4.999147E-03   1.660599E-02   1.150019E-02

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1   6.925253E-03   2.877690E-03   5.652833E-03   1.232132E-03   2.009602E-04   1.198251E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.17553037     0.82397420    -0.19345500    -0.22999915    -0.37368570     0.00308639     0.12785487    -0.01400617
 ref:   2    -0.27960161    -0.05080676    -0.52465952     0.66023362    -0.37905990    -0.07063725     0.01609732    -0.10632034

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14
 ref:   1    -0.04357813     0.00694981    -0.06361420     0.03509641     0.01348336    -0.03157585
 ref:   2     0.07089569    -0.05319201    -0.04007575    -0.00061142    -0.00437712     0.10481161

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 52  1   -196.4729942633  6.4415E-05  0.0000E+00  2.3660E-02  1.0000E-04   
 mr-sdci # 52  2   -196.4725826041  1.8538E-09  0.0000E+00  7.0533E-04  1.0000E-04   
 mr-sdci # 52  3   -196.4725697095  2.7612E-07  0.0000E+00  7.0393E-03  1.0000E-04   
 mr-sdci # 52  4   -196.4650254301  1.4485E-04  0.0000E+00  3.2201E-02  1.0000E-04   
 mr-sdci # 52  5   -196.4644631719  3.4675E-09  1.7333E-05  7.5171E-03  1.0000E-04   
 mr-sdci # 52  6   -196.3972435419  2.6957E-01  0.0000E+00  6.8988E-01  1.0000E-04   
 mr-sdci # 52  7   -195.7375137579  1.1801E-03  0.0000E+00  2.7636E-01  1.0000E-04   
 mr-sdci # 52  8   -195.7321264590  1.5772E-03  0.0000E+00  3.2331E-01  1.0000E-04   
 mr-sdci # 52  9   -195.5871532720  5.3987E-01  0.0000E+00  1.0646E+00  1.0000E-04   
 mr-sdci # 52 10   -195.0355614494  6.1662E-02  0.0000E+00  1.9750E+00  1.0000E-04   
 mr-sdci # 52 11   -194.0175124994  1.0945E-02  0.0000E+00  2.4335E+00  1.0000E-04   
 mr-sdci # 52 12   -192.9054225878  1.5736E-02  0.0000E+00  3.6268E+00  1.0000E-04   
 mr-sdci # 52 13   -192.6314623306  5.8887E-01  0.0000E+00  3.6998E+00  1.0000E-04   
 mr-sdci # 52 14   -192.0353562628 -5.3376E-01  0.0000E+00  3.8637E+00  1.0000E-04   
 
 root number  5 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.009000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  53

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58457288
   ht   2     0.00000000   -50.58426737
   ht   3     0.00000000     0.00000000   -50.58408128
   ht   4     0.00000000     0.00000000     0.00000000   -50.57653579
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57598154
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.02147768
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.83782212
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.83714548
   ht   9    -0.15471391     0.17674945    -0.02093096     0.10395579    -0.11573405    -0.04497299     0.01730190     0.03084715
   ht  10    -1.23055024    -7.91993895     7.56101610    16.22918191    -2.90524491   -43.41645229     0.48841644    -4.73818847
   ht  11     0.00356068     0.06603177    -0.03051528    -0.11175855    -0.03329672     0.15375429    -0.00507839     0.01808227
   ht  12    -0.00960914    -0.01101358    -0.00459888     0.00756614    -0.00119855     0.00174302     0.00124956    -0.00117077
   ht  13    -0.10078745     0.11980222    -0.06903328     0.10388880    -0.14171334    -0.02683023     0.01334800     0.01447162
   ht  14     0.05076075    -0.01571138    -0.03676918    -0.06018433    -0.03967661    -0.07470124    -0.00462990    -0.01049466
   ht  15     0.02887752    -0.00327887     0.04591527     0.02861445     0.10356992     0.00089034    -0.01817000    -0.00286351

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15
   ht   9    -0.00585451
   ht  10    -0.07991094   -80.34438674
   ht  11     0.00001641     0.29189698    -0.00225415
   ht  12     0.00000522    -0.00066329     0.00000133    -0.00001895
   ht  13    -0.00101129    -0.04064478     0.00005262    -0.00001009    -0.00256769
   ht  14     0.00022793    -0.14968597     0.00055708     0.00002821     0.00000634    -0.00145951
   ht  15     0.00039219    -0.00929443     0.00009461     0.00000883     0.00023809     0.00014665    -0.00071356

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.176061       0.822520      -0.198569      -0.228686       0.374800       3.108798E-03   0.127146      -1.681422E-02
 ref    2   0.277951      -5.473593E-02  -0.524652       0.661589       0.377365      -7.066814E-02   3.982415E-02   9.895271E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1  -3.233894E-02   3.522783E-02  -5.806698E-02   5.315635E-02   1.624426E-02   1.809348E-02   1.978900E-02
 ref    2   6.590564E-02  -4.607210E-02  -3.183368E-02   3.575410E-02  -6.864410E-03  -1.380441E-02  -0.117540    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.108254       0.679535       0.314690       0.489998       0.282879       5.003651E-03   1.775203E-02   1.007436E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1   5.389360E-03   3.363639E-03   4.385158E-03   4.103953E-03   3.109962E-04   5.179355E-04   1.420726E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.17606139     0.82251999    -0.19856905    -0.22868643     0.37480022     0.00310880     0.12714586    -0.01681422
 ref:   2     0.27795087    -0.05473593    -0.52465236     0.66158907     0.37736463    -0.07066814     0.03982415     0.09895271

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15
 ref:   1    -0.03233894     0.03522783    -0.05806698     0.05315635     0.01624426     0.01809348     0.01978900
 ref:   2     0.06590564    -0.04607210    -0.03183368     0.03575410    -0.00686441    -0.01380441    -0.11754001

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 53  1   -196.4729945780  3.1471E-07  0.0000E+00  2.3725E-02  1.0000E-04   
 mr-sdci # 53  2   -196.4725826048  7.3536E-10  0.0000E+00  7.0734E-04  1.0000E-04   
 mr-sdci # 53  3   -196.4725760959  6.3864E-06  0.0000E+00  4.9846E-03  1.0000E-04   
 mr-sdci # 53  4   -196.4650256779  2.4775E-07  0.0000E+00  3.2102E-02  1.0000E-04   
 mr-sdci # 53  5   -196.4644773985  1.4227E-05  0.0000E+00  3.7299E-03  1.0000E-04   
 mr-sdci # 53  6   -196.3972507653  7.2234E-06  1.3279E-01  6.8936E-01  1.0000E-04   
 mr-sdci # 53  7   -195.7378530469  3.3929E-04  0.0000E+00  2.7632E-01  1.0000E-04   
 mr-sdci # 53  8   -195.7347513916  2.6249E-03  0.0000E+00  3.3360E-01  1.0000E-04   
 mr-sdci # 53  9   -195.6017007290  1.4547E-02  0.0000E+00  1.0045E+00  1.0000E-04   
 mr-sdci # 53 10   -195.4608508244  4.2529E-01  0.0000E+00  1.1330E+00  1.0000E-04   
 mr-sdci # 53 11   -194.0313584702  1.3846E-02  0.0000E+00  2.4115E+00  1.0000E-04   
 mr-sdci # 53 12   -193.3798119360  4.7439E-01  0.0000E+00  2.9380E+00  1.0000E-04   
 mr-sdci # 53 13   -192.6545845846  2.3122E-02  0.0000E+00  3.9337E+00  1.0000E-04   
 mr-sdci # 53 14   -192.3922324423  3.5688E-01  0.0000E+00  3.4090E+00  1.0000E-04   
 mr-sdci # 53 15   -191.8851998898  4.2585E-01  0.0000E+00  4.2657E+00  1.0000E-04   
 
 root number  6 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.009000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  54

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58457288
   ht   2     0.00000000   -50.58426737
   ht   3     0.00000000     0.00000000   -50.58408128
   ht   4     0.00000000     0.00000000     0.00000000   -50.57653579
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57598154
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.02147768
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.83782212
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.83714548
   ht   9    -0.15471391     0.17674945    -0.02093096     0.10395579    -0.11573405    -0.04497299     0.01730190     0.03084715
   ht  10    -1.23055024    -7.91993895     7.56101610    16.22918191    -2.90524491   -43.41645229     0.48841644    -4.73818847
   ht  11     0.00356068     0.06603177    -0.03051528    -0.11175855    -0.03329672     0.15375429    -0.00507839     0.01808227
   ht  12    -0.00960914    -0.01101358    -0.00459888     0.00756614    -0.00119855     0.00174302     0.00124956    -0.00117077
   ht  13    -0.10078745     0.11980222    -0.06903328     0.10388880    -0.14171334    -0.02683023     0.01334800     0.01447162
   ht  14     0.05076075    -0.01571138    -0.03676918    -0.06018433    -0.03967661    -0.07470124    -0.00462990    -0.01049466
   ht  15     0.02887752    -0.00327887     0.04591527     0.02861445     0.10356992     0.00089034    -0.01817000    -0.00286351
   ht  16     8.91450579   -12.15951131    -4.26984423     3.22158645    -3.69333329     1.96153174     0.69061545    -1.28519440

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15         ht  16
   ht   9    -0.00585451
   ht  10    -0.07991094   -80.34438674
   ht  11     0.00001641     0.29189698    -0.00225415
   ht  12     0.00000522    -0.00066329     0.00000133    -0.00001895
   ht  13    -0.00101129    -0.04064478     0.00005262    -0.00001009    -0.00256769
   ht  14     0.00022793    -0.14968597     0.00055708     0.00002821     0.00000634    -0.00145951
   ht  15     0.00039219    -0.00929443     0.00009461     0.00000883     0.00023809     0.00014665    -0.00071356
   ht  16     0.04497959     1.93682339    -0.00523585    -0.00222921     0.01784204    -0.00149507     0.00016490    -8.48588230

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   8.944177E-02  -0.822479       0.198995       0.198041       0.372982      -0.193270       0.103375       7.889811E-02
 ref    2  -0.107168       5.468919E-02   0.523980      -0.398886       0.382717       0.587505       6.186108E-02  -5.738254E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   1.026274E-02  -1.694550E-02  -4.636568E-02  -1.570620E-02  -4.072883E-02  -3.115451E-03  -5.089301E-02   0.114208    
 ref    2  -0.104173       2.904089E-02  -3.162058E-02  -1.988218E-02  -1.660978E-02   3.728722E-02  -0.100881      -5.478454E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   1.948489E-02   0.679462       0.314154       0.198330       0.285588       0.382516       1.451318E-02   9.517668E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   1.095724E-02   1.130523E-03   3.149637E-03   6.419857E-04   1.934722E-03   1.400043E-03   1.276701E-02   1.604478E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15        ev   16

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.08944177    -0.82247855     0.19899465     0.19804089     0.37298222    -0.19327010     0.10337496     0.07889811
 ref:   2    -0.10716837     0.05468919     0.52397983    -0.39888594     0.38271679     0.58750547     0.06186108    -0.05738254

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1     0.01026274    -0.01694550    -0.04636568    -0.01570620    -0.04072883    -0.00311545    -0.05089301     0.11420786
 ref:   2    -0.10417252     0.02904089    -0.03162058    -0.01988218    -0.01660978     0.03728722    -0.10088067    -0.05478454

 trial vector basis is being transformed.  new dimension:   8

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 54  1   -196.4750334540  2.0389E-03  5.8513E-03  1.5119E-01  1.0000E-04   
 mr-sdci # 54  2   -196.4725826048  1.4943E-11  0.0000E+00  7.0796E-04  1.0000E-04   
 mr-sdci # 54  3   -196.4725760978  1.8757E-09  0.0000E+00  4.9880E-03  1.0000E-04   
 mr-sdci # 54  4   -196.4705582883  5.5326E-03  0.0000E+00  1.5316E-01  1.0000E-04   
 mr-sdci # 54  5   -196.4644774233  2.4868E-08  0.0000E+00  3.6557E-03  1.0000E-04   
 mr-sdci # 54  6   -196.4635534486  6.6303E-02  0.0000E+00  9.2041E-02  1.0000E-04   
 mr-sdci # 54  7   -195.7419092125  4.0562E-03  0.0000E+00  2.8604E-01  1.0000E-04   
 mr-sdci # 54  8   -195.7359565971  1.2052E-03  0.0000E+00  3.2846E-01  1.0000E-04   
 mr-sdci # 54  9   -195.6830558215  8.1355E-02  0.0000E+00  5.4693E-01  1.0000E-04   
 mr-sdci # 54 10   -195.4895768086  2.8726E-02  0.0000E+00  1.0471E+00  1.0000E-04   
 mr-sdci # 54 11   -194.0670522593  3.5694E-02  0.0000E+00  2.3873E+00  1.0000E-04   
 mr-sdci # 54 12   -193.5016112274  1.2180E-01  0.0000E+00  2.5992E+00  1.0000E-04   
 mr-sdci # 54 13   -192.6887995206  3.4215E-02  0.0000E+00  3.8471E+00  1.0000E-04   
 mr-sdci # 54 14   -192.4004033286  8.1709E-03  0.0000E+00  3.3902E+00  1.0000E-04   
 mr-sdci # 54 15   -192.2780578128  3.9286E-01  0.0000E+00  4.0508E+00  1.0000E-04   
 mr-sdci # 54 16   -190.8049638984  2.4236E-02  0.0000E+00  4.5269E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.012000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  55

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58671866
   ht   2     0.00000000   -50.58426781
   ht   3     0.00000000     0.00000000   -50.58426130
   ht   4     0.00000000     0.00000000     0.00000000   -50.58224349
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616263
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57523865
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.85359442
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.84764180
   ht   9     1.14522941    -0.71761936     0.44787057     0.76289275     0.85197397     0.77133368     0.24064435     0.12561321

                ht   9
   ht   9    -0.21197902

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -3.617510E-03  -0.822921       0.200758      -0.189188      -0.377432       0.209702      -9.597352E-02   8.944270E-02
 ref    2  -1.157050E-03   5.496903E-02   0.520278       0.331203      -0.369368      -0.648201      -6.876224E-02  -5.042159E-02

              v      9
 ref    1  -3.996600E-02
 ref    2  -2.832814E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   1.442514E-05   0.680220       0.310993       0.145488       0.278887       0.464139       1.393916E-02   1.054233E-02

              v      9
 ref    1   2.399764E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00361751    -0.82292091     0.20075808    -0.18918841    -0.37743198     0.20970164    -0.09597352     0.08944270
 ref:   2    -0.00115705     0.05496903     0.52027756     0.33120309    -0.36936780    -0.64820092    -0.06876224    -0.05042159

                ci   9
 ref:   1    -0.03996600
 ref:   2    -0.02832814

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 55  1   -196.4833229377  8.2895E-03  0.0000E+00  1.1516E-01  1.0000E-04   
 mr-sdci # 55  2   -196.4725826054  5.4393E-10  1.7438E-07  7.0678E-04  1.0000E-04   
 mr-sdci # 55  3   -196.4725761206  2.2858E-08  0.0000E+00  5.0283E-03  1.0000E-04   
 mr-sdci # 55  4   -196.4724159937  1.8577E-03  0.0000E+00  2.5526E-02  1.0000E-04   
 mr-sdci # 55  5   -196.4644774868  6.3478E-08  0.0000E+00  3.6147E-03  1.0000E-04   
 mr-sdci # 55  6   -196.4643465425  7.9309E-04  0.0000E+00  2.1450E-02  1.0000E-04   
 mr-sdci # 55  7   -195.7457624822  3.8533E-03  0.0000E+00  2.4865E-01  1.0000E-04   
 mr-sdci # 55  8   -195.7361352203  1.7862E-04  0.0000E+00  3.2189E-01  1.0000E-04   
 mr-sdci # 55  9   -193.7266530352 -1.9564E+00  0.0000E+00  3.3610E+00  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.006000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  56

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58671866
   ht   2     0.00000000   -50.58426781
   ht   3     0.00000000     0.00000000   -50.58426130
   ht   4     0.00000000     0.00000000     0.00000000   -50.58224349
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616263
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57523865
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.85359442
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.84764180
   ht   9     1.14522941    -0.71761936     0.44787057     0.76289275     0.85197397     0.77133368     0.24064435     0.12561321
   ht  10     0.00308181     0.00729740    -0.00500744     0.00835429     0.00017037    -0.00175038    -0.00112895     0.00008313

                ht   9         ht  10
   ht   9    -0.21197902
   ht  10    -0.00006680    -0.00000746

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   3.631145E-03  -0.823291      -0.198536      -0.189910      -0.377448      -0.209656       0.113686      -6.658192E-02
 ref    2   1.086280E-03   5.332030E-02  -0.520461       0.331160      -0.369311       0.648247       5.782925E-02   6.265990E-02

              v      9       v     10
 ref    1   2.886018E-02   3.038151E-02
 ref    2  -4.141248E-03   6.312975E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   1.436522E-05   0.680651       0.310296       0.145733       0.278858       0.464180       1.626870E-02   8.359415E-03

              v      9       v     10
 ref    1   8.500599E-04   4.908402E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00363115    -0.82329107    -0.19853555    -0.18990970    -0.37744798    -0.20965586     0.11368589    -0.06658192
 ref:   2     0.00108628     0.05332030    -0.52046140     0.33116009    -0.36931121     0.64824735     0.05782925     0.06265990

                ci   9         ci  10
 ref:   1     0.02886018     0.03038151
 ref:   2    -0.00414125     0.06312975

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 56  1   -196.4833287080  5.7703E-06  0.0000E+00  1.1492E-01  1.0000E-04   
 mr-sdci # 56  2   -196.4725827258  1.2043E-07  0.0000E+00  3.9574E-04  1.0000E-04   
 mr-sdci # 56  3   -196.4725761232  2.5352E-09  6.1778E-06  5.0311E-03  1.0000E-04   
 mr-sdci # 56  4   -196.4724161565  1.6279E-07  0.0000E+00  2.5474E-02  1.0000E-04   
 mr-sdci # 56  5   -196.4644774882  1.3396E-09  0.0000E+00  3.6069E-03  1.0000E-04   
 mr-sdci # 56  6   -196.4643466337  9.1176E-08  0.0000E+00  2.1431E-02  1.0000E-04   
 mr-sdci # 56  7   -195.7476006539  1.8382E-03  0.0000E+00  2.4643E-01  1.0000E-04   
 mr-sdci # 56  8   -195.7384444750  2.3093E-03  0.0000E+00  3.2383E-01  1.0000E-04   
 mr-sdci # 56  9   -193.7885157511  6.1863E-02  0.0000E+00  3.1837E+00  1.0000E-04   
 mr-sdci # 56 10   -193.5462685280 -1.9433E+00  0.0000E+00  3.4835E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.006000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  57

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58671866
   ht   2     0.00000000   -50.58426781
   ht   3     0.00000000     0.00000000   -50.58426130
   ht   4     0.00000000     0.00000000     0.00000000   -50.58224349
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616263
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57523865
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.85359442
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.84764180
   ht   9     1.14522941    -0.71761936     0.44787057     0.76289275     0.85197397     0.77133368     0.24064435     0.12561321
   ht  10     0.00308181     0.00729740    -0.00500744     0.00835429     0.00017037    -0.00175038    -0.00112895     0.00008313
   ht  11    -0.01101318    -0.02369252    -0.01914990     0.01269586     0.01072053    -0.05346186     0.00265961     0.00304742

                ht   9         ht  10         ht  11
   ht   9    -0.21197902
   ht  10    -0.00006680    -0.00000746
   ht  11     0.00064981    -0.00000342    -0.00021836

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -3.140312E-03   0.821163      -0.210090       0.186578      -0.378901      -0.207126       0.118336      -6.055656E-02
 ref    2  -6.274540E-04  -5.858728E-02  -0.514357      -0.339787      -0.364948       0.650601       5.307677E-02   6.690533E-02

              v      9       v     10       v     11
 ref    1   4.582243E-02  -1.206970E-02  -4.804186E-02
 ref    2  -2.761829E-02  -7.695104E-02   1.488890E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   1.025526E-05   0.677741       0.308701       0.150267       0.276753       0.466183       1.682044E-02   8.143420E-03

              v      9       v     10       v     11
 ref    1   2.862466E-03   6.067141E-03   2.529699E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00314031     0.82116265    -0.21008951     0.18657828    -0.37890105    -0.20712568     0.11833554    -0.06055656
 ref:   2    -0.00062745    -0.05858728    -0.51435738    -0.33978687    -0.36494841     0.65060144     0.05307677     0.06690533

                ci   9         ci  10         ci  11
 ref:   1     0.04582243    -0.01206970    -0.04804186
 ref:   2    -0.02761829    -0.07695104     0.01488890

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 57  1   -196.4833763884  4.7680E-05  0.0000E+00  1.1222E-01  1.0000E-04   
 mr-sdci # 57  2   -196.4725827261  2.8709E-10  0.0000E+00  3.9575E-04  1.0000E-04   
 mr-sdci # 57  3   -196.4725808007  4.6776E-06  0.0000E+00  2.2703E-03  1.0000E-04   
 mr-sdci # 57  4   -196.4724177242  1.5678E-06  2.1839E-04  2.4878E-02  1.0000E-04   
 mr-sdci # 57  5   -196.4644779256  4.3748E-07  0.0000E+00  3.0347E-03  1.0000E-04   
 mr-sdci # 57  6   -196.4643484443  1.8106E-06  0.0000E+00  2.0831E-02  1.0000E-04   
 mr-sdci # 57  7   -195.7481167162  5.1606E-04  0.0000E+00  2.3546E-01  1.0000E-04   
 mr-sdci # 57  8   -195.7390190802  5.7461E-04  0.0000E+00  3.2222E-01  1.0000E-04   
 mr-sdci # 57  9   -193.8400039012  5.1488E-02  0.0000E+00  3.1721E+00  1.0000E-04   
 mr-sdci # 57 10   -193.5605105887  1.4242E-02  0.0000E+00  3.4797E+00  1.0000E-04   
 mr-sdci # 57 11   -193.4875657333 -5.7949E-01  0.0000E+00  3.5583E+00  1.0000E-04   
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.005000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  58

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58671866
   ht   2     0.00000000   -50.58426781
   ht   3     0.00000000     0.00000000   -50.58426130
   ht   4     0.00000000     0.00000000     0.00000000   -50.58224349
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616263
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57523865
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.85359442
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.84764180
   ht   9     1.14522941    -0.71761936     0.44787057     0.76289275     0.85197397     0.77133368     0.24064435     0.12561321
   ht  10     0.00308181     0.00729740    -0.00500744     0.00835429     0.00017037    -0.00175038    -0.00112895     0.00008313
   ht  11    -0.01101318    -0.02369252    -0.01914990     0.01269586     0.01072053    -0.05346186     0.00265961     0.00304742
   ht  12     0.36182938    -0.18202245     0.12024901     0.14496863     0.25405326     0.08494442     0.03504321    -0.00220670

                ht   9         ht  10         ht  11         ht  12
   ht   9    -0.21197902
   ht  10    -0.00006680    -0.00000746
   ht  11     0.00064981    -0.00000342    -0.00021836
   ht  12    -0.01477626    -0.00000200    -0.00004950    -0.01072226

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   6.339158E-04  -0.819829      -0.223977      -0.172149      -0.389334       0.190363       0.106273       7.698152E-02
 ref    2  -8.281799E-05   5.860461E-02  -0.491666       0.362642      -0.330376      -0.673862       6.105905E-02  -5.891156E-02

              v      9       v     10       v     11       v     12
 ref    1   2.715792E-02   3.642989E-02  -5.244034E-02  -7.550977E-02
 ref    2   3.553546E-03  -7.181224E-02  -3.325235E-03  -5.338454E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   4.087081E-07   0.675553       0.291901       0.161144       0.260729       0.490327       1.502222E-02   9.396727E-03

              v      9       v     10       v     11       v     12
 ref    1   7.501805E-04   6.484134E-03   2.761047E-03   8.551634E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00063392    -0.81982851    -0.22397679    -0.17214925    -0.38933360     0.19036305     0.10627330     0.07698152
 ref:   2    -0.00008282     0.05860461    -0.49166556     0.36264176    -0.33037597    -0.67386154     0.06105905    -0.05891156

                ci   9         ci  10         ci  11         ci  12
 ref:   1     0.02715792     0.03642989    -0.05244034    -0.07550977
 ref:   2     0.00355355    -0.07181224    -0.00332524    -0.05338454

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 58  1   -196.4872691553  3.8928E-03  0.0000E+00  5.4085E-02  1.0000E-04   
 mr-sdci # 58  2   -196.4725827264  3.5141E-10  0.0000E+00  3.9516E-04  1.0000E-04   
 mr-sdci # 58  3   -196.4725808812  8.0491E-08  0.0000E+00  2.2170E-03  1.0000E-04   
 mr-sdci # 58  4   -196.4725639248  1.4620E-04  0.0000E+00  9.4967E-03  1.0000E-04   
 mr-sdci # 58  5   -196.4644779861  6.0477E-08  3.0516E-06  3.1009E-03  1.0000E-04   
 mr-sdci # 58  6   -196.4644588399  1.1040E-04  0.0000E+00  9.9418E-03  1.0000E-04   
 mr-sdci # 58  7   -195.7514605747  3.3439E-03  0.0000E+00  2.6330E-01  1.0000E-04   
 mr-sdci # 58  8   -195.7396090730  5.8999E-04  0.0000E+00  3.1327E-01  1.0000E-04   
 mr-sdci # 58  9   -195.2799865900  1.4400E+00  0.0000E+00  1.5484E+00  1.0000E-04   
 mr-sdci # 58 10   -193.7063738134  1.4586E-01  0.0000E+00  3.2704E+00  1.0000E-04   
 mr-sdci # 58 11   -193.4926273438  5.0616E-03  0.0000E+00  3.5636E+00  1.0000E-04   
 mr-sdci # 58 12   -191.2242821726 -2.2773E+00  0.0000E+00  3.9760E+00  1.0000E-04   
 
 root number  5 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.007000
time for eigenvalue solver             0.001000
time for vector access                 0.000000

          starting ci iteration  59

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58671866
   ht   2     0.00000000   -50.58426781
   ht   3     0.00000000     0.00000000   -50.58426130
   ht   4     0.00000000     0.00000000     0.00000000   -50.58224349
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616263
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57523865
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.85359442
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.84764180
   ht   9     1.14522941    -0.71761936     0.44787057     0.76289275     0.85197397     0.77133368     0.24064435     0.12561321
   ht  10     0.00308181     0.00729740    -0.00500744     0.00835429     0.00017037    -0.00175038    -0.00112895     0.00008313
   ht  11    -0.01101318    -0.02369252    -0.01914990     0.01269586     0.01072053    -0.05346186     0.00265961     0.00304742
   ht  12     0.36182938    -0.18202245     0.12024901     0.14496863     0.25405326     0.08494442     0.03504321    -0.00220670
   ht  13     0.00632661    -0.01037568    -0.01467825     0.00109121    -0.03493931    -0.00803694    -0.00085156    -0.00067409

                ht   9         ht  10         ht  11         ht  12         ht  13
   ht   9    -0.21197902
   ht  10    -0.00006680    -0.00000746
   ht  11     0.00064981    -0.00000342    -0.00021836
   ht  12    -0.01477626    -0.00000200    -0.00004950    -0.01072226
   ht  13     0.00051797     0.00000055    -0.00001442     0.00010337    -0.00010610

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   6.604904E-04  -0.819826       0.223964      -0.172135      -0.392013      -0.184804      -0.100043       8.477191E-02
 ref    2  -9.387389E-05   5.859910E-02   0.491692       0.362612      -0.320656       0.678507      -6.726294E-02  -5.474384E-02

              v      9       v     10       v     11       v     12       v     13
 ref    1  -2.787456E-02  -2.734659E-02  -3.725283E-02   4.263774E-02   7.747185E-02
 ref    2  -1.351156E-03   3.404455E-02   7.228214E-02   2.921127E-02   6.033202E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   4.450598E-07   0.675549       0.291921       0.161118       0.256495       0.494524       1.453295E-02   1.018316E-02

              v      9       v     10       v     11       v     12       v     13
 ref    1   7.788169E-04   1.906867E-03   6.612481E-03   2.671275E-03   9.641839E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00066049    -0.81982614     0.22396419    -0.17213479    -0.39201323    -0.18480396    -0.10004320     0.08477191
 ref:   2    -0.00009387     0.05859910     0.49169164     0.36261180    -0.32065649     0.67850682    -0.06726294    -0.05474384

                ci   9         ci  10         ci  11         ci  12         ci  13
 ref:   1    -0.02787456    -0.02734659    -0.03725283     0.04263774     0.07747185
 ref:   2    -0.00135116     0.03404455     0.07228214     0.02921127     0.06033202

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 59  1   -196.4872726257  3.4704E-06  0.0000E+00  5.3588E-02  1.0000E-04   
 mr-sdci # 59  2   -196.4725827264  2.5580E-13  0.0000E+00  3.9517E-04  1.0000E-04   
 mr-sdci # 59  3   -196.4725808812  4.0004E-12  0.0000E+00  2.2175E-03  1.0000E-04   
 mr-sdci # 59  4   -196.4725642867  3.6184E-07  0.0000E+00  9.3665E-03  1.0000E-04   
 mr-sdci # 59  5   -196.4644802228  2.2367E-06  0.0000E+00  1.5460E-03  1.0000E-04   
 mr-sdci # 59  6   -196.4644588769  3.7011E-08  2.2332E-05  9.8632E-03  1.0000E-04   
 mr-sdci # 59  7   -195.7544652239  3.0046E-03  0.0000E+00  2.4629E-01  1.0000E-04   
 mr-sdci # 59  8   -195.7399302476  3.2117E-04  0.0000E+00  3.1825E-01  1.0000E-04   
 mr-sdci # 59  9   -195.2852663717  5.2798E-03  0.0000E+00  1.5167E+00  1.0000E-04   
 mr-sdci # 59 10   -193.9063420227  1.9997E-01  0.0000E+00  3.0525E+00  1.0000E-04   
 mr-sdci # 59 11   -193.7063183003  2.1369E-01  0.0000E+00  3.2756E+00  1.0000E-04   
 mr-sdci # 59 12   -193.2793302869  2.0550E+00  0.0000E+00  3.4966E+00  1.0000E-04   
 mr-sdci # 59 13   -191.1794889113 -1.5093E+00  0.0000E+00  4.0405E+00  1.0000E-04   
 
 root number  6 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.008000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  60

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58671866
   ht   2     0.00000000   -50.58426781
   ht   3     0.00000000     0.00000000   -50.58426130
   ht   4     0.00000000     0.00000000     0.00000000   -50.58224349
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616263
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57523865
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.85359442
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.84764180
   ht   9     1.14522941    -0.71761936     0.44787057     0.76289275     0.85197397     0.77133368     0.24064435     0.12561321
   ht  10     0.00308181     0.00729740    -0.00500744     0.00835429     0.00017037    -0.00175038    -0.00112895     0.00008313
   ht  11    -0.01101318    -0.02369252    -0.01914990     0.01269586     0.01072053    -0.05346186     0.00265961     0.00304742
   ht  12     0.36182938    -0.18202245     0.12024901     0.14496863     0.25405326     0.08494442     0.03504321    -0.00220670
   ht  13     0.00632661    -0.01037568    -0.01467825     0.00109121    -0.03493931    -0.00803694    -0.00085156    -0.00067409
   ht  14    -0.05345584     0.02683892    -0.03429918    -0.04318809    -0.02348982     0.04685540    -0.00725950     0.00005426

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14
   ht   9    -0.21197902
   ht  10    -0.00006680    -0.00000746
   ht  11     0.00064981    -0.00000342    -0.00021836
   ht  12    -0.01477626    -0.00000200    -0.00004950    -0.01072226
   ht  13     0.00051797     0.00000055    -0.00001442     0.00010337    -0.00010610
   ht  14     0.00340542     0.00000322     0.00005549     0.00048883     0.00001683    -0.00064403

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -2.242965E-04  -0.820733       0.229288      -0.160100      -0.404925       0.154769      -0.100741      -8.318997E-02
 ref    2   4.507725E-04   5.972164E-02   0.478213       0.379066      -0.268216      -0.701354      -6.444660E-02   5.601507E-02

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1  -2.370845E-02  -2.760748E-02  -3.892353E-02   2.575482E-02   3.300720E-02   9.372350E-02
 ref    2  -2.097792E-02   3.966854E-02   6.747254E-02   5.044584E-02  -4.404260E-02   4.190787E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   2.535048E-07   0.677169       0.281261       0.169323       0.235904       0.515851       1.430212E-02   1.005826E-02

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1   1.002164E-03   2.335766E-03   6.067585E-03   3.208094E-03   3.029226E-03   1.054036E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00022430    -0.82073310     0.22928824    -0.16010024    -0.40492513     0.15476916    -0.10074103    -0.08318997
 ref:   2     0.00045077     0.05972164     0.47821342     0.37906598    -0.26821589    -0.70135410    -0.06444660     0.05601507

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14
 ref:   1    -0.02370845    -0.02760748    -0.03892353     0.02575482     0.03300720     0.09372350
 ref:   2    -0.02097792     0.03966854     0.06747254     0.05044584    -0.04404260     0.04190787

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 60  1   -196.4877517612  4.7914E-04  1.9284E-04  2.5985E-02  1.0000E-04   
 mr-sdci # 60  2   -196.4725827267  2.3266E-10  0.0000E+00  3.9734E-04  1.0000E-04   
 mr-sdci # 60  3   -196.4725808931  1.1905E-08  0.0000E+00  2.2490E-03  1.0000E-04   
 mr-sdci # 60  4   -196.4725750980  1.0811E-05  0.0000E+00  5.4045E-03  1.0000E-04   
 mr-sdci # 60  5   -196.4644802522  2.9386E-08  0.0000E+00  1.5018E-03  1.0000E-04   
 mr-sdci # 60  6   -196.4644761258  1.7249E-05  0.0000E+00  3.8587E-03  1.0000E-04   
 mr-sdci # 60  7   -195.7560535669  1.5883E-03  0.0000E+00  2.3279E-01  1.0000E-04   
 mr-sdci # 60  8   -195.7399628070  3.2559E-05  0.0000E+00  3.1880E-01  1.0000E-04   
 mr-sdci # 60  9   -195.5146288743  2.2936E-01  0.0000E+00  8.3752E-01  1.0000E-04   
 mr-sdci # 60 10   -193.9445817791  3.8240E-02  0.0000E+00  2.8782E+00  1.0000E-04   
 mr-sdci # 60 11   -193.7164844124  1.0166E-02  0.0000E+00  3.2655E+00  1.0000E-04   
 mr-sdci # 60 12   -193.4068557951  1.2753E-01  0.0000E+00  3.5793E+00  1.0000E-04   
 mr-sdci # 60 13   -192.6134356569  1.4339E+00  0.0000E+00  3.4538E+00  1.0000E-04   
 mr-sdci # 60 14   -190.9134816361 -1.4869E+00  0.0000E+00  4.1635E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.014000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  61

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58671866
   ht   2     0.00000000   -50.58426781
   ht   3     0.00000000     0.00000000   -50.58426130
   ht   4     0.00000000     0.00000000     0.00000000   -50.58224349
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616263
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57523865
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.85359442
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.84764180
   ht   9     1.14522941    -0.71761936     0.44787057     0.76289275     0.85197397     0.77133368     0.24064435     0.12561321
   ht  10     0.00308181     0.00729740    -0.00500744     0.00835429     0.00017037    -0.00175038    -0.00112895     0.00008313
   ht  11    -0.01101318    -0.02369252    -0.01914990     0.01269586     0.01072053    -0.05346186     0.00265961     0.00304742
   ht  12     0.36182938    -0.18202245     0.12024901     0.14496863     0.25405326     0.08494442     0.03504321    -0.00220670
   ht  13     0.00632661    -0.01037568    -0.01467825     0.00109121    -0.03493931    -0.00803694    -0.00085156    -0.00067409
   ht  14    -0.05345584     0.02683892    -0.03429918    -0.04318809    -0.02348982     0.04685540    -0.00725950     0.00005426
   ht  15     0.10785697     0.06465332     0.16740102     0.06263594    -0.06183108    -0.11471111     0.00145264     0.01103459

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15
   ht   9    -0.21197902
   ht  10    -0.00006680    -0.00000746
   ht  11     0.00064981    -0.00000342    -0.00021836
   ht  12    -0.01477626    -0.00000200    -0.00004950    -0.01072226
   ht  13     0.00051797     0.00000055    -0.00001442     0.00010337    -0.00010610
   ht  14     0.00340542     0.00000322     0.00005549     0.00048883     0.00001683    -0.00064403
   ht  15     0.00020789    -0.00000997     0.00012308    -0.00100826    -0.00002235     0.00031854    -0.00588714

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   3.602984E-05   0.821451      -0.247893      -0.124739      -0.412582       0.133084       9.853136E-02  -8.353518E-02
 ref    2   2.455967E-04  -6.011747E-02  -0.421258       0.441297      -0.230560      -0.714689       5.913951E-02   5.573728E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1  -2.902670E-02   4.123966E-02   1.025938E-02   2.792579E-02   2.078493E-02  -3.638597E-02  -9.262034E-02
 ref    2  -3.941197E-02  -6.722979E-02  -2.556035E-02  -1.931306E-02   5.462716E-02   4.152174E-02  -4.303364E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   6.161588E-08   0.678396       0.238909       0.210303       0.223382       0.528492       1.320591E-02   1.008477E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1   2.395852E-03   6.220554E-03   7.585864E-04   1.152844E-03   3.416140E-03   3.047994E-03   1.043042E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00003603     0.82145082    -0.24789279    -0.12473865    -0.41258187     0.13308422     0.09853136    -0.08353518
 ref:   2     0.00024560    -0.06011747    -0.42125784     0.44129711    -0.23056035    -0.71468931     0.05913951     0.05573728

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15
 ref:   1    -0.02902670     0.04123966     0.01025938     0.02792579     0.02078493    -0.03638597    -0.09262034
 ref:   2    -0.03941197    -0.06722979    -0.02556035    -0.01931306     0.05462716     0.04152174    -0.04303364

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 61  1   -196.4879137636  1.6200E-04  0.0000E+00  1.2874E-02  1.0000E-04   
 mr-sdci # 61  2   -196.4725827267  7.1211E-11  6.1643E-08  3.9607E-04  1.0000E-04   
 mr-sdci # 61  3   -196.4725809884  9.5276E-08  0.0000E+00  2.3262E-03  1.0000E-04   
 mr-sdci # 61  4   -196.4725782697  3.1716E-06  0.0000E+00  3.6305E-03  1.0000E-04   
 mr-sdci # 61  5   -196.4644802589  6.7281E-09  0.0000E+00  1.4849E-03  1.0000E-04   
 mr-sdci # 61  6   -196.4644787460  2.6202E-06  0.0000E+00  3.0480E-03  1.0000E-04   
 mr-sdci # 61  7   -195.7600829405  4.0294E-03  0.0000E+00  2.2407E-01  1.0000E-04   
 mr-sdci # 61  8   -195.7399636003  7.9331E-07  0.0000E+00  3.1857E-01  1.0000E-04   
 mr-sdci # 61  9   -195.6207594390  1.0613E-01  0.0000E+00  6.3056E-01  1.0000E-04   
 mr-sdci # 61 10   -194.3504183072  4.0584E-01  0.0000E+00  2.7692E+00  1.0000E-04   
 mr-sdci # 61 11   -193.8315157544  1.1503E-01  0.0000E+00  2.8606E+00  1.0000E-04   
 mr-sdci # 61 12   -193.4812357722  7.4380E-02  0.0000E+00  3.5796E+00  1.0000E-04   
 mr-sdci # 61 13   -193.3990092113  7.8557E-01  0.0000E+00  3.4518E+00  1.0000E-04   
 mr-sdci # 61 14   -192.0398816741  1.1264E+00  0.0000E+00  3.2251E+00  1.0000E-04   
 mr-sdci # 61 15   -190.9091975246 -1.3689E+00  0.0000E+00  4.1461E+00  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  62

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58671866
   ht   2     0.00000000   -50.58426781
   ht   3     0.00000000     0.00000000   -50.58426130
   ht   4     0.00000000     0.00000000     0.00000000   -50.58224349
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616263
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57523865
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.85359442
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.84764180
   ht   9     1.14522941    -0.71761936     0.44787057     0.76289275     0.85197397     0.77133368     0.24064435     0.12561321
   ht  10     0.00308181     0.00729740    -0.00500744     0.00835429     0.00017037    -0.00175038    -0.00112895     0.00008313
   ht  11    -0.01101318    -0.02369252    -0.01914990     0.01269586     0.01072053    -0.05346186     0.00265961     0.00304742
   ht  12     0.36182938    -0.18202245     0.12024901     0.14496863     0.25405326     0.08494442     0.03504321    -0.00220670
   ht  13     0.00632661    -0.01037568    -0.01467825     0.00109121    -0.03493931    -0.00803694    -0.00085156    -0.00067409
   ht  14    -0.05345584     0.02683892    -0.03429918    -0.04318809    -0.02348982     0.04685540    -0.00725950     0.00005426
   ht  15     0.10785697     0.06465332     0.16740102     0.06263594    -0.06183108    -0.11471111     0.00145264     0.01103459
   ht  16     0.00147635     0.00623220    -0.00317874     0.00501120     0.00052536    -0.00063447    -0.00071630    -0.00052644

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15         ht  16
   ht   9    -0.21197902
   ht  10    -0.00006680    -0.00000746
   ht  11     0.00064981    -0.00000342    -0.00021836
   ht  12    -0.01477626    -0.00000200    -0.00004950    -0.01072226
   ht  13     0.00051797     0.00000055    -0.00001442     0.00010337    -0.00010610
   ht  14     0.00340542     0.00000322     0.00005549     0.00048883     0.00001683    -0.00064403
   ht  15     0.00020789    -0.00000997     0.00012308    -0.00100826    -0.00002235     0.00031854    -0.00588714
   ht  16     0.00001703    -0.00000151    -0.00000064     0.00000492     0.00000043     0.00000125    -0.00000019    -0.00000311

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   6.201206E-06   0.820422       0.253442       0.120356       0.412623      -0.132958       0.111138      -1.820323E-02
 ref    2   2.393661E-04  -6.566764E-02   0.421593      -0.440182       0.230342       0.714758       2.536317E-02   7.708869E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1  -5.075559E-03   7.890727E-02   4.500177E-02  -6.829054E-03  -3.107443E-02   3.264190E-02   7.958412E-02   8.955449E-02
 ref    2  -3.870060E-02   1.069162E-02  -7.246926E-02  -1.035759E-02   8.754790E-03  -4.679447E-02   2.969494E-02   8.650582E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   5.733456E-08   0.677404       0.241974       0.208245       0.223315       0.528558       1.299506E-02   6.274024E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   1.523498E-03   6.340668E-03   7.276953E-03   1.539156E-04   1.042267E-03   3.255216E-03   7.215421E-03   1.550326E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15        ev   16

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00000620     0.82042152     0.25344209     0.12035561     0.41262275    -0.13295848     0.11113850    -0.01820323
 ref:   2     0.00023937    -0.06566764     0.42159349    -0.44018163     0.23034157     0.71475847     0.02536317     0.07708869

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1    -0.00507556     0.07890727     0.04500177    -0.00682905    -0.03107443     0.03264190     0.07958412     0.08955449
 ref:   2    -0.03870060     0.01069162    -0.07246926    -0.01035759     0.00875479    -0.04679447     0.02969494     0.08650582

 trial vector basis is being transformed.  new dimension:   8

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 62  1   -196.4879140617  2.9818E-07  0.0000E+00  1.2899E-02  1.0000E-04   
 mr-sdci # 62  2   -196.4725827953  6.8523E-08  0.0000E+00  1.4175E-04  1.0000E-04   
 mr-sdci # 62  3   -196.4725809907  2.2597E-09  1.6748E-06  2.3177E-03  1.0000E-04   
 mr-sdci # 62  4   -196.4725782798  1.0166E-08  0.0000E+00  3.6298E-03  1.0000E-04   
 mr-sdci # 62  5   -196.4644802590  7.9460E-11  0.0000E+00  1.4856E-03  1.0000E-04   
 mr-sdci # 62  6   -196.4644787487  2.7007E-09  0.0000E+00  3.0420E-03  1.0000E-04   
 mr-sdci # 62  7   -195.7708963160  1.0813E-02  0.0000E+00  2.6422E-01  1.0000E-04   
 mr-sdci # 62  8   -195.7490691139  9.1055E-03  0.0000E+00  3.1338E-01  1.0000E-04   
 mr-sdci # 62  9   -195.6255757231  4.8163E-03  0.0000E+00  6.4536E-01  1.0000E-04   
 mr-sdci # 62 10   -195.5400798732  1.1897E+00  0.0000E+00  6.2924E-01  1.0000E-04   
 mr-sdci # 62 11   -194.3339963020  5.0248E-01  0.0000E+00  2.7844E+00  1.0000E-04   
 mr-sdci # 62 12   -193.6917719907  2.1054E-01  0.0000E+00  2.8619E+00  1.0000E-04   
 mr-sdci # 62 13   -193.4781313790  7.9122E-02  0.0000E+00  3.6431E+00  1.0000E-04   
 mr-sdci # 62 14   -192.0436972321  3.8156E-03  0.0000E+00  3.2596E+00  1.0000E-04   
 mr-sdci # 62 15   -190.9136412860  4.4438E-03  0.0000E+00  4.1384E+00  1.0000E-04   
 mr-sdci # 62 16   -190.7326844682 -7.2279E-02  0.0000E+00  3.7477E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.011000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  63

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59959927
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426619
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426348
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616546
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616395
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88258152
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.86075432
   ht   9     0.00018787     0.01882707    -0.00455561    -0.00014035     0.01057817    -0.03666724     0.00272427    -0.00058851

                ht   9
   ht   9    -0.00007449

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   2.225946E-05  -0.820647      -0.257112       0.110535       0.413667      -0.129716       0.111179       1.794562E-02
 ref    2  -3.801213E-04   6.532113E-02  -0.404559      -0.456014       0.224661       0.716563       2.533545E-02  -7.664682E-02

              v      9
 ref    1  -5.774460E-02
 ref    2   8.042211E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   1.449877E-07   0.677728       0.229774       0.220167       0.221593       0.530288       1.300275E-02   6.196780E-03

              v      9
 ref    1   9.802155E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00002226    -0.82064697    -0.25711174     0.11053473     0.41366654    -0.12971584     0.11117942     0.01794562
 ref:   2    -0.00038012     0.06532113    -0.40455872    -0.45601380     0.22466141     0.71656262     0.02533545    -0.07664682

                ci   9
 ref:   1    -0.05774460
 ref:   2     0.08042211

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 63  1   -196.4879167699  2.7081E-06  0.0000E+00  1.2460E-02  1.0000E-04   
 mr-sdci # 63  2   -196.4725827953  8.7397E-13  0.0000E+00  1.4150E-04  1.0000E-04   
 mr-sdci # 63  3   -196.4725821047  1.1140E-06  0.0000E+00  1.2005E-03  1.0000E-04   
 mr-sdci # 63  4   -196.4725782935  1.3636E-08  4.0079E-06  3.5719E-03  1.0000E-04   
 mr-sdci # 63  5   -196.4644802594  3.9780E-10  0.0000E+00  1.4854E-03  1.0000E-04   
 mr-sdci # 63  6   -196.4644790403  2.9155E-07  0.0000E+00  2.7209E-03  1.0000E-04   
 mr-sdci # 63  7   -195.7708981596  1.8436E-06  0.0000E+00  2.6388E-01  1.0000E-04   
 mr-sdci # 63  8   -195.7491409184  7.1805E-05  0.0000E+00  3.1680E-01  1.0000E-04   
 mr-sdci # 63  9   -193.3239453479 -2.3016E+00  0.0000E+00  3.5998E+00  1.0000E-04   
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  64

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59959927
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426619
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426348
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616546
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616395
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88258152
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.86075432
   ht   9     0.00018787     0.01882707    -0.00455561    -0.00014035     0.01057817    -0.03666724     0.00272427    -0.00058851
   ht  10    -0.00997241    -0.02178427     0.00681963    -0.00955883    -0.00168974     0.00770388    -0.00001916     0.00622094

                ht   9         ht  10
   ht   9    -0.00007449
   ht  10     0.00002039    -0.00013639

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   2.437069E-06   0.817721      -0.286653      -3.087083E-02  -0.416347      -0.120838       0.109470       2.907450E-02
 ref    2  -3.396708E-04  -6.047184E-02  -0.233205       0.563783      -0.209270       0.721219       3.226901E-02  -7.158803E-02

              v      9       v     10
 ref    1   2.734645E-02   5.368699E-02
 ref    2  -4.544225E-02  -7.428771E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   1.153822E-07   0.672325       0.136554       0.318804       0.217139       0.534759       1.302499E-02   5.970173E-03

              v      9       v     10
 ref    1   2.812826E-03   8.400956E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00000244     0.81772125    -0.28665290    -0.03087083    -0.41634748    -0.12083808     0.10947011     0.02907450
 ref:   2    -0.00033967    -0.06047184    -0.23320467     0.56378292    -0.20926968     0.72121909     0.03226901    -0.07158803

                ci   9         ci  10
 ref:   1     0.02734645     0.05368699
 ref:   2    -0.04544225    -0.07428771

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 64  1   -196.4879171437  3.7381E-07  0.0000E+00  1.2787E-02  1.0000E-04   
 mr-sdci # 64  2   -196.4725827956  3.0291E-10  0.0000E+00  1.3762E-04  1.0000E-04   
 mr-sdci # 64  3   -196.4725821694  6.4749E-08  0.0000E+00  1.1053E-03  1.0000E-04   
 mr-sdci # 64  4   -196.4725816353  3.3418E-06  0.0000E+00  1.6409E-03  1.0000E-04   
 mr-sdci # 64  5   -196.4644802800  2.0658E-08  8.2941E-07  1.4357E-03  1.0000E-04   
 mr-sdci # 64  6   -196.4644790729  3.2600E-08  0.0000E+00  2.6454E-03  1.0000E-04   
 mr-sdci # 64  7   -195.7726793623  1.7812E-03  0.0000E+00  2.5478E-01  1.0000E-04   
 mr-sdci # 64  8   -195.7522540650  3.1131E-03  0.0000E+00  3.1715E-01  1.0000E-04   
 mr-sdci # 64  9   -194.5566230297  1.2327E+00  0.0000E+00  2.4455E+00  1.0000E-04   
 mr-sdci # 64 10   -193.2889731699 -2.2511E+00  0.0000E+00  3.5869E+00  1.0000E-04   
 
 root number  5 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.006000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  65

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59959927
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426619
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426348
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616546
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616395
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88258152
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.86075432
   ht   9     0.00018787     0.01882707    -0.00455561    -0.00014035     0.01057817    -0.03666724     0.00272427    -0.00058851
   ht  10    -0.00997241    -0.02178427     0.00681963    -0.00955883    -0.00168974     0.00770388    -0.00001916     0.00622094
   ht  11     0.00110675    -0.00677680     0.01001852    -0.01139580     0.02696277     0.00490964     0.00132535     0.00332869

                ht   9         ht  10         ht  11
   ht   9    -0.00007449
   ht  10     0.00002039    -0.00013639
   ht  11     0.00000136    -0.00000353    -0.00004313

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   5.079895E-06  -0.817601       0.287281      -2.810829E-02   0.414101       0.128355      -0.108087      -3.267702E-02
 ref    2  -3.351817E-04   6.028314E-02   0.226955       0.566341       0.222330      -0.717314      -3.299468E-02   6.937902E-02

              v      9       v     10       v     11
 ref    1  -3.326495E-02  -8.791618E-03   5.576771E-02
 ref    2   1.724420E-02  -0.100887      -6.183071E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   1.123726E-07   0.672106       0.134039       0.321533       0.220910       0.531014       1.277149E-02   5.881235E-03

              v      9       v     10       v     11
 ref    1   1.403919E-03   1.025552E-02   6.933074E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00000508    -0.81760144     0.28728068    -0.02810829     0.41410116     0.12835514    -0.10808721    -0.03267702
 ref:   2    -0.00033518     0.06028314     0.22695474     0.56634131     0.22232973    -0.71731392    -0.03299468     0.06937902

                ci   9         ci  10         ci  11
 ref:   1    -0.03326495    -0.00879162     0.05576771
 ref:   2     0.01724420    -0.10088721    -0.06183071

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 65  1   -196.4879171524  8.7288E-09  0.0000E+00  1.2798E-02  1.0000E-04   
 mr-sdci # 65  2   -196.4725827956  2.1984E-11  0.0000E+00  1.3969E-04  1.0000E-04   
 mr-sdci # 65  3   -196.4725821718  2.3936E-09  0.0000E+00  1.1024E-03  1.0000E-04   
 mr-sdci # 65  4   -196.4725816494  1.4174E-08  0.0000E+00  1.6084E-03  1.0000E-04   
 mr-sdci # 65  5   -196.4644808422  5.6217E-07  0.0000E+00  8.7828E-04  1.0000E-04   
 mr-sdci # 65  6   -196.4644790741  1.2443E-09  1.8555E-06  2.6322E-03  1.0000E-04   
 mr-sdci # 65  7   -195.7732619566  5.8259E-04  0.0000E+00  2.5257E-01  1.0000E-04   
 mr-sdci # 65  8   -195.7528334462  5.7938E-04  0.0000E+00  3.1885E-01  1.0000E-04   
 mr-sdci # 65  9   -194.6387279511  8.2105E-02  0.0000E+00  2.3804E+00  1.0000E-04   
 mr-sdci # 65 10   -193.7363574004  4.4738E-01  0.0000E+00  3.1781E+00  1.0000E-04   
 mr-sdci # 65 11   -193.2808119042 -1.0532E+00  0.0000E+00  3.5848E+00  1.0000E-04   
 
 root number  6 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.008000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  66

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59959927
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426619
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426348
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616546
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616395
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88258152
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.86075432
   ht   9     0.00018787     0.01882707    -0.00455561    -0.00014035     0.01057817    -0.03666724     0.00272427    -0.00058851
   ht  10    -0.00997241    -0.02178427     0.00681963    -0.00955883    -0.00168974     0.00770388    -0.00001916     0.00622094
   ht  11     0.00110675    -0.00677680     0.01001852    -0.01139580     0.02696277     0.00490964     0.00132535     0.00332869
   ht  12     0.02400742    -0.00764709     0.01622301    -0.00621874    -0.00269382    -0.00920184     0.00055849     0.00494461

                ht   9         ht  10         ht  11         ht  12
   ht   9    -0.00007449
   ht  10     0.00002039    -0.00013639
   ht  11     0.00000136    -0.00000353    -0.00004313
   ht  12    -0.00000259    -0.00000371    -0.00000247    -0.00006380

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -5.033211E-05  -0.817589       0.287471      -2.647572E-02   0.430040      -5.498968E-02  -0.108275       2.962518E-02
 ref    2   4.100567E-04   6.022084E-02   0.223524       0.567724       9.523083E-02   0.744890      -3.084077E-02  -7.045616E-02

              v      9       v     10       v     11       v     12
 ref    1   3.947759E-02   1.153456E-02   4.803083E-03   5.485766E-02
 ref    2  -4.890733E-03   8.000129E-02  -6.775450E-02  -5.913505E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   1.706798E-07   0.672079       0.132602       0.323012       0.194003       0.557886       1.267457E-02   5.841721E-03

              v      9       v     10       v     11       v     12
 ref    1   1.582399E-03   6.533252E-03   4.613742E-03   6.506317E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00005033    -0.81758944     0.28747088    -0.02647572     0.43003973    -0.05498968    -0.10827474     0.02962518
 ref:   2     0.00041006     0.06022084     0.22352392     0.56772444     0.09523083     0.74489042    -0.03084077    -0.07045616

                ci   9         ci  10         ci  11         ci  12
 ref:   1     0.03947759     0.01153456     0.00480308     0.05485766
 ref:   2    -0.00489073     0.08000129    -0.06775450    -0.05913505

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 66  1   -196.4879231435  5.9911E-06  3.6183E-05  1.2793E-02  1.0000E-04   
 mr-sdci # 66  2   -196.4725827956  8.3134E-13  0.0000E+00  1.3955E-04  1.0000E-04   
 mr-sdci # 66  3   -196.4725821722  4.1975E-10  0.0000E+00  1.1098E-03  1.0000E-04   
 mr-sdci # 66  4   -196.4725816722  2.2734E-08  0.0000E+00  1.6514E-03  1.0000E-04   
 mr-sdci # 66  5   -196.4644808511  8.9126E-09  0.0000E+00  8.9844E-04  1.0000E-04   
 mr-sdci # 66  6   -196.4644805938  1.5197E-06  0.0000E+00  1.2751E-03  1.0000E-04   
 mr-sdci # 66  7   -195.7740195262  7.5757E-04  0.0000E+00  2.4474E-01  1.0000E-04   
 mr-sdci # 66  8   -195.7531730802  3.3963E-04  0.0000E+00  3.1397E-01  1.0000E-04   
 mr-sdci # 66  9   -194.7430712942  1.0434E-01  0.0000E+00  2.3247E+00  1.0000E-04   
 mr-sdci # 66 10   -193.9503758717  2.1402E-01  0.0000E+00  3.0603E+00  1.0000E-04   
 mr-sdci # 66 11   -193.5542398591  2.7343E-01  0.0000E+00  3.5567E+00  1.0000E-04   
 mr-sdci # 66 12   -193.2775420617 -4.1423E-01  0.0000E+00  3.6009E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.012000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  67

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59959927
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426619
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426348
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616546
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616395
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88258152
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.86075432
   ht   9     0.00018787     0.01882707    -0.00455561    -0.00014035     0.01057817    -0.03666724     0.00272427    -0.00058851
   ht  10    -0.00997241    -0.02178427     0.00681963    -0.00955883    -0.00168974     0.00770388    -0.00001916     0.00622094
   ht  11     0.00110675    -0.00677680     0.01001852    -0.01139580     0.02696277     0.00490964     0.00132535     0.00332869
   ht  12     0.02400742    -0.00764709     0.01622301    -0.00621874    -0.00269382    -0.00920184     0.00055849     0.00494461
   ht  13     0.18855471     0.04928638    -0.05333034     0.00434681     0.00148055     0.09184925     0.00436497    -0.01289783

                ht   9         ht  10         ht  11         ht  12         ht  13
   ht   9    -0.00007449
   ht  10     0.00002039    -0.00013639
   ht  11     0.00000136    -0.00000353    -0.00004313
   ht  12    -0.00000259    -0.00000371    -0.00000247    -0.00006380
   ht  13     0.00003644    -0.00000136     0.00000373    -0.00002382    -0.00173091

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   6.379148E-05   0.817253      -0.289171      -1.643576E-02  -0.432292      -3.290005E-02  -0.107575      -3.157093E-02
 ref    2  -2.166255E-04  -5.989476E-02  -0.201965       0.575784      -5.696874E-02   0.748784      -3.066649E-02   6.913534E-02

              v      9       v     10       v     11       v     12       v     13
 ref    1  -3.054543E-02  -3.172774E-02  -7.949424E-03   5.265990E-02   4.927501E-03
 ref    2   2.977677E-02  -7.426312E-02   8.536895E-02  -5.310168E-02  -4.925972E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   5.099597E-08   0.671490       0.124409       0.331797       0.190122       0.561759       1.251290E-02   5.776419E-03

              v      9       v     10       v     11       v     12       v     13
 ref    1   1.819679E-03   6.521661E-03   7.351051E-03   5.592853E-03   2.450801E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00006379     0.81725316    -0.28917061    -0.01643576    -0.43229171    -0.03290005    -0.10757538    -0.03157093
 ref:   2    -0.00021663    -0.05989476    -0.20196467     0.57578396    -0.05696874     0.74878369    -0.03066649     0.06913534

                ci   9         ci  10         ci  11         ci  12         ci  13
 ref:   1    -0.03054543    -0.03172774    -0.00794942     0.05265990     0.00492750
 ref:   2     0.02977677    -0.07426312     0.08536895    -0.05310168    -0.04925972

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 67  1   -196.4879464307  2.3287E-05  0.0000E+00  5.7826E-03  1.0000E-04   
 mr-sdci # 67  2   -196.4725827956  3.9684E-11  4.2210E-09  1.3685E-04  1.0000E-04   
 mr-sdci # 67  3   -196.4725821838  1.1506E-08  0.0000E+00  1.1559E-03  1.0000E-04   
 mr-sdci # 67  4   -196.4725817018  2.9575E-08  0.0000E+00  1.6838E-03  1.0000E-04   
 mr-sdci # 67  5   -196.4644808559  4.7650E-09  0.0000E+00  9.1766E-04  1.0000E-04   
 mr-sdci # 67  6   -196.4644806263  3.2498E-08  0.0000E+00  1.2701E-03  1.0000E-04   
 mr-sdci # 67  7   -195.7746431952  6.2367E-04  0.0000E+00  2.3752E-01  1.0000E-04   
 mr-sdci # 67  8   -195.7533804763  2.0740E-04  0.0000E+00  3.1632E-01  1.0000E-04   
 mr-sdci # 67  9   -194.7920381431  4.8967E-02  0.0000E+00  2.1707E+00  1.0000E-04   
 mr-sdci # 67 10   -194.4227504962  4.7237E-01  0.0000E+00  2.7949E+00  1.0000E-04   
 mr-sdci # 67 11   -193.5964501833  4.2210E-02  0.0000E+00  3.3799E+00  1.0000E-04   
 mr-sdci # 67 12   -193.3033810200  2.5839E-02  0.0000E+00  3.6347E+00  1.0000E-04   
 mr-sdci # 67 13   -191.7451720577 -1.7330E+00  0.0000E+00  4.9508E+00  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.009000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  68

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59959927
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426619
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426348
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616546
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616395
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88258152
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.86075432
   ht   9     0.00018787     0.01882707    -0.00455561    -0.00014035     0.01057817    -0.03666724     0.00272427    -0.00058851
   ht  10    -0.00997241    -0.02178427     0.00681963    -0.00955883    -0.00168974     0.00770388    -0.00001916     0.00622094
   ht  11     0.00110675    -0.00677680     0.01001852    -0.01139580     0.02696277     0.00490964     0.00132535     0.00332869
   ht  12     0.02400742    -0.00764709     0.01622301    -0.00621874    -0.00269382    -0.00920184     0.00055849     0.00494461
   ht  13     0.18855471     0.04928638    -0.05333034     0.00434681     0.00148055     0.09184925     0.00436497    -0.01289783
   ht  14    -0.00060097     0.00052728     0.00007674    -0.00072353    -0.00098876    -0.00009186     0.00010656    -0.00008463

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14
   ht   9    -0.00007449
   ht  10     0.00002039    -0.00013639
   ht  11     0.00000136    -0.00000353    -0.00004313
   ht  12    -0.00000259    -0.00000371    -0.00000247    -0.00006380
   ht  13     0.00003644    -0.00000136     0.00000373    -0.00002382    -0.00173091
   ht  14     0.00000003     0.00000044     0.00000027     0.00000001     0.00000238    -0.00000013

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7         v:   8

   eig(s)   1.764815E-09   4.663465E-07   7.877588E-07   8.886779E-07   2.404895E-06   1.613112E-05    1.00000        1.00000    
 
   x:   1  -1.453596E-05   4.410669E-05   2.022405E-04  -5.612123E-04  -4.521279E-04   3.686889E-03  -5.044034E-02   3.201952E-02
   x:   2   1.150102E-05  -1.526811E-04  -3.415092E-04   4.533827E-06  -5.316823E-04   9.441794E-04   0.288894      -3.551628E-02
   x:   3  -1.191852E-06   2.235602E-04   1.343981E-04  -2.412874E-04   2.293689E-04  -1.049299E-03   4.095335E-02  -9.014922E-02
   x:   4  -1.287494E-05  -2.384376E-04  -9.658276E-06   9.617293E-05  -1.930910E-04   7.414451E-05   0.124394      -0.200646    
   x:   5  -2.268347E-05   5.291723E-04  -2.139874E-04   3.342329E-05  -6.921002E-05   2.988650E-05  -1.192310E-02   2.200242E-02
   x:   6  -2.877802E-06   7.797799E-05   6.758424E-04   3.213428E-04   5.747363E-05   1.822090E-03   0.107220      -6.870241E-02
   x:   7   1.980338E-06   2.822918E-05  -4.855634E-05  -2.424218E-05  -1.000034E-05   8.821634E-05  -0.221085      -0.955169    
   x:   8  -2.109842E-06   7.766741E-05   2.056594E-05  -7.776285E-05   1.489559E-04  -2.499215E-04   0.914510      -0.178258    
   x:   9   2.756613E-03   1.108132E-02  -0.964725      -0.252174      -7.444551E-02   6.304496E-03   3.969771E-13  -7.791323E-13
   x:  10   3.958750E-03   2.699334E-02  -8.333520E-02   2.888749E-02   0.992751       7.695671E-02   2.941228E-13  -4.989903E-13
   x:  11  -7.172656E-03   0.997186      -4.166017E-03   6.841343E-02  -2.950341E-02   1.000027E-03   1.667021E-14  -1.326585E-14
   x:  12  -4.537130E-03   6.865693E-02   0.248912      -0.964460       4.917245E-02  -2.660424E-02  -3.158009E-13   4.563303E-13
   x:  13   1.125550E-04  -1.318138E-03   1.918662E-02  -2.644962E-02  -7.484222E-02   0.996649       4.227914E-14  -8.348792E-14
   x:  14   0.999952       7.327076E-03   4.086781E-03  -3.301570E-03  -3.705107E-03  -5.477218E-04    0.00000        0.00000    

                v:   9         v:  10         v:  11         v:  12         v:  13         v:  14

   eig(s)    1.00000        1.00000        1.00000        1.00000        1.00000        1.00002    
 
   x:   1   0.137724       1.599238E-02   0.318677      -0.365243      -0.145871      -0.849092    
   x:   2  -0.515431      -0.334977      -0.256259       0.272363      -0.590795      -0.220249    
   x:   3  -0.233954      -0.713636       0.296969      -0.496959       0.188422       0.235632    
   x:   4   0.731511      -0.487619       7.150520E-02   0.384216      -0.134988      -2.073405E-02
   x:   5   0.284993      -8.635675E-02  -0.787498      -0.528208      -0.107353      -3.766838E-03
   x:   6  -0.164824      -0.205511      -0.330116       0.283157       0.744648      -0.413186    
   x:   7  -0.104171       0.146420      -3.504975E-02  -5.340454E-02  -4.497954E-02  -1.947960E-02
   x:   8   7.925249E-02   0.263353       9.546739E-02  -0.189188       8.893136E-02   5.869786E-02
   x:   9  -1.754787E-07  -7.672985E-05   4.955517E-05   1.727230E-04   8.028571E-04  -1.906632E-04
   x:  10  -4.312172E-07  -1.421124E-04  -6.218713E-05   1.481445E-04  -4.623240E-04  -2.424601E-04
   x:  11   6.159743E-07   3.053050E-05   3.623601E-04   4.977590E-04  -1.635213E-04  -2.358394E-05
   x:  12   9.482936E-08   4.099368E-05  -3.875145E-04   4.638171E-04   2.427662E-05   2.103556E-04
   x:  13   1.606512E-08  -1.297047E-05   1.918526E-05  -3.883840E-06   7.285108E-06   4.396818E-03
   x:  14   2.344120E-05  -4.147849E-06  -8.740393E-06  -1.093529E-05   1.699080E-06  -9.132124E-06
 bummer (warning):overlap matrix: # small eigenvalues=                      1

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -5.958373E-05  -0.818651       0.285353       1.327819E-02  -0.432459      -3.062661E-02  -0.110506       1.243461E-02
 ref    2   2.109686E-04   6.020734E-02   0.199522      -0.576603      -5.303100E-02   0.749073      -1.990038E-02  -7.203550E-02

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1  -2.680888E-02  -2.650741E-02  -2.481748E-02   4.738823E-02   3.848067E-02   9.188096E-03
 ref    2   2.169491E-02  -8.074030E-02   7.658475E-02  -6.366627E-02   5.427403E-03   4.704572E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   4.805797E-08   0.673815       0.121235       0.332647       0.189833       0.562048       1.260750E-02   5.343733E-03

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1   1.189385E-03   7.221638E-03   6.481131E-03   6.299038E-03   1.510219E-03   2.297720E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00005958    -0.81865146     0.28535285     0.01327819    -0.43245886    -0.03062661    -0.11050556     0.01243461
 ref:   2     0.00021097     0.06020734     0.19952204    -0.57660263    -0.05303100     0.74907265    -0.01990038    -0.07203550

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14
 ref:   1    -0.02680888    -0.02650741    -0.02481748     0.04738823     0.03848067     0.00918810
 ref:   2     0.02169491    -0.08074030     0.07658475    -0.06366627     0.00542740     0.04704572

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 68  1   -196.4879467218  2.9113E-07  0.0000E+00  5.8459E-03  1.0000E-04   
 mr-sdci # 68  2   -196.4725827987  3.0643E-09  0.0000E+00  4.5059E-05  1.0000E-04   
 mr-sdci # 68  3   -196.4725821865  2.7622E-09  5.3479E-07  1.1589E-03  1.0000E-04   
 mr-sdci # 68  4   -196.4725817036  1.8884E-09  0.0000E+00  1.6718E-03  1.0000E-04   
 mr-sdci # 68  5   -196.4644808574  1.5695E-09  0.0000E+00  9.0495E-04  1.0000E-04   
 mr-sdci # 68  6   -196.4644806272  9.3101E-10  0.0000E+00  1.2692E-03  1.0000E-04   
 mr-sdci # 68  7   -195.7772187553  2.5756E-03  0.0000E+00  2.2806E-01  1.0000E-04   
 mr-sdci # 68  8   -195.7577791479  4.3987E-03  0.0000E+00  2.7651E-01  1.0000E-04   
 mr-sdci # 68  9   -194.8116276071  1.9589E-02  0.0000E+00  2.1075E+00  1.0000E-04   
 mr-sdci # 68 10   -194.4339254186  1.1175E-02  0.0000E+00  2.7488E+00  1.0000E-04   
 mr-sdci # 68 11   -193.8114707193  2.1502E-01  0.0000E+00  2.9234E+00  1.0000E-04   
 mr-sdci # 68 12   -193.3153060914  1.1925E-02  0.0000E+00  3.6316E+00  1.0000E-04   
 mr-sdci # 68 13   -192.9550836604  1.2099E+00  0.0000E+00  3.9866E+00  1.0000E-04   
 mr-sdci # 68 14   -191.5069660420 -5.3673E-01  0.0000E+00  5.1326E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.008000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  69

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59959927
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426619
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426348
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616546
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616395
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88258152
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.86075432
   ht   9     0.00018787     0.01882707    -0.00455561    -0.00014035     0.01057817    -0.03666724     0.00272427    -0.00058851
   ht  10    -0.00997241    -0.02178427     0.00681963    -0.00955883    -0.00168974     0.00770388    -0.00001916     0.00622094
   ht  11     0.00110675    -0.00677680     0.01001852    -0.01139580     0.02696277     0.00490964     0.00132535     0.00332869
   ht  12     0.02400742    -0.00764709     0.01622301    -0.00621874    -0.00269382    -0.00920184     0.00055849     0.00494461
   ht  13     0.18855471     0.04928638    -0.05333034     0.00434681     0.00148055     0.09184925     0.00436497    -0.01289783
   ht  14    -0.00060097     0.00052728     0.00007674    -0.00072353    -0.00098876    -0.00009186     0.00010656    -0.00008463
   ht  15    -0.01169700    -0.00293677     0.00591251     0.00396161    -0.00693479     0.01654964    -0.00027658     0.00120374

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15
   ht   9    -0.00007449
   ht  10     0.00002039    -0.00013639
   ht  11     0.00000136    -0.00000353    -0.00004313
   ht  12    -0.00000259    -0.00000371    -0.00000247    -0.00006380
   ht  13     0.00003644    -0.00000136     0.00000373    -0.00002382    -0.00173091
   ht  14     0.00000003     0.00000044     0.00000027     0.00000001     0.00000238    -0.00000013
   ht  15     0.00001075    -0.00000763     0.00000221     0.00000382     0.00001092    -0.00000028    -0.00002525

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7         v:   8

   eig(s)   1.741285E-09   2.940325E-07   4.667780E-07   7.939947E-07   8.969939E-07   2.404899E-06   1.613389E-05    1.00000    
 
   x:   1  -1.180574E-05  -3.144730E-04   6.049469E-05  -2.740927E-04   5.086805E-04  -4.517488E-04  -3.683528E-03   6.790997E-03
   x:   2   1.237699E-05  -1.166493E-04  -1.460128E-04   3.316072E-04   8.992986E-06  -5.315978E-04  -9.433471E-04  -0.153921    
   x:   3  -2.315286E-06   1.286703E-04   2.175663E-04  -1.383511E-04   2.458178E-04   2.291750E-04   1.047653E-03   4.438819E-02
   x:   4  -1.357924E-05   7.358397E-05  -2.425821E-04   2.590835E-05  -8.475979E-05  -1.931951E-04  -7.517233E-05   7.790449E-02
   x:   5  -2.143156E-05  -1.300761E-04   5.368718E-04   1.984130E-04  -3.740506E-05  -6.902196E-05  -2.807279E-05  -9.622144E-03
   x:   6  -6.250406E-06   4.167818E-04   5.453482E-05  -6.129958E-04  -3.251306E-04   5.710775E-05  -1.826210E-03  -1.393844E-02
   x:   7   2.084098E-06  -1.360330E-05   2.914686E-05   4.544899E-05   2.621754E-05  -9.994321E-06  -8.813770E-05   0.881933    
   x:   8  -2.326687E-06   2.455808E-05   7.665366E-05  -2.363893E-05   7.887439E-05   1.489133E-04   2.495797E-04  -0.436039    
   x:   9   3.557830E-03  -0.139158       2.157054E-02   0.936801       0.311396      -7.450947E-02  -6.367034E-03  -1.569931E-13
   x:  10   3.986822E-03  -4.587312E-03   2.744035E-02   8.483655E-02  -2.324967E-02   0.992749      -7.694939E-02  -1.331723E-13
   x:  11  -7.383684E-03   5.726893E-02   0.995417       6.523432E-03  -6.997798E-02  -2.949289E-02  -9.887568E-04   2.299063E-13
   x:  12  -4.056320E-03  -7.633703E-02   7.398577E-02  -0.321188       0.939362       4.912392E-02   2.655792E-02   4.681124E-13
   x:  13   2.342330E-04  -1.393920E-02  -6.172298E-04  -2.252617E-02   2.330160E-02  -7.482149E-02  -0.996563      -1.481605E-14
   x:  14   0.999911       9.392242E-03   7.028824E-03  -3.968477E-03   3.348944E-03  -3.706618E-03   5.455533E-04   8.445046E-12
   x:  15  -8.890389E-03   0.985507      -4.901599E-02   0.107137       0.120990      -1.404315E-03  -1.324405E-02    0.00000    

                v:   9         v:  10         v:  11         v:  12         v:  13         v:  14         v:  15

   eig(s)    1.00000        1.00000        1.00000        1.00000        1.00000        1.00000        1.00002    
 
   x:   1   7.339547E-02  -0.109997       1.790259E-02  -0.314041      -0.344368      -0.206473      -0.849877    
   x:   2  -0.260846       0.389564      -0.484850       0.262495       0.395004      -0.497879      -0.220479    
   x:   3  -0.140138      -8.533163E-03  -0.763397      -0.286056      -0.482652       0.158134       0.236124    
   x:   4   0.708202      -0.414573      -0.373792      -7.230195E-02   0.414810      -5.551800E-02  -2.030924E-02
   x:   5   0.210656      -0.201123      -5.876795E-02   0.794045      -0.497980      -0.181973      -4.512190E-03
   x:   6  -5.672431E-02   0.109447      -0.189223       0.323913       0.181880       0.801091      -0.411162    
   x:   7   0.181587       0.428828       9.041046E-03   3.686046E-02  -3.937416E-02  -4.422451E-02  -1.950433E-02
   x:   8   0.569927       0.654121       5.256608E-02  -9.291171E-02  -0.194284       6.767303E-02   5.879437E-02
   x:   9  -2.076753E-06  -4.061156E-05  -1.412254E-05  -5.684807E-05   4.779822E-05   8.222251E-04  -1.889037E-04
   x:  10  -1.953795E-06  -3.780203E-05  -1.527732E-04   6.500248E-05   2.259944E-04  -4.235544E-04  -2.430452E-04
   x:  11   6.217850E-07   3.781252E-06   4.754819E-05  -3.670767E-04   5.114728E-04  -9.013230E-05  -2.340422E-05
   x:  12   9.998058E-08   2.553219E-06   7.420259E-05   3.816067E-04   4.551681E-04   9.495808E-05   2.109070E-04
   x:  13  -2.175471E-07  -5.044020E-06  -8.915951E-06  -1.939300E-05  -8.241147E-06  -2.007914E-06   4.396824E-03
   x:  14   1.849519E-05  -1.484323E-05  -3.480944E-07   8.819639E-06  -1.117995E-05  -1.987818E-07  -9.144418E-06
   x:  15  -2.448636E-06  -4.599622E-05   1.471435E-04  -1.294260E-05  -1.561112E-04  -3.797255E-04  -1.028802E-04
 bummer (warning):overlap matrix: # small eigenvalues=                      1

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -3.634036E-05  -0.821836       0.275602      -2.062638E-02  -0.432349       3.213551E-02  -0.110475       1.285357E-02
 ref    2   1.644459E-04   5.878734E-02   0.130611      -0.596141      -5.564792E-02  -0.748887      -1.946936E-02  -7.148586E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1   4.358300E-04   3.742850E-02   1.609924E-02   2.622788E-02   4.326259E-02  -1.756111E-02   4.843551E-02
 ref    2   3.817088E-02  -2.204147E-03   8.348983E-02  -7.852501E-02  -1.796431E-03   8.504280E-02  -5.619272E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   2.836309E-08   0.678870       9.301561E-02   0.355810       0.190023       0.561864       1.258379E-02   5.275442E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1   1.457206E-03   1.405751E-03   7.229738E-03   6.854079E-03   1.874879E-03   7.540671E-03   5.503620E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00003634    -0.82183553     0.27560168    -0.02062638    -0.43234941     0.03213551    -0.11047505     0.01285357
 ref:   2     0.00016445     0.05878734     0.13061136    -0.59614146    -0.05564792    -0.74888687    -0.01946936    -0.07148586

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15
 ref:   1     0.00043583     0.03742850     0.01609924     0.02622788     0.04326259    -0.01756111     0.04843551
 ref:   2     0.03817088    -0.00220415     0.08348983    -0.07852501    -0.00179643     0.08504280    -0.05619272

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 69  1   -196.4879480944  1.3725E-06  0.0000E+00  6.0925E-03  1.0000E-04   
 mr-sdci # 69  2   -196.4725827987  1.3742E-11  0.0000E+00  4.3941E-05  1.0000E-04   
 mr-sdci # 69  3   -196.4725827080  5.2150E-07  0.0000E+00  5.9812E-04  1.0000E-04   
 mr-sdci # 69  4   -196.4725817165  1.2827E-08  9.5094E-07  1.6259E-03  1.0000E-04   
 mr-sdci # 69  5   -196.4644808575  8.1485E-11  0.0000E+00  9.0568E-04  1.0000E-04   
 mr-sdci # 69  6   -196.4644806349  7.6779E-09  0.0000E+00  1.2539E-03  1.0000E-04   
 mr-sdci # 69  7   -195.7773359494  1.1719E-04  0.0000E+00  2.3168E-01  1.0000E-04   
 mr-sdci # 69  8   -195.7578282550  4.9107E-05  0.0000E+00  2.7860E-01  1.0000E-04   
 mr-sdci # 69  9   -195.4400355818  6.2841E-01  0.0000E+00  1.3246E+00  1.0000E-04   
 mr-sdci # 69 10   -194.6933006962  2.5938E-01  0.0000E+00  2.2901E+00  1.0000E-04   
 mr-sdci # 69 11   -194.4032971397  5.9183E-01  0.0000E+00  2.7402E+00  1.0000E-04   
 mr-sdci # 69 12   -193.8108554410  4.9555E-01  0.0000E+00  2.9190E+00  1.0000E-04   
 mr-sdci # 69 13   -192.9583706127  3.2870E-03  0.0000E+00  3.9637E+00  1.0000E-04   
 mr-sdci # 69 14   -191.6605603165  1.5359E-01  0.0000E+00  4.5610E+00  1.0000E-04   
 mr-sdci # 69 15   -190.9347996062  2.1158E-02  0.0000E+00  4.5851E+00  1.0000E-04   
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.009000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  70

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59959927
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426619
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426348
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616546
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616395
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88258152
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.86075432
   ht   9     0.00018787     0.01882707    -0.00455561    -0.00014035     0.01057817    -0.03666724     0.00272427    -0.00058851
   ht  10    -0.00997241    -0.02178427     0.00681963    -0.00955883    -0.00168974     0.00770388    -0.00001916     0.00622094
   ht  11     0.00110675    -0.00677680     0.01001852    -0.01139580     0.02696277     0.00490964     0.00132535     0.00332869
   ht  12     0.02400742    -0.00764709     0.01622301    -0.00621874    -0.00269382    -0.00920184     0.00055849     0.00494461
   ht  13     0.18855471     0.04928638    -0.05333034     0.00434681     0.00148055     0.09184925     0.00436497    -0.01289783
   ht  14    -0.00060097     0.00052728     0.00007674    -0.00072353    -0.00098876    -0.00009186     0.00010656    -0.00008463
   ht  15    -0.01169700    -0.00293677     0.00591251     0.00396161    -0.00693479     0.01654964    -0.00027658     0.00120374
   ht  16    -0.01123422     0.01968509    -0.00274895     0.01246526     0.00807135    -0.01074134     0.00240033    -0.00104292

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15         ht  16
   ht   9    -0.00007449
   ht  10     0.00002039    -0.00013639
   ht  11     0.00000136    -0.00000353    -0.00004313
   ht  12    -0.00000259    -0.00000371    -0.00000247    -0.00006380
   ht  13     0.00003644    -0.00000136     0.00000373    -0.00002382    -0.00173091
   ht  14     0.00000003     0.00000044     0.00000027     0.00000001     0.00000238    -0.00000013
   ht  15     0.00001075    -0.00000763     0.00000221     0.00000382     0.00001092    -0.00000028    -0.00002525
   ht  16    -0.00001108     0.00000654     0.00000217     0.00000516     0.00003628     0.00000001     0.00000175    -0.00004206

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7         v:   8

   eig(s)   1.738186E-09   2.903082E-07   4.522214E-07   4.898625E-07   8.345380E-07   9.027107E-07   2.408261E-06   1.613389E-05
 
   x:   1   1.241657E-05   2.691799E-04  -2.760871E-04  -2.526348E-04   8.551257E-05   5.092120E-04  -4.595406E-04   3.683458E-03
   x:   2  -1.355880E-05   1.839404E-04   4.049875E-04   2.866800E-04  -1.800190E-04   1.574417E-05  -5.158140E-04   9.434665E-04
   x:   3   2.600036E-06  -1.457655E-04  -2.341101E-04   4.648542E-05   6.452849E-05   2.544502E-04   2.275385E-04  -1.047669E-03
   x:   4   1.291885E-05  -3.295900E-05   3.535931E-04   5.394789E-05   7.180065E-05  -5.062220E-05  -1.828383E-04   7.524858E-05
   x:   5   2.097591E-05   1.493176E-04  -2.991013E-04   4.950834E-04  -1.169387E-04  -4.118620E-05  -6.270016E-05   2.812203E-05
   x:   6   7.082463E-06  -4.594302E-04  -2.309464E-04  -2.288581E-04   5.701506E-04  -2.498029E-04   4.983046E-05   1.826145E-03
   x:   7  -2.218768E-06   2.081663E-05   1.132099E-05   6.247383E-05  -3.146963E-05   2.562582E-05  -8.068477E-06   8.815252E-05
   x:   8   2.422132E-06  -3.051530E-05  -8.179569E-05   1.850655E-05  -9.967177E-07   7.724165E-05   1.481040E-04  -2.495857E-04
   x:   9  -3.907265E-03   0.164857       0.140882       0.241402      -0.929543       0.157025      -7.704875E-02   6.364848E-03
   x:  10  -3.936640E-03   1.920224E-03  -2.736151E-02   1.196411E-02  -8.969726E-02  -4.351160E-02   0.991579       7.695096E-02
   x:  11   7.490596E-03  -7.096017E-02  -0.769664       0.630173       2.630101E-02  -6.219831E-02  -2.910139E-02   9.890556E-04
   x:  12   4.270224E-03   5.589287E-02  -0.176960      -0.116659       0.111806       0.967533       5.106444E-02  -2.655634E-02
   x:  13  -2.321266E-04   1.323461E-02  -4.926146E-03  -7.614046E-03   1.715268E-02   2.672911E-02  -7.468275E-02   0.996563    
   x:  14  -0.999903      -1.012678E-02  -7.037571E-03   2.237802E-03   2.871474E-03   3.829305E-03  -3.701150E-03  -5.455640E-04
   x:  15   9.086412E-03  -0.972503       0.150781       8.958289E-02  -0.108069       0.107428      -1.090567E-03   1.324439E-02
   x:  16  -2.503450E-03   0.136455       0.576972       0.723026       0.320516       0.145573       4.228042E-02   3.115836E-04

                v:   9         v:  10         v:  11         v:  12         v:  13         v:  14         v:  15         v:  16

   eig(s)    1.00000        1.00000        1.00000        1.00000        1.00000        1.00000        1.00000        1.00002    
 
   x:   1   1.806871E-03   0.137005       2.751190E-02   5.916514E-02  -0.253505       0.420995      -0.109640       0.850548    
   x:   2  -0.136131      -0.527185      -4.624217E-02   0.331708       0.183986      -0.359127      -0.617772       0.216585    
   x:   3   5.358445E-02  -0.181280       0.192770       0.803240      -7.190348E-02   0.420314       0.211531      -0.235224    
   x:   4   2.775083E-02   0.758080      -3.468159E-02   0.448309      -7.149761E-02  -0.432271      -0.173390       1.806717E-02
   x:   5  -2.442546E-02   0.265564       5.951933E-02  -1.928628E-02   0.863559       0.375270      -0.195943       3.069341E-03
   x:   6  -1.020637E-02  -0.137032      -1.957689E-02   0.156093       0.377289      -0.401368       0.694791       0.412539    
   x:   7   0.866425      -7.600792E-02  -0.483152       5.161437E-02   5.794729E-02   2.761063E-02  -5.429791E-02   1.904541E-02
   x:   8  -0.475848       2.604708E-02  -0.849341       0.113547      -1.277287E-02   0.166080       8.631506E-02  -5.849397E-02
   x:   9   2.554079E-07   3.105184E-05   2.386632E-05   6.562003E-05   1.539767E-05  -2.002557E-04   7.975957E-04   1.917545E-04
   x:  10  -3.574475E-08  -5.882422E-06   6.608247E-05   9.203378E-05  -1.586106E-06  -1.574163E-04  -4.721961E-04   2.406599E-04
   x:  11   4.773444E-09   5.094659E-06  -1.228593E-05  -2.886766E-05  -4.693638E-04  -4.109869E-04  -1.286610E-04   2.281216E-05
   x:  12  -5.138334E-08  -5.947283E-06   2.948623E-06  -1.648645E-04   2.776664E-04  -5.118005E-04  -2.255649E-05  -2.122295E-04
   x:  13   7.926526E-09   1.283528E-06   6.193570E-06   6.427336E-06  -2.297488E-05   1.942986E-05   1.948513E-05  -4.396731E-03
   x:  14  -1.290957E-06   2.338210E-05   7.707694E-07   2.422500E-06   1.158558E-05   8.741932E-06   1.872591E-08   9.135272E-06
   x:  15   3.491199E-07   4.308506E-05   1.605780E-05  -1.522111E-04  -3.833573E-05   2.401668E-04  -3.289459E-04   1.022213E-04
   x:  16  -2.346622E-07  -2.839098E-05   3.487245E-05  -1.466476E-04  -1.748952E-04   2.195984E-04   4.535211E-04   1.722974E-04
 bummer (warning):overlap matrix: # small eigenvalues=                      1

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   5.960721E-05  -0.821720      -0.233614      -0.148318      -0.433505       5.433091E-03   0.104692       3.780659E-02
 ref    2  -1.417871E-04   5.952710E-02  -0.462281       0.398331      -9.413815E-03  -0.750877       3.037576E-02  -6.237267E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   1.393991E-02  -1.886468E-03   2.529812E-02   1.195112E-02   4.951760E-02  -5.445993E-02  -8.353895E-03  -9.910936E-02
 ref    2   2.434365E-02  -4.400704E-02   8.578324E-02   4.925613E-02  -5.363846E-03   1.473035E-02   3.600733E-02   0.123907    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   2.365659E-08   0.678767       0.268279       0.180666       0.188016       0.563846       1.188314E-02   5.319689E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   7.869344E-04   1.940178E-03   7.998759E-03   2.568995E-03   2.480763E-03   3.182867E-03   1.366315E-03   2.517559E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15        ev   16

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00005961    -0.82171998    -0.23361396    -0.14831757    -0.43350537     0.00543309     0.10469218     0.03780659
 ref:   2    -0.00014179     0.05952710    -0.46228096     0.39833123    -0.00941382    -0.75087702     0.03037576    -0.06237267

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1     0.01393991    -0.00188647     0.02529812     0.01195112     0.04951760    -0.05445993    -0.00835390    -0.09910936
 ref:   2     0.02434365    -0.04400704     0.08578324     0.04925613    -0.00536385     0.01473035     0.03600733     0.12390694

 trial vector basis is being transformed.  new dimension:   8

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 70  1   -196.4879484280  3.3361E-07  0.0000E+00  5.9770E-03  1.0000E-04   
 mr-sdci # 70  2   -196.4725827987  1.5632E-13  0.0000E+00  4.3868E-05  1.0000E-04   
 mr-sdci # 70  3   -196.4725827259  1.7895E-08  0.0000E+00  5.2781E-04  1.0000E-04   
 mr-sdci # 70  4   -196.4725826778  9.6130E-07  0.0000E+00  7.0287E-04  1.0000E-04   
 mr-sdci # 70  5   -196.4644808615  3.9361E-09  2.6064E-07  9.1252E-04  1.0000E-04   
 mr-sdci # 70  6   -196.4644806749  4.0033E-08  0.0000E+00  1.1415E-03  1.0000E-04   
 mr-sdci # 70  7   -195.7778168602  4.8091E-04  0.0000E+00  2.3228E-01  1.0000E-04   
 mr-sdci # 70  8   -195.7689786172  1.1150E-02  0.0000E+00  3.0150E-01  1.0000E-04   
 mr-sdci # 70  9   -195.6508369366  2.1080E-01  0.0000E+00  7.2730E-01  1.0000E-04   
 mr-sdci # 70 10   -195.4209664682  7.2767E-01  0.0000E+00  1.3327E+00  1.0000E-04   
 mr-sdci # 70 11   -194.4145013163  1.1204E-02  0.0000E+00  2.7642E+00  1.0000E-04   
 mr-sdci # 70 12   -194.0597898979  2.4893E-01  0.0000E+00  2.5062E+00  1.0000E-04   
 mr-sdci # 70 13   -192.9607177872  2.3472E-03  0.0000E+00  3.9595E+00  1.0000E-04   
 mr-sdci # 70 14   -192.7112396235  1.0507E+00  0.0000E+00  3.1778E+00  1.0000E-04   
 mr-sdci # 70 15   -191.1343002455  1.9950E-01  0.0000E+00  5.3930E+00  1.0000E-04   
 mr-sdci # 70 16   -190.5355309801 -1.9715E-01  0.0000E+00  3.9234E+00  1.0000E-04   
 
 root number  5 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.011000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  71

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59963363
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426793
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426788
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616607
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616588
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88950206
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88066382
   ht   9    -0.00075677     0.00105615     0.00038181    -0.00292536     0.01006789     0.01067354    -0.00053410     0.00015926

                ht   9
   ht   9    -0.00001041

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   5.234187E-05   0.822149       0.236259       0.141601       0.433186       1.732034E-02   0.102977      -4.326614E-02
 ref    2  -1.628423E-04  -5.981471E-02   0.453105      -0.408695      -3.001128E-02   0.750337       3.481380E-02   6.109815E-02

              v      9
 ref    1   2.347669E-02
 ref    2   4.879465E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   2.925730E-08   0.679506       0.261122       0.187083       0.188551       0.563306       1.181632E-02   5.604943E-03

              v      9
 ref    1   2.932073E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00005234     0.82214857     0.23625851     0.14160081     0.43318642     0.01732034     0.10297730    -0.04326614
 ref:   2    -0.00016284    -0.05981471     0.45310509    -0.40869523    -0.03001128     0.75033742     0.03481380     0.06109815

                ci   9
 ref:   1     0.02347669
 ref:   2     0.04879465

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 71  1   -196.4879488716  4.4361E-07  0.0000E+00  5.7557E-03  1.0000E-04   
 mr-sdci # 71  2   -196.4725827987  1.0850E-11  0.0000E+00  4.3420E-05  1.0000E-04   
 mr-sdci # 71  3   -196.4725827262  3.2248E-10  0.0000E+00  5.2858E-04  1.0000E-04   
 mr-sdci # 71  4   -196.4725826813  3.5077E-09  0.0000E+00  6.8384E-04  1.0000E-04   
 mr-sdci # 71  5   -196.4644810449  1.8348E-07  0.0000E+00  4.6278E-04  1.0000E-04   
 mr-sdci # 71  6   -196.4644806760  1.0326E-09  3.8810E-07  1.1190E-03  1.0000E-04   
 mr-sdci # 71  7   -195.7788299289  1.0131E-03  0.0000E+00  2.2143E-01  1.0000E-04   
 mr-sdci # 71  8   -195.7692322902  2.5367E-04  0.0000E+00  3.0538E-01  1.0000E-04   
 mr-sdci # 71  9   -193.4776593547 -2.1732E+00  0.0000E+00  3.5394E+00  1.0000E-04   
 
 root number  6 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  72

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59963363
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426793
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426788
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616607
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616588
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88950206
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88066382
   ht   9    -0.00075677     0.00105615     0.00038181    -0.00292536     0.01006789     0.01067354    -0.00053410     0.00015926
   ht  10    -0.00409108     0.00911125    -0.01547650     0.00147891    -0.00409410     0.00401829    -0.00007518    -0.00091053

                ht   9         ht  10
   ht   9    -0.00001041
   ht  10     0.00000082    -0.00001666

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -4.716205E-05  -0.821554      -0.221081       0.167242      -0.431564       4.126908E-02   0.103118      -4.299731E-02
 ref    2   1.470702E-04   6.318671E-02  -0.499496      -0.349890       7.148787E-02   0.747515       3.450851E-02   6.149390E-02

              v      9       v     10
 ref    1  -6.834555E-03  -2.488012E-02
 ref    2  -6.229301E-02  -1.202890E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   2.385389E-08   0.678943       0.298373       0.150393       0.191358       0.560482       1.182420E-02   5.630269E-03

              v      9       v     10
 ref    1   3.927130E-03   7.637151E-04

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00004716    -0.82155374    -0.22108077     0.16724199    -0.43156389     0.04126908     0.10311822    -0.04299731
 ref:   2     0.00014707     0.06318671    -0.49949571    -0.34988960     0.07148787     0.74751495     0.03450851     0.06149390

                ci   9         ci  10
 ref:   1    -0.00683455    -0.02488012
 ref:   2    -0.06229301    -0.01202890

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 72  1   -196.4879497796  9.0799E-07  8.6385E-06  5.4562E-03  1.0000E-04   
 mr-sdci # 72  2   -196.4725827987  2.0613E-11  0.0000E+00  4.0838E-05  1.0000E-04   
 mr-sdci # 72  3   -196.4725827314  5.1261E-09  0.0000E+00  5.1866E-04  1.0000E-04   
 mr-sdci # 72  4   -196.4725826871  5.8332E-09  0.0000E+00  6.9281E-04  1.0000E-04   
 mr-sdci # 72  5   -196.4644810452  3.0848E-10  0.0000E+00  4.5783E-04  1.0000E-04   
 mr-sdci # 72  6   -196.4644809661  2.9008E-07  0.0000E+00  6.6405E-04  1.0000E-04   
 mr-sdci # 72  7   -195.7788419452  1.2016E-05  0.0000E+00  2.2283E-01  1.0000E-04   
 mr-sdci # 72  8   -195.7692979214  6.5631E-05  0.0000E+00  3.0652E-01  1.0000E-04   
 mr-sdci # 72  9   -194.0552751973  5.7762E-01  0.0000E+00  3.2989E+00  1.0000E-04   
 mr-sdci # 72 10   -193.0903576218 -2.3306E+00  0.0000E+00  3.8612E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.012000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  73

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59963363
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426793
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426788
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616607
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616588
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88950206
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88066382
   ht   9    -0.00075677     0.00105615     0.00038181    -0.00292536     0.01006789     0.01067354    -0.00053410     0.00015926
   ht  10    -0.00409108     0.00911125    -0.01547650     0.00147891    -0.00409410     0.00401829    -0.00007518    -0.00091053
   ht  11     0.05011436     0.02227819     0.00527350     0.01456934    -0.02595700     0.02012466    -0.00138207    -0.00150194

                ht   9         ht  10         ht  11
   ht   9    -0.00001041
   ht  10     0.00000082    -0.00001666
   ht  11     0.00000046    -0.00000416    -0.00028349

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -1.672154E-05   0.821870       0.231939      -0.150105       0.430314      -5.272502E-02  -0.103262      -4.288072E-02
 ref    2   1.227590E-04  -6.278182E-02   0.472246       0.385945      -9.132920E-02  -0.745350      -3.430448E-02   6.157471E-02

              v      9       v     10       v     11
 ref    1   2.838110E-03   2.916424E-02  -5.965869E-03
 ref    2  -6.280723E-02   1.635982E-02  -1.670968E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   1.534938E-08   0.679412       0.276812       0.171485       0.193511       0.558326       1.183985E-02   5.630201E-03

              v      9       v     10       v     11
 ref    1   3.952803E-03   1.118197E-03   3.148049E-04

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00001672     0.82186996     0.23193928    -0.15010533     0.43031436    -0.05272502    -0.10326206    -0.04288072
 ref:   2     0.00012276    -0.06278182     0.47224560     0.38594494    -0.09132920    -0.74534971    -0.03430448     0.06157471

                ci   9         ci  10         ci  11
 ref:   1     0.00283811     0.02916424    -0.00596587
 ref:   2    -0.06280723     0.01635982    -0.01670968

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 73  1   -196.4879553057  5.5262E-06  0.0000E+00  3.0432E-03  1.0000E-04   
 mr-sdci # 73  2   -196.4725827987  9.0239E-13  0.0000E+00  4.0942E-05  1.0000E-04   
 mr-sdci # 73  3   -196.4725827372  5.8410E-09  5.4687E-08  4.7136E-04  1.0000E-04   
 mr-sdci # 73  4   -196.4725826891  2.0031E-09  0.0000E+00  6.8845E-04  1.0000E-04   
 mr-sdci # 73  5   -196.4644810460  7.8241E-10  0.0000E+00  4.6214E-04  1.0000E-04   
 mr-sdci # 73  6   -196.4644809714  5.3363E-09  0.0000E+00  6.8689E-04  1.0000E-04   
 mr-sdci # 73  7   -195.7788883011  4.6356E-05  0.0000E+00  2.2392E-01  1.0000E-04   
 mr-sdci # 73  8   -195.7693015885  3.6671E-06  0.0000E+00  3.0598E-01  1.0000E-04   
 mr-sdci # 73  9   -194.1388372764  8.3562E-02  0.0000E+00  3.3636E+00  1.0000E-04   
 mr-sdci # 73 10   -193.6171533958  5.2680E-01  0.0000E+00  3.3615E+00  1.0000E-04   
 mr-sdci # 73 11   -192.5212870269 -1.8932E+00  0.0000E+00  3.7260E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.014000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  74

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59963363
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426793
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426788
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616607
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616588
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88950206
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88066382
   ht   9    -0.00075677     0.00105615     0.00038181    -0.00292536     0.01006789     0.01067354    -0.00053410     0.00015926
   ht  10    -0.00409108     0.00911125    -0.01547650     0.00147891    -0.00409410     0.00401829    -0.00007518    -0.00091053
   ht  11     0.05011436     0.02227819     0.00527350     0.01456934    -0.02595700     0.02012466    -0.00138207    -0.00150194
   ht  12    -0.00374062    -0.00132099    -0.00113548    -0.00073656    -0.00106517     0.00040290     0.00022774    -0.00030590

                ht   9         ht  10         ht  11         ht  12
   ht   9    -0.00001041
   ht  10     0.00000082    -0.00001666
   ht  11     0.00000046    -0.00000416    -0.00028349
   ht  12    -0.00000001    -0.00000012     0.00000505    -0.00000158

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -1.835113E-05   0.824348       0.226093      -0.145359       0.431010       4.669918E-02  -9.954315E-02   5.091230E-02
 ref    2   1.207838E-04  -5.780943E-02   0.464989       0.395411      -8.088884E-02   0.746557      -3.949317E-02  -5.868252E-02

              v      9       v     10       v     11       v     12
 ref    1   1.134310E-02  -2.539237E-02  -2.084360E-02  -8.275304E-04
 ref    2  -3.444920E-02   2.407256E-03  -6.486710E-02  -1.908140E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   1.492549E-08   0.682891       0.267333       0.177479       0.192313       0.559528       1.146855E-02   6.035700E-03

              v      9       v     10       v     11       v     12
 ref    1   1.315413E-03   6.505672E-04   4.642197E-03   4.325804E-06

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00001835     0.82434756     0.22609323    -0.14535944     0.43101031     0.04669918    -0.09954315     0.05091230
 ref:   2     0.00012078    -0.05780943     0.46498921     0.39541102    -0.08088884     0.74655712    -0.03949317    -0.05868252

                ci   9         ci  10         ci  11         ci  12
 ref:   1     0.01134310    -0.02539237    -0.02084360    -0.00082753
 ref:   2    -0.03444920     0.00240726    -0.06486710    -0.00190814

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 74  1   -196.4879553467  4.1003E-08  0.0000E+00  2.8899E-03  1.0000E-04   
 mr-sdci # 74  2   -196.4725827987  2.4230E-12  0.0000E+00  4.0528E-05  1.0000E-04   
 mr-sdci # 74  3   -196.4725827832  4.5984E-08  0.0000E+00  1.9660E-04  1.0000E-04   
 mr-sdci # 74  4   -196.4725826892  4.0224E-11  9.5918E-08  6.8937E-04  1.0000E-04   
 mr-sdci # 74  5   -196.4644810462  1.2475E-10  0.0000E+00  4.6486E-04  1.0000E-04   
 mr-sdci # 74  6   -196.4644809792  7.8413E-09  0.0000E+00  6.8787E-04  1.0000E-04   
 mr-sdci # 74  7   -195.7792291622  3.4086E-04  0.0000E+00  2.2443E-01  1.0000E-04   
 mr-sdci # 74  8   -195.7709584127  1.6568E-03  0.0000E+00  2.9399E-01  1.0000E-04   
 mr-sdci # 74  9   -194.5634110192  4.2457E-01  0.0000E+00  2.9722E+00  1.0000E-04   
 mr-sdci # 74 10   -193.6345281081  1.7375E-02  0.0000E+00  3.3761E+00  1.0000E-04   
 mr-sdci # 74 11   -193.3321711560  8.1088E-01  0.0000E+00  3.4009E+00  1.0000E-04   
 mr-sdci # 74 12   -192.3877342557 -1.6721E+00  0.0000E+00  3.8207E+00  1.0000E-04   
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  75

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59963363
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426793
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426788
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616607
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616588
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88950206
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88066382
   ht   9    -0.00075677     0.00105615     0.00038181    -0.00292536     0.01006789     0.01067354    -0.00053410     0.00015926
   ht  10    -0.00409108     0.00911125    -0.01547650     0.00147891    -0.00409410     0.00401829    -0.00007518    -0.00091053
   ht  11     0.05011436     0.02227819     0.00527350     0.01456934    -0.02595700     0.02012466    -0.00138207    -0.00150194
   ht  12    -0.00374062    -0.00132099    -0.00113548    -0.00073656    -0.00106517     0.00040290     0.00022774    -0.00030590
   ht  13     0.00292921    -0.00067650    -0.00414234    -0.00546354    -0.00135955    -0.00147174     0.00048843    -0.00014616

                ht   9         ht  10         ht  11         ht  12         ht  13
   ht   9    -0.00001041
   ht  10     0.00000082    -0.00001666
   ht  11     0.00000046    -0.00000416    -0.00028349
   ht  12    -0.00000001    -0.00000012     0.00000505    -0.00000158
   ht  13     0.00000032    -0.00000008    -0.00000207     0.00000015    -0.00000296

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   1.149882E-05   0.821890       0.203160       0.187141       0.430910      -4.761364E-02   9.473820E-02  -6.017181E-02
 ref    2  -1.202745E-04  -4.774069E-02   0.507268      -0.341035      -8.247139E-02  -0.746385       4.498693E-02   5.436886E-02

              v      9       v     10       v     11       v     12       v     13
 ref    1  -1.747398E-02   2.085852E-02   2.400809E-02  -1.830837E-02   1.649843E-02
 ref    2   3.533167E-02  -1.270180E-02   3.951298E-02  -4.597737E-02   2.447067E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   1.459818E-08   0.677782       0.298594       0.151327       0.192485       0.559358       1.099915E-02   6.576620E-03

              v      9       v     10       v     11       v     12       v     13
 ref    1   1.553667E-03   5.964134E-04   2.137664E-03   2.449115E-03   8.710117E-04

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00001150     0.82188952     0.20315965     0.18714081     0.43091030    -0.04761364     0.09473820    -0.06017181
 ref:   2    -0.00012027    -0.04774069     0.50726777    -0.34103513    -0.08247139    -0.74638493     0.04498693     0.05436886

                ci   9         ci  10         ci  11         ci  12         ci  13
 ref:   1    -0.01747398     0.02085852     0.02400809    -0.01830837     0.01649843
 ref:   2     0.03533167    -0.01270180     0.03951298    -0.04597737     0.02447067

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 75  1   -196.4879554411  9.4391E-08  0.0000E+00  2.8309E-03  1.0000E-04   
 mr-sdci # 75  2   -196.4725827988  1.9860E-11  0.0000E+00  3.8758E-05  1.0000E-04   
 mr-sdci # 75  3   -196.4725827834  2.6052E-10  0.0000E+00  1.9860E-04  1.0000E-04   
 mr-sdci # 75  4   -196.4725827668  7.7661E-08  0.0000E+00  2.9414E-04  1.0000E-04   
 mr-sdci # 75  5   -196.4644810462  6.6294E-12  9.4907E-08  4.6577E-04  1.0000E-04   
 mr-sdci # 75  6   -196.4644809821  2.9146E-09  0.0000E+00  6.9415E-04  1.0000E-04   
 mr-sdci # 75  7   -195.7806907379  1.4616E-03  0.0000E+00  2.0844E-01  1.0000E-04   
 mr-sdci # 75  8   -195.7714762853  5.1787E-04  0.0000E+00  2.8544E-01  1.0000E-04   
 mr-sdci # 75  9   -194.6575525906  9.4142E-02  0.0000E+00  2.8924E+00  1.0000E-04   
 mr-sdci # 75 10   -193.8205123171  1.8598E-01  0.0000E+00  3.2863E+00  1.0000E-04   
 mr-sdci # 75 11   -193.4861926446  1.5402E-01  0.0000E+00  3.3305E+00  1.0000E-04   
 mr-sdci # 75 12   -192.9032794742  5.1555E-01  0.0000E+00  3.7602E+00  1.0000E-04   
 mr-sdci # 75 13   -191.9946722262 -9.6605E-01  0.0000E+00  4.4042E+00  1.0000E-04   
 
 root number  5 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.009000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  76

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59963363
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426793
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426788
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616607
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616588
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88950206
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88066382
   ht   9    -0.00075677     0.00105615     0.00038181    -0.00292536     0.01006789     0.01067354    -0.00053410     0.00015926
   ht  10    -0.00409108     0.00911125    -0.01547650     0.00147891    -0.00409410     0.00401829    -0.00007518    -0.00091053
   ht  11     0.05011436     0.02227819     0.00527350     0.01456934    -0.02595700     0.02012466    -0.00138207    -0.00150194
   ht  12    -0.00374062    -0.00132099    -0.00113548    -0.00073656    -0.00106517     0.00040290     0.00022774    -0.00030590
   ht  13     0.00292921    -0.00067650    -0.00414234    -0.00546354    -0.00135955    -0.00147174     0.00048843    -0.00014616
   ht  14    -0.00093342     0.00099979     0.00114063    -0.00273577     0.00716502     0.00762763    -0.00074299     0.00048409

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14
   ht   9    -0.00001041
   ht  10     0.00000082    -0.00001666
   ht  11     0.00000046    -0.00000416    -0.00028349
   ht  12    -0.00000001    -0.00000012     0.00000505    -0.00000158
   ht  13     0.00000032    -0.00000008    -0.00000207     0.00000015    -0.00000296
   ht  14    -0.00000221     0.00000037     0.00000351     0.00000010     0.00000017    -0.00000507

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -1.962280E-05   0.821454      -0.208915       0.182662      -0.432382      -3.166444E-02  -8.809865E-02   6.531621E-02
 ref    2   1.034613E-04  -4.799962E-02  -0.498261      -0.354030       5.482654E-02  -0.748922      -4.049385E-02  -5.108211E-02

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1  -2.786119E-02  -1.863578E-02   3.762274E-03  -5.728514E-03  -3.695282E-02   5.437342E-02
 ref    2  -4.795523E-02   3.980752E-02  -2.870929E-02   1.051808E-02  -6.009387E-02   9.762148E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   1.108929E-08   0.677090       0.291909       0.158703       0.189961       0.561887       9.401124E-03   6.875589E-03

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1   3.075950E-03   1.931931E-03   8.383779E-04   1.434459E-04   4.976784E-03   1.248642E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00001962     0.82145376    -0.20891509     0.18266237    -0.43238244    -0.03166444    -0.08809865     0.06531621
 ref:   2     0.00010346    -0.04799962    -0.49826084    -0.35402974     0.05482654    -0.74892230    -0.04049385    -0.05108211

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14
 ref:   1    -0.02786119    -0.01863578     0.00376227    -0.00572851    -0.03695282     0.05437342
 ref:   2    -0.04795523     0.03980752    -0.02870929     0.01051808    -0.06009387     0.09762148

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 76  1   -196.4879555280  8.6877E-08  0.0000E+00  2.8280E-03  1.0000E-04   
 mr-sdci # 76  2   -196.4725827988  1.4069E-12  0.0000E+00  3.8725E-05  1.0000E-04   
 mr-sdci # 76  3   -196.4725827837  2.4421E-10  0.0000E+00  1.9620E-04  1.0000E-04   
 mr-sdci # 76  4   -196.4725827675  7.2887E-10  0.0000E+00  2.8941E-04  1.0000E-04   
 mr-sdci # 76  5   -196.4644811519  1.0573E-07  0.0000E+00  1.5502E-04  1.0000E-04   
 mr-sdci # 76  6   -196.4644809823  1.4013E-10  1.4957E-07  6.9280E-04  1.0000E-04   
 mr-sdci # 76  7   -195.7842189965  3.5283E-03  0.0000E+00  2.1550E-01  1.0000E-04   
 mr-sdci # 76  8   -195.7715592312  8.2946E-05  0.0000E+00  2.8504E-01  1.0000E-04   
 mr-sdci # 76  9   -195.6256396326  9.6809E-01  0.0000E+00  6.0430E-01  1.0000E-04   
 mr-sdci # 76 10   -194.6196812707  7.9917E-01  0.0000E+00  2.9647E+00  1.0000E-04   
 mr-sdci # 76 11   -193.6848276542  1.9864E-01  0.0000E+00  3.2121E+00  1.0000E-04   
 mr-sdci # 76 12   -193.0428924128  1.3961E-01  0.0000E+00  3.4854E+00  1.0000E-04   
 mr-sdci # 76 13   -192.1252914809  1.3062E-01  0.0000E+00  4.4207E+00  1.0000E-04   
 mr-sdci # 76 14   -190.6596208146 -2.0516E+00  0.0000E+00  3.8548E+00  1.0000E-04   
 
 root number  6 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  77

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59963363
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426793
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426788
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616607
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616588
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88950206
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88066382
   ht   9    -0.00075677     0.00105615     0.00038181    -0.00292536     0.01006789     0.01067354    -0.00053410     0.00015926
   ht  10    -0.00409108     0.00911125    -0.01547650     0.00147891    -0.00409410     0.00401829    -0.00007518    -0.00091053
   ht  11     0.05011436     0.02227819     0.00527350     0.01456934    -0.02595700     0.02012466    -0.00138207    -0.00150194
   ht  12    -0.00374062    -0.00132099    -0.00113548    -0.00073656    -0.00106517     0.00040290     0.00022774    -0.00030590
   ht  13     0.00292921    -0.00067650    -0.00414234    -0.00546354    -0.00135955    -0.00147174     0.00048843    -0.00014616
   ht  14    -0.00093342     0.00099979     0.00114063    -0.00273577     0.00716502     0.00762763    -0.00074299     0.00048409
   ht  15    -0.00049777     0.00948216    -0.00780200     0.00515944    -0.00354794     0.00387919    -0.00006160    -0.00192592

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15
   ht   9    -0.00001041
   ht  10     0.00000082    -0.00001666
   ht  11     0.00000046    -0.00000416    -0.00028349
   ht  12    -0.00000001    -0.00000012     0.00000505    -0.00000158
   ht  13     0.00000032    -0.00000008    -0.00000207     0.00000015    -0.00000296
   ht  14    -0.00000221     0.00000037     0.00000351     0.00000010     0.00000017    -0.00000507
   ht  15    -0.00000029    -0.00000338    -0.00000473    -0.00000008     0.00000008     0.00000016    -0.00000758

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   3.659424E-05   0.821529       0.196391       0.195749       0.424077      -9.008518E-02   8.815878E-02  -6.444939E-02
 ref    2  -9.191717E-05  -4.608921E-02   0.517406      -0.325687       0.156033       0.734526       3.968204E-02   5.219449E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1  -5.640361E-03  -3.204774E-02   1.617775E-02  -2.151117E-02   4.968848E-02   4.160973E-02   4.688585E-02
 ref    2  -1.398374E-02  -6.117819E-02   1.728328E-02  -8.976685E-04   1.598102E-02   0.116011      -1.250248E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   9.787904E-09   0.677034       0.306278       0.144389       0.204187       0.547644       9.346634E-03   6.877989E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1   2.273586E-04   4.769829E-03   5.604313E-04   4.635364E-04   2.724338E-03   1.518995E-02   2.354595E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00003659     0.82152909     0.19639122     0.19574903     0.42407664    -0.09008518     0.08815878    -0.06444939
 ref:   2    -0.00009192    -0.04608921     0.51740555    -0.32568663     0.15603261     0.73452632     0.03968204     0.05219449

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15
 ref:   1    -0.00564036    -0.03204774     0.01617775    -0.02151117     0.04968848     0.04160973     0.04688585
 ref:   2    -0.01398374    -0.06117819     0.01728328    -0.00089767     0.01598102     0.11601112    -0.01250248

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 77  1   -196.4879559759  4.4788E-07  2.3195E-06  2.6505E-03  1.0000E-04   
 mr-sdci # 77  2   -196.4725827988  2.6077E-12  0.0000E+00  3.8249E-05  1.0000E-04   
 mr-sdci # 77  3   -196.4725827842  4.7122E-10  0.0000E+00  1.8933E-04  1.0000E-04   
 mr-sdci # 77  4   -196.4725827692  1.6099E-09  0.0000E+00  2.7177E-04  1.0000E-04   
 mr-sdci # 77  5   -196.4644811530  1.0860E-09  0.0000E+00  1.3201E-04  1.0000E-04   
 mr-sdci # 77  6   -196.4644811400  1.5769E-07  0.0000E+00  2.6870E-04  1.0000E-04   
 mr-sdci # 77  7   -195.7842592629  4.0266E-05  0.0000E+00  2.1762E-01  1.0000E-04   
 mr-sdci # 77  8   -195.7716585931  9.9362E-05  0.0000E+00  2.8164E-01  1.0000E-04   
 mr-sdci # 77  9   -195.6935454511  6.7906E-02  0.0000E+00  5.8484E-01  1.0000E-04   
 mr-sdci # 77 10   -195.6096708660  9.8999E-01  0.0000E+00  7.4698E-01  1.0000E-04   
 mr-sdci # 77 11   -193.8226243074  1.3780E-01  0.0000E+00  3.1661E+00  1.0000E-04   
 mr-sdci # 77 12   -193.2000854666  1.5719E-01  0.0000E+00  3.7697E+00  1.0000E-04   
 mr-sdci # 77 13   -192.6649032222  5.3961E-01  0.0000E+00  3.8955E+00  1.0000E-04   
 mr-sdci # 77 14   -190.7425590905  8.2938E-02  0.0000E+00  3.8224E+00  1.0000E-04   
 mr-sdci # 77 15   -190.2325965531 -9.0170E-01  0.0000E+00  4.3848E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.016000
time for eigenvalue solver             0.001000
time for vector access                 0.000000

          starting ci iteration  78

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59963363
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426793
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426788
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616607
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616588
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88950206
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88066382
   ht   9    -0.00075677     0.00105615     0.00038181    -0.00292536     0.01006789     0.01067354    -0.00053410     0.00015926
   ht  10    -0.00409108     0.00911125    -0.01547650     0.00147891    -0.00409410     0.00401829    -0.00007518    -0.00091053
   ht  11     0.05011436     0.02227819     0.00527350     0.01456934    -0.02595700     0.02012466    -0.00138207    -0.00150194
   ht  12    -0.00374062    -0.00132099    -0.00113548    -0.00073656    -0.00106517     0.00040290     0.00022774    -0.00030590
   ht  13     0.00292921    -0.00067650    -0.00414234    -0.00546354    -0.00135955    -0.00147174     0.00048843    -0.00014616
   ht  14    -0.00093342     0.00099979     0.00114063    -0.00273577     0.00716502     0.00762763    -0.00074299     0.00048409
   ht  15    -0.00049777     0.00948216    -0.00780200     0.00515944    -0.00354794     0.00387919    -0.00006160    -0.00192592
   ht  16     0.04617236    -0.00095520     0.00477213     0.00839494    -0.01514045     0.01929804     0.00054956    -0.00080188

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15         ht  16
   ht   9    -0.00001041
   ht  10     0.00000082    -0.00001666
   ht  11     0.00000046    -0.00000416    -0.00028349
   ht  12    -0.00000001    -0.00000012     0.00000505    -0.00000158
   ht  13     0.00000032    -0.00000008    -0.00000207     0.00000015    -0.00000296
   ht  14    -0.00000221     0.00000037     0.00000351     0.00000010     0.00000017    -0.00000507
   ht  15    -0.00000029    -0.00000338    -0.00000473    -0.00000008     0.00000008     0.00000016    -0.00000758
   ht  16     0.00000012     0.00000427    -0.00006517     0.00000319    -0.00000179     0.00000062    -0.00000194    -0.00011561

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   1.222208E-05   0.820700      -0.199851       0.195727       0.425550       8.284778E-02  -8.077217E-02   7.326145E-02
 ref    2  -2.041662E-05  -4.840549E-02  -0.517404      -0.325353       0.143498      -0.737076      -3.674705E-02  -4.296701E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   1.214645E-02   2.556341E-02  -3.587574E-02  -2.117920E-02   4.164620E-02  -1.916615E-02  -4.342402E-02  -4.474745E-02
 ref    2   4.732854E-02   6.284806E-02   3.707640E-02  -1.299980E-02  -1.470389E-03  -5.443837E-02  -0.110611       6.833060E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   5.662174E-10   0.675891       0.307648       0.144164       0.201684       0.550145       7.874488E-03   7.213404E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   2.387527E-03   4.603367E-03   2.661728E-03   6.175533E-04   1.736568E-03   3.330878E-03   1.412050E-02   2.049025E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15        ev   16

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00001222     0.82069970    -0.19985115     0.19572651     0.42554952     0.08284778    -0.08077217     0.07326145
 ref:   2    -0.00002042    -0.04840549    -0.51740428    -0.32535335     0.14349820    -0.73707632    -0.03674705    -0.04296701

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1     0.01214645     0.02556341    -0.03587574    -0.02117920     0.04164620    -0.01916615    -0.04342402    -0.04474745
 ref:   2     0.04732854     0.06284806     0.03707640    -0.01299980    -0.00147039    -0.05443837    -0.11061127     0.00683306

 trial vector basis is being transformed.  new dimension:   8

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 78  1   -196.4879580105  2.0346E-06  0.0000E+00  1.4946E-03  1.0000E-04   
 mr-sdci # 78  2   -196.4725827988  3.5662E-11  0.0000E+00  3.6783E-05  1.0000E-04   
 mr-sdci # 78  3   -196.4725827843  1.1318E-10  1.2338E-08  1.8206E-04  1.0000E-04   
 mr-sdci # 78  4   -196.4725827692  7.3186E-13  0.0000E+00  2.7191E-04  1.0000E-04   
 mr-sdci # 78  5   -196.4644811530  6.5576E-11  0.0000E+00  1.3290E-04  1.0000E-04   
 mr-sdci # 78  6   -196.4644811407  7.1073E-10  0.0000E+00  2.5728E-04  1.0000E-04   
 mr-sdci # 78  7   -195.7861612793  1.9020E-03  0.0000E+00  2.3325E-01  1.0000E-04   
 mr-sdci # 78  8   -195.7724924978  8.3390E-04  0.0000E+00  2.8118E-01  1.0000E-04   
 mr-sdci # 78  9   -195.7184434306  2.4898E-02  0.0000E+00  5.2695E-01  1.0000E-04   
 mr-sdci # 78 10   -195.6351249292  2.5454E-02  0.0000E+00  6.3571E-01  1.0000E-04   
 mr-sdci # 78 11   -194.7087817145  8.8616E-01  0.0000E+00  2.5475E+00  1.0000E-04   
 mr-sdci # 78 12   -193.8073439136  6.0726E-01  0.0000E+00  3.0948E+00  1.0000E-04   
 mr-sdci # 78 13   -193.1166651808  4.5176E-01  0.0000E+00  4.1033E+00  1.0000E-04   
 mr-sdci # 78 14   -191.7900536386  1.0475E+00  0.0000E+00  4.0351E+00  1.0000E-04   
 mr-sdci # 78 15   -190.7332904846  5.0069E-01  0.0000E+00  3.8216E+00  1.0000E-04   
 mr-sdci # 78 16   -190.1683355435 -3.6720E-01  0.0000E+00  4.3428E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.015000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  79

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59964321
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426799
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426797
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616636
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616634
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.89784648
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88417770
   ht   9    -0.00152451     0.00118593    -0.00092356     0.00122216     0.00133367     0.00116657    -0.00017233     0.00017364

                ht   9
   ht   9    -0.00000051

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7         v:   8

   eig(s)   6.732225E-09    1.00000        1.00000        1.00000        1.00000        1.00000        1.00000        1.00000    
 
   x:   1  -3.012857E-05   0.184328      -0.143547       0.189960       0.207325       0.181349      -2.759181E-02   0.763120    
   x:   2   2.344460E-05   0.856565       0.111701      -0.147818      -0.161330      -0.141117       2.147062E-02   0.184328    
   x:   3  -1.825758E-05   0.111701       0.913012       0.115114       0.125636       0.109895      -1.672034E-02  -0.143547    
   x:   4   2.416089E-05  -0.147818       0.115114       0.847666      -0.166259      -0.145428       2.212660E-02   0.189960    
   x:   5   2.636946E-05  -0.161330       0.125636      -0.166259       0.818543      -0.158722       2.414922E-02   0.207325    
   x:   6   2.306564E-05  -0.141117       0.109895      -0.145428      -0.158722       0.861164       2.112357E-02   0.181349    
   x:   7  -3.509383E-06   2.147062E-02  -1.672034E-02   2.212660E-02   2.414922E-02   2.112357E-02   0.996786      -2.759181E-02
   x:   8   3.423625E-06  -0.389346       0.303205      -0.401242      -0.437920      -0.383053       5.828063E-02   0.500348    
   x:   9    1.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000    

                v:   9

   eig(s)    1.00000    
 
   x:   1   0.500348    
   x:   2  -0.389346    
   x:   3   0.303205    
   x:   4  -0.401242    
   x:   5  -0.437920    
   x:   6  -0.383053    
   x:   7   5.828063E-02
   x:   8  -5.685644E-02
   x:   9   6.021526E-05
 bummer (warning):overlap matrix: # small eigenvalues=                      1

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -8.721018E-06   0.817813      -0.204529       0.202849      -0.425533      -8.293451E-02   7.593970E-02  -7.820312E-02
 ref    2   1.808424E-05  -5.622650E-02  -0.528304      -0.305999      -0.143648       0.737047       3.961211E-02   4.189107E-02

              v      9
 ref    1  -5.839900E-02
 ref    2   3.986237E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   4.030960E-10   0.671980       0.320937       0.134783       0.201713       0.550116       7.335958E-03   7.870590E-03

              v      9
 ref    1   4.999452E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00000872     0.81781348    -0.20452851     0.20284915    -0.42553269    -0.08293451     0.07593970    -0.07820312
 ref:   2     0.00001808    -0.05622650    -0.52830383    -0.30599949    -0.14364841     0.73704701     0.03961211     0.04189107

                ci   9
 ref:   1    -0.05839900
 ref:   2     0.03986237

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 79  1   -196.4879580190  8.4882E-09  0.0000E+00  1.4675E-03  1.0000E-04   
 mr-sdci # 79  2   -196.4725827988  1.7764E-12  0.0000E+00  3.6591E-05  1.0000E-04   
 mr-sdci # 79  3   -196.4725827935  9.2377E-09  0.0000E+00  1.1449E-04  1.0000E-04   
 mr-sdci # 79  4   -196.4725827692  5.4357E-11  2.7355E-08  2.7002E-04  1.0000E-04   
 mr-sdci # 79  5   -196.4644811530  4.3343E-12  0.0000E+00  1.3258E-04  1.0000E-04   
 mr-sdci # 79  6   -196.4644811407  1.4495E-12  0.0000E+00  2.5666E-04  1.0000E-04   
 mr-sdci # 79  7   -195.7868413013  6.8002E-04  0.0000E+00  2.3408E-01  1.0000E-04   
 mr-sdci # 79  8   -195.7731453894  6.5289E-04  0.0000E+00  2.7149E-01  1.0000E-04   
 mr-sdci # 79  9   -194.0117107406 -1.7067E+00  0.0000E+00  3.0411E+00  1.0000E-04   
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  80

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59964321
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426799
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426797
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616636
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616634
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.89784648
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88417770
   ht   9    -0.00152451     0.00118593    -0.00092356     0.00122216     0.00133367     0.00116657    -0.00017233     0.00017364
   ht  10     0.00230914     0.00121062    -0.00265368     0.00451530     0.00068381    -0.00089663    -0.00031962     0.00004488

                ht   9         ht  10
   ht   9    -0.00000051
   ht  10    -0.00000010    -0.00000143

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7         v:   8

   eig(s)   6.721296E-09   1.520516E-08    1.00000        1.00000        1.00000        1.00000        1.00000        1.00000    
 
   x:   1   2.847117E-05   4.668755E-05   0.461680       0.206270      -9.161576E-02  -4.478242E-02   0.140459      -0.361128    
   x:   2  -2.428850E-05   2.307585E-05   0.221292       4.015608E-02  -0.121096      -0.846261      -0.238235      -5.854831E-02
   x:   3   2.012877E-05  -5.177142E-05  -0.269552      -3.739074E-02   0.454107      -0.385060       0.369702      -0.456274    
   x:   4  -2.734923E-05   8.833885E-05  -0.378237      -0.307302       0.218307       1.866444E-02   0.326236      -8.548346E-02
   x:   5  -2.683775E-05   1.256506E-05   0.235036       0.513873       0.537700       0.279229      -0.180862      -0.297303    
   x:   6  -2.241446E-05  -1.854479E-05   0.358152      -0.222534      -0.415542       0.180747       0.485987      -0.388540    
   x:   7   3.740892E-06  -6.382874E-06   0.565812      -0.587657       0.504895       2.271660E-03  -1.390048E-02   0.272972    
   x:   8  -3.451414E-06   7.122629E-07   0.136025       0.448421       7.972372E-02  -0.150245       0.641340       0.580344    
   x:   9  -0.999356      -3.589266E-02   1.045348E-12   3.325173E-13  -1.053952E-12  -3.808406E-13  -6.022783E-13   1.736591E-13
   x:  10  -3.589266E-02   0.999356        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000    

                v:   9         v:  10

   eig(s)    1.00000        1.00000    
 
   x:   1  -0.693679       0.320271    
   x:   2   0.320835       0.235533    
   x:   3  -0.125272      -0.459239    
   x:   4   9.269663E-02   0.769523    
   x:   5   0.410212       0.155657    
   x:   6   0.467676      -0.104856    
   x:   7  -3.723106E-02  -5.915465E-02
   x:   8   5.664651E-02   1.270961E-02
   x:   9  -5.487708E-05  -2.478675E-05
   x:  10   1.158680E-05  -1.173943E-04
 bummer (warning):overlap matrix: # small eigenvalues=                      1

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -7.843005E-06  -0.820878      -0.197869      -0.196985      -0.424329      -8.889159E-02   7.214624E-02   8.115862E-02
 ref    2   1.819476E-05   6.221370E-02  -0.540151       0.283317      -0.153967       0.734961       4.074353E-02  -4.056092E-02

              v      9       v     10
 ref    1   2.788257E-02  -8.580234E-02
 ref    2  -3.874927E-02   1.085675E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   3.925620E-10   0.677711       0.330916       0.119072       0.203761       0.548069       6.865115E-03   8.231910E-03

              v      9       v     10
 ref    1   2.278944E-03   7.479911E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00000784    -0.82087811    -0.19786938    -0.19698548    -0.42432899    -0.08889159     0.07214624     0.08115862
 ref:   2     0.00001819     0.06221370    -0.54015115     0.28331687    -0.15396697     0.73496072     0.04074353    -0.04056092

                ci   9         ci  10
 ref:   1     0.02788257    -0.08580234
 ref:   2    -0.03874927     0.01085675

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 80  1   -196.4879580196  6.0355E-10  0.0000E+00  1.4778E-03  1.0000E-04   
 mr-sdci # 80  2   -196.4725827988  5.7980E-12  0.0000E+00  3.6468E-05  1.0000E-04   
 mr-sdci # 80  3   -196.4725827935  1.4609E-11  0.0000E+00  1.1483E-04  1.0000E-04   
 mr-sdci # 80  4   -196.4725827870  1.7795E-08  0.0000E+00  1.6459E-04  1.0000E-04   
 mr-sdci # 80  5   -196.4644811534  3.7657E-10  6.2452E-09  1.2670E-04  1.0000E-04   
 mr-sdci # 80  6   -196.4644811408  8.1656E-11  0.0000E+00  2.5411E-04  1.0000E-04   
 mr-sdci # 80  7   -195.7877303924  8.8909E-04  0.0000E+00  2.2778E-01  1.0000E-04   
 mr-sdci # 80  8   -195.7733607136  2.1532E-04  0.0000E+00  2.6726E-01  1.0000E-04   
 mr-sdci # 80  9   -194.0766155264  6.4905E-02  0.0000E+00  2.9800E+00  1.0000E-04   
 mr-sdci # 80 10   -193.6251892976 -2.0099E+00  0.0000E+00  3.5970E+00  1.0000E-04   
 
 root number  5 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  81

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59964321
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426799
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426797
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616636
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616634
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.89784648
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88417770
   ht   9    -0.00152451     0.00118593    -0.00092356     0.00122216     0.00133367     0.00116657    -0.00017233     0.00017364
   ht  10     0.00230914     0.00121062    -0.00265368     0.00451530     0.00068381    -0.00089663    -0.00031962     0.00004488
   ht  11     0.00003589     0.00047238     0.00084002    -0.00012847    -0.00054114     0.00101518     0.00011863     0.00011523

                ht   9         ht  10         ht  11
   ht   9    -0.00000051
   ht  10    -0.00000010    -0.00000143
   ht  11     0.00000000     0.00000005    -0.00000023

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7         v:   8

   eig(s)   3.811330E-09   6.723137E-09   1.521888E-08    1.00000        1.00000        1.00000        1.00000        1.00000    
 
   x:   1  -1.626021E-06  -2.842531E-05   4.669257E-05   0.204523      -1.683349E-02  -8.064379E-02   0.530988       5.438391E-02
   x:   2   9.140977E-06   2.407319E-05   2.337859E-05  -0.355255      -0.146153      -0.132531       0.256357      -0.685739    
   x:   3   1.787939E-05  -2.060029E-05  -5.115831E-05  -0.512798       0.504980      -7.819136E-02   5.117613E-02   0.219486    
   x:   4  -4.911683E-06   2.750784E-05   8.818944E-05  -0.220257       0.238055       0.103224      -0.375895       0.319731    
   x:   5  -1.045034E-05   2.711282E-05   1.217839E-05  -0.136905       0.237379      -0.124619       0.670958       0.318387    
   x:   6   2.126038E-05   2.188130E-05  -1.784419E-05   0.526310      -8.473796E-02   0.166249       0.139742       0.277222    
   x:   7   2.462610E-06  -3.805919E-06  -6.296842E-06  -0.466862      -0.758060       0.160186       8.844683E-02   0.405340    
   x:   8   2.380248E-06   3.392856E-06   7.912736E-07   8.237196E-02  -0.168810      -0.943576      -0.175729       0.184064    
   x:   9   2.638021E-02   0.998997      -3.616987E-02  -3.855850E-12  -1.963202E-12  -1.512398E-12  -1.200980E-12  -7.170712E-13
   x:  10  -3.373844E-02   3.705160E-02   0.998744       7.832875E-13  -1.061962E-13  -1.991859E-12  -2.477542E-13   2.048982E-13
   x:  11   0.999082      -2.512675E-02   3.468204E-02    0.00000        0.00000      -3.254112E-12    0.00000        0.00000    

                v:   9         v:  10         v:  11

   eig(s)    1.00000        1.00000        1.00000    
 
   x:   1  -0.334259       0.673710      -0.317589    
   x:   2  -0.360573      -0.343570      -0.225518    
   x:   3  -0.442061       0.102282       0.468239    
   x:   4  -0.210567      -0.109209      -0.764442    
   x:   5   0.426272      -0.386984      -0.162352    
   x:   6  -0.571149      -0.501053       0.120556    
   x:   7  -6.943095E-02   3.357983E-02   6.044888E-02
   x:   8  -7.590634E-02  -6.121635E-02  -1.066546E-02
   x:   9  -2.651165E-06   5.494790E-05   2.448626E-05
   x:  10   3.211286E-06  -1.078924E-05   1.174264E-04
   x:  11   2.677405E-05   6.735003E-06  -1.165953E-05
 bummer (warning):overlap matrix: # small eigenvalues=                      2

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -7.638082E-06  -0.821022       0.197641      -0.196615      -0.427216       7.377784E-02   7.335401E-02   7.994334E-02
 ref    2   2.355407E-05   6.259945E-02   0.540722       0.282140      -0.127785      -0.739962       4.222321E-02  -4.074520E-02

              v      9       v     10       v     11
 ref    1  -1.306768E-02   3.917645E-02   8.065530E-02
 ref    2   6.239994E-02  -5.527132E-02   1.635910E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   6.131346E-10   0.677996       0.331442       0.118261       0.198842       0.552987       7.163610E-03   8.051109E-03

              v      9       v     10       v     11
 ref    1   4.064517E-03   4.589713E-03   6.772898E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00000764    -0.82102182     0.19764136    -0.19661514    -0.42721593     0.07377784     0.07335401     0.07994334
 ref:   2     0.00002355     0.06259945     0.54072165     0.28214047    -0.12778514    -0.73996183     0.04222321    -0.04074520

                ci   9         ci  10         ci  11
 ref:   1    -0.01306768     0.03917645     0.08065530
 ref:   2     0.06239994    -0.05527132     0.01635910

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 81  1   -196.4879580295  9.8600E-09  0.0000E+00  1.4425E-03  1.0000E-04   
 mr-sdci # 81  2   -196.4725827988  9.4502E-13  0.0000E+00  3.6592E-05  1.0000E-04   
 mr-sdci # 81  3   -196.4725827935  1.4282E-12  0.0000E+00  1.1492E-04  1.0000E-04   
 mr-sdci # 81  4   -196.4725827871  1.2844E-10  0.0000E+00  1.6542E-04  1.0000E-04   
 mr-sdci # 81  5   -196.4644811587  5.3165E-09  0.0000E+00  7.2295E-05  1.0000E-04   
 mr-sdci # 81  6   -196.4644811408  5.3745E-11  2.0761E-08  2.5154E-04  1.0000E-04   
 mr-sdci # 81  7   -195.7887922393  1.0618E-03  0.0000E+00  2.1890E-01  1.0000E-04   
 mr-sdci # 81  8   -195.7734163439  5.5630E-05  0.0000E+00  2.6520E-01  1.0000E-04   
 mr-sdci # 81  9   -194.6217982949  5.4518E-01  0.0000E+00  2.1198E+00  1.0000E-04   
 mr-sdci # 81 10   -194.0431296669  4.1794E-01  0.0000E+00  3.0019E+00  1.0000E-04   
 mr-sdci # 81 11   -193.5257535503 -1.1830E+00  0.0000E+00  3.6550E+00  1.0000E-04   
 
 root number  6 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.008000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  82

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59964321
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426799
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426797
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616636
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616634
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.89784648
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88417770
   ht   9    -0.00152451     0.00118593    -0.00092356     0.00122216     0.00133367     0.00116657    -0.00017233     0.00017364
   ht  10     0.00230914     0.00121062    -0.00265368     0.00451530     0.00068381    -0.00089663    -0.00031962     0.00004488
   ht  11     0.00003589     0.00047238     0.00084002    -0.00012847    -0.00054114     0.00101518     0.00011863     0.00011523
   ht  12    -0.00089540     0.00260371     0.00028144     0.00064729     0.00091227     0.00038901    -0.00011480     0.00047743

                ht   9         ht  10         ht  11         ht  12
   ht   9    -0.00000051
   ht  10    -0.00000010    -0.00000143
   ht  11     0.00000000     0.00000005    -0.00000023
   ht  12    -0.00000010    -0.00000005    -0.00000005    -0.00000071

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7         v:   8

   eig(s)   3.770779E-09   6.614228E-09   1.115360E-08   1.523921E-08    1.00000        1.00000        1.00000        1.00000    
 
   x:   1   1.335902E-07   3.059953E-05  -9.820250E-06  -4.765722E-05  -7.290011E-02   0.151641       0.549974      -4.968681E-02
   x:   2   4.754290E-06  -3.207246E-05   4.913963E-05  -1.982723E-05  -5.110784E-02   0.359504      -6.344930E-02  -7.278325E-02
   x:   3   1.787215E-05   1.923165E-05   6.365165E-06   5.159833E-05   0.182592      -0.639639       4.774694E-02  -0.231834    
   x:   4  -6.523162E-06  -2.944670E-05   1.413042E-05  -8.725021E-05   9.617245E-02  -0.476024      -0.293047      -7.509629E-02
   x:   5  -1.228829E-05  -2.930564E-05   1.361877E-05  -1.107631E-05  -8.395140E-02  -0.299001       0.685357      -0.229698    
   x:   6   2.025845E-05  -2.332525E-05   4.434675E-06   1.828217E-05  -2.994395E-02   0.183847       0.298192       6.237581E-03
   x:   7   2.701583E-06   4.057764E-06  -1.838858E-06   6.157053E-06  -0.961120      -0.143101      -0.149419      -0.152498    
   x:   8   1.587247E-06  -4.898161E-06   9.124089E-06  -1.298987E-07  -0.133373      -0.261429       0.156164       0.925630    
   x:   9   8.491045E-03  -0.987280      -0.155824       3.041815E-02   1.090960E-11   1.887344E-12  -2.235969E-12  -6.052374E-13
   x:  10  -3.620798E-02  -4.077270E-02   6.181302E-02  -0.996597      -3.208937E-12  -1.053118E-12   1.375405E-12   3.246967E-13
   x:  11   0.996456      -4.699437E-03   7.798756E-02  -3.117348E-02   8.315703E-13  -5.854780E-12   1.668417E-12   1.895073E-12
   x:  12  -7.545074E-02  -0.153604       0.982759       6.998022E-02  -6.749434E-12    0.00000        0.00000        0.00000    

                v:   9         v:  10         v:  11         v:  12

   eig(s)    1.00000        1.00000        1.00000        1.00000    
 
   x:   1   0.116707       0.485042      -0.585414      -0.274145    
   x:   2  -0.229921       0.578358       0.618782      -0.297605    
   x:   3   8.543659E-02   0.527192       0.119479       0.449431    
   x:   4   0.296703      -1.402446E-02   1.307781E-03  -0.764464    
   x:   5  -0.339128      -0.354034       0.323983      -0.189108    
   x:   6   0.847075      -9.643021E-02   0.374040       9.800739E-02
   x:   7   5.897676E-02   4.804930E-02  -2.247553E-02   6.242330E-02
   x:   8  -4.649173E-02   0.123128       0.121436      -2.487650E-02
   x:   9  -6.931702E-06   2.232534E-05  -4.766033E-05   2.842360E-05
   x:  10  -1.800745E-06  -3.781070E-06   2.006059E-05   1.161711E-04
   x:  11  -1.926206E-05  -1.678578E-05  -1.161470E-05  -1.051019E-05
   x:  12   9.805031E-06  -1.788724E-05  -5.282655E-05   2.078225E-05
 bummer (warning):overlap matrix: # small eigenvalues=                      2

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   1.155615E-05   0.821153       0.196982       0.196729      -0.429793       5.686787E-02   7.353305E-02  -8.051519E-02
 ref    2  -2.483261E-05  -6.215308E-02   0.540808      -0.282073      -9.850282E-02  -0.744424       4.195946E-02   4.019624E-02

              v      9       v     10       v     11       v     12
 ref    1   2.061938E-02  -3.642466E-02   5.384499E-03  -9.625376E-02
 ref    2  -6.496264E-02   5.407268E-02   2.129263E-02  -5.380729E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   7.502032E-10   0.678155       0.331276       0.118268       0.194425       0.557401       7.167705E-03   8.098434E-03

              v      9       v     10       v     11       v     12
 ref    1   4.645303E-03   4.250611E-03   4.823691E-04   9.293739E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00001156     0.82115301     0.19698208     0.19672919    -0.42979306     0.05686787     0.07353305    -0.08051519
 ref:   2    -0.00002483    -0.06215308     0.54080835    -0.28207297    -0.09850282    -0.74442407     0.04195946     0.04019624

                ci   9         ci  10         ci  11         ci  12
 ref:   1     0.02061938    -0.03642466     0.00538450    -0.09625376
 ref:   2    -0.06496264     0.05407268     0.02129263    -0.00538073

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 82  1   -196.4879580450  1.5550E-08  3.8826E-07  1.4282E-03  1.0000E-04   
 mr-sdci # 82  2   -196.4725827988  5.6986E-12  0.0000E+00  3.6367E-05  1.0000E-04   
 mr-sdci # 82  3   -196.4725827935  2.8564E-12  0.0000E+00  1.1596E-04  1.0000E-04   
 mr-sdci # 82  4   -196.4725827871  2.4158E-13  0.0000E+00  1.6550E-04  1.0000E-04   
 mr-sdci # 82  5   -196.4644811587  6.3167E-12  0.0000E+00  7.1073E-05  1.0000E-04   
 mr-sdci # 82  6   -196.4644811554  1.4628E-08  0.0000E+00  1.0718E-04  1.0000E-04   
 mr-sdci # 82  7   -195.7897063396  9.1410E-04  0.0000E+00  2.1123E-01  1.0000E-04   
 mr-sdci # 82  8   -195.7734447178  2.8374E-05  0.0000E+00  2.6466E-01  1.0000E-04   
 mr-sdci # 82  9   -194.6825719170  6.0774E-02  0.0000E+00  2.1208E+00  1.0000E-04   
 mr-sdci # 82 10   -194.0532400624  1.0110E-02  0.0000E+00  2.7833E+00  1.0000E-04   
 mr-sdci # 82 11   -193.8070635272  2.8131E-01  0.0000E+00  2.9562E+00  1.0000E-04   
 mr-sdci # 82 12   -193.3485905479 -4.5875E-01  0.0000E+00  3.7349E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.014000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  83

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59964321
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426799
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426797
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616636
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616634
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.89784648
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88417770
   ht   9    -0.00152451     0.00118593    -0.00092356     0.00122216     0.00133367     0.00116657    -0.00017233     0.00017364
   ht  10     0.00230914     0.00121062    -0.00265368     0.00451530     0.00068381    -0.00089663    -0.00031962     0.00004488
   ht  11     0.00003589     0.00047238     0.00084002    -0.00012847    -0.00054114     0.00101518     0.00011863     0.00011523
   ht  12    -0.00089540     0.00260371     0.00028144     0.00064729     0.00091227     0.00038901    -0.00011480     0.00047743
   ht  13    -0.00958891    -0.00447343     0.00448385     0.00406474     0.00273195    -0.00191363     0.00128484     0.00075124

                ht   9         ht  10         ht  11         ht  12         ht  13
   ht   9    -0.00000051
   ht  10    -0.00000010    -0.00000143
   ht  11     0.00000000     0.00000005    -0.00000023
   ht  12    -0.00000010    -0.00000005    -0.00000005    -0.00000071
   ht  13    -0.00000020     0.00000028    -0.00000003     0.00000003    -0.00001118

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7         v:   8

   eig(s)   3.754727E-09   6.610240E-09   1.114267E-08   1.521898E-08   1.664176E-07    1.00000        1.00000        1.00000    
 
   x:   1   2.195170E-06  -3.160729E-05  -1.153673E-05  -4.977132E-05  -1.886777E-04   0.248872      -0.166448      -0.312346    
   x:   2   5.489460E-06   3.169391E-05   4.828482E-05  -2.105452E-05  -8.871584E-05  -0.172993      -5.014432E-02   0.157215    
   x:   3   1.695246E-05  -1.867653E-05   7.355914E-06   5.264357E-05   8.824847E-05   1.521753E-02  -0.112368      -0.396761    
   x:   4  -7.296965E-06   2.976066E-05   1.443549E-05  -8.641061E-05   8.102394E-05  -8.631370E-02   2.029752E-02  -1.338699E-02
   x:   5  -1.291397E-05   2.954826E-05   1.396599E-05  -1.055375E-05   5.375365E-05   0.374996      -0.364213      -0.587111    
   x:   6   2.053363E-05   2.321820E-05   4.185093E-06   1.783568E-05  -3.799528E-05   0.114794      -5.191821E-02  -0.135052    
   x:   7   2.446627E-06  -3.916043E-06  -1.586456E-06   6.473419E-06   2.609398E-05   0.368828      -0.709667       0.572378    
   x:   8   1.406027E-06   4.991587E-06   9.246833E-06   9.595914E-09   1.515178E-05   0.781422       0.563710       0.171959    
   x:   9   5.971391E-03   0.987094      -0.157155       2.994768E-02  -3.896433E-03  -4.750164E-13   1.633498E-13  -2.154505E-12
   x:  10  -3.491309E-02   3.974682E-02   5.810274E-02  -0.996853       1.047223E-02   2.460324E-13  -9.317577E-14   3.690245E-13
   x:  11   0.996298       7.604339E-03   7.969490E-02  -2.984491E-02   9.559114E-03   1.198731E-13   5.937387E-13   4.249029E-12
   x:  12  -7.770117E-02   0.154864       0.982601       6.605722E-02  -1.056227E-02   7.732304E-13   2.835516E-13   1.810197E-12
   x:  13  -9.957169E-03   4.993745E-03   8.397252E-03   1.154085E-02   0.999836        0.00000        0.00000       3.029375E-13

                v:   9         v:  10         v:  11         v:  12         v:  13

   eig(s)    1.00000        1.00000        1.00000        1.00000        1.00000    
 
   x:   1  -9.732362E-02   0.251276       0.371748       9.279066E-02   0.770312    
   x:   2   0.234143       0.379243      -0.750296       0.246569       0.347064    
   x:   3  -0.186384       0.714785      -7.063053E-02  -0.380063      -0.366919    
   x:   4  -0.365574       0.224877       0.164900       0.840097      -0.273476    
   x:   5   0.361063      -0.295337      -0.237041       0.251701      -0.211883    
   x:   6  -0.793581      -0.330191      -0.442896      -0.116112       0.132104    
   x:   7  -8.024881E-02   0.106301       4.901563E-02  -4.260985E-02  -0.103754    
   x:   8   2.726742E-02   0.154912      -0.109228       4.160987E-02  -6.031663E-02
   x:   9   5.416490E-06   2.154303E-05   4.052923E-05  -3.447260E-05   1.736254E-05
   x:  10   2.196361E-06  -4.416589E-06  -2.166959E-05  -1.108378E-04  -3.371582E-05
   x:  11   1.996563E-05  -1.215884E-05   1.482728E-05   1.110351E-05  -2.924872E-06
   x:  12  -8.906057E-06  -1.531299E-05   5.231902E-05  -2.382494E-05   4.463838E-06
   x:  13   3.111030E-07  -1.950992E-06  -6.465935E-06  -1.193950E-05   2.512358E-04
 bummer (warning):overlap matrix: # small eigenvalues=                      2

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -6.765538E-06  -0.821136       0.197280      -0.196499      -0.429654       5.791030E-02  -7.177883E-02  -8.218686E-02
 ref    2   1.133577E-05   6.217271E-02   0.540439       0.282776      -0.100308      -0.744183      -4.191537E-02   3.870465E-02

              v      9       v     10       v     11       v     12       v     13
 ref    1  -2.138041E-02   2.801056E-02  -2.068266E-02   3.184319E-02   9.206221E-02
 ref    2   7.249782E-02  -5.242238E-02   2.222425E-02   1.576861E-02  -4.343782E-04

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   1.742721E-10   0.678131       0.330994       0.118574       0.194664       0.557162       6.909099E-03   8.252731E-03

              v      9       v     10       v     11       v     12       v     13
 ref    1   5.713056E-03   3.532697E-03   9.216900E-04   1.262638E-03   8.475639E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00000677    -0.82113647     0.19728016    -0.19649943    -0.42965387     0.05791030    -0.07177883    -0.08218686
 ref:   2     0.00001134     0.06217271     0.54043899     0.28277565    -0.10030832    -0.74418287    -0.04191537     0.03870465

                ci   9         ci  10         ci  11         ci  12         ci  13
 ref:   1    -0.02138041     0.02801056    -0.02068266     0.03184319     0.09206221
 ref:   2     0.07249782    -0.05242238     0.02222425     0.01576861    -0.00043438

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 83  1   -196.4879583938  3.4881E-07  0.0000E+00  5.6951E-04  1.0000E-04   
 mr-sdci # 83  2   -196.4725827988  2.1316E-14  0.0000E+00  3.6360E-05  1.0000E-04   
 mr-sdci # 83  3   -196.4725827935  6.3736E-12  4.6150E-09  1.1698E-04  1.0000E-04   
 mr-sdci # 83  4   -196.4725827871  1.0893E-11  0.0000E+00  1.6775E-04  1.0000E-04   
 mr-sdci # 83  5   -196.4644811588  1.3735E-11  0.0000E+00  7.0951E-05  1.0000E-04   
 mr-sdci # 83  6   -196.4644811555  4.6825E-12  0.0000E+00  1.0675E-04  1.0000E-04   
 mr-sdci # 83  7   -195.7906044470  8.9811E-04  0.0000E+00  2.0122E-01  1.0000E-04   
 mr-sdci # 83  8   -195.7736295146  1.8480E-04  0.0000E+00  2.6250E-01  1.0000E-04   
 mr-sdci # 83  9   -194.7253527988  4.2781E-02  0.0000E+00  2.0376E+00  1.0000E-04   
 mr-sdci # 83 10   -194.0983149975  4.5075E-02  0.0000E+00  3.1329E+00  1.0000E-04   
 mr-sdci # 83 11   -193.8591506447  5.2087E-02  0.0000E+00  3.0138E+00  1.0000E-04   
 mr-sdci # 83 12   -193.7422392882  3.9365E-01  0.0000E+00  3.8100E+00  1.0000E-04   
 mr-sdci # 83 13   -193.3002978912  1.8363E-01  0.0000E+00  3.6399E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.014000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  84

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59964321
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426799
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426797
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616636
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616634
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.89784648
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88417770
   ht   9    -0.00152451     0.00118593    -0.00092356     0.00122216     0.00133367     0.00116657    -0.00017233     0.00017364
   ht  10     0.00230914     0.00121062    -0.00265368     0.00451530     0.00068381    -0.00089663    -0.00031962     0.00004488
   ht  11     0.00003589     0.00047238     0.00084002    -0.00012847    -0.00054114     0.00101518     0.00011863     0.00011523
   ht  12    -0.00089540     0.00260371     0.00028144     0.00064729     0.00091227     0.00038901    -0.00011480     0.00047743
   ht  13    -0.00958891    -0.00447343     0.00448385     0.00406474     0.00273195    -0.00191363     0.00128484     0.00075124
   ht  14    -0.00135643     0.00082126    -0.00067067     0.00057253     0.00076741     0.00070251    -0.00010448     0.00008395

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14
   ht   9    -0.00000051
   ht  10    -0.00000010    -0.00000143
   ht  11     0.00000000     0.00000005    -0.00000023
   ht  12    -0.00000010    -0.00000005    -0.00000005    -0.00000071
   ht  13    -0.00000020     0.00000028    -0.00000003     0.00000003    -0.00001118
   ht  14    -0.00000009    -0.00000003     0.00000000    -0.00000006    -0.00000015    -0.00000020

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7         v:   8

   eig(s)   2.206883E-09   3.754881E-09   6.715556E-09   1.117546E-08   1.522259E-08   1.664202E-07    1.00000        1.00000    
 
   x:   1  -3.186395E-05   2.537957E-06   2.701214E-05  -1.027761E-05  -5.016076E-05  -1.885706E-04   2.043419E-02   5.504024E-02
   x:   2   2.376848E-05   5.221367E-06  -2.793690E-05   4.744476E-05  -2.093386E-05  -8.877804E-05  -9.185228E-02   8.797994E-02
   x:   3  -1.584186E-05   1.712611E-05   1.641118E-05   8.131799E-06   5.242778E-05   8.829971E-05  -0.196984      -5.634884E-02
   x:   4   1.821816E-05  -7.506333E-06  -2.703195E-05   1.368646E-05  -8.627339E-05   8.097972E-05  -7.236264E-02  -3.425681E-02
   x:   5   2.056472E-05  -1.314501E-05  -2.654721E-05   1.323230E-05  -1.036323E-05   5.369396E-05  -5.563564E-02   0.138791    
   x:   6   1.726993E-05   2.034400E-05  -2.086029E-05   3.610490E-06   1.802315E-05  -3.804956E-05   2.108219E-02   6.483321E-03
   x:   7  -2.740022E-06   2.477492E-06   3.510599E-06  -1.476428E-06   6.447789E-06   2.610195E-05   0.344219       0.919223    
   x:   8   3.033238E-06   1.370738E-06  -4.499459E-06   9.168852E-06   1.172309E-08   1.514526E-05   0.908339      -0.347283    
   x:   9   0.141405       3.775097E-03  -0.978425      -0.147717       2.897255E-02  -3.882012E-03  -2.155944E-11   5.020622E-12
   x:  10   2.540131E-02  -3.522849E-02  -3.434693E-02   5.511696E-02  -0.996888       1.047919E-02   2.888018E-12   1.395518E-12
   x:  11   1.641465E-02   0.996167      -6.735761E-03   7.964206E-02  -3.004883E-02   9.560805E-03   5.677368E-13   2.181508E-12
   x:  12   7.985762E-02  -7.874438E-02  -0.135143       0.982340       6.367552E-02  -1.054657E-02  -2.420977E-12  -7.672075E-12
   x:  13   4.869524E-03  -1.001125E-02  -4.197817E-03   8.207431E-03   1.158636E-02   0.999828      -1.844683E-14   1.145451E-13
   x:  14   0.986250      -9.788197E-03   0.152243      -6.114760E-02   1.680837E-02  -3.955038E-03   3.157771E-11    0.00000    

                v:   9         v:  10         v:  11         v:  12         v:  13         v:  14

   eig(s)    1.00000        1.00000        1.00000        1.00000        1.00000        1.00000    
 
   x:   1  -0.402775      -0.100219      -0.239369      -0.404721       6.431886E-02   0.773982    
   x:   2   0.212873       0.229652      -0.443753       0.699730       0.287478       0.341447    
   x:   3  -0.308632      -0.195291      -0.731812       1.499148E-02  -0.397095      -0.362178    
   x:   4   5.359345E-02  -0.368799      -0.215927      -0.240041       0.821131      -0.276053    
   x:   5  -0.794807       0.368257       0.207953       0.223766       0.265941      -0.215107    
   x:   6  -0.217514      -0.787440       0.285171       0.477503      -7.962180E-02   0.128329    
   x:   7   6.976903E-02  -8.204537E-02  -9.497775E-02  -5.489318E-02  -4.831407E-02  -0.102836    
   x:   8  -0.102147       2.566911E-02  -0.173281       8.741534E-02   4.336946E-02  -6.060200E-02
   x:   9   2.514790E-06   5.017682E-06  -1.675366E-05  -3.993166E-05  -3.738544E-05   1.792992E-05
   x:  10  -2.591648E-07   2.257849E-06   4.197389E-06   2.894595E-05  -1.093023E-04  -3.328722E-05
   x:  11  -5.051839E-07   2.011094E-05   1.304312E-05  -1.437089E-05   1.040551E-05  -2.920534E-06
   x:  12   8.936681E-08  -8.800916E-06   2.094797E-05  -4.885915E-05  -2.666401E-05   4.916427E-06
   x:  13  -1.075960E-08   3.289898E-07   2.293641E-06   9.227955E-06  -9.949603E-06   2.512333E-04
   x:  14  -3.553588E-06   3.049100E-07  -1.349664E-05  -2.958339E-05  -2.060500E-05   1.489437E-05
 bummer (warning):overlap matrix: # small eigenvalues=                      3

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   7.771274E-06  -0.843346      -5.850818E-02   0.192720       0.429543       5.872360E-02   6.912917E-02   8.444089E-02
 ref    2  -8.863318E-06  -2.957113E-02  -0.537908      -0.292711       0.101717      -0.743991       4.504232E-02  -3.894506E-02

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1   8.438293E-03   1.977754E-02  -1.248302E-02  -2.984936E-02   4.760879E-02  -0.115885    
 ref    2  -3.902717E-02  -6.983451E-02  -5.004105E-03  -2.177205E-02   2.598969E-02   2.986006E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   1.389511E-10   0.712106       0.292769       0.122821       0.194854       0.556972       6.807653E-03   8.646982E-03

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1   1.594325E-03   5.268009E-03   1.808669E-04   1.365006E-03   2.942061E-03   1.432105E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00000777    -0.84334564    -0.05850818     0.19272038     0.42954347     0.05872360     0.06912917     0.08444089
 ref:   2    -0.00000886    -0.02957113    -0.53790848    -0.29271064     0.10171677    -0.74399139     0.04504232    -0.03894506

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14
 ref:   1     0.00843829     0.01977754    -0.01248302    -0.02984936     0.04760879    -0.11588541
 ref:   2    -0.03902717    -0.06983451    -0.00500410    -0.02177205     0.02598969     0.02986006

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 84  1   -196.4879584013  7.4597E-09  0.0000E+00  5.4863E-04  1.0000E-04   
 mr-sdci # 84  2   -196.4725827988  1.0836E-11  0.0000E+00  3.7327E-05  1.0000E-04   
 mr-sdci # 84  3   -196.4725827985  4.9457E-09  0.0000E+00  6.3695E-05  1.0000E-04   
 mr-sdci # 84  4   -196.4725827871  4.9880E-12  9.4707E-09  1.6784E-04  1.0000E-04   
 mr-sdci # 84  5   -196.4644811588  7.6028E-13  0.0000E+00  7.0866E-05  1.0000E-04   
 mr-sdci # 84  6   -196.4644811555  5.4996E-11  0.0000E+00  1.0398E-04  1.0000E-04   
 mr-sdci # 84  7   -195.7912815010  6.7705E-04  0.0000E+00  2.0304E-01  1.0000E-04   
 mr-sdci # 84  8   -195.7740315803  4.0207E-04  0.0000E+00  2.6791E-01  1.0000E-04   
 mr-sdci # 84  9   -195.5551158829  8.2976E-01  0.0000E+00  1.1114E+00  1.0000E-04   
 mr-sdci # 84 10   -194.7242004370  6.2589E-01  0.0000E+00  2.0141E+00  1.0000E-04   
 mr-sdci # 84 11   -193.9041608390  4.5010E-02  0.0000E+00  3.0732E+00  1.0000E-04   
 mr-sdci # 84 12   -193.7448305051  2.5912E-03  0.0000E+00  3.8735E+00  1.0000E-04   
 mr-sdci # 84 13   -193.4902673946  1.8997E-01  0.0000E+00  2.9881E+00  1.0000E-04   
 mr-sdci # 84 14   -191.1068660837 -6.8319E-01  0.0000E+00  4.5067E+00  1.0000E-04   
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.009000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  85

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59964321
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426799
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426797
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616636
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616634
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.89784648
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88417770
   ht   9    -0.00152451     0.00118593    -0.00092356     0.00122216     0.00133367     0.00116657    -0.00017233     0.00017364
   ht  10     0.00230914     0.00121062    -0.00265368     0.00451530     0.00068381    -0.00089663    -0.00031962     0.00004488
   ht  11     0.00003589     0.00047238     0.00084002    -0.00012847    -0.00054114     0.00101518     0.00011863     0.00011523
   ht  12    -0.00089540     0.00260371     0.00028144     0.00064729     0.00091227     0.00038901    -0.00011480     0.00047743
   ht  13    -0.00958891    -0.00447343     0.00448385     0.00406474     0.00273195    -0.00191363     0.00128484     0.00075124
   ht  14    -0.00135643     0.00082126    -0.00067067     0.00057253     0.00076741     0.00070251    -0.00010448     0.00008395
   ht  15    -0.00166757    -0.00075421     0.00078426    -0.00187266    -0.00008381     0.00027172     0.00002321    -0.00020872

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15
   ht   9    -0.00000051
   ht  10    -0.00000010    -0.00000143
   ht  11     0.00000000     0.00000005    -0.00000023
   ht  12    -0.00000010    -0.00000005    -0.00000005    -0.00000071
   ht  13    -0.00000020     0.00000028    -0.00000003     0.00000003    -0.00001118
   ht  14    -0.00000009    -0.00000003     0.00000000    -0.00000006    -0.00000015    -0.00000020
   ht  15     0.00000001     0.00000020    -0.00000001     0.00000003    -0.00000007    -0.00000002    -0.00000038

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7         v:   8

   eig(s)   2.162498E-09   3.744501E-09   4.362571E-09   6.738968E-09   1.117620E-08   1.553527E-08   1.665278E-07    1.00000    
 
   x:   1   2.547591E-05  -3.764988E-06  -4.660923E-05   3.180409E-05   1.091475E-05   4.279461E-05   1.876713E-04  -4.567458E-02
   x:   2  -2.657634E-05   2.861469E-06  -2.057660E-05  -2.575888E-05  -4.719514E-05   1.808576E-05   8.837348E-05  -0.112625    
   x:   3   1.944868E-05   2.002026E-05   2.277603E-05   1.366709E-05  -8.534629E-06  -4.882365E-05  -8.788866E-05  -9.198154E-03
   x:   4  -2.492401E-05  -1.339517E-05  -4.701648E-05  -2.182349E-05  -1.296237E-05   7.948684E-05  -8.187792E-05   9.469801E-03
   x:   5  -2.090968E-05  -1.303868E-05  -1.305314E-07  -2.632417E-05  -1.320494E-05   1.044279E-05  -5.371320E-05  -0.149255    
   x:   6  -1.613013E-05   2.131093E-05   4.854265E-06  -2.161567E-05  -3.751606E-06  -1.689762E-05   3.817120E-05   5.627972E-04
   x:   7   3.046159E-06   2.698277E-06   1.819216E-06   3.267597E-06   1.438082E-06  -6.189933E-06  -2.608325E-05  -0.762075    
   x:   8  -3.567392E-06   9.052666E-07  -3.927291E-06  -4.114181E-06  -9.133917E-06  -5.812904E-07  -1.524726E-05   0.618077    
   x:   9  -0.146471      -1.837188E-03  -7.088224E-02  -0.975356       0.146997      -2.398794E-02   3.914686E-03  -7.441334E-11
   x:  10  -4.544969E-02  -5.477280E-02  -0.158560      -1.339684E-02  -5.058255E-02   0.983337      -1.015865E-02  -2.368808E-11
   x:  11  -9.748906E-03   0.987376      -0.133250      -3.426160E-03  -7.940101E-02   2.883129E-02  -9.559074E-03  -4.821492E-12
   x:  12  -8.052322E-02  -7.738460E-02   7.976459E-03  -0.134948      -0.982679      -5.902426E-02   1.054528E-02  -9.334738E-12
   x:  13  -1.137808E-03  -6.403029E-03   2.814857E-02  -6.995084E-03  -8.534968E-03  -6.730322E-03  -0.999499       1.373750E-12
   x:  14  -0.974774       1.743416E-02   0.157334       0.144431       6.080378E-02  -1.357653E-02   3.990323E-03   1.262349E-10
   x:  15   0.140413       0.125510       0.962533      -9.679335E-02  -1.004542E-02   0.167117       2.578149E-02  -6.703897E-11

                v:   9         v:  10         v:  11         v:  12         v:  13         v:  14         v:  15

   eig(s)    1.00000        1.00000        1.00000        1.00000        1.00000        1.00000        1.00000    
 
   x:   1  -0.106777      -0.372401      -8.530589E-02  -0.243102       0.407646      -8.487245E-02  -0.779792    
   x:   2   1.275175E-02   0.227444       0.272749      -0.422690      -0.708209      -0.248545      -0.345005    
   x:   3  -0.308088      -0.188517      -0.138992      -0.766352       3.548474E-02   0.356630       0.366605    
   x:   4  -6.899070E-02   0.110579      -0.332702      -0.204502       0.201634      -0.851514       0.254319    
   x:   5  -0.266023      -0.767790       0.325582       0.210997      -0.242538      -0.252084       0.211089    
   x:   6  -5.685771E-02  -0.197378      -0.821309       0.188575      -0.470099       0.106172      -0.124233    
   x:   7   0.603691      -0.133195      -8.585308E-02  -0.117892       6.517460E-02   3.312825E-02   0.102192    
   x:   8   0.671054      -0.340030       1.994068E-02  -0.197836      -7.926807E-02  -5.333197E-02   5.815615E-02
   x:   9   6.733999E-07   2.829810E-06   6.525098E-06  -1.611519E-05   4.240052E-05   3.485175E-05  -1.739802E-05
   x:  10   5.232966E-07  -1.910812E-06   2.336898E-07  -8.505608E-07  -2.235494E-05   1.100906E-04   3.593464E-05
   x:  11   2.243434E-07  -1.601451E-06   1.910009E-05   1.553369E-05   1.311982E-05  -1.048574E-05   2.711113E-06
   x:  12   1.813135E-07  -3.286507E-07  -1.045368E-05   2.071684E-05   4.978418E-05   2.451813E-05  -4.519639E-06
   x:  13  -2.108026E-08   3.351047E-08   2.855842E-07   2.745648E-06  -9.910021E-06   1.633044E-05  -2.508689E-04
   x:  14  -1.403853E-06  -2.745605E-06   1.462405E-06  -1.323737E-05   3.109334E-05   1.863510E-05  -1.468085E-05
   x:  15   1.265403E-06  -3.430441E-06  -3.830334E-06  -1.143471E-05   1.155157E-05  -4.478184E-05  -2.590124E-05
 bummer (warning):overlap matrix: # small eigenvalues=                      4

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   7.884060E-06  -0.146542       0.836424       0.175260      -0.429346       6.014766E-02   6.292328E-02   8.660273E-02
 ref    2  -8.966888E-06   0.588414       7.012509E-02   0.157324      -0.104183      -0.743650       4.673015E-02  -3.788728E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1   3.226668E-02   7.155479E-03   5.563248E-03   1.269036E-02  -2.202591E-02   6.043466E-02   0.121087    
 ref    2  -2.808837E-03  -3.875351E-02  -7.395873E-02   5.168050E-03  -1.065114E-02  -3.677108E-02   7.006588E-04

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   1.425635E-10   0.367705       0.704523       5.546706E-02   0.195193       0.556633       6.143046E-03   8.935478E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1   1.049028E-03   1.553035E-03   5.500844E-03   1.877539E-04   5.985877E-04   5.004460E-03   1.466255E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00000788    -0.14654179     0.83642411     0.17526019    -0.42934646     0.06014766     0.06292328     0.08660273
 ref:   2    -0.00000897     0.58841360     0.07012509     0.15732428    -0.10418324    -0.74364999     0.04673015    -0.03788728

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15
 ref:   1     0.03226668     0.00715548     0.00556325     0.01269036    -0.02202591     0.06043466     0.12108697
 ref:   2    -0.00280884    -0.03875351    -0.07395873     0.00516805    -0.01065114    -0.03677108     0.00070066

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 85  1   -196.4879584017  4.1337E-10  0.0000E+00  5.3976E-04  1.0000E-04   
 mr-sdci # 85  2   -196.4725827989  6.9171E-11  0.0000E+00  5.4385E-05  1.0000E-04   
 mr-sdci # 85  3   -196.4725827988  3.5158E-10  0.0000E+00  3.8189E-05  1.0000E-04   
 mr-sdci # 85  4   -196.4725827980  1.0899E-08  0.0000E+00  7.3017E-05  1.0000E-04   
 mr-sdci # 85  5   -196.4644811588  8.4718E-11  0.0000E+00  6.9312E-05  1.0000E-04   
 mr-sdci # 85  6   -196.4644811555  1.3500E-12  4.1776E-09  1.0378E-04  1.0000E-04   
 mr-sdci # 85  7   -195.7955140260  4.2325E-03  0.0000E+00  1.9898E-01  1.0000E-04   
 mr-sdci # 85  8   -195.7740862780  5.4698E-05  0.0000E+00  2.6437E-01  1.0000E-04   
 mr-sdci # 85  9   -195.6432298002  8.8114E-02  0.0000E+00  7.0324E-01  1.0000E-04   
 mr-sdci # 85 10   -195.5549629570  8.3076E-01  0.0000E+00  1.1149E+00  1.0000E-04   
 mr-sdci # 85 11   -194.6435075612  7.3935E-01  0.0000E+00  2.2272E+00  1.0000E-04   
 mr-sdci # 85 12   -193.9041559778  1.5933E-01  0.0000E+00  3.0805E+00  1.0000E-04   
 mr-sdci # 85 13   -193.5698358174  7.9568E-02  0.0000E+00  2.9605E+00  1.0000E-04   
 mr-sdci # 85 14   -191.3681832544  2.6132E-01  0.0000E+00  4.6272E+00  1.0000E-04   
 mr-sdci # 85 15   -190.2676217097 -4.6567E-01  0.0000E+00  4.2981E+00  1.0000E-04   
 
 root number  6 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.016000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  86

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59964321
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426799
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426797
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616636
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616634
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.89784648
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88417770
   ht   9    -0.00152451     0.00118593    -0.00092356     0.00122216     0.00133367     0.00116657    -0.00017233     0.00017364
   ht  10     0.00230914     0.00121062    -0.00265368     0.00451530     0.00068381    -0.00089663    -0.00031962     0.00004488
   ht  11     0.00003589     0.00047238     0.00084002    -0.00012847    -0.00054114     0.00101518     0.00011863     0.00011523
   ht  12    -0.00089540     0.00260371     0.00028144     0.00064729     0.00091227     0.00038901    -0.00011480     0.00047743
   ht  13    -0.00958891    -0.00447343     0.00448385     0.00406474     0.00273195    -0.00191363     0.00128484     0.00075124
   ht  14    -0.00135643     0.00082126    -0.00067067     0.00057253     0.00076741     0.00070251    -0.00010448     0.00008395
   ht  15    -0.00166757    -0.00075421     0.00078426    -0.00187266    -0.00008381     0.00027172     0.00002321    -0.00020872
   ht  16     0.00015924    -0.00169846    -0.00073774    -0.00017819    -0.00031999    -0.00003355     0.00007701    -0.00013946

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15         ht  16
   ht   9    -0.00000051
   ht  10    -0.00000010    -0.00000143
   ht  11     0.00000000     0.00000005    -0.00000023
   ht  12    -0.00000010    -0.00000005    -0.00000005    -0.00000071
   ht  13    -0.00000020     0.00000028    -0.00000003     0.00000003    -0.00001118
   ht  14    -0.00000009    -0.00000003     0.00000000    -0.00000006    -0.00000015    -0.00000020
   ht  15     0.00000001     0.00000020    -0.00000001     0.00000003    -0.00000007    -0.00000002    -0.00000038
   ht  16     0.00000003     0.00000002     0.00000001     0.00000007    -0.00000002     0.00000002    -0.00000001    -0.00000018

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7         v:   8

   eig(s)   1.941657E-09   2.403418E-09   3.764666E-09   4.365110E-09   6.760544E-09   1.120013E-08   1.553563E-08   1.665278E-07
 
   x:   1   2.178551E-05  -1.404946E-05   3.968573E-06  -4.673549E-05   3.141720E-05   1.083180E-05   4.279072E-05  -1.876717E-04
   x:   2  -4.554500E-05  -9.470466E-06   1.437796E-06  -1.929982E-05  -2.320261E-05  -4.555234E-05   1.822599E-05  -8.836855E-05
   x:   3   3.357054E-06  -2.490731E-05  -1.830019E-05   2.310746E-05   1.475955E-05  -7.665707E-06  -4.874847E-05   8.789081E-05
   x:   4  -2.164281E-05   1.339946E-05   1.424258E-05  -4.670933E-05  -2.161048E-05  -1.295049E-05   7.949000E-05   8.187843E-05
   x:   5  -2.016461E-05   9.341484E-06   1.372322E-05   2.895958E-07  -2.575698E-05  -1.297626E-05   1.046246E-05   5.371412E-05
   x:   6  -1.460379E-05   7.469323E-06  -2.112906E-05   4.715726E-06  -2.161340E-05  -3.821456E-06  -1.690293E-05  -3.817111E-05
   x:   7   3.293056E-06  -1.004901E-06  -2.875402E-06   1.731891E-06   3.149702E-06   1.374719E-06  -6.196372E-06   2.608302E-05
   x:   8  -5.134008E-06  -2.778536E-07  -4.731095E-07  -3.809476E-06  -3.870246E-06  -8.999544E-06  -5.730804E-07   1.524767E-05
   x:   9  -0.145846       5.465371E-02   1.098881E-02  -6.724084E-02  -0.974700       0.143048      -2.410240E-02  -3.914940E-03
   x:  10  -3.139379E-02   3.339353E-02   5.571959E-02  -0.158109      -1.380098E-02  -5.145035E-02   0.983288       1.015868E-02
   x:  11  -7.891280E-02  -9.279165E-02  -0.978426      -0.142750      -7.159542E-03  -8.062849E-02   2.870417E-02   9.558872E-03
   x:  12  -9.275665E-02   1.900441E-02   8.400699E-02   1.137837E-02  -0.127468      -0.981804      -5.959047E-02  -1.054576E-02
   x:  13  -3.364746E-04   1.538713E-03   6.081593E-03   2.823703E-02  -6.842547E-03  -8.529456E-03  -6.735703E-03   0.999499    
   x:  14  -0.694200       0.685424      -3.770315E-02   0.152682       0.140511       6.002180E-02  -1.360049E-02  -3.990452E-03
   x:  15   0.108942      -8.784267E-02  -0.134586       0.961594      -9.457061E-02  -9.953002E-03   0.167127      -2.578144E-02
   x:  16   0.684931       0.713723      -0.113260      -3.465480E-02  -6.881731E-02  -5.173521E-02  -5.196460E-03  -1.476365E-04

                v:   9         v:  10         v:  11         v:  12         v:  13         v:  14         v:  15         v:  16

   eig(s)    1.00000        1.00000        1.00000        1.00000        1.00000        1.00000        1.00000        1.00000    
 
   x:   1  -1.582789E-02   0.159388      -0.354748       0.109748      -0.281673       0.378129      -7.914460E-02  -0.779660    
   x:   2  -0.107082      -5.725348E-02   0.215527      -0.197163      -0.360993      -0.759607      -0.270100      -0.345917    
   x:   3   7.823590E-02   0.284602      -0.157118       0.236864      -0.756909      -6.893831E-02   0.346840       0.366235    
   x:   4   2.838140E-02   5.232324E-02   0.119351       0.353384      -0.176036       0.203861      -0.848634       0.254174    
   x:   5  -7.332223E-02   0.392233      -0.721815      -0.354093       0.184026      -0.224523      -0.256180       0.210887    
   x:   6   1.205821E-02   0.121936      -0.176238       0.792638       0.356295      -0.415711       0.102212      -0.124267    
   x:   7  -0.899679      -0.350065      -0.178125       0.112233      -9.282347E-02   5.898970E-02   3.405734E-02   0.102233    
   x:   8   0.407937      -0.772218      -0.452731       4.293923E-02  -0.130449      -8.365502E-02  -5.540328E-02   5.807106E-02
   x:   9  -1.222934E-07  -1.663855E-06   2.530592E-06  -4.983137E-06  -2.253922E-05   3.901966E-05   3.549102E-05  -1.736938E-05
   x:  10  -1.394723E-07  -4.196961E-07  -2.012280E-06   1.265780E-07   8.324599E-07  -2.477070E-05   1.095673E-04   3.594639E-05
   x:  11  -8.590419E-08   1.876065E-07  -1.567731E-06  -2.133318E-05   1.103030E-05   1.448617E-05  -1.008034E-05   2.723927E-06
   x:  12  -2.315496E-07   2.136212E-06   3.001221E-07   6.376271E-06   1.504710E-05   5.174505E-05   2.587283E-05  -4.461648E-06
   x:  13   4.030540E-09   4.127913E-08   4.357900E-08  -4.983186E-07   3.889730E-06  -9.658477E-06   1.622472E-05  -2.508701E-04
   x:  14   4.065969E-07   1.295974E-06  -2.658818E-06  -1.898398E-07  -1.745062E-05   2.869241E-05   1.909876E-05  -1.466201E-05
   x:  15  -3.349517E-07  -1.131790E-06  -3.673946E-06   5.367311E-06  -1.155546E-05   1.123623E-05  -4.465062E-05  -2.591062E-05
   x:  16  -2.302891E-07   2.854735E-06   8.074043E-07  -4.033851E-06  -2.171450E-05  -2.900380E-05  -8.511571E-06  -1.667964E-06
 bummer (warning):overlap matrix: # small eigenvalues=                      5

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -5.854719E-06  -0.128445      -0.839427       0.175099       8.916344E-02  -0.424271       6.190758E-02  -8.689902E-02
 ref    2   8.630660E-06   0.590857      -5.852718E-02   0.152845       0.734860       0.154436       4.668157E-02   3.774297E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1  -2.945075E-02  -8.266157E-03   2.976265E-02   9.519906E-03  -1.039417E-02  -2.582816E-02  -4.721918E-02   0.157140    
 ref    2   3.571537E-03   3.839015E-02   2.281784E-02  -6.953313E-02  -1.307547E-02   9.092198E-03   4.445684E-02  -1.865149E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   1.087660E-10   0.365610       0.708064       5.402107E-02   0.547970       0.203857       6.011717E-03   8.975972E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   8.801025E-04   1.542133E-03   1.406469E-03   4.925485E-03   2.790065E-04   7.497620E-04   4.206062E-03   2.504082E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15        ev   16

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00000585    -0.12844451    -0.83942743     0.17509868     0.08916344    -0.42427134     0.06190758    -0.08689902
 ref:   2     0.00000863     0.59085703    -0.05852718     0.15284477     0.73486019     0.15443583     0.04668157     0.03774297

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1    -0.02945075    -0.00826616     0.02976265     0.00951991    -0.01039417    -0.02582816    -0.04721918     0.15713989
 ref:   2     0.00357154     0.03839015     0.02281784    -0.06953313    -0.01307547     0.00909220     0.04445684    -0.01865149

 trial vector basis is being transformed.  new dimension:   8

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 86  1   -196.4879584042  2.5289E-09  9.8748E-08  5.5726E-04  1.0000E-04   
 mr-sdci # 86  2   -196.4725827989  8.6189E-12  0.0000E+00  5.4978E-05  1.0000E-04   
 mr-sdci # 86  3   -196.4725827988  2.4869E-13  0.0000E+00  3.7945E-05  1.0000E-04   
 mr-sdci # 86  4   -196.4725827980  4.7606E-12  0.0000E+00  7.3550E-05  1.0000E-04   
 mr-sdci # 86  5   -196.4644811596  7.7540E-10  0.0000E+00  6.4468E-05  1.0000E-04   
 mr-sdci # 86  6   -196.4644811588  3.2550E-09  0.0000E+00  6.9823E-05  1.0000E-04   
 mr-sdci # 86  7   -195.7958146374  3.0061E-04  0.0000E+00  2.0107E-01  1.0000E-04   
 mr-sdci # 86  8   -195.7740958650  9.5870E-06  0.0000E+00  2.6484E-01  1.0000E-04   
 mr-sdci # 86  9   -195.6449716930  1.7419E-03  0.0000E+00  6.9467E-01  1.0000E-04   
 mr-sdci # 86 10   -195.5551283062  1.6535E-04  0.0000E+00  1.1247E+00  1.0000E-04   
 mr-sdci # 86 11   -195.3161773086  6.7267E-01  0.0000E+00  1.3779E+00  1.0000E-04   
 mr-sdci # 86 12   -194.4779527571  5.7380E-01  0.0000E+00  2.5225E+00  1.0000E-04   
 mr-sdci # 86 13   -193.6226412200  5.2805E-02  0.0000E+00  3.4769E+00  1.0000E-04   
 mr-sdci # 86 14   -192.7265440556  1.3584E+00  0.0000E+00  2.7533E+00  1.0000E-04   
 mr-sdci # 86 15   -191.0274526679  7.5983E-01  0.0000E+00  4.6156E+00  1.0000E-04   
 mr-sdci # 86 16   -189.9980528837 -1.7028E-01  0.0000E+00  4.4211E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.013000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  87

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59964361
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426800
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426800
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616636
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616636
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.90749984
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88578107
   ht   9     0.00685384    -0.00597103     0.00215215    -0.00078659     0.00231057     0.00177039    -0.00032338    -0.00003632

                ht   9
   ht   9    -0.00000435

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -2.921933E-06   4.683355E-02   0.842296      -0.200348      -6.311954E-02   0.428920       6.193466E-02   8.653214E-02
 ref    2   9.185433E-06  -0.584343      -1.156670E-02  -0.185224      -0.742912      -0.109327       4.666652E-02  -3.779399E-02

              v      9
 ref    1   2.359383E-02
 ref    2   1.924696E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   9.290987E-11   0.343651       0.709597       7.444748E-02   0.555902       0.195925       6.013666E-03   8.916196E-03

              v      9
 ref    1   5.603733E-04

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00000292     0.04683355     0.84229630    -0.20034846    -0.06311954     0.42891985     0.06193466     0.08653214
 ref:   2     0.00000919    -0.58434341    -0.01156670    -0.18522412    -0.74291155    -0.10932661     0.04666652    -0.03779399

                ci   9
 ref:   1     0.02359383
 ref:   2     0.00192470

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 87  1   -196.4879584734  6.9180E-08  3.9149E-08  3.2409E-04  1.0000E-04   
 mr-sdci # 87  2   -196.4725827989  1.5156E-11  0.0000E+00  5.2350E-05  1.0000E-04   
 mr-sdci # 87  3   -196.4725827988  5.3433E-12  0.0000E+00  3.5882E-05  1.0000E-04   
 mr-sdci # 87  4   -196.4725827982  1.2298E-10  0.0000E+00  7.5759E-05  1.0000E-04   
 mr-sdci # 87  5   -196.4644811597  1.1392E-10  0.0000E+00  6.0489E-05  1.0000E-04   
 mr-sdci # 87  6   -196.4644811588  2.6347E-11  0.0000E+00  6.8000E-05  1.0000E-04   
 mr-sdci # 87  7   -195.7958147845  1.4713E-07  0.0000E+00  2.0089E-01  1.0000E-04   
 mr-sdci # 87  8   -195.7745554591  4.5959E-04  0.0000E+00  2.6613E-01  1.0000E-04   
 mr-sdci # 87  9   -193.7216931552 -1.9233E+00  0.0000E+00  3.6685E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.011000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  88

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59964361
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426800
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426800
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616636
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616636
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.90749984
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88578107
   ht   9     0.00685384    -0.00597103     0.00215215    -0.00078659     0.00231057     0.00177039    -0.00032338    -0.00003632
   ht  10    -0.00430006     0.00417769    -0.00077793    -0.00037041    -0.00090188    -0.00059895     0.00051140     0.00040107

                ht   9         ht  10
   ht   9    -0.00000435
   ht  10     0.00000086    -0.00000177

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   5.277765E-07  -4.763926E-02   0.843224      -0.196214      -6.148348E-02   0.429157      -6.047358E-02   8.718719E-02
 ref    2   2.185531E-06   0.584294      -1.014560E-02  -0.185462      -0.743323      -0.106493      -4.601864E-02  -3.468532E-02

              v      9       v     10
 ref    1  -2.241564E-02  -1.181851E-02
 ref    2   3.448480E-02  -3.874269E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   5.055092E-12   0.343669       0.711130       7.289603E-02   0.556310       0.195517       5.774769E-03   8.804677E-03

              v      9       v     10
 ref    1   1.691662E-03   1.640673E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00000053    -0.04763926     0.84322401    -0.19621356    -0.06148348     0.42915743    -0.06047358     0.08718719
 ref:   2     0.00000219     0.58429422    -0.01014560    -0.18546231    -0.74332317    -0.10649303    -0.04601864    -0.03468532

                ci   9         ci  10
 ref:   1    -0.02241564    -0.01181851
 ref:   2     0.03448480    -0.03874269

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 88  1   -196.4879585152  4.1840E-08  1.1474E-08  1.9885E-04  1.0000E-04   
 mr-sdci # 88  2   -196.4725827989  2.1316E-14  0.0000E+00  5.2339E-05  1.0000E-04   
 mr-sdci # 88  3   -196.4725827988  8.2423E-13  0.0000E+00  3.5711E-05  1.0000E-04   
 mr-sdci # 88  4   -196.4725827982  1.1333E-11  0.0000E+00  7.4644E-05  1.0000E-04   
 mr-sdci # 88  5   -196.4644811598  1.6406E-11  0.0000E+00  5.9354E-05  1.0000E-04   
 mr-sdci # 88  6   -196.4644811588  7.6739E-13  0.0000E+00  6.7884E-05  1.0000E-04   
 mr-sdci # 88  7   -195.7964070593  5.9227E-04  0.0000E+00  2.1151E-01  1.0000E-04   
 mr-sdci # 88  8   -195.7751714680  6.1601E-04  0.0000E+00  2.7163E-01  1.0000E-04   
 mr-sdci # 88  9   -195.5590604849  1.8374E+00  0.0000E+00  1.1688E+00  1.0000E-04   
 mr-sdci # 88 10   -191.2055793449 -4.3495E+00  0.0000E+00  4.3117E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.014000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  89

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59964361
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426800
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426800
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616636
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616636
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.90749984
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88578107
   ht   9     0.00685384    -0.00597103     0.00215215    -0.00078659     0.00231057     0.00177039    -0.00032338    -0.00003632
   ht  10    -0.00430006     0.00417769    -0.00077793    -0.00037041    -0.00090188    -0.00059895     0.00051140     0.00040107
   ht  11     0.00240141     0.00061583    -0.00088425     0.00100903    -0.00055895     0.00009984     0.00005511    -0.00006539

                ht   9         ht  10         ht  11
   ht   9    -0.00000435
   ht  10     0.00000086    -0.00000177
   ht  11    -0.00000032     0.00000011    -0.00000043

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7         v:   8

   eig(s)   5.545016E-09   2.035506E-08   5.200609E-08    1.00000        1.00000        1.00000        1.00000        1.00000    
 
   x:   1   3.935738E-05  -1.059957E-04  -1.226299E-04   6.804915E-02   4.622634E-02   0.161496       5.893269E-02   0.141934    
   x:   2   1.906804E-05   1.005951E-04   1.020777E-04   0.171433       0.201308       0.157690      -0.161524      -9.638748E-03
   x:   3  -2.003626E-05  -2.201238E-05  -3.829157E-05   0.845125      -3.758872E-02   3.848271E-02  -0.131593       1.648943E-02
   x:   4   2.094160E-05  -4.775529E-06   1.523071E-05   0.272288      -0.109571      -0.407538      -0.576678      -0.146610    
   x:   5  -1.380249E-05  -2.496544E-05  -4.139394E-05  -0.362195       0.380492      -8.489351E-02  -0.685617       0.193887    
   x:   6  -1.671456E-07  -1.736987E-05  -3.267584E-05  -0.118361      -2.179401E-02  -0.356357       7.249614E-02  -0.831849    
   x:   7   1.448462E-06   1.120928E-05   4.643250E-06   1.841482E-02  -3.867511E-02  -0.798377       0.275847       0.466220    
   x:   8  -1.330206E-06   8.005178E-06  -6.346933E-07  -0.179079      -0.892846       9.941234E-02  -0.262955       0.105209    
   x:   9  -6.334539E-02  -0.162183      -0.984725      -1.574744E-12  -9.664470E-14  -3.136838E-12   3.188971E-12  -6.662068E-12
   x:  10  -6.767946E-03   0.986754      -0.162082      -1.897122E-12   2.863045E-12  -5.526007E-12   6.380843E-12  -1.118057E-11
   x:  11   0.997969      -3.602564E-03  -6.360397E-02   2.384718E-13   2.544941E-12  -1.140845E-12   2.069889E-12  -2.139311E-12

                v:   9         v:  10         v:  11

   eig(s)    1.00000        1.00000        1.00000    
 
   x:   1  -0.273571      -0.599292       0.713821    
   x:   2  -0.559702      -0.424660      -0.620834    
   x:   3  -0.231424       0.421061       0.185558    
   x:   4   0.458503      -0.427102      -3.275344E-02
   x:   5  -0.266368       0.309960       0.205290    
   x:   6  -0.365746       5.313688E-02   0.157174    
   x:   7  -0.254434      -1.854454E-02  -4.718186E-02
   x:   8  -0.280449       3.951059E-02  -2.112021E-02
   x:   9   1.112176E-05  -9.624009E-06  -1.935769E-04
   x:  10   1.856051E-05  -6.478042E-06   1.207268E-04
   x:  11   4.297458E-06   5.288517E-05  -2.043887E-05
 bummer (warning):overlap matrix: # small eigenvalues=                      1

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -5.727266E-07   5.032954E-02   0.852866       0.147930      -5.602469E-02   0.429904      -5.277130E-02  -8.819481E-02
 ref    2  -2.592448E-08  -0.583979       2.075634E-03   0.186718      -0.744616      -9.703803E-02  -3.911862E-02   1.407643E-02

              v      9       v     10       v     11
 ref    1  -3.198513E-02  -2.215690E-02  -8.459011E-03
 ref    2   5.526494E-02  -1.742823E-02  -3.595130E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   3.286879E-13   0.343564       0.727384       5.674673E-02   0.557592       0.194234       4.315076E-03   7.976471E-03

              v      9       v     10       v     11
 ref    1   4.077262E-03   7.946717E-04   1.364051E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00000057     0.05032954     0.85286557     0.14792984    -0.05602469     0.42990412    -0.05277130    -0.08819481
 ref:   2    -0.00000003    -0.58397882     0.00207563     0.18671769    -0.74461638    -0.09703803    -0.03911862     0.01407643

                ci   9         ci  10         ci  11
 ref:   1    -0.03198513    -0.02215690    -0.00845901
 ref:   2     0.05526494    -0.01742823    -0.03595130

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 89  1   -196.4879585226  7.3806E-09  0.0000E+00  8.0255E-05  1.0000E-04   
 mr-sdci # 89  2   -196.4725827989  0.0000E+00  7.2893E-10  5.2348E-05  1.0000E-04   
 mr-sdci # 89  3   -196.4725827988  6.2030E-12  0.0000E+00  3.5198E-05  1.0000E-04   
 mr-sdci # 89  4   -196.4725827984  1.6893E-10  0.0000E+00  6.7242E-05  1.0000E-04   
 mr-sdci # 89  5   -196.4644811598  1.4353E-11  0.0000E+00  5.8208E-05  1.0000E-04   
 mr-sdci # 89  6   -196.4644811588  1.0346E-11  0.0000E+00  6.7418E-05  1.0000E-04   
 mr-sdci # 89  7   -195.7994190884  3.0120E-03  0.0000E+00  2.1461E-01  1.0000E-04   
 mr-sdci # 89  8   -195.7782156336  3.0442E-03  0.0000E+00  2.7178E-01  1.0000E-04   
 mr-sdci # 89  9   -195.7476148076  1.8855E-01  0.0000E+00  4.5695E-01  1.0000E-04   
 mr-sdci # 89 10   -192.4588416099  1.2533E+00  0.0000E+00  3.4606E+00  1.0000E-04   
 mr-sdci # 89 11   -191.1695675725 -4.1466E+00  0.0000E+00  4.3257E+00  1.0000E-04   
 
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mr-sdci  convergence criteria satisfied after 89 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 89  1   -196.4879585226  7.3806E-09  0.0000E+00  8.0255E-05  1.0000E-04   
 mr-sdci # 89  2   -196.4725827989  0.0000E+00  7.2893E-10  5.2348E-05  1.0000E-04   
 mr-sdci # 89  3   -196.4725827988  6.2030E-12  0.0000E+00  3.5198E-05  1.0000E-04   
 mr-sdci # 89  4   -196.4725827984  1.6893E-10  0.0000E+00  6.7242E-05  1.0000E-04   
 mr-sdci # 89  5   -196.4644811598  1.4353E-11  0.0000E+00  5.8208E-05  1.0000E-04   
 mr-sdci # 89  6   -196.4644811588  1.0346E-11  0.0000E+00  6.7418E-05  1.0000E-04   
 mr-sdci # 89  7   -195.7994190884  3.0120E-03  0.0000E+00  2.1461E-01  1.0000E-04   
 mr-sdci # 89  8   -195.7782156336  3.0442E-03  0.0000E+00  2.7178E-01  1.0000E-04   
 mr-sdci # 89  9   -195.7476148076  1.8855E-01  0.0000E+00  4.5695E-01  1.0000E-04   
 mr-sdci # 89 10   -192.4588416099  1.2533E+00  0.0000E+00  3.4606E+00  1.0000E-04   
 mr-sdci # 89 11   -191.1695675725 -4.1466E+00  0.0000E+00  4.3257E+00  1.0000E-04   

####################CIUDGINFO####################

   ci vector at position   1 energy= -196.487958522628
   ci vector at position   2 energy= -196.472582798924
   ci vector at position   3 energy= -196.472582798844
   ci vector at position   4 energy= -196.472582798353
   ci vector at position   5 energy= -196.464481159765
   ci vector at position   6 energy= -196.464481158802

################END OF CIUDGINFO################

 
    6 of the  12 expansion vectors are transformed.
    6 of the  11 matrix-vector products are transformed.

    6 expansion eigenvectors written to unit nvfile (= 11)
    6 matrix-vector products written to unit nhvfil (= 10)
maximum overlap with reference    1(overlap= 0.00000)
weight of reference states=  0.0000

 information on vector: 1 from unit 11 written to unit 48 filename civout              
maximum overlap with reference    2(overlap= 0.58398)
weight of reference states=  0.3436

 information on vector: 2 from unit 11 written to unit 48 filename civout              
maximum overlap with reference    1(overlap= 0.85287)
weight of reference states=  0.7274

 information on vector: 3 from unit 11 written to unit 48 filename civout              
maximum overlap with reference    2(overlap= 0.18672)
weight of reference states=  0.0567

 information on vector: 4 from unit 11 written to unit 48 filename civout              
maximum overlap with reference    2(overlap= 0.74462)
weight of reference states=  0.5576

 information on vector: 5 from unit 11 written to unit 48 filename civout              
maximum overlap with reference    1(overlap= 0.42990)
weight of reference states=  0.1942

 information on vector: 6 from unit 11 written to unit 48 filename civout              


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =      -196.4879585226

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     2    3    4   11   14   17

                                         symmetry   ag   ag   ag   b1g  b2g  b3g

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z   3  3       4  0.657613                        +-   +-   +    +-   +-   +-   +  
 z   3  3       5  0.355186                        +-   +    +-   +-   +-   +-   +  
 z   3  3       6  0.594431                        +    +-   +-   +-   +-   +-   +  
 y   3  3     125  0.024164              1( b3g)   +-   +-    -   +-   +-   +    +  
 y   3  3     137 -0.027197              1( b2g)   +-   +-    -   +-   +    +-   +  
 y   3  3     149  0.024599              1( b1g)   +-   +-    -   +    +-   +-   +  
 y   3  3     161 -0.059759              1( b3g)   +-   +-   +    +-   +-    -   +  
 y   3  3     162  0.010107              2( b3g)   +-   +-   +    +-   +-    -   +  
 y   3  3     173  0.054503              1( b2g)   +-   +-   +    +-    -   +-   +  
 y   3  3     185 -0.059005              1( b1g)   +-   +-   +     -   +-   +-   +  
 y   3  3     193  0.019418              1( ag )   +-   +-        +-   +-   +-   +  
 y   3  3     194 -0.036756              2( ag )   +-   +-        +-   +-   +-   +  
 y   3  3     195 -0.016042              3( ag )   +-   +-        +-   +-   +-   +  
 y   3  3     209  0.016015              1( b3g)   +-    -   +-   +-   +-   +    +  
 y   3  3     221 -0.013058              1( b2g)   +-    -   +-   +-   +    +-   +  
 y   3  3     233  0.011953              1( b1g)   +-    -   +-   +    +-   +-   +  
 y   3  3     241  0.010010              1( ag )   +-    -   +    +-   +-   +-   +  
 y   3  3     243  0.060667              3( ag )   +-    -   +    +-   +-   +-   +  
 y   3  3     257 -0.027145              1( b3g)   +-   +    +-   +-   +-    -   +  
 y   3  3     269  0.032264              1( b2g)   +-   +    +-   +-    -   +-   +  
 y   3  3     281 -0.034181              1( b1g)   +-   +    +-    -   +-   +-   +  
 y   3  3     289  0.017338              1( ag )   +-   +     -   +-   +-   +-   +  
 y   3  3     290 -0.019568              2( ag )   +-   +     -   +-   +-   +-   +  
 y   3  3     303  0.028776              3( ag )   +-        +-   +-   +-   +-   +  
 y   3  3     317  0.022769              1( b3g)    -   +-   +-   +-   +-   +    +  
 y   3  3     329 -0.021179              1( b2g)    -   +-   +-   +-   +    +-   +  
 y   3  3     341  0.024715              1( b1g)    -   +-   +-   +    +-   +-   +  
 y   3  3     349  0.016753              1( ag )    -   +-   +    +-   +-   +-   +  
 y   3  3     350  0.063254              2( ag )    -   +-   +    +-   +-   +-   +  
 y   3  3     352  0.011173              4( ag )    -   +-   +    +-   +-   +-   +  
 y   3  3     362  0.024558              2( ag )    -   +    +-   +-   +-   +-   +  
 y   3  3     363 -0.027863              3( ag )    -   +    +-   +-   +-   +-   +  
 y   3  3     377 -0.052411              1( b3g)   +    +-   +-   +-   +-    -   +  
 y   3  3     389  0.055165              1( b2g)   +    +-   +-   +-    -   +-   +  
 y   3  3     401 -0.049042              1( b1g)   +    +-   +-    -   +-   +-   +  
 y   3  3     409  0.029010              1( ag )   +    +-    -   +-   +-   +-   +  
 y   3  3     410 -0.027297              2( ag )   +    +-    -   +-   +-   +-   +  
 y   3  3     411 -0.015576              3( ag )   +    +-    -   +-   +-   +-   +  
 y   3  3     421  0.015668              1( ag )   +     -   +-   +-   +-   +-   +  
 y   3  3     423  0.048981              3( ag )   +     -   +-   +-   +-   +-   +  
 y   3  3     433  0.014615              1( ag )        +-   +-   +-   +-   +-   +  
 y   3  3     434  0.035623              2( ag )        +-   +-   +-   +-   +-   +  
 y   3  3     435 -0.012734              3( ag )        +-   +-   +-   +-   +-   +  
 x   3  3    3939 -0.012052    1( b2g)   1( b3g)   +-   +-   +    +-    -    -   +  
 x   3  3    4125  0.012196    1( b1g)   1( b3g)   +-   +-   +     -   +-    -   +  
 x   3  3    4299 -0.012027    1( b1g)   1( b2g)   +-   +-   +     -    -   +-   +  
 x   3  3    6905 -0.010407    3( ag )   1( b2g)   +-    -    -   +-   +    +-   +  
 x   3  3    7281 -0.011551    3( ag )   1( b3g)   +-    -   +    +-   +-    -   +  
 x   3  3    7463  0.015930    3( ag )   1( b2g)   +-    -   +    +-    -   +-   +  
 x   3  3    7471  0.010264    5( ag )   2( b2g)   +-    -   +    +-    -   +-   +  
 x   3  3    7500  0.011579    4( b1u)   4( b3u)   +-    -   +    +-    -   +-   +  
 x   3  3    7649 -0.014378    3( ag )   1( b1g)   +-    -   +     -   +-   +-   +  
 x   3  3   11558  0.013223    2( ag )   1( b3g)    -   +-    -   +-   +-   +    +  
 x   3  3   11926  0.011440    2( ag )   1( b1g)    -   +-    -   +    +-   +-   +  
 x   3  3   12116 -0.016119    2( ag )   1( b3g)    -   +-   +    +-   +-    -   +  
 x   3  3   12124  0.010127    4( ag )   2( b3g)    -   +-   +    +-   +-    -   +  
 x   3  3   12151 -0.010821    5( b1u)   5( b2u)    -   +-   +    +-   +-    -   +  
 x   3  3   12298  0.011729    2( ag )   1( b2g)    -   +-   +    +-    -   +-   +  
 x   3  3   12484 -0.013140    2( ag )   1( b1g)    -   +-   +     -   +-   +-   +  
 x   3  3   13413  0.014710    2( ag )   3( ag )    -    -   +    +-   +-   +-   +  
 x   3  3   14541 -0.011032    1( b2g)   1( b3g)   +    +-   +-   +-    -    -   +  
 x   3  3   14727  0.010835    1( b1g)   1( b3g)   +    +-   +-    -   +-    -   +  
 x   3  3   14901 -0.010924    1( b1g)   1( b2g)   +    +-   +-    -    -   +-   +  
 x   3  3   15833  0.013110    3( ag )   1( b2g)   +     -   +-   +-    -   +-   +  
 x   3  3   16019 -0.011271    3( ag )   1( b1g)   +     -   +-    -   +-   +-   +  
 w   3  3   19056  0.016000    1( b3g)   1( b3g)   +-   +-   +    +-   +-        +  
 w   3  3   19057 -0.010283    1( b3g)   2( b3g)   +-   +-   +    +-   +-        +  
 w   3  3   19499 -0.012222    1( b2g)   1( b3g)   +-   +-   +    +-   +     -   +  
 w   3  3   19695  0.015871    1( b2g)   1( b2g)   +-   +-   +    +-        +-   +  
 w   3  3   19696 -0.010303    1( b2g)   2( b2g)   +-   +-   +    +-        +-   +  
 w   3  3   19697  0.010024    2( b2g)   2( b2g)   +-   +-   +    +-        +-   +  
 w   3  3   20355  0.012225    1( b1g)   1( b3g)   +-   +-   +    +    +-    -   +  
 w   3  3   20557 -0.012118    1( b1g)   1( b2g)   +-   +-   +    +     -   +-   +  
 w   3  3   20762  0.015982    1( b1g)   1( b1g)   +-   +-   +         +-   +-   +  
 w   3  3   20763 -0.010286    1( b1g)   2( b1g)   +-   +-   +         +-   +-   +  
 w   3  3   24201  0.012922    3( ag )   1( b3g)   +-   +    +    +-   +-    -   +  
 w   3  3   24411 -0.014082    3( ag )   1( b2g)   +-   +    +    +-    -   +-   +  
 w   3  3   24625  0.013954    3( ag )   1( b1g)   +-   +    +     -   +-   +-   +  
 w   3  3   25668 -0.016489    3( ag )   3( ag )   +-        +    +-   +-   +-   +  
 w   3  3   25675  0.010329    3( ag )   5( ag )   +-        +    +-   +-   +-   +  
 w   3  3   25677 -0.010110    5( ag )   5( ag )   +-        +    +-   +-   +-   +  
 w   3  3   25725 -0.011292    1( b3u)   2( b3u)   +-        +    +-   +-   +-   +  
 w   3  3   25726 -0.010277    2( b3u)   2( b3u)   +-        +    +-   +-   +-   +  
 w   3  3   25904  0.014424    1( b3g)   1( b3g)   +    +-   +-   +-   +-        +  
 w   3  3   26347 -0.011020    1( b2g)   1( b3g)   +    +-   +-   +-   +     -   +  
 w   3  3   26543  0.014492    1( b2g)   1( b2g)   +    +-   +-   +-        +-   +  
 w   3  3   27203  0.011015    1( b1g)   1( b3g)   +    +-   +-   +    +-    -   +  
 w   3  3   27405 -0.011071    1( b1g)   1( b2g)   +    +-   +-   +     -   +-   +  
 w   3  3   27610  0.014341    1( b1g)   1( b1g)   +    +-   +-        +-   +-   +  
 w   3  3   27849  0.012353    1( b1u)   1( b2u)   +    +-    -   +-   +-   +    +  
 w   3  3   27850  0.010847    2( b1u)   1( b2u)   +    +-    -   +-   +-   +    +  
 w   3  3   27854  0.010909    1( b1u)   2( b2u)   +    +-    -   +-   +-   +    +  
 w   3  3   27855  0.011168    2( b1u)   2( b2u)   +    +-    -   +-   +-   +    +  
 w   3  3   28068 -0.011881    1( b1u)   1( b3u)   +    +-    -   +-   +    +-   +  
 w   3  3   28069 -0.010152    2( b1u)   1( b3u)   +    +-    -   +-   +    +-   +  
 w   3  3   28073 -0.010262    1( b1u)   2( b3u)   +    +-    -   +-   +    +-   +  
 w   3  3   28074 -0.010125    2( b1u)   2( b3u)   +    +-    -   +-   +    +-   +  
 w   3  3   28282  0.011316    1( b2u)   1( b3u)   +    +-    -   +    +-   +-   +  
 w   3  3   28480  0.017675    2( ag )   1( b3g)   +    +-   +    +-   +-    -   +  
 w   3  3   28488 -0.010832    4( ag )   2( b3g)   +    +-   +    +-   +-    -   +  
 w   3  3   28690 -0.016087    2( ag )   1( b2g)   +    +-   +    +-    -   +-   +  
 w   3  3   28698  0.010062    4( ag )   2( b2g)   +    +-   +    +-    -   +-   +  
 w   3  3   28904  0.016807    2( ag )   1( b1g)   +    +-   +     -   +-   +-   +  
 w   3  3   28912 -0.010438    4( ag )   2( b1g)   +    +-   +     -   +-   +-   +  
 w   3  3   29089  0.014864    2( ag )   2( ag )   +    +-        +-   +-   +-   +  
 w   3  3   29133  0.012372    1( b2u)   1( b2u)   +    +-        +-   +-   +-   +  
 w   3  3   29134  0.017193    1( b2u)   2( b2u)   +    +-        +-   +-   +-   +  
 w   3  3   29135  0.014227    2( b2u)   2( b2u)   +    +-        +-   +-   +-   +  
 w   3  3   29947 -0.013543    2( ag )   3( ag )   +     -   +    +-   +-   +-   +  
 w   3  3   30193 -0.011521    3( ag )   1( b3g)   +    +    +-   +-   +-    -   +  
 w   3  3   30403  0.012722    3( ag )   1( b2g)   +    +    +-   +-    -   +-   +  
 w   3  3   30617 -0.011689    3( ag )   1( b1g)   +    +    +-    -   +-   +-   +  
 w   3  3   31018  0.014430    3( ag )   3( ag )   +         +-   +-   +-   +-   +  
 w   3  3   31871 -0.015550    2( ag )   2( ag )        +-   +    +-   +-   +-   +  
 w   3  3   31876 -0.010489    2( ag )   4( ag )        +-   +    +-   +-   +-   +  
 w   3  3   31878 -0.010089    4( ag )   4( ag )        +-   +    +-   +-   +-   +  
 w   3  3   31901 -0.011634    1( b1u)   2( b1u)        +-   +    +-   +-   +-   +  
 w   3  3   32115 -0.011843    1( b1u)   2( b1u)        +    +-   +-   +-   +-   +  

 ci coefficient statistics:
           rq > 0.1                3
      0.1> rq > 0.01             115
     0.01> rq > 0.001           2585
    0.001> rq > 0.0001          3008
   0.0001> rq > 0.00001         3850
  0.00001> rq > 0.000001        8694
 0.000001> rq                  13903
           all                 32158
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:         126 2x:           0 4x:           0
All internal counts: zz :          15 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :          12 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:         193
  iref  icsf         v(icsf)             hv(icsf)
     1     1     -0.000000423310      0.000083038465
     2     2      0.000000386647     -0.000075846628

 number of reference csfs (nref) is     2.  root number (iroot) is  1.
 c0**2 =   0.00000000  c**2 (all zwalks) =   0.91195991

 warning: cnot**2 < 0.5, dv3 is not computed.

 warning: pople ci energy extrapolation is not computed because of small fac.
 fac =      -0.999999999999

 eref      =   -196.164656079566   "relaxed" cnot**2         =   0.000000000000
 eci       =   -196.487958522628   deltae = eci - eref       =  -0.323302443062
 eci+dv1   =   -196.811260965689   dv1 = (1-cnot**2)*deltae  =  -0.323302443062
 eci+dv2   =********************   dv2 = dv1 / cnot**2       =*****************
 eci+dv3   =   -196.487958522628   dv3 = dv1 / (2*cnot**2-1) =   0.000000000000
 eci+pople =   -196.164656079566   ( 12e- scaled deltae )    =   0.000000000000


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 2) =      -196.4725827989

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     2    3    4   11   14   17

                                         symmetry   ag   ag   ag   b1g  b2g  b3g

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1 -0.377347                        +-   +-   +-   +-   +-   +     - 
 z*  3  1       2 -0.448524                        +-   +-   +-   +-   +    +-   +  
 z   3  2       3  0.187149                        +-   +-   +-   +    +-   +-   +  
 z   3  3       4 -0.048096                        +-   +-   +    +-   +-   +-   +  
 z   3  3       5 -0.617101                        +-   +    +-   +-   +-   +-   +  
 z   3  3       6  0.421937                        +    +-   +-   +-   +-   +-   +  
 y   3  1     129  0.010374              1( ag )   +-   +-    -   +-   +    +-   +  
 y   3  3     209 -0.010696              1( b3g)   +-    -   +-   +-   +-   +    +  
 y   3  3     289  0.015901              1( ag )   +-   +     -   +-   +-   +-   +  
 y   3  3     341  0.013246              1( b1g)    -   +-   +-   +    +-   +-   +  
 y   3  3     361  0.013024              1( ag )    -   +    +-   +-   +-   +-   +  
 y   3  3     433 -0.010233              1( ag )        +-   +-   +-   +-   +-   +  
 x   3  3    8031  0.010434    1( b2g)   1( b3g)   +-   +    +-   +-    -    -   +  
 x   3  3    8217 -0.010766    1( b1g)   1( b3g)   +-   +    +-    -   +-    -   +  
 x   3  3    8391  0.010447    1( b1g)   1( b2g)   +-   +    +-    -    -   +-   +  
 x   3  3   13604  0.010324    2( ag )   1( b3g)    -   +    +-   +-   +-    -   +  
 w   1  1   20863  0.015024    1( b2u)   1( b2u)   +-   +-        +-   +-   +     - 
 w   1  1   20864  0.015125    1( b2u)   2( b2u)   +-   +-        +-   +-   +     - 
 w   1  1   20865  0.010607    2( b2u)   2( b2u)   +-   +-        +-   +-   +     - 
 w   3  1   21108  0.011640    1( b1u)   1( b1u)   +-   +-        +-   +    +-   +  
 w   3  1   21123  0.017437    1( b2u)   1( b2u)   +-   +-        +-   +    +-   +  
 w   3  1   21124  0.017549    1( b2u)   2( b2u)   +-   +-        +-   +    +-   +  
 w   3  1   21125  0.012392    2( b2u)   2( b2u)   +-   +-        +-   +    +-   +  
 w   3  1   21138  0.010813    1( b3u)   1( b3u)   +-   +-        +-   +    +-   +  
 w   3  3   21624 -0.012366    1( b3g)   1( b3g)   +-   +    +-   +-   +-        +  
 w   3  3   21625  0.010702    1( b3g)   2( b3g)   +-   +    +-   +-   +-        +  
 w   3  3   21626 -0.010488    2( b3g)   2( b3g)   +-   +    +-   +-   +-        +  
 w   3  3   22263 -0.011493    1( b2g)   1( b2g)   +-   +    +-   +-        +-   +  
 w   3  3   22264  0.010570    1( b2g)   2( b2g)   +-   +    +-   +-        +-   +  
 w   3  3   22265 -0.010669    2( b2g)   2( b2g)   +-   +    +-   +-        +-   +  
 w   3  3   23330 -0.012349    1( b1g)   1( b1g)   +-   +    +-        +-   +-   +  
 w   3  3   23331  0.010671    1( b1g)   2( b1g)   +-   +    +-        +-   +-   +  
 w   3  3   23332 -0.010503    2( b1g)   2( b1g)   +-   +    +-        +-   +-   +  
 w   1  1   23446  0.010074    1( b3u)   1( b3u)   +-   +     -   +-   +-   +     - 
 w   3  1   23691  0.010145    1( b2u)   1( b2u)   +-   +     -   +-   +    +-   +  
 w   3  1   23706  0.012229    1( b3u)   1( b3u)   +-   +     -   +-   +    +-   +  
 w   3  1   23707  0.010818    1( b3u)   2( b3u)   +-   +     -   +-   +    +-   +  
 w   3  3   24838 -0.015261    1( b1u)   1( b1u)   +-   +         +-   +-   +-   +  
 w   3  3   24839 -0.011231    1( b1u)   2( b1u)   +-   +         +-   +-   +-   +  
 w   3  3   24853 -0.023088    1( b2u)   1( b2u)   +-   +         +-   +-   +-   +  
 w   3  3   24854 -0.023397    1( b2u)   2( b2u)   +-   +         +-   +-   +-   +  
 w   3  3   24855 -0.016646    2( b2u)   2( b2u)   +-   +         +-   +-   +-   +  
 w   3  3   24868 -0.014268    1( b3u)   1( b3u)   +-   +         +-   +-   +-   +  
 w   3  3   25724 -0.010864    1( b3u)   1( b3u)   +-        +    +-   +-   +-   +  
 w   1  1   27696  0.015711    1( b1u)   1( b1u)   +    +-    -   +-   +-   +     - 
 w   1  1   27697  0.013399    1( b1u)   2( b1u)   +    +-    -   +-   +-   +     - 
 w   1  1   27711  0.013908    1( b2u)   1( b2u)   +    +-    -   +-   +-   +     - 
 w   1  1   27712  0.010475    1( b2u)   2( b2u)   +    +-    -   +-   +-   +     - 
 w   1  1   27726  0.012147    1( b3u)   1( b3u)   +    +-    -   +-   +-   +     - 
 w   3  1   27956  0.018557    1( b1u)   1( b1u)   +    +-    -   +-   +    +-   +  
 w   3  1   27957  0.015755    1( b1u)   2( b1u)   +    +-    -   +-   +    +-   +  
 w   3  1   27971  0.015637    1( b2u)   1( b2u)   +    +-    -   +-   +    +-   +  
 w   3  1   27972  0.011473    1( b2u)   2( b2u)   +    +-    -   +-   +    +-   +  
 w   3  1   27986  0.015308    1( b3u)   1( b3u)   +    +-    -   +-   +    +-   +  
 w   3  1   27987  0.011170    1( b3u)   2( b3u)   +    +-    -   +-   +    +-   +  
 w   3  3   29118  0.013261    1( b1u)   1( b1u)   +    +-        +-   +-   +-   +  
 w   3  3   29119  0.010242    1( b1u)   2( b1u)   +    +-        +-   +-   +-   +  
 w   3  3   29133  0.017910    1( b2u)   1( b2u)   +    +-        +-   +-   +-   +  
 w   3  3   29134  0.017697    1( b2u)   2( b2u)   +    +-        +-   +-   +-   +  
 w   3  3   29135  0.012238    2( b2u)   2( b2u)   +    +-        +-   +-   +-   +  
 w   3  3   29148  0.010188    1( b3u)   1( b3u)   +    +-        +-   +-   +-   +  
 w   1  1   29224  0.010145    1( b3u)   1( b3u)   +     -   +-   +-   +-   +     - 
 w   3  1   29484  0.012380    1( b3u)   1( b3u)   +     -   +-   +-   +    +-   +  
 w   3  1   29485  0.012059    1( b3u)   2( b3u)   +     -   +-   +-   +    +-   +  
 w   3  3   30428 -0.010956    2( b1u)   2( b3u)   +    +    +-   +-    -   +-   +  
 w   3  3   30799  0.010656    1( ag )   1( ag )   +    +     -   +-   +-   +-   +  
 w   3  3   30830  0.028287    1( b1u)   1( b1u)   +    +     -   +-   +-   +-   +  
 w   3  3   30831  0.022373    1( b1u)   2( b1u)   +    +     -   +-   +-   +-   +  
 w   3  3   30832  0.012396    2( b1u)   2( b1u)   +    +     -   +-   +-   +-   +  
 w   3  3   30845  0.026631    1( b2u)   1( b2u)   +    +     -   +-   +-   +-   +  
 w   3  3   30846  0.019960    1( b2u)   2( b2u)   +    +     -   +-   +-   +-   +  
 w   3  3   30847  0.010137    2( b2u)   2( b2u)   +    +     -   +-   +-   +-   +  
 w   3  3   30860  0.028065    1( b3u)   1( b3u)   +    +     -   +-   +-   +-   +  
 w   3  3   30861  0.022017    1( b3u)   2( b3u)   +    +     -   +-   +-   +-   +  
 w   3  3   30862  0.011984    2( b3u)   2( b3u)   +    +     -   +-   +-   +-   +  
 w   3  3   31044  0.010165    1( b1u)   1( b1u)   +         +-   +-   +-   +-   +  
 w   3  3   31074  0.018529    1( b3u)   1( b3u)   +         +-   +-   +-   +-   +  
 w   3  3   31075  0.019631    1( b3u)   2( b3u)   +         +-   +-   +-   +-   +  
 w   3  3   31076  0.014281    2( b3u)   2( b3u)   +         +-   +-   +-   +-   +  
 w   1  1   31120  0.012440    1( b1u)   1( b1u)        +-   +-   +-   +-   +     - 
 w   1  1   31121  0.012688    1( b1u)   2( b1u)        +-   +-   +-   +-   +     - 
 w   3  1   31380  0.014941    1( b1u)   1( b1u)        +-   +-   +-   +    +-   +  
 w   3  1   31381  0.015277    1( b1u)   2( b1u)        +-   +-   +-   +    +-   +  
 w   3  1   31382  0.010857    2( b1u)   2( b1u)        +-   +-   +-   +    +-   +  
 w   3  1   31410  0.010748    1( b3u)   1( b3u)        +-   +-   +-   +    +-   +  
 w   3  3   31900  0.014101    1( b1u)   1( b1u)        +-   +    +-   +-   +-   +  
 w   3  3   31901  0.012184    1( b1u)   2( b1u)        +-   +    +-   +-   +-   +  
 w   3  3   31915  0.011975    1( b2u)   1( b2u)        +-   +    +-   +-   +-   +  
 w   3  3   31930  0.010476    1( b3u)   1( b3u)        +-   +    +-   +-   +-   +  
 w   3  3   32114  0.025663    1( b1u)   1( b1u)        +    +-   +-   +-   +-   +  
 w   3  3   32115  0.024065    1( b1u)   2( b1u)        +    +-   +-   +-   +-   +  
 w   3  3   32116  0.015786    2( b1u)   2( b1u)        +    +-   +-   +-   +-   +  
 w   3  3   32129  0.014429    1( b2u)   1( b2u)        +    +-   +-   +-   +-   +  
 w   3  3   32144  0.023043    1( b3u)   1( b3u)        +    +-   +-   +-   +-   +  
 w   3  3   32145  0.020059    1( b3u)   2( b3u)        +    +-   +-   +-   +-   +  
 w   3  3   32146  0.012336    2( b3u)   2( b3u)        +    +-   +-   +-   +-   +  

 ci coefficient statistics:
           rq > 0.1                5
      0.1> rq > 0.01              91
     0.01> rq > 0.001           3746
    0.001> rq > 0.0001          7610
   0.0001> rq > 0.00001         9455
  0.00001> rq > 0.000001        9349
 0.000001> rq                   1902
           all                 32158
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:         126 2x:           0 4x:           0
All internal counts: zz :          15 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :          12 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:         193
  iref  icsf         v(icsf)             hv(icsf)
     1     1     -0.377346973216     74.020844756290
     2     2     -0.448523779966     87.983219030025

 number of reference csfs (nref) is     2.  root number (iroot) is  2.
 c0**2 =   0.34356432  c**2 (all zwalks) =   0.93974700

 warning: cnot**2 < 0.5, dv3 is not computed.

 warning: pople ci energy extrapolation is not computed because of small fac.
 fac =      -0.312871361220

 eref      =   -196.161544994610   "relaxed" cnot**2         =   0.343564319390
 eci       =   -196.472582798924   deltae = eci - eref       =  -0.311037804315
 eci+dv1   =   -196.676759111695   dv1 = (1-cnot**2)*deltae  =  -0.204176312771
 eci+dv2   =   -197.066871266170   dv2 = dv1 / cnot**2       =  -0.594288467246
 eci+dv3   =   -196.472582798924   dv3 = dv1 / (2*cnot**2-1) =   0.000000000000
 eci+pople =   -196.161544994610   ( 12e- scaled deltae )    =   0.000000000000


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 3) =      -196.4725827988

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     2    3    4   11   14   17

                                         symmetry   ag   ag   ag   b1g  b2g  b3g

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1  0.604535                        +-   +-   +-   +-   +-   +     - 
 z*  3  1       2 -0.601599                        +-   +-   +-   +-   +    +-   +  
 z   3  2       3 -0.425043                        +-   +-   +-   +    +-   +-   +  
 z   3  3       4  0.120070                        +-   +-   +    +-   +-   +-   +  
 z   3  3       5 -0.114649                        +-   +    +-   +-   +-   +-   +  
 z   3  3       6 -0.064325                        +    +-   +-   +-   +-   +-   +  
 y   1  1       7  0.012321              1( b3g)   +-   +-   +-   +-   +-         - 
 y   3  1      45  0.012261              1( b2g)   +-   +-   +-   +-        +-   +  
 y   1  1     115 -0.013515              1( ag )   +-   +-    -   +-   +-   +     - 
 y   3  1     129  0.013925              1( ag )   +-   +-    -   +-   +    +-   +  
 y   1  1     307 -0.012358              1( ag )    -   +-   +-   +-   +-   +     - 
 y   3  1     321  0.012051              1( ag )    -   +-   +-   +-   +    +-   +  
 x   1  1     997 -0.013310    1( b1g)   1( b2g)   +-   +-   +-    -    -   +     - 
 x   1  1    1000 -0.010600    2( b1g)   2( b2g)   +-   +-   +-    -    -   +     - 
 x   3  1    1241 -0.011219    1( b1g)   1( b3g)   +-   +-   +-    -   +     -   +  
 x   1  1    5091  0.014747    3( ag )   1( b2g)   +-    -   +-   +-    -   +     - 
 x   1  1    5099  0.011876    5( ag )   2( b2g)   +-    -   +-   +-    -   +     - 
 x   1  1    5128  0.012575    4( b1u)   4( b3u)   +-    -   +-   +-    -   +     - 
 x   1  1    5649 -0.013341    3( ag )   1( b1g)   +-    -   +-    -   +-   +     - 
 x   1  1    5657 -0.010633    5( ag )   2( b1g)   +-    -   +-    -   +-   +     - 
 x   3  1    5883  0.012845    3( ag )   1( b1g)   +-    -   +-    -   +    +-   +  
 x   3  1    5891  0.010655    5( ag )   2( b1g)   +-    -   +-    -   +    +-   +  
 x   3  1   10162  0.010283    2( ag )   1( b3g)    -   +-   +-   +-   +     -   +  
 x   3  1   10718  0.010219    2( ag )   1( b1g)    -   +-   +-    -   +    +-   +  
 x   3  1   11645  0.010228    1( ag )   2( ag )    -   +-    -   +-   +    +-   +  
 w   3  1   17252 -0.011999    1( b3g)   1( b3g)   +-   +-   +-   +-   +         +  
 w   3  1   17253  0.010408    1( b3g)   2( b3g)   +-   +-   +-   +-   +         +  
 w   3  1   17254 -0.010240    2( b3g)   2( b3g)   +-   +-   +-   +-   +         +  
 w   1  1   17417 -0.012058    1( b2g)   1( b2g)   +-   +-   +-   +-        +     - 
 w   1  1   17418  0.010459    1( b2g)   2( b2g)   +-   +-   +-   +-        +     - 
 w   1  1   17419 -0.010290    2( b2g)   2( b2g)   +-   +-   +-   +-        +     - 
 w   1  1   17821  0.010880    1( b1g)   1( b2g)   +-   +-   +-   +     -   +     - 
 w   1  1   18484 -0.012065    1( b1g)   1( b1g)   +-   +-   +-        +-   +     - 
 w   1  1   18485  0.010461    1( b1g)   2( b1g)   +-   +-   +-        +-   +     - 
 w   1  1   18486 -0.010290    2( b1g)   2( b1g)   +-   +-   +-        +-   +     - 
 w   3  1   18744  0.012006    1( b1g)   1( b1g)   +-   +-   +-        +    +-   +  
 w   3  1   18745 -0.010410    1( b1g)   2( b1g)   +-   +-   +-        +    +-   +  
 w   3  1   18746  0.010240    2( b1g)   2( b1g)   +-   +-   +-        +    +-   +  
 w   1  1   20848 -0.016014    1( b1u)   1( b1u)   +-   +-        +-   +-   +     - 
 w   1  1   20849 -0.011845    1( b1u)   2( b1u)   +-   +-        +-   +-   +     - 
 w   1  1   20863 -0.024066    1( b2u)   1( b2u)   +-   +-        +-   +-   +     - 
 w   1  1   20864 -0.024228    1( b2u)   2( b2u)   +-   +-        +-   +-   +     - 
 w   1  1   20865 -0.016990    2( b2u)   2( b2u)   +-   +-        +-   +-   +     - 
 w   1  1   20878 -0.013758    1( b3u)   1( b3u)   +-   +-        +-   +-   +     - 
 w   3  1   21108  0.015603    1( b1u)   1( b1u)   +-   +-        +-   +    +-   +  
 w   3  1   21109  0.011464    1( b1u)   2( b1u)   +-   +-        +-   +    +-   +  
 w   3  1   21123  0.023403    1( b2u)   1( b2u)   +-   +-        +-   +    +-   +  
 w   3  1   21124  0.023552    1( b2u)   2( b2u)   +-   +-        +-   +    +-   +  
 w   3  1   21125  0.016628    2( b2u)   2( b2u)   +-   +-        +-   +    +-   +  
 w   3  1   21138  0.014507    1( b3u)   1( b3u)   +-   +-        +-   +    +-   +  
 w   3  2   21368  0.010515    1( b1u)   1( b1u)   +-   +-        +    +-   +-   +  
 w   3  2   21383  0.016885    1( b2u)   1( b2u)   +-   +-        +    +-   +-   +  
 w   3  2   21384  0.016991    1( b2u)   2( b2u)   +-   +-        +    +-   +-   +  
 w   3  2   21385  0.011916    2( b2u)   2( b2u)   +-   +-        +    +-   +-   +  
 w   3  2   21398  0.010427    1( b3u)   1( b3u)   +-   +-        +    +-   +-   +  
 w   1  1   21675 -0.010941    3( ag )   1( b2g)   +-   +    +-   +-    -   +     - 
 w   1  1   22317  0.010922    3( ag )   1( b1g)   +-   +    +-    -   +-   +     - 
 w   3  1   22607 -0.010510    3( ag )   1( b1g)   +-   +    +-    -   +    +-   +  
 w   1  1   23431 -0.014075    1( b2u)   1( b2u)   +-   +     -   +-   +-   +     - 
 w   1  1   23432 -0.011371    1( b2u)   2( b2u)   +-   +     -   +-   +-   +     - 
 w   1  1   23446 -0.016144    1( b3u)   1( b3u)   +-   +     -   +-   +-   +     - 
 w   1  1   23447 -0.014219    1( b3u)   2( b3u)   +-   +     -   +-   +-   +     - 
 w   3  1   23691  0.013619    1( b2u)   1( b2u)   +-   +     -   +-   +    +-   +  
 w   3  1   23692  0.010939    1( b2u)   2( b2u)   +-   +     -   +-   +    +-   +  
 w   3  1   23706  0.016420    1( b3u)   1( b3u)   +-   +     -   +-   +    +-   +  
 w   3  1   23707  0.014536    1( b3u)   2( b3u)   +-   +     -   +-   +    +-   +  
 w   3  2   23951  0.010165    1( b2u)   1( b2u)   +-   +     -   +    +-   +-   +  
 w   3  2   23966  0.011776    1( b3u)   1( b3u)   +-   +     -   +    +-   +-   +  
 w   3  2   23967  0.010487    1( b3u)   2( b3u)   +-   +     -   +    +-   +-   +  
 w   1  1   24888 -0.011922    3( ag )   3( ag )   +-        +-   +-   +-   +     - 
 w   1  1   24945 -0.012078    1( b3u)   2( b3u)   +-        +-   +-   +-   +     - 
 w   3  1   25148  0.011116    3( ag )   3( ag )   +-        +-   +-   +    +-   +  
 w   3  1   25205  0.011935    1( b3u)   2( b3u)   +-        +-   +-   +    +-   +  
 w   1  1   27696 -0.025176    1( b1u)   1( b1u)   +    +-    -   +-   +-   +     - 
 w   1  1   27697 -0.021480    1( b1u)   2( b1u)   +    +-    -   +-   +-   +     - 
 w   1  1   27698 -0.012664    2( b1u)   2( b1u)   +    +-    -   +-   +-   +     - 
 w   1  1   27711 -0.022262    1( b2u)   1( b2u)   +    +-    -   +-   +-   +     - 
 w   1  1   27712 -0.016756    1( b2u)   2( b2u)   +    +-    -   +-   +-   +     - 
 w   1  1   27726 -0.019465    1( b3u)   1( b3u)   +    +-    -   +-   +-   +     - 
 w   1  1   27727 -0.013876    1( b3u)   2( b3u)   +    +-    -   +-   +-   +     - 
 w   3  1   27956  0.024896    1( b1u)   1( b1u)   +    +-    -   +-   +    +-   +  
 w   3  1   27957  0.021140    1( b1u)   2( b1u)   +    +-    -   +-   +    +-   +  
 w   3  1   27958  0.012400    2( b1u)   2( b1u)   +    +-    -   +-   +    +-   +  
 w   3  1   27971  0.020993    1( b2u)   1( b2u)   +    +-    -   +-   +    +-   +  
 w   3  1   27972  0.015404    1( b2u)   2( b2u)   +    +-    -   +-   +    +-   +  
 w   3  1   27986  0.020516    1( b3u)   1( b3u)   +    +-    -   +-   +    +-   +  
 w   3  1   27987  0.014966    1( b3u)   2( b3u)   +    +-    -   +-   +    +-   +  
 w   3  2   28216  0.017006    1( b1u)   1( b1u)   +    +-    -   +    +-   +-   +  
 w   3  2   28217  0.014343    1( b1u)   2( b1u)   +    +-    -   +    +-   +-   +  
 w   3  2   28231  0.015496    1( b2u)   1( b2u)   +    +-    -   +    +-   +-   +  
 w   3  2   28232  0.011595    1( b2u)   2( b2u)   +    +-    -   +    +-   +-   +  
 w   3  2   28246  0.014443    1( b3u)   1( b3u)   +    +-    -   +    +-   +-   +  
 w   3  2   28247  0.010547    1( b3u)   2( b3u)   +    +-    -   +    +-   +-   +  
 w   1  1   29194 -0.010271    1( b1u)   1( b1u)   +     -   +-   +-   +-   +     - 
 w   1  1   29224 -0.016257    1( b3u)   1( b3u)   +     -   +-   +-   +-   +     - 
 w   1  1   29225 -0.015767    1( b3u)   2( b3u)   +     -   +-   +-   +-   +     - 
 w   1  1   29226 -0.010463    2( b3u)   2( b3u)   +     -   +-   +-   +-   +     - 
 w   3  1   29454  0.010545    1( b1u)   1( b1u)   +     -   +-   +-   +    +-   +  
 w   3  1   29484  0.016606    1( b3u)   1( b3u)   +     -   +-   +-   +    +-   +  
 w   3  1   29485  0.016175    1( b3u)   2( b3u)   +     -   +-   +-   +    +-   +  
 w   3  1   29486  0.010707    2( b3u)   2( b3u)   +     -   +-   +-   +    +-   +  
 w   3  2   29744  0.011630    1( b3u)   1( b3u)   +     -   +-   +    +-   +-   +  
 w   3  2   29745  0.011263    1( b3u)   2( b3u)   +     -   +-   +    +-   +-   +  
 w   1  1   31120 -0.019948    1( b1u)   1( b1u)        +-   +-   +-   +-   +     - 
 w   1  1   31121 -0.020356    1( b1u)   2( b1u)        +-   +-   +-   +-   +     - 
 w   1  1   31122 -0.014464    2( b1u)   2( b1u)        +-   +-   +-   +-   +     - 
 w   1  1   31135 -0.010642    1( b2u)   1( b2u)        +-   +-   +-   +-   +     - 
 w   1  1   31150 -0.013599    1( b3u)   1( b3u)        +-   +-   +-   +-   +     - 
 w   1  1   31151 -0.010754    1( b3u)   2( b3u)        +-   +-   +-   +-   +     - 
 w   3  1   31380  0.020061    1( b1u)   1( b1u)        +-   +-   +-   +    +-   +  
 w   3  1   31381  0.020522    1( b1u)   2( b1u)        +-   +-   +-   +    +-   +  
 w   3  1   31382  0.014588    2( b1u)   2( b1u)        +-   +-   +-   +    +-   +  
 w   3  1   31410  0.014391    1( b3u)   1( b3u)        +-   +-   +-   +    +-   +  
 w   3  1   31411  0.011624    1( b3u)   2( b3u)        +-   +-   +-   +    +-   +  
 w   3  2   31640  0.013814    1( b1u)   1( b1u)        +-   +-   +    +-   +-   +  
 w   3  2   31641  0.014116    1( b1u)   2( b1u)        +-   +-   +    +-   +-   +  
 w   3  2   31642  0.010096    2( b1u)   2( b1u)        +-   +-   +    +-   +-   +  

 ci coefficient statistics:
           rq > 0.1                5
      0.1> rq > 0.01             112
     0.01> rq > 0.001           3134
    0.001> rq > 0.0001          8092
   0.0001> rq > 0.00001        11197
  0.00001> rq > 0.000001        8091
 0.000001> rq                   1527
           all                 32158
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:         126 2x:           0 4x:           0
All internal counts: zz :          15 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :          12 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:         193
  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.604534786207   -118.588357697114
     2     2     -0.601599271703    118.012522704233

 number of reference csfs (nref) is     2.  root number (iroot) is  3.
 c0**2 =   0.72738399  c**2 (all zwalks) =   0.93974478

 pople ci energy extrapolation is computed with 12 correlated electrons.

 eref      =   -196.164662482777   "relaxed" cnot**2         =   0.727383991448
 eci       =   -196.472582798844   deltae = eci - eref       =  -0.307920316067
 eci+dv1   =   -196.556526806362   dv1 = (1-cnot**2)*deltae  =  -0.083944007518
 eci+dv2   =   -196.587988153519   dv2 = dv1 / cnot**2       =  -0.115405354675
 eci+dv3   =   -196.657169248804   dv3 = dv1 / (2*cnot**2-1) =  -0.184586449960
 eci+pople =   -196.596625967900   ( 12e- scaled deltae )    =  -0.431963485124


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 4) =      -196.4725827984

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     2    3    4   11   14   17

                                         symmetry   ag   ag   ag   b1g  b2g  b3g

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1  0.236632                        +-   +-   +-   +-   +-   +     - 
 z*  3  1       2  0.027427                        +-   +-   +-   +-   +    +-   +  
 z   3  2       3  0.590052                        +-   +-   +-   +    +-   +-   +  
 z   3  3       4  0.528905                        +-   +-   +    +-   +-   +-   +  
 z   3  3       5 -0.303133                        +-   +    +-   +-   +-   +-   +  
 z   3  3       6 -0.403991                        +    +-   +-   +-   +-   +-   +  
 y   3  2     107  0.012024              1( b1g)   +-   +-   +-        +-   +-   +  
 y   3  3     137 -0.014192              1( b2g)   +-   +-    -   +-   +    +-   +  
 y   3  2     143 -0.013258              1( ag )   +-   +-    -   +    +-   +-   +  
 y   3  3     193 -0.014356              1( ag )   +-   +-        +-   +-   +-   +  
 y   3  3     291 -0.014380              3( ag )   +-   +     -   +-   +-   +-   +  
 y   3  2     335 -0.012355              1( ag )    -   +-   +-   +    +-   +-   +  
 y   3  3     349 -0.012325              1( ag )    -   +-   +    +-   +-   +-   +  
 y   3  3     433  0.010388              1( ag )        +-   +-   +-   +-   +-   +  
 x   3  2    1659 -0.010280    1( b2g)   1( b3g)   +-   +-   +-   +     -    -   +  
 x   3  2    6301  0.012021    3( ag )   1( b2g)   +-    -   +-   +     -   +-   +  
 x   3  2    6309  0.010108    5( ag )   2( b2g)   +-    -   +-   +     -   +-   +  
 x   3  2    6338  0.010668    4( b1u)   4( b3u)   +-    -   +-   +     -   +-   +  
 x   3  3    7463  0.010369    3( ag )   1( b2g)   +-    -   +    +-    -   +-   +  
 w   3  2   17726  0.011801    1( b3g)   1( b3g)   +-   +-   +-   +    +-        +  
 w   3  2   17727 -0.010219    1( b3g)   2( b3g)   +-   +-   +-   +    +-        +  
 w   3  2   17728  0.010041    2( b3g)   2( b3g)   +-   +-   +-   +    +-        +  
 w   3  2   18365  0.011791    1( b2g)   1( b2g)   +-   +-   +-   +         +-   +  
 w   3  2   18366 -0.010216    1( b2g)   2( b2g)   +-   +-   +-   +         +-   +  
 w   3  2   18367  0.010042    2( b2g)   2( b2g)   +-   +-   +-   +         +-   +  
 w   3  3   19056  0.010284    1( b3g)   1( b3g)   +-   +-   +    +-   +-        +  
 w   3  3   19695  0.010740    1( b2g)   1( b2g)   +-   +-   +    +-        +-   +  
 w   3  3   20762  0.010011    1( b1g)   1( b1g)   +-   +-   +         +-   +-   +  
 w   3  2   21368 -0.014580    1( b1u)   1( b1u)   +-   +-        +    +-   +-   +  
 w   3  2   21369 -0.010459    1( b1u)   2( b1u)   +-   +-        +    +-   +-   +  
 w   3  2   21383 -0.023416    1( b2u)   1( b2u)   +-   +-        +    +-   +-   +  
 w   3  2   21384 -0.023551    1( b2u)   2( b2u)   +-   +-        +    +-   +-   +  
 w   3  2   21385 -0.016510    2( b2u)   2( b2u)   +-   +-        +    +-   +-   +  
 w   3  2   21398 -0.014511    1( b3u)   1( b3u)   +-   +-        +    +-   +-   +  
 w   3  2   21399 -0.010027    1( b3u)   2( b3u)   +-   +-        +    +-   +-   +  
 w   3  2   23951 -0.014121    1( b2u)   1( b2u)   +-   +     -   +    +-   +-   +  
 w   3  2   23952 -0.011580    1( b2u)   2( b2u)   +-   +     -   +    +-   +-   +  
 w   3  2   23966 -0.016361    1( b3u)   1( b3u)   +-   +     -   +    +-   +-   +  
 w   3  2   23967 -0.014573    1( b3u)   2( b3u)   +-   +     -   +    +-   +-   +  
 w   3  3   24838 -0.012608    1( b1u)   1( b1u)   +-   +         +-   +-   +-   +  
 w   3  3   24853 -0.020719    1( b2u)   1( b2u)   +-   +         +-   +-   +-   +  
 w   3  3   24854 -0.019074    1( b2u)   2( b2u)   +-   +         +-   +-   +-   +  
 w   3  3   24855 -0.012311    2( b2u)   2( b2u)   +-   +         +-   +-   +-   +  
 w   3  3   24868 -0.018376    1( b3u)   1( b3u)   +-   +         +-   +-   +-   +  
 w   3  3   24869 -0.015063    1( b3u)   2( b3u)   +-   +         +-   +-   +-   +  
 w   3  2   25408 -0.011195    3( ag )   3( ag )   +-        +-   +    +-   +-   +  
 w   3  2   25465 -0.011841    1( b3u)   2( b3u)   +-        +-   +    +-   +-   +  
 w   3  3   25724 -0.014514    1( b3u)   1( b3u)   +-        +    +-   +-   +-   +  
 w   3  3   25725 -0.015748    1( b3u)   2( b3u)   +-        +    +-   +-   +-   +  
 w   3  3   25726 -0.011683    2( b3u)   2( b3u)   +-        +    +-   +-   +-   +  
 w   3  2   28216 -0.023584    1( b1u)   1( b1u)   +    +-    -   +    +-   +-   +  
 w   3  2   28217 -0.019894    1( b1u)   2( b1u)   +    +-    -   +    +-   +-   +  
 w   3  2   28218 -0.011751    2( b1u)   2( b1u)   +    +-    -   +    +-   +-   +  
 w   3  2   28231 -0.021519    1( b2u)   1( b2u)   +    +-    -   +    +-   +-   +  
 w   3  2   28232 -0.016107    1( b2u)   2( b2u)   +    +-    -   +    +-   +-   +  
 w   3  2   28246 -0.020072    1( b3u)   1( b3u)   +    +-    -   +    +-   +-   +  
 w   3  2   28247 -0.014658    1( b3u)   2( b3u)   +    +-    -   +    +-   +-   +  
 w   3  3   28497 -0.010768    2( b1u)   2( b2u)   +    +-   +    +-   +-    -   +  
 w   3  3   28716  0.010395    2( b1u)   2( b3u)   +    +-   +    +-    -   +-   +  
 w   3  3   29087 -0.011060    1( ag )   1( ag )   +    +-        +-   +-   +-   +  
 w   3  3   29118 -0.025777    1( b1u)   1( b1u)   +    +-        +-   +-   +-   +  
 w   3  3   29119 -0.020752    1( b1u)   2( b1u)   +    +-        +-   +-   +-   +  
 w   3  3   29120 -0.011886    2( b1u)   2( b1u)   +    +-        +-   +-   +-   +  
 w   3  3   29133 -0.029802    1( b2u)   1( b2u)   +    +-        +-   +-   +-   +  
 w   3  3   29134 -0.026345    1( b2u)   2( b2u)   +    +-        +-   +-   +-   +  
 w   3  3   29135 -0.016226    2( b2u)   2( b2u)   +    +-        +-   +-   +-   +  
 w   3  3   29148 -0.021837    1( b3u)   1( b3u)   +    +-        +-   +-   +-   +  
 w   3  3   29149 -0.015357    1( b3u)   2( b3u)   +    +-        +-   +-   +-   +  
 w   3  2   29744 -0.016135    1( b3u)   1( b3u)   +     -   +-   +    +-   +-   +  
 w   3  2   29745 -0.015613    1( b3u)   2( b3u)   +     -   +-   +    +-   +-   +  
 w   3  2   29746 -0.010270    2( b3u)   2( b3u)   +     -   +-   +    +-   +-   +  
 w   3  3   29974 -0.017509    1( b1u)   1( b1u)   +     -   +    +-   +-   +-   +  
 w   3  3   29975 -0.011524    1( b1u)   2( b1u)   +     -   +    +-   +-   +-   +  
 w   3  3   29989 -0.019030    1( b2u)   1( b2u)   +     -   +    +-   +-   +-   +  
 w   3  3   29990 -0.013738    1( b2u)   2( b2u)   +     -   +    +-   +-   +-   +  
 w   3  3   30004 -0.024945    1( b3u)   1( b3u)   +     -   +    +-   +-   +-   +  
 w   3  3   30005 -0.022409    1( b3u)   2( b3u)   +     -   +    +-   +-   +-   +  
 w   3  3   30006 -0.013851    2( b3u)   2( b3u)   +     -   +    +-   +-   +-   +  
 w   3  2   31640 -0.019161    1( b1u)   1( b1u)        +-   +-   +    +-   +-   +  
 w   3  2   31641 -0.019582    1( b1u)   2( b1u)        +-   +-   +    +-   +-   +  
 w   3  2   31642 -0.014009    2( b1u)   2( b1u)        +-   +-   +    +-   +-   +  
 w   3  2   31655 -0.010238    1( b2u)   1( b2u)        +-   +-   +    +-   +-   +  
 w   3  2   31670 -0.013748    1( b3u)   1( b3u)        +-   +-   +    +-   +-   +  
 w   3  2   31671 -0.011014    1( b3u)   2( b3u)        +-   +-   +    +-   +-   +  
 w   3  3   31869 -0.010583    1( ag )   1( ag )        +-   +    +-   +-   +-   +  
 w   3  3   31900 -0.028602    1( b1u)   1( b1u)        +-   +    +-   +-   +-   +  
 w   3  3   31901 -0.027117    1( b1u)   2( b1u)        +-   +    +-   +-   +-   +  
 w   3  3   31902 -0.018124    2( b1u)   2( b1u)        +-   +    +-   +-   +-   +  
 w   3  3   31915 -0.020507    1( b2u)   1( b2u)        +-   +    +-   +-   +-   +  
 w   3  3   31916 -0.014463    1( b2u)   2( b2u)        +-   +    +-   +-   +-   +  
 w   3  3   31930 -0.021072    1( b3u)   1( b3u)        +-   +    +-   +-   +-   +  
 w   3  3   31931 -0.015902    1( b3u)   2( b3u)        +-   +    +-   +-   +-   +  

 ci coefficient statistics:
           rq > 0.1                5
      0.1> rq > 0.01              87
     0.01> rq > 0.001           3618
    0.001> rq > 0.0001          6841
   0.0001> rq > 0.00001         8559
  0.00001> rq > 0.000001        9177
 0.000001> rq                   3871
           all                 32158
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:         126 2x:           0 4x:           0
All internal counts: zz :          15 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :          12 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:         193
  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.236631536351    -46.418330805980
     2     2      0.027427175391     -5.379827944346

 number of reference csfs (nref) is     2.  root number (iroot) is  4.
 c0**2 =   0.05674673  c**2 (all zwalks) =   0.93974608

 warning: cnot**2 < 0.5, dv3 is not computed.

 warning: pople ci energy extrapolation is not computed because of small fac.
 fac =      -0.886506532108

 eref      =   -196.162732972261   "relaxed" cnot**2         =   0.056746733946
 eci       =   -196.472582798353   deltae = eci - eref       =  -0.309849826092
 eci+dv1   =   -196.764849658800   dv1 = (1-cnot**2)*deltae  =  -0.292266860447
 eci+dv2   =   -201.622956046716   dv2 = dv1 / cnot**2       =  -5.150373248363
 eci+dv3   =   -196.472582798353   dv3 = dv1 / (2*cnot**2-1) =   0.000000000000
 eci+pople =   -196.162732972261   ( 12e- scaled deltae )    =   0.000000000000


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 5) =      -196.4644811598

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     2    3    4   11   14   17

                                         symmetry   ag   ag   ag   b1g  b2g  b3g

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1 -0.566139                        +-   +-   +-   +-   +-   +     - 
 z*  3  1       2 -0.486908                        +-   +-   +-   +-   +    +-   +  
 z   3  2       3 -0.079232                        +-   +-   +-   +    +-   +-   +  
 z   3  3       4  0.220961                        +-   +-   +    +-   +-   +-   +  
 z   3  3       5  0.349077                        +-   +    +-   +-   +-   +-   +  
 z   3  3       6 -0.453027                        +    +-   +-   +-   +-   +-   +  
 y   1  1       7 -0.011764              1( b3g)   +-   +-   +-   +-   +-         - 
 y   3  1      45  0.010117              1( b2g)   +-   +-   +-   +-        +-   +  
 y   1  1     115  0.012715              1( ag )   +-   +-    -   +-   +-   +     - 
 y   3  1     129  0.011328              1( ag )   +-   +-    -   +-   +    +-   +  
 y   3  3     289 -0.010172              1( ag )   +-   +     -   +-   +-   +-   +  
 y   1  1     307  0.011624              1( ag )    -   +-   +-   +-   +-   +     - 
 y   3  3     341 -0.011401              1( b1g)    -   +-   +-   +    +-   +-   +  
 y   3  3     433  0.011290              1( ag )        +-   +-   +-   +-   +-   +  
 x   1  1     997  0.012469    1( b1g)   1( b2g)   +-   +-   +-    -    -   +     - 
 x   1  1    5091 -0.013817    3( ag )   1( b2g)   +-    -   +-   +-    -   +     - 
 x   1  1    5099 -0.011121    5( ag )   2( b2g)   +-    -   +-   +-    -   +     - 
 x   1  1    5128 -0.011777    4( b1u)   4( b3u)   +-    -   +-   +-    -   +     - 
 x   1  1    5649  0.012484    3( ag )   1( b1g)   +-    -   +-    -   +-   +     - 
 x   3  1    5883  0.010392    3( ag )   1( b1g)   +-    -   +-    -   +    +-   +  
 w   1  1   17417  0.011319    1( b2g)   1( b2g)   +-   +-   +-   +-        +     - 
 w   1  1   17821 -0.010200    1( b1g)   1( b2g)   +-   +-   +-   +     -   +     - 
 w   1  1   18484  0.011301    1( b1g)   1( b1g)   +-   +-   +-        +-   +     - 
 w   1  1   20848  0.014995    1( b1u)   1( b1u)   +-   +-        +-   +-   +     - 
 w   1  1   20849  0.011085    1( b1u)   2( b1u)   +-   +-        +-   +-   +     - 
 w   1  1   20863  0.022554    1( b2u)   1( b2u)   +-   +-        +-   +-   +     - 
 w   1  1   20864  0.022706    1( b2u)   2( b2u)   +-   +-        +-   +-   +     - 
 w   1  1   20865  0.015924    2( b2u)   2( b2u)   +-   +-        +-   +-   +     - 
 w   1  1   20878  0.012878    1( b3u)   1( b3u)   +-   +-        +-   +-   +     - 
 w   3  1   21108  0.012625    1( b1u)   1( b1u)   +-   +-        +-   +    +-   +  
 w   3  1   21123  0.018934    1( b2u)   1( b2u)   +-   +-        +-   +    +-   +  
 w   3  1   21124  0.019054    1( b2u)   2( b2u)   +-   +-        +-   +    +-   +  
 w   3  1   21125  0.013453    2( b2u)   2( b2u)   +-   +-        +-   +    +-   +  
 w   3  1   21138  0.011753    1( b3u)   1( b3u)   +-   +-        +-   +    +-   +  
 w   1  1   21675  0.010252    3( ag )   1( b2g)   +-   +    +-   +-    -   +     - 
 w   1  1   22317 -0.010224    3( ag )   1( b1g)   +-   +    +-    -   +-   +     - 
 w   1  1   23431  0.013196    1( b2u)   1( b2u)   +-   +     -   +-   +-   +     - 
 w   1  1   23432  0.010667    1( b2u)   2( b2u)   +-   +     -   +-   +-   +     - 
 w   1  1   23446  0.015107    1( b3u)   1( b3u)   +-   +     -   +-   +-   +     - 
 w   1  1   23447  0.013309    1( b3u)   2( b3u)   +-   +     -   +-   +-   +     - 
 w   3  1   23691  0.011021    1( b2u)   1( b2u)   +-   +     -   +-   +    +-   +  
 w   3  1   23706  0.013303    1( b3u)   1( b3u)   +-   +     -   +-   +    +-   +  
 w   3  1   23707  0.011781    1( b3u)   2( b3u)   +-   +     -   +-   +    +-   +  
 w   3  3   24854  0.010573    1( b2u)   2( b2u)   +-   +         +-   +-   +-   +  
 w   1  1   24888  0.011158    3( ag )   3( ag )   +-        +-   +-   +-   +     - 
 w   1  1   24945  0.011302    1( b3u)   2( b3u)   +-        +-   +-   +-   +     - 
 w   1  1   27696  0.023588    1( b1u)   1( b1u)   +    +-    -   +-   +-   +     - 
 w   1  1   27697  0.020123    1( b1u)   2( b1u)   +    +-    -   +-   +-   +     - 
 w   1  1   27698  0.011863    2( b1u)   2( b1u)   +    +-    -   +-   +-   +     - 
 w   1  1   27711  0.020852    1( b2u)   1( b2u)   +    +-    -   +-   +-   +     - 
 w   1  1   27712  0.015688    1( b2u)   2( b2u)   +    +-    -   +-   +-   +     - 
 w   1  1   27726  0.018218    1( b3u)   1( b3u)   +    +-    -   +-   +-   +     - 
 w   1  1   27727  0.012987    1( b3u)   2( b3u)   +    +-    -   +-   +-   +     - 
 w   3  1   27956  0.020157    1( b1u)   1( b1u)   +    +-    -   +-   +    +-   +  
 w   3  1   27957  0.017118    1( b1u)   2( b1u)   +    +-    -   +-   +    +-   +  
 w   3  1   27958  0.010042    2( b1u)   2( b1u)   +    +-    -   +-   +    +-   +  
 w   3  1   27971  0.016985    1( b2u)   1( b2u)   +    +-    -   +-   +    +-   +  
 w   3  1   27972  0.012464    1( b2u)   2( b2u)   +    +-    -   +-   +    +-   +  
 w   3  1   27986  0.016608    1( b3u)   1( b3u)   +    +-    -   +-   +    +-   +  
 w   3  1   27987  0.012113    1( b3u)   2( b3u)   +    +-    -   +-   +    +-   +  
 w   3  3   29118 -0.018833    1( b1u)   1( b1u)   +    +-        +-   +-   +-   +  
 w   3  3   29119 -0.014842    1( b1u)   2( b1u)   +    +-        +-   +-   +-   +  
 w   3  3   29133 -0.023668    1( b2u)   1( b2u)   +    +-        +-   +-   +-   +  
 w   3  3   29134 -0.022295    1( b2u)   2( b2u)   +    +-        +-   +-   +-   +  
 w   3  3   29135 -0.014718    2( b2u)   2( b2u)   +    +-        +-   +-   +-   +  
 w   3  3   29148 -0.015178    1( b3u)   1( b3u)   +    +-        +-   +-   +-   +  
 w   3  3   29149 -0.010260    1( b3u)   2( b3u)   +    +-        +-   +-   +-   +  
 w   1  1   29224  0.015212    1( b3u)   1( b3u)   +     -   +-   +-   +-   +     - 
 w   1  1   29225  0.014754    1( b3u)   2( b3u)   +     -   +-   +-   +-   +     - 
 w   3  1   29484  0.013443    1( b3u)   1( b3u)   +     -   +-   +-   +    +-   +  
 w   3  1   29485  0.013091    1( b3u)   2( b3u)   +     -   +-   +-   +    +-   +  
 w   3  3   30830 -0.019219    1( b1u)   1( b1u)   +    +     -   +-   +-   +-   +  
 w   3  3   30831 -0.014566    1( b1u)   2( b1u)   +    +     -   +-   +-   +-   +  
 w   3  3   30845 -0.019295    1( b2u)   1( b2u)   +    +     -   +-   +-   +-   +  
 w   3  3   30846 -0.014684    1( b2u)   2( b2u)   +    +     -   +-   +-   +-   +  
 w   3  3   30860 -0.020683    1( b3u)   1( b3u)   +    +     -   +-   +-   +-   +  
 w   3  3   30861 -0.016664    1( b3u)   2( b3u)   +    +     -   +-   +-   +-   +  
 w   3  3   31074 -0.013911    1( b3u)   1( b3u)   +         +-   +-   +-   +-   +  
 w   3  3   31075 -0.015335    1( b3u)   2( b3u)   +         +-   +-   +-   +-   +  
 w   3  3   31076 -0.011587    2( b3u)   2( b3u)   +         +-   +-   +-   +-   +  
 w   1  1   31120  0.018700    1( b1u)   1( b1u)        +-   +-   +-   +-   +     - 
 w   1  1   31121  0.019087    1( b1u)   2( b1u)        +-   +-   +-   +-   +     - 
 w   1  1   31122  0.013565    2( b1u)   2( b1u)        +-   +-   +-   +-   +     - 
 w   1  1   31150  0.012726    1( b3u)   1( b3u)        +-   +-   +-   +-   +     - 
 w   1  1   31151  0.010065    1( b3u)   2( b3u)        +-   +-   +-   +-   +     - 
 w   3  1   31380  0.016251    1( b1u)   1( b1u)        +-   +-   +-   +    +-   +  
 w   3  1   31381  0.016629    1( b1u)   2( b1u)        +-   +-   +-   +    +-   +  
 w   3  1   31382  0.011823    2( b1u)   2( b1u)        +-   +-   +-   +    +-   +  
 w   3  1   31410  0.011640    1( b3u)   1( b3u)        +-   +-   +-   +    +-   +  
 w   3  3   31900 -0.020437    1( b1u)   1( b1u)        +-   +    +-   +-   +-   +  
 w   3  3   31901 -0.018499    1( b1u)   2( b1u)        +-   +    +-   +-   +-   +  
 w   3  3   31902 -0.011698    2( b1u)   2( b1u)        +-   +    +-   +-   +-   +  
 w   3  3   31915 -0.016038    1( b2u)   1( b2u)        +-   +    +-   +-   +-   +  
 w   3  3   31916 -0.011731    1( b2u)   2( b2u)        +-   +    +-   +-   +-   +  
 w   3  3   31930 -0.015117    1( b3u)   1( b3u)        +-   +    +-   +-   +-   +  
 w   3  3   31931 -0.011091    1( b3u)   2( b3u)        +-   +    +-   +-   +-   +  
 w   3  3   32114 -0.017127    1( b1u)   1( b1u)        +    +-   +-   +-   +-   +  
 w   3  3   32115 -0.015172    1( b1u)   2( b1u)        +    +-   +-   +-   +-   +  
 w   3  3   32129 -0.010535    1( b2u)   1( b2u)        +    +-   +-   +-   +-   +  
 w   3  3   32144 -0.017128    1( b3u)   1( b3u)        +    +-   +-   +-   +-   +  
 w   3  3   32145 -0.015337    1( b3u)   2( b3u)        +    +-   +-   +-   +-   +  

 ci coefficient statistics:
           rq > 0.1                5
      0.1> rq > 0.01              96
     0.01> rq > 0.001           3622
    0.001> rq > 0.0001          7661
   0.0001> rq > 0.00001         9479
  0.00001> rq > 0.000001        8799
 0.000001> rq                   2496
           all                 32158
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:         126 2x:           0 4x:           0
All internal counts: zz :          15 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :          12 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:         193
  iref  icsf         v(icsf)             hv(icsf)
     1     1     -0.566138681665    111.054749786003
     2     2     -0.486907914068     95.512473000171

 number of reference csfs (nref) is     2.  root number (iroot) is  5.
 c0**2 =   0.55759232  c**2 (all zwalks) =   0.93978230

 pople ci energy extrapolation is computed with 12 correlated electrons.

 eref      =   -196.161539518075   "relaxed" cnot**2         =   0.557592323659
 eci       =   -196.464481159765   deltae = eci - eref       =  -0.302941641690
 eci+dv1   =   -196.598504867531   dv1 = (1-cnot**2)*deltae  =  -0.134023707767
 eci+dv2   =   -196.704842624728   dv2 = dv1 / cnot**2       =  -0.240361464963
 eci+dv3   =   -197.628036467585   dv3 = dv1 / (2*cnot**2-1) =  -1.163555307820
 eci+pople =   -196.790941861666   ( 12e- scaled deltae )    =  -0.629402343591


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 6) =      -196.4644811588

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     2    3    4   11   14   17

                                         symmetry   ag   ag   ag   b1g  b2g  b3g

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1  0.235372                        +-   +-   +-   +-   +-   +     - 
 z*  3  1       2 -0.372604                        +-   +-   +-   +-   +    +-   +  
 z   3  2       3  0.607977                        +-   +-   +-   +    +-   +-   +  
 z   3  3       4 -0.385784                        +-   +-   +    +-   +-   +-   +  
 z   3  3       5  0.449504                        +-   +    +-   +-   +-   +-   +  
 z   3  3       6  0.158199                        +    +-   +-   +-   +-   +-   +  
 y   3  2     107  0.012632              1( b1g)   +-   +-   +-        +-   +-   +  
 y   3  2     143 -0.013721              1( ag )   +-   +-    -   +    +-   +-   +  
 y   3  3     193  0.010609              1( ag )   +-   +-        +-   +-   +-   +  
 y   3  3     209  0.010662              1( b3g)   +-    -   +-   +-   +-   +    +  
 y   3  3     291  0.012386              3( ag )   +-   +     -   +-   +-   +-   +  
 y   3  2     335 -0.012805              1( ag )    -   +-   +-   +    +-   +-   +  
 x   3  2    1659 -0.010576    1( b2g)   1( b3g)   +-   +-   +-   +     -    -   +  
 x   3  2    6301  0.012397    3( ag )   1( b2g)   +-    -   +-   +     -   +-   +  
 x   3  2    6309  0.010412    5( ag )   2( b2g)   +-    -   +-   +     -   +-   +  
 x   3  2    6338  0.010993    4( b1u)   4( b3u)   +-    -   +-   +     -   +-   +  
 x   3  2   10954 -0.010291    2( ag )   1( b3g)    -   +-   +-   +    +-    -   +  
 x   3  2   11877 -0.010059    1( ag )   2( ag )    -   +-    -   +    +-   +-   +  
 w   3  2   17726  0.012142    1( b3g)   1( b3g)   +-   +-   +-   +    +-        +  
 w   3  2   17727 -0.010526    1( b3g)   2( b3g)   +-   +-   +-   +    +-        +  
 w   3  2   17728  0.010349    2( b3g)   2( b3g)   +-   +-   +-   +    +-        +  
 w   3  2   18365  0.012148    1( b2g)   1( b2g)   +-   +-   +-   +         +-   +  
 w   3  2   18366 -0.010528    1( b2g)   2( b2g)   +-   +-   +-   +         +-   +  
 w   3  2   18367  0.010348    2( b2g)   2( b2g)   +-   +-   +-   +         +-   +  
 w   3  1   21123  0.014492    1( b2u)   1( b2u)   +-   +-        +-   +    +-   +  
 w   3  1   21124  0.014583    1( b2u)   2( b2u)   +-   +-        +-   +    +-   +  
 w   3  1   21125  0.010296    2( b2u)   2( b2u)   +-   +-        +-   +    +-   +  
 w   3  2   21368 -0.015033    1( b1u)   1( b1u)   +-   +-        +    +-   +-   +  
 w   3  2   21369 -0.010786    1( b1u)   2( b1u)   +-   +-        +    +-   +-   +  
 w   3  2   21383 -0.024163    1( b2u)   1( b2u)   +-   +-        +    +-   +-   +  
 w   3  2   21384 -0.024315    1( b2u)   2( b2u)   +-   +-        +    +-   +-   +  
 w   3  2   21385 -0.017051    2( b2u)   2( b2u)   +-   +-        +    +-   +-   +  
 w   3  2   21398 -0.014920    1( b3u)   1( b3u)   +-   +-        +    +-   +-   +  
 w   3  2   21399 -0.010296    1( b3u)   2( b3u)   +-   +-        +    +-   +-   +  
 w   3  1   23706  0.010172    1( b3u)   1( b3u)   +-   +     -   +-   +    +-   +  
 w   3  2   23951 -0.014542    1( b2u)   1( b2u)   +-   +     -   +    +-   +-   +  
 w   3  2   23952 -0.011916    1( b2u)   2( b2u)   +-   +     -   +    +-   +-   +  
 w   3  2   23966 -0.016851    1( b3u)   1( b3u)   +-   +     -   +    +-   +-   +  
 w   3  2   23967 -0.015006    1( b3u)   2( b3u)   +-   +     -   +    +-   +-   +  
 w   3  3   24838  0.015016    1( b1u)   1( b1u)   +-   +         +-   +-   +-   +  
 w   3  3   24853  0.023962    1( b2u)   1( b2u)   +-   +         +-   +-   +-   +  
 w   3  3   24854  0.022815    1( b2u)   2( b2u)   +-   +         +-   +-   +-   +  
 w   3  3   24855  0.015268    2( b2u)   2( b2u)   +-   +         +-   +-   +-   +  
 w   3  3   24868  0.019072    1( b3u)   1( b3u)   +-   +         +-   +-   +-   +  
 w   3  3   24869  0.014937    1( b3u)   2( b3u)   +-   +         +-   +-   +-   +  
 w   3  2   25408 -0.011555    3( ag )   3( ag )   +-        +-   +    +-   +-   +  
 w   3  2   25465 -0.012223    1( b3u)   2( b3u)   +-        +-   +    +-   +-   +  
 w   3  2   25466 -0.010051    2( b3u)   2( b3u)   +-        +-   +    +-   +-   +  
 w   3  3   25709  0.010423    1( b2u)   1( b2u)   +-        +    +-   +-   +-   +  
 w   3  3   25724  0.014908    1( b3u)   1( b3u)   +-        +    +-   +-   +-   +  
 w   3  3   25725  0.015239    1( b3u)   2( b3u)   +-        +    +-   +-   +-   +  
 w   3  3   25726  0.010675    2( b3u)   2( b3u)   +-        +    +-   +-   +-   +  
 w   3  1   27956  0.015436    1( b1u)   1( b1u)   +    +-    -   +-   +    +-   +  
 w   3  1   27957  0.013110    1( b1u)   2( b1u)   +    +-    -   +-   +    +-   +  
 w   3  1   27971  0.013003    1( b2u)   1( b2u)   +    +-    -   +-   +    +-   +  
 w   3  1   27986  0.012704    1( b3u)   1( b3u)   +    +-    -   +-   +    +-   +  
 w   3  2   28216 -0.024312    1( b1u)   1( b1u)   +    +-    -   +    +-   +-   +  
 w   3  2   28217 -0.020505    1( b1u)   2( b1u)   +    +-    -   +    +-   +-   +  
 w   3  2   28218 -0.012108    2( b1u)   2( b1u)   +    +-    -   +    +-   +-   +  
 w   3  2   28231 -0.022178    1( b2u)   1( b2u)   +    +-    -   +    +-   +-   +  
 w   3  2   28232 -0.016598    1( b2u)   2( b2u)   +    +-    -   +    +-   +-   +  
 w   3  2   28246 -0.020668    1( b3u)   1( b3u)   +    +-    -   +    +-   +-   +  
 w   3  2   28247 -0.015092    1( b3u)   2( b3u)   +    +-    -   +    +-   +-   +  
 w   3  3   29118  0.014938    1( b1u)   1( b1u)   +    +-        +-   +-   +-   +  
 w   3  3   29119  0.012181    1( b1u)   2( b1u)   +    +-        +-   +-   +-   +  
 w   3  3   29133  0.016361    1( b2u)   1( b2u)   +    +-        +-   +-   +-   +  
 w   3  3   29134  0.013797    1( b2u)   2( b2u)   +    +-        +-   +-   +-   +  
 w   3  3   29148  0.013027    1( b3u)   1( b3u)   +    +-        +-   +-   +-   +  
 w   3  1   29484  0.010300    1( b3u)   1( b3u)   +     -   +-   +-   +    +-   +  
 w   3  1   29485  0.010038    1( b3u)   2( b3u)   +     -   +-   +-   +    +-   +  
 w   3  2   29744 -0.016643    1( b3u)   1( b3u)   +     -   +-   +    +-   +-   +  
 w   3  2   29745 -0.016119    1( b3u)   2( b3u)   +     -   +-   +    +-   +-   +  
 w   3  2   29746 -0.010611    2( b3u)   2( b3u)   +     -   +-   +    +-   +-   +  
 w   3  3   29974  0.016215    1( b1u)   1( b1u)   +     -   +    +-   +-   +-   +  
 w   3  3   29975  0.011589    1( b1u)   2( b1u)   +     -   +    +-   +-   +-   +  
 w   3  3   29989  0.016400    1( b2u)   1( b2u)   +     -   +    +-   +-   +-   +  
 w   3  3   29990  0.011807    1( b2u)   2( b2u)   +     -   +    +-   +-   +-   +  
 w   3  3   30004  0.020424    1( b3u)   1( b3u)   +     -   +    +-   +-   +-   +  
 w   3  3   30005  0.017744    1( b3u)   2( b3u)   +     -   +    +-   +-   +-   +  
 w   3  3   30006  0.010708    2( b3u)   2( b3u)   +     -   +    +-   +-   +-   +  
 w   3  3   30830 -0.013645    1( b1u)   1( b1u)   +    +     -   +-   +-   +-   +  
 w   3  3   30831 -0.012172    1( b1u)   2( b1u)   +    +     -   +-   +-   +-   +  
 w   3  3   30845 -0.010203    1( b2u)   1( b2u)   +    +     -   +-   +-   +-   +  
 w   3  3   30860 -0.010028    1( b3u)   1( b3u)   +    +     -   +-   +-   +-   +  
 w   3  1   31380  0.012419    1( b1u)   1( b1u)        +-   +-   +-   +    +-   +  
 w   3  1   31381  0.012698    1( b1u)   2( b1u)        +-   +-   +-   +    +-   +  
 w   3  2   31640 -0.019747    1( b1u)   1( b1u)        +-   +-   +    +-   +-   +  
 w   3  2   31641 -0.020178    1( b1u)   2( b1u)        +-   +-   +    +-   +-   +  
 w   3  2   31642 -0.014433    2( b1u)   2( b1u)        +-   +-   +    +-   +-   +  
 w   3  2   31655 -0.010538    1( b2u)   1( b2u)        +-   +-   +    +-   +-   +  
 w   3  2   31670 -0.014179    1( b3u)   1( b3u)        +-   +-   +    +-   +-   +  
 w   3  2   31671 -0.011371    1( b3u)   2( b3u)        +-   +-   +    +-   +-   +  
 w   3  3   31900  0.016784    1( b1u)   1( b1u)        +-   +    +-   +-   +-   +  
 w   3  3   31901  0.016330    1( b1u)   2( b1u)        +-   +    +-   +-   +-   +  
 w   3  3   31902  0.011233    2( b1u)   2( b1u)        +-   +    +-   +-   +-   +  
 w   3  3   31915  0.011387    1( b2u)   1( b2u)        +-   +    +-   +-   +-   +  
 w   3  3   31930  0.012342    1( b3u)   1( b3u)        +-   +    +-   +-   +-   +  
 w   3  3   32114 -0.013032    1( b1u)   1( b1u)        +    +-   +-   +-   +-   +  
 w   3  3   32115 -0.014139    1( b1u)   2( b1u)        +    +-   +-   +-   +-   +  
 w   3  3   32116 -0.010552    2( b1u)   2( b1u)        +    +-   +-   +-   +-   +  

 ci coefficient statistics:
           rq > 0.1                6
      0.1> rq > 0.01              94
     0.01> rq > 0.001           3998
    0.001> rq > 0.0001          7945
   0.0001> rq > 0.00001         9461
  0.00001> rq > 0.000001        8881
 0.000001> rq                   1773
           all                 32158
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:         126 2x:           0 4x:           0
All internal counts: zz :          15 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :          12 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         536    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:         193
  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.235371909286    -46.171866647832
     2     2     -0.372604339955     73.091589093388

 number of reference csfs (nref) is     2.  root number (iroot) is  6.
 c0**2 =   0.19423393  c**2 (all zwalks) =   0.93977915

 warning: cnot**2 < 0.5, dv3 is not computed.

 warning: pople ci energy extrapolation is not computed because of small fac.
 fac =      -0.611532140331

 eref      =   -196.164510243389   "relaxed" cnot**2         =   0.194233929834
 eci       =   -196.464481158802   deltae = eci - eref       =  -0.299970915413
 eci+dv1   =   -196.706187544479   dv1 = (1-cnot**2)*deltae  =  -0.241706385676
 eci+dv2   =   -197.708889825621   dv2 = dv1 / cnot**2       =  -1.244408666819
 eci+dv3   =   -196.464481158802   dv3 = dv1 / (2*cnot**2-1) =   0.000000000000
 eci+pople =   -196.164510243389   ( 12e- scaled deltae )    =   0.000000000000
 passed aftci ... 
