 total ao core energy =    0.000000000
 MCSCF calculation performed for  4 DRTs.

 DRT  first state   no.of aver. states   weights
  1   ground state          3             0.167 0.167 0.167
  2   ground state          1             0.167
  3   ground state          1             0.167
  4   ground state          1             0.167

 DRT file header:
  title                                                                          
 Molecular symmetry group:    ag 
 Total number of electrons:    19
 Spin multiplicity:            2
 Number of active orbitals:    6
 Number of active electrons:  11
 Total number of CSFs:         3

 DRT file header:
  title                                                                          
 Molecular symmetry group:    b1g
 Total number of electrons:    19
 Spin multiplicity:            2
 Number of active orbitals:    6
 Number of active electrons:  11
 Total number of CSFs:         1

 DRT file header:
  title                                                                          
 Molecular symmetry group:    b2g
 Total number of electrons:    19
 Spin multiplicity:            2
 Number of active orbitals:    6
 Number of active electrons:  11
 Total number of CSFs:         1

 DRT file header:
  title                                                                          
 Molecular symmetry group:    b3g
 Total number of electrons:    19
 Spin multiplicity:            2
 Number of active orbitals:    6
 Number of active electrons:  11
 Total number of CSFs:         1

 Number of active-double rotations:         3
 Number of active-active rotations:         0
 Number of double-virtual rotations:       21
 Number of active-virtual rotations:       24
 
 iter     emc (average)    demc       wnorm      knorm      apxde  qcoupl
    1   -130.7018923752  1.307E+02  5.066E+00  1.764E-01  4.196E-01  F   *not conv.*     
    2   -163.2602964502  3.256E+01  1.385E+00  9.080E-02  4.424E-02  F   *not conv.*     
    3   -196.0632319046  3.280E+01  1.874E+00  1.054E-01  8.043E-02  F   *not conv.*     
    4   -196.1453191690  8.209E-02  4.662E-02  1.222E-02  1.820E-04  F   *not conv.*     
    5   -196.1455018692  1.827E-04  4.197E-04  7.198E-05  8.027E-09  F   *not conv.*     

 final mcscf convergence values:
    6   -196.1455018772  8.027E-09  1.330E-07  4.988E-09  1.576E-17  T   *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 wt 0.167 total energy=     -196.163092170, rel. (eV)=   0.000000
   DRT #1 state # 2 wt 0.167 total energy=     -196.163092170, rel. (eV)=   0.000000
   DRT #1 state # 3 wt 0.167 total energy=     -196.057550413, rel. (eV)=   2.871939
   DRT #2 state # 1 wt 0.167 total energy=     -196.163092170, rel. (eV)=   0.000000
   DRT #3 state # 1 wt 0.167 total energy=     -196.163092170, rel. (eV)=   0.000000
   DRT #4 state # 1 wt 0.167 total energy=     -196.163092170, rel. (eV)=   0.000000
   ------------------------------------------------------------


