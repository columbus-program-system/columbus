1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      2009-03.    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


================ Computing sorting integral file structure ================

                    -----z----- -----y----- -----x----- -----w----- ---total---

                CSFs         6         432       16740       14980       32158
      internal walks        24         144         360         280         808
valid internal walks         6         144         360         280         790
 getinfoarray: info=                     6 :                     1
                  8192                  6552                  8192
                  5460                     0
 icd(3)=                  5356 ci%nnlev=                   595  l2rec=
                  8192  n2max=                  5460
 lcore1,lcore2=             131030557             131007241
 lencor,maxblo             131072000                 60000
========================================
 current settings:
 minbl3         156
 minbl4         225
 locmaxbl3     1424
 locmaxbuf      712
 maxbl3       60000
 maxbl3       60000
 maxbl4       60000
 maxbuf       30006
========================================

 sorted 4-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)

 sorted 3-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)
 Orig.  diagonal integrals:  1electron:        34
                             0ext.    :        42
                             2ext.    :       336
                             4ext.    :       812


 Orig. off-diag. integrals:  4ext.    :     12615
                             3ext.    :      9360
                             2ext.    :      3708
                             1ext.    :       576
                             0ext.    :        99
                             2ext. SO :        12
                             1ext. SO :        48
                             0ext. SO :       138
                             1electron:        76


 Sorted integrals            3ext.  w :      8712 x :      8064
                             4ext.  w :     10563 x :      8841


Cycle #  1 sortfile size=    131068(       4 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core 131030557
Cycle #  2 sortfile size=    131068(       4 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core 131030557
 minimum size of srtscr:     65534 WP (     2 records)
 maximum size of srtscr:    131068 WP (     4 records)
diagi   file:      4 records  of   1536 WP each=>       6144 WP total
ofdgi   file:      9 records  of   1536 WP each=>      13824 WP total
fil3w   file:      1 records  of  30006 WP each=>      30006 WP total
fil3x   file:      1 records  of  30006 WP each=>      30006 WP total
fil4w   file:      1 records  of  30006 WP each=>      30006 WP total
fil4x   file:      1 records  of  30006 WP each=>      30006 WP total
  detected spin-orbit CI calculation ...
 compressed index vector length=                    21
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 0,
  GSET = 0,
   DAVCOR =10,
  NCOREL = 11
  NROOT = 6
  IVMODE = 3
  NBKITR = 1
  NVBKMN = 6
  RTOLBK = 1e-4,1e-4,1e-4,1e-4,1e-4,1e-4,
  NITER = 120
  NVCIMN = 8
  RTOLCI = 1e-4,1e-4,1e-4,1e-4,1e-4,1e-4,
  NVCIMX = 16
  NVRFMX = 16
  NVBKMX = 16
  update_mode=10
  IDEN  = 1
  CSFPRN = 10,
 /&end
 ------------------------------------------------------------------------
 Detected spin-orbit CI calculation ...
 Disabling tasklist usage (with_tsklst=0)!
lodens (list->root)=  6
invlodens (root->list)= -1 -1 -1 -1 -1  1
 USING SEGMENTS OF EQUAL SIZE

****************  list of control variables  ****************
 lvlprt =    0      nroot  =    6      noldv  =   0      noldhv =   0
 nunitv =    8      nbkitr =    1      niter  = 120      davcor =  10
 csfprn =   10      ivmode =    3      istrt  =   0      vout   =   0
 iortls =    0      nvbkmx =   16      ibktv  =  -1      ibkthv =  -1
 nvcimx =   16      icitv  =   -1      icithv =  -1      frcsub =   0
 nvbkmn =    6      nvcimn =    8      maxseg = 300      nrfitr =  30
 ncorel =   11      nvrfmx =   16      nvrfmn =   8      iden   =   1
 itran  =    0      froot  =    0      rtmode =   0      ncouple=   1
 skipso =    F      dalton2=    0      molcas =   0      finalv =   0
 finalw =    0      cosmocalc=   0    with_tsklst=   0
 nsegwx =    1     1     1     1
 nseg0x =    1     1     1     1
 nseg1x =    1     1     1     1
 nseg2x =    1     1     1     1
 nseg3x =    1     1     1     1
 nseg4x =    1     1     1     1
 no0ex  =      0    no1ex  =      0    no2ex  =     0    no3ex  =     0
 no4ex  =      0    nodiag =      0
 cdg4ex =    1      c3ex1ex=    1      c2ex0ex=   1
 fileloc=    0     0     0     0     0     0     0     1     1     1
 directhd=   1      noaqccshift_zyxw=      0
 critical_crit=-1.00000    critical_delta= 0.05000

 ctol   = 0.010000    lrtshift=1.000000    smalld =0.001000


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-04    1.000E-04
    2        1.000E-04    1.000E-04
    3        1.000E-04    1.000E-04
    4        1.000E-04    1.000E-04
    5        1.000E-04    1.000E-04
    6        1.000E-04    1.000E-04
 Computing density:                    .drt1.state6
 using                      1  nodes and                      1  cores.
 szdg/szodg per processor=                  6144                 13824
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core          131071999 DP per process

********** Integral sort section *************


 workspace allocation information: lencor= 131071999

 echo of the input for program cisrt:
 ------------------------------------------------------------------------
  &input
  maxbl3=60000
  maxbl4=60000
  &end
 ------------------------------------------------------------------------
 
 ( 6) listing file:                    ciudgls             
 ( 5) input file:                      cisrtin   
 (17) cidrt file:                      cidrtfl             
 (11) transformed integrals file:      moints    
 (12) diagonal integral file:          diagint             
 (13) off-diagonal integral file:      ofdgint             
 (31) 4-external w integrals file:     fil4w               
 (32) 4-external x integrals file:     fil4x               
 (33) 3-external w integrals file:     fil3w               
 (34) 3-external x integrals file:     fil3x               
 (21) scratch da sorting file:         srtscr              
 (12) 2-e integral file [fsplit=2]:    moints2   

 input integral file header information:
 Cu atom                                                                         
 aoints SIFS file created by argos.      zam792            14:38:58.196 03-Nov-14
  cidrt_title                                                                    
 MO-coefficients from mcscf.x                                                    
  with dummy occupation 1.0 for active orbitals                                  
  total ao core energy =    0.000000000                                          
 MCSCF energy =    -196.145501877                                                
 SIFS file created by program tran.      zam792            14:38:58.446 03-Nov-14

 input energy(*) values:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total core energy =   0.000000000000E+00

 nsym = 8 nmot=  34

 symmetry  =    1    2    3    4    5    6    7    8
 slabel(*) =   ag  b1g  b2g  b3g   au  b1u  b2u  b3u
 nmpsy(*)  =    9    3    3    3    1    5    5    5

 info(*) =          1      8192      6552      8192      5460         0

 orbital labels, i:molab(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012  13:tout:013  14:tout:014  15:tout:015  16:tout:016  17:tout:017  18:tout:018  19:tout:019  20:tout:020
  21:tout:021  22:tout:022  23:tout:023  24:tout:024  25:tout:025  26:tout:026  27:tout:027  28:tout:028  29:tout:029  30:tout:030
  31:tout:031  32:tout:032  33:tout:033  34:tout:034

 input parameters:
 prnopt=  0
 ldamin=    4095 ldamax=   32767 ldainc=      64
 maxbuf=   30006 maxbl3=   60000 maxbl4=   60000 intmxo=    1536
  Using 32 bit compression 

 drt information:
  cidrt_title                                                                    
 nmotd =  38 nfctd =   4 nfvtc =   0 nmot  =  34
 nlevel =  34 niot  =   6 lowinl=  29
 orbital-to-level map(*)
   -1  29  30  31   1   2   3   4   5   6  32   7   8  33   9  10  34  11  12  13
   -1  14  15  16  17  18  -1  19  20  21  22  23  -1  24  25  26  27  28
 compressed map(*)
   29  30  31   1   2   3   4   5   6  32   7   8  33   9  10  34  11  12  13  14
   15  16  17  18  19  20  21  22  23  24  25  26  27  28
 levsym(*)
    1   1   1   1   1   1   2   2   3   3   4   4   5   6   6   6   6   6   7   7
    7   7   7   8   8   8   8   8   1   1   1   2   3   4
 repartitioning mu(*)=
   0.  0.  0.  0.  0.  0.

 new core energy added to the energy(*) list.
 from the integral file: h1_core= -1.458883147958E+02

 indxdg: diagonal integral statistics.
 total number of integrals contributing to diagonal matrix elements:      1190
 number with all external indices:       812
 number with half external - half internal indices:       336
 number with all internal indices:        42

 indxof: off-diagonal integral statistics.
    4-external integrals: num=      12615 strt=          1
    3-external integrals: num=       9360 strt=      12616
    2-external integrals: num=       3708 strt=      21976
    1-external integrals: num=        576 strt=      25684
    0-external integrals: num=         99 strt=      26260

 total number of off-diagonal integrals:       26358


 indxof(2nd)  ittp=   3 numx(ittp)=        3708
 indxof(2nd)  ittp=   4 numx(ittp)=         576
 indxof(2nd)  ittp=   5 numx(ittp)=          99

 indxof: spin-orbit integral statistics.
    0-external so integrals: num=         12 strt=      26359
    1-external so integrals: num=         48 strt=      26371
    2-external so integrals: num=        138 strt=      26419

 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg= 130894995
 pro2e     1786    2381    2976    3571    4166    4187    4208    4803   48491   92179
   124946  133138  138598  160437

 pro2e:     23309 integrals read in     5 records.

 pro2e:         0 integrals 34-ext integrals skipped.
 pro1e     1786    2381    2976    3571    4166    4187    4208    4803   48491   92179
   124946  133138  138598  160437
 pro1e: eref =    0.000000000000000E+00
 total size of srtscr:                     4  records of                  32767 
 WP =               1048544 Bytes
 putdg     1786    2381    2976    3571    5107   37874   59719    4803   48491   92179
   124946  133138  138598  160437

 putf:       4 buffers of length    1536 written to file 12
 diagonal integral file completed.

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep4e:    53 blocks of linear combinations of 4-external integrals processed.
 number of sorted 4-external integrals      19404
 number of original 4-external integrals    12615


 putf34: external integral file complete. nfilw=    31 nfilx=    32 nrecw=     1 nrecx=     1 lbufp= 30006

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep3e:    43 blocks of linear combinations of 3-external integrals processed.
 number of sorted 3-external integrals      16776
 number of original 3-external integrals     9360


 putf34: external integral file complete. nfilw=    33 nfilx=    34 nrecw=     1 nrecx=     1 lbufp= 30006
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      3708         3     21976     21976      3708     26556
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd       576         4     25684     25684       576     26556
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd        99         5     26260     26260        99     26556
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd        12         6     26359     26359        12     26556
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd        48         7     26371     26371        48     26556
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd       138         8     26419     26419       138     26556

 putf:       9 buffers of length    1536 written to file 13
 off-diagonal files sort completed.
 executing brd_struct for cisrtinfo
cisrtinfo:
bufszi  1536
 diagfile 4ext:     812 2ext:     336 0ext:      42
 fil4w,fil4x  :   12615 fil3w,fil3x :    9360
 ofdgint  2ext:    3708 1ext:     576 0ext:      99so0ext:      12so1ext:      48so2ext:     138
buffer minbl4     225 minbl3     156 maxbl2     228nbas:   6   2   2   2   1   5   5   5 maxbuf 30006
 CIUDG version 5.9.7 ( 5-Oct-2004)

 workspace allocation information: lcore= 131071999

 core energy values from the integral file:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -1.458883147958E+02, ietype=    6,   fcore energy of type: H1(*)   

 total core repulsion energy = -1.458883147958E+02
 bummer (warning): calculation: setting iden=0                      0
 nmot  =    38 niot  =     7 nfct  =     4 nfvt  =     0
 nrow  =    39 nsym  =     8 ssym  =     1 lenbuf=  1600
 nwalk,xbar:        808       24      144      360      280
 nvalwt,nvalw:      790        6      144      360      280
 ncsft:           32158
 total number of valid internal walks:     790
 nvalz,nvaly,nvalx,nvalw =        6     144     360     280

 cisrt info file parameters:
 file number  12 blocksize   1536
 mxbld   1536
 nd4ext,nd2ext,nd0ext   812   336    42
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int    12615     9360     3708      576       99       12       48      138
 minbl4,minbl3,maxbl2   225   156   228
 maxbuf 30006
 number of external orbitals per symmetry block:   6   2   2   2   1   5   5   5
 nmsym   8 number of internal orbitals   6
 executing brd_struct for drt
 executing brd_struct for orbinf
 executing brd_struct for momap
 calcthrxt: niot,maxw1=                     7                    66
 block size     0
 pthz,pthy,pthx,pthw:    24   144   360   280 total internal walks:     808
 maxlp3,n2lp,n1lp,n0lp    66     0     0     0
 orbsym(*)= 1 1 1 2 3 4

 setref:        4 references kept,
                8 references were marked as invalid, out of
               12 total.
 pdinf%rmuval=  0.000000000000000E+000  0.000000000000000E+000
  0.000000000000000E+000  0.000000000000000E+000  0.000000000000000E+000
  0.000000000000000E+000
 nmb.of records onel     1
 nmb.of records 2-ext     3
 nmb.of records 1-ext     1
 nmb.of records 0-ext     1
 nmb.of records 2-int     1
 nmb.of records 1-int     1
 nmb.of records 0-int     1
 ---------memory usage in DP -----------------
 < n-ex core usage >
     routines:
    fourex            61660
    threx             60409
    twoex              2552
    onex               1633
    allin              1536
    diagon             2109
               =======
   maximum            61660
 
  __ static summary __ 
   reflst                 6
   hrfspc                 6
               -------
   static->               6
 
  __ core required  __ 
   totstc                 6
   max n-ex           61660
               -------
   totnec->           61666
 
  __ core available __ 
   totspc         131071999
   totnec -           61666
               -------
   totvec->       131010333

 number of external paths / symmetry
 vertex x      48      46      46      46      36      52      52      52
 vertex w      76      46      46      46      36      52      52      52
segment: free space=   131010333
 reducing frespc by                  2541 to              131007792 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1          18|         6|         0|         6|         0|         1|
 -------------------------------------------------------------------------------
  Y 2         144|       432|         6|       144|         6|         2|
 -------------------------------------------------------------------------------
  X 3         360|     16740|       438|       360|       150|         3|
 -------------------------------------------------------------------------------
  W 4         280|     14980|     17178|       280|       510|         4|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=          24DP  conft+indsym=        1440DP  drtbuffer=        1077 DP

dimension of the ci-matrix ->>>     32158

 executing brd_struct for civct
 gentasklist: ntask=                    20
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS   
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1     360      18      16740          6     360       6
     2  4   1    25      two-ext wz   2X  4 1     280      18      14980          6     280       6
     3  4   3    26      two-ext wx*  WX  4 3     280     360      14980      16740     280     360
     4  4   3    27      two-ext wx+  WX  4 3     280     360      14980      16740     280     360
     5  2   1    11      one-ext yz   1X  2 1     144      18        432          6     144       6
     6  3   2    15      1ex3ex yx    3X  3 2     360     144      16740        432     360     144
     7  4   2    16      1ex3ex yw    3X  4 2     280     144      14980        432     280     144
     8  1   1     1      allint zz    OX  1 1      18      18          6          6       6       6
     9  2   2     5      0ex2ex yy    OX  2 2     144     144        432        432     144     144
    10  3   3     6      0ex2ex xx*   OX  3 3     360     360      16740      16740     360     360
    11  3   3    18      0ex2ex xx+   OX  3 3     360     360      16740      16740     360     360
    12  4   4     7      0ex2ex ww*   OX  4 4     280     280      14980      14980     280     280
    13  4   4    19      0ex2ex ww+   OX  4 4     280     280      14980      14980     280     280
    14  2   2    42      four-ext y   4X  2 2     144     144        432        432     144     144
    15  3   3    43      four-ext x   4X  3 3     360     360      16740      16740     360     360
    16  4   4    44      four-ext w   4X  4 4     280     280      14980      14980     280     280
    17  1   1    75      dg-024ext z  OX  1 1      18      18          6          6       6       6
    18  2   2    76      dg-024ext y  OX  2 2     144     144        432        432     144     144
    19  3   3    77      dg-024ext x  OX  3 3     360     360      16740      16740     360     360
    20  4   4    78      dg-024ext w  OX  4 4     280     280      14980      14980     280     280
----------------------------------------------------------------------------------------------------
REDTASK #   1 TIME=  19.000 N=  1 (task/type/sgbra)=(   1/24/0) (
REDTASK #   2 TIME=  18.000 N=  1 (task/type/sgbra)=(   2/25/0) (
REDTASK #   3 TIME=  17.000 N=  1 (task/type/sgbra)=(   3/26/1) (
REDTASK #   4 TIME=  16.000 N=  1 (task/type/sgbra)=(   4/27/2) (
REDTASK #   5 TIME=  15.000 N=  1 (task/type/sgbra)=(   5/11/0) (
REDTASK #   6 TIME=  14.000 N=  1 (task/type/sgbra)=(   6/15/0) (
REDTASK #   7 TIME=  13.000 N=  1 (task/type/sgbra)=(   7/16/0) (
REDTASK #   8 TIME=  12.000 N=  1 (task/type/sgbra)=(   8/ 1/0) (
REDTASK #   9 TIME=  11.000 N=  1 (task/type/sgbra)=(   9/ 5/0) (
REDTASK #  10 TIME=  10.000 N=  1 (task/type/sgbra)=(  10/ 6/1) (
REDTASK #  11 TIME=   9.000 N=  1 (task/type/sgbra)=(  11/18/2) (
REDTASK #  12 TIME=   8.000 N=  1 (task/type/sgbra)=(  12/ 7/1) (
REDTASK #  13 TIME=   7.000 N=  1 (task/type/sgbra)=(  13/19/2) (
REDTASK #  14 TIME=   6.000 N=  1 (task/type/sgbra)=(  14/42/1) (
REDTASK #  15 TIME=   5.000 N=  1 (task/type/sgbra)=(  15/43/1) (
REDTASK #  16 TIME=   4.000 N=  1 (task/type/sgbra)=(  16/44/1) (
REDTASK #  17 TIME=   3.000 N=  1 (task/type/sgbra)=(  17/75/1) (
REDTASK #  18 TIME=   2.000 N=  1 (task/type/sgbra)=(  18/76/1) (
REDTASK #  19 TIME=   1.000 N=  1 (task/type/sgbra)=(  19/77/1) (
REDTASK #  20 TIME=   0.000 N=  1 (task/type/sgbra)=(  20/78/1) (
 initializing v-file: 1:                 32158

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      6 vectors will be written to unit 11 beginning with logical record   1

            6 vectors will be created
 bummer (warning):strefv: reducing number of start vectors to nref                      4
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:         126 2x:           0 4x:           0
All internal counts: zz :          15 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :          12 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:           0    task #     2:           0    task #     3:           0    task #     4:           0
task #     5:           0    task #     6:           0    task #     7:           0    task #     8:          26
task #     9:           0    task #    10:           0    task #    11:           0    task #    12:           0
task #    13:           0    task #    14:           0    task #    15:           0    task #    16:           0
task #    17:         105    task #    18:           0    task #    19:           0    task #    20:           0
 reference space has dimension       4
 dsyevx: computed roots 1 to    4(converged:   4)

    root           eigenvalues
    ----           ------------
       1        -196.1662328324
       2        -196.1630921702
       3        -196.1599515074
       4        -196.0575504130

 strefv generated    4 initial ci vector(s).
 bummer (warning):startv: num req > num generated, ngen=                       4
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=                     6

         vector  1 from unit 11 written to unit 49 filename cirefv              
 ufvoutnew: ... writing  recamt=                     6

         vector  2 from unit 11 written to unit 49 filename cirefv              
 ufvoutnew: ... writing  recamt=                     6

         vector  3 from unit 11 written to unit 49 filename cirefv              
 ufvoutnew: ... writing  recamt=                     6

         vector  4 from unit 11 written to unit 49 filename cirefv              
 ufvoutnew: ... writing  recamt=                     6

         vector  5 from unit 11 written to unit 49 filename cirefv              
 ufvoutnew: ... writing  recamt=                     6

         vector  6 from unit 11 written to unit 49 filename cirefv              

 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=     6)...
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:             32158
 number of initial trial vectors:                         4
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:              16
 number of roots to converge:                             6
 number of iterations:                                    1
 residual norm convergence criteria:               0.000100  0.000100  0.000100  0.000100  0.000100  0.000100

          starting bk iteration   1

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:           0 xx:           0 ww:           0
One-external counts: yz :         471 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:         150 wz:         160 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :          12 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :         153 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:           0    task #     4:           0
task #     5:         512    task #     6:           0    task #     7:           0    task #     8:          26
task #     9:           0    task #    10:           0    task #    11:           0    task #    12:           0
task #    13:           0    task #    14:           0    task #    15:           0    task #    16:           0
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:           0 xx:           0 ww:           0
One-external counts: yz :         471 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:         150 wz:         160 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :          12 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :         153 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:           0    task #     4:           0
task #     5:         512    task #     6:           0    task #     7:           0    task #     8:          26
task #     9:           0    task #    10:           0    task #    11:           0    task #    12:           0
task #    13:           0    task #    14:           0    task #    15:           0    task #    16:           0
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:           0 xx:           0 ww:           0
One-external counts: yz :         471 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:         150 wz:         160 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :          12 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :         153 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:           0    task #     4:           0
task #     5:         512    task #     6:           0    task #     7:           0    task #     8:          26
task #     9:           0    task #    10:           0    task #    11:           0    task #    12:           0
task #    13:           0    task #    14:           0    task #    15:           0    task #    16:           0
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:           0 xx:           0 ww:           0
One-external counts: yz :         471 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:         150 wz:         160 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :          12 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :         153 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:           0    task #     4:           0
task #     5:         512    task #     6:           0    task #     7:           0    task #     8:          26
task #     9:           0    task #    10:           0    task #    11:           0    task #    12:           0
task #    13:           0    task #    14:           0    task #    15:           0    task #    16:           0
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4
   ht   1   -50.27791804
   ht   2     0.00000000   -50.27477737
   ht   3     0.00000000     0.00000000   -50.27163671
   ht   4     0.00000000     0.00000000     0.00000000   -50.16923562

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4
 ref    1   -1.00000       1.077531E-11   6.230155E-13  -2.775558E-17
 ref    2   1.077542E-11    1.00000      -3.519129E-12  -2.775558E-17
 ref    3  -6.225298E-13  -3.519185E-12   -1.00000      -2.775558E-17
 ref    4    0.00000        0.00000        0.00000        1.00000    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4
 ref    1    1.00000        1.00000        1.00000        1.00000    

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4

          Spin-Orbit CI: perturbational estimate of Spin-Orbit CI energies and wavefunctions 

                SO   1         SO   2         SO   3         SO   4

   energy  -196.16623283  -196.16309217  -196.15995151  -196.05755041
 
   NR   1    -1.00000000     0.00000000     0.00000000     0.00000000
   NR   2     0.00000000     1.00000000     0.00000000     0.00000000
   NR   3     0.00000000     0.00000000    -1.00000000     0.00000000
   NR   4     0.00000000     0.00000000     0.00000000     1.00000000

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1    -1.00000000     0.00000000     0.00000000     0.00000000
 ref:   2     0.00000000     1.00000000     0.00000000     0.00000000
 ref:   3     0.00000000     0.00000000    -1.00000000     0.00000000
 ref:   4     0.00000000     0.00000000     0.00000000     1.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -196.1662328324  7.8160E-14  4.0698E-01  1.7462E+00  1.0000E-04   
 mr-sdci #  1  2   -196.1630921702  2.1316E-14  0.0000E+00  1.7461E+00  1.0000E-04   
 mr-sdci #  1  3   -196.1599515074 -1.4211E-14  0.0000E+00  1.7459E+00  1.0000E-04   
 mr-sdci #  1  4   -196.0575504130  7.1054E-15  0.0000E+00  2.0130E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.004000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mr-sdci  convergence not reached after  1 iterations.

 warning: *** the expansion subspace is invariant before all roots are converged.
 generate additional expansion vectors and try again.***


 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -196.1662328324  7.8160E-14  4.0698E-01  1.7462E+00  1.0000E-04   
 mr-sdci #  1  2   -196.1630921702  2.1316E-14  0.0000E+00  1.7461E+00  1.0000E-04   
 mr-sdci #  1  3   -196.1599515074 -1.4211E-14  0.0000E+00  1.7459E+00  1.0000E-04   
 mr-sdci #  1  4   -196.0575504130  7.1054E-15  0.0000E+00  2.0130E+00  1.0000E-04   
 
 expansion vectors are not transformed.
 matrix-vector products are not transformed.

    5 expansion eigenvectors written to unit nvfile (= 11)
    4 matrix-vector products written to unit nhvfil (= 10)

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:             32158
 number of initial trial vectors:                         5
 number of initial matrix-vector products:                4
 maximum dimension of the subspace vectors:              16
 number of roots to converge:                             6
 number of iterations:                                  120
 residual norm convergence criteria:               0.000100  0.000100  0.000100  0.000100  0.000100  0.000100

          starting ci iteration   1

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1   -50.27791804
   ht   2     0.00000000   -50.27477737
   ht   3     0.00000000     0.00000000   -50.27163671
   ht   4     0.00000000     0.00000000     0.00000000   -50.16923562
   ht   5     0.40697916     0.00304100    -0.00564540     0.00115654    -4.32256226

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1  -0.976827       7.395931E-03   1.358453E-02  -2.096097E-03   0.213461    
 ref    2  -7.219587E-03  -0.999973       2.030487E-04  -1.612836E-05   1.595840E-03
 ref    3   1.325849E-02   1.025812E-04   0.999908       3.085952E-05  -2.964127E-03
 ref    4  -2.010844E-03  -6.253939E-07  -2.367517E-06   0.999998       6.178307E-04

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1   0.954423        1.00000        1.00000        1.00000       4.557714E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5

          Spin-Orbit CI: perturbational estimate of Spin-Orbit CI energies and wavefunctions 

                SO   1         SO   2         SO   3         SO   4         SO   5

   energy  -196.45188390  -196.16309234  -196.15995269  -196.05755106  -190.18441961
 
   NR   1    -0.97682698     0.00739593     0.01358453    -0.00209610     0.21346059
   NR   2    -0.00721959    -0.99997264     0.00020305    -0.00001613     0.00159584
   NR   3     0.01325849     0.00010258     0.99990770     0.00003086    -0.00296413
   NR   4    -0.00201084    -0.00000063    -0.00000237     0.99999779     0.00061783
   NR   5     0.68561661     0.00005707     0.00020962    -0.00055975     3.13746133

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1    -0.97682698     0.00739593     0.01358453    -0.00209610     0.21346059
 ref:   2    -0.00721959    -0.99997264     0.00020305    -0.00001613     0.00159584
 ref:   3     0.01325849     0.00010258     0.99990770     0.00003086    -0.00296413
 ref:   4    -0.00201084    -0.00000063    -0.00000237     0.99999779     0.00061783

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -196.4518839047  2.8565E-01  0.0000E+00  3.4761E-01  1.0000E-04   
 mr-sdci #  1  2   -196.1630923437  1.7356E-07  4.2561E-01  1.7461E+00  1.0000E-04   
 mr-sdci #  1  3   -196.1599526909  1.1835E-06  0.0000E+00  1.7459E+00  1.0000E-04   
 mr-sdci #  1  4   -196.0575510603  6.4738E-07  0.0000E+00  2.0130E+00  1.0000E-04   
 mr-sdci #  1  5   -190.1844196114  4.4296E+01  0.0000E+00  4.4481E+00  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.005000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   2

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1   -50.27791804
   ht   2     0.00000000   -50.27477737
   ht   3     0.00000000     0.00000000   -50.27163671
   ht   4     0.00000000     0.00000000     0.00000000   -50.16923562
   ht   5     0.40697916     0.00304100    -0.00564540     0.00115654    -4.32256226
   ht   6     0.00000994     0.42561823    -0.00304852    -0.00068646    -0.15499563  -748.33416178

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.976828      -5.712034E-03   1.353326E-02   2.124192E-03  -4.633243E-03   0.213460    
 ref    2   7.097292E-03   0.766424       7.095226E-03  -3.836972E-03   0.642243       1.559091E-03
 ref    3  -1.325764E-02  -5.235144E-03   0.999883      -2.828321E-06  -4.645209E-03  -2.963862E-03
 ref    4   2.010991E-03  -5.844344E-04  -2.017688E-06  -0.999984      -5.300496E-03   6.178907E-04

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.954423       0.587467        1.00000       0.999986       0.412547       4.557697E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.97682796    -0.00571203     0.01353326     0.00212419    -0.00463324     0.21346047
 ref:   2     0.00709729     0.76642432     0.00709523    -0.00383697     0.64224286     0.00155909
 ref:   3    -0.01325764    -0.00523514     0.99988323    -0.00000283    -0.00464521    -0.00296386
 ref:   4     0.00201099    -0.00058443    -0.00000202    -0.99998357    -0.00530050     0.00061789

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1   -196.4518839340  2.9325E-08  0.0000E+00  3.4761E-01  1.0000E-04   
 mr-sdci #  2  2   -196.2555014607  9.2409E-02  0.0000E+00  1.4830E+00  1.0000E-04   
 mr-sdci #  2  3   -196.1599528414  1.5054E-07  4.0386E-01  1.7459E+00  1.0000E-04   
 mr-sdci #  2  4   -196.0575517250  6.6463E-07  0.0000E+00  2.0130E+00  1.0000E-04   
 mr-sdci #  2  5   -196.0314963654  5.8471E+00  0.0000E+00  2.0490E+00  1.0000E-04   
 mr-sdci #  2  6   -190.1843960433  4.4296E+01  0.0000E+00  4.4481E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.006000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   3

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1   -50.27791804
   ht   2     0.00000000   -50.27477737
   ht   3     0.00000000     0.00000000   -50.27163671
   ht   4     0.00000000     0.00000000     0.00000000   -50.16923562
   ht   5     0.40697916     0.00304100    -0.00564540     0.00115654    -4.32256226
   ht   6     0.00000994     0.42561823    -0.00304852    -0.00068646    -0.15499563  -748.33416178
   ht   7     0.00003408     0.00010442    -0.40391504     0.00115068    -0.26556122    -1.79699439   -54.63684244

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.976868       9.443767E-03   5.658312E-03   2.094825E-03   4.589501E-03  -4.407910E-03  -0.213454    
 ref    2   7.104351E-03   1.612042E-03  -0.766441      -3.875226E-03  -0.642259      -9.273420E-04  -1.561008E-03
 ref    3  -1.020045E-02   0.878500       1.225339E-03  -2.230821E-03   1.327915E-03  -0.477620       2.087157E-03
 ref    4   2.004619E-03  -1.665785E-03   5.898902E-04  -0.999981       5.346893E-03   1.577564E-03  -6.153257E-04

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.954430       0.771857       0.587466       0.999986       0.412548       0.228144       4.556977E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.97686812     0.00944377     0.00565831     0.00209483     0.00458950    -0.00440791    -0.21345397
 ref:   2     0.00710435     0.00161204    -0.76644140    -0.00387523    -0.64225867    -0.00092734    -0.00156101
 ref:   3    -0.01020045     0.87850013     0.00122534    -0.00223082     0.00132792    -0.47762001     0.00208716
 ref:   4     0.00200462    -0.00166579     0.00058989    -0.99998070     0.00534689     0.00157756    -0.00061533

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1   -196.4518854476  1.5136E-06  0.0000E+00  3.4754E-01  1.0000E-04   
 mr-sdci #  3  2   -196.3695407963  1.1404E-01  0.0000E+00  9.7367E-01  1.0000E-04   
 mr-sdci #  3  3   -196.2555005120  9.5548E-02  0.0000E+00  1.4830E+00  1.0000E-04   
 mr-sdci #  3  4   -196.0575523726  6.4760E-07  7.2933E-01  2.0130E+00  1.0000E-04   
 mr-sdci #  3  5   -196.0314982328  1.8674E-06  0.0000E+00  2.0490E+00  1.0000E-04   
 mr-sdci #  3  6   -195.4508673462  5.2665E+00  0.0000E+00  2.9987E+00  1.0000E-04   
 mr-sdci #  3  7   -190.1833894049  4.4295E+01  0.0000E+00  4.4486E+00  1.0000E-04   
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.005000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   4

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.27791804
   ht   2     0.00000000   -50.27477737
   ht   3     0.00000000     0.00000000   -50.27163671
   ht   4     0.00000000     0.00000000     0.00000000   -50.16923562
   ht   5     0.40697916     0.00304100    -0.00564540     0.00115654    -4.32256226
   ht   6     0.00000994     0.42561823    -0.00304852    -0.00068646    -0.15499563  -748.33416178
   ht   7     0.00003408     0.00010442    -0.40391504     0.00115068    -0.26556122    -1.79699439   -54.63684244
   ht   8    -0.00003921     0.00143926     0.00009157     0.72933517     0.01928158    -0.67227362     0.05011166   -15.97399496

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -4.123618E-02   0.976000      -9.416722E-03  -5.663812E-03   4.594539E-03  -4.405023E-03  -2.230173E-03   0.213444    
 ref    2  -1.059497E-03   7.065755E-03  -1.613000E-03   0.766444      -0.642266      -9.297351E-04   4.351950E-04   1.563580E-03
 ref    3   3.268782E-03  -1.005060E-02  -0.878498      -1.218497E-03   1.322434E-03  -0.477622      -1.609003E-04  -2.087978E-03
 ref    4  -0.950721      -4.003320E-02  -3.155048E-03   1.282743E-03   2.880760E-03   2.946751E-05   0.307410       2.428308E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.905583       0.954329       0.771859       0.587472       0.412537       0.228143       9.450596E-02   4.557090E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.04123618     0.97599956    -0.00941672    -0.00566381     0.00459454    -0.00440502    -0.00223017     0.21344366
 ref:   2    -0.00105950     0.00706575    -0.00161300     0.76644450    -0.64226606    -0.00092974     0.00043519     0.00156358
 ref:   3     0.00326878    -0.01005060    -0.87849769    -0.00121850     0.00132243    -0.47762170    -0.00016090    -0.00208798
 ref:   4    -0.95072118    -0.04003320    -0.00315505     0.00128274     0.00288076     0.00002947     0.30740978     0.00242831

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1   -196.4605042247  8.6188E-03  0.0000E+00  4.8096E-01  1.0000E-04   
 mr-sdci #  4  2   -196.4518689898  8.2328E-02  0.0000E+00  3.4782E-01  1.0000E-04   
 mr-sdci #  4  3   -196.3695390274  1.1404E-01  0.0000E+00  9.7367E-01  1.0000E-04   
 mr-sdci #  4  4   -196.2555001407  1.9795E-01  0.0000E+00  1.4830E+00  1.0000E-04   
 mr-sdci #  4  5   -196.0314984004  1.6757E-07  5.2284E-01  2.0490E+00  1.0000E-04   
 mr-sdci #  4  6   -195.4508704169  3.0707E-06  0.0000E+00  2.9987E+00  1.0000E-04   
 mr-sdci #  4  7   -192.1970666963  2.0137E+00  0.0000E+00  4.2269E+00  1.0000E-04   
 mr-sdci #  4  8   -190.1832324897  4.4295E+01  0.0000E+00  4.4485E+00  1.0000E-04   
 
 root number  5 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.006000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   5

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.27791804
   ht   2     0.00000000   -50.27477737
   ht   3     0.00000000     0.00000000   -50.27163671
   ht   4     0.00000000     0.00000000     0.00000000   -50.16923562
   ht   5     0.40697916     0.00304100    -0.00564540     0.00115654    -4.32256226
   ht   6     0.00000994     0.42561823    -0.00304852    -0.00068646    -0.15499563  -748.33416178
   ht   7     0.00003408     0.00010442    -0.40391504     0.00115068    -0.26556122    -1.79699439   -54.63684244
   ht   8    -0.00003921     0.00143926     0.00009157     0.72933517     0.01928158    -0.67227362     0.05011166   -15.97399496
   ht   9     0.00046588     0.38863862    -0.00397701    -0.00283610    -0.16852378  -145.62850621    -0.43078449    -0.10381333

                ht   9
   ht   9   -33.90105205

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -4.192885E-02   0.975677      -2.442446E-02  -9.421855E-03  -6.153330E-03  -4.393075E-03   2.248475E-03   7.378686E-02
 ref    2   3.322845E-03  -7.240567E-03  -0.483254      -2.658271E-03   0.862457      -3.011327E-03  -3.146865E-06   0.141973    
 ref    3   3.239391E-03  -9.922316E-03   4.857261E-03  -0.878490      -1.487598E-03  -0.477613       1.563634E-04  -2.097891E-03
 ref    4  -0.950682      -4.081578E-02  -4.375453E-03  -3.152896E-03   8.948280E-04   4.129873E-05  -0.307411       5.982715E-04

              v      9
 ref    1   0.200260    
 ref    2  -4.896871E-02
 ref    3  -1.470415E-03
 ref    4   2.387829E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.905576       0.953763       0.234174       0.771850       0.743872       0.228143       9.450689E-02   2.560567E-02

              v      9
 ref    1   4.250998E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.04192885     0.97567707    -0.02442446    -0.00942185    -0.00615333    -0.00439307     0.00224848     0.07378686
 ref:   2     0.00332285    -0.00724057    -0.48325413    -0.00265827     0.86245667    -0.00301133    -0.00000315     0.14197326
 ref:   3     0.00323939    -0.00992232     0.00485726    -0.87848974    -0.00148760    -0.47761326     0.00015636    -0.00209789
 ref:   4    -0.95068203    -0.04081578    -0.00437545    -0.00315290     0.00089483     0.00004130    -0.30741147     0.00059827

                ci   9
 ref:   1     0.20026029
 ref:   2    -0.04896871
 ref:   3    -0.00147042
 ref:   4     0.00238783

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1   -196.4605067186  2.4939E-06  0.0000E+00  4.8095E-01  1.0000E-04   
 mr-sdci #  5  2   -196.4518875801  1.8590E-05  0.0000E+00  3.4735E-01  1.0000E-04   
 mr-sdci #  5  3   -196.4311132515  6.1574E-02  0.0000E+00  4.6596E-01  1.0000E-04   
 mr-sdci #  5  4   -196.3695387398  1.1404E-01  0.0000E+00  9.7366E-01  1.0000E-04   
 mr-sdci #  5  5   -196.2526159293  2.2112E-01  0.0000E+00  1.4926E+00  1.0000E-04   
 mr-sdci #  5  6   -195.4508859481  1.5531E-05  1.0283E+00  2.9988E+00  1.0000E-04   
 mr-sdci #  5  7   -192.1970737178  7.0215E-06  0.0000E+00  4.2269E+00  1.0000E-04   
 mr-sdci #  5  8   -190.4696519008  2.8642E-01  0.0000E+00  4.2185E+00  1.0000E-04   
 mr-sdci #  5  9   -190.1425326638  4.4254E+01  0.0000E+00  4.4259E+00  1.0000E-04   
 
 root number  6 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.008000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   6

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.27791804
   ht   2     0.00000000   -50.27477737
   ht   3     0.00000000     0.00000000   -50.27163671
   ht   4     0.00000000     0.00000000     0.00000000   -50.16923562
   ht   5     0.40697916     0.00304100    -0.00564540     0.00115654    -4.32256226
   ht   6     0.00000994     0.42561823    -0.00304852    -0.00068646    -0.15499563  -748.33416178
   ht   7     0.00003408     0.00010442    -0.40391504     0.00115068    -0.26556122    -1.79699439   -54.63684244
   ht   8    -0.00003921     0.00143926     0.00009157     0.72933517     0.01928158    -0.67227362     0.05011166   -15.97399496
   ht   9     0.00046588     0.38863862    -0.00397701    -0.00283610    -0.16852378  -145.62850621    -0.43078449    -0.10381333
   ht  10    -0.01564888    -0.00950779     0.75389325     0.00800474     1.04873628     0.63702102    51.87198215    -0.84772455

                ht   9         ht  10
   ht   9   -33.90105205
   ht  10     0.94904123  -100.80077310

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   3.635031E-02   0.975530      -2.093116E-02  -3.180676E-02  -5.967651E-03   3.809045E-03  -3.990096E-03   1.669371E-03
 ref    2  -3.561561E-03  -6.002663E-03  -0.483067      -1.316515E-02   0.862419      -1.070840E-02  -6.189075E-03  -5.521749E-04
 ref    3   5.631567E-04  -2.290795E-02   1.436138E-02  -0.794836      -1.032656E-02  -0.573796       0.195267       4.511271E-03
 ref    4   0.950900      -3.524840E-02  -5.254483E-03   4.181960E-03   7.907655E-04  -2.555056E-03   1.016096E-02  -0.307223    

              v      9       v     10
 ref    1   6.992416E-02  -0.201526    
 ref    2   0.142651       4.642467E-02
 ref    3   2.305328E-03  -4.006381E-04
 ref    4  -1.855489E-04  -2.104977E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.905546       0.953461       0.234026       0.632968       0.743909       0.329378       3.828659E-02   9.440952E-02

              v      9       v     10
 ref    1   2.524397E-02   4.277238E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.03635031     0.97552966    -0.02093116    -0.03180676    -0.00596765     0.00380905    -0.00399010     0.00166937
 ref:   2    -0.00356156    -0.00600266    -0.48306700    -0.01316515     0.86241897    -0.01070840    -0.00618908    -0.00055217
 ref:   3     0.00056316    -0.02290795     0.01436138    -0.79483649    -0.01032656    -0.57379615     0.19526680     0.00451127
 ref:   4     0.95090037    -0.03524840    -0.00525448     0.00418196     0.00079077    -0.00255506     0.01016096    -0.30722316

                ci   9         ci  10
 ref:   1     0.06992416    -0.20152552
 ref:   2     0.14265075     0.04642467
 ref:   3     0.00230533    -0.00040064
 ref:   4    -0.00018555    -0.00210498

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  6  1   -196.4605235233  1.6805E-05  3.1430E-02  4.8092E-01  1.0000E-04   
 mr-sdci #  6  2   -196.4520359059  1.4833E-04  0.0000E+00  3.4549E-01  1.0000E-04   
 mr-sdci #  6  3   -196.4311451179  3.1866E-05  0.0000E+00  4.6545E-01  1.0000E-04   
 mr-sdci #  6  4   -196.3885104719  1.8972E-02  0.0000E+00  8.3621E-01  1.0000E-04   
 mr-sdci #  6  5   -196.2526418479  2.5919E-05  0.0000E+00  1.4925E+00  1.0000E-04   
 mr-sdci #  6  6   -196.0060995342  5.5521E-01  0.0000E+00  1.8378E+00  1.0000E-04   
 mr-sdci #  6  7   -193.6986242576  1.5016E+00  0.0000E+00  3.0946E+00  1.0000E-04   
 mr-sdci #  6  8   -192.1945972087  1.7249E+00  0.0000E+00  4.2276E+00  1.0000E-04   
 mr-sdci #  6  9   -190.4570830219  3.1455E-01  0.0000E+00  4.2187E+00  1.0000E-04   
 mr-sdci #  6 10   -190.1397002993  4.4251E+01  0.0000E+00  4.4310E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.011000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   7

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.27791804
   ht   2     0.00000000   -50.27477737
   ht   3     0.00000000     0.00000000   -50.27163671
   ht   4     0.00000000     0.00000000     0.00000000   -50.16923562
   ht   5     0.40697916     0.00304100    -0.00564540     0.00115654    -4.32256226
   ht   6     0.00000994     0.42561823    -0.00304852    -0.00068646    -0.15499563  -748.33416178
   ht   7     0.00003408     0.00010442    -0.40391504     0.00115068    -0.26556122    -1.79699439   -54.63684244
   ht   8    -0.00003921     0.00143926     0.00009157     0.72933517     0.01928158    -0.67227362     0.05011166   -15.97399496
   ht   9     0.00046588     0.38863862    -0.00397701    -0.00283610    -0.16852378  -145.62850621    -0.43078449    -0.10381333
   ht  10    -0.01564888    -0.00950779     0.75389325     0.00800474     1.04873628     0.63702102    51.87198215    -0.84772455
   ht  11     0.00003945     0.00010094    -0.00000190    -0.01116781     0.00751442     0.01675636     0.00026386    -1.36199690

                ht   9         ht  10         ht  11
   ht   9   -33.90105205
   ht  10     0.94904123  -100.80077310
   ht  11     0.00511491    -0.00614874    -0.47241743

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   1.935633E-02   0.976012       2.091569E-02  -3.182829E-02  -5.961438E-03   3.818050E-03  -4.033834E-03  -3.820102E-03
 ref    2  -2.450803E-03  -6.080956E-03   0.483073      -1.317082E-02   0.862419      -1.071201E-02  -6.180918E-03   7.536451E-04
 ref    3  -3.031046E-04  -2.288319E-02  -1.437470E-02  -0.794836      -1.033133E-02  -0.573798       0.195221      -5.808720E-03
 ref    4   0.956312      -1.889176E-02   2.576823E-03   2.477045E-03   1.177538E-03  -2.115747E-03   1.151217E-02   0.189533    

              v      9       v     10       v     11
 ref    1  -7.040319E-02  -0.164907       0.115517    
 ref    2  -0.142480       4.078317E-02  -2.325226E-02
 ref    3  -2.235865E-03   9.724070E-04   2.077353E-03
 ref    4  -6.560744E-03  -0.128116      -0.180499    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.914913       0.953516       0.234010       0.632956       0.743910       0.329378       3.829821E-02   3.597185E-02

              v      9       v     10       v     11
 ref    1   2.530524E-02   4.527218E-02   4.646910E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.01935633     0.97601173     0.02091569    -0.03182829    -0.00596144     0.00381805    -0.00403383    -0.00382010
 ref:   2    -0.00245080    -0.00608096     0.48307299    -0.01317082     0.86241868    -0.01071201    -0.00618092     0.00075365
 ref:   3    -0.00030310    -0.02288319    -0.01437470    -0.79483550    -0.01033133    -0.57379806     0.19522092    -0.00580872
 ref:   4     0.95631177    -0.01889176     0.00257682     0.00247704     0.00117754    -0.00211575     0.01151217     0.18953349

                ci   9         ci  10         ci  11
 ref:   1    -0.07040319    -0.16490660     0.11551685
 ref:   2    -0.14248013     0.04078317    -0.02325226
 ref:   3    -0.00223586     0.00097241     0.00207735
 ref:   4    -0.00656074    -0.12811626    -0.18049923

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  7  1   -196.4867807670  2.6257E-02  0.0000E+00  5.6607E-02  1.0000E-04   
 mr-sdci #  7  2   -196.4520392380  3.3321E-06  2.6874E-02  3.4536E-01  1.0000E-04   
 mr-sdci #  7  3   -196.4311456165  4.9860E-07  0.0000E+00  4.6546E-01  1.0000E-04   
 mr-sdci #  7  4   -196.3885113631  8.9129E-07  0.0000E+00  8.3619E-01  1.0000E-04   
 mr-sdci #  7  5   -196.2526421841  3.3615E-07  0.0000E+00  1.4925E+00  1.0000E-04   
 mr-sdci #  7  6   -196.0061017817  2.2474E-06  0.0000E+00  1.8378E+00  1.0000E-04   
 mr-sdci #  7  7   -193.6986854254  6.1168E-05  0.0000E+00  3.0939E+00  1.0000E-04   
 mr-sdci #  7  8   -193.2446911389  1.0501E+00  0.0000E+00  2.8183E+00  1.0000E-04   
 mr-sdci #  7  9   -190.4573282730  2.4525E-04  0.0000E+00  4.2210E+00  1.0000E-04   
 mr-sdci #  7 10   -190.1610250757  2.1325E-02  0.0000E+00  4.9732E+00  1.0000E-04   
 mr-sdci #  7 11   -190.0935733139  4.4205E+01  0.0000E+00  5.4339E+00  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.008000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   8

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.27791804
   ht   2     0.00000000   -50.27477737
   ht   3     0.00000000     0.00000000   -50.27163671
   ht   4     0.00000000     0.00000000     0.00000000   -50.16923562
   ht   5     0.40697916     0.00304100    -0.00564540     0.00115654    -4.32256226
   ht   6     0.00000994     0.42561823    -0.00304852    -0.00068646    -0.15499563  -748.33416178
   ht   7     0.00003408     0.00010442    -0.40391504     0.00115068    -0.26556122    -1.79699439   -54.63684244
   ht   8    -0.00003921     0.00143926     0.00009157     0.72933517     0.01928158    -0.67227362     0.05011166   -15.97399496
   ht   9     0.00046588     0.38863862    -0.00397701    -0.00283610    -0.16852378  -145.62850621    -0.43078449    -0.10381333
   ht  10    -0.01564888    -0.00950779     0.75389325     0.00800474     1.04873628     0.63702102    51.87198215    -0.84772455
   ht  11     0.00003945     0.00010094    -0.00000190    -0.01116781     0.00751442     0.01675636     0.00026386    -1.36199690
   ht  12    -0.00224566    -0.00025981     0.00075793     0.00053094    -0.36121309    -0.04229852    -0.12454522     0.06528412

                ht   9         ht  10         ht  11         ht  12
   ht   9   -33.90105205
   ht  10     0.94904123  -100.80077310
   ht  11     0.00511491    -0.00614874    -0.47241743
   ht  12    -0.01618190     0.02479926     0.01077533    -0.58262694

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -3.257372E-03  -0.974775      -6.126256E-03   3.050573E-02   7.005028E-03   3.576305E-03   1.413170E-02   1.048290E-02
 ref    2   2.534449E-03  -1.971422E-03  -0.483127       1.315362E-02  -0.862404       1.094024E-02   6.307317E-03   5.326878E-04
 ref    3  -1.512674E-04   2.531677E-02   1.405479E-02   0.794847       1.039276E-02   0.573462      -0.194783       4.673881E-03
 ref    4  -0.956499       3.224418E-03  -2.939795E-03  -2.440311E-03  -1.207396E-03   1.924580E-03  -7.766372E-03   0.180717    

              v      9       v     10       v     11       v     12
 ref    1  -3.988899E-02  -6.213543E-02   0.118561       0.170162    
 ref    2   5.615760E-04  -0.144494      -2.622677E-02  -3.063810E-02
 ref    3  -2.097037E-02  -2.258248E-03  -1.520149E-03   1.761425E-03
 ref    4   5.763055E-02  -5.767417E-03   0.182296      -0.125598    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.914906       0.950841       0.233655       0.632891       0.743899       0.328995       3.824015E-02   3.279069E-02

              v      9       v     10       v     11       v     12
 ref    1   5.352483E-03   2.477767E-02   4.797889E-02   4.567193E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00325737    -0.97477478    -0.00612626     0.03050573     0.00700503     0.00357631     0.01413170     0.01048290
 ref:   2     0.00253445    -0.00197142    -0.48312669     0.01315362    -0.86240372     0.01094024     0.00630732     0.00053269
 ref:   3    -0.00015127     0.02531677     0.01405479     0.79484706     0.01039276     0.57346246    -0.19478281     0.00467388
 ref:   4    -0.95649853     0.00322442    -0.00293979    -0.00244031    -0.00120740     0.00192458    -0.00776637     0.18071710

                ci   9         ci  10         ci  11         ci  12
 ref:   1    -0.03988899    -0.06213543     0.11856110     0.17016243
 ref:   2     0.00056158    -0.14449392    -0.02622677    -0.03063810
 ref:   3    -0.02097037    -0.00225825    -0.00152015     0.00176143
 ref:   4     0.05763055    -0.00576742     0.18229648    -0.12559809

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  8  1   -196.4867893459  8.5790E-06  0.0000E+00  5.6437E-02  1.0000E-04   
 mr-sdci #  8  2   -196.4703032229  1.8264E-02  0.0000E+00  7.3770E-02  1.0000E-04   
 mr-sdci #  8  3   -196.4311558589  1.0242E-05  4.5716E-02  4.6522E-01  1.0000E-04   
 mr-sdci #  8  4   -196.3885118787  5.1553E-07  0.0000E+00  8.3620E-01  1.0000E-04   
 mr-sdci #  8  5   -196.2526447954  2.6113E-06  0.0000E+00  1.4925E+00  1.0000E-04   
 mr-sdci #  8  6   -196.0066958302  5.9405E-04  0.0000E+00  1.8343E+00  1.0000E-04   
 mr-sdci #  8  7   -193.7147532808  1.6068E-02  0.0000E+00  3.1046E+00  1.0000E-04   
 mr-sdci #  8  8   -193.2640502828  1.9359E-02  0.0000E+00  2.8230E+00  1.0000E-04   
 mr-sdci #  8  9   -193.0527263704  2.5954E+00  0.0000E+00  3.2168E+00  1.0000E-04   
 mr-sdci #  8 10   -190.4523549334  2.9133E-01  0.0000E+00  4.2464E+00  1.0000E-04   
 mr-sdci #  8 11   -190.1392611795  4.5688E-02  0.0000E+00  5.5574E+00  1.0000E-04   
 mr-sdci #  8 12   -190.0670442805  4.4179E+01  0.0000E+00  5.1408E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.008000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   9

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.27791804
   ht   2     0.00000000   -50.27477737
   ht   3     0.00000000     0.00000000   -50.27163671
   ht   4     0.00000000     0.00000000     0.00000000   -50.16923562
   ht   5     0.40697916     0.00304100    -0.00564540     0.00115654    -4.32256226
   ht   6     0.00000994     0.42561823    -0.00304852    -0.00068646    -0.15499563  -748.33416178
   ht   7     0.00003408     0.00010442    -0.40391504     0.00115068    -0.26556122    -1.79699439   -54.63684244
   ht   8    -0.00003921     0.00143926     0.00009157     0.72933517     0.01928158    -0.67227362     0.05011166   -15.97399496
   ht   9     0.00046588     0.38863862    -0.00397701    -0.00283610    -0.16852378  -145.62850621    -0.43078449    -0.10381333
   ht  10    -0.01564888    -0.00950779     0.75389325     0.00800474     1.04873628     0.63702102    51.87198215    -0.84772455
   ht  11     0.00003945     0.00010094    -0.00000190    -0.01116781     0.00751442     0.01675636     0.00026386    -1.36199690
   ht  12    -0.00224566    -0.00025981     0.00075793     0.00053094    -0.36121309    -0.04229852    -0.12454522     0.06528412
   ht  13     0.00014977    -0.00114653    -0.00027680     0.00019204     0.02688075    -0.18274898     0.05090686     0.02969538

                ht   9         ht  10         ht  11         ht  12         ht  13
   ht   9   -33.90105205
   ht  10     0.94904123  -100.80077310
   ht  11     0.00511491    -0.00614874    -0.47241743
   ht  12    -0.01618190     0.02479926     0.01077533    -0.58262694
   ht  13     0.73423779     0.02089041     0.00613599     0.00664186    -0.98382162

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -3.322778E-03   0.974673       1.543736E-02   3.047170E-02  -6.924828E-03  -3.626336E-03   1.390701E-02  -1.111430E-02
 ref    2   1.370998E-03   1.257691E-02  -0.477077       1.131953E-02   0.865766      -1.263420E-02   7.190916E-03  -7.754483E-04
 ref    3  -1.181561E-04  -2.559589E-02   1.224367E-02   0.794883      -1.027480E-02  -0.573391      -0.194748      -2.850057E-03
 ref    4  -0.956503      -3.257381E-03  -7.287575E-04  -2.453820E-03   1.234253E-03  -1.936198E-03  -8.453879E-03  -0.178960    

              v      9       v     10       v     11       v     12       v     13
 ref    1   3.928875E-02   6.504804E-03   0.159522       1.554023E-02  -0.145538    
 ref    2   1.203394E-03  -1.092744E-02   9.309675E-02   5.350306E-02   0.103421    
 ref    3   1.793591E-02   1.428823E-02   1.678469E-03   2.968306E-03   3.331841E-04
 ref    4  -6.232450E-02   7.759622E-03   6.659117E-02  -0.205942       4.680186E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.914911       0.950812       0.227992       0.632902       0.749706       0.328954       3.824330E-02   3.215903E-02

              v      9       v     10       v     11       v     12       v     13
 ref    1   5.751094E-03   4.260866E-04   3.855145E-02   4.552487E-02   3.406781E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00332278     0.97467331     0.01543736     0.03047170    -0.00692483    -0.00362634     0.01390701    -0.01111430
 ref:   2     0.00137100     0.01257691    -0.47707748     0.01131953     0.86576591    -0.01263420     0.00719092    -0.00077545
 ref:   3    -0.00011816    -0.02559589     0.01224367     0.79488313    -0.01027480    -0.57339143    -0.19474782    -0.00285006
 ref:   4    -0.95650300    -0.00325738    -0.00072876    -0.00245382     0.00123425    -0.00193620    -0.00845388    -0.17896026

                ci   9         ci  10         ci  11         ci  12         ci  13
 ref:   1     0.03928875     0.00650480     0.15952192     0.01554023    -0.14553820
 ref:   2     0.00120339    -0.01092744     0.09309675     0.05350306     0.10342104
 ref:   3     0.01793591     0.01428823     0.00167847     0.00296831     0.00033318
 ref:   4    -0.06232450     0.00775962     0.06659117    -0.20594170     0.04680186

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  9  1   -196.4867896190  2.7305E-07  0.0000E+00  5.6439E-02  1.0000E-04   
 mr-sdci #  9  2   -196.4703085976  5.3747E-06  0.0000E+00  7.3575E-02  1.0000E-04   
 mr-sdci #  9  3   -196.4618292209  3.0673E-02  0.0000E+00  8.5549E-02  1.0000E-04   
 mr-sdci #  9  4   -196.3885132726  1.3939E-06  1.1770E-01  8.3619E-01  1.0000E-04   
 mr-sdci #  9  5   -196.2527046272  5.9832E-05  0.0000E+00  1.4920E+00  1.0000E-04   
 mr-sdci #  9  6   -196.0068043813  1.0855E-04  0.0000E+00  1.8335E+00  1.0000E-04   
 mr-sdci #  9  7   -193.7190125244  4.2592E-03  0.0000E+00  3.1091E+00  1.0000E-04   
 mr-sdci #  9  8   -193.2672692161  3.2189E-03  0.0000E+00  2.8252E+00  1.0000E-04   
 mr-sdci #  9  9   -193.0567355586  4.0092E-03  0.0000E+00  3.2125E+00  1.0000E-04   
 mr-sdci #  9 10   -192.9664265383  2.5141E+00  0.0000E+00  2.9016E+00  1.0000E-04   
 mr-sdci #  9 11   -190.2084611935  6.9200E-02  0.0000E+00  5.0308E+00  1.0000E-04   
 mr-sdci #  9 12   -190.1118582312  4.4814E-02  0.0000E+00  5.7867E+00  1.0000E-04   
 mr-sdci #  9 13   -190.0139792672  4.4126E+01  0.0000E+00  4.9662E+00  1.0000E-04   
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.007000
time for eigenvalue solver             0.001000
time for vector access                 0.000000

          starting ci iteration  10

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.27791804
   ht   2     0.00000000   -50.27477737
   ht   3     0.00000000     0.00000000   -50.27163671
   ht   4     0.00000000     0.00000000     0.00000000   -50.16923562
   ht   5     0.40697916     0.00304100    -0.00564540     0.00115654    -4.32256226
   ht   6     0.00000994     0.42561823    -0.00304852    -0.00068646    -0.15499563  -748.33416178
   ht   7     0.00003408     0.00010442    -0.40391504     0.00115068    -0.26556122    -1.79699439   -54.63684244
   ht   8    -0.00003921     0.00143926     0.00009157     0.72933517     0.01928158    -0.67227362     0.05011166   -15.97399496
   ht   9     0.00046588     0.38863862    -0.00397701    -0.00283610    -0.16852378  -145.62850621    -0.43078449    -0.10381333
   ht  10    -0.01564888    -0.00950779     0.75389325     0.00800474     1.04873628     0.63702102    51.87198215    -0.84772455
   ht  11     0.00003945     0.00010094    -0.00000190    -0.01116781     0.00751442     0.01675636     0.00026386    -1.36199690
   ht  12    -0.00224566    -0.00025981     0.00075793     0.00053094    -0.36121309    -0.04229852    -0.12454522     0.06528412
   ht  13     0.00014977    -0.00114653    -0.00027680     0.00019204     0.02688075    -0.18274898     0.05090686     0.02969538
   ht  14    -0.00044242     0.00009684    -0.07586187     0.00048938    -0.09662605     0.02065707    16.63548069     0.04975555

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14
   ht   9   -33.90105205
   ht  10     0.94904123  -100.80077310
   ht  11     0.00511491    -0.00614874    -0.47241743
   ht  12    -0.01618190     0.02479926     0.01077533    -0.58262694
   ht  13     0.73423779     0.02089041     0.00613599     0.00664186    -0.98382162
   ht  14    -0.07977711   -10.12481546     0.00170441     0.03371168    -0.01010218    -7.36756058

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   3.308691E-03   0.973706       1.656180E-02   5.301400E-02  -6.798507E-03  -3.013834E-04  -1.390490E-02  -1.114933E-02
 ref    2  -1.383633E-03   1.247152E-02  -0.476843       1.767436E-02   0.865772       1.386698E-02  -7.189069E-03  -7.750623E-04
 ref    3  -1.556050E-03  -4.554468E-02   2.710931E-02   0.863824      -7.804055E-03   0.440144       0.195209      -5.045771E-03
 ref    4   0.956505      -3.293219E-03  -7.296034E-04  -4.253703E-04   1.214861E-03   2.344190E-03   8.461305E-03  -0.178859    

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1   3.923258E-02   6.139371E-03   0.118028      -7.131846E-02   2.616383E-02  -0.164801    
 ref    2   1.143534E-03  -1.092324E-02   3.642120E-02  -9.236131E-02   8.187888E-02   7.526017E-02
 ref    3   2.171392E-02   1.785020E-02  -0.107819      -6.403905E-02   2.273526E-02  -4.519644E-02
 ref    4  -6.257789E-02   7.951843E-03   2.396341E-02  -0.120595      -0.180427       3.697253E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.914917       0.950343       0.228389       0.749316       0.749669       0.193925       3.842323E-02   3.214079E-02

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1   5.927990E-03   5.388705E-04   2.745639E-02   3.226105E-02   4.045955E-02   3.623313E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00330869     0.97370560     0.01656180     0.05301400    -0.00679851    -0.00030138    -0.01390490    -0.01114933
 ref:   2    -0.00138363     0.01247152    -0.47684279     0.01767436     0.86577165     0.01386698    -0.00718907    -0.00077506
 ref:   3    -0.00155605    -0.04554468     0.02710931     0.86382445    -0.00780406     0.44014422     0.19520913    -0.00504577
 ref:   4     0.95650506    -0.00329322    -0.00072960    -0.00042537     0.00121486     0.00234419     0.00846130    -0.17885867

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14
 ref:   1     0.03923258     0.00613937     0.11802797    -0.07131846     0.02616383    -0.16480095
 ref:   2     0.00114353    -0.01092324     0.03642120    -0.09236131     0.08187888     0.07526017
 ref:   3     0.02171392     0.01785020    -0.10781950    -0.06403905     0.02273526    -0.04519644
 ref:   4    -0.06257789     0.00795184     0.02396341    -0.12059484    -0.18042718     0.03697253

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 10  1   -196.4867897649  1.4591E-07  0.0000E+00  5.6436E-02  1.0000E-04   
 mr-sdci # 10  2   -196.4703162625  7.6649E-06  0.0000E+00  7.3418E-02  1.0000E-04   
 mr-sdci # 10  3   -196.4618304673  1.2464E-06  0.0000E+00  8.5581E-02  1.0000E-04   
 mr-sdci # 10  4   -196.4578233457  6.9310E-02  0.0000E+00  1.5553E-01  1.0000E-04   
 mr-sdci # 10  5   -196.2527138113  9.1841E-06  3.2800E-01  1.4920E+00  1.0000E-04   
 mr-sdci # 10  6   -196.0575395771  5.0735E-02  0.0000E+00  1.6597E+00  1.0000E-04   
 mr-sdci # 10  7   -193.7190196004  7.0760E-06  0.0000E+00  3.1087E+00  1.0000E-04   
 mr-sdci # 10  8   -193.2674594949  1.9028E-04  0.0000E+00  2.8250E+00  1.0000E-04   
 mr-sdci # 10  9   -193.0573279760  5.9242E-04  0.0000E+00  3.2085E+00  1.0000E-04   
 mr-sdci # 10 10   -192.9670389007  6.1236E-04  0.0000E+00  2.8985E+00  1.0000E-04   
 mr-sdci # 10 11   -190.3734236800  1.6496E-01  0.0000E+00  5.0478E+00  1.0000E-04   
 mr-sdci # 10 12   -190.1325293959  2.0671E-02  0.0000E+00  5.2987E+00  1.0000E-04   
 mr-sdci # 10 13   -190.1101118360  9.6133E-02  0.0000E+00  5.6245E+00  1.0000E-04   
 mr-sdci # 10 14   -189.9827748262  4.4094E+01  0.0000E+00  4.9200E+00  1.0000E-04   
 
 root number  5 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.009000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  11

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.27791804
   ht   2     0.00000000   -50.27477737
   ht   3     0.00000000     0.00000000   -50.27163671
   ht   4     0.00000000     0.00000000     0.00000000   -50.16923562
   ht   5     0.40697916     0.00304100    -0.00564540     0.00115654    -4.32256226
   ht   6     0.00000994     0.42561823    -0.00304852    -0.00068646    -0.15499563  -748.33416178
   ht   7     0.00003408     0.00010442    -0.40391504     0.00115068    -0.26556122    -1.79699439   -54.63684244
   ht   8    -0.00003921     0.00143926     0.00009157     0.72933517     0.01928158    -0.67227362     0.05011166   -15.97399496
   ht   9     0.00046588     0.38863862    -0.00397701    -0.00283610    -0.16852378  -145.62850621    -0.43078449    -0.10381333
   ht  10    -0.01564888    -0.00950779     0.75389325     0.00800474     1.04873628     0.63702102    51.87198215    -0.84772455
   ht  11     0.00003945     0.00010094    -0.00000190    -0.01116781     0.00751442     0.01675636     0.00026386    -1.36199690
   ht  12    -0.00224566    -0.00025981     0.00075793     0.00053094    -0.36121309    -0.04229852    -0.12454522     0.06528412
   ht  13     0.00014977    -0.00114653    -0.00027680     0.00019204     0.02688075    -0.18274898     0.05090686     0.02969538
   ht  14    -0.00044242     0.00009684    -0.07586187     0.00048938    -0.09662605     0.02065707    16.63548069     0.04975555
   ht  15     0.00013793    -0.24053065     0.00308451     0.00009851     0.07848536  -135.92402191    -0.76398664    -0.14302076

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15
   ht   9   -33.90105205
   ht  10     0.94904123  -100.80077310
   ht  11     0.00511491    -0.00614874    -0.47241743
   ht  12    -0.01618190     0.02479926     0.01077533    -0.58262694
   ht  13     0.73423779     0.02089041     0.00613599     0.00664186    -0.98382162
   ht  14    -0.07977711   -10.12481546     0.00170441     0.03371168    -0.01010218    -7.36756058
   ht  15   -25.76106282     0.54039046     0.00067566     0.00867521    -0.03148620     0.18766815   -28.52180058

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -3.336222E-03  -0.972861      -8.101045E-03  -5.006331E-02  -4.698970E-02   2.846940E-04   1.387657E-02   1.111371E-02
 ref    2   1.154520E-04  -4.114283E-02   0.343342       0.900852      -0.145207      -1.118020E-02   3.016620E-03  -2.526883E-03
 ref    3   1.577681E-03   4.621923E-02  -1.950091E-02  -0.132521      -0.853797      -0.440166      -0.195176       5.194115E-03
 ref    4  -0.956506       3.335997E-03   7.458037E-04   1.236087E-04   4.081593E-04  -2.340419E-03  -8.393478E-03   0.178669    

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1  -3.887156E-02  -8.036975E-03  -0.115074      -1.038289E-02  -2.960907E-02   0.180028       1.489714E-02
 ref    2   3.117730E-03  -9.591092E-03  -0.118831      -0.163037       2.182067E-02  -7.320194E-02  -3.203436E-02
 ref    3  -2.134256E-02  -1.751386E-02   8.994013E-02  -8.663018E-02  -1.689506E-02   4.926814E-02  -3.755740E-03
 ref    4   6.321788E-02  -7.139157E-03  -1.924128E-02   4.804591E-03  -0.216125      -4.380059E-02  -4.094690E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.914917       0.950299       0.118330       0.831602       0.752262       0.193877       3.836589E-02   3.207961E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1   5.972724E-03   5.142847E-04   3.582237E-02   3.421685E-02   4.834844E-02   4.211456E-02   1.278997E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00333622    -0.97286095    -0.00810105    -0.05006331    -0.04698970     0.00028469     0.01387657     0.01111371
 ref:   2     0.00011545    -0.04114283     0.34334153     0.90085172    -0.14520665    -0.01118020     0.00301662    -0.00252688
 ref:   3     0.00157768     0.04621923    -0.01950091    -0.13252145    -0.85379684    -0.44016601    -0.19517628     0.00519412
 ref:   4    -0.95650576     0.00333600     0.00074580     0.00012361     0.00040816    -0.00234042    -0.00839348     0.17866934

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15
 ref:   1    -0.03887156    -0.00803698    -0.11507425    -0.01038289    -0.02960907     0.18002830     0.01489714
 ref:   2     0.00311773    -0.00959109    -0.11883110    -0.16303734     0.02182067    -0.07320194    -0.03203436
 ref:   3    -0.02134256    -0.01751386     0.08994013    -0.08663018    -0.01689506     0.04926814    -0.00375574
 ref:   4     0.06321788    -0.00713916    -0.01924128     0.00480459    -0.21612533    -0.04380059    -0.00409469

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 11  1   -196.4867898358  7.0952E-08  0.0000E+00  5.6432E-02  1.0000E-04   
 mr-sdci # 11  2   -196.4703303011  1.4039E-05  0.0000E+00  7.3601E-02  1.0000E-04   
 mr-sdci # 11  3   -196.4618926008  6.2134E-05  0.0000E+00  9.8396E-02  1.0000E-04   
 mr-sdci # 11  4   -196.4592155783  1.3922E-03  0.0000E+00  2.5510E-01  1.0000E-04   
 mr-sdci # 11  5   -196.4577943305  2.0508E-01  0.0000E+00  1.5764E-01  1.0000E-04   
 mr-sdci # 11  6   -196.0575431156  3.5385E-06  9.0808E-02  1.6596E+00  1.0000E-04   
 mr-sdci # 11  7   -193.7194657208  4.4612E-04  0.0000E+00  3.1082E+00  1.0000E-04   
 mr-sdci # 11  8   -193.2677908839  3.3139E-04  0.0000E+00  2.8249E+00  1.0000E-04   
 mr-sdci # 11  9   -193.0579147496  5.8677E-04  0.0000E+00  3.2039E+00  1.0000E-04   
 mr-sdci # 11 10   -192.9808848994  1.3846E-02  0.0000E+00  2.9219E+00  1.0000E-04   
 mr-sdci # 11 11   -190.4300965969  5.6673E-02  0.0000E+00  4.9773E+00  1.0000E-04   
 mr-sdci # 11 12   -190.1988071053  6.6278E-02  0.0000E+00  4.9757E+00  1.0000E-04   
 mr-sdci # 11 13   -190.1190666866  8.9549E-03  0.0000E+00  5.8506E+00  1.0000E-04   
 mr-sdci # 11 14   -189.9895428303  6.7680E-03  0.0000E+00  4.8794E+00  1.0000E-04   
 mr-sdci # 11 15   -189.8840813846  4.3996E+01  0.0000E+00  4.8503E+00  1.0000E-04   
 
 root number  6 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.009000
time for eigenvalue solver             0.001000
time for vector access                 0.000000

          starting ci iteration  12

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.27791804
   ht   2     0.00000000   -50.27477737
   ht   3     0.00000000     0.00000000   -50.27163671
   ht   4     0.00000000     0.00000000     0.00000000   -50.16923562
   ht   5     0.40697916     0.00304100    -0.00564540     0.00115654    -4.32256226
   ht   6     0.00000994     0.42561823    -0.00304852    -0.00068646    -0.15499563  -748.33416178
   ht   7     0.00003408     0.00010442    -0.40391504     0.00115068    -0.26556122    -1.79699439   -54.63684244
   ht   8    -0.00003921     0.00143926     0.00009157     0.72933517     0.01928158    -0.67227362     0.05011166   -15.97399496
   ht   9     0.00046588     0.38863862    -0.00397701    -0.00283610    -0.16852378  -145.62850621    -0.43078449    -0.10381333
   ht  10    -0.01564888    -0.00950779     0.75389325     0.00800474     1.04873628     0.63702102    51.87198215    -0.84772455
   ht  11     0.00003945     0.00010094    -0.00000190    -0.01116781     0.00751442     0.01675636     0.00026386    -1.36199690
   ht  12    -0.00224566    -0.00025981     0.00075793     0.00053094    -0.36121309    -0.04229852    -0.12454522     0.06528412
   ht  13     0.00014977    -0.00114653    -0.00027680     0.00019204     0.02688075    -0.18274898     0.05090686     0.02969538
   ht  14    -0.00044242     0.00009684    -0.07586187     0.00048938    -0.09662605     0.02065707    16.63548069     0.04975555
   ht  15     0.00013793    -0.24053065     0.00308451     0.00009851     0.07848536  -135.92402191    -0.76398664    -0.14302076
   ht  16     0.00025569     0.00667511     0.19307992    -0.00051740    -0.11028046    -3.15556672    94.44922229     0.36616336

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15         ht  16
   ht   9   -33.90105205
   ht  10     0.94904123  -100.80077310
   ht  11     0.00511491    -0.00614874    -0.47241743
   ht  12    -0.01618190     0.02479926     0.01077533    -0.58262694
   ht  13     0.73423779     0.02089041     0.00613599     0.00664186    -0.98382162
   ht  14    -0.07977711   -10.12481546     0.00170441     0.03371168    -0.01010218    -7.36756058
   ht  15   -25.76106282     0.54039046     0.00067566     0.00867521    -0.03148620     0.18766815   -28.52180058
   ht  16    -0.81881586   -60.41211325     0.04076937     0.25071213    -0.09257064   -35.55699093     0.32772115  -189.46302045

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   3.331441E-03  -0.972209       1.216715E-02  -7.241016E-02   2.567382E-02   8.430311E-05  -7.130588E-03  -3.497888E-03
 ref    2  -1.216703E-04  -4.163090E-02  -0.347398       0.199639      -0.888758      -1.279541E-02  -2.275722E-03   2.558285E-03
 ref    3  -1.361452E-03   5.620186E-02   6.456222E-02  -0.850132      -0.214504      -0.374423       0.226894      -4.827802E-02
 ref    4   0.956506       3.319754E-03  -7.777429E-04   4.874031E-04   6.128093E-05  -2.448836E-03  -2.069088E-03  -0.174133    

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1  -3.397663E-02   2.480287E-02   8.082138E-03   0.123303      -5.488460E-02   4.180801E-02   0.160593      -3.312072E-02
 ref    2   2.024080E-03  -2.347639E-03   9.586430E-03   0.147853       9.097902E-02  -0.122778      -3.746990E-02   3.069239E-02
 ref    3   7.895887E-02   7.548174E-02   1.775864E-02  -6.473335E-02   2.544836E-02  -8.229946E-02   7.661414E-02  -1.422705E-02
 ref    4  -1.875202E-02  -7.348888E-02   6.952569E-03   1.516099E-02  -0.188130      -0.106929      -4.235242E-02   1.005231E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.914917       0.950092       0.125002       0.767823       0.836562       0.140363       5.154140E-02   3.267189E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   7.744649E-03   1.171880E-02   5.209280E-04   4.148440E-02   4.732985E-02   3.502941E-02   3.485742E-02   2.342463E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15        ev   16

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00333144    -0.97220864     0.01216715    -0.07241016     0.02567382     0.00008430    -0.00713059    -0.00349789
 ref:   2    -0.00012167    -0.04163090    -0.34739791     0.19963851    -0.88875806    -0.01279541    -0.00227572     0.00255829
 ref:   3    -0.00136145     0.05620186     0.06456222    -0.85013165    -0.21450383    -0.37442345     0.22689445    -0.04827802
 ref:   4     0.95650602     0.00331975    -0.00077774     0.00048740     0.00006128    -0.00244884    -0.00206909    -0.17413311

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1    -0.03397663     0.02480287     0.00808214     0.12330313    -0.05488460     0.04180801     0.16059258    -0.03312072
 ref:   2     0.00202408    -0.00234764     0.00958643     0.14785289     0.09097902    -0.12277826    -0.03746990     0.03069239
 ref:   3     0.07895887     0.07548174     0.01775864    -0.06473335     0.02544836    -0.08229946     0.07661414    -0.01422705
 ref:   4    -0.01875202    -0.07348888     0.00695257     0.01516099    -0.18812955    -0.10692895    -0.04235242     0.01005231

 trial vector basis is being transformed.  new dimension:   8

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 12  1   -196.4867898501  1.4270E-08  9.2742E-04  5.6437E-02  1.0000E-04   
 mr-sdci # 12  2   -196.4703364450  6.1439E-06  0.0000E+00  7.3127E-02  1.0000E-04   
 mr-sdci # 12  3   -196.4619004488  7.8480E-06  0.0000E+00  9.7580E-02  1.0000E-04   
 mr-sdci # 12  4   -196.4601959571  9.8038E-04  0.0000E+00  1.2728E-01  1.0000E-04   
 mr-sdci # 12  5   -196.4591183402  1.3240E-03  0.0000E+00  2.4938E-01  1.0000E-04   
 mr-sdci # 12  6   -196.1646793416  1.0714E-01  0.0000E+00  1.0147E+00  1.0000E-04   
 mr-sdci # 12  7   -194.5564253702  8.3696E-01  0.0000E+00  3.4261E+00  1.0000E-04   
 mr-sdci # 12  8   -193.2823697083  1.4579E-02  0.0000E+00  2.7345E+00  1.0000E-04   
 mr-sdci # 12  9   -193.1429521557  8.5037E-02  0.0000E+00  2.7615E+00  1.0000E-04   
 mr-sdci # 12 10   -193.0122183390  3.1333E-02  0.0000E+00  2.7726E+00  1.0000E-04   
 mr-sdci # 12 11   -192.9808845882  2.5508E+00  0.0000E+00  2.9217E+00  1.0000E-04   
 mr-sdci # 12 12   -190.3935458486  1.9474E-01  0.0000E+00  4.9557E+00  1.0000E-04   
 mr-sdci # 12 13   -190.1196059314  5.3924E-04  0.0000E+00  5.6448E+00  1.0000E-04   
 mr-sdci # 12 14   -190.1171580769  1.2762E-01  0.0000E+00  5.2565E+00  1.0000E-04   
 mr-sdci # 12 15   -189.9618906866  7.7809E-02  0.0000E+00  4.9935E+00  1.0000E-04   
 mr-sdci # 12 16   -189.8778558846  4.3990E+01  0.0000E+00  4.8603E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.013000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  13

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59847505
   ht   2     0.00000000   -50.58202165
   ht   3     0.00000000     0.00000000   -50.57358565
   ht   4     0.00000000     0.00000000     0.00000000   -50.57188116
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57080354
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.27636455
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.66811057
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -47.39405491
   ht   9    -0.02756420     0.00001291     0.00073472    -0.00597598    -0.00023268     0.01011373    -0.00287437     0.04676117

                ht   9
   ht   9    -0.01792034

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   1.195483E-03  -0.972218      -1.210569E-02  -7.227519E-02  -2.587906E-02  -2.855617E-05  -7.289504E-03   1.893442E-03
 ref    2  -9.712730E-04  -4.169795E-02   0.347240       0.198604       0.889048       1.281541E-02  -2.136403E-03  -2.142955E-03
 ref    3  -2.016092E-03   5.609755E-02  -6.486866E-02  -0.850327       0.213581       0.374469       0.225164       3.695780E-02
 ref    4   0.956172       1.230642E-03   1.217429E-04  -5.149729E-04   1.231053E-03   1.234432E-03  -2.516193E-03   3.283428E-02

              v      9
 ref    1   2.852353E-03
 ref    2  -1.775326E-03
 ref    3   4.180454E-02
 ref    4   0.176088    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.914271       0.950095       0.124930       0.767723       0.836695       0.140393       5.076267E-02   2.452146E-03

              v      9
 ref    1   3.276586E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00119548    -0.97221809    -0.01210569    -0.07227519    -0.02587906    -0.00002856    -0.00728950     0.00189344
 ref:   2    -0.00097127    -0.04169795     0.34723999     0.19860438     0.88904809     0.01281541    -0.00213640    -0.00214296
 ref:   3    -0.00201609     0.05609755    -0.06486866    -0.85032683     0.21358092     0.37446948     0.22516358     0.03695780
 ref:   4     0.95617160     0.00123064     0.00012174    -0.00051497     0.00123105     0.00123443    -0.00251619     0.03283428

                ci   9
 ref:   1     0.00285235
 ref:   2    -0.00177533
 ref:   3     0.04180454
 ref:   4     0.17608791

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 13  1   -196.4877221769  9.3233E-04  0.0000E+00  2.1853E-02  1.0000E-04   
 mr-sdci # 13  2   -196.4703378903  1.4454E-06  1.5675E-03  7.3092E-02  1.0000E-04   
 mr-sdci # 13  3   -196.4619007667  3.1789E-07  0.0000E+00  9.7585E-02  1.0000E-04   
 mr-sdci # 13  4   -196.4601968006  8.4349E-07  0.0000E+00  1.2718E-01  1.0000E-04   
 mr-sdci # 13  5   -196.4591198570  1.5168E-06  0.0000E+00  2.4938E-01  1.0000E-04   
 mr-sdci # 13  6   -196.1648243021  1.4496E-04  0.0000E+00  1.0147E+00  1.0000E-04   
 mr-sdci # 13  7   -194.5590789718  2.6536E-03  0.0000E+00  3.4095E+00  1.0000E-04   
 mr-sdci # 13  8   -194.1733037845  8.9093E-01  0.0000E+00  2.3747E+00  1.0000E-04   
 mr-sdci # 13  9   -193.0418320105 -1.0112E-01  0.0000E+00  2.6672E+00  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.006000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  14

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59847505
   ht   2     0.00000000   -50.58202165
   ht   3     0.00000000     0.00000000   -50.57358565
   ht   4     0.00000000     0.00000000     0.00000000   -50.57188116
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57080354
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.27636455
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.66811057
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -47.39405491
   ht   9    -0.02756420     0.00001291     0.00073472    -0.00597598    -0.00023268     0.01011373    -0.00287437     0.04676117
   ht  10     0.01206290    -0.12456576     0.00510560    -0.03641846    -0.00161475     0.01664813    -0.08344307    -0.01934028

                ht   9         ht  10
   ht   9    -0.01792034
   ht  10     0.00010634    -0.04029996

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -2.354478E-05   0.969444      -2.525452E-03   5.392411E-02  -4.930835E-02   3.538842E-03   0.100712       5.662439E-02
 ref    2   1.090320E-03   5.270276E-02  -0.342761      -0.171870       0.895743       1.312774E-02   8.494169E-04   2.529976E-03
 ref    3   2.042852E-03  -3.695085E-02   7.483451E-02   0.855888       0.190537       0.374857       0.101995      -0.199495    
 ref    4  -0.956174      -1.557249E-04  -4.613047E-05   6.257119E-04   1.313691E-03   1.279201E-03   7.410056E-03   7.739370E-03

              v      9       v     10
 ref    1   7.977312E-03  -6.563004E-04
 ref    2   2.280491E-03  -1.788432E-03
 ref    3  -4.169143E-02   4.236324E-02
 ref    4  -3.173758E-02   0.175977    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.914274       0.943965       0.123092       0.764992       0.841092       0.140705       2.060159E-02   4.307086E-02

              v      9       v     10
 ref    1   2.814287E-03   3.276613E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00002354     0.96944439    -0.00252545     0.05392411    -0.04930835     0.00353884     0.10071216     0.05662439
 ref:   2     0.00109032     0.05270276    -0.34276116    -0.17186955     0.89574251     0.01312774     0.00084942     0.00252998
 ref:   3     0.00204285    -0.03695085     0.07483451     0.85588821     0.19053742     0.37485746     0.10199518    -0.19949495
 ref:   4    -0.95617407    -0.00015572    -0.00004613     0.00062571     0.00131369     0.00127920     0.00741006     0.00773937

                ci   9         ci  10
 ref:   1     0.00797731    -0.00065630
 ref:   2     0.00228049    -0.00178843
 ref:   3    -0.04169143     0.04236324
 ref:   4    -0.03173758     0.17597688

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 14  1   -196.4877224224  2.4544E-07  0.0000E+00  2.1860E-02  1.0000E-04   
 mr-sdci # 14  2   -196.4720552452  1.7174E-03  0.0000E+00  3.5037E-02  1.0000E-04   
 mr-sdci # 14  3   -196.4619121306  1.1364E-05  2.6133E-03  9.7928E-02  1.0000E-04   
 mr-sdci # 14  4   -196.4602180455  2.1245E-05  0.0000E+00  1.2514E-01  1.0000E-04   
 mr-sdci # 14  5   -196.4591599563  4.0099E-05  0.0000E+00  2.4925E-01  1.0000E-04   
 mr-sdci # 14  6   -196.1650994465  2.7514E-04  0.0000E+00  1.0146E+00  1.0000E-04   
 mr-sdci # 14  7   -194.7486869436  1.8961E-01  0.0000E+00  2.4871E+00  1.0000E-04   
 mr-sdci # 14  8   -194.5119239872  3.3862E-01  0.0000E+00  3.3430E+00  1.0000E-04   
 mr-sdci # 14  9   -194.1692397238  1.1274E+00  0.0000E+00  2.3890E+00  1.0000E-04   
 mr-sdci # 14 10   -193.0398551117  2.7637E-02  0.0000E+00  2.6676E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.006000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  15

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59847505
   ht   2     0.00000000   -50.58202165
   ht   3     0.00000000     0.00000000   -50.57358565
   ht   4     0.00000000     0.00000000     0.00000000   -50.57188116
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57080354
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.27636455
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.66811057
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -47.39405491
   ht   9    -0.02756420     0.00001291     0.00073472    -0.00597598    -0.00023268     0.01011373    -0.00287437     0.04676117
   ht  10     0.01206290    -0.12456576     0.00510560    -0.03641846    -0.00161475     0.01664813    -0.08344307    -0.01934028
   ht  11    -0.00909437    -0.03686252    -0.09083954    -0.00238862    -0.07986077     0.00653002    -0.03267167    -0.00470766

                ht   9         ht  10         ht  11
   ht   9    -0.01792034
   ht  10     0.00010634    -0.04029996
   ht  11    -0.00013660    -0.00046976    -0.06127552

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   2.503127E-05   0.968095       6.176078E-02  -6.322511E-02  -1.223861E-02  -3.591686E-03  -0.100711       5.662408E-02
 ref    2  -1.271786E-03   6.155627E-02  -0.250976       0.813666      -0.474886      -1.234375E-02  -9.473532E-04   2.232192E-03
 ref    3  -2.059723E-03  -3.600729E-02  -3.681645E-02  -0.452976      -0.753676      -0.374729      -0.101964      -0.199376    
 ref    4   0.956174      -1.132691E-04  -9.517849E-04   4.659165E-04  -1.292009E-03  -1.271047E-03  -7.391794E-03   7.803307E-03

              v      9       v     10       v     11
 ref    1   1.294671E-03  -7.895379E-03  -6.857827E-04
 ref    2   3.396992E-02   2.660615E-03   4.555473E-04
 ref    3  -1.668189E-02   3.984357E-02   4.206419E-02
 ref    4  -1.110246E-02   3.035476E-02   0.175886    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.914274       0.942293       6.815955E-02   0.871238       0.793696       0.140588       2.059492E-02   4.302302E-02

              v      9       v     10       v     11
 ref    1   1.557182E-03   2.578337E-03   3.270587E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00002503     0.96809460     0.06176078    -0.06322511    -0.01223861    -0.00359169    -0.10071142     0.05662408
 ref:   2    -0.00127179     0.06155627    -0.25097570     0.81366630    -0.47488639    -0.01234375    -0.00094735     0.00223219
 ref:   3    -0.00205972    -0.03600729    -0.03681645    -0.45297581    -0.75367564    -0.37472869    -0.10196368    -0.19937617
 ref:   4     0.95617376    -0.00011327    -0.00095178     0.00046592    -0.00129201    -0.00127105    -0.00739179     0.00780331

                ci   9         ci  10         ci  11
 ref:   1     0.00129467    -0.00789538    -0.00068578
 ref:   2     0.03396992     0.00266062     0.00045555
 ref:   3    -0.01668189     0.03984357     0.04206419
 ref:   4    -0.01110246     0.03035476     0.17588576

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 15  1   -196.4877224407  1.8300E-08  0.0000E+00  2.1858E-02  1.0000E-04   
 mr-sdci # 15  2   -196.4720622572  7.0120E-06  0.0000E+00  3.5130E-02  1.0000E-04   
 mr-sdci # 15  3   -196.4664071420  4.4950E-03  0.0000E+00  1.2440E-01  1.0000E-04   
 mr-sdci # 15  4   -196.4605268150  3.0877E-04  6.4993E-03  1.6642E-01  1.0000E-04   
 mr-sdci # 15  5   -196.4600588547  8.9890E-04  0.0000E+00  1.2868E-01  1.0000E-04   
 mr-sdci # 15  6   -196.1651843279  8.4881E-05  0.0000E+00  1.0141E+00  1.0000E-04   
 mr-sdci # 15  7   -194.7486929887  6.0452E-06  0.0000E+00  2.4869E+00  1.0000E-04   
 mr-sdci # 15  8   -194.5119456281  2.1641E-05  0.0000E+00  3.3418E+00  1.0000E-04   
 mr-sdci # 15  9   -194.2759618839  1.0672E-01  0.0000E+00  2.6567E+00  1.0000E-04   
 mr-sdci # 15 10   -194.1671031343  1.1272E+00  0.0000E+00  2.3866E+00  1.0000E-04   
 mr-sdci # 15 11   -193.0364110669  5.5526E-02  0.0000E+00  2.6674E+00  1.0000E-04   
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.006000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  16

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59847505
   ht   2     0.00000000   -50.58202165
   ht   3     0.00000000     0.00000000   -50.57358565
   ht   4     0.00000000     0.00000000     0.00000000   -50.57188116
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57080354
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.27636455
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.66811057
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -47.39405491
   ht   9    -0.02756420     0.00001291     0.00073472    -0.00597598    -0.00023268     0.01011373    -0.00287437     0.04676117
   ht  10     0.01206290    -0.12456576     0.00510560    -0.03641846    -0.00161475     0.01664813    -0.08344307    -0.01934028
   ht  11    -0.00909437    -0.03686252    -0.09083954    -0.00238862    -0.07986077     0.00653002    -0.03267167    -0.00470766
   ht  12    -0.00010046     0.01763268     0.29966226     0.01373164    -0.10630528     0.06973321     0.07023357     0.06057508

                ht   9         ht  10         ht  11         ht  12
   ht   9    -0.01792034
   ht  10     0.00010634    -0.04029996
   ht  11    -0.00013660    -0.00046976    -0.06127552
   ht  12    -0.00013950    -0.00052339     0.00375795    -0.14540864

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -3.910828E-05  -0.969486       6.608395E-02   1.116803E-02  -2.507931E-02  -4.101044E-03  -0.105663       2.723702E-02
 ref    2  -6.500283E-04  -4.721757E-02  -0.717667      -0.584858      -0.302138      -8.331955E-03   1.292327E-02  -4.574683E-02
 ref    3  -2.203834E-03   3.308875E-02   0.132428       0.265720      -0.828211      -0.375611      -8.471513E-02  -0.150909    
 ref    4   0.956175       1.201337E-04  -6.785032E-04   1.340835E-04  -1.161785E-03  -1.292218E-03  -9.404347E-03   1.044261E-02

              v      9       v     10       v     11       v     12
 ref    1   3.981312E-02  -7.396138E-03  -5.227377E-03  -1.565537E-05
 ref    2   4.374358E-02   1.333812E-03  -2.931346E-02   6.821297E-03
 ref    3  -0.139258       3.885235E-02   4.305122E-02   3.473955E-02
 ref    4  -2.459917E-03   3.103981E-02   2.715633E-02   0.173788    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.914275       0.943228       0.536951       0.412791       0.777852       0.141171       1.859678E-02   2.571711E-02

              v      9       v     10       v     11       v     12
 ref    1   2.289747E-02   2.529457E-03   3.477479E-03   3.145556E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00003911    -0.96948603     0.06608395     0.01116803    -0.02507931    -0.00410104    -0.10566300     0.02723702
 ref:   2    -0.00065003    -0.04721757    -0.71766742    -0.58485798    -0.30213841    -0.00833195     0.01292327    -0.04574683
 ref:   3    -0.00220383     0.03308875     0.13242845     0.26571962    -0.82821123    -0.37561073    -0.08471513    -0.15090871
 ref:   4     0.95617470     0.00012013    -0.00067850     0.00013408    -0.00116178    -0.00129222    -0.00940435     0.01044261

                ci   9         ci  10         ci  11         ci  12
 ref:   1     0.03981312    -0.00739614    -0.00522738    -0.00001566
 ref:   2     0.04374358     0.00133381    -0.02931346     0.00682130
 ref:   3    -0.13925815     0.03885235     0.04305122     0.03473955
 ref:   4    -0.00245992     0.03103981     0.02715633     0.17378777

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 16  1   -196.4877224747  3.4024E-08  0.0000E+00  2.1860E-02  1.0000E-04   
 mr-sdci # 16  2   -196.4720631482  8.9098E-07  0.0000E+00  3.5255E-02  1.0000E-04   
 mr-sdci # 16  3   -196.4701518942  3.7448E-03  0.0000E+00  7.5550E-02  1.0000E-04   
 mr-sdci # 16  4   -196.4637953753  3.2686E-03  0.0000E+00  4.0742E-02  1.0000E-04   
 mr-sdci # 16  5   -196.4600791828  2.0328E-05  4.0273E-03  1.1757E-01  1.0000E-04   
 mr-sdci # 16  6   -196.1653873305  2.0300E-04  0.0000E+00  1.0112E+00  1.0000E-04   
 mr-sdci # 16  7   -194.7567301687  8.0372E-03  0.0000E+00  2.4168E+00  1.0000E-04   
 mr-sdci # 16  8   -194.6141410206  1.0220E-01  0.0000E+00  2.9860E+00  1.0000E-04   
 mr-sdci # 16  9   -194.4354028949  1.5944E-01  0.0000E+00  3.0337E+00  1.0000E-04   
 mr-sdci # 16 10   -194.1678310110  7.2788E-04  0.0000E+00  2.3823E+00  1.0000E-04   
 mr-sdci # 16 11   -193.3188064784  2.8240E-01  0.0000E+00  2.9508E+00  1.0000E-04   
 mr-sdci # 16 12   -193.0241322301  2.6306E+00  0.0000E+00  2.6810E+00  1.0000E-04   
 
 root number  5 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.006000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  17

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59847505
   ht   2     0.00000000   -50.58202165
   ht   3     0.00000000     0.00000000   -50.57358565
   ht   4     0.00000000     0.00000000     0.00000000   -50.57188116
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57080354
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.27636455
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.66811057
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -47.39405491
   ht   9    -0.02756420     0.00001291     0.00073472    -0.00597598    -0.00023268     0.01011373    -0.00287437     0.04676117
   ht  10     0.01206290    -0.12456576     0.00510560    -0.03641846    -0.00161475     0.01664813    -0.08344307    -0.01934028
   ht  11    -0.00909437    -0.03686252    -0.09083954    -0.00238862    -0.07986077     0.00653002    -0.03267167    -0.00470766
   ht  12    -0.00010046     0.01763268     0.29966226     0.01373164    -0.10630528     0.06973321     0.07023357     0.06057508
   ht  13     0.02622422     0.06734180    -0.11391243    -0.12834445     0.01350054     0.14379311     0.05617426     0.05401759

                ht   9         ht  10         ht  11         ht  12         ht  13
   ht   9    -0.01792034
   ht  10     0.00010634    -0.04029996
   ht  11    -0.00013660    -0.00046976    -0.06127552
   ht  12    -0.00013950    -0.00052339     0.00375795    -0.14540864
   ht  13    -0.00037037     0.00071384    -0.00106259    -0.00195941    -0.09460822

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -2.479161E-05   0.966081      -0.105862      -8.855336E-03  -2.039207E-02  -4.563416E-03  -9.865111E-02   4.882523E-02
 ref    2  -4.381234E-06   8.476951E-02   0.752995       0.601032      -0.118999      -1.359944E-02   1.743125E-02  -2.479766E-02
 ref    3   1.479214E-03  -2.076146E-02  -1.139312E-02  -0.160837      -0.862068      -0.382455      -0.100393      -0.155863    
 ref    4  -0.956176      -1.748336E-04   2.665514E-04  -1.271502E-04  -2.250529E-04  -1.333318E-03  -8.420980E-03   1.083509E-02

              v      9       v     10       v     11       v     12       v     13
 ref    1  -3.554451E-02  -6.058709E-03  -1.225598E-02   3.861496E-03   5.764093E-05
 ref    2  -6.334026E-02   1.513562E-03   7.276586E-03   3.351325E-02   4.598907E-03
 ref    3   4.663690E-02   2.905717E-02   0.126960      -2.016590E-02   3.143466E-02
 ref    4   3.819400E-03   3.057751E-02   1.377673E-02  -1.900218E-02   0.174470    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.914274       0.940928       0.578338       0.387186       0.757737       0.146479       2.018556E-02   2.740962E-02

              v      9       v     10       v     11       v     12       v     13
 ref    1   7.464988E-03   1.818302E-03   1.651175E-02   1.905795E-03   3.144895E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00002479     0.96608050    -0.10586193    -0.00885534    -0.02039207    -0.00456342    -0.09865111     0.04882523
 ref:   2    -0.00000438     0.08476951     0.75299526     0.60103160    -0.11899887    -0.01359944     0.01743125    -0.02479766
 ref:   3     0.00147921    -0.02076146    -0.01139312    -0.16083719    -0.86206753    -0.38245481    -0.10039300    -0.15586338
 ref:   4    -0.95617583    -0.00017483     0.00026655    -0.00012715    -0.00022505    -0.00133332    -0.00842098     0.01083509

                ci   9         ci  10         ci  11         ci  12         ci  13
 ref:   1    -0.03554451    -0.00605871    -0.01225598     0.00386150     0.00005764
 ref:   2    -0.06334026     0.00151356     0.00727659     0.03351325     0.00459891
 ref:   3     0.04663690     0.02905717     0.12695980    -0.02016590     0.03143466
 ref:   4     0.00381940     0.03057751     0.01377673    -0.01900218     0.17446966

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 17  1   -196.4877226637  1.8900E-07  0.0000E+00  2.1877E-02  1.0000E-04   
 mr-sdci # 17  2   -196.4720744325  1.1284E-05  0.0000E+00  3.5050E-02  1.0000E-04   
 mr-sdci # 17  3   -196.4706318420  4.7995E-04  0.0000E+00  6.3287E-02  1.0000E-04   
 mr-sdci # 17  4   -196.4638033926  8.0173E-06  0.0000E+00  4.0819E-02  1.0000E-04   
 mr-sdci # 17  5   -196.4633372423  3.2581E-03  0.0000E+00  4.6869E-02  1.0000E-04   
 mr-sdci # 17  6   -196.1672828311  1.8955E-03  5.0520E+00  9.9197E-01  1.0000E-04   
 mr-sdci # 17  7   -194.7643532670  7.6231E-03  0.0000E+00  2.4926E+00  1.0000E-04   
 mr-sdci # 17  8   -194.6366573095  2.2516E-02  0.0000E+00  3.0440E+00  1.0000E-04   
 mr-sdci # 17  9   -194.5245874496  8.9185E-02  0.0000E+00  2.6908E+00  1.0000E-04   
 mr-sdci # 17 10   -194.1686616641  8.3065E-04  0.0000E+00  2.3685E+00  1.0000E-04   
 mr-sdci # 17 11   -193.8801730444  5.6137E-01  0.0000E+00  2.8427E+00  1.0000E-04   
 mr-sdci # 17 12   -193.2804178695  2.5629E-01  0.0000E+00  2.9672E+00  1.0000E-04   
 mr-sdci # 17 13   -193.0209943157  2.9014E+00  0.0000E+00  2.6794E+00  1.0000E-04   
 
 root number  6 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  18

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59847505
   ht   2     0.00000000   -50.58202165
   ht   3     0.00000000     0.00000000   -50.57358565
   ht   4     0.00000000     0.00000000     0.00000000   -50.57188116
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57080354
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.27636455
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.66811057
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -47.39405491
   ht   9    -0.02756420     0.00001291     0.00073472    -0.00597598    -0.00023268     0.01011373    -0.00287437     0.04676117
   ht  10     0.01206290    -0.12456576     0.00510560    -0.03641846    -0.00161475     0.01664813    -0.08344307    -0.01934028
   ht  11    -0.00909437    -0.03686252    -0.09083954    -0.00238862    -0.07986077     0.00653002    -0.03267167    -0.00470766
   ht  12    -0.00010046     0.01763268     0.29966226     0.01373164    -0.10630528     0.06973321     0.07023357     0.06057508
   ht  13     0.02622422     0.06734180    -0.11391243    -0.12834445     0.01350054     0.14379311     0.05617426     0.05401759
   ht  14    -6.52194418   -78.15669138   -77.65103338   649.38024572   197.51272155 -1295.76691653   708.02883051  -108.02739571

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14
   ht   9    -0.01792034
   ht  10     0.00010634    -0.04029996
   ht  11    -0.00013660    -0.00046976    -0.06127552
   ht  12    -0.00013950    -0.00052339     0.00375795    -0.14540864
   ht  13    -0.00037037     0.00071384    -0.00106259    -0.00195941    -0.09460822
   ht  14     0.61645971     2.41843140     1.27435539     0.85884083     4.13631980-57909.65458629

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   4.405987E-05  -0.966043      -0.106764      -1.021834E-02  -1.652612E-02   7.350487E-04  -4.969938E-03   0.111982    
 ref    2   3.081942E-05  -8.556205E-02   0.752939       0.584716      -0.182995       1.222854E-02  -3.421292E-03  -2.216733E-02
 ref    3   2.010180E-03   1.698145E-02  -9.794272E-03  -0.256212      -0.846536       0.382515       6.322450E-02   2.572958E-02
 ref    4  -0.956178       1.182964E-04   2.852389E-04  -2.146799E-04  -4.309621E-04   3.061284E-04  -1.127664E-03   1.115385E-02

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1  -2.705029E-02  -3.052560E-03  -1.245279E-02   2.452759E-03  -4.216833E-03  -1.287456E-02
 ref    2  -6.662194E-02   2.023575E-03   7.266457E-03   3.280275E-02  -7.954445E-03  -3.436073E-03
 ref    3  -2.575480E-04   5.596957E-03   0.128738      -7.377607E-03   1.338509E-02   0.144651    
 ref    4   6.666354E-03   3.204397E-02   1.361773E-02  -3.020082E-02  -0.163522       6.283333E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.914281       0.940848       0.578411       0.407642       0.750384       0.146468       4.035014E-03   1.381781E-02

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1   5.214708E-03   1.071555E-03   1.696668E-02   2.048555E-03   2.699976E-02   2.504959E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00004406    -0.96604266    -0.10676434    -0.01021834    -0.01652612     0.00073505    -0.00496994     0.11198213
 ref:   2     0.00003082    -0.08556205     0.75293872     0.58471637    -0.18299460     0.01222854    -0.00342129    -0.02216733
 ref:   3     0.00201018     0.01698145    -0.00979427    -0.25621213    -0.84653633     0.38251460     0.06322450     0.02572958
 ref:   4    -0.95617824     0.00011830     0.00028524    -0.00021468    -0.00043096     0.00030613    -0.00112766     0.01115385

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14
 ref:   1    -0.02705029    -0.00305256    -0.01245279     0.00245276    -0.00421683    -0.01287456
 ref:   2    -0.06662194     0.00202357     0.00726646     0.03280275    -0.00795445    -0.00343607
 ref:   3    -0.00025755     0.00559696     0.12873759    -0.00737761     0.01338509     0.14465131
 ref:   4     0.00666635     0.03204397     0.01361773    -0.03020082    -0.16352230     0.06283333

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 18  1   -196.4877229516  2.8785E-07  1.9427E-04  2.1869E-02  1.0000E-04   
 mr-sdci # 18  2   -196.4720782647  3.8321E-06  0.0000E+00  3.5072E-02  1.0000E-04   
 mr-sdci # 18  3   -196.4706323470  5.0498E-07  0.0000E+00  6.3334E-02  1.0000E-04   
 mr-sdci # 18  4   -196.4638121858  8.7932E-06  0.0000E+00  4.1504E-02  1.0000E-04   
 mr-sdci # 18  5   -196.4635215604  1.8432E-04  0.0000E+00  5.1207E-02  1.0000E-04   
 mr-sdci # 18  6   -196.2660686401  9.8786E-02  0.0000E+00  1.2166E+00  1.0000E-04   
 mr-sdci # 18  7   -195.3368716485  5.7252E-01  0.0000E+00  1.8594E+00  1.0000E-04   
 mr-sdci # 18  8   -194.7427827420  1.0613E-01  0.0000E+00  2.3550E+00  1.0000E-04   
 mr-sdci # 18  9   -194.5326891436  8.1017E-03  0.0000E+00  2.6117E+00  1.0000E-04   
 mr-sdci # 18 10   -194.1757863723  7.1247E-03  0.0000E+00  2.3375E+00  1.0000E-04   
 mr-sdci # 18 11   -193.8802361025  6.3058E-05  0.0000E+00  2.8547E+00  1.0000E-04   
 mr-sdci # 18 12   -193.2859244061  5.5065E-03  0.0000E+00  2.9556E+00  1.0000E-04   
 mr-sdci # 18 13   -193.0836178304  6.2624E-02  0.0000E+00  2.7662E+00  1.0000E-04   
 mr-sdci # 18 14   -191.3099079797  1.1927E+00  0.0000E+00  4.9282E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.017000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  19

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59847505
   ht   2     0.00000000   -50.58202165
   ht   3     0.00000000     0.00000000   -50.57358565
   ht   4     0.00000000     0.00000000     0.00000000   -50.57188116
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57080354
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.27636455
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.66811057
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -47.39405491
   ht   9    -0.02756420     0.00001291     0.00073472    -0.00597598    -0.00023268     0.01011373    -0.00287437     0.04676117
   ht  10     0.01206290    -0.12456576     0.00510560    -0.03641846    -0.00161475     0.01664813    -0.08344307    -0.01934028
   ht  11    -0.00909437    -0.03686252    -0.09083954    -0.00238862    -0.07986077     0.00653002    -0.03267167    -0.00470766
   ht  12    -0.00010046     0.01763268     0.29966226     0.01373164    -0.10630528     0.06973321     0.07023357     0.06057508
   ht  13     0.02622422     0.06734180    -0.11391243    -0.12834445     0.01350054     0.14379311     0.05617426     0.05401759
   ht  14    -6.52194418   -78.15669138   -77.65103338   649.38024572   197.51272155 -1295.76691653   708.02883051  -108.02739571
   ht  15    -0.27393011    -0.01608617     0.00324024     0.03538075     0.01300962     0.00796805    -0.01238818    -0.04657249

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15
   ht   9    -0.01792034
   ht  10     0.00010634    -0.04029996
   ht  11    -0.00013660    -0.00046976    -0.06127552
   ht  12    -0.00013950    -0.00052339     0.00375795    -0.14540864
   ht  13    -0.00037037     0.00071384    -0.00106259    -0.00195941    -0.09460822
   ht  14     0.61645971     2.41843140     1.27435539     0.85884083     4.13631980-57909.65458629
   ht  15     0.00210299     0.00006452    -0.00020322     0.00020694    -0.00006648     0.00541199    -0.00707869

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -2.806205E-04   0.966051       0.106676       1.022693E-02   1.654952E-02   6.981593E-04  -8.256282E-03   2.917667E-03
 ref    2   2.140624E-04   8.549405E-02  -0.752948      -0.584680       0.183099       1.225287E-02   3.695700E-03   4.515029E-03
 ref    3  -1.876801E-03  -1.699555E-02   9.767763E-03   0.256402       0.846471       0.382536       7.373707E-03  -6.325043E-02
 ref    4   0.955079       1.496320E-04   8.642538E-05   1.460153E-04   5.806833E-05   1.284077E-03   6.407234E-02   1.385706E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1  -0.112169       2.545228E-02  -1.246432E-02   2.164932E-03  -4.559637E-03   3.885223E-03  -1.227380E-02
 ref    2   2.095859E-02   6.682827E-02   7.226534E-03   3.273359E-02  -8.625453E-03  -6.074598E-04  -3.907615E-03
 ref    3  -2.484676E-02   4.903545E-04   0.128792      -6.454820E-03   1.463766E-02  -2.461609E-02   0.143314    
 ref    4  -1.296266E-02  -6.816073E-03   1.493420E-02  -4.133951E-02  -0.171937       7.399765E-02   9.231340E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.912180       0.940852       0.578406       0.407698       0.750312       0.146486       4.241460E-03   4.221533E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1   1.380654E-02   5.160536E-03   1.701799E-02   2.826795E-03   2.987194E-02   6.097067E-03   2.922663E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00028062     0.96605084     0.10667620     0.01022693     0.01654952     0.00069816    -0.00825628     0.00291767
 ref:   2     0.00021406     0.08549405    -0.75294818    -0.58468017     0.18309946     0.01225287     0.00369570     0.00451503
 ref:   3    -0.00187680    -0.01699555     0.00976776     0.25640209     0.84647077     0.38253568     0.00737371    -0.06325043
 ref:   4     0.95507920     0.00014963     0.00008643     0.00014602     0.00005807     0.00128408     0.06407234     0.01385706

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15
 ref:   1    -0.11216902     0.02545228    -0.01246432     0.00216493    -0.00455964     0.00388522    -0.01227380
 ref:   2     0.02095859     0.06682827     0.00722653     0.03273359    -0.00862545    -0.00060746    -0.00390761
 ref:   3    -0.02484676     0.00049035     0.12879201    -0.00645482     0.01463766    -0.02461609     0.14331416
 ref:   4    -0.01296266    -0.00681607     0.01493420    -0.04133951    -0.17193745     0.07399765     0.09231340

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 19  1   -196.4879120151  1.8906E-04  0.0000E+00  1.5452E-02  1.0000E-04   
 mr-sdci # 19  2   -196.4720783504  8.5708E-08  4.5949E-04  3.5049E-02  1.0000E-04   
 mr-sdci # 19  3   -196.4706325404  1.9346E-07  0.0000E+00  6.3322E-02  1.0000E-04   
 mr-sdci # 19  4   -196.4638121978  1.1975E-08  0.0000E+00  4.1512E-02  1.0000E-04   
 mr-sdci # 19  5   -196.4635219204  3.5998E-07  0.0000E+00  5.1209E-02  1.0000E-04   
 mr-sdci # 19  6   -196.2661133962  4.4756E-05  0.0000E+00  1.2163E+00  1.0000E-04   
 mr-sdci # 19  7   -195.4648469948  1.2798E-01  0.0000E+00  1.1692E+00  1.0000E-04   
 mr-sdci # 19  8   -195.3307275329  5.8794E-01  0.0000E+00  1.8602E+00  1.0000E-04   
 mr-sdci # 19  9   -194.7382767410  2.0559E-01  0.0000E+00  2.3561E+00  1.0000E-04   
 mr-sdci # 19 10   -194.5304986362  3.5471E-01  0.0000E+00  2.6146E+00  1.0000E-04   
 mr-sdci # 19 11   -193.8803773917  1.4129E-04  0.0000E+00  2.8523E+00  1.0000E-04   
 mr-sdci # 19 12   -193.2902065980  4.2822E-03  0.0000E+00  2.9602E+00  1.0000E-04   
 mr-sdci # 19 13   -193.0902306802  6.6128E-03  0.0000E+00  2.8205E+00  1.0000E-04   
 mr-sdci # 19 14   -192.4662669805  1.1564E+00  0.0000E+00  2.8542E+00  1.0000E-04   
 mr-sdci # 19 15   -191.2048115813  1.2429E+00  0.0000E+00  4.8750E+00  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.008000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  20

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59847505
   ht   2     0.00000000   -50.58202165
   ht   3     0.00000000     0.00000000   -50.57358565
   ht   4     0.00000000     0.00000000     0.00000000   -50.57188116
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57080354
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.27636455
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.66811057
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -47.39405491
   ht   9    -0.02756420     0.00001291     0.00073472    -0.00597598    -0.00023268     0.01011373    -0.00287437     0.04676117
   ht  10     0.01206290    -0.12456576     0.00510560    -0.03641846    -0.00161475     0.01664813    -0.08344307    -0.01934028
   ht  11    -0.00909437    -0.03686252    -0.09083954    -0.00238862    -0.07986077     0.00653002    -0.03267167    -0.00470766
   ht  12    -0.00010046     0.01763268     0.29966226     0.01373164    -0.10630528     0.06973321     0.07023357     0.06057508
   ht  13     0.02622422     0.06734180    -0.11391243    -0.12834445     0.01350054     0.14379311     0.05617426     0.05401759
   ht  14    -6.52194418   -78.15669138   -77.65103338   649.38024572   197.51272155 -1295.76691653   708.02883051  -108.02739571
   ht  15    -0.27393011    -0.01608617     0.00324024     0.03538075     0.01300962     0.00796805    -0.01238818    -0.04657249
   ht  16    -0.06412214    -0.37252631     0.08697565    -0.02961605    -0.08745172    -0.00871039     0.00698442    -0.01912877

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15         ht  16
   ht   9    -0.01792034
   ht  10     0.00010634    -0.04029996
   ht  11    -0.00013660    -0.00046976    -0.06127552
   ht  12    -0.00013950    -0.00052339     0.00375795    -0.14540864
   ht  13    -0.00037037     0.00071384    -0.00106259    -0.00195941    -0.09460822
   ht  14     0.61645971     2.41843140     1.27435539     0.85884083     4.13631980-57909.65458629
   ht  15     0.00210299     0.00006452    -0.00020322     0.00020694    -0.00006648     0.00541199    -0.00707869
   ht  16    -0.00004797     0.00468726    -0.00043948     0.00022027     0.00062728     0.02218046    -0.00018222    -0.01531057

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   3.503376E-04  -0.962997       0.113457      -1.950510E-03   4.565913E-03  -1.677429E-03  -0.146326      -2.066204E-02
 ref    2   2.573307E-04  -8.763098E-02  -0.752246       0.573947       0.215907      -1.233338E-02  -6.553515E-03   3.334337E-03
 ref    3  -1.792070E-03   5.553950E-03   9.318236E-03  -0.305803       0.829892      -0.382744      -2.863540E-02   5.036773E-03
 ref    4   0.955086       2.566649E-04   9.485879E-05  -1.209449E-04   1.465718E-05  -1.240687E-03  -2.078308E-03   6.392301E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1  -7.176694E-04   7.766926E-03  -1.484656E-02   2.826117E-03   1.158402E-03   1.585883E-03   2.153365E-03  -1.376062E-02
 ref    2   4.492439E-03   7.017799E-02   8.087456E-03  -3.665788E-02   8.320878E-03  -1.056241E-02  -1.016011E-02  -5.378987E-03
 ref    3  -6.397295E-02  -3.230756E-03   0.128006       1.096309E-02  -1.603616E-02   1.243366E-02  -2.300952E-02   0.142960    
 ref    4   1.393007E-02  -9.485728E-03   1.426930E-02   3.170686E-02   0.169439       5.245744E-03   8.902096E-02   9.592320E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.912193       0.935073       0.578834       0.422935       0.735357       0.146650       2.227869E-02   4.549558E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   4.307282E-03   5.085692E-03   1.687492E-02   2.477301E-03   2.903724E-02   2.961933E-04   8.562034E-03   2.985707E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15        ev   16

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00035034    -0.96299660     0.11345684    -0.00195051     0.00456591    -0.00167743    -0.14632646    -0.02066204
 ref:   2     0.00025733    -0.08763098    -0.75224629     0.57394715     0.21590737    -0.01233338    -0.00655351     0.00333434
 ref:   3    -0.00179207     0.00555395     0.00931824    -0.30580285     0.82989177    -0.38274414    -0.02863540     0.00503677
 ref:   4     0.95508610     0.00025666     0.00009486    -0.00012094     0.00001466    -0.00124069    -0.00207831     0.06392301

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1    -0.00071767     0.00776693    -0.01484656     0.00282612     0.00115840     0.00158588     0.00215336    -0.01376062
 ref:   2     0.00449244     0.07017799     0.00808746    -0.03665788     0.00832088    -0.01056241    -0.01016011    -0.00537899
 ref:   3    -0.06397295    -0.00323076     0.12800578     0.01096309    -0.01603616     0.01243366    -0.02300952     0.14295986
 ref:   4     0.01393007    -0.00948573     0.01426930     0.03170686     0.16943880     0.00524574     0.08902096     0.09592320

 trial vector basis is being transformed.  new dimension:   8

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 20  1   -196.4879122937  2.7851E-07  0.0000E+00  1.5367E-02  1.0000E-04   
 mr-sdci # 20  2   -196.4725282913  4.4994E-04  0.0000E+00  1.6878E-02  1.0000E-04   
 mr-sdci # 20  3   -196.4706328363  2.9586E-07  1.7896E-03  6.3259E-02  1.0000E-04   
 mr-sdci # 20  4   -196.4638246476  1.2450E-05  0.0000E+00  4.1374E-02  1.0000E-04   
 mr-sdci # 20  5   -196.4635445689  2.2649E-05  0.0000E+00  5.0506E-02  1.0000E-04   
 mr-sdci # 20  6   -196.2661304044  1.7008E-05  0.0000E+00  1.2166E+00  1.0000E-04   
 mr-sdci # 20  7   -195.5880716556  1.2322E-01  0.0000E+00  8.4942E-01  1.0000E-04   
 mr-sdci # 20  8   -195.4640255271  1.3330E-01  0.0000E+00  1.1742E+00  1.0000E-04   
 mr-sdci # 20  9   -195.3305996084  5.9232E-01  0.0000E+00  1.8610E+00  1.0000E-04   
 mr-sdci # 20 10   -194.5373841865  6.8856E-03  0.0000E+00  2.6184E+00  1.0000E-04   
 mr-sdci # 20 11   -193.8816053804  1.2280E-03  0.0000E+00  2.8585E+00  1.0000E-04   
 mr-sdci # 20 12   -193.3138824352  2.3676E-02  0.0000E+00  3.0000E+00  1.0000E-04   
 mr-sdci # 20 13   -193.1063915254  1.6161E-02  0.0000E+00  2.8329E+00  1.0000E-04   
 mr-sdci # 20 14   -192.6325900086  1.6632E-01  0.0000E+00  2.8819E+00  1.0000E-04   
 mr-sdci # 20 15   -192.3744081974  1.1696E+00  0.0000E+00  3.0190E+00  1.0000E-04   
 mr-sdci # 20 16   -191.1978065352  1.3200E+00  0.0000E+00  4.8785E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.012000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  21

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59959750
   ht   2     0.00000000   -50.58421350
   ht   3     0.00000000     0.00000000   -50.58231804
   ht   4     0.00000000     0.00000000     0.00000000   -50.57550985
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57522977
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.37781561
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.69975686
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.57571073
   ht   9    -0.05743623    -0.01601529     1.32354527     0.10457565     0.07491561     0.00389529    -0.02145345    -0.01493939

                ht   9
   ht   9    -0.08856653

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -3.880086E-04   0.966459       7.751620E-02   1.907169E-03  -4.484195E-03  -1.646583E-03   0.147428       1.965707E-02
 ref    2  -4.063360E-05   6.026701E-02  -0.751983      -0.573685      -0.216405      -1.247629E-02  -6.187886E-03  -4.825487E-03
 ref    3   1.788571E-03  -5.181046E-03   1.008945E-02   0.305827      -0.829876      -0.382740       2.861697E-02  -5.252550E-03
 ref    4  -0.955087      -2.582009E-04   2.115854E-05   1.209419E-04  -1.465651E-05  -1.244331E-03   9.922053E-04  -6.400809E-02

              v      9
 ref    1  -1.674164E-03
 ref    2   0.136988    
 ref    3   1.675003E-03
 ref    4   7.742957E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.912194       0.937702       0.571589       0.422648       0.735545       0.146650       2.259332E-02   4.534310E-03

              v      9
 ref    1   1.883125E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00038801     0.96645886     0.07751620     0.00190717    -0.00448420    -0.00164658     0.14742835     0.01965707
 ref:   2    -0.00004063     0.06026701    -0.75198332    -0.57368481    -0.21640489    -0.01247629    -0.00618789    -0.00482549
 ref:   3     0.00178857    -0.00518105     0.01008945     0.30582715    -0.82987551    -0.38274046     0.02861697    -0.00525255
 ref:   4    -0.95508694    -0.00025820     0.00002116     0.00012094    -0.00001466    -0.00124433     0.00099221    -0.06400809

                ci   9
 ref:   1    -0.00167416
 ref:   2     0.13698790
 ref:   3     0.00167500
 ref:   4     0.00774296

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 21  1   -196.4879123139  2.0257E-08  0.0000E+00  1.5358E-02  1.0000E-04   
 mr-sdci # 21  2   -196.4725295335  1.2422E-06  0.0000E+00  1.6476E-02  1.0000E-04   
 mr-sdci # 21  3   -196.4719044486  1.2716E-03  0.0000E+00  4.3490E-02  1.0000E-04   
 mr-sdci # 21  4   -196.4638246523  4.7823E-09  5.8729E-04  4.1365E-02  1.0000E-04   
 mr-sdci # 21  5   -196.4635445870  1.8083E-08  0.0000E+00  5.0497E-02  1.0000E-04   
 mr-sdci # 21  6   -196.2661308295  4.2506E-07  0.0000E+00  1.2166E+00  1.0000E-04   
 mr-sdci # 21  7   -195.5964272238  8.3556E-03  0.0000E+00  7.8889E-01  1.0000E-04   
 mr-sdci # 21  8   -195.4641376061  1.1208E-04  0.0000E+00  1.1737E+00  1.0000E-04   
 mr-sdci # 21  9   -194.1773281099 -1.1533E+00  0.0000E+00  2.6787E+00  1.0000E-04   
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  22

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59959750
   ht   2     0.00000000   -50.58421350
   ht   3     0.00000000     0.00000000   -50.58231804
   ht   4     0.00000000     0.00000000     0.00000000   -50.57550985
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57522977
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.37781561
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.69975686
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.57571073
   ht   9    -0.05743623    -0.01601529     1.32354527     0.10457565     0.07491561     0.00389529    -0.02145345    -0.01493939
   ht  10    -0.04259404     0.04519231     0.01955114    -0.44208540    -0.23281998     0.03192280     0.01134944    -0.00104599

                ht   9         ht  10
   ht   9    -0.08856653
   ht  10     0.00053927    -0.02061936

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -4.052587E-04   0.966538      -7.656199E-02   1.573264E-05   4.519951E-03   1.577323E-03   0.147079       1.885764E-02
 ref    2   1.389912E-04   5.855449E-02   0.753474      -0.573360       0.209198       1.490369E-02  -9.530267E-03  -5.991916E-03
 ref    3   1.691769E-03  -4.656489E-03  -1.085313E-02   0.295776       0.833630       0.381625       3.044668E-02  -4.747213E-03
 ref    4  -0.955091      -2.646438E-04  -1.337469E-05   7.402668E-05   1.643936E-05   1.461739E-03   1.763602E-04  -6.421175E-02

              v      9       v     10
 ref    1   1.206476E-02   1.081040E-02
 ref    2   0.143722      -6.403244E-02
 ref    3  -2.777323E-02  -2.249367E-02
 ref    4   1.690231E-02   2.986804E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.912202       0.937646       0.573702       0.416225       0.738723       0.145864       2.265015E-02   4.537199E-03

              v      9       v     10
 ref    1   2.185851E-02   4.731904E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00040526     0.96653815    -0.07656199     0.00001573     0.00451995     0.00157732     0.14707920     0.01885764
 ref:   2     0.00013899     0.05855449     0.75347355    -0.57336015     0.20919833     0.01490369    -0.00953027    -0.00599192
 ref:   3     0.00169177    -0.00465649    -0.01085313     0.29577599     0.83362992     0.38162487     0.03044668    -0.00474721
 ref:   4    -0.95509103    -0.00026464    -0.00001337     0.00007403     0.00001644     0.00146174     0.00017636    -0.06421175

                ci   9         ci  10
 ref:   1     0.01206476     0.01081040
 ref:   2     0.14372163    -0.06403244
 ref:   3    -0.02777323    -0.02249367
 ref:   4     0.01690231     0.00298680

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 22  1   -196.4879124348  1.2089E-07  0.0000E+00  1.5320E-02  1.0000E-04   
 mr-sdci # 22  2   -196.4725299863  4.5281E-07  0.0000E+00  1.6393E-02  1.0000E-04   
 mr-sdci # 22  3   -196.4719052965  8.4788E-07  0.0000E+00  4.3445E-02  1.0000E-04   
 mr-sdci # 22  4   -196.4643024129  4.7776E-04  0.0000E+00  2.2044E-02  1.0000E-04   
 mr-sdci # 22  5   -196.4635446564  6.9367E-08  7.8083E-04  5.0549E-02  1.0000E-04   
 mr-sdci # 22  6   -196.2665322675  4.0144E-04  0.0000E+00  1.2078E+00  1.0000E-04   
 mr-sdci # 22  7   -195.5980575824  1.6304E-03  0.0000E+00  7.8183E-01  1.0000E-04   
 mr-sdci # 22  8   -195.4643400661  2.0246E-04  0.0000E+00  1.1724E+00  1.0000E-04   
 mr-sdci # 22  9   -194.2572149947  7.9887E-02  0.0000E+00  2.8423E+00  1.0000E-04   
 mr-sdci # 22 10   -194.1330246493 -4.0436E-01  0.0000E+00  2.6233E+00  1.0000E-04   
 
 root number  5 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.006000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  23

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59959750
   ht   2     0.00000000   -50.58421350
   ht   3     0.00000000     0.00000000   -50.58231804
   ht   4     0.00000000     0.00000000     0.00000000   -50.57550985
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57522977
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.37781561
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.69975686
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.57571073
   ht   9    -0.05743623    -0.01601529     1.32354527     0.10457565     0.07491561     0.00389529    -0.02145345    -0.01493939
   ht  10    -0.04259404     0.04519231     0.01955114    -0.44208540    -0.23281998     0.03192280     0.01134944    -0.00104599
   ht  11     0.01617466     0.05433301     0.09312726     0.20821358     0.17159217    -0.01298877     0.03306873     0.00575025

                ht   9         ht  10         ht  11
   ht   9    -0.08856653
   ht  10     0.00053927    -0.02061936
   ht  11    -0.00311526     0.00267441    -0.02289054

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   4.020907E-04   0.966322      -7.941101E-02   2.070533E-04  -1.964320E-03  -1.563242E-03   0.147026       1.815209E-02
 ref    2  -1.522388E-04   6.141451E-02   0.754366      -0.545022      -0.271226      -1.316706E-02  -9.122737E-03  -4.847740E-03
 ref    3  -1.732774E-03  -2.009250E-03  -6.203662E-03   0.392881      -0.791987      -0.374941       3.233658E-02   3.027169E-05
 ref    4   0.955091      -2.603424E-04  -5.795733E-06   8.552441E-05  -8.133547E-05  -1.506011E-03   1.860122E-05  -6.415215E-02

              v      9       v     10       v     11
 ref    1   1.263836E-02  -1.408198E-02  -5.521330E-03
 ref    2  -6.472696E-02  -0.133906       5.745125E-02
 ref    3  -0.128997       4.342241E-02  -2.847390E-02
 ref    4  -3.866855E-03  -1.673075E-02  -2.888342E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.912202       0.937554       0.575413       0.451404       0.700810       0.140759       2.274548E-02   4.468498E-03

              v      9       v     10       v     11
 ref    1   2.100445E-02   2.029449E-02   4.150237E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00040209     0.96632191    -0.07941101     0.00020705    -0.00196432    -0.00156324     0.14702584     0.01815209
 ref:   2    -0.00015224     0.06141451     0.75436632    -0.54502163    -0.27122575    -0.01316706    -0.00912274    -0.00484774
 ref:   3    -0.00173277    -0.00200925    -0.00620366     0.39288130    -0.79198664    -0.37494123     0.03233658     0.00003027
 ref:   4     0.95509098    -0.00026034    -0.00000580     0.00008552    -0.00008134    -0.00150601     0.00001860    -0.06415215

                ci   9         ci  10         ci  11
 ref:   1     0.01263836    -0.01408198    -0.00552133
 ref:   2    -0.06472696    -0.13390581     0.05745125
 ref:   3    -0.12899687     0.04342241    -0.02847390
 ref:   4    -0.00386685    -0.01673075    -0.00288834

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 23  1   -196.4879124370  2.1983E-09  0.0000E+00  1.5320E-02  1.0000E-04   
 mr-sdci # 23  2   -196.4725311397  1.1534E-06  0.0000E+00  1.6219E-02  1.0000E-04   
 mr-sdci # 23  3   -196.4719082836  2.9871E-06  0.0000E+00  4.3238E-02  1.0000E-04   
 mr-sdci # 23  4   -196.4643038527  1.4398E-06  0.0000E+00  2.1477E-02  1.0000E-04   
 mr-sdci # 23  5   -196.4642141555  6.6950E-04  0.0000E+00  2.3868E-02  1.0000E-04   
 mr-sdci # 23  6   -196.2676460198  1.1138E-03  2.4139E-01  1.1736E+00  1.0000E-04   
 mr-sdci # 23  7   -195.5982036330  1.4605E-04  0.0000E+00  7.8129E-01  1.0000E-04   
 mr-sdci # 23  8   -195.4652926129  9.5255E-04  0.0000E+00  1.1624E+00  1.0000E-04   
 mr-sdci # 23  9   -194.4100388985  1.5282E-01  0.0000E+00  2.4495E+00  1.0000E-04   
 mr-sdci # 23 10   -194.2549470248  1.2192E-01  0.0000E+00  2.8421E+00  1.0000E-04   
 mr-sdci # 23 11   -194.0908645943  2.0926E-01  0.0000E+00  2.6660E+00  1.0000E-04   
 
 root number  6 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.006000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  24

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59959750
   ht   2     0.00000000   -50.58421350
   ht   3     0.00000000     0.00000000   -50.58231804
   ht   4     0.00000000     0.00000000     0.00000000   -50.57550985
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57522977
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.37781561
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.69975686
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.57571073
   ht   9    -0.05743623    -0.01601529     1.32354527     0.10457565     0.07491561     0.00389529    -0.02145345    -0.01493939
   ht  10    -0.04259404     0.04519231     0.01955114    -0.44208540    -0.23281998     0.03192280     0.01134944    -0.00104599
   ht  11     0.01617466     0.05433301     0.09312726     0.20821358     0.17159217    -0.01298877     0.03306873     0.00575025
   ht  12     3.05283656    -1.63010220    -2.03228528   -10.47379149    25.45102260    -5.55615992    -1.45805642     1.32248245

                ht   9         ht  10         ht  11         ht  12
   ht   9    -0.08856653
   ht  10     0.00053927    -0.02061936
   ht  11    -0.00311526     0.00267441    -0.02289054
   ht  12     0.06218403    -0.01548114    -0.19595019   -22.32467564

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -4.032812E-04   0.966307      -7.959320E-02  -3.361715E-04   1.901422E-03  -1.662320E-04  -0.146701       1.990956E-02
 ref    2   1.231642E-04   6.156596E-02   0.754412       0.523047       0.311462       8.474379E-03   9.291466E-03  -4.324210E-03
 ref    3   1.220915E-03  -1.738169E-03  -4.667947E-03  -0.454289       0.766063       0.394746      -3.082272E-02   2.782150E-03
 ref    4  -0.955103      -2.585103E-04   5.177352E-06  -1.306381E-04   1.627639E-04   7.231495E-03  -7.264437E-04  -6.488410E-02

              v      9       v     10       v     11       v     12
 ref    1  -1.761630E-02  -9.111722E-03  -6.376456E-03  -1.008520E-03
 ref    2   1.195860E-02  -0.149425       5.475984E-02  -6.733952E-04
 ref    3   0.125335      -1.911727E-03  -2.286172E-02  -4.694138E-02
 ref    4   4.794578E-03  -1.553008E-02  -3.727913E-03  -2.787019E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.912223       0.937542       0.575494       0.479957       0.683865       0.155949       2.255816E-02   4.632777E-03

              v      9       v     10       v     11       v     12
 ref    1   1.618514E-02   2.265574E-02   3.575855E-03   2.981712E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00040328     0.96630682    -0.07959320    -0.00033617     0.00190142    -0.00016623    -0.14670125     0.01990956
 ref:   2     0.00012316     0.06156596     0.75441179     0.52304730     0.31146160     0.00847438     0.00929147    -0.00432421
 ref:   3     0.00122091    -0.00173817    -0.00466795    -0.45428890     0.76606320     0.39474606    -0.03082272     0.00278215
 ref:   4    -0.95510297    -0.00025851     0.00000518    -0.00013064     0.00016276     0.00723149    -0.00072644    -0.06488410

                ci   9         ci  10         ci  11         ci  12
 ref:   1    -0.01761630    -0.00911172    -0.00637646    -0.00100852
 ref:   2     0.01195860    -0.14942517     0.05475984    -0.00067340
 ref:   3     0.12533479    -0.00191173    -0.02286172    -0.04694138
 ref:   4     0.00479458    -0.01553008    -0.00372791    -0.02787019

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 24  1   -196.4879125729  1.3594E-07  4.2606E-05  1.5267E-02  1.0000E-04   
 mr-sdci # 24  2   -196.4725311609  2.1208E-08  0.0000E+00  1.6207E-02  1.0000E-04   
 mr-sdci # 24  3   -196.4719089270  6.4336E-07  0.0000E+00  4.3167E-02  1.0000E-04   
 mr-sdci # 24  4   -196.4643063572  2.5044E-06  0.0000E+00  2.1381E-02  1.0000E-04   
 mr-sdci # 24  5   -196.4642298086  1.5653E-05  0.0000E+00  2.3277E-02  1.0000E-04   
 mr-sdci # 24  6   -196.4119064157  1.4426E-01  0.0000E+00  6.3880E-01  1.0000E-04   
 mr-sdci # 24  7   -195.5985161914  3.1256E-04  0.0000E+00  7.7635E-01  1.0000E-04   
 mr-sdci # 24  8   -195.4684894285  3.1968E-03  0.0000E+00  1.1535E+00  1.0000E-04   
 mr-sdci # 24  9   -194.6625644233  2.5253E-01  0.0000E+00  1.8858E+00  1.0000E-04   
 mr-sdci # 24 10   -194.2810569770  2.6110E-02  0.0000E+00  2.8100E+00  1.0000E-04   
 mr-sdci # 24 11   -194.0918904364  1.0258E-03  0.0000E+00  2.6644E+00  1.0000E-04   
 mr-sdci # 24 12   -191.8590315769 -1.4549E+00  0.0000E+00  5.5119E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.014000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  25

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59959750
   ht   2     0.00000000   -50.58421350
   ht   3     0.00000000     0.00000000   -50.58231804
   ht   4     0.00000000     0.00000000     0.00000000   -50.57550985
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57522977
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.37781561
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.69975686
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.57571073
   ht   9    -0.05743623    -0.01601529     1.32354527     0.10457565     0.07491561     0.00389529    -0.02145345    -0.01493939
   ht  10    -0.04259404     0.04519231     0.01955114    -0.44208540    -0.23281998     0.03192280     0.01134944    -0.00104599
   ht  11     0.01617466     0.05433301     0.09312726     0.20821358     0.17159217    -0.01298877     0.03306873     0.00575025
   ht  12     3.05283656    -1.63010220    -2.03228528   -10.47379149    25.45102260    -5.55615992    -1.45805642     1.32248245
   ht  13     0.22124681     0.01586689     0.00229813     0.01634903    -0.02373594     0.00568590     0.00386509    -0.02889055

                ht   9         ht  10         ht  11         ht  12         ht  13
   ht   9    -0.08856653
   ht  10     0.00053927    -0.02061936
   ht  11    -0.00311526     0.00267441    -0.02289054
   ht  12     0.06218403    -0.01548114    -0.19595019   -22.32467564
   ht  13     0.00026551     0.00028297    -0.00002960     0.00090444    -0.00162770

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   7.417102E-05   0.966382      -7.858028E-02   1.742345E-04   1.850970E-03  -3.057218E-04   2.524362E-02  -0.145748    
 ref    2  -4.032722E-05   6.071183E-02   0.754499      -0.521922       0.313292       8.548272E-03  -5.752687E-03   9.059545E-03
 ref    3   1.532840E-03  -1.572158E-03  -4.724701E-03   0.456992       0.764488       0.394706       3.733395E-03  -3.068854E-02
 ref    4  -0.955375       1.608378E-04  -1.238970E-04   2.979849E-04   2.112994E-04   7.333048E-03  -3.646746E-02  -2.466643E-03

              v      9       v     10       v     11       v     12       v     13
 ref    1   1.795901E-02  -9.335919E-03   6.480888E-03   2.545415E-03   7.455991E-03
 ref    2  -1.177860E-02  -0.149292      -5.509003E-02  -1.420655E-03  -8.723936E-04
 ref    3  -0.125109      -2.383748E-03   2.306582E-02  -3.437347E-02   3.739808E-02
 ref    4  -7.729772E-03  -1.275202E-02   1.986081E-03  -9.316029E-02  -0.135329    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.912743       0.937583       0.575465       0.481245       0.682597       0.155920       2.014147E-03   2.227247E-02

              v      9       v     10       v     11       v     12       v     13
 ref    1   1.617324E-02   2.254361E-02   3.612889E-03   9.868872E-03   1.976886E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00007417     0.96638198    -0.07858028     0.00017423     0.00185097    -0.00030572     0.02524362    -0.14574814
 ref:   2    -0.00004033     0.06071183     0.75449861    -0.52192248     0.31329202     0.00854827    -0.00575269     0.00905955
 ref:   3     0.00153284    -0.00157216    -0.00472470     0.45699167     0.76448766     0.39470582     0.00373340    -0.03068854
 ref:   4    -0.95537464     0.00016084    -0.00012390     0.00029798     0.00021130     0.00733305    -0.03646746    -0.00246664

                ci   9         ci  10         ci  11         ci  12         ci  13
 ref:   1     0.01795901    -0.00933592     0.00648089     0.00254541     0.00745599
 ref:   2    -0.01177860    -0.14929219    -0.05509003    -0.00142066    -0.00087239
 ref:   3    -0.12510888    -0.00238375     0.02306582    -0.03437347     0.03739808
 ref:   4    -0.00772977    -0.01275202     0.00198608    -0.09316029    -0.13532884

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 25  1   -196.4879432362  3.0663E-05  0.0000E+00  6.5961E-03  1.0000E-04   
 mr-sdci # 25  2   -196.4725331826  2.0217E-06  4.2034E-05  1.5504E-02  1.0000E-04   
 mr-sdci # 25  3   -196.4719091370  2.1002E-07  0.0000E+00  4.3136E-02  1.0000E-04   
 mr-sdci # 25  4   -196.4643072600  9.0280E-07  0.0000E+00  2.1173E-02  1.0000E-04   
 mr-sdci # 25  5   -196.4642298881  7.9561E-08  0.0000E+00  2.3278E-02  1.0000E-04   
 mr-sdci # 25  6   -196.4119290883  2.2673E-05  0.0000E+00  6.3855E-01  1.0000E-04   
 mr-sdci # 25  7   -195.6210901747  2.2574E-02  0.0000E+00  7.0727E-01  1.0000E-04   
 mr-sdci # 25  8   -195.5984807940  1.2999E-01  0.0000E+00  7.7659E-01  1.0000E-04   
 mr-sdci # 25  9   -194.6633089934  7.4457E-04  0.0000E+00  1.8902E+00  1.0000E-04   
 mr-sdci # 25 10   -194.2817247230  6.6775E-04  0.0000E+00  2.8081E+00  1.0000E-04   
 mr-sdci # 25 11   -194.0921347219  2.4429E-04  0.0000E+00  2.6642E+00  1.0000E-04   
 mr-sdci # 25 12   -192.0002862443  1.4125E-01  0.0000E+00  5.3448E+00  1.0000E-04   
 mr-sdci # 25 13   -191.1811812217 -1.9252E+00  0.0000E+00  5.4137E+00  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.006000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  26

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59959750
   ht   2     0.00000000   -50.58421350
   ht   3     0.00000000     0.00000000   -50.58231804
   ht   4     0.00000000     0.00000000     0.00000000   -50.57550985
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57522977
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.37781561
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.69975686
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.57571073
   ht   9    -0.05743623    -0.01601529     1.32354527     0.10457565     0.07491561     0.00389529    -0.02145345    -0.01493939
   ht  10    -0.04259404     0.04519231     0.01955114    -0.44208540    -0.23281998     0.03192280     0.01134944    -0.00104599
   ht  11     0.01617466     0.05433301     0.09312726     0.20821358     0.17159217    -0.01298877     0.03306873     0.00575025
   ht  12     3.05283656    -1.63010220    -2.03228528   -10.47379149    25.45102260    -5.55615992    -1.45805642     1.32248245
   ht  13     0.22124681     0.01586689     0.00229813     0.01634903    -0.02373594     0.00568590     0.00386509    -0.02889055
   ht  14     0.01353379     0.08467577     0.04306971     0.03008605    -0.00001142     0.00167297    -0.00772426    -0.00513849

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14
   ht   9    -0.08856653
   ht  10     0.00053927    -0.02061936
   ht  11    -0.00311526     0.00267441    -0.02289054
   ht  12     0.06218403    -0.01548114    -0.19595019   -22.32467564
   ht  13     0.00026551     0.00028297    -0.00002960     0.00090444    -0.00162770
   ht  14    -0.00088148     0.00027958    -0.00019702     0.01548853    -0.00008628    -0.00092696

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -2.306296E-05   0.966132      -7.947336E-02  -6.302846E-04   5.789275E-04  -1.592357E-03  -0.142039      -9.476475E-03
 ref    2  -4.700215E-05   6.136271E-02   0.754442      -0.514844       0.324798      -8.667212E-03   6.764841E-03   4.828745E-03
 ref    3   1.535427E-03  -8.809575E-04  -4.712323E-03   0.473865       0.753881      -0.395315      -2.969920E-02  -4.161145E-04
 ref    4  -0.955371      -3.760425E-05  -1.266743E-04   2.755350E-04   1.636467E-04  -7.562459E-03  -2.406296E-03   3.663770E-02

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1  -2.283488E-02  -2.057790E-03   1.323182E-02   4.760026E-02  -1.357548E-02  -9.142972E-03
 ref    2   1.545085E-02   0.149130      -4.199703E-02   3.598602E-02  -3.121779E-03   3.235340E-04
 ref    3   0.123435      -6.205409E-03   2.631087E-02   1.969456E-02   3.240281E-02  -3.684018E-02
 ref    4   8.780536E-03   1.393986E-02   2.213594E-03   2.856262E-03   9.355634E-02   0.138493    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.912736       0.937176       0.575521       0.489614       0.673831       0.156409       2.110863E-02   1.455614E-03

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1   1.607344E-02   2.247694E-02   2.635994E-03   3.956812E-03   9.996770E-03   2.062110E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00002306     0.96613154    -0.07947336    -0.00063028     0.00057893    -0.00159236    -0.14203884    -0.00947647
 ref:   2    -0.00004700     0.06136271     0.75444170    -0.51484447     0.32479804    -0.00866721     0.00676484     0.00482875
 ref:   3     0.00153543    -0.00088096    -0.00471232     0.47386547     0.75388131    -0.39531492    -0.02969920    -0.00041611
 ref:   4    -0.95537100    -0.00003760    -0.00012667     0.00027554     0.00016365    -0.00756246    -0.00240630     0.03663770

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14
 ref:   1    -0.02283488    -0.00205779     0.01323182     0.04760026    -0.01357548    -0.00914297
 ref:   2     0.01545085     0.14913042    -0.04199703     0.03598602    -0.00312178     0.00032353
 ref:   3     0.12343493    -0.00620541     0.02631087     0.01969456     0.03240281    -0.03684018
 ref:   4     0.00878054     0.01393986     0.00221359     0.00285626     0.09355634     0.13849259

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 26  1   -196.4879433009  6.4714E-08  0.0000E+00  6.6153E-03  1.0000E-04   
 mr-sdci # 26  2   -196.4725756539  4.2471E-05  0.0000E+00  5.0870E-03  1.0000E-04   
 mr-sdci # 26  3   -196.4719091453  8.3196E-09  5.6046E-04  4.3113E-02  1.0000E-04   
 mr-sdci # 26  4   -196.4643083664  1.1064E-06  0.0000E+00  2.1121E-02  1.0000E-04   
 mr-sdci # 26  5   -196.4642325445  2.6564E-06  0.0000E+00  2.3883E-02  1.0000E-04   
 mr-sdci # 26  6   -196.4121271147  1.9803E-04  0.0000E+00  6.3598E-01  1.0000E-04   
 mr-sdci # 26  7   -195.6801623738  5.9072E-02  0.0000E+00  4.8082E-01  1.0000E-04   
 mr-sdci # 26  8   -195.6208975331  2.2417E-02  0.0000E+00  7.0948E-01  1.0000E-04   
 mr-sdci # 26  9   -194.6675893799  4.2804E-03  0.0000E+00  1.9204E+00  1.0000E-04   
 mr-sdci # 26 10   -194.3065957962  2.4871E-02  0.0000E+00  2.7366E+00  1.0000E-04   
 mr-sdci # 26 11   -194.1001683942  8.0337E-03  0.0000E+00  2.6159E+00  1.0000E-04   
 mr-sdci # 26 12   -193.4556169974  1.4553E+00  0.0000E+00  3.9578E+00  1.0000E-04   
 mr-sdci # 26 13   -191.8785587050  6.9738E-01  0.0000E+00  5.3367E+00  1.0000E-04   
 mr-sdci # 26 14   -191.1777312947 -1.4549E+00  0.0000E+00  5.4081E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.008000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  27

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59959750
   ht   2     0.00000000   -50.58421350
   ht   3     0.00000000     0.00000000   -50.58231804
   ht   4     0.00000000     0.00000000     0.00000000   -50.57550985
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57522977
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.37781561
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.69975686
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.57571073
   ht   9    -0.05743623    -0.01601529     1.32354527     0.10457565     0.07491561     0.00389529    -0.02145345    -0.01493939
   ht  10    -0.04259404     0.04519231     0.01955114    -0.44208540    -0.23281998     0.03192280     0.01134944    -0.00104599
   ht  11     0.01617466     0.05433301     0.09312726     0.20821358     0.17159217    -0.01298877     0.03306873     0.00575025
   ht  12     3.05283656    -1.63010220    -2.03228528   -10.47379149    25.45102260    -5.55615992    -1.45805642     1.32248245
   ht  13     0.22124681     0.01586689     0.00229813     0.01634903    -0.02373594     0.00568590     0.00386509    -0.02889055
   ht  14     0.01353379     0.08467577     0.04306971     0.03008605    -0.00001142     0.00167297    -0.00772426    -0.00513849
   ht  15    -0.03669954     0.00607972     0.58755050     0.01528569     0.05656467    -0.00005601    -0.02671409    -0.00332220

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15
   ht   9    -0.08856653
   ht  10     0.00053927    -0.02061936
   ht  11    -0.00311526     0.00267441    -0.02289054
   ht  12     0.06218403    -0.01548114    -0.19595019   -22.32467564
   ht  13     0.00026551     0.00028297    -0.00002960     0.00090444    -0.00162770
   ht  14    -0.00088148     0.00027958    -0.00019702     0.01548853    -0.00008628    -0.00092696
   ht  15    -0.00990418     0.00070288    -0.00113319     0.00660128     0.00013246    -0.00033819    -0.01853594

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   3.200473E-05   0.968637       3.796637E-02  -3.314770E-04  -4.534432E-04   1.549049E-03  -0.141858      -9.805711E-03
 ref    2  -3.132329E-05   2.925534E-02  -0.752588      -0.519816      -0.322337       9.041281E-03   1.781612E-02   3.934658E-03
 ref    3  -1.534708E-03  -6.921585E-04   4.457480E-03   0.468656      -0.757131       0.395322      -2.879879E-02  -5.576110E-04
 ref    4   0.955371      -4.099516E-05  -6.016036E-05   2.814914E-04  -1.626844E-04   7.558034E-03  -2.500619E-03   3.663028E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1  -8.945193E-03  -2.210889E-02  -4.027550E-03   4.921754E-02   1.115383E-02  -1.252199E-02   5.310006E-03
 ref    2  -0.106991      -6.516281E-03  -8.514031E-02  -4.492454E-03  -2.000198E-02  -3.914141E-02   9.127800E-02
 ref    3  -1.083831E-02   0.124363      -1.436913E-02   2.476533E-02  -3.036000E-02  -2.698500E-02  -2.960534E-02
 ref    4  -1.179230E-03   7.656339E-03  -1.287664E-02  -9.539409E-04  -9.223651E-02   0.131013       4.929368E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.912736       0.939114       0.567850       0.489847       0.677149       0.156421       2.127665E-02   1.453722E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1   1.164603E-02   1.605596E-02   7.637374E-03   3.056780E-03   9.953791E-03   1.958146E-02   1.166621E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00003200     0.96863688     0.03796637    -0.00033148    -0.00045344     0.00154905    -0.14185772    -0.00980571
 ref:   2    -0.00003132     0.02925534    -0.75258823    -0.51981600    -0.32233710     0.00904128     0.01781612     0.00393466
 ref:   3    -0.00153471    -0.00069216     0.00445748     0.46865601    -0.75713112     0.39532235    -0.02879879    -0.00055761
 ref:   4     0.95537077    -0.00004100    -0.00006016     0.00028149    -0.00016268     0.00755803    -0.00250062     0.03663028

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15
 ref:   1    -0.00894519    -0.02210889    -0.00402755     0.04921754     0.01115383    -0.01252199     0.00531001
 ref:   2    -0.10699137    -0.00651628    -0.08514031    -0.00449245    -0.02000198    -0.03914141     0.09127800
 ref:   3    -0.01083831     0.12436267    -0.01436913     0.02476533    -0.03036000    -0.02698500    -0.02960534
 ref:   4    -0.00117923     0.00765634    -0.01287664    -0.00095394    -0.09223651     0.13101306     0.04929368

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 27  1   -196.4879433060  5.1664E-09  0.0000E+00  6.6150E-03  1.0000E-04   
 mr-sdci # 27  2   -196.4725757695  1.1561E-07  0.0000E+00  4.9930E-03  1.0000E-04   
 mr-sdci # 27  3   -196.4725179045  6.0876E-04  0.0000E+00  1.7579E-02  1.0000E-04   
 mr-sdci # 27  4   -196.4643096577  1.2913E-06  1.3430E-04  2.1277E-02  1.0000E-04   
 mr-sdci # 27  5   -196.4642327592  2.1477E-07  0.0000E+00  2.3807E-02  1.0000E-04   
 mr-sdci # 27  6   -196.4121282006  1.0859E-06  0.0000E+00  6.3594E-01  1.0000E-04   
 mr-sdci # 27  7   -195.6821611092  1.9987E-03  0.0000E+00  4.6635E-01  1.0000E-04   
 mr-sdci # 27  8   -195.6209061298  8.5967E-06  0.0000E+00  7.0957E-01  1.0000E-04   
 mr-sdci # 27  9   -195.5128157945  8.4523E-01  0.0000E+00  1.1169E+00  1.0000E-04   
 mr-sdci # 27 10   -194.6595289226  3.5293E-01  0.0000E+00  1.9369E+00  1.0000E-04   
 mr-sdci # 27 11   -194.2215530802  1.2138E-01  0.0000E+00  2.7992E+00  1.0000E-04   
 mr-sdci # 27 12   -193.5356455077  8.0029E-02  0.0000E+00  3.7045E+00  1.0000E-04   
 mr-sdci # 27 13   -191.9096214220  3.1063E-02  0.0000E+00  5.2633E+00  1.0000E-04   
 mr-sdci # 27 14   -191.2012860975  2.3555E-02  0.0000E+00  5.1192E+00  1.0000E-04   
 mr-sdci # 27 15   -191.0456488036 -1.3288E+00  0.0000E+00  4.0444E+00  1.0000E-04   
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.002000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  28

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59959750
   ht   2     0.00000000   -50.58421350
   ht   3     0.00000000     0.00000000   -50.58231804
   ht   4     0.00000000     0.00000000     0.00000000   -50.57550985
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57522977
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.37781561
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.69975686
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.57571073
   ht   9    -0.05743623    -0.01601529     1.32354527     0.10457565     0.07491561     0.00389529    -0.02145345    -0.01493939
   ht  10    -0.04259404     0.04519231     0.01955114    -0.44208540    -0.23281998     0.03192280     0.01134944    -0.00104599
   ht  11     0.01617466     0.05433301     0.09312726     0.20821358     0.17159217    -0.01298877     0.03306873     0.00575025
   ht  12     3.05283656    -1.63010220    -2.03228528   -10.47379149    25.45102260    -5.55615992    -1.45805642     1.32248245
   ht  13     0.22124681     0.01586689     0.00229813     0.01634903    -0.02373594     0.00568590     0.00386509    -0.02889055
   ht  14     0.01353379     0.08467577     0.04306971     0.03008605    -0.00001142     0.00167297    -0.00772426    -0.00513849
   ht  15    -0.03669954     0.00607972     0.58755050     0.01528569     0.05656467    -0.00005601    -0.02671409    -0.00332220
   ht  16     0.04767278    -0.02570679    -0.06806881     0.06766369     0.18143803    -0.05843737     0.01570718    -0.00617240

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15         ht  16
   ht   9    -0.08856653
   ht  10     0.00053927    -0.02061936
   ht  11    -0.00311526     0.00267441    -0.02289054
   ht  12     0.06218403    -0.01548114    -0.19595019   -22.32467564
   ht  13     0.00026551     0.00028297    -0.00002960     0.00090444    -0.00162770
   ht  14    -0.00088148     0.00027958    -0.00019702     0.01548853    -0.00008628    -0.00092696
   ht  15    -0.00990418     0.00070288    -0.00113319     0.00660128     0.00013246    -0.00033819    -0.01853594
   ht  16     0.00066141    -0.00008082    -0.00104535    -0.09551079    -0.00010525    -0.00001653     0.00045497    -0.00431651

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -3.713214E-05  -0.968473       4.196869E-02  -4.071670E-05  -5.002095E-04   2.691113E-03   0.138830      -2.803132E-03
 ref    2   5.366836E-05  -3.231140E-02  -0.751151      -0.481335      -0.379251       1.691822E-02  -2.192238E-02   6.916899E-03
 ref    3   1.425257E-03   5.546698E-04   2.112293E-03   0.556140      -0.699192       0.382369       3.953333E-02  -1.248671E-02
 ref    4  -0.955369       4.303477E-05  -2.623693E-05   1.286801E-04  -1.211995E-04   6.730023E-03   2.738274E-03   3.543249E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1  -2.728902E-02  -3.327410E-02  -1.160378E-02   4.397550E-02  -1.771661E-02  -1.381069E-02   8.022314E-03   5.027507E-03
 ref    2   2.512383E-02  -0.123764       4.035482E-02  -2.789635E-02  -1.483312E-02  -2.806834E-03   1.322553E-02   0.102376    
 ref    3   8.217333E-02   6.158536E-02   0.105243       2.003719E-02  -1.854213E-02   4.593003E-02   4.616097E-02  -1.575813E-03
 ref    4   1.025900E-02   5.639234E-03   1.115870E-02  -4.362725E-03   1.418366E-02   0.115212      -0.119315       3.890263E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.912733       0.938984       0.565994       0.540976       0.632700       0.146545       2.132485E-02   1.467080E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   8.233601E-03   2.024925E-02   1.296383E-02   3.132573E-03   1.078887E-03   1.558200E-02   1.660618E-02   1.202197E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15        ev   16

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00003713    -0.96847274     0.04196869    -0.00004072    -0.00050021     0.00269111     0.13883038    -0.00280313
 ref:   2     0.00005367    -0.03231140    -0.75115112    -0.48133533    -0.37925058     0.01691822    -0.02192238     0.00691690
 ref:   3     0.00142526     0.00055467     0.00211229     0.55614040    -0.69919184     0.38236906     0.03953333    -0.01248671
 ref:   4    -0.95536936     0.00004303    -0.00002624     0.00012868    -0.00012120     0.00673002     0.00273827     0.03543249

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1    -0.02728902    -0.03327410    -0.01160378     0.04397550    -0.01771661    -0.01381069     0.00802231     0.00502751
 ref:   2     0.02512383    -0.12376399     0.04035482    -0.02789635    -0.01483312    -0.00280683     0.01322553     0.10237574
 ref:   3     0.08217333     0.06158536     0.10524332     0.02003719    -0.01854213     0.04593003     0.04616097    -0.00157581
 ref:   4     0.01025900     0.00563923     0.01115870    -0.00436273     0.01418366     0.11521207    -0.11931501     0.03890263

 trial vector basis is being transformed.  new dimension:   8

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 28  1   -196.4879433478  4.1715E-08  0.0000E+00  6.6283E-03  1.0000E-04   
 mr-sdci # 28  2   -196.4725757847  1.5185E-08  0.0000E+00  4.9971E-03  1.0000E-04   
 mr-sdci # 28  3   -196.4725215125  3.6079E-06  0.0000E+00  1.7377E-02  1.0000E-04   
 mr-sdci # 28  4   -196.4644813196  1.7166E-04  0.0000E+00  8.6055E-03  1.0000E-04   
 mr-sdci # 28  5   -196.4642341890  1.4298E-06  1.9306E-04  2.4252E-02  1.0000E-04   
 mr-sdci # 28  6   -196.4151399322  3.0117E-03  0.0000E+00  5.8912E-01  1.0000E-04   
 mr-sdci # 28  7   -195.6836484673  1.4874E-03  0.0000E+00  4.6803E-01  1.0000E-04   
 mr-sdci # 28  8   -195.6221567100  1.2506E-03  0.0000E+00  6.9658E-01  1.0000E-04   
 mr-sdci # 28  9   -195.5514290953  3.8613E-02  0.0000E+00  1.1316E+00  1.0000E-04   
 mr-sdci # 28 10   -195.4806805896  8.2115E-01  0.0000E+00  1.2798E+00  1.0000E-04   
 mr-sdci # 28 11   -194.5599899126  3.3844E-01  0.0000E+00  2.0425E+00  1.0000E-04   
 mr-sdci # 28 12   -193.6175790621  8.1934E-02  0.0000E+00  3.6380E+00  1.0000E-04   
 mr-sdci # 28 13   -192.8299270108  9.2031E-01  0.0000E+00  3.7659E+00  1.0000E-04   
 mr-sdci # 28 14   -191.5487397358  3.4745E-01  0.0000E+00  5.0323E+00  1.0000E-04   
 mr-sdci # 28 15   -191.1662068451  1.2056E-01  0.0000E+00  5.4091E+00  1.0000E-04   
 mr-sdci # 28 16   -190.9728586930 -2.2495E-01  0.0000E+00  3.7600E+00  1.0000E-04   
 
 root number  5 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  29

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59962855
   ht   2     0.00000000   -50.58426099
   ht   3     0.00000000     0.00000000   -50.58420672
   ht   4     0.00000000     0.00000000     0.00000000   -50.57616652
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57591939
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.52682514
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.79533367
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.73384191
   ht   9     0.01203393     0.05313281     0.05102416    -0.21630514    -0.00345515    -0.06260295    -0.00785666     0.01311174

                ht   9
   ht   9    -0.00598752

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   3.690304E-05   0.968561      -3.993835E-02   4.407445E-05   1.274373E-04  -2.680021E-03  -0.138769      -2.401892E-03
 ref    2  -5.524013E-05   3.089306E-02   0.750985       0.439148       0.427678      -1.356622E-02   2.165408E-02   6.407849E-03
 ref    3  -1.429981E-03  -1.833346E-04  -2.651653E-03  -0.626308       0.639767      -0.374896      -4.039891E-02  -1.376908E-02
 ref    4   0.955369      -4.231327E-05   2.501319E-05  -1.462980E-04   1.458480E-04  -6.687630E-03  -2.690601E-03   3.539517E-02

              v      9
 ref    1  -9.805816E-03
 ref    2   3.133131E-02
 ref    3   8.425945E-02
 ref    4   3.372070E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.912733       0.939064       0.565580       0.585113       0.592210       0.140783       2.136497E-02   1.489235E-03

              v      9
 ref    1   8.188831E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00003690     0.96856057    -0.03993835     0.00004407     0.00012744    -0.00268002    -0.13876872    -0.00240189
 ref:   2    -0.00005524     0.03089306     0.75098460     0.43914847     0.42767848    -0.01356622     0.02165408     0.00640785
 ref:   3    -0.00142998    -0.00018333    -0.00265165    -0.62630820     0.63976680    -0.37489583    -0.04039891    -0.01376908
 ref:   4     0.95536935    -0.00004231     0.00002501    -0.00014630     0.00014585    -0.00668763    -0.00269060     0.03539517

                ci   9
 ref:   1    -0.00980582
 ref:   2     0.03133131
 ref:   3     0.08425945
 ref:   4     0.00337207

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 29  1   -196.4879433478  8.5777E-11  0.0000E+00  6.6286E-03  1.0000E-04   
 mr-sdci # 29  2   -196.4725758626  7.7895E-08  0.0000E+00  4.8449E-03  1.0000E-04   
 mr-sdci # 29  3   -196.4725216788  1.6635E-07  0.0000E+00  1.7460E-02  1.0000E-04   
 mr-sdci # 29  4   -196.4644824064  1.0868E-06  0.0000E+00  8.5922E-03  1.0000E-04   
 mr-sdci # 29  5   -196.4644108476  1.7666E-04  0.0000E+00  1.2872E-02  1.0000E-04   
 mr-sdci # 29  6   -196.4160187704  8.7884E-04  7.4845E-02  5.7024E-01  1.0000E-04   
 mr-sdci # 29  7   -195.6837264226  7.7955E-05  0.0000E+00  4.6276E-01  1.0000E-04   
 mr-sdci # 29  8   -195.6223653801  2.0867E-04  0.0000E+00  6.9449E-01  1.0000E-04   
 mr-sdci # 29  9   -194.3356292173 -1.2158E+00  0.0000E+00  2.7829E+00  1.0000E-04   
 
 root number  6 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.004000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  30

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59962855
   ht   2     0.00000000   -50.58426099
   ht   3     0.00000000     0.00000000   -50.58420672
   ht   4     0.00000000     0.00000000     0.00000000   -50.57616652
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57591939
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.52682514
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.79533367
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.73384191
   ht   9     0.01203393     0.05313281     0.05102416    -0.21630514    -0.00345515    -0.06260295    -0.00785666     0.01311174
   ht  10     2.54499991     1.39103041     0.19696334    -7.89432811     9.59036121     1.27903794    -0.53571912     0.58125170

                ht   9         ht  10
   ht   9    -0.00598752
   ht  10    -0.03557097    -4.80443710

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   3.748225E-05  -0.968540      -4.043918E-02  -6.316050E-05   2.050218E-04   1.866905E-03  -0.138733      -2.360984E-03
 ref    2  -4.331433E-05  -3.127065E-02   0.750899       0.336766       0.512358       1.005760E-02   2.170326E-02   6.424183E-03
 ref    3  -9.624804E-04   3.358277E-04  -4.128614E-03  -0.762448       0.503238       0.316505      -3.960228E-02  -1.343327E-02
 ref    4   0.955366       4.095063E-05   3.813949E-05  -1.183501E-04   6.143861E-05   4.227934E-04  -2.533498E-03   3.546078E-02

              v      9       v     10
 ref    1   9.207752E-03   1.342110E-02
 ref    2  -3.175678E-02   8.129502E-03
 ref    3  -9.068202E-02   0.111055    
 ref    4  -5.618641E-03   5.218339E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.912725       0.939047       0.565502       0.694738       0.515759       0.100280       2.129268E-02   1.484764E-03

              v      9       v     10
 ref    1   9.348074E-03   1.530252E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00003748    -0.96853951    -0.04043918    -0.00006316     0.00020502     0.00186691    -0.13873316    -0.00236098
 ref:   2    -0.00004331    -0.03127065     0.75089901     0.33676571     0.51235755     0.01005760     0.02170326     0.00642418
 ref:   3    -0.00096248     0.00033583    -0.00412861    -0.76244801     0.50323776     0.31650463    -0.03960228    -0.01343327
 ref:   4     0.95536572     0.00004095     0.00003814    -0.00011835     0.00006144     0.00042279    -0.00253350     0.03546078

                ci   9         ci  10
 ref:   1     0.00920775     0.01342110
 ref:   2    -0.03175678     0.00812950
 ref:   3    -0.09068202     0.11105495
 ref:   4    -0.00561864     0.05218339

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 30  1   -196.4879434306  8.2735E-08  1.6795E-05  6.6554E-03  1.0000E-04   
 mr-sdci # 30  2   -196.4725758654  2.8347E-09  0.0000E+00  4.8531E-03  1.0000E-04   
 mr-sdci # 30  3   -196.4725219493  2.7045E-07  0.0000E+00  1.7475E-02  1.0000E-04   
 mr-sdci # 30  4   -196.4644957911  1.3385E-05  0.0000E+00  1.4018E-02  1.0000E-04   
 mr-sdci # 30  5   -196.4644275621  1.6715E-05  0.0000E+00  1.6478E-02  1.0000E-04   
 mr-sdci # 30  6   -196.4560677672  4.0049E-02  0.0000E+00  2.2601E-01  1.0000E-04   
 mr-sdci # 30  7   -195.6837525714  2.6149E-05  0.0000E+00  4.6226E-01  1.0000E-04   
 mr-sdci # 30  8   -195.6223702172  4.8371E-06  0.0000E+00  6.9462E-01  1.0000E-04   
 mr-sdci # 30  9   -194.3395045480  3.8753E-03  0.0000E+00  2.8480E+00  1.0000E-04   
 mr-sdci # 30 10   -192.2036442331 -3.2770E+00  0.0000E+00  4.4634E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.011000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  31

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59962855
   ht   2     0.00000000   -50.58426099
   ht   3     0.00000000     0.00000000   -50.58420672
   ht   4     0.00000000     0.00000000     0.00000000   -50.57616652
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57591939
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.52682514
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.79533367
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.73384191
   ht   9     0.01203393     0.05313281     0.05102416    -0.21630514    -0.00345515    -0.06260295    -0.00785666     0.01311174
   ht  10     2.54499991     1.39103041     0.19696334    -7.89432811     9.59036121     1.27903794    -0.53571912     0.58125170
   ht  11    -0.11945600     0.00650945     0.00502394    -0.01545885     0.02976870    -0.00825350    -0.00063621     0.00883992

                ht   9         ht  10         ht  11
   ht   9    -0.00598752
   ht  10    -0.03557097    -4.80443710
   ht  11    -0.00008231     0.00164038    -0.00079283

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   1.887364E-05   0.968545       4.031649E-02  -6.385318E-05  -2.047639E-04  -1.864920E-03  -6.463864E-02  -0.122779    
 ref    2  -2.783342E-05   3.117570E-02  -0.750902       0.336799      -0.512337      -1.005376E-02   5.876812E-03   2.159395E-02
 ref    3  -9.116222E-04  -3.362387E-04   4.127165E-03  -0.762415      -0.503286      -0.316493      -2.987795E-03  -4.325693E-02
 ref    4   0.954927      -1.903675E-05  -3.250221E-06  -8.956097E-05  -7.223279E-05  -6.122712E-04  -6.683042E-02   3.161717E-02

              v      9       v     10       v     11
 ref    1   9.207431E-03  -3.291157E-03  -1.305350E-02
 ref    2  -3.175380E-02   5.033567E-03  -1.080964E-02
 ref    3  -9.067626E-02  -2.735448E-03  -0.119760    
 ref    4  -5.706263E-03  -0.213848       3.495868E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.911887       0.939051       0.565497       0.694711       0.515786       0.100273       8.687923E-03   1.841173E-02

              v      9       v     10       v     11
 ref    1   9.347826E-03   4.577463E-02   1.585178E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00001887     0.96854474     0.04031649    -0.00006385    -0.00020476    -0.00186492    -0.06463864    -0.12277876
 ref:   2    -0.00002783     0.03117570    -0.75090247     0.33679889    -0.51233660    -0.01005376     0.00587681     0.02159395
 ref:   3    -0.00091162    -0.00033624     0.00412717    -0.76241536    -0.50328623    -0.31649271    -0.00298780    -0.04325693
 ref:   4     0.95492740    -0.00001904    -0.00000325    -0.00008956    -0.00007223    -0.00061227    -0.06683042     0.03161717

                ci   9         ci  10         ci  11
 ref:   1     0.00920743    -0.00329116    -0.01305350
 ref:   2    -0.03175380     0.00503357    -0.01080964
 ref:   3    -0.09067626    -0.00273545    -0.11975990
 ref:   4    -0.00570626    -0.21384803     0.03495868

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 31  1   -196.4879530522  9.6216E-06  0.0000E+00  4.1580E-03  1.0000E-04   
 mr-sdci # 31  2   -196.4725758697  4.2880E-09  6.2531E-06  4.8559E-03  1.0000E-04   
 mr-sdci # 31  3   -196.4725219602  1.0908E-08  0.0000E+00  1.7475E-02  1.0000E-04   
 mr-sdci # 31  4   -196.4644958026  1.1480E-08  0.0000E+00  1.4021E-02  1.0000E-04   
 mr-sdci # 31  5   -196.4644275637  1.6167E-09  0.0000E+00  1.6477E-02  1.0000E-04   
 mr-sdci # 31  6   -196.4560684141  6.4692E-07  0.0000E+00  2.2587E-01  1.0000E-04   
 mr-sdci # 31  7   -195.6897694412  6.0169E-03  0.0000E+00  4.9919E-01  1.0000E-04   
 mr-sdci # 31  8   -195.6821502591  5.9780E-02  0.0000E+00  4.7237E-01  1.0000E-04   
 mr-sdci # 31  9   -194.3395047025  1.5448E-07  0.0000E+00  2.8481E+00  1.0000E-04   
 mr-sdci # 31 10   -193.4788263632  1.2752E+00  0.0000E+00  3.4546E+00  1.0000E-04   
 mr-sdci # 31 11   -191.9764260071 -2.5836E+00  0.0000E+00  4.4746E+00  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.005000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  32

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59962855
   ht   2     0.00000000   -50.58426099
   ht   3     0.00000000     0.00000000   -50.58420672
   ht   4     0.00000000     0.00000000     0.00000000   -50.57616652
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57591939
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.52682514
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.79533367
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.73384191
   ht   9     0.01203393     0.05313281     0.05102416    -0.21630514    -0.00345515    -0.06260295    -0.00785666     0.01311174
   ht  10     2.54499991     1.39103041     0.19696334    -7.89432811     9.59036121     1.27903794    -0.53571912     0.58125170
   ht  11    -0.11945600     0.00650945     0.00502394    -0.01545885     0.02976870    -0.00825350    -0.00063621     0.00883992
   ht  12     0.00082985    -0.01399542    -0.02500005     0.01496562     0.01301478    -0.00530435     0.00432593     0.00109984

                ht   9         ht  10         ht  11         ht  12
   ht   9    -0.00598752
   ht  10    -0.03557097    -4.80443710
   ht  11    -0.00008231     0.00164038    -0.00079283
   ht  12     0.00003774     0.00084814     0.00000511    -0.00017669

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -1.987734E-05  -0.967548      -5.971217E-02  -2.813604E-04   2.640916E-04  -3.255577E-03  -0.127209      -4.566719E-02
 ref    2   2.763469E-05  -4.623255E-02   0.750131       0.335366       0.513253      -1.061234E-02   1.781375E-02   1.161667E-02
 ref    3   9.126636E-04   1.121911E-03  -4.268081E-03  -0.764101       0.501340      -0.315648      -3.114092E-02  -2.971897E-02
 ref    4  -0.954927       1.188039E-05   5.121087E-06  -8.738716E-05   7.133970E-05  -5.712490E-04  -2.681304E-02   6.876910E-02

              v      9       v     10       v     11       v     12
 ref    1   5.848223E-03   4.860707E-02   2.785301E-03   1.280333E-02
 ref    2   2.586777E-02  -2.411003E-02   2.494549E-03   1.090234E-02
 ref    3   8.510351E-02  -3.331092E-02  -5.604104E-03   0.119859    
 ref    4   1.081210E-02   3.015517E-02  -0.211593      -3.493074E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.911887       0.938288       0.566280       0.696321       0.514770       9.975752E-02   1.818815E-02   7.832847E-03

              v      9       v     10       v     11       v     12
 ref    1   8.062851E-03   4.962893E-03   4.481681E-02   1.586922E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00001988    -0.96754829    -0.05971217    -0.00028136     0.00026409    -0.00325558    -0.12720898    -0.04566719
 ref:   2     0.00002763    -0.04623255     0.75013111     0.33536593     0.51325255    -0.01061234     0.01781375     0.01161667
 ref:   3     0.00091266     0.00112191    -0.00426808    -0.76410118     0.50134043    -0.31564849    -0.03114092    -0.02971897
 ref:   4    -0.95492743     0.00001188     0.00000512    -0.00008739     0.00007134    -0.00057125    -0.02681304     0.06876910

                ci   9         ci  10         ci  11         ci  12
 ref:   1     0.00584822     0.04860707     0.00278530     0.01280333
 ref:   2     0.02586777    -0.02411003     0.00249455     0.01090234
 ref:   3     0.08510351    -0.03331092    -0.00560410     0.11985942
 ref:   4     0.01081210     0.03015517    -0.21159259    -0.03493074

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 32  1   -196.4879530523  4.8296E-11  0.0000E+00  4.1575E-03  1.0000E-04   
 mr-sdci # 32  2   -196.4725809845  5.1148E-06  0.0000E+00  1.9435E-03  1.0000E-04   
 mr-sdci # 32  3   -196.4725222088  2.4867E-07  5.8291E-05  1.7420E-02  1.0000E-04   
 mr-sdci # 32  4   -196.4644964621  6.5955E-07  0.0000E+00  1.4144E-02  1.0000E-04   
 mr-sdci # 32  5   -196.4644276147  5.0969E-08  0.0000E+00  1.6400E-02  1.0000E-04   
 mr-sdci # 32  6   -196.4561870365  1.1862E-04  0.0000E+00  2.2295E-01  1.0000E-04   
 mr-sdci # 32  7   -195.7008965285  1.1127E-02  0.0000E+00  3.6527E-01  1.0000E-04   
 mr-sdci # 32  8   -195.6866294753  4.4792E-03  0.0000E+00  4.9568E-01  1.0000E-04   
 mr-sdci # 32  9   -194.3758393101  3.6335E-02  0.0000E+00  2.6068E+00  1.0000E-04   
 mr-sdci # 32 10   -193.9197671301  4.4094E-01  0.0000E+00  3.2106E+00  1.0000E-04   
 mr-sdci # 32 11   -193.4710596584  1.4946E+00  0.0000E+00  3.4598E+00  1.0000E-04   
 mr-sdci # 32 12   -191.9763570652 -1.6412E+00  0.0000E+00  4.4731E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  33

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59962855
   ht   2     0.00000000   -50.58426099
   ht   3     0.00000000     0.00000000   -50.58420672
   ht   4     0.00000000     0.00000000     0.00000000   -50.57616652
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57591939
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.52682514
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.79533367
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.73384191
   ht   9     0.01203393     0.05313281     0.05102416    -0.21630514    -0.00345515    -0.06260295    -0.00785666     0.01311174
   ht  10     2.54499991     1.39103041     0.19696334    -7.89432811     9.59036121     1.27903794    -0.53571912     0.58125170
   ht  11    -0.11945600     0.00650945     0.00502394    -0.01545885     0.02976870    -0.00825350    -0.00063621     0.00883992
   ht  12     0.00082985    -0.01399542    -0.02500005     0.01496562     0.01301478    -0.00530435     0.00432593     0.00109984
   ht  13    -0.00275584    -0.02299419    -0.11391478     0.02534369     0.01775154    -0.03470768     0.00064990    -0.00648515

                ht   9         ht  10         ht  11         ht  12         ht  13
   ht   9    -0.00598752
   ht  10    -0.03557097    -4.80443710
   ht  11    -0.00008231     0.00164038    -0.00079283
   ht  12     0.00003774     0.00084814     0.00000511    -0.00017669
   ht  13     0.00010514     0.00380621    -0.00001991    -0.00007810    -0.00128690

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   2.060552E-05  -0.968192      -4.821831E-02   2.918580E-04   2.461570E-04   3.247562E-03   0.125909       4.825057E-02
 ref    2  -3.375236E-05  -3.729218E-02   0.750152      -0.336201       0.513075       1.074098E-02  -1.887633E-02  -1.155560E-02
 ref    3  -9.127848E-04   1.071188E-03  -4.264197E-03   0.763438       0.502349       0.315641       3.102014E-02   3.017205E-02
 ref    4   0.954927       1.211183E-05   1.677753E-05   8.757878E-05   7.096878E-05   5.708226E-04   2.804719E-02  -6.820523E-02

              v      9       v     10       v     11       v     12       v     13
 ref    1   2.829139E-03  -5.166017E-02   3.755756E-03  -5.381202E-03  -1.214977E-02
 ref    2   2.208779E-02   5.438764E-03  -6.634205E-03  -7.152022E-02  -6.612422E-03
 ref    3   8.693715E-02   3.086072E-02  -7.145899E-03  -3.247877E-03  -0.120257    
 ref    4   8.899077E-03  -3.542504E-02  -0.207632       3.813168E-02   3.385994E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.911887       0.938788       0.565071       0.695870       0.515601       9.975580E-02   1.795840E-02   8.023956E-03

              v      9       v     10       v     11       v     12       v     13
 ref    1   8.133136E-03   4.905671E-03   4.322037E-02   6.608673E-03   1.579952E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00002061    -0.96819215    -0.04821831     0.00029186     0.00024616     0.00324756     0.12590944     0.04825057
 ref:   2    -0.00003375    -0.03729218     0.75015154    -0.33620119     0.51307539     0.01074098    -0.01887633    -0.01155560
 ref:   3    -0.00091278     0.00107119    -0.00426420     0.76343847     0.50234904     0.31564150     0.03102014     0.03017205
 ref:   4     0.95492742     0.00001211     0.00001678     0.00008758     0.00007097     0.00057082     0.02804719    -0.06820523

                ci   9         ci  10         ci  11         ci  12         ci  13
 ref:   1     0.00282914    -0.05166017     0.00375576    -0.00538120    -0.01214977
 ref:   2     0.02208779     0.00543876    -0.00663421    -0.07152022    -0.00661242
 ref:   3     0.08693715     0.03086072    -0.00714590    -0.00324788    -0.12025676
 ref:   4     0.00889908    -0.03542504    -0.20763234     0.03813168     0.03385994

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 33  1   -196.4879530527  4.3770E-10  0.0000E+00  4.1563E-03  1.0000E-04   
 mr-sdci # 33  2   -196.4725809863  1.8086E-09  0.0000E+00  1.9375E-03  1.0000E-04   
 mr-sdci # 33  3   -196.4725705277  4.8319E-05  0.0000E+00  6.0494E-03  1.0000E-04   
 mr-sdci # 33  4   -196.4644965154  5.3246E-08  6.7743E-05  1.4155E-02  1.0000E-04   
 mr-sdci # 33  5   -196.4644277698  1.5512E-07  0.0000E+00  1.6306E-02  1.0000E-04   
 mr-sdci # 33  6   -196.4561871995  1.6290E-07  0.0000E+00  2.2288E-01  1.0000E-04   
 mr-sdci # 33  7   -195.7017739559  8.7743E-04  0.0000E+00  3.6436E-01  1.0000E-04   
 mr-sdci # 33  8   -195.6867216857  9.2210E-05  0.0000E+00  4.9357E-01  1.0000E-04   
 mr-sdci # 33  9   -194.3814283112  5.5890E-03  0.0000E+00  2.5938E+00  1.0000E-04   
 mr-sdci # 33 10   -193.9824191131  6.2652E-02  0.0000E+00  3.0476E+00  1.0000E-04   
 mr-sdci # 33 11   -193.4783414140  7.2818E-03  0.0000E+00  3.4794E+00  1.0000E-04   
 mr-sdci # 33 12   -192.9589566067  9.8260E-01  0.0000E+00  3.9372E+00  1.0000E-04   
 mr-sdci # 33 13   -191.9724867906 -8.5744E-01  0.0000E+00  4.4586E+00  1.0000E-04   
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  34

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59962855
   ht   2     0.00000000   -50.58426099
   ht   3     0.00000000     0.00000000   -50.58420672
   ht   4     0.00000000     0.00000000     0.00000000   -50.57616652
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57591939
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.52682514
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.79533367
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.73384191
   ht   9     0.01203393     0.05313281     0.05102416    -0.21630514    -0.00345515    -0.06260295    -0.00785666     0.01311174
   ht  10     2.54499991     1.39103041     0.19696334    -7.89432811     9.59036121     1.27903794    -0.53571912     0.58125170
   ht  11    -0.11945600     0.00650945     0.00502394    -0.01545885     0.02976870    -0.00825350    -0.00063621     0.00883992
   ht  12     0.00082985    -0.01399542    -0.02500005     0.01496562     0.01301478    -0.00530435     0.00432593     0.00109984
   ht  13    -0.00275584    -0.02299419    -0.11391478     0.02534369     0.01775154    -0.03470768     0.00064990    -0.00648515
   ht  14     0.07713990     0.01250441     0.05042058    -0.12769290     0.15503054    -0.07137869    -0.01297390    -0.01178222

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14
   ht   9    -0.00598752
   ht  10    -0.03557097    -4.80443710
   ht  11    -0.00008231     0.00164038    -0.00079283
   ht  12     0.00003774     0.00084814     0.00000511    -0.00017669
   ht  13     0.00010514     0.00380621    -0.00001991    -0.00007810    -0.00128690
   ht  14    -0.00059537    -0.02943461     0.00004622     0.00003293    -0.00002211    -0.00317748

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   2.066839E-05   0.968211      -4.784799E-02   1.465663E-03  -1.087717E-04  -1.491997E-03  -0.128841       4.066566E-02
 ref    2  -2.541298E-05   3.700382E-02   0.750125       5.963359E-03  -0.613289       1.679487E-02   1.902023E-02  -9.422182E-03
 ref    3  -3.688442E-04  -1.024207E-03  -5.710837E-03   0.915396       8.847610E-03   0.314860      -3.361757E-02   2.953300E-02
 ref    4   0.954933      -1.215973E-05   1.808539E-05   4.641110E-04  -3.152409E-05  -4.631245E-04  -2.387578E-02  -6.959577E-02

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1  -6.848854E-04  -1.196492E-03   5.171243E-02  -8.711388E-03  -3.438818E-03   6.787546E-03
 ref    2  -1.645022E-02  -1.585091E-02  -5.411998E-03  -2.182954E-03  -7.098750E-02   1.480855E-02
 ref    3  -1.700786E-02  -7.830439E-02  -3.149220E-02  -2.996877E-03   3.029256E-03   0.197892    
 ref    4  -1.333119E-02   1.658046E-02   3.731116E-02   0.215293       1.520562E-02   4.308715E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.911897       0.938802       0.565009       0.837989       0.376202       9.942159E-02   1.866194E-02   7.458243E-03

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1   7.380670E-04   6.659171E-03   5.087346E-03   4.644067E-02   5.291438E-03   4.128308E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00002067     0.96821081    -0.04784799     0.00146566    -0.00010877    -0.00149200    -0.12884088     0.04066566
 ref:   2    -0.00002541     0.03700382     0.75012466     0.00596336    -0.61328935     0.01679487     0.01902023    -0.00942218
 ref:   3    -0.00036884    -0.00102421    -0.00571084     0.91539646     0.00884761     0.31486041    -0.03361757     0.02953300
 ref:   4     0.95493312    -0.00001216     0.00001809     0.00046411    -0.00003152    -0.00046312    -0.02387578    -0.06959577

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14
 ref:   1    -0.00068489    -0.00119649     0.05171243    -0.00871139    -0.00343882     0.00678755
 ref:   2    -0.01645022    -0.01585091    -0.00541200    -0.00218295    -0.07098750     0.01480855
 ref:   3    -0.01700786    -0.07830439    -0.03149220    -0.00299688     0.00302926     0.19789192
 ref:   4    -0.01333119     0.01658046     0.03731116     0.21529291     0.01520562     0.04308715

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 34  1   -196.4879531832  1.3046E-07  0.0000E+00  4.1399E-03  1.0000E-04   
 mr-sdci # 34  2   -196.4725809865  1.2518E-10  0.0000E+00  1.9377E-03  1.0000E-04   
 mr-sdci # 34  3   -196.4725706564  1.2867E-07  0.0000E+00  6.0885E-03  1.0000E-04   
 mr-sdci # 34  4   -196.4652599189  7.6340E-04  0.0000E+00  1.2039E-01  1.0000E-04   
 mr-sdci # 34  5   -196.4644503369  2.2567E-05  4.3403E-05  1.4207E-02  1.0000E-04   
 mr-sdci # 34  6   -196.4634587167  7.2715E-03  0.0000E+00  1.4056E-01  1.0000E-04   
 mr-sdci # 34  7   -195.7022616834  4.8773E-04  0.0000E+00  3.5629E-01  1.0000E-04   
 mr-sdci # 34  8   -195.6883699122  1.6482E-03  0.0000E+00  4.8492E-01  1.0000E-04   
 mr-sdci # 34  9   -195.3659715363  9.8454E-01  0.0000E+00  1.6211E+00  1.0000E-04   
 mr-sdci # 34 10   -194.2713461305  2.8893E-01  0.0000E+00  2.3049E+00  1.0000E-04   
 mr-sdci # 34 11   -193.9822305194  5.0389E-01  0.0000E+00  3.0504E+00  1.0000E-04   
 mr-sdci # 34 12   -193.3072722493  3.4832E-01  0.0000E+00  3.4929E+00  1.0000E-04   
 mr-sdci # 34 13   -192.9478149387  9.7533E-01  0.0000E+00  3.9390E+00  1.0000E-04   
 mr-sdci # 34 14   -189.4759619175 -2.0728E+00  0.0000E+00  4.5264E+00  1.0000E-04   
 
 root number  5 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.008000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  35

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59962855
   ht   2     0.00000000   -50.58426099
   ht   3     0.00000000     0.00000000   -50.58420672
   ht   4     0.00000000     0.00000000     0.00000000   -50.57616652
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57591939
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.52682514
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.79533367
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.73384191
   ht   9     0.01203393     0.05313281     0.05102416    -0.21630514    -0.00345515    -0.06260295    -0.00785666     0.01311174
   ht  10     2.54499991     1.39103041     0.19696334    -7.89432811     9.59036121     1.27903794    -0.53571912     0.58125170
   ht  11    -0.11945600     0.00650945     0.00502394    -0.01545885     0.02976870    -0.00825350    -0.00063621     0.00883992
   ht  12     0.00082985    -0.01399542    -0.02500005     0.01496562     0.01301478    -0.00530435     0.00432593     0.00109984
   ht  13    -0.00275584    -0.02299419    -0.11391478     0.02534369     0.01775154    -0.03470768     0.00064990    -0.00648515
   ht  14     0.07713990     0.01250441     0.05042058    -0.12769290     0.15503054    -0.07137869    -0.01297390    -0.01178222
   ht  15    -0.00247953    -0.07171917     0.01417853     0.19844803     0.07282546    -0.17134589     0.02200682    -0.00662669

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15
   ht   9    -0.00598752
   ht  10    -0.03557097    -4.80443710
   ht  11    -0.00008231     0.00164038    -0.00079283
   ht  12     0.00003774     0.00084814     0.00000511    -0.00017669
   ht  13     0.00010514     0.00380621    -0.00001991    -0.00007810    -0.00128690
   ht  14    -0.00059537    -0.02943461     0.00004622     0.00003293    -0.00002211    -0.00317748
   ht  15     0.00034219     0.02315066    -0.00001529    -0.00005946    -0.00022194    -0.00004636    -0.00244489

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   2.565538E-05  -0.968192      -4.826629E-02  -2.336588E-03  -1.033907E-04  -7.968825E-04   0.129575      -3.935425E-02
 ref    2  -1.256494E-05  -3.735457E-02   0.750132      -5.286044E-02  -0.591414       0.153213      -2.023229E-02   1.111116E-02
 ref    3  -1.904834E-04   7.790452E-04  -5.477784E-03  -0.656114       0.226809       0.673806       3.299853E-02  -2.731249E-02
 ref    4   0.954933       1.288421E-05   1.740483E-05  -1.597249E-04   2.419709E-05  -5.929268E-05   2.286482E-02   7.008695E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1   4.037201E-03   2.245513E-02   4.779364E-02  -4.046377E-03   1.034931E-02  -6.328271E-04  -9.750992E-03
 ref    2  -4.104001E-02  -4.651492E-02  -1.754256E-02   1.841652E-02   8.750712E-02   5.480696E-02   1.244190E-02
 ref    3  -3.928736E-02  -8.950138E-02   2.172329E-03  -6.069610E-03  -3.435119E-03   4.569392E-02  -0.190539    
 ref    4  -9.597821E-03   2.345913E-02   1.930674E-02   0.211706      -5.064226E-02   2.269180E-03  -4.378649E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.911897       0.938792       0.565058       0.433285       0.401213       0.477490       1.881082E-02   7.330367E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1   3.336196E-03   1.122870E-02   2.969443E-03   4.521160E-02   1.034104E-02   5.097286E-03   3.847231E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00002566    -0.96819227    -0.04826629    -0.00233659    -0.00010339    -0.00079688     0.12957536    -0.03935425
 ref:   2    -0.00001256    -0.03735457     0.75013197    -0.05286044    -0.59141423     0.15321256    -0.02023229     0.01111116
 ref:   3    -0.00019048     0.00077905    -0.00547778    -0.65611397     0.22680884     0.67380625     0.03299853    -0.02731249
 ref:   4     0.95493300     0.00001288     0.00001740    -0.00015972     0.00002420    -0.00005929     0.02286482     0.07008695

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15
 ref:   1     0.00403720     0.02245513     0.04779364    -0.00404638     0.01034931    -0.00063283    -0.00975099
 ref:   2    -0.04104001    -0.04651492    -0.01754256     0.01841652     0.08750712     0.05480696     0.01244190
 ref:   3    -0.03928736    -0.08950138     0.00217233    -0.00606961    -0.00343512     0.04569392    -0.19053916
 ref:   4    -0.00959782     0.02345913     0.01930674     0.21170551    -0.05064226     0.00226918    -0.04378649

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 35  1   -196.4879532198  3.6629E-08  0.0000E+00  4.1201E-03  1.0000E-04   
 mr-sdci # 35  2   -196.4725809912  4.7832E-09  0.0000E+00  1.9430E-03  1.0000E-04   
 mr-sdci # 35  3   -196.4725706606  4.2084E-09  0.0000E+00  6.0880E-03  1.0000E-04   
 mr-sdci # 35  4   -196.4670893183  1.8294E-03  0.0000E+00  9.7248E-02  1.0000E-04   
 mr-sdci # 35  5   -196.4644740436  2.3707E-05  0.0000E+00  4.1315E-03  1.0000E-04   
 mr-sdci # 35  6   -196.4640812411  6.2252E-04  9.4109E-04  4.6889E-02  1.0000E-04   
 mr-sdci # 35  7   -195.7023638409  1.0216E-04  0.0000E+00  3.5246E-01  1.0000E-04   
 mr-sdci # 35  8   -195.6886878943  3.1798E-04  0.0000E+00  4.8313E-01  1.0000E-04   
 mr-sdci # 35  9   -195.4884359897  1.2246E-01  0.0000E+00  1.4125E+00  1.0000E-04   
 mr-sdci # 35 10   -194.4230852879  1.5174E-01  0.0000E+00  2.1856E+00  1.0000E-04   
 mr-sdci # 35 11   -194.0986851371  1.1645E-01  0.0000E+00  2.5871E+00  1.0000E-04   
 mr-sdci # 35 12   -193.3277682619  2.0496E-02  0.0000E+00  3.4767E+00  1.0000E-04   
 mr-sdci # 35 13   -193.0930345017  1.4522E-01  0.0000E+00  3.7491E+00  1.0000E-04   
 mr-sdci # 35 14   -191.6887238592  2.2128E+00  0.0000E+00  5.0264E+00  1.0000E-04   
 mr-sdci # 35 15   -189.1921007807 -1.9741E+00  0.0000E+00  4.8780E+00  1.0000E-04   
 
 root number  6 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  36

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59962855
   ht   2     0.00000000   -50.58426099
   ht   3     0.00000000     0.00000000   -50.58420672
   ht   4     0.00000000     0.00000000     0.00000000   -50.57616652
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57591939
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.52682514
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.79533367
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.73384191
   ht   9     0.01203393     0.05313281     0.05102416    -0.21630514    -0.00345515    -0.06260295    -0.00785666     0.01311174
   ht  10     2.54499991     1.39103041     0.19696334    -7.89432811     9.59036121     1.27903794    -0.53571912     0.58125170
   ht  11    -0.11945600     0.00650945     0.00502394    -0.01545885     0.02976870    -0.00825350    -0.00063621     0.00883992
   ht  12     0.00082985    -0.01399542    -0.02500005     0.01496562     0.01301478    -0.00530435     0.00432593     0.00109984
   ht  13    -0.00275584    -0.02299419    -0.11391478     0.02534369     0.01775154    -0.03470768     0.00064990    -0.00648515
   ht  14     0.07713990     0.01250441     0.05042058    -0.12769290     0.15503054    -0.07137869    -0.01297390    -0.01178222
   ht  15    -0.00247953    -0.07171917     0.01417853     0.19844803     0.07282546    -0.17134589     0.02200682    -0.00662669
   ht  16    -0.06142692     0.02044866     0.24355635    -0.14089890    -0.68276640    -0.53915107    -0.02071820     0.01050817

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15         ht  16
   ht   9    -0.00598752
   ht  10    -0.03557097    -4.80443710
   ht  11    -0.00008231     0.00164038    -0.00079283
   ht  12     0.00003774     0.00084814     0.00000511    -0.00017669
   ht  13     0.00010514     0.00380621    -0.00001991    -0.00007810    -0.00128690
   ht  14    -0.00059537    -0.02943461     0.00004622     0.00003293    -0.00002211    -0.00317748
   ht  15     0.00034219     0.02315066    -0.00001529    -0.00005946    -0.00022194    -0.00004636    -0.00244489
   ht  16     0.00080852     0.13557866     0.00014875     0.00026814     0.00038183     0.00145815     0.00015032    -0.04291123

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   2.285291E-05  -0.968342      -4.447174E-02   8.140677E-03  -1.035147E-04   4.432854E-05  -0.129961      -3.955434E-02
 ref    2  -1.195987E-05  -3.439719E-02   0.750304       6.070526E-03  -0.589153      -0.169601       2.187982E-02   1.050092E-02
 ref    3   7.325294E-05   3.650520E-03  -3.003244E-03   0.444983       0.238205      -0.827409      -3.020974E-02  -2.849536E-02
 ref    4   0.954933       9.447948E-06   1.436524E-05  -3.744726E-04   2.486227E-05  -4.393578E-05  -2.304232E-02   6.999891E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1  -1.366918E-02  -6.088315E-03   5.006190E-02  -2.688340E-03  -1.119130E-02  -2.854997E-03   1.846415E-04  -1.000168E-02
 ref    2   5.791427E-02  -9.631785E-03  -2.293342E-02   1.609433E-02  -8.146234E-02   5.020484E-02  -4.414058E-02   1.282551E-02
 ref    3   9.010851E-02   1.616279E-02  -8.196233E-03  -1.306101E-02   8.007403E-03   2.453970E-02  -4.883634E-02  -0.187782    
 ref    4   3.658393E-03  -1.064337E-02   2.330497E-02   0.210649       5.873187E-02   2.519252E-03  -5.034567E-03  -4.304696E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.911898       0.938883       0.564943       0.198113       0.403843       0.713369       1.881218E-02   7.386649E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   1.167384E-02   5.043561E-04   3.642436E-03   4.480974E-02   1.027491E-02   3.137221E-03   4.358760E-03   3.737953E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15        ev   16

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00002285    -0.96834213    -0.04447174     0.00814068    -0.00010351     0.00004433    -0.12996106    -0.03955434
 ref:   2    -0.00001196    -0.03439719     0.75030416     0.00607053    -0.58915321    -0.16960072     0.02187982     0.01050092
 ref:   3     0.00007325     0.00365052    -0.00300324     0.44498285     0.23820503    -0.82740859    -0.03020974    -0.02849536
 ref:   4     0.95493333     0.00000945     0.00001437    -0.00037447     0.00002486    -0.00004394    -0.02304232     0.06999891

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1    -0.01366918    -0.00608831     0.05006190    -0.00268834    -0.01119130    -0.00285500     0.00018464    -0.01000168
 ref:   2     0.05791427    -0.00963179    -0.02293342     0.01609433    -0.08146234     0.05020484    -0.04414058     0.01282551
 ref:   3     0.09010851     0.01616279    -0.00819623    -0.01306101     0.00800740     0.02453970    -0.04883634    -0.18778167
 ref:   4     0.00365839    -0.01064337     0.02330497     0.21064876     0.05873187     0.00251925    -0.00503457    -0.04304696

 trial vector basis is being transformed.  new dimension:   8

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 36  1   -196.4879532565  3.6708E-08  4.7899E-06  4.1194E-03  1.0000E-04   
 mr-sdci # 36  2   -196.4725810384  4.7122E-08  0.0000E+00  1.8488E-03  1.0000E-04   
 mr-sdci # 36  3   -196.4725706949  3.4344E-08  0.0000E+00  6.1404E-03  1.0000E-04   
 mr-sdci # 36  4   -196.4716965503  4.6072E-03  0.0000E+00  6.5763E-02  1.0000E-04   
 mr-sdci # 36  5   -196.4644740468  3.2234E-09  0.0000E+00  4.1016E-03  1.0000E-04   
 mr-sdci # 36  6   -196.4644574069  3.7617E-04  0.0000E+00  1.0550E-02  1.0000E-04   
 mr-sdci # 36  7   -195.7024790584  1.1522E-04  0.0000E+00  3.5447E-01  1.0000E-04   
 mr-sdci # 36  8   -195.6887035165  1.5622E-05  0.0000E+00  4.8268E-01  1.0000E-04   
 mr-sdci # 36  9   -195.6009395895  1.1250E-01  0.0000E+00  7.9469E-01  1.0000E-04   
 mr-sdci # 36 10   -195.4419400106  1.0189E+00  0.0000E+00  1.4674E+00  1.0000E-04   
 mr-sdci # 36 11   -194.1042208414  5.5357E-03  0.0000E+00  2.6272E+00  1.0000E-04   
 mr-sdci # 36 12   -193.3402079377  1.2440E-02  0.0000E+00  3.4673E+00  1.0000E-04   
 mr-sdci # 36 13   -193.1013635376  8.3290E-03  0.0000E+00  3.6697E+00  1.0000E-04   
 mr-sdci # 36 14   -192.4809845155  7.9226E-01  0.0000E+00  3.3400E+00  1.0000E-04   
 mr-sdci # 36 15   -191.5453477177  2.3532E+00  0.0000E+00  4.7400E+00  1.0000E-04   
 mr-sdci # 36 16   -189.1660825112 -1.8068E+00  0.0000E+00  4.9240E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.016000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  37

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59963846
   ht   2     0.00000000   -50.58426624
   ht   3     0.00000000     0.00000000   -50.58425590
   ht   4     0.00000000     0.00000000     0.00000000   -50.58338175
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57615925
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57614261
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.81416426
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.80038872
   ht   9    -0.07004457    -0.00823692     0.00152686    -0.00542086     0.00499060    -0.01515855    -0.00160834    -0.00137124

                ht   9
   ht   9    -0.00020291

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -3.009618E-06   0.968348      -4.434152E-02   8.232069E-03  -1.038413E-04   4.503252E-05  -1.813583E-02  -0.134632    
 ref    2   1.093334E-05   3.429570E-02   0.750309       6.061391E-03  -0.589004      -0.170119      -6.143797E-04   2.425430E-02
 ref    3  -9.021546E-05  -3.691442E-03  -2.996534E-03   0.444948       0.238937      -0.827213       1.046244E-02  -3.878963E-02
 ref    4  -0.955067      -5.872655E-06   1.374949E-05  -4.069589E-04   2.493330E-05  -4.399359E-05  -5.336005E-02   4.937610E-03

              v      9
 ref    1  -1.025747E-02
 ref    2   6.507050E-04
 ref    3  -2.722182E-02
 ref    4   0.171921    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.912153       0.938887       0.564939       0.198084       0.404016       0.713222       3.286043E-03   2.024298E-02

              v      9
 ref    1   3.040341E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00000301     0.96834780    -0.04434152     0.00823207    -0.00010384     0.00004503    -0.01813583    -0.13463170
 ref:   2     0.00001093     0.03429570     0.75030889     0.00606139    -0.58900385    -0.17011857    -0.00061438     0.02425430
 ref:   3    -0.00009022    -0.00369144    -0.00299653     0.44494837     0.23893663    -0.82721311     0.01046244    -0.03878963
 ref:   4    -0.95506679    -0.00000587     0.00001375    -0.00040696     0.00002493    -0.00004399    -0.05336005     0.00493761

                ci   9
 ref:   1    -0.01025747
 ref:   2     0.00065070
 ref:   3    -0.02722182
 ref:   4     0.17192073

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 37  1   -196.4879565523  3.2958E-06  0.0000E+00  2.4253E-03  1.0000E-04   
 mr-sdci # 37  2   -196.4725810464  8.0800E-09  1.4565E-06  1.8440E-03  1.0000E-04   
 mr-sdci # 37  3   -196.4725706952  2.3945E-10  0.0000E+00  6.1413E-03  1.0000E-04   
 mr-sdci # 37  4   -196.4716975054  9.5513E-07  0.0000E+00  6.5547E-02  1.0000E-04   
 mr-sdci # 37  5   -196.4644740520  5.2494E-09  0.0000E+00  4.0913E-03  1.0000E-04   
 mr-sdci # 37  6   -196.4644574475  4.0593E-08  0.0000E+00  1.0508E-02  1.0000E-04   
 mr-sdci # 37  7   -195.7308557770  2.8377E-02  0.0000E+00  4.8652E-01  1.0000E-04   
 mr-sdci # 37  8   -195.7008464862  1.2143E-02  0.0000E+00  3.3642E-01  1.0000E-04   
 mr-sdci # 37  9   -192.9905374794 -2.6104E+00  0.0000E+00  4.0899E+00  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  38

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59963846
   ht   2     0.00000000   -50.58426624
   ht   3     0.00000000     0.00000000   -50.58425590
   ht   4     0.00000000     0.00000000     0.00000000   -50.58338175
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57615925
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57614261
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.81416426
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.80038872
   ht   9    -0.07004457    -0.00823692     0.00152686    -0.00542086     0.00499060    -0.01515855    -0.00160834    -0.00137124
   ht  10    -0.00166477    -0.00314938    -0.00352392     0.00593660    -0.00768446    -0.00242084    -0.00060718    -0.00050063

                ht   9         ht  10
   ht   9    -0.00020291
   ht  10    -0.00000069    -0.00004356

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   1.962073E-06  -0.969092      -2.144008E-02   9.281593E-03   9.636117E-05  -9.186206E-05   5.393185E-02  -0.124039    
 ref    2  -1.061527E-05  -1.655169E-02   0.750911       5.914643E-03   0.589156       0.169596      -5.084623E-03   2.150936E-02
 ref    3   9.058546E-05   4.062586E-03  -2.992784E-03   0.444920      -0.238196       0.827441       2.724513E-04  -3.965190E-02
 ref    4   0.955067       1.289395E-06   1.262124E-05  -4.103401E-04  -2.472285E-05   4.509294E-05   4.979622E-02   1.971185E-02

              v      9       v     10
 ref    1   1.482154E-02   9.743740E-03
 ref    2  -1.955500E-02   7.901693E-04
 ref    3   7.840093E-03   2.686307E-02
 ref    4  -2.985234E-03  -0.172206    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.912152       0.939430       0.564335       0.198075       0.403842       0.713421       5.414236E-03   1.780916E-02

              v      9       v     10
 ref    1   6.724550E-04   3.047209E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00000196    -0.96909211    -0.02144008     0.00928159     0.00009636    -0.00009186     0.05393185    -0.12403902
 ref:   2    -0.00001062    -0.01655169     0.75091069     0.00591464     0.58915590     0.16959637    -0.00508462     0.02150936
 ref:   3     0.00009059     0.00406259    -0.00299278     0.44492012    -0.23819622     0.82744074     0.00027245    -0.03965190
 ref:   4     0.95506674     0.00000129     0.00001262    -0.00041034    -0.00002472     0.00004509     0.04979622     0.01971185

                ci   9         ci  10
 ref:   1     0.01482154     0.00974374
 ref:   2    -0.01955500     0.00079017
 ref:   3     0.00784009     0.02686307
 ref:   4    -0.00298523    -0.17220598

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 38  1   -196.4879565528  4.6999E-10  0.0000E+00  2.4244E-03  1.0000E-04   
 mr-sdci # 38  2   -196.4725822591  1.2126E-06  0.0000E+00  1.0345E-03  1.0000E-04   
 mr-sdci # 38  3   -196.4725707500  5.4858E-08  9.8944E-06  6.1559E-03  1.0000E-04   
 mr-sdci # 38  4   -196.4716982247  7.1929E-07  0.0000E+00  6.5528E-02  1.0000E-04   
 mr-sdci # 38  5   -196.4644740544  2.3406E-09  0.0000E+00  4.0972E-03  1.0000E-04   
 mr-sdci # 38  6   -196.4644575407  9.3229E-08  0.0000E+00  1.0527E-02  1.0000E-04   
 mr-sdci # 38  7   -195.7323653476  1.5096E-03  0.0000E+00  4.6194E-01  1.0000E-04   
 mr-sdci # 38  8   -195.7196561176  1.8810E-02  0.0000E+00  3.2364E-01  1.0000E-04   
 mr-sdci # 38  9   -194.3435068166  1.3530E+00  0.0000E+00  2.0187E+00  1.0000E-04   
 mr-sdci # 38 10   -192.9820287214 -2.4599E+00  0.0000E+00  4.0977E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.008000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  39

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59963846
   ht   2     0.00000000   -50.58426624
   ht   3     0.00000000     0.00000000   -50.58425590
   ht   4     0.00000000     0.00000000     0.00000000   -50.58338175
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57615925
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57614261
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.81416426
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.80038872
   ht   9    -0.07004457    -0.00823692     0.00152686    -0.00542086     0.00499060    -0.01515855    -0.00160834    -0.00137124
   ht  10    -0.00166477    -0.00314938    -0.00352392     0.00593660    -0.00768446    -0.00242084    -0.00060718    -0.00050063
   ht  11    -0.00080478     0.00822072    -0.09787040     0.02267157    -0.00177439    -0.00209832    -0.00133726     0.00178158

                ht   9         ht  10         ht  11
   ht   9    -0.00020291
   ht  10    -0.00000069    -0.00004356
   ht  11     0.00000452    -0.00000547    -0.00039146

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   1.962020E-06  -0.969042      -2.353520E-02  -9.375997E-03   9.515935E-05  -9.574925E-05  -5.383776E-02   0.124037    
 ref    2  -1.031604E-05  -1.821331E-02   0.750918      -2.569018E-03   0.589741       0.167747       5.035105E-03  -2.177805E-02
 ref    3   9.059981E-05   4.062966E-03  -1.159993E-03  -0.444835      -0.235439       0.828275      -2.202399E-04   3.981902E-02
 ref    4   0.955067       1.247229E-06   1.676103E-05   4.072489E-04  -2.484488E-05   4.409221E-05  -4.981477E-02  -1.969438E-02

              v      9       v     10       v     11
 ref    1  -1.525509E-02   5.201473E-03   1.056223E-02
 ref    2   1.660888E-02   5.111734E-02   7.823370E-03
 ref    3  -6.132190E-03  -3.245006E-02   2.265602E-02
 ref    4   2.655817E-03   2.595908E-02  -0.170258    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.912152       0.939392       0.564434       0.197973       0.403226       0.714178       5.405417E-03   1.783295E-02

              v      9       v     10       v     11
 ref    1   5.532298E-04   4.366917E-03   2.967393E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00000196    -0.96904246    -0.02353520    -0.00937600     0.00009516    -0.00009575    -0.05383776     0.12403727
 ref:   2    -0.00001032    -0.01821331     0.75091827    -0.00256902     0.58974144     0.16774668     0.00503510    -0.02177805
 ref:   3     0.00009060     0.00406297    -0.00115999    -0.44483481    -0.23543888     0.82827457    -0.00022024     0.03981902
 ref:   4     0.95506674     0.00000125     0.00001676     0.00040725    -0.00002484     0.00004409    -0.04981477    -0.01969438

                ci   9         ci  10         ci  11
 ref:   1    -0.01525509     0.00520147     0.01056223
 ref:   2     0.01660888     0.05111734     0.00782337
 ref:   3    -0.00613219    -0.03245006     0.02265602
 ref:   4     0.00265582     0.02595908    -0.17025824

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 39  1   -196.4879565528  3.6806E-12  0.0000E+00  2.4243E-03  1.0000E-04   
 mr-sdci # 39  2   -196.4725822591  3.1093E-11  0.0000E+00  1.0368E-03  1.0000E-04   
 mr-sdci # 39  3   -196.4725781650  7.4150E-06  0.0000E+00  3.1225E-03  1.0000E-04   
 mr-sdci # 39  4   -196.4717002887  2.0640E-06  1.0739E-03  6.5217E-02  1.0000E-04   
 mr-sdci # 39  5   -196.4644740679  1.3562E-08  0.0000E+00  4.1255E-03  1.0000E-04   
 mr-sdci # 39  6   -196.4644577591  2.1836E-07  0.0000E+00  1.0283E-02  1.0000E-04   
 mr-sdci # 39  7   -195.7323665877  1.2401E-06  0.0000E+00  4.6208E-01  1.0000E-04   
 mr-sdci # 39  8   -195.7197394965  8.3379E-05  0.0000E+00  3.2534E-01  1.0000E-04   
 mr-sdci # 39  9   -194.3475036568  3.9968E-03  0.0000E+00  2.0742E+00  1.0000E-04   
 mr-sdci # 39 10   -193.1750826368  1.9305E-01  0.0000E+00  4.0493E+00  1.0000E-04   
 mr-sdci # 39 11   -192.9783698073 -1.1259E+00  0.0000E+00  4.1053E+00  1.0000E-04   
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  40

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59963846
   ht   2     0.00000000   -50.58426624
   ht   3     0.00000000     0.00000000   -50.58425590
   ht   4     0.00000000     0.00000000     0.00000000   -50.58338175
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57615925
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57614261
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.81416426
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.80038872
   ht   9    -0.07004457    -0.00823692     0.00152686    -0.00542086     0.00499060    -0.01515855    -0.00160834    -0.00137124
   ht  10    -0.00166477    -0.00314938    -0.00352392     0.00593660    -0.00768446    -0.00242084    -0.00060718    -0.00050063
   ht  11    -0.00080478     0.00822072    -0.09787040     0.02267157    -0.00177439    -0.00209832    -0.00133726     0.00178158
   ht  12    -0.02833286    -0.04682200     0.35524729     0.95996908     0.62626565     0.40944471    -0.01516700     0.01791890

                ht   9         ht  10         ht  11         ht  12
   ht   9    -0.00020291
   ht  10    -0.00000069    -0.00004356
   ht  11     0.00000452    -0.00000547    -0.00039146
   ht  12     0.00007830     0.00000018     0.00016881    -0.05223716

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   1.588039E-06  -0.969043       2.214119E-02   1.224927E-02  -8.587729E-05   1.603066E-05  -5.336762E-02  -0.124244    
 ref    2  -9.262568E-06  -1.717929E-02  -0.750934      -2.868136E-03  -0.546709       0.277611       5.089677E-03   2.163667E-02
 ref    3   7.124410E-05   5.306717E-03  -9.536151E-04   0.435897       0.390920       0.772573      -1.657305E-04  -3.970692E-02
 ref    4   0.955067       9.933225E-08  -1.476656E-05  -4.076977E-04   3.218430E-05   3.625223E-05  -4.986386E-02   1.947901E-02

              v      9       v     10       v     11       v     12
 ref    1   1.524695E-02  -3.762384E-03  -1.111611E-02  -7.841807E-04
 ref    2  -1.487531E-02  -5.803840E-02  -1.068379E-02   2.463195E-02
 ref    3   4.418693E-03   4.369697E-02  -2.189489E-02  -3.151624E-02
 ref    4  -2.035867E-03  -3.994981E-02   0.165844      -2.388446E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.912153       0.939368       0.564393       0.190165       0.451709       0.673936       5.360440E-03   1.786085E-02

              v      9       v     10       v     11       v     12
 ref    1   4.774139E-04   6.888025E-03   2.822124E-02   2.171089E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00000159    -0.96904303     0.02214119     0.01224927    -0.00008588     0.00001603    -0.05336762    -0.12424426
 ref:   2    -0.00000926    -0.01717929    -0.75093392    -0.00286814    -0.54670872     0.27761056     0.00508968     0.02163667
 ref:   3     0.00007124     0.00530672    -0.00095362     0.43589749     0.39091972     0.77257251    -0.00016573    -0.03970692
 ref:   4     0.95506683     0.00000010    -0.00001477    -0.00040770     0.00003218     0.00003625    -0.04986386     0.01947901

                ci   9         ci  10         ci  11         ci  12
 ref:   1     0.01524695    -0.00376238    -0.01111611    -0.00078418
 ref:   2    -0.01487531    -0.05803840    -0.01068379     0.02463195
 ref:   3     0.00441869     0.04369697    -0.02189489    -0.03151624
 ref:   4    -0.00203587    -0.03994981     0.16584372    -0.02388446

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 40  1   -196.4879565539  1.1240E-09  0.0000E+00  2.4256E-03  1.0000E-04   
 mr-sdci # 40  2   -196.4725822625  3.3547E-09  0.0000E+00  1.0336E-03  1.0000E-04   
 mr-sdci # 40  3   -196.4725781746  9.5550E-09  0.0000E+00  3.1116E-03  1.0000E-04   
 mr-sdci # 40  4   -196.4723046413  6.0435E-04  0.0000E+00  2.1390E-02  1.0000E-04   
 mr-sdci # 40  5   -196.4644743326  2.6469E-07  4.9791E-06  3.9207E-03  1.0000E-04   
 mr-sdci # 40  6   -196.4644692437  1.1485E-05  0.0000E+00  7.8668E-03  1.0000E-04   
 mr-sdci # 40  7   -195.7324067264  4.0139E-05  0.0000E+00  4.6217E-01  1.0000E-04   
 mr-sdci # 40  8   -195.7197951143  5.5618E-05  0.0000E+00  3.2359E-01  1.0000E-04   
 mr-sdci # 40  9   -194.3502097989  2.7061E-03  0.0000E+00  2.0772E+00  1.0000E-04   
 mr-sdci # 40 10   -193.2503084142  7.5226E-02  0.0000E+00  3.8912E+00  1.0000E-04   
 mr-sdci # 40 11   -192.9854109371  7.0411E-03  0.0000E+00  4.1029E+00  1.0000E-04   
 mr-sdci # 40 12   -191.9984542636 -1.3418E+00  0.0000E+00  4.3270E+00  1.0000E-04   
 
 root number  5 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.008000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  41

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59963846
   ht   2     0.00000000   -50.58426624
   ht   3     0.00000000     0.00000000   -50.58425590
   ht   4     0.00000000     0.00000000     0.00000000   -50.58338175
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57615925
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57614261
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.81416426
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.80038872
   ht   9    -0.07004457    -0.00823692     0.00152686    -0.00542086     0.00499060    -0.01515855    -0.00160834    -0.00137124
   ht  10    -0.00166477    -0.00314938    -0.00352392     0.00593660    -0.00768446    -0.00242084    -0.00060718    -0.00050063
   ht  11    -0.00080478     0.00822072    -0.09787040     0.02267157    -0.00177439    -0.00209832    -0.00133726     0.00178158
   ht  12    -0.02833286    -0.04682200     0.35524729     0.95996908     0.62626565     0.40944471    -0.01516700     0.01791890
   ht  13     0.00636938     0.01081193     0.01621932    -0.00275533    -0.04165181    -0.02039140     0.00280569     0.00031696

                ht   9         ht  10         ht  11         ht  12         ht  13
   ht   9    -0.00020291
   ht  10    -0.00000069    -0.00004356
   ht  11     0.00000452    -0.00000547    -0.00039146
   ht  12     0.00007830     0.00000018     0.00016881    -0.05223716
   ht  13     0.00000521    -0.00000319     0.00002273     0.00046605    -0.00016548

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -1.979792E-06  -0.969012       2.314876E-02   1.276044E-02  -3.261807E-05   9.677499E-06  -5.326277E-02  -0.123715    
 ref    2   1.140044E-05  -1.791383E-02  -0.750892       1.841091E-03  -0.558374       0.253501       5.080153E-03   2.115949E-02
 ref    3  -6.460734E-05   5.634342E-03   1.014643E-03   0.435461       0.357651       0.788851      -1.105098E-04  -4.047531E-02
 ref    4  -0.955067       6.656766E-08  -1.490184E-05  -3.350885E-04   9.824462E-06   3.862009E-05  -4.987781E-02   1.923375E-02

              v      9       v     10       v     11       v     12       v     13
 ref    1   2.585266E-02  -1.757083E-02   2.759000E-04   1.330879E-02   1.443626E-02
 ref    2  -8.460594E-03   1.521095E-02  -5.938925E-02  -9.838168E-03  -5.263152E-02
 ref    3  -4.825979E-02   2.857557E-04   1.573209E-02   2.505011E-02   2.417045E-02
 ref    4   2.500903E-04   1.654407E-03   1.302060E-02  -0.172969      -6.574385E-04

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.912152       0.939336       0.564376       0.189793       0.439696       0.686549       5.350538E-03   1.776129E-02

              v      9       v     10       v     11       v     12       v     13
 ref    1   3.069012E-03   5.429258E-04   3.944194E-03   3.081971E-02   3.563126E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00000198    -0.96901173     0.02314876     0.01276044    -0.00003262     0.00000968    -0.05326277    -0.12371492
 ref:   2     0.00001140    -0.01791383    -0.75089240     0.00184109    -0.55837390     0.25350145     0.00508015     0.02115949
 ref:   3    -0.00006461     0.00563434     0.00101464     0.43546108     0.35765091     0.78885112    -0.00011051    -0.04047531
 ref:   4    -0.95506657     0.00000007    -0.00001490    -0.00033509     0.00000982     0.00003862    -0.04987781     0.01923375

                ci   9         ci  10         ci  11         ci  12         ci  13
 ref:   1     0.02585266    -0.01757083     0.00027590     0.01330879     0.01443626
 ref:   2    -0.00846059     0.01521095    -0.05938925    -0.00983817    -0.05263152
 ref:   3    -0.04825979     0.00028576     0.01573209     0.02505011     0.02417045
 ref:   4     0.00025009     0.00165441     0.01302060    -0.17296904    -0.00065744

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 41  1   -196.4879565550  1.0935E-09  0.0000E+00  2.4245E-03  1.0000E-04   
 mr-sdci # 41  2   -196.4725822631  6.7669E-10  0.0000E+00  1.0395E-03  1.0000E-04   
 mr-sdci # 41  3   -196.4725781983  2.3759E-08  0.0000E+00  3.0957E-03  1.0000E-04   
 mr-sdci # 41  4   -196.4723572977  5.2656E-05  0.0000E+00  2.2662E-02  1.0000E-04   
 mr-sdci # 41  5   -196.4644789311  4.5985E-06  0.0000E+00  2.5199E-03  1.0000E-04   
 mr-sdci # 41  6   -196.4644692641  2.0357E-08  1.4023E-05  7.8285E-03  1.0000E-04   
 mr-sdci # 41  7   -195.7324069903  2.6386E-07  0.0000E+00  4.6226E-01  1.0000E-04   
 mr-sdci # 41  8   -195.7203170484  5.2193E-04  0.0000E+00  3.2610E-01  1.0000E-04   
 mr-sdci # 41  9   -194.5723290329  2.2212E-01  0.0000E+00  2.5492E+00  1.0000E-04   
 mr-sdci # 41 10   -194.3483928225  1.0981E+00  0.0000E+00  2.1238E+00  1.0000E-04   
 mr-sdci # 41 11   -193.0731477641  8.7737E-02  0.0000E+00  4.1710E+00  1.0000E-04   
 mr-sdci # 41 12   -192.9671199336  9.6867E-01  0.0000E+00  4.1036E+00  1.0000E-04   
 mr-sdci # 41 13   -191.3863612230 -1.7150E+00  0.0000E+00  4.1497E+00  1.0000E-04   
 
 root number  6 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  42

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59963846
   ht   2     0.00000000   -50.58426624
   ht   3     0.00000000     0.00000000   -50.58425590
   ht   4     0.00000000     0.00000000     0.00000000   -50.58338175
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57615925
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57614261
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.81416426
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.80038872
   ht   9    -0.07004457    -0.00823692     0.00152686    -0.00542086     0.00499060    -0.01515855    -0.00160834    -0.00137124
   ht  10    -0.00166477    -0.00314938    -0.00352392     0.00593660    -0.00768446    -0.00242084    -0.00060718    -0.00050063
   ht  11    -0.00080478     0.00822072    -0.09787040     0.02267157    -0.00177439    -0.00209832    -0.00133726     0.00178158
   ht  12    -0.02833286    -0.04682200     0.35524729     0.95996908     0.62626565     0.40944471    -0.01516700     0.01791890
   ht  13     0.00636938     0.01081193     0.01621932    -0.00275533    -0.04165181    -0.02039140     0.00280569     0.00031696
   ht  14     0.01719868    -0.01572972     0.00644134    -0.11893496     0.01025055    -0.01257805     0.00035297    -0.00127087

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14
   ht   9    -0.00020291
   ht  10    -0.00000069    -0.00004356
   ht  11     0.00000452    -0.00000547    -0.00039146
   ht  12     0.00007830     0.00000018     0.00016881    -0.05223716
   ht  13     0.00000521    -0.00000319     0.00002273     0.00046605    -0.00016548
   ht  14    -0.00000032     0.00001687     0.00006034     0.00158886    -0.00001103    -0.00058061

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -8.681917E-07   0.969024       2.272465E-02   1.264275E-02   4.047499E-05  -1.232675E-05   5.292421E-02   0.124007    
 ref    2   9.499896E-06   1.756204E-02  -0.750911       3.162242E-03   0.437746       0.429413      -5.042318E-03  -2.112024E-02
 ref    3  -6.628796E-05  -5.585527E-03   1.854225E-03   0.434711      -0.606428       0.618943      -1.441836E-05   4.058777E-02
 ref    4  -0.955066       8.392439E-08  -1.223989E-05  -2.896387E-04  -3.068702E-06  -1.516021E-05   4.987857E-02  -1.893385E-02

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1  -1.399959E-02   1.741791E-02   2.273851E-02  -2.273897E-02  -3.127007E-02   1.115059E-02
 ref    2   7.215646E-03  -1.516852E-02  -5.979582E-02  -1.196364E-02  -4.018622E-03   5.502265E-02
 ref    3   4.418492E-02  -9.841214E-05   1.240965E-02  -1.100519E-02   2.472491E-02  -3.640666E-02
 ref    4   6.462577E-03  -1.683890E-03  -2.551248E-02   0.149001      -8.826504E-02   2.739693E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.912151       0.939347       0.564386       0.189143       0.559377       0.567486       5.314269E-03   1.782970E-02

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1   2.242126E-03   5.363127E-04   4.897465E-03   2.298263E-02   9.396005E-03   5.227864E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00000087     0.96902414     0.02272465     0.01264275     0.00004047    -0.00001233     0.05292421     0.12400717
 ref:   2     0.00000950     0.01756204    -0.75091054     0.00316224     0.43774626     0.42941308    -0.00504232    -0.02112024
 ref:   3    -0.00006629    -0.00558553     0.00185422     0.43471050    -0.60642841     0.61894270    -0.00001442     0.04058777
 ref:   4    -0.95506576     0.00000008    -0.00001224    -0.00028964    -0.00000307    -0.00001516     0.04987857    -0.01893385

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14
 ref:   1    -0.01399959     0.01741791     0.02273851    -0.02273897    -0.03127007     0.01115059
 ref:   2     0.00721565    -0.01516852    -0.05979582    -0.01196364    -0.00401862     0.05502265
 ref:   3     0.04418492    -0.00009841     0.01240965    -0.01100519     0.02472491    -0.03640666
 ref:   4     0.00646258    -0.00168389    -0.02551248     0.14900110    -0.08826504     0.02739693

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 42  1   -196.4879565580  2.9727E-09  2.0555E-06  2.4193E-03  1.0000E-04   
 mr-sdci # 42  2   -196.4725822633  1.1107E-10  0.0000E+00  1.0428E-03  1.0000E-04   
 mr-sdci # 42  3   -196.4725782306  3.2282E-08  0.0000E+00  3.0175E-03  1.0000E-04   
 mr-sdci # 42  4   -196.4723634439  6.1461E-06  0.0000E+00  2.2230E-02  1.0000E-04   
 mr-sdci # 42  5   -196.4644790515  1.2037E-07  0.0000E+00  2.3598E-03  1.0000E-04   
 mr-sdci # 42  6   -196.4644781129  8.8488E-06  0.0000E+00  2.4065E-03  1.0000E-04   
 mr-sdci # 42  7   -195.7324142180  7.2277E-06  0.0000E+00  4.6276E-01  1.0000E-04   
 mr-sdci # 42  8   -195.7204324505  1.1540E-04  0.0000E+00  3.2477E-01  1.0000E-04   
 mr-sdci # 42  9   -194.7457370621  1.7341E-01  0.0000E+00  2.1332E+00  1.0000E-04   
 mr-sdci # 42 10   -194.3483991042  6.2817E-06  0.0000E+00  2.1206E+00  1.0000E-04   
 mr-sdci # 42 11   -193.1321804773  5.9033E-02  0.0000E+00  3.9161E+00  1.0000E-04   
 mr-sdci # 42 12   -192.9993577734  3.2238E-02  0.0000E+00  4.0235E+00  1.0000E-04   
 mr-sdci # 42 13   -192.7369947478  1.3506E+00  0.0000E+00  3.6292E+00  1.0000E-04   
 mr-sdci # 42 14   -190.5953539812 -1.8856E+00  0.0000E+00  5.2789E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.018000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  43

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59963846
   ht   2     0.00000000   -50.58426624
   ht   3     0.00000000     0.00000000   -50.58425590
   ht   4     0.00000000     0.00000000     0.00000000   -50.58338175
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57615925
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57614261
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.81416426
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.80038872
   ht   9    -0.07004457    -0.00823692     0.00152686    -0.00542086     0.00499060    -0.01515855    -0.00160834    -0.00137124
   ht  10    -0.00166477    -0.00314938    -0.00352392     0.00593660    -0.00768446    -0.00242084    -0.00060718    -0.00050063
   ht  11    -0.00080478     0.00822072    -0.09787040     0.02267157    -0.00177439    -0.00209832    -0.00133726     0.00178158
   ht  12    -0.02833286    -0.04682200     0.35524729     0.95996908     0.62626565     0.40944471    -0.01516700     0.01791890
   ht  13     0.00636938     0.01081193     0.01621932    -0.00275533    -0.04165181    -0.02039140     0.00280569     0.00031696
   ht  14     0.01719868    -0.01572972     0.00644134    -0.11893496     0.01025055    -0.01257805     0.00035297    -0.00127087
   ht  15    -0.04438927    -0.00399582     0.00109018    -0.00050367     0.00107927    -0.00590657     0.00021479    -0.00248742

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15
   ht   9    -0.00020291
   ht  10    -0.00000069    -0.00004356
   ht  11     0.00000452    -0.00000547    -0.00039146
   ht  12     0.00007830     0.00000018     0.00016881    -0.05223716
   ht  13     0.00000521    -0.00000319     0.00002273     0.00046605    -0.00016548
   ht  14    -0.00000032     0.00001687     0.00006034     0.00158886    -0.00001103    -0.00058061
   ht  15    -0.00005374    -0.00000154    -0.00000031    -0.00000598     0.00000565     0.00001256    -0.00009636

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -1.357836E-06   0.969023       2.275233E-02   1.266459E-02   4.048548E-05  -1.236201E-05   1.141528E-02  -0.134346    
 ref    2  -2.729867E-08   1.758178E-02  -0.750910       3.281262E-03   0.437581       0.429581      -6.298088E-04   2.146767E-02
 ref    3   1.529760E-05  -5.596571E-03   1.926219E-03   0.434715      -0.606666       0.618705      -1.027011E-02  -3.833863E-02
 ref    4   0.954947      -6.119858E-07  -7.757183E-06  -5.772213E-05  -3.983677E-06  -7.021522E-06   7.002737E-02   3.356576E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1   3.304664E-04   1.555484E-02   1.660569E-02  -2.025038E-02  -3.895282E-02  -1.231841E-02   8.923763E-03
 ref    2  -6.954452E-03  -9.441590E-03  -1.458047E-02   6.064217E-02  -7.051083E-03  -2.002651E-02   5.208266E-02
 ref    3   2.051458E-02  -3.979523E-02  -1.711520E-04  -1.052133E-02   1.665369E-02  -1.951488E-02  -4.091104E-02
 ref    4   1.614935E-02  -8.348683E-03   9.767063E-04   5.902666E-03  -7.536156E-03   0.264892       8.040163E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.911924       0.939346       0.564387       0.189149       0.559521       0.567336       5.140013E-03   1.999076E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1   7.301233E-04   1.984457E-03   4.893224E-04   4.233091E-03   1.901179E-03   7.110166E-02   1.093037E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00000136     0.96902322     0.02275233     0.01266459     0.00004049    -0.00001236     0.01141528    -0.13434577
 ref:   2    -0.00000003     0.01758178    -0.75090965     0.00328126     0.43758123     0.42958098    -0.00062981     0.02146767
 ref:   3     0.00001530    -0.00559657     0.00192622     0.43471525    -0.60666575     0.61870532    -0.01027011    -0.03833863
 ref:   4     0.95494694    -0.00000061    -0.00000776    -0.00005772    -0.00000398    -0.00000702     0.07002737     0.00335658

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15
 ref:   1     0.00033047     0.01555484     0.01660569    -0.02025038    -0.03895282    -0.01231841     0.00892376
 ref:   2    -0.00695445    -0.00944159    -0.01458047     0.06064217    -0.00705108    -0.02002651     0.05208266
 ref:   3     0.02051458    -0.03979523    -0.00017115    -0.01052133     0.01665369    -0.01951488    -0.04091104
 ref:   4     0.01614935    -0.00834868     0.00097671     0.00590267    -0.00753616     0.26489248     0.08040163

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 43  1   -196.4879583270  1.7691E-06  0.0000E+00  7.4818E-04  1.0000E-04   
 mr-sdci # 43  2   -196.4725822633  1.6506E-11  4.8612E-07  1.0428E-03  1.0000E-04   
 mr-sdci # 43  3   -196.4725782313  6.8498E-10  0.0000E+00  3.0184E-03  1.0000E-04   
 mr-sdci # 43  4   -196.4723652779  1.8340E-06  0.0000E+00  2.2265E-02  1.0000E-04   
 mr-sdci # 43  5   -196.4644790515  4.0657E-11  0.0000E+00  2.3588E-03  1.0000E-04   
 mr-sdci # 43  6   -196.4644781161  3.1888E-09  0.0000E+00  2.4069E-03  1.0000E-04   
 mr-sdci # 43  7   -195.7870626405  5.4648E-02  0.0000E+00  3.8094E-01  1.0000E-04   
 mr-sdci # 43  8   -195.7218467667  1.4143E-03  0.0000E+00  2.8545E-01  1.0000E-04   
 mr-sdci # 43  9   -195.1613823980  4.1565E-01  0.0000E+00  1.1642E+00  1.0000E-04   
 mr-sdci # 43 10   -194.7144659483  3.6607E-01  0.0000E+00  2.1437E+00  1.0000E-04   
 mr-sdci # 43 11   -194.3454603185  1.2133E+00  0.0000E+00  2.1177E+00  1.0000E-04   
 mr-sdci # 43 12   -193.1297764027  1.3042E-01  0.0000E+00  3.9461E+00  1.0000E-04   
 mr-sdci # 43 13   -192.7827056702  4.5711E-02  0.0000E+00  3.4902E+00  1.0000E-04   
 mr-sdci # 43 14   -190.7513078672  1.5595E-01  0.0000E+00  4.7389E+00  1.0000E-04   
 mr-sdci # 43 15   -190.5890640610 -9.5628E-01  0.0000E+00  5.2520E+00  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.008000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  44

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59963846
   ht   2     0.00000000   -50.58426624
   ht   3     0.00000000     0.00000000   -50.58425590
   ht   4     0.00000000     0.00000000     0.00000000   -50.58338175
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57615925
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57614261
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.81416426
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.80038872
   ht   9    -0.07004457    -0.00823692     0.00152686    -0.00542086     0.00499060    -0.01515855    -0.00160834    -0.00137124
   ht  10    -0.00166477    -0.00314938    -0.00352392     0.00593660    -0.00768446    -0.00242084    -0.00060718    -0.00050063
   ht  11    -0.00080478     0.00822072    -0.09787040     0.02267157    -0.00177439    -0.00209832    -0.00133726     0.00178158
   ht  12    -0.02833286    -0.04682200     0.35524729     0.95996908     0.62626565     0.40944471    -0.01516700     0.01791890
   ht  13     0.00636938     0.01081193     0.01621932    -0.00275533    -0.04165181    -0.02039140     0.00280569     0.00031696
   ht  14     0.01719868    -0.01572972     0.00644134    -0.11893496     0.01025055    -0.01257805     0.00035297    -0.00127087
   ht  15    -0.04438927    -0.00399582     0.00109018    -0.00050367     0.00107927    -0.00590657     0.00021479    -0.00248742
   ht  16     0.00073847     0.01216959     0.00355777    -0.00609816     0.00697150    -0.00091716     0.00163134     0.00046797

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15         ht  16
   ht   9    -0.00020291
   ht  10    -0.00000069    -0.00004356
   ht  11     0.00000452    -0.00000547    -0.00039146
   ht  12     0.00007830     0.00000018     0.00016881    -0.05223716
   ht  13     0.00000521    -0.00000319     0.00002273     0.00046605    -0.00016548
   ht  14    -0.00000032     0.00001687     0.00006034     0.00158886    -0.00001103    -0.00058061
   ht  15    -0.00005374    -0.00000154    -0.00000031    -0.00000598     0.00000565     0.00001256    -0.00009636
   ht  16     0.00000173     0.00000054     0.00000539    -0.00002199    -0.00000058    -0.00000810     0.00000140    -0.00001861

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -2.842908E-06  -0.968729       3.352879E-02  -1.448884E-02  -4.123559E-05  -1.053802E-05  -1.419547E-02  -0.106458    
 ref    2  -1.511041E-07  -2.592607E-02  -0.750667      -3.472044E-03  -0.441245       0.425816       1.525250E-03   2.115747E-02
 ref    3   1.584342E-05   6.475925E-03   1.946302E-03  -0.434720       0.601344       0.623878       8.327795E-03  -4.151586E-02
 ref    4   0.954947       1.859211E-07  -7.795696E-06   5.791483E-05   4.045583E-06  -6.991933E-06  -6.979669E-02   6.583013E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   0.109313       6.991367E-03  -1.316015E-02   3.953777E-02   5.924390E-02   2.782012E-02  -9.930657E-03  -7.153099E-03
 ref    2  -6.802237E-03   6.365884E-03   8.927904E-03  -4.925052E-02   3.901978E-02   2.745367E-02  -1.596482E-02   4.788777E-02
 ref    3  -2.870535E-03  -2.130200E-02   3.946952E-02   3.877818E-03  -2.478036E-02  -5.914181E-03  -2.253062E-02  -3.606817E-02
 ref    4   1.864213E-03  -1.604203E-02   8.374700E-03  -4.314937E-03   8.241298E-03  -1.642337E-03   0.269569       6.317796E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.911924       0.939151       0.564629       0.189203       0.556312       0.570543       5.144767E-03   1.354790E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   1.200722E-02   8.005258E-04   1.880875E-03   4.022505E-03   5.714368E-03   1.565338E-03   7.352840E-02   7.636773E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15        ev   16

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00000284    -0.96872942     0.03352879    -0.01448884    -0.00004124    -0.00001054    -0.01419547    -0.10645824
 ref:   2    -0.00000015    -0.02592607    -0.75066685    -0.00347204    -0.44124547     0.42581626     0.00152525     0.02115747
 ref:   3     0.00001584     0.00647593     0.00194630    -0.43471995     0.60134363     0.62387819     0.00832779    -0.04151586
 ref:   4     0.95494695     0.00000019    -0.00000780     0.00005791     0.00000405    -0.00000699    -0.06979669     0.00658301

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1     0.10931257     0.00699137    -0.01316015     0.03953777     0.05924390     0.02782012    -0.00993066    -0.00715310
 ref:   2    -0.00680224     0.00636588     0.00892790    -0.04925052     0.03901978     0.02745367    -0.01596482     0.04788777
 ref:   3    -0.00287054    -0.02130200     0.03946952     0.00387782    -0.02478036    -0.00591418    -0.02253062    -0.03606817
 ref:   4     0.00186421    -0.01604203     0.00837470    -0.00431494     0.00824130    -0.00164234     0.26956868     0.06317796

 trial vector basis is being transformed.  new dimension:   8

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 44  1   -196.4879583272  1.8715E-10  0.0000E+00  7.4670E-04  1.0000E-04   
 mr-sdci # 44  2   -196.4725827393  4.7598E-07  0.0000E+00  6.2208E-04  1.0000E-04   
 mr-sdci # 44  3   -196.4725782360  4.7206E-09  3.5549E-06  3.0035E-03  1.0000E-04   
 mr-sdci # 44  4   -196.4723656465  3.6858E-07  0.0000E+00  2.2269E-02  1.0000E-04   
 mr-sdci # 44  5   -196.4644790546  3.0958E-09  0.0000E+00  2.3583E-03  1.0000E-04   
 mr-sdci # 44  6   -196.4644781364  2.0365E-08  0.0000E+00  2.3810E-03  1.0000E-04   
 mr-sdci # 44  7   -195.7872618281  1.9919E-04  0.0000E+00  3.8209E-01  1.0000E-04   
 mr-sdci # 44  8   -195.7461267716  2.4280E-02  0.0000E+00  3.9931E-01  1.0000E-04   
 mr-sdci # 44  9   -195.4499217978  2.8854E-01  0.0000E+00  9.5973E-01  1.0000E-04   
 mr-sdci # 44 10   -195.1594301811  4.4496E-01  0.0000E+00  1.1750E+00  1.0000E-04   
 mr-sdci # 44 11   -194.7137450844  3.6828E-01  0.0000E+00  2.1374E+00  1.0000E-04   
 mr-sdci # 44 12   -193.1534768233  2.3700E-02  0.0000E+00  3.7822E+00  1.0000E-04   
 mr-sdci # 44 13   -192.9431979915  1.6049E-01  0.0000E+00  2.8703E+00  1.0000E-04   
 mr-sdci # 44 14   -192.4885595411  1.7373E+00  0.0000E+00  3.2755E+00  1.0000E-04   
 mr-sdci # 44 15   -190.7495361452  1.6047E-01  0.0000E+00  4.7252E+00  1.0000E-04   
 mr-sdci # 44 16   -190.4836853046  1.3176E+00  0.0000E+00  5.3520E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  45

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59964353
   ht   2     0.00000000   -50.58426794
   ht   3     0.00000000     0.00000000   -50.58426344
   ht   4     0.00000000     0.00000000     0.00000000   -50.58405085
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616426
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616334
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.89894703
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.85781198
   ht   9     0.00014024     0.00406798     0.06446562    -0.01594323    -0.00561148     0.00892877    -0.00057270    -0.00058610

                ht   9
   ht   9    -0.00018830

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -3.056069E-06   0.968738      -3.332794E-02   1.442610E-02   4.100535E-05  -1.117011E-05  -1.420639E-02   0.106404    
 ref    2  -3.422642E-07   2.574343E-02   0.750514       4.918100E-03   0.444538       0.422421       1.614382E-03  -2.192875E-02
 ref    3   1.597472E-05  -6.475116E-03  -2.757245E-03   0.434713      -0.596504       0.628505       8.307038E-03   4.164518E-02
 ref    4   0.954947      -1.870497E-07   5.951978E-06  -5.760567E-05  -4.060990E-06  -6.878533E-06  -6.979597E-02  -6.591089E-03

              v      9
 ref    1   9.202455E-03
 ref    2   0.128200    
 ref    3  -2.120913E-02
 ref    4  -5.174106E-04

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.911924       0.939157       0.564390       0.189208       0.553431       0.573458       5.144912E-03   1.358040E-02

              v      9
 ref    1   1.697002E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00000306     0.96873757    -0.03332794     0.01442610     0.00004101    -0.00001117    -0.01420639     0.10640377
 ref:   2    -0.00000034     0.02574343     0.75051425     0.00491810     0.44453786     0.42242090     0.00161438    -0.02192875
 ref:   3     0.00001597    -0.00647512    -0.00275724     0.43471328    -0.59650398     0.62850513     0.00830704     0.04164518
 ref:   4     0.95494696    -0.00000019     0.00000595    -0.00005761    -0.00000406    -0.00000688    -0.06979597    -0.00659109

                ci   9
 ref:   1     0.00920245
 ref:   2     0.12820000
 ref:   3    -0.02120913
 ref:   4    -0.00051741

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 45  1   -196.4879583279  6.3778E-10  0.0000E+00  7.4875E-04  1.0000E-04   
 mr-sdci # 45  2   -196.4725827393  1.9185E-13  0.0000E+00  6.2216E-04  1.0000E-04   
 mr-sdci # 45  3   -196.4725808620  2.6260E-06  0.0000E+00  2.2337E-03  1.0000E-04   
 mr-sdci # 45  4   -196.4723657090  6.2578E-08  2.0008E-04  2.2380E-02  1.0000E-04   
 mr-sdci # 45  5   -196.4644790579  3.2837E-09  0.0000E+00  2.3660E-03  1.0000E-04   
 mr-sdci # 45  6   -196.4644781516  1.5149E-08  0.0000E+00  2.3683E-03  1.0000E-04   
 mr-sdci # 45  7   -195.7872625539  7.2581E-07  0.0000E+00  3.8200E-01  1.0000E-04   
 mr-sdci # 45  8   -195.7461842125  5.7441E-05  0.0000E+00  4.0167E-01  1.0000E-04   
 mr-sdci # 45  9   -194.1172068833 -1.3327E+00  0.0000E+00  3.4362E+00  1.0000E-04   
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  46

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59964353
   ht   2     0.00000000   -50.58426794
   ht   3     0.00000000     0.00000000   -50.58426344
   ht   4     0.00000000     0.00000000     0.00000000   -50.58405085
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616426
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616334
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.89894703
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.85781198
   ht   9     0.00014024     0.00406798     0.06446562    -0.01594323    -0.00561148     0.00892877    -0.00057270    -0.00058610
   ht  10     0.04902647     0.00256553    -0.21863237    -0.32880057    -0.00570914     0.02475615     0.00216325    -0.00801319

                ht   9         ht  10
   ht   9    -0.00018830
   ht  10     0.00015881    -0.00846789

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -3.474836E-06   0.968744      -3.330995E-02  -1.398784E-02   1.869967E-05   1.434100E-05  -1.317219E-02  -0.106584    
 ref    2   4.999716E-07   2.569869E-02   0.750497      -7.361893E-03   0.463464      -0.401527       2.257975E-03   2.128314E-02
 ref    3  -2.750752E-06  -6.347785E-03  -4.048704E-03  -0.433757      -0.567516      -0.655242       9.472119E-03  -4.218006E-02
 ref    4   0.954948      -2.349566E-07   6.431939E-06   1.221094E-04  -1.009087E-05   7.025483E-06  -6.954818E-02   5.720535E-03

              v      9       v     10
 ref    1   8.823552E-03  -4.784450E-03
 ref    2   0.122534      -6.777589E-02
 ref    3  -2.499134E-02  -4.016212E-02
 ref    4  -2.395181E-03  -2.029914E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.911925       0.939165       0.564371       0.188395       0.536873       0.590565       5.105276E-03   1.362492E-02

              v      9       v     10
 ref    1   1.572276E-02   6.641513E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00000347     0.96874379    -0.03330995    -0.01398784     0.00001870     0.00001434    -0.01317219    -0.10658361
 ref:   2     0.00000050     0.02569869     0.75049687    -0.00736189     0.46346352    -0.40152692     0.00225798     0.02128314
 ref:   3    -0.00000275    -0.00634778    -0.00404870    -0.43375657    -0.56751644    -0.65524155     0.00947212    -0.04218006
 ref:   4     0.95494762    -0.00000023     0.00000643     0.00012211    -0.00001009     0.00000703    -0.06954818     0.00572053

                ci   9         ci  10
 ref:   1     0.00882355    -0.00478445
 ref:   2     0.12253410    -0.06777589
 ref:   3    -0.02499134    -0.04016212
 ref:   4    -0.00239518    -0.02029914

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 46  1   -196.4879583324  4.5329E-09  0.0000E+00  7.3233E-04  1.0000E-04   
 mr-sdci # 46  2   -196.4725827393  1.1632E-11  0.0000E+00  6.2210E-04  1.0000E-04   
 mr-sdci # 46  3   -196.4725808632  1.1651E-09  0.0000E+00  2.2202E-03  1.0000E-04   
 mr-sdci # 46  4   -196.4724993751  1.3367E-04  0.0000E+00  1.3846E-02  1.0000E-04   
 mr-sdci # 46  5   -196.4644797542  6.9632E-07  1.0948E-06  1.8043E-03  1.0000E-04   
 mr-sdci # 46  6   -196.4644781560  4.4004E-09  0.0000E+00  2.3276E-03  1.0000E-04   
 mr-sdci # 46  7   -195.7877373781  4.7482E-04  0.0000E+00  3.7983E-01  1.0000E-04   
 mr-sdci # 46  8   -195.7464760870  2.9187E-04  0.0000E+00  4.0201E-01  1.0000E-04   
 mr-sdci # 46  9   -194.1200924087  2.8855E-03  0.0000E+00  3.3564E+00  1.0000E-04   
 mr-sdci # 46 10   -193.7714352332 -1.3880E+00  0.0000E+00  2.8727E+00  1.0000E-04   
 
 root number  5 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  47

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59964353
   ht   2     0.00000000   -50.58426794
   ht   3     0.00000000     0.00000000   -50.58426344
   ht   4     0.00000000     0.00000000     0.00000000   -50.58405085
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616426
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616334
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.89894703
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.85781198
   ht   9     0.00014024     0.00406798     0.06446562    -0.01594323    -0.00561148     0.00892877    -0.00057270    -0.00058610
   ht  10     0.04902647     0.00256553    -0.21863237    -0.32880057    -0.00570914     0.02475615     0.00216325    -0.00801319
   ht  11    -0.00085649     0.00698551     0.00408357     0.01028380    -0.02103679     0.02284499    -0.00052904     0.00195291

                ht   9         ht  10         ht  11
   ht   9    -0.00018830
   ht  10     0.00015881    -0.00846789
   ht  11    -0.00000669     0.00004365    -0.00004973

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -3.530955E-06   0.968732      -3.340885E-02  -1.455049E-02   2.059525E-05  -1.445456E-05  -1.286217E-02   0.106587    
 ref    2   1.002712E-06   2.576831E-02   0.750493      -7.555356E-03   0.466445       0.397995       1.884063E-03  -2.344202E-02
 ref    3  -3.343476E-06  -6.598940E-03  -4.154160E-03  -0.433618      -0.562632       0.659531       9.596321E-03   4.214457E-02
 ref    4   0.954948      -2.091398E-07   6.443052E-06   1.174503E-04  -8.432586E-06  -6.924730E-06  -6.956035E-02  -5.487552E-03

              v      9       v     10       v     11
 ref    1  -5.075563E-03   8.404812E-03   3.760459E-03
 ref    2  -0.106691       0.114374       4.272470E-03
 ref    3  -1.643335E-02  -2.739313E-02   3.762525E-02
 ref    4  -7.693753E-03  -3.580023E-03   2.020728E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.911925       0.939150       0.564372       0.188294       0.534125       0.593381       5.099717E-03   1.371654E-02

              v      9       v     10       v     11
 ref    1   1.173799E-02   1.391528E-02   1.856389E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00000353     0.96873214    -0.03340885    -0.01455049     0.00002060    -0.00001445    -0.01286217     0.10658673
 ref:   2     0.00000100     0.02576831     0.75049253    -0.00755536     0.46644461     0.39799490     0.00188406    -0.02344202
 ref:   3    -0.00000334    -0.00659894    -0.00415416    -0.43361847    -0.56263197     0.65953103     0.00959632     0.04214457
 ref:   4     0.95494760    -0.00000021     0.00000644     0.00011745    -0.00000843    -0.00000692    -0.06956035    -0.00548755

                ci   9         ci  10         ci  11
 ref:   1    -0.00507556     0.00840481     0.00376046
 ref:   2    -0.10669106     0.11437413     0.00427247
 ref:   3    -0.01643335    -0.02739313     0.03762525
 ref:   4    -0.00769375    -0.00358002     0.02020728

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 47  1   -196.4879583326  1.7589E-10  0.0000E+00  7.3633E-04  1.0000E-04   
 mr-sdci # 47  2   -196.4725827397  4.4696E-10  0.0000E+00  6.1731E-04  1.0000E-04   
 mr-sdci # 47  3   -196.4725808632  7.6291E-11  0.0000E+00  2.2213E-03  1.0000E-04   
 mr-sdci # 47  4   -196.4725043145  4.9394E-06  0.0000E+00  1.4545E-02  1.0000E-04   
 mr-sdci # 47  5   -196.4644806225  8.6831E-07  0.0000E+00  1.2254E-03  1.0000E-04   
 mr-sdci # 47  6   -196.4644781562  2.6183E-10  1.9949E-06  2.3274E-03  1.0000E-04   
 mr-sdci # 47  7   -195.7877549581  1.7580E-05  0.0000E+00  3.7940E-01  1.0000E-04   
 mr-sdci # 47  8   -195.7473049835  8.2890E-04  0.0000E+00  3.9315E-01  1.0000E-04   
 mr-sdci # 47  9   -194.4805871944  3.6049E-01  0.0000E+00  2.8547E+00  1.0000E-04   
 mr-sdci # 47 10   -194.1185724462  3.4714E-01  0.0000E+00  3.3070E+00  1.0000E-04   
 mr-sdci # 47 11   -193.1340917157 -1.5797E+00  0.0000E+00  3.4437E+00  1.0000E-04   
 
 root number  6 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  48

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59964353
   ht   2     0.00000000   -50.58426794
   ht   3     0.00000000     0.00000000   -50.58426344
   ht   4     0.00000000     0.00000000     0.00000000   -50.58405085
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616426
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616334
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.89894703
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.85781198
   ht   9     0.00014024     0.00406798     0.06446562    -0.01594323    -0.00561148     0.00892877    -0.00057270    -0.00058610
   ht  10     0.04902647     0.00256553    -0.21863237    -0.32880057    -0.00570914     0.02475615     0.00216325    -0.00801319
   ht  11    -0.00085649     0.00698551     0.00408357     0.01028380    -0.02103679     0.02284499    -0.00052904     0.00195291
   ht  12    -0.00434142     0.01979277    -0.00544270    -0.02078796    -0.02485353     0.00490416     0.00065352     0.00390102

                ht   9         ht  10         ht  11         ht  12
   ht   9    -0.00018830
   ht  10     0.00015881    -0.00846789
   ht  11    -0.00000669     0.00004365    -0.00004973
   ht  12    -0.00000324    -0.00014280    -0.00001452    -0.00008267

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -3.017874E-06   0.968731      -3.342966E-02   1.448950E-02   1.518917E-05  -3.379095E-05  -1.392826E-02  -0.107342    
 ref    2   6.139155E-07   2.579414E-02   0.750497       6.819393E-03   0.419892      -0.446793       2.550143E-03   2.460290E-02
 ref    3  -3.608771E-06  -6.582725E-03  -3.757182E-03   0.433425      -0.631718      -0.593833       9.374627E-03  -4.208133E-02
 ref    4   0.954948      -2.386101E-07   5.737674E-06  -1.442071E-04  -1.071499E-05  -1.286372E-05  -6.973653E-02   5.449307E-03

              v      9       v     10       v     11       v     12
 ref    1   2.927994E-02  -1.841119E-02  -1.258643E-02   8.698446E-03
 ref    2   1.866431E-02   0.132746      -9.994826E-02  -2.786152E-02
 ref    3   9.482050E-03   9.322436E-03   2.772061E-02  -4.148969E-02
 ref    4   2.236676E-02  -1.046291E-02   7.218991E-04  -1.349896E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.911925       0.939149       0.564378       0.188114       0.575377       0.552262       5.151567E-03   1.392810E-02

              v      9       v     10       v     11       v     12
 ref    1   1.795853E-03   1.815698E-02   1.091703E-02   2.755543E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00000302     0.96873150    -0.03342966     0.01448950     0.00001519    -0.00003379    -0.01392826    -0.10734179
 ref:   2     0.00000061     0.02579414     0.75049743     0.00681939     0.41989191    -0.44679307     0.00255014     0.02460290
 ref:   3    -0.00000361    -0.00658273    -0.00375718     0.43342514    -0.63171805    -0.59383325     0.00937463    -0.04208133
 ref:   4     0.95494788    -0.00000024     0.00000574    -0.00014421    -0.00001071    -0.00001286    -0.06973653     0.00544931

                ci   9         ci  10         ci  11         ci  12
 ref:   1     0.02927994    -0.01841119    -0.01258643     0.00869845
 ref:   2     0.01866431     0.13274649    -0.09994826    -0.02786152
 ref:   3     0.00948205     0.00932244     0.02772061    -0.04148969
 ref:   4     0.02236676    -0.01046291     0.00072190    -0.01349896

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 48  1   -196.4879583329  3.1357E-10  1.4440E-07  7.3207E-04  1.0000E-04   
 mr-sdci # 48  2   -196.4725827397  2.6574E-12  0.0000E+00  6.1679E-04  1.0000E-04   
 mr-sdci # 48  3   -196.4725808647  1.5147E-09  0.0000E+00  2.2286E-03  1.0000E-04   
 mr-sdci # 48  4   -196.4725074877  3.1732E-06  0.0000E+00  1.3509E-02  1.0000E-04   
 mr-sdci # 48  5   -196.4644806331  1.0539E-08  0.0000E+00  1.1839E-03  1.0000E-04   
 mr-sdci # 48  6   -196.4644799857  1.8294E-06  0.0000E+00  1.7927E-03  1.0000E-04   
 mr-sdci # 48  7   -195.7878703207  1.1536E-04  0.0000E+00  3.7971E-01  1.0000E-04   
 mr-sdci # 48  8   -195.7479132826  6.0830E-04  0.0000E+00  3.8703E-01  1.0000E-04   
 mr-sdci # 48  9   -194.6649613109  1.8437E-01  0.0000E+00  2.3001E+00  1.0000E-04   
 mr-sdci # 48 10   -194.3392983791  2.2073E-01  0.0000E+00  3.3144E+00  1.0000E-04   
 mr-sdci # 48 11   -194.1131164717  9.7902E-01  0.0000E+00  3.3774E+00  1.0000E-04   
 mr-sdci # 48 12   -192.9707675666 -1.8271E-01  0.0000E+00  3.4226E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.014000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  49

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59964353
   ht   2     0.00000000   -50.58426794
   ht   3     0.00000000     0.00000000   -50.58426344
   ht   4     0.00000000     0.00000000     0.00000000   -50.58405085
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616426
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616334
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.89894703
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.85781198
   ht   9     0.00014024     0.00406798     0.06446562    -0.01594323    -0.00561148     0.00892877    -0.00057270    -0.00058610
   ht  10     0.04902647     0.00256553    -0.21863237    -0.32880057    -0.00570914     0.02475615     0.00216325    -0.00801319
   ht  11    -0.00085649     0.00698551     0.00408357     0.01028380    -0.02103679     0.02284499    -0.00052904     0.00195291
   ht  12    -0.00434142     0.01979277    -0.00544270    -0.02078796    -0.02485353     0.00490416     0.00065352     0.00390102
   ht  13    -0.00032575     0.00137581     0.00004336    -0.00001448    -0.00320966    -0.00166923    -0.00084948     0.00036459

                ht   9         ht  10         ht  11         ht  12         ht  13
   ht   9    -0.00018830
   ht  10     0.00015881    -0.00846789
   ht  11    -0.00000669     0.00004365    -0.00004973
   ht  12    -0.00000324    -0.00014280    -0.00001452    -0.00008267
   ht  13     0.00000005    -0.00000092    -0.00000012    -0.00000182    -0.00000366

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -3.642580E-07   0.968731      -3.348831E-02   1.441248E-02  -1.522764E-05   3.348383E-05   1.207611E-02   0.107428    
 ref    2   1.878852E-08   2.584078E-02   0.750496       6.769152E-03  -0.421555       0.445224      -2.770154E-03  -2.461200E-02
 ref    3   7.686452E-06  -6.546247E-03  -3.730661E-03   0.433432       0.629499       0.596186      -1.200760E-02   4.205501E-02
 ref    4   0.954974       2.977139E-07   6.109347E-06  -1.232918E-04   1.214601E-05   8.670773E-06   5.532990E-02  -4.927015E-03

              v      9       v     10       v     11       v     12       v     13
 ref    1   2.181426E-02   1.631164E-02  -2.083184E-02  -2.065162E-02   5.015898E-03
 ref    2  -7.724379E-03   5.987158E-02   0.143100      -6.403012E-02  -2.365304E-02
 ref    3  -2.721436E-03   2.097755E-02  -7.562111E-03   2.511555E-02  -4.442644E-02
 ref    4  -3.746213E-02   8.404110E-02  -6.752189E-02  -3.165766E-02  -3.650140E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.911976       0.939150       0.564380       0.188117       0.573978       0.553662       3.359087E-03   1.393939E-02

              v      9       v     10       v     11       v     12       v     13
 ref    1   1.946345E-03   1.135364E-02   2.552784E-02   6.159344E-03   3.890686E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00000036     0.96873073    -0.03348831     0.01441248    -0.00001523     0.00003348     0.01207611     0.10742785
 ref:   2     0.00000002     0.02584078     0.75049620     0.00676915    -0.42155514     0.44522428    -0.00277015    -0.02461200
 ref:   3     0.00000769    -0.00654625    -0.00373066     0.43343198     0.62949893     0.59618567    -0.01200760     0.04205501
 ref:   4     0.95497436     0.00000030     0.00000611    -0.00012329     0.00001215     0.00000867     0.05532990    -0.00492701

                ci   9         ci  10         ci  11         ci  12         ci  13
 ref:   1     0.02181426     0.01631164    -0.02083184    -0.02065162     0.00501590
 ref:   2    -0.00772438     0.05987158     0.14309958    -0.06403012    -0.02365304
 ref:   3    -0.00272144     0.02097755    -0.00756211     0.02511555    -0.04442644
 ref:   4    -0.03746213     0.08404110    -0.06752189    -0.03165766    -0.03650140

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 49  1   -196.4879584891  1.5626E-07  0.0000E+00  3.1502E-04  1.0000E-04   
 mr-sdci # 49  2   -196.4725827399  1.6624E-10  7.7804E-08  6.1318E-04  1.0000E-04   
 mr-sdci # 49  3   -196.4725808648  7.9972E-11  0.0000E+00  2.2286E-03  1.0000E-04   
 mr-sdci # 49  4   -196.4725077292  2.4147E-07  0.0000E+00  1.3576E-02  1.0000E-04   
 mr-sdci # 49  5   -196.4644806339  8.3470E-10  0.0000E+00  1.1839E-03  1.0000E-04   
 mr-sdci # 49  6   -196.4644799926  6.9181E-09  0.0000E+00  1.7716E-03  1.0000E-04   
 mr-sdci # 49  7   -195.8093553567  2.1485E-02  0.0000E+00  2.8261E-01  1.0000E-04   
 mr-sdci # 49  8   -195.7479165777  3.2951E-06  0.0000E+00  3.8747E-01  1.0000E-04   
 mr-sdci # 49  9   -194.7035379179  3.8577E-02  0.0000E+00  2.1593E+00  1.0000E-04   
 mr-sdci # 49 10   -194.5893535842  2.5006E-01  0.0000E+00  2.3382E+00  1.0000E-04   
 mr-sdci # 49 11   -194.2192837858  1.0617E-01  0.0000E+00  3.3014E+00  1.0000E-04   
 mr-sdci # 49 12   -194.0945786483  1.1238E+00  0.0000E+00  3.4111E+00  1.0000E-04   
 mr-sdci # 49 13   -192.9115231564 -3.1675E-02  0.0000E+00  3.4731E+00  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.008000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  50

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59964353
   ht   2     0.00000000   -50.58426794
   ht   3     0.00000000     0.00000000   -50.58426344
   ht   4     0.00000000     0.00000000     0.00000000   -50.58405085
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616426
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616334
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.89894703
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.85781198
   ht   9     0.00014024     0.00406798     0.06446562    -0.01594323    -0.00561148     0.00892877    -0.00057270    -0.00058610
   ht  10     0.04902647     0.00256553    -0.21863237    -0.32880057    -0.00570914     0.02475615     0.00216325    -0.00801319
   ht  11    -0.00085649     0.00698551     0.00408357     0.01028380    -0.02103679     0.02284499    -0.00052904     0.00195291
   ht  12    -0.00434142     0.01979277    -0.00544270    -0.02078796    -0.02485353     0.00490416     0.00065352     0.00390102
   ht  13    -0.00032575     0.00137581     0.00004336    -0.00001448    -0.00320966    -0.00166923    -0.00084948     0.00036459
   ht  14    -0.00005251    -0.01037170     0.00016270    -0.00095372    -0.00467048    -0.00144507    -0.00041711    -0.00162625

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14
   ht   9    -0.00018830
   ht  10     0.00015881    -0.00846789
   ht  11    -0.00000669     0.00004365    -0.00004973
   ht  12    -0.00000324    -0.00014280    -0.00001452    -0.00008267
   ht  13     0.00000005    -0.00000092    -0.00000012    -0.00000182    -0.00000366
   ht  14    -0.00000012    -0.00001164    -0.00000014     0.00000084     0.00000011    -0.00000387

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -3.336625E-07   0.968648       3.520117E-02   1.481355E-02   1.527064E-05   2.463068E-05   1.193667E-02  -0.114791    
 ref    2  -1.851780E-07   2.716116E-02  -0.750449       6.804670E-03   0.421527       0.445249      -2.758955E-03   2.094223E-02
 ref    3  -7.580226E-06  -6.724311E-03   3.731978E-03   0.433427      -0.629536       0.596148      -1.204170E-02  -3.972275E-02
 ref    4  -0.954974       2.598336E-08  -6.088230E-06  -1.230684E-04  -1.214572E-05   8.507523E-06   5.533156E-02   3.956913E-03

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1   9.304543E-03  -2.587743E-02  -1.384565E-02  -1.504152E-02   5.954450E-03  -0.130311    
 ref    2  -2.761709E-02  -6.375364E-02   0.136262      -7.428253E-02   2.315158E-02  -1.040674E-02
 ref    3  -1.542689E-03  -1.680522E-02  -1.055865E-02   2.349587E-02   3.957694E-02   5.467100E-02
 ref    4  -4.861346E-02  -7.145437E-02  -7.551976E-02  -2.893724E-02   3.571668E-02   1.012418E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.911976       0.939063       0.564427       0.188124       0.574001       0.553639       3.356680E-03   1.520917E-02

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1   3.214927E-03   1.012231E-02   2.457386E-02   7.133561E-03   3.413467E-03   2.018076E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00000033     0.96864836     0.03520117     0.01481355     0.00001527     0.00002463     0.01193667    -0.11479127
 ref:   2    -0.00000019     0.02716116    -0.75044898     0.00680467     0.42152720     0.44524903    -0.00275895     0.02094223
 ref:   3    -0.00000758    -0.00672431     0.00373198     0.43342668    -0.62953636     0.59614782    -0.01204170    -0.03972275
 ref:   4    -0.95497439     0.00000003    -0.00000609    -0.00012307    -0.00001215     0.00000851     0.05533156     0.00395691

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14
 ref:   1     0.00930454    -0.02587743    -0.01384565    -0.01504152     0.00595445    -0.13031132
 ref:   2    -0.02761709    -0.06375364     0.13626239    -0.07428253     0.02315158    -0.01040674
 ref:   3    -0.00154269    -0.01680522    -0.01055865     0.02349587     0.03957694     0.05467100
 ref:   4    -0.04861346    -0.07145437    -0.07551976    -0.02893724     0.03571668     0.01012418

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 50  1   -196.4879584893  1.3284E-10  0.0000E+00  3.1233E-04  1.0000E-04   
 mr-sdci # 50  2   -196.4725827851  4.5254E-08  0.0000E+00  1.6060E-04  1.0000E-04   
 mr-sdci # 50  3   -196.4725808651  2.4828E-10  1.5185E-06  2.2302E-03  1.0000E-04   
 mr-sdci # 50  4   -196.4725077495  2.0320E-08  0.0000E+00  1.3539E-02  1.0000E-04   
 mr-sdci # 50  5   -196.4644806339  1.4921E-13  0.0000E+00  1.1839E-03  1.0000E-04   
 mr-sdci # 50  6   -196.4644800005  7.8995E-09  0.0000E+00  1.7682E-03  1.0000E-04   
 mr-sdci # 50  7   -195.8093555410  1.8422E-07  0.0000E+00  2.8263E-01  1.0000E-04   
 mr-sdci # 50  8   -195.7639478736  1.6031E-02  0.0000E+00  1.9655E-01  1.0000E-04   
 mr-sdci # 50  9   -194.7341126850  3.0575E-02  0.0000E+00  2.0953E+00  1.0000E-04   
 mr-sdci # 50 10   -194.6073073564  1.7954E-02  0.0000E+00  2.3629E+00  1.0000E-04   
 mr-sdci # 50 11   -194.2372125048  1.7929E-02  0.0000E+00  3.2597E+00  1.0000E-04   
 mr-sdci # 50 12   -194.0992238735  4.6452E-03  0.0000E+00  3.3664E+00  1.0000E-04   
 mr-sdci # 50 13   -192.9261168767  1.4594E-02  0.0000E+00  3.4893E+00  1.0000E-04   
 mr-sdci # 50 14   -190.9081580834 -1.5804E+00  0.0000E+00  4.9620E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.007000
time for eigenvalue solver             0.001000
time for vector access                 0.000000

          starting ci iteration  51

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59964353
   ht   2     0.00000000   -50.58426794
   ht   3     0.00000000     0.00000000   -50.58426344
   ht   4     0.00000000     0.00000000     0.00000000   -50.58405085
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616426
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616334
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.89894703
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.85781198
   ht   9     0.00014024     0.00406798     0.06446562    -0.01594323    -0.00561148     0.00892877    -0.00057270    -0.00058610
   ht  10     0.04902647     0.00256553    -0.21863237    -0.32880057    -0.00570914     0.02475615     0.00216325    -0.00801319
   ht  11    -0.00085649     0.00698551     0.00408357     0.01028380    -0.02103679     0.02284499    -0.00052904     0.00195291
   ht  12    -0.00434142     0.01979277    -0.00544270    -0.02078796    -0.02485353     0.00490416     0.00065352     0.00390102
   ht  13    -0.00032575     0.00137581     0.00004336    -0.00001448    -0.00320966    -0.00166923    -0.00084948     0.00036459
   ht  14    -0.00005251    -0.01037170     0.00016270    -0.00095372    -0.00467048    -0.00144507    -0.00041711    -0.00162625
   ht  15    -0.00075169    -0.00232608    -0.03410591     0.01836706     0.00226123    -0.00616258     0.00037811    -0.00009258

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15
   ht   9    -0.00018830
   ht  10     0.00015881    -0.00846789
   ht  11    -0.00000669     0.00004365    -0.00004973
   ht  12    -0.00000324    -0.00014280    -0.00001452    -0.00008267
   ht  13     0.00000005    -0.00000092    -0.00000012    -0.00000182    -0.00000366
   ht  14    -0.00000012    -0.00001164    -0.00000014     0.00000084     0.00000011    -0.00000387
   ht  15     0.00003249     0.00003568     0.00000226     0.00000768    -0.00000013     0.00000029    -0.00006259

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -2.454677E-07   0.969264      -6.105899E-03  -1.508858E-02  -1.514719E-05  -2.447893E-05   1.180884E-02   0.113546    
 ref    2  -4.849227E-07  -4.756180E-03  -0.750887      -1.148011E-03  -0.418405      -0.448206      -7.801789E-04  -2.754659E-02
 ref    3  -7.358100E-06  -6.701400E-03   8.114951E-04  -0.433428       0.633683      -0.591747      -1.172840E-02   3.807933E-02
 ref    4  -0.954974       4.479126E-08   1.815455E-07   1.246982E-04   1.231032E-05  -8.979883E-06   5.535894E-02  -3.735935E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1   1.866448E-02  -6.173817E-03   2.543247E-02  -1.836403E-02  -2.431207E-03  -0.128798      -2.476151E-02
 ref    2   6.581949E-02   5.042547E-02   4.465050E-02   9.411026E-02   1.576968E-02  -3.300451E-04  -0.132470    
 ref    3   1.726939E-02   1.424607E-03   1.824051E-02  -1.046235E-03  -4.944189E-02   5.157480E-02   4.949321E-02
 ref    4  -9.504409E-04   5.243794E-02   6.924201E-02  -8.075266E-02  -3.424536E-02   1.077453E-02  -5.689089E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.911976       0.939541       0.563870       0.188089       0.576616       0.551053       3.342225E-03   1.511541E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1   4.979704E-03   5.332611E-03   7.767650E-03   1.571606E-02   3.871839E-03   1.936502E-02   2.064342E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00000025     0.96926445    -0.00610590    -0.01508858    -0.00001515    -0.00002448     0.01180884     0.11354560
 ref:   2    -0.00000048    -0.00475618    -0.75088736    -0.00114801    -0.41840487    -0.44820570    -0.00078018    -0.02754659
 ref:   3    -0.00000736    -0.00670140     0.00081150    -0.43342762     0.63368254    -0.59174693    -0.01172840     0.03807933
 ref:   4    -0.95497435     0.00000004     0.00000018     0.00012470     0.00001231    -0.00000898     0.05535894    -0.00373594

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15
 ref:   1     0.01866448    -0.00617382     0.02543247    -0.01836403    -0.00243121    -0.12879776    -0.02476151
 ref:   2     0.06581949     0.05042547     0.04465050     0.09411026     0.01576968    -0.00033005    -0.13247016
 ref:   3     0.01726939     0.00142461     0.01824051    -0.00104623    -0.04944189     0.05157480     0.04949321
 ref:   4    -0.00095044     0.05243794     0.06924201    -0.08075266    -0.03424536     0.01077453    -0.00568909

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 51  1   -196.4879584895  1.8640E-10  0.0000E+00  3.0954E-04  1.0000E-04   
 mr-sdci # 51  2   -196.4725827853  1.4449E-10  0.0000E+00  1.6350E-04  1.0000E-04   
 mr-sdci # 51  3   -196.4725827085  1.8434E-06  0.0000E+00  7.3385E-04  1.0000E-04   
 mr-sdci # 51  4   -196.4725079168  1.6734E-07  6.1120E-05  1.3544E-02  1.0000E-04   
 mr-sdci # 51  5   -196.4644806347  8.0613E-10  0.0000E+00  1.1793E-03  1.0000E-04   
 mr-sdci # 51  6   -196.4644800239  2.3377E-08  0.0000E+00  1.7410E-03  1.0000E-04   
 mr-sdci # 51  7   -195.8094340576  7.8517E-05  0.0000E+00  2.7942E-01  1.0000E-04   
 mr-sdci # 51  8   -195.7643889015  4.4103E-04  0.0000E+00  2.0129E-01  1.0000E-04   
 mr-sdci # 51  9   -195.7215513873  9.8744E-01  0.0000E+00  5.3055E-01  1.0000E-04   
 mr-sdci # 51 10   -194.7225053666  1.1520E-01  0.0000E+00  2.0712E+00  1.0000E-04   
 mr-sdci # 51 11   -194.6006405819  3.6343E-01  0.0000E+00  2.3339E+00  1.0000E-04   
 mr-sdci # 51 12   -194.2138444476  1.1462E-01  0.0000E+00  3.3022E+00  1.0000E-04   
 mr-sdci # 51 13   -193.0165664432  9.0450E-02  0.0000E+00  3.1592E+00  1.0000E-04   
 mr-sdci # 51 14   -190.9129234620  4.7654E-03  0.0000E+00  4.9229E+00  1.0000E-04   
 mr-sdci # 51 15   -189.9036498924 -8.4589E-01  0.0000E+00  4.3130E+00  1.0000E-04   
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  52

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59964353
   ht   2     0.00000000   -50.58426794
   ht   3     0.00000000     0.00000000   -50.58426344
   ht   4     0.00000000     0.00000000     0.00000000   -50.58405085
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616426
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616334
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.89894703
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.85781198
   ht   9     0.00014024     0.00406798     0.06446562    -0.01594323    -0.00561148     0.00892877    -0.00057270    -0.00058610
   ht  10     0.04902647     0.00256553    -0.21863237    -0.32880057    -0.00570914     0.02475615     0.00216325    -0.00801319
   ht  11    -0.00085649     0.00698551     0.00408357     0.01028380    -0.02103679     0.02284499    -0.00052904     0.00195291
   ht  12    -0.00434142     0.01979277    -0.00544270    -0.02078796    -0.02485353     0.00490416     0.00065352     0.00390102
   ht  13    -0.00032575     0.00137581     0.00004336    -0.00001448    -0.00320966    -0.00166923    -0.00084948     0.00036459
   ht  14    -0.00005251    -0.01037170     0.00016270    -0.00095372    -0.00467048    -0.00144507    -0.00041711    -0.00162625
   ht  15    -0.00075169    -0.00232608    -0.03410591     0.01836706     0.00226123    -0.00616258     0.00037811    -0.00009258
   ht  16     0.06517752    -0.00682565    -0.11265752    -0.09869986     0.03546992     0.13003582    -0.00936459    -0.00035498

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15         ht  16
   ht   9    -0.00018830
   ht  10     0.00015881    -0.00846789
   ht  11    -0.00000669     0.00004365    -0.00004973
   ht  12    -0.00000324    -0.00014280    -0.00001452    -0.00008267
   ht  13     0.00000005    -0.00000092    -0.00000012    -0.00000182    -0.00000366
   ht  14    -0.00000012    -0.00001164    -0.00000014     0.00000084     0.00000011    -0.00000387
   ht  15     0.00003249     0.00003568     0.00000226     0.00000768    -0.00000013     0.00000029    -0.00006259
   ht  16     0.00011312    -0.00082657     0.00000453    -0.00003305     0.00001391     0.00000545    -0.00002230    -0.00234308

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   3.257477E-07  -0.968758       2.781582E-02  -2.167579E-02   6.144637E-06  -2.817632E-05   9.091864E-03   0.114805    
 ref    2   1.465618E-07   2.108549E-02   0.750320       2.077159E-02   0.393285      -0.470402      -1.590674E-03  -2.510993E-02
 ref    3   3.494877E-07   1.002957E-02   1.168701E-02  -0.433364      -0.665192      -0.556274      -1.024402E-02   3.595121E-02
 ref    4   0.954974       5.859262E-07   2.211158E-06  -6.335049E-05  -6.165180E-06  -5.777024E-06   5.619554E-02  -3.568838E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   1.589456E-02   3.676622E-03   2.958170E-03  -2.916519E-02   3.011926E-03  -2.189726E-02   0.127840      -2.032636E-02
 ref    2   6.706005E-02   1.136351E-03   6.673753E-02   5.798296E-02   6.827452E-02  -4.912974E-02  -5.258934E-03  -0.142534    
 ref    3   9.950948E-03  -3.677894E-02   5.817978E-03  -1.696445E-02  -2.601903E-03  -9.679266E-02  -7.207681E-02   3.118279E-02
 ref    4  -1.482967E-03  -7.557591E-03   6.976355E-02  -9.596431E-02  -2.743457E-02  -3.507089E-02  -1.699267E-02  -1.177087E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.911975       0.939038       0.563891       0.188706       0.597154       0.530719       3.348071E-03   1.511603E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   4.850908E-03   1.424617E-03   9.363450E-03   1.370957E-02   5.429907E-03   1.349201E-02   2.185443E-02   2.183999E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15        ev   16

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00000033    -0.96875815     0.02781582    -0.02167579     0.00000614    -0.00002818     0.00909186     0.11480547
 ref:   2     0.00000015     0.02108549     0.75032034     0.02077159     0.39328514    -0.47040238    -0.00159067    -0.02510993
 ref:   3     0.00000035     0.01002957     0.01168701    -0.43336389    -0.66519237    -0.55627399    -0.01024402     0.03595121
 ref:   4     0.95497381     0.00000059     0.00000221    -0.00006335    -0.00000617    -0.00000578     0.05619554    -0.00356884

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1     0.01589456     0.00367662     0.00295817    -0.02916519     0.00301193    -0.02189726     0.12783959    -0.02032636
 ref:   2     0.06706005     0.00113635     0.06673753     0.05798296     0.06827452    -0.04912974    -0.00525893    -0.14253389
 ref:   3     0.00995095    -0.03677894     0.00581798    -0.01696445    -0.00260190    -0.09679266    -0.07207681     0.03118279
 ref:   4    -0.00148297    -0.00755759     0.06976355    -0.09596431    -0.02743457    -0.03507089    -0.01699267    -0.01177087

 trial vector basis is being transformed.  new dimension:   8

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 52  1   -196.4879584901  6.6395E-10  0.0000E+00  3.0125E-04  1.0000E-04   
 mr-sdci # 52  2   -196.4725827857  4.0993E-10  0.0000E+00  1.6755E-04  1.0000E-04   
 mr-sdci # 52  3   -196.4725827148  6.3371E-09  0.0000E+00  6.4459E-04  1.0000E-04   
 mr-sdci # 52  4   -196.4725759737  6.8057E-05  0.0000E+00  6.8802E-03  1.0000E-04   
 mr-sdci # 52  5   -196.4644807206  8.5831E-08  3.8899E-07  1.1632E-03  1.0000E-04   
 mr-sdci # 52  6   -196.4644800384  1.4502E-08  0.0000E+00  1.7667E-03  1.0000E-04   
 mr-sdci # 52  7   -195.8102711192  8.3706E-04  0.0000E+00  2.7328E-01  1.0000E-04   
 mr-sdci # 52  8   -195.7650981292  7.0923E-04  0.0000E+00  2.0862E-01  1.0000E-04   
 mr-sdci # 52  9   -195.7246706631  3.1193E-03  0.0000E+00  4.7397E-01  1.0000E-04   
 mr-sdci # 52 10   -195.5976338734  8.7513E-01  0.0000E+00  1.1515E+00  1.0000E-04   
 mr-sdci # 52 11   -194.7039941983  1.0335E-01  0.0000E+00  2.1443E+00  1.0000E-04   
 mr-sdci # 52 12   -194.3247138700  1.1087E-01  0.0000E+00  2.8927E+00  1.0000E-04   
 mr-sdci # 52 13   -193.3774929345  3.6093E-01  0.0000E+00  3.4790E+00  1.0000E-04   
 mr-sdci # 52 14   -191.8715676130  9.5864E-01  0.0000E+00  3.0612E+00  1.0000E-04   
 mr-sdci # 52 15   -190.8755614507  9.7191E-01  0.0000E+00  4.8999E+00  1.0000E-04   
 mr-sdci # 52 16   -189.8499153572 -6.3377E-01  0.0000E+00  4.2803E+00  1.0000E-04   
 
 root number  5 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.011000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  53

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59964369
   ht   2     0.00000000   -50.58426799
   ht   3     0.00000000     0.00000000   -50.58426792
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426118
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616592
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616524
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.92195632
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.87678333
   ht   9     0.00069647    -0.00109357     0.00266642    -0.01355630    -0.00644039     0.01653488     0.00055955     0.00080625

                ht   9
   ht   9    -0.00001892

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   4.048951E-07   0.968675      -3.062035E-02   2.160965E-02  -3.610657E-06   2.825478E-05  -9.146776E-03  -0.114776    
 ref    2  -6.626238E-07  -2.327526E-02  -0.750273      -2.004682E-02  -0.390198       0.472980       1.244234E-03   2.498482E-02
 ref    3   4.160222E-07  -1.002136E-02  -1.124633E-02   0.433371       0.668838       0.551884       1.009242E-02  -3.599970E-02
 ref    4   0.954974      -5.507815E-07  -1.807902E-06   6.444917E-05   2.472814E-06   5.674960E-06  -5.626116E-02   3.602279E-03

              v      9
 ref    1  -8.004894E-03
 ref    2   4.861958E-02
 ref    3   1.438216E-02
 ref    4   8.730165E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.911975       0.938973       0.563973       0.188679       0.599599       0.528286       3.352387E-03   1.510678E-02

              v      9
 ref    1   2.711004E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00000040     0.96867502    -0.03062035     0.02160965    -0.00000361     0.00002825    -0.00914678    -0.11477624
 ref:   2    -0.00000066    -0.02327526    -0.75027276    -0.02004682    -0.39019786     0.47298015     0.00124423     0.02498482
 ref:   3     0.00000042    -0.01002136    -0.01124633     0.43337069     0.66883848     0.55188426     0.01009242    -0.03599970
 ref:   4     0.95497366    -0.00000055    -0.00000181     0.00006445     0.00000247     0.00000567    -0.05626116     0.00360228

                ci   9
 ref:   1    -0.00800489
 ref:   2     0.04861958
 ref:   3     0.01438216
 ref:   4     0.00873016

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 53  1   -196.4879584907  5.6504E-10  0.0000E+00  2.9986E-04  1.0000E-04   
 mr-sdci # 53  2   -196.4725827857  2.0840E-11  0.0000E+00  1.6784E-04  1.0000E-04   
 mr-sdci # 53  3   -196.4725827168  1.9929E-09  0.0000E+00  6.2248E-04  1.0000E-04   
 mr-sdci # 53  4   -196.4725759944  2.0654E-08  0.0000E+00  6.7923E-03  1.0000E-04   
 mr-sdci # 53  5   -196.4644809934  2.7287E-07  0.0000E+00  6.3578E-04  1.0000E-04   
 mr-sdci # 53  6   -196.4644800385  1.0348E-10  1.0329E-06  1.7678E-03  1.0000E-04   
 mr-sdci # 53  7   -195.8104089934  1.3787E-04  0.0000E+00  2.7304E-01  1.0000E-04   
 mr-sdci # 53  8   -195.7651130901  1.4961E-05  0.0000E+00  2.1029E-01  1.0000E-04   
 mr-sdci # 53  9   -193.4862135526 -2.2385E+00  0.0000E+00  3.9047E+00  1.0000E-04   
 
 root number  6 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.006000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  54

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59964369
   ht   2     0.00000000   -50.58426799
   ht   3     0.00000000     0.00000000   -50.58426792
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426118
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616592
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616524
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.92195632
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.87678333
   ht   9     0.00069647    -0.00109357     0.00266642    -0.01355630    -0.00644039     0.01653488     0.00055955     0.00080625
   ht  10     0.00535847    -0.01595466    -0.00293105     0.01617569    -0.02685060     0.00165569     0.00020544     0.00195968

                ht   9         ht  10
   ht   9    -0.00001892
   ht  10     0.00000118    -0.00004887

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -2.792487E-08  -0.968762      -2.826514E-02  -2.088196E-02  -4.121805E-06   1.073061E-06  -9.020043E-03  -0.114664    
 ref    2   3.437271E-07   2.143968E-02  -0.750297       2.116830E-02  -0.370036       0.488922       1.210042E-03   2.495611E-02
 ref    3  -4.501675E-07   9.694907E-03  -1.194460E-02  -0.433372       0.691373       0.523327       1.024663E-02  -3.589059E-02
 ref    4  -0.954973       6.384192E-07  -1.604680E-06  -5.639722E-05   1.956672E-06  -6.522294E-06  -5.618405E-02   3.671094E-03

              v      9       v     10
 ref    1  -2.056254E-02  -5.646518E-02
 ref    2  -3.297670E-02   4.186298E-02
 ref    3  -3.903820E-02  -4.346844E-02
 ref    4  -2.006628E-02  -1.946447E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.911974       0.939053       0.563887       0.188696       0.614923       0.512915       3.344467E-03   1.507216E-02

              v      9       v     10
 ref    1   3.436917E-03   7.209197E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00000003    -0.96876190    -0.02826514    -0.02088196    -0.00000412     0.00000107    -0.00902004    -0.11466360
 ref:   2     0.00000034     0.02143968    -0.75029692     0.02116830    -0.37003648     0.48892154     0.00121004     0.02495611
 ref:   3    -0.00000045     0.00969491    -0.01194460    -0.43337236     0.69137251     0.52332687     0.01024663    -0.03589059
 ref:   4    -0.95497348     0.00000064    -0.00000160    -0.00005640     0.00000196    -0.00000652    -0.05618405     0.00367109

                ci   9         ci  10
 ref:   1    -0.02056254    -0.05646518
 ref:   2    -0.03297670     0.04186298
 ref:   3    -0.03903820    -0.04346844
 ref:   4    -0.02006628    -0.01946447

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 54  1   -196.4879584909  1.5618E-10  3.3306E-08  2.9210E-04  1.0000E-04   
 mr-sdci # 54  2   -196.4725827858  7.5680E-11  0.0000E+00  1.6629E-04  1.0000E-04   
 mr-sdci # 54  3   -196.4725827172  3.7943E-10  0.0000E+00  6.2100E-04  1.0000E-04   
 mr-sdci # 54  4   -196.4725762703  2.7588E-07  0.0000E+00  6.6552E-03  1.0000E-04   
 mr-sdci # 54  5   -196.4644809942  8.0956E-10  0.0000E+00  6.3823E-04  1.0000E-04   
 mr-sdci # 54  6   -196.4644806830  6.4456E-07  0.0000E+00  8.4724E-04  1.0000E-04   
 mr-sdci # 54  7   -195.8104293615  2.0368E-05  0.0000E+00  2.7471E-01  1.0000E-04   
 mr-sdci # 54  8   -195.7651223609  9.2708E-06  0.0000E+00  2.0961E-01  1.0000E-04   
 mr-sdci # 54  9   -193.6309872263  1.4477E-01  0.0000E+00  3.7556E+00  1.0000E-04   
 mr-sdci # 54 10   -192.9581126972 -2.6395E+00  0.0000E+00  3.7966E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.013000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  55

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59964369
   ht   2     0.00000000   -50.58426799
   ht   3     0.00000000     0.00000000   -50.58426792
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426118
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616592
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616524
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.92195632
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.87678333
   ht   9     0.00069647    -0.00109357     0.00266642    -0.01355630    -0.00644039     0.01653488     0.00055955     0.00080625
   ht  10     0.00535847    -0.01595466    -0.00293105     0.01617569    -0.02685060     0.00165569     0.00020544     0.00195968
   ht  11    -0.00322110     0.00071074     0.00002833     0.00063544     0.00203710     0.00120157     0.00003265    -0.00019891

                ht   9         ht  10         ht  11
   ht   9    -0.00001892
   ht  10     0.00000118    -0.00004887
   ht  11    -0.00000011     0.00000121    -0.00000133

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -2.365921E-07   0.968829      -2.498655E-02   2.192359E-02  -4.056758E-06  -1.136346E-06   9.996436E-03   0.114583    
 ref    2  -5.631394E-07  -1.896628E-02  -0.750461      -1.737677E-02  -0.369671      -0.489198      -1.481495E-03  -2.494779E-02
 ref    3   4.471117E-07  -1.006572E-02  -9.777711E-03   0.433395       0.691765      -0.522811      -7.397203E-03   3.611003E-02
 ref    4   0.954960       1.348886E-08  -5.035844E-06  -3.320795E-05   3.495471E-06   5.077150E-06   6.456443E-02  -3.704701E-03

              v      9       v     10       v     11
 ref    1  -7.078740E-03  -2.160971E-02   5.585178E-02
 ref    2   6.987315E-03  -3.218725E-02  -4.217724E-02
 ref    3   3.475688E-02  -3.709752E-02   5.145044E-02
 ref    4   0.124865      -1.148541E-02   4.823323E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.911949       0.939091       0.563911       0.188613       0.615195       0.512646       4.325407E-03   1.506926E-02

              v      9       v     10       v     11
 ref    1   1.689833E-02   3.011140E-03   9.871933E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00000024     0.96882897    -0.02498655     0.02192359    -0.00000406    -0.00000114     0.00999644     0.11458274
 ref:   2    -0.00000056    -0.01896628    -0.75046082    -0.01737677    -0.36967073    -0.48919812    -0.00148150    -0.02494779
 ref:   3     0.00000045    -0.01006572    -0.00977771     0.43339461     0.69176494    -0.52281059    -0.00739720     0.03611003
 ref:   4     0.95495998     0.00000001    -0.00000504    -0.00003321     0.00000350     0.00000508     0.06456443    -0.00370470

                ci   9         ci  10         ci  11
 ref:   1    -0.00707874    -0.02160971     0.05585178
 ref:   2     0.00698732    -0.03218725    -0.04217724
 ref:   3     0.03475688    -0.03709752     0.05145044
 ref:   4     0.12486535    -0.01148541     0.04823323

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 55  1   -196.4879585127  2.1881E-08  0.0000E+00  1.6235E-04  1.0000E-04   
 mr-sdci # 55  2   -196.4725827858  4.1588E-11  1.0414E-08  1.6514E-04  1.0000E-04   
 mr-sdci # 55  3   -196.4725827184  1.1911E-09  0.0000E+00  6.1397E-04  1.0000E-04   
 mr-sdci # 55  4   -196.4725770596  7.8934E-07  0.0000E+00  5.8743E-03  1.0000E-04   
 mr-sdci # 55  5   -196.4644809945  2.4826E-10  0.0000E+00  6.3899E-04  1.0000E-04   
 mr-sdci # 55  6   -196.4644806832  2.1832E-10  0.0000E+00  8.3953E-04  1.0000E-04   
 mr-sdci # 55  7   -195.8178316529  7.4023E-03  0.0000E+00  2.5378E-01  1.0000E-04   
 mr-sdci # 55  8   -195.7651456126  2.3252E-05  0.0000E+00  2.0892E-01  1.0000E-04   
 mr-sdci # 55  9   -194.0236983472  3.9271E-01  0.0000E+00  2.8001E+00  1.0000E-04   
 mr-sdci # 55 10   -193.6289990267  6.7089E-01  0.0000E+00  3.7423E+00  1.0000E-04   
 mr-sdci # 55 11   -192.9032022387 -1.8008E+00  0.0000E+00  4.0216E+00  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.008000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  56

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59964369
   ht   2     0.00000000   -50.58426799
   ht   3     0.00000000     0.00000000   -50.58426792
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426118
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616592
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616524
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.92195632
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.87678333
   ht   9     0.00069647    -0.00109357     0.00266642    -0.01355630    -0.00644039     0.01653488     0.00055955     0.00080625
   ht  10     0.00535847    -0.01595466    -0.00293105     0.01617569    -0.02685060     0.00165569     0.00020544     0.00195968
   ht  11    -0.00322110     0.00071074     0.00002833     0.00063544     0.00203710     0.00120157     0.00003265    -0.00019891
   ht  12    -0.00008765     0.00237969     0.00030245     0.00079067    -0.00067618     0.00017899    -0.00005630    -0.00015716

                ht   9         ht  10         ht  11         ht  12
   ht   9    -0.00001892
   ht  10     0.00000118    -0.00004887
   ht  11    -0.00000011     0.00000121    -0.00000133
   ht  12     0.00000009     0.00000014    -0.00000001    -0.00000040

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7         v:   8

   eig(s)   5.394605E-09   2.045925E-08   1.849156E-07   4.996924E-07    1.00000        1.00000        1.00000        1.00000    
 
   x:   1  -3.097987E-06  -6.493816E-05   1.916882E-05  -1.042385E-04  -3.848699E-02   0.147121       0.264017      -0.470359    
   x:   2   4.774558E-05   1.664877E-05  -4.250993E-05   3.130433E-04  -4.047987E-02   4.299806E-02   9.957422E-02   0.121646    
   x:   3   5.934157E-06  -4.338166E-07   4.866056E-05   6.139040E-05  -0.156241      -0.153993      -0.839721      -0.458450    
   x:   4   1.611098E-05   1.650642E-05  -2.451758E-04  -3.373871E-04  -8.726979E-02   0.106956       0.126231      -0.429520    
   x:   5  -1.163916E-05   4.883726E-05  -1.619770E-04   5.206789E-04  -9.240994E-02   0.108232       0.181702      -0.384968    
   x:   6   3.159881E-06   1.459061E-05   3.289331E-04  -1.053319E-05  -3.976140E-02   0.112457       0.311854      -0.413737    
   x:   7  -1.154597E-06   6.636810E-07   1.164505E-05  -3.453091E-06   0.127102       0.950385      -0.258830       0.110168    
   x:   8  -3.451154E-06  -4.702954E-06   1.873061E-05  -3.813382E-05  -0.968802       0.117306       4.531593E-02   0.194384    
   x:   9  -2.403602E-03  -2.686716E-02   0.997309       6.816585E-02  -1.895717E-14  -7.996360E-13   2.079995E-13  -1.405449E-13
   x:  10  -1.246925E-03  -9.246128E-03   6.793676E-02  -0.997646       1.523085E-13   1.051263E-13   1.723581E-13  -3.018399E-13
   x:  11   1.886558E-02   0.999417       2.747623E-02  -7.415100E-03   2.484441E-12  -1.648578E-12   3.628699E-12  -6.123222E-12
   x:  12   0.999818      -1.893413E-02   1.963851E-03  -9.404306E-04  -6.691469E-13    0.00000        0.00000        0.00000    

                v:   9         v:  10         v:  11         v:  12

   eig(s)    1.00000        1.00000        1.00000        1.00000    
 
   x:   1   0.132061       0.802258      -3.333954E-02  -0.154148    
   x:   2   0.877274      -2.786048E-02   5.268071E-02   0.445688    
   x:   3   0.125184       1.857729E-02  -0.117554       8.205161E-02
   x:   4   0.215939      -0.414472       0.598947      -0.450964    
   x:   5  -0.378232      -9.131908E-02   0.290756       0.750061    
   x:   6   6.676370E-02  -0.417261      -0.732807      -4.208910E-02
   x:   7  -2.266056E-02  -1.686685E-02  -2.552418E-02  -5.820351E-03
   x:   8  -5.117749E-02   2.725582E-02  -3.687860E-02  -5.568982E-02
   x:   9  -4.809672E-07   8.362185E-07   4.457978E-04  -3.188236E-06
   x:  10   4.348124E-08   4.032139E-06   1.708809E-06   7.076515E-04
   x:  11   6.764577E-06   7.037643E-05  -4.745464E-06  -3.988039E-05
   x:  12  -5.065266E-05   9.394172E-06  -4.865306E-06  -4.688222E-06
 bummer (warning):overlap matrix: # small eigenvalues=                      1

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   9.646952E-09   0.966727      -6.748735E-02  -2.506433E-02   5.041387E-06  -2.117443E-06  -1.087925E-02  -0.108467    
 ref    2  -5.283431E-07  -5.180808E-02  -0.748886       1.839529E-02   0.370365      -0.488673       1.981137E-03   2.558295E-02
 ref    3   3.208353E-07  -1.193464E-02  -9.815400E-03  -0.433352      -0.691023      -0.523791       6.508288E-03  -3.822607E-02
 ref    4   0.954960      -2.979801E-07  -5.061221E-06   3.278378E-05  -3.546097E-06   5.115782E-06  -6.453425E-02   4.483238E-03

              v      9       v     10       v     11       v     12
 ref    1   9.385673E-02   4.380928E-02   7.196935E-03  -7.305662E-02
 ref    2   1.228775E-02   1.687667E-02   2.850150E-02   4.069943E-02
 ref    3  -4.565530E-02   1.496847E-02   4.239777E-02  -4.518960E-02
 ref    4  -6.206980E-02   0.107977       8.269045E-03  -4.994270E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.911949       0.937388       0.565481       0.188760       0.614683       0.513159       4.329311E-03   1.390097E-02

              v      9       v     10       v     11       v     12
 ref    1   1.489714E-02   1.408717E-02   2.730079E-03   1.153009E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00000001     0.96672717    -0.06748735    -0.02506433     0.00000504    -0.00000212    -0.01087925    -0.10846730
 ref:   2    -0.00000053    -0.05180808    -0.74888588     0.01839529     0.37036463    -0.48867347     0.00198114     0.02558295
 ref:   3     0.00000032    -0.01193464    -0.00981540    -0.43335184    -0.69102288    -0.52379110     0.00650829    -0.03822607
 ref:   4     0.95495997    -0.00000030    -0.00000506     0.00003278    -0.00000355     0.00000512    -0.06453425     0.00448324

                ci   9         ci  10         ci  11         ci  12
 ref:   1     0.09385673     0.04380928     0.00719694    -0.07305662
 ref:   2     0.01228775     0.01687667     0.02850150     0.04069943
 ref:   3    -0.04565530     0.01496847     0.04239777    -0.04518960
 ref:   4    -0.06206980     0.10797705     0.00826904    -0.04994270

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 56  1   -196.4879585127  1.5376E-11  0.0000E+00  1.6218E-04  1.0000E-04   
 mr-sdci # 56  2   -196.4725827946  8.7555E-09  0.0000E+00  1.2296E-04  1.0000E-04   
 mr-sdci # 56  3   -196.4725827195  1.1130E-09  8.6665E-08  6.0405E-04  1.0000E-04   
 mr-sdci # 56  4   -196.4725771005  4.0910E-08  0.0000E+00  5.8076E-03  1.0000E-04   
 mr-sdci # 56  5   -196.4644809949  4.3918E-10  0.0000E+00  6.4031E-04  1.0000E-04   
 mr-sdci # 56  6   -196.4644806837  4.4388E-10  0.0000E+00  8.3735E-04  1.0000E-04   
 mr-sdci # 56  7   -195.8179519465  1.2029E-04  0.0000E+00  2.5340E-01  1.0000E-04   
 mr-sdci # 56  8   -195.7703976578  5.2520E-03  0.0000E+00  2.2200E-01  1.0000E-04   
 mr-sdci # 56  9   -194.1847190213  1.6102E-01  0.0000E+00  2.7983E+00  1.0000E-04   
 mr-sdci # 56 10   -193.9729082688  3.4391E-01  0.0000E+00  2.8981E+00  1.0000E-04   
 mr-sdci # 56 11   -193.6168631628  7.1366E-01  0.0000E+00  3.7190E+00  1.0000E-04   
 mr-sdci # 56 12   -192.8676157122 -1.4571E+00  0.0000E+00  4.0504E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.006000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  57

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59964369
   ht   2     0.00000000   -50.58426799
   ht   3     0.00000000     0.00000000   -50.58426792
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426118
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616592
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616524
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.92195632
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.87678333
   ht   9     0.00069647    -0.00109357     0.00266642    -0.01355630    -0.00644039     0.01653488     0.00055955     0.00080625
   ht  10     0.00535847    -0.01595466    -0.00293105     0.01617569    -0.02685060     0.00165569     0.00020544     0.00195968
   ht  11    -0.00322110     0.00071074     0.00002833     0.00063544     0.00203710     0.00120157     0.00003265    -0.00019891
   ht  12    -0.00008765     0.00237969     0.00030245     0.00079067    -0.00067618     0.00017899    -0.00005630    -0.00015716
   ht  13    -0.00149179    -0.00327951     0.00187967    -0.00154076    -0.00092931    -0.00296740    -0.00001919     0.00064246

                ht   9         ht  10         ht  11         ht  12         ht  13
   ht   9    -0.00001892
   ht  10     0.00000118    -0.00004887
   ht  11    -0.00000011     0.00000121    -0.00000133
   ht  12     0.00000009     0.00000014    -0.00000001    -0.00000040
   ht  13     0.00000049    -0.00000081    -0.00000002     0.00000018    -0.00000251

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7         v:   8

   eig(s)   5.392368E-09   2.023306E-08   4.088185E-08   1.850488E-07   4.997112E-07    1.00000        1.00000        1.00000    
 
   x:   1  -3.260727E-06  -6.149001E-05  -3.616113E-05   2.011079E-05   1.040439E-04  -0.210083       5.070717E-02  -0.223062    
   x:   2   4.721322E-05   2.335124E-05  -6.236794E-05  -4.061398E-05  -3.134481E-04   2.867373E-02  -9.429511E-04   0.160465    
   x:   3   6.243980E-06  -4.481623E-06   3.870548E-05   4.749235E-05  -6.115552E-05  -0.450347       0.124905      -0.645052    
   x:   4   1.578415E-05   2.053312E-05  -3.824242E-05  -2.440474E-04   3.372074E-04  -0.252973       5.226056E-02  -0.230462    
   x:   5  -1.184830E-05   5.058539E-05  -1.464677E-05  -1.615117E-04  -5.207700E-04  -0.230095       3.337139E-02  -0.171795    
   x:   6   2.737726E-06   1.974511E-05  -4.693302E-05   3.305610E-04   1.012824E-05  -0.177152       1.232782E-03  -0.167132    
   x:   7  -1.156409E-06   6.694368E-07   1.076943E-08   1.165250E-05   3.449439E-06   5.632398E-02   0.988045       0.137389    
   x:   8  -3.341640E-06  -6.048611E-06   1.256259E-05   1.834624E-05   3.821311E-05  -0.775072      -4.179870E-02   0.615602    
   x:   9  -2.174897E-03  -2.954525E-02   2.781308E-02   0.996840      -6.825417E-02  -1.470791E-13  -1.291426E-15  -1.528717E-14
   x:  10  -1.270496E-03  -8.747272E-03  -5.254694E-03   6.819209E-02   0.997619      -5.411746E-15   7.830304E-14   1.168027E-13
   x:  11   1.772949E-02   0.993893       0.105235       2.706989E-02   7.441150E-03  -9.319779E-13   1.699787E-12   4.682391E-13
   x:  12   0.999808      -1.686340E-02  -9.761577E-03   2.018015E-03   9.360683E-04   4.568995E-12  -5.150880E-13  -7.142922E-12
   x:  13   7.995750E-03  -0.104610       0.993996      -3.037825E-02   6.405051E-03   9.322462E-13    0.00000      -2.316766E-12

                v:   9         v:  10         v:  11         v:  12         v:  13

   eig(s)    1.00000        1.00000        1.00000        1.00000        1.00000    
 
   x:   1  -0.281341       0.870787       0.203695      -3.551890E-02   0.152846    
   x:   2   0.603502       0.180738       0.611135       4.934298E-02  -0.447914    
   x:   3   0.486440      -3.775901E-02  -0.328261      -0.114898      -8.054542E-02
   x:   4  -3.748346E-02  -0.302187       0.480269       0.595533       0.449579    
   x:   5  -0.503363      -0.124441       1.657675E-02   0.290922      -0.750105    
   x:   6  -0.251904      -0.316965       0.483066      -0.736104       3.948314E-02
   x:   7  -2.577184E-02  -1.919440E-02   2.413499E-04  -2.554094E-02   5.791893E-03
   x:   8   3.331245E-02  -7.765536E-04  -0.113937      -3.613763E-02   5.614413E-02
   x:   9  -7.542417E-07   9.381356E-07   2.448713E-06   4.457878E-04   3.546699E-06
   x:  10   1.212069E-07   3.647381E-06  -3.927162E-06   2.299413E-06  -7.076410E-04
   x:  11   2.257300E-07   6.927090E-05  -1.406743E-05  -4.721288E-06   3.991083E-05
   x:  12  -3.695900E-05  -2.611325E-06  -3.580416E-05  -4.658274E-06   4.813399E-06
   x:  13  -1.284933E-05   8.707302E-06   1.025569E-04  -1.282973E-05  -2.002714E-05
 bummer (warning):overlap matrix: # small eigenvalues=                      1

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   4.939815E-08   0.956811      -0.153623      -2.564537E-02  -3.821608E-06  -3.047189E-06  -1.092514E-02  -0.108786    
 ref    2   5.487180E-07  -0.118778      -0.741385       9.553990E-03  -0.368156      -0.490340       1.987651E-03   2.560771E-02
 ref    3  -3.191962E-07  -1.221726E-02  -3.628028E-03  -0.433437       0.693382      -0.520666       6.497861E-03  -3.827139E-02
 ref    4  -0.954960      -4.708938E-09   1.861858E-06   2.508370E-05   2.262147E-06   6.076705E-06  -6.450402E-02   4.835698E-03

              v      9       v     10       v     11       v     12       v     13
 ref    1  -9.842200E-02  -4.648352E-02   7.843100E-03  -1.103801E-02   7.442707E-02
 ref    2  -8.802063E-03  -1.785867E-02   2.851588E-02  -7.510619E-03  -4.066666E-02
 ref    3   4.198883E-02  -1.297038E-02   2.637690E-02  -3.979351E-02   4.717987E-02
 ref    4   7.535808E-02  -0.106894       9.575272E-03   1.816447E-02   4.813090E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.911949       0.929744       0.573265       0.188616       0.616317       0.511527       4.326301E-03   1.397822E-02

              v      9       v     10       v     11       v     12       v     13
 ref    1   1.720627E-02   1.407427E-02   1.662096E-03   2.091719E-03   1.173569E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00000005     0.95681079    -0.15362273    -0.02564537    -0.00000382    -0.00000305    -0.01092514    -0.10878596
 ref:   2     0.00000055    -0.11877810    -0.74138522     0.00955399    -0.36815563    -0.49033984     0.00198765     0.02560771
 ref:   3    -0.00000032    -0.01221726    -0.00362803    -0.43343682     0.69338163    -0.52066645     0.00649786    -0.03827139
 ref:   4    -0.95496003     0.00000000     0.00000186     0.00002508     0.00000226     0.00000608    -0.06450402     0.00483570

                ci   9         ci  10         ci  11         ci  12         ci  13
 ref:   1    -0.09842200    -0.04648352     0.00784310    -0.01103801     0.07442707
 ref:   2    -0.00880206    -0.01785867     0.02851588    -0.00751062    -0.04066666
 ref:   3     0.04198883    -0.01297038     0.02637690    -0.03979351     0.04717987
 ref:   4     0.07535808    -0.10689427     0.00957527     0.01816447     0.04813090

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 57  1   -196.4879585128  4.1780E-12  0.0000E+00  1.6212E-04  1.0000E-04   
 mr-sdci # 57  2   -196.4725827947  1.2362E-10  0.0000E+00  1.1427E-04  1.0000E-04   
 mr-sdci # 57  3   -196.4725827819  6.2432E-08  0.0000E+00  2.0592E-04  1.0000E-04   
 mr-sdci # 57  4   -196.4725771706  7.0072E-08  6.6663E-06  5.6818E-03  1.0000E-04   
 mr-sdci # 57  5   -196.4644809968  1.8686E-09  0.0000E+00  6.5011E-04  1.0000E-04   
 mr-sdci # 57  6   -196.4644806847  1.0595E-09  0.0000E+00  8.3476E-04  1.0000E-04   
 mr-sdci # 57  7   -195.8179526475  7.0099E-07  0.0000E+00  2.5342E-01  1.0000E-04   
 mr-sdci # 57  8   -195.7704889029  9.1245E-05  0.0000E+00  2.1600E-01  1.0000E-04   
 mr-sdci # 57  9   -194.2560431509  7.1324E-02  0.0000E+00  2.8849E+00  1.0000E-04   
 mr-sdci # 57 10   -193.9740098118  1.1015E-03  0.0000E+00  2.8981E+00  1.0000E-04   
 mr-sdci # 57 11   -193.6640247307  4.7162E-02  0.0000E+00  3.7686E+00  1.0000E-04   
 mr-sdci # 57 12   -193.3164922149  4.4888E-01  0.0000E+00  3.1020E+00  1.0000E-04   
 mr-sdci # 57 13   -192.8655494387 -5.1194E-01  0.0000E+00  4.0741E+00  1.0000E-04   
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.008000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  58

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59964369
   ht   2     0.00000000   -50.58426799
   ht   3     0.00000000     0.00000000   -50.58426792
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426118
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616592
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616524
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.92195632
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.87678333
   ht   9     0.00069647    -0.00109357     0.00266642    -0.01355630    -0.00644039     0.01653488     0.00055955     0.00080625
   ht  10     0.00535847    -0.01595466    -0.00293105     0.01617569    -0.02685060     0.00165569     0.00020544     0.00195968
   ht  11    -0.00322110     0.00071074     0.00002833     0.00063544     0.00203710     0.00120157     0.00003265    -0.00019891
   ht  12    -0.00008765     0.00237969     0.00030245     0.00079067    -0.00067618     0.00017899    -0.00005630    -0.00015716
   ht  13    -0.00149179    -0.00327951     0.00187967    -0.00154076    -0.00092931    -0.00296740    -0.00001919     0.00064246
   ht  14    -0.01858452    -0.00974020    -0.00052972     0.05046871     0.02521266     0.04826494    -0.00142582    -0.00169913

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14
   ht   9    -0.00001892
   ht  10     0.00000118    -0.00004887
   ht  11    -0.00000011     0.00000121    -0.00000133
   ht  12     0.00000009     0.00000014    -0.00000001    -0.00000040
   ht  13     0.00000049    -0.00000081    -0.00000002     0.00000018    -0.00000251
   ht  14    -0.00000344    -0.00000470    -0.00000222     0.00000020     0.00000248    -0.00024814

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7         v:   8

   eig(s)   5.366343E-09   1.976575E-08   4.071112E-08   1.821832E-07   4.995452E-07   2.776495E-06    1.00000        1.00000    
 
   x:   1   4.826105E-06  -6.588541E-05  -3.404476E-05   3.180266E-05  -1.009919E-04  -3.667342E-04   3.064757E-02  -2.665041E-02
   x:   2  -4.692279E-05   2.140409E-05  -6.052071E-05  -3.300259E-05   3.152123E-04  -1.920152E-04  -7.019817E-02  -6.029970E-02
   x:   3  -6.023521E-06  -5.548353E-06   3.897771E-05   4.771695E-05   6.111391E-05  -8.014390E-06   0.165226       6.212876E-03
   x:   4  -1.956234E-05   3.593554E-05  -4.706363E-05  -2.773254E-04  -3.450699E-04   9.855173E-04   1.099980E-02  -4.257334E-02
   x:   5   9.691847E-06   5.826293E-05  -1.849532E-05  -1.764665E-04   5.169281E-04   4.965152E-04  -1.905489E-02  -4.485612E-02
   x:   6  -5.480532E-06   3.065947E-05  -5.262344E-05   2.990729E-04  -1.897225E-05   9.639113E-04  -1.113338E-02   5.307372E-04
   x:   7   1.263262E-06   2.029793E-07   3.089804E-07   1.263986E-05  -3.220623E-06  -2.971902E-05   0.570932      -0.813520    
   x:   8   3.535459E-06  -6.722191E-06   1.280015E-05   1.921397E-05  -3.798985E-05  -3.174882E-05  -0.800164      -0.574434    
   x:   9   3.965680E-03  -3.679556E-02   3.212789E-02   0.996029       6.589736E-02   3.437659E-02   5.926098E-13  -3.914748E-13
   x:  10   1.305022E-03  -8.598748E-03  -5.389082E-03   6.607610E-02  -0.997743      -6.211895E-03   1.084173E-13   1.597308E-14
   x:  11  -2.494624E-02   0.991926       0.118952       3.374928E-02  -6.919228E-03  -1.115887E-02   1.728534E-12   1.224133E-12
   x:  12  -0.999661      -2.432103E-02  -8.134805E-03   3.493384E-03  -8.053166E-04  -2.782645E-03   6.768426E-12  -2.437074E-12
   x:  13  -5.351037E-03  -0.117857       0.992301      -3.617053E-02  -6.796877E-03   8.112019E-03   2.275217E-12   1.217946E-12
   x:  14  -3.147109E-03   1.317790E-02  -7.888271E-03  -3.317337E-02  -8.493424E-03   0.999289        0.00000        0.00000    

                v:   9         v:  10         v:  11         v:  12         v:  13         v:  14

   eig(s)    1.00000        1.00000        1.00000        1.00000        1.00000        1.00000    
 
   x:   1  -0.279808       0.763100       0.498055      -2.719979E-02  -0.181898       0.236326    
   x:   2  -0.192029      -0.475567       0.719449       5.067055E-02   0.435724       0.135529    
   x:   3  -0.909347      -0.107096      -0.338703      -0.114763       7.929680E-02   1.006577E-02
   x:   4  -0.182116      -7.008638E-02   0.213058       0.577166      -0.372897      -0.665147    
   x:   5   3.889768E-02   0.413672      -0.163058       0.277354       0.791897      -0.307201    
   x:   6   1.227771E-02   5.503140E-02   0.189498      -0.756056       3.255855E-02  -0.622980    
   x:   7   0.102838      -2.392444E-02   2.173721E-03  -2.493041E-02  -8.149636E-03   1.950818E-02
   x:   8  -0.111866       2.018435E-02  -0.108253      -3.512378E-02  -5.907106E-02   1.999965E-02
   x:   9   4.110606E-07   7.049752E-07   2.666168E-06   4.455446E-04  -9.511610E-07  -1.510200E-05
   x:  10  -1.175209E-06   2.518224E-06  -2.092139E-06   1.125205E-06   7.022360E-04   8.741342E-05
   x:  11  -1.472016E-05   3.833546E-05   2.074346E-05  -2.955677E-06  -4.595600E-05   4.872663E-05
   x:  12   1.705889E-05   3.080512E-05  -3.749227E-05  -4.526774E-06  -5.189960E-06   2.556107E-06
   x:  13   1.044845E-05   4.072313E-06   8.990937E-05  -1.449559E-05   2.579367E-05  -4.732484E-05
   x:  14   8.370890E-07  -1.287003E-06   2.335576E-06   3.998347E-06  -3.796914E-05   1.525611E-03
 bummer (warning):overlap matrix: # small eigenvalues=                      1

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -2.820827E-07  -0.958178      -0.142690       3.578684E-02   3.771907E-06  -3.122253E-06  -8.842017E-03  -0.108163    
 ref    2  -4.504760E-07   0.110555      -0.742716      -1.320040E-03   0.368085      -0.490393       1.622673E-03   2.533553E-02
 ref    3   2.297975E-07   1.594580E-02   1.609575E-03   0.433216      -0.693456      -0.520565       9.866785E-03  -4.005331E-02
 ref    4   0.954960       1.169549E-07   1.991435E-06   3.779598E-06  -2.334081E-06   5.966901E-06  -6.517165E-02   3.502100E-03

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1   0.111963       7.900235E-03   2.124534E-02  -1.980607E-02   6.466882E-02   2.142556E-02
 ref    2   5.035893E-03  -9.473708E-03   3.684050E-02  -6.658561E-03  -3.641181E-02  -1.201605E-02
 ref    3  -5.243455E-02  -1.187071E-02   3.813029E-02  -3.400965E-02   1.026269E-02   0.109129    
 ref    4  -3.616016E-02   0.103253       6.837384E-02   2.132719E-02   3.517196E-02   3.822780E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.911948       0.930582       0.571991       0.188958       0.616367       0.511473       4.425512E-03   1.395775E-02

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1   1.661796E-02   1.095433E-02   7.937488E-03   2.048122E-03   6.850265E-03   1.397395E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00000028    -0.95817792    -0.14268991     0.03578684     0.00000377    -0.00000312    -0.00884202    -0.10816345
 ref:   2    -0.00000045     0.11055477    -0.74271647    -0.00132004     0.36808506    -0.49039280     0.00162267     0.02533553
 ref:   3     0.00000023     0.01594580     0.00160958     0.43321554    -0.69345566    -0.52056524     0.00986679    -0.04005331
 ref:   4     0.95495987     0.00000012     0.00000199     0.00000378    -0.00000233     0.00000597    -0.06517165     0.00350210

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14
 ref:   1     0.11196277     0.00790023     0.02124534    -0.01980607     0.06466882     0.02142556
 ref:   2     0.00503589    -0.00947371     0.03684050    -0.00665856    -0.03641181    -0.01201605
 ref:   3    -0.05243455    -0.01187071     0.03813029    -0.03400965     0.01026269     0.10912903
 ref:   4    -0.03616016     0.10325334     0.06837384     0.02132719     0.03517196     0.03822780

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 58  1   -196.4879585129  1.5951E-10  0.0000E+00  1.5700E-04  1.0000E-04   
 mr-sdci # 58  2   -196.4725827948  9.7948E-11  0.0000E+00  1.1174E-04  1.0000E-04   
 mr-sdci # 58  3   -196.4725827821  2.0079E-10  0.0000E+00  2.0449E-04  1.0000E-04   
 mr-sdci # 58  4   -196.4725817292  4.5586E-06  0.0000E+00  1.6307E-03  1.0000E-04   
 mr-sdci # 58  5   -196.4644809968  2.9715E-11  1.3648E-07  6.5122E-04  1.0000E-04   
 mr-sdci # 58  6   -196.4644806848  6.8020E-11  0.0000E+00  8.3635E-04  1.0000E-04   
 mr-sdci # 58  7   -195.8194772209  1.5246E-03  0.0000E+00  2.3802E-01  1.0000E-04   
 mr-sdci # 58  8   -195.7716873076  1.1984E-03  0.0000E+00  2.0018E-01  1.0000E-04   
 mr-sdci # 58  9   -194.2590636576  3.0205E-03  0.0000E+00  2.9693E+00  1.0000E-04   
 mr-sdci # 58 10   -194.2423227503  2.6831E-01  0.0000E+00  2.5451E+00  1.0000E-04   
 mr-sdci # 58 11   -193.8106947277  1.4667E-01  0.0000E+00  3.4278E+00  1.0000E-04   
 mr-sdci # 58 12   -193.3711583385  5.4666E-02  0.0000E+00  3.2300E+00  1.0000E-04   
 mr-sdci # 58 13   -192.9927268841  1.2718E-01  0.0000E+00  3.7602E+00  1.0000E-04   
 mr-sdci # 58 14   -191.8534057449 -1.8162E-02  0.0000E+00  4.9309E+00  1.0000E-04   
 
 root number  5 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.008000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  59

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59964369
   ht   2     0.00000000   -50.58426799
   ht   3     0.00000000     0.00000000   -50.58426792
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426118
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616592
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616524
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.92195632
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.87678333
   ht   9     0.00069647    -0.00109357     0.00266642    -0.01355630    -0.00644039     0.01653488     0.00055955     0.00080625
   ht  10     0.00535847    -0.01595466    -0.00293105     0.01617569    -0.02685060     0.00165569     0.00020544     0.00195968
   ht  11    -0.00322110     0.00071074     0.00002833     0.00063544     0.00203710     0.00120157     0.00003265    -0.00019891
   ht  12    -0.00008765     0.00237969     0.00030245     0.00079067    -0.00067618     0.00017899    -0.00005630    -0.00015716
   ht  13    -0.00149179    -0.00327951     0.00187967    -0.00154076    -0.00092931    -0.00296740    -0.00001919     0.00064246
   ht  14    -0.01858452    -0.00974020    -0.00052972     0.05046871     0.02521266     0.04826494    -0.00142582    -0.00169913
   ht  15     0.00009924    -0.00012463    -0.00206235     0.00828510     0.00272687    -0.00987259     0.00020878    -0.00028373

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15
   ht   9    -0.00001892
   ht  10     0.00000118    -0.00004887
   ht  11    -0.00000011     0.00000121    -0.00000133
   ht  12     0.00000009     0.00000014    -0.00000001    -0.00000040
   ht  13     0.00000049    -0.00000081    -0.00000002     0.00000018    -0.00000251
   ht  14    -0.00000344    -0.00000470    -0.00000222     0.00000020     0.00000248    -0.00024814
   ht  15     0.00000414    -0.00000128     0.00000002    -0.00000005    -0.00000034    -0.00000127    -0.00000679

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7         v:   8

   eig(s)   5.366155E-09   1.972795E-08   4.011907E-08   6.125284E-08   1.898498E-07   4.995684E-07   2.776624E-06    1.00000    
 
   x:   1   4.818814E-06   6.574657E-05  -3.373328E-05  -7.841389E-06  -3.163661E-05  -1.010475E-04  -3.667085E-04   2.048165E-02
   x:   2  -4.689637E-05  -2.192575E-05  -6.079728E-05  -1.597634E-06   3.187496E-05   3.152374E-04  -1.920339E-04  -8.582862E-02
   x:   3  -6.119439E-06   7.217335E-06   4.660479E-05  -4.332252E-05  -3.674919E-05   6.137543E-05  -8.292177E-06   0.158491    
   x:   4  -1.918091E-05  -4.235194E-05  -8.094724E-05   2.056719E-04   2.319854E-04  -3.460058E-04   9.866055E-04  -3.234675E-03
   x:   5   9.857081E-06  -6.093223E-05  -3.262230E-05   8.955912E-05   1.578176E-04   5.166745E-04   4.968562E-04  -3.243396E-02
   x:   6  -5.924715E-06  -2.319720E-05  -8.225900E-06  -2.747815E-04  -2.403858E-04  -1.768609E-05   9.625638E-04  -1.037513E-02
   x:   7   1.267042E-06  -2.581701E-07   5.155989E-08   1.090895E-06  -1.330432E-05  -3.261456E-06  -2.968889E-05   0.279876    
   x:   8   3.515774E-06   7.062816E-06   1.415248E-05  -7.752196E-06  -1.730855E-05  -3.796370E-05  -3.178497E-05  -0.942122    
   x:   9   3.643133E-03   4.253300E-02   6.612120E-02  -0.233542      -0.966347       6.519091E-02   3.445648E-02   2.001258E-11
   x:  10   1.272570E-03   9.133236E-03  -1.980846E-03  -2.339431E-02  -6.160611E-02  -0.997762      -6.196867E-03  -1.892868E-12
   x:  11  -2.482101E-02  -0.990662       0.128476      -1.710014E-02  -3.163874E-02  -6.931240E-03  -1.115786E-02  -8.963626E-12
   x:  12  -0.999663       2.407163E-02  -8.631480E-03   2.909877E-04  -3.523970E-03  -8.094756E-04  -2.782026E-03   7.820455E-12
   x:  13  -5.540735E-03   0.122669       0.975939       0.177502       2.908679E-02  -6.829906E-03   8.118306E-03   6.227132E-12
   x:  14  -3.145508E-03  -1.319451E-02  -7.684479E-03  -3.302943E-04   3.402427E-02  -8.408674E-03   0.999263       6.593394E-13
   x:  15   1.809911E-03  -2.990771E-02  -0.162875       0.955569      -0.243641      -7.352579E-03   6.908045E-03   3.356191E-11

                v:   9         v:  10         v:  11         v:  12         v:  13         v:  14         v:  15

   eig(s)    1.00000        1.00000        1.00000        1.00000        1.00000        1.00000        1.00000    
 
   x:   1  -4.673626E-02  -0.294872       0.756174      -0.499133      -2.828622E-02   0.181290       0.236319    
   x:   2  -6.152057E-02  -0.177223      -0.480614      -0.718193       6.269020E-02  -0.433992       0.135536    
   x:   3  -0.141672      -0.895472      -0.125395       0.340050      -0.120322      -8.366912E-02   1.014792E-02
   x:   4  -5.742103E-02  -0.180518      -7.797199E-02  -0.211013       0.563299       0.391576      -0.665478    
   x:   5  -2.207993E-02   3.011101E-02   0.413732       0.162823       0.306125      -0.782126      -0.307300    
   x:   6  -1.348796E-04   1.652856E-02   6.027374E-02  -0.191762      -0.753865      -5.741731E-02  -0.622577    
   x:   7  -0.939052       0.197896       4.499703E-05  -4.401021E-03  -1.494536E-02   8.118404E-03   1.950139E-02
   x:   8  -0.297250      -8.271858E-02   2.450287E-02   0.107707      -3.517831E-02   5.802854E-02   2.001071E-02
   x:   9   4.115756E-06  -3.425956E-06  -2.506162E-06  -1.402734E-06   4.452308E-04   1.562173E-05  -1.533917E-05
   x:  10  -3.456940E-07  -9.706412E-07   2.710150E-06   2.006374E-06   2.430596E-05  -7.018099E-04   8.746110E-05
   x:  11  -6.698835E-07  -1.564025E-05   3.794132E-05  -2.079291E-05  -4.237322E-06   4.585361E-05   4.872466E-05
   x:  12   2.214216E-06   1.630013E-05   3.128702E-05   3.739184E-05  -4.455999E-06   5.056763E-06   2.557703E-06
   x:  13   1.158009E-06   1.025085E-05   4.243759E-06  -8.997853E-05  -1.330228E-05  -2.623359E-05  -4.731602E-05
   x:  14   9.507457E-08   8.298012E-07  -1.297728E-06  -2.324236E-06   3.557653E-06   3.821214E-05   1.525606E-03
   x:  15   6.910540E-06  -6.476175E-06  -5.411959E-06   2.049295E-06  -2.607560E-04  -3.771650E-05   4.354318E-06
 bummer (warning):overlap matrix: # small eigenvalues=                      1

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   2.480488E-07   0.957411       0.146899      -3.911523E-02   2.786246E-07   3.202464E-06  -8.733477E-03   0.107800    
 ref    2   3.950951E-07  -0.113772       0.742227       2.710151E-03   0.364467       0.493082       1.408683E-03  -2.561525E-02
 ref    3  -2.363215E-07  -1.753059E-02  -1.110925E-03  -0.433160      -0.697245       0.515460       1.033508E-02   4.080127E-02
 ref    4  -0.954960       2.555166E-08  -2.039040E-06  -3.917018E-07   2.101396E-06  -6.050445E-06  -6.494547E-02  -2.820548E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1  -1.232888E-02   0.111449      -1.525610E-02   5.252959E-03   7.143470E-02  -1.264906E-02   1.040957E-02
 ref    2  -3.475442E-03   5.904455E-03   3.631268E-03   8.067177E-03  -1.021654E-02  -1.959623E-02  -8.844777E-02
 ref    3   2.238796E-02  -5.127532E-02   8.071458E-03  -6.950473E-03   4.090407E-02  -0.109774       2.350821E-02
 ref    4   1.545847E-02  -4.118090E-02  -0.109111       5.268377E-02   4.319376E-02  -3.576800E-02   9.436373E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.911948       0.929888       0.572481       0.189165       0.618987       0.508829       4.402987E-03   1.394959E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1   9.042647E-04   1.678071E-02   1.221627E-02   2.916561E-03   8.746137E-03   1.387368E-02   8.573048E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00000025     0.95741149     0.14689883    -0.03911523     0.00000028     0.00000320    -0.00873348     0.10779958
 ref:   2     0.00000040    -0.11377180     0.74222673     0.00271015     0.36446735     0.49308199     0.00140868    -0.02561525
 ref:   3    -0.00000024    -0.01753059    -0.00111093    -0.43316042    -0.69724513     0.51546001     0.01033508     0.04080127
 ref:   4    -0.95495983     0.00000003    -0.00000204    -0.00000039     0.00000210    -0.00000605    -0.06494547    -0.00282055

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15
 ref:   1    -0.01232888     0.11144874    -0.01525610     0.00525296     0.07143470    -0.01264906     0.01040957
 ref:   2    -0.00347544     0.00590445     0.00363127     0.00806718    -0.01021654    -0.01959623    -0.08844777
 ref:   3     0.02238796    -0.05127532     0.00807146    -0.00695047     0.04090407    -0.10977392     0.02350821
 ref:   4     0.01545847    -0.04118090    -0.10911089     0.05268377     0.04319376    -0.03576800     0.00943637

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 59  1   -196.4879585129  1.4829E-11  0.0000E+00  1.5734E-04  1.0000E-04   
 mr-sdci # 59  2   -196.4725827950  1.5599E-10  0.0000E+00  1.1210E-04  1.0000E-04   
 mr-sdci # 59  3   -196.4725827821  1.9632E-11  0.0000E+00  2.0430E-04  1.0000E-04   
 mr-sdci # 59  4   -196.4725818190  8.9856E-08  0.0000E+00  1.6488E-03  1.0000E-04   
 mr-sdci # 59  5   -196.4644811479  1.5109E-07  0.0000E+00  1.8816E-04  1.0000E-04   
 mr-sdci # 59  6   -196.4644806849  5.1486E-11  3.1925E-07  8.3435E-04  1.0000E-04   
 mr-sdci # 59  7   -195.8195450434  6.7823E-05  0.0000E+00  2.3607E-01  1.0000E-04   
 mr-sdci # 59  8   -195.7719299201  2.4261E-04  0.0000E+00  1.9985E-01  1.0000E-04   
 mr-sdci # 59  9   -195.5865437403  1.3275E+00  0.0000E+00  7.7668E-01  1.0000E-04   
 mr-sdci # 59 10   -194.2589523469  1.6630E-02  0.0000E+00  2.9650E+00  1.0000E-04   
 mr-sdci # 59 11   -194.2291705854  4.1848E-01  0.0000E+00  2.5627E+00  1.0000E-04   
 mr-sdci # 59 12   -193.4430368970  7.1879E-02  0.0000E+00  3.0058E+00  1.0000E-04   
 mr-sdci # 59 13   -193.1345923968  1.4187E-01  0.0000E+00  3.8334E+00  1.0000E-04   
 mr-sdci # 59 14   -192.0261162642  1.7271E-01  0.0000E+00  4.4928E+00  1.0000E-04   
 mr-sdci # 59 15   -189.9175687106 -9.5799E-01  0.0000E+00  4.6340E+00  1.0000E-04   
 
 root number  6 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.006000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  60

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59964369
   ht   2     0.00000000   -50.58426799
   ht   3     0.00000000     0.00000000   -50.58426792
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426118
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616592
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616524
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.92195632
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.87678333
   ht   9     0.00069647    -0.00109357     0.00266642    -0.01355630    -0.00644039     0.01653488     0.00055955     0.00080625
   ht  10     0.00535847    -0.01595466    -0.00293105     0.01617569    -0.02685060     0.00165569     0.00020544     0.00195968
   ht  11    -0.00322110     0.00071074     0.00002833     0.00063544     0.00203710     0.00120157     0.00003265    -0.00019891
   ht  12    -0.00008765     0.00237969     0.00030245     0.00079067    -0.00067618     0.00017899    -0.00005630    -0.00015716
   ht  13    -0.00149179    -0.00327951     0.00187967    -0.00154076    -0.00092931    -0.00296740    -0.00001919     0.00064246
   ht  14    -0.01858452    -0.00974020    -0.00052972     0.05046871     0.02521266     0.04826494    -0.00142582    -0.00169913
   ht  15     0.00009924    -0.00012463    -0.00206235     0.00828510     0.00272687    -0.00987259     0.00020878    -0.00028373
   ht  16    -0.00318229     0.00552128     0.00050965    -0.00741431     0.01434406    -0.00438884    -0.00026721    -0.00134138

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15         ht  16
   ht   9    -0.00001892
   ht  10     0.00000118    -0.00004887
   ht  11    -0.00000011     0.00000121    -0.00000133
   ht  12     0.00000009     0.00000014    -0.00000001    -0.00000040
   ht  13     0.00000049    -0.00000081    -0.00000002     0.00000018    -0.00000251
   ht  14    -0.00000344    -0.00000470    -0.00000222     0.00000020     0.00000248    -0.00024814
   ht  15     0.00000414    -0.00000128     0.00000002    -0.00000005    -0.00000034    -0.00000127    -0.00000679
   ht  16     0.00000164     0.00000860    -0.00000051     0.00000008    -0.00000003     0.00000779    -0.00000083    -0.00001564

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7         v:   8

   eig(s)   5.361657E-09   1.963186E-08   4.011876E-08   6.065561E-08   1.764837E-07   1.908365E-07   5.117005E-07   2.778370E-06
 
   x:   1  -5.339123E-06  -6.779032E-05   3.383802E-05  -2.143902E-06  -9.404366E-05   6.975964E-06   8.530347E-05   3.650260E-04
   x:   2   4.766452E-05   2.542998E-05   6.058425E-05  -1.202254E-05   1.636390E-04   1.214760E-05  -2.898984E-04   1.945772E-04
   x:   3   6.230246E-06  -6.577949E-06  -4.665801E-05  -4.445810E-05   7.970523E-06   4.047464E-05  -5.845045E-05   8.505312E-06
   x:   4   1.825425E-05   3.727458E-05   8.132524E-05   2.170080E-04  -1.034892E-04  -2.696596E-04   3.175111E-04  -9.898265E-04
   x:   5  -7.986581E-06   6.946793E-05   3.214285E-05   6.334783E-05   4.207182E-04  -4.822591E-05  -4.505492E-04  -4.896901E-04
   x:   6   5.809117E-06   2.280953E-05   8.175674E-06  -2.688196E-04  -1.443518E-04   2.104702E-04   5.792446E-06  -9.644945E-04
   x:   7  -1.302133E-06   8.684076E-08  -4.063962E-08   1.580132E-06  -9.831772E-06   1.110527E-05   2.040201E-06   2.954159E-05
   x:   8  -3.685785E-06  -7.820173E-06  -1.410996E-05  -5.414888E-06  -3.792839E-05   7.559088E-06   3.201261E-05   3.110576E-05
   x:   9  -3.572931E-03  -4.162154E-02  -6.625098E-02  -0.231946      -0.255990       0.932441      -6.245554E-02  -3.447275E-02
   x:  10  -1.918020E-03  -1.224158E-02   2.168607E-03  -1.323990E-02  -0.200213       7.205962E-03   0.979531       6.907342E-03
   x:  11   2.620384E-02   0.990378      -0.128200      -1.010896E-02  -3.469020E-02   2.392254E-02   5.230685E-03   1.112186E-02
   x:  12   0.999613      -2.559198E-02   8.662888E-03   1.266891E-03  -5.411435E-03   2.268579E-03   4.931238E-04   2.774454E-03
   x:  13   5.262385E-03  -0.123285      -0.975867       0.176906       2.036218E-02  -2.502549E-02   7.424357E-03  -8.094843E-03
   x:  14   3.285314E-03   1.377428E-02   7.652160E-03  -1.968790E-03   3.143687E-02  -2.669414E-02   1.380096E-02  -0.998921    
   x:  15  -2.538548E-03   2.494773E-02   0.163461       0.953760       6.530675E-04   0.250643       1.117415E-02  -6.814917E-03
   x:  16   5.094579E-03   2.453960E-02  -1.485015E-03  -7.045628E-02   0.944324       0.256443       0.190315       2.597763E-02

                v:   9         v:  10         v:  11         v:  12         v:  13         v:  14         v:  15         v:  16

   eig(s)    1.00000        1.00000        1.00000        1.00000        1.00000        1.00000        1.00000        1.00000    
 
   x:   1   4.261645E-02   4.305565E-03  -0.409202      -0.734492       0.445687       1.758402E-02  -0.194299       0.233540    
   x:   2  -4.532087E-02   1.742872E-02  -3.469739E-02   0.432230       0.789812      -1.872719E-02   0.407395       0.139610    
   x:   3   0.240775      -6.160147E-02  -0.863444       0.336441      -0.241284       0.128133       6.896147E-02   1.060882E-02
   x:   4   2.495812E-02   1.577145E-02  -0.147513       8.835953E-02   0.229994      -0.589646      -0.347828      -0.669325    
   x:   5  -3.515557E-02   5.497538E-02  -8.653632E-02  -0.376219      -0.175709      -0.248468       0.816280      -0.296293    
   x:   6  -1.003407E-02   3.301183E-03   1.933297E-02  -8.656535E-02   0.174357       0.756490       2.809974E-03  -0.623967    
   x:   7   0.499058       0.862479       7.854275E-02   6.883173E-03   1.191400E-02   1.483423E-02  -1.101595E-02   1.928066E-02
   x:   8  -0.828939       0.498730      -0.223657       6.013852E-02  -7.091348E-02   3.170369E-02  -6.398952E-02   1.902928E-02
   x:   9  -9.946498E-07  -4.175451E-06  -3.742526E-06   4.741838E-06   6.935008E-06  -4.450647E-04   1.881313E-05  -1.439375E-05
   x:  10  -6.537270E-07   2.225136E-06  -8.573402E-06   6.054524E-06   1.814408E-05   3.023258E-05   7.001226E-04   9.622567E-05
   x:  11   7.738224E-07  -1.299609E-06  -2.116340E-05  -3.668729E-05   1.716155E-05   8.666836E-07  -4.694864E-05   4.812459E-05
   x:  12  -2.215264E-06   2.656837E-06   6.520608E-06  -2.951669E-05  -4.172199E-05   3.484419E-06  -3.797023E-06   2.567219E-06
   x:  13  -3.086323E-07  -7.986094E-07   1.571042E-05  -1.619921E-05   8.822916E-05   1.600196E-05   2.350345E-05  -4.713491E-05
   x:  14  -6.440397E-08   3.353160E-08   1.021220E-06   1.116749E-06   3.024670E-06  -4.013368E-06  -5.711302E-05   1.525013E-03
   x:  15  -1.254321E-06  -8.106434E-06  -3.469299E-06   5.447588E-06  -3.376303E-06   2.629038E-04   1.738880E-05   4.149294E-06
   x:  16  -1.551573E-06   4.008775E-06  -1.421620E-05   1.702644E-05   4.109109E-05   5.247929E-05  -3.413997E-04  -6.825942E-05
 bummer (warning):overlap matrix: # small eigenvalues=                      1

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   2.960068E-07  -0.957619      -0.145716       3.846242E-02  -2.272441E-07   6.618726E-07   8.686287E-03   0.104382    
 ref    2   1.285357E-06   0.112818      -0.742368      -3.615535E-03   0.596103      -0.143432       3.154353E-03  -3.682522E-02
 ref    3  -1.264603E-07   1.732165E-02   5.321364E-04   0.433173      -0.202844      -0.843017      -8.273570E-03   3.380460E-02
 ref    4  -0.954960      -5.150287E-08   1.964494E-06   2.445907E-06   2.728588E-06   1.023452E-07   6.443282E-02  -6.117879E-04

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1  -2.874781E-02  -1.092113E-02  -9.329023E-02  -7.565086E-02  -6.443680E-03   2.390168E-02   5.565259E-02  -2.826420E-02
 ref    2  -5.685410E-02   3.789729E-03  -1.024066E-02  -4.607475E-03  -8.122687E-03  -3.685595E-02  -3.049666E-02   0.106316    
 ref    3  -4.064123E-02   2.702451E-02   3.511984E-02   2.484775E-02   6.235151E-03  -6.998786E-02   8.626619E-02  -4.911750E-02
 ref    4   9.883156E-03   1.503088E-02   8.794685E-02  -7.736140E-02  -5.332764E-02  -3.080231E-03   6.732441E-02  -3.133855E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.911949       0.930061       0.572344       0.189131       0.396485       0.731250       4.305441E-03   1.339481E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   5.808211E-03   1.089884E-03   1.777599E-02   1.234648E-02   2.990214E-03   6.837439E-03   1.600169E-02   1.549666E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15        ev   16

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00000030    -0.95761867    -0.14571571     0.03846242    -0.00000023     0.00000066     0.00868629     0.10438195
 ref:   2     0.00000129     0.11281765    -0.74236829    -0.00361554     0.59610325    -0.14343242     0.00315435    -0.03682522
 ref:   3    -0.00000013     0.01732165     0.00053214     0.43317258    -0.20284400    -0.84301680    -0.00827357     0.03380460
 ref:   4    -0.95496000    -0.00000005     0.00000196     0.00000245     0.00000273     0.00000010     0.06443282    -0.00061179

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1    -0.02874781    -0.01092113    -0.09329023    -0.07565086    -0.00644368     0.02390168     0.05565259    -0.02826420
 ref:   2    -0.05685410     0.00378973    -0.01024066    -0.00460748    -0.00812269    -0.03685595    -0.03049666     0.10631634
 ref:   3    -0.04064123     0.02702451     0.03511984     0.02484775     0.00623515    -0.06998786     0.08626619    -0.04911750
 ref:   4     0.00988316     0.01503088     0.08794685    -0.07736140    -0.05332764    -0.00308023     0.06732441    -0.03133855

 trial vector basis is being transformed.  new dimension:   8

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 60  1   -196.4879585131  1.9087E-10  9.6389E-09  1.5680E-04  1.0000E-04   
 mr-sdci # 60  2   -196.4725827950  6.3665E-12  0.0000E+00  1.1194E-04  1.0000E-04   
 mr-sdci # 60  3   -196.4725827822  4.3073E-11  0.0000E+00  2.0430E-04  1.0000E-04   
 mr-sdci # 60  4   -196.4725818533  3.4248E-08  0.0000E+00  1.6598E-03  1.0000E-04   
 mr-sdci # 60  5   -196.4644811507  2.7664E-09  0.0000E+00  1.7494E-04  1.0000E-04   
 mr-sdci # 60  6   -196.4644811440  4.5918E-07  0.0000E+00  2.4198E-04  1.0000E-04   
 mr-sdci # 60  7   -195.8198753996  3.3036E-04  0.0000E+00  2.3622E-01  1.0000E-04   
 mr-sdci # 60  8   -195.7726913284  7.6141E-04  0.0000E+00  2.0584E-01  1.0000E-04   
 mr-sdci # 60  9   -195.7505384376  1.6399E-01  0.0000E+00  3.9785E-01  1.0000E-04   
 mr-sdci # 60 10   -195.5841644282  1.3252E+00  0.0000E+00  7.7393E-01  1.0000E-04   
 mr-sdci # 60 11   -194.2427787481  1.3608E-02  0.0000E+00  2.8045E+00  1.0000E-04   
 mr-sdci # 60 12   -194.2175262023  7.7449E-01  0.0000E+00  2.7885E+00  1.0000E-04   
 mr-sdci # 60 13   -193.4429566791  3.0836E-01  0.0000E+00  3.0050E+00  1.0000E-04   
 mr-sdci # 60 14   -192.2366014537  2.1049E-01  0.0000E+00  4.4897E+00  1.0000E-04   
 mr-sdci # 60 15   -190.5831295830  6.6556E-01  0.0000E+00  3.8117E+00  1.0000E-04   
 mr-sdci # 60 16   -189.8378174219 -1.2098E-02  0.0000E+00  4.6077E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.014000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  61

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59964372
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426799
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426706
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616635
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616635
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.93156060
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88437653
   ht   9    -0.00315058     0.00013820     0.00001352     0.00045880     0.00025583    -0.00024991     0.00017439    -0.00000877

                ht   9
   ht   9    -0.00000047

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7         v:   8

   eig(s)   5.487585E-09    1.00000        1.00000        1.00000        1.00000        1.00000        1.00000        1.00000    
 
   x:   1  -6.226469E-05  -9.593194E-02  -6.408240E-02   9.943031E-02   0.105388       4.265687E-02   4.345508E-02  -3.462997E-04
   x:   2   2.732135E-06  -0.132253       4.410768E-03  -0.114188      -6.666381E-03   0.983542      -2.219719E-03  -1.394986E-02
   x:   3   2.672766E-07   0.724221       5.417557E-04   0.667076      -9.628949E-04   0.174619      -8.091064E-04  -1.117516E-03
   x:   4   9.069986E-06  -0.668474      -2.013538E-03   0.727898       4.498971E-02  -1.165024E-02  -1.731527E-02  -2.075917E-02
   x:   5   5.058251E-06   4.257437E-02  -0.133097      -4.612833E-02   0.895862       2.300713E-04  -0.372605      -0.174909    
   x:   6  -4.941253E-06  -1.441235E-03   0.191808       2.306970E-03  -0.353132      -1.065610E-02  -0.620863      -0.668474    
   x:   7   3.653691E-06   9.268510E-03  -0.681255      -8.602858E-03  -4.050336E-02  -6.799783E-03   0.434790      -0.584543    
   x:   8  -1.825235E-07  -7.171880E-03  -0.690838       6.460436E-03  -0.240651       6.196125E-03  -0.533347       0.424537    
   x:   9    1.00000        0.00000       4.847506E-12    0.00000       1.775438E-12    0.00000       3.755849E-12  -2.951951E-12

                v:   9

   eig(s)    1.00000    
 
   x:   1  -0.980811    
   x:   2   4.303736E-02
   x:   3   4.210215E-03
   x:   4   0.142873    
   x:   5   7.967898E-02
   x:   6  -7.783599E-02
   x:   7   5.755396E-02
   x:   8  -2.875272E-03
   x:   9  -6.348288E-05
 bummer (warning):overlap matrix: # small eigenvalues=                      1

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -3.896055E-07   0.957585       0.145921      -3.853016E-02   2.032880E-07   6.908253E-07   8.290307E-03  -0.104417    
 ref    2  -6.390093E-07  -0.112992       0.742344       3.246473E-03  -0.593230      -0.154887       2.636633E-03   3.678499E-02
 ref    3   6.397153E-07  -1.731937E-02  -7.511931E-04  -0.433172       0.219044      -0.838954      -8.894738E-03  -3.378571E-02
 ref    4   0.954969       3.328159E-08  -1.289946E-07   4.106853E-07  -1.906220E-06  -1.772803E-06   5.438495E-02  -5.591665E-05

              v      9
 ref    1   1.167840E-03
 ref    2  -1.348414E-02
 ref    3  -9.015910E-03
 ref    4  -0.198871    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.911965       0.930035       0.572368       0.189133       0.399902       0.727833       3.112520E-03   1.339749E-02

              v      9
 ref    1   3.981420E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00000039     0.95758464     0.14592129    -0.03853016     0.00000020     0.00000069     0.00829031    -0.10441682
 ref:   2    -0.00000064    -0.11299188     0.74234363     0.00324647    -0.59322999    -0.15488741     0.00263663     0.03678499
 ref:   3     0.00000064    -0.01731937    -0.00075119    -0.43317212     0.21904372    -0.83895359    -0.00889474    -0.03378571
 ref:   4     0.95496869     0.00000003    -0.00000013     0.00000041    -0.00000191    -0.00000177     0.05438495    -0.00005592

                ci   9
 ref:   1     0.00116784
 ref:   2    -0.01348414
 ref:   3    -0.00901591
 ref:   4    -0.19887114

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 61  1   -196.4879585192  6.1028E-09  0.0000E+00  1.0772E-04  1.0000E-04   
 mr-sdci # 61  2   -196.4725827950  2.1316E-14  4.1727E-09  1.1192E-04  1.0000E-04   
 mr-sdci # 61  3   -196.4725827825  2.9645E-10  0.0000E+00  1.9345E-04  1.0000E-04   
 mr-sdci # 61  4   -196.4725818540  7.1748E-10  0.0000E+00  1.6604E-03  1.0000E-04   
 mr-sdci # 61  5   -196.4644811507  5.5309E-11  0.0000E+00  1.7713E-04  1.0000E-04   
 mr-sdci # 61  6   -196.4644811443  2.8521E-10  0.0000E+00  2.3142E-04  1.0000E-04   
 mr-sdci # 61  7   -195.8254113342  5.5359E-03  0.0000E+00  2.3085E-01  1.0000E-04   
 mr-sdci # 61  8   -195.7727000939  8.7655E-06  0.0000E+00  2.0555E-01  1.0000E-04   
 mr-sdci # 61  9   -193.6905110446 -2.0600E+00  0.0000E+00  3.6699E+00  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.007000
time for eigenvalue solver             0.001000
time for vector access                 0.000000

          starting ci iteration  62

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59964372
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426799
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426706
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616635
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616635
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.93156060
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88437653
   ht   9    -0.00315058     0.00013820     0.00001352     0.00045880     0.00025583    -0.00024991     0.00017439    -0.00000877
   ht  10    -0.00002617    -0.00174897    -0.00038908     0.00072321     0.00060512     0.00007061     0.00002290     0.00026367

                ht   9         ht  10
   ht   9    -0.00000047
   ht  10    -0.00000001    -0.00000017

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7         v:   8

   eig(s)   1.919460E-09   5.487629E-09    1.00000        1.00000        1.00000        1.00000        1.00000        1.00000    
 
   x:   1  -7.358135E-07   6.226249E-05  -1.215148E-02  -2.427235E-02  -2.458608E-02  -7.767646E-02   0.154244      -7.585711E-02
   x:   2  -3.456569E-05  -2.853473E-06  -0.190856      -0.254820       0.173562      -0.208445       0.270200      -0.111601    
   x:   3  -7.690681E-06  -2.942715E-07  -8.083874E-02   0.943199       4.889427E-03  -1.763807E-02   0.244115       8.650125E-02
   x:   4   1.432895E-05  -9.019749E-06  -0.542871      -7.937098E-02  -0.349040      -0.502920       0.410018      -0.121805    
   x:   5   1.198220E-05  -5.016227E-06   0.118242      -0.104378       0.642328       0.193697       0.656732       4.051200E-02
   x:   6   1.378767E-06   4.946123E-06  -0.802891       1.610094E-02   0.313148       0.405263      -0.288921       4.254764E-02
   x:   7   4.704407E-07  -3.652062E-06   4.178641E-02   7.850089E-02  -0.120053       0.330765       7.117689E-02  -0.927265    
   x:   8   5.192992E-06   2.007513E-07   4.267266E-02   0.145732       0.567767      -0.621381      -0.394531      -0.310162    
   x:   9   3.509839E-03  -0.999994       4.898890E-13   9.125310E-13  -1.707000E-12   3.868558E-12   1.298937E-12  -1.161479E-11
   x:  10   0.999994       3.509839E-03   2.939847E-13   4.800943E-13    0.00000       2.584094E-13    0.00000       1.162071E-12

                v:   9         v:  10

   eig(s)    1.00000        1.00000    
 
   x:   1   6.397555E-02   0.979279    
   x:   2  -0.859856      -1.589037E-02
   x:   3  -0.190864       1.818313E-03
   x:   4   0.342977      -0.153781    
   x:   5   0.290141      -8.888619E-02
   x:   6   4.067783E-02   7.658956E-02
   x:   7   6.829884E-03  -5.779867E-02
   x:   8   0.128880      -1.195560E-03
   x:   9   2.004839E-06   6.345122E-05
   x:  10  -4.026919E-05   3.158979E-06
 bummer (warning):overlap matrix: # small eigenvalues=                      2

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -4.103803E-07   0.961754      -0.115846      -3.671822E-02  -6.273824E-07  -3.604480E-07  -8.497487E-03  -0.107913    
 ref    2  -6.473732E-07  -8.967667E-02  -0.745519       3.278824E-03  -0.598041      -0.135126      -2.616685E-03   3.498604E-02
 ref    3   6.393359E-07  -1.653309E-02   9.324912E-05  -0.433203       0.191096      -0.845757       8.849649E-03  -3.367521E-02
 ref    4   0.954969       1.034824E-08   1.326353E-07   4.220569E-07  -1.968373E-06  -1.713984E-06  -5.438485E-02   2.940887E-05

              v      9       v     10
 ref    1   5.358396E-03  -8.551607E-02
 ref    2  -1.116458E-02  -4.670134E-02
 ref    3  -9.202479E-03   3.185613E-03
 ref    4  -0.198512      -1.218023E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.911965       0.933287       0.569219       0.189024       0.394171       0.733565       3.115083E-03   1.400324E-02

              v      9       v     10
 ref    1   3.964501E-02   9.652519E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00000041     0.96175427    -0.11584601    -0.03671822    -0.00000063    -0.00000036    -0.00849749    -0.10791291
 ref:   2    -0.00000065    -0.08967667    -0.74551934     0.00327882    -0.59804123    -0.13512603    -0.00261669     0.03498604
 ref:   3     0.00000064    -0.01653309     0.00009325    -0.43320339     0.19109638    -0.84575736     0.00884965    -0.03367521
 ref:   4     0.95496869     0.00000001     0.00000013     0.00000042    -0.00000197    -0.00000171    -0.05438485     0.00002941

                ci   9         ci  10
 ref:   1     0.00535840    -0.08551607
 ref:   2    -0.01116458    -0.04670134
 ref:   3    -0.00920248     0.00318561
 ref:   4    -0.19851188    -0.01218023

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 62  1   -196.4879585192  9.9476E-14  0.0000E+00  1.0769E-04  1.0000E-04   
 mr-sdci # 62  2   -196.4725827977  2.7626E-09  0.0000E+00  5.5385E-05  1.0000E-04   
 mr-sdci # 62  3   -196.4725827826  6.7338E-11  1.2616E-08  1.8660E-04  1.0000E-04   
 mr-sdci # 62  4   -196.4725818551  1.0941E-09  0.0000E+00  1.6584E-03  1.0000E-04   
 mr-sdci # 62  5   -196.4644811509  1.6664E-10  0.0000E+00  1.8045E-04  1.0000E-04   
 mr-sdci # 62  6   -196.4644811446  2.6704E-10  0.0000E+00  2.2667E-04  1.0000E-04   
 mr-sdci # 62  7   -195.8254125291  1.1949E-06  0.0000E+00  2.3079E-01  1.0000E-04   
 mr-sdci # 62  8   -195.7768819502  4.1819E-03  0.0000E+00  1.9903E-01  1.0000E-04   
 mr-sdci # 62  9   -193.6918521110  1.3411E-03  0.0000E+00  3.6618E+00  1.0000E-04   
 mr-sdci # 62 10   -193.1459869914 -2.4382E+00  0.0000E+00  3.4651E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.005000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  63

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59964372
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426799
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426706
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616635
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616635
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.93156060
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88437653
   ht   9    -0.00315058     0.00013820     0.00001352     0.00045880     0.00025583    -0.00024991     0.00017439    -0.00000877
   ht  10    -0.00002617    -0.00174897    -0.00038908     0.00072321     0.00060512     0.00007061     0.00002290     0.00026367
   ht  11    -0.00077258     0.00130541     0.00004934    -0.00111388    -0.00005756     0.00117847     0.00005313    -0.00010662

                ht   9         ht  10         ht  11
   ht   9    -0.00000047
   ht  10    -0.00000001    -0.00000017
   ht  11    -0.00000003     0.00000004    -0.00000042

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7         v:   8

   eig(s)   1.886548E-09   5.469025E-09   6.640110E-09    1.00000        1.00000        1.00000        1.00000        1.00000    
 
   x:   1   7.527032E-07  -6.369029E-05  -7.298509E-06  -2.717184E-02  -6.000829E-02  -9.071016E-02  -0.105537      -2.299559E-02
   x:   2  -3.660761E-05   5.588037E-06   2.230616E-05  -0.176159       1.229500E-02  -0.182168      -0.423867      -0.126190    
   x:   3  -7.746226E-06   3.074615E-07   2.942459E-07   5.486232E-02  -3.689930E-03  -0.614203       0.461486       0.586748    
   x:   4   1.608521E-05   6.384037E-06  -2.172741E-05  -0.527479       2.108609E-02  -0.487390      -0.129862      -0.366815    
   x:   5   1.201821E-05   4.999823E-06  -7.715151E-07  -3.001534E-02  -0.315160      -0.163729      -0.677510       0.519614    
   x:   6  -5.530522E-07  -1.964069E-06   2.377254E-05  -0.311656       4.800162E-02  -0.310970       0.199061      -0.207220    
   x:   7   3.662040E-07   3.764960E-06   6.461781E-07   0.616957      -0.544036      -0.363505       3.873775E-05  -0.432801    
   x:   8   5.354043E-06  -3.954283E-07  -1.662368E-06   0.456421       0.773424      -0.296896      -0.284112      -6.681868E-02
   x:   9   8.980425E-05   0.992044      -0.125891       2.727467E-12   8.359020E-13  -5.909440E-13  -6.688946E-13  -3.821227E-14
   x:  10   0.996514       1.041362E-02   8.277178E-02   2.842330E-12  -7.426667E-12   7.476726E-13  -8.764816E-13   8.376131E-13
   x:  11  -8.342423E-02   0.125460       0.988585        0.00000      -4.796522E-12    0.00000        0.00000        0.00000    

                v:   9         v:  10         v:  11

   eig(s)    1.00000        1.00000        1.00000    
 
   x:   1   3.612136E-02  -0.192360      -0.968224    
   x:   2   0.362425      -0.744907       0.231961    
   x:   3   0.223856      -0.109054       2.201189E-02
   x:   4   0.270411       0.509069      -9.024952E-03
   x:   5  -0.320469       0.197823       4.596460E-02
   x:   6  -0.793551      -0.298803       4.788804E-02
   x:   7  -2.174979E-02   1.449245E-02   5.704406E-02
   x:   8  -0.105898       9.186684E-02  -2.257548E-02
   x:   9  -3.493548E-06  -1.704343E-05  -6.105238E-05
   x:  10   1.590750E-05  -3.640556E-05   7.292003E-06
   x:  11   1.485737E-05   3.497114E-05  -2.216321E-05
 bummer (warning):overlap matrix: # small eigenvalues=                      3

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   4.044551E-07   0.954990      -0.162455      -3.662450E-02  -6.001196E-07   4.265411E-07  -8.417558E-03  -0.107898    
 ref    2   6.526325E-07  -0.125780      -0.740280       3.986686E-03  -0.596070       0.143571      -2.651179E-03   3.500774E-02
 ref    3  -7.942687E-07  -1.653586E-02   4.918229E-04  -0.433203       0.203040       0.842971       8.689009E-03  -3.292249E-02
 ref    4  -0.954969      -5.251713E-08  -9.081829E-07   3.393660E-07  -2.001580E-06   1.592321E-06  -5.435670E-02  -1.221365E-04

              v      9       v     10       v     11
 ref    1  -1.576057E-02   7.530707E-03   8.686581E-02
 ref    2  -6.207666E-03   1.783862E-02   4.584820E-02
 ref    3   6.695787E-02  -2.963537E-03   2.815718E-02
 ref    4   1.577665E-02   0.198884      -2.168733E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.911965       0.928101       0.574407       0.189022       0.396525       0.731212       3.108034E-03   1.395137E-02

              v      9       v     10       v     11
 ref    1   5.019190E-03   3.993859E-02   1.091089E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00000040     0.95499046    -0.16245500    -0.03662450    -0.00000060     0.00000043    -0.00841756    -0.10789773
 ref:   2     0.00000065    -0.12577960    -0.74028022     0.00398669    -0.59607018     0.14357111    -0.00265118     0.03500774
 ref:   3    -0.00000079    -0.01653586     0.00049182    -0.43320259     0.20303952     0.84297074     0.00868901    -0.03292249
 ref:   4    -0.95496866    -0.00000005    -0.00000091     0.00000034    -0.00000200     0.00000159    -0.05435670    -0.00012214

                ci   9         ci  10         ci  11
 ref:   1    -0.01576057     0.00753071     0.08686581
 ref:   2    -0.00620767     0.01783862     0.04584820
 ref:   3     0.06695787    -0.00296354     0.02815718
 ref:   4     0.01577665     0.19888408    -0.02168733

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 63  1   -196.4879585192  1.0118E-11  0.0000E+00  1.0690E-04  1.0000E-04   
 mr-sdci # 63  2   -196.4725827977  1.3266E-11  0.0000E+00  5.4724E-05  1.0000E-04   
 mr-sdci # 63  3   -196.4725827936  1.1086E-08  0.0000E+00  1.1409E-04  1.0000E-04   
 mr-sdci # 63  4   -196.4725818552  7.0386E-11  7.8347E-07  1.6610E-03  1.0000E-04   
 mr-sdci # 63  5   -196.4644811509  3.3232E-11  0.0000E+00  1.7869E-04  1.0000E-04   
 mr-sdci # 63  6   -196.4644811448  2.3245E-10  0.0000E+00  2.2279E-04  1.0000E-04   
 mr-sdci # 63  7   -195.8254224500  9.9209E-06  0.0000E+00  2.3106E-01  1.0000E-04   
 mr-sdci # 63  8   -195.7770337260  1.5178E-04  0.0000E+00  1.9555E-01  1.0000E-04   
 mr-sdci # 63  9   -194.4796688660  7.8782E-01  0.0000E+00  2.1013E+00  1.0000E-04   
 mr-sdci # 63 10   -193.6594248395  5.1344E-01  0.0000E+00  3.7324E+00  1.0000E-04   
 mr-sdci # 63 11   -192.8327619642 -1.4100E+00  0.0000E+00  3.8434E+00  1.0000E-04   
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  64

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59964372
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426799
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426706
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616635
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616635
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.93156060
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88437653
   ht   9    -0.00315058     0.00013820     0.00001352     0.00045880     0.00025583    -0.00024991     0.00017439    -0.00000877
   ht  10    -0.00002617    -0.00174897    -0.00038908     0.00072321     0.00060512     0.00007061     0.00002290     0.00026367
   ht  11    -0.00077258     0.00130541     0.00004934    -0.00111388    -0.00005756     0.00117847     0.00005313    -0.00010662
   ht  12     0.00043160     0.00700107    -0.00407779     0.01361572    -0.00337880    -0.01884908     0.00030118    -0.00014368

                ht   9         ht  10         ht  11         ht  12
   ht   9    -0.00000047
   ht  10    -0.00000001    -0.00000017
   ht  11    -0.00000003     0.00000004    -0.00000042
   ht  12    -0.00000019     0.00000011     0.00000049    -0.00002912

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7         v:   8

   eig(s)   1.885453E-09   5.468813E-09   6.635316E-09   3.548102E-07    1.00000        1.00000        1.00000        1.00000    
 
   x:   1  -7.797467E-07  -6.370311E-05  -7.274630E-06   8.451732E-06  -0.104777      -6.310558E-02  -2.946213E-02  -1.271525E-03
   x:   2   3.637508E-05   5.494144E-06   2.176996E-05   1.385545E-04  -0.258466      -4.115983E-02   2.200189E-02  -9.740707E-02
   x:   3   7.888454E-06   3.696634E-07   5.892697E-07  -8.059806E-05  -0.548538      -1.799874E-02  -0.120077      -0.190956    
   x:   4  -1.656904E-05   6.157140E-06  -2.272344E-05   2.690473E-04  -0.556842       0.355770      -0.231389       0.125405    
   x:   5  -1.190016E-05   5.052991E-06  -5.221738E-07  -6.682548E-05  -0.453513      -0.642585       0.413171      -0.167626    
   x:   6   1.220928E-06  -1.652397E-06   2.515635E-05  -3.725957E-04  -0.300385       0.349347      -0.219394       0.133765    
   x:   7  -3.759615E-07   3.760877E-06   6.208403E-07   6.003366E-06   5.724289E-02  -0.556976      -0.814965       0.119993    
   x:   8  -5.350595E-06  -3.942342E-07  -1.650389E-06  -2.449179E-06   0.109857       0.149153      -0.218350      -0.936979    
   x:   9  -1.741200E-05   0.991935      -0.126749       3.049515E-04   1.620398E-12  -4.675282E-13  -1.672194E-12   9.663591E-13
   x:  10  -0.996471       1.062004E-02   8.324577E-02  -1.438896E-03   6.401958E-13   3.664252E-12   1.455428E-13  -3.877728E-12
   x:  11   8.391429E-02   0.126301       0.988429       3.913380E-03   5.642821E-14   5.480326E-12  -1.457384E-12  -3.845638E-12
   x:  12  -1.762217E-03  -7.814839E-04  -3.709683E-03   0.999991        0.00000       2.161946E-13    0.00000      -2.539487E-13

                v:   9         v:  10         v:  11         v:  12

   eig(s)    1.00000        1.00000        1.00000        1.00000    
 
   x:   1  -7.049579E-02  -0.203020       0.968350      -1.676164E-02
   x:   2  -0.239852      -0.855803      -0.231822      -0.279034    
   x:   3   0.783556      -8.188018E-02  -2.172190E-02   0.163597    
   x:   4  -0.232863       0.372823       7.776588E-03  -0.548456    
   x:   5  -0.312850       0.253509      -4.587721E-02   0.135484    
   x:   6  -0.371124      -7.776123E-02  -4.642197E-02   0.758790    
   x:   7  -6.606424E-02   1.283747E-02  -5.707003E-02  -1.218447E-02
   x:   8  -0.171298       0.101019       2.252507E-02   4.813004E-03
   x:   9  -1.872682E-06  -1.535743E-05   6.107797E-05   7.758928E-06
   x:  10   6.207804E-06  -3.911002E-05  -7.269881E-06  -3.256467E-06
   x:  11   7.217523E-06   2.957778E-05   2.209963E-05  -2.279468E-05
   x:  12   4.054286E-07   1.355072E-06   1.257472E-08   4.914966E-04
 bummer (warning):overlap matrix: # small eigenvalues=                      3

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   3.886181E-07   0.954223       0.166534      -3.826590E-02   6.639041E-07   1.972264E-07  -8.145607E-03   0.107998    
 ref    2   7.350187E-07  -0.128662       0.739715       1.089816E-02   0.591786       0.160317      -2.894210E-03  -3.561042E-02
 ref    3  -5.588990E-07  -1.794639E-02   3.257366E-03  -0.433178      -0.226722       0.836913       8.433289E-03   3.165770E-02
 ref    4  -0.954969      -4.797049E-08   8.861439E-07   1.687677E-06   1.969179E-06   1.605139E-06  -5.433408E-02   3.343282E-04

              v      9       v     10       v     11       v     12
 ref    1   1.453385E-02  -9.846262E-03   4.823758E-03   8.708014E-02
 ref    2   1.320318E-02  -3.706687E-03  -4.696496E-02   4.457835E-02
 ref    3  -4.920012E-02   3.783461E-02  -0.104340       2.515349E-02
 ref    4  -1.782594E-02  -0.187631      -6.529851E-02  -2.220598E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.911965       0.927417       0.574923       0.189226       0.401613       0.726124       3.098040E-03   1.393405E-02

              v      9       v     10       v     11       v     12
 ref    1   3.123973E-03   3.674769E-02   1.737967E-02   1.069598E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00000039     0.95422292     0.16653442    -0.03826590     0.00000066     0.00000020    -0.00814561     0.10799828
 ref:   2     0.00000074    -0.12866215     0.73971514     0.01089816     0.59178595     0.16031746    -0.00289421    -0.03561042
 ref:   3    -0.00000056    -0.01794639     0.00325737    -0.43317820    -0.22672164     0.83691253     0.00843329     0.03165770
 ref:   4    -0.95496867    -0.00000005     0.00000089     0.00000169     0.00000197     0.00000161    -0.05433408     0.00033433

                ci   9         ci  10         ci  11         ci  12
 ref:   1     0.01453385    -0.00984626     0.00482376     0.08708014
 ref:   2     0.01320318    -0.00370669    -0.04696496     0.04457835
 ref:   3    -0.04920012     0.03783461    -0.10433984     0.02515349
 ref:   4    -0.01782594    -0.18763140    -0.06529851    -0.02220598

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 64  1   -196.4879585192  9.3792E-12  0.0000E+00  1.0676E-04  1.0000E-04   
 mr-sdci # 64  2   -196.4725827977  5.9543E-12  0.0000E+00  5.4497E-05  1.0000E-04   
 mr-sdci # 64  3   -196.4725827937  4.3244E-11  0.0000E+00  1.1005E-04  1.0000E-04   
 mr-sdci # 64  4   -196.4725824441  5.8893E-07  0.0000E+00  8.6524E-04  1.0000E-04   
 mr-sdci # 64  5   -196.4644811510  4.8345E-11  7.3555E-09  1.7620E-04  1.0000E-04   
 mr-sdci # 64  6   -196.4644811454  5.6197E-10  0.0000E+00  2.0256E-04  1.0000E-04   
 mr-sdci # 64  7   -195.8254523396  2.9890E-05  0.0000E+00  2.3134E-01  1.0000E-04   
 mr-sdci # 64  8   -195.7774508490  4.1712E-04  0.0000E+00  1.9582E-01  1.0000E-04   
 mr-sdci # 64  9   -194.5077411239  2.8072E-02  0.0000E+00  1.9533E+00  1.0000E-04   
 mr-sdci # 64 10   -193.6799823636  2.0558E-02  0.0000E+00  3.7029E+00  1.0000E-04   
 mr-sdci # 64 11   -193.4657549367  6.3299E-01  0.0000E+00  3.6795E+00  1.0000E-04   
 mr-sdci # 64 12   -192.8322448017 -1.3853E+00  0.0000E+00  3.8348E+00  1.0000E-04   
 
 root number  5 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  65

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59964372
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426799
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426706
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616635
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616635
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.93156060
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88437653
   ht   9    -0.00315058     0.00013820     0.00001352     0.00045880     0.00025583    -0.00024991     0.00017439    -0.00000877
   ht  10    -0.00002617    -0.00174897    -0.00038908     0.00072321     0.00060512     0.00007061     0.00002290     0.00026367
   ht  11    -0.00077258     0.00130541     0.00004934    -0.00111388    -0.00005756     0.00117847     0.00005313    -0.00010662
   ht  12     0.00043160     0.00700107    -0.00407779     0.01361572    -0.00337880    -0.01884908     0.00030118    -0.00014368
   ht  13     0.00027721    -0.00041376    -0.00101147     0.00012959     0.00018013     0.00091811    -0.00000981    -0.00002047

                ht   9         ht  10         ht  11         ht  12         ht  13
   ht   9    -0.00000047
   ht  10    -0.00000001    -0.00000017
   ht  11    -0.00000003     0.00000004    -0.00000042
   ht  12    -0.00000019     0.00000011     0.00000049    -0.00002912
   ht  13     0.00000000    -0.00000002    -0.00000002     0.00000035    -0.00000020

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7         v:   8

   eig(s)   1.863117E-09   3.211067E-09   5.541797E-09   6.649770E-09   3.548130E-07    1.00000        1.00000        1.00000    
 
   x:   1  -2.436530E-06   1.696572E-05   6.146638E-05  -8.366654E-06   8.436280E-06  -5.299345E-02   1.961536E-02  -8.305280E-02
   x:   2   3.727044E-05  -5.236859E-06  -3.591962E-06   2.141195E-05   1.385772E-04   7.564137E-03   0.135747      -0.141350    
   x:   3   1.040578E-05  -1.879198E-05   3.136008E-06  -7.351594E-07  -8.054065E-05  -0.121897       0.376492      -0.255083    
   x:   4  -1.689749E-05   1.431223E-06  -7.290456E-06  -2.234146E-05   2.690389E-04  -0.163625       0.598010      -0.339373    
   x:   5  -1.216095E-05   9.000349E-07  -5.480868E-06  -2.078688E-07  -6.683538E-05   0.191636      -0.278029      -0.471923    
   x:   6  -8.433825E-07   1.554783E-05  -5.078175E-07   2.619851E-05  -3.726460E-04  -0.139787       0.444347      -0.164214    
   x:   7  -2.919779E-07  -9.263442E-07  -3.644077E-06   6.934812E-07   6.003891E-06  -0.948266      -0.275334       1.953815E-02
   x:   8  -5.274704E-06  -8.726437E-07   4.400805E-07  -1.693083E-06  -2.448124E-06  -5.355619E-03   0.362316       0.736764    
   x:   9   1.359586E-02  -0.167538      -0.980328      -0.103451       3.018529E-04  -1.491297E-12  -1.401674E-11  -8.845314E-12
   x:  10  -0.987615      -0.134448       7.477810E-04   8.085172E-02  -1.437669E-03  -3.820960E-13   3.952924E-11   1.874243E-11
   x:  11   9.073722E-02  -7.227876E-02  -9.076335E-02   0.989085       3.911072E-03   1.566632E-12   3.781204E-11   1.455287E-11
   x:  12  -2.142259E-03   2.921284E-03   1.514907E-04  -3.530264E-03   0.999987       9.441876E-14   1.729962E-12   6.138555E-13
   x:  13  -0.127253       0.973972      -0.175264       6.677650E-02  -2.855599E-03    0.00000      -2.201838E-11  -1.309547E-11

                v:   9         v:  10         v:  11         v:  12         v:  13

   eig(s)    1.00000        1.00000        1.00000        1.00000        1.00000    
 
   x:   1   9.673424E-02  -1.685719E-02  -0.239595      -0.960515       1.648026E-02
   x:   2   0.267425      -0.244105      -0.828414       0.257229       0.279277    
   x:   3   6.458344E-02   0.847419      -0.159342       6.506043E-02  -0.162528    
   x:   4   8.019840E-03  -0.210351       0.393287      -3.361683E-02   0.548015    
   x:   5   0.759768      -2.946401E-02   0.255951       3.541698E-02  -0.135580    
   x:   6  -4.447435E-02  -0.418648      -3.798764E-02   3.030304E-02  -0.759239    
   x:   7   0.143971      -1.808539E-02   1.619036E-02   5.599275E-02   1.218733E-02
   x:   8   0.561193       3.606388E-02   9.509851E-02  -2.392997E-02  -4.793239E-03
   x:   9   7.160313E-07  -5.482419E-07  -1.770369E-05  -6.045982E-05  -7.775125E-06
   x:  10  -2.330208E-06   1.834703E-06  -3.912588E-05   9.010619E-06   3.280945E-06
   x:  11  -2.364626E-06   1.040137E-05   2.789844E-05  -2.288512E-05   2.278372E-05
   x:  12  -1.062027E-07   1.062626E-06   1.075724E-06   1.498026E-07  -4.914963E-04
   x:  13   1.261118E-06   2.329349E-05  -9.841646E-06   8.079464E-06   1.180640E-05
 bummer (warning):overlap matrix: # small eigenvalues=                      4

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   3.613425E-07   0.952468       0.176161       3.886890E-02   2.620374E-07   9.344288E-08  -8.832635E-03   0.107437    
 ref    2   6.820307E-07  -0.136084       0.738372      -1.177119E-02   0.605386       9.704792E-02  -2.538623E-03  -3.611266E-02
 ref    3  -4.666738E-07  -1.833519E-02   3.523054E-03   0.433161      -0.137244       0.856148       8.042257E-03   3.253636E-02
 ref    4  -0.954969       1.002978E-07   6.913255E-07  -3.052252E-06  -2.315398E-07   1.204485E-06  -5.399662E-02  -1.817880E-03

              v      9       v     10       v     11       v     12       v     13
 ref    1   4.569556E-03  -1.393838E-02  -1.370890E-02   1.926765E-02   8.979191E-02
 ref    2   5.606953E-03  -1.125061E-02  -2.984044E-02  -3.466933E-02   5.290912E-02
 ref    3  -1.422438E-02   7.012208E-02  -4.214265E-02  -8.993262E-02   2.495943E-02
 ref    4  -6.238350E-02  -7.819193E-02  -0.168948       1.855366E-02  -3.981559E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.911965       0.926051       0.576239       0.189278       0.385328       0.742408       3.064772E-03   1.390870E-02

              v      9       v     10       v     11       v     12       v     13
 ref    1   4.146352E-03   1.135194E-02   3.139785E-02   1.000532E-02   1.307022E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00000036     0.95246841     0.17616051     0.03886890     0.00000026     0.00000009    -0.00883264     0.10743676
 ref:   2     0.00000068    -0.13608419     0.73837231    -0.01177119     0.60538625     0.09704792    -0.00253862    -0.03611266
 ref:   3    -0.00000047    -0.01833519     0.00352305     0.43316068    -0.13724377     0.85614792     0.00804226     0.03253636
 ref:   4    -0.95496880     0.00000010     0.00000069    -0.00000305    -0.00000023     0.00000120    -0.05399662    -0.00181788

                ci   9         ci  10         ci  11         ci  12         ci  13
 ref:   1     0.00456956    -0.01393838    -0.01370890     0.01926765     0.08979191
 ref:   2     0.00560695    -0.01125061    -0.02984044    -0.03466933     0.05290912
 ref:   3    -0.01422438     0.07012208    -0.04214265    -0.08993262     0.02495943
 ref:   4    -0.06238350    -0.07819193    -0.16894810     0.01855366    -0.03981559

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 65  1   -196.4879585193  2.4833E-11  0.0000E+00  1.0608E-04  1.0000E-04   
 mr-sdci # 65  2   -196.4725827978  3.2600E-11  0.0000E+00  5.4939E-05  1.0000E-04   
 mr-sdci # 65  3   -196.4725827937  5.0903E-11  0.0000E+00  1.0792E-04  1.0000E-04   
 mr-sdci # 65  4   -196.4725824467  2.5504E-09  0.0000E+00  8.9226E-04  1.0000E-04   
 mr-sdci # 65  5   -196.4644811584  7.4524E-09  0.0000E+00  8.1758E-05  1.0000E-04   
 mr-sdci # 65  6   -196.4644811455  1.0801E-10  1.4788E-08  1.9369E-04  1.0000E-04   
 mr-sdci # 65  7   -195.8255194248  6.7085E-05  0.0000E+00  2.2951E-01  1.0000E-04   
 mr-sdci # 65  8   -195.7792095163  1.7587E-03  0.0000E+00  1.8064E-01  1.0000E-04   
 mr-sdci # 65  9   -194.6503257104  1.4258E-01  0.0000E+00  2.0435E+00  1.0000E-04   
 mr-sdci # 65 10   -194.2721687288  5.9219E-01  0.0000E+00  2.8907E+00  1.0000E-04   
 mr-sdci # 65 11   -193.5242974531  5.8543E-02  0.0000E+00  3.8667E+00  1.0000E-04   
 mr-sdci # 65 12   -193.4303589557  5.9811E-01  0.0000E+00  3.3850E+00  1.0000E-04   
 mr-sdci # 65 13   -192.6117532805 -8.3120E-01  0.0000E+00  4.0935E+00  1.0000E-04   
 
 root number  6 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.008000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  66

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59964372
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426799
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426706
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616635
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616635
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.93156060
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88437653
   ht   9    -0.00315058     0.00013820     0.00001352     0.00045880     0.00025583    -0.00024991     0.00017439    -0.00000877
   ht  10    -0.00002617    -0.00174897    -0.00038908     0.00072321     0.00060512     0.00007061     0.00002290     0.00026367
   ht  11    -0.00077258     0.00130541     0.00004934    -0.00111388    -0.00005756     0.00117847     0.00005313    -0.00010662
   ht  12     0.00043160     0.00700107    -0.00407779     0.01361572    -0.00337880    -0.01884908     0.00030118    -0.00014368
   ht  13     0.00027721    -0.00041376    -0.00101147     0.00012959     0.00018013     0.00091811    -0.00000981    -0.00002047
   ht  14     0.00022855    -0.00062277    -0.00089368    -0.00232309    -0.00095914    -0.00053300    -0.00003866     0.00002252

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14
   ht   9    -0.00000047
   ht  10    -0.00000001    -0.00000017
   ht  11    -0.00000003     0.00000004    -0.00000042
   ht  12    -0.00000019     0.00000011     0.00000049    -0.00002912
   ht  13     0.00000000    -0.00000002    -0.00000002     0.00000035    -0.00000020
   ht  14     0.00000003     0.00000002    -0.00000003     0.00000005     0.00000002    -0.00000056

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7         v:   8

   eig(s)   1.862688E-09   3.167817E-09   5.530237E-09   6.644403E-09   8.366979E-09   3.549077E-07    1.00000        1.00000    
 
   x:   1  -2.548204E-06   1.773806E-05   6.143972E-05   7.905393E-06  -1.610879E-06  -8.508826E-06   4.867718E-02   3.890446E-02
   x:   2   3.740961E-05  -6.491643E-06  -4.525777E-06  -2.208814E-05  -1.228678E-05  -1.383552E-04   9.098511E-03  -3.992810E-02
   x:   3   1.059355E-05  -2.014690E-05   2.348398E-06  -4.770841E-08  -1.466328E-05   8.082121E-05  -6.916598E-02   4.902485E-02
   x:   4  -1.648581E-05  -3.232087E-06  -1.037658E-05   1.957368E-05  -5.102494E-05  -2.682431E-04  -0.158675       5.738644E-02
   x:   5  -1.201301E-05  -8.210231E-07  -6.623305E-06  -7.421674E-07  -1.755720E-05   6.713967E-05   0.526825      -0.227536    
   x:   6  -8.464958E-07   1.499979E-05  -1.132363E-06  -2.651349E-05  -4.256763E-06   3.727695E-04  -0.181109       7.272988E-02
   x:   7  -2.804539E-07  -1.030814E-06  -3.684025E-06  -7.006673E-07  -5.157730E-07  -5.990287E-06   0.208263       0.963792    
   x:   8  -5.276711E-06  -8.335295E-07   4.876870E-07   1.718309E-06   4.266997E-07   2.439926E-06  -0.783497       7.238347E-02
   x:   9   1.446291E-02  -0.173391      -0.975523       0.113488       7.212278E-02  -3.135136E-04   2.187931E-11  -1.064403E-11
   x:  10  -0.987176      -0.137669       1.147946E-03  -8.030220E-02   8.879787E-03   1.436530E-03  -7.230209E-11   3.626675E-11
   x:  11   9.114604E-02  -7.480878E-02  -9.500313E-02  -0.986085       6.849408E-02  -3.917697E-03  -7.278059E-11   4.253769E-11
   x:  12  -2.017067E-03   1.421411E-03  -9.083259E-04   2.592434E-03  -1.688981E-02  -0.999850      -3.604111E-12   1.795271E-12
   x:  13  -0.129978       0.967996      -0.187907      -7.251579E-02  -7.424847E-02   2.875250E-03   3.324306E-11  -2.739152E-11
   x:  14  -8.270356E-03   9.147226E-02   6.338908E-02   5.516532E-02   0.992084      -1.652644E-02  -3.294076E-12   1.369836E-12

                v:   9         v:  10         v:  11         v:  12         v:  13         v:  14

   eig(s)    1.00000        1.00000        1.00000        1.00000        1.00000        1.00000    
 
   x:   1  -6.234521E-02  -6.428971E-03  -0.350325       0.125695      -0.923797      -1.616920E-02
   x:   2  -0.182045      -0.221394      -0.704914      -0.544586       0.210743      -0.279782    
   x:   3   0.103908       0.877381      -0.396313       9.009178E-02   0.145028       0.161260    
   x:   4   0.235313      -0.167203      -0.272590       0.690132       0.186246      -0.550386    
   x:   5  -0.680109      -6.258729E-03  -0.111200       0.391160       0.157163       0.134254    
   x:   6   0.202103      -0.389402      -0.361849       0.213916       0.135653       0.757848    
   x:   7  -0.154480      -1.826123E-02   1.685410E-02  -2.120461E-03   5.564933E-02  -1.222527E-02
   x:   8  -0.607903       3.438777E-02   7.359222E-02   6.677089E-02  -1.635637E-02   4.818132E-03
   x:   9  -6.543217E-07  -3.328705E-07  -1.858230E-05   2.129031E-06  -6.015469E-05   7.817703E-06
   x:  10   2.191310E-06   1.929225E-06  -2.225926E-05  -3.326281E-05   3.251107E-06  -3.256374E-06
   x:  11   2.209382E-06   1.023694E-05   1.567068E-05   2.668800E-05  -1.866102E-05  -2.278594E-05
   x:  12   1.018975E-07   1.079166E-06  -2.354805E-07   1.842467E-06   9.564245E-07   4.914930E-04
   x:  13  -1.169545E-06   2.329651E-05  -4.078265E-06  -1.036237E-05   6.190451E-06  -1.180153E-05
   x:  14   8.446048E-08   8.721743E-07  -3.256102E-05   3.565273E-05   2.234397E-05  -1.527793E-05
 bummer (warning):overlap matrix: # small eigenvalues=                      5

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -3.342240E-07  -0.952968       0.173067       4.048131E-02  -2.424912E-07  -2.490483E-07  -8.936808E-03   0.107377    
 ref    2  -5.791755E-07   0.133816       0.738831      -8.539586E-03  -0.610496       5.661086E-02  -2.357405E-03  -3.633083E-02
 ref    3   5.538976E-07   1.870056E-02   1.617931E-03   0.433156       8.005955E-02   0.863374       8.155461E-03   3.229472E-02
 ref    4   0.954969      -8.760248E-08   6.148501E-07  -1.050597E-06   2.396727E-07   7.304220E-08  -5.390138E-02  -2.031546E-03

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1  -1.682206E-03  -7.365790E-03   1.197618E-02  -2.307269E-02  -7.503903E-02   5.170201E-02
 ref    2  -2.326280E-02   5.866382E-03   3.284600E-02   5.263467E-03  -5.908298E-02   3.140627E-03
 ref    3  -4.536334E-02   5.756296E-02  -1.852041E-02   3.391678E-02  -6.428003E-02  -7.208024E-02
 ref    4   1.510865E-02   5.268449E-02   0.130285      -0.126325       9.160309E-03  -6.684142E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.911966       0.926405       0.575826       0.189336       0.379115       0.748620       3.057294E-03   1.389678E-02

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1   2.830091E-03   6.177820E-03   1.853959E-02   1.766851E-02   1.333749E-02   1.234630E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00000033    -0.95296831     0.17306672     0.04048131    -0.00000024    -0.00000025    -0.00893681     0.10737681
 ref:   2    -0.00000058     0.13381630     0.73883096    -0.00853959    -0.61049623     0.05661086    -0.00235740    -0.03633083
 ref:   3     0.00000055     0.01870056     0.00161793     0.43315646     0.08005955     0.86337435     0.00815546     0.03229472
 ref:   4     0.95496888    -0.00000009     0.00000061    -0.00000105     0.00000024     0.00000007    -0.05390138    -0.00203155

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14
 ref:   1    -0.00168221    -0.00736579     0.01197618    -0.02307269    -0.07503903     0.05170201
 ref:   2    -0.02326280     0.00586638     0.03284600     0.00526347    -0.05908298     0.00314063
 ref:   3    -0.04536334     0.05756296    -0.01852041     0.03391678    -0.06428003    -0.07208024
 ref:   4     0.01510865     0.05268449     0.13028544    -0.12632539     0.00916031    -0.06684142

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 66  1   -196.4879585193  4.6093E-11  4.2220E-09  1.0595E-04  1.0000E-04   
 mr-sdci # 66  2   -196.4725827978  2.4016E-12  0.0000E+00  5.5019E-05  1.0000E-04   
 mr-sdci # 66  3   -196.4725827938  6.3636E-11  0.0000E+00  1.1009E-04  1.0000E-04   
 mr-sdci # 66  4   -196.4725824816  3.4908E-08  0.0000E+00  9.6202E-04  1.0000E-04   
 mr-sdci # 66  5   -196.4644811585  9.3650E-12  0.0000E+00  7.8828E-05  1.0000E-04   
 mr-sdci # 66  6   -196.4644811566  1.1127E-08  0.0000E+00  1.1420E-04  1.0000E-04   
 mr-sdci # 66  7   -195.8255574235  3.7999E-05  0.0000E+00  2.2822E-01  1.0000E-04   
 mr-sdci # 66  8   -195.7792997498  9.0234E-05  0.0000E+00  1.7880E-01  1.0000E-04   
 mr-sdci # 66  9   -194.8712300725  2.2090E-01  0.0000E+00  2.4468E+00  1.0000E-04   
 mr-sdci # 66 10   -194.5754924494  3.0332E-01  0.0000E+00  2.0290E+00  1.0000E-04   
 mr-sdci # 66 11   -194.1168190464  5.9252E-01  0.0000E+00  2.8025E+00  1.0000E-04   
 mr-sdci # 66 12   -193.4745202905  4.4161E-02  0.0000E+00  3.5258E+00  1.0000E-04   
 mr-sdci # 66 13   -192.6498532542  3.8100E-02  0.0000E+00  3.9559E+00  1.0000E-04   
 mr-sdci # 66 14   -192.4164752823  1.7987E-01  0.0000E+00  3.7848E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.018000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  67

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59964372
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426799
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426706
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616635
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616635
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.93156060
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88437653
   ht   9    -0.00315058     0.00013820     0.00001352     0.00045880     0.00025583    -0.00024991     0.00017439    -0.00000877
   ht  10    -0.00002617    -0.00174897    -0.00038908     0.00072321     0.00060512     0.00007061     0.00002290     0.00026367
   ht  11    -0.00077258     0.00130541     0.00004934    -0.00111388    -0.00005756     0.00117847     0.00005313    -0.00010662
   ht  12     0.00043160     0.00700107    -0.00407779     0.01361572    -0.00337880    -0.01884908     0.00030118    -0.00014368
   ht  13     0.00027721    -0.00041376    -0.00101147     0.00012959     0.00018013     0.00091811    -0.00000981    -0.00002047
   ht  14     0.00022855    -0.00062277    -0.00089368    -0.00232309    -0.00095914    -0.00053300    -0.00003866     0.00002252
   ht  15    -0.00194816    -0.00000428    -0.00008978     0.00006265     0.00011580    -0.00013076     0.00007479    -0.00000099

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15
   ht   9    -0.00000047
   ht  10    -0.00000001    -0.00000017
   ht  11    -0.00000003     0.00000004    -0.00000042
   ht  12    -0.00000019     0.00000011     0.00000049    -0.00002912
   ht  13     0.00000000    -0.00000002    -0.00000002     0.00000035    -0.00000020
   ht  14     0.00000003     0.00000002    -0.00000003     0.00000005     0.00000002    -0.00000056
   ht  15    -0.00000010     0.00000000    -0.00000002    -0.00000005     0.00000000     0.00000001    -0.00000019

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7         v:   8

   eig(s)   1.860850E-09   2.358579E-09   3.239951E-09   5.559358E-09   6.645097E-09   8.367074E-09   3.549078E-07    1.00000    
 
   x:   1  -2.191894E-07  -4.749402E-05  -3.725549E-06  -5.755661E-05   7.634291E-06   1.735728E-06  -8.494721E-06  -5.149199E-02
   x:   2  -3.723987E-05   4.678968E-06   5.735304E-06   4.435834E-06  -2.210427E-05   1.229241E-05  -1.383551E-04  -6.037485E-03
   x:   3  -1.046440E-05   4.582259E-06   1.979732E-05  -1.990747E-06  -7.705505E-08   1.467280E-05   8.082186E-05   6.523994E-02
   x:   4   1.660657E-05   1.917312E-06   2.513375E-06   1.031363E-05   1.955521E-05   5.102283E-05  -2.682435E-04   0.153855    
   x:   5   1.217269E-05   2.389996E-06  -6.594702E-08   6.367794E-06  -7.337480E-07   1.755102E-05   6.713883E-05  -0.508008    
   x:   6   5.599892E-07  -6.243171E-06  -1.383521E-05   1.087385E-06  -2.653254E-05   4.267567E-06   3.727704E-04   0.175058    
   x:   7   3.991529E-07   2.068033E-06   4.165846E-07   3.525851E-06  -6.945438E-07   5.114877E-07  -5.990844E-06  -0.280931    
   x:   8   5.271754E-06  -1.585881E-07   8.635359E-07  -4.627148E-07   1.718356E-06  -4.267910E-07   2.439933E-06   0.775722    
   x:   9  -7.936569E-03   0.136545       0.130398       0.973177       0.109895      -7.168863E-02  -3.131713E-04   1.209585E-10
   x:  10   0.986946      -1.878225E-02   0.137930       6.434306E-04  -8.050164E-02  -8.825160E-03   1.436600E-03   1.062829E-10
   x:  11  -8.907226E-02   4.680524E-02   6.326528E-02   9.058330E-02  -0.986400      -6.831774E-02  -3.917597E-03   1.025591E-10
   x:  12   1.982539E-03  -7.623059E-04  -1.288725E-03   9.260323E-04   2.588328E-03   1.689101E-02  -0.999850       4.060231E-12
   x:  13   0.120095      -0.266788      -0.933957       0.177181      -7.228214E-02   7.415671E-02   2.875073E-03  -4.961005E-11
   x:  14   6.698744E-03  -3.674660E-02  -8.368162E-02  -6.290989E-02   5.528715E-02  -0.992126      -1.652651E-02   2.395542E-11
   x:  15   5.890554E-02   0.951990      -0.284058      -9.679851E-02   1.302607E-02  -4.033291E-03  -3.669554E-04  -2.419632E-10

                v:   9         v:  10         v:  11         v:  12         v:  13         v:  14         v:  15

   eig(s)    1.00000        1.00000        1.00000        1.00000        1.00000        1.00000        1.00000    
 
   x:   1  -2.909948E-02   6.111068E-02   1.078783E-02  -0.206629      -8.225008E-02   0.971050       1.584507E-02
   x:   2   4.369265E-02   0.181225       0.222863      -0.747595       0.510493      -0.133297       0.279777    
   x:   3  -5.397077E-02  -0.103710      -0.876426      -0.412223      -0.110405      -7.633121E-02  -0.161272    
   x:   4  -7.123717E-02  -0.234727       0.167902      -0.270196      -0.707449      -0.107468       0.550388    
   x:   5   0.274187       0.677208       6.360742E-03  -0.117907      -0.402684      -0.118418      -0.134232    
   x:   6  -8.773607E-02  -0.201381       0.390319      -0.368330      -0.231836      -7.065670E-02  -0.757857    
   x:   7  -0.943752       0.164695       1.760078E-02   5.537125E-03  -1.096973E-03  -5.285297E-02   1.223799E-02
   x:   8  -0.125347       0.609328      -3.468519E-02   7.663622E-02  -6.367724E-02   1.040920E-02  -4.818239E-03
   x:   9   3.341675E-07   5.555026E-07   6.000405E-07  -9.492186E-06   8.137520E-07   6.226634E-05  -7.838082E-06
   x:  10   2.387100E-08  -2.205557E-06  -1.879491E-06  -2.373859E-05   3.234617E-05  -1.671558E-06   3.255678E-06
   x:  11   2.011394E-08  -2.222467E-06  -1.021500E-05   1.919653E-05  -2.509182E-05   1.756549E-05   2.278137E-05
   x:  12  -4.820134E-10  -1.020780E-07  -1.079060E-06  -2.690724E-07  -1.892741E-06  -9.633620E-07  -4.914928E-04
   x:  13   4.722410E-09   1.171791E-06  -2.330832E-05  -5.398485E-06   9.850528E-06  -5.963501E-06   1.180311E-05
   x:  14   4.593386E-08  -9.869689E-08  -8.136038E-07  -3.411240E-05  -3.783425E-05  -1.534758E-05   1.527954E-05
   x:  15  -5.430687E-07   1.634889E-07  -3.622060E-07  -9.105137E-06  -2.120130E-06   3.754246E-05  -2.004894E-06
 bummer (warning):overlap matrix: # small eigenvalues=                      6

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   1.300101E-08   0.952840       0.173773      -4.048337E-02  -1.902618E-07   3.153006E-07   7.034072E-03  -0.107449    
 ref    2   2.845059E-09  -0.134367       0.738732       8.463589E-03  -0.612557      -2.616915E-02   1.781752E-05   3.626111E-02
 ref    3  -6.933671E-08  -1.869480E-02   1.560618E-03  -0.433157       3.700845E-02  -0.866288      -6.994905E-03  -3.222858E-02
 ref    4  -0.954966       1.161324E-07   4.364682E-07   7.463180E-07  -8.946044E-08  -5.331477E-07   5.172373E-02   1.714473E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1  -7.521229E-03  -1.463705E-03   1.118946E-02   7.677085E-04  -9.164983E-02  -1.004687E-02   1.973623E-02
 ref    2  -1.344542E-02  -2.257714E-02   1.049135E-03   3.104938E-02  -4.768193E-02  -3.554658E-02  -8.252826E-04
 ref    3   5.570619E-03  -4.489299E-02  -6.344633E-02   7.119750E-03   2.212933E-03  -9.856725E-02  -2.176087E-02
 ref    4  -3.292674E-02   1.867845E-02  -1.497792E-02   5.916270E-02  -4.989625E-02   4.251093E-02  -0.261863    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.911961       0.926307       0.575924       0.189336       0.376595       0.751140       2.773752E-03   1.390181E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1   1.352550E-03   2.876134E-03   4.376080E-03   4.515569E-03   1.316779E-02   1.288718E-02   6.943599E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00000001     0.95283961     0.17377339    -0.04048337    -0.00000019     0.00000032     0.00703407    -0.10744917
 ref:   2     0.00000000    -0.13436745     0.73873184     0.00846359    -0.61255654    -0.02616915     0.00001782     0.03626111
 ref:   3    -0.00000007    -0.01869480     0.00156062    -0.43315698     0.03700845    -0.86628823    -0.00699490    -0.03222858
 ref:   4    -0.95496633     0.00000012     0.00000044     0.00000075    -0.00000009    -0.00000053     0.05172373     0.00171447

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15
 ref:   1    -0.00752123    -0.00146370     0.01118946     0.00076771    -0.09164983    -0.01004687     0.01973623
 ref:   2    -0.01344542    -0.02257714     0.00104914     0.03104938    -0.04768193    -0.03554658    -0.00082528
 ref:   3     0.00557062    -0.04489299    -0.06344633     0.00711975     0.00221293    -0.09856725    -0.02176087
 ref:   4    -0.03292674     0.01867845    -0.01497792     0.05916270    -0.04989625     0.04251093    -0.26186304

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 67  1   -196.4879585239  4.5466E-09  0.0000E+00  2.6961E-05  1.0000E-04   
 mr-sdci # 67  2   -196.4725827978  3.9790E-13  0.0000E+00  5.4924E-05  1.0000E-04   
 mr-sdci # 67  3   -196.4725827938  1.8346E-11  4.8746E-09  1.0947E-04  1.0000E-04   
 mr-sdci # 67  4   -196.4725824816  5.3177E-11  0.0000E+00  9.6200E-04  1.0000E-04   
 mr-sdci # 67  5   -196.4644811585  6.1213E-11  0.0000E+00  7.5558E-05  1.0000E-04   
 mr-sdci # 67  6   -196.4644811568  1.2944E-10  0.0000E+00  1.1345E-04  1.0000E-04   
 mr-sdci # 67  7   -195.8363294885  1.0772E-02  0.0000E+00  2.2732E-01  1.0000E-04   
 mr-sdci # 67  8   -195.7793048578  5.1080E-06  0.0000E+00  1.7860E-01  1.0000E-04   
 mr-sdci # 67  9   -195.6042783922  7.3305E-01  0.0000E+00  6.0047E-01  1.0000E-04   
 mr-sdci # 67 10   -194.8706397719  2.9515E-01  0.0000E+00  2.4418E+00  1.0000E-04   
 mr-sdci # 67 11   -194.5321450609  4.1533E-01  0.0000E+00  2.0843E+00  1.0000E-04   
 mr-sdci # 67 12   -193.9043217021  4.2980E-01  0.0000E+00  2.6877E+00  1.0000E-04   
 mr-sdci # 67 13   -192.6832456015  3.3392E-02  0.0000E+00  3.8997E+00  1.0000E-04   
 mr-sdci # 67 14   -192.6173347425  2.0086E-01  0.0000E+00  3.5675E+00  1.0000E-04   
 mr-sdci # 67 15   -191.1484092374  5.6528E-01  0.0000E+00  4.6327E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.015000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  68

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59964372
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426799
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426706
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616635
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616635
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.93156060
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88437653
   ht   9    -0.00315058     0.00013820     0.00001352     0.00045880     0.00025583    -0.00024991     0.00017439    -0.00000877
   ht  10    -0.00002617    -0.00174897    -0.00038908     0.00072321     0.00060512     0.00007061     0.00002290     0.00026367
   ht  11    -0.00077258     0.00130541     0.00004934    -0.00111388    -0.00005756     0.00117847     0.00005313    -0.00010662
   ht  12     0.00043160     0.00700107    -0.00407779     0.01361572    -0.00337880    -0.01884908     0.00030118    -0.00014368
   ht  13     0.00027721    -0.00041376    -0.00101147     0.00012959     0.00018013     0.00091811    -0.00000981    -0.00002047
   ht  14     0.00022855    -0.00062277    -0.00089368    -0.00232309    -0.00095914    -0.00053300    -0.00003866     0.00002252
   ht  15    -0.00194816    -0.00000428    -0.00008978     0.00006265     0.00011580    -0.00013076     0.00007479    -0.00000099
   ht  16    -0.00039693     0.00116686    -0.00082800    -0.00059259    -0.00027186     0.00104017     0.00002934    -0.00018926

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15         ht  16
   ht   9    -0.00000047
   ht  10    -0.00000001    -0.00000017
   ht  11    -0.00000003     0.00000004    -0.00000042
   ht  12    -0.00000019     0.00000011     0.00000049    -0.00002912
   ht  13     0.00000000    -0.00000002    -0.00000002     0.00000035    -0.00000020
   ht  14     0.00000003     0.00000002    -0.00000003     0.00000005     0.00000002    -0.00000056
   ht  15    -0.00000010     0.00000000    -0.00000002    -0.00000005     0.00000000     0.00000001    -0.00000019
   ht  16    -0.00000002     0.00000004    -0.00000003     0.00000017    -0.00000002    -0.00000003    -0.00000001    -0.00000020

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7         v:   8

   eig(s)   1.809143E-09   2.348455E-09   2.622678E-09   3.240658E-09   5.560391E-09   6.780882E-09   8.369365E-09   3.549270E-07
 
   x:   1  -3.468528E-06   4.840826E-05  -4.062504E-07  -3.984397E-06   5.732794E-05   6.710593E-06  -1.797281E-06  -8.436785E-06
   x:   2   4.255610E-05  -6.531802E-06   1.449588E-05   6.430708E-06  -3.810046E-06  -1.794598E-05  -1.205649E-05  -1.385212E-04
   x:   3   6.258858E-06  -8.459632E-07  -1.858001E-05   1.922654E-05   1.717033E-06  -2.769328E-06  -1.500112E-05   8.094064E-05
   x:   4  -1.995041E-05  -2.215189E-07  -1.144750E-05   2.048017E-06  -1.073592E-05   1.744048E-05  -5.111352E-05  -2.681499E-04
   x:   5  -1.280585E-05  -2.462716E-06  -1.483990E-06  -1.490679E-07  -6.420089E-06  -1.348909E-06  -1.764951E-05   6.717671E-05
   x:   6   6.247592E-06   9.845212E-07   2.824781E-05  -1.286545E-05  -3.560257E-07  -2.185867E-05  -4.019807E-06   3.726088E-04
   x:   7  -1.552090E-07  -2.188440E-06   3.861008E-07   4.399195E-07  -3.506942E-06  -6.103905E-07  -5.078767E-07  -5.995034E-06
   x:   8  -6.121591E-06   5.070117E-07  -2.546616E-06   7.529539E-07   3.806576E-07   1.050726E-06   3.674921E-07   2.467838E-06
   x:   9   9.909246E-03  -0.132272      -3.491560E-02   0.129985      -0.974354       9.854511E-02   7.221012E-02  -3.137216E-04
   x:  10  -0.952215      -5.597697E-02   0.253834       0.143071       1.278904E-03  -7.192303E-02   8.745198E-03   1.433610E-03
   x:  11   0.125185      -6.983449E-02   0.137716       6.972283E-02  -7.884118E-02  -0.972557       5.938515E-02  -3.901577E-03
   x:  12  -3.803775E-03   1.943945E-03  -6.340775E-03  -1.522408E-03  -1.073878E-03   1.422655E-03  -1.701816E-02  -0.999823    
   x:  13  -0.115465       0.245552       0.129360      -0.931619      -0.176181      -7.126229E-02  -7.481572E-02   2.876150E-03
   x:  14  -1.369303E-02   4.039561E-02  -1.709549E-02  -8.466876E-02   6.154717E-02   4.251968E-02   0.992396      -1.652938E-02
   x:  15  -3.277096E-02  -0.938219      -0.166130      -0.285727       9.633814E-02   1.157984E-02   4.021814E-03  -3.662948E-04
   x:  16   0.250818      -0.179741       0.933123       3.237452E-02   1.752573E-02   0.179517       2.071223E-02  -7.386697E-03

                v:   9         v:  10         v:  11         v:  12         v:  13         v:  14         v:  15         v:  16

   eig(s)    1.00000        1.00000        1.00000        1.00000        1.00000        1.00000        1.00000        1.00000    
 
   x:   1   5.545722E-02   1.640866E-02  -6.190333E-02  -1.474150E-02  -0.246471       8.330261E-02  -0.961590       1.624636E-02
   x:   2  -8.583090E-03  -4.054488E-02  -0.196236       3.700734E-02  -0.600344       0.684222       0.228735       0.278410    
   x:   3  -5.107626E-02   7.293096E-02   0.135513       0.905927      -0.290088      -0.205608       2.952041E-02  -0.160334    
   x:   4  -0.120567       0.100635       0.239049      -0.237337      -0.473367      -0.574891       6.384536E-02   0.550645    
   x:   5   0.392661      -0.375859      -0.702383      -2.200343E-02  -0.211692      -0.376020       8.120971E-02  -0.133870    
   x:   6  -0.135159       0.120776       0.196588      -0.347014      -0.470964      -5.414659E-02   9.014128E-02  -0.758430    
   x:   7   0.521963       0.837638      -0.150640      -1.488003E-02   4.195436E-03  -5.216629E-03   5.300100E-02   1.219947E-02
   x:   8  -0.731306       0.353760      -0.572249       1.600383E-02   6.303076E-02  -8.818628E-02  -2.341926E-02  -4.619980E-03
   x:   9  -1.109348E-07  -2.977280E-07  -5.524746E-07  -6.545660E-07  -1.059543E-05   1.022405E-05  -6.124827E-05  -7.816431E-06
   x:  10   1.128278E-07  -1.634895E-07   1.977823E-06   1.230469E-05  -1.348710E-05   3.537315E-05   5.725350E-06   3.207589E-06
   x:  11  -6.944741E-08   5.156984E-08   2.809096E-06   8.207473E-07   1.245219E-05  -2.819351E-05  -2.132321E-05   2.284178E-05
   x:  12  -1.710408E-09   2.459255E-09   1.246367E-07   1.245091E-06  -3.200767E-07  -2.733842E-06   3.846945E-07  -4.914892E-04
   x:  13  -1.931827E-07   2.184474E-07  -4.725217E-07   2.548678E-05   1.178875E-06   4.791069E-06   5.642193E-06   1.181700E-05
   x:  14  -1.982098E-08  -3.587816E-08   1.431587E-07   1.533979E-06  -4.214725E-05  -2.965059E-05   1.315547E-05   1.528998E-05
   x:  15   1.363605E-07   5.359090E-07  -1.196926E-07   5.136222E-07  -1.020703E-05   4.339613E-06  -3.706407E-05  -1.990462E-06
   x:  16   1.916743E-07  -2.210768E-07  -8.407425E-07   1.816722E-05   1.040596E-05  -2.646935E-05  -1.312598E-05   1.238533E-05
 bummer (warning):overlap matrix: # small eigenvalues=                      7

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -3.944851E-08  -3.874422E-02   0.967768       4.079113E-02   1.365426E-07   2.718111E-07   6.911102E-03   0.107703    
 ref    2   2.036082E-08  -0.750277      -2.974680E-02  -6.880780E-03  -0.612368      -3.026787E-02   1.421627E-04  -3.653995E-02
 ref    3   7.692095E-08  -3.241072E-03  -1.839411E-02   0.433161       4.280490E-02  -0.866021      -6.894779E-03   3.196759E-02
 ref    4   0.954966       4.557118E-08   1.253460E-07  -6.841855E-07  -1.486460E-07  -5.256805E-07   5.173893E-02  -1.743910E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   2.246783E-02   1.599064E-02   3.878320E-03  -4.480405E-03  -3.291694E-02   6.343057E-02  -5.308218E-02   6.517330E-02
 ref    2  -3.198794E-02   2.887368E-03   2.089513E-02   1.260305E-02  -6.731682E-02   1.097067E-02  -9.343011E-03  -1.385671E-04
 ref    3  -3.129643E-02  -1.739753E-02   3.604637E-02   3.549519E-02  -3.946294E-03  -6.397399E-02  -9.681894E-02   4.509940E-02
 ref    4  -1.272130E-02   3.044699E-02  -1.941320E-02   5.669333E-02  -1.798986E-02   6.561471E-02  -0.135642      -0.226631    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.911961       0.564427       0.937798       0.189339       0.376827       0.750909       2.772238E-03   1.396009E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   2.669329E-03   1.493731E-03   2.127861E-03   4.652954E-03   5.954288E-03   1.254175E-02   3.067763E-02   5.764316E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15        ev   16

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00000004    -0.03874422     0.96776810     0.04079113     0.00000014     0.00000027     0.00691110     0.10770309
 ref:   2     0.00000002    -0.75027660    -0.02974680    -0.00688078    -0.61236810    -0.03026787     0.00014216    -0.03653995
 ref:   3     0.00000008    -0.00324107    -0.01839411     0.43316063     0.04280490    -0.86602115    -0.00689478     0.03196759
 ref:   4     0.95496633     0.00000005     0.00000013    -0.00000068    -0.00000015    -0.00000053     0.05173893    -0.00174391

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1     0.02246783     0.01599064     0.00387832    -0.00448040    -0.03291694     0.06343057    -0.05308218     0.06517330
 ref:   2    -0.03198794     0.00288737     0.02089513     0.01260305    -0.06731682     0.01097067    -0.00934301    -0.00013857
 ref:   3    -0.03129643    -0.01739753     0.03604637     0.03549519    -0.00394629    -0.06397399    -0.09681894     0.04509940
 ref:   4    -0.01272130     0.03044699    -0.01941320     0.05669333    -0.01798986     0.06561471    -0.13564184    -0.22663104

 trial vector basis is being transformed.  new dimension:   8

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 68  1   -196.4879585239  5.3291E-13  0.0000E+00  2.7029E-05  1.0000E-04   
 mr-sdci # 68  2   -196.4725827989  1.1127E-09  0.0000E+00  4.8477E-05  1.0000E-04   
 mr-sdci # 68  3   -196.4725827978  3.9504E-09  0.0000E+00  5.5458E-05  1.0000E-04   
 mr-sdci # 68  4   -196.4725824817  8.8853E-11  2.8388E-07  9.5913E-04  1.0000E-04   
 mr-sdci # 68  5   -196.4644811586  9.1426E-11  0.0000E+00  7.4043E-05  1.0000E-04   
 mr-sdci # 68  6   -196.4644811568  1.6058E-12  0.0000E+00  1.1279E-04  1.0000E-04   
 mr-sdci # 68  7   -195.8363327978  3.3092E-06  0.0000E+00  2.2737E-01  1.0000E-04   
 mr-sdci # 68  8   -195.7793180344  1.3177E-05  0.0000E+00  1.7877E-01  1.0000E-04   
 mr-sdci # 68  9   -195.6237977698  1.9519E-02  0.0000E+00  7.2507E-01  1.0000E-04   
 mr-sdci # 68 10   -195.6017972147  7.3116E-01  0.0000E+00  6.2469E-01  1.0000E-04   
 mr-sdci # 68 11   -194.8643821204  3.3224E-01  0.0000E+00  2.3848E+00  1.0000E-04   
 mr-sdci # 68 12   -194.0368303486  1.3251E-01  0.0000E+00  2.5167E+00  1.0000E-04   
 mr-sdci # 68 13   -193.2919245713  6.0868E-01  0.0000E+00  2.8539E+00  1.0000E-04   
 mr-sdci # 68 14   -192.6550448352  3.7710E-02  0.0000E+00  3.5689E+00  1.0000E-04   
 mr-sdci # 68 15   -191.4418685561  2.9346E-01  0.0000E+00  3.9641E+00  1.0000E-04   
 mr-sdci # 68 16   -190.9468237797  1.1090E+00  0.0000E+00  4.5782E+00  1.0000E-04   
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.011000
time for eigenvalue solver             0.001000
time for vector access                 0.000000

          starting ci iteration  69

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59964373
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426800
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426769
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616636
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616636
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.94801800
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.89100324
   ht   9     0.00038041    -0.00517987    -0.00311433     0.00810049    -0.00075542    -0.01545133    -0.00023069     0.00017723

                ht   9
   ht   9    -0.00001338

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -2.840196E-08  -3.763428E-02  -0.967567       4.623546E-02  -6.252975E-09   7.818838E-07  -6.904319E-03  -0.107250    
 ref    2   1.352410E-08  -0.750303       2.880590E-02  -7.907000E-03   0.609183      -6.933456E-02  -1.810797E-04   3.626655E-02
 ref    3   4.894752E-08  -3.757401E-03   2.084110E-02   0.433012      -9.805368E-02  -0.861519       6.480614E-03  -3.323666E-02
 ref    4   0.954966       4.263825E-08  -1.100670E-07   1.365018E-06   1.388440E-07  -4.267544E-07  -5.171141E-02   1.889657E-03

              v      9
 ref    1   4.621605E-02
 ref    2  -2.824568E-02
 ref    3  -0.128909    
 ref    4   8.806266E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.911961       0.564385       0.937451       0.189699       0.380718       0.747022       2.763771E-03   1.392607E-02

              v      9
 ref    1   1.962889E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00000003    -0.03763428    -0.96756749     0.04623546    -0.00000001     0.00000078    -0.00690432    -0.10725000
 ref:   2     0.00000001    -0.75030325     0.02880590    -0.00790700     0.60918265    -0.06933456    -0.00018108     0.03626655
 ref:   3     0.00000005    -0.00375740     0.02084110     0.43301169    -0.09805368    -0.86151863     0.00648061    -0.03323666
 ref:   4     0.95496633     0.00000004    -0.00000011     0.00000137     0.00000014    -0.00000043    -0.05171141     0.00188966

                ci   9
 ref:   1     0.04621605
 ref:   2    -0.02824568
 ref:   3    -0.12890928
 ref:   4     0.00880627

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 69  1   -196.4879585239  1.8474E-13  0.0000E+00  2.7040E-05  1.0000E-04   
 mr-sdci # 69  2   -196.4725827989  2.3448E-13  0.0000E+00  4.8595E-05  1.0000E-04   
 mr-sdci # 69  3   -196.4725827978  6.5228E-12  0.0000E+00  5.4841E-05  1.0000E-04   
 mr-sdci # 69  4   -196.4725826736  1.9191E-07  9.0020E-08  4.9084E-04  1.0000E-04   
 mr-sdci # 69  5   -196.4644811586  1.8872E-11  0.0000E+00  7.3946E-05  1.0000E-04   
 mr-sdci # 69  6   -196.4644811573  5.3191E-10  0.0000E+00  8.9746E-05  1.0000E-04   
 mr-sdci # 69  7   -195.8363528963  2.0099E-05  0.0000E+00  2.2685E-01  1.0000E-04   
 mr-sdci # 69  8   -195.7795389101  2.2088E-04  0.0000E+00  1.8149E-01  1.0000E-04   
 mr-sdci # 69  9   -193.4549892971 -2.1688E+00  0.0000E+00  3.8231E+00  1.0000E-04   
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.013000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  70

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59964373
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426800
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426769
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616636
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616636
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.94801800
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.89100324
   ht   9     0.00038041    -0.00517987    -0.00311433     0.00810049    -0.00075542    -0.01545133    -0.00023069     0.00017723
   ht  10     0.00017433     0.00434526     0.00076730    -0.00156143     0.00023970     0.00847383    -0.00003373    -0.00007318

                ht   9         ht  10
   ht   9    -0.00001338
   ht  10     0.00000222    -0.00000415

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   3.369971E-08   3.516559E-02   0.966927      -5.963904E-02   1.464838E-07   1.021414E-06  -6.972173E-03  -0.103764    
 ref    2  -5.246868E-09   0.750090      -2.583909E-02   2.334671E-02   0.606018      -9.302412E-02  -1.574974E-03   3.901320E-02
 ref    3  -4.450655E-08   1.252936E-02  -2.713131E-02  -0.432502      -0.131557      -0.857042       7.037983E-03  -3.391331E-02
 ref    4  -0.954966      -2.686091E-08   9.810768E-08  -8.654450E-07   1.144685E-07  -4.468273E-07  -5.167563E-02   1.188387E-03

              v      9       v     10
 ref    1  -4.333943E-02  -3.152059E-02
 ref    2  -2.446517E-02   7.368154E-02
 ref    3   1.228473E-02   0.176106    
 ref    4   1.893510E-03  -1.334297E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.911961       0.564028       0.936352       0.191160       0.384565       0.743175       2.770995E-03   1.344051E-02

              v      9       v     10
 ref    1   2.631351E-03   3.761401E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00000003     0.03516559     0.96692721    -0.05963904     0.00000015     0.00000102    -0.00697217    -0.10376393
 ref:   2    -0.00000001     0.75008958    -0.02583909     0.02334671     0.60601772    -0.09302412    -0.00157497     0.03901320
 ref:   3    -0.00000004     0.01252936    -0.02713131    -0.43250180    -0.13155657    -0.85704250     0.00703798    -0.03391331
 ref:   4    -0.95496633    -0.00000003     0.00000010    -0.00000087     0.00000011    -0.00000045    -0.05167563     0.00118839

                ci   9         ci  10
 ref:   1    -0.04333943    -0.03152059
 ref:   2    -0.02446517     0.07368154
 ref:   3     0.01228473     0.17610640
 ref:   4     0.00189351    -0.01334297

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 70  1   -196.4879585239  5.6843E-14  0.0000E+00  2.7016E-05  1.0000E-04   
 mr-sdci # 70  2   -196.4725827989  5.9615E-12  0.0000E+00  4.7798E-05  1.0000E-04   
 mr-sdci # 70  3   -196.4725827978  2.8422E-12  0.0000E+00  5.4411E-05  1.0000E-04   
 mr-sdci # 70  4   -196.4725827857  1.1207E-07  1.7724E-08  2.4558E-04  1.0000E-04   
 mr-sdci # 70  5   -196.4644811586  2.4457E-11  0.0000E+00  7.4306E-05  1.0000E-04   
 mr-sdci # 70  6   -196.4644811574  1.0380E-10  0.0000E+00  8.9294E-05  1.0000E-04   
 mr-sdci # 70  7   -195.8365990091  2.4611E-04  0.0000E+00  2.2472E-01  1.0000E-04   
 mr-sdci # 70  8   -195.7810361903  1.4973E-03  0.0000E+00  1.9565E-01  1.0000E-04   
 mr-sdci # 70  9   -195.6222828078  2.1673E+00  0.0000E+00  7.4546E-01  1.0000E-04   
 mr-sdci # 70 10   -190.7422629805 -4.8595E+00  0.0000E+00  4.2478E+00  1.0000E-04   
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  71

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59964373
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426800
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426769
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616636
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616636
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.94801800
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.89100324
   ht   9     0.00038041    -0.00517987    -0.00311433     0.00810049    -0.00075542    -0.01545133    -0.00023069     0.00017723
   ht  10     0.00017433     0.00434526     0.00076730    -0.00156143     0.00023970     0.00847383    -0.00003373    -0.00007318
   ht  11     0.00013990     0.00181879    -0.00022148     0.00069253     0.00099101     0.00071519     0.00000087     0.00005010

                ht   9         ht  10         ht  11
   ht   9    -0.00001338
   ht  10     0.00000222    -0.00000415
   ht  11     0.00000028    -0.00000025    -0.00000055

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7         v:   8

   eig(s)   9.271416E-09   4.365559E-08   1.431571E-07    1.00000        1.00000        1.00000        1.00000        1.00000    
 
   x:   1   2.743887E-06  -1.887039E-06  -8.058877E-06   9.484330E-03   0.479511       0.761414       0.298869      -0.295634    
   x:   2   3.580390E-05  -1.046825E-04   8.317124E-05   0.162411       0.125838      -1.363870E-02  -0.534263      -0.224577    
   x:   3  -4.349313E-06  -2.711449E-05   5.732087E-05  -0.195545      -0.512619       0.620625      -0.414106       0.238005    
   x:   4   1.358656E-05   6.210536E-05  -1.508136E-04  -0.144152      -0.337248       0.109910       0.149001       0.210913    
   x:   5   1.959790E-05  -7.677564E-06   1.365560E-05  -0.230496      -0.115735       3.298325E-03   0.506806       0.234872    
   x:   6   1.398110E-05  -2.250794E-04   2.659872E-04  -7.699632E-02  -8.833470E-02  -4.332279E-02   0.319860       0.134657    
   x:   7   6.754109E-08  -3.345110E-07   4.697552E-06   0.448780      -0.592673       3.673486E-02   0.262396      -0.609908    
   x:   8   1.026895E-06   2.126962E-06  -3.021076E-06   0.808736       7.253481E-02   0.139893       5.950589E-02   0.561916    
   x:   9  -1.283715E-03   0.199175      -0.979963       2.454499E-14   3.920347E-13  -1.282236E-13  -2.779831E-13  -4.444408E-13
   x:  10  -3.294302E-03  -0.979959      -0.199170       1.433462E-13   5.742798E-13  -2.466746E-13  -3.898438E-13  -5.993668E-13
   x:  11   0.999994      -2.972610E-03  -1.914137E-03    0.00000        0.00000        0.00000        0.00000        0.00000    

                v:   9         v:  10         v:  11

   eig(s)    1.00000        1.00000        1.00000    
 
   x:   1  -5.049240E-02   0.103947      -1.227219E-02
   x:   2   0.367855       0.618684       0.321935    
   x:   3   9.072986E-02  -0.234142       0.150414    
   x:   4  -0.349516       0.720422      -0.381510    
   x:   5   0.777486       0.123969       3.994319E-02
   x:   6  -0.348894       0.126309       0.852214    
   x:   7   6.272395E-02  -3.349532E-02   9.511284E-03
   x:   8   3.992459E-02   1.227758E-02  -8.898192E-03
   x:   9   4.788588E-06  -2.697266E-05   3.644432E-04
   x:  10   1.127808E-05  -4.946661E-05  -1.846519E-04
   x:  11  -1.824998E-05  -3.764657E-05  -1.848500E-05
 bummer (warning):overlap matrix: # small eigenvalues=                      1

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -3.096564E-08   1.501829E-02   0.589638      -0.769313       1.372159E-07  -1.021062E-06   7.076412E-03  -9.641815E-02
 ref    2  -1.159375E-08   0.749501       2.808046E-02   3.615333E-02   0.605941       9.352447E-02   1.374215E-03   3.988370E-02
 ref    3   2.856376E-08  -2.556079E-02   0.343732       0.262953      -0.132264       0.856934      -6.696712E-03  -3.651924E-02
 ref    4   0.954966       2.644195E-08  -3.764070E-07  -3.460358E-07   1.983272E-07   4.450002E-07   5.176417E-02   1.445053E-04

              v      9       v     10       v     11
 ref    1  -5.639663E-02   1.945265E-02   2.899424E-02
 ref    2  -9.225625E-03   4.090719E-02  -8.240316E-02
 ref    3   1.061363E-02   1.788484E-03  -0.180072    
 ref    4   5.282601E-03   1.926967E-02   9.785187E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.911961       0.562631       0.466613       0.662294       0.384658       0.743082       2.776340E-03   1.222085E-02

              v      9       v     10       v     11
 ref    1   3.406248E-03   2.426323E-03   4.015260E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00000003     0.01501829     0.58963838    -0.76931317     0.00000014    -0.00000102     0.00707641    -0.09641815
 ref:   2    -0.00000001     0.74950093     0.02808046     0.03615333     0.60594081     0.09352447     0.00137421     0.03988370
 ref:   3     0.00000003    -0.02556079     0.34373177     0.26295275    -0.13226400     0.85693358    -0.00669671    -0.03651924
 ref:   4     0.95496632     0.00000003    -0.00000038    -0.00000035     0.00000020     0.00000045     0.05176417     0.00014451

                ci   9         ci  10         ci  11
 ref:   1    -0.05639663     0.01945265     0.02899424
 ref:   2    -0.00922562     0.04090719    -0.08240316
 ref:   3     0.01061363     0.00178848    -0.18007193
 ref:   4     0.00528260     0.01926967     0.00978519

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 71  1   -196.4879585239  9.1660E-13  0.0000E+00  2.6842E-05  1.0000E-04   
 mr-sdci # 71  2   -196.4725827989  8.1712E-12  0.0000E+00  4.7208E-05  1.0000E-04   
 mr-sdci # 71  3   -196.4725827980  2.6970E-10  0.0000E+00  6.6616E-05  1.0000E-04   
 mr-sdci # 71  4   -196.4725827977  1.1977E-08  0.0000E+00  5.4160E-05  1.0000E-04   
 mr-sdci # 71  5   -196.4644811587  4.6107E-11  1.7182E-09  7.1321E-05  1.0000E-04   
 mr-sdci # 71  6   -196.4644811574  2.8422E-14  0.0000E+00  8.9287E-05  1.0000E-04   
 mr-sdci # 71  7   -195.8366221629  2.3154E-05  0.0000E+00  2.2465E-01  1.0000E-04   
 mr-sdci # 71  8   -195.7841392277  3.1030E-03  0.0000E+00  1.9693E-01  1.0000E-04   
 mr-sdci # 71  9   -195.7123074669  9.0025E-02  0.0000E+00  3.9431E-01  1.0000E-04   
 mr-sdci # 71 10   -193.4184598988  2.6762E+00  0.0000E+00  2.9313E+00  1.0000E-04   
 mr-sdci # 71 11   -190.6428824169 -4.2215E+00  0.0000E+00  4.1090E+00  1.0000E-04   
 
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.006000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mr-sdci  convergence criteria satisfied after 71 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 71  1   -196.4879585239  9.1660E-13  0.0000E+00  2.6842E-05  1.0000E-04   
 mr-sdci # 71  2   -196.4725827989  8.1712E-12  0.0000E+00  4.7208E-05  1.0000E-04   
 mr-sdci # 71  3   -196.4725827980  2.6970E-10  0.0000E+00  6.6616E-05  1.0000E-04   
 mr-sdci # 71  4   -196.4725827977  1.1977E-08  0.0000E+00  5.4160E-05  1.0000E-04   
 mr-sdci # 71  5   -196.4644811587  4.6107E-11  1.7182E-09  7.1321E-05  1.0000E-04   
 mr-sdci # 71  6   -196.4644811574  2.8422E-14  0.0000E+00  8.9287E-05  1.0000E-04   
 mr-sdci # 71  7   -195.8366221629  2.3154E-05  0.0000E+00  2.2465E-01  1.0000E-04   
 mr-sdci # 71  8   -195.7841392277  3.1030E-03  0.0000E+00  1.9693E-01  1.0000E-04   
 mr-sdci # 71  9   -195.7123074669  9.0025E-02  0.0000E+00  3.9431E-01  1.0000E-04   
 mr-sdci # 71 10   -193.4184598988  2.6762E+00  0.0000E+00  2.9313E+00  1.0000E-04   
 mr-sdci # 71 11   -190.6428824169 -4.2215E+00  0.0000E+00  4.1090E+00  1.0000E-04   

####################CIUDGINFO####################

   ci vector at position   1 energy= -196.487958523858
   ci vector at position   2 energy= -196.472582798909
   ci vector at position   3 energy= -196.472582798043
   ci vector at position   4 energy= -196.472582797657
   ci vector at position   5 energy= -196.464481158693
   ci vector at position   6 energy= -196.464481157395

################END OF CIUDGINFO################

 
    6 of the  12 expansion vectors are transformed.
    6 of the  11 matrix-vector products are transformed.

    6 expansion eigenvectors written to unit nvfile (= 11)
    6 matrix-vector products written to unit nhvfil (= 10)
maximum overlap with reference    4(overlap= 0.95497)
weight of reference states=  0.9120

 information on vector: 1 from unit 11 written to unit 48 filename civout              
maximum overlap with reference    2(overlap= 0.74950)
weight of reference states=  0.5626

 information on vector: 2 from unit 11 written to unit 48 filename civout              
maximum overlap with reference    1(overlap= 0.58964)
weight of reference states=  0.4666

 information on vector: 3 from unit 11 written to unit 48 filename civout              
maximum overlap with reference    1(overlap= 0.76931)
weight of reference states=  0.6623

 information on vector: 4 from unit 11 written to unit 48 filename civout              
maximum overlap with reference    2(overlap= 0.60594)
weight of reference states=  0.3847

 information on vector: 5 from unit 11 written to unit 48 filename civout              
maximum overlap with reference    3(overlap= 0.85693)
weight of reference states=  0.7431

 information on vector: 6 from unit 11 written to unit 48 filename civout              


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =      -196.4879585239

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     2    3    4   11   14   17

                                         symmetry   ag   ag   ag   b1g  b2g  b3g

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       4 -0.657612                        +-   +-   +    +-   +-   +-    - 
 z*  1  1       5 -0.355185                        +-   +    +-   +-   +-   +-    - 
 z*  1  1       6 -0.594432                        +    +-   +-   +-   +-   +-    - 
 y   1  1     115 -0.024165              1( b3g)   +-   +-    -   +-   +-   +     - 
 y   1  1     127  0.027199              1( b2g)   +-   +-    -   +-   +    +-    - 
 y   1  1     139 -0.024600              1( b1g)   +-   +-    -   +    +-   +-    - 
 y   1  1     151  0.059757              1( b3g)   +-   +-   +    +-   +-    -    - 
 y   1  1     152 -0.010108              2( b3g)   +-   +-   +    +-   +-    -    - 
 y   1  1     163 -0.054502              1( b2g)   +-   +-   +    +-    -   +-    - 
 y   1  1     175  0.059003              1( b1g)   +-   +-   +     -   +-   +-    - 
 y   1  1     187 -0.019414              1( ag )   +-   +-        +-   +-   +-    - 
 y   1  1     188  0.036756              2( ag )   +-   +-        +-   +-   +-    - 
 y   1  1     189  0.016041              3( ag )   +-   +-        +-   +-   +-    - 
 y   1  1     199 -0.016016              1( b3g)   +-    -   +-   +-   +-   +     - 
 y   1  1     211  0.013059              1( b2g)   +-    -   +-   +-   +    +-    - 
 y   1  1     223 -0.011954              1( b1g)   +-    -   +-   +    +-   +-    - 
 y   1  1     235 -0.010008              1( ag )   +-    -   +    +-   +-   +-    - 
 y   1  1     237 -0.060667              3( ag )   +-    -   +    +-   +-   +-    - 
 y   1  1     247  0.027142              1( b3g)   +-   +    +-   +-   +-    -    - 
 y   1  1     259 -0.032263              1( b2g)   +-   +    +-   +-    -   +-    - 
 y   1  1     271  0.034177              1( b1g)   +-   +    +-    -   +-   +-    - 
 y   1  1     283 -0.017336              1( ag )   +-   +     -   +-   +-   +-    - 
 y   1  1     284  0.019569              2( ag )   +-   +     -   +-   +-   +-    - 
 y   1  1     297 -0.028775              3( ag )   +-        +-   +-   +-   +-    - 
 y   1  1     307 -0.022770              1( b3g)    -   +-   +-   +-   +-   +     - 
 y   1  1     319  0.021180              1( b2g)    -   +-   +-   +-   +    +-    - 
 y   1  1     331 -0.024715              1( b1g)    -   +-   +-   +    +-   +-    - 
 y   1  1     343 -0.016751              1( ag )    -   +-   +    +-   +-   +-    - 
 y   1  1     344 -0.063257              2( ag )    -   +-   +    +-   +-   +-    - 
 y   1  1     346 -0.011174              4( ag )    -   +-   +    +-   +-   +-    - 
 y   1  1     356 -0.024559              2( ag )    -   +    +-   +-   +-   +-    - 
 y   1  1     357  0.027864              3( ag )    -   +    +-   +-   +-   +-    - 
 y   1  1     367  0.052410              1( b3g)   +    +-   +-   +-   +-    -    - 
 y   1  1     379 -0.055164              1( b2g)   +    +-   +-   +-    -   +-    - 
 y   1  1     391  0.049040              1( b1g)   +    +-   +-    -   +-   +-    - 
 y   1  1     403 -0.029012              1( ag )   +    +-    -   +-   +-   +-    - 
 y   1  1     404  0.027298              2( ag )   +    +-    -   +-   +-   +-    - 
 y   1  1     405  0.015575              3( ag )   +    +-    -   +-   +-   +-    - 
 y   1  1     415 -0.015670              1( ag )   +     -   +-   +-   +-   +-    - 
 y   1  1     417 -0.048981              3( ag )   +     -   +-   +-   +-   +-    - 
 y   1  1     427 -0.014619              1( ag )        +-   +-   +-   +-   +-    - 
 y   1  1     428 -0.035624              2( ag )        +-   +-   +-   +-   +-    - 
 y   1  1     429  0.012734              3( ag )        +-   +-   +-   +-   +-    - 
 x   1  1    3799  0.012052    1( b2g)   1( b3g)   +-   +-   +    +-    -    -    - 
 x   1  1    3985 -0.012197    1( b1g)   1( b3g)   +-   +-   +     -   +-    -    - 
 x   1  1    4159  0.012028    1( b1g)   1( b2g)   +-   +-   +     -    -   +-    - 
 x   1  1    6765  0.010408    3( ag )   1( b2g)   +-    -    -   +-   +    +-    - 
 x   1  1    7141  0.011551    3( ag )   1( b3g)   +-    -   +    +-   +-    -    - 
 x   1  1    7323 -0.015931    3( ag )   1( b2g)   +-    -   +    +-    -   +-    - 
 x   1  1    7331 -0.010264    5( ag )   2( b2g)   +-    -   +    +-    -   +-    - 
 x   1  1    7360 -0.011579    4( b1u)   4( b3u)   +-    -   +    +-    -   +-    - 
 x   1  1    7509  0.014378    3( ag )   1( b1g)   +-    -   +     -   +-   +-    - 
 x   1  1   11418 -0.013223    2( ag )   1( b3g)    -   +-    -   +-   +-   +     - 
 x   1  1   11786 -0.011440    2( ag )   1( b1g)    -   +-    -   +    +-   +-    - 
 x   1  1   11976  0.016120    2( ag )   1( b3g)    -   +-   +    +-   +-    -    - 
 x   1  1   11984 -0.010127    4( ag )   2( b3g)    -   +-   +    +-   +-    -    - 
 x   1  1   12011  0.010821    5( b1u)   5( b2u)    -   +-   +    +-   +-    -    - 
 x   1  1   12158 -0.011729    2( ag )   1( b2g)    -   +-   +    +-    -   +-    - 
 x   1  1   12344  0.013140    2( ag )   1( b1g)    -   +-   +     -   +-   +-    - 
 x   1  1   13275 -0.014710    2( ag )   3( ag )    -    -   +    +-   +-   +-    - 
 x   1  1   14401  0.011032    1( b2g)   1( b3g)   +    +-   +-   +-    -    -    - 
 x   1  1   14587 -0.010835    1( b1g)   1( b3g)   +    +-   +-    -   +-    -    - 
 x   1  1   14761  0.010924    1( b1g)   1( b2g)   +    +-   +-    -    -   +-    - 
 x   1  1   15693 -0.013110    3( ag )   1( b2g)   +     -   +-   +-    -   +-    - 
 x   1  1   15879  0.011271    3( ag )   1( b1g)   +     -   +-    -   +-   +-    - 
 w   1  1   18918 -0.016000    1( b3g)   1( b3g)   +-   +-   +    +-   +-         - 
 w   1  1   18919  0.010283    1( b3g)   2( b3g)   +-   +-   +    +-   +-         - 
 w   1  1   19331  0.012221    1( b2g)   1( b3g)   +-   +-   +    +-   +     -    - 
 w   1  1   19557 -0.015871    1( b2g)   1( b2g)   +-   +-   +    +-        +-    - 
 w   1  1   19558  0.010303    1( b2g)   2( b2g)   +-   +-   +    +-        +-    - 
 w   1  1   19559 -0.010024    2( b2g)   2( b2g)   +-   +-   +    +-        +-    - 
 w   1  1   20187 -0.012225    1( b1g)   1( b3g)   +-   +-   +    +    +-    -    - 
 w   1  1   20389  0.012118    1( b1g)   1( b2g)   +-   +-   +    +     -   +-    - 
 w   1  1   20624 -0.015981    1( b1g)   1( b1g)   +-   +-   +         +-   +-    - 
 w   1  1   20625  0.010286    1( b1g)   2( b1g)   +-   +-   +         +-   +-    - 
 w   1  1   24033 -0.012921    3( ag )   1( b3g)   +-   +    +    +-   +-    -    - 
 w   1  1   24243  0.014082    3( ag )   1( b2g)   +-   +    +    +-    -   +-    - 
 w   1  1   24457 -0.013953    3( ag )   1( b1g)   +-   +    +     -   +-   +-    - 
 w   1  1   25530  0.016488    3( ag )   3( ag )   +-        +    +-   +-   +-    - 
 w   1  1   25537 -0.010329    3( ag )   5( ag )   +-        +    +-   +-   +-    - 
 w   1  1   25539  0.010110    5( ag )   5( ag )   +-        +    +-   +-   +-    - 
 w   1  1   25587  0.011293    1( b3u)   2( b3u)   +-        +    +-   +-   +-    - 
 w   1  1   25588  0.010277    2( b3u)   2( b3u)   +-        +    +-   +-   +-    - 
 w   1  1   25766 -0.014423    1( b3g)   1( b3g)   +    +-   +-   +-   +-         - 
 w   1  1   26179  0.011019    1( b2g)   1( b3g)   +    +-   +-   +-   +     -    - 
 w   1  1   26405 -0.014491    1( b2g)   1( b2g)   +    +-   +-   +-        +-    - 
 w   1  1   27035 -0.011015    1( b1g)   1( b3g)   +    +-   +-   +    +-    -    - 
 w   1  1   27237  0.011071    1( b1g)   1( b2g)   +    +-   +-   +     -   +-    - 
 w   1  1   27472 -0.014341    1( b1g)   1( b1g)   +    +-   +-        +-   +-    - 
 w   1  1   27681 -0.012358    1( b1u)   1( b2u)   +    +-    -   +-   +-   +     - 
 w   1  1   27682 -0.010848    2( b1u)   1( b2u)   +    +-    -   +-   +-   +     - 
 w   1  1   27686 -0.010910    1( b1u)   2( b2u)   +    +-    -   +-   +-   +     - 
 w   1  1   27687 -0.011167    2( b1u)   2( b2u)   +    +-    -   +-   +-   +     - 
 w   1  1   27900  0.011886    1( b1u)   1( b3u)   +    +-    -   +-   +    +-    - 
 w   1  1   27901  0.010153    2( b1u)   1( b3u)   +    +-    -   +-   +    +-    - 
 w   1  1   27905  0.010263    1( b1u)   2( b3u)   +    +-    -   +-   +    +-    - 
 w   1  1   27906  0.010124    2( b1u)   2( b3u)   +    +-    -   +-   +    +-    - 
 w   1  1   28114 -0.011322    1( b2u)   1( b3u)   +    +-    -   +    +-   +-    - 
 w   1  1   28312 -0.017675    2( ag )   1( b3g)   +    +-   +    +-   +-    -    - 
 w   1  1   28320  0.010831    4( ag )   2( b3g)   +    +-   +    +-   +-    -    - 
 w   1  1   28522  0.016087    2( ag )   1( b2g)   +    +-   +    +-    -   +-    - 
 w   1  1   28530 -0.010061    4( ag )   2( b2g)   +    +-   +    +-    -   +-    - 
 w   1  1   28736 -0.016807    2( ag )   1( b1g)   +    +-   +     -   +-   +-    - 
 w   1  1   28744  0.010438    4( ag )   2( b1g)   +    +-   +     -   +-   +-    - 
 w   1  1   28951 -0.014864    2( ag )   2( ag )   +    +-        +-   +-   +-    - 
 w   1  1   28995 -0.012370    1( b2u)   1( b2u)   +    +-        +-   +-   +-    - 
 w   1  1   28996 -0.017191    1( b2u)   2( b2u)   +    +-        +-   +-   +-    - 
 w   1  1   28997 -0.014227    2( b2u)   2( b2u)   +    +-        +-   +-   +-    - 
 w   1  1   29809  0.013542    2( ag )   3( ag )   +     -   +    +-   +-   +-    - 
 w   1  1   30025  0.011520    3( ag )   1( b3g)   +    +    +-   +-   +-    -    - 
 w   1  1   30235 -0.012721    3( ag )   1( b2g)   +    +    +-   +-    -   +-    - 
 w   1  1   30449  0.011688    3( ag )   1( b1g)   +    +    +-    -   +-   +-    - 
 w   1  1   30880 -0.014429    3( ag )   3( ag )   +         +-   +-   +-   +-    - 
 w   1  1   31733  0.015550    2( ag )   2( ag )        +-   +    +-   +-   +-    - 
 w   1  1   31738  0.010489    2( ag )   4( ag )        +-   +    +-   +-   +-    - 
 w   1  1   31740  0.010089    4( ag )   4( ag )        +-   +    +-   +-   +-    - 
 w   1  1   31763  0.011635    1( b1u)   2( b1u)        +-   +    +-   +-   +-    - 
 w   1  1   31977  0.011844    1( b1u)   2( b1u)        +    +-   +-   +-   +-    - 

 ci coefficient statistics:
           rq > 0.1                3
      0.1> rq > 0.01             115
     0.01> rq > 0.001           2585
    0.001> rq > 0.0001          3008
   0.0001> rq > 0.00001         3849
  0.00001> rq > 0.000001        8677
 0.000001> rq                  13921
           all                 32158
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:         126 2x:           0 4x:           0
All internal counts: zz :          15 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :          12 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:         193
  iref  icsf         v(icsf)             hv(icsf)
     1     3      0.000000001716     -0.000000336829
     2     4     -0.657612426268    128.929881411863
     3     5     -0.355185464914     69.636792195502
     4     6     -0.594432384612    116.542957215513

 number of reference csfs (nref) is     4.  root number (iroot) is  1.
 c0**2 =   0.91196068  c**2 (all zwalks) =   0.91196068

 pople ci energy extrapolation is computed with 12 correlated electrons.

 eref      =   -196.057550412962   "relaxed" cnot**2         =   0.911960677543
 eci       =   -196.487958523858   deltae = eci - eref       =  -0.430408110895
 eci+dv1   =   -196.525851362321   dv1 = (1-cnot**2)*deltae  =  -0.037892838463
 eci+dv2   =   -196.529509480381   dv2 = dv1 / cnot**2       =  -0.041550956523
 eci+dv3   =   -196.533949367801   dv3 = dv1 / (2*cnot**2-1) =  -0.045990843943
 eci+pople =   -196.524911533060   ( 12e- scaled deltae )    =  -0.467361120097


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 2) =      -196.4725827989

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     2    3    4   11   14   17

                                         symmetry   ag   ag   ag   b1g  b2g  b3g

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z   3  3       1  0.468874                        +-   +-   +-   +-   +-   +    +  
 z   3  2       2  0.396575                        +-   +-   +-   +-   +    +-   +  
 z*  1  1       4 -0.226145                        +-   +-   +    +-   +-   +-    - 
 z*  1  1       5 -0.475408                        +-   +    +-   +-   +-   +-    - 
 z*  1  1       6  0.534247                        +    +-   +-   +-   +-   +-    - 
 y   3  3     121 -0.010487              1( ag )   +-   +-    -   +-   +-   +    +  
 y   1  1     283  0.013313              1( ag )   +-   +     -   +-   +-   +-    - 
 y   1  1     331  0.014038              1( b1g)    -   +-   +-   +    +-   +-    - 
 y   1  1     355  0.010254              1( ag )    -   +    +-   +-   +-   +-    - 
 y   1  1     403 -0.011028              1( ag )   +    +-    -   +-   +-   +-    - 
 y   1  1     427 -0.013159              1( ag )        +-   +-   +-   +-   +-    - 
 x   3  3    1137 -0.010343    1( b1g)   1( b2g)   +-   +-   +-    -    -   +    +  
 x   3  3    5231  0.011450    3( ag )   1( b2g)   +-    -   +-   +-    -   +    +  
 x   3  3    5789 -0.010348    3( ag )   1( b1g)   +-    -   +-    -   +-   +    +  
 x   1  1   15693  0.010085    3( ag )   1( b2g)   +     -   +-   +-    -   +-    - 
 w   3  3   20986 -0.012430    1( b1u)   1( b1u)   +-   +-        +-   +-   +    +  
 w   3  3   21001 -0.018663    1( b2u)   1( b2u)   +-   +-        +-   +-   +    +  
 w   3  3   21002 -0.018785    1( b2u)   2( b2u)   +-   +-        +-   +-   +    +  
 w   3  3   21003 -0.013173    2( b2u)   2( b2u)   +-   +-        +-   +-   +    +  
 w   3  3   21016 -0.010665    1( b3u)   1( b3u)   +-   +-        +-   +-   +    +  
 w   3  2   21154 -0.010299    1( b1u)   1( b1u)   +-   +-        +-   +    +-   +  
 w   3  2   21169 -0.015419    1( b2u)   1( b2u)   +-   +-        +-   +    +-   +  
 w   3  2   21170 -0.015518    1( b2u)   2( b2u)   +-   +-        +-   +    +-   +  
 w   3  2   21171 -0.010958    2( b2u)   2( b2u)   +-   +-        +-   +    +-   +  
 w   3  3   23569 -0.010898    1( b2u)   1( b2u)   +-   +     -   +-   +-   +    +  
 w   3  3   23584 -0.012515    1( b3u)   1( b3u)   +-   +     -   +-   +-   +    +  
 w   3  3   23585 -0.011021    1( b3u)   2( b3u)   +-   +     -   +-   +-   +    +  
 w   3  2   23752 -0.010809    1( b3u)   1( b3u)   +-   +     -   +-   +    +-   +  
 w   1  1   24700 -0.010006    1( b1u)   1( b1u)   +-   +         +-   +-   +-    - 
 w   1  1   24715 -0.014579    1( b2u)   1( b2u)   +-   +         +-   +-   +-    - 
 w   1  1   24716 -0.015432    1( b2u)   2( b2u)   +-   +         +-   +-   +-    - 
 w   1  1   24717 -0.011410    2( b2u)   2( b2u)   +-   +         +-   +-   +-    - 
 w   1  1   25766  0.010040    1( b3g)   1( b3g)   +    +-   +-   +-   +-         - 
 w   1  1   26405  0.010452    1( b2g)   1( b2g)   +    +-   +-   +-        +-    - 
 w   1  1   27472  0.010857    1( b1g)   1( b1g)   +    +-   +-        +-   +-    - 
 w   3  3   27834 -0.019525    1( b1u)   1( b1u)   +    +-    -   +-   +-   +    +  
 w   3  3   27835 -0.016655    1( b1u)   2( b1u)   +    +-    -   +-   +-   +    +  
 w   3  3   27849 -0.017280    1( b2u)   1( b2u)   +    +-    -   +-   +-   +    +  
 w   3  3   27850 -0.013015    1( b2u)   2( b2u)   +    +-    -   +-   +-   +    +  
 w   3  3   27864 -0.015090    1( b3u)   1( b3u)   +    +-    -   +-   +-   +    +  
 w   3  3   27865 -0.010754    1( b3u)   2( b3u)   +    +-    -   +-   +-   +    +  
 w   3  2   28002 -0.016410    1( b1u)   1( b1u)   +    +-    -   +-   +    +-   +  
 w   3  2   28003 -0.013933    1( b1u)   2( b1u)   +    +-    -   +-   +    +-   +  
 w   3  2   28017 -0.013828    1( b2u)   1( b2u)   +    +-    -   +-   +    +-   +  
 w   3  2   28018 -0.010146    1( b2u)   2( b2u)   +    +-    -   +-   +    +-   +  
 w   3  2   28032 -0.013532    1( b3u)   1( b3u)   +    +-    -   +-   +    +-   +  
 w   1  1   28980  0.021268    1( b1u)   1( b1u)   +    +-        +-   +-   +-    - 
 w   1  1   28981  0.016714    1( b1u)   2( b1u)   +    +-        +-   +-   +-    - 
 w   1  1   28995  0.027008    1( b2u)   1( b2u)   +    +-        +-   +-   +-    - 
 w   1  1   28996  0.025626    1( b2u)   2( b2u)   +    +-        +-   +-   +-    - 
 w   1  1   28997  0.017039    2( b2u)   2( b2u)   +    +-        +-   +-   +-    - 
 w   1  1   29010  0.017035    1( b3u)   1( b3u)   +    +-        +-   +-   +-    - 
 w   1  1   29011  0.011450    1( b3u)   2( b3u)   +    +-        +-   +-   +-    - 
 w   3  3   29362 -0.012604    1( b3u)   1( b3u)   +     -   +-   +-   +-   +    +  
 w   3  3   29363 -0.012223    1( b3u)   2( b3u)   +     -   +-   +-   +-   +    +  
 w   3  2   29530 -0.010949    1( b3u)   1( b3u)   +     -   +-   +-   +    +-   +  
 w   3  2   29531 -0.010667    1( b3u)   2( b3u)   +     -   +-   +-   +    +-   +  
 w   1  1   30692  0.024919    1( b1u)   1( b1u)   +    +     -   +-   +-   +-    - 
 w   1  1   30693  0.019088    1( b1u)   2( b1u)   +    +     -   +-   +-   +-    - 
 w   1  1   30694  0.010269    2( b1u)   2( b1u)   +    +     -   +-   +-   +-    - 
 w   1  1   30707  0.024651    1( b2u)   1( b2u)   +    +     -   +-   +-   +-    - 
 w   1  1   30708  0.018691    1( b2u)   2( b2u)   +    +     -   +-   +-   +-    - 
 w   1  1   30722  0.026311    1( b3u)   1( b3u)   +    +     -   +-   +-   +-    - 
 w   1  1   30723  0.021066    1( b3u)   2( b3u)   +    +     -   +-   +-   +-    - 
 w   1  1   30724  0.011654    2( b3u)   2( b3u)   +    +     -   +-   +-   +-    - 
 w   1  1   30880  0.010149    3( ag )   3( ag )   +         +-   +-   +-   +-    - 
 w   1  1   30936  0.017627    1( b3u)   1( b3u)   +         +-   +-   +-   +-    - 
 w   1  1   30937  0.019259    1( b3u)   2( b3u)   +         +-   +-   +-   +-    - 
 w   1  1   30938  0.014432    2( b3u)   2( b3u)   +         +-   +-   +-   +-    - 
 w   3  3   31258 -0.015456    1( b1u)   1( b1u)        +-   +-   +-   +-   +    +  
 w   3  3   31259 -0.015764    1( b1u)   2( b1u)        +-   +-   +-   +-   +    +  
 w   3  3   31260 -0.011198    2( b1u)   2( b1u)        +-   +-   +-   +-   +    +  
 w   3  3   31288 -0.010543    1( b3u)   1( b3u)        +-   +-   +-   +-   +    +  
 w   3  2   31426 -0.013205    1( b1u)   1( b1u)        +-   +-   +-   +    +-   +  
 w   3  2   31427 -0.013499    1( b1u)   2( b1u)        +-   +-   +-   +    +-   +  
 w   1  1   31762  0.023023    1( b1u)   1( b1u)        +-   +    +-   +-   +-    - 
 w   1  1   31763  0.020715    1( b1u)   2( b1u)        +-   +    +-   +-   +-    - 
 w   1  1   31764  0.013001    2( b1u)   2( b1u)        +-   +    +-   +-   +-    - 
 w   1  1   31777  0.018257    1( b2u)   1( b2u)        +-   +    +-   +-   +-    - 
 w   1  1   31778  0.013404    1( b2u)   2( b2u)        +-   +    +-   +-   +-    - 
 w   1  1   31792  0.017043    1( b3u)   1( b3u)        +-   +    +-   +-   +-    - 
 w   1  1   31793  0.012457    1( b3u)   2( b3u)        +-   +    +-   +-   +-    - 
 w   1  1   31976  0.022315    1( b1u)   1( b1u)        +    +-   +-   +-   +-    - 
 w   1  1   31977  0.020062    1( b1u)   2( b1u)        +    +-   +-   +-   +-    - 
 w   1  1   31978  0.012586    2( b1u)   2( b1u)        +    +-   +-   +-   +-    - 
 w   1  1   31991  0.013429    1( b2u)   1( b2u)        +    +-   +-   +-   +-    - 
 w   1  1   32006  0.021743    1( b3u)   1( b3u)        +    +-   +-   +-   +-    - 
 w   1  1   32007  0.019344    1( b3u)   2( b3u)        +    +-   +-   +-   +-    - 
 w   1  1   32008  0.012105    2( b3u)   2( b3u)        +    +-   +-   +-   +-    - 

 ci coefficient statistics:
           rq > 0.1                5
      0.1> rq > 0.01              84
     0.01> rq > 0.001           3613
    0.001> rq > 0.0001          6344
   0.0001> rq > 0.00001         8940
  0.00001> rq > 0.000001        9262
 0.000001> rq                   3910
           all                 32158
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:         126 2x:           0 4x:           0
All internal counts: zz :          15 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :          12 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:         193
  iref  icsf         v(icsf)             hv(icsf)
     1     3      0.007454665890     -1.462240194715
     2     4     -0.226145011083     44.361288952442
     3     5     -0.475408438407     93.257604619131
     4     6      0.534247341689   -104.799602305258

 number of reference csfs (nref) is     4.  root number (iroot) is  2.
 c0**2 =   0.56263054  c**2 (all zwalks) =   0.93974476

 pople ci energy extrapolation is computed with 12 correlated electrons.

 eref      =   -196.163089782100   "relaxed" cnot**2         =   0.562630543492
 eci       =   -196.472582798909   deltae = eci - eref       =  -0.309493016809
 eci+dv1   =   -196.607945591464   dv1 = (1-cnot**2)*deltae  =  -0.135362792555
 eci+dv2   =   -196.713171928171   dv2 = dv1 / cnot**2       =  -0.240589129262
 eci+dv3   =   -197.553228000180   dv3 = dv1 / (2*cnot**2-1) =  -1.080645201271
 eci+pople =   -196.797188791098   ( 12e- scaled deltae )    =  -0.634099008999


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 3) =      -196.4725827980

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     2    3    4   11   14   17

                                         symmetry   ag   ag   ag   b1g  b2g  b3g

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z   3  3       1 -0.469899                        +-   +-   +-   +-   +-   +    +  
 z   3  2       2  0.502322                        +-   +-   +-   +-   +    +-   +  
 z*  3  1       3 -0.659992                        +-   +-   +-   +    +-   +-   +  
 z*  1  1       4 -0.124342                        +-   +-   +    +-   +-   +-    - 
 z*  1  1       5  0.095106                        +-   +    +-   +-   +-   +-    - 
 z*  1  1       6  0.080731                        +    +-   +-   +-   +-   +-    - 
 y   3  2      51 -0.010238              1( b2g)   +-   +-   +-   +-        +-   +  
 y   3  1     109 -0.013451              1( b1g)   +-   +-   +-        +-   +-   +  
 y   3  3     121  0.010504              1( ag )   +-   +-    -   +-   +-   +    +  
 y   3  2     131 -0.011632              1( ag )   +-   +-    -   +-   +    +-   +  
 y   3  1     141  0.014826              1( ag )   +-   +-    -   +    +-   +-   +  
 y   3  2     323 -0.010062              1( ag )    -   +-   +-   +-   +    +-   +  
 y   3  1     333  0.013829              1( ag )    -   +-   +-   +    +-   +-   +  
 x   3  3    1137  0.010340    1( b1g)   1( b2g)   +-   +-   +-    -    -   +    +  
 x   3  1    1615  0.011478    1( b2g)   1( b3g)   +-   +-   +-   +     -    -   +  
 x   3  3    5231 -0.011460    3( ag )   1( b2g)   +-    -   +-   +-    -   +    +  
 x   3  3    5789  0.010362    3( ag )   1( b1g)   +-    -   +-    -   +-   +    +  
 x   3  2    5927 -0.010728    3( ag )   1( b1g)   +-    -   +-    -   +    +-   +  
 x   3  1    6071  0.010743    3( ag )   1( b3g)   +-    -   +-   +    +-    -   +  
 x   3  1    6253 -0.013445    3( ag )   1( b2g)   +-    -   +-   +     -   +-   +  
 x   3  1    6261 -0.011307    5( ag )   2( b2g)   +-    -   +-   +     -   +-   +  
 x   3  1    6290 -0.011932    4( b1u)   4( b3u)   +-    -   +-   +     -   +-   +  
 x   3  1   10906  0.011171    2( ag )   1( b3g)    -   +-   +-   +    +-    -   +  
 x   3  1   11831  0.010914    1( ag )   2( ag )    -   +-    -   +    +-   +-   +  
 w   3  2   17298  0.010021    1( b3g)   1( b3g)   +-   +-   +-   +-   +         +  
 w   3  1   17680 -0.013172    1( b3g)   1( b3g)   +-   +-   +-   +    +-        +  
 w   3  1   17681  0.011421    1( b3g)   2( b3g)   +-   +-   +-   +    +-        +  
 w   3  1   17682 -0.011234    2( b3g)   2( b3g)   +-   +-   +-   +    +-        +  
 w   3  1   18123  0.010290    1( b2g)   1( b3g)   +-   +-   +-   +    +     -   +  
 w   3  1   18319 -0.013170    1( b2g)   1( b2g)   +-   +-   +-   +         +-   +  
 w   3  1   18320  0.011420    1( b2g)   2( b2g)   +-   +-   +-   +         +-   +  
 w   3  1   18321 -0.011234    2( b2g)   2( b2g)   +-   +-   +-   +         +-   +  
 w   3  2   18790 -0.010013    1( b1g)   1( b1g)   +-   +-   +-        +    +-   +  
 w   3  3   20986  0.012437    1( b1u)   1( b1u)   +-   +-        +-   +-   +    +  
 w   3  3   21001  0.018713    1( b2u)   1( b2u)   +-   +-        +-   +-   +    +  
 w   3  3   21002  0.018845    1( b2u)   2( b2u)   +-   +-        +-   +-   +    +  
 w   3  3   21003  0.013217    2( b2u)   2( b2u)   +-   +-        +-   +-   +    +  
 w   3  3   21016  0.010693    1( b3u)   1( b3u)   +-   +-        +-   +-   +    +  
 w   3  2   21154 -0.013035    1( b1u)   1( b1u)   +-   +-        +-   +    +-   +  
 w   3  2   21169 -0.019541    1( b2u)   1( b2u)   +-   +-        +-   +    +-   +  
 w   3  2   21170 -0.019666    1( b2u)   2( b2u)   +-   +-        +-   +    +-   +  
 w   3  2   21171 -0.013885    2( b2u)   2( b2u)   +-   +-        +-   +    +-   +  
 w   3  2   21184 -0.012103    1( b3u)   1( b3u)   +-   +-        +-   +    +-   +  
 w   3  1   21322  0.016321    1( b1u)   1( b1u)   +-   +-        +    +-   +-   +  
 w   3  1   21323  0.011708    1( b1u)   2( b1u)   +-   +-        +    +-   +-   +  
 w   3  1   21337  0.026208    1( b2u)   1( b2u)   +-   +-        +    +-   +-   +  
 w   3  1   21338  0.026367    1( b2u)   2( b2u)   +-   +-        +    +-   +-   +  
 w   3  1   21339  0.018488    2( b2u)   2( b2u)   +-   +-        +    +-   +-   +  
 w   3  1   21352  0.016205    1( b3u)   1( b3u)   +-   +-        +    +-   +-   +  
 w   3  1   21353  0.011190    1( b3u)   2( b3u)   +-   +-        +    +-   +-   +  
 w   3  1   23005  0.010132    3( ag )   1( b2g)   +-   +    +-   +     -   +-   +  
 w   3  3   23569  0.010934    1( b2u)   1( b2u)   +-   +     -   +-   +-   +    +  
 w   3  3   23584  0.012547    1( b3u)   1( b3u)   +-   +     -   +-   +-   +    +  
 w   3  3   23585  0.011054    1( b3u)   2( b3u)   +-   +     -   +-   +-   +    +  
 w   3  2   23737 -0.011371    1( b2u)   1( b2u)   +-   +     -   +-   +    +-   +  
 w   3  2   23752 -0.013708    1( b3u)   1( b3u)   +-   +     -   +-   +    +-   +  
 w   3  2   23753 -0.012133    1( b3u)   2( b3u)   +-   +     -   +-   +    +-   +  
 w   3  1   23905  0.015790    1( b2u)   1( b2u)   +-   +     -   +    +-   +-   +  
 w   3  1   23906  0.012946    1( b2u)   2( b2u)   +-   +     -   +    +-   +-   +  
 w   3  1   23920  0.018289    1( b3u)   1( b3u)   +-   +     -   +    +-   +-   +  
 w   3  1   23921  0.016288    1( b3u)   2( b3u)   +-   +     -   +    +-   +-   +  
 w   3  1   25362  0.012533    3( ag )   3( ag )   +-        +-   +    +-   +-   +  
 w   3  1   25418  0.010801    1( b3u)   1( b3u)   +-        +-   +    +-   +-   +  
 w   3  1   25419  0.013252    1( b3u)   2( b3u)   +-        +-   +    +-   +-   +  
 w   3  1   25420  0.010896    2( b3u)   2( b3u)   +-        +-   +    +-   +-   +  
 w   3  3   27834  0.019562    1( b1u)   1( b1u)   +    +-    -   +-   +-   +    +  
 w   3  3   27835  0.016692    1( b1u)   2( b1u)   +    +-    -   +-   +-   +    +  
 w   3  3   27849  0.017298    1( b2u)   1( b2u)   +    +-    -   +-   +-   +    +  
 w   3  3   27850  0.013022    1( b2u)   2( b2u)   +    +-    -   +-   +-   +    +  
 w   3  3   27864  0.015128    1( b3u)   1( b3u)   +    +-    -   +-   +-   +    +  
 w   3  3   27865  0.010787    1( b3u)   2( b3u)   +    +-    -   +-   +-   +    +  
 w   3  2   28002 -0.020790    1( b1u)   1( b1u)   +    +-    -   +-   +    +-   +  
 w   3  2   28003 -0.017654    1( b1u)   2( b1u)   +    +-    -   +-   +    +-   +  
 w   3  2   28004 -0.010357    2( b1u)   2( b1u)   +    +-    -   +-   +    +-   +  
 w   3  2   28017 -0.017529    1( b2u)   1( b2u)   +    +-    -   +-   +    +-   +  
 w   3  2   28018 -0.012863    1( b2u)   2( b2u)   +    +-    -   +-   +    +-   +  
 w   3  2   28032 -0.017125    1( b3u)   1( b3u)   +    +-    -   +-   +    +-   +  
 w   3  2   28033 -0.012489    1( b3u)   2( b3u)   +    +-    -   +-   +    +-   +  
 w   3  1   28170  0.026398    1( b1u)   1( b1u)   +    +-    -   +    +-   +-   +  
 w   3  1   28171  0.022264    1( b1u)   2( b1u)   +    +-    -   +    +-   +-   +  
 w   3  1   28172  0.013146    2( b1u)   2( b1u)   +    +-    -   +    +-   +-   +  
 w   3  1   28185  0.024064    1( b2u)   1( b2u)   +    +-    -   +    +-   +-   +  
 w   3  1   28186  0.018007    1( b2u)   2( b2u)   +    +-    -   +    +-   +-   +  
 w   3  1   28200  0.022436    1( b3u)   1( b3u)   +    +-    -   +    +-   +-   +  
 w   3  1   28201  0.016384    1( b3u)   2( b3u)   +    +-    -   +    +-   +-   +  
 w   3  3   29362  0.012635    1( b3u)   1( b3u)   +     -   +-   +-   +-   +    +  
 w   3  3   29363  0.012257    1( b3u)   2( b3u)   +     -   +-   +-   +-   +    +  
 w   3  2   29530 -0.013870    1( b3u)   1( b3u)   +     -   +-   +-   +    +-   +  
 w   3  2   29531 -0.013512    1( b3u)   2( b3u)   +     -   +-   +-   +    +-   +  
 w   3  1   29668  0.010689    1( b1u)   1( b1u)   +     -   +-   +    +-   +-   +  
 w   3  1   29683  0.010561    1( b2u)   1( b2u)   +     -   +-   +    +-   +-   +  
 w   3  1   29698  0.018052    1( b3u)   1( b3u)   +     -   +-   +    +-   +-   +  
 w   3  1   29699  0.017477    1( b3u)   2( b3u)   +     -   +-   +    +-   +-   +  
 w   3  1   29700  0.011502    2( b3u)   2( b3u)   +     -   +-   +    +-   +-   +  
 w   3  3   31258  0.015507    1( b1u)   1( b1u)        +-   +-   +-   +-   +    +  
 w   3  3   31259  0.015831    1( b1u)   2( b1u)        +-   +-   +-   +-   +    +  
 w   3  3   31260  0.011250    2( b1u)   2( b1u)        +-   +-   +-   +-   +    +  
 w   3  3   31288  0.010569    1( b3u)   1( b3u)        +-   +-   +-   +-   +    +  
 w   3  2   31426 -0.016745    1( b1u)   1( b1u)        +-   +-   +-   +    +-   +  
 w   3  2   31427 -0.017126    1( b1u)   2( b1u)        +-   +-   +-   +    +-   +  
 w   3  2   31428 -0.012174    2( b1u)   2( b1u)        +-   +-   +-   +    +-   +  
 w   3  2   31456 -0.012018    1( b3u)   1( b3u)        +-   +-   +-   +    +-   +  
 w   3  1   31594  0.021444    1( b1u)   1( b1u)        +-   +-   +    +-   +-   +  
 w   3  1   31595  0.021913    1( b1u)   2( b1u)        +-   +-   +    +-   +-   +  
 w   3  1   31596  0.015673    2( b1u)   2( b1u)        +-   +-   +    +-   +-   +  
 w   3  1   31609  0.011439    1( b2u)   1( b2u)        +-   +-   +    +-   +-   +  
 w   3  1   31624  0.015383    1( b3u)   1( b3u)        +-   +-   +    +-   +-   +  
 w   3  1   31625  0.012332    1( b3u)   2( b3u)        +-   +-   +    +-   +-   +  

 ci coefficient statistics:
           rq > 0.1                4
      0.1> rq > 0.01             104
     0.01> rq > 0.001           3159
    0.001> rq > 0.0001          8091
   0.0001> rq > 0.00001        11183
  0.00001> rq > 0.000001        8053
 0.000001> rq                   1564
           all                 32158
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:         126 2x:           0 4x:           0
All internal counts: zz :          15 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :          12 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:         193
  iref  icsf         v(icsf)             hv(icsf)
     1     3     -0.659992359897    129.466688231258
     2     4     -0.124342104105     24.392721506711
     3     5      0.095105640257    -18.657570893430
     4     6      0.080731030221    -15.837176769850

 number of reference csfs (nref) is     4.  root number (iroot) is  3.
 c0**2 =   0.46661346  c**2 (all zwalks) =   0.93974582

 warning: cnot**2 < 0.5, dv3 is not computed.

 warning: pople ci energy extrapolation is not computed because of small fac.
 fac =      -0.066773087951

 eref      =   -196.164637026290   "relaxed" cnot**2         =   0.466613456025
 eci       =   -196.472582798043   deltae = eci - eref       =  -0.307945771753
 eci+dv1   =   -196.636836928971   dv1 = (1-cnot**2)*deltae  =  -0.164254130927
 eci+dv2   =   -196.824596073285   dv2 = dv1 / cnot**2       =  -0.352013275242
 eci+dv3   =   -196.472582798043   dv3 = dv1 / (2*cnot**2-1) =   0.000000000000
 eci+pople =   -196.164637026290   ( 12e- scaled deltae )    =   0.000000000000


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 4) =      -196.4725827977

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     2    3    4   11   14   17

                                         symmetry   ag   ag   ag   b1g  b2g  b3g

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z   3  3       1 -0.351000                        +-   +-   +-   +-   +-   +    +  
 z   3  2       2  0.392746                        +-   +-   +-   +-   +    +-   +  
 z*  3  1       3  0.358051                        +-   +-   +-   +    +-   +-   +  
 z*  1  1       4  0.479443                        +-   +-   +    +-   +-   +-    - 
 z*  1  1       5 -0.500787                        +-   +    +-   +-   +-   +-    - 
 z*  1  1       6 -0.231170                        +    +-   +-   +-   +-   +-    - 
 y   1  1     127 -0.012216              1( b2g)   +-   +-    -   +-   +    +-    - 
 y   1  1     187 -0.013066              1( ag )   +-   +-        +-   +-   +-    - 
 y   1  1     199 -0.012241              1( b3g)   +-    -   +-   +-   +-   +     - 
 y   1  1     285 -0.014791              3( ag )   +-   +     -   +-   +-   +-    - 
 y   1  1     343 -0.010797              1( ag )    -   +-   +    +-   +-   +-    - 
 w   3  3   21001  0.013959    1( b2u)   1( b2u)   +-   +-        +-   +-   +    +  
 w   3  3   21002  0.014046    1( b2u)   2( b2u)   +-   +-        +-   +-   +    +  
 w   3  2   21154 -0.010171    1( b1u)   1( b1u)   +-   +-        +-   +    +-   +  
 w   3  2   21169 -0.015274    1( b2u)   1( b2u)   +-   +-        +-   +    +-   +  
 w   3  2   21170 -0.015371    1( b2u)   2( b2u)   +-   +-        +-   +    +-   +  
 w   3  2   21171 -0.010852    2( b2u)   2( b2u)   +-   +-        +-   +    +-   +  
 w   3  1   21337 -0.014204    1( b2u)   1( b2u)   +-   +-        +    +-   +-   +  
 w   3  1   21338 -0.014286    1( b2u)   2( b2u)   +-   +-        +    +-   +-   +  
 w   3  1   21339 -0.010012    2( b2u)   2( b2u)   +-   +-        +    +-   +-   +  
 w   1  1   21486 -0.010338    1( b3g)   1( b3g)   +-   +    +-   +-   +-         - 
 w   3  2   23752 -0.010716    1( b3u)   1( b3u)   +-   +     -   +-   +    +-   +  
 w   1  1   24482 -0.010455    2( b2u)   2( b3u)   +-   +    +     -   +-   +-    - 
 w   1  1   24669 -0.010054    1( ag )   1( ag )   +-   +         +-   +-   +-    - 
 w   1  1   24700 -0.017187    1( b1u)   1( b1u)   +-   +         +-   +-   +-    - 
 w   1  1   24701 -0.011210    1( b1u)   2( b1u)   +-   +         +-   +-   +-    - 
 w   1  1   24715 -0.027536    1( b2u)   1( b2u)   +-   +         +-   +-   +-    - 
 w   1  1   24716 -0.026101    1( b2u)   2( b2u)   +-   +         +-   +-   +-    - 
 w   1  1   24717 -0.017386    2( b2u)   2( b2u)   +-   +         +-   +-   +-    - 
 w   1  1   24730 -0.022247    1( b3u)   1( b3u)   +-   +         +-   +-   +-    - 
 w   1  1   24731 -0.017550    1( b3u)   2( b3u)   +-   +         +-   +-   +-    - 
 w   1  1   25571 -0.012002    1( b2u)   1( b2u)   +-        +    +-   +-   +-    - 
 w   1  1   25586 -0.017430    1( b3u)   1( b3u)   +-        +    +-   +-   +-    - 
 w   1  1   25587 -0.017992    1( b3u)   2( b3u)   +-        +    +-   +-   +-    - 
 w   1  1   25588 -0.012727    2( b3u)   2( b3u)   +-        +    +-   +-   +-    - 
 w   3  3   27834  0.014620    1( b1u)   1( b1u)   +    +-    -   +-   +-   +    +  
 w   3  3   27835  0.012480    1( b1u)   2( b1u)   +    +-    -   +-   +-   +    +  
 w   3  3   27849  0.012926    1( b2u)   1( b2u)   +    +-    -   +-   +-   +    +  
 w   3  3   27864  0.011293    1( b3u)   1( b3u)   +    +-    -   +-   +-   +    +  
 w   3  2   28002 -0.016246    1( b1u)   1( b1u)   +    +-    -   +-   +    +-   +  
 w   3  2   28003 -0.013789    1( b1u)   2( b1u)   +    +-    -   +-   +    +-   +  
 w   3  2   28017 -0.013701    1( b2u)   1( b2u)   +    +-    -   +-   +    +-   +  
 w   3  2   28018 -0.010051    1( b2u)   2( b2u)   +    +-    -   +-   +    +-   +  
 w   3  2   28032 -0.013398    1( b3u)   1( b3u)   +    +-    -   +-   +    +-   +  
 w   3  1   28170 -0.014314    1( b1u)   1( b1u)   +    +-    -   +    +-   +-   +  
 w   3  1   28171 -0.012075    1( b1u)   2( b1u)   +    +-    -   +    +-   +-   +  
 w   3  1   28185 -0.013055    1( b2u)   1( b2u)   +    +-    -   +    +-   +-   +  
 w   3  1   28200 -0.012176    1( b3u)   1( b3u)   +    +-    -   +    +-   +-   +  
 w   1  1   28980 -0.019546    1( b1u)   1( b1u)   +    +-        +-   +-   +-    - 
 w   1  1   28981 -0.015886    1( b1u)   2( b1u)   +    +-        +-   +-   +-    - 
 w   1  1   28995 -0.021692    1( b2u)   1( b2u)   +    +-        +-   +-   +-    - 
 w   1  1   28996 -0.018520    1( b2u)   2( b2u)   +    +-        +-   +-   +-    - 
 w   1  1   28997 -0.010936    2( b2u)   2( b2u)   +    +-        +-   +-   +-    - 
 w   1  1   29010 -0.016919    1( b3u)   1( b3u)   +    +-        +-   +-   +-    - 
 w   1  1   29011 -0.012094    1( b3u)   2( b3u)   +    +-        +-   +-   +-    - 
 w   3  2   29530 -0.010826    1( b3u)   1( b3u)   +     -   +-   +-   +    +-   +  
 w   3  2   29531 -0.010541    1( b3u)   2( b3u)   +     -   +-   +-   +    +-   +  
 w   1  1   29836 -0.019284    1( b1u)   1( b1u)   +     -   +    +-   +-   +-    - 
 w   1  1   29837 -0.013601    1( b1u)   2( b1u)   +     -   +    +-   +-   +-    - 
 w   1  1   29851 -0.019730    1( b2u)   1( b2u)   +     -   +    +-   +-   +-    - 
 w   1  1   29852 -0.014209    1( b2u)   2( b2u)   +     -   +    +-   +-   +-    - 
 w   1  1   29866 -0.024810    1( b3u)   1( b3u)   +     -   +    +-   +-   +-    - 
 w   1  1   29867 -0.021696    1( b3u)   2( b3u)   +     -   +    +-   +-   +-    - 
 w   1  1   29868 -0.013156    2( b3u)   2( b3u)   +     -   +    +-   +-   +-    - 
 w   1  1   30692  0.014377    1( b1u)   1( b1u)   +    +     -   +-   +-   +-    - 
 w   1  1   30693  0.013075    1( b1u)   2( b1u)   +    +     -   +-   +-   +-    - 
 w   1  1   30707  0.010278    1( b2u)   1( b2u)   +    +     -   +-   +-   +-    - 
 w   3  3   31258  0.011571    1( b1u)   1( b1u)        +-   +-   +-   +-   +    +  
 w   3  3   31259  0.011806    1( b1u)   2( b1u)        +-   +-   +-   +-   +    +  
 w   3  2   31426 -0.013103    1( b1u)   1( b1u)        +-   +-   +-   +    +-   +  
 w   3  2   31427 -0.013407    1( b1u)   2( b1u)        +-   +-   +-   +    +-   +  
 w   3  1   31594 -0.011630    1( b1u)   1( b1u)        +-   +-   +    +-   +-   +  
 w   3  1   31595 -0.011885    1( b1u)   2( b1u)        +-   +-   +    +-   +-   +  
 w   1  1   31762 -0.021902    1( b1u)   1( b1u)        +-   +    +-   +-   +-    - 
 w   1  1   31763 -0.021178    1( b1u)   2( b1u)        +-   +    +-   +-   +-    - 
 w   1  1   31764 -0.014468    2( b1u)   2( b1u)        +-   +    +-   +-   +-    - 
 w   1  1   31777 -0.015049    1( b2u)   1( b2u)        +-   +    +-   +-   +-    - 
 w   1  1   31778 -0.010417    1( b2u)   2( b2u)        +-   +    +-   +-   +-    - 
 w   1  1   31792 -0.016101    1( b3u)   1( b3u)        +-   +    +-   +-   +-    - 
 w   1  1   31793 -0.012300    1( b3u)   2( b3u)        +-   +    +-   +-   +-    - 
 w   1  1   31976  0.013847    1( b1u)   1( b1u)        +    +-   +-   +-   +-    - 
 w   1  1   31977  0.015354    1( b1u)   2( b1u)        +    +-   +-   +-   +-    - 
 w   1  1   31978  0.011648    2( b1u)   2( b1u)        +    +-   +-   +-   +-    - 

 ci coefficient statistics:
           rq > 0.1                6
      0.1> rq > 0.01              77
     0.01> rq > 0.001           4183
    0.001> rq > 0.0001          7875
   0.0001> rq > 0.00001         9402
  0.00001> rq > 0.000001        8912
 0.000001> rq                   1703
           all                 32158
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:         126 2x:           0 4x:           0
All internal counts: zz :          15 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :          12 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:         193
  iref  icsf         v(icsf)             hv(icsf)
     1     3      0.358050887192    -70.238661625068
     2     4      0.479443210547    -94.049816680414
     3     5     -0.500787407905     98.236741258053
     4     6     -0.231170286225     45.347473279766

 number of reference csfs (nref) is     4.  root number (iroot) is  4.
 c0**2 =   0.66229396  c**2 (all zwalks) =   0.93974428

 pople ci energy extrapolation is computed with 12 correlated electrons.

 eref      =   -196.165570857639   "relaxed" cnot**2         =   0.662293959108
 eci       =   -196.472582797657   deltae = eci - eref       =  -0.307011940017
 eci+dv1   =   -196.576262584427   dv1 = (1-cnot**2)*deltae  =  -0.103679786770
 eci+dv2   =   -196.629129275764   dv2 = dv1 / cnot**2       =  -0.156546478107
 eci+dv3   =   -196.792002532501   dv3 = dv1 / (2*cnot**2-1) =  -0.319419734844
 eci+pople =   -196.656099589109   ( 12e- scaled deltae )    =  -0.490528731470


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 5) =      -196.4644811587

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     2    3    4   11   14   17

                                         symmetry   ag   ag   ag   b1g  b2g  b3g

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z   3  3       1 -0.571526                        +-   +-   +-   +-   +-   +    +  
 z   3  2       2 -0.478000                        +-   +-   +-   +-   +    +-   +  
 z*  3  1       3  0.093525                        +-   +-   +-   +    +-   +-   +  
 z*  1  1       4 -0.229984                        +-   +-   +    +-   +-   +-    - 
 z*  1  1       5 -0.338395                        +-   +    +-   +-   +-   +-    - 
 z*  1  1       6  0.456626                        +    +-   +-   +-   +-   +-    - 
 y   3  3      17 -0.011874              1( b3g)   +-   +-   +-   +-   +-        +  
 y   3  3     121  0.012832              1( ag )   +-   +-    -   +-   +-   +    +  
 y   3  2     131  0.011119              1( ag )   +-   +-    -   +-   +    +-   +  
 y   3  3     313  0.011735              1( ag )    -   +-   +-   +-   +-   +    +  
 y   1  1     331  0.011370              1( b1g)    -   +-   +-   +    +-   +-    - 
 y   1  1     427 -0.011388              1( ag )        +-   +-   +-   +-   +-    - 
 x   3  3    1137  0.012587    1( b1g)   1( b2g)   +-   +-   +-    -    -   +    +  
 x   3  3    1140  0.010017    2( b1g)   2( b2g)   +-   +-   +-    -    -   +    +  
 x   3  3    5231 -0.013947    3( ag )   1( b2g)   +-    -   +-   +-    -   +    +  
 x   3  3    5239 -0.011227    5( ag )   2( b2g)   +-    -   +-   +-    -   +    +  
 x   3  3    5268 -0.011889    4( b1u)   4( b3u)   +-    -   +-   +-    -   +    +  
 x   3  3    5789  0.012602    3( ag )   1( b1g)   +-    -   +-    -   +-   +    +  
 x   3  3    5797  0.010053    5( ag )   2( b1g)   +-    -   +-    -   +-   +    +  
 x   3  2    5927  0.010202    3( ag )   1( b1g)   +-    -   +-    -   +    +-   +  
 w   3  3   17555  0.011425    1( b2g)   1( b2g)   +-   +-   +-   +-        +    +  
 w   3  3   17989 -0.010297    1( b1g)   1( b2g)   +-   +-   +-   +     -   +    +  
 w   3  3   18622  0.011409    1( b1g)   1( b1g)   +-   +-   +-        +-   +    +  
 w   3  3   20986  0.015132    1( b1u)   1( b1u)   +-   +-        +-   +-   +    +  
 w   3  3   20987  0.011188    1( b1u)   2( b1u)   +-   +-        +-   +-   +    +  
 w   3  3   21001  0.022764    1( b2u)   1( b2u)   +-   +-        +-   +-   +    +  
 w   3  3   21002  0.022921    1( b2u)   2( b2u)   +-   +-        +-   +-   +    +  
 w   3  3   21003  0.016075    2( b2u)   2( b2u)   +-   +-        +-   +-   +    +  
 w   3  3   21016  0.013004    1( b3u)   1( b3u)   +-   +-        +-   +-   +    +  
 w   3  2   21154  0.012393    1( b1u)   1( b1u)   +-   +-        +-   +    +-   +  
 w   3  2   21169  0.018586    1( b2u)   1( b2u)   +-   +-        +-   +    +-   +  
 w   3  2   21170  0.018704    1( b2u)   2( b2u)   +-   +-        +-   +    +-   +  
 w   3  2   21171  0.013206    2( b2u)   2( b2u)   +-   +-        +-   +    +-   +  
 w   3  2   21184  0.011538    1( b3u)   1( b3u)   +-   +-        +-   +    +-   +  
 w   3  3   21843  0.010348    3( ag )   1( b2g)   +-   +    +-   +-    -   +    +  
 w   3  3   22485 -0.010321    3( ag )   1( b1g)   +-   +    +-    -   +-   +    +  
 w   3  3   23569  0.013317    1( b2u)   1( b2u)   +-   +     -   +-   +-   +    +  
 w   3  3   23570  0.010766    1( b2u)   2( b2u)   +-   +     -   +-   +-   +    +  
 w   3  3   23584  0.015255    1( b3u)   1( b3u)   +-   +     -   +-   +-   +    +  
 w   3  3   23585  0.013438    1( b3u)   2( b3u)   +-   +     -   +-   +-   +    +  
 w   3  2   23737  0.010819    1( b2u)   1( b2u)   +-   +     -   +-   +    +-   +  
 w   3  2   23752  0.013058    1( b3u)   1( b3u)   +-   +     -   +-   +    +-   +  
 w   3  2   23753  0.011564    1( b3u)   2( b3u)   +-   +     -   +-   +    +-   +  
 w   1  1   24716 -0.010033    1( b2u)   2( b2u)   +-   +         +-   +-   +-    - 
 w   3  3   25026  0.011263    3( ag )   3( ag )   +-        +-   +-   +-   +    +  
 w   3  3   25083  0.011412    1( b3u)   2( b3u)   +-        +-   +-   +-   +    +  
 w   3  3   27834  0.023804    1( b1u)   1( b1u)   +    +-    -   +-   +-   +    +  
 w   3  3   27835  0.020312    1( b1u)   2( b1u)   +    +-    -   +-   +-   +    +  
 w   3  3   27836  0.011976    2( b1u)   2( b1u)   +    +-    -   +-   +-   +    +  
 w   3  3   27849  0.021043    1( b2u)   1( b2u)   +    +-    -   +-   +-   +    +  
 w   3  3   27850  0.015835    1( b2u)   2( b2u)   +    +-    -   +-   +-   +    +  
 w   3  3   27864  0.018396    1( b3u)   1( b3u)   +    +-    -   +-   +-   +    +  
 w   3  3   27865  0.013115    1( b3u)   2( b3u)   +    +-    -   +-   +-   +    +  
 w   3  2   28002  0.019787    1( b1u)   1( b1u)   +    +-    -   +-   +    +-   +  
 w   3  2   28003  0.016803    1( b1u)   2( b1u)   +    +-    -   +-   +    +-   +  
 w   3  2   28017  0.016674    1( b2u)   1( b2u)   +    +-    -   +-   +    +-   +  
 w   3  2   28018  0.012234    1( b2u)   2( b2u)   +    +-    -   +-   +    +-   +  
 w   3  2   28032  0.016303    1( b3u)   1( b3u)   +    +-    -   +-   +    +-   +  
 w   3  2   28033  0.011890    1( b3u)   2( b3u)   +    +-    -   +-   +    +-   +  
 w   1  1   28980  0.019182    1( b1u)   1( b1u)   +    +-        +-   +-   +-    - 
 w   1  1   28981  0.015126    1( b1u)   2( b1u)   +    +-        +-   +-   +-    - 
 w   1  1   28995  0.024047    1( b2u)   1( b2u)   +    +-        +-   +-   +-    - 
 w   1  1   28996  0.022615    1( b2u)   2( b2u)   +    +-        +-   +-   +-    - 
 w   1  1   28997  0.014902    2( b2u)   2( b2u)   +    +-        +-   +-   +-    - 
 w   1  1   29010  0.015482    1( b3u)   1( b3u)   +    +-        +-   +-   +-    - 
 w   1  1   29011  0.010479    1( b3u)   2( b3u)   +    +-        +-   +-   +-    - 
 w   3  3   29362  0.015361    1( b3u)   1( b3u)   +     -   +-   +-   +-   +    +  
 w   3  3   29363  0.014898    1( b3u)   2( b3u)   +     -   +-   +-   +-   +    +  
 w   3  2   29530  0.013195    1( b3u)   1( b3u)   +     -   +-   +-   +    +-   +  
 w   3  2   29531  0.012849    1( b3u)   2( b3u)   +     -   +-   +-   +    +-   +  
 w   1  1   30692  0.018894    1( b1u)   1( b1u)   +    +     -   +-   +-   +-    - 
 w   1  1   30693  0.014276    1( b1u)   2( b1u)   +    +     -   +-   +-   +-    - 
 w   1  1   30707  0.019057    1( b2u)   1( b2u)   +    +     -   +-   +-   +-    - 
 w   1  1   30708  0.014514    1( b2u)   2( b2u)   +    +     -   +-   +-   +-    - 
 w   1  1   30722  0.020446    1( b3u)   1( b3u)   +    +     -   +-   +-   +-    - 
 w   1  1   30723  0.016499    1( b3u)   2( b3u)   +    +     -   +-   +-   +-    - 
 w   1  1   30936  0.013767    1( b3u)   1( b3u)   +         +-   +-   +-   +-    - 
 w   1  1   30937  0.015213    1( b3u)   2( b3u)   +         +-   +-   +-   +-    - 
 w   1  1   30938  0.011519    2( b3u)   2( b3u)   +         +-   +-   +-   +-    - 
 w   3  3   31258  0.018872    1( b1u)   1( b1u)        +-   +-   +-   +-   +    +  
 w   3  3   31259  0.019267    1( b1u)   2( b1u)        +-   +-   +-   +-   +    +  
 w   3  3   31260  0.013694    2( b1u)   2( b1u)        +-   +-   +-   +-   +    +  
 w   3  3   31273  0.010052    1( b2u)   1( b2u)        +-   +-   +-   +-   +    +  
 w   3  3   31288  0.012851    1( b3u)   1( b3u)        +-   +-   +-   +-   +    +  
 w   3  3   31289  0.010163    1( b3u)   2( b3u)        +-   +-   +-   +-   +    +  
 w   3  2   31426  0.015953    1( b1u)   1( b1u)        +-   +-   +-   +    +-   +  
 w   3  2   31427  0.016324    1( b1u)   2( b1u)        +-   +-   +-   +    +-   +  
 w   3  2   31428  0.011607    2( b1u)   2( b1u)        +-   +-   +-   +    +-   +  
 w   3  2   31456  0.011426    1( b3u)   1( b3u)        +-   +-   +-   +    +-   +  
 w   1  1   31762  0.020830    1( b1u)   1( b1u)        +-   +    +-   +-   +-    - 
 w   1  1   31763  0.018880    1( b1u)   2( b1u)        +-   +    +-   +-   +-    - 
 w   1  1   31764  0.011960    2( b1u)   2( b1u)        +-   +    +-   +-   +-    - 
 w   1  1   31777  0.016303    1( b2u)   1( b2u)        +-   +    +-   +-   +-    - 
 w   1  1   31778  0.011913    1( b2u)   2( b2u)        +-   +    +-   +-   +-    - 
 w   1  1   31792  0.015406    1( b3u)   1( b3u)        +-   +    +-   +-   +-    - 
 w   1  1   31793  0.011311    1( b3u)   2( b3u)        +-   +    +-   +-   +-    - 
 w   1  1   31976  0.016816    1( b1u)   1( b1u)        +    +-   +-   +-   +-    - 
 w   1  1   31977  0.014835    1( b1u)   2( b1u)        +    +-   +-   +-   +-    - 
 w   1  1   31991  0.010411    1( b2u)   1( b2u)        +    +-   +-   +-   +-    - 
 w   1  1   32006  0.016941    1( b3u)   1( b3u)        +    +-   +-   +-   +-    - 
 w   1  1   32007  0.015195    1( b3u)   2( b3u)        +    +-   +-   +-   +-    - 

 ci coefficient statistics:
           rq > 0.1                5
      0.1> rq > 0.01              96
     0.01> rq > 0.001           3650
    0.001> rq > 0.0001          7766
   0.0001> rq > 0.00001         9470
  0.00001> rq > 0.000001        8816
 0.000001> rq                   2355
           all                 32158
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:         126 2x:           0 4x:           0
All internal counts: zz :          15 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :          12 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:         193
  iref  icsf         v(icsf)             hv(icsf)
     1     3      0.093524667137    -18.345794169755
     2     4     -0.229984472538     45.114268319909
     3     5     -0.338394648060     66.380732460312
     4     6      0.456626078894    -89.573080414146

 number of reference csfs (nref) is     4.  root number (iroot) is  5.
 c0**2 =   0.38465803  c**2 (all zwalks) =   0.93978441

 warning: cnot**2 < 0.5, dv3 is not computed.

 warning: pople ci energy extrapolation is not computed because of small fac.
 fac =      -0.230683930532

 eref      =   -196.162949336750   "relaxed" cnot**2         =   0.384658034734
 eci       =   -196.464481158693   deltae = eci - eref       =  -0.301531821943
 eci+dv1   =   -196.650026342598   dv1 = (1-cnot**2)*deltae  =  -0.185545183905
 eci+dv2   =   -196.946845147355   dv2 = dv1 / cnot**2       =  -0.482363988661
 eci+dv3   =   -196.464481158693   dv3 = dv1 / (2*cnot**2-1) =   0.000000000000
 eci+pople =   -196.162949336750   ( 12e- scaled deltae )    =   0.000000000000


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 6) =      -196.4644811574

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     2    3    4   11   14   17

                                         symmetry   ag   ag   ag   b1g  b2g  b3g

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z   3  3       1  0.221976                        +-   +-   +-   +-   +-   +    +  
 z   3  2       2 -0.383969                        +-   +-   +-   +-   +    +-   +  
 z*  3  1       3 -0.605943                        +-   +-   +-   +    +-   +-   +  
 z*  1  1       4  0.380476                        +-   +-   +    +-   +-   +-    - 
 z*  1  1       5 -0.457603                        +-   +    +-   +-   +-   +-    - 
 z*  1  1       6 -0.147489                        +    +-   +-   +-   +-   +-    - 
 y   3  1     109 -0.012590              1( b1g)   +-   +-   +-        +-   +-   +  
 y   3  1     141  0.013677              1( ag )   +-   +-    -   +    +-   +-   +  
 y   1  1     187 -0.010466              1( ag )   +-   +-        +-   +-   +-    - 
 y   1  1     199 -0.010767              1( b3g)   +-    -   +-   +-   +-   +     - 
 y   1  1     285 -0.012329              3( ag )   +-   +     -   +-   +-   +-    - 
 y   3  1     333  0.012765              1( ag )    -   +-   +-   +    +-   +-   +  
 x   3  1    1615  0.010541    1( b2g)   1( b3g)   +-   +-   +-   +     -    -   +  
 x   3  1    6253 -0.012356    3( ag )   1( b2g)   +-    -   +-   +     -   +-   +  
 x   3  1    6261 -0.010376    5( ag )   2( b2g)   +-    -   +-   +     -   +-   +  
 x   3  1    6290 -0.010956    4( b1u)   4( b3u)   +-    -   +-   +     -   +-   +  
 x   3  1   10906  0.010256    2( ag )   1( b3g)    -   +-   +-   +    +-    -   +  
 x   3  1   11831  0.010025    1( ag )   2( ag )    -   +-    -   +    +-   +-   +  
 w   3  1   17680 -0.012101    1( b3g)   1( b3g)   +-   +-   +-   +    +-        +  
 w   3  1   17681  0.010490    1( b3g)   2( b3g)   +-   +-   +-   +    +-        +  
 w   3  1   17682 -0.010314    2( b3g)   2( b3g)   +-   +-   +-   +    +-        +  
 w   3  1   18319 -0.012108    1( b2g)   1( b2g)   +-   +-   +-   +         +-   +  
 w   3  1   18320  0.010493    1( b2g)   2( b2g)   +-   +-   +-   +         +-   +  
 w   3  1   18321 -0.010314    2( b2g)   2( b2g)   +-   +-   +-   +         +-   +  
 w   3  2   21169  0.014926    1( b2u)   1( b2u)   +-   +-        +-   +    +-   +  
 w   3  2   21170  0.015024    1( b2u)   2( b2u)   +-   +-        +-   +    +-   +  
 w   3  2   21171  0.010609    2( b2u)   2( b2u)   +-   +-        +-   +    +-   +  
 w   3  1   21322  0.014979    1( b1u)   1( b1u)   +-   +-        +    +-   +-   +  
 w   3  1   21323  0.010749    1( b1u)   2( b1u)   +-   +-        +    +-   +-   +  
 w   3  1   21337  0.024080    1( b2u)   1( b2u)   +-   +-        +    +-   +-   +  
 w   3  1   21338  0.024233    1( b2u)   2( b2u)   +-   +-        +    +-   +-   +  
 w   3  1   21339  0.016995    2( b2u)   2( b2u)   +-   +-        +    +-   +-   +  
 w   3  1   21352  0.014869    1( b3u)   1( b3u)   +-   +-        +    +-   +-   +  
 w   3  1   21353  0.010262    1( b3u)   2( b3u)   +-   +-        +    +-   +-   +  
 w   3  2   23752  0.010475    1( b3u)   1( b3u)   +-   +     -   +-   +    +-   +  
 w   3  1   23905  0.014491    1( b2u)   1( b2u)   +-   +     -   +    +-   +-   +  
 w   3  1   23906  0.011875    1( b2u)   2( b2u)   +-   +     -   +    +-   +-   +  
 w   3  1   23920  0.016795    1( b3u)   1( b3u)   +-   +     -   +    +-   +-   +  
 w   3  1   23921  0.014957    1( b3u)   2( b3u)   +-   +     -   +    +-   +-   +  
 w   1  1   24700 -0.015170    1( b1u)   1( b1u)   +-   +         +-   +-   +-    - 
 w   1  1   24701 -0.010010    1( b1u)   2( b1u)   +-   +         +-   +-   +-    - 
 w   1  1   24715 -0.024180    1( b2u)   1( b2u)   +-   +         +-   +-   +-    - 
 w   1  1   24716 -0.023054    1( b2u)   2( b2u)   +-   +         +-   +-   +-    - 
 w   1  1   24717 -0.015451    2( b2u)   2( b2u)   +-   +         +-   +-   +-    - 
 w   1  1   24730 -0.019149    1( b3u)   1( b3u)   +-   +         +-   +-   +-    - 
 w   1  1   24731 -0.014970    1( b3u)   2( b3u)   +-   +         +-   +-   +-    - 
 w   3  1   25362  0.011517    3( ag )   3( ag )   +-        +-   +    +-   +-   +  
 w   3  1   25419  0.012184    1( b3u)   2( b3u)   +-        +-   +    +-   +-   +  
 w   3  1   25420  0.010018    2( b3u)   2( b3u)   +-        +-   +    +-   +-   +  
 w   1  1   25571 -0.010508    1( b2u)   1( b2u)   +-        +    +-   +-   +-    - 
 w   1  1   25586 -0.014963    1( b3u)   1( b3u)   +-        +    +-   +-   +-    - 
 w   1  1   25587 -0.015258    1( b3u)   2( b3u)   +-        +    +-   +-   +-    - 
 w   1  1   25588 -0.010662    2( b3u)   2( b3u)   +-        +    +-   +-   +-    - 
 w   3  2   28002  0.015895    1( b1u)   1( b1u)   +    +-    -   +-   +    +-   +  
 w   3  2   28003  0.013504    1( b1u)   2( b1u)   +    +-    -   +-   +    +-   +  
 w   3  2   28017  0.013390    1( b2u)   1( b2u)   +    +-    -   +-   +    +-   +  
 w   3  2   28032  0.013081    1( b3u)   1( b3u)   +    +-    -   +-   +    +-   +  
 w   3  1   28170  0.024226    1( b1u)   1( b1u)   +    +-    -   +    +-   +-   +  
 w   3  1   28171  0.020435    1( b1u)   2( b1u)   +    +-    -   +    +-   +-   +  
 w   3  1   28172  0.012069    2( b1u)   2( b1u)   +    +-    -   +    +-   +-   +  
 w   3  1   28185  0.022101    1( b2u)   1( b2u)   +    +-    -   +    +-   +-   +  
 w   3  1   28186  0.016542    1( b2u)   2( b2u)   +    +-    -   +    +-   +-   +  
 w   3  1   28200  0.020598    1( b3u)   1( b3u)   +    +-    -   +    +-   +-   +  
 w   3  1   28201  0.015042    1( b3u)   2( b3u)   +    +-    -   +    +-   +-   +  
 w   1  1   28980 -0.014488    1( b1u)   1( b1u)   +    +-        +-   +-   +-    - 
 w   1  1   28981 -0.011826    1( b1u)   2( b1u)   +    +-        +-   +-   +-    - 
 w   1  1   28995 -0.015792    1( b2u)   1( b2u)   +    +-        +-   +-   +-    - 
 w   1  1   28996 -0.013265    1( b2u)   2( b2u)   +    +-        +-   +-   +-    - 
 w   1  1   29010 -0.012659    1( b3u)   1( b3u)   +    +-        +-   +-   +-    - 
 w   3  2   29530  0.010607    1( b3u)   1( b3u)   +     -   +-   +-   +    +-   +  
 w   3  2   29531  0.010341    1( b3u)   2( b3u)   +     -   +-   +-   +    +-   +  
 w   3  1   29698  0.016588    1( b3u)   1( b3u)   +     -   +-   +    +-   +-   +  
 w   3  1   29699  0.016066    1( b3u)   2( b3u)   +     -   +-   +    +-   +-   +  
 w   3  1   29700  0.010577    2( b3u)   2( b3u)   +     -   +-   +    +-   +-   +  
 w   1  1   29836 -0.016202    1( b1u)   1( b1u)   +     -   +    +-   +-   +-    - 
 w   1  1   29837 -0.011627    1( b1u)   2( b1u)   +     -   +    +-   +-   +-    - 
 w   1  1   29851 -0.016322    1( b2u)   1( b2u)   +     -   +    +-   +-   +-    - 
 w   1  1   29852 -0.011751    1( b2u)   2( b2u)   +     -   +    +-   +-   +-    - 
 w   1  1   29866 -0.020269    1( b3u)   1( b3u)   +     -   +    +-   +-   +-    - 
 w   1  1   29867 -0.017581    1( b3u)   2( b3u)   +     -   +    +-   +-   +-    - 
 w   1  1   29868 -0.010599    2( b3u)   2( b3u)   +     -   +    +-   +-   +-    - 
 w   1  1   30692  0.014094    1( b1u)   1( b1u)   +    +     -   +-   +-   +-    - 
 w   1  1   30693  0.012512    1( b1u)   2( b1u)   +    +     -   +-   +-   +-    - 
 w   1  1   30707  0.010653    1( b2u)   1( b2u)   +    +     -   +-   +-   +-    - 
 w   1  1   30722  0.010502    1( b3u)   1( b3u)   +    +     -   +-   +-   +-    - 
 w   3  2   31426  0.012791    1( b1u)   1( b1u)        +-   +-   +-   +    +-   +  
 w   3  2   31427  0.013082    1( b1u)   2( b1u)        +-   +-   +-   +    +-   +  
 w   3  1   31594  0.019678    1( b1u)   1( b1u)        +-   +-   +    +-   +-   +  
 w   3  1   31595  0.020110    1( b1u)   2( b1u)        +-   +-   +    +-   +-   +  
 w   3  1   31596  0.014386    2( b1u)   2( b1u)        +-   +-   +    +-   +-   +  
 w   3  1   31609  0.010501    1( b2u)   1( b2u)        +-   +-   +    +-   +-   +  
 w   3  1   31624  0.014131    1( b3u)   1( b3u)        +-   +-   +    +-   +-   +  
 w   3  1   31625  0.011333    1( b3u)   2( b3u)        +-   +-   +    +-   +-   +  
 w   1  1   31762 -0.016295    1( b1u)   1( b1u)        +-   +    +-   +-   +-    - 
 w   1  1   31763 -0.015889    1( b1u)   2( b1u)        +-   +    +-   +-   +-    - 
 w   1  1   31764 -0.010955    2( b1u)   2( b1u)        +-   +    +-   +-   +-    - 
 w   1  1   31777 -0.011000    1( b2u)   1( b2u)        +-   +    +-   +-   +-    - 
 w   1  1   31792 -0.011975    1( b3u)   1( b3u)        +-   +    +-   +-   +-    - 
 w   1  1   31976  0.013431    1( b1u)   1( b1u)        +    +-   +-   +-   +-    - 
 w   1  1   31977  0.014492    1( b1u)   2( b1u)        +    +-   +-   +-   +-    - 
 w   1  1   31978  0.010770    2( b1u)   2( b1u)        +    +-   +-   +-   +-    - 

 ci coefficient statistics:
           rq > 0.1                6
      0.1> rq > 0.01              95
     0.01> rq > 0.001           3972
    0.001> rq > 0.0001          7938
   0.0001> rq > 0.00001         9453
  0.00001> rq > 0.000001        8881
 0.000001> rq                   1813
           all                 32158
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:         126 2x:           0 4x:           0
All internal counts: zz :          15 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :          12 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         512    task #     6:        4079    task #     7:        3244    task #     8:          26
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:         193
  iref  icsf         v(icsf)             hv(icsf)
     1     3     -0.605942821147    118.861714407919
     2     4      0.380475919404    -74.634056848330
     3     5     -0.457602807423     89.763538159789
     4     6     -0.147489063950     28.931242185356

 number of reference csfs (nref) is     4.  root number (iroot) is  6.
 c0**2 =   0.74308198  c**2 (all zwalks) =   0.93978759

 pople ci energy extrapolation is computed with 12 correlated electrons.

 eref      =   -196.159988476135   "relaxed" cnot**2         =   0.743081981092
 eci       =   -196.464481157395   deltae = eci - eref       =  -0.304492681260
 eci+dv1   =   -196.542710813836   dv1 = (1-cnot**2)*deltae  =  -0.078229656441
 eci+dv2   =   -196.569758446350   dv2 = dv1 / cnot**2       =  -0.105277288956
 eci+dv3   =   -196.625393241642   dv3 = dv1 / (2*cnot**2-1) =  -0.160912084247
 eci+pople =   -196.575462182227   ( 12e- scaled deltae )    =  -0.415473706092
 passed aftci ... 
