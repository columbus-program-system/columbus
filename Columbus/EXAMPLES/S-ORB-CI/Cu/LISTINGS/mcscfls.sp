

     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 This program allows the csf mixing coefficient and orbital expansion coefficient
 optimization using the graphical unitary group approach and the exponential
 operator mcscf method.
 references:  r. shepard and j. simons, ' int. j. quantum chem. symp. 14, 211 (1980).
              r. shepard, i. shavitt, and j. simons, j. chem. phys. 76, 543 (1982).
              r. shepard in "ab initio methods in quantum chemistry ii" advances in chemical
                  physics 69, edited by k. p. lawley (wiley, new york, 1987) pp. 63-200.
 Original autor: Ron Shepard, ANL
 Later revisions: Michal Dallos, University Vienna

 This Version of Program MCSCF is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.4.0.2     **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 Workspace allocation information:
       131072000 of real*8 words ( 1000.00 MB) of work space has been allocated.

 user input information:

 ======== echo of the mcscf input ========
 ------------------------------------------------------------------------
  &input
   niter=100,
   nmiter=50,
   nciitr=300,
   tol(3)=1.e-4,
   tol(2)=1.e-4,
   tol(1)=1.e-8,
   NSTATE=0,
   npath=1,3,9,10,13,17,19,21,-11,12, 2,
   ncoupl=5,
   tol(9)=1.e-3,
   FCIORB=  1,2,20,1,3,20,1,4,20,2,1,20,3,1,20,4,1,20
   NAVST(1) = 3,
   WAVST(1,1)=1 ,
   WAVST(1,2)=1 ,
   WAVST(1,3)=1 ,
   NAVST(2) = 1,
   WAVST(2,1)=1 ,
   NAVST(3) = 1,
   WAVST(3,1)=1 ,
   NAVST(4) = 1,
   WAVST(4,1)=1 ,
  &end
 ------------------------------------------------------------------------


 ***  Integral file informations  ***


 input integral file : /bigscratch/Columbus_C70/Cu_SO/WORK/aoints               
    

 Integral file header information:
 Cu atom                                                                         
 aoints SIFS file created by argos.      zam792            14:38:58.196 03-Nov-14

 Core type energy values:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 total ao core energy =    0.000000000


   ******  Basis set information:  ******

 Number of irreps:                  8
 Total number of basis functions:  38

 irrep no.              1    2    3    4    5    6    7    8
 irrep label            ag  b1g  b2g  b3g   au  b1u  b2u  b3u
 no. of bas.fcions.    10    3    3    3    1    6    6    6
 inconsistent sifs parameter
 i,info(i),infoloc(i):     5  2700  2730
 bummer (warning):inconsistent sifs parameter                       5


 ***  MCSCF optimization procedure parmeters:  ***


 maximum number of mcscf iterations:        niter=   100

 maximum number of psci micro-iterations:   nmiter=   50
 maximum r,s subspace dimension allowed:    nvrsmx=   30

 tol(1)=  1.0000E-08. . . . delta-emc convergence criterion.
 tol(2)=  1.0000E-04. . . . wnorm convergence criterion.
 tol(3)=  1.0000E-04. . . . knorm convergence criterion.
 tol(4)=  1.0000E-08. . . . apxde convergence criterion.
 tol(5)=  1.0000E-04. . . . small diagonal matrix element tolerance.
 tol(6)=  1.0000E-06. . . . minimum ci-psci residual norm.
 tol(7)=  1.0000E-05. . . . maximum ci-psci residual norm.
 tol(8)=  1.0000E+00. . . . maximum abs(k(xy)) allowed.
 tol(9)=  1.0000E-03. . . . wnorm coupling tolerance.
 tol(10)= 0.0000E+00. . . . maximum psci emergency shift parameter.
 tol(11)= 0.0000E+00. . . . minimum psci emergency shift parameter.
 tol(12)= 0.0000E+00. . . . increment of psci emergency shift parameter.


 *** State averaging informations: ***


 MCSCF calculation performed for  4 DRTs.

 DRT  first state   no.of aver.states   weights
  1   ground state          3             0.167 0.167 0.167
  2   ground state          1             0.167
  3   ground state          1             0.167
  4   ground state          1             0.167

 The number of hmc(*) eigenvalues and eigenvectors calculated each iteration per DRT:
 DRT.   no.of eigenv.(=ncol)
    1        4
    2        2
    3        2
    4        2

 Orbitals included in invariant subspaces:
   symmetry   orbital   mask
       1       2(  2)    20
       1       3(  3)    20
       1       4(  4)    20
       2       1( 11)    20
       3       1( 14)    20
       4       1( 17)    20

 npath(*) options:
  2:  orbital-state coupling terms will be included beginning on iteration ncoupl=  5
  3:  print intermediate timing information.
  9:  suppress the drt listing.
 10:  suppress the hmc(*) eigenvector listing.
 12:  diagonalize the hmc(*) matrix iteratively.
        nunitv= 1 nciitr=** mxvadd=20 nvcimx=20
       rtolci(*),wnorm=     1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 0.0000E+00
   noldv =   0  0  0  0
 13:  get initial orbitals from the formatted file, mocoef.
 17:  print the final natural orbitals and occupations.
 19:  transform the virtual orbitals to diagonalize qvv(*).
 21:  write out the one- and two- electron density for further use (files:mcd1fl, mcd2fl).


   ******  DRT info section  ******


 Informations for the DRT no.  1

 DRT file header:
  title                                                                          
 Molecular symmetry group:    ag 
 Total number of electrons:   19
 Spin multiplicity:            2
 Number of active orbitals:    6
 Number of active electrons:  11
 Total number of CSFs:         3

 Informations for the DRT no.  2

 DRT file header:
  title                                                                          
 Molecular symmetry group:    b1g
 Total number of electrons:   19
 Spin multiplicity:            2
 Number of active orbitals:    6
 Number of active electrons:  11
 Total number of CSFs:         1

 Informations for the DRT no.  3

 DRT file header:
  title                                                                          
 Molecular symmetry group:    b2g
 Total number of electrons:   19
 Spin multiplicity:            2
 Number of active orbitals:    6
 Number of active electrons:  11
 Total number of CSFs:         1

 Informations for the DRT no.  4

 DRT file header:
  title                                                                          
 Molecular symmetry group:    b3g
 Total number of electrons:   19
 Spin multiplicity:            2
 Number of active orbitals:    6
 Number of active electrons:  11
 Total number of CSFs:         1
 

 faar:   0 active-active rotations allowed out of:   3 possible.


 Number of active-double rotations:         3
 Number of active-active rotations:         0
 Number of double-virtual rotations:       21
 Number of active-virtual rotations:       24
 lenbfsdef=                131071  lenbfs=                   124
  number of integrals per class 1:11 (cf adda 
 class  1 (pq|rs):         #          75
 class  2 (pq|ri):         #          39
 class  3 (pq|ia):         #         345
 class  4 (pi|qa):         #         564
 class  5 (pq|ra):         #         408
 class  6 (pq|ij)/(pi|qj): #         108
 class  7 (pq|ab):         #        1236
 class  8 (pa|qb):         #        2220
 class  9 p(bp,ai)         #         504
 class 10p(ai,jp):        #          63
 class 11p(ai,bj):        #         276

 Size of orbital-Hessian matrix B:                     1269
 Size of the orbital-state Hessian matrix C:            576
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:           1845


 Source of the initial MO coeficients:

 Input MO coefficient file: /bigscratch/Columbus_C70/Cu_SO/WORK/mocoef                  
 

               starting mcscf iteration...   1

 orbital-state coupling will not be calculated this iteration.

 *** Starting integral transformation ***

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     8, naopsy(1) =    10, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 64959, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 65000

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore= 131067519

 inoutp: segmentation information:
 in-core transformation space,   avcinc = 130901727
 address segment size,           sizesg = 130750241
 number of in-core blocks,       nincbk =       106
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:       9468 transformed 1/r12    array elements were written in       2 records.


 mosort: allocated sort2 space, avc2is=   130929372 available sort2 space, avcisx=   130929624

 trial vectors are generated internally.

 trial vector  1 is unit matrix column     1
 ciiter=   1 noldhv=  1 noldv=  1

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -196.1610313236     -196.1610313236        0.0000000018        0.0000010000
    2         0.0000000000        0.0000000000        0.0000000000        0.0000010000
    3         0.0000000000        0.0000000000        0.0000000000        0.0000010000

 trial vectors are generated internally.

 trial vector  1 is unit matrix column     1
 ciiter=   1 noldhv=  1 noldv=  1

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -196.0167743095     -196.0167743095        0.0000000000        0.0000010000

 trial vectors are generated internally.

 trial vector  1 is unit matrix column     1
 ciiter=   1 noldhv=  1 noldv=  1

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -196.0167743090     -196.0167743090        0.0000000000        0.0000010000

 trial vectors are generated internally.

 trial vector  1 is unit matrix column     1
 ciiter=   1 noldhv=  1 noldv=  1

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -196.0167743090     -196.0167743090        0.0000000000        0.0000010000
 
  tol(10)=  0.000000000000000E+000  eshsci=  0.633192197737291     
 Total number of micro iterations:    7

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0=-0.98431022 pnorm= 0.0000E+00 rznorm= 1.5559E-06 rpnorm= 0.0000E+00 noldr=  7 nnewr=  7 nolds=  0 nnews=  0
 

 fdd(*) eigenvalues. symmetry block  1
   -18.195638

 qvv(*) eigenvalues. symmetry block  1
    -2.175784   -2.094149   -2.094142    2.763594    2.763599   23.642926

 qvv(*) eigenvalues. symmetry block  2
    -2.076822    2.773932

 qvv(*) eigenvalues. symmetry block  3
    -2.076824    2.773928

 qvv(*) eigenvalues. symmetry block  4
    -2.076815    2.773940

 qvv(*) eigenvalues. symmetry block  5
    -9.199418

 fdd(*) eigenvalues. symmetry block  6
   -14.342510

 qvv(*) eigenvalues. symmetry block  6
    -2.834119   -1.202154    0.750306    8.061388    8.066623

 fdd(*) eigenvalues. symmetry block  7
   -14.342509

 qvv(*) eigenvalues. symmetry block  7
    -2.834117   -1.202154    0.750308    8.061389    8.066625

 fdd(*) eigenvalues. symmetry block  8
   -14.342515

 qvv(*) eigenvalues. symmetry block  8
    -2.834123   -1.202157    0.750295    8.061385    8.066619

 restrt: restart information saved on the restart file (unit= 13).

 not all mcscf convergence criteria are satisfied.
 iter=    1 emc=   -130.7018923752 demc= 1.3070E+02 wnorm= 5.0655E+00 knorm= 1.7645E-01 apxde= 4.1961E-01    *not conv.*     

               starting mcscf iteration...   2

 orbital-state coupling will not be calculated this iteration.

 *** Starting integral transformation ***

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     8, naopsy(1) =    10, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 64959, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 65000

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore= 131067519

 inoutp: segmentation information:
 in-core transformation space,   avcinc = 130901727
 address segment size,           sizesg = 130750241
 number of in-core blocks,       nincbk =       106
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:      10646 transformed 1/r12    array elements were written in       2 records.


 mosort: allocated sort2 space, avc2is=   130929372 available sort2 space, avcisx=   130929624

   1 trial vectors read from nvfile (unit= 29).
 ciiter=   2 noldhv=  2 noldv=  2

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -195.9322189645     -195.9322189645        0.0000000682        0.0000100000
    2      -195.8297682185     -195.8297682185        0.0000000000        0.0000100000
    3         0.0000000000        0.0000000000        0.0000000000        0.0000100000

   1 trial vectors read from nvfile (unit= 29).
 ciiter=   1 noldhv=  1 noldv=  1

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -195.9332637386     -195.9332637386        0.0000000000        0.0000100000

   1 trial vectors read from nvfile (unit= 29).
 ciiter=   1 noldhv=  1 noldv=  1

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -195.9332637291     -195.9332637291        0.0000000000        0.0000100000

   1 trial vectors read from nvfile (unit= 29).
 ciiter=   1 noldhv=  1 noldv=  1

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -195.9332640504     -195.9332640504        0.0000000000        0.0000100000
 
  tol(10)=  0.000000000000000E+000  eshsci=  0.173173895806172     
 Total number of micro iterations:    8

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.99586895 pnorm= 0.0000E+00 rznorm= 6.9936E-06 rpnorm= 0.0000E+00 noldr=  8 nnewr=  8 nolds=  0 nnews=  0
 

 fdd(*) eigenvalues. symmetry block  1
   -14.303918

 qvv(*) eigenvalues. symmetry block  1
    -0.635990   -0.029301   -0.023599    5.899856    5.900639   26.472386

 qvv(*) eigenvalues. symmetry block  2
    -0.024828    5.906779

 qvv(*) eigenvalues. symmetry block  3
    -0.026855    5.902602

 qvv(*) eigenvalues. symmetry block  4
    -0.021062    5.914547

 qvv(*) eigenvalues. symmetry block  5
    -9.030570

 fdd(*) eigenvalues. symmetry block  6
   -10.597819

 qvv(*) eigenvalues. symmetry block  6
    -1.086848   -0.297809    3.157713   11.565219   11.574976

 fdd(*) eigenvalues. symmetry block  7
   -10.595865

 qvv(*) eigenvalues. symmetry block  7
    -1.085367   -0.297111    3.161956   11.568208   11.575281

 fdd(*) eigenvalues. symmetry block  8
   -10.601452

 qvv(*) eigenvalues. symmetry block  8
    -1.089608   -0.299106    3.149825   11.565183   11.568886

 restrt: restart information saved on the restart file (unit= 13).

 not all mcscf convergence criteria are satisfied.
 iter=    2 emc=   -163.2602964502 demc= 3.2558E+01 wnorm= 1.3854E+00 knorm= 9.0802E-02 apxde= 4.4237E-02    *not conv.*     

               starting mcscf iteration...   3

 orbital-state coupling will not be calculated this iteration.

 *** Starting integral transformation ***

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     8, naopsy(1) =    10, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 64959, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 65000

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore= 131067519

 inoutp: segmentation information:
 in-core transformation space,   avcinc = 130901727
 address segment size,           sizesg = 130750241
 number of in-core blocks,       nincbk =       106
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:      10646 transformed 1/r12    array elements were written in       2 records.


 mosort: allocated sort2 space, avc2is=   130929372 available sort2 space, avcisx=   130929624

   2 trial vectors read from nvfile (unit= 29).
 ciiter=   2 noldhv=  3 noldv=  3

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -196.0855605196     -196.0855605196        0.0000000000        0.0000100000
    2      -196.0846691303     -196.0846691303        0.0000000000        0.0000100000
    3      -195.9525701290     -195.9525701290        0.0000000000        0.0000100000

   1 trial vectors read from nvfile (unit= 29).
 ciiter=   1 noldhv=  1 noldv=  1

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -196.0855389070     -196.0855389070        0.0000000000        0.0000100000

   1 trial vectors read from nvfile (unit= 29).
 ciiter=   1 noldhv=  1 noldv=  1

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -196.0855751638     -196.0855751638        0.0000000000        0.0000100000

   1 trial vectors read from nvfile (unit= 29).
 ciiter=   1 noldhv=  1 noldv=  1

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -196.0854775776     -196.0854775776        0.0000000000        0.0000100000
 
  tol(10)=  0.000000000000000E+000  eshsci=  0.234286047323899     
 Total number of micro iterations:    7

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.99443132 pnorm= 0.0000E+00 rznorm= 1.5586E-06 rpnorm= 0.0000E+00 noldr=  7 nnewr=  7 nolds=  0 nnews=  0
 

 fdd(*) eigenvalues. symmetry block  1
   -10.791060

 qvv(*) eigenvalues. symmetry block  1
     0.707806    1.732673    1.733208    8.861415    8.862836   29.093668

 qvv(*) eigenvalues. symmetry block  2
     1.733491    8.862961

 qvv(*) eigenvalues. symmetry block  3
     1.733351    8.862951

 qvv(*) eigenvalues. symmetry block  4
     1.733746    8.862985

 qvv(*) eigenvalues. symmetry block  5
    -9.256053

 fdd(*) eigenvalues. symmetry block  6
    -7.172778

 qvv(*) eigenvalues. symmetry block  6
     0.134340    0.848347    5.612566   14.752232   14.753603

 fdd(*) eigenvalues. symmetry block  7
    -7.173166

 qvv(*) eigenvalues. symmetry block  7
     0.134338    0.848312    5.612305   14.752187   14.753321

 fdd(*) eigenvalues. symmetry block  8
    -7.172074

 qvv(*) eigenvalues. symmetry block  8
     0.134346    0.848411    5.613053   14.752835   14.753626

 restrt: restart information saved on the restart file (unit= 13).

 not all mcscf convergence criteria are satisfied.
 iter=    3 emc=   -196.0632319046 demc= 3.2803E+01 wnorm= 1.8743E+00 knorm= 1.0539E-01 apxde= 8.0425E-02    *not conv.*     

               starting mcscf iteration...   4

 orbital-state coupling will not be calculated this iteration.

 *** Starting integral transformation ***

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     8, naopsy(1) =    10, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 64959, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 65000

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore= 131067519

 inoutp: segmentation information:
 in-core transformation space,   avcinc = 130901727
 address segment size,           sizesg = 130750241
 number of in-core blocks,       nincbk =       106
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:      10646 transformed 1/r12    array elements were written in       2 records.


 mosort: allocated sort2 space, avc2is=   130929372 available sort2 space, avcisx=   130929624

   3 trial vectors read from nvfile (unit= 29).
 ciiter=   1 noldhv=  3 noldv=  3

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -196.1621215531     -196.1621215531        0.0000000000        0.0000100000
    2      -196.1620889035     -196.1620889035        0.0000000000        0.0000100000
    3      -196.0613512861     -196.0613512861        0.0000000000        0.0000100000

   1 trial vectors read from nvfile (unit= 29).
 ciiter=   1 noldhv=  1 noldv=  1

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -196.1621186875     -196.1621186875        0.0000000000        0.0000100000

   1 trial vectors read from nvfile (unit= 29).
 ciiter=   1 noldhv=  1 noldv=  1

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -196.1621219378     -196.1621219378        0.0000000000        0.0000100000

   1 trial vectors read from nvfile (unit= 29).
 ciiter=   1 noldhv=  1 noldv=  1

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -196.1621126458     -196.1621126458        0.0000000000        0.0000100000
 
  tol(10)=  0.000000000000000E+000  eshsci=  5.828076005250165E-003
 Total number of micro iterations:    6

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.99992533 pnorm= 0.0000E+00 rznorm= 1.0567E-06 rpnorm= 0.0000E+00 noldr=  6 nnewr=  6 nolds=  0 nnews=  0
 

 fdd(*) eigenvalues. symmetry block  1
   -10.850394

 qvv(*) eigenvalues. symmetry block  1
     0.712562    1.726690    1.726722    8.810660    8.810720   29.076188

 qvv(*) eigenvalues. symmetry block  2
     1.726742    8.810740

 qvv(*) eigenvalues. symmetry block  3
     1.726732    8.810732

 qvv(*) eigenvalues. symmetry block  4
     1.726760    8.810757

 qvv(*) eigenvalues. symmetry block  5
    -9.423452

 fdd(*) eigenvalues. symmetry block  6
    -7.228118

 qvv(*) eigenvalues. symmetry block  6
     0.137665    0.861924    5.626089   14.720403   14.720444

 fdd(*) eigenvalues. symmetry block  7
    -7.228128

 qvv(*) eigenvalues. symmetry block  7
     0.137665    0.861919    5.626072   14.720400   14.720438

 fdd(*) eigenvalues. symmetry block  8
    -7.228099

 qvv(*) eigenvalues. symmetry block  8
     0.137666    0.861931    5.626120   14.720419   14.720444

 restrt: restart information saved on the restart file (unit= 13).

 not all mcscf convergence criteria are satisfied.
 iter=    4 emc=   -196.1453191690 demc= 8.2087E-02 wnorm= 4.6625E-02 knorm= 1.2220E-02 apxde= 1.8197E-04    *not conv.*     

               starting mcscf iteration...   5

 orbital-state coupling will not be calculated this iteration.

 *** Starting integral transformation ***

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     8, naopsy(1) =    10, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 64959, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 65000

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore= 131067519

 inoutp: segmentation information:
 in-core transformation space,   avcinc = 130901727
 address segment size,           sizesg = 130750241
 number of in-core blocks,       nincbk =       106
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:      10646 transformed 1/r12    array elements were written in       2 records.


 mosort: allocated sort2 space, avc2is=   130929372 available sort2 space, avcisx=   130929624

   3 trial vectors read from nvfile (unit= 29).
 ciiter=   1 noldhv=  3 noldv=  3

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -196.1630891463     -196.1630891463        0.0000000000        0.0000010000
    2      -196.1630891097     -196.1630891097        0.0000000000        0.0000010000
    3      -196.0575654726     -196.0575654726        0.0000000000        0.0000010000

   1 trial vectors read from nvfile (unit= 29).
 ciiter=   1 noldhv=  1 noldv=  1

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -196.1630891590     -196.1630891590        0.0000000000        0.0000010000

   1 trial vectors read from nvfile (unit= 29).
 ciiter=   1 noldhv=  1 noldv=  1

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -196.1630891463     -196.1630891463        0.0000000000        0.0000010000

   1 trial vectors read from nvfile (unit= 29).
 ciiter=   1 noldhv=  1 noldv=  1

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -196.1630891811     -196.1630891811        0.0000000000        0.0000010000
 
  tol(10)=  0.000000000000000E+000  eshsci=  5.246491502356888E-005
 Total number of micro iterations:    5

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 1.00000000 pnorm= 0.0000E+00 rznorm= 1.3000E-07 rpnorm= 0.0000E+00 noldr=  5 nnewr=  5 nolds=  0 nnews=  0
 

 fdd(*) eigenvalues. symmetry block  1
   -10.833463

 qvv(*) eigenvalues. symmetry block  1
     0.714668    1.730659    1.730659    8.822563    8.822564   29.086045

 qvv(*) eigenvalues. symmetry block  2
     1.730659    8.822564

 qvv(*) eigenvalues. symmetry block  3
     1.730659    8.822564

 qvv(*) eigenvalues. symmetry block  4
     1.730659    8.822564

 qvv(*) eigenvalues. symmetry block  5
    -9.427479

 fdd(*) eigenvalues. symmetry block  6
    -7.213132

 qvv(*) eigenvalues. symmetry block  6
     0.139008    0.864691    5.635380   14.733254   14.733254

 fdd(*) eigenvalues. symmetry block  7
    -7.213132

 qvv(*) eigenvalues. symmetry block  7
     0.139008    0.864691    5.635380   14.733254   14.733254

 fdd(*) eigenvalues. symmetry block  8
    -7.213132

 qvv(*) eigenvalues. symmetry block  8
     0.139008    0.864691    5.635380   14.733254   14.733254

 restrt: restart information saved on the restart file (unit= 13).

 not all mcscf convergence criteria are satisfied.
 iter=    5 emc=   -196.1455018692 demc= 1.8270E-04 wnorm= 4.1972E-04 knorm= 7.1976E-05 apxde= 8.0271E-09    *not conv.*     

               starting mcscf iteration...   6

 orbital-state coupling will be calculated this iteration.

 *** Starting integral transformation ***

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     8, naopsy(1) =    10, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 64959, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 65000

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore= 131067519

 inoutp: segmentation information:
 in-core transformation space,   avcinc = 130901727
 address segment size,           sizesg = 130750241
 number of in-core blocks,       nincbk =       106
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:      10643 transformed 1/r12    array elements were written in       2 records.


 mosort: allocated sort2 space, avc2is=   130929372 available sort2 space, avcisx=   130929624

   3 trial vectors read from nvfile (unit= 29).
 ciiter=   1 noldhv=  3 noldv=  3

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -196.1630921702     -196.1630921702        0.0000000000        0.0000010000
    2      -196.1630921699     -196.1630921699        0.0000000000        0.0000010000
    3      -196.0575504129     -196.0575504129        0.0000000000        0.0000010000

   1 trial vectors read from nvfile (unit= 29).
 ciiter=   1 noldhv=  1 noldv=  1

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -196.1630921699     -196.1630921699        0.0000000000        0.0000010000

   1 trial vectors read from nvfile (unit= 29).
 ciiter=   1 noldhv=  1 noldv=  1

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -196.1630921698     -196.1630921698        0.0000000000        0.0000010000

   1 trial vectors read from nvfile (unit= 29).
 ciiter=   1 noldhv=  1 noldv=  1

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -196.1630921704     -196.1630921704        0.0000000000        0.0000010000
 
  tol(10)=  0.000000000000000E+000  eshsci=  1.662777645751249E-008
 performing all-state projection
 Total number of micro iterations:    1

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 1.00000000 pnorm= 0.0000E+00 rznorm= 1.0115E-07 rpnorm= 7.0085E-10 noldr=  1 nnewr=  1 nolds=  0 nnews=  0
 

 fdd(*) eigenvalues. symmetry block  1
   -10.833383

 qvv(*) eigenvalues. symmetry block  1
     0.714678    1.730678    1.730678    8.822611    8.822611   29.086087

 qvv(*) eigenvalues. symmetry block  2
     1.730678    8.822611

 qvv(*) eigenvalues. symmetry block  3
     1.730678    8.822611

 qvv(*) eigenvalues. symmetry block  4
     1.730678    8.822611

 qvv(*) eigenvalues. symmetry block  5
    -9.427487

 fdd(*) eigenvalues. symmetry block  6
    -7.213074

 qvv(*) eigenvalues. symmetry block  6
     0.139015    0.864705    5.635420   14.733308   14.733308

 fdd(*) eigenvalues. symmetry block  7
    -7.213074

 qvv(*) eigenvalues. symmetry block  7
     0.139015    0.864705    5.635420   14.733308   14.733308

 fdd(*) eigenvalues. symmetry block  8
    -7.213074

 qvv(*) eigenvalues. symmetry block  8
     0.139015    0.864705    5.635420   14.733308   14.733308

 restrt: restart information saved on the restart file (unit= 13).

 all mcscf convergence criteria are satisfied.

 final mcscf convergence values:
 iter=    6 emc=   -196.1455018772 demc= 8.0275E-09 wnorm= 1.3302E-07 knorm= 4.9884E-09 apxde= 1.5759E-17    *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 wt 0.167 total energy=     -196.163092170, rel. (eV)=   0.000000
   DRT #1 state # 2 wt 0.167 total energy=     -196.163092170, rel. (eV)=   0.000000
   DRT #1 state # 3 wt 0.167 total energy=     -196.057550413, rel. (eV)=   2.871939
   DRT #2 state # 1 wt 0.167 total energy=     -196.163092170, rel. (eV)=   0.000000
   DRT #3 state # 1 wt 0.167 total energy=     -196.163092170, rel. (eV)=   0.000000
   DRT #4 state # 1 wt 0.167 total energy=     -196.163092170, rel. (eV)=   0.000000
   ------------------------------------------------------------


 MO-coefficient print-out skipped (no flag 32)
 They may be found in the MOCOEF directory.

          natural orbitals of the final iteration,block  1    -   ag
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     1.83333333     1.83333333     1.83333333     0.00000000     0.00000000     0.00000000     0.00000000
               MO    9        MO   10
  occ(*)=     0.00000000     0.00000000

          natural orbitals of the final iteration,block  2    -  b1g
               MO    1        MO    2        MO    3
  occ(*)=     1.83333333     0.00000000     0.00000000

          natural orbitals of the final iteration,block  3    -  b2g
               MO    1        MO    2        MO    3
  occ(*)=     1.83333333     0.00000000     0.00000000

          natural orbitals of the final iteration,block  4    -  b3g
               MO    1        MO    2        MO    3
  occ(*)=     1.83333333     0.00000000     0.00000000

          natural orbitals of the final iteration,block  5    -   au
               MO    1
  occ(*)=     0.00000000

          natural orbitals of the final iteration,block  6    -  b1u
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6
  occ(*)=     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

          natural orbitals of the final iteration,block  7    -  b2u
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6
  occ(*)=     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

          natural orbitals of the final iteration,block  8    -  b3u
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6
  occ(*)=     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
 d1(*), fmc(*), and qmc(*) written to the 1-particle density matrix file.
        100 d2(*) elements written to the 2-particle density matrix file: mcd2fl                                                      


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                         ag partial gross atomic populations
   ao class       1 ag       2 ag       3 ag       4 ag       5 ag       6 ag
     3_ s       2.000000   0.906178   0.869371   0.057784   0.000000   0.000000
     3_ d       0.000000   0.927155   0.963962   1.775550   0.000000   0.000000
 
   ao class       7 ag       8 ag       9 ag      10 ag

                        b1g partial gross atomic populations
   ao class       1b1g       2b1g       3b1g
     3_ d       1.833333   0.000000   0.000000

                        b2g partial gross atomic populations
   ao class       1b2g       2b2g       3b2g
     3_ d       1.833333   0.000000   0.000000

                        b3g partial gross atomic populations
   ao class       1b3g       2b3g       3b3g
     3_ d       1.833333   0.000000   0.000000

                         au partial gross atomic populations
   ao class       1 au

                        b1u partial gross atomic populations
   ao class       1b1u       2b1u       3b1u       4b1u       5b1u       6b1u
     3_ p       2.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000

                        b2u partial gross atomic populations
   ao class       1b2u       2b2u       3b2u       4b2u       5b2u       6b2u
     3_ p       2.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000

                        b3u partial gross atomic populations
   ao class       1b3u       2b3u       3b3u       4b3u       5b3u       6b3u
     3_ p       2.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000


                        gross atomic populations
     ao            3_
      s         3.833333
      p         6.000000
      d         9.166667
      f         0.000000
    total      19.000000
 

 Total number of electrons:   19.00000000

 !timer: mcscf                           cpu_time=     0.041 walltime=     0.041
