1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      2009-03.    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


================ Computing sorting integral file structure ================

                    -----z----- -----y----- -----x----- -----w----- ---total---

                CSFs         6         432       16740       14980       32158
      internal walks        24         144         360         280         808
valid internal walks         6         144         360         280         790
 getinfoarray: info=                     6 :                     1
                  8192                  6552                  8192
                  5460                     0
 icd(3)=                  5356 ci%nnlev=                   595  l2rec=
                  8192  n2max=                  5460
 lcore1,lcore2=             131030557             131007241
 lencor,maxblo             131072000                 60000
========================================
 current settings:
 minbl3         156
 minbl4         225
 locmaxbl3     1424
 locmaxbuf      712
 maxbl3       60000
 maxbl3       60000
 maxbl4       60000
 maxbuf       30006
========================================

 sorted 4-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)

 sorted 3-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)
 Orig.  diagonal integrals:  1electron:        34
                             0ext.    :        42
                             2ext.    :       336
                             4ext.    :       812


 Orig. off-diag. integrals:  4ext.    :     12615
                             3ext.    :      9360
                             2ext.    :      3708
                             1ext.    :       576
                             0ext.    :        99
                             2ext. SO :        12
                             1ext. SO :        48
                             0ext. SO :       138
                             1electron:        76


 Sorted integrals            3ext.  w :      8712 x :      8064
                             4ext.  w :     10563 x :      8841


Cycle #  1 sortfile size=    131068(       4 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core 131030557
Cycle #  2 sortfile size=    131068(       4 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core 131030557
 minimum size of srtscr:     65534 WP (     2 records)
 maximum size of srtscr:    131068 WP (     4 records)
diagi   file:      4 records  of   1536 WP each=>       6144 WP total
ofdgi   file:      9 records  of   1536 WP each=>      13824 WP total
fil3w   file:      1 records  of  30006 WP each=>      30006 WP total
fil3x   file:      1 records  of  30006 WP each=>      30006 WP total
fil4w   file:      1 records  of  30006 WP each=>      30006 WP total
fil4x   file:      1 records  of  30006 WP each=>      30006 WP total
  detected spin-orbit CI calculation ...
 compressed index vector length=                    21
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 0,
  GSET = 0,
   DAVCOR =10,
  NCOREL = 11
  NROOT = 6
  IVMODE = 3
  NBKITR = 1
  NVBKMN = 6
  RTOLBK = 1e-4,1e-4,1e-4,1e-4,1e-4,1e-4,
  NITER = 120
  NVCIMN = 8
  RTOLCI = 1e-4,1e-4,1e-4,1e-4,1e-4,1e-4,
  NVCIMX = 16
  NVRFMX = 16
  NVBKMX = 16
  update_mode=10
  IDEN  = 1
  CSFPRN = 10,
 /&end
 ------------------------------------------------------------------------
 Detected spin-orbit CI calculation ...
 Disabling tasklist usage (with_tsklst=0)!
lodens (list->root)=  6
invlodens (root->list)= -1 -1 -1 -1 -1  1
 USING SEGMENTS OF EQUAL SIZE

****************  list of control variables  ****************
 lvlprt =    0      nroot  =    6      noldv  =   0      noldhv =   0
 nunitv =    8      nbkitr =    1      niter  = 120      davcor =  10
 csfprn =   10      ivmode =    3      istrt  =   0      vout   =   0
 iortls =    0      nvbkmx =   16      ibktv  =  -1      ibkthv =  -1
 nvcimx =   16      icitv  =   -1      icithv =  -1      frcsub =   0
 nvbkmn =    6      nvcimn =    8      maxseg = 300      nrfitr =  30
 ncorel =   11      nvrfmx =   16      nvrfmn =   8      iden   =   1
 itran  =    0      froot  =    0      rtmode =   0      ncouple=   1
 skipso =    F      dalton2=    0      molcas =   0      finalv =   0
 finalw =    0      cosmocalc=   0    with_tsklst=   0
 nsegwx =    1     1     1     1
 nseg0x =    1     1     1     1
 nseg1x =    1     1     1     1
 nseg2x =    1     1     1     1
 nseg3x =    1     1     1     1
 nseg4x =    1     1     1     1
 no0ex  =      0    no1ex  =      0    no2ex  =     0    no3ex  =     0
 no4ex  =      0    nodiag =      0
 cdg4ex =    1      c3ex1ex=    1      c2ex0ex=   1
 fileloc=    0     0     0     0     0     0     0     1     1     1
 directhd=   1      noaqccshift_zyxw=      0
 critical_crit=-1.00000    critical_delta= 0.05000

 ctol   = 0.010000    lrtshift=1.000000    smalld =0.001000


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-04    1.000E-04
    2        1.000E-04    1.000E-04
    3        1.000E-04    1.000E-04
    4        1.000E-04    1.000E-04
    5        1.000E-04    1.000E-04
    6        1.000E-04    1.000E-04
 Computing density:                    .drt1.state6
 using                      1  nodes and                      1  cores.
 szdg/szodg per processor=                  6144                 13824
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core          131071999 DP per process

********** Integral sort section *************


 workspace allocation information: lencor= 131071999

 echo of the input for program cisrt:
 ------------------------------------------------------------------------
  &input
  maxbl3=60000
  maxbl4=60000
  &end
 ------------------------------------------------------------------------
 
 ( 6) listing file:                    ciudgls             
 ( 5) input file:                      cisrtin   
 (17) cidrt file:                      cidrtfl             
 (11) transformed integrals file:      moints    
 (12) diagonal integral file:          diagint             
 (13) off-diagonal integral file:      ofdgint             
 (31) 4-external w integrals file:     fil4w               
 (32) 4-external x integrals file:     fil4x               
 (33) 3-external w integrals file:     fil3w               
 (34) 3-external x integrals file:     fil3x               
 (21) scratch da sorting file:         srtscr              
 (12) 2-e integral file [fsplit=2]:    moints2   

 input integral file header information:
 Cu atom                                                                         
 aoints SIFS file created by argos.      zam792            14:38:58.196 03-Nov-14
  cidrt_title                                                                    
 MO-coefficients from mcscf.x                                                    
  with dummy occupation 1.0 for active orbitals                                  
  total ao core energy =    0.000000000                                          
 MCSCF energy =    -196.145501877                                                
 SIFS file created by program tran.      zam792            14:38:58.446 03-Nov-14

 input energy(*) values:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total core energy =   0.000000000000E+00

 nsym = 8 nmot=  34

 symmetry  =    1    2    3    4    5    6    7    8
 slabel(*) =   ag  b1g  b2g  b3g   au  b1u  b2u  b3u
 nmpsy(*)  =    9    3    3    3    1    5    5    5

 info(*) =          1      8192      6552      8192      5460         0

 orbital labels, i:molab(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012  13:tout:013  14:tout:014  15:tout:015  16:tout:016  17:tout:017  18:tout:018  19:tout:019  20:tout:020
  21:tout:021  22:tout:022  23:tout:023  24:tout:024  25:tout:025  26:tout:026  27:tout:027  28:tout:028  29:tout:029  30:tout:030
  31:tout:031  32:tout:032  33:tout:033  34:tout:034

 input parameters:
 prnopt=  0
 ldamin=    4095 ldamax=   32767 ldainc=      64
 maxbuf=   30006 maxbl3=   60000 maxbl4=   60000 intmxo=    1536
  Using 32 bit compression 

 drt information:
  cidrt_title                                                                    
 nmotd =  38 nfctd =   4 nfvtc =   0 nmot  =  34
 nlevel =  34 niot  =   6 lowinl=  29
 orbital-to-level map(*)
   -1  29  30  31   1   2   3   4   5   6  32   7   8  33   9  10  34  11  12  13
   -1  14  15  16  17  18  -1  19  20  21  22  23  -1  24  25  26  27  28
 compressed map(*)
   29  30  31   1   2   3   4   5   6  32   7   8  33   9  10  34  11  12  13  14
   15  16  17  18  19  20  21  22  23  24  25  26  27  28
 levsym(*)
    1   1   1   1   1   1   2   2   3   3   4   4   5   6   6   6   6   6   7   7
    7   7   7   8   8   8   8   8   1   1   1   2   3   4
 repartitioning mu(*)=
   0.  0.  0.  0.  0.  0.

 new core energy added to the energy(*) list.
 from the integral file: h1_core= -1.458883147958E+02

 indxdg: diagonal integral statistics.
 total number of integrals contributing to diagonal matrix elements:      1190
 number with all external indices:       812
 number with half external - half internal indices:       336
 number with all internal indices:        42

 indxof: off-diagonal integral statistics.
    4-external integrals: num=      12615 strt=          1
    3-external integrals: num=       9360 strt=      12616
    2-external integrals: num=       3708 strt=      21976
    1-external integrals: num=        576 strt=      25684
    0-external integrals: num=         99 strt=      26260

 total number of off-diagonal integrals:       26358


 indxof(2nd)  ittp=   3 numx(ittp)=        3708
 indxof(2nd)  ittp=   4 numx(ittp)=         576
 indxof(2nd)  ittp=   5 numx(ittp)=          99

 indxof: spin-orbit integral statistics.
    0-external so integrals: num=         12 strt=      26359
    1-external so integrals: num=         48 strt=      26371
    2-external so integrals: num=        138 strt=      26419

 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg= 130894995
 pro2e     1786    2381    2976    3571    4166    4187    4208    4803   48491   92179
   124946  133138  138598  160437

 pro2e:     23309 integrals read in     5 records.

 pro2e:         0 integrals 34-ext integrals skipped.
 pro1e     1786    2381    2976    3571    4166    4187    4208    4803   48491   92179
   124946  133138  138598  160437
 pro1e: eref =    0.000000000000000E+00
 total size of srtscr:                     4  records of                  32767 
 WP =               1048544 Bytes
 putdg     1786    2381    2976    3571    5107   37874   59719    4803   48491   92179
   124946  133138  138598  160437

 putf:       4 buffers of length    1536 written to file 12
 diagonal integral file completed.

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep4e:    53 blocks of linear combinations of 4-external integrals processed.
 number of sorted 4-external integrals      19404
 number of original 4-external integrals    12615


 putf34: external integral file complete. nfilw=    31 nfilx=    32 nrecw=     1 nrecx=     1 lbufp= 30006

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep3e:    43 blocks of linear combinations of 3-external integrals processed.
 number of sorted 3-external integrals      16776
 number of original 3-external integrals     9360


 putf34: external integral file complete. nfilw=    33 nfilx=    34 nrecw=     1 nrecx=     1 lbufp= 30006
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      3708         3     21976     21976      3708     26556
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd       576         4     25684     25684       576     26556
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd        99         5     26260     26260        99     26556
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd        12         6     26359     26359        12     26556
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd        48         7     26371     26371        48     26556
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd       138         8     26419     26419       138     26556

 putf:       9 buffers of length    1536 written to file 13
 off-diagonal files sort completed.
 executing brd_struct for cisrtinfo
cisrtinfo:
bufszi  1536
 diagfile 4ext:     812 2ext:     336 0ext:      42
 fil4w,fil4x  :   12615 fil3w,fil3x :    9360
 ofdgint  2ext:    3708 1ext:     576 0ext:      99so0ext:      12so1ext:      48so2ext:     138
buffer minbl4     225 minbl3     156 maxbl2     228nbas:   6   2   2   2   1   5   5   5 maxbuf 30006
 CIUDG version 5.9.7 ( 5-Oct-2004)

 workspace allocation information: lcore= 131071999

 core energy values from the integral file:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -1.458883147958E+02, ietype=    6,   fcore energy of type: H1(*)   

 total core repulsion energy = -1.458883147958E+02
 bummer (warning): calculation: setting iden=0                      0
 nmot  =    38 niot  =     7 nfct  =     4 nfvt  =     0
 nrow  =    39 nsym  =     8 ssym  =     3 lenbuf=  1600
 nwalk,xbar:        808       24      144      360      280
 nvalwt,nvalw:      790        6      144      360      280
 ncsft:           32158
 total number of valid internal walks:     790
 nvalz,nvaly,nvalx,nvalw =        6     144     360     280

 cisrt info file parameters:
 file number  12 blocksize   1536
 mxbld   1536
 nd4ext,nd2ext,nd0ext   812   336    42
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int    12615     9360     3708      576       99       12       48      138
 minbl4,minbl3,maxbl2   225   156   228
 maxbuf 30006
 number of external orbitals per symmetry block:   6   2   2   2   1   5   5   5
 nmsym   8 number of internal orbitals   6
 executing brd_struct for drt
 executing brd_struct for orbinf
 executing brd_struct for momap
 calcthrxt: niot,maxw1=                     7                    66
 block size     0
 pthz,pthy,pthx,pthw:    24   144   360   280 total internal walks:     808
 maxlp3,n2lp,n1lp,n0lp    66     0     0     0
 orbsym(*)= 1 1 1 2 3 4

 setref:        2 references kept,
               10 references were marked as invalid, out of
               12 total.
 pdinf%rmuval=  0.000000000000000E+000  0.000000000000000E+000
  0.000000000000000E+000  0.000000000000000E+000  0.000000000000000E+000
  0.000000000000000E+000
 nmb.of records onel     1
 nmb.of records 2-ext     3
 nmb.of records 1-ext     1
 nmb.of records 0-ext     1
 nmb.of records 2-int     1
 nmb.of records 1-int     1
 nmb.of records 0-int     1
 ---------memory usage in DP -----------------
 < n-ex core usage >
     routines:
    fourex            61660
    threx             60409
    twoex              2552
    onex               1633
    allin              1536
    diagon             2109
               =======
   maximum            61660
 
  __ static summary __ 
   reflst                 6
   hrfspc                 6
               -------
   static->               6
 
  __ core required  __ 
   totstc                 6
   max n-ex           61660
               -------
   totnec->           61666
 
  __ core available __ 
   totspc         131071999
   totnec -           61666
               -------
   totvec->       131010333

 number of external paths / symmetry
 vertex x      48      46      46      46      36      52      52      52
 vertex w      76      46      46      46      36      52      52      52
segment: free space=   131010333
 reducing frespc by                  2541 to              131007792 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1          22|         6|         0|         6|         0|         1|
 -------------------------------------------------------------------------------
  Y 2         144|       432|         6|       144|         6|         2|
 -------------------------------------------------------------------------------
  X 3         360|     16740|       438|       360|       150|         3|
 -------------------------------------------------------------------------------
  W 4         280|     14980|     17178|       280|       510|         4|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=          24DP  conft+indsym=        1440DP  drtbuffer=        1077 DP

dimension of the ci-matrix ->>>     32158

 executing brd_struct for civct
 gentasklist: ntask=                    20
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS   
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1     360      22      16740          6     360       6
     2  4   1    25      two-ext wz   2X  4 1     280      22      14980          6     280       6
     3  4   3    26      two-ext wx*  WX  4 3     280     360      14980      16740     280     360
     4  4   3    27      two-ext wx+  WX  4 3     280     360      14980      16740     280     360
     5  2   1    11      one-ext yz   1X  2 1     144      22        432          6     144       6
     6  3   2    15      1ex3ex yx    3X  3 2     360     144      16740        432     360     144
     7  4   2    16      1ex3ex yw    3X  4 2     280     144      14980        432     280     144
     8  1   1     1      allint zz    OX  1 1      22      22          6          6       6       6
     9  2   2     5      0ex2ex yy    OX  2 2     144     144        432        432     144     144
    10  3   3     6      0ex2ex xx*   OX  3 3     360     360      16740      16740     360     360
    11  3   3    18      0ex2ex xx+   OX  3 3     360     360      16740      16740     360     360
    12  4   4     7      0ex2ex ww*   OX  4 4     280     280      14980      14980     280     280
    13  4   4    19      0ex2ex ww+   OX  4 4     280     280      14980      14980     280     280
    14  2   2    42      four-ext y   4X  2 2     144     144        432        432     144     144
    15  3   3    43      four-ext x   4X  3 3     360     360      16740      16740     360     360
    16  4   4    44      four-ext w   4X  4 4     280     280      14980      14980     280     280
    17  1   1    75      dg-024ext z  OX  1 1      22      22          6          6       6       6
    18  2   2    76      dg-024ext y  OX  2 2     144     144        432        432     144     144
    19  3   3    77      dg-024ext x  OX  3 3     360     360      16740      16740     360     360
    20  4   4    78      dg-024ext w  OX  4 4     280     280      14980      14980     280     280
----------------------------------------------------------------------------------------------------
REDTASK #   1 TIME=  19.000 N=  1 (task/type/sgbra)=(   1/24/0) (
REDTASK #   2 TIME=  18.000 N=  1 (task/type/sgbra)=(   2/25/0) (
REDTASK #   3 TIME=  17.000 N=  1 (task/type/sgbra)=(   3/26/1) (
REDTASK #   4 TIME=  16.000 N=  1 (task/type/sgbra)=(   4/27/2) (
REDTASK #   5 TIME=  15.000 N=  1 (task/type/sgbra)=(   5/11/0) (
REDTASK #   6 TIME=  14.000 N=  1 (task/type/sgbra)=(   6/15/0) (
REDTASK #   7 TIME=  13.000 N=  1 (task/type/sgbra)=(   7/16/0) (
REDTASK #   8 TIME=  12.000 N=  1 (task/type/sgbra)=(   8/ 1/0) (
REDTASK #   9 TIME=  11.000 N=  1 (task/type/sgbra)=(   9/ 5/0) (
REDTASK #  10 TIME=  10.000 N=  1 (task/type/sgbra)=(  10/ 6/1) (
REDTASK #  11 TIME=   9.000 N=  1 (task/type/sgbra)=(  11/18/2) (
REDTASK #  12 TIME=   8.000 N=  1 (task/type/sgbra)=(  12/ 7/1) (
REDTASK #  13 TIME=   7.000 N=  1 (task/type/sgbra)=(  13/19/2) (
REDTASK #  14 TIME=   6.000 N=  1 (task/type/sgbra)=(  14/42/1) (
REDTASK #  15 TIME=   5.000 N=  1 (task/type/sgbra)=(  15/43/1) (
REDTASK #  16 TIME=   4.000 N=  1 (task/type/sgbra)=(  16/44/1) (
REDTASK #  17 TIME=   3.000 N=  1 (task/type/sgbra)=(  17/75/1) (
REDTASK #  18 TIME=   2.000 N=  1 (task/type/sgbra)=(  18/76/1) (
REDTASK #  19 TIME=   1.000 N=  1 (task/type/sgbra)=(  19/77/1) (
REDTASK #  20 TIME=   0.000 N=  1 (task/type/sgbra)=(  20/78/1) (
 initializing v-file: 1:                 32158

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      6 vectors will be written to unit 11 beginning with logical record   1

            6 vectors will be created
 bummer (warning):strefv: reducing number of start vectors to nref                      2
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:         126 2x:           0 4x:           0
All internal counts: zz :          15 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :          12 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:           0    task #     2:           0    task #     3:           0    task #     4:           0
task #     5:           0    task #     6:           0    task #     7:           0    task #     8:          29
task #     9:           0    task #    10:           0    task #    11:           0    task #    12:           0
task #    13:           0    task #    14:           0    task #    15:           0    task #    16:           0
task #    17:         105    task #    18:           0    task #    19:           0    task #    20:           0
 reference space has dimension       2
 dsyevx: computed roots 1 to    2(converged:   2)

    root           eigenvalues
    ----           ------------
       1        -196.1646625014
       2        -196.1615218388

 strefv generated    2 initial ci vector(s).
 bummer (warning):startv: num req > num generated, ngen=                       2
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=                     6

         vector  1 from unit 11 written to unit 49 filename cirefv              
 ufvoutnew: ... writing  recamt=                     6

         vector  2 from unit 11 written to unit 49 filename cirefv              
 ufvoutnew: ... writing  recamt=                     6

         vector  3 from unit 11 written to unit 49 filename cirefv              
 ufvoutnew: ... writing  recamt=                     6

         vector  4 from unit 11 written to unit 49 filename cirefv              
 ufvoutnew: ... writing  recamt=                     6

         vector  5 from unit 11 written to unit 49 filename cirefv              
 ufvoutnew: ... writing  recamt=                     6

         vector  6 from unit 11 written to unit 49 filename cirefv              

 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=     6)...
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:             32158
 number of initial trial vectors:                         2
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:              16
 number of roots to converge:                             6
 number of iterations:                                    1
 residual norm convergence criteria:               0.000100  0.000100  0.000100  0.000100  0.000100  0.000100

          starting bk iteration   1

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:           0 xx:           0 ww:           0
One-external counts: yz :         471 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:         150 wz:         160 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :          12 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :         153 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:           0    task #     4:           0
task #     5:         535    task #     6:           0    task #     7:           0    task #     8:          29
task #     9:           0    task #    10:           0    task #    11:           0    task #    12:           0
task #    13:           0    task #    14:           0    task #    15:           0    task #    16:           0
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:           0 xx:           0 ww:           0
One-external counts: yz :         471 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:         150 wz:         160 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :          12 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :         153 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:           0    task #     4:           0
task #     5:         535    task #     6:           0    task #     7:           0    task #     8:          29
task #     9:           0    task #    10:           0    task #    11:           0    task #    12:           0
task #    13:           0    task #    14:           0    task #    15:           0    task #    16:           0
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2
   ht   1   -50.27634771
   ht   2     0.00000000   -50.27320704

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2
 ref    1    1.00000       3.636162E-12
 ref    2  -3.636162E-12    1.00000    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2
 ref    1    1.00000        1.00000    

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2

          Spin-Orbit CI: perturbational estimate of Spin-Orbit CI energies and wavefunctions 

                SO   1         SO   2

   energy  -196.16466250  -196.16152184
 
   NR   1     1.00000000     0.00000000
   NR   2     0.00000000     1.00000000

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     1.00000000     0.00000000
 ref:   2     0.00000000     1.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -196.1646625014  0.0000E+00  4.0429E-01  1.7461E+00  1.0000E-04   
 mr-sdci #  1  2   -196.1615218388  0.0000E+00  0.0000E+00  1.7460E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mr-sdci  convergence not reached after  1 iterations.

 warning: *** the expansion subspace is invariant before all roots are converged.
 generate additional expansion vectors and try again.***


 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -196.1646625014  0.0000E+00  4.0429E-01  1.7461E+00  1.0000E-04   
 mr-sdci #  1  2   -196.1615218388  0.0000E+00  0.0000E+00  1.7460E+00  1.0000E-04   
 
 expansion vectors are not transformed.
 matrix-vector products are not transformed.

    3 expansion eigenvectors written to unit nvfile (= 11)
    2 matrix-vector products written to unit nhvfil (= 10)

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:             32158
 number of initial trial vectors:                         3
 number of initial matrix-vector products:                2
 maximum dimension of the subspace vectors:              16
 number of roots to converge:                             6
 number of iterations:                                  120
 residual norm convergence criteria:               0.000100  0.000100  0.000100  0.000100  0.000100  0.000100

          starting ci iteration   1

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3
   ht   1   -50.27634771
   ht   2     0.00000000   -50.27320704
   ht   3    -0.40428965    -0.00042142  -105.01164411

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3
 ref    1  -0.842140       1.031814E-03  -0.539258    
 ref    2  -8.626695E-04  -0.999999      -5.661943E-04

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3
 ref    1   0.709201        1.00000       0.290799    

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3

          Spin-Orbit CI: perturbational estimate of Spin-Orbit CI energies and wavefunctions 

                SO   1         SO   2         SO   3

   energy  -196.34333374  -196.16152184  -195.72891892
 
   NR   1    -0.84214025     0.00103181    -0.53925757
   NR   2    -0.00086267    -0.99999947    -0.00056619
   NR   3    -0.37217437    -0.00000802     0.58121207

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1    -0.84214025     0.00103181    -0.53925757
 ref:   2    -0.00086267    -0.99999947    -0.00056619

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -196.3433337440  1.7867E-01  0.0000E+00  1.1399E+00  1.0000E-04   
 mr-sdci #  1  2   -196.1615218422  3.3779E-09  4.0176E-01  1.7460E+00  1.0000E-04   
 mr-sdci #  1  3   -195.7289189189  4.9841E+01  0.0000E+00  2.6166E+00  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.003000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   2

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4
   ht   1   -50.27634771
   ht   2     0.00000000   -50.27320704
   ht   3    -0.40428965    -0.00042142  -105.01164411
   ht   4     0.00005603     0.40175812     0.89667707    -6.16649529

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4
 ref    1   8.253258E-03   0.842108      -0.539241      -2.214497E-03
 ref    2  -0.968864       6.923988E-03  -5.032082E-03   0.247445    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4
 ref    1   0.938766       0.709194       0.290806       6.123370E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.00825326     0.84210824    -0.53924084    -0.00221450
 ref:   2    -0.96886425     0.00692399    -0.00503208     0.24744453

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1   -196.4426933282  9.9360E-02  0.0000E+00  4.1908E-01  1.0000E-04   
 mr-sdci #  2  2   -196.3433312839  1.8181E-01  0.0000E+00  1.1400E+00  1.0000E-04   
 mr-sdci #  2  3   -195.7289386232  1.9704E-05  7.4927E-01  2.6165E+00  1.0000E-04   
 mr-sdci #  2  4   -191.8509216405  4.5963E+01  0.0000E+00  4.8018E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.004000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   3

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1   -50.27634771
   ht   2     0.00000000   -50.27320704
   ht   3    -0.40428965    -0.00042142  -105.01164411
   ht   4     0.00005603     0.40175812     0.89667707    -6.16649529
   ht   5     0.56766398     0.00543915    64.17026187    -0.55106057   -48.71612589

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1  -9.368486E-03   0.319467      -0.935816       5.466512E-03   0.148567    
 ref    2   0.968844       7.407209E-03  -7.629201E-03  -0.247367       6.212354E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1   0.938746       0.102114       0.875810       6.122018E-02   2.211065E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1    -0.00936849     0.31946653    -0.93581592     0.00546651     0.14856667
 ref:   2     0.96884366     0.00740721    -0.00762920    -0.24736672     0.00621235

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1   -196.4426942097  8.8143E-07  0.0000E+00  4.1907E-01  1.0000E-04   
 mr-sdci #  3  2   -196.4180282186  7.4697E-02  0.0000E+00  6.5521E-01  1.0000E-04   
 mr-sdci #  3  3   -196.2607247886  5.3179E-01  0.0000E+00  1.4005E+00  1.0000E-04   
 mr-sdci #  3  4   -191.8511560507  2.3441E-04  2.8025E+00  4.8015E+00  1.0000E-04   
 mr-sdci #  3  5   -191.1864039528  4.5298E+01  0.0000E+00  3.8103E+00  1.0000E-04   
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.003000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   4

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1   -50.27634771
   ht   2     0.00000000   -50.27320704
   ht   3    -0.40428965    -0.00042142  -105.01164411
   ht   4     0.00005603     0.40175812     0.89667707    -6.16649529
   ht   5     0.56766398     0.00543915    64.17026187    -0.55106057   -48.71612589
   ht   6    -0.10319127     2.25393441    -1.01649511   -23.19174779     1.46538896 -4397.06618860

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   9.358139E-03   0.319470       0.935815      -4.691654E-03  -7.421868E-04   0.148590    
 ref    2  -0.968838       7.388461E-03   7.613867E-03   0.193602       0.154085       4.062375E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.938735       0.102116       0.875807       3.750367E-02   2.374265E-02   2.209553E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.00935814     0.31947046     0.93581487    -0.00469165    -0.00074219     0.14859013
 ref:   2    -0.96883806     0.00738846     0.00761387     0.19360179     0.15408470     0.00406237

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1   -196.4426948399  6.3021E-07  0.0000E+00  4.1898E-01  1.0000E-04   
 mr-sdci #  4  2   -196.4180284565  2.3793E-07  0.0000E+00  6.5521E-01  1.0000E-04   
 mr-sdci #  4  3   -196.2607264340  1.6454E-06  0.0000E+00  1.4005E+00  1.0000E-04   
 mr-sdci #  4  4   -192.0816350496  2.3048E-01  0.0000E+00  3.7560E+00  1.0000E-04   
 mr-sdci #  4  5   -191.4888515345  3.0245E-01  7.2645E-01  3.0775E+00  1.0000E-04   
 mr-sdci #  4  6   -191.1862402509  4.5298E+01  0.0000E+00  3.8092E+00  1.0000E-04   
 
 root number  5 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.005000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   5

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1   -50.27634771
   ht   2     0.00000000   -50.27320704
   ht   3    -0.40428965    -0.00042142  -105.01164411
   ht   4     0.00005603     0.40175812     0.89667707    -6.16649529
   ht   5     0.56766398     0.00543915    64.17026187    -0.55106057   -48.71612589
   ht   6    -0.10319127     2.25393441    -1.01649511   -23.19174779     1.46538896 -4397.06618860
   ht   7    -0.02025675    -1.34765719    -0.16071959    12.58929107     0.56890320   267.28992172  -276.20018120

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1  -9.531980E-03   0.319472      -0.935813      -2.252884E-03  -3.066519E-03   0.148541      -4.750941E-03
 ref    2   0.968671       7.860700E-03  -7.656542E-03   0.195734      -0.107785      -5.682870E-03  -0.107677    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.938414       0.102124       0.875804       3.831698E-02   1.162709E-02   2.209669E-02   1.161701E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.00953198     0.31947183    -0.93581272    -0.00225288    -0.00306652     0.14854089    -0.00475094
 ref:   2     0.96867087     0.00786070    -0.00765654     0.19573427    -0.10778535    -0.00568287    -0.10767749

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1   -196.4427584116  6.3572E-05  0.0000E+00  4.1631E-01  1.0000E-04   
 mr-sdci #  5  2   -196.4180304623  2.0057E-06  0.0000E+00  6.5519E-01  1.0000E-04   
 mr-sdci #  5  3   -196.2607268774  4.4335E-07  0.0000E+00  1.4005E+00  1.0000E-04   
 mr-sdci #  5  4   -192.4840828602  4.0245E-01  0.0000E+00  3.6136E+00  1.0000E-04   
 mr-sdci #  5  5   -191.5347096901  4.5858E-02  0.0000E+00  2.1857E+00  1.0000E-04   
 mr-sdci #  5  6   -191.1876088985  1.3686E-03  4.2845E+00  3.8088E+00  1.0000E-04   
 mr-sdci #  5  7   -190.2009651420  4.4313E+01  0.0000E+00  2.4101E+00  1.0000E-04   
 
 root number  6 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.006000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   6

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.27634771
   ht   2     0.00000000   -50.27320704
   ht   3    -0.40428965    -0.00042142  -105.01164411
   ht   4     0.00005603     0.40175812     0.89667707    -6.16649529
   ht   5     0.56766398     0.00543915    64.17026187    -0.55106057   -48.71612589
   ht   6    -0.10319127     2.25393441    -1.01649511   -23.19174779     1.46538896 -4397.06618860
   ht   7    -0.02025675    -1.34765719    -0.16071959    12.58929107     0.56890320   267.28992172  -276.20018120
   ht   8    -2.04546636     0.18078623   -14.90009580    -1.58195950    39.50647198   -35.51325821    36.10011765 -5305.24520286

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -9.551346E-03  -0.318815       0.936085      -2.180274E-03  -0.102579       1.841759E-03  -0.107084      -3.775559E-03
 ref    2   0.968669      -8.133972E-03   7.586944E-03   0.195729       4.169342E-03   0.107841       1.811044E-03  -0.107689    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.938411       0.101709       0.876312       3.831449E-02   1.053984E-02   1.163303E-02   1.147019E-02   1.161116E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00955135    -0.31881470     0.93608450    -0.00218027    -0.10257903     0.00184176    -0.10708365    -0.00377556
 ref:   2     0.96866885    -0.00813397     0.00758694     0.19572873     0.00416934     0.10784079     0.00181104    -0.10768893

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  6  1   -196.4427592960  8.8443E-07  3.5954E-02  4.1630E-01  1.0000E-04   
 mr-sdci #  6  2   -196.4180864503  5.5988E-05  0.0000E+00  6.5562E-01  1.0000E-04   
 mr-sdci #  6  3   -196.2609238282  1.9695E-04  0.0000E+00  1.4009E+00  1.0000E-04   
 mr-sdci #  6  4   -192.4840869160  4.0559E-06  0.0000E+00  3.6135E+00  1.0000E-04   
 mr-sdci #  6  5   -191.5785429062  4.3833E-02  0.0000E+00  2.6785E+00  1.0000E-04   
 mr-sdci #  6  6   -191.5347044889  3.4710E-01  0.0000E+00  2.1855E+00  1.0000E-04   
 mr-sdci #  6  7   -190.7789016564  5.7794E-01  0.0000E+00  2.7571E+00  1.0000E-04   
 mr-sdci #  6  8   -190.2007788869  4.4312E+01  0.0000E+00  2.4092E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.011000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   7

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.27634771
   ht   2     0.00000000   -50.27320704
   ht   3    -0.40428965    -0.00042142  -105.01164411
   ht   4     0.00005603     0.40175812     0.89667707    -6.16649529
   ht   5     0.56766398     0.00543915    64.17026187    -0.55106057   -48.71612589
   ht   6    -0.10319127     2.25393441    -1.01649511   -23.19174779     1.46538896 -4397.06618860
   ht   7    -0.02025675    -1.34765719    -0.16071959    12.58929107     0.56890320   267.28992172  -276.20018120
   ht   8    -2.04546636     0.18078623   -14.90009580    -1.58195950    39.50647198   -35.51325821    36.10011765 -5305.24520286
   ht   9     0.00004406    -0.00858157    -0.00792852    -1.50192705     0.00714458    -2.63191946     1.41562659    -0.05887739

                ht   9
   ht   9    -1.31711120

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -8.768808E-03   0.318819       0.936091      -2.119775E-03  -3.696680E-03  -0.102449      -9.627320E-04   0.107215    
 ref    2   0.973107       7.720418E-03   6.830881E-03   7.068607E-02  -3.448239E-02  -2.234402E-03   0.135560      -5.590344E-03

              v      9
 ref    1   1.191105E-03
 ref    2   0.168399    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.947014       0.101705       0.876313       5.001013E-03   1.202700E-03   1.050080E-02   1.837742E-02   1.152634E-02

              v      9
 ref    1   2.835950E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00876881     0.31881919     0.93609082    -0.00211978    -0.00369668    -0.10244903    -0.00096273     0.10721515
 ref:   2     0.97310706     0.00772042     0.00683088     0.07068607    -0.03448239    -0.00223440     0.13555994    -0.00559034

                ci   9
 ref:   1     0.00119110
 ref:   2     0.16839859

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  7  1   -196.4645576256  2.1798E-02  0.0000E+00  1.0523E-01  1.0000E-04   
 mr-sdci #  7  2   -196.4180864600  9.6898E-09  7.4541E-02  6.5562E-01  1.0000E-04   
 mr-sdci #  7  3   -196.2609249570  1.1288E-06  0.0000E+00  1.4009E+00  1.0000E-04   
 mr-sdci #  7  4   -194.2770192472  1.7929E+00  0.0000E+00  3.1695E+00  1.0000E-04   
 mr-sdci #  7  5   -191.7842126822  2.0567E-01  0.0000E+00  1.7190E+00  1.0000E-04   
 mr-sdci #  7  6   -191.5783287872  4.3624E-02  0.0000E+00  2.6759E+00  1.0000E-04   
 mr-sdci #  7  7   -191.0094797086  2.3058E-01  0.0000E+00  3.6732E+00  1.0000E-04   
 mr-sdci #  7  8   -190.7784698302  5.7769E-01  0.0000E+00  2.7618E+00  1.0000E-04   
 mr-sdci #  7  9   -189.9027242891  4.4014E+01  0.0000E+00  4.0026E+00  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.006000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   8

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.27634771
   ht   2     0.00000000   -50.27320704
   ht   3    -0.40428965    -0.00042142  -105.01164411
   ht   4     0.00005603     0.40175812     0.89667707    -6.16649529
   ht   5     0.56766398     0.00543915    64.17026187    -0.55106057   -48.71612589
   ht   6    -0.10319127     2.25393441    -1.01649511   -23.19174779     1.46538896 -4397.06618860
   ht   7    -0.02025675    -1.34765719    -0.16071959    12.58929107     0.56890320   267.28992172  -276.20018120
   ht   8    -2.04546636     0.18078623   -14.90009580    -1.58195950    39.50647198   -35.51325821    36.10011765 -5305.24520286
   ht   9     0.00004406    -0.00858157    -0.00792852    -1.50192705     0.00714458    -2.63191946     1.41562659    -0.05887739
   ht  10    -0.03952408    -0.00060102     7.80514744    -0.10945250    -2.90547354    -0.10831149     0.02101858    -2.02224288

                ht   9         ht  10
   ht   9    -1.31711120
   ht  10    -0.01645552    -2.02064159

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.365778       7.627658E-03   0.914752       3.887462E-03  -0.149383       5.413924E-04   1.530080E-02   1.321970E-03
 ref    2   5.142893E-03  -0.973128       6.401490E-03  -7.067161E-02  -1.007437E-03   3.444400E-02  -5.317214E-03  -0.135592    

              v      9       v     10
 ref    1  -6.214984E-02   5.428448E-02
 ref    2  -0.111255      -0.126418    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.133820       0.947036       0.836813       5.009589E-03   2.231643E-02   1.186682E-03   2.623874E-04   1.838694E-02

              v      9       v     10
 ref    1   1.624031E-02   1.892835E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.36577825     0.00762766     0.91475243     0.00388746    -0.14938344     0.00054139     0.01530080     0.00132197
 ref:   2     0.00514289    -0.97312793     0.00640149    -0.07067161    -0.00100744     0.03444400    -0.00531721    -0.13559200

                ci   9         ci  10
 ref:   1    -0.06214984     0.05428448
 ref:   2    -0.11125516    -0.12641812

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  8  1   -196.4683106623  3.7530E-03  0.0000E+00  1.0064E-01  1.0000E-04   
 mr-sdci #  8  2   -196.4645575913  4.6471E-02  0.0000E+00  1.0523E-01  1.0000E-04   
 mr-sdci #  8  3   -196.2630830179  2.1581E-03  4.8391E-01  1.3718E+00  1.0000E-04   
 mr-sdci #  8  4   -194.2772320141  2.1277E-04  0.0000E+00  3.1697E+00  1.0000E-04   
 mr-sdci #  8  5   -192.6168362588  8.3262E-01  0.0000E+00  2.6418E+00  1.0000E-04   
 mr-sdci #  8  6   -191.7840926555  2.0576E-01  0.0000E+00  1.7175E+00  1.0000E-04   
 mr-sdci #  8  7   -191.1777975378  1.6832E-01  0.0000E+00  1.6816E+00  1.0000E-04   
 mr-sdci #  8  8   -191.0094763589  2.3101E-01  0.0000E+00  3.6739E+00  1.0000E-04   
 mr-sdci #  8  9   -189.9161714325  1.3447E-02  0.0000E+00  4.7190E+00  1.0000E-04   
 mr-sdci #  8 10   -189.8913439752  4.4003E+01  0.0000E+00  4.6295E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.005000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   9

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.27634771
   ht   2     0.00000000   -50.27320704
   ht   3    -0.40428965    -0.00042142  -105.01164411
   ht   4     0.00005603     0.40175812     0.89667707    -6.16649529
   ht   5     0.56766398     0.00543915    64.17026187    -0.55106057   -48.71612589
   ht   6    -0.10319127     2.25393441    -1.01649511   -23.19174779     1.46538896 -4397.06618860
   ht   7    -0.02025675    -1.34765719    -0.16071959    12.58929107     0.56890320   267.28992172  -276.20018120
   ht   8    -2.04546636     0.18078623   -14.90009580    -1.58195950    39.50647198   -35.51325821    36.10011765 -5305.24520286
   ht   9     0.00004406    -0.00858157    -0.00792852    -1.50192705     0.00714458    -2.63191946     1.41562659    -0.05887739
   ht  10    -0.03952408    -0.00060102     7.80514744    -0.10945250    -2.90547354    -0.10831149     0.02101858    -2.02224288
   ht  11    -0.20852559    -0.00172117   106.57411644    -0.74542285   -63.35550915     0.61485475    -0.28974522     2.07680014

                ht   9         ht  10         ht  11
   ht   9    -1.31711120
   ht  10    -0.01645552    -2.02064159
   ht  11     0.09505302    -8.23925080  -118.56938430

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.463276       4.244828E-03   0.861373       3.784887E-03   1.497604E-02  -1.307824E-03  -1.773658E-02  -2.479585E-03
 ref    2  -9.472329E-03   0.973054      -1.046865E-02   7.076552E-02   1.339968E-04  -3.446282E-02  -7.615514E-03  -0.135545    

              v      9       v     10       v     11
 ref    1  -7.472212E-02  -1.711557E-02  -0.192238    
 ref    2  -6.200070E-03  -0.167177       1.878938E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.214715       0.946853       0.742074       5.022085E-03   2.242998E-04   1.189396E-03   3.725822E-04   1.837866E-02

              v      9       v     10       v     11
 ref    1   5.621836E-03   2.824112E-02   3.730859E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.46327639     0.00424483     0.86137334     0.00378489     0.01497604    -0.00130782    -0.01773658    -0.00247958
 ref:   2    -0.00947233     0.97305450    -0.01046865     0.07076552     0.00013400    -0.03446282    -0.00761551    -0.13554524

                ci   9         ci  10         ci  11
 ref:   1    -0.07472212    -0.01711557    -0.19223827
 ref:   2    -0.00620007    -0.16717709     0.01878938

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  9  1   -196.4684645662  1.5390E-04  0.0000E+00  1.0807E-01  1.0000E-04   
 mr-sdci #  9  2   -196.4645596528  2.0616E-06  0.0000E+00  1.0522E-01  1.0000E-04   
 mr-sdci #  9  3   -196.4564780304  1.9340E-01  0.0000E+00  2.1668E-01  1.0000E-04   
 mr-sdci #  9  4   -194.2787584019  1.5264E-03  2.4343E+00  3.1695E+00  1.0000E-04   
 mr-sdci #  9  5   -193.1957243130  5.7889E-01  0.0000E+00  2.2693E+00  1.0000E-04   
 mr-sdci #  9  6   -191.7841034994  1.0844E-05  0.0000E+00  1.7178E+00  1.0000E-04   
 mr-sdci #  9  7   -191.2008457425  2.3048E-02  0.0000E+00  1.4906E+00  1.0000E-04   
 mr-sdci #  9  8   -191.0097774474  3.0109E-04  0.0000E+00  3.6736E+00  1.0000E-04   
 mr-sdci #  9  9   -190.1683463193  2.5217E-01  0.0000E+00  5.1657E+00  1.0000E-04   
 mr-sdci #  9 10   -189.9049542381  1.3610E-02  0.0000E+00  4.0099E+00  1.0000E-04   
 mr-sdci #  9 11   -189.5589327339  4.3671E+01  0.0000E+00  5.0292E+00  1.0000E-04   
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  10

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.27634771
   ht   2     0.00000000   -50.27320704
   ht   3    -0.40428965    -0.00042142  -105.01164411
   ht   4     0.00005603     0.40175812     0.89667707    -6.16649529
   ht   5     0.56766398     0.00543915    64.17026187    -0.55106057   -48.71612589
   ht   6    -0.10319127     2.25393441    -1.01649511   -23.19174779     1.46538896 -4397.06618860
   ht   7    -0.02025675    -1.34765719    -0.16071959    12.58929107     0.56890320   267.28992172  -276.20018120
   ht   8    -2.04546636     0.18078623   -14.90009580    -1.58195950    39.50647198   -35.51325821    36.10011765 -5305.24520286
   ht   9     0.00004406    -0.00858157    -0.00792852    -1.50192705     0.00714458    -2.63191946     1.41562659    -0.05887739
   ht  10    -0.03952408    -0.00060102     7.80514744    -0.10945250    -2.90547354    -0.10831149     0.02101858    -2.02224288
   ht  11    -0.20852559    -0.00172117   106.57411644    -0.74542285   -63.35550915     0.61485475    -0.28974522     2.07680014
   ht  12    -0.00388863    -0.63878625    -0.12450671    15.65654324    -0.01770409    -1.07331405     2.02236334    -1.68736249

                ht   9         ht  10         ht  11         ht  12
   ht   9    -1.31711120
   ht  10    -0.01645552    -2.02064159
   ht  11     0.09505302    -8.23925080  -118.56938430
   ht  12     8.51818659     0.12130091    -0.48755066 -5379.39893904

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.463285       4.176087E-03   0.861368       3.471810E-03  -1.895912E-03  -1.497605E-02  -1.318109E-03   1.773643E-02
 ref    2   9.450514E-03   0.973059      -1.038395E-02   5.096117E-02  -4.929042E-02  -1.258315E-04  -3.421411E-02   7.635100E-03

              v      9       v     10       v     11       v     12
 ref    1  -2.520413E-03  -7.471399E-02   1.714340E-02  -0.192238    
 ref    2  -0.135678      -6.107834E-03   0.167047       1.879078E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.214723       0.946861       0.742063       2.609095E-03   2.433140E-03   2.242979E-04   1.172343E-03   3.728758E-04

              v      9       v     10       v     11       v     12
 ref    1   1.841492E-02   5.619486E-03   2.819875E-02   3.730857E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.46328524     0.00417609     0.86136821     0.00347181    -0.00189591    -0.01497605    -0.00131811     0.01773643
 ref:   2     0.00945051     0.97305885    -0.01038395     0.05096117    -0.04929042    -0.00012583    -0.03421411     0.00763510

                ci   9         ci  10         ci  11         ci  12
 ref:   1    -0.00252041    -0.07471399     0.01714340    -0.19223807
 ref:   2    -0.13567818    -0.00610783     0.16704747     0.01879078

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 10  1   -196.4684645805  1.4338E-08  0.0000E+00  1.0807E-01  1.0000E-04   
 mr-sdci # 10  2   -196.4645601561  5.0323E-07  0.0000E+00  1.0518E-01  1.0000E-04   
 mr-sdci # 10  3   -196.4564791295  1.0990E-06  0.0000E+00  2.1668E-01  1.0000E-04   
 mr-sdci # 10  4   -194.4917007474  2.1294E-01  0.0000E+00  2.2655E+00  1.0000E-04   
 mr-sdci # 10  5   -194.0258590239  8.3013E-01  1.4410E+00  2.2835E+00  1.0000E-04   
 mr-sdci # 10  6   -193.1957241104  1.4116E+00  0.0000E+00  2.2693E+00  1.0000E-04   
 mr-sdci # 10  7   -191.7834746389  5.8263E-01  0.0000E+00  1.7189E+00  1.0000E-04   
 mr-sdci # 10  8   -191.2008453293  1.9107E-01  0.0000E+00  1.4906E+00  1.0000E-04   
 mr-sdci # 10  9   -191.0076319734  8.3929E-01  0.0000E+00  3.6848E+00  1.0000E-04   
 mr-sdci # 10 10   -190.1683342530  2.6338E-01  0.0000E+00  5.1657E+00  1.0000E-04   
 mr-sdci # 10 11   -189.9038666642  3.4493E-01  0.0000E+00  4.0028E+00  1.0000E-04   
 mr-sdci # 10 12   -189.5589327226  4.3671E+01  0.0000E+00  5.0292E+00  1.0000E-04   
 
 root number  5 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  11

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.27634771
   ht   2     0.00000000   -50.27320704
   ht   3    -0.40428965    -0.00042142  -105.01164411
   ht   4     0.00005603     0.40175812     0.89667707    -6.16649529
   ht   5     0.56766398     0.00543915    64.17026187    -0.55106057   -48.71612589
   ht   6    -0.10319127     2.25393441    -1.01649511   -23.19174779     1.46538896 -4397.06618860
   ht   7    -0.02025675    -1.34765719    -0.16071959    12.58929107     0.56890320   267.28992172  -276.20018120
   ht   8    -2.04546636     0.18078623   -14.90009580    -1.58195950    39.50647198   -35.51325821    36.10011765 -5305.24520286
   ht   9     0.00004406    -0.00858157    -0.00792852    -1.50192705     0.00714458    -2.63191946     1.41562659    -0.05887739
   ht  10    -0.03952408    -0.00060102     7.80514744    -0.10945250    -2.90547354    -0.10831149     0.02101858    -2.02224288
   ht  11    -0.20852559    -0.00172117   106.57411644    -0.74542285   -63.35550915     0.61485475    -0.28974522     2.07680014
   ht  12    -0.00388863    -0.63878625    -0.12450671    15.65654324    -0.01770409    -1.07331405     2.02236334    -1.68736249
   ht  13    -0.10412577     0.38579297    -2.33818253    -7.97841792     2.74921107    22.07426422   -13.09044209     1.00835905

                ht   9         ht  10         ht  11         ht  12         ht  13
   ht   9    -1.31711120
   ht  10    -0.01645552    -2.02064159
   ht  11     0.09505302    -8.23925080  -118.56938430
   ht  12     8.51818659     0.12130091    -0.48755066 -5379.39893904
   ht  13    -7.83974501    -1.00974799    -0.13566880   502.43136637 -7841.28496578

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.463436      -4.305232E-03  -0.861287      -3.033173E-03   9.346729E-07   2.374673E-03  -1.497373E-02   1.320650E-03
 ref    2  -9.661880E-03  -0.973060       1.062857E-02  -5.302711E-02   2.615084E-02   3.891504E-02  -1.070095E-04   3.423679E-02

              v      9       v     10       v     11       v     12       v     13
 ref    1   1.774035E-02   2.507270E-03  -7.467096E-02   1.739467E-02  -0.192233    
 ref    2   7.670116E-03   0.135614      -5.657028E-03   0.167122       1.882246E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.214866       0.946864       0.741928       2.821074E-03   6.838667E-04   1.520019E-03   2.242240E-04   1.173902E-03

              v      9       v     10       v     11       v     12       v     13
 ref    1   3.735506E-04   1.839735E-02   5.607755E-03   2.823238E-02   3.730796E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.46343613    -0.00430523    -0.86128660    -0.00303317     0.00000093     0.00237467    -0.01497373     0.00132065
 ref:   2    -0.00966188    -0.97305981     0.01062857    -0.05302711     0.02615084     0.03891504    -0.00010701     0.03423679

                ci   9         ci  10         ci  11         ci  12         ci  13
 ref:   1     0.01774035     0.00250727    -0.07467096     0.01739467    -0.19223339
 ref:   2     0.00767012     0.13561366    -0.00565703     0.16712213     0.01882246

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 11  1   -196.4684653931  8.1259E-07  0.0000E+00  1.0806E-01  1.0000E-04   
 mr-sdci # 11  2   -196.4645610097  8.5366E-07  0.0000E+00  1.0516E-01  1.0000E-04   
 mr-sdci # 11  3   -196.4564844830  5.3536E-06  0.0000E+00  2.1657E-01  1.0000E-04   
 mr-sdci # 11  4   -194.5443519983  5.2651E-02  0.0000E+00  2.2763E+00  1.0000E-04   
 mr-sdci # 11  5   -194.1213573647  9.5498E-02  0.0000E+00  1.1554E+00  1.0000E-04   
 mr-sdci # 11  6   -193.9046373998  7.0891E-01  4.2014E+00  2.0246E+00  1.0000E-04   
 mr-sdci # 11  7   -193.1957160790  1.4122E+00  0.0000E+00  2.2693E+00  1.0000E-04   
 mr-sdci # 11  8   -191.7834566379  5.8261E-01  0.0000E+00  1.7198E+00  1.0000E-04   
 mr-sdci # 11  9   -191.2008375488  1.9321E-01  0.0000E+00  1.4907E+00  1.0000E-04   
 mr-sdci # 11 10   -191.0075850918  8.3925E-01  0.0000E+00  3.6831E+00  1.0000E-04   
 mr-sdci # 11 11   -190.1682025878  2.6434E-01  0.0000E+00  5.1663E+00  1.0000E-04   
 mr-sdci # 11 12   -189.8996838328  3.4075E-01  0.0000E+00  4.0025E+00  1.0000E-04   
 mr-sdci # 11 13   -189.5589317614  4.3671E+01  0.0000E+00  5.0291E+00  1.0000E-04   
 
 root number  6 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  12

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.27634771
   ht   2     0.00000000   -50.27320704
   ht   3    -0.40428965    -0.00042142  -105.01164411
   ht   4     0.00005603     0.40175812     0.89667707    -6.16649529
   ht   5     0.56766398     0.00543915    64.17026187    -0.55106057   -48.71612589
   ht   6    -0.10319127     2.25393441    -1.01649511   -23.19174779     1.46538896 -4397.06618860
   ht   7    -0.02025675    -1.34765719    -0.16071959    12.58929107     0.56890320   267.28992172  -276.20018120
   ht   8    -2.04546636     0.18078623   -14.90009580    -1.58195950    39.50647198   -35.51325821    36.10011765 -5305.24520286
   ht   9     0.00004406    -0.00858157    -0.00792852    -1.50192705     0.00714458    -2.63191946     1.41562659    -0.05887739
   ht  10    -0.03952408    -0.00060102     7.80514744    -0.10945250    -2.90547354    -0.10831149     0.02101858    -2.02224288
   ht  11    -0.20852559    -0.00172117   106.57411644    -0.74542285   -63.35550915     0.61485475    -0.28974522     2.07680014
   ht  12    -0.00388863    -0.63878625    -0.12450671    15.65654324    -0.01770409    -1.07331405     2.02236334    -1.68736249
   ht  13    -0.10412577     0.38579297    -2.33818253    -7.97841792     2.74921107    22.07426422   -13.09044209     1.00835905
   ht  14     0.27084213    -0.86732571     5.64725691    16.91077876    -8.13335490   -50.98620348    26.11453328    -1.06007367

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14
   ht   9    -1.31711120
   ht  10    -0.01645552    -2.02064159
   ht  11     0.09505302    -8.23925080  -118.56938430
   ht  12     8.51818659     0.12130091    -0.48755066 -5379.39893904
   ht  13    -7.83974501    -1.00974799    -0.13566880   502.43136637 -7841.28496578
   ht  14    15.60919744     2.05216119     0.57179326  -868.64861446  1998.86223164-86815.68894321

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.463443       4.342274E-03   0.861283       3.114867E-03  -2.481199E-04   1.772901E-03  -1.431376E-03  -1.497551E-02
 ref    2   9.775146E-03   0.973064      -1.072882E-02   5.373912E-02  -2.647476E-02   1.553071E-02  -3.408545E-02  -6.646744E-05

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1  -1.326406E-03   1.773978E-02  -2.518716E-03   7.464594E-02   1.735930E-02   0.192245    
 ref    2  -3.423149E-02   7.670866E-03  -0.135631       5.536550E-03   0.167138      -1.874714E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.214875       0.946873       0.741923       2.897596E-03   7.009747E-04   2.443462E-04   1.163867E-03   2.242702E-04

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1   1.173554E-03   3.735419E-04   1.840220E-02   5.602669E-03   2.823634E-02   3.730948E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.46344296     0.00434227     0.86128297     0.00311487    -0.00024812     0.00177290    -0.00143138    -0.01497551
 ref:   2     0.00977515     0.97306419    -0.01072882     0.05373912    -0.02647476     0.01553071    -0.03408545    -0.00006647

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14
 ref:   1    -0.00132641     0.01773978    -0.00251872     0.07464594     0.01735930     0.19224471
 ref:   2    -0.03423149     0.00767087    -0.13563133     0.00553655     0.16713764    -0.01874714

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 12  1   -196.4684654396  4.6542E-08  2.7160E-03  1.0806E-01  1.0000E-04   
 mr-sdci # 12  2   -196.4645653958  4.3860E-06  0.0000E+00  1.0511E-01  1.0000E-04   
 mr-sdci # 12  3   -196.4564846448  1.6176E-07  0.0000E+00  2.1657E-01  1.0000E-04   
 mr-sdci # 12  4   -194.5569859486  1.2634E-02  0.0000E+00  2.3156E+00  1.0000E-04   
 mr-sdci # 12  5   -194.1358539612  1.4497E-02  0.0000E+00  1.2103E+00  1.0000E-04   
 mr-sdci # 12  6   -193.9942270224  8.9590E-02  0.0000E+00  1.0652E+00  1.0000E-04   
 mr-sdci # 12  7   -193.8036049174  6.0789E-01  0.0000E+00  1.7314E+00  1.0000E-04   
 mr-sdci # 12  8   -193.1957028093  1.4122E+00  0.0000E+00  2.2693E+00  1.0000E-04   
 mr-sdci # 12  9   -191.7834333529  5.8260E-01  0.0000E+00  1.7201E+00  1.0000E-04   
 mr-sdci # 12 10   -191.2008374197  1.9325E-01  0.0000E+00  1.4907E+00  1.0000E-04   
 mr-sdci # 12 11   -191.0075482656  8.3935E-01  0.0000E+00  3.6837E+00  1.0000E-04   
 mr-sdci # 12 12   -190.1681561755  2.6847E-01  0.0000E+00  5.1664E+00  1.0000E-04   
 mr-sdci # 12 13   -189.8988618751  3.3993E-01  0.0000E+00  4.0018E+00  1.0000E-04   
 mr-sdci # 12 14   -189.5589040848  4.3671E+01  0.0000E+00  5.0292E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.013000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  13

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.27634771
   ht   2     0.00000000   -50.27320704
   ht   3    -0.40428965    -0.00042142  -105.01164411
   ht   4     0.00005603     0.40175812     0.89667707    -6.16649529
   ht   5     0.56766398     0.00543915    64.17026187    -0.55106057   -48.71612589
   ht   6    -0.10319127     2.25393441    -1.01649511   -23.19174779     1.46538896 -4397.06618860
   ht   7    -0.02025675    -1.34765719    -0.16071959    12.58929107     0.56890320   267.28992172  -276.20018120
   ht   8    -2.04546636     0.18078623   -14.90009580    -1.58195950    39.50647198   -35.51325821    36.10011765 -5305.24520286
   ht   9     0.00004406    -0.00858157    -0.00792852    -1.50192705     0.00714458    -2.63191946     1.41562659    -0.05887739
   ht  10    -0.03952408    -0.00060102     7.80514744    -0.10945250    -2.90547354    -0.10831149     0.02101858    -2.02224288
   ht  11    -0.20852559    -0.00172117   106.57411644    -0.74542285   -63.35550915     0.61485475    -0.28974522     2.07680014
   ht  12    -0.00388863    -0.63878625    -0.12450671    15.65654324    -0.01770409    -1.07331405     2.02236334    -1.68736249
   ht  13    -0.10412577     0.38579297    -2.33818253    -7.97841792     2.74921107    22.07426422   -13.09044209     1.00835905
   ht  14     0.27084213    -0.86732571     5.64725691    16.91077876    -8.13335490   -50.98620348    26.11453328    -1.06007367
   ht  15    -0.00049698    -0.00005370     0.08480212    -0.00884627    -0.15167987    -0.01739749     0.01571835     0.28389497

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15
   ht   9    -1.31711120
   ht  10    -0.01645552    -2.02064159
   ht  11     0.09505302    -8.23925080  -118.56938430
   ht  12     8.51818659     0.12130091    -0.48755066 -5379.39893904
   ht  13    -7.83974501    -1.00974799    -0.13566880   502.43136637 -7841.28496578
   ht  14    15.60919744     2.05216119     0.57179326  -868.64861446  1998.86223164-86815.68894321
   ht  15    -0.00603926    -0.02398345     0.01274648     0.04252268    -0.04339757     0.04591655    -0.05323580

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.627161      -4.185257E-03   0.750223      -1.703433E-03   5.316119E-04  -6.534324E-03  -1.920108E-02   7.314508E-03
 ref    2  -1.430297E-02  -0.972902      -1.796914E-02  -5.379120E-02  -2.657727E-02   1.527994E-02   1.482896E-03   3.396509E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1  -1.320455E-02  -1.342950E-03   1.658112E-02  -2.446712E-03   0.108910      -0.126168       0.122930    
 ref    2   1.071104E-03  -3.423108E-02   8.635712E-03  -0.135575       0.128801       0.108165      -5.258341E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.393536       0.946556       0.563157       2.896395E-03   7.066338E-04   2.761741E-04   3.708804E-04   1.207130E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1   1.755075E-04   1.173570E-03   3.495092E-04   1.838662E-02   2.845126E-02   2.761812E-02   1.513947E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.62716111    -0.00418526     0.75022300    -0.00170343     0.00053161    -0.00653432    -0.01920108     0.00731451
 ref:   2    -0.01430297    -0.97290192    -0.01796914    -0.05379120    -0.02657727     0.01527994     0.00148290     0.03396509

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15
 ref:   1    -0.01320455    -0.00134295     0.01658112    -0.00244671     0.10891042    -0.12616827     0.12293014
 ref:   2     0.00107110    -0.03423108     0.00863571    -0.13557520     0.12880134     0.10816508    -0.00525834

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 13  1   -196.4713175157  2.8521E-03  0.0000E+00  5.1225E-02  1.0000E-04   
 mr-sdci # 13  2   -196.4645659285  5.3275E-07  3.6075E-03  1.0509E-01  1.0000E-04   
 mr-sdci # 13  3   -196.4586417904  2.1571E-03  0.0000E+00  1.8031E-01  1.0000E-04   
 mr-sdci # 13  4   -194.5579604217  9.7447E-04  0.0000E+00  2.3217E+00  1.0000E-04   
 mr-sdci # 13  5   -194.1360554556  2.0149E-04  0.0000E+00  1.2225E+00  1.0000E-04   
 mr-sdci # 13  6   -194.0062659460  1.2039E-02  0.0000E+00  1.5812E+00  1.0000E-04   
 mr-sdci # 13  7   -193.9190481593  1.1544E-01  0.0000E+00  3.0297E+00  1.0000E-04   
 mr-sdci # 13  8   -193.7916185006  5.9592E-01  0.0000E+00  2.0197E+00  1.0000E-04   
 mr-sdci # 13  9   -193.1354740134  1.3520E+00  0.0000E+00  2.2311E+00  1.0000E-04   
 mr-sdci # 13 10   -191.7834317660  5.8259E-01  0.0000E+00  1.7202E+00  1.0000E-04   
 mr-sdci # 13 11   -191.1766793326  1.6913E-01  0.0000E+00  1.2017E+00  1.0000E-04   
 mr-sdci # 13 12   -191.0074917554  8.3934E-01  0.0000E+00  3.6822E+00  1.0000E-04   
 mr-sdci # 13 13   -189.9180895959  1.9228E-02  0.0000E+00  4.2080E+00  1.0000E-04   
 mr-sdci # 13 14   -189.8644531487  3.0555E-01  0.0000E+00  4.3431E+00  1.0000E-04   
 mr-sdci # 13 15   -188.9709379787  4.3083E+01  0.0000E+00  5.1233E+00  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.008000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  14

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.27634771
   ht   2     0.00000000   -50.27320704
   ht   3    -0.40428965    -0.00042142  -105.01164411
   ht   4     0.00005603     0.40175812     0.89667707    -6.16649529
   ht   5     0.56766398     0.00543915    64.17026187    -0.55106057   -48.71612589
   ht   6    -0.10319127     2.25393441    -1.01649511   -23.19174779     1.46538896 -4397.06618860
   ht   7    -0.02025675    -1.34765719    -0.16071959    12.58929107     0.56890320   267.28992172  -276.20018120
   ht   8    -2.04546636     0.18078623   -14.90009580    -1.58195950    39.50647198   -35.51325821    36.10011765 -5305.24520286
   ht   9     0.00004406    -0.00858157    -0.00792852    -1.50192705     0.00714458    -2.63191946     1.41562659    -0.05887739
   ht  10    -0.03952408    -0.00060102     7.80514744    -0.10945250    -2.90547354    -0.10831149     0.02101858    -2.02224288
   ht  11    -0.20852559    -0.00172117   106.57411644    -0.74542285   -63.35550915     0.61485475    -0.28974522     2.07680014
   ht  12    -0.00388863    -0.63878625    -0.12450671    15.65654324    -0.01770409    -1.07331405     2.02236334    -1.68736249
   ht  13    -0.10412577     0.38579297    -2.33818253    -7.97841792     2.74921107    22.07426422   -13.09044209     1.00835905
   ht  14     0.27084213    -0.86732571     5.64725691    16.91077876    -8.13335490   -50.98620348    26.11453328    -1.06007367
   ht  15    -0.00049698    -0.00005370     0.08480212    -0.00884627    -0.15167987    -0.01739749     0.01571835     0.28389497
   ht  16    -0.00000683    -0.00311072     0.00102105    -0.52347420    -0.00053741    -1.52018892     0.88445263    -0.04151560

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15         ht  16
   ht   9    -1.31711120
   ht  10    -0.01645552    -2.02064159
   ht  11     0.09505302    -8.23925080  -118.56938430
   ht  12     8.51818659     0.12130091    -0.48755066 -5379.39893904
   ht  13    -7.83974501    -1.00974799    -0.13566880   502.43136637 -7841.28496578
   ht  14    15.60919744     2.05216119     0.57179326  -868.64861446  1998.86223164-86815.68894321
   ht  15    -0.00603926    -0.02398345     0.01274648     0.04252268    -0.04339757     0.04591655    -0.05323580
   ht  16    -0.20459827    -0.00447914     0.01977815     0.67070359    -0.31614021     0.04311541    -0.00117385    -0.17182104

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.627188       4.843044E-03  -0.750196       2.366683E-03   7.885536E-04   9.142184E-04  -8.547461E-03  -1.883500E-02
 ref    2  -1.753883E-02   0.967952       2.167355E-02   0.153676      -4.396112E-02  -1.473169E-02  -1.572437E-02   3.465673E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1  -5.906372E-03  -1.320513E-02   9.630312E-04   1.645490E-02  -9.391296E-04   0.166839      -0.115864       4.052564E-02
 ref    2   1.460697E-04   3.359975E-04   1.187427E-02   1.905387E-03  -5.278956E-02   5.114071E-03  -5.442678E-02  -0.174202    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.393672       0.936955       0.563263       2.362179E-02   1.933202E-03   2.178583E-04   3.203148E-04   3.667680E-04

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   3.490656E-05   1.744883E-04   1.419256E-04   2.743943E-04   2.787619E-03   2.786139E-02   1.638669E-02   3.198863E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15        ev   16

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.62718753     0.00484304    -0.75019575     0.00236668     0.00078855     0.00091422    -0.00854746    -0.01883500
 ref:   2    -0.01753883     0.96795214     0.02167355     0.15367560    -0.04396112    -0.01473169    -0.01572437     0.00346567

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1    -0.00590637    -0.01320513     0.00096303     0.01645490    -0.00093913     0.16683895    -0.11586378     0.04052564
 ref:   2     0.00014607     0.00033600     0.01187427     0.00190539    -0.05278956     0.00511407    -0.05442678    -0.17420191

 trial vector basis is being transformed.  new dimension:   8

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 14  1   -196.4713176332  1.1751E-07  0.0000E+00  5.1210E-02  1.0000E-04   
 mr-sdci # 14  2   -196.4672306447  2.6647E-03  0.0000E+00  6.5008E-02  1.0000E-04   
 mr-sdci # 14  3   -196.4586420612  2.7079E-07  7.4649E-03  1.8030E-01  1.0000E-04   
 mr-sdci # 14  4   -195.1691843576  6.1122E-01  0.0000E+00  2.8090E+00  1.0000E-04   
 mr-sdci # 14  5   -194.2856291624  1.4957E-01  0.0000E+00  1.5027E+00  1.0000E-04   
 mr-sdci # 14  6   -194.0828314735  7.6566E-02  0.0000E+00  1.2304E+00  1.0000E-04   
 mr-sdci # 14  7   -193.9702273657  5.1179E-02  0.0000E+00  2.0504E+00  1.0000E-04   
 mr-sdci # 14  8   -193.9189222595  1.2730E-01  0.0000E+00  2.9716E+00  1.0000E-04   
 mr-sdci # 14  9   -193.7292041992  5.9373E-01  0.0000E+00  2.5020E+00  1.0000E-04   
 mr-sdci # 14 10   -193.1354385076  1.3520E+00  0.0000E+00  2.2309E+00  1.0000E-04   
 mr-sdci # 14 11   -191.7664073862  5.8973E-01  0.0000E+00  1.3296E+00  1.0000E-04   
 mr-sdci # 14 12   -191.1762747469  1.6878E-01  0.0000E+00  1.1852E+00  1.0000E-04   
 mr-sdci # 14 13   -190.7234901842  8.0540E-01  0.0000E+00  2.0027E+00  1.0000E-04   
 mr-sdci # 14 14   -189.8871510044  2.2698E-02  0.0000E+00  4.5436E+00  1.0000E-04   
 mr-sdci # 14 15   -188.9818614488  1.0923E-02  0.0000E+00  5.0503E+00  1.0000E-04   
 mr-sdci # 14 16   -188.8671304634  4.2979E+01  0.0000E+00  4.3660E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  15

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58300284
   ht   2     0.00000000   -50.57891585
   ht   3     0.00000000     0.00000000   -50.57032727
   ht   4     0.00000000     0.00000000     0.00000000   -49.28086956
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -48.39731437
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.19451668
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.08191257
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.03060746
   ht   9    -0.06038893    -0.01297268    -0.07406336     0.00687772     0.02539797     0.00502681     0.11915563     0.19867288

                ht   9
   ht   9    -0.15890914

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.742237       5.149383E-03  -0.632929      -3.194051E-03   4.779269E-03   5.432481E-02  -4.751586E-04   8.891883E-03
 ref    2   1.383489E-02   0.967938       2.455249E-02  -0.153612      -4.446689E-02  -7.056376E-03  -1.436855E-02   1.510574E-02

              v      9
 ref    1   5.305500E-02
 ref    2  -1.442685E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.551108       0.936930       0.401202       2.360672E-02   2.000145E-03   3.000978E-03   2.066809E-04   3.072489E-04

              v      9
 ref    1   2.816915E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.74223738     0.00514938    -0.63292890    -0.00319405     0.00477927     0.05432481    -0.00047516     0.00889188
 ref:   2     0.01383489     0.96793794     0.02455249    -0.15361157    -0.04446689    -0.00705638    -0.01436855     0.01510574

                ci   9
 ref:   1     0.05305500
 ref:   2    -0.00144269

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 15  1   -196.4716073874  2.8975E-04  0.0000E+00  4.2656E-02  1.0000E-04   
 mr-sdci # 15  2   -196.4672306453  5.4545E-10  0.0000E+00  6.5010E-02  1.0000E-04   
 mr-sdci # 15  3   -196.4657314057  7.0893E-03  0.0000E+00  6.8919E-02  1.0000E-04   
 mr-sdci # 15  4   -195.1692441199  5.9762E-05  6.2462E+00  2.8093E+00  1.0000E-04   
 mr-sdci # 15  5   -194.2860240651  3.9490E-04  0.0000E+00  1.5229E+00  1.0000E-04   
 mr-sdci # 15  6   -194.1930071942  1.1018E-01  0.0000E+00  3.3076E+00  1.0000E-04   
 mr-sdci # 15  7   -194.0827024192  1.1248E-01  0.0000E+00  1.2346E+00  1.0000E-04   
 mr-sdci # 15  8   -193.9607304541  4.1808E-02  0.0000E+00  1.4122E+00  1.0000E-04   
 mr-sdci # 15  9   -193.8326244314  1.0342E-01  0.0000E+00  3.0434E+00  1.0000E-04   
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.006000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  16

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58300284
   ht   2     0.00000000   -50.57891585
   ht   3     0.00000000     0.00000000   -50.57032727
   ht   4     0.00000000     0.00000000     0.00000000   -49.28086956
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -48.39731437
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.19451668
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.08191257
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.03060746
   ht   9    -0.06038893    -0.01297268    -0.07406336     0.00687772     0.02539797     0.00502681     0.11915563     0.19867288
   ht  10     3.86617490    -5.77747881     1.03457305  -113.85313474   -14.77490189    -9.52896195    -2.45570270    13.46908937

                ht   9         ht  10
   ht   9    -0.15890914
   ht  10    -0.95180458-58302.86997719

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.742357      -7.754638E-03  -0.632753      -5.857137E-03  -1.738410E-03   6.011154E-03   5.411083E-02  -6.066856E-04
 ref    2  -1.453539E-02  -0.967630       2.947422E-02  -0.127555       8.789505E-02  -4.535933E-02  -6.176624E-03  -1.509416E-02

              v      9       v     10
 ref    1  -8.870849E-03   5.305839E-02
 ref    2  -1.525867E-02  -1.329156E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.551305       0.936368       0.401245       1.630455E-02   7.728562E-03   2.093603E-03   2.966133E-03   2.282018E-04

              v      9       v     10
 ref    1   3.115190E-04   2.816960E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.74235682    -0.00775464    -0.63275327    -0.00585714    -0.00173841     0.00601115     0.05411083    -0.00060669
 ref:   2    -0.01453539    -0.96763009     0.02947422    -0.12755487     0.08789505    -0.04535933    -0.00617662    -0.01509416

                ci   9         ci  10
 ref:   1    -0.00887085     0.05305839
 ref:   2    -0.01525867    -0.00132916

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 16  1   -196.4716078581  4.7073E-07  0.0000E+00  4.2645E-02  1.0000E-04   
 mr-sdci # 16  2   -196.4672535873  2.2942E-05  0.0000E+00  6.5576E-02  1.0000E-04   
 mr-sdci # 16  3   -196.4657339159  2.5102E-06  0.0000E+00  6.8869E-02  1.0000E-04   
 mr-sdci # 16  4   -195.3352427916  1.6600E-01  0.0000E+00  2.0774E+00  1.0000E-04   
 mr-sdci # 16  5   -194.9711133553  6.8509E-01  4.2869E-01  1.9877E+00  1.0000E-04   
 mr-sdci # 16  6   -194.2817716733  8.8764E-02  0.0000E+00  1.5337E+00  1.0000E-04   
 mr-sdci # 16  7   -194.1924744970  1.0977E-01  0.0000E+00  3.3020E+00  1.0000E-04   
 mr-sdci # 16  8   -194.0813966562  1.2067E-01  0.0000E+00  1.2487E+00  1.0000E-04   
 mr-sdci # 16  9   -193.9606594908  1.2804E-01  0.0000E+00  1.4152E+00  1.0000E-04   
 mr-sdci # 16 10   -193.8325402379  6.9710E-01  0.0000E+00  3.0432E+00  1.0000E-04   
 
 root number  5 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.006000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  17

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58300284
   ht   2     0.00000000   -50.57891585
   ht   3     0.00000000     0.00000000   -50.57032727
   ht   4     0.00000000     0.00000000     0.00000000   -49.28086956
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -48.39731437
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.19451668
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.08191257
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.03060746
   ht   9    -0.06038893    -0.01297268    -0.07406336     0.00687772     0.02539797     0.00502681     0.11915563     0.19867288
   ht  10     3.86617490    -5.77747881     1.03457305  -113.85313474   -14.77490189    -9.52896195    -2.45570270    13.46908937
   ht  11     7.00809842    -0.49140491     3.11133433    21.04598859     4.38422402     1.49681597     1.60461517     0.37477039

                ht   9         ht  10         ht  11
   ht   9    -0.15890914
   ht  10    -0.95180458-58302.86997719
   ht  11     0.75565185   278.23081460-11482.18848263

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.743352       5.972493E-03   0.631530      -2.494463E-03   1.106797E-02  -1.966790E-02  -3.066334E-03  -5.149350E-02
 ref    2   1.378799E-02   0.967729      -2.584738E-02  -0.127843      -7.159704E-02  -5.024982E-02   4.542428E-02   8.216562E-03

              v      9       v     10       v     11
 ref    1   5.760185E-04   8.398534E-03  -5.296734E-02
 ref    2   1.526326E-02   1.509416E-02   2.049533E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.552762       0.936535       0.399498       1.635017E-02   5.248636E-03   2.911871E-03   2.072767E-03   2.719093E-03

              v      9       v     10       v     11
 ref    1   2.332990E-04   2.983690E-04   2.809740E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.74335191     0.00597249     0.63153018    -0.00249446     0.01106797    -0.01966790    -0.00306633    -0.05149350
 ref:   2     0.01378799     0.96772884    -0.02584738    -0.12784345    -0.07159704    -0.05024982     0.04542428     0.00821656

                ci   9         ci  10         ci  11
 ref:   1     0.00057602     0.00839853    -0.05296734
 ref:   2     0.01526326     0.01509416     0.00204953

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 17  1   -196.4716133915  5.5333E-06  0.0000E+00  4.2614E-02  1.0000E-04   
 mr-sdci # 17  2   -196.4672555267  1.9394E-06  0.0000E+00  6.5637E-02  1.0000E-04   
 mr-sdci # 17  3   -196.4657502960  1.6380E-05  0.0000E+00  6.8576E-02  1.0000E-04   
 mr-sdci # 17  4   -195.3465850129  1.1342E-02  0.0000E+00  2.0622E+00  1.0000E-04   
 mr-sdci # 17  5   -194.9841195053  1.3006E-02  0.0000E+00  1.6668E+00  1.0000E-04   
 mr-sdci # 17  6   -194.9136360765  6.3186E-01 -1.3298E+00  1.4354E+00  1.0000E-04   
 mr-sdci # 17  7   -194.2804082424  8.7934E-02  0.0000E+00  1.5209E+00  1.0000E-04   
 mr-sdci # 17  8   -194.1855001359  1.0410E-01  0.0000E+00  3.3140E+00  1.0000E-04   
 mr-sdci # 17  9   -194.0813288492  1.2067E-01  0.0000E+00  1.2486E+00  1.0000E-04   
 mr-sdci # 17 10   -193.9604686185  1.2793E-01  0.0000E+00  1.4033E+00  1.0000E-04   
 mr-sdci # 17 11   -193.8233700957  2.0570E+00  0.0000E+00  3.0275E+00  1.0000E-04   
 
 root number  6 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.006000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  18

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58300284
   ht   2     0.00000000   -50.57891585
   ht   3     0.00000000     0.00000000   -50.57032727
   ht   4     0.00000000     0.00000000     0.00000000   -49.28086956
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -48.39731437
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.19451668
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.08191257
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.03060746
   ht   9    -0.06038893    -0.01297268    -0.07406336     0.00687772     0.02539797     0.00502681     0.11915563     0.19867288
   ht  10     3.86617490    -5.77747881     1.03457305  -113.85313474   -14.77490189    -9.52896195    -2.45570270    13.46908937
   ht  11     7.00809842    -0.49140491     3.11133433    21.04598859     4.38422402     1.49681597     1.60461517     0.37477039
   ht  12    -5.74114912    -2.27609849    -1.45502290     7.38448278    40.47319668    22.92237922    24.96842132     0.81075820

                ht   9         ht  10         ht  11         ht  12
   ht   9    -0.15890914
   ht  10    -0.95180458-58302.86997719
   ht  11     0.75565185   278.23081460-11482.18848263
   ht  12    -2.36696026    65.71582745  -191.82713332-26444.36955070

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.743503      -5.419798E-03  -0.631342      -2.494624E-03   1.916772E-02  -1.259397E-02  -7.923281E-03  -3.375224E-03
 ref    2  -1.358544E-02  -0.967759       2.476021E-02  -0.127843      -4.826338E-02  -6.581662E-02  -3.160563E-02   4.552559E-02

              v      9       v     10       v     11       v     12
 ref    1  -5.094065E-02   5.757802E-04  -8.390444E-03  -5.302226E-02
 ref    2   7.711516E-03   1.526315E-02  -1.509855E-02   1.879408E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.552982       0.936586       0.399206       1.635017E-02   2.696755E-03   4.490436E-03   1.061694E-03   2.083972E-03

              v      9       v     10       v     11       v     12
 ref    1   2.654417E-03   2.332953E-04   2.983658E-04   2.814892E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.74350332    -0.00541980    -0.63134243    -0.00249462     0.01916772    -0.01259397    -0.00792328    -0.00337522
 ref:   2    -0.01358544    -0.96775855     0.02476021    -0.12784344    -0.04826338    -0.06581662    -0.03160563     0.04552559

                ci   9         ci  10         ci  11         ci  12
 ref:   1    -0.05094065     0.00057578    -0.00839044    -0.05302226
 ref:   2     0.00771152     0.01526315    -0.01509855     0.00187941

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 18  1   -196.4716141595  7.6806E-07  6.5297E-04  4.2604E-02  1.0000E-04   
 mr-sdci # 18  2   -196.4672565573  1.0305E-06  0.0000E+00  6.5565E-02  1.0000E-04   
 mr-sdci # 18  3   -196.4657530720  2.7760E-06  0.0000E+00  6.8563E-02  1.0000E-04   
 mr-sdci # 18  4   -195.3465850130  8.1712E-11  0.0000E+00  2.0622E+00  1.0000E-04   
 mr-sdci # 18  5   -194.9914786418  7.3591E-03  0.0000E+00  1.2060E+00  1.0000E-04   
 mr-sdci # 18  6   -194.9656519096  5.2016E-02  0.0000E+00  1.6767E+00  1.0000E-04   
 mr-sdci # 18  7   -194.8566170564  5.7621E-01  0.0000E+00  9.6918E-01  1.0000E-04   
 mr-sdci # 18  8   -194.2803323174  9.4832E-02  0.0000E+00  1.5233E+00  1.0000E-04   
 mr-sdci # 18  9   -194.1833578005  1.0203E-01  0.0000E+00  3.3147E+00  1.0000E-04   
 mr-sdci # 18 10   -194.0813288485  1.2086E-01  0.0000E+00  1.2486E+00  1.0000E-04   
 mr-sdci # 18 11   -193.9604679161  1.3710E-01  0.0000E+00  1.4031E+00  1.0000E-04   
 mr-sdci # 18 12   -193.8219027741  2.6456E+00  0.0000E+00  3.0258E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.013000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  19

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58300284
   ht   2     0.00000000   -50.57891585
   ht   3     0.00000000     0.00000000   -50.57032727
   ht   4     0.00000000     0.00000000     0.00000000   -49.28086956
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -48.39731437
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.19451668
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.08191257
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.03060746
   ht   9    -0.06038893    -0.01297268    -0.07406336     0.00687772     0.02539797     0.00502681     0.11915563     0.19867288
   ht  10     3.86617490    -5.77747881     1.03457305  -113.85313474   -14.77490189    -9.52896195    -2.45570270    13.46908937
   ht  11     7.00809842    -0.49140491     3.11133433    21.04598859     4.38422402     1.49681597     1.60461517     0.37477039
   ht  12    -5.74114912    -2.27609849    -1.45502290     7.38448278    40.47319668    22.92237922    24.96842132     0.81075820
   ht  13     0.22982262     0.00790197     0.15972222     0.01534311    -0.00505163    -0.00926500     0.06548827     0.13425912

                ht   9         ht  10         ht  11         ht  12         ht  13
   ht   9    -0.15890914
   ht  10    -0.95180458-58302.86997719
   ht  11     0.75565185   278.23081460-11482.18848263
   ht  12    -2.36696026    65.71582745  -191.82713332-26444.36955070
   ht  13    -0.00236824    -0.01986016    -0.00296203    -0.06674199    -0.01967780

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.792781       7.866355E-03  -0.561949       0.132937       1.478802E-02  -1.887030E-03  -8.093816E-04   2.691043E-03
 ref    2   1.451803E-02   0.967438       3.471192E-02  -8.709378E-03   0.127589       6.303280E-02   5.162116E-02   3.184456E-02

              v      9       v     10       v     11       v     12       v     13
 ref    1  -3.089568E-03   5.358699E-02  -3.960316E-03   8.876556E-04  -2.665849E-02
 ref    2   4.552582E-02  -7.734000E-03  -1.531225E-02   1.513237E-02  -1.366230E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.628713       0.935999       0.316992       1.774814E-02   1.649769E-02   3.976695E-03   2.665400E-03   1.021317E-03

              v      9       v     10       v     11       v     12       v     13
 ref    1   2.082146E-03   2.931380E-03   2.501490E-04   2.297767E-04   7.125416E-04

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.79278113     0.00786636    -0.56194950     0.13293716     0.01478802    -0.00188703    -0.00080938     0.00269104
 ref:   2     0.01451803     0.96743844     0.03471192    -0.00870938     0.12758918     0.06303280     0.05162116     0.03184456

                ci   9         ci  10         ci  11         ci  12         ci  13
 ref:   1    -0.00308957     0.05358699    -0.00396032     0.00088766    -0.02665849
 ref:   2     0.04552582    -0.00773400    -0.01531225     0.01513237    -0.00136623

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 19  1   -196.4723992381  7.8508E-04  0.0000E+00  3.0385E-02  1.0000E-04   
 mr-sdci # 19  2   -196.4672569441  3.8688E-07  1.1008E-03  6.5578E-02  1.0000E-04   
 mr-sdci # 19  3   -196.4661118746  3.5880E-04  0.0000E+00  6.4808E-02  1.0000E-04   
 mr-sdci # 19  4   -195.3753125298  2.8728E-02  0.0000E+00  1.4376E+00  1.0000E-04   
 mr-sdci # 19  5   -195.3463380826  3.5486E-01  0.0000E+00  2.0551E+00  1.0000E-04   
 mr-sdci # 19  6   -194.9856279867  1.9976E-02  0.0000E+00  1.4503E+00  1.0000E-04   
 mr-sdci # 19  7   -194.9594281808  1.0281E-01  0.0000E+00  1.4641E+00  1.0000E-04   
 mr-sdci # 19  8   -194.8556238577  5.7529E-01  0.0000E+00  9.8254E-01  1.0000E-04   
 mr-sdci # 19  9   -194.2803269271  9.6969E-02  0.0000E+00  1.5248E+00  1.0000E-04   
 mr-sdci # 19 10   -194.1829760810  1.0165E-01  0.0000E+00  3.3014E+00  1.0000E-04   
 mr-sdci # 19 11   -194.0808463192  1.2038E-01  0.0000E+00  1.2648E+00  1.0000E-04   
 mr-sdci # 19 12   -193.9586211086  1.3672E-01  0.0000E+00  1.3150E+00  1.0000E-04   
 mr-sdci # 19 13   -192.6786288441  1.9551E+00  0.0000E+00  3.0515E+00  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.006000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  20

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58300284
   ht   2     0.00000000   -50.57891585
   ht   3     0.00000000     0.00000000   -50.57032727
   ht   4     0.00000000     0.00000000     0.00000000   -49.28086956
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -48.39731437
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.19451668
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.08191257
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.03060746
   ht   9    -0.06038893    -0.01297268    -0.07406336     0.00687772     0.02539797     0.00502681     0.11915563     0.19867288
   ht  10     3.86617490    -5.77747881     1.03457305  -113.85313474   -14.77490189    -9.52896195    -2.45570270    13.46908937
   ht  11     7.00809842    -0.49140491     3.11133433    21.04598859     4.38422402     1.49681597     1.60461517     0.37477039
   ht  12    -5.74114912    -2.27609849    -1.45502290     7.38448278    40.47319668    22.92237922    24.96842132     0.81075820
   ht  13     0.22982262     0.00790197     0.15972222     0.01534311    -0.00505163    -0.00926500     0.06548827     0.13425912
   ht  14     0.00976801    -0.10144962     0.00085810    -0.13690961    -0.06536567    -0.06968128    -0.08206455     0.01234577

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14
   ht   9    -0.15890914
   ht  10    -0.95180458-58302.86997719
   ht  11     0.75565185   278.23081460-11482.18848263
   ht  12    -2.36696026    65.71582745  -191.82713332-26444.36955070
   ht  13    -0.00236824    -0.01986016    -0.00296203    -0.06674199    -0.01967780
   ht  14    -0.00402035     0.81049876    -0.42244252     0.20186531     0.00077474    -0.03674918

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.792809      -1.731086E-02   0.561698      -6.502273E-03   0.133124      -1.160969E-02  -1.071261E-04   1.988338E-03
 ref    2  -1.555609E-02  -0.962400      -5.259096E-02  -0.193572      -6.662189E-03   1.015253E-02   8.041942E-03   1.640249E-02

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1  -8.311946E-03  -4.174586E-02  -3.255979E-02   3.099561E-03   4.808773E-03  -2.654909E-02
 ref    2  -1.419198E-02  -1.035289E-02   1.383627E-02  -4.805732E-03  -1.322090E-02  -9.209707E-04

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.628788       0.926513       0.318270       3.751241E-02   1.776638E-02   2.378587E-04   6.468430E-05   2.729950E-04

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1   2.705008E-04   1.849899E-03   1.251582E-03   3.270233E-05   1.979166E-04   7.057024E-04

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.79280864    -0.01731086     0.56169773    -0.00650227     0.13312399    -0.01160969    -0.00010713     0.00198834
 ref:   2    -0.01555609    -0.96239986    -0.05259096    -0.19357204    -0.00666219     0.01015253     0.00804194     0.01640249

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14
 ref:   1    -0.00831195    -0.04174586    -0.03255979     0.00309956     0.00480877    -0.02654909
 ref:   2    -0.01419198    -0.01035289     0.01383627    -0.00480573    -0.01322090    -0.00092097

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 20  1   -196.4723992624  2.4368E-08  0.0000E+00  3.0393E-02  1.0000E-04   
 mr-sdci # 20  2   -196.4683000371  1.0431E-03  0.0000E+00  4.0568E-02  1.0000E-04   
 mr-sdci # 20  3   -196.4661126874  8.1281E-07  1.1399E-03  6.4619E-02  1.0000E-04   
 mr-sdci # 20  4   -195.6171571077  2.4184E-01  0.0000E+00  1.4951E+00  1.0000E-04   
 mr-sdci # 20  5   -195.3753067727  2.8969E-02  0.0000E+00  1.4353E+00  1.0000E-04   
 mr-sdci # 20  6   -195.1823636110  1.9674E-01  0.0000E+00  1.0364E+00  1.0000E-04   
 mr-sdci # 20  7   -194.9678848806  8.4567E-03  0.0000E+00  8.3007E-01  1.0000E-04   
 mr-sdci # 20  8   -194.8606385431  5.0147E-03  0.0000E+00  7.7455E-01  1.0000E-04   
 mr-sdci # 20  9   -194.6585487178  3.7822E-01  0.0000E+00  2.5156E+00  1.0000E-04   
 mr-sdci # 20 10   -194.1956971313  1.2721E-02  0.0000E+00  2.6524E+00  1.0000E-04   
 mr-sdci # 20 11   -194.1532198142  7.2373E-02  0.0000E+00  2.1930E+00  1.0000E-04   
 mr-sdci # 20 12   -194.0056880630  4.7067E-02  0.0000E+00  8.7436E-01  1.0000E-04   
 mr-sdci # 20 13   -193.3703530254  6.9172E-01  0.0000E+00  3.3170E+00  1.0000E-04   
 mr-sdci # 20 14   -192.6773462375  2.7902E+00  0.0000E+00  3.0360E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.009000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  21

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58300284
   ht   2     0.00000000   -50.57891585
   ht   3     0.00000000     0.00000000   -50.57032727
   ht   4     0.00000000     0.00000000     0.00000000   -49.28086956
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -48.39731437
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.19451668
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.08191257
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.03060746
   ht   9    -0.06038893    -0.01297268    -0.07406336     0.00687772     0.02539797     0.00502681     0.11915563     0.19867288
   ht  10     3.86617490    -5.77747881     1.03457305  -113.85313474   -14.77490189    -9.52896195    -2.45570270    13.46908937
   ht  11     7.00809842    -0.49140491     3.11133433    21.04598859     4.38422402     1.49681597     1.60461517     0.37477039
   ht  12    -5.74114912    -2.27609849    -1.45502290     7.38448278    40.47319668    22.92237922    24.96842132     0.81075820
   ht  13     0.22982262     0.00790197     0.15972222     0.01534311    -0.00505163    -0.00926500     0.06548827     0.13425912
   ht  14     0.00976801    -0.10144962     0.00085810    -0.13690961    -0.06536567    -0.06968128    -0.08206455     0.01234577
   ht  15    -0.48145936    -0.03872784    -0.37651172     0.12037937     0.05221539     0.03922190     0.19541183     0.25825660

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15
   ht   9    -0.15890914
   ht  10    -0.95180458-58302.86997719
   ht  11     0.75565185   278.23081460-11482.18848263
   ht  12    -2.36696026    65.71582745  -191.82713332-26444.36955070
   ht  13    -0.00236824    -0.01986016    -0.00296203    -0.06674199    -0.01967780
   ht  14    -0.00402035     0.81049876    -0.42244252     0.20186531     0.00077474    -0.03674918
   ht  15     0.01437269     0.18453320    -0.06371691     0.08628126     0.00674484    -0.00173960    -0.04689033

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.801482       8.688796E-03  -0.546268       1.611514E-02   0.122484      -0.108227      -5.589542E-03   3.896026E-03
 ref    2  -1.354332E-02  -0.963842       4.901359E-03   0.192394      -2.223993E-02   4.541939E-03   9.930802E-03  -8.026106E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1  -4.269862E-03  -9.291683E-03  -9.422969E-04   2.088067E-03   1.038940E-04  -1.685348E-02   6.307394E-03
 ref    2  -1.649984E-02   1.435592E-02   1.762624E-02   4.735512E-03  -1.330652E-02  -1.760549E-03   2.371181E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.642556       0.929067       0.298432       3.727499E-02   1.549691E-02   1.173361E-02   1.298638E-04   7.959739E-05

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1   2.904763E-04   2.924278E-04   3.115721E-04   2.678510E-05   1.770744E-04   2.871394E-04   4.540572E-05

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.80148152     0.00868880    -0.54626750     0.01611514     0.12248387    -0.10822651    -0.00558954     0.00389603
 ref:   2    -0.01354332    -0.96384183     0.00490136     0.19239358    -0.02223993     0.00454194     0.00993080    -0.00802611

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15
 ref:   1    -0.00426986    -0.00929168    -0.00094230     0.00208807     0.00010389    -0.01685348     0.00630739
 ref:   2    -0.01649984     0.01435592     0.01762624     0.00473551    -0.01330652    -0.00176055     0.00237118

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 21  1   -196.4724069719  7.7095E-06  0.0000E+00  3.1524E-02  1.0000E-04   
 mr-sdci # 21  2   -196.4683054091  5.3720E-06  0.0000E+00  3.9757E-02  1.0000E-04   
 mr-sdci # 21  3   -196.4672101828  1.0975E-03  0.0000E+00  4.5119E-02  1.0000E-04   
 mr-sdci # 21  4   -195.6175939575  4.3685E-04  3.9406E-01  1.5251E+00  1.0000E-04   
 mr-sdci # 21  5   -195.5532424923  1.7794E-01  0.0000E+00  1.3734E+00  1.0000E-04   
 mr-sdci # 21  6   -195.3658420734  1.8348E-01  0.0000E+00  1.4574E+00  1.0000E-04   
 mr-sdci # 21  7   -195.1795529534  2.1167E-01  0.0000E+00  1.0026E+00  1.0000E-04   
 mr-sdci # 21  8   -194.9667332287  1.0609E-01  0.0000E+00  7.9919E-01  1.0000E-04   
 mr-sdci # 21  9   -194.8602203486  2.0167E-01  0.0000E+00  7.7980E-01  1.0000E-04   
 mr-sdci # 21 10   -194.6343869188  4.3869E-01  0.0000E+00  2.4574E+00  1.0000E-04   
 mr-sdci # 21 11   -194.1692760822  1.6056E-02  0.0000E+00  1.1275E+00  1.0000E-04   
 mr-sdci # 21 12   -194.0078936723  2.2056E-03  0.0000E+00  8.3459E-01  1.0000E-04   
 mr-sdci # 21 13   -193.4185491372  4.8196E-02  0.0000E+00  3.4415E+00  1.0000E-04   
 mr-sdci # 21 14   -192.9057036192  2.2836E-01  0.0000E+00  3.1700E+00  1.0000E-04   
 mr-sdci # 21 15   -192.0690495096  3.0872E+00  0.0000E+00  4.2702E+00  1.0000E-04   
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.008000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  22

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58300284
   ht   2     0.00000000   -50.57891585
   ht   3     0.00000000     0.00000000   -50.57032727
   ht   4     0.00000000     0.00000000     0.00000000   -49.28086956
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -48.39731437
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.19451668
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.08191257
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -48.03060746
   ht   9    -0.06038893    -0.01297268    -0.07406336     0.00687772     0.02539797     0.00502681     0.11915563     0.19867288
   ht  10     3.86617490    -5.77747881     1.03457305  -113.85313474   -14.77490189    -9.52896195    -2.45570270    13.46908937
   ht  11     7.00809842    -0.49140491     3.11133433    21.04598859     4.38422402     1.49681597     1.60461517     0.37477039
   ht  12    -5.74114912    -2.27609849    -1.45502290     7.38448278    40.47319668    22.92237922    24.96842132     0.81075820
   ht  13     0.22982262     0.00790197     0.15972222     0.01534311    -0.00505163    -0.00926500     0.06548827     0.13425912
   ht  14     0.00976801    -0.10144962     0.00085810    -0.13690961    -0.06536567    -0.06968128    -0.08206455     0.01234577
   ht  15    -0.48145936    -0.03872784    -0.37651172     0.12037937     0.05221539     0.03922190     0.19541183     0.25825660
   ht  16     0.93147169    -4.02796978     0.18702292    17.38898933    10.30893643     8.28561587     9.31377227    -0.29881874

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15         ht  16
   ht   9    -0.15890914
   ht  10    -0.95180458-58302.86997719
   ht  11     0.75565185   278.23081460-11482.18848263
   ht  12    -2.36696026    65.71582745  -191.82713332-26444.36955070
   ht  13    -0.00236824    -0.01986016    -0.00296203    -0.06674199    -0.01967780
   ht  14    -0.00402035     0.81049876    -0.42244252     0.20186531     0.00077474    -0.03674918
   ht  15     0.01437269     0.18453320    -0.06371691     0.08628126     0.00674484    -0.00173960    -0.04689033
   ht  16    -0.26435840   -37.10876439    17.17709060   -14.43453199    -0.07863817     0.22581854    -0.08951047   -75.29612547

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.801210       4.105838E-03   0.546708       2.324904E-02  -0.118412      -0.110955      -7.758033E-03   5.733168E-03
 ref    2  -1.187601E-02   0.963067      -2.519473E-02   0.169279       4.878833E-02  -7.668019E-03  -2.169950E-02   4.261165E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1  -2.280930E-03  -3.527000E-03  -1.316419E-02   9.106042E-04  -2.228719E-03  -4.498068E-04   1.665676E-02   6.326803E-03
 ref    2   3.253086E-02  -6.465775E-02  -2.468389E-02  -1.776909E-02  -5.376490E-03  -1.566006E-02   7.127024E-04   2.491808E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.642078       0.927515       0.299525       2.919579E-02   1.640178E-02   1.236973E-02   5.310552E-04   1.848622E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   1.063459E-03   4.193064E-03   7.825901E-04   3.165698E-04   3.387384E-05   2.454397E-04   2.779556E-04   4.623754E-05

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15        ev   16

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.80120985     0.00410584     0.54670808     0.02324904    -0.11841233    -0.11095464    -0.00775803     0.00573317
 ref:   2    -0.01187601     0.96306718    -0.02519473     0.16927867     0.04878833    -0.00766802    -0.02169950     0.04261165

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1    -0.00228093    -0.00352700    -0.01316419     0.00091060    -0.00222872    -0.00044981     0.01665676     0.00632680
 ref:   2     0.03253086    -0.06465775    -0.02468389    -0.01776909    -0.00537649    -0.01566006     0.00071270     0.00249181

 trial vector basis is being transformed.  new dimension:   8

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 22  1   -196.4724078078  8.3583E-07  0.0000E+00  3.1657E-02  1.0000E-04   
 mr-sdci # 22  2   -196.4683693013  6.3892E-05  0.0000E+00  3.2715E-02  1.0000E-04   
 mr-sdci # 22  3   -196.4672187837  8.6009E-06  0.0000E+00  4.4540E-02  1.0000E-04   
 mr-sdci # 22  4   -195.7866537078  1.6906E-01  0.0000E+00  7.8066E-01  1.0000E-04   
 mr-sdci # 22  5   -195.5547939372  1.5514E-03  1.1069E+00  1.3194E+00  1.0000E-04   
 mr-sdci # 22  6   -195.3675605755  1.7185E-03  0.0000E+00  1.4560E+00  1.0000E-04   
 mr-sdci # 22  7   -195.1969069842  1.7354E-02  0.0000E+00  1.1800E+00  1.0000E-04   
 mr-sdci # 22  8   -195.0133710131  4.6638E-02  0.0000E+00  1.2638E+00  1.0000E-04   
 mr-sdci # 22  9   -194.9119059361  5.1686E-02  0.0000E+00  1.1837E+00  1.0000E-04   
 mr-sdci # 22 10   -194.7785112775  1.4412E-01  0.0000E+00  2.0461E+00  1.0000E-04   
 mr-sdci # 22 11   -194.5626635636  3.9339E-01  0.0000E+00  2.2614E+00  1.0000E-04   
 mr-sdci # 22 12   -194.1692714952  1.6138E-01  0.0000E+00  1.1266E+00  1.0000E-04   
 mr-sdci # 22 13   -194.0077567379  5.8921E-01  0.0000E+00  8.3154E-01  1.0000E-04   
 mr-sdci # 22 14   -193.4138818703  5.0818E-01  0.0000E+00  3.4202E+00  1.0000E-04   
 mr-sdci # 22 15   -192.9042703417  8.3522E-01  0.0000E+00  3.1701E+00  1.0000E-04   
 mr-sdci # 22 16   -192.0690096684  3.2019E+00  0.0000E+00  4.2704E+00  1.0000E-04   
 
 root number  5 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.011000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  23

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58409301
   ht   2     0.00000000   -50.58005451
   ht   3     0.00000000     0.00000000   -50.57890399
   ht   4     0.00000000     0.00000000     0.00000000   -49.89833891
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -49.66647914
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.47924578
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.30859219
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.12505622
   ht   9    -2.07722447    10.96384051    -5.56691537  -139.30980997   -95.39754672   -44.46236096   -49.69043933    56.94226388

                ht   9
   ht   9 -9203.75064950

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.801136      -6.346525E-03   0.546787      -2.773325E-02  -8.746452E-02   6.964657E-02   0.116417      -1.115164E-02
 ref    2   1.164161E-02  -0.962904      -2.884859E-02  -0.169062       3.764411E-02  -4.362644E-02   3.190800E-03  -1.864816E-02

              v      9
 ref    1   9.208657E-03
 ref    2   4.018326E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.641955       0.927225       0.299808       2.935096E-02   9.067121E-03   6.753911E-03   1.356310E-02   4.721129E-04

              v      9
 ref    1   1.699494E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.80113610    -0.00634652     0.54678717    -0.02773325    -0.08746452     0.06964657     0.11641700    -0.01115164
 ref:   2     0.01164161    -0.96290419    -0.02884859    -0.16906162     0.03764411    -0.04362644     0.00319080    -0.01864816

                ci   9
 ref:   1     0.00920866
 ref:   2     0.04018326

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 23  1   -196.4724079797  1.7192E-07  0.0000E+00  3.1711E-02  1.0000E-04   
 mr-sdci # 23  2   -196.4683756218  6.3205E-06  0.0000E+00  3.2827E-02  1.0000E-04   
 mr-sdci # 23  3   -196.4672217374  2.9537E-06  0.0000E+00  4.4210E-02  1.0000E-04   
 mr-sdci # 23  4   -195.7898098702  3.1562E-03  0.0000E+00  7.9286E-01  1.0000E-04   
 mr-sdci # 23  5   -195.6257131971  7.0919E-02  0.0000E+00  9.0171E-01  1.0000E-04   
 mr-sdci # 23  6   -195.4573835593  8.9823E-02  3.1191E-01  1.2447E+00  1.0000E-04   
 mr-sdci # 23  7   -195.3661422331  1.6924E-01  0.0000E+00  1.4614E+00  1.0000E-04   
 mr-sdci # 23  8   -195.1948508995  1.8148E-01  0.0000E+00  1.1703E+00  1.0000E-04   
 mr-sdci # 23  9   -195.0071192301  9.5213E-02  0.0000E+00  1.2797E+00  1.0000E-04   
 
 root number  6 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  24

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58409301
   ht   2     0.00000000   -50.58005451
   ht   3     0.00000000     0.00000000   -50.57890399
   ht   4     0.00000000     0.00000000     0.00000000   -49.89833891
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -49.66647914
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.47924578
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.30859219
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.12505622
   ht   9    -2.07722447    10.96384051    -5.56691537  -139.30980997   -95.39754672   -44.46236096   -49.69043933    56.94226388
   ht  10     4.19337979    -0.18104728    -0.54599510    21.07093148     7.24453040    10.44173562     7.16817747    -8.46998654

                ht   9         ht  10
   ht   9 -9203.75064950
   ht  10   174.65780057  -437.71156679

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.801211       7.523025E-03  -0.546606      -3.220313E-02  -9.336524E-02   2.198018E-02  -9.903319E-02  -8.258306E-02
 ref    2  -1.166095E-02   0.962830       3.102959E-02  -0.167395       4.776668E-02  -1.937147E-02  -1.233089E-02   3.703703E-02

              v      9       v     10
 ref    1  -1.141824E-02   1.062618E-02
 ref    2  -1.846759E-02   3.912014E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.642075       0.927097       0.299741       2.905828E-02   1.099872E-02   8.583822E-04   9.959623E-03   8.191703E-03

              v      9       v     10
 ref    1   4.714281E-04   1.643301E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.80121097     0.00752303    -0.54660572    -0.03220313    -0.09336524     0.02198018    -0.09903319    -0.08258306
 ref:   2    -0.01166095     0.96282960     0.03102959    -0.16739545     0.04776668    -0.01937147    -0.01233089     0.03703703

                ci   9         ci  10
 ref:   1    -0.01141824     0.01062618
 ref:   2    -0.01846759     0.03912014

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 24  1   -196.4724080110  3.1314E-08  1.9258E-04  3.1685E-02  1.0000E-04   
 mr-sdci # 24  2   -196.4683759982  3.7641E-07  0.0000E+00  3.2875E-02  1.0000E-04   
 mr-sdci # 24  3   -196.4672387032  1.6966E-05  0.0000E+00  4.2693E-02  1.0000E-04   
 mr-sdci # 24  4   -195.7919076399  2.0978E-03  0.0000E+00  7.9609E-01  1.0000E-04   
 mr-sdci # 24  5   -195.6467865277  2.1073E-02  0.0000E+00  8.5187E-01  1.0000E-04   
 mr-sdci # 24  6   -195.5376312877  8.0248E-02  0.0000E+00  7.4231E-01  1.0000E-04   
 mr-sdci # 24  7   -195.3676451178  1.5029E-03  0.0000E+00  1.4446E+00  1.0000E-04   
 mr-sdci # 24  8   -195.3298248555  1.3497E-01  0.0000E+00  1.4622E+00  1.0000E-04   
 mr-sdci # 24  9   -195.1948453966  1.8773E-01  0.0000E+00  1.1702E+00  1.0000E-04   
 mr-sdci # 24 10   -195.0062810674  2.2777E-01  0.0000E+00  1.2845E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  25

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58409301
   ht   2     0.00000000   -50.58005451
   ht   3     0.00000000     0.00000000   -50.57890399
   ht   4     0.00000000     0.00000000     0.00000000   -49.89833891
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -49.66647914
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.47924578
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.30859219
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.12505622
   ht   9    -2.07722447    10.96384051    -5.56691537  -139.30980997   -95.39754672   -44.46236096   -49.69043933    56.94226388
   ht  10     4.19337979    -0.18104728    -0.54599510    21.07093148     7.24453040    10.44173562     7.16817747    -8.46998654
   ht  11    -0.45813606    -0.01683832    -0.03737200    -0.00700616    -0.01895110     0.01095691     0.02843349    -0.00267856

                ht   9         ht  10         ht  11
   ht   9 -9203.75064950
   ht  10   174.65780057  -437.71156679
   ht  11    -0.12372150     0.06976899    -0.00785020

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.806525       7.432330E-03  -0.538218       3.545943E-02   0.121797       7.446488E-02  -6.751027E-03   3.382284E-02
 ref    2   1.295546E-02   0.962728       3.351186E-02   0.166909      -4.753034E-02   7.656260E-03  -2.203733E-02  -3.793812E-02

              v      9       v     10       v     11
 ref    1   2.733030E-03   7.230216E-03  -9.855915E-02
 ref    2  -1.999991E-02   3.933133E-02   1.260276E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.650650       0.926900       0.290802       2.911598E-02   1.709366E-02   5.603637E-03   5.312205E-04   2.583285E-03

              v      9       v     10       v     11
 ref    1   4.074660E-04   1.599229E-03   9.872736E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.80652503     0.00743233    -0.53821837     0.03545943     0.12179705     0.07446488    -0.00675103     0.03382284
 ref:   2     0.01295546     0.96272763     0.03351186     0.16690900    -0.04753034     0.00765626    -0.02203733    -0.03793812

                ci   9         ci  10         ci  11
 ref:   1     0.00273303     0.00723022    -0.09855915
 ref:   2    -0.01999991     0.03933133     0.01260276

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 25  1   -196.4725476095  1.3960E-04  0.0000E+00  1.3495E-02  1.0000E-04   
 mr-sdci # 25  2   -196.4683763607  3.6244E-07  2.3182E-04  3.2822E-02  1.0000E-04   
 mr-sdci # 25  3   -196.4672593888  2.0686E-05  0.0000E+00  3.9642E-02  1.0000E-04   
 mr-sdci # 25  4   -195.7921327559  2.2512E-04  0.0000E+00  7.9114E-01  1.0000E-04   
 mr-sdci # 25  5   -195.6641046345  1.7318E-02  0.0000E+00  6.8299E-01  1.0000E-04   
 mr-sdci # 25  6   -195.5555558248  1.7925E-02  0.0000E+00  9.8639E-01  1.0000E-04   
 mr-sdci # 25  7   -195.5355930580  1.6795E-01  0.0000E+00  8.1935E-01  1.0000E-04   
 mr-sdci # 25  8   -195.3369289653  7.1041E-03  0.0000E+00  1.4064E+00  1.0000E-04   
 mr-sdci # 25  9   -195.1974402965  2.5949E-03  0.0000E+00  1.1846E+00  1.0000E-04   
 mr-sdci # 25 10   -195.0066531084  3.7204E-04  0.0000E+00  1.2671E+00  1.0000E-04   
 mr-sdci # 25 11   -192.2416329579 -2.3210E+00  0.0000E+00  4.3947E+00  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  26

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58409301
   ht   2     0.00000000   -50.58005451
   ht   3     0.00000000     0.00000000   -50.57890399
   ht   4     0.00000000     0.00000000     0.00000000   -49.89833891
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -49.66647914
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.47924578
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.30859219
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.12505622
   ht   9    -2.07722447    10.96384051    -5.56691537  -139.30980997   -95.39754672   -44.46236096   -49.69043933    56.94226388
   ht  10     4.19337979    -0.18104728    -0.54599510    21.07093148     7.24453040    10.44173562     7.16817747    -8.46998654
   ht  11    -0.45813606    -0.01683832    -0.03737200    -0.00700616    -0.01895110     0.01095691     0.02843349    -0.00267856
   ht  12    -0.11061385    -0.36605099     0.02851703    -0.01457151     0.02766881     0.00259636    -0.15347007     0.10785467

                ht   9         ht  10         ht  11         ht  12
   ht   9 -9203.75064950
   ht  10   174.65780057  -437.71156679
   ht  11    -0.12372150     0.06976899    -0.00785020
   ht  12    -0.10995767     0.11204646    -0.00002957    -0.01147698

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.806523      -1.209797E-02  -0.538124       2.541998E-02  -0.123060       7.601085E-02   1.087785E-02  -2.770762E-02
 ref    2  -1.290788E-02  -0.961663       4.211481E-02   0.175350       2.949246E-02   3.513664E-03  -7.035484E-03   1.501978E-02

              v      9       v     10       v     11       v     12
 ref    1  -1.675717E-02  -1.189798E-02   1.539586E-03   0.103837    
 ref    2   1.497394E-02  -1.114736E-02   5.745895E-02  -2.517767E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.650647       0.924941       0.291351       3.139377E-02   1.601346E-02   5.789995E-03   1.678258E-04   9.933060E-04

              v      9       v     10       v     11       v     12
 ref    1   5.050218E-04   2.658256E-04   3.303902E-03   1.141596E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.80652346    -0.01209797    -0.53812363     0.02541998    -0.12305954     0.07601085     0.01087785    -0.02770762
 ref:   2    -0.01290788    -0.96166268     0.04211481     0.17534993     0.02949246     0.00351366    -0.00703548     0.01501978

                ci   9         ci  10         ci  11         ci  12
 ref:   1    -0.01675717    -0.01189798     0.00153959     0.10383665
 ref:   2     0.01497394    -0.01114736     0.05745895    -0.02517767

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 26  1   -196.4725476097  2.3775E-10  0.0000E+00  1.3494E-02  1.0000E-04   
 mr-sdci # 26  2   -196.4685544566  1.7810E-04  0.0000E+00  2.0247E-02  1.0000E-04   
 mr-sdci # 26  3   -196.4672600311  6.4234E-07  2.6632E-04  3.9618E-02  1.0000E-04   
 mr-sdci # 26  4   -195.8512814535  5.9149E-02  0.0000E+00  9.6122E-01  1.0000E-04   
 mr-sdci # 26  5   -195.6694160135  5.3114E-03  0.0000E+00  7.4575E-01  1.0000E-04   
 mr-sdci # 26  6   -195.5579241523  2.3683E-03  0.0000E+00  1.0583E+00  1.0000E-04   
 mr-sdci # 26  7   -195.5439873634  8.3943E-03  0.0000E+00  7.8637E-01  1.0000E-04   
 mr-sdci # 26  8   -195.3789723055  4.2043E-02  0.0000E+00  1.4391E+00  1.0000E-04   
 mr-sdci # 26  9   -195.2690139975  7.1574E-02  0.0000E+00  1.4560E+00  1.0000E-04   
 mr-sdci # 26 10   -195.0610547852  5.4402E-02  0.0000E+00  1.0913E+00  1.0000E-04   
 mr-sdci # 26 11   -194.1420049479  1.9004E+00  0.0000E+00  3.5022E+00  1.0000E-04   
 mr-sdci # 26 12   -192.0049926112 -2.1643E+00  0.0000E+00  4.3222E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.008000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  27

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58409301
   ht   2     0.00000000   -50.58005451
   ht   3     0.00000000     0.00000000   -50.57890399
   ht   4     0.00000000     0.00000000     0.00000000   -49.89833891
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -49.66647914
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.47924578
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.30859219
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.12505622
   ht   9    -2.07722447    10.96384051    -5.56691537  -139.30980997   -95.39754672   -44.46236096   -49.69043933    56.94226388
   ht  10     4.19337979    -0.18104728    -0.54599510    21.07093148     7.24453040    10.44173562     7.16817747    -8.46998654
   ht  11    -0.45813606    -0.01683832    -0.03737200    -0.00700616    -0.01895110     0.01095691     0.02843349    -0.00267856
   ht  12    -0.11061385    -0.36605099     0.02851703    -0.01457151     0.02766881     0.00259636    -0.15347007     0.10785467
   ht  13    -0.06832445    -0.07132594     0.27374309     0.02099482     0.00933890    -0.02503908     0.01513577    -0.00905878

                ht   9         ht  10         ht  11         ht  12         ht  13
   ht   9 -9203.75064950
   ht  10   174.65780057  -437.71156679
   ht  11    -0.12372150     0.06976899    -0.00785020
   ht  12    -0.10995767     0.11204646    -0.00002957    -0.01147698
   ht  13    -0.36221719     0.29134675    -0.00008356    -0.00047859    -0.00632616

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.806995       1.300881E-02   0.536890       4.203837E-02   9.767955E-02  -9.642919E-02  -4.322257E-02  -2.069825E-02
 ref    2  -1.287507E-02   0.961587      -4.394258E-02   0.167035      -6.257504E-02  -2.363667E-03   4.423787E-03   9.758347E-03

              v      9       v     10       v     11       v     12       v     13
 ref    1   9.226850E-04  -7.448314E-03  -4.708662E-03   2.109005E-02   0.106735    
 ref    2  -5.778549E-03  -1.357044E-02  -5.420156E-02   1.805186E-02  -3.851490E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.651406       0.924820       0.290182       2.966796E-02   1.345693E-02   9.304175E-03   1.887761E-03   5.236429E-04

              v      9       v     10       v     11       v     12       v     13
 ref    1   3.424298E-05   2.396343E-04   2.959980E-03   7.706598E-04   1.287568E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.80699472     0.01300881     0.53688978     0.04203837     0.09767955    -0.09642919    -0.04322257    -0.02069825
 ref:   2    -0.01287507     0.96158747    -0.04394258     0.16703512    -0.06257504    -0.00236367     0.00442379     0.00975835

                ci   9         ci  10         ci  11         ci  12         ci  13
 ref:   1     0.00092268    -0.00744831    -0.00470866     0.02109005     0.10673465
 ref:   2    -0.00577855    -0.01357044    -0.05420156     0.01805186    -0.03851490

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 27  1   -196.4725476921  8.2317E-08  0.0000E+00  1.3465E-02  1.0000E-04   
 mr-sdci # 27  2   -196.4685544719  1.5331E-08  0.0000E+00  2.0241E-02  1.0000E-04   
 mr-sdci # 27  3   -196.4675175582  2.5753E-04  0.0000E+00  1.4268E-02  1.0000E-04   
 mr-sdci # 27  4   -195.8576710628  6.3896E-03 -1.1288E-01  9.1083E-01  1.0000E-04   
 mr-sdci # 27  5   -195.7543771124  8.4961E-02  0.0000E+00  7.4292E-01  1.0000E-04   
 mr-sdci # 27  6   -195.5872828196  2.9359E-02  0.0000E+00  9.5492E-01  1.0000E-04   
 mr-sdci # 27  7   -195.5485150401  4.5277E-03  0.0000E+00  7.7167E-01  1.0000E-04   
 mr-sdci # 27  8   -195.4597742021  8.0802E-02  0.0000E+00  9.7927E-01  1.0000E-04   
 mr-sdci # 27  9   -195.2989775048  2.9964E-02  0.0000E+00  1.5117E+00  1.0000E-04   
 mr-sdci # 27 10   -195.0666850419  5.6303E-03  0.0000E+00  1.0479E+00  1.0000E-04   
 mr-sdci # 27 11   -194.1589006154  1.6896E-02  0.0000E+00  3.5117E+00  1.0000E-04   
 mr-sdci # 27 12   -192.7950662358  7.9007E-01  0.0000E+00  3.7788E+00  1.0000E-04   
 mr-sdci # 27 13   -191.7092035314 -2.2986E+00  0.0000E+00  4.7161E+00  1.0000E-04   
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.006000
time for eigenvalue solver             0.001000
time for vector access                 0.000000

          starting ci iteration  28

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58409301
   ht   2     0.00000000   -50.58005451
   ht   3     0.00000000     0.00000000   -50.57890399
   ht   4     0.00000000     0.00000000     0.00000000   -49.89833891
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -49.66647914
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.47924578
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.30859219
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.12505622
   ht   9    -2.07722447    10.96384051    -5.56691537  -139.30980997   -95.39754672   -44.46236096   -49.69043933    56.94226388
   ht  10     4.19337979    -0.18104728    -0.54599510    21.07093148     7.24453040    10.44173562     7.16817747    -8.46998654
   ht  11    -0.45813606    -0.01683832    -0.03737200    -0.00700616    -0.01895110     0.01095691     0.02843349    -0.00267856
   ht  12    -0.11061385    -0.36605099     0.02851703    -0.01457151     0.02766881     0.00259636    -0.15347007     0.10785467
   ht  13    -0.06832445    -0.07132594     0.27374309     0.02099482     0.00933890    -0.02503908     0.01513577    -0.00905878
   ht  14     6.04931985     2.48717040    -0.08584856    28.25455723    -6.25845485     3.57192486    -8.20955071    12.99033145

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14
   ht   9 -9203.75064950
   ht  10   174.65780057  -437.71156679
   ht  11    -0.12372150     0.06976899    -0.00785020
   ht  12    -0.10995767     0.11204646    -0.00002957    -0.01147698
   ht  13    -0.36221719     0.29134675    -0.00008356    -0.00047859    -0.00632616
   ht  14     5.77917969    -0.47666474     0.01062778    -0.10514616    -0.04241649   -56.23250636

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.806984       1.257068E-02   0.536913       3.339560E-02   7.314976E-02   9.440982E-02   6.549869E-02  -5.175065E-02
 ref    2  -1.289055E-02   0.961662      -4.327360E-02   0.139513      -0.100948       4.856709E-02  -7.967624E-03   8.921376E-03

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1  -2.300027E-02  -1.767254E-03   6.509291E-03  -4.494330E-03  -2.041749E-02   0.111453    
 ref    2   1.262580E-02   6.394826E-03   1.491408E-02  -5.422703E-02  -1.821062E-02  -4.300605E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.651389       0.924952       0.290149       2.057905E-02   1.554143E-02   1.127198E-02   4.353561E-03   2.757721E-03

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1   6.884230E-04   4.401699E-05   2.648008E-04   2.960769E-03   7.485007E-04   1.427122E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.80698388     0.01257068     0.53691334     0.03339560     0.07314976     0.09440982     0.06549869    -0.05175065
 ref:   2    -0.01289055     0.96166214    -0.04327360     0.13951265    -0.10094820     0.04856709    -0.00796762     0.00892138

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14
 ref:   1    -0.02300027    -0.00176725     0.00650929    -0.00449433    -0.02041749     0.11145267
 ref:   2     0.01262580     0.00639483     0.01491408    -0.05422703    -0.01821062    -0.04300605

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 28  1   -196.4725476971  5.0066E-09  0.0000E+00  1.3470E-02  1.0000E-04   
 mr-sdci # 28  2   -196.4685547911  3.1918E-07  0.0000E+00  1.9419E-02  1.0000E-04   
 mr-sdci # 28  3   -196.4675197287  2.1705E-06  0.0000E+00  1.5213E-02  1.0000E-04   
 mr-sdci # 28  4   -195.9599761698  1.0231E-01  0.0000E+00  1.6868E+00  1.0000E-04   
 mr-sdci # 28  5   -195.7621984957  7.8214E-03 -1.0008E-01  8.3975E-01  1.0000E-04   
 mr-sdci # 28  6   -195.6771090247  8.9826E-02  0.0000E+00  1.2796E+00  1.0000E-04   
 mr-sdci # 28  7   -195.5728881708  2.4373E-02  0.0000E+00  1.0492E+00  1.0000E-04   
 mr-sdci # 28  8   -195.5454043335  8.5630E-02  0.0000E+00  8.1886E-01  1.0000E-04   
 mr-sdci # 28  9   -195.4550473468  1.5607E-01  0.0000E+00  9.8967E-01  1.0000E-04   
 mr-sdci # 28 10   -195.2963068746  2.2962E-01  0.0000E+00  1.5758E+00  1.0000E-04   
 mr-sdci # 28 11   -195.0206271824  8.6173E-01  0.0000E+00  1.0469E+00  1.0000E-04   
 mr-sdci # 28 12   -194.1583664339  1.3633E+00  0.0000E+00  3.5006E+00  1.0000E-04   
 mr-sdci # 28 13   -192.7947247196  1.0855E+00  0.0000E+00  3.7845E+00  1.0000E-04   
 mr-sdci # 28 14   -191.4428188014 -1.9711E+00  0.0000E+00  4.7444E+00  1.0000E-04   
 
 root number  5 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.008000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  29

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58409301
   ht   2     0.00000000   -50.58005451
   ht   3     0.00000000     0.00000000   -50.57890399
   ht   4     0.00000000     0.00000000     0.00000000   -49.89833891
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -49.66647914
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.47924578
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.30859219
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.12505622
   ht   9    -2.07722447    10.96384051    -5.56691537  -139.30980997   -95.39754672   -44.46236096   -49.69043933    56.94226388
   ht  10     4.19337979    -0.18104728    -0.54599510    21.07093148     7.24453040    10.44173562     7.16817747    -8.46998654
   ht  11    -0.45813606    -0.01683832    -0.03737200    -0.00700616    -0.01895110     0.01095691     0.02843349    -0.00267856
   ht  12    -0.11061385    -0.36605099     0.02851703    -0.01457151     0.02766881     0.00259636    -0.15347007     0.10785467
   ht  13    -0.06832445    -0.07132594     0.27374309     0.02099482     0.00933890    -0.02503908     0.01513577    -0.00905878
   ht  14     6.04931985     2.48717040    -0.08584856    28.25455723    -6.25845485     3.57192486    -8.20955071    12.99033145
   ht  15     3.37141082    -5.53610163     3.62965968    -2.83290278   -16.25565968     3.07537119     7.59356144    -4.64141995

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15
   ht   9 -9203.75064950
   ht  10   174.65780057  -437.71156679
   ht  11    -0.12372150     0.06976899    -0.00785020
   ht  12    -0.10995767     0.11204646    -0.00002957    -0.01147698
   ht  13    -0.36221719     0.29134675    -0.00008356    -0.00047859    -0.00632616
   ht  14     5.77917969    -0.47666474     0.01062778    -0.10514616    -0.04241649   -56.23250636
   ht  15    22.88683057   -18.35699922    -0.07960066     0.19451437    -0.01008303     5.73227643   -36.32692064

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.807438       1.414504E-02   0.536163      -4.894006E-02  -7.242984E-03  -4.527465E-03  -0.125394      -6.224924E-02
 ref    2   1.335338E-02   0.961501      -4.682386E-02  -6.694426E-02   0.135815      -9.426853E-02   9.141865E-03   8.357767E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1   5.995321E-03   2.964067E-02   1.323719E-03  -6.878286E-03  -6.084454E-03   1.857141E-02  -0.116974    
 ref    2   6.822499E-03  -1.985227E-02  -6.059409E-03  -1.457174E-02  -5.398967E-02   2.143387E-02   4.066043E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.652135       0.924685       0.289663       6.876663E-03   1.849813E-02   8.907054E-03   1.580713E-02   3.944820E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1   8.249036E-05   1.272682E-03   3.846867E-05   2.596465E-04   2.951905E-03   8.043080E-04   1.533610E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.80743835     0.01414504     0.53616288    -0.04894006    -0.00724298    -0.00452746    -0.12539361    -0.06224924
 ref:   2     0.01335338     0.96150147    -0.04682386    -0.06694426     0.13581482    -0.09426853     0.00914186     0.00835777

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15
 ref:   1     0.00599532     0.02964067     0.00132372    -0.00687829    -0.00608445     0.01857141    -0.11697365
 ref:   2     0.00682250    -0.01985227    -0.00605941    -0.01457174    -0.05398967     0.02143387     0.04066043

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 29  1   -196.4725499495  2.2524E-06  0.0000E+00  1.2974E-02  1.0000E-04   
 mr-sdci # 29  2   -196.4685566039  1.8128E-06  0.0000E+00  1.8211E-02  1.0000E-04   
 mr-sdci # 29  3   -196.4675278105  8.0817E-06  0.0000E+00  1.8945E-02  1.0000E-04   
 mr-sdci # 29  4   -195.9869128205  2.6937E-02  0.0000E+00  1.6643E+00  1.0000E-04   
 mr-sdci # 29  5   -195.9361753189  1.7398E-01  0.0000E+00  1.7651E+00  1.0000E-04   
 mr-sdci # 29  6   -195.6996392306  2.2530E-02  6.0138E-02  1.2632E+00  1.0000E-04   
 mr-sdci # 29  7   -195.6483808155  7.5493E-02  0.0000E+00  9.2282E-01  1.0000E-04   
 mr-sdci # 29  8   -195.5471264089  1.7221E-03  0.0000E+00  9.4552E-01  1.0000E-04   
 mr-sdci # 29  9   -195.5193220948  6.4275E-02  0.0000E+00  1.1447E+00  1.0000E-04   
 mr-sdci # 29 10   -195.4334240077  1.3712E-01  0.0000E+00  9.9275E-01  1.0000E-04   
 mr-sdci # 29 11   -195.2962557890  2.7563E-01  0.0000E+00  1.5731E+00  1.0000E-04   
 mr-sdci # 29 12   -195.0204709236  8.6210E-01  0.0000E+00  1.0481E+00  1.0000E-04   
 mr-sdci # 29 13   -194.0997799743  1.3051E+00  0.0000E+00  3.5257E+00  1.0000E-04   
 mr-sdci # 29 14   -192.7655881722  1.3228E+00  0.0000E+00  3.8269E+00  1.0000E-04   
 mr-sdci # 29 15   -191.3259300181 -1.5783E+00  0.0000E+00  4.7782E+00  1.0000E-04   
 
 root number  6 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.009000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  30

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58409301
   ht   2     0.00000000   -50.58005451
   ht   3     0.00000000     0.00000000   -50.57890399
   ht   4     0.00000000     0.00000000     0.00000000   -49.89833891
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -49.66647914
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.47924578
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.30859219
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.12505622
   ht   9    -2.07722447    10.96384051    -5.56691537  -139.30980997   -95.39754672   -44.46236096   -49.69043933    56.94226388
   ht  10     4.19337979    -0.18104728    -0.54599510    21.07093148     7.24453040    10.44173562     7.16817747    -8.46998654
   ht  11    -0.45813606    -0.01683832    -0.03737200    -0.00700616    -0.01895110     0.01095691     0.02843349    -0.00267856
   ht  12    -0.11061385    -0.36605099     0.02851703    -0.01457151     0.02766881     0.00259636    -0.15347007     0.10785467
   ht  13    -0.06832445    -0.07132594     0.27374309     0.02099482     0.00933890    -0.02503908     0.01513577    -0.00905878
   ht  14     6.04931985     2.48717040    -0.08584856    28.25455723    -6.25845485     3.57192486    -8.20955071    12.99033145
   ht  15     3.37141082    -5.53610163     3.62965968    -2.83290278   -16.25565968     3.07537119     7.59356144    -4.64141995
   ht  16     3.71797804    -2.60246899     1.09418646     3.73177054    -0.93561260    -3.00044722    18.70115139    -3.19143014

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15         ht  16
   ht   9 -9203.75064950
   ht  10   174.65780057  -437.71156679
   ht  11    -0.12372150     0.06976899    -0.00785020
   ht  12    -0.10995767     0.11204646    -0.00002957    -0.01147698
   ht  13    -0.36221719     0.29134675    -0.00008356    -0.00047859    -0.00632616
   ht  14     5.77917969    -0.47666474     0.01062778    -0.10514616    -0.04241649   -56.23250636
   ht  15    22.88683057   -18.35699922    -0.07960066     0.19451437    -0.01008303     5.73227643   -36.32692064
   ht  16    -1.47118779    -3.68620289    -0.09759642     0.34614034    -0.04567923    -6.43951910   -11.63379421   -35.26345505

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.807355      -1.431212E-02  -0.536281      -3.064161E-02   3.179881E-02   2.280901E-02  -0.125710      -6.228887E-02
 ref    2   1.337648E-02  -0.961469       4.706256E-02  -0.113199      -8.737839E-02   0.111003       1.522380E-02   8.149087E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   9.068757E-04  -1.161835E-02  -2.765751E-02  -9.066152E-03  -1.298332E-02  -1.079808E-02   1.111991E-02   0.116425    
 ref    2   7.431609E-03  -2.403538E-03   2.099783E-02  -1.415747E-02  -3.460000E-03   6.681282E-02   3.967298E-02  -3.195192E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.652002       0.924628       0.289813       1.375303E-02   8.646147E-03   1.284197E-02   1.603484E-02   3.946311E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   5.605124E-05   1.407630E-04   1.205847E-03   2.826292E-04   1.805382E-04   4.580552E-03   1.697598E-03   1.457573E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15        ev   16

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.80735547    -0.01431212    -0.53628142    -0.03064161     0.03179881     0.02280901    -0.12571030    -0.06228887
 ref:   2     0.01337648    -0.96146907     0.04706256    -0.11319946    -0.08737839     0.11100325     0.01522380     0.00814909

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1     0.00090688    -0.01161835    -0.02765751    -0.00906615    -0.01298332    -0.01079808     0.01111991     0.11642508
 ref:   2     0.00743161    -0.00240354     0.02099783    -0.01415747    -0.00346000     0.06681282     0.03967298    -0.03195192

 trial vector basis is being transformed.  new dimension:   8

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 30  1   -196.4725511655  1.2160E-06  3.5735E-05  1.2546E-02  1.0000E-04   
 mr-sdci # 30  2   -196.4685566875  8.3607E-08  0.0000E+00  1.8374E-02  1.0000E-04   
 mr-sdci # 30  3   -196.4675282695  4.5906E-07  0.0000E+00  1.9380E-02  1.0000E-04   
 mr-sdci # 30  4   -196.0519702294  6.5057E-02  0.0000E+00  1.6587E+00  1.0000E-04   
 mr-sdci # 30  5   -195.9519993902  1.5824E-02  0.0000E+00  1.7584E+00  1.0000E-04   
 mr-sdci # 30  6   -195.7223923724  2.2753E-02  0.0000E+00  7.3995E-01  1.0000E-04   
 mr-sdci # 30  7   -195.6498319569  1.4511E-03  0.0000E+00  8.5184E-01  1.0000E-04   
 mr-sdci # 30  8   -195.5471446835  1.8275E-05  0.0000E+00  9.4262E-01  1.0000E-04   
 mr-sdci # 30  9   -195.5308617877  1.1540E-02  0.0000E+00  1.4016E+00  1.0000E-04   
 mr-sdci # 30 10   -195.4983343330  6.4910E-02  0.0000E+00  1.7234E+00  1.0000E-04   
 mr-sdci # 30 11   -195.4298008503  1.3355E-01  0.0000E+00  1.0445E+00  1.0000E-04   
 mr-sdci # 30 12   -195.0284658330  7.9949E-03  0.0000E+00  1.0986E+00  1.0000E-04   
 mr-sdci # 30 13   -194.8138961765  7.1412E-01  0.0000E+00  2.5765E+00  1.0000E-04   
 mr-sdci # 30 14   -193.6364245641  8.7084E-01  0.0000E+00  3.6293E+00  1.0000E-04   
 mr-sdci # 30 15   -192.6842585065  1.3583E+00  0.0000E+00  3.8825E+00  1.0000E-04   
 mr-sdci # 30 16   -191.2816345527 -7.8738E-01  0.0000E+00  4.8021E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.014000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  31

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58423637
   ht   2     0.00000000   -50.58024189
   ht   3     0.00000000     0.00000000   -50.57921347
   ht   4     0.00000000     0.00000000     0.00000000   -50.16365543
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.06368459
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.83407758
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.76151716
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.65882989
   ht   9     0.17171729     0.00344779     0.01597696     0.00633442    -0.00054048     0.00316641    -0.02479200    -0.03850532

                ht   9
   ht   9    -0.00117329

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.806717      -1.528094E-02   0.536800      -2.499908E-02  -2.895633E-02   3.510314E-02   0.145129       2.052414E-02
 ref    2   1.261203E-02  -0.961454      -4.746085E-02  -0.112296       8.851555E-02   0.109802      -2.312053E-02  -3.282781E-03

              v      9
 ref    1  -5.418678E-02
 ref    2  -7.935021E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.650952       0.924628       0.290406       1.323536E-02   8.673472E-03   1.328877E-02   2.159705E-02   4.320170E-04

              v      9
 ref    1   2.999171E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.80671722    -0.01528094     0.53679971    -0.02499908    -0.02895633     0.03510314     0.14512921     0.02052414
 ref:   2     0.01261203    -0.96145439    -0.04746085    -0.11229607     0.08851555     0.10980227    -0.02312053    -0.00328278

                ci   9
 ref:   1    -0.05418678
 ref:   2    -0.00793502

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 31  1   -196.4725759141  2.4749E-05  0.0000E+00  5.6565E-03  1.0000E-04   
 mr-sdci # 31  2   -196.4685571792  4.9171E-07  1.1028E-04  1.8323E-02  1.0000E-04   
 mr-sdci # 31  3   -196.4675286430  3.7350E-07  0.0000E+00  1.9447E-02  1.0000E-04   
 mr-sdci # 31  4   -196.0547297289  2.7595E-03  0.0000E+00  1.6544E+00  1.0000E-04   
 mr-sdci # 31  5   -195.9523540922  3.5470E-04  0.0000E+00  1.7623E+00  1.0000E-04   
 mr-sdci # 31  6   -195.7231232223  7.3085E-04  0.0000E+00  7.2208E-01  1.0000E-04   
 mr-sdci # 31  7   -195.6736924613  2.3861E-02  0.0000E+00  6.9312E-01  1.0000E-04   
 mr-sdci # 31  8   -195.5908508133  4.3706E-02  0.0000E+00  8.4084E-01  1.0000E-04   
 mr-sdci # 31  9   -191.7094476216 -3.8214E+00  0.0000E+00  3.9904E+00  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  32

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58423637
   ht   2     0.00000000   -50.58024189
   ht   3     0.00000000     0.00000000   -50.57921347
   ht   4     0.00000000     0.00000000     0.00000000   -50.16365543
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.06368459
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.83407758
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.76151716
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.65882989
   ht   9     0.17171729     0.00344779     0.01597696     0.00633442    -0.00054048     0.00316641    -0.02479200    -0.03850532
   ht  10     0.01990213     0.17618219     0.01548526     0.18652045     0.23251384     0.10861327    -0.02432703     0.04674517

                ht   9         ht  10
   ht   9    -0.00117329
   ht  10    -0.00012168    -0.00800376

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.806699       1.943925E-02  -0.536688      -4.819721E-03   3.699494E-02  -2.662378E-02   0.147289       1.975740E-02
 ref    2   1.226121E-02   0.958250       5.463282E-02  -0.158934       9.888019E-03  -0.122828      -2.206917E-02  -4.435005E-03

              v      9       v     10
 ref    1   3.948771E-03  -5.443392E-02
 ref    2   1.401286E-02  -9.460212E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.650913       0.918621       0.291018       2.528311E-02   1.466399E-03   1.579553E-02   2.218109E-02   4.100243E-04

              v      9       v     10
 ref    1   2.119530E-04   3.052547E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.80669851     0.01943925    -0.53668769    -0.00481972     0.03699494    -0.02662378     0.14728898     0.01975740
 ref:   2     0.01226121     0.95825002     0.05463282    -0.15893357     0.00988802    -0.12282795    -0.02206917    -0.00443500

                ci   9         ci  10
 ref:   1     0.00394877    -0.05443392
 ref:   2     0.01401286    -0.00946021

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 32  1   -196.4725759386  2.4506E-08  0.0000E+00  5.6536E-03  1.0000E-04   
 mr-sdci # 32  2   -196.4686653165  1.0814E-04  0.0000E+00  2.6626E-02  1.0000E-04   
 mr-sdci # 32  3   -196.4675292120  5.6900E-07  1.0404E-04  1.9492E-02  1.0000E-04   
 mr-sdci # 32  4   -196.2824197082  2.2769E-01  0.0000E+00  1.1160E+00  1.0000E-04   
 mr-sdci # 32  5   -196.0071956193  5.4842E-02  0.0000E+00  1.7072E+00  1.0000E-04   
 mr-sdci # 32  6   -195.7288793412  5.7561E-03  0.0000E+00  6.7011E-01  1.0000E-04   
 mr-sdci # 32  7   -195.6746092042  9.1674E-04  0.0000E+00  6.6138E-01  1.0000E-04   
 mr-sdci # 32  8   -195.5920681273  1.2173E-03  0.0000E+00  8.4143E-01  1.0000E-04   
 mr-sdci # 32  9   -193.8992646482  2.1898E+00  0.0000E+00  3.7704E+00  1.0000E-04   
 mr-sdci # 32 10   -191.7046487154 -3.7937E+00  0.0000E+00  3.9894E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.008000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  33

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58423637
   ht   2     0.00000000   -50.58024189
   ht   3     0.00000000     0.00000000   -50.57921347
   ht   4     0.00000000     0.00000000     0.00000000   -50.16365543
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.06368459
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.83407758
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.76151716
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.65882989
   ht   9     0.17171729     0.00344779     0.01597696     0.00633442    -0.00054048     0.00316641    -0.02479200    -0.03850532
   ht  10     0.01990213     0.17618219     0.01548526     0.18652045     0.23251384     0.10861327    -0.02432703     0.04674517
   ht  11    -0.00271567    -0.10322869     0.21207777     0.23757971    -0.23250308     0.02167900    -0.04898957     0.00057338

                ht   9         ht  10         ht  11
   ht   9    -0.00117329
   ht  10    -0.00012168    -0.00800376
   ht  11    -0.00007736     0.00047009    -0.00604094

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.806779       2.467393E-02   0.535765       2.104276E-02  -3.560255E-02  -3.225118E-02   0.147036       2.286394E-02
 ref    2   1.226743E-02   0.957604      -6.466586E-02   0.147876       5.597289E-02  -0.123167      -2.431096E-02  -5.008621E-03

              v      9       v     10       v     11
 ref    1   5.972778E-03   2.929061E-02   4.734568E-02
 ref    2   1.138549E-02  -2.619409E-02   1.813490E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.651042       0.917615       0.291225       2.231004E-02   4.400506E-03   1.621026E-02   2.221058E-02   5.478458E-04

              v      9       v     10       v     11
 ref    1   1.653035E-04   1.544071E-03   2.570488E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.80677850     0.02467393     0.53576451     0.02104276    -0.03560255    -0.03225118     0.14703590     0.02286394
 ref:   2     0.01226743     0.95760416    -0.06466586     0.14787578     0.05597289    -0.12316704    -0.02431096    -0.00500862

                ci   9         ci  10         ci  11
 ref:   1     0.00597278     0.02929061     0.04734568
 ref:   2     0.01138549    -0.02619409     0.01813490

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 33  1   -196.4725759443  5.7585E-09  0.0000E+00  5.6579E-03  1.0000E-04   
 mr-sdci # 33  2   -196.4686664043  1.0878E-06  0.0000E+00  2.6839E-02  1.0000E-04   
 mr-sdci # 33  3   -196.4676348361  1.0562E-04  0.0000E+00  2.5252E-02  1.0000E-04   
 mr-sdci # 33  4   -196.2950626279  1.2643E-02  2.3904E-01  1.0929E+00  1.0000E-04   
 mr-sdci # 33  5   -196.2175825838  2.1039E-01  0.0000E+00  1.2852E+00  1.0000E-04   
 mr-sdci # 33  6   -195.7299224241  1.0431E-03  0.0000E+00  6.5901E-01  1.0000E-04   
 mr-sdci # 33  7   -195.6776255064  3.0163E-03  0.0000E+00  5.6674E-01  1.0000E-04   
 mr-sdci # 33  8   -195.5933131657  1.2450E-03  0.0000E+00  8.0479E-01  1.0000E-04   
 mr-sdci # 33  9   -193.9070508888  7.7862E-03  0.0000E+00  3.7447E+00  1.0000E-04   
 mr-sdci # 33 10   -192.9697583120  1.2651E+00  0.0000E+00  3.6446E+00  1.0000E-04   
 mr-sdci # 33 11   -191.5637583921 -3.8660E+00  0.0000E+00  3.8521E+00  1.0000E-04   
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.008000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  34

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58423637
   ht   2     0.00000000   -50.58024189
   ht   3     0.00000000     0.00000000   -50.57921347
   ht   4     0.00000000     0.00000000     0.00000000   -50.16365543
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.06368459
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.83407758
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.76151716
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.65882989
   ht   9     0.17171729     0.00344779     0.01597696     0.00633442    -0.00054048     0.00316641    -0.02479200    -0.03850532
   ht  10     0.01990213     0.17618219     0.01548526     0.18652045     0.23251384     0.10861327    -0.02432703     0.04674517
   ht  11    -0.00271567    -0.10322869     0.21207777     0.23757971    -0.23250308     0.02167900    -0.04898957     0.00057338
   ht  12     1.66571003   -23.63564431   -11.18026817    -4.18976858     7.03047906     4.47325258    -0.34210752     1.45408987

                ht   9         ht  10         ht  11         ht  12
   ht   9    -0.00117329
   ht  10    -0.00012168    -0.00800376
   ht  11    -0.00007736     0.00047009    -0.00604094
   ht  12    -0.00082986     0.03612066     0.06485894   -20.57642401

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.806640       6.929597E-02  -0.531459      -2.921801E-02   3.722643E-02  -3.471112E-02  -0.146321      -2.557755E-02
 ref    2   1.176807E-02   0.940231       0.148849      -0.176715      -4.383103E-02  -0.137115       2.526057E-02   7.545101E-03

              v      9       v     10       v     11       v     12
 ref    1   7.912276E-04   2.820209E-02  -3.881774E-02   3.795387E-02
 ref    2   2.558249E-02  -3.089769E-02  -0.120410      -1.663751E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.650807       0.888836       0.304605       3.208189E-02   3.306967E-03   2.000530E-02   2.204804E-02   7.111395E-04

              v      9       v     10       v     11       v     12
 ref    1   6.550901E-04   1.750025E-03   1.600548E-02   1.717303E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.80664037     0.06929597    -0.53145946    -0.02921801     0.03722643    -0.03471112    -0.14632137    -0.02557755
 ref:   2     0.01176807     0.94023073     0.14884873    -0.17671503    -0.04383103    -0.13711470     0.02526057     0.00754510

                ci   9         ci  10         ci  11         ci  12
 ref:   1     0.00079123     0.02820209    -0.03881774     0.03795387
 ref:   2     0.02558249    -0.03089769    -0.12041039    -0.01663751

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 34  1   -196.4725759730  2.8678E-08  0.0000E+00  5.6754E-03  1.0000E-04   
 mr-sdci # 34  2   -196.4688779983  2.1159E-04  0.0000E+00  5.1170E-02  1.0000E-04   
 mr-sdci # 34  3   -196.4676767212  4.1885E-05  0.0000E+00  2.8606E-02  1.0000E-04   
 mr-sdci # 34  4   -196.4235757066  1.2851E-01  0.0000E+00  5.6543E-01  1.0000E-04   
 mr-sdci # 34  5   -196.2182670009  6.8442E-04  4.4940E-01  1.2880E+00  1.0000E-04   
 mr-sdci # 34  6   -195.7407320438  1.0810E-02  0.0000E+00  5.0209E-01  1.0000E-04   
 mr-sdci # 34  7   -195.6777168172  9.1311E-05  0.0000E+00  5.7184E-01  1.0000E-04   
 mr-sdci # 34  8   -195.6034793103  1.0166E-02  0.0000E+00  7.3556E-01  1.0000E-04   
 mr-sdci # 34  9   -194.0779589104  1.7091E-01  0.0000E+00  3.5876E+00  1.0000E-04   
 mr-sdci # 34 10   -192.9715537669  1.7955E-03  0.0000E+00  3.6577E+00  1.0000E-04   
 mr-sdci # 34 11   -191.7433871274  1.7963E-01  0.0000E+00  4.3794E+00  1.0000E-04   
 mr-sdci # 34 12   -191.5480510099 -3.4804E+00  0.0000E+00  3.8680E+00  1.0000E-04   
 
 root number  5 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.008000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  35

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58423637
   ht   2     0.00000000   -50.58024189
   ht   3     0.00000000     0.00000000   -50.57921347
   ht   4     0.00000000     0.00000000     0.00000000   -50.16365543
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.06368459
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.83407758
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.76151716
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.65882989
   ht   9     0.17171729     0.00344779     0.01597696     0.00633442    -0.00054048     0.00316641    -0.02479200    -0.03850532
   ht  10     0.01990213     0.17618219     0.01548526     0.18652045     0.23251384     0.10861327    -0.02432703     0.04674517
   ht  11    -0.00271567    -0.10322869     0.21207777     0.23757971    -0.23250308     0.02167900    -0.04898957     0.00057338
   ht  12     1.66571003   -23.63564431   -11.18026817    -4.18976858     7.03047906     4.47325258    -0.34210752     1.45408987
   ht  13   -17.26755891    62.29706968   -42.88462828    14.90023411    -5.91741001   -11.25607386    -5.47119764     1.50515828

                ht   9         ht  10         ht  11         ht  12         ht  13
   ht   9    -0.00117329
   ht  10    -0.00012168    -0.00800376
   ht  11    -0.00007736     0.00047009    -0.00604094
   ht  12    -0.00082986     0.03612066     0.06485894   -20.57642401
   ht  13     0.07867332    -0.08447075     0.18862381    26.08887153  -147.54998543

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.806375       0.109722      -0.521964       6.199406E-02  -3.284955E-02   2.032805E-02   0.149135      -3.152417E-02
 ref    2   1.169182E-02   0.923602       0.227305       0.166477       7.841698E-02   0.143182      -1.643756E-02   1.098895E-02

              v      9       v     10       v     11       v     12       v     13
 ref    1   8.279157E-03   7.597721E-04   5.684077E-02  -3.545542E-02  -2.424690E-02
 ref    2  -3.522691E-02  -1.508114E-02   7.732494E-02   3.761147E-02   0.129192    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.650378       0.865079       0.324114       3.155795E-02   7.228315E-03   2.091440E-02   2.251146E-02   1.114531E-03

              v      9       v     10       v     11       v     12       v     13
 ref    1   1.309480E-03   2.280181E-04   9.210020E-03   2.671709E-03   1.727848E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.80637511     0.10972223    -0.52196380     0.06199406    -0.03284955     0.02032805     0.14913507    -0.03152417
 ref:   2     0.01169182     0.92360165     0.22730536     0.16647727     0.07841698     0.14318229    -0.01643756     0.01098895

                ci   9         ci  10         ci  11         ci  12         ci  13
 ref:   1     0.00827916     0.00075977     0.05684077    -0.03545542    -0.02424690
 ref:   2    -0.03522691    -0.01508114     0.07732494     0.03761147     0.12919197

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 35  1   -196.4725759982  2.5215E-08  0.0000E+00  5.6210E-03  1.0000E-04   
 mr-sdci # 35  2   -196.4689083981  3.0400E-05  0.0000E+00  5.1143E-02  1.0000E-04   
 mr-sdci # 35  3   -196.4679164166  2.3970E-04  0.0000E+00  4.4738E-02  1.0000E-04   
 mr-sdci # 35  4   -196.4336540505  1.0078E-02  0.0000E+00  4.5012E-01  1.0000E-04   
 mr-sdci # 35  5   -196.3760274532  1.5776E-01  0.0000E+00  8.4620E-01  1.0000E-04   
 mr-sdci # 35  6   -195.7475274028  6.7954E-03  4.1159E-04  4.8110E-01  1.0000E-04   
 mr-sdci # 35  7   -195.6820785882  4.3618E-03  0.0000E+00  4.6370E-01  1.0000E-04   
 mr-sdci # 35  8   -195.6086358584  5.1565E-03  0.0000E+00  6.4749E-01  1.0000E-04   
 mr-sdci # 35  9   -194.4085888886  3.3063E-01  0.0000E+00  3.3710E+00  1.0000E-04   
 mr-sdci # 35 10   -193.6950587546  7.2350E-01  0.0000E+00  3.4203E+00  1.0000E-04   
 mr-sdci # 35 11   -191.8871026624  1.4372E-01  0.0000E+00  4.1721E+00  1.0000E-04   
 mr-sdci # 35 12   -191.5541372191  6.0862E-03  0.0000E+00  3.9226E+00  1.0000E-04   
 mr-sdci # 35 13   -190.5622071951 -4.2517E+00  0.0000E+00  4.4613E+00  1.0000E-04   
 
 root number  6 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.009000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  36

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58423637
   ht   2     0.00000000   -50.58024189
   ht   3     0.00000000     0.00000000   -50.57921347
   ht   4     0.00000000     0.00000000     0.00000000   -50.16365543
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.06368459
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.83407758
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.76151716
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.65882989
   ht   9     0.17171729     0.00344779     0.01597696     0.00633442    -0.00054048     0.00316641    -0.02479200    -0.03850532
   ht  10     0.01990213     0.17618219     0.01548526     0.18652045     0.23251384     0.10861327    -0.02432703     0.04674517
   ht  11    -0.00271567    -0.10322869     0.21207777     0.23757971    -0.23250308     0.02167900    -0.04898957     0.00057338
   ht  12     1.66571003   -23.63564431   -11.18026817    -4.18976858     7.03047906     4.47325258    -0.34210752     1.45408987
   ht  13   -17.26755891    62.29706968   -42.88462828    14.90023411    -5.91741001   -11.25607386    -5.47119764     1.50515828
   ht  14    -0.51842994     2.40213118     0.11854174    -7.44216665    -9.33236364     0.73882237    -1.33866342     0.22929265

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14
   ht   9    -0.00117329
   ht  10    -0.00012168    -0.00800376
   ht  11    -0.00007736     0.00047009    -0.00604094
   ht  12    -0.00082986     0.03612066     0.06485894   -20.57642401
   ht  13     0.07867332    -0.08447075     0.18862381    26.08887153  -147.54998543
   ht  14     0.00233699    -0.04716321    -0.00171031     0.96204525     3.34700910   -10.33679413

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.806489       0.112399       0.521031      -6.308722E-02  -3.364449E-02   2.017066E-02   0.149031       2.934324E-02
 ref    2   1.229847E-02   0.920421      -0.234106      -0.175328       7.657855E-02   0.143174      -1.588714E-02  -9.986777E-03

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1   1.357856E-02   7.543618E-03   1.025512E-03   5.693264E-02   3.529145E-02  -2.482250E-02
 ref    2  -1.120713E-02  -3.207050E-02  -1.639323E-02   7.771787E-02  -3.827240E-02   0.129277    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.650576       0.859808       0.326279       3.472002E-02   6.996226E-03   2.090577E-02   2.246273E-02   9.607616E-04

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1   3.099770E-04   1.085423E-03   2.697896E-04   9.281392E-03   2.710263E-03   1.732870E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.80648936     0.11239892     0.52103118    -0.06308722    -0.03364449     0.02017066     0.14903129     0.02934324
 ref:   2     0.01229847     0.92042071    -0.23410583    -0.17532834     0.07657855     0.14317441    -0.01588714    -0.00998678

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14
 ref:   1     0.01357856     0.00754362     0.00102551     0.05693264     0.03529145    -0.02482250
 ref:   2    -0.01120713    -0.03207050    -0.01639323     0.07771787    -0.03827240     0.12927700

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 36  1   -196.4725761753  1.7706E-07  9.5049E-06  5.5783E-03  1.0000E-04   
 mr-sdci # 36  2   -196.4689537138  4.5316E-05  0.0000E+00  4.8858E-02  1.0000E-04   
 mr-sdci # 36  3   -196.4679175609  1.1443E-06  0.0000E+00  4.4255E-02  1.0000E-04   
 mr-sdci # 36  4   -196.4351517576  1.4977E-03  0.0000E+00  4.1490E-01  1.0000E-04   
 mr-sdci # 36  5   -196.3775796845  1.5522E-03  0.0000E+00  8.3248E-01  1.0000E-04   
 mr-sdci # 36  6   -195.7475345034  7.1007E-06  0.0000E+00  4.7231E-01  1.0000E-04   
 mr-sdci # 36  7   -195.6828736202  7.9503E-04  0.0000E+00  4.8629E-01  1.0000E-04   
 mr-sdci # 36  8   -195.6093386968  7.0284E-04  0.0000E+00  6.1039E-01  1.0000E-04   
 mr-sdci # 36  9   -195.5230095288  1.1144E+00  0.0000E+00  1.9706E+00  1.0000E-04   
 mr-sdci # 36 10   -194.2380900983  5.4303E-01  0.0000E+00  3.2827E+00  1.0000E-04   
 mr-sdci # 36 11   -193.6890174805  1.8019E+00  0.0000E+00  3.4342E+00  1.0000E-04   
 mr-sdci # 36 12   -191.8843432891  3.3021E-01  0.0000E+00  4.1577E+00  1.0000E-04   
 mr-sdci # 36 13   -191.5536019238  9.9139E-01  0.0000E+00  3.9215E+00  1.0000E-04   
 mr-sdci # 36 14   -190.5327563403 -3.1037E+00  0.0000E+00  4.4959E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.014000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  37

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58423637
   ht   2     0.00000000   -50.58024189
   ht   3     0.00000000     0.00000000   -50.57921347
   ht   4     0.00000000     0.00000000     0.00000000   -50.16365543
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.06368459
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.83407758
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.76151716
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.65882989
   ht   9     0.17171729     0.00344779     0.01597696     0.00633442    -0.00054048     0.00316641    -0.02479200    -0.03850532
   ht  10     0.01990213     0.17618219     0.01548526     0.18652045     0.23251384     0.10861327    -0.02432703     0.04674517
   ht  11    -0.00271567    -0.10322869     0.21207777     0.23757971    -0.23250308     0.02167900    -0.04898957     0.00057338
   ht  12     1.66571003   -23.63564431   -11.18026817    -4.18976858     7.03047906     4.47325258    -0.34210752     1.45408987
   ht  13   -17.26755891    62.29706968   -42.88462828    14.90023411    -5.91741001   -11.25607386    -5.47119764     1.50515828
   ht  14    -0.51842994     2.40213118     0.11854174    -7.44216665    -9.33236364     0.73882237    -1.33866342     0.22929265
   ht  15    -0.11576249    -0.01022509     0.02860317    -0.02110621    -0.01956792    -0.00255049     0.00696411     0.01739021

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15
   ht   9    -0.00117329
   ht  10    -0.00012168    -0.00800376
   ht  11    -0.00007736     0.00047009    -0.00604094
   ht  12    -0.00082986     0.03612066     0.06485894   -20.57642401
   ht  13     0.07867332    -0.08447075     0.18862381    26.08887153  -147.54998543
   ht  14     0.00233699    -0.04716321    -0.00171031     0.96204525     3.34700910   -10.33679413
   ht  15     0.00033892     0.00012276    -0.00004260     0.00729617     0.00795103    -0.01238892    -0.00052150

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.806039      -0.115178       0.521299      -6.325084E-02   3.363340E-02   5.533734E-02  -0.117853      -2.436203E-02
 ref    2   1.113960E-02  -0.919562      -0.236983      -0.175694      -7.704362E-02   0.131312       6.079889E-02  -9.375111E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1   4.679236E-02   7.780896E-02   2.368298E-02  -1.870389E-02  -4.578025E-02   7.651519E-04   0.132736    
 ref    2  -1.163384E-02  -1.426892E-02   3.146414E-02   8.410908E-03  -8.740001E-02   0.120269      -5.015331E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.649822       0.858860       0.327914       3.486907E-02   7.066926E-03   2.030510E-02   1.758590E-02   6.814010E-04

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1   2.324872E-03   6.257837E-03   1.550876E-03   4.205789E-04   9.734593E-03   1.446511E-02   2.013415E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.80603852    -0.11517779     0.52129895    -0.06325084     0.03363340     0.05533734    -0.11785328    -0.02436203
 ref:   2     0.01113960    -0.91956173    -0.23698336    -0.17569406    -0.07704362     0.13131213     0.06079889    -0.00937511

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15
 ref:   1     0.04679236     0.07780896     0.02368298    -0.01870389    -0.04578025     0.00076515     0.13273579
 ref:   2    -0.01163384    -0.01426892     0.03146414     0.00841091    -0.08740001     0.12026856    -0.05015331

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 37  1   -196.4725844679  8.2926E-06  0.0000E+00  2.5522E-03  1.0000E-04   
 mr-sdci # 37  2   -196.4689583279  4.6142E-06  4.7777E-04  4.8867E-02  1.0000E-04   
 mr-sdci # 37  3   -196.4679196726  2.1117E-06  0.0000E+00  4.4532E-02  1.0000E-04   
 mr-sdci # 37  4   -196.4355491067  3.9735E-04  0.0000E+00  4.1246E-01  1.0000E-04   
 mr-sdci # 37  5   -196.3776331370  5.3452E-05  0.0000E+00  8.3336E-01  1.0000E-04   
 mr-sdci # 37  6   -195.7569067721  9.3723E-03  0.0000E+00  6.2890E-01  1.0000E-04   
 mr-sdci # 37  7   -195.7143988817  3.1525E-02  0.0000E+00  4.8676E-01  1.0000E-04   
 mr-sdci # 37  8   -195.6229738132  1.3635E-02  0.0000E+00  6.3780E-01  1.0000E-04   
 mr-sdci # 37  9   -195.5396300484  1.6621E-02  0.0000E+00  1.8948E+00  1.0000E-04   
 mr-sdci # 37 10   -194.7758349617  5.3774E-01  0.0000E+00  2.0535E+00  1.0000E-04   
 mr-sdci # 37 11   -194.0275244980  3.3851E-01  0.0000E+00  3.4186E+00  1.0000E-04   
 mr-sdci # 37 12   -193.5663318377  1.6820E+00  0.0000E+00  3.5994E+00  1.0000E-04   
 mr-sdci # 37 13   -191.8372804145  2.8368E-01  0.0000E+00  4.2011E+00  1.0000E-04   
 mr-sdci # 37 14   -190.5737706065  4.1014E-02  0.0000E+00  4.4134E+00  1.0000E-04   
 mr-sdci # 37 15   -189.3849354069 -3.2993E+00  0.0000E+00  3.7454E+00  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  38

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58423637
   ht   2     0.00000000   -50.58024189
   ht   3     0.00000000     0.00000000   -50.57921347
   ht   4     0.00000000     0.00000000     0.00000000   -50.16365543
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.06368459
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.83407758
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.76151716
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.65882989
   ht   9     0.17171729     0.00344779     0.01597696     0.00633442    -0.00054048     0.00316641    -0.02479200    -0.03850532
   ht  10     0.01990213     0.17618219     0.01548526     0.18652045     0.23251384     0.10861327    -0.02432703     0.04674517
   ht  11    -0.00271567    -0.10322869     0.21207777     0.23757971    -0.23250308     0.02167900    -0.04898957     0.00057338
   ht  12     1.66571003   -23.63564431   -11.18026817    -4.18976858     7.03047906     4.47325258    -0.34210752     1.45408987
   ht  13   -17.26755891    62.29706968   -42.88462828    14.90023411    -5.91741001   -11.25607386    -5.47119764     1.50515828
   ht  14    -0.51842994     2.40213118     0.11854174    -7.44216665    -9.33236364     0.73882237    -1.33866342     0.22929265
   ht  15    -0.11576249    -0.01022509     0.02860317    -0.02110621    -0.01956792    -0.00255049     0.00696411     0.01739021
   ht  16     0.10754584     0.46909088     0.46125375     0.03360438    -0.15093689    -0.16178223     0.04848248    -0.09300644

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15         ht  16
   ht   9    -0.00117329
   ht  10    -0.00012168    -0.00800376
   ht  11    -0.00007736     0.00047009    -0.00604094
   ht  12    -0.00082986     0.03612066     0.06485894   -20.57642401
   ht  13     0.07867332    -0.08447075     0.18862381    26.08887153  -147.54998543
   ht  14     0.00233699    -0.04716321    -0.00171031     0.96204525     3.34700910   -10.33679413
   ht  15     0.00033892     0.00012276    -0.00004260     0.00729617     0.00795103    -0.01238892    -0.00052150
   ht  16    -0.00041217     0.00205107    -0.00179385     0.35632754    -0.29577443     0.00970624     0.00017161    -0.01975878

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.805578       0.184963      -0.485461      -0.138795      -4.182239E-02  -5.502336E-02   0.117972       1.888150E-02
 ref    2   9.575024E-03   0.764073       0.422116      -0.417668       4.986205E-02  -0.131047      -5.987461E-02   1.393282E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   4.876816E-02  -7.472125E-02   2.289837E-02  -2.016088E-02   1.182252E-02  -1.534992E-02   0.101704       0.102622    
 ref    2  -1.343756E-02   1.018493E-02   3.220871E-02   1.196775E-02  -3.898299E-02   0.103057       9.233086E-02  -8.243237E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.649047       0.618018       0.413855       0.193710       4.235336E-03   2.020095E-02   1.750227E-02   5.506348E-04

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   2.558901E-03   5.686998E-03   1.561736E-03   5.496880E-04   1.659446E-03   1.085637E-02   1.886876E-02   1.732646E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15        ev   16

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.80557762     0.18496282    -0.48546087    -0.13879502    -0.04182239    -0.05502336     0.11797162     0.01888150
 ref:   2     0.00957502     0.76407252     0.42211639    -0.41766758     0.04986205    -0.13104724    -0.05987461     0.01393282

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1     0.04876816    -0.07472125     0.02289837    -0.02016088     0.01182252    -0.01534992     0.10170436     0.10262243
 ref:   2    -0.01343756     0.01018493     0.03220871     0.01196775    -0.03898299     0.10305704     0.09233086    -0.08243237

 trial vector basis is being transformed.  new dimension:   8

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 38  1   -196.4725845074  3.9529E-08  0.0000E+00  2.5433E-03  1.0000E-04   
 mr-sdci # 38  2   -196.4703067551  1.3484E-03  0.0000E+00  5.1994E-02  1.0000E-04   
 mr-sdci # 38  3   -196.4679837180  6.4045E-05  3.0864E-04  4.0344E-02  1.0000E-04   
 mr-sdci # 38  4   -196.4591671019  2.3618E-02  0.0000E+00  1.1831E-01  1.0000E-04   
 mr-sdci # 38  5   -196.3867600628  9.1269E-03  0.0000E+00  7.7283E-01  1.0000E-04   
 mr-sdci # 38  6   -195.7570754160  1.6864E-04  0.0000E+00  6.3772E-01  1.0000E-04   
 mr-sdci # 38  7   -195.7145316389  1.3276E-04  0.0000E+00  4.8681E-01  1.0000E-04   
 mr-sdci # 38  8   -195.6295704480  6.5966E-03  0.0000E+00  5.8771E-01  1.0000E-04   
 mr-sdci # 38  9   -195.5510438543  1.1414E-02  0.0000E+00  1.7710E+00  1.0000E-04   
 mr-sdci # 38 10   -194.8299789899  5.4144E-02  0.0000E+00  2.0011E+00  1.0000E-04   
 mr-sdci # 38 11   -194.0278643062  3.3981E-04  0.0000E+00  3.4210E+00  1.0000E-04   
 mr-sdci # 38 12   -193.5670867361  7.5490E-04  0.0000E+00  3.6766E+00  1.0000E-04   
 mr-sdci # 38 13   -193.4845773005  1.6473E+00  0.0000E+00  3.8515E+00  1.0000E-04   
 mr-sdci # 38 14   -190.5836330413  9.8624E-03  0.0000E+00  4.3801E+00  1.0000E-04   
 mr-sdci # 38 15   -190.2473225198  8.6239E-01  0.0000E+00  4.0606E+00  1.0000E-04   
 mr-sdci # 38 16   -189.2175413438 -2.0641E+00  0.0000E+00  4.0007E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  39

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58426971
   ht   2     0.00000000   -50.58199196
   ht   3     0.00000000     0.00000000   -50.57966892
   ht   4     0.00000000     0.00000000     0.00000000   -50.57085231
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.49844527
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.86876062
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.82621684
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.74125565
   ht   9     0.12008610     0.11354040     0.38064643     0.13828283    -0.08458198    -0.04627345     0.00719745    -0.06526801

                ht   9
   ht   9    -0.01068924

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.804892      -0.265779       0.402467       0.233676       6.940407E-02  -5.778908E-02  -0.116358       1.788392E-02
 ref    2   9.293216E-03  -0.628487      -0.617523       0.366403      -0.168681      -0.129967       6.246425E-02   1.413978E-02

              v      9
 ref    1   2.571549E-02
 ref    2  -3.031511E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.647937       0.465634       0.543315       0.188856       3.327026E-02   2.023094E-02   1.744103E-02   5.197681E-04

              v      9
 ref    1   1.580292E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.80489185    -0.26577881     0.40246714     0.23367585     0.06940407    -0.05778908    -0.11635829     0.01788392
 ref:   2     0.00929322    -0.62848716    -0.61752308     0.36640297    -0.16868116    -0.12996679     0.06246425     0.01413978

                ci   9
 ref:   1     0.02571549
 ref:   2    -0.03031511

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 39  1   -196.4725845408  3.3354E-08  0.0000E+00  2.6328E-03  1.0000E-04   
 mr-sdci # 39  2   -196.4706099434  3.0319E-04  0.0000E+00  5.4840E-02  1.0000E-04   
 mr-sdci # 39  3   -196.4686887595  7.0504E-04  0.0000E+00  4.9028E-02  1.0000E-04   
 mr-sdci # 39  4   -196.4610561771  1.8891E-03  3.5718E-03  1.0003E-01  1.0000E-04   
 mr-sdci # 39  5   -196.4403219977  5.3562E-02  0.0000E+00  3.1255E-01  1.0000E-04   
 mr-sdci # 39  6   -195.7578358937  7.6048E-04  0.0000E+00  6.6518E-01  1.0000E-04   
 mr-sdci # 39  7   -195.7159877068  1.4561E-03  0.0000E+00  4.6130E-01  1.0000E-04   
 mr-sdci # 39  8   -195.6297525696  1.8212E-04  0.0000E+00  5.8651E-01  1.0000E-04   
 mr-sdci # 39  9   -193.2649682084 -2.2861E+00  0.0000E+00  4.1704E+00  1.0000E-04   
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  40

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58426971
   ht   2     0.00000000   -50.58199196
   ht   3     0.00000000     0.00000000   -50.57966892
   ht   4     0.00000000     0.00000000     0.00000000   -50.57085231
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.49844527
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.86876062
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.82621684
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.74125565
   ht   9     0.12008610     0.11354040     0.38064643     0.13828283    -0.08458198    -0.04627345     0.00719745    -0.06526801
   ht  10    -0.49693563     0.46190101    -1.14175485     0.17232016     0.52203672     0.34753348    -0.07687516    -0.17177662

                ht   9         ht  10
   ht   9    -0.01068924
   ht  10     0.00385103    -0.15535369

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.786386      -0.296713       0.351123      -0.324179       7.050153E-02   6.048325E-02   0.116419      -1.451673E-02
 ref    2  -1.989604E-02  -0.438461      -0.714448      -0.456065      -0.166599       0.129705      -6.251869E-02  -1.804822E-02

              v      9       v     10
 ref    1  -8.626071E-04  -3.701823E-02
 ref    2  -2.832917E-02   1.978292E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.618799       0.280287       0.633723       0.313087       3.272578E-02   2.048159E-02   1.746203E-02   5.364734E-04

              v      9       v     10
 ref    1   8.032858E-04   1.761713E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.78638617    -0.29671329     0.35112304    -0.32417886     0.07050153     0.06048325     0.11641927    -0.01451673
 ref:   2    -0.01989604    -0.43846120    -0.71444779    -0.45606474    -0.16659927     0.12970494    -0.06251869    -0.01804822

                ci   9         ci  10
 ref:   1    -0.00086261    -0.03701823
 ref:   2    -0.02832917     0.01978292

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 40  1   -196.4725866093  2.0685E-06  0.0000E+00  4.3801E-03  1.0000E-04   
 mr-sdci # 40  2   -196.4722440747  1.6341E-03  0.0000E+00  4.8791E-02  1.0000E-04   
 mr-sdci # 40  3   -196.4687501280  6.1368E-05  0.0000E+00  4.9157E-02  1.0000E-04   
 mr-sdci # 40  4   -196.4641769608  3.1208E-03  0.0000E+00  5.2273E-02  1.0000E-04   
 mr-sdci # 40  5   -196.4403368078  1.4810E-05  3.0125E-02  3.1192E-01  1.0000E-04   
 mr-sdci # 40  6   -195.7581725316  3.3664E-04  0.0000E+00  6.6539E-01  1.0000E-04   
 mr-sdci # 40  7   -195.7178664592  1.8788E-03  0.0000E+00  4.3093E-01  1.0000E-04   
 mr-sdci # 40  8   -195.6338703809  4.1178E-03  0.0000E+00  5.7752E-01  1.0000E-04   
 mr-sdci # 40  9   -195.2438753395  1.9789E+00  0.0000E+00  1.7214E+00  1.0000E-04   
 mr-sdci # 40 10   -192.3138372243 -2.5161E+00  0.0000E+00  4.6761E+00  1.0000E-04   
 
 root number  5 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.005000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  41

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58426971
   ht   2     0.00000000   -50.58199196
   ht   3     0.00000000     0.00000000   -50.57966892
   ht   4     0.00000000     0.00000000     0.00000000   -50.57085231
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.49844527
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.86876062
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.82621684
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.74125565
   ht   9     0.12008610     0.11354040     0.38064643     0.13828283    -0.08458198    -0.04627345     0.00719745    -0.06526801
   ht  10    -0.49693563     0.46190101    -1.14175485     0.17232016     0.52203672     0.34753348    -0.07687516    -0.17177662
   ht  11     0.83722980    -1.56349829    -0.28803707    -2.52652473    -0.08739624     0.50328410    -0.48471510    -0.38811913

                ht   9         ht  10         ht  11
   ht   9    -0.01068924
   ht  10     0.00385103    -0.15535369
   ht  11     0.03392940    -0.01007104    -1.22023596

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.784411       0.308076      -0.305142       0.329999      -0.165787      -6.042586E-02  -0.117104       1.415183E-02
 ref    2  -1.865346E-02   0.419127       0.652123       0.443863       0.374604      -0.129740       6.296778E-02   1.812626E-02

              v      9       v     10       v     11
 ref    1   2.814604E-03  -2.353517E-02   2.983774E-02
 ref    2   2.469628E-02   2.716878E-02  -1.199214E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.615648       0.270578       0.518377       0.305914       0.167814       2.048373E-02   1.767833E-02   5.288358E-04

              v      9       v     10       v     11
 ref    1   6.178280E-04   1.292047E-03   1.034102E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.78441063     0.30807649    -0.30514215     0.32999891    -0.16578733    -0.06042586    -0.11710418     0.01415183
 ref:   2    -0.01865346     0.41912669     0.65212338     0.44386304     0.37460445    -0.12973993     0.06296778     0.01812626

                ci   9         ci  10         ci  11
 ref:   1     0.00281460    -0.02353517     0.02983774
 ref:   2     0.02469628     0.02716878    -0.01199214

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 41  1   -196.4725867425  1.3325E-07  0.0000E+00  4.4422E-03  1.0000E-04   
 mr-sdci # 41  2   -196.4722504634  6.3886E-06  0.0000E+00  4.9140E-02  1.0000E-04   
 mr-sdci # 41  3   -196.4701509069  1.4008E-03  0.0000E+00  6.1440E-02  1.0000E-04   
 mr-sdci # 41  4   -196.4641790241  2.0633E-06  0.0000E+00  5.2112E-02  1.0000E-04   
 mr-sdci # 41  5   -196.4595462739  1.9209E-02  0.0000E+00  1.3293E-01  1.0000E-04   
 mr-sdci # 41  6   -195.7581728146  2.8300E-07  9.4556E-03  6.6600E-01  1.0000E-04   
 mr-sdci # 41  7   -195.7187395091  8.7305E-04  0.0000E+00  3.9575E-01  1.0000E-04   
 mr-sdci # 41  8   -195.6341735701  3.0319E-04  0.0000E+00  5.8158E-01  1.0000E-04   
 mr-sdci # 41  9   -195.2651379301  2.1263E-02  0.0000E+00  1.7579E+00  1.0000E-04   
 mr-sdci # 41 10   -194.2011850438  1.8873E+00  0.0000E+00  2.9869E+00  1.0000E-04   
 mr-sdci # 41 11   -191.9864828636 -2.0414E+00  0.0000E+00  4.7875E+00  1.0000E-04   
 
 root number  6 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  42

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58426971
   ht   2     0.00000000   -50.58199196
   ht   3     0.00000000     0.00000000   -50.57966892
   ht   4     0.00000000     0.00000000     0.00000000   -50.57085231
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.49844527
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.86876062
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.82621684
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.74125565
   ht   9     0.12008610     0.11354040     0.38064643     0.13828283    -0.08458198    -0.04627345     0.00719745    -0.06526801
   ht  10    -0.49693563     0.46190101    -1.14175485     0.17232016     0.52203672     0.34753348    -0.07687516    -0.17177662
   ht  11     0.83722980    -1.56349829    -0.28803707    -2.52652473    -0.08739624     0.50328410    -0.48471510    -0.38811913
   ht  12    -2.91631438     6.69682377     1.33189338     5.73311774    -1.53679731     7.53966303    -4.00789565     1.04531395

                ht   9         ht  10         ht  11         ht  12
   ht   9    -0.01068924
   ht  10     0.00385103    -0.15535369
   ht  11     0.03392940    -0.01007104    -1.22023596
   ht  12     0.06013116    -0.39105773    -1.07037508   -15.40618472

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.768656      -0.354483       0.272886       0.350532       0.161057       6.480374E-02  -0.116161       1.648456E-02
 ref    2  -3.239279E-02  -0.372878      -0.678297       0.410813      -0.411943       0.126408       6.907622E-02   2.151356E-02

              v      9       v     10       v     11       v     12
 ref    1  -1.104848E-02  -3.000381E-02  -8.660442E-03   2.326450E-02
 ref    2   2.665808E-02   1.171477E-03   2.353493E-02  -1.199249E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.591881       0.264696       0.534554       0.291640       0.195637       2.017856E-02   1.826494E-02   7.345741E-04

              v      9       v     10       v     11       v     12
 ref    1   8.327222E-04   9.016009E-04   6.288963E-04   6.850568E-04

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.76865592    -0.35448320     0.27288644     0.35053225     0.16105714     0.06480374    -0.11616116     0.01648456
 ref:   2    -0.03239279    -0.37287783    -0.67829702     0.41081305    -0.41194310     0.12640822     0.06907622     0.02151356

                ci   9         ci  10         ci  11         ci  12
 ref:   1    -0.01104848    -0.03000381    -0.00866044     0.02326450
 ref:   2     0.02665808     0.00117148     0.02353493    -0.01199249

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 42  1   -196.4725878989  1.1564E-06  1.0646E-05  6.3439E-03  1.0000E-04   
 mr-sdci # 42  2   -196.4723742705  1.2381E-04  0.0000E+00  4.7106E-02  1.0000E-04   
 mr-sdci # 42  3   -196.4703271665  1.7626E-04  0.0000E+00  5.0920E-02  1.0000E-04   
 mr-sdci # 42  4   -196.4642925258  1.1350E-04  0.0000E+00  4.8674E-02  1.0000E-04   
 mr-sdci # 42  5   -196.4601304112  5.8414E-04  0.0000E+00  1.0684E-01  1.0000E-04   
 mr-sdci # 42  6   -195.7590422074  8.6939E-04  0.0000E+00  6.0928E-01  1.0000E-04   
 mr-sdci # 42  7   -195.7203885904  1.6491E-03  0.0000E+00  3.6491E-01  1.0000E-04   
 mr-sdci # 42  8   -195.6404462980  6.2727E-03  0.0000E+00  5.7959E-01  1.0000E-04   
 mr-sdci # 42  9   -195.4323990216  1.6726E-01  0.0000E+00  1.6211E+00  1.0000E-04   
 mr-sdci # 42 10   -194.9311759320  7.2999E-01  0.0000E+00  2.8509E+00  1.0000E-04   
 mr-sdci # 42 11   -194.0475871827  2.0611E+00  0.0000E+00  2.6905E+00  1.0000E-04   
 mr-sdci # 42 12   -191.7692941857 -1.7978E+00  0.0000E+00  4.7659E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.013000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  43

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58426971
   ht   2     0.00000000   -50.58199196
   ht   3     0.00000000     0.00000000   -50.57966892
   ht   4     0.00000000     0.00000000     0.00000000   -50.57085231
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.49844527
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.86876062
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.82621684
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.74125565
   ht   9     0.12008610     0.11354040     0.38064643     0.13828283    -0.08458198    -0.04627345     0.00719745    -0.06526801
   ht  10    -0.49693563     0.46190101    -1.14175485     0.17232016     0.52203672     0.34753348    -0.07687516    -0.17177662
   ht  11     0.83722980    -1.56349829    -0.28803707    -2.52652473    -0.08739624     0.50328410    -0.48471510    -0.38811913
   ht  12    -2.91631438     6.69682377     1.33189338     5.73311774    -1.53679731     7.53966303    -4.00789565     1.04531395
   ht  13     0.03000292    -0.08905764     0.04840330    -0.04190180     0.01636937    -0.01903197     0.00765410    -0.01588799

                ht   9         ht  10         ht  11         ht  12         ht  13
   ht   9    -0.01068924
   ht  10     0.00385103    -0.15535369
   ht  11     0.03392940    -0.01007104    -1.22023596
   ht  12     0.06013116    -0.39105773    -1.07037508   -15.40618472
   ht  13    -0.00042012     0.00217211    -0.00066115     0.04510165    -0.00056957

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -9.166180E-02   0.838387       0.266807      -0.360768       0.164610      -7.731919E-02   8.858156E-02   3.494512E-02
 ref    2  -0.333281       7.757911E-02  -0.686901      -0.428312      -0.407551      -0.104055      -0.100754      -3.248142E-03

              v      9       v     10       v     11       v     12       v     13
 ref    1   5.400050E-02   3.060202E-02   1.227285E-02   9.311160E-04  -2.648952E-02
 ref    2   1.695032E-02   4.818719E-04  -7.975076E-03   4.711293E-02  -8.066357E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.119478       0.708911       0.543018       0.313604       0.193194       1.680568E-02   1.799812E-02   1.231712E-03

              v      9       v     10       v     11       v     12       v     13
 ref    1   3.203368E-03   9.367156E-04   2.142247E-04   2.220495E-03   7.667607E-04

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.09166180     0.83838669     0.26680683    -0.36076798     0.16461032    -0.07731919     0.08858156     0.03494512
 ref:   2    -0.33328075     0.07757911    -0.68690062    -0.42831168    -0.40755081    -0.10405489    -0.10075429    -0.00324814

                ci   9         ci  10         ci  11         ci  12         ci  13
 ref:   1     0.05400050     0.03060202     0.01227285     0.00093112    -0.02648952
 ref:   2     0.01695032     0.00048187    -0.00797508     0.04711293    -0.00806636

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 43  1   -196.4727636639  1.7577E-04  0.0000E+00  2.1919E-02  1.0000E-04   
 mr-sdci # 43  2   -196.4725784023  2.0413E-04  6.2997E-06  5.5104E-03  1.0000E-04   
 mr-sdci # 43  3   -196.4703306474  3.4808E-06  0.0000E+00  5.0939E-02  1.0000E-04   
 mr-sdci # 43  4   -196.4646631564  3.7063E-04  0.0000E+00  2.1353E-02  1.0000E-04   
 mr-sdci # 43  5   -196.4601396223  9.2111E-06  0.0000E+00  1.0619E-01  1.0000E-04   
 mr-sdci # 43  6   -195.8185041744  5.9462E-02  0.0000E+00  8.0662E-01  1.0000E-04   
 mr-sdci # 43  7   -195.7251378278  4.7492E-03  0.0000E+00  3.0555E-01  1.0000E-04   
 mr-sdci # 43  8   -195.6585033998  1.8057E-02  0.0000E+00  7.2100E-01  1.0000E-04   
 mr-sdci # 43  9   -195.5990613080  1.6666E-01  0.0000E+00  9.0330E-01  1.0000E-04   
 mr-sdci # 43 10   -194.9322940653  1.1181E-03  0.0000E+00  2.8500E+00  1.0000E-04   
 mr-sdci # 43 11   -194.1642569209  1.1667E-01  0.0000E+00  2.5574E+00  1.0000E-04   
 mr-sdci # 43 12   -192.5004689512  7.3117E-01  0.0000E+00  4.0915E+00  1.0000E-04   
 mr-sdci # 43 13   -191.5869943975 -1.8976E+00  0.0000E+00  4.7772E+00  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  44

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58426971
   ht   2     0.00000000   -50.58199196
   ht   3     0.00000000     0.00000000   -50.57966892
   ht   4     0.00000000     0.00000000     0.00000000   -50.57085231
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.49844527
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.86876062
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.82621684
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.74125565
   ht   9     0.12008610     0.11354040     0.38064643     0.13828283    -0.08458198    -0.04627345     0.00719745    -0.06526801
   ht  10    -0.49693563     0.46190101    -1.14175485     0.17232016     0.52203672     0.34753348    -0.07687516    -0.17177662
   ht  11     0.83722980    -1.56349829    -0.28803707    -2.52652473    -0.08739624     0.50328410    -0.48471510    -0.38811913
   ht  12    -2.91631438     6.69682377     1.33189338     5.73311774    -1.53679731     7.53966303    -4.00789565     1.04531395
   ht  13     0.03000292    -0.08905764     0.04840330    -0.04190180     0.01636937    -0.01903197     0.00765410    -0.01588799
   ht  14     0.04254339    -0.00683535    -0.00759264     0.01083695    -0.00737167    -0.00369569     0.01136414     0.00222053

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14
   ht   9    -0.01068924
   ht  10     0.00385103    -0.15535369
   ht  11     0.03392940    -0.01007104    -1.22023596
   ht  12     0.06013116    -0.39105773    -1.07037508   -15.40618472
   ht  13    -0.00042012     0.00217211    -0.00066115     0.04510165    -0.00056957
   ht  14    -0.00005624     0.00080121     0.00149163     0.00859168    -0.00002586    -0.00017894

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.152883       0.827523       0.270408       0.360946       0.167738      -6.408207E-02   0.105644      -4.545668E-02
 ref    2  -0.338094       5.088185E-02  -0.682011       0.438723      -0.404874      -0.104098      -9.073569E-02  -4.256047E-02

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1  -3.531754E-02  -3.035824E-02   1.057843E-02  -2.936485E-02   1.121283E-02   2.279570E-02
 ref    2  -2.108527E-02  -2.619959E-04  -8.329914E-03  -1.015661E-02  -4.611314E-02   1.083343E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.137681       0.687383       0.538260       0.322760       0.192059       1.494281E-02   1.939370E-02   3.877704E-03

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1   1.691917E-03   9.216916E-04   1.812907E-04   9.654512E-04   2.252149E-03   6.370070E-04

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.15288306     0.82752257     0.27040833     0.36094641     0.16773784    -0.06408207     0.10564439    -0.04545668
 ref:   2    -0.33809409     0.05088185    -0.68201119     0.43872285    -0.40487425    -0.10409756    -0.09073569    -0.04256047

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14
 ref:   1    -0.03531754    -0.03035824     0.01057843    -0.02936485     0.01121283     0.02279570
 ref:   2    -0.02108527    -0.00026200    -0.00832991    -0.01015661    -0.04611314     0.01083343

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 44  1   -196.4728491169  8.5453E-05  0.0000E+00  1.3184E-02  1.0000E-04   
 mr-sdci # 44  2   -196.4725820588  3.6565E-06  0.0000E+00  1.4028E-03  1.0000E-04   
 mr-sdci # 44  3   -196.4703355746  4.9272E-06  1.1052E-03  5.0701E-02  1.0000E-04   
 mr-sdci # 44  4   -196.4647299456  6.6789E-05  0.0000E+00  1.4384E-02  1.0000E-04   
 mr-sdci # 44  5   -196.4601871124  4.7490E-05  0.0000E+00  1.0462E-01  1.0000E-04   
 mr-sdci # 44  6   -195.8248450476  6.3409E-03  0.0000E+00  8.5125E-01  1.0000E-04   
 mr-sdci # 44  7   -195.7266722815  1.5345E-03  0.0000E+00  2.8128E-01  1.0000E-04   
 mr-sdci # 44  8   -195.7079298575  4.9426E-02  0.0000E+00  5.5970E-01  1.0000E-04   
 mr-sdci # 44  9   -195.6199795295  2.0918E-02  0.0000E+00  6.6745E-01  1.0000E-04   
 mr-sdci # 44 10   -194.9344121383  2.1181E-03  0.0000E+00  2.8672E+00  1.0000E-04   
 mr-sdci # 44 11   -194.1671200697  2.8631E-03  0.0000E+00  2.5179E+00  1.0000E-04   
 mr-sdci # 44 12   -193.5196650720  1.0192E+00  0.0000E+00  3.6564E+00  1.0000E-04   
 mr-sdci # 44 13   -192.3767781017  7.8978E-01  0.0000E+00  3.9301E+00  1.0000E-04   
 mr-sdci # 44 14   -191.5662740226  9.8264E-01  0.0000E+00  4.8479E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.009000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  45

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58426971
   ht   2     0.00000000   -50.58199196
   ht   3     0.00000000     0.00000000   -50.57966892
   ht   4     0.00000000     0.00000000     0.00000000   -50.57085231
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.49844527
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.86876062
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.82621684
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.74125565
   ht   9     0.12008610     0.11354040     0.38064643     0.13828283    -0.08458198    -0.04627345     0.00719745    -0.06526801
   ht  10    -0.49693563     0.46190101    -1.14175485     0.17232016     0.52203672     0.34753348    -0.07687516    -0.17177662
   ht  11     0.83722980    -1.56349829    -0.28803707    -2.52652473    -0.08739624     0.50328410    -0.48471510    -0.38811913
   ht  12    -2.91631438     6.69682377     1.33189338     5.73311774    -1.53679731     7.53966303    -4.00789565     1.04531395
   ht  13     0.03000292    -0.08905764     0.04840330    -0.04190180     0.01636937    -0.01903197     0.00765410    -0.01588799
   ht  14     0.04254339    -0.00683535    -0.00759264     0.01083695    -0.00737167    -0.00369569     0.01136414     0.00222053
   ht  15     0.11940354    -0.16288423     0.22303614    -0.60172209    -0.14048921     0.19476130     0.06682722     0.01219798

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15
   ht   9    -0.01068924
   ht  10     0.00385103    -0.15535369
   ht  11     0.03392940    -0.01007104    -1.22023596
   ht  12     0.06013116    -0.39105773    -1.07037508   -15.40618472
   ht  13    -0.00042012     0.00217211    -0.00066115     0.04510165    -0.00056957
   ht  14    -0.00005624     0.00080121     0.00149163     0.00859168    -0.00002586    -0.00017894
   ht  15     0.00234346     0.00787979    -0.03304953    -0.01856042    -0.00037277     0.00003514    -0.04676197

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.163651       0.827666      -0.201695       0.291074      -0.320075       6.489174E-02  -0.105584       4.558812E-02
 ref    2  -0.310949       5.045918E-02   0.534944       0.572963       0.473550       0.101008       9.179628E-02   4.290339E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1   3.518361E-02   1.231191E-02   2.765609E-02  -2.312170E-02   2.092699E-02   1.437542E-02   2.372705E-02
 ref    2   2.224272E-02  -5.118850E-02   1.089977E-02  -2.218432E-02  -8.827513E-03  -3.774915E-02   5.420851E-04

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.123471       0.687577       0.326846       0.413011       0.326697       1.441349E-02   1.957452E-02   3.918978E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1   1.732625E-03   2.771846E-03   8.836641E-04   1.026757E-03   5.158639E-04   1.631651E-03   5.632670E-04

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.16365088     0.82766597    -0.20169487     0.29107428    -0.32007458     0.06489174    -0.10558392     0.04558812
 ref:   2    -0.31094899     0.05045918     0.53494395     0.57296305     0.47355002     0.10100770     0.09179628     0.04290339

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15
 ref:   1     0.03518361     0.01231191     0.02765609    -0.02312170     0.02092699     0.01437542     0.02372705
 ref:   2     0.02224272    -0.05118850     0.01089977    -0.02218432    -0.00882751    -0.03774915     0.00054209

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 45  1   -196.4728512706  2.1538E-06  0.0000E+00  1.3110E-02  1.0000E-04   
 mr-sdci # 45  2   -196.4725820590  1.9779E-10  0.0000E+00  1.3998E-03  1.0000E-04   
 mr-sdci # 45  3   -196.4722988836  1.9633E-03  0.0000E+00  3.5438E-02  1.0000E-04   
 mr-sdci # 45  4   -196.4647631657  3.3220E-05  8.0337E-05  1.8447E-02  1.0000E-04   
 mr-sdci # 45  5   -196.4642076238  4.0205E-03  0.0000E+00  3.7301E-02  1.0000E-04   
 mr-sdci # 45  6   -195.8258527726  1.0077E-03  0.0000E+00  8.6132E-01  1.0000E-04   
 mr-sdci # 45  7   -195.7267267956  5.4514E-05  0.0000E+00  2.7616E-01  1.0000E-04   
 mr-sdci # 45  8   -195.7079420442  1.2187E-05  0.0000E+00  5.5849E-01  1.0000E-04   
 mr-sdci # 45  9   -195.6200522958  7.2766E-05  0.0000E+00  6.6669E-01  1.0000E-04   
 mr-sdci # 45 10   -195.4587972589  5.2439E-01  0.0000E+00  1.0632E+00  1.0000E-04   
 mr-sdci # 45 11   -194.9081319580  7.4101E-01  0.0000E+00  2.8364E+00  1.0000E-04   
 mr-sdci # 45 12   -193.7354965034  2.1583E-01  0.0000E+00  3.0780E+00  1.0000E-04   
 mr-sdci # 45 13   -193.4192267804  1.0424E+00  0.0000E+00  3.5154E+00  1.0000E-04   
 mr-sdci # 45 14   -192.3127499358  7.4648E-01  0.0000E+00  4.0372E+00  1.0000E-04   
 mr-sdci # 45 15   -190.8925329057  6.4521E-01  0.0000E+00  4.7275E+00  1.0000E-04   
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.009000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  46

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58426971
   ht   2     0.00000000   -50.58199196
   ht   3     0.00000000     0.00000000   -50.57966892
   ht   4     0.00000000     0.00000000     0.00000000   -50.57085231
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.49844527
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.86876062
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.82621684
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.74125565
   ht   9     0.12008610     0.11354040     0.38064643     0.13828283    -0.08458198    -0.04627345     0.00719745    -0.06526801
   ht  10    -0.49693563     0.46190101    -1.14175485     0.17232016     0.52203672     0.34753348    -0.07687516    -0.17177662
   ht  11     0.83722980    -1.56349829    -0.28803707    -2.52652473    -0.08739624     0.50328410    -0.48471510    -0.38811913
   ht  12    -2.91631438     6.69682377     1.33189338     5.73311774    -1.53679731     7.53966303    -4.00789565     1.04531395
   ht  13     0.03000292    -0.08905764     0.04840330    -0.04190180     0.01636937    -0.01903197     0.00765410    -0.01588799
   ht  14     0.04254339    -0.00683535    -0.00759264     0.01083695    -0.00737167    -0.00369569     0.01136414     0.00222053
   ht  15     0.11940354    -0.16288423     0.22303614    -0.60172209    -0.14048921     0.19476130     0.06682722     0.01219798
   ht  16     0.12637781     0.00339745    -0.07109579     0.20283336    -0.10883738    -0.04468313     0.05204028     0.00326741

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15         ht  16
   ht   9    -0.01068924
   ht  10     0.00385103    -0.15535369
   ht  11     0.03392940    -0.01007104    -1.22023596
   ht  12     0.06013116    -0.39105773    -1.07037508   -15.40618472
   ht  13    -0.00042012     0.00217211    -0.00066115     0.04510165    -0.00056957
   ht  14    -0.00005624     0.00080121     0.00149163     0.00859168    -0.00002586    -0.00017894
   ht  15     0.00234346     0.00787979    -0.03304953    -0.01856042    -0.00037277     0.00003514    -0.04676197
   ht  16    -0.00072950     0.00229419     0.02440233     0.05560178    -0.00009509    -0.00017519     0.00307392    -0.00369365

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.149420       0.824400       0.224216       0.231988       0.365522       4.583927E-02   8.590943E-02  -8.943385E-02
 ref    2   0.350128       5.491550E-02  -0.493014       0.652471      -0.378904       7.803934E-02  -0.106854      -4.524321E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   3.746042E-02  -8.749220E-03  -2.100551E-02  -2.484542E-03   5.138005E-03   3.330721E-02  -2.837813E-02  -3.065113E-02
 ref    2   2.211375E-02   3.003593E-02  -4.932689E-02   4.495823E-02  -9.933273E-03  -4.643813E-02   1.992306E-02   5.375447E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.144916       0.682650       0.293335       0.479537       0.277174       8.191377E-03   1.879827E-02   1.004536E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   1.892301E-03   9.787062E-04   2.874374E-03   2.027416E-03   1.250690E-04   3.265870E-03   1.202247E-03   9.683869E-04

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15        ev   16

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.14942000     0.82439962     0.22421575     0.23198820     0.36552172     0.04583927     0.08590943    -0.08943385
 ref:   2     0.35012846     0.05491550    -0.49301395     0.65247099    -0.37890396     0.07803934    -0.10685431    -0.04524321

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1     0.03746042    -0.00874922    -0.02100551    -0.00248454     0.00513800     0.03330721    -0.02837813    -0.03065113
 ref:   2     0.02211375     0.03003593    -0.04932689     0.04495823    -0.00993327    -0.04643813     0.01992306     0.00537545

 trial vector basis is being transformed.  new dimension:   8

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 46  1   -196.4728706188  1.9348E-05  0.0000E+00  1.1688E-02  1.0000E-04   
 mr-sdci # 46  2   -196.4725821368  7.7867E-08  0.0000E+00  1.2770E-03  1.0000E-04   
 mr-sdci # 46  3   -196.4723985157  9.9632E-05  0.0000E+00  2.4170E-02  1.0000E-04   
 mr-sdci # 46  4   -196.4648503177  8.7152E-05  0.0000E+00  1.1965E-02  1.0000E-04   
 mr-sdci # 46  5   -196.4643039557  9.6332E-05  1.7477E-04  2.4848E-02  1.0000E-04   
 mr-sdci # 46  6   -195.9005382853  7.4686E-02  0.0000E+00  1.1252E+00  1.0000E-04   
 mr-sdci # 46  7   -195.7269741872  2.4739E-04  0.0000E+00  2.7153E-01  1.0000E-04   
 mr-sdci # 46  8   -195.7232612736  1.5319E-02  0.0000E+00  4.2241E-01  1.0000E-04   
 mr-sdci # 46  9   -195.6342461196  1.4194E-02  0.0000E+00  5.6234E-01  1.0000E-04   
 mr-sdci # 46 10   -195.5298962940  7.1099E-02  0.0000E+00  8.9064E-01  1.0000E-04   
 mr-sdci # 46 11   -195.0875341301  1.7940E-01  0.0000E+00  2.3573E+00  1.0000E-04   
 mr-sdci # 46 12   -194.3637185093  6.2822E-01  0.0000E+00  2.5801E+00  1.0000E-04   
 mr-sdci # 46 13   -193.4826440978  6.3417E-02  0.0000E+00  3.1389E+00  1.0000E-04   
 mr-sdci # 46 14   -192.4890344297  1.7628E-01  0.0000E+00  3.6220E+00  1.0000E-04   
 mr-sdci # 46 15   -191.3667000238  4.7417E-01  0.0000E+00  5.1627E+00  1.0000E-04   
 mr-sdci # 46 16   -190.8703357559  1.6528E+00  0.0000E+00  4.5552E+00  1.0000E-04   
 
 root number  5 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.011000
time for eigenvalue solver             0.001000
time for vector access                 0.000000

          starting ci iteration  47

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58455582
   ht   2     0.00000000   -50.58426734
   ht   3     0.00000000     0.00000000   -50.58408372
   ht   4     0.00000000     0.00000000     0.00000000   -50.57653552
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57598916
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.01222349
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.83865939
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.83494648
   ht   9     0.16572356     0.18762716    -0.01292041     0.08442736    -0.11699288     0.03771619    -0.00923770    -0.03515352

                ht   9
   ht   9    -0.00582790

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.164171      -0.827549      -0.194511      -0.213113      -0.380257      -4.523729E-02   0.109056      -5.808751E-02
 ref    2  -0.317015      -4.622030E-02   0.509170      -0.671595       0.353657      -7.918922E-02  -8.755545E-02  -7.614986E-02

              v      9
 ref    1  -2.813492E-02
 ref    2  -2.729353E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.127451       0.686974       0.297089       0.496458       0.269669       8.317345E-03   1.955909E-02   9.172959E-03

              v      9
 ref    1   1.536511E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.16417081    -0.82754921    -0.19451083    -0.21311329    -0.38025707    -0.04523729     0.10905566    -0.05808751
 ref:   2    -0.31701533    -0.04622030     0.50917021    -0.67159546     0.35365726    -0.07918922    -0.08755545    -0.07614986

                ci   9
 ref:   1    -0.02813492
 ref:   2    -0.02729353

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 47  1   -196.4728773488  6.7300E-06  0.0000E+00  1.0683E-02  1.0000E-04   
 mr-sdci # 47  2   -196.4725821625  2.5629E-08  0.0000E+00  1.2588E-03  1.0000E-04   
 mr-sdci # 47  3   -196.4725203759  1.2186E-04  0.0000E+00  1.3653E-02  1.0000E-04   
 mr-sdci # 47  4   -196.4648544296  4.1119E-06  0.0000E+00  1.1586E-02  1.0000E-04   
 mr-sdci # 47  5   -196.4644354316  1.3148E-04  0.0000E+00  1.0746E-02  1.0000E-04   
 mr-sdci # 47  6   -195.9023425288  1.8042E-03  2.2002E-02  1.1181E+00  1.0000E-04   
 mr-sdci # 47  7   -195.7282376271  1.2634E-03  0.0000E+00  2.5770E-01  1.0000E-04   
 mr-sdci # 47  8   -195.7243595504  1.0983E-03  0.0000E+00  4.0480E-01  1.0000E-04   
 mr-sdci # 47  9   -193.8066713194 -1.8276E+00  0.0000E+00  3.0553E+00  1.0000E-04   
 
 root number  6 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.005000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  48

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58455582
   ht   2     0.00000000   -50.58426734
   ht   3     0.00000000     0.00000000   -50.58408372
   ht   4     0.00000000     0.00000000     0.00000000   -50.57653552
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57598916
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.01222349
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.83865939
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.83494648
   ht   9     0.16572356     0.18762716    -0.01292041     0.08442736    -0.11699288     0.03771619    -0.00923770    -0.03515352
   ht  10    -3.15794509     7.42098629    -7.24272621   -15.69629142     1.85644649   -40.24693546     1.57895811    -4.98298576

                ht   9         ht  10
   ht   9    -0.00582790
   ht  10     0.06081474   -72.18634950

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.164446      -0.827596      -0.194048      -0.213127      -0.380258       4.560084E-02   7.732548E-02   9.562761E-02
 ref    2   0.316467      -4.603569E-02   0.509521      -0.671582       0.353697       7.879170E-02  -0.110704       3.453553E-02

              v      9       v     10
 ref    1  -3.631023E-02   2.309279E-02
 ref    2  -1.708122E-02   2.522714E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.127194       0.687034       0.297266       0.496446       0.269697       8.287569E-03   1.823466E-02   1.033734E-02

              v      9       v     10
 ref    1   1.610201E-03   1.169686E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.16444637    -0.82759593    -0.19404815    -0.21312744    -0.38025751     0.04560084     0.07732548     0.09562761
 ref:   2     0.31646731    -0.04603569     0.50952108    -0.67158219     0.35369704     0.07879170    -0.11070424     0.03453553

                ci   9         ci  10
 ref:   1    -0.03631023     0.02309279
 ref:   2    -0.01708122     0.02522714

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 48  1   -196.4728778647  5.1590E-07  3.4594E-05  1.0007E-02  1.0000E-04   
 mr-sdci # 48  2   -196.4725821636  1.1505E-09  0.0000E+00  1.2694E-03  1.0000E-04   
 mr-sdci # 48  3   -196.4725207021  3.2619E-07  0.0000E+00  1.3616E-02  1.0000E-04   
 mr-sdci # 48  4   -196.4648544299  2.4417E-10  0.0000E+00  1.1571E-02  1.0000E-04   
 mr-sdci # 48  5   -196.4644364032  9.7157E-07  0.0000E+00  1.0659E-02  1.0000E-04   
 mr-sdci # 48  6   -195.9029503377  6.0781E-04  0.0000E+00  1.0749E+00  1.0000E-04   
 mr-sdci # 48  7   -195.7284023919  1.6476E-04  0.0000E+00  2.7496E-01  1.0000E-04   
 mr-sdci # 48  8   -195.7275581518  3.1986E-03  0.0000E+00  3.6525E-01  1.0000E-04   
 mr-sdci # 48  9   -194.6716264029  8.6496E-01  0.0000E+00  3.1422E+00  1.0000E-04   
 mr-sdci # 48 10   -193.7869391551 -1.7430E+00  0.0000E+00  3.0545E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.015000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  49

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58455582
   ht   2     0.00000000   -50.58426734
   ht   3     0.00000000     0.00000000   -50.58408372
   ht   4     0.00000000     0.00000000     0.00000000   -50.57653552
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57598916
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.01222349
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.83865939
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.83494648
   ht   9     0.16572356     0.18762716    -0.01292041     0.08442736    -0.11699288     0.03771619    -0.00923770    -0.03515352
   ht  10    -3.15794509     7.42098629    -7.24272621   -15.69629142     1.85644649   -40.24693546     1.57895811    -4.98298576
   ht  11    -0.02022061     0.05521744    -0.02903713    -0.11931853    -0.03688368    -0.15067011     0.00994170    -0.01836756

                ht   9         ht  10         ht  11
   ht   9    -0.00582790
   ht  10     0.06081474   -72.18634950
   ht  11     0.00000472    -0.27459842    -0.00220770

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.167447      -0.827416      -0.191479      -0.216437      -0.378757      -3.643886E-02   3.483351E-02   0.119830    
 ref    2  -0.310633      -4.454102E-02   0.512418      -0.669457       0.358538      -7.657418E-02   9.737185E-02  -6.764235E-02

              v      9       v     10       v     11
 ref    1  -3.848545E-02   2.445482E-02   1.210282E-02
 ref    2  -2.437638E-02   2.590196E-02   9.609772E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.124532       0.686601       0.299237       0.495018       0.272006       7.191396E-03   1.069465E-02   1.893479E-02

              v      9       v     10       v     11
 ref    1   2.075338E-03   1.268950E-03   2.388260E-04

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.16744677    -0.82741603    -0.19147887    -0.21643737    -0.37875744    -0.03643886     0.03483351     0.11983031
 ref:   2    -0.31063348    -0.04454102     0.51241839    -0.66945686     0.35853760    -0.07657418     0.09737185    -0.06764235

                ci   9         ci  10         ci  11
 ref:   1    -0.03848545     0.02445482     0.01210282
 ref:   2    -0.02437638     0.02590196     0.00960977

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 49  1   -196.4729082797  3.0415E-05  0.0000E+00  9.4393E-03  1.0000E-04   
 mr-sdci # 49  2   -196.4725821833  1.9716E-08  5.3670E-07  1.2534E-03  1.0000E-04   
 mr-sdci # 49  3   -196.4725210295  3.2737E-07  0.0000E+00  1.3491E-02  1.0000E-04   
 mr-sdci # 49  4   -196.4648768943  2.2464E-05  0.0000E+00  1.2178E-02  1.0000E-04   
 mr-sdci # 49  5   -196.4644368407  4.3752E-07  0.0000E+00  1.0617E-02  1.0000E-04   
 mr-sdci # 49  6   -196.1079249420  2.0497E-01  0.0000E+00  9.8753E-01  1.0000E-04   
 mr-sdci # 49  7   -195.7288996506  4.9726E-04  0.0000E+00  3.6494E-01  1.0000E-04   
 mr-sdci # 49  8   -195.7282645444  7.0639E-04  0.0000E+00  2.6922E-01  1.0000E-04   
 mr-sdci # 49  9   -194.9510027593  2.7938E-01  0.0000E+00  2.7600E+00  1.0000E-04   
 mr-sdci # 49 10   -193.7901080532  3.1689E-03  0.0000E+00  3.0494E+00  1.0000E-04   
 mr-sdci # 49 11   -193.0596311512 -2.0279E+00  0.0000E+00  3.1405E+00  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  50

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58455582
   ht   2     0.00000000   -50.58426734
   ht   3     0.00000000     0.00000000   -50.58408372
   ht   4     0.00000000     0.00000000     0.00000000   -50.57653552
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57598916
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.01222349
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.83865939
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.83494648
   ht   9     0.16572356     0.18762716    -0.01292041     0.08442736    -0.11699288     0.03771619    -0.00923770    -0.03515352
   ht  10    -3.15794509     7.42098629    -7.24272621   -15.69629142     1.85644649   -40.24693546     1.57895811    -4.98298576
   ht  11    -0.02022061     0.05521744    -0.02903713    -0.11931853    -0.03688368    -0.15067011     0.00994170    -0.01836756
   ht  12    -0.01063030     0.01177283     0.00439546    -0.00633657     0.00112537     0.00164267     0.00136796    -0.00095269

                ht   9         ht  10         ht  11         ht  12
   ht   9    -0.00582790
   ht  10     0.06081474   -72.18634950
   ht  11     0.00000472    -0.27459842    -0.00220770
   ht  12    -0.00000795    -0.00118918    -0.00000212    -0.00001961

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.167987       0.826602      -0.194624       0.216422       0.378748      -3.670154E-02   0.121688       2.101386E-03
 ref    2  -0.310820       4.621242E-02   0.512126       0.669475      -0.358539      -7.564819E-02  -3.163638E-02  -0.112921    

              v      9       v     10       v     11       v     12
 ref    1  -2.525295E-02  -7.020202E-02  -1.447666E-02   2.104836E-02
 ref    2  -3.346314E-02   3.564253E-02  -3.120955E-02  -3.545395E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.124829       0.685406       0.300151       0.495036       0.272000       7.069652E-03   1.580872E-02   1.275552E-02

              v      9       v     10       v     11       v     12
 ref    1   1.757493E-03   6.198714E-03   1.183610E-03   1.700016E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.16798658     0.82660181    -0.19462392     0.21642227     0.37874780    -0.03670154     0.12168753     0.00210139
 ref:   2    -0.31082041     0.04621242     0.51212596     0.66947531    -0.35853930    -0.07564819    -0.03163638    -0.11292078

                ci   9         ci  10         ci  11         ci  12
 ref:   1    -0.02525295    -0.07020202    -0.01447666     0.02104836
 ref:   2    -0.03346314     0.03564253    -0.03120955    -0.03545395

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 50  1   -196.4729084107  1.3095E-07  0.0000E+00  9.5008E-03  1.0000E-04   
 mr-sdci # 50  2   -196.4725825935  4.1018E-07  0.0000E+00  7.3906E-04  1.0000E-04   
 mr-sdci # 50  3   -196.4725211586  1.2916E-07  5.5756E-05  1.3568E-02  1.0000E-04   
 mr-sdci # 50  4   -196.4648768959  1.5445E-09  0.0000E+00  1.2195E-02  1.0000E-04   
 mr-sdci # 50  5   -196.4644370864  2.4567E-07  0.0000E+00  1.0694E-02  1.0000E-04   
 mr-sdci # 50  6   -196.1088882923  9.6335E-04  0.0000E+00  9.9433E-01  1.0000E-04   
 mr-sdci # 50  7   -195.7354810970  6.5814E-03  0.0000E+00  3.0162E-01  1.0000E-04   
 mr-sdci # 50  8   -195.7288485568  5.8401E-04  0.0000E+00  3.4293E-01  1.0000E-04   
 mr-sdci # 50  9   -194.9806408287  2.9638E-02  0.0000E+00  2.6843E+00  1.0000E-04   
 mr-sdci # 50 10   -194.0153443100  2.2524E-01  0.0000E+00  2.4489E+00  1.0000E-04   
 mr-sdci # 50 11   -193.7849649788  7.2533E-01  0.0000E+00  3.1091E+00  1.0000E-04   
 mr-sdci # 50 12   -192.7425081911 -1.6212E+00  0.0000E+00  3.4725E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  51

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58455582
   ht   2     0.00000000   -50.58426734
   ht   3     0.00000000     0.00000000   -50.58408372
   ht   4     0.00000000     0.00000000     0.00000000   -50.57653552
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57598916
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.01222349
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.83865939
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.83494648
   ht   9     0.16572356     0.18762716    -0.01292041     0.08442736    -0.11699288     0.03771619    -0.00923770    -0.03515352
   ht  10    -3.15794509     7.42098629    -7.24272621   -15.69629142     1.85644649   -40.24693546     1.57895811    -4.98298576
   ht  11    -0.02022061     0.05521744    -0.02903713    -0.11931853    -0.03688368    -0.15067011     0.00994170    -0.01836756
   ht  12    -0.01063030     0.01177283     0.00439546    -0.00633657     0.00112537     0.00164267     0.00136796    -0.00095269
   ht  13    -0.11495416    -0.12963121     0.06246714    -0.08832268     0.14786026    -0.01917502     0.01020202     0.01717809

                ht   9         ht  10         ht  11         ht  12         ht  13
   ht   9    -0.00582790
   ht  10     0.06081474   -72.18634950
   ht  11     0.00000472    -0.27459842    -0.00220770
   ht  12    -0.00000795    -0.00118918    -0.00000212    -0.00001961
   ht  13     0.00104066    -0.02640790    -0.00002429    -0.00000659    -0.00262193

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.169534       0.824636       0.200198      -0.206306      -0.385033      -3.670260E-02  -0.119916      -1.988133E-02
 ref    2   0.306597       5.137028E-02  -0.511770      -0.679253       0.343293      -7.566747E-02   1.677329E-02   0.113873    

              v      9       v     10       v     11       v     12       v     13
 ref    1  -1.044504E-02  -2.171434E-02   6.757467E-02   3.282890E-02   2.841270E-02
 ref    2   2.570643E-02  -5.031889E-02  -3.796901E-02  -4.400091E-03   0.101417    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.122743       0.682663       0.301988       0.503947       0.266100       7.072647E-03   1.466127E-02   1.336226E-02

              v      9       v     10       v     11       v     12       v     13
 ref    1   7.699196E-04   3.003503E-03   6.007982E-03   1.097097E-03   1.109262E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.16953424     0.82463589     0.20019793    -0.20630560    -0.38503318    -0.03670260    -0.11991633    -0.01988133
 ref:   2     0.30659671     0.05137028    -0.51176988    -0.67925343     0.34329278    -0.07566747     0.01677329     0.11387270

                ci   9         ci  10         ci  11         ci  12         ci  13
 ref:   1    -0.01044504    -0.02171434     0.06757467     0.03282890     0.02841270
 ref:   2     0.02570643    -0.05031889    -0.03796901    -0.00440009     0.10141667

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 51  1   -196.4729086092  1.9855E-07  0.0000E+00  9.5109E-03  1.0000E-04   
 mr-sdci # 51  2   -196.4725825952  1.6994E-09  0.0000E+00  7.3059E-04  1.0000E-04   
 mr-sdci # 51  3   -196.4725694274  4.8269E-05  0.0000E+00  6.8436E-03  1.0000E-04   
 mr-sdci # 51  4   -196.4648815160  4.6201E-06  3.3078E-05  1.1198E-02  1.0000E-04   
 mr-sdci # 51  5   -196.4644628430  2.5757E-05  0.0000E+00  7.4974E-03  1.0000E-04   
 mr-sdci # 51  6   -196.1088884498  1.5749E-07  0.0000E+00  9.9431E-01  1.0000E-04   
 mr-sdci # 51  7   -195.7360175843  5.3649E-04  0.0000E+00  3.0355E-01  1.0000E-04   
 mr-sdci # 51  8   -195.7303112554  1.4627E-03  0.0000E+00  3.1491E-01  1.0000E-04   
 mr-sdci # 51  9   -195.0775054984  9.6865E-02  0.0000E+00  2.1116E+00  1.0000E-04   
 mr-sdci # 51 10   -194.9515740116  9.3623E-01  0.0000E+00  2.5247E+00  1.0000E-04   
 mr-sdci # 51 11   -194.0086876172  2.2372E-01  0.0000E+00  2.5210E+00  1.0000E-04   
 mr-sdci # 51 12   -192.8224457287  7.9938E-02  0.0000E+00  3.4428E+00  1.0000E-04   
 mr-sdci # 51 13   -191.9762493204 -1.5064E+00  0.0000E+00  4.0220E+00  1.0000E-04   
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.008000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  52

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58455582
   ht   2     0.00000000   -50.58426734
   ht   3     0.00000000     0.00000000   -50.58408372
   ht   4     0.00000000     0.00000000     0.00000000   -50.57653552
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57598916
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.01222349
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.83865939
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.83494648
   ht   9     0.16572356     0.18762716    -0.01292041     0.08442736    -0.11699288     0.03771619    -0.00923770    -0.03515352
   ht  10    -3.15794509     7.42098629    -7.24272621   -15.69629142     1.85644649   -40.24693546     1.57895811    -4.98298576
   ht  11    -0.02022061     0.05521744    -0.02903713    -0.11931853    -0.03688368    -0.15067011     0.00994170    -0.01836756
   ht  12    -0.01063030     0.01177283     0.00439546    -0.00633657     0.00112537     0.00164267     0.00136796    -0.00095269
   ht  13    -0.11495416    -0.12963121     0.06246714    -0.08832268     0.14786026    -0.01917502     0.01020202     0.01717809
   ht  14     0.05523270     0.02610256     0.03756404     0.06332559     0.04018239    -0.07724344    -0.00213975    -0.01415587

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14
   ht   9    -0.00582790
   ht  10     0.06081474   -72.18634950
   ht  11     0.00000472    -0.27459842    -0.00220770
   ht  12    -0.00000795    -0.00118918    -0.00000212    -0.00001961
   ht  13     0.00104066    -0.02640790    -0.00002429    -0.00000659    -0.00262193
   ht  14    -0.00028882    -0.14557292    -0.00054901     0.00002686     0.00007039    -0.00152654

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.165448      -0.823802      -0.205114       0.208093      -0.385119      -2.544159E-03  -0.128223      -3.337689E-05
 ref    2   0.307372      -5.245523E-02   0.506953       0.681135       0.343006      -6.969377E-02   2.883082E-02   0.103763    

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1  -4.404790E-02  -6.921756E-03   6.907469E-02   3.983522E-02  -1.526986E-02  -3.620705E-02
 ref    2  -7.059410E-02  -5.203101E-02  -3.814633E-02  -3.162002E-03   8.264930E-04  -0.103918    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.121851       0.681401       0.299072       0.507247       0.265969       4.863695E-03   1.727229E-02   1.076675E-02

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1   6.923744E-03   2.755137E-03   6.226456E-03   1.596843E-03   2.338516E-04   1.210993E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.16544757    -0.82380195    -0.20511362     0.20809264    -0.38511851    -0.00254416    -0.12822274    -0.00003338
 ref:   2     0.30737216    -0.05245523     0.50695251     0.68113478     0.34300555    -0.06969377     0.02883082     0.10376292

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14
 ref:   1    -0.04404790    -0.00692176     0.06907469     0.03983522    -0.01526986    -0.03620705
 ref:   2    -0.07059410    -0.05203101    -0.03814633    -0.00316200     0.00082649    -0.10391812

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 52  1   -196.4729642899  5.5681E-05  0.0000E+00  2.1675E-02  1.0000E-04   
 mr-sdci # 52  2   -196.4725825987  3.4908E-09  0.0000E+00  7.1531E-04  1.0000E-04   
 mr-sdci # 52  3   -196.4725698553  4.2788E-07  0.0000E+00  6.9627E-03  1.0000E-04   
 mr-sdci # 52  4   -196.4650148028  1.3329E-04  0.0000E+00  3.0039E-02  1.0000E-04   
 mr-sdci # 52  5   -196.4644628434  3.1425E-10  1.7834E-05  7.5038E-03  1.0000E-04   
 mr-sdci # 52  6   -196.3889015894  2.8001E-01  0.0000E+00  7.1237E-01  1.0000E-04   
 mr-sdci # 52  7   -195.7379372144  1.9196E-03  0.0000E+00  2.6690E-01  1.0000E-04   
 mr-sdci # 52  8   -195.7310584869  7.4723E-04  0.0000E+00  3.3484E-01  1.0000E-04   
 mr-sdci # 52  9   -195.5929820230  5.1548E-01  0.0000E+00  1.0375E+00  1.0000E-04   
 mr-sdci # 52 10   -195.0422070078  9.0633E-02  0.0000E+00  1.9027E+00  1.0000E-04   
 mr-sdci # 52 11   -194.0176928414  9.0052E-03  0.0000E+00  2.4540E+00  1.0000E-04   
 mr-sdci # 52 12   -192.8451235314  2.2678E-02  0.0000E+00  3.7235E+00  1.0000E-04   
 mr-sdci # 52 13   -192.6340372474  6.5779E-01  0.0000E+00  3.6626E+00  1.0000E-04   
 mr-sdci # 52 14   -191.9400638064 -5.4897E-01  0.0000E+00  3.9993E+00  1.0000E-04   
 
 root number  5 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.008000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  53

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58455582
   ht   2     0.00000000   -50.58426734
   ht   3     0.00000000     0.00000000   -50.58408372
   ht   4     0.00000000     0.00000000     0.00000000   -50.57653552
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57598916
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.01222349
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.83865939
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.83494648
   ht   9     0.16572356     0.18762716    -0.01292041     0.08442736    -0.11699288     0.03771619    -0.00923770    -0.03515352
   ht  10    -3.15794509     7.42098629    -7.24272621   -15.69629142     1.85644649   -40.24693546     1.57895811    -4.98298576
   ht  11    -0.02022061     0.05521744    -0.02903713    -0.11931853    -0.03688368    -0.15067011     0.00994170    -0.01836756
   ht  12    -0.01063030     0.01177283     0.00439546    -0.00633657     0.00112537     0.00164267     0.00136796    -0.00095269
   ht  13    -0.11495416    -0.12963121     0.06246714    -0.08832268     0.14786026    -0.01917502     0.01020202     0.01717809
   ht  14     0.05523270     0.02610256     0.03756404     0.06332559     0.04018239    -0.07724344    -0.00213975    -0.01415587
   ht  15    -0.02382826     0.00491085     0.04651807     0.02256984     0.10491018    -0.00056297     0.01729501     0.00547328

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15
   ht   9    -0.00582790
   ht  10     0.06081474   -72.18634950
   ht  11     0.00000472    -0.27459842    -0.00220770
   ht  12    -0.00000795    -0.00118918    -0.00000212    -0.00001961
   ht  13     0.00104066    -0.02640790    -0.00002429    -0.00000659    -0.00262193
   ht  14    -0.00028882    -0.14557292    -0.00054901     0.00002686     0.00007039    -0.00152654
   ht  15     0.00034628     0.00804699     0.00008449    -0.00001103    -0.00025546    -0.00014508    -0.00072915

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.165916      -0.822527      -0.209288       0.206336      -0.386378      -2.593672E-03  -0.123213      -3.423199E-02
 ref    2  -0.306081      -5.559752E-02   0.506897       0.682734       0.340552      -6.974108E-02   5.367783E-02  -9.078248E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1   2.920776E-02  -3.759294E-02   5.919517E-02  -6.186355E-02  -2.404826E-02  -2.218337E-02  -2.410806E-02
 ref    2   6.685361E-02  -4.787998E-02  -2.571056E-02   4.062692E-02  -2.060667E-03  -5.540346E-03  -0.115277    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.121214       0.679642       0.300746       0.508700       0.265264       4.870545E-03   1.806282E-02   9.413288E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1   5.322498E-03   3.705721E-03   4.165101E-03   5.477645E-03   5.825652E-04   5.227971E-04   1.386995E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.16591621    -0.82252689    -0.20928842     0.20633591    -0.38637827    -0.00259367    -0.12321326    -0.03423199
 ref:   2    -0.30608080    -0.05559752     0.50689673     0.68273391     0.34055220    -0.06974108     0.05367783    -0.09078248

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15
 ref:   1     0.02920776    -0.03759294     0.05919517    -0.06186355    -0.02404826    -0.02218337    -0.02410806
 ref:   2     0.06685361    -0.04787998    -0.02571056     0.04062692    -0.00206067    -0.00554035    -0.11527685

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 53  1   -196.4729644602  1.7032E-07  0.0000E+00  2.1751E-02  1.0000E-04   
 mr-sdci # 53  2   -196.4725825992  4.5409E-10  0.0000E+00  7.1786E-04  1.0000E-04   
 mr-sdci # 53  3   -196.4725764622  6.6069E-06  0.0000E+00  4.7233E-03  1.0000E-04   
 mr-sdci # 53  4   -196.4650151955  3.9270E-07  0.0000E+00  2.9872E-02  1.0000E-04   
 mr-sdci # 53  5   -196.4644774603  1.4617E-05  0.0000E+00  3.6644E-03  1.0000E-04   
 mr-sdci # 53  6   -196.3889357985  3.4209E-05  1.4322E-01  7.1072E-01  1.0000E-04   
 mr-sdci # 53  7   -195.7390743224  1.1371E-03  0.0000E+00  2.7261E-01  1.0000E-04   
 mr-sdci # 53  8   -195.7334301425  2.3717E-03  0.0000E+00  3.4474E-01  1.0000E-04   
 mr-sdci # 53  9   -195.6154745927  2.2493E-02  0.0000E+00  9.3853E-01  1.0000E-04   
 mr-sdci # 53 10   -195.4427784232  4.0057E-01  0.0000E+00  1.1520E+00  1.0000E-04   
 mr-sdci # 53 11   -194.0485527227  3.0860E-02  0.0000E+00  2.4021E+00  1.0000E-04   
 mr-sdci # 53 12   -193.3822341774  5.3711E-01  0.0000E+00  2.9097E+00  1.0000E-04   
 mr-sdci # 53 13   -192.6906880931  5.6651E-02  0.0000E+00  4.0114E+00  1.0000E-04   
 mr-sdci # 53 14   -192.3719086698  4.3184E-01  0.0000E+00  3.3989E+00  1.0000E-04   
 mr-sdci # 53 15   -191.8299245999  4.6322E-01  0.0000E+00  4.3054E+00  1.0000E-04   
 
 root number  6 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.008000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  54

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58455582
   ht   2     0.00000000   -50.58426734
   ht   3     0.00000000     0.00000000   -50.58408372
   ht   4     0.00000000     0.00000000     0.00000000   -50.57653552
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57598916
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.01222349
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.83865939
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.83494648
   ht   9     0.16572356     0.18762716    -0.01292041     0.08442736    -0.11699288     0.03771619    -0.00923770    -0.03515352
   ht  10    -3.15794509     7.42098629    -7.24272621   -15.69629142     1.85644649   -40.24693546     1.57895811    -4.98298576
   ht  11    -0.02022061     0.05521744    -0.02903713    -0.11931853    -0.03688368    -0.15067011     0.00994170    -0.01836756
   ht  12    -0.01063030     0.01177283     0.00439546    -0.00633657     0.00112537     0.00164267     0.00136796    -0.00095269
   ht  13    -0.11495416    -0.12963121     0.06246714    -0.08832268     0.14786026    -0.01917502     0.01020202     0.01717809
   ht  14     0.05523270     0.02610256     0.03756404     0.06332559     0.04018239    -0.07724344    -0.00213975    -0.01415587
   ht  15    -0.02382826     0.00491085     0.04651807     0.02256984     0.10491018    -0.00056297     0.01729501     0.00547328
   ht  16     8.09564802    13.02257644     4.61495332    -4.51751350     3.54747879     2.36400300     1.03925602    -1.04623958

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15         ht  16
   ht   9    -0.00582790
   ht  10     0.06081474   -72.18634950
   ht  11     0.00000472    -0.27459842    -0.00220770
   ht  12    -0.00000795    -0.00118918    -0.00000212    -0.00001961
   ht  13     0.00104066    -0.02640790    -0.00002429    -0.00000659    -0.00262193
   ht  14    -0.00028882    -0.14557292    -0.00054901     0.00002686     0.00007039    -0.00152654
   ht  15     0.00034628     0.00804699     0.00008449    -0.00001103    -0.00025546    -0.00014508    -0.00072915
   ht  16    -0.04703555     2.50045487     0.00917955    -0.00267070     0.02066547     0.00082595    -0.00243413    -9.30567023

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.108431       0.822375       0.209625       0.181969       0.384424       0.163900      -0.113522       6.304672E-02
 ref    2   0.168792       5.537852E-02  -0.506373       0.449399      -0.347106       0.573866       5.245096E-02   6.645343E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   8.496536E-03  -1.820778E-02  -4.735609E-02  -1.241187E-02  -4.612764E-02  -3.200356E-02   4.432461E-02  -0.116033    
 ref    2   0.104199      -3.114786E-02   2.641101E-02   1.943776E-02   1.753344E-02   1.191036E-02  -0.107605      -4.961946E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   4.024808E-02   0.679367       0.300356       0.235072       0.268265       0.356185       1.563832E-02   8.390947E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   1.092956E-02   1.301713E-03   2.940141E-03   5.318810E-04   2.435181E-03   1.166085E-03   1.354357E-02   1.592576E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15        ev   16

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.10843099     0.82237481     0.20962463     0.18196898     0.38442437     0.16389963    -0.11352187     0.06304672
 ref:   2     0.16879218     0.05537852    -0.50637311     0.44939854    -0.34710613     0.57386560     0.05245096     0.06645343

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1     0.00849654    -0.01820778    -0.04735609    -0.01241187    -0.04612764    -0.03200356     0.04432461    -0.11603306
 ref:   2     0.10419869    -0.03114786     0.02641101     0.01943776     0.01753344     0.01191036    -0.10760531    -0.04961946

 trial vector basis is being transformed.  new dimension:   8

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 54  1   -196.4743448581  1.3804E-03  4.1012E-03  1.2558E-01  1.0000E-04   
 mr-sdci # 54  2   -196.4725825994  2.4632E-10  0.0000E+00  7.1989E-04  1.0000E-04   
 mr-sdci # 54  3   -196.4725764632  9.9647E-10  0.0000E+00  4.7249E-03  1.0000E-04   
 mr-sdci # 54  4   -196.4697150452  4.6998E-03  0.0000E+00  1.7454E-01  1.0000E-04   
 mr-sdci # 54  5   -196.4644774998  3.9465E-08  0.0000E+00  3.5644E-03  1.0000E-04   
 mr-sdci # 54  6   -196.4631853359  7.4250E-02  0.0000E+00  1.1625E-01  1.0000E-04   
 mr-sdci # 54  7   -195.7430370151  3.9627E-03  0.0000E+00  2.7381E-01  1.0000E-04   
 mr-sdci # 54  8   -195.7342241912  7.9405E-04  0.0000E+00  3.4583E-01  1.0000E-04   
 mr-sdci # 54  9   -195.6862310440  7.0756E-02  0.0000E+00  5.2378E-01  1.0000E-04   
 mr-sdci # 54 10   -195.4866692979  4.3891E-02  0.0000E+00  1.0205E+00  1.0000E-04   
 mr-sdci # 54 11   -194.0968425424  4.8290E-02  0.0000E+00  2.3757E+00  1.0000E-04   
 mr-sdci # 54 12   -193.5833452975  2.0111E-01  0.0000E+00  2.4297E+00  1.0000E-04   
 mr-sdci # 54 13   -192.7156340746  2.4946E-02  0.0000E+00  3.9593E+00  1.0000E-04   
 mr-sdci # 54 14   -192.3753431802  3.4345E-03  0.0000E+00  3.4384E+00  1.0000E-04   
 mr-sdci # 54 15   -192.2689519351  4.3903E-01  0.0000E+00  4.0485E+00  1.0000E-04   
 mr-sdci # 54 16   -190.6781501705 -1.9219E-01  0.0000E+00  4.5090E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.014000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  55

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58603006
   ht   2     0.00000000   -50.58426780
   ht   3     0.00000000     0.00000000   -50.58426167
   ht   4     0.00000000     0.00000000     0.00000000   -50.58140025
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616270
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57487054
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.85472222
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.84590940
   ht   9     1.05499329     0.60032593     0.30641746     0.28770674     0.67990771    -0.88381479    -0.19995474     0.07095250

                ht   9
   ht   9    -0.15241892

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -4.542385E-03  -0.822889      -0.211051      -0.177679      -0.388166      -0.189178       0.106501       7.588874E-02
 ref    2   1.310259E-04  -5.573205E-02   0.502480      -0.360752       0.334352      -0.665199      -6.132072E-02   5.997429E-02

              v      9
 ref    1  -3.546242E-02
 ref    2   3.591711E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   2.065043E-05   0.680253       0.297029       0.161712       0.262464       0.478279       1.510268E-02   9.356017E-03

              v      9
 ref    1   2.547622E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00454238    -0.82288913    -0.21105085    -0.17767881    -0.38816588    -0.18917813     0.10650092     0.07588874
 ref:   2     0.00013103    -0.05573205     0.50248022    -0.36075189     0.33435188    -0.66519948    -0.06132072     0.05997429

                ci   9
 ref:   1    -0.03546242
 ref:   2     0.03591711

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 55  1   -196.4826504443  8.3056E-03  0.0000E+00  1.2172E-01  1.0000E-04   
 mr-sdci # 55  2   -196.4725826002  8.0303E-10  1.7876E-07  7.1822E-04  1.0000E-04   
 mr-sdci # 55  3   -196.4725764869  2.3644E-08  0.0000E+00  4.7677E-03  1.0000E-04   
 mr-sdci # 55  4   -196.4723911064  2.6761E-03  0.0000E+00  2.7810E-02  1.0000E-04   
 mr-sdci # 55  5   -196.4644775634  6.3641E-08  0.0000E+00  3.5117E-03  1.0000E-04   
 mr-sdci # 55  6   -196.4643203594  1.1350E-03  0.0000E+00  2.3785E-02  1.0000E-04   
 mr-sdci # 55  7   -195.7460635831  3.0266E-03  0.0000E+00  2.4200E-01  1.0000E-04   
 mr-sdci # 55  8   -195.7347137360  4.8954E-04  0.0000E+00  3.3602E-01  1.0000E-04   
 mr-sdci # 55  9   -193.7492189830 -1.9370E+00  0.0000E+00  3.3462E+00  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.003000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  56

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58603006
   ht   2     0.00000000   -50.58426780
   ht   3     0.00000000     0.00000000   -50.58426167
   ht   4     0.00000000     0.00000000     0.00000000   -50.58140025
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616270
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57487054
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.85472222
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.84590940
   ht   9     1.05499329     0.60032593     0.30641746     0.28770674     0.67990771    -0.88381479    -0.19995474     0.07095250
   ht  10     0.00476232    -0.00738508    -0.00458527     0.00793234    -0.00003051     0.00052801     0.00120859     0.00021294

                ht   9         ht  10
   ht   9    -0.15241892
   ht  10    -0.00005775    -0.00000758

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -4.560172E-03  -0.823418      -0.208197      -0.178582       0.388187      -0.189112      -0.118528       5.671303E-02
 ref    2   5.249648E-05  -5.365617E-02   0.502730      -0.360679      -0.334269      -0.665265       5.193252E-02   6.831320E-02

              v      9       v     10
 ref    1   2.564749E-02   2.799778E-02
 ref    2  -6.190702E-03  -6.537569E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   2.079792E-05   0.680896       0.296084       0.161981       0.262425       0.478341       1.674599E-02   7.883061E-03

              v      9       v     10
 ref    1   6.961184E-04   5.057857E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00456017    -0.82341802    -0.20819719    -0.17858234     0.38818671    -0.18911179    -0.11852849     0.05671303
 ref:   2     0.00005250    -0.05365617     0.50273007    -0.36067905    -0.33426908    -0.66526535     0.05193252     0.06831320

                ci   9         ci  10
 ref:   1     0.02564749     0.02799778
 ref:   2    -0.00619070    -0.06537569

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 56  1   -196.4826596453  9.2009E-06  0.0000E+00  1.2139E-01  1.0000E-04   
 mr-sdci # 56  2   -196.4725827243  1.2408E-07  0.0000E+00  4.0089E-04  1.0000E-04   
 mr-sdci # 56  3   -196.4725764905  3.6055E-09  5.5986E-06  4.7710E-03  1.0000E-04   
 mr-sdci # 56  4   -196.4723914285  3.2217E-07  0.0000E+00  2.7729E-02  1.0000E-04   
 mr-sdci # 56  5   -196.4644775653  1.8962E-09  0.0000E+00  3.5015E-03  1.0000E-04   
 mr-sdci # 56  6   -196.4643205487  1.8930E-07  0.0000E+00  2.3748E-02  1.0000E-04   
 mr-sdci # 56  7   -195.7484372212  2.3736E-03  0.0000E+00  2.4528E-01  1.0000E-04   
 mr-sdci # 56  8   -195.7364281999  1.7145E-03  0.0000E+00  3.3568E-01  1.0000E-04   
 mr-sdci # 56  9   -193.8008428913  5.1624E-02  0.0000E+00  3.1584E+00  1.0000E-04   
 mr-sdci # 56 10   -193.5627598993 -1.9239E+00  0.0000E+00  3.4802E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.007000
time for eigenvalue solver             0.001000
time for vector access                 0.000000

          starting ci iteration  57

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58603006
   ht   2     0.00000000   -50.58426780
   ht   3     0.00000000     0.00000000   -50.58426167
   ht   4     0.00000000     0.00000000     0.00000000   -50.58140025
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616270
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57487054
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.85472222
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.84590940
   ht   9     1.05499329     0.60032593     0.30641746     0.28770674     0.67990771    -0.88381479    -0.19995474     0.07095250
   ht  10     0.00476232    -0.00738508    -0.00458527     0.00793234    -0.00003051     0.00052801     0.00120859     0.00021294
   ht  11    -0.00267908     0.01953555    -0.01592564     0.01855348     0.00573372     0.03853287    -0.00218033     0.00288248

                ht   9         ht  10         ht  11
   ht   9    -0.15241892
   ht  10    -0.00005775    -0.00000758
   ht  11     0.00049626    -0.00000343    -0.00017559

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   3.980698E-03   0.820664      -0.221612       0.174963       0.389402      -0.186745       0.121391      -5.282833E-02
 ref    2   5.128287E-04   5.991141E-02   0.495848       0.369228      -0.330127      -0.667213      -4.848633E-02  -7.090894E-02

              v      9       v     10       v     11
 ref    1  -4.755728E-02  -5.382810E-03  -3.338178E-02
 ref    2  -3.210498E-02  -4.986314E-02   5.327979E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   1.610895E-05   0.677079       0.294977       0.166942       0.260618       0.480046       1.708674E-02   7.818910E-03

              v      9       v     10       v     11
 ref    1   3.292424E-03   2.515308E-03   3.953080E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00398070     0.82066423    -0.22161171     0.17496329     0.38940161    -0.18674517     0.12139118    -0.05282833
 ref:   2     0.00051283     0.05991141     0.49584789     0.36922844    -0.33012712    -0.66721255    -0.04848633    -0.07090894

                ci   9         ci  10         ci  11
 ref:   1    -0.04755728    -0.00538281    -0.03338178
 ref:   2    -0.03210498    -0.04986314     0.05327979

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 57  1   -196.4827212033  6.1558E-05  0.0000E+00  1.1820E-01  1.0000E-04   
 mr-sdci # 57  2   -196.4725827247  3.6926E-10  0.0000E+00  4.0068E-04  1.0000E-04   
 mr-sdci # 57  3   -196.4725810426  4.5522E-06  0.0000E+00  2.1084E-03  1.0000E-04   
 mr-sdci # 57  4   -196.4723937200  2.2915E-06  2.6284E-04  2.6960E-02  1.0000E-04   
 mr-sdci # 57  5   -196.4644779273  3.6197E-07  0.0000E+00  3.0457E-03  1.0000E-04   
 mr-sdci # 57  6   -196.4643232109  2.6622E-06  0.0000E+00  2.2968E-02  1.0000E-04   
 mr-sdci # 57  7   -195.7489727318  5.3551E-04  0.0000E+00  2.3686E-01  1.0000E-04   
 mr-sdci # 57  8   -195.7368211570  3.9296E-04  0.0000E+00  3.3520E-01  1.0000E-04   
 mr-sdci # 57  9   -193.9463124510  1.4547E-01  0.0000E+00  3.1001E+00  1.0000E-04   
 mr-sdci # 57 10   -193.6643540429  1.0159E-01  0.0000E+00  3.3494E+00  1.0000E-04   
 mr-sdci # 57 11   -193.5552053851 -5.4164E-01  0.0000E+00  3.4765E+00  1.0000E-04   
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  58

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58603006
   ht   2     0.00000000   -50.58426780
   ht   3     0.00000000     0.00000000   -50.58426167
   ht   4     0.00000000     0.00000000     0.00000000   -50.58140025
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616270
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57487054
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.85472222
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.84590940
   ht   9     1.05499329     0.60032593     0.30641746     0.28770674     0.67990771    -0.88381479    -0.19995474     0.07095250
   ht  10     0.00476232    -0.00738508    -0.00458527     0.00793234    -0.00003051     0.00052801     0.00120859     0.00021294
   ht  11    -0.00267908     0.01953555    -0.01592564     0.01855348     0.00573372     0.03853287    -0.00218033     0.00288248
   ht  12     0.41782641     0.19009078     0.10892399     0.03595030     0.26342918    -0.15397888    -0.03340851    -0.00563621

                ht   9         ht  10         ht  11         ht  12
   ht   9    -0.15241892
   ht  10    -0.00005775    -0.00000758
   ht  11     0.00049626    -0.00000343    -0.00017559
   ht  12    -0.01354256    -0.00000152    -0.00004120    -0.01283806

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -6.719206E-04   0.818952       0.235508      -0.160484      -0.397853      -0.171909       0.112630       6.699329E-02
 ref    2  -2.009128E-04   6.002061E-02  -0.471725      -0.388202       0.298377      -0.688529      -5.551476E-02   6.489971E-02

              v      9       v     10       v     11       v     12
 ref    1   2.441330E-02   4.501960E-02  -3.027104E-02  -6.649179E-02
 ref    2  -2.095861E-03   5.383611E-02   3.573621E-02   6.093534E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   4.918432E-07   0.674284       0.277989       0.176456       0.247316       0.503625       1.576741E-02   8.700074E-03

              v      9       v     10       v     11       v     12
 ref    1   6.004018E-04   4.925091E-03   2.193412E-03   8.134274E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00067192     0.81895174     0.23550772    -0.16048397    -0.39785301    -0.17190933     0.11263003     0.06699329
 ref:   2    -0.00020091     0.06002061    -0.47172521    -0.38820158     0.29837718    -0.68852864    -0.05551476     0.06489971

                ci   9         ci  10         ci  11         ci  12
 ref:   1     0.02441330     0.04501960    -0.03027104    -0.06649179
 ref:   2    -0.00209586     0.05383611     0.03573621     0.06093534

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 58  1   -196.4872364618  4.5153E-03  0.0000E+00  5.6211E-02  1.0000E-04   
 mr-sdci # 58  2   -196.4725827252  5.1666E-10  0.0000E+00  3.9943E-04  1.0000E-04   
 mr-sdci # 58  3   -196.4725811244  8.1789E-08  0.0000E+00  2.0852E-03  1.0000E-04   
 mr-sdci # 58  4   -196.4725632124  1.6949E-04  0.0000E+00  9.7779E-03  1.0000E-04   
 mr-sdci # 58  5   -196.4644779781  5.0796E-08  3.0558E-06  3.1076E-03  1.0000E-04   
 mr-sdci # 58  6   -196.4644575851  1.3437E-04  0.0000E+00  1.0452E-02  1.0000E-04   
 mr-sdci # 58  7   -195.7514767168  2.5040E-03  0.0000E+00  2.5958E-01  1.0000E-04   
 mr-sdci # 58  8   -195.7376357244  8.1457E-04  0.0000E+00  3.2874E-01  1.0000E-04   
 mr-sdci # 58  9   -195.3215603436  1.3752E+00  0.0000E+00  1.4564E+00  1.0000E-04   
 mr-sdci # 58 10   -193.8894428526  2.2509E-01  0.0000E+00  3.1540E+00  1.0000E-04   
 mr-sdci # 58 11   -193.5665014270  1.1296E-02  0.0000E+00  3.4118E+00  1.0000E-04   
 mr-sdci # 58 12   -191.2647202995 -2.3186E+00  0.0000E+00  3.9484E+00  1.0000E-04   
 
 root number  5 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  59

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58603006
   ht   2     0.00000000   -50.58426780
   ht   3     0.00000000     0.00000000   -50.58426167
   ht   4     0.00000000     0.00000000     0.00000000   -50.58140025
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616270
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57487054
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.85472222
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.84590940
   ht   9     1.05499329     0.60032593     0.30641746     0.28770674     0.67990771    -0.88381479    -0.19995474     0.07095250
   ht  10     0.00476232    -0.00738508    -0.00458527     0.00793234    -0.00003051     0.00052801     0.00120859     0.00021294
   ht  11    -0.00267908     0.01953555    -0.01592564     0.01855348     0.00573372     0.03853287    -0.00218033     0.00288248
   ht  12     0.41782641     0.19009078     0.10892399     0.03595030     0.26342918    -0.15397888    -0.03340851    -0.00563621
   ht  13     0.00586337     0.00897265    -0.01584878    -0.00031648    -0.03399287     0.00727843     0.00110807    -0.00038729

                ht   9         ht  10         ht  11         ht  12         ht  13
   ht   9    -0.15241892
   ht  10    -0.00005775    -0.00000758
   ht  11     0.00049626    -0.00000343    -0.00017559
   ht  12    -0.01354256    -0.00000152    -0.00004120    -0.01283806
   ht  13     0.00041248     0.00000049    -0.00001158     0.00011340    -0.00010484

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -6.954724E-04   0.818946       0.235544      -0.160420      -0.400273      -0.166205      -0.107488      -7.499395E-02
 ref    2  -2.106374E-04   6.001096E-02  -0.471644      -0.388303       0.288413      -0.692735       6.183057E-02  -6.126609E-02

              v      9       v     10       v     11       v     12       v     13
 ref    1  -2.489320E-02  -3.911462E-02   2.326706E-02   2.856263E-02   6.851465E-02
 ref    2   6.973563E-04  -4.649847E-02   3.531625E-02  -4.848120E-02  -6.839323E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   5.280500E-07   0.674274       0.277929       0.176514       0.243401       0.507506       1.537672E-02   9.377625E-03

              v      9       v     10       v     11       v     12       v     13
 ref    1   6.201577E-04   3.692062E-03   1.788594E-03   3.166250E-03   9.371892E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00069547     0.81894610     0.23554382    -0.16041987    -0.40027332    -0.16620493    -0.10748814    -0.07499395
 ref:   2    -0.00021064     0.06001096    -0.47164449    -0.38830316     0.28841309    -0.69273533     0.06183057    -0.06126609

                ci   9         ci  10         ci  11         ci  12         ci  13
 ref:   1    -0.02489320    -0.03911462     0.02326706     0.02856263     0.06851465
 ref:   2     0.00069736    -0.04649847     0.03531625    -0.04848120    -0.06839323

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 59  1   -196.4872399183  3.4565E-06  0.0000E+00  5.5712E-02  1.0000E-04   
 mr-sdci # 59  2   -196.4725827252  1.0090E-12  0.0000E+00  3.9947E-04  1.0000E-04   
 mr-sdci # 59  3   -196.4725811245  4.1325E-11  0.0000E+00  2.0836E-03  1.0000E-04   
 mr-sdci # 59  4   -196.4725635395  3.2704E-07  0.0000E+00  9.6609E-03  1.0000E-04   
 mr-sdci # 59  5   -196.4644802311  2.2530E-06  0.0000E+00  1.5392E-03  1.0000E-04   
 mr-sdci # 59  6   -196.4644576270  4.1810E-08  2.3901E-05  1.0367E-02  1.0000E-04   
 mr-sdci # 59  7   -195.7544839110  3.0072E-03  0.0000E+00  2.3895E-01  1.0000E-04   
 mr-sdci # 59  8   -195.7380352159  3.9949E-04  0.0000E+00  3.3529E-01  1.0000E-04   
 mr-sdci # 59  9   -195.3249499800  3.3896E-03  0.0000E+00  1.4312E+00  1.0000E-04   
 mr-sdci # 59 10   -193.9417682893  5.2325E-02  0.0000E+00  3.1395E+00  1.0000E-04   
 mr-sdci # 59 11   -193.8540701920  2.8757E-01  0.0000E+00  2.9748E+00  1.0000E-04   
 mr-sdci # 59 12   -193.3932327914  2.1285E+00  0.0000E+00  3.4011E+00  1.0000E-04   
 mr-sdci # 59 13   -191.2081762065 -1.5075E+00  0.0000E+00  4.0242E+00  1.0000E-04   
 
 root number  6 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  60

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58603006
   ht   2     0.00000000   -50.58426780
   ht   3     0.00000000     0.00000000   -50.58426167
   ht   4     0.00000000     0.00000000     0.00000000   -50.58140025
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616270
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57487054
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.85472222
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.84590940
   ht   9     1.05499329     0.60032593     0.30641746     0.28770674     0.67990771    -0.88381479    -0.19995474     0.07095250
   ht  10     0.00476232    -0.00738508    -0.00458527     0.00793234    -0.00003051     0.00052801     0.00120859     0.00021294
   ht  11    -0.00267908     0.01953555    -0.01592564     0.01855348     0.00573372     0.03853287    -0.00218033     0.00288248
   ht  12     0.41782641     0.19009078     0.10892399     0.03595030     0.26342918    -0.15397888    -0.03340851    -0.00563621
   ht  13     0.00586337     0.00897265    -0.01584878    -0.00031648    -0.03399287     0.00727843     0.00110807    -0.00038729
   ht  14    -0.05982503    -0.03760322    -0.03611054    -0.03951028    -0.02756961    -0.04732539     0.00869051     0.00102482

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14
   ht   9    -0.15241892
   ht  10    -0.00005775    -0.00000758
   ht  11     0.00049626    -0.00000343    -0.00017559
   ht  12    -0.01354256    -0.00000152    -0.00004120    -0.01283806
   ht  13     0.00041248     0.00000049    -0.00001158     0.00011340    -0.00010484
   ht  14     0.00265048     0.00000168     0.00005943     0.00048483     0.00001109    -0.00069058

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -1.275698E-04  -0.820061      -0.237278      -0.151689      -0.410340      -0.139768       0.106992      -7.548298E-02
 ref    2  -4.597615E-04  -6.211405E-02   0.466022      -0.393620       0.242258      -0.710726      -6.018427E-02  -6.088026E-02

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1  -1.839626E-02  -3.708292E-02   2.403501E-02   1.184147E-02   3.667747E-02   8.825922E-02
 ref    2   2.102235E-02  -4.615566E-02   2.718161E-02  -6.788176E-02   5.288916E-02  -4.642490E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   2.276547E-07   0.676358       0.273477       0.177947       0.227068       0.524667       1.506946E-02   9.404086E-03

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1   7.803614E-04   3.505488E-03   1.316521E-03   4.748153E-03   4.142500E-03   9.944960E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00012757    -0.82006067    -0.23727778    -0.15168886    -0.41033990    -0.13976843     0.10699211    -0.07548298
 ref:   2    -0.00045976    -0.06211405     0.46602183    -0.39362038     0.24225825    -0.71072649    -0.06018427    -0.06088026

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14
 ref:   1    -0.01839626    -0.03708292     0.02403501     0.01184147     0.03667747     0.08825922
 ref:   2     0.02102235    -0.04615566     0.02718161    -0.06788176     0.05288916    -0.04642490

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 60  1   -196.4877436992  5.0378E-04  1.9317E-04  2.6048E-02  1.0000E-04   
 mr-sdci # 60  2   -196.4725827257  5.1252E-10  0.0000E+00  4.0260E-04  1.0000E-04   
 mr-sdci # 60  3   -196.4725811265  2.0736E-09  0.0000E+00  2.1024E-03  1.0000E-04   
 mr-sdci # 60  4   -196.4725748213  1.1282E-05  0.0000E+00  5.4601E-03  1.0000E-04   
 mr-sdci # 60  5   -196.4644802545  2.3451E-08  0.0000E+00  1.5089E-03  1.0000E-04   
 mr-sdci # 60  6   -196.4644758659  1.8239E-05  0.0000E+00  3.9890E-03  1.0000E-04   
 mr-sdci # 60  7   -195.7557775642  1.2937E-03  0.0000E+00  2.2694E-01  1.0000E-04   
 mr-sdci # 60  8   -195.7380396546  4.4387E-06  0.0000E+00  3.3526E-01  1.0000E-04   
 mr-sdci # 60  9   -195.5307536589  2.0580E-01  0.0000E+00  8.0302E-01  1.0000E-04   
 mr-sdci # 60 10   -193.9458083941  4.0401E-03  0.0000E+00  3.0513E+00  1.0000E-04   
 mr-sdci # 60 11   -193.8905409547  3.6471E-02  0.0000E+00  2.9154E+00  1.0000E-04   
 mr-sdci # 60 12   -193.4833911064  9.0158E-02  0.0000E+00  3.5226E+00  1.0000E-04   
 mr-sdci # 60 13   -192.5995286210  1.3914E+00  0.0000E+00  3.5052E+00  1.0000E-04   
 mr-sdci # 60 14   -190.9342313874 -1.4411E+00  0.0000E+00  4.1280E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.014000
time for eigenvalue solver             0.001000
time for vector access                 0.000000

          starting ci iteration  61

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58603006
   ht   2     0.00000000   -50.58426780
   ht   3     0.00000000     0.00000000   -50.58426167
   ht   4     0.00000000     0.00000000     0.00000000   -50.58140025
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616270
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57487054
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.85472222
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.84590940
   ht   9     1.05499329     0.60032593     0.30641746     0.28770674     0.67990771    -0.88381479    -0.19995474     0.07095250
   ht  10     0.00476232    -0.00738508    -0.00458527     0.00793234    -0.00003051     0.00052801     0.00120859     0.00021294
   ht  11    -0.00267908     0.01953555    -0.01592564     0.01855348     0.00573372     0.03853287    -0.00218033     0.00288248
   ht  12     0.41782641     0.19009078     0.10892399     0.03595030     0.26342918    -0.15397888    -0.03340851    -0.00563621
   ht  13     0.00586337     0.00897265    -0.01584878    -0.00031648    -0.03399287     0.00727843     0.00110807    -0.00038729
   ht  14    -0.05982503    -0.03760322    -0.03611054    -0.03951028    -0.02756961    -0.04732539     0.00869051     0.00102482
   ht  15     0.11043956    -0.00874594     0.16427297     0.08142735    -0.02098386     0.14604105    -0.00762303     0.01864950

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15
   ht   9    -0.15241892
   ht  10    -0.00005775    -0.00000758
   ht  11     0.00049626    -0.00000343    -0.00017559
   ht  12    -0.01354256    -0.00000152    -0.00004120    -0.01283806
   ht  13     0.00041248     0.00000049    -0.00001158     0.00011340    -0.00010484
   ht  14     0.00265048     0.00000168     0.00005943     0.00048483     0.00001109    -0.00069058
   ht  15    -0.00030026    -0.00000729     0.00004500    -0.00122395    -0.00000982     0.00038924    -0.00604760

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   2.115770E-05  -0.821162      -0.254243      -0.113321       0.417631      -0.116278       0.101740      -8.039256E-02
 ref    2  -2.747383E-04  -6.249879E-02   0.405042      -0.455910      -0.201439      -0.723441      -5.754809E-02  -5.703318E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1  -2.501417E-02   2.794289E-02  -3.077287E-03   3.063621E-02   8.072715E-03  -5.042192E-02   8.454263E-02
 ref    2   4.017393E-02   5.819989E-02  -4.165071E-03   6.108498E-04  -6.314534E-02  -4.744761E-02  -4.934655E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   7.592880E-08   0.678213       0.228699       0.220696       0.214993       0.536888       1.366290E-02   9.715747E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1   2.239653E-03   4.168032E-03   2.681752E-05   9.389507E-04   4.052503E-03   4.793646E-03   9.582538E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00002116    -0.82116167    -0.25424342    -0.11332107     0.41763076    -0.11627758     0.10174043    -0.08039256
 ref:   2    -0.00027474    -0.06249879     0.40504187    -0.45591025    -0.20143899    -0.72344113    -0.05754809    -0.05703318

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15
 ref:   1    -0.02501417     0.02794289    -0.00307729     0.03063621     0.00807271    -0.05042192     0.08454263
 ref:   2     0.04017393     0.05819989    -0.00416507     0.00061085    -0.06314534    -0.04744761    -0.04934655

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 61  1   -196.4879147600  1.7106E-04  0.0000E+00  1.2812E-02  1.0000E-04   
 mr-sdci # 61  2   -196.4725827258  1.3284E-10  6.2488E-08  4.0076E-04  1.0000E-04   
 mr-sdci # 61  3   -196.4725812433  1.1677E-07  0.0000E+00  2.1265E-03  1.0000E-04   
 mr-sdci # 61  4   -196.4725781981  3.3767E-06  0.0000E+00  3.6317E-03  1.0000E-04   
 mr-sdci # 61  5   -196.4644802620  7.4592E-09  0.0000E+00  1.4870E-03  1.0000E-04   
 mr-sdci # 61  6   -196.4644787476  2.8817E-06  0.0000E+00  3.1239E-03  1.0000E-04   
 mr-sdci # 61  7   -195.7589163499  3.1388E-03  0.0000E+00  2.2463E-01  1.0000E-04   
 mr-sdci # 61  8   -195.7382394759  1.9982E-04  0.0000E+00  3.3136E-01  1.0000E-04   
 mr-sdci # 61  9   -195.6330730967  1.0232E-01  0.0000E+00  6.3278E-01  1.0000E-04   
 mr-sdci # 61 10   -194.5077500981  5.6194E-01  0.0000E+00  2.5902E+00  1.0000E-04   
 mr-sdci # 61 11   -193.9027809737  1.2240E-02  0.0000E+00  2.7587E+00  1.0000E-04   
 mr-sdci # 61 12   -193.6730049137  1.8961E-01  0.0000E+00  3.5380E+00  1.0000E-04   
 mr-sdci # 61 13   -193.4631779012  8.6365E-01  0.0000E+00  3.3833E+00  1.0000E-04   
 mr-sdci # 61 14   -191.9139518648  9.7972E-01  0.0000E+00  3.3371E+00  1.0000E-04   
 mr-sdci # 61 15   -190.9225687915 -1.3464E+00  0.0000E+00  4.0948E+00  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.009000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  62

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.58603006
   ht   2     0.00000000   -50.58426780
   ht   3     0.00000000     0.00000000   -50.58426167
   ht   4     0.00000000     0.00000000     0.00000000   -50.58140025
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616270
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57487054
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.85472222
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.84590940
   ht   9     1.05499329     0.60032593     0.30641746     0.28770674     0.67990771    -0.88381479    -0.19995474     0.07095250
   ht  10     0.00476232    -0.00738508    -0.00458527     0.00793234    -0.00003051     0.00052801     0.00120859     0.00021294
   ht  11    -0.00267908     0.01953555    -0.01592564     0.01855348     0.00573372     0.03853287    -0.00218033     0.00288248
   ht  12     0.41782641     0.19009078     0.10892399     0.03595030     0.26342918    -0.15397888    -0.03340851    -0.00563621
   ht  13     0.00586337     0.00897265    -0.01584878    -0.00031648    -0.03399287     0.00727843     0.00110807    -0.00038729
   ht  14    -0.05982503    -0.03760322    -0.03611054    -0.03951028    -0.02756961    -0.04732539     0.00869051     0.00102482
   ht  15     0.11043956    -0.00874594     0.16427297     0.08142735    -0.02098386     0.14604105    -0.00762303     0.01864950
   ht  16    -0.00254184     0.00637231     0.00282438    -0.00486011    -0.00048144    -0.00024618    -0.00074353     0.00041743

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15         ht  16
   ht   9    -0.15241892
   ht  10    -0.00005775    -0.00000758
   ht  11     0.00049626    -0.00000343    -0.00017559
   ht  12    -0.01354256    -0.00000152    -0.00004120    -0.01283806
   ht  13     0.00041248     0.00000049    -0.00001158     0.00011340    -0.00010484
   ht  14     0.00265048     0.00000168     0.00005943     0.00048483     0.00001109    -0.00069058
   ht  15    -0.00030026    -0.00000729     0.00004500    -0.00122395    -0.00000982     0.00038924    -0.00604760
   ht  16    -0.00001550     0.00000154     0.00000084    -0.00000537    -0.00000042    -0.00000043    -0.00000114    -0.00000317

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -2.260068E-05   0.819991       0.259861       0.109059      -0.417758      -0.115823      -0.111061      -1.668993E-02
 ref    2  -2.658827E-04   6.789349E-02  -0.405173       0.455020       0.200650      -0.723658       2.449820E-02  -7.720967E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1  -8.503704E-03   7.955121E-02   3.127417E-02  -1.188200E-03   3.050789E-02   4.636400E-02   6.021000E-02  -9.629116E-02
 ref    2   3.994632E-02  -8.644546E-03   6.087393E-02   2.354661E-02   4.969605E-03   5.317888E-02  -2.477218E-02   9.147777E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   7.120442E-08   0.676995       0.231693       0.218937       0.214782       0.537096       1.293462E-02   6.239887E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   1.668022E-03   6.403123E-03   4.683709E-03   5.558545E-04   9.554281E-04   4.977615E-03   4.238905E-03   1.764017E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15        ev   16

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00002260     0.81999090     0.25986095     0.10905922    -0.41775761    -0.11582328    -0.11106061    -0.01668993
 ref:   2    -0.00026588     0.06789349    -0.40517281     0.45501962     0.20065024    -0.72365783     0.02449820    -0.07720967

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1    -0.00850370     0.07955121     0.03127417    -0.00118820     0.03050789     0.04636400     0.06021000    -0.09629116
 ref:   2     0.03994632    -0.00864455     0.06087393     0.02354661     0.00496960     0.05317888    -0.02477218     0.09147777

 trial vector basis is being transformed.  new dimension:   8

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 62  1   -196.4879153656  6.0558E-07  0.0000E+00  1.2846E-02  1.0000E-04   
 mr-sdci # 62  2   -196.4725827949  6.9109E-08  0.0000E+00  1.4676E-04  1.0000E-04   
 mr-sdci # 62  3   -196.4725812450  1.6400E-09  1.4470E-06  2.1201E-03  1.0000E-04   
 mr-sdci # 62  4   -196.4725782075  9.4392E-09  0.0000E+00  3.6291E-03  1.0000E-04   
 mr-sdci # 62  5   -196.4644802623  3.2863E-10  0.0000E+00  1.4885E-03  1.0000E-04   
 mr-sdci # 62  6   -196.4644787558  8.2625E-09  0.0000E+00  3.1176E-03  1.0000E-04   
 mr-sdci # 62  7   -195.7709590363  1.2043E-02  0.0000E+00  2.6233E-01  1.0000E-04   
 mr-sdci # 62  8   -195.7468039554  8.5645E-03  0.0000E+00  3.2759E-01  1.0000E-04   
 mr-sdci # 62  9   -195.6354230503  2.3500E-03  0.0000E+00  6.3601E-01  1.0000E-04   
 mr-sdci # 62 10   -195.5327829931  1.0250E+00  0.0000E+00  6.4464E-01  1.0000E-04   
 mr-sdci # 62 11   -194.4979637577  5.9518E-01  0.0000E+00  2.5923E+00  1.0000E-04   
 mr-sdci # 62 12   -193.8014282315  1.2842E-01  0.0000E+00  2.8259E+00  1.0000E-04   
 mr-sdci # 62 13   -193.6721065415  2.0893E-01  0.0000E+00  3.5091E+00  1.0000E-04   
 mr-sdci # 62 14   -191.9182112640  4.2594E-03  0.0000E+00  3.3859E+00  1.0000E-04   
 mr-sdci # 62 15   -190.9355414060  1.2973E-02  0.0000E+00  4.0616E+00  1.0000E-04   
 mr-sdci # 62 16   -190.7666262809  8.8476E-02  0.0000E+00  3.7959E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.011000
time for eigenvalue solver             0.001000
time for vector access                 0.000000

          starting ci iteration  63

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59960057
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426645
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426341
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616547
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616396
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88264424
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.85848916
   ht   9     0.00028057     0.01920532    -0.00430294     0.00486802    -0.00511075    -0.03038589    -0.00238239    -0.00088197

                ht   9
   ht   9    -0.00006145

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   4.362782E-05   0.820289      -0.262084       0.101115      -0.417364      -0.117262       0.111096       1.633911E-02
 ref    2   3.803279E-04   6.745702E-02   0.391356       0.467086       0.203088      -0.722980      -2.447740E-02   7.656243E-02

              v      9
 ref    1  -5.341971E-02
 ref    2  -8.237015E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   1.465527E-07   0.677425       0.221847       0.228394       0.215438       0.536450       1.294154E-02   6.128773E-03

              v      9
 ref    1   9.638508E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00004363     0.82028919    -0.26208357     0.10111537    -0.41736427    -0.11726194     0.11109632     0.01633911
 ref:   2     0.00038033     0.06745702     0.39135577     0.46708648     0.20308775    -0.72297952    -0.02447740     0.07656243

                ci   9
 ref:   1    -0.05341971
 ref:   2    -0.08237015

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 63  1   -196.4879172239  1.8583E-06  0.0000E+00  1.2686E-02  1.0000E-04   
 mr-sdci # 63  2   -196.4725827949  1.1724E-12  0.0000E+00  1.4646E-04  1.0000E-04   
 mr-sdci # 63  3   -196.4725822224  9.7742E-07  0.0000E+00  1.0909E-03  1.0000E-04   
 mr-sdci # 63  4   -196.4725782189  1.1438E-08  3.9384E-06  3.5764E-03  1.0000E-04   
 mr-sdci # 63  5   -196.4644802624  8.9798E-11  0.0000E+00  1.4881E-03  1.0000E-04   
 mr-sdci # 63  6   -196.4644789968  2.4095E-07  0.0000E+00  2.8490E-03  1.0000E-04   
 mr-sdci # 63  7   -195.7709607043  1.6680E-06  0.0000E+00  2.6203E-01  1.0000E-04   
 mr-sdci # 63  8   -195.7469415211  1.3757E-04  0.0000E+00  3.3188E-01  1.0000E-04   
 mr-sdci # 63  9   -193.4976765984 -2.1377E+00  0.0000E+00  3.3791E+00  1.0000E-04   
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.006000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  64

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59960057
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426645
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426341
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616547
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616396
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88264424
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.85848916
   ht   9     0.00028057     0.01920532    -0.00430294     0.00486802    -0.00511075    -0.03038589    -0.00238239    -0.00088197
   ht  10    -0.01141583    -0.01804694     0.00547386    -0.00896178    -0.00306726     0.00167277    -0.00069235     0.00633635

                ht   9         ht  10
   ht   9    -0.00006145
   ht  10     0.00001547    -0.00012831

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -3.147354E-05   0.816724      -0.287780       4.396133E-02  -0.419194      -0.110550       0.109302       2.746047E-02
 ref    2  -3.490560E-04   6.146562E-02   0.258817       0.552377       0.191445      -0.726159      -3.141289E-02   7.197199E-02

              v      9       v     10
 ref    1   1.883582E-02   5.027249E-02
 ref    2   4.021010E-02   7.615697E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   1.228306E-07   0.670817       0.149804       0.307053       0.212375       0.539528       1.293371E-02   5.934044E-03

              v      9       v     10
 ref    1   1.971641E-03   8.327207E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00003147     0.81672430    -0.28778043     0.04396133    -0.41919385    -0.11054997     0.10930208     0.02746047
 ref:   2    -0.00034906     0.06146562     0.25881676     0.55237671     0.19144547    -0.72615880    -0.03141289     0.07197199

                ci   9         ci  10
 ref:   1     0.01883582     0.05027249
 ref:   2     0.04021010     0.07615697

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 64  1   -196.4879174649  2.4107E-07  0.0000E+00  1.2920E-02  1.0000E-04   
 mr-sdci # 64  2   -196.4725827954  4.3996E-10  0.0000E+00  1.4125E-04  1.0000E-04   
 mr-sdci # 64  3   -196.4725822679  4.5569E-08  0.0000E+00  1.0288E-03  1.0000E-04   
 mr-sdci # 64  4   -196.4725816635  3.4445E-06  0.0000E+00  1.5999E-03  1.0000E-04   
 mr-sdci # 64  5   -196.4644802824  2.0007E-08  8.3369E-07  1.4385E-03  1.0000E-04   
 mr-sdci # 64  6   -196.4644790174  2.0611E-08  0.0000E+00  2.7775E-03  1.0000E-04   
 mr-sdci # 64  7   -195.7727561526  1.7954E-03  0.0000E+00  2.5172E-01  1.0000E-04   
 mr-sdci # 64  8   -195.7504253884  3.4839E-03  0.0000E+00  3.2794E-01  1.0000E-04   
 mr-sdci # 64  9   -194.6036773507  1.1060E+00  0.0000E+00  2.4097E+00  1.0000E-04   
 mr-sdci # 64 10   -193.4474898723 -2.0853E+00  0.0000E+00  3.4012E+00  1.0000E-04   
 
 root number  5 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.005000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  65

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59960057
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426645
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426341
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616547
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616396
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88264424
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.85848916
   ht   9     0.00028057     0.01920532    -0.00430294     0.00486802    -0.00511075    -0.03038589    -0.00238239    -0.00088197
   ht  10    -0.01141583    -0.01804694     0.00547386    -0.00896178    -0.00306726     0.00167277    -0.00069235     0.00633635
   ht  11     0.00268143    -0.00527799     0.01110356    -0.01015611    -0.02748949     0.00314351    -0.00150574     0.00316788

                ht   9         ht  10         ht  11
   ht   9    -0.00006145
   ht  10     0.00001547    -0.00012831
   ht  11     0.00000280    -0.00000324    -0.00004319

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -3.322699E-05  -0.816579       0.288504      -4.187642E-02  -0.417165       0.118024      -0.107883      -3.088066E-02
 ref    2  -3.468921E-04  -6.123895E-02  -0.253831      -0.554704       0.204428       0.722623       3.208403E-02  -7.007185E-02

              v      9       v     10       v     11
 ref    1  -2.815643E-02  -1.910512E-02   5.483071E-02
 ref    2  -1.566942E-02   9.407096E-02   6.068160E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   1.214382E-07   0.670552       0.147665       0.309450       0.215817       0.536113       1.266823E-02   5.863679E-03

              v      9       v     10       v     11
 ref    1   1.038315E-03   9.214351E-03   6.688663E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00003323    -0.81657936     0.28850443    -0.04187642    -0.41716475     0.11802382    -0.10788348    -0.03088066
 ref:   2    -0.00034689    -0.06123895    -0.25383137    -0.55470384     0.20442754     0.72262257     0.03208403    -0.07007185

                ci   9         ci  10         ci  11
 ref:   1    -0.02815643    -0.01910512     0.05483071
 ref:   2    -0.01566942     0.09407096     0.06068160

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 65  1   -196.4879174676  2.6766E-09  0.0000E+00  1.2926E-02  1.0000E-04   
 mr-sdci # 65  2   -196.4725827954  2.7441E-11  0.0000E+00  1.4332E-04  1.0000E-04   
 mr-sdci # 65  3   -196.4725822697  1.7213E-09  0.0000E+00  1.0298E-03  1.0000E-04   
 mr-sdci # 65  4   -196.4725816801  1.6639E-08  0.0000E+00  1.5606E-03  1.0000E-04   
 mr-sdci # 65  5   -196.4644808441  5.6168E-07  0.0000E+00  8.6829E-04  1.0000E-04   
 mr-sdci # 65  6   -196.4644790187  1.3131E-09  1.9362E-06  2.7631E-03  1.0000E-04   
 mr-sdci # 65  7   -195.7733624251  6.0627E-04  0.0000E+00  2.4991E-01  1.0000E-04   
 mr-sdci # 65  8   -195.7509659227  5.4053E-04  0.0000E+00  3.2908E-01  1.0000E-04   
 mr-sdci # 65  9   -194.6878667041  8.4189E-02  0.0000E+00  2.3356E+00  1.0000E-04   
 mr-sdci # 65 10   -193.7123727811  2.6488E-01  0.0000E+00  3.1906E+00  1.0000E-04   
 mr-sdci # 65 11   -193.4387339230 -1.0592E+00  0.0000E+00  3.3911E+00  1.0000E-04   
 
 root number  6 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  66

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59960057
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426645
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426341
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616547
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616396
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88264424
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.85848916
   ht   9     0.00028057     0.01920532    -0.00430294     0.00486802    -0.00511075    -0.03038589    -0.00238239    -0.00088197
   ht  10    -0.01141583    -0.01804694     0.00547386    -0.00896178    -0.00306726     0.00167277    -0.00069235     0.00633635
   ht  11     0.00268143    -0.00527799     0.01110356    -0.01015611    -0.02748949     0.00314351    -0.00150574     0.00316788
   ht  12     0.02613861    -0.00406147     0.01585504    -0.00494006     0.00232700    -0.00880139    -0.00097372     0.00468564

                ht   9         ht  10         ht  11         ht  12
   ht   9    -0.00006145
   ht  10     0.00001547    -0.00012831
   ht  11     0.00000280    -0.00000324    -0.00004319
   ht  12    -0.00000334    -0.00000381    -0.00000291    -0.00006489

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   6.862337E-05  -0.816550       0.288950      -3.931313E-02   0.431418       4.285068E-02   0.107995       2.861868E-02
 ref    2   4.191304E-04  -6.115109E-02  -0.248578      -0.557103      -7.420621E-02   0.747277      -3.031707E-02   7.092779E-02

              v      9       v     10       v     11       v     12
 ref    1   3.208335E-02   8.543702E-03  -4.233347E-02   3.914780E-02
 ref    2   1.949747E-03  -9.356069E-02   2.418180E-03   6.536469E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   1.803794E-07   0.670493       0.145283       0.311909       0.191628       0.560260       1.258197E-02   5.849780E-03

              v      9       v     10       v     11       v     12
 ref    1   1.033143E-03   8.826598E-03   1.797970E-03   5.805093E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00006862    -0.81654976     0.28894995    -0.03931313     0.43141808     0.04285068     0.10799464     0.02861868
 ref:   2     0.00041913    -0.06115109    -0.24857773    -0.55710281    -0.07420621     0.74727747    -0.03031707     0.07092779

                ci   9         ci  10         ci  11         ci  12
 ref:   1     0.03208335     0.00854370    -0.04233347     0.03914780
 ref:   2     0.00194975    -0.09356069     0.00241818     0.06536469

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 66  1   -196.4879229886  5.5210E-06  3.6624E-05  1.2812E-02  1.0000E-04   
 mr-sdci # 66  2   -196.4725827954  1.5277E-12  0.0000E+00  1.4316E-04  1.0000E-04   
 mr-sdci # 66  3   -196.4725822707  1.0170E-09  0.0000E+00  1.0418E-03  1.0000E-04   
 mr-sdci # 66  4   -196.4725817091  2.8991E-08  0.0000E+00  1.5921E-03  1.0000E-04   
 mr-sdci # 66  5   -196.4644808532  9.0870E-09  0.0000E+00  8.7741E-04  1.0000E-04   
 mr-sdci # 66  6   -196.4644805987  1.5800E-06  0.0000E+00  1.2752E-03  1.0000E-04   
 mr-sdci # 66  7   -195.7740598576  6.9743E-04  0.0000E+00  2.4154E-01  1.0000E-04   
 mr-sdci # 66  8   -195.7512163865  2.5046E-04  0.0000E+00  3.2481E-01  1.0000E-04   
 mr-sdci # 66  9   -194.8248323033  1.3697E-01  0.0000E+00  2.1792E+00  1.0000E-04   
 mr-sdci # 66 10   -193.8617139284  1.4934E-01  0.0000E+00  3.1172E+00  1.0000E-04   
 mr-sdci # 66 11   -193.5197726818  8.1039E-02  0.0000E+00  3.5600E+00  1.0000E-04   
 mr-sdci # 66 12   -193.3696942433 -4.3173E-01  0.0000E+00  3.5490E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.013000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  67

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59960057
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426645
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426341
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616547
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616396
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88264424
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.85848916
   ht   9     0.00028057     0.01920532    -0.00430294     0.00486802    -0.00511075    -0.03038589    -0.00238239    -0.00088197
   ht  10    -0.01141583    -0.01804694     0.00547386    -0.00896178    -0.00306726     0.00167277    -0.00069235     0.00633635
   ht  11     0.00268143    -0.00527799     0.01110356    -0.01015611    -0.02748949     0.00314351    -0.00150574     0.00316788
   ht  12     0.02613861    -0.00406147     0.01585504    -0.00494006     0.00232700    -0.00880139    -0.00097372     0.00468564
   ht  13    -0.18637379    -0.03726210     0.05997949     0.00205525    -0.01262761    -0.09758718     0.00250721     0.01242034

                ht   9         ht  10         ht  11         ht  12         ht  13
   ht   9    -0.00006145
   ht  10     0.00001547    -0.00012831
   ht  11     0.00000280    -0.00000324    -0.00004319
   ht  12    -0.00000334    -0.00000381    -0.00000291    -0.00006489
   ht  13    -0.00002854    -0.00001050    -0.00000587     0.00003779    -0.00175481

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -5.799911E-05  -0.816335      -0.290690      -2.979418E-02  -0.433082       1.994958E-02  -0.107454      -3.052142E-02
 ref    2  -2.132441E-04  -6.084643E-02   0.228810      -0.565546       3.453967E-02   0.750152       3.033372E-02  -6.937455E-02

              v      9       v     10       v     11       v     12       v     13
 ref    1  -2.728623E-02  -2.451355E-02  -7.472292E-03   5.208529E-02  -1.872062E-02
 ref    2  -2.186204E-02   8.026847E-02  -8.365253E-02   3.264428E-02  -5.739081E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   4.883696E-08   0.670105       0.136855       0.320730       0.188753       0.563127       1.246645E-02   5.744386E-03

              v      9       v     10       v     11       v     12       v     13
 ref    1   1.222487E-03   7.043941E-03   7.053580E-03   3.778527E-03   3.644167E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00005800    -0.81633488    -0.29068994    -0.02979418    -0.43308184     0.01994958    -0.10745379    -0.03052142
 ref:   2    -0.00021324    -0.06084643     0.22880995    -0.56554629     0.03453967     0.75015247     0.03033372    -0.06937455

                ci   9         ci  10         ci  11         ci  12         ci  13
 ref:   1    -0.02728623    -0.02451355    -0.00747229     0.05208529    -0.01872062
 ref:   2    -0.02186204     0.08026847    -0.08365253     0.03264428    -0.05739081

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 67  1   -196.4879465548  2.3566E-05  0.0000E+00  5.7412E-03  1.0000E-04   
 mr-sdci # 67  2   -196.4725827954  1.5326E-11  4.5063E-09  1.4186E-04  1.0000E-04   
 mr-sdci # 67  3   -196.4725822780  7.3500E-09  0.0000E+00  1.0755E-03  1.0000E-04   
 mr-sdci # 67  4   -196.4725817581  4.9029E-08  0.0000E+00  1.6296E-03  1.0000E-04   
 mr-sdci # 67  5   -196.4644808572  4.0014E-09  0.0000E+00  8.9384E-04  1.0000E-04   
 mr-sdci # 67  6   -196.4644806378  3.9106E-08  0.0000E+00  1.2624E-03  1.0000E-04   
 mr-sdci # 67  7   -195.7745376998  4.7784E-04  0.0000E+00  2.3593E-01  1.0000E-04   
 mr-sdci # 67  8   -195.7515520851  3.3570E-04  0.0000E+00  3.2815E-01  1.0000E-04   
 mr-sdci # 67  9   -194.8670731310  4.2241E-02  0.0000E+00  2.0837E+00  1.0000E-04   
 mr-sdci # 67 10   -194.3535480303  4.9183E-01  0.0000E+00  2.8608E+00  1.0000E-04   
 mr-sdci # 67 11   -193.5672512098  4.7479E-02  0.0000E+00  3.3349E+00  1.0000E-04   
 mr-sdci # 67 12   -193.5118402963  1.4215E-01  0.0000E+00  3.4622E+00  1.0000E-04   
 mr-sdci # 67 13   -191.6516151535 -2.0205E+00  0.0000E+00  4.9622E+00  1.0000E-04   
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  68

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59960057
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426645
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426341
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616547
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616396
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88264424
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.85848916
   ht   9     0.00028057     0.01920532    -0.00430294     0.00486802    -0.00511075    -0.03038589    -0.00238239    -0.00088197
   ht  10    -0.01141583    -0.01804694     0.00547386    -0.00896178    -0.00306726     0.00167277    -0.00069235     0.00633635
   ht  11     0.00268143    -0.00527799     0.01110356    -0.01015611    -0.02748949     0.00314351    -0.00150574     0.00316788
   ht  12     0.02613861    -0.00406147     0.01585504    -0.00494006     0.00232700    -0.00880139    -0.00097372     0.00468564
   ht  13    -0.18637379    -0.03726210     0.05997949     0.00205525    -0.01262761    -0.09758718     0.00250721     0.01242034
   ht  14     0.00063386    -0.00061334    -0.00001377     0.00085747    -0.00097519     0.00014462     0.00011377     0.00009180

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14
   ht   9    -0.00006145
   ht  10     0.00001547    -0.00012831
   ht  11     0.00000280    -0.00000324    -0.00004319
   ht  12    -0.00000334    -0.00000381    -0.00000291    -0.00006489
   ht  13    -0.00002854    -0.00001050    -0.00000587     0.00003779    -0.00175481
   ht  14     0.00000009    -0.00000057    -0.00000028    -0.00000005     0.00000224    -0.00000014

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7         v:   8

   eig(s)   1.856583E-09   4.680297E-07   6.963644E-07   9.033147E-07   2.345275E-06   1.656623E-05    1.00000        1.00000    
 
   x:   1   1.595407E-05   7.312147E-05   2.253123E-04  -5.923358E-04  -4.126373E-04   3.648961E-03  -1.683957E-02   7.510336E-02
   x:   2  -1.258315E-05  -1.249201E-04  -3.288943E-04  -5.862907E-05  -4.350416E-04   7.175788E-04  -5.766889E-02  -0.314034    
   x:   3   2.645219E-06   2.480562E-04   1.097223E-04  -2.354651E-04   2.075621E-04  -1.182899E-03   7.614397E-02  -9.902587E-02
   x:   4   1.564309E-05  -2.129666E-04  -9.416395E-05   4.814261E-05  -1.821719E-04  -4.937862E-05   0.139705      -0.207678    
   x:   5  -2.253944E-05  -5.400145E-04   1.324213E-04  -6.504286E-05  -4.414234E-05   2.414102E-04   2.857964E-02  -3.008360E-02
   x:   6   3.624412E-06   5.040320E-05   5.832478E-04   2.754651E-04  -5.370442E-05   1.922953E-03   5.232572E-02  -0.195590    
   x:   7   2.086347E-06  -3.185149E-05   4.282807E-05   2.764124E-05  -8.293570E-06  -5.177197E-05  -0.967957       0.128463    
   x:   8   2.114782E-06   7.515781E-05   1.841373E-05  -6.889740E-05   1.506666E-04  -2.411217E-04  -0.174873      -0.887211    
   x:   9  -2.543325E-03  -8.363562E-03  -0.967145      -0.238915      -8.547239E-02   1.297986E-02  -5.783634E-13  -2.353941E-13
   x:  10  -4.885203E-03   2.842651E-02  -0.102423       6.304881E-02   0.990114       6.615471E-02  -1.203483E-13  -4.072176E-13
   x:  11   7.351871E-03   0.997004      -2.156129E-02   6.499736E-02  -3.511944E-02   2.423356E-03  -4.502403E-13   4.276262E-14
   x:  12   4.677511E-03   7.099828E-02   0.230360      -0.966551       8.479476E-02  -2.143141E-02   3.728932E-13  -4.685491E-14
   x:  13   1.264409E-04   2.668796E-03  -2.438031E-02   2.199807E-02   6.264697E-02  -0.997482       6.278711E-14   2.712193E-14
   x:  14   0.999947      -7.545092E-03  -3.876246E-03   3.740994E-03   4.473408E-03   5.647270E-04   3.191433E-12    0.00000    

                v:   9         v:  10         v:  11         v:  12         v:  13         v:  14

   eig(s)    1.00000        1.00000        1.00000        1.00000        1.00000        1.00002    
 
   x:   1  -0.142073       4.381329E-02  -0.239726      -0.443487       0.114854      -0.839415    
   x:   2   0.479363      -0.362365       0.355381       4.261853E-02   0.617347      -0.166523    
   x:   3   0.150531      -0.672268      -0.359402      -0.502377      -0.241962       0.263994    
   x:   4  -0.794462      -0.402430      -2.510400E-02   0.246368       0.287809       8.461734E-03
   x:   5   0.259292       1.582264E-02  -0.793877       0.506447       0.201270      -5.963418E-02
   x:   6   0.119712      -0.321583       0.198942       0.448805      -0.639718      -0.437050    
   x:   7  -0.103456      -0.169743      -3.940942E-02   6.938820E-02  -2.352933E-02   1.093781E-02
   x:   8  -6.269732E-02   0.346986      -0.122737      -0.160291      -0.118358       5.639356E-02
   x:   9   1.097049E-07  -7.456452E-05  -1.265251E-04   2.411038E-04  -6.505477E-04  -1.775136E-04
   x:  10   2.802212E-07  -1.537005E-04   6.768266E-05   5.076684E-05   3.716050E-04  -2.721154E-04
   x:  11  -6.692591E-07   2.773779E-05  -3.135400E-04   4.474742E-04   3.252538E-04  -3.767176E-05
   x:  12  -2.513563E-08   2.715242E-05   3.445224E-04   4.852124E-04  -1.571781E-05   2.598383E-04
   x:  13   3.230793E-08   1.388140E-05   2.392741E-05   1.197265E-05  -4.355173E-07  -4.401211E-03
   x:  14   2.609510E-05   2.684328E-06  -7.933989E-06   1.037011E-05   7.071636E-06   8.401545E-06
 bummer (warning):overlap matrix: # small eigenvalues=                      1

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   5.411851E-05  -0.817937       0.286573       2.542119E-02  -0.433161      -1.815457E-02  -0.110091       1.269626E-02
 ref    2   2.077427E-04  -6.156478E-02  -0.225950       0.566618       3.143060E-02  -0.750289       2.028139E-02   7.209788E-02

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1  -2.325110E-02  -2.107962E-02  -6.138594E-03  -4.507948E-02  -4.998220E-02   1.241842E-03
 ref    2  -1.529826E-02   8.486996E-02  -5.757888E-02  -7.062551E-02  -6.738389E-03   5.213196E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   4.608586E-08   0.672811       0.133178       0.321702       0.188616       0.563263       1.253126E-02   5.359300E-03

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1   7.746503E-04   7.647260E-03   3.353010E-03   7.020123E-03   2.543626E-03   2.719284E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00005412    -0.81793680     0.28657347     0.02542119    -0.43316095    -0.01815457    -0.11009054     0.01269626
 ref:   2     0.00020774    -0.06156478    -0.22595032     0.56661764     0.03143060    -0.75028904     0.02028139     0.07209788

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14
 ref:   1    -0.02325110    -0.02107962    -0.00613859    -0.04507948    -0.04998220     0.00124184
 ref:   2    -0.01529826     0.08486996    -0.05757888    -0.07062551    -0.00673839     0.05213196

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 68  1   -196.4879468143  2.5947E-07  0.0000E+00  5.8203E-03  1.0000E-04   
 mr-sdci # 68  2   -196.4725827987  3.2429E-09  0.0000E+00  4.6918E-05  1.0000E-04   
 mr-sdci # 68  3   -196.4725822803  2.2599E-09  4.5030E-07  1.0835E-03  1.0000E-04   
 mr-sdci # 68  4   -196.4725817619  3.7615E-09  0.0000E+00  1.6128E-03  1.0000E-04   
 mr-sdci # 68  5   -196.4644808589  1.6952E-09  0.0000E+00  8.8152E-04  1.0000E-04   
 mr-sdci # 68  6   -196.4644806383  4.9028E-10  0.0000E+00  1.2617E-03  1.0000E-04   
 mr-sdci # 68  7   -195.7772185977  2.6809E-03  0.0000E+00  2.2631E-01  1.0000E-04   
 mr-sdci # 68  8   -195.7559138332  4.3617E-03  0.0000E+00  2.8878E-01  1.0000E-04   
 mr-sdci # 68  9   -194.8890818992  2.2009E-02  0.0000E+00  2.0128E+00  1.0000E-04   
 mr-sdci # 68 10   -194.3616175651  8.0695E-03  0.0000E+00  2.8269E+00  1.0000E-04   
 mr-sdci # 68 11   -193.7762467153  2.0900E-01  0.0000E+00  3.0349E+00  1.0000E-04   
 mr-sdci # 68 12   -193.5293977694  1.7557E-02  0.0000E+00  3.3566E+00  1.0000E-04   
 mr-sdci # 68 13   -192.8817925265  1.2302E+00  0.0000E+00  3.9718E+00  1.0000E-04   
 mr-sdci # 68 14   -191.3871964578 -5.3101E-01  0.0000E+00  5.1670E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.008000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  69

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59960057
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426645
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426341
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616547
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616396
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88264424
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.85848916
   ht   9     0.00028057     0.01920532    -0.00430294     0.00486802    -0.00511075    -0.03038589    -0.00238239    -0.00088197
   ht  10    -0.01141583    -0.01804694     0.00547386    -0.00896178    -0.00306726     0.00167277    -0.00069235     0.00633635
   ht  11     0.00268143    -0.00527799     0.01110356    -0.01015611    -0.02748949     0.00314351    -0.00150574     0.00316788
   ht  12     0.02613861    -0.00406147     0.01585504    -0.00494006     0.00232700    -0.00880139    -0.00097372     0.00468564
   ht  13    -0.18637379    -0.03726210     0.05997949     0.00205525    -0.01262761    -0.09758718     0.00250721     0.01242034
   ht  14     0.00063386    -0.00061334    -0.00001377     0.00085747    -0.00097519     0.00014462     0.00011377     0.00009180
   ht  15    -0.01067766    -0.00582093     0.00546240     0.00059492     0.00583275     0.01686532     0.00054110     0.00155084

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15
   ht   9    -0.00006145
   ht  10     0.00001547    -0.00012831
   ht  11     0.00000280    -0.00000324    -0.00004319
   ht  12    -0.00000334    -0.00000381    -0.00000291    -0.00006489
   ht  13    -0.00002854    -0.00001050    -0.00000587     0.00003779    -0.00175481
   ht  14     0.00000009    -0.00000057    -0.00000028    -0.00000005     0.00000224    -0.00000014
   ht  15     0.00000942    -0.00000704     0.00000165     0.00000396    -0.00000601     0.00000024    -0.00002214

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7         v:   8

   eig(s)   1.821883E-09   2.414846E-07   4.687158E-07   7.031270E-07   9.080136E-07   2.345376E-06   1.656840E-05    1.00000    
 
   x:   1   1.278201E-05  -2.642297E-04  -8.753663E-05  -2.734943E-04   5.633380E-04  -4.143336E-04   3.646297E-03   4.286631E-03
   x:   2  -1.442016E-05  -1.703819E-04   1.135062E-04   3.108908E-04   5.595837E-05  -4.357995E-04   7.162174E-04  -0.141147    
   x:   3   4.093981E-06   1.264706E-04  -2.413424E-04  -1.038078E-04   2.417051E-04   2.084431E-04  -1.181571E-03   4.624272E-02
   x:   4   1.567745E-05  -6.090182E-06   2.124483E-04   9.805667E-05  -4.409238E-05  -1.820615E-04  -4.923822E-05   7.775350E-02
   x:   5  -2.128655E-05   9.255044E-05   5.465686E-04  -1.164947E-04   7.277398E-05  -4.334834E-05   2.427244E-04   1.928916E-02
   x:   6   8.124818E-06   4.039962E-04  -2.395456E-05  -5.327353E-04  -2.611424E-04  -5.177006E-05   1.926631E-03  -3.013974E-03
   x:   7   2.274679E-06   1.707590E-05   3.317873E-05  -3.999652E-05  -2.743274E-05  -8.228693E-06  -5.164575E-05  -0.896202    
   x:   8   2.497894E-06   3.304035E-05  -7.350854E-05  -1.712646E-05   7.078699E-05   1.509113E-04  -2.407458E-04  -0.410263    
   x:   9  -3.645424E-03  -0.137136      -6.059089E-03   0.951925       0.259918      -8.517891E-02   1.302702E-02   3.024989E-12
   x:  10  -5.010734E-03  -1.317675E-02  -2.976439E-02   0.102369      -6.147994E-02   0.990090       6.616336E-02   2.571932E-12
   x:  11   7.704662E-03   5.809948E-02  -0.995280       1.709342E-02  -6.669135E-02  -3.517805E-02   2.411950E-03   5.907554E-14
   x:  12   4.217311E-03  -4.748943E-02  -7.442322E-02  -0.260797       0.957335       8.499274E-02  -2.140005E-02   4.459438E-14
   x:  13   2.501941E-04   9.891232E-03  -2.237400E-03   2.624417E-02  -2.039959E-02   6.273710E-02  -0.997415      -3.323357E-13
   x:  14   0.999871      -1.260179E-02   7.169747E-03   3.515539E-03  -3.898556E-03   4.464886E-03   5.627961E-04   1.624967E-12
   x:  15   1.193370E-02   0.987488       5.385409E-02   0.119798       8.539344E-02   6.967862E-03   1.151921E-02   3.322135E-12

                v:   9         v:  10         v:  11         v:  12         v:  13         v:  14         v:  15

   eig(s)    1.00000        1.00000        1.00000        1.00000        1.00000        1.00000        1.00002    
 
   x:   1   8.296916E-02  -0.112528       4.933106E-02   0.257891      -0.381169       0.246156      -0.839999    
   x:   2  -0.265686       0.397027      -0.523304      -0.316232       0.254557       0.534171      -0.166942    
   x:   3  -7.773996E-02  -5.926728E-03  -0.695246       0.395364      -0.511757      -0.142599       0.264332    
   x:   4   0.797319      -0.309835      -0.342867       3.302741E-02   0.336955       0.173145       8.498558E-03
   x:   5  -0.203591       0.147534       2.660102E-02   0.779999       0.568987      -7.636948E-03  -5.916941E-02
   x:   6  -3.346691E-03   0.118383      -0.307782      -0.224065       0.234093      -0.772315      -0.435610    
   x:   7  -0.114668      -0.418468      -5.307231E-02   3.326830E-02   5.536419E-02  -3.806276E-02   1.097933E-02
   x:   8   0.475559       0.723034       0.156654       0.127960      -0.181291      -7.600203E-02   5.649729E-02
   x:   9  -2.436083E-06  -4.173100E-05  -9.652566E-06   9.716071E-05   2.705142E-05  -7.009206E-04  -1.764101E-04
   x:  10  -1.846733E-06  -3.265314E-05  -1.702935E-04  -5.288443E-05   1.708892E-04   3.265211E-04  -2.724588E-04
   x:  11   5.322965E-07   4.672154E-08   4.845684E-05   3.038539E-04   5.347855E-04   1.563173E-04  -3.759364E-05
   x:  12  -7.384898E-08  -4.623338E-07   4.640227E-05  -3.656010E-04   4.432923E-04  -1.502831E-04   2.602241E-04
   x:  13   2.103341E-07   4.511157E-06   1.113826E-05  -2.405857E-05   1.236024E-05  -8.937970E-06  -4.401206E-03
   x:  14  -2.229691E-05   1.363019E-05  -1.059132E-07   7.865169E-06   1.250863E-05   2.697698E-06   8.407814E-06
   x:  15  -2.930123E-06  -4.810106E-05   1.245925E-04  -4.461181E-05  -1.385135E-04   3.879629E-04  -7.490644E-05
 bummer (warning):overlap matrix: # small eigenvalues=                      1

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -2.473682E-05  -0.820538       0.280028      -9.960595E-03  -0.433197       1.726255E-02   0.110233       1.280662E-02
 ref    2  -1.625093E-04  -6.020814E-02  -0.155369       0.590025       2.988661E-02   0.750358      -1.979354E-02   7.181492E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1   8.180251E-03  -2.773995E-02   1.616433E-02  -1.042336E-02   5.656521E-02  -2.105565E-02   5.216973E-02
 ref    2   3.509958E-02  -4.695947E-03  -8.772224E-02  -6.438512E-02   1.771240E-02  -8.586628E-02   7.985227E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   2.702120E-08   0.676908       0.102555       0.348229       0.188553       0.563334       1.254305E-02   5.321393E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1   1.298897E-03   7.915569E-04   7.956477E-03   4.254090E-03   3.513352E-03   7.816358E-03   9.098066E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00002474    -0.82053795     0.28002844    -0.00996060    -0.43319736     0.01726255     0.11023279     0.01280662
 ref:   2    -0.00016251    -0.06020814    -0.15536868     0.59002526     0.02988661     0.75035750    -0.01979354     0.07181492

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15
 ref:   1     0.00818025    -0.02773995     0.01616433    -0.01042336     0.05656521    -0.02105565     0.05216973
 ref:   2     0.03509958    -0.00469595    -0.08772224    -0.06438512     0.01771240    -0.08586628     0.07985227

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 69  1   -196.4879478392  1.0250E-06  0.0000E+00  6.0902E-03  1.0000E-04   
 mr-sdci # 69  2   -196.4725827987  8.6828E-12  0.0000E+00  4.6363E-05  1.0000E-04   
 mr-sdci # 69  3   -196.4725827125  4.3223E-07  0.0000E+00  5.7015E-04  1.0000E-04   
 mr-sdci # 69  4   -196.4725817787  1.6803E-08  9.1177E-07  1.5567E-03  1.0000E-04   
 mr-sdci # 69  5   -196.4644808589  1.7074E-11  0.0000E+00  8.8111E-04  1.0000E-04   
 mr-sdci # 69  6   -196.4644806497  1.1463E-08  0.0000E+00  1.2388E-03  1.0000E-04   
 mr-sdci # 69  7   -195.7773035595  8.4962E-05  0.0000E+00  2.2875E-01  1.0000E-04   
 mr-sdci # 69  8   -195.7559277431  1.3910E-05  0.0000E+00  2.8992E-01  1.0000E-04   
 mr-sdci # 69  9   -195.4338634695  5.4478E-01  0.0000E+00  1.2925E+00  1.0000E-04   
 mr-sdci # 69 10   -194.7868772770  4.2526E-01  0.0000E+00  2.1073E+00  1.0000E-04   
 mr-sdci # 69 11   -194.3476876758  5.7144E-01  0.0000E+00  2.8511E+00  1.0000E-04   
 mr-sdci # 69 12   -193.7732374536  2.4384E-01  0.0000E+00  3.0019E+00  1.0000E-04   
 mr-sdci # 69 13   -192.8911915156  9.3990E-03  0.0000E+00  3.9172E+00  1.0000E-04   
 mr-sdci # 69 14   -191.4263332988  3.9137E-02  0.0000E+00  4.8511E+00  1.0000E-04   
 mr-sdci # 69 15   -191.1014578915  1.6592E-01  0.0000E+00  4.3632E+00  1.0000E-04   
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.008000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  70

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59960057
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426645
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426341
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616547
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616396
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88264424
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.85848916
   ht   9     0.00028057     0.01920532    -0.00430294     0.00486802    -0.00511075    -0.03038589    -0.00238239    -0.00088197
   ht  10    -0.01141583    -0.01804694     0.00547386    -0.00896178    -0.00306726     0.00167277    -0.00069235     0.00633635
   ht  11     0.00268143    -0.00527799     0.01110356    -0.01015611    -0.02748949     0.00314351    -0.00150574     0.00316788
   ht  12     0.02613861    -0.00406147     0.01585504    -0.00494006     0.00232700    -0.00880139    -0.00097372     0.00468564
   ht  13    -0.18637379    -0.03726210     0.05997949     0.00205525    -0.01262761    -0.09758718     0.00250721     0.01242034
   ht  14     0.00063386    -0.00061334    -0.00001377     0.00085747    -0.00097519     0.00014462     0.00011377     0.00009180
   ht  15    -0.01067766    -0.00582093     0.00546240     0.00059492     0.00583275     0.01686532     0.00054110     0.00155084
   ht  16    -0.01173759     0.01722239     0.00065112     0.01503974    -0.00325270    -0.00560727    -0.00200090    -0.00105329

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15         ht  16
   ht   9    -0.00006145
   ht  10     0.00001547    -0.00012831
   ht  11     0.00000280    -0.00000324    -0.00004319
   ht  12    -0.00000334    -0.00000381    -0.00000291    -0.00006489
   ht  13    -0.00002854    -0.00001050    -0.00000587     0.00003779    -0.00175481
   ht  14     0.00000009    -0.00000057    -0.00000028    -0.00000005     0.00000224    -0.00000014
   ht  15     0.00000942    -0.00000704     0.00000165     0.00000396    -0.00000601     0.00000024    -0.00002214
   ht  16    -0.00000796     0.00000140     0.00000316     0.00000391    -0.00003897     0.00000006     0.00000093    -0.00003879

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7         v:   8

   eig(s)   1.821535E-09   2.401044E-07   4.561652E-07   4.962998E-07   7.260783E-07   9.151579E-07   2.349148E-06   1.656863E-05
 
   x:   1   1.301087E-05  -2.398258E-04  -2.816349E-04  -2.722663E-04  -1.420490E-04   5.394252E-04   4.237395E-04   3.645395E-03
   x:   2  -1.475438E-05  -1.998497E-04   3.196505E-04   2.835015E-04   1.957474E-04   8.904634E-05   4.206847E-04   7.174855E-04
   x:   3   4.123069E-06   1.295416E-04  -2.210880E-04   9.576007E-05  -8.676330E-05   2.466477E-04  -2.098090E-04  -1.181503E-03
   x:   4   1.540517E-05  -3.110307E-05   3.555365E-04   1.499598E-04   1.586529E-06  -7.548616E-06   1.687398E-04  -4.811969E-05
   x:   5  -2.123311E-05   9.523443E-05   4.025751E-04  -3.863509E-04  -7.366900E-05   6.593212E-05   4.614189E-05   2.424790E-04
   x:   6   8.282326E-06   4.161045E-04  -1.288036E-04  -1.722560E-04  -4.984323E-04  -2.477707E-04   5.631472E-05   1.926204E-03
   x:   7   2.309604E-06   1.999715E-05   3.100986E-06  -5.610907E-05  -2.791425E-05  -3.075634E-05   9.991926E-06  -5.179605E-05
   x:   8   2.528869E-06   3.592754E-05  -8.067957E-05   1.021226E-05  -3.806215E-06   6.747089E-05  -1.500421E-04  -2.408190E-04
   x:   9  -3.725990E-03  -0.146337       0.121717       0.230879       0.925891       0.213319       8.661404E-02   1.301136E-02
   x:  10  -4.992783E-03  -1.159141E-02  -2.893677E-02   1.481294E-02   0.106712      -7.330486E-02  -0.988758       6.618231E-02
   x:  11   7.737899E-03   6.355244E-02  -0.816839       0.568887      -1.379521E-02  -6.030227E-02   3.478763E-02   2.414895E-03
   x:  12   4.287513E-03  -3.821781E-02  -0.164400      -0.128772      -0.164813       0.959020      -8.701040E-02  -2.138137E-02
   x:  13   2.464919E-04   9.371550E-03   4.419294E-03   1.166103E-02   2.279414E-02  -2.131941E-02  -6.278512E-02  -0.997407    
   x:  14   0.999869      -1.278922E-02   6.533929E-03  -2.875243E-03   3.209586E-03  -4.085073E-03  -4.446327E-03   5.627883E-04
   x:  15   1.199050E-02   0.983532       0.103560       5.046662E-02   0.110847       8.232035E-02  -7.233952E-03   1.152235E-02
   x:  16  -8.351228E-04  -7.323910E-02   0.528496       0.776897      -0.301939       0.136076      -4.538719E-02   3.785241E-03

                v:   9         v:  10         v:  11         v:  12         v:  13         v:  14         v:  15         v:  16

   eig(s)    1.00000        1.00000        1.00000        1.00000        1.00000        1.00000        1.00000        1.00002    
 
   x:   1   5.206516E-03   0.148648       1.082190E-02   5.372726E-02   0.215833       0.451036      -0.134539       0.840679    
   x:   2   0.110699      -0.545611      -0.106297       0.297506      -0.314861      -0.201873      -0.650962       0.163107    
   x:   3  -5.394645E-02  -0.105698      -0.220334       0.784864       0.134786       0.436497       0.207419      -0.263892    
   x:   4   2.031312E-02   0.713140       0.268124       0.449882      -4.069969E-02  -0.346305      -0.308250      -1.151023E-02
   x:   5  -4.374626E-02  -0.231249      -2.294717E-02   9.378184E-02   0.849482      -0.457895      -2.093393E-02   5.968498E-02
   x:   6   3.853363E-03  -0.124284      -9.699100E-03   0.269146      -0.325176      -0.457591       0.637835       0.435933    
   x:   7   0.873735       0.168665      -0.445652      -4.196532E-02   6.036990E-02  -4.627494E-02   4.331155E-02  -1.055605E-02
   x:   8   0.468033      -0.251200       0.817876       8.486754E-02   5.874870E-02   0.155427       0.104685      -5.615113E-02
   x:   9  -5.857639E-07   3.419705E-05  -1.894524E-05   8.088966E-05   2.813583E-05  -1.760227E-04   6.802400E-04   1.784625E-04
   x:  10   1.668592E-07   1.689042E-06  -7.760698E-05   9.818598E-05  -2.983446E-05  -1.122355E-04  -3.748562E-04   2.706453E-04
   x:  11  -4.295234E-08   4.919066E-06   1.247585E-05  -2.625593E-05   3.979460E-04  -4.421384E-04  -2.246230E-04   3.670698E-05
   x:  12   5.766279E-08  -2.005987E-06  -4.824090E-06  -1.721739E-04  -2.830675E-04  -4.938821E-04   2.504912E-05  -2.613076E-04
   x:  13   5.411023E-09  -1.679778E-06   7.046617E-06  -8.041461E-06  -2.795710E-05  -2.408927E-05  -1.127084E-05   4.401132E-03
   x:  14  -2.678640E-06  -2.462824E-05  -6.901288E-06  -3.496324E-06   1.127835E-05  -9.800537E-06  -3.432453E-06  -8.404187E-06
   x:  15  -8.256829E-07   4.566652E-05  -3.903949E-06  -1.472081E-04   3.227949E-06   2.299750E-04  -3.360655E-04   7.441389E-05
   x:  16   4.629858E-07  -1.766446E-05  -4.121586E-05  -1.967011E-04   1.898574E-04   1.919550E-04   3.527074E-04   1.968495E-04
 bummer (warning):overlap matrix: # small eigenvalues=                      1

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   5.436951E-05  -0.819857      -0.260318       0.108940      -0.433504      -5.546627E-03   0.106469       3.235505E-02
 ref    2   1.308321E-04  -6.654629E-02   0.401497       0.458569      -9.604893E-03   0.750876      -2.715679E-02   6.343090E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   1.703187E-02  -6.390982E-03   2.627659E-02  -1.758242E-02  -4.994773E-02  -5.649699E-02  -2.388972E-02  -7.841099E-02
 ref    2  -2.859104E-02  -3.928868E-02  -8.989354E-02   3.018933E-02  -1.329042E-02  -2.460377E-02  -2.256700E-02  -0.131529    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   2.007309E-08   0.676594       0.228966       0.222153       0.188018       0.563846       1.207313E-02   5.070329E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   1.107532E-03   1.584445E-03   8.771308E-03   1.220537E-03   2.671411E-03   3.797255E-03   1.079988E-03   2.344821E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15        ev   16

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00005437    -0.81985677    -0.26031827     0.10893976    -0.43350379    -0.00554663     0.10646894     0.03235505
 ref:   2     0.00013083    -0.06654629     0.40149715     0.45856858    -0.00960489     0.75087623    -0.02715679     0.06343090

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1     0.01703187    -0.00639098     0.02627659    -0.01758242    -0.04994773    -0.05649699    -0.02388972    -0.07841099
 ref:   2    -0.02859104    -0.03928868    -0.08989354     0.03018933    -0.01329042    -0.02460377    -0.02256700    -0.13152919

 trial vector basis is being transformed.  new dimension:   8

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 70  1   -196.4879484273  5.8808E-07  0.0000E+00  5.8532E-03  1.0000E-04   
 mr-sdci # 70  2   -196.4725827987  1.2342E-11  0.0000E+00  4.4542E-05  1.0000E-04   
 mr-sdci # 70  3   -196.4725827199  7.3621E-09  0.0000E+00  5.6139E-04  1.0000E-04   
 mr-sdci # 70  4   -196.4725826839  9.0522E-07  0.0000E+00  7.0764E-04  1.0000E-04   
 mr-sdci # 70  5   -196.4644808612  2.2871E-09  2.5447E-07  8.9148E-04  1.0000E-04   
 mr-sdci # 70  6   -196.4644806924  4.2660E-08  0.0000E+00  1.1522E-03  1.0000E-04   
 mr-sdci # 70  7   -195.7775764264  2.7287E-04  0.0000E+00  2.3202E-01  1.0000E-04   
 mr-sdci # 70  8   -195.7674992919  1.1572E-02  0.0000E+00  3.1151E-01  1.0000E-04   
 mr-sdci # 70  9   -195.6327177908  1.9885E-01  0.0000E+00  7.7896E-01  1.0000E-04   
 mr-sdci # 70 10   -195.4268658093  6.3999E-01  0.0000E+00  1.2931E+00  1.0000E-04   
 mr-sdci # 70 11   -194.3691121017  2.1424E-02  0.0000E+00  2.7929E+00  1.0000E-04   
 mr-sdci # 70 12   -194.0399886814  2.6675E-01  0.0000E+00  2.6556E+00  1.0000E-04   
 mr-sdci # 70 13   -192.8940014729  2.8100E-03  0.0000E+00  3.9053E+00  1.0000E-04   
 mr-sdci # 70 14   -192.6899845906  1.2637E+00  0.0000E+00  3.1653E+00  1.0000E-04   
 mr-sdci # 70 15   -191.1335065551  3.2049E-02  0.0000E+00  5.0454E+00  1.0000E-04   
 mr-sdci # 70 16   -190.9166613983  1.5004E-01  0.0000E+00  4.3772E+00  1.0000E-04   
 
 root number  5 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.013000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  71

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59963363
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426792
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426789
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616607
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616590
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88926163
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.87918450
   ht   9    -0.00015542     0.00119968     0.00025171     0.00287431     0.01008965     0.00927194    -0.00054389     0.00019795

                ht   9
   ht   9    -0.00000991

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   4.673515E-05   0.820355       0.263163      -9.778345E-02   0.432699       2.685980E-02   0.105335      -3.715244E-02
 ref    2   1.478153E-04   6.650157E-02  -0.383376      -0.473829       4.653294E-02  -0.749496      -3.101458E-02  -6.250257E-02

              v      9
 ref    1   2.595952E-02
 ref    2  -4.009572E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   2.403353E-08   0.677405       0.216232       0.234075       0.189394       0.562465       1.205730E-02   5.286875E-03

              v      9
 ref    1   2.281563E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00004674     0.82035502     0.26316257    -0.09778345     0.43269886     0.02685980     0.10533471    -0.03715244
 ref:   2     0.00014782     0.06650157    -0.38337618    -0.47382851     0.04653294    -0.74949580    -0.03101458    -0.06250257

                ci   9
 ref:   1     0.02595952
 ref:   2    -0.04009572

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 71  1   -196.4879487944  3.6709E-07  0.0000E+00  5.7247E-03  1.0000E-04   
 mr-sdci # 71  2   -196.4725827987  1.0438E-11  0.0000E+00  4.4065E-05  1.0000E-04   
 mr-sdci # 71  3   -196.4725827211  1.2365E-09  0.0000E+00  5.6068E-04  1.0000E-04   
 mr-sdci # 71  4   -196.4725826855  1.5821E-09  0.0000E+00  7.0075E-04  1.0000E-04   
 mr-sdci # 71  5   -196.4644810450  1.8386E-07  0.0000E+00  4.6595E-04  1.0000E-04   
 mr-sdci # 71  6   -196.4644806932  7.8101E-10  3.7736E-07  1.1337E-03  1.0000E-04   
 mr-sdci # 71  7   -195.7784360119  8.5959E-04  0.0000E+00  2.2242E-01  1.0000E-04   
 mr-sdci # 71  8   -195.7677900936  2.9080E-04  0.0000E+00  3.1508E-01  1.0000E-04   
 mr-sdci # 71  9   -193.6419410121 -1.9908E+00  0.0000E+00  3.4033E+00  1.0000E-04   
 
 root number  6 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  72

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59963363
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426792
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426789
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616607
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616590
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88926163
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.87918450
   ht   9    -0.00015542     0.00119968     0.00025171     0.00287431     0.01008965     0.00927194    -0.00054389     0.00019795
   ht  10    -0.00438833     0.00767832    -0.01648267    -0.00111488    -0.00434376     0.00410727     0.00007434    -0.00078594

                ht   9         ht  10
   ht   9    -0.00000991
   ht  10     0.00000092    -0.00001627

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -4.562220E-05   0.819955      -0.257726      -0.114233      -0.430073       5.465328E-02   0.105313      -3.720921E-02
 ref    2  -1.344613E-04   6.914181E-02   0.416882      -0.444230      -9.467381E-02  -0.744935      -3.107372E-02  -6.281305E-02

              v      9       v     10
 ref    1  -1.653402E-02   2.048625E-02
 ref    2   5.590750E-02   2.149131E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   2.016123E-08   0.677107       0.240214       0.210389       0.193926       0.557915       1.205632E-02   5.330005E-03

              v      9       v     10
 ref    1   3.399023E-03   4.243053E-04

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00004562     0.81995515    -0.25772640    -0.11423290    -0.43007300     0.05465328     0.10531259    -0.03720921
 ref:   2    -0.00013446     0.06914181     0.41688217    -0.44422959    -0.09467381    -0.74493524    -0.03107372    -0.06281305

                ci   9         ci  10
 ref:   1    -0.01653402     0.02048625
 ref:   2     0.05590750     0.00214913

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 72  1   -196.4879497544  9.6006E-07  8.4969E-06  5.4605E-03  1.0000E-04   
 mr-sdci # 72  2   -196.4725827987  3.1577E-11  0.0000E+00  4.0549E-05  1.0000E-04   
 mr-sdci # 72  3   -196.4725827226  1.5263E-09  0.0000E+00  5.6446E-04  1.0000E-04   
 mr-sdci # 72  4   -196.4725826896  4.1437E-09  0.0000E+00  7.1714E-04  1.0000E-04   
 mr-sdci # 72  5   -196.4644810454  3.6956E-10  0.0000E+00  4.5964E-04  1.0000E-04   
 mr-sdci # 72  6   -196.4644809742  2.8096E-07  0.0000E+00  6.5822E-04  1.0000E-04   
 mr-sdci # 72  7   -195.7784364255  4.1359E-07  0.0000E+00  2.2211E-01  1.0000E-04   
 mr-sdci # 72  8   -195.7678900868  9.9993E-05  0.0000E+00  3.1513E-01  1.0000E-04   
 mr-sdci # 72  9   -194.0293876564  3.8745E-01  0.0000E+00  3.3124E+00  1.0000E-04   
 mr-sdci # 72 10   -193.1675348038 -2.2593E+00  0.0000E+00  3.8851E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  73

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59963363
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426792
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426789
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616607
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616590
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88926163
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.87918450
   ht   9    -0.00015542     0.00119968     0.00025171     0.00287431     0.01008965     0.00927194    -0.00054389     0.00019795
   ht  10    -0.00438833     0.00767832    -0.01648267    -0.00111488    -0.00434376     0.00410727     0.00007434    -0.00078594
   ht  11     0.04688178     0.01511393     0.01152200    -0.01535281    -0.03206176     0.02638525    -0.00078938    -0.00099639

                ht   9         ht  10         ht  11
   ht   9    -0.00000991
   ht  10     0.00000092    -0.00001627
   ht  11    -0.00000021    -0.00000249    -0.00028097

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -2.543590E-05   0.820041      -0.263707      -9.894133E-02   0.427862      -6.988316E-02  -0.105393      -3.716424E-02
 ref    2  -1.236692E-04   6.904057E-02   0.390231      -0.467830       0.121053       0.741105       3.093798E-02  -6.283413E-02

              v      9       v     10       v     11
 ref    1  -5.650468E-03  -2.551809E-02  -7.195251E-03
 ref    2   5.398555E-02   1.879792E-02   2.474681E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   1.594107E-08   0.677235       0.221822       0.228654       0.197720       0.554120       1.206483E-02   5.329308E-03

              v      9       v     10       v     11
 ref    1   2.946367E-03   1.004535E-03   5.789569E-05

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00002544     0.82004141    -0.26370727    -0.09894133     0.42786217    -0.06988316    -0.10539294    -0.03716424
 ref:   2    -0.00012367     0.06904057     0.39023067    -0.46782968     0.12105276     0.74110481     0.03093798    -0.06283413

                ci   9         ci  10         ci  11
 ref:   1    -0.00565047    -0.02551809    -0.00719525
 ref:   2     0.05398555     0.01879792     0.00247468

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 73  1   -196.4879552904  5.5360E-06  0.0000E+00  3.0480E-03  1.0000E-04   
 mr-sdci # 73  2   -196.4725827987  1.2079E-13  0.0000E+00  4.0600E-05  1.0000E-04   
 mr-sdci # 73  3   -196.4725827312  8.5610E-09  7.0792E-08  5.1765E-04  1.0000E-04   
 mr-sdci # 73  4   -196.4725826902  5.4050E-10  0.0000E+00  7.1822E-04  1.0000E-04   
 mr-sdci # 73  5   -196.4644810463  9.5239E-10  0.0000E+00  4.6442E-04  1.0000E-04   
 mr-sdci # 73  6   -196.4644809804  6.2172E-09  0.0000E+00  6.7996E-04  1.0000E-04   
 mr-sdci # 73  7   -195.7785151959  7.8770E-05  0.0000E+00  2.2277E-01  1.0000E-04   
 mr-sdci # 73  8   -195.7678903936  3.0680E-07  0.0000E+00  3.1503E-01  1.0000E-04   
 mr-sdci # 73  9   -194.1154873155  8.6100E-02  0.0000E+00  3.4116E+00  1.0000E-04   
 mr-sdci # 73 10   -193.8003525444  6.3282E-01  0.0000E+00  3.2310E+00  1.0000E-04   
 mr-sdci # 73 11   -192.3338508500 -2.0353E+00  0.0000E+00  3.8364E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.014000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  74

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59963363
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426792
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426789
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616607
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616590
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88926163
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.87918450
   ht   9    -0.00015542     0.00119968     0.00025171     0.00287431     0.01008965     0.00927194    -0.00054389     0.00019795
   ht  10    -0.00438833     0.00767832    -0.01648267    -0.00111488    -0.00434376     0.00410727     0.00007434    -0.00078594
   ht  11     0.04688178     0.01511393     0.01152200    -0.01535281    -0.03206176     0.02638525    -0.00078938    -0.00099639
   ht  12     0.00494024     0.00337542     0.00121335    -0.00164539     0.00205577    -0.00066814    -0.00043119     0.00004901

                ht   9         ht  10         ht  11         ht  12
   ht   9    -0.00000991
   ht  10     0.00000092    -0.00001627
   ht  11    -0.00000021    -0.00000249    -0.00028097
   ht  12    -0.00000010     0.00000032    -0.00000551    -0.00000248

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -3.065600E-05   0.822948      -0.250422       0.108815       0.428867       6.343015E-02  -0.103876       4.115809E-02
 ref    2  -1.236867E-04   6.435600E-02   0.408269       0.452863       0.109872      -0.742846       3.389946E-02   6.120070E-02

              v      9       v     10       v     11       v     12
 ref    1   8.255001E-03  -8.180597E-03   5.295199E-02  -6.434598E-03
 ref    2   4.832926E-02  -6.137885E-03  -3.213991E-02   3.642904E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   1.623820E-08   0.681385       0.229395       0.216925       0.195999       0.555843       1.193943E-02   5.439515E-03

              v      9       v     10       v     11       v     12
 ref    1   2.403862E-03   1.045958E-04   3.836887E-03   5.467480E-05

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00003066     0.82294823    -0.25042235     0.10881462     0.42886716     0.06343015    -0.10387617     0.04115809
 ref:   2    -0.00012369     0.06435600     0.40826851     0.45286266     0.10987210    -0.74284552     0.03389946     0.06120070

                ci   9         ci  10         ci  11         ci  12
 ref:   1     0.00825500    -0.00818060     0.05295199    -0.00643460
 ref:   2     0.04832926    -0.00613789    -0.03213991     0.00364290

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 74  1   -196.4879553475  5.7037E-08  0.0000E+00  2.8742E-03  1.0000E-04   
 mr-sdci # 74  2   -196.4725827987  3.1761E-12  0.0000E+00  4.0193E-05  1.0000E-04   
 mr-sdci # 74  3   -196.4725827807  4.9509E-08  0.0000E+00  1.9224E-04  1.0000E-04   
 mr-sdci # 74  4   -196.4725826903  1.0548E-10  1.0154E-07  7.1390E-04  1.0000E-04   
 mr-sdci # 74  5   -196.4644810467  3.2153E-10  0.0000E+00  4.6688E-04  1.0000E-04   
 mr-sdci # 74  6   -196.4644809833  2.9512E-09  0.0000E+00  6.7782E-04  1.0000E-04   
 mr-sdci # 74  7   -195.7786852386  1.7004E-04  0.0000E+00  2.2264E-01  1.0000E-04   
 mr-sdci # 74  8   -195.7692550458  1.3647E-03  0.0000E+00  3.0795E-01  1.0000E-04   
 mr-sdci # 74  9   -194.2735727651  1.5809E-01  0.0000E+00  3.2029E+00  1.0000E-04   
 mr-sdci # 74 10   -193.8846825337  8.4330E-02  0.0000E+00  3.2258E+00  1.0000E-04   
 mr-sdci # 74 11   -193.1463475397  8.1250E-01  0.0000E+00  3.5761E+00  1.0000E-04   
 mr-sdci # 74 12   -192.2706428901 -1.7693E+00  0.0000E+00  3.9040E+00  1.0000E-04   
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.006000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  75

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59963363
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426792
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426789
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616607
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616590
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88926163
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.87918450
   ht   9    -0.00015542     0.00119968     0.00025171     0.00287431     0.01008965     0.00927194    -0.00054389     0.00019795
   ht  10    -0.00438833     0.00767832    -0.01648267    -0.00111488    -0.00434376     0.00410727     0.00007434    -0.00078594
   ht  11     0.04688178     0.01511393     0.01152200    -0.01535281    -0.03206176     0.02638525    -0.00078938    -0.00099639
   ht  12     0.00494024     0.00337542     0.00121335    -0.00164539     0.00205577    -0.00066814    -0.00043119     0.00004901
   ht  13    -0.00233921     0.00125855     0.00545470    -0.00632234     0.00133898     0.00212821    -0.00055224     0.00014351

                ht   9         ht  10         ht  11         ht  12         ht  13
   ht   9    -0.00000991
   ht  10     0.00000092    -0.00001627
   ht  11    -0.00000021    -0.00000249    -0.00028097
   ht  12    -0.00000010     0.00000032    -0.00000551    -0.00000248
   ht  13    -0.00000030     0.00000050    -0.00000018    -0.00000008    -0.00000346

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   2.544154E-05   0.821003       0.233231      -0.152823       0.428713       6.446676E-02  -9.865203E-02  -5.331574E-02
 ref    2   1.238561E-04   5.305235E-02  -0.454283      -0.408312       0.111666      -0.742579       4.133888E-02  -5.644273E-02

              v      9       v     10       v     11       v     12       v     13
 ref    1  -1.447969E-02   7.632194E-03  -3.229897E-02   4.133434E-02   3.877514E-02
 ref    2  -4.512155E-02  -7.731123E-03   1.026207E-02  -3.068315E-02  -2.572055E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   1.598761E-08   0.676860       0.260770       0.190073       0.196264       0.555580       1.144113E-02   6.028350E-03

              v      9       v     10       v     11       v     12       v     13
 ref    1   2.245616E-03   1.180206E-04   1.148533E-03   2.649984E-03   2.165058E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00002544     0.82100283     0.23323056    -0.15282267     0.42871265     0.06446676    -0.09865203    -0.05331574
 ref:   2     0.00012386     0.05305235    -0.45428343    -0.40831163     0.11166606    -0.74257917     0.04133888    -0.05644273

                ci   9         ci  10         ci  11         ci  12         ci  13
 ref:   1    -0.01447969     0.00763219    -0.03229897     0.04133434     0.03877514
 ref:   2    -0.04512155    -0.00773112     0.01026207    -0.03068315    -0.02572055

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 75  1   -196.4879553979  5.0395E-08  0.0000E+00  2.8350E-03  1.0000E-04   
 mr-sdci # 75  2   -196.4725827987  2.3213E-11  0.0000E+00  3.8322E-05  1.0000E-04   
 mr-sdci # 75  3   -196.4725827809  2.0076E-10  0.0000E+00  1.8990E-04  1.0000E-04   
 mr-sdci # 75  4   -196.4725827660  7.5693E-08  0.0000E+00  2.9573E-04  1.0000E-04   
 mr-sdci # 75  5   -196.4644810467  8.4057E-12  9.5069E-08  4.6784E-04  1.0000E-04   
 mr-sdci # 75  6   -196.4644809860  2.6643E-09  0.0000E+00  6.7760E-04  1.0000E-04   
 mr-sdci # 75  7   -195.7800893466  1.4041E-03  0.0000E+00  2.0533E-01  1.0000E-04   
 mr-sdci # 75  8   -195.7702981875  1.0431E-03  0.0000E+00  2.9044E-01  1.0000E-04   
 mr-sdci # 75  9   -194.3839164414  1.1034E-01  0.0000E+00  3.0971E+00  1.0000E-04   
 mr-sdci # 75 10   -193.9709914178  8.6309E-02  0.0000E+00  3.1007E+00  1.0000E-04   
 mr-sdci # 75 11   -193.3449317399  1.9858E-01  0.0000E+00  3.5474E+00  1.0000E-04   
 mr-sdci # 75 12   -192.7877031433  5.1706E-01  0.0000E+00  3.6776E+00  1.0000E-04   
 mr-sdci # 75 13   -191.6099101013 -1.2841E+00  0.0000E+00  4.7129E+00  1.0000E-04   
 
 root number  5 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  76

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59963363
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426792
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426789
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616607
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616590
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88926163
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.87918450
   ht   9    -0.00015542     0.00119968     0.00025171     0.00287431     0.01008965     0.00927194    -0.00054389     0.00019795
   ht  10    -0.00438833     0.00767832    -0.01648267    -0.00111488    -0.00434376     0.00410727     0.00007434    -0.00078594
   ht  11     0.04688178     0.01511393     0.01152200    -0.01535281    -0.03206176     0.02638525    -0.00078938    -0.00099639
   ht  12     0.00494024     0.00337542     0.00121335    -0.00164539     0.00205577    -0.00066814    -0.00043119     0.00004901
   ht  13    -0.00233921     0.00125855     0.00545470    -0.00632234     0.00133898     0.00212821    -0.00055224     0.00014351
   ht  14    -0.00073312     0.00113233     0.00086260     0.00313078     0.00701619     0.00751064    -0.00073685     0.00055640

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14
   ht   9    -0.00000991
   ht  10     0.00000092    -0.00001627
   ht  11    -0.00000021    -0.00000249    -0.00028097
   ht  12    -0.00000010     0.00000032    -0.00000551    -0.00000248
   ht  13    -0.00000030     0.00000050    -0.00000018    -0.00000008    -0.00000346
   ht  14    -0.00000204     0.00000025     0.00000323    -0.00000011    -0.00000012    -0.00000502

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   3.482783E-05   0.820614      -0.238253      -0.147056       0.430666      -4.984520E-02  -8.878107E-02  -6.458069E-02
 ref    2   1.060513E-04   5.322252E-02   0.442908      -0.420602       8.632089E-02   0.745948       3.985097E-02  -4.956208E-02

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1   2.831034E-02   1.584373E-02  -2.354318E-02  -2.680552E-03   4.827891E-02   6.721616E-02
 ref    2  -4.948711E-02   4.590054E-02  -9.117365E-03  -5.978899E-03  -3.829545E-02  -9.877430E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   1.245987E-08   0.676239       0.252932       0.198531       0.192924       0.558924       9.470179E-03   6.627065E-03

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1   3.250450E-03   2.357883E-03   6.374075E-04   4.293260E-05   3.797394E-03   1.427437E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00003483     0.82061361    -0.23825260    -0.14705562     0.43066578    -0.04984520    -0.08878107    -0.06458069
 ref:   2     0.00010605     0.05322252     0.44290846    -0.42060197     0.08632089     0.74594850     0.03985097    -0.04956208

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14
 ref:   1     0.02831034     0.01584373    -0.02354318    -0.00268055     0.04827891     0.06721616
 ref:   2    -0.04948711     0.04590054    -0.00911736    -0.00597890    -0.03829545    -0.09877430

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 76  1   -196.4879554963  9.8383E-08  0.0000E+00  2.8387E-03  1.0000E-04   
 mr-sdci # 76  2   -196.4725827987  1.3429E-12  0.0000E+00  3.8291E-05  1.0000E-04   
 mr-sdci # 76  3   -196.4725827812  3.0576E-10  0.0000E+00  1.8982E-04  1.0000E-04   
 mr-sdci # 76  4   -196.4725827665  5.3596E-10  0.0000E+00  2.9090E-04  1.0000E-04   
 mr-sdci # 76  5   -196.4644811509  1.0424E-07  0.0000E+00  1.6816E-04  1.0000E-04   
 mr-sdci # 76  6   -196.4644809861  1.1100E-10  1.4751E-07  6.7708E-04  1.0000E-04   
 mr-sdci # 76  7   -195.7835390672  3.4497E-03  0.0000E+00  2.1355E-01  1.0000E-04   
 mr-sdci # 76  8   -195.7707075551  4.0937E-04  0.0000E+00  2.8845E-01  1.0000E-04   
 mr-sdci # 76  9   -195.6210779971  1.2372E+00  0.0000E+00  6.3344E-01  1.0000E-04   
 mr-sdci # 76 10   -194.3479080522  3.7692E-01  0.0000E+00  3.1808E+00  1.0000E-04   
 mr-sdci # 76 11   -193.5144930545  1.6956E-01  0.0000E+00  3.3498E+00  1.0000E-04   
 mr-sdci # 76 12   -193.0682780117  2.8057E-01  0.0000E+00  3.4531E+00  1.0000E-04   
 mr-sdci # 76 13   -191.6199814693  1.0071E-02  0.0000E+00  4.7323E+00  1.0000E-04   
 mr-sdci # 76 14   -190.9533561155 -1.7366E+00  0.0000E+00  3.8151E+00  1.0000E-04   
 
 root number  6 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.009000
time for eigenvalue solver             0.001000
time for vector access                 0.000000

          starting ci iteration  77

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59963363
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426792
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426789
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616607
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616590
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88926163
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.87918450
   ht   9    -0.00015542     0.00119968     0.00025171     0.00287431     0.01008965     0.00927194    -0.00054389     0.00019795
   ht  10    -0.00438833     0.00767832    -0.01648267    -0.00111488    -0.00434376     0.00410727     0.00007434    -0.00078594
   ht  11     0.04688178     0.01511393     0.01152200    -0.01535281    -0.03206176     0.02638525    -0.00078938    -0.00099639
   ht  12     0.00494024     0.00337542     0.00121335    -0.00164539     0.00205577    -0.00066814    -0.00043119     0.00004901
   ht  13    -0.00233921     0.00125855     0.00545470    -0.00632234     0.00133898     0.00212821    -0.00055224     0.00014351
   ht  14    -0.00073312     0.00113233     0.00086260     0.00313078     0.00701619     0.00751064    -0.00073685     0.00055640
   ht  15    -0.00166288     0.00864378    -0.00797401    -0.00455377    -0.00469936     0.00472958    -0.00003110    -0.00185425

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15
   ht   9    -0.00000991
   ht  10     0.00000092    -0.00001627
   ht  11    -0.00000021    -0.00000249    -0.00028097
   ht  12    -0.00000010     0.00000032    -0.00000551    -0.00000248
   ht  13    -0.00000030     0.00000050    -0.00000018    -0.00000008    -0.00000346
   ht  14    -0.00000204     0.00000025     0.00000323    -0.00000011    -0.00000012    -0.00000502
   ht  15    -0.00000014    -0.00000336    -0.00000307    -0.00000004    -0.00000012     0.00000017    -0.00000753

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   4.636031E-05   0.820758       0.227200      -0.162886       0.422955      -9.521294E-02  -8.856066E-02  -6.496852E-02
 ref    2   9.372473E-05   5.141515E-02  -0.467615      -0.393191      -0.164915      -0.732583       4.081041E-02  -4.993493E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1   7.692465E-03  -2.926223E-02  -2.389094E-02   1.376932E-02  -5.646954E-02  -6.844346E-02   6.870942E-03
 ref    2   3.983148E-04   6.387669E-02  -9.435564E-03   1.134249E-02   1.043365E-02   0.101401       3.351391E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   1.093360E-08   0.676288       0.270284       0.181131       0.206088       0.545743       9.508480E-03   6.714406E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1   5.933268E-05   4.936510E-03   6.598067E-04   3.182463E-04   3.297670E-03   1.496666E-02   1.170392E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00004636     0.82075847     0.22720033    -0.16288614     0.42295519    -0.09521294    -0.08856066    -0.06496852
 ref:   2     0.00009372     0.05141515    -0.46761494    -0.39319084    -0.16491465    -0.73258275     0.04081041    -0.04993493

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15
 ref:   1     0.00769247    -0.02926223    -0.02389094     0.01376932    -0.05646954    -0.06844346     0.00687094
 ref:   2     0.00039831     0.06387669    -0.00943556     0.01134249     0.01043365     0.10140094     0.03351391

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 77  1   -196.4879558912  3.9490E-07  2.5035E-06  2.8022E-03  1.0000E-04   
 mr-sdci # 77  2   -196.4725827987  2.8209E-12  0.0000E+00  3.7761E-05  1.0000E-04   
 mr-sdci # 77  3   -196.4725827817  4.4862E-10  0.0000E+00  1.8444E-04  1.0000E-04   
 mr-sdci # 77  4   -196.4725827681  1.6213E-09  0.0000E+00  2.7636E-04  1.0000E-04   
 mr-sdci # 77  5   -196.4644811527  1.8127E-09  0.0000E+00  1.3718E-04  1.0000E-04   
 mr-sdci # 77  6   -196.4644811375  1.5138E-07  0.0000E+00  2.8811E-04  1.0000E-04   
 mr-sdci # 77  7   -195.7836238907  8.4824E-05  0.0000E+00  2.1039E-01  1.0000E-04   
 mr-sdci # 77  8   -195.7707789303  7.1375E-05  0.0000E+00  2.8560E-01  1.0000E-04   
 mr-sdci # 77  9   -195.6705855373  4.9508E-02  0.0000E+00  5.9550E-01  1.0000E-04   
 mr-sdci # 77 10   -195.5928681474  1.2450E+00  0.0000E+00  8.8404E-01  1.0000E-04   
 mr-sdci # 77 11   -193.5145506482  5.7594E-05  0.0000E+00  3.3502E+00  1.0000E-04   
 mr-sdci # 77 12   -193.1157733265  4.7495E-02  0.0000E+00  3.4854E+00  1.0000E-04   
 mr-sdci # 77 13   -192.2612766395  6.4130E-01  0.0000E+00  4.2519E+00  1.0000E-04   
 mr-sdci # 77 14   -190.9554464380  2.0903E-03  0.0000E+00  3.8077E+00  1.0000E-04   
 mr-sdci # 77 15   -189.9526421600 -1.1809E+00  0.0000E+00  4.5615E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.015000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  78

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59963363
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426792
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426789
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616607
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616590
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88926163
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.87918450
   ht   9    -0.00015542     0.00119968     0.00025171     0.00287431     0.01008965     0.00927194    -0.00054389     0.00019795
   ht  10    -0.00438833     0.00767832    -0.01648267    -0.00111488    -0.00434376     0.00410727     0.00007434    -0.00078594
   ht  11     0.04688178     0.01511393     0.01152200    -0.01535281    -0.03206176     0.02638525    -0.00078938    -0.00099639
   ht  12     0.00494024     0.00337542     0.00121335    -0.00164539     0.00205577    -0.00066814    -0.00043119     0.00004901
   ht  13    -0.00233921     0.00125855     0.00545470    -0.00632234     0.00133898     0.00212821    -0.00055224     0.00014351
   ht  14    -0.00073312     0.00113233     0.00086260     0.00313078     0.00701619     0.00751064    -0.00073685     0.00055640
   ht  15    -0.00166288     0.00864378    -0.00797401    -0.00455377    -0.00469936     0.00472958    -0.00003110    -0.00185425
   ht  16     0.04731384    -0.00628834     0.00483400    -0.00760293    -0.02092008     0.02093966     0.00132589    -0.00068349

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15         ht  16
   ht   9    -0.00000991
   ht  10     0.00000092    -0.00001627
   ht  11    -0.00000021    -0.00000249    -0.00028097
   ht  12    -0.00000010     0.00000032    -0.00000551    -0.00000248
   ht  13    -0.00000030     0.00000050    -0.00000018    -0.00000008    -0.00000346
   ht  14    -0.00000204     0.00000025     0.00000323    -0.00000011    -0.00000012    -0.00000502
   ht  15    -0.00000014    -0.00000336    -0.00000307    -0.00000004    -0.00000012     0.00000017    -0.00000753
   ht  16     0.00000076     0.00000514    -0.00007142    -0.00000313     0.00000113     0.00000091    -0.00000131    -0.00012771

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   1.260186E-05   0.820533       0.227953      -0.162971       0.425062       8.531507E-02  -8.172249E-02   7.348558E-02
 ref    2   1.955526E-05   5.177008E-02  -0.467504      -0.393276      -0.147773       0.736230       3.716475E-02   3.985707E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1  -4.226825E-03  -2.400816E-02  -3.952001E-02  -2.596835E-02  -4.866157E-02  -3.046299E-02  -6.731709E-02  -5.230675E-03
 ref    2   5.403142E-02   6.090026E-02  -2.862990E-02  -1.058243E-02  -2.218449E-02   5.879425E-02   9.154062E-02  -2.999121E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   5.412152E-10   0.675955       0.270523       0.181225       0.202515       0.549313       8.059784E-03   6.988716E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   2.937260E-03   4.285233E-03   2.381502E-03   7.863432E-04   2.860100E-03   4.384758E-03   1.291128E-02   9.268326E-04

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15        ev   16

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00001260     0.82053313     0.22795268    -0.16297080     0.42506188     0.08531507    -0.08172249     0.07348558
 ref:   2     0.00001956     0.05177008    -0.46750431    -0.39327597    -0.14777315     0.73622964     0.03716475     0.03985707

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1    -0.00422683    -0.02400816    -0.03952001    -0.02596835    -0.04866157    -0.03046299    -0.06731709    -0.00523068
 ref:   2     0.05403142     0.06090026    -0.02862990    -0.01058243    -0.02218449     0.05879425     0.09154062    -0.02999121

 trial vector basis is being transformed.  new dimension:   8

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 78  1   -196.4879580261  2.1350E-06  0.0000E+00  1.4546E-03  1.0000E-04   
 mr-sdci # 78  2   -196.4725827988  3.0575E-11  0.0000E+00  3.6852E-05  1.0000E-04   
 mr-sdci # 78  3   -196.4725827817  7.4110E-12  1.2735E-08  1.8316E-04  1.0000E-04   
 mr-sdci # 78  4   -196.4725827681  6.6080E-13  0.0000E+00  2.7624E-04  1.0000E-04   
 mr-sdci # 78  5   -196.4644811528  5.5060E-11  0.0000E+00  1.3885E-04  1.0000E-04   
 mr-sdci # 78  6   -196.4644811395  2.0073E-09  0.0000E+00  2.5696E-04  1.0000E-04   
 mr-sdci # 78  7   -195.7855263314  1.9024E-03  0.0000E+00  2.3020E-01  1.0000E-04   
 mr-sdci # 78  8   -195.7716950343  9.1610E-04  0.0000E+00  2.9115E-01  1.0000E-04   
 mr-sdci # 78  9   -195.7059039297  3.5318E-02  0.0000E+00  5.7700E-01  1.0000E-04   
 mr-sdci # 78 10   -195.6397235641  4.6855E-02  0.0000E+00  6.1506E-01  1.0000E-04   
 mr-sdci # 78 11   -194.5632470225  1.0487E+00  0.0000E+00  2.7317E+00  1.0000E-04   
 mr-sdci # 78 12   -193.5084831966  3.9271E-01  0.0000E+00  3.3309E+00  1.0000E-04   
 mr-sdci # 78 13   -192.8315945422  5.7032E-01  0.0000E+00  3.9649E+00  1.0000E-04   
 mr-sdci # 78 14   -191.5405492068  5.8510E-01  0.0000E+00  4.2472E+00  1.0000E-04   
 mr-sdci # 78 15   -190.9371394734  9.8450E-01  0.0000E+00  3.8081E+00  1.0000E-04   
 mr-sdci # 78 16   -189.9394343362 -9.7723E-01  0.0000E+00  4.5432E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.014000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  79

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59964323
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426799
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426797
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616636
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616634
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.89721154
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88338024
   ht   9     0.00161267    -0.00135196    -0.00024023     0.00094910    -0.00161295    -0.00140986     0.00012739    -0.00023406

                ht   9
   ht   9    -0.00000054

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7         v:   8

   eig(s)   7.012846E-09    1.00000        1.00000        1.00000        1.00000        1.00000        1.00000        1.00000    
 
   x:   1   3.187090E-05   0.201926       3.587866E-02  -0.141757       0.240947       0.210608      -1.947411E-02   0.759209    
   x:   2  -2.672681E-05   0.830665      -3.008770E-02   0.118877      -0.202057      -0.176615       1.633091E-02   0.201926    
   x:   3  -4.748872E-06  -3.008770E-02   0.994654       2.112222E-02  -3.590188E-02  -3.138134E-02   2.901708E-03   3.587866E-02
   x:   4   1.876280E-05   0.118877       2.112222E-02   0.916546       0.141848       0.123988      -1.146465E-02  -0.141757    
   x:   5  -3.189154E-05  -0.202057      -3.590188E-02   0.141848       0.758897      -0.210745       1.948671E-02   0.240947    
   x:   6  -2.787596E-05  -0.176615      -3.138134E-02   0.123988      -0.210745       0.815791       1.703307E-02   0.210608    
   x:   7   2.577578E-06   1.633091E-02   2.901708E-03  -1.146465E-02   1.948671E-02   1.703307E-02   0.998425      -1.947411E-02
   x:   8  -4.649077E-06  -0.426494      -7.578029E-02   0.299408      -0.508910      -0.444831       4.113179E-02   0.508581    
   x:   9    1.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000    

                v:   9

   eig(s)    1.00000    
 
   x:   1   0.508581    
   x:   2  -0.426494    
   x:   3  -7.578028E-02
   x:   4   0.299408    
   x:   5  -0.508910    
   x:   6  -0.444831    
   x:   7   4.113179E-02
   x:   8  -7.418780E-02
   x:   9  -6.266633E-05
 bummer (warning):overlap matrix: # small eigenvalues=                      1

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   8.034072E-06   0.818238       0.235034       0.164453      -0.424213       8.943656E-02   7.981412E-02  -7.592702E-02
 ref    2   1.559472E-05   5.645388E-02  -0.469543       0.390186       0.154912       0.734761      -3.841891E-02  -4.012876E-02

              v      9
 ref    1   6.122902E-02
 ref    2   5.682178E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   3.077417E-10   0.672700       0.275711       0.179290       0.203955       0.547873       7.846306E-03   7.375230E-03

              v      9
 ref    1   6.977708E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00000803     0.81823753     0.23503356     0.16445339    -0.42421330     0.08943656     0.07981412    -0.07592702
 ref:   2     0.00001559     0.05645388    -0.46954302     0.39018556     0.15491191     0.73476124    -0.03841891    -0.04012876

                ci   9
 ref:   1     0.06122902
 ref:   2     0.05682178

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 79  1   -196.4879580375  1.1389E-08  0.0000E+00  1.4248E-03  1.0000E-04   
 mr-sdci # 79  2   -196.4725827988  1.0374E-12  0.0000E+00  3.6684E-05  1.0000E-04   
 mr-sdci # 79  3   -196.4725827920  1.0275E-08  0.0000E+00  1.3740E-04  1.0000E-04   
 mr-sdci # 79  4   -196.4725827681  1.3927E-12  2.8694E-08  2.7627E-04  1.0000E-04   
 mr-sdci # 79  5   -196.4644811530  2.3200E-10  0.0000E+00  1.3819E-04  1.0000E-04   
 mr-sdci # 79  6   -196.4644811396  7.2703E-11  0.0000E+00  2.6069E-04  1.0000E-04   
 mr-sdci # 79  7   -195.7856673432  1.4101E-04  0.0000E+00  2.3154E-01  1.0000E-04   
 mr-sdci # 79  8   -195.7721133686  4.1833E-04  0.0000E+00  2.8591E-01  1.0000E-04   
 mr-sdci # 79  9   -194.2197214607 -1.4862E+00  0.0000E+00  3.0422E+00  1.0000E-04   
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.007000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  80

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59964323
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426799
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426797
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616636
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616634
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.89721154
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88338024
   ht   9     0.00161267    -0.00135196    -0.00024023     0.00094910    -0.00161295    -0.00140986     0.00012739    -0.00023406
   ht  10     0.00233154     0.00085488     0.00292673    -0.00441925     0.00073044    -0.00112907    -0.00025682     0.00007109

                ht   9         ht  10
   ht   9    -0.00000054
   ht  10     0.00000002    -0.00000146

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7         v:   8

   eig(s)   7.000586E-09   1.568415E-08    1.00000        1.00000        1.00000        1.00000        1.00000        1.00000    
 
   x:   1   3.011706E-05   4.724330E-05   5.251537E-02   0.698414      -4.293125E-03  -0.121534       5.829867E-02  -0.194054    
   x:   2  -2.734294E-05   1.588391E-05  -0.507757       0.153566       0.409136      -0.565671       0.183544      -7.580572E-02
   x:   3  -6.919480E-06   5.763913E-05  -0.148884      -0.239649      -0.623858      -0.377234      -0.364689       0.134930    
   x:   4   2.203218E-05  -8.659794E-05  -0.396728       0.221544      -0.359565      -0.234064      -0.132370      -2.233009E-02
   x:   5  -3.241167E-05   1.323373E-05  -0.310288       0.373756      -0.311862       0.564035      -7.442635E-02  -0.281365    
   x:   6  -2.701747E-05  -2.335580E-05   0.660393       0.384432      -0.112436      -0.339327      -0.187367       2.243993E-02
   x:   7   2.773893E-06  -5.172650E-06   0.145473      -0.307604      -0.104241      -0.172794       0.100826      -0.911045    
   x:   8  -4.696230E-06   1.166665E-06   7.940704E-02   4.454230E-03  -0.439702      -7.084217E-02   0.872686       0.167985    
   x:   9   0.999294       3.757380E-02   1.532356E-12   3.622116E-13  -5.173103E-13  -1.400496E-12   1.581796E-13  -6.040756E-12
   x:  10  -3.757380E-02   0.999294       2.382141E-13    0.00000        0.00000        0.00000        0.00000      -3.021395E-13

                v:   9         v:  10

   eig(s)    1.00000        1.00000    
 
   x:   1  -0.565232      -0.366235    
   x:   2   0.408436      -0.158018    
   x:   3   7.816361E-03  -0.487476    
   x:   4  -0.197605       0.743225    
   x:   5   0.494120      -0.140620    
   x:   6   0.473158       0.169765    
   x:   7  -3.508951E-02   4.575148E-02
   x:   8   7.293246E-02  -1.410883E-02
   x:   9   6.205290E-05  -8.746779E-06
   x:  10   4.570133E-06   1.187631E-04
 bummer (warning):overlap matrix: # small eigenvalues=                      1

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -6.925099E-06  -0.820383      -0.225110      -0.167610       0.424035       9.027883E-02  -7.412199E-02   8.095224E-02
 ref    2  -1.560083E-05  -6.322837E-02   0.495317      -0.355761      -0.156371       0.734452       4.052860E-02   3.801237E-02

              v      9       v     10
 ref    1  -5.827448E-02  -6.546344E-02
 ref    2  -5.676426E-02  -3.125690E-03

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   2.913430E-10   0.677027       0.296013       0.154659       0.204258       0.547570       7.136637E-03   7.998205E-03

              v      9       v     10
 ref    1   6.618096E-03   4.295233E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00000693    -0.82038342    -0.22511037    -0.16760988     0.42403547     0.09027883    -0.07412199     0.08095224
 ref:   2    -0.00001560    -0.06322837     0.49531661    -0.35576080    -0.15637070     0.73445211     0.04052860     0.03801237

                ci   9         ci  10
 ref:   1    -0.05827448    -0.06546344
 ref:   2    -0.05676426    -0.00312569

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 80  1   -196.4879580386  1.0850E-09  0.0000E+00  1.4366E-03  1.0000E-04   
 mr-sdci # 80  2   -196.4725827988  4.9099E-12  0.0000E+00  3.6595E-05  1.0000E-04   
 mr-sdci # 80  3   -196.4725827920  3.3211E-11  0.0000E+00  1.3765E-04  1.0000E-04   
 mr-sdci # 80  4   -196.4725827866  1.8491E-08  0.0000E+00  1.6877E-04  1.0000E-04   
 mr-sdci # 80  5   -196.4644811533  2.8371E-10  6.9216E-09  1.3331E-04  1.0000E-04   
 mr-sdci # 80  6   -196.4644811396  2.5935E-12  0.0000E+00  2.6034E-04  1.0000E-04   
 mr-sdci # 80  7   -195.7869176198  1.2503E-03  0.0000E+00  2.2175E-01  1.0000E-04   
 mr-sdci # 80  8   -195.7725624000  4.4903E-04  0.0000E+00  2.7858E-01  1.0000E-04   
 mr-sdci # 80  9   -194.2209767923  1.2553E-03  0.0000E+00  3.0501E+00  1.0000E-04   
 mr-sdci # 80 10   -193.6246182645 -2.0151E+00  0.0000E+00  3.4829E+00  1.0000E-04   
 
 root number  5 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.005000
time for eigenvalue solver             0.001000
time for vector access                 0.000000

          starting ci iteration  81

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59964323
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426799
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426797
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616636
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616634
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.89721154
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88338024
   ht   9     0.00161267    -0.00135196    -0.00024023     0.00094910    -0.00161295    -0.00140986     0.00012739    -0.00023406
   ht  10     0.00233154     0.00085488     0.00292673    -0.00441925     0.00073044    -0.00112907    -0.00025682     0.00007109
   ht  11     0.00028009    -0.00095134     0.00073186     0.00038674     0.00039067    -0.00156560    -0.00008324    -0.00016092

                ht   9         ht  10         ht  11
   ht   9    -0.00000054
   ht  10     0.00000002    -0.00000146
   ht  11    -0.00000006    -0.00000003    -0.00000029

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7         v:   8

   eig(s)   4.176345E-09   7.005984E-09   1.569313E-08    1.00000        1.00000        1.00000        1.00000        1.00000    
 
   x:   1   8.160998E-06  -2.977031E-05  -4.708221E-05  -0.130367      -0.359051       0.318970       0.434338       0.132681    
   x:   2  -1.953313E-05   2.652155E-05  -1.639215E-05   7.948269E-02  -0.206808      -0.376825       8.991692E-02   0.714531    
   x:   3   1.575378E-05   7.637610E-06  -5.720979E-05  -0.602644       0.350775      -0.344486       0.200873      -3.899832E-04
   x:   4   6.182695E-06  -2.181738E-05   8.676894E-05  -0.346609      -0.126013      -0.133080       0.346992       0.273036    
   x:   5   6.667503E-06   3.273932E-05  -1.299995E-05   0.104453      -0.312104       0.455539       0.341504       2.355295E-02
   x:   6  -3.274521E-05   2.560265E-05   2.249281E-05  -0.455450       0.132404       0.268187       0.222606      -0.279932    
   x:   7  -1.641686E-06  -2.850358E-06   5.124261E-06   0.500139       0.604550      -3.143857E-02   0.612527       2.769597E-02
   x:   8  -3.396648E-06   4.552706E-06  -1.254517E-06   0.157747      -0.457132      -0.582625       0.318033      -0.563568    
   x:   9   4.469744E-02  -0.998279      -3.795577E-02   1.373193E-12   2.208248E-12  -8.513335E-13   4.226229E-13  -1.506896E-13
   x:  10   2.622559E-02   3.915319E-02  -0.998889      -1.244745E-12   6.126125E-13   7.176898E-13  -6.033317E-13   1.137634E-12
   x:  11   0.998656       4.365237E-02   2.793051E-02    0.00000      -2.795834E-12    0.00000        0.00000        0.00000    

                v:   9         v:  10         v:  11

   eig(s)    1.00000        1.00000        1.00000    
 
   x:   1  -0.423561       0.478994      -0.370475    
   x:   2  -0.220890      -0.469490      -0.145130    
   x:   3   0.328979       5.557053E-02  -0.493398    
   x:   4   0.206902       0.261061       0.735091    
   x:   5   0.641815      -0.367304      -0.141936    
   x:   6  -0.450496      -0.580339       0.188293    
   x:   7  -6.951501E-02   2.146342E-02   4.628804E-02
   x:   8  -3.259244E-02  -8.207651E-02  -1.193573E-02
   x:   9   1.321392E-05  -6.077659E-05  -7.659459E-06
   x:  10   2.642716E-06  -1.972120E-06   1.188052E-04
   x:  11  -2.727217E-05  -2.963860E-05   7.801241E-06
 bummer (warning):overlap matrix: # small eigenvalues=                      2

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   7.430464E-06   0.820555       0.227077       0.164078      -0.426368       7.853299E-02  -7.455688E-02   8.018804E-02
 ref    2   1.983633E-05   6.307107E-02  -0.490132       0.362899       0.136022       0.738491       4.227747E-02   3.810332E-02

              v      9       v     10       v     11
 ref    1  -1.235886E-02  -7.636710E-02   4.453038E-02
 ref    2   3.020110E-02  -9.518818E-02  -4.082794E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   4.486919E-10   0.677289       0.291793       0.158617       0.200291       0.551536       7.346113E-03   7.881984E-03

              v      9       v     10       v     11
 ref    1   1.064848E-03   1.489272E-02   3.649875E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00000743     0.82055529     0.22707705     0.16407826    -0.42636758     0.07853299    -0.07455688     0.08018804
 ref:   2     0.00001984     0.06307107    -0.49013158     0.36289851     0.13602193     0.73849104     0.04227747     0.03810332

                ci   9         ci  10         ci  11
 ref:   1    -0.01235886    -0.07636710     0.04453038
 ref:   2     0.03020110    -0.09518818    -0.04082794

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 81  1   -196.4879580438  5.1916E-09  0.0000E+00  1.4170E-03  1.0000E-04   
 mr-sdci # 81  2   -196.4725827988  2.1316E-13  0.0000E+00  3.6702E-05  1.0000E-04   
 mr-sdci # 81  3   -196.4725827921  7.7158E-11  0.0000E+00  1.3391E-04  1.0000E-04   
 mr-sdci # 81  4   -196.4725827867  7.8742E-11  0.0000E+00  1.7058E-04  1.0000E-04   
 mr-sdci # 81  5   -196.4644811585  5.2463E-09  0.0000E+00  7.3546E-05  1.0000E-04   
 mr-sdci # 81  6   -196.4644811396  3.7886E-11  2.2314E-08  2.5965E-04  1.0000E-04   
 mr-sdci # 81  7   -195.7879039039  9.8628E-04  0.0000E+00  2.1513E-01  1.0000E-04   
 mr-sdci # 81  8   -195.7725830229  2.0623E-05  0.0000E+00  2.7750E-01  1.0000E-04   
 mr-sdci # 81  9   -194.5310860523  3.1011E-01  0.0000E+00  2.2612E+00  1.0000E-04   
 mr-sdci # 81 10   -194.0080893192  3.8347E-01  0.0000E+00  3.1978E+00  1.0000E-04   
 mr-sdci # 81 11   -193.5226208870 -1.0406E+00  0.0000E+00  3.5644E+00  1.0000E-04   
 
 root number  6 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.006000
time for eigenvalue solver             0.001000
time for vector access                 0.000000

          starting ci iteration  82

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59964323
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426799
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426797
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616636
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616634
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.89721154
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88338024
   ht   9     0.00161267    -0.00135196    -0.00024023     0.00094910    -0.00161295    -0.00140986     0.00012739    -0.00023406
   ht  10     0.00233154     0.00085488     0.00292673    -0.00441925     0.00073044    -0.00112907    -0.00025682     0.00007109
   ht  11     0.00028009    -0.00095134     0.00073186     0.00038674     0.00039067    -0.00156560    -0.00008324    -0.00016092
   ht  12    -0.00096272     0.00268811    -0.00000529    -0.00066876     0.00069259    -0.00040348    -0.00011174     0.00044293

                ht   9         ht  10         ht  11         ht  12
   ht   9    -0.00000054
   ht  10     0.00000002    -0.00000146
   ht  11    -0.00000006    -0.00000003    -0.00000029
   ht  12     0.00000009    -0.00000006     0.00000005    -0.00000075

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7         v:   8

   eig(s)   4.173870E-09   6.877385E-09   1.183843E-08   1.572702E-08    1.00000        1.00000        1.00000        1.00000    
 
   x:   1  -7.684907E-06   3.219125E-05  -9.682072E-06  -4.839982E-05   0.291096       0.210645      -6.435059E-02   0.561609    
   x:   2   1.840829E-05  -3.497656E-05   4.980843E-05  -1.159861E-05   7.256208E-02  -0.180431       4.952402E-02   0.273372    
   x:   3  -1.582134E-05  -7.774534E-06   3.707371E-06  -5.705246E-05   0.346440      -0.682992       0.182628      -4.688799E-02
   x:   4  -5.776636E-06   2.428159E-05  -1.764346E-05   8.534402E-05   0.373795      -0.248198       9.742538E-02   0.346148    
   x:   5  -7.093060E-06  -3.454482E-05   9.285763E-06  -1.195676E-05   0.102121       0.378541      -0.202802       0.467446    
   x:   6   3.276762E-05  -2.410065E-05  -1.347250E-05   2.147652E-05   0.208285      -0.158031       8.006185E-02   0.166633    
   x:   7   1.698471E-06   3.179565E-06  -2.102816E-06   4.928408E-06  -2.107719E-02  -0.325119      -0.941342      -1.597942E-02
   x:   8   3.208360E-06  -5.959954E-06   8.197298E-06  -4.580717E-07   0.772010       0.347886      -0.129991      -0.491242    
   x:   9  -3.959639E-02   0.985369       0.163130      -2.945824E-02   1.180303E-13   1.914139E-12   2.309443E-12  -8.345507E-13
   x:  10  -2.700125E-02  -4.505930E-02   8.595925E-02  -0.994913       4.346826E-13   2.008703E-14   9.416479E-13  -6.859287E-15
   x:  11  -0.998682      -3.489682E-02  -2.685790E-02   2.636352E-02  -9.601156E-13   1.443026E-12   2.294265E-12   1.793971E-12
   x:  12  -1.836376E-02  -0.160620       0.982486       9.265839E-02  -8.436573E-13   1.294813E-12   1.786736E-12    0.00000    

                v:   9         v:  10         v:  11         v:  12

   eig(s)    1.00000        1.00000        1.00000        1.00000    
 
   x:   1   0.404204       5.889686E-02   0.528499       0.324321    
   x:   2   0.433716      -0.439901      -0.673013       0.224558    
   x:   3  -0.359766       1.756893E-02   0.131827       0.480449    
   x:   4  -0.125212      -0.298015       0.121223      -0.741741    
   x:   5  -0.676635       3.126019E-02  -0.316542       0.166232    
   x:   6   0.191420       0.840770      -0.348150      -0.180893    
   x:   7   6.502054E-02   2.050693E-02   2.139520E-02  -4.865032E-02
   x:   8   6.853143E-02  -7.486203E-02  -0.117487       2.562840E-02
   x:   9  -1.674157E-05   1.607411E-05  -5.688110E-05   1.236740E-05
   x:  10  -1.782158E-06  -3.805319E-06  -1.294516E-05  -1.180691E-04
   x:  11   2.356070E-05   1.900149E-05  -2.709351E-05  -5.732001E-06
   x:  12  -6.728427E-06   2.754943E-05   5.008703E-05  -1.957098E-05
 bummer (warning):overlap matrix: # small eigenvalues=                      2

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   1.050703E-05  -0.820369       0.227707       0.164136      -0.428742       6.431437E-02  -7.397970E-02  -8.115300E-02
 ref    2   1.964802E-05  -6.333164E-02  -0.489924       0.363133       0.111400       0.742604       4.274546E-02  -3.735866E-02

              v      9       v     10       v     11       v     12
 ref    1   6.078826E-03  -6.028724E-02  -2.751457E-02  -7.670024E-02
 ref    2  -2.698450E-02  -9.000915E-02  -5.140426E-02   1.553661E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   4.964422E-10   0.677017       0.291876       0.158806       0.196230       0.555597       7.300170E-03   7.981478E-03

              v      9       v     10       v     11       v     12
 ref    1   7.651153E-04   1.173620E-02   3.399450E-03   6.124313E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00001051    -0.82036925     0.22770684     0.16413588    -0.42874218     0.06431437    -0.07397970    -0.08115300
 ref:   2     0.00001965    -0.06333164    -0.48992415     0.36313313     0.11140029     0.74260392     0.04274546    -0.03735866

                ci   9         ci  10         ci  11         ci  12
 ref:   1     0.00607883    -0.06028724    -0.02751457    -0.07670024
 ref:   2    -0.02698450    -0.09000915    -0.05140426     0.01553661

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 82  1   -196.4879580574  1.3592E-08  3.6649E-07  1.4071E-03  1.0000E-04   
 mr-sdci # 82  2   -196.4725827988  4.4693E-12  0.0000E+00  3.6267E-05  1.0000E-04   
 mr-sdci # 82  3   -196.4725827921  4.5617E-12  0.0000E+00  1.3284E-04  1.0000E-04   
 mr-sdci # 82  4   -196.4725827867  1.2577E-12  0.0000E+00  1.7082E-04  1.0000E-04   
 mr-sdci # 82  5   -196.4644811586  4.8956E-12  0.0000E+00  7.2943E-05  1.0000E-04   
 mr-sdci # 82  6   -196.4644811550  1.5376E-08  0.0000E+00  1.0482E-04  1.0000E-04   
 mr-sdci # 82  7   -195.7886677187  7.6381E-04  0.0000E+00  2.1236E-01  1.0000E-04   
 mr-sdci # 82  8   -195.7726686985  8.5676E-05  0.0000E+00  2.7606E-01  1.0000E-04   
 mr-sdci # 82  9   -194.5832923986  5.2206E-02  0.0000E+00  2.1506E+00  1.0000E-04   
 mr-sdci # 82 10   -194.0478400277  3.9751E-02  0.0000E+00  2.7698E+00  1.0000E-04   
 mr-sdci # 82 11   -193.6258077031  1.0319E-01  0.0000E+00  3.4232E+00  1.0000E-04   
 mr-sdci # 82 12   -193.4493158030 -5.9167E-02  0.0000E+00  3.4545E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.013000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  83

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59964323
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426799
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426797
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616636
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616634
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.89721154
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88338024
   ht   9     0.00161267    -0.00135196    -0.00024023     0.00094910    -0.00161295    -0.00140986     0.00012739    -0.00023406
   ht  10     0.00233154     0.00085488     0.00292673    -0.00441925     0.00073044    -0.00112907    -0.00025682     0.00007109
   ht  11     0.00028009    -0.00095134     0.00073186     0.00038674     0.00039067    -0.00156560    -0.00008324    -0.00016092
   ht  12    -0.00096272     0.00268811    -0.00000529    -0.00066876     0.00069259    -0.00040348    -0.00011174     0.00044293
   ht  13    -0.00871676    -0.00420367    -0.00400697    -0.00304721     0.00109963    -0.00238432     0.00119470     0.00065786

                ht   9         ht  10         ht  11         ht  12         ht  13
   ht   9    -0.00000054
   ht  10     0.00000002    -0.00000146
   ht  11    -0.00000006    -0.00000003    -0.00000029
   ht  12     0.00000009    -0.00000006     0.00000005    -0.00000075
   ht  13     0.00000020     0.00000031     0.00000004     0.00000008    -0.00000997

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7         v:   8

   eig(s)   4.160322E-09   6.877245E-09   1.181436E-08   1.570875E-08   1.548539E-07    1.00000        1.00000        1.00000    
 
   x:   1   5.977607E-06   3.200746E-05  -1.220718E-05   5.032616E-05  -1.716505E-04  -0.337211       5.473550E-02   0.300475    
   x:   2  -1.911585E-05  -3.503195E-05   4.871989E-05   1.278600E-05  -8.338984E-05   4.887694E-02  -0.126755       1.427275E-02
   x:   3   1.499725E-05  -7.868502E-06   2.339279E-06   5.799318E-05  -7.873248E-05   0.607513       7.374691E-02  -0.277354    
   x:   4   5.287749E-06   2.422854E-05  -1.799411E-05  -8.473322E-05  -6.105945E-05   0.108642       6.436937E-02   6.730189E-03
   x:   5   7.287559E-06  -3.452599E-05   9.499665E-06   1.176194E-05   2.172150E-05  -0.434240       7.492755E-04   0.413002    
   x:   6  -3.322889E-05  -2.413084E-05  -1.388099E-05  -2.106097E-05  -4.687308E-05   8.029288E-02   4.086334E-02  -3.201011E-02
   x:   7  -1.464153E-06   3.204207E-06  -1.757762E-06  -5.218707E-06   2.428059E-05   0.554188      -0.106998       0.807442    
   x:   8  -3.065566E-06  -5.942342E-06   8.378269E-06   3.441481E-07   1.334057E-05   2.982851E-02   0.978895       9.477713E-02
   x:   9   4.042574E-02   0.985403       0.162488       3.058638E-02  -3.106580E-03  -2.342349E-12  -8.160932E-12   2.730200E-12
   x:  10   2.586637E-02  -4.521705E-02   8.061619E-02   0.995332       1.011434E-02   5.259477E-13   2.263107E-13  -5.259621E-13
   x:  11   0.998589      -3.541479E-02  -2.912257E-02  -2.510594E-02  -9.366735E-03  -3.373126E-12   2.546868E-13   2.920003E-12
   x:  12   2.065740E-02  -0.160256       0.982895      -8.728566E-02  -1.380965E-02  -3.786456E-12  -3.551040E-12   4.020278E-12
   x:  13   9.504607E-03   9.739648E-04   1.299258E-02  -1.141487E-02   0.999805        0.00000      -4.520004E-13    0.00000    

                v:   9         v:  10         v:  11         v:  12         v:  13

   eig(s)    1.00000        1.00000        1.00000        1.00000        1.00000    
 
   x:   1  -0.154462       7.038366E-02   0.370476       3.883978E-02   0.790844    
   x:   2  -0.249518      -0.429094      -0.762429       0.146433       0.363618    
   x:   3   0.500152       1.663976E-02   0.104594       0.369259       0.388386    
   x:   4   0.360480      -0.293079      -2.943940E-02  -0.854752       0.191572    
   x:   5   0.712971       2.378103E-02  -0.266496       0.229338      -9.141052E-02
   x:   6  -2.444039E-02   0.847475      -0.431595      -0.233322       0.177015    
   x:   7  -0.123450       1.975489E-02   4.301241E-02  -1.585521E-02  -0.108313    
   x:   8  -9.807603E-02  -7.471577E-02  -0.102463       5.300805E-02  -5.815195E-02
   x:   9   1.578425E-05   1.585080E-05  -5.225294E-05   2.156410E-05  -1.520850E-05
   x:  10   2.527638E-06  -3.778777E-06  -1.823129E-05  -1.089798E-04  -4.354200E-05
   x:  11  -2.061067E-05   1.938141E-05  -2.924063E-05  -5.117190E-06   1.199396E-06
   x:  12   5.788750E-06   2.741186E-05   4.839900E-05  -2.381260E-05   1.246262E-06
   x:  13   1.652220E-06   8.594598E-08  -7.252858E-06  -1.969179E-05   2.225002E-04
 bummer (warning):overlap matrix: # small eigenvalues=                      2

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   8.386267E-06  -0.820470      -0.227533       0.163874      -0.428810      -6.386313E-02  -7.135149E-02   8.350918E-02
 ref    2   1.092636E-05  -6.322073E-02   0.489720       0.363428       0.110619      -0.742721       4.319551E-02   3.554236E-02

              v      9       v     10       v     11       v     12       v     13
 ref    1  -5.551802E-03  -3.586818E-02   6.506826E-02   3.695676E-03   6.887969E-02
 ref    2   2.889576E-02  -7.792168E-02   5.526412E-02   4.519227E-02  -1.294429E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   1.897148E-10   0.677167       0.291597       0.158934       0.196114       0.555713       6.956888E-03   8.237043E-03

              v      9       v     10       v     11       v     12       v     13
 ref    1   8.657874E-04   7.358315E-03   7.288002E-03   2.055999E-03   4.911966E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00000839    -0.82046963    -0.22753348     0.16387436    -0.42880964    -0.06386313    -0.07135149     0.08350918
 ref:   2     0.00001093    -0.06322073     0.48972004     0.36342763     0.11061869    -0.74272079     0.04319551     0.03554236

                ci   9         ci  10         ci  11         ci  12         ci  13
 ref:   1    -0.00555180    -0.03586818     0.06506826     0.00369568     0.06887969
 ref:   2     0.02889576    -0.07792168     0.05526412     0.04519227    -0.01294429

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 83  1   -196.4879584046  3.4720E-07  0.0000E+00  5.6030E-04  1.0000E-04   
 mr-sdci # 83  2   -196.4725827988  1.2079E-12  0.0000E+00  3.6217E-05  1.0000E-04   
 mr-sdci # 83  3   -196.4725827921  4.1993E-12  6.2437E-09  1.3369E-04  1.0000E-04   
 mr-sdci # 83  4   -196.4725827867  2.7072E-12  0.0000E+00  1.7184E-04  1.0000E-04   
 mr-sdci # 83  5   -196.4644811586  1.4708E-12  0.0000E+00  7.2874E-05  1.0000E-04   
 mr-sdci # 83  6   -196.4644811550  9.4644E-12  0.0000E+00  1.0584E-04  1.0000E-04   
 mr-sdci # 83  7   -195.7897268322  1.0591E-03  0.0000E+00  1.9950E-01  1.0000E-04   
 mr-sdci # 83  8   -195.7729470114  2.7831E-04  0.0000E+00  2.7109E-01  1.0000E-04   
 mr-sdci # 83  9   -194.5838653254  5.7293E-04  0.0000E+00  2.1206E+00  1.0000E-04   
 mr-sdci # 83 10   -194.2462297371  1.9839E-01  0.0000E+00  3.1462E+00  1.0000E-04   
 mr-sdci # 83 11   -193.8020324276  1.7622E-01  0.0000E+00  3.6774E+00  1.0000E-04   
 mr-sdci # 83 12   -193.6064585469  1.5714E-01  0.0000E+00  3.6051E+00  1.0000E-04   
 mr-sdci # 83 13   -193.4056429690  5.7405E-01  0.0000E+00  3.2532E+00  1.0000E-04   
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.016000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  84

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59964323
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426799
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426797
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616636
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616634
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.89721154
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88338024
   ht   9     0.00161267    -0.00135196    -0.00024023     0.00094910    -0.00161295    -0.00140986     0.00012739    -0.00023406
   ht  10     0.00233154     0.00085488     0.00292673    -0.00441925     0.00073044    -0.00112907    -0.00025682     0.00007109
   ht  11     0.00028009    -0.00095134     0.00073186     0.00038674     0.00039067    -0.00156560    -0.00008324    -0.00016092
   ht  12    -0.00096272     0.00268811    -0.00000529    -0.00066876     0.00069259    -0.00040348    -0.00011174     0.00044293
   ht  13    -0.00871676    -0.00420367    -0.00400697    -0.00304721     0.00109963    -0.00238432     0.00119470     0.00065786
   ht  14     0.00162422    -0.00096901    -0.00054280     0.00072248    -0.00120197    -0.00068207     0.00014021    -0.00009280

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14
   ht   9    -0.00000054
   ht  10     0.00000002    -0.00000146
   ht  11    -0.00000006    -0.00000003    -0.00000029
   ht  12     0.00000009    -0.00000006     0.00000005    -0.00000075
   ht  13     0.00000020     0.00000031     0.00000004     0.00000008    -0.00000997
   ht  14    -0.00000011     0.00000003    -0.00000002     0.00000007     0.00000014    -0.00000028

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7         v:   8

   eig(s)   3.048016E-09   4.250894E-09   7.006574E-09   1.185085E-08   1.570998E-08   1.548594E-07    1.00000        1.00000    
 
   x:   1   3.866652E-05  -5.469075E-06   2.538200E-05  -1.041687E-05   5.061111E-05  -1.714554E-04  -6.745879E-03   3.813690E-02
   x:   2  -3.191234E-05  -9.915736E-06  -3.035143E-05   4.773787E-05   1.270172E-05  -8.350106E-05  -4.778026E-03   0.125866    
   x:   3  -7.552944E-06   1.791752E-05  -5.344578E-06   1.572568E-06   5.789350E-05  -7.879518E-05   0.247903      -0.117956    
   x:   4   2.128188E-05  -1.141569E-06   2.063851E-05  -1.705196E-05  -8.462839E-05  -6.097410E-05   9.991858E-02  -4.468106E-02
   x:   5  -2.712278E-05   1.604751E-05  -2.917054E-05   8.181796E-06   1.156120E-05   2.157939E-05  -1.720883E-02   0.123027    
   x:   6  -2.475245E-05  -2.702664E-05  -2.234068E-05  -1.435845E-05  -2.118292E-05  -4.695264E-05   4.720903E-02  -4.195754E-02
   x:   7   2.806690E-06  -2.393032E-06   2.581442E-06  -1.594371E-06  -5.196620E-06   2.429688E-05   0.855503       0.481545    
   x:   8  -4.188061E-06  -1.844793E-06  -5.372331E-06   8.295181E-06   3.422466E-07   1.332962E-05   0.440548      -0.847356    
   x:   9   0.169872      -2.752183E-02   0.972671       0.152926       3.003116E-02  -3.130901E-03  -1.819481E-12  -9.947906E-13
   x:  10  -1.454976E-02   3.248754E-02  -3.961484E-02   7.870408E-02   0.995423       1.012224E-02   3.381998E-13   9.036339E-13
   x:  11   0.271885       0.961324      -1.458344E-02  -3.145248E-02  -2.539878E-02  -9.379353E-03   7.182931E-12  -2.245052E-12
   x:  12  -7.957129E-02   5.017285E-02  -0.136524       0.982327      -8.576233E-02  -1.378268E-02  -1.783913E-13  -2.821121E-12
   x:  13  -3.542226E-03   1.090200E-02   2.528570E-03   1.253439E-02  -1.146461E-02   0.999787       9.916512E-14   2.570315E-14
   x:  14   0.943751      -0.267222      -0.182988       6.561929E-02   9.984036E-03   6.012180E-03    0.00000        0.00000    

                v:   9         v:  10         v:  11         v:  12         v:  13         v:  14

   eig(s)    1.00000        1.00000        1.00000        1.00000        1.00000        1.00000    
 
   x:   1  -0.421818      -0.176416       4.842672E-02  -0.388272       1.027101E-02  -0.797647    
   x:   2   6.860840E-02  -0.237787      -0.498511       0.703798      -0.232575      -0.353481    
   x:   3   0.550132       0.556321       0.118567      -7.724117E-02  -0.382973      -0.381837    
   x:   4   3.158357E-02   0.422702      -0.222194       0.166918       0.832001      -0.197200    
   x:   5  -0.675556       0.598380       9.711651E-02   0.273991      -0.275041       9.992055E-02
   x:   6   4.745685E-02  -0.187110       0.815845       0.486400       0.165050      -0.171227    
   x:   7  -6.630835E-02  -0.129045      -6.641630E-04  -5.530995E-02   2.739100E-02   0.106631    
   x:   8  -0.225156      -0.120184      -0.103332       7.328158E-02  -5.718378E-02   5.872926E-02
   x:   9  -3.800131E-06   7.619341E-06   1.522599E-05   5.046630E-05  -2.847970E-05   1.627273E-05
   x:  10  -2.474887E-08   3.550691E-06  -3.132862E-06   3.155989E-05   1.061809E-04   4.281018E-05
   x:  11   1.277510E-06  -2.578764E-05   1.450971E-05   2.831409E-05   1.842487E-06  -9.102445E-07
   x:  12   2.965445E-07   6.032616E-06   3.058527E-05  -4.323335E-05   2.916575E-05  -2.067263E-06
   x:  13   7.441445E-08   2.298147E-06   6.733693E-07   1.287435E-05   1.608699E-05  -2.225291E-04
   x:  14   4.661420E-06   1.287730E-05   6.463030E-06   3.609431E-05  -2.527099E-05   1.742671E-05
 bummer (warning):overlap matrix: # small eigenvalues=                      3

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -9.230169E-06  -0.850088      -5.408792E-02  -0.161928       0.428815      -6.382829E-02   6.902865E-02   8.547029E-02
 ref    2  -9.039487E-06   3.888546E-02   0.489182      -0.367544      -0.110558      -0.742730      -4.635544E-02   3.644465E-02

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1   5.391732E-03  -2.070221E-02   1.441516E-02  -3.016533E-02   4.734464E-02   0.116917    
 ref    2   3.994608E-02  -7.548290E-02  -3.178180E-03   6.017184E-04  -3.742720E-02   5.765619E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   1.669083E-10   0.724162       0.242225       0.161309       0.196105       0.555721       6.913782E-03   8.633382E-03

              v      9       v     10       v     11       v     12       v     13       v     14
 ref    1   1.624760E-03   6.126249E-03   2.178977E-04   9.103092E-04   3.642310E-03   1.699373E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00000923    -0.85008818    -0.05408792    -0.16192808     0.42881483    -0.06382829     0.06902865     0.08547029
 ref:   2    -0.00000904     0.03888546     0.48918203    -0.36754366    -0.11055823    -0.74272965    -0.04635544     0.03644465

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14
 ref:   1     0.00539173    -0.02070221     0.01441516    -0.03016533     0.04734464     0.11691662
 ref:   2     0.03994608    -0.07548290    -0.00317818     0.00060172    -0.03742720     0.05765619

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 84  1   -196.4879584080  3.4013E-09  0.0000E+00  5.4499E-04  1.0000E-04   
 mr-sdci # 84  2   -196.4725827988  9.4218E-12  0.0000E+00  3.8510E-05  1.0000E-04   
 mr-sdci # 84  3   -196.4725827986  6.5109E-09  0.0000E+00  6.2939E-05  1.0000E-04   
 mr-sdci # 84  4   -196.4725827867  6.6080E-13  9.7368E-09  1.7195E-04  1.0000E-04   
 mr-sdci # 84  5   -196.4644811586  2.1316E-14  0.0000E+00  7.2876E-05  1.0000E-04   
 mr-sdci # 84  6   -196.4644811550  1.2250E-11  0.0000E+00  1.0507E-04  1.0000E-04   
 mr-sdci # 84  7   -195.7902271827  5.0035E-04  0.0000E+00  1.9866E-01  1.0000E-04   
 mr-sdci # 84  8   -195.7733553942  4.0838E-04  0.0000E+00  2.7819E-01  1.0000E-04   
 mr-sdci # 84  9   -195.6288994298  1.0450E+00  0.0000E+00  9.6359E-01  1.0000E-04   
 mr-sdci # 84 10   -194.4766379379  2.3041E-01  0.0000E+00  2.2548E+00  1.0000E-04   
 mr-sdci # 84 11   -194.0415159086  2.3948E-01  0.0000E+00  3.4241E+00  1.0000E-04   
 mr-sdci # 84 12   -193.6670266964  6.0568E-02  0.0000E+00  3.4292E+00  1.0000E-04   
 mr-sdci # 84 13   -193.4356931315  3.0050E-02  0.0000E+00  3.1982E+00  1.0000E-04   
 mr-sdci # 84 14   -191.1490052162 -3.9154E-01  0.0000E+00  4.3965E+00  1.0000E-04   
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.009000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  85

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59964323
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426799
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426797
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616636
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616634
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.89721154
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88338024
   ht   9     0.00161267    -0.00135196    -0.00024023     0.00094910    -0.00161295    -0.00140986     0.00012739    -0.00023406
   ht  10     0.00233154     0.00085488     0.00292673    -0.00441925     0.00073044    -0.00112907    -0.00025682     0.00007109
   ht  11     0.00028009    -0.00095134     0.00073186     0.00038674     0.00039067    -0.00156560    -0.00008324    -0.00016092
   ht  12    -0.00096272     0.00268811    -0.00000529    -0.00066876     0.00069259    -0.00040348    -0.00011174     0.00044293
   ht  13    -0.00871676    -0.00420367    -0.00400697    -0.00304721     0.00109963    -0.00238432     0.00119470     0.00065786
   ht  14     0.00162422    -0.00096901    -0.00054280     0.00072248    -0.00120197    -0.00068207     0.00014021    -0.00009280
   ht  15     0.00144622     0.00068430     0.00103347    -0.00203654     0.00016108    -0.00035927    -0.00002223     0.00019948

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15
   ht   9    -0.00000054
   ht  10     0.00000002    -0.00000146
   ht  11    -0.00000006    -0.00000003    -0.00000029
   ht  12     0.00000009    -0.00000006     0.00000005    -0.00000075
   ht  13     0.00000020     0.00000031     0.00000004     0.00000008    -0.00000997
   ht  14    -0.00000011     0.00000003    -0.00000002     0.00000007     0.00000014    -0.00000028
   ht  15     0.00000001    -0.00000022    -0.00000002    -0.00000003     0.00000005    -0.00000001    -0.00000039

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7         v:   8

   eig(s)   2.968905E-09   4.224559E-09   4.478886E-09   7.009949E-09   1.185948E-08   1.600660E-08   1.549575E-07    1.00000    
 
   x:   1  -2.856866E-05   2.079470E-05   4.272638E-05   2.699979E-05   1.234720E-05   4.432580E-05   1.706832E-04  -3.264150E-02
   x:   2   3.566436E-05   1.283007E-05   8.660273E-06  -2.969667E-05  -4.705539E-05   1.068426E-05   8.313717E-05  -0.114455    
   x:   3   1.414860E-05  -7.819923E-06   3.336829E-05  -4.117028E-06   6.129892E-08   5.356651E-05   7.826732E-05   1.408124E-02
   x:   4  -3.253413E-05  -1.347590E-05  -4.476573E-05   1.865828E-05   1.449813E-05  -7.766100E-05   6.195194E-05   3.035138E-03
   x:   5   2.734981E-05  -1.580069E-05   4.755589E-06  -2.902165E-05  -7.958231E-06   1.122519E-05  -2.164764E-05  -0.107060    
   x:   6   2.244823E-05   2.062676E-05  -2.223381E-05  -2.282165E-05   1.377305E-05  -1.995543E-05   4.711313E-05   2.069369E-02
   x:   7  -3.137693E-06   1.879607E-06  -2.068990E-06   2.505222E-06   1.480165E-06  -4.977164E-06  -2.427904E-05  -0.772306    
   x:   8   5.029191E-06   2.656041E-06   2.316860E-06  -5.236393E-06  -8.175178E-06  -1.268779E-07  -1.342606E-05   0.614232    
   x:   9  -0.168074       3.385520E-02   3.574425E-03   0.973031      -0.151525       2.885310E-02   3.121880E-03   7.527574E-11
   x:  10   4.704426E-02   1.789116E-02   0.158489      -3.205120E-02  -6.319495E-02   0.983479      -9.779782E-03  -6.982372E-11
   x:  11  -0.279316      -0.887029       0.364883      -1.299781E-02   3.171130E-02  -2.759710E-02   9.337886E-03   1.380301E-11
   x:  12   8.133820E-02  -4.671096E-02   2.336446E-02  -0.135358      -0.983173      -7.425558E-02   1.380161E-02  -1.970592E-11
   x:  13  -2.784123E-03  -1.890278E-02  -2.178282E-02   1.549304E-03  -1.358191E-02  -6.773570E-03  -0.999464       6.873411E-14
   x:  14  -0.912552       0.338929       0.125269      -0.180112      -6.445522E-02   7.226839E-03  -6.050579E-03  -1.225866E-10
   x:  15   0.228355       0.307086       0.908304       3.534417E-02   3.273811E-02  -0.159883      -2.554658E-02   1.501928E-10

                v:   9         v:  10         v:  11         v:  12         v:  13         v:  14         v:  15

   eig(s)    1.00000        1.00000        1.00000        1.00000        1.00000        1.00000        1.00000    
 
   x:   1   7.642320E-02   0.404207      -0.183214       4.892855E-02   0.388562       1.034114E-03  -0.801728    
   x:   2   4.619573E-02  -8.435015E-02  -0.237437      -0.497865      -0.716996       0.178821      -0.356854    
   x:   3  -0.345684      -0.492292       0.590279       0.116686       3.833787E-02   0.349250      -0.390464    
   x:   4  -0.120424       1.161995E-02   0.390529      -0.222934      -0.116316      -0.861411      -0.166079    
   x:   5   0.140327       0.686665       0.574513       9.569757E-02  -0.297013       0.246781       9.485045E-02
   x:   6  -6.410391E-02  -4.372159E-02  -0.196734       0.816513      -0.471976      -0.196863      -0.163207    
   x:   7  -0.591518       0.135548      -0.142369      -1.893777E-04   6.115498E-02  -1.130141E-02   0.105545    
   x:   8  -0.695939       0.308580      -0.141685      -0.102811      -7.223342E-02   6.506227E-02   5.540965E-02
   x:   9   3.943624E-07   4.072521E-06   7.515227E-06   1.520678E-05  -5.230484E-05   2.528148E-05   1.573701E-05
   x:  10  -4.650417E-07   1.706298E-06  -8.289995E-07  -3.085497E-06  -2.399460E-05  -1.066395E-04   4.652419E-05
   x:  11   9.405111E-08  -2.231863E-06  -2.619334E-05   1.458604E-05  -2.775076E-05  -2.918239E-06  -7.892340E-07
   x:  12  -1.340183E-07   6.548274E-08   5.875871E-06   3.057163E-05   4.503874E-05  -2.638883E-05  -1.394511E-06
   x:  13  -4.980585E-10  -9.864026E-08   2.585282E-06   6.638052E-07  -1.293005E-05  -2.443160E-05  -2.217617E-04
   x:  14  -7.141496E-07  -4.119734E-06   1.322677E-05   6.424601E-06  -3.786374E-05   2.280536E-05   1.694060E-05
   x:  15   9.964985E-07  -3.548374E-06   9.381906E-06  -1.184213E-07  -8.963267E-06  -4.671333E-05   2.740055E-05
 bummer (warning):overlap matrix: # small eigenvalues=                      4

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   9.348201E-06   0.316924       0.797677      -0.122748       0.428468       6.611413E-02  -6.181743E-02  -8.893793E-02
 ref    2   9.197095E-06   0.570516      -0.219176       4.870635E-02  -0.114517       0.742129       4.846836E-02  -3.458065E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1   1.690728E-02  -2.580980E-02   4.121063E-03  -8.362073E-03   1.599993E-02   8.188215E-02  -0.109604    
 ref    2  -2.612160E-02  -3.251727E-02   7.626349E-02  -2.461969E-03  -3.072418E-02   5.753489E-02  -1.266484E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   1.719754E-10   0.425929       0.684326       1.743947E-02   0.196699       0.555127       6.170576E-03   9.105778E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15
 ref    1   9.681940E-04   1.723519E-03   5.833104E-03   7.598556E-05   1.199973E-03   1.001495E-02   1.217337E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00000935     0.31692379     0.79767662    -0.12274837     0.42846843     0.06611413    -0.06181743    -0.08893793
 ref:   2     0.00000920     0.57051588    -0.21917554     0.04870635    -0.11451735     0.74212947     0.04846836    -0.03458065

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15
 ref:   1     0.01690728    -0.02580980     0.00412106    -0.00836207     0.01599993     0.08188215    -0.10960372
 ref:   2    -0.02612160    -0.03251727     0.07626349    -0.00246197    -0.03072418     0.05753489    -0.01266484

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 85  1   -196.4879584084  3.9272E-10  0.0000E+00  5.3558E-04  1.0000E-04   
 mr-sdci # 85  2   -196.4725827989  6.6542E-11  0.0000E+00  4.9492E-05  1.0000E-04   
 mr-sdci # 85  3   -196.4725827988  2.0739E-10  0.0000E+00  4.4943E-05  1.0000E-04   
 mr-sdci # 85  4   -196.4725827983  1.1553E-08  0.0000E+00  7.0677E-05  1.0000E-04   
 mr-sdci # 85  5   -196.4644811586  5.7604E-11  0.0000E+00  7.1977E-05  1.0000E-04   
 mr-sdci # 85  6   -196.4644811550  6.3807E-12  4.4026E-09  1.0444E-04  1.0000E-04   
 mr-sdci # 85  7   -195.7943218588  4.0947E-03  0.0000E+00  1.9628E-01  1.0000E-04   
 mr-sdci # 85  8   -195.7735041037  1.4871E-04  0.0000E+00  2.7194E-01  1.0000E-04   
 mr-sdci # 85  9   -195.6465028648  1.7603E-02  0.0000E+00  8.4114E-01  1.0000E-04   
 mr-sdci # 85 10   -195.6079446791  1.1313E+00  0.0000E+00  8.8514E-01  1.0000E-04   
 mr-sdci # 85 11   -194.3949023635  3.5339E-01  0.0000E+00  2.4594E+00  1.0000E-04   
 mr-sdci # 85 12   -194.0331697594  3.6614E-01  0.0000E+00  3.2476E+00  1.0000E-04   
 mr-sdci # 85 13   -193.5284232707  9.2730E-02  0.0000E+00  2.9649E+00  1.0000E-04   
 mr-sdci # 85 14   -191.2485088882  9.9504E-02  0.0000E+00  4.5250E+00  1.0000E-04   
 mr-sdci # 85 15   -190.3904676614 -5.4667E-01  0.0000E+00  4.1307E+00  1.0000E-04   
 
 root number  6 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.017000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  86

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59964323
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426799
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426797
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616636
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616634
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.89721154
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88338024
   ht   9     0.00161267    -0.00135196    -0.00024023     0.00094910    -0.00161295    -0.00140986     0.00012739    -0.00023406
   ht  10     0.00233154     0.00085488     0.00292673    -0.00441925     0.00073044    -0.00112907    -0.00025682     0.00007109
   ht  11     0.00028009    -0.00095134     0.00073186     0.00038674     0.00039067    -0.00156560    -0.00008324    -0.00016092
   ht  12    -0.00096272     0.00268811    -0.00000529    -0.00066876     0.00069259    -0.00040348    -0.00011174     0.00044293
   ht  13    -0.00871676    -0.00420367    -0.00400697    -0.00304721     0.00109963    -0.00238432     0.00119470     0.00065786
   ht  14     0.00162422    -0.00096901    -0.00054280     0.00072248    -0.00120197    -0.00068207     0.00014021    -0.00009280
   ht  15     0.00144622     0.00068430     0.00103347    -0.00203654     0.00016108    -0.00035927    -0.00002223     0.00019948
   ht  16     0.00003168    -0.00160064     0.00053258     0.00021502    -0.00017646     0.00044022     0.00006750    -0.00012560

                ht   9         ht  10         ht  11         ht  12         ht  13         ht  14         ht  15         ht  16
   ht   9    -0.00000054
   ht  10     0.00000002    -0.00000146
   ht  11    -0.00000006    -0.00000003    -0.00000029
   ht  12     0.00000009    -0.00000006     0.00000005    -0.00000075
   ht  13     0.00000020     0.00000031     0.00000004     0.00000008    -0.00000997
   ht  14    -0.00000011     0.00000003    -0.00000002     0.00000007     0.00000014    -0.00000028
   ht  15     0.00000001    -0.00000022    -0.00000002    -0.00000003     0.00000005    -0.00000001    -0.00000039
   ht  16    -0.00000003     0.00000002    -0.00000001     0.00000007    -0.00000001    -0.00000002     0.00000002    -0.00000018

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7         v:   8

   eig(s)   2.265990E-09   3.050824E-09   4.249404E-09   4.479142E-09   7.017898E-09   1.187556E-08   1.600660E-08   1.549599E-07
 
   x:   1   8.155057E-06   2.787199E-05  -1.982834E-05  -4.290158E-05   2.692907E-05  -1.239005E-05  -4.432504E-05   1.706840E-04
   x:   2  -4.590751E-05  -2.161102E-05  -9.218863E-06  -8.424086E-06  -2.832039E-05   4.580070E-05  -1.067409E-05   8.301027E-05
   x:   3   5.316633E-06  -1.685911E-05   6.683991E-06  -3.342929E-05  -4.557548E-06   3.734712E-07  -5.357059E-05   7.830911E-05
   x:   4   1.739757E-05   2.798341E-05   1.289531E-05   4.488079E-05   1.856596E-05  -1.440536E-05   7.766049E-05   6.196795E-05
   x:   5  -1.189576E-05  -2.511265E-05   1.581660E-05  -4.581580E-06  -2.884817E-05   7.871157E-06  -1.122449E-05  -2.166116E-05
   x:   6  -1.525189E-06  -2.270077E-05  -2.210109E-05   2.189997E-05  -2.331399E-05  -1.332678E-05   1.995214E-05   4.714796E-05
   x:   7   2.363403E-06   2.541758E-06  -2.017778E-06   2.036142E-06   2.440131E-06  -1.424116E-06   4.976678E-06  -2.427343E-05
   x:   8  -4.750078E-06  -3.607313E-06  -2.347131E-06  -2.313643E-06  -5.124207E-06   8.084300E-06   1.274377E-07  -1.343591E-05
   x:   9   8.209996E-02   0.148053      -3.679495E-02  -4.354717E-03   0.972998       0.149666      -2.884959E-02   3.117568E-03
   x:  10  -2.252189E-02  -4.116370E-02  -1.635765E-02  -0.158662      -3.202083E-02   6.323588E-02  -0.983482      -9.778196E-03
   x:  11   0.178800       0.214131       0.890877      -0.355651      -9.714055E-03  -3.295375E-02   2.760620E-02   9.330888E-03
   x:  12  -6.448272E-02  -6.353664E-02   5.176021E-02  -2.233302E-02  -0.132020       0.982598       7.421809E-02   1.381323E-02
   x:  13   6.356903E-03   3.057492E-04   1.818739E-02   2.192985E-02   1.496179E-03   1.371709E-02   6.771532E-03  -0.999456    
   x:  14   0.250728       0.887015      -0.313380      -0.126663      -0.178594       6.407429E-02  -7.226572E-03  -6.052738E-03
   x:  15  -0.111309      -0.194677      -0.300332      -0.911680       3.332628E-02  -3.203275E-02   0.159880      -2.554319E-02
   x:  16   0.938792      -0.319065      -0.115330      -1.163741E-02  -4.143835E-02   4.119272E-02  -3.905286E-04   4.020087E-03

                v:   9         v:  10         v:  11         v:  12         v:  13         v:  14         v:  15         v:  16

   eig(s)    1.00000        1.00000        1.00000        1.00000        1.00000        1.00000        1.00000        1.00000    
 
   x:   1  -8.011492E-03   6.243265E-02   0.404344       0.205132       7.201708E-02  -0.378395      -6.346884E-03  -0.801284    
   x:   2  -9.800007E-02  -6.898221E-02  -0.113679       6.793234E-02   0.476917       0.754933       0.203461      -0.359623    
   x:   3  -8.194094E-02   0.127843      -0.533320      -0.547523      -0.343351      -9.004142E-02   0.339108      -0.389536    
   x:   4  -2.864261E-02   0.111659      -2.365576E-02  -0.440913       9.207897E-02   0.137004      -0.858560      -0.165448    
   x:   5  -6.127646E-02   6.592577E-02   0.733598      -0.504276      -0.227863       0.275265       0.250675       9.438612E-02
   x:   6   1.977029E-03   2.496435E-02  -1.629486E-02   0.423294      -0.761165       0.418621      -0.197276      -0.162469    
   x:   7  -0.899157       0.397927       2.376554E-03   0.131796       3.232403E-02  -5.865955E-02  -1.225110E-02   0.105638    
   x:   8   0.412984       0.893989      -1.248962E-02   9.098578E-02   9.304086E-02   7.456370E-02   6.735993E-02   5.513397E-02
   x:   9   1.229434E-07   6.075255E-07   4.863247E-06  -3.248585E-06  -2.104281E-05   5.018453E-05   2.619939E-05   1.564563E-05
   x:  10  -1.027096E-07   1.150846E-06   1.150786E-06  -1.361941E-07   2.875880E-06   2.681685E-05  -1.059535E-04   4.657602E-05
   x:  11   1.624878E-08  -5.516490E-07  -2.686832E-06   2.894062E-05  -8.614825E-06   2.752579E-05  -2.326025E-06  -8.363421E-07
   x:  12  -9.076598E-08  -1.469485E-06   3.306210E-06   4.161668E-06  -2.650339E-05  -4.673642E-05  -2.788114E-05  -1.215538E-06
   x:  13  -1.078126E-09  -3.789800E-08  -7.361512E-08  -2.453778E-06  -2.575100E-06   1.285037E-05  -2.439898E-05  -2.217574E-04
   x:  14  -2.339403E-07  -1.108827E-06  -3.357003E-06  -1.133844E-05  -1.377420E-05   3.617473E-05   2.345860E-05   1.687237E-05
   x:  15   2.229819E-07  -2.372313E-06  -2.476608E-06  -9.034827E-06  -2.824434E-06   9.730842E-06  -4.653862E-05   2.743964E-05
   x:  16  -8.145214E-08  -2.317607E-06   4.531891E-06   4.266465E-06   2.429037E-05   2.207504E-05   9.299298E-06  -4.334310E-06
 bummer (warning):overlap matrix: # small eigenvalues=                      5

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -7.010245E-06   0.250633      -0.821001      -0.122216       3.051459E-02  -0.432464      -6.084840E-02   8.915924E-02
 ref    2  -9.508042E-06   0.586569       0.172007       4.741921E-02  -0.749050      -5.285251E-02   4.836230E-02   3.446383E-02

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   1.291933E-02  -2.735784E-02   2.549558E-02   3.980470E-03   3.729371E-03   1.460455E-02   6.322327E-02  -0.145993    
 ref    2  -2.493257E-02  -3.107213E-02  -2.455908E-02   7.257880E-02  -3.010606E-02  -2.136232E-02   5.016474E-02  -2.632411E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   1.395464E-10   0.406881       0.703629       1.718536E-02   0.562007       0.189819       6.041439E-03   9.137126E-03

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16
 ref    1   7.885421E-04   1.713929E-03   1.253173E-03   5.283526E-03   9.202828E-04   6.696417E-04   6.513683E-03   2.200683E-02

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11        ev   12        ev   13        ev   14        ev   15        ev   16

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11       v     12       v     13       v     14       v     15       v     16

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00000701     0.25063334    -0.82100064    -0.12221613     0.03051459    -0.43246427    -0.06084840     0.08915924
 ref:   2    -0.00000951     0.58656921     0.17200749     0.04741921    -0.74905030    -0.05285251     0.04836230     0.03446383

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1     0.01291933    -0.02735784     0.02549558     0.00398047     0.00372937     0.01460455     0.06322327    -0.14599270
 ref:   2    -0.02493257    -0.03107213    -0.02455908     0.07257880    -0.03010606    -0.02136232     0.05016474    -0.02632411

 trial vector basis is being transformed.  new dimension:   8

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 86  1   -196.4879584130  4.6361E-09  9.5203E-08  5.4818E-04  1.0000E-04   
 mr-sdci # 86  2   -196.4725827989  9.6207E-12  0.0000E+00  5.1490E-05  1.0000E-04   
 mr-sdci # 86  3   -196.4725827988  3.8511E-12  0.0000E+00  4.3200E-05  1.0000E-04   
 mr-sdci # 86  4   -196.4725827983  1.3500E-13  0.0000E+00  7.0724E-05  1.0000E-04   
 mr-sdci # 86  5   -196.4644811595  8.6661E-10  0.0000E+00  7.3978E-05  1.0000E-04   
 mr-sdci # 86  6   -196.4644811586  3.5797E-09  0.0000E+00  7.1284E-05  1.0000E-04   
 mr-sdci # 86  7   -195.7946677737  3.4591E-04  0.0000E+00  1.9822E-01  1.0000E-04   
 mr-sdci # 86  8   -195.7735093356  5.2319E-06  0.0000E+00  2.7258E-01  1.0000E-04   
 mr-sdci # 86  9   -195.6497412142  3.2383E-03  0.0000E+00  8.6485E-01  1.0000E-04   
 mr-sdci # 86 10   -195.6083254783  3.8080E-04  0.0000E+00  8.8834E-01  1.0000E-04   
 mr-sdci # 86 11   -195.2983283198  9.0343E-01  0.0000E+00  1.5656E+00  1.0000E-04   
 mr-sdci # 86 12   -194.3633124230  3.3014E-01  0.0000E+00  2.6235E+00  1.0000E-04   
 mr-sdci # 86 13   -193.6552163785  1.2679E-01  0.0000E+00  3.6548E+00  1.0000E-04   
 mr-sdci # 86 14   -192.7742843818  1.5258E+00  0.0000E+00  2.5639E+00  1.0000E-04   
 mr-sdci # 86 15   -191.0292608162  6.3879E-01  0.0000E+00  4.4934E+00  1.0000E-04   
 mr-sdci # 86 16   -190.1274970944  1.8806E-01  0.0000E+00  4.2105E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.014000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  87

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59964362
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426800
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426800
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616636
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616636
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.90635298
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88519454
   ht   9     0.00679878     0.00392924     0.00324573     0.00053413     0.00305538     0.00317463     0.00031249    -0.00014703

                ht   9
   ht   9    -0.00000425

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   2.065912E-06  -0.196180       0.831488       0.148109      -4.780574E-03   0.433513       6.092343E-02  -8.853846E-02
 ref    2   7.654981E-06  -0.592255      -0.121622      -0.101692       0.750867       8.280290E-03  -4.833240E-02  -3.432385E-02

              v      9
 ref    1   3.691148E-02
 ref    2   1.322502E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   6.286673E-11   0.389252       0.706165       3.227764E-02   0.563824       0.188002       6.047686E-03   9.017185E-03

              v      9
 ref    1   1.537359E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00000207    -0.19618048     0.83148827     0.14810922    -0.00478057     0.43351322     0.06092343    -0.08853846
 ref:   2     0.00000765    -0.59225452    -0.12162181    -0.10169218     0.75086703     0.00828029    -0.04833240    -0.03432385

                ci   9
 ref:   1     0.03691148
 ref:   2     0.01322502

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 87  1   -196.4879584790  6.6014E-08  3.4417E-08  3.0676E-04  1.0000E-04   
 mr-sdci # 87  2   -196.4725827989  1.1326E-11  0.0000E+00  5.0687E-05  1.0000E-04   
 mr-sdci # 87  3   -196.4725827988  3.6451E-12  0.0000E+00  4.0280E-05  1.0000E-04   
 mr-sdci # 87  4   -196.4725827984  1.5839E-10  0.0000E+00  7.0208E-05  1.0000E-04   
 mr-sdci # 87  5   -196.4644811596  1.2851E-10  0.0000E+00  6.6933E-05  1.0000E-04   
 mr-sdci # 87  6   -196.4644811586  2.4997E-11  0.0000E+00  7.0880E-05  1.0000E-04   
 mr-sdci # 87  7   -195.7946690913  1.3176E-06  0.0000E+00  1.9778E-01  1.0000E-04   
 mr-sdci # 87  8   -195.7739677308  4.5840E-04  0.0000E+00  2.7549E-01  1.0000E-04   
 mr-sdci # 87  9   -193.7815697113 -1.8682E+00  0.0000E+00  3.6065E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  88

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59964362
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426800
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426800
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616636
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616636
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.90635298
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88519454
   ht   9     0.00679878     0.00392924     0.00324573     0.00053413     0.00305538     0.00317463     0.00031249    -0.00014703
   ht  10     0.00409759     0.00282140     0.00123185    -0.00050268     0.00129502     0.00156769     0.00049175     0.00031969

                ht   9         ht  10
   ht   9    -0.00000425
   ht  10    -0.00000076    -0.00000150

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   4.255366E-07  -0.192809       0.834342       0.135994       4.778311E-03   0.433513       5.919996E-02  -8.942944E-02
 ref    2  -2.374043E-06  -0.592901      -0.120924      -9.871350E-02  -0.750867       8.276427E-03  -4.779223E-02  -3.142865E-02

              v      9       v     10
 ref    1  -2.310615E-02   3.104025E-02
 ref    2  -3.337769E-02  -1.450565E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   5.817161E-12   0.388707       0.710749       2.823866E-02   0.563824       0.188002       5.788732E-03   8.985384E-03

              v      9       v     10
 ref    1   1.647964E-03   1.173911E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.00000043    -0.19280852     0.83434194     0.13599376     0.00477831     0.43351324     0.05919996    -0.08942944
 ref:   2    -0.00000237    -0.59290102    -0.12092379    -0.09871350    -0.75086714     0.00827643    -0.04779223    -0.03142865

                ci   9         ci  10
 ref:   1    -0.02310615     0.03104025
 ref:   2    -0.03337769    -0.01450565

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 88  1   -196.4879585147  3.5675E-08  1.1797E-08  1.9629E-04  1.0000E-04   
 mr-sdci # 88  2   -196.4725827989  2.8422E-14  0.0000E+00  5.0750E-05  1.0000E-04   
 mr-sdci # 88  3   -196.4725827988  1.9114E-12  0.0000E+00  4.0121E-05  1.0000E-04   
 mr-sdci # 88  4   -196.4725827984  1.6229E-11  0.0000E+00  6.8845E-05  1.0000E-04   
 mr-sdci # 88  5   -196.4644811596  6.1604E-12  0.0000E+00  6.6298E-05  1.0000E-04   
 mr-sdci # 88  6   -196.4644811586 -2.8422E-14  0.0000E+00  7.0881E-05  1.0000E-04   
 mr-sdci # 88  7   -195.7953460454  6.7695E-04  0.0000E+00  2.0935E-01  1.0000E-04   
 mr-sdci # 88  8   -195.7745381360  5.7041E-04  0.0000E+00  2.7956E-01  1.0000E-04   
 mr-sdci # 88  9   -195.5106888454  1.7291E+00  0.0000E+00  1.2833E+00  1.0000E-04   
 mr-sdci # 88 10   -191.4385681756 -4.1698E+00  0.0000E+00  4.3182E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.013000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  89

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       15006 2x:        4440 4x:         784
All internal counts: zz :          15 yy:        1208 xx:        9888 ww:        5992
One-external counts: yz :         471 yx:       13100 yw:       10700
Two-external counts: yy :        1940 ww:        7240 xx:       12440 xz:         150 wz:         160 wx:       11960
Three-ext.   counts: yx :        5405 yw:        4633

SO-0ex       counts: zz :          12 yy:         768 xx:        5648 ww:        3704
SO-1ex       counts: yz :         153 yx:        4440 yw:        3300
SO-2ex       counts: yy :         306 xx:       10800 wx:       24960
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:        1255
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1   -50.59964362
   ht   2     0.00000000   -50.58426800
   ht   3     0.00000000     0.00000000   -50.58426800
   ht   4     0.00000000     0.00000000     0.00000000   -50.58426800
   ht   5     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616636
   ht   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -50.57616636
   ht   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.90635298
   ht   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000   -49.88519454
   ht   9     0.00679878     0.00392924     0.00324573     0.00053413     0.00305538     0.00317463     0.00031249    -0.00014703
   ht  10     0.00409759     0.00282140     0.00123185    -0.00050268     0.00129502     0.00156769     0.00049175     0.00031969
   ht  11     0.00248450    -0.00020225    -0.00101671    -0.00081052    -0.00038976     0.00011810    -0.00003136     0.00009265

                ht   9         ht  10         ht  11
   ht   9    -0.00000425
   ht  10    -0.00000076    -0.00000150
   ht  11    -0.00000036    -0.00000013    -0.00000045

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7         v:   8

   eig(s)   5.975479E-09   1.804696E-08   5.147431E-08    1.00000        1.00000        1.00000        1.00000        1.00000    
 
   x:   1   4.401152E-05   9.696701E-05  -1.252334E-04  -1.060734E-02  -0.155768       0.160312      -9.617604E-02   5.930712E-02
   x:   2  -6.636059E-06   6.558965E-05  -6.938790E-05  -9.029275E-02  -0.266591      -0.298739      -0.253132      -0.171574    
   x:   3  -2.284294E-05   3.299028E-05  -5.919458E-05   0.481261      -0.186211       0.505734       0.190143      -3.979771E-02
   x:   4  -1.683360E-05  -8.135797E-06  -1.083659E-05  -0.472870      -8.834098E-02   0.109465      -0.641287      -0.101379    
   x:   5  -1.023647E-05   3.348828E-05  -5.602063E-05  -0.187061       0.160389      -0.385124       0.242883       0.716624    
   x:   6  -1.923062E-07   3.894009E-05  -5.822240E-05  -1.970701E-02   0.731766      -0.177000       0.136295      -0.528987    
   x:   7  -6.979396E-07   1.066165E-05  -4.876658E-06  -0.667962       9.030825E-02   0.560909       0.375471       2.124999E-02
   x:   8   2.201589E-06   5.886504E-06   3.779295E-06   0.234443       0.541133       0.350767      -0.510612       0.401690    
   x:   9  -5.372872E-02   0.131701      -0.989832      -2.193387E-13  -6.554337E-12   2.914847E-14   7.961149E-13   1.284717E-12
   x:  10   2.740357E-02   0.991085       0.130380       7.107160E-13   1.150168E-11  -1.008964E-13  -4.214056E-13  -3.600915E-12
   x:  11   0.998179      -2.011978E-02  -5.685881E-02    0.00000      -2.666463E-12    0.00000        0.00000       2.314016E-12

                v:   9         v:  10         v:  11

   eig(s)    1.00000        1.00000        1.00000    
 
   x:   1  -0.270120       0.567161       0.736560    
   x:   2   0.707069      -0.234427       0.427920    
   x:   3  -0.109010      -0.584070       0.295274    
   x:   4  -0.434523      -0.382319       1.014672E-02
   x:   5  -0.151522      -0.333474       0.290272    
   x:   6  -0.119289      -0.139740       0.317239    
   x:   7   0.295701      -4.379889E-03   4.652866E-02
   x:   8   0.319010       7.290758E-02   3.305680E-03
   x:   9   8.726276E-06   1.268561E-05  -1.889917E-04
   x:  10  -1.661766E-05  -9.981485E-06  -1.083529E-04
   x:  11   5.640407E-06  -4.903316E-05  -2.683801E-05
 bummer (warning):overlap matrix: # small eigenvalues=                      1

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -3.646432E-07  -0.167005       0.848814       5.847398E-02   3.125942E-04   0.433539       4.963915E-02   9.198892E-02
 ref    2   4.028404E-07  -0.597862      -0.112368      -7.637572E-02  -0.750912       5.415283E-04  -3.972804E-02   1.093977E-02

              v      9       v     10       v     11
 ref    1   2.929213E-02  -3.189382E-02  -2.205924E-02
 ref    2   5.347860E-02   8.160066E-03   1.140969E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   2.952450E-13   0.385329       0.733113       9.252457E-03   0.563870       0.187957       4.042363E-03   8.581640E-03

              v      9       v     10       v     11
 ref    1   3.717990E-03   1.083803E-03   6.167910E-04

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6        ev    7        ev    8

               ev    9        ev   10        ev   11

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8

              v      9       v     10       v     11

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.00000036    -0.16700503     0.84881448     0.05847398     0.00031259     0.43353946     0.04963915     0.09198892
 ref:   2     0.00000040    -0.59786179    -0.11236832    -0.07637572    -0.75091245     0.00054153    -0.03972804     0.01093977

                ci   9         ci  10         ci  11
 ref:   1     0.02929213    -0.03189382    -0.02205924
 ref:   2     0.05347860     0.00816007     0.01140969

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 89  1   -196.4879585227  7.9291E-09  0.0000E+00  7.9099E-05  1.0000E-04   
 mr-sdci # 89  2   -196.4725827989  5.5422E-13  6.2966E-10  5.1036E-05  1.0000E-04   
 mr-sdci # 89  3   -196.4725827988  9.3436E-12  0.0000E+00  4.0014E-05  1.0000E-04   
 mr-sdci # 89  4   -196.4725827985  1.0132E-10  0.0000E+00  6.4717E-05  1.0000E-04   
 mr-sdci # 89  5   -196.4644811596  2.6247E-11  0.0000E+00  6.3866E-05  1.0000E-04   
 mr-sdci # 89  6   -196.4644811586  4.2988E-12  0.0000E+00  7.1060E-05  1.0000E-04   
 mr-sdci # 89  7   -195.7994211919  4.0751E-03  0.0000E+00  2.1742E-01  1.0000E-04   
 mr-sdci # 89  8   -195.7774490662  2.9109E-03  0.0000E+00  2.8217E-01  1.0000E-04   
 mr-sdci # 89  9   -195.7457309007  2.3504E-01  0.0000E+00  5.0618E-01  1.0000E-04   
 mr-sdci # 89 10   -192.7213708331  1.2828E+00  0.0000E+00  3.3129E+00  1.0000E-04   
 mr-sdci # 89 11   -191.2680837597 -4.0302E+00  0.0000E+00  4.3298E+00  1.0000E-04   
 
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.006000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mr-sdci  convergence criteria satisfied after 89 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 89  1   -196.4879585227  7.9291E-09  0.0000E+00  7.9099E-05  1.0000E-04   
 mr-sdci # 89  2   -196.4725827989  5.5422E-13  6.2966E-10  5.1036E-05  1.0000E-04   
 mr-sdci # 89  3   -196.4725827988  9.3436E-12  0.0000E+00  4.0014E-05  1.0000E-04   
 mr-sdci # 89  4   -196.4725827985  1.0132E-10  0.0000E+00  6.4717E-05  1.0000E-04   
 mr-sdci # 89  5   -196.4644811596  2.6247E-11  0.0000E+00  6.3866E-05  1.0000E-04   
 mr-sdci # 89  6   -196.4644811586  4.2988E-12  0.0000E+00  7.1060E-05  1.0000E-04   
 mr-sdci # 89  7   -195.7994211919  4.0751E-03  0.0000E+00  2.1742E-01  1.0000E-04   
 mr-sdci # 89  8   -195.7774490662  2.9109E-03  0.0000E+00  2.8217E-01  1.0000E-04   
 mr-sdci # 89  9   -195.7457309007  2.3504E-01  0.0000E+00  5.0618E-01  1.0000E-04   
 mr-sdci # 89 10   -192.7213708331  1.2828E+00  0.0000E+00  3.3129E+00  1.0000E-04   
 mr-sdci # 89 11   -191.2680837597 -4.0302E+00  0.0000E+00  4.3298E+00  1.0000E-04   

####################CIUDGINFO####################

   ci vector at position   1 energy= -196.487958522650
   ci vector at position   2 energy= -196.472582798886
   ci vector at position   3 energy= -196.472582798811
   ci vector at position   4 energy= -196.472582798533
   ci vector at position   5 energy= -196.464481159639
   ci vector at position   6 energy= -196.464481158605

################END OF CIUDGINFO################

 
    6 of the  12 expansion vectors are transformed.
    6 of the  11 matrix-vector products are transformed.

    6 expansion eigenvectors written to unit nvfile (= 11)
    6 matrix-vector products written to unit nhvfil (= 10)
maximum overlap with reference    2(overlap= 0.00000)
weight of reference states=  0.0000

 information on vector: 1 from unit 11 written to unit 48 filename civout              
maximum overlap with reference    2(overlap= 0.59786)
weight of reference states=  0.3853

 information on vector: 2 from unit 11 written to unit 48 filename civout              
maximum overlap with reference    1(overlap= 0.84881)
weight of reference states=  0.7331

 information on vector: 3 from unit 11 written to unit 48 filename civout              
maximum overlap with reference    2(overlap= 0.07638)
weight of reference states=  0.0093

 information on vector: 4 from unit 11 written to unit 48 filename civout              
maximum overlap with reference    2(overlap= 0.75091)
weight of reference states=  0.5639

 information on vector: 5 from unit 11 written to unit 48 filename civout              
maximum overlap with reference    1(overlap= 0.43354)
weight of reference states=  0.1880

 information on vector: 6 from unit 11 written to unit 48 filename civout              


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =      -196.4879585227

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     2    3    4   11   14   17

                                         symmetry   ag   ag   ag   b1g  b2g  b3g

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z   3  2       4 -0.657613                        +-   +-   +    +-   +-   +-   +  
 z   3  2       5 -0.355186                        +-   +    +-   +-   +-   +-   +  
 z   3  2       6 -0.594431                        +    +-   +-   +-   +-   +-   +  
 y   3  2     123 -0.024164              1( b3g)   +-   +-    -   +-   +-   +    +  
 y   3  2     135  0.027197              1( b2g)   +-   +-    -   +-   +    +-   +  
 y   3  2     143 -0.024599              1( b1g)   +-   +-    -   +    +-   +-   +  
 y   3  2     159  0.059759              1( b3g)   +-   +-   +    +-   +-    -   +  
 y   3  2     160 -0.010107              2( b3g)   +-   +-   +    +-   +-    -   +  
 y   3  2     171 -0.054503              1( b2g)   +-   +-   +    +-    -   +-   +  
 y   3  2     179  0.059004              1( b1g)   +-   +-   +     -   +-   +-   +  
 y   3  2     191 -0.019418              1( ag )   +-   +-        +-   +-   +-   +  
 y   3  2     192  0.036756              2( ag )   +-   +-        +-   +-   +-   +  
 y   3  2     193  0.016042              3( ag )   +-   +-        +-   +-   +-   +  
 y   3  2     207 -0.016015              1( b3g)   +-    -   +-   +-   +-   +    +  
 y   3  2     219  0.013058              1( b2g)   +-    -   +-   +-   +    +-   +  
 y   3  2     227 -0.011953              1( b1g)   +-    -   +-   +    +-   +-   +  
 y   3  2     239 -0.010009              1( ag )   +-    -   +    +-   +-   +-   +  
 y   3  2     241 -0.060667              3( ag )   +-    -   +    +-   +-   +-   +  
 y   3  2     255  0.027145              1( b3g)   +-   +    +-   +-   +-    -   +  
 y   3  2     267 -0.032264              1( b2g)   +-   +    +-   +-    -   +-   +  
 y   3  2     275  0.034181              1( b1g)   +-   +    +-    -   +-   +-   +  
 y   3  2     287 -0.017338              1( ag )   +-   +     -   +-   +-   +-   +  
 y   3  2     288  0.019568              2( ag )   +-   +     -   +-   +-   +-   +  
 y   3  2     301 -0.028776              3( ag )   +-        +-   +-   +-   +-   +  
 y   3  2     315 -0.022769              1( b3g)    -   +-   +-   +-   +-   +    +  
 y   3  2     327  0.021179              1( b2g)    -   +-   +-   +-   +    +-   +  
 y   3  2     335 -0.024715              1( b1g)    -   +-   +-   +    +-   +-   +  
 y   3  2     347 -0.016751              1( ag )    -   +-   +    +-   +-   +-   +  
 y   3  2     348 -0.063253              2( ag )    -   +-   +    +-   +-   +-   +  
 y   3  2     350 -0.011173              4( ag )    -   +-   +    +-   +-   +-   +  
 y   3  2     360 -0.024558              2( ag )    -   +    +-   +-   +-   +-   +  
 y   3  2     361  0.027863              3( ag )    -   +    +-   +-   +-   +-   +  
 y   3  2     375  0.052412              1( b3g)   +    +-   +-   +-   +-    -   +  
 y   3  2     387 -0.055165              1( b2g)   +    +-   +-   +-    -   +-   +  
 y   3  2     395  0.049042              1( b1g)   +    +-   +-    -   +-   +-   +  
 y   3  2     407 -0.029009              1( ag )   +    +-    -   +-   +-   +-   +  
 y   3  2     408  0.027297              2( ag )   +    +-    -   +-   +-   +-   +  
 y   3  2     409  0.015576              3( ag )   +    +-    -   +-   +-   +-   +  
 y   3  2     419 -0.015668              1( ag )   +     -   +-   +-   +-   +-   +  
 y   3  2     421 -0.048981              3( ag )   +     -   +-   +-   +-   +-   +  
 y   3  2     431 -0.014615              1( ag )        +-   +-   +-   +-   +-   +  
 y   3  2     432 -0.035623              2( ag )        +-   +-   +-   +-   +-   +  
 y   3  2     433  0.012734              3( ag )        +-   +-   +-   +-   +-   +  
 x   3  2    3891  0.012052    1( b2g)   1( b3g)   +-   +-   +    +-    -    -   +  
 x   3  2    4079 -0.012196    1( b1g)   1( b3g)   +-   +-   +     -   +-    -   +  
 x   3  2    4253  0.012027    1( b1g)   1( b2g)   +-   +-   +     -    -   +-   +  
 x   3  2    6859  0.010407    3( ag )   1( b2g)   +-    -    -   +-   +    +-   +  
 x   3  2    7235  0.011551    3( ag )   1( b3g)   +-    -   +    +-   +-    -   +  
 x   3  2    7417 -0.015930    3( ag )   1( b2g)   +-    -   +    +-    -   +-   +  
 x   3  2    7425 -0.010264    5( ag )   2( b2g)   +-    -   +    +-    -   +-   +  
 x   3  2    7454 -0.011580    4( b1u)   4( b3u)   +-    -   +    +-    -   +-   +  
 x   3  2    7601  0.014378    3( ag )   1( b1g)   +-    -   +     -   +-   +-   +  
 x   3  2   11512 -0.013223    2( ag )   1( b3g)    -   +-    -   +-   +-   +    +  
 x   3  2   11878 -0.011440    2( ag )   1( b1g)    -   +-    -   +    +-   +-   +  
 x   3  2   12070  0.016119    2( ag )   1( b3g)    -   +-   +    +-   +-    -   +  
 x   3  2   12078 -0.010127    4( ag )   2( b3g)    -   +-   +    +-   +-    -   +  
 x   3  2   12105  0.010821    5( b1u)   5( b2u)    -   +-   +    +-   +-    -   +  
 x   3  2   12252 -0.011729    2( ag )   1( b2g)    -   +-   +    +-    -   +-   +  
 x   3  2   12436  0.013140    2( ag )   1( b1g)    -   +-   +     -   +-   +-   +  
 x   3  2   13367 -0.014710    2( ag )   3( ag )    -    -   +    +-   +-   +-   +  
 x   3  2   14493  0.011032    1( b2g)   1( b3g)   +    +-   +-   +-    -    -   +  
 x   3  2   14681 -0.010835    1( b1g)   1( b3g)   +    +-   +-    -   +-    -   +  
 x   3  2   14855  0.010924    1( b1g)   1( b2g)   +    +-   +-    -    -   +-   +  
 x   3  2   15787 -0.013110    3( ag )   1( b2g)   +     -   +-   +-    -   +-   +  
 x   3  2   15971  0.011271    3( ag )   1( b1g)   +     -   +-    -   +-   +-   +  
 w   3  2   19010 -0.016000    1( b3g)   1( b3g)   +-   +-   +    +-   +-        +  
 w   3  2   19011  0.010283    1( b3g)   2( b3g)   +-   +-   +    +-   +-        +  
 w   3  2   19423  0.012222    1( b2g)   1( b3g)   +-   +-   +    +-   +     -   +  
 w   3  2   19649 -0.015871    1( b2g)   1( b2g)   +-   +-   +    +-        +-   +  
 w   3  2   19650  0.010303    1( b2g)   2( b2g)   +-   +-   +    +-        +-   +  
 w   3  2   19651 -0.010024    2( b2g)   2( b2g)   +-   +-   +    +-        +-   +  
 w   3  2   20309 -0.012225    1( b1g)   1( b3g)   +-   +-   +    +    +-    -   +  
 w   3  2   20511  0.012118    1( b1g)   1( b2g)   +-   +-   +    +     -   +-   +  
 w   3  2   20716 -0.015982    1( b1g)   1( b1g)   +-   +-   +         +-   +-   +  
 w   3  2   20717  0.010286    1( b1g)   2( b1g)   +-   +-   +         +-   +-   +  
 w   3  2   24155 -0.012922    3( ag )   1( b3g)   +-   +    +    +-   +-    -   +  
 w   3  2   24365  0.014082    3( ag )   1( b2g)   +-   +    +    +-    -   +-   +  
 w   3  2   24549 -0.013954    3( ag )   1( b1g)   +-   +    +     -   +-   +-   +  
 w   3  2   25622  0.016489    3( ag )   3( ag )   +-        +    +-   +-   +-   +  
 w   3  2   25629 -0.010329    3( ag )   5( ag )   +-        +    +-   +-   +-   +  
 w   3  2   25631  0.010110    5( ag )   5( ag )   +-        +    +-   +-   +-   +  
 w   3  2   25679  0.011292    1( b3u)   2( b3u)   +-        +    +-   +-   +-   +  
 w   3  2   25680  0.010277    2( b3u)   2( b3u)   +-        +    +-   +-   +-   +  
 w   3  2   25858 -0.014424    1( b3g)   1( b3g)   +    +-   +-   +-   +-        +  
 w   3  2   26271  0.011020    1( b2g)   1( b3g)   +    +-   +-   +-   +     -   +  
 w   3  2   26497 -0.014492    1( b2g)   1( b2g)   +    +-   +-   +-        +-   +  
 w   3  2   27157 -0.011015    1( b1g)   1( b3g)   +    +-   +-   +    +-    -   +  
 w   3  2   27359  0.011071    1( b1g)   1( b2g)   +    +-   +-   +     -   +-   +  
 w   3  2   27564 -0.014341    1( b1g)   1( b1g)   +    +-   +-        +-   +-   +  
 w   3  2   27803 -0.012353    1( b1u)   1( b2u)   +    +-    -   +-   +-   +    +  
 w   3  2   27804 -0.010847    2( b1u)   1( b2u)   +    +-    -   +-   +-   +    +  
 w   3  2   27808 -0.010908    1( b1u)   2( b2u)   +    +-    -   +-   +-   +    +  
 w   3  2   27809 -0.011168    2( b1u)   2( b2u)   +    +-    -   +-   +-   +    +  
 w   3  2   28022  0.011881    1( b1u)   1( b3u)   +    +-    -   +-   +    +-   +  
 w   3  2   28023  0.010152    2( b1u)   1( b3u)   +    +-    -   +-   +    +-   +  
 w   3  2   28027  0.010262    1( b1u)   2( b3u)   +    +-    -   +-   +    +-   +  
 w   3  2   28028  0.010125    2( b1u)   2( b3u)   +    +-    -   +-   +    +-   +  
 w   3  2   28206 -0.011316    1( b2u)   1( b3u)   +    +-    -   +    +-   +-   +  
 w   3  2   28434 -0.017675    2( ag )   1( b3g)   +    +-   +    +-   +-    -   +  
 w   3  2   28442  0.010832    4( ag )   2( b3g)   +    +-   +    +-   +-    -   +  
 w   3  2   28644  0.016087    2( ag )   1( b2g)   +    +-   +    +-    -   +-   +  
 w   3  2   28652 -0.010062    4( ag )   2( b2g)   +    +-   +    +-    -   +-   +  
 w   3  2   28828 -0.016807    2( ag )   1( b1g)   +    +-   +     -   +-   +-   +  
 w   3  2   28836  0.010438    4( ag )   2( b1g)   +    +-   +     -   +-   +-   +  
 w   3  2   29043 -0.014864    2( ag )   2( ag )   +    +-        +-   +-   +-   +  
 w   3  2   29087 -0.012371    1( b2u)   1( b2u)   +    +-        +-   +-   +-   +  
 w   3  2   29088 -0.017193    1( b2u)   2( b2u)   +    +-        +-   +-   +-   +  
 w   3  2   29089 -0.014227    2( b2u)   2( b2u)   +    +-        +-   +-   +-   +  
 w   3  2   29901  0.013543    2( ag )   3( ag )   +     -   +    +-   +-   +-   +  
 w   3  2   30147  0.011521    3( ag )   1( b3g)   +    +    +-   +-   +-    -   +  
 w   3  2   30357 -0.012722    3( ag )   1( b2g)   +    +    +-   +-    -   +-   +  
 w   3  2   30541  0.011689    3( ag )   1( b1g)   +    +    +-    -   +-   +-   +  
 w   3  2   30972 -0.014430    3( ag )   3( ag )   +         +-   +-   +-   +-   +  
 w   3  2   31825  0.015550    2( ag )   2( ag )        +-   +    +-   +-   +-   +  
 w   3  2   31830  0.010489    2( ag )   4( ag )        +-   +    +-   +-   +-   +  
 w   3  2   31832  0.010089    4( ag )   4( ag )        +-   +    +-   +-   +-   +  
 w   3  2   31855  0.011634    1( b1u)   2( b1u)        +-   +    +-   +-   +-   +  
 w   3  2   32069  0.011843    1( b1u)   2( b1u)        +    +-   +-   +-   +-   +  

 ci coefficient statistics:
           rq > 0.1                3
      0.1> rq > 0.01             115
     0.01> rq > 0.001           2585
    0.001> rq > 0.0001          3008
   0.0001> rq > 0.00001         3848
  0.00001> rq > 0.000001        8692
 0.000001> rq                  13907
           all                 32158
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:         126 2x:           0 4x:           0
All internal counts: zz :          15 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :          12 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:         193
  iref  icsf         v(icsf)             hv(icsf)
     1     1     -0.000000542693      0.000106456264
     2     2      0.000000027010     -0.000005297416

 number of reference csfs (nref) is     2.  root number (iroot) is  1.
 c0**2 =   0.00000000  c**2 (all zwalks) =   0.91196013

 warning: cnot**2 < 0.5, dv3 is not computed.

 warning: pople ci energy extrapolation is not computed because of small fac.
 fac =      -0.999999999999

 eref      =   -196.162936247727   "relaxed" cnot**2         =   0.000000000000
 eci       =   -196.487958522650   deltae = eci - eref       =  -0.325022274923
 eci+dv1   =   -196.812980797574   dv1 = (1-cnot**2)*deltae  =  -0.325022274923
 eci+dv2   =********************   dv2 = dv1 / cnot**2       =*****************
 eci+dv3   =   -196.487958522650   dv3 = dv1 / (2*cnot**2-1) =   0.000000000000
 eci+pople =   -196.162936247727   ( 12e- scaled deltae )    =   0.000000000000


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 2) =      -196.4725827989

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     2    3    4   11   14   17

                                         symmetry   ag   ag   ag   b1g  b2g  b3g

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  3  1       1  0.304662                        +-   +-   +-   +-   +-   +    +  
 z*  1  1       2 -0.540843                        +-   +-   +-   +-   +    +-    - 
 z   3  3       3 -0.016077                        +-   +-   +-   +    +-   +-   +  
 z   3  2       4 -0.112177                        +-   +-   +    +-   +-   +-   +  
 z   3  2       5 -0.570438                        +-   +    +-   +-   +-   +-   +  
 z   3  2       6  0.464948                        +    +-   +-   +-   +-   +-   +  
 y   1  1      43  0.011021              1( b2g)   +-   +-   +-   +-        +-    - 
 y   1  1     127  0.012513              1( ag )   +-   +-    -   +-   +    +-    - 
 y   3  2     287  0.015077              1( ag )   +-   +     -   +-   +-   +-   +  
 y   1  1     319  0.010844              1( ag )    -   +-   +-   +-   +    +-    - 
 y   3  2     335  0.013616              1( b1g)    -   +-   +-   +    +-   +-   +  
 y   3  2     359  0.012118              1( ag )    -   +    +-   +-   +-   +-   +  
 y   3  2     407 -0.010430              1( ag )   +    +-    -   +-   +-   +-   +  
 y   3  2     431 -0.011349              1( ag )        +-   +-   +-   +-   +-   +  
 x   1  1    1195 -0.010108    1( b1g)   1( b3g)   +-   +-   +-    -   +     -    - 
 x   1  1    5835  0.011538    3( ag )   1( b1g)   +-    -   +-    -   +    +-    - 
 w   1  1   17206 -0.010821    1( b3g)   1( b3g)   +-   +-   +-   +-   +          - 
 w   1  1   18698  0.010810    1( b1g)   1( b1g)   +-   +-   +-        +    +-    - 
 w   3  1   20909 -0.012128    1( b2u)   1( b2u)   +-   +-        +-   +-   +    +  
 w   3  1   20910 -0.012208    1( b2u)   2( b2u)   +-   +-        +-   +-   +    +  
 w   1  1   21062  0.014037    1( b1u)   1( b1u)   +-   +-        +-   +    +-    - 
 w   1  1   21063  0.010319    1( b1u)   2( b1u)   +-   +-        +-   +    +-    - 
 w   1  1   21077  0.021029    1( b2u)   1( b2u)   +-   +-        +-   +    +-    - 
 w   1  1   21078  0.021164    1( b2u)   2( b2u)   +-   +-        +-   +    +-    - 
 w   1  1   21079  0.014944    2( b2u)   2( b2u)   +-   +-        +-   +    +-    - 
 w   1  1   21092  0.013036    1( b3u)   1( b3u)   +-   +-        +-   +    +-    - 
 w   3  2   21578 -0.011394    1( b3g)   1( b3g)   +-   +    +-   +-   +-        +  
 w   3  2   22217 -0.010576    1( b2g)   1( b2g)   +-   +    +-   +-        +-   +  
 w   3  2   23284 -0.011500    1( b1g)   1( b1g)   +-   +    +-        +-   +-   +  
 w   1  1   23645  0.012235    1( b2u)   1( b2u)   +-   +     -   +-   +    +-    - 
 w   1  1   23660  0.014748    1( b3u)   1( b3u)   +-   +     -   +-   +    +-    - 
 w   1  1   23661  0.013047    1( b3u)   2( b3u)   +-   +     -   +-   +    +-    - 
 w   3  2   24792 -0.013482    1( b1u)   1( b1u)   +-   +         +-   +-   +-   +  
 w   3  2   24793 -0.010110    1( b1u)   2( b1u)   +-   +         +-   +-   +-   +  
 w   3  2   24807 -0.020192    1( b2u)   1( b2u)   +-   +         +-   +-   +-   +  
 w   3  2   24808 -0.020698    1( b2u)   2( b2u)   +-   +         +-   +-   +-   +  
 w   3  2   24809 -0.014880    2( b2u)   2( b2u)   +-   +         +-   +-   +-   +  
 w   3  2   24822 -0.011795    1( b3u)   1( b3u)   +-   +         +-   +-   +-   +  
 w   1  1   25159  0.010710    1( b3u)   2( b3u)   +-        +-   +-   +    +-    - 
 w   3  1   27742 -0.012684    1( b1u)   1( b1u)   +    +-    -   +-   +-   +    +  
 w   3  1   27743 -0.010817    1( b1u)   2( b1u)   +    +-    -   +-   +-   +    +  
 w   3  1   27757 -0.011232    1( b2u)   1( b2u)   +    +-    -   +-   +-   +    +  
 w   1  1   27910  0.022379    1( b1u)   1( b1u)   +    +-    -   +-   +    +-    - 
 w   1  1   27911  0.019000    1( b1u)   2( b1u)   +    +-    -   +-   +    +-    - 
 w   1  1   27912  0.011145    2( b1u)   2( b1u)   +    +-    -   +-   +    +-    - 
 w   1  1   27925  0.018860    1( b2u)   1( b2u)   +    +-    -   +-   +    +-    - 
 w   1  1   27926  0.013838    1( b2u)   2( b2u)   +    +-    -   +-   +    +-    - 
 w   1  1   27940  0.018454    1( b3u)   1( b3u)   +    +-    -   +-   +    +-    - 
 w   1  1   27941  0.013465    1( b3u)   2( b3u)   +    +-    -   +-   +    +-    - 
 w   3  2   29072  0.016215    1( b1u)   1( b1u)   +    +-        +-   +-   +-   +  
 w   3  2   29073  0.012626    1( b1u)   2( b1u)   +    +-        +-   +-   +-   +  
 w   3  2   29087  0.021286    1( b2u)   1( b2u)   +    +-        +-   +-   +-   +  
 w   3  2   29088  0.020653    1( b2u)   2( b2u)   +    +-        +-   +-   +-   +  
 w   3  2   29089  0.014038    2( b2u)   2( b2u)   +    +-        +-   +-   +-   +  
 w   3  2   29102  0.012709    1( b3u)   1( b3u)   +    +-        +-   +-   +-   +  
 w   1  1   29438  0.014930    1( b3u)   1( b3u)   +     -   +-   +-   +    +-    - 
 w   1  1   29439  0.014544    1( b3u)   2( b3u)   +     -   +-   +-   +    +-    - 
 w   3  2   30382 -0.010654    2( b1u)   2( b3u)   +    +    +-   +-    -   +-   +  
 w   3  2   30753  0.010397    1( ag )   1( ag )   +    +     -   +-   +-   +-   +  
 w   3  2   30784  0.027269    1( b1u)   1( b1u)   +    +     -   +-   +-   +-   +  
 w   3  2   30785  0.021346    1( b1u)   2( b1u)   +    +     -   +-   +-   +-   +  
 w   3  2   30786  0.011716    2( b1u)   2( b1u)   +    +     -   +-   +-   +-   +  
 w   3  2   30799  0.026096    1( b2u)   1( b2u)   +    +     -   +-   +-   +-   +  
 w   3  2   30800  0.019635    1( b2u)   2( b2u)   +    +     -   +-   +-   +-   +  
 w   3  2   30801  0.010027    2( b2u)   2( b2u)   +    +     -   +-   +-   +-   +  
 w   3  2   30814  0.027624    1( b3u)   1( b3u)   +    +     -   +-   +-   +-   +  
 w   3  2   30815  0.021823    1( b3u)   2( b3u)   +    +     -   +-   +-   +-   +  
 w   3  2   30816  0.011945    2( b3u)   2( b3u)   +    +     -   +-   +-   +-   +  
 w   3  2   31028  0.018329    1( b3u)   1( b3u)   +         +-   +-   +-   +-   +  
 w   3  2   31029  0.019628    1( b3u)   2( b3u)   +         +-   +-   +-   +-   +  
 w   3  2   31030  0.014429    2( b3u)   2( b3u)   +         +-   +-   +-   +-   +  
 w   3  1   31166 -0.010039    1( b1u)   1( b1u)        +-   +-   +-   +-   +    +  
 w   3  1   31167 -0.010237    1( b1u)   2( b1u)        +-   +-   +-   +-   +    +  
 w   1  1   31334  0.018018    1( b1u)   1( b1u)        +-   +-   +-   +    +-    - 
 w   1  1   31335  0.018423    1( b1u)   2( b1u)        +-   +-   +-   +    +-    - 
 w   1  1   31336  0.013093    2( b1u)   2( b1u)        +-   +-   +-   +    +-    - 
 w   1  1   31364  0.012957    1( b3u)   1( b3u)        +-   +-   +-   +    +-    - 
 w   1  1   31365  0.010474    1( b3u)   2( b3u)        +-   +-   +-   +    +-    - 
 w   3  2   31854  0.017389    1( b1u)   1( b1u)        +-   +    +-   +-   +-   +  
 w   3  2   31855  0.015318    1( b1u)   2( b1u)        +-   +    +-   +-   +-   +  
 w   3  2   31869  0.014303    1( b2u)   1( b2u)        +-   +    +-   +-   +-   +  
 w   3  2   31870  0.010642    1( b2u)   2( b2u)        +-   +    +-   +-   +-   +  
 w   3  2   31884  0.012898    1( b3u)   1( b3u)        +-   +    +-   +-   +-   +  
 w   3  2   32068  0.024635    1( b1u)   1( b1u)        +    +-   +-   +-   +-   +  
 w   3  2   32069  0.022792    1( b1u)   2( b1u)        +    +-   +-   +-   +-   +  
 w   3  2   32070  0.014744    2( b1u)   2( b1u)        +    +-   +-   +-   +-   +  
 w   3  2   32083  0.014165    1( b2u)   1( b2u)        +    +-   +-   +-   +-   +  
 w   3  2   32098  0.022731    1( b3u)   1( b3u)        +    +-   +-   +-   +-   +  
 w   3  2   32099  0.019937    1( b3u)   2( b3u)        +    +-   +-   +-   +-   +  
 w   3  2   32100  0.012335    2( b3u)   2( b3u)        +    +-   +-   +-   +-   +  

 ci coefficient statistics:
           rq > 0.1                5
      0.1> rq > 0.01              85
     0.01> rq > 0.001           3418
    0.001> rq > 0.0001          6576
   0.0001> rq > 0.00001         9152
  0.00001> rq > 0.000001        9109
 0.000001> rq                   3813
           all                 32158
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:         126 2x:           0 4x:           0
All internal counts: zz :          15 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :          12 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:         193
  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.304661681071    -59.762528122744
     2     2     -0.540842550493    106.092868661910

 number of reference csfs (nref) is     2.  root number (iroot) is  2.
 c0**2 =   0.38532940  c**2 (all zwalks) =   0.93974689

 warning: cnot**2 < 0.5, dv3 is not computed.

 warning: pople ci energy extrapolation is not computed because of small fac.
 fac =      -0.229341191327

 eref      =   -196.161749164386   "relaxed" cnot**2         =   0.385329404337
 eci       =   -196.472582798886   deltae = eci - eref       =  -0.310833634499
 eci+dv1   =   -196.663643094156   dv1 = (1-cnot**2)*deltae  =  -0.191060295270
 eci+dv2   =   -196.968419070708   dv2 = dv1 / cnot**2       =  -0.495836271823
 eci+dv3   =   -196.472582798886   dv3 = dv1 / (2*cnot**2-1) =   0.000000000000
 eci+pople =   -196.161749164386   ( 12e- scaled deltae )    =   0.000000000000


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 3) =      -196.4725827988

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     2    3    4   11   14   17

                                         symmetry   ag   ag   ag   b1g  b2g  b3g

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  3  1       1  0.679659                        +-   +-   +-   +-   +-   +    +  
 z*  1  1       2  0.520746                        +-   +-   +-   +-   +    +-    - 
 z   3  3       3 -0.361626                        +-   +-   +-   +    +-   +-   +  
 z   3  2       4 -0.197961                        +-   +-   +    +-   +-   +-   +  
 z   3  2       5  0.065155                        +-   +    +-   +-   +-   +-   +  
 z   3  2       6  0.180069                        +    +-   +-   +-   +-   +-   +  
 y   3  1       9  0.013852              1( b3g)   +-   +-   +-   +-   +-        +  
 y   1  1      43 -0.010613              1( b2g)   +-   +-   +-   +-        +-    - 
 y   3  1     117 -0.015195              1( ag )   +-   +-    -   +-   +-   +    +  
 y   1  1     127 -0.012053              1( ag )   +-   +-    -   +-   +    +-    - 
 y   3  1     251 -0.010058              3( ag )   +-   +    +-   +-   +-    -   +  
 y   3  1     309 -0.013895              1( ag )    -   +-   +-   +-   +-   +    +  
 y   1  1     319 -0.010429              1( ag )    -   +-   +-   +-   +    +-    - 
 x   3  1    1043 -0.014969    1( b1g)   1( b2g)   +-   +-   +-    -    -   +    +  
 x   3  1    1046 -0.011916    2( b1g)   2( b2g)   +-   +-   +-    -    -   +    +  
 x   3  1    2906  0.010793    2( ag )   1( b1g)   +-   +-    -    -   +-   +    +  
 x   3  1    2949 -0.010314    4( b2u)   5( b3u)   +-   +-    -    -   +-   +    +  
 x   3  1    5137  0.016582    3( ag )   1( b2g)   +-    -   +-   +-    -   +    +  
 x   3  1    5139 -0.010171    5( ag )   1( b2g)   +-    -   +-   +-    -   +    +  
 x   3  1    5143 -0.010432    3( ag )   2( b2g)   +-    -   +-   +-    -   +    +  
 x   3  1    5145  0.013351    5( ag )   2( b2g)   +-    -   +-   +-    -   +    +  
 x   3  1    5174  0.014137    4( b1u)   4( b3u)   +-    -   +-   +-    -   +    +  
 x   3  1    5697 -0.015001    3( ag )   1( b1g)   +-    -   +-    -   +-   +    +  
 x   3  1    5705 -0.011954    5( ag )   2( b1g)   +-    -   +-    -   +-   +    +  
 x   1  1    5835 -0.011119    3( ag )   1( b1g)   +-    -   +-    -   +    +-    - 
 x   3  1    9972  0.010598    2( ag )   1( b2g)    -   +-   +-   +-    -   +    +  
 x   3  1   10532 -0.011164    2( ag )   1( b1g)    -   +-   +-    -   +-   +    +  
 x   3  1   11459 -0.010829    1( ag )   2( ag )    -   +-    -   +-   +-   +    +  
 w   1  1   17206  0.010380    1( b3g)   1( b3g)   +-   +-   +-   +-   +          - 
 w   3  1   17463 -0.013562    1( b2g)   1( b2g)   +-   +-   +-   +-        +    +  
 w   3  1   17464  0.011761    1( b2g)   2( b2g)   +-   +-   +-   +-        +    +  
 w   3  1   17465 -0.011569    2( b2g)   2( b2g)   +-   +-   +-   +-        +    +  
 w   3  1   17867  0.012237    1( b1g)   1( b2g)   +-   +-   +-   +     -   +    +  
 w   3  1   17870  0.010185    2( b1g)   2( b2g)   +-   +-   +-   +     -   +    +  
 w   3  1   18530 -0.013569    1( b1g)   1( b1g)   +-   +-   +-        +-   +    +  
 w   3  1   18531  0.011763    1( b1g)   2( b1g)   +-   +-   +-        +-   +    +  
 w   3  1   18532 -0.011568    2( b1g)   2( b1g)   +-   +-   +-        +-   +    +  
 w   1  1   18698 -0.010393    1( b1g)   1( b1g)   +-   +-   +-        +    +-    - 
 w   3  1   19850 -0.010065    2( b2u)   2( b3u)   +-   +-   +     -   +-   +    +  
 w   3  1   20865 -0.010013    2( ag )   2( ag )   +-   +-        +-   +-   +    +  
 w   3  1   20894 -0.018007    1( b1u)   1( b1u)   +-   +-        +-   +-   +    +  
 w   3  1   20895 -0.013320    1( b1u)   2( b1u)   +-   +-        +-   +-   +    +  
 w   3  1   20909 -0.027055    1( b2u)   1( b2u)   +-   +-        +-   +-   +    +  
 w   3  1   20910 -0.027236    1( b2u)   2( b2u)   +-   +-        +-   +-   +    +  
 w   3  1   20911 -0.019099    2( b2u)   2( b2u)   +-   +-        +-   +-   +    +  
 w   3  1   20924 -0.015467    1( b3u)   1( b3u)   +-   +-        +-   +-   +    +  
 w   3  1   20925 -0.010262    1( b3u)   2( b3u)   +-   +-        +-   +-   +    +  
 w   1  1   21062 -0.013502    1( b1u)   1( b1u)   +-   +-        +-   +    +-    - 
 w   1  1   21077 -0.020258    1( b2u)   1( b2u)   +-   +-        +-   +    +-    - 
 w   1  1   21078 -0.020388    1( b2u)   2( b2u)   +-   +-        +-   +    +-    - 
 w   1  1   21079 -0.014394    2( b2u)   2( b2u)   +-   +-        +-   +    +-    - 
 w   1  1   21092 -0.012561    1( b3u)   1( b3u)   +-   +-        +-   +    +-    - 
 w   3  3   21429  0.014368    1( b2u)   1( b2u)   +-   +-        +    +-   +-   +  
 w   3  3   21430  0.014461    1( b2u)   2( b2u)   +-   +-        +    +-   +-   +  
 w   3  3   21431  0.010142    2( b2u)   2( b2u)   +-   +-        +    +-   +-   +  
 w   3  1   21721 -0.012302    3( ag )   1( b2g)   +-   +    +-   +-    -   +    +  
 w   3  1   22393  0.012281    3( ag )   1( b1g)   +-   +    +-    -   +-   +    +  
 w   3  1   23462 -0.010757    1( b1u)   1( b1u)   +-   +     -   +-   +-   +    +  
 w   3  1   23477 -0.015821    1( b2u)   1( b2u)   +-   +     -   +-   +-   +    +  
 w   3  1   23478 -0.012780    1( b2u)   2( b2u)   +-   +     -   +-   +-   +    +  
 w   3  1   23492 -0.018149    1( b3u)   1( b3u)   +-   +     -   +-   +-   +    +  
 w   3  1   23493 -0.015985    1( b3u)   2( b3u)   +-   +     -   +-   +-   +    +  
 w   1  1   23645 -0.011789    1( b2u)   1( b2u)   +-   +     -   +-   +    +-    - 
 w   1  1   23660 -0.014216    1( b3u)   1( b3u)   +-   +     -   +-   +    +-    - 
 w   1  1   23661 -0.012586    1( b3u)   2( b3u)   +-   +     -   +-   +    +-    - 
 w   3  3   24012  0.010016    1( b3u)   1( b3u)   +-   +     -   +    +-   +-   +  
 w   3  1   24934 -0.013403    3( ag )   3( ag )   +-        +-   +-   +-   +    +  
 w   3  1   24941  0.010180    3( ag )   5( ag )   +-        +-   +-   +-   +    +  
 w   3  1   24943 -0.010007    5( ag )   5( ag )   +-        +-   +-   +-   +    +  
 w   3  1   24990 -0.011039    1( b3u)   1( b3u)   +-        +-   +-   +-   +    +  
 w   3  1   24991 -0.013577    1( b3u)   2( b3u)   +-        +-   +-   +-   +    +  
 w   3  1   24992 -0.011201    2( b3u)   2( b3u)   +-        +-   +-   +-   +    +  
 w   1  1   25159 -0.010332    1( b3u)   2( b3u)   +-        +-   +-   +    +-    - 
 w   3  1   26000 -0.010247    2( ag )   1( b2g)   +    +-   +-   +-    -   +    +  
 w   3  1   26026  0.010471    2( b1u)   2( b3u)   +    +-   +-   +-    -   +    +  
 w   3  1   27742 -0.028305    1( b1u)   1( b1u)   +    +-    -   +-   +-   +    +  
 w   3  1   27743 -0.024149    1( b1u)   2( b1u)   +    +-    -   +-   +-   +    +  
 w   3  1   27744 -0.014238    2( b1u)   2( b1u)   +    +-    -   +-   +-   +    +  
 w   3  1   27757 -0.025031    1( b2u)   1( b2u)   +    +-    -   +-   +-   +    +  
 w   3  1   27758 -0.018842    1( b2u)   2( b2u)   +    +-    -   +-   +-   +    +  
 w   3  1   27772 -0.021883    1( b3u)   1( b3u)   +    +-    -   +-   +-   +    +  
 w   3  1   27773 -0.015599    1( b3u)   2( b3u)   +    +-    -   +-   +-   +    +  
 w   1  1   27910 -0.021550    1( b1u)   1( b1u)   +    +-    -   +-   +    +-    - 
 w   1  1   27911 -0.018298    1( b1u)   2( b1u)   +    +-    -   +-   +    +-    - 
 w   1  1   27912 -0.010733    2( b1u)   2( b1u)   +    +-    -   +-   +    +-    - 
 w   1  1   27925 -0.018173    1( b2u)   1( b2u)   +    +-    -   +-   +    +-    - 
 w   1  1   27926 -0.013335    1( b2u)   2( b2u)   +    +-    -   +-   +    +-    - 
 w   1  1   27940 -0.017758    1( b3u)   1( b3u)   +    +-    -   +-   +    +-    - 
 w   1  1   27941 -0.012954    1( b3u)   2( b3u)   +    +-    -   +-   +    +-    - 
 w   3  3   28262  0.014470    1( b1u)   1( b1u)   +    +-    -   +    +-   +-   +  
 w   3  3   28263  0.012205    1( b1u)   2( b1u)   +    +-    -   +    +-   +-   +  
 w   3  3   28277  0.013180    1( b2u)   1( b2u)   +    +-    -   +    +-   +-   +  
 w   3  3   28292  0.012288    1( b3u)   1( b3u)   +    +-    -   +    +-   +-   +  
 w   3  2   29072  0.010472    1( b1u)   1( b1u)   +    +-        +-   +-   +-   +  
 w   3  2   29087  0.012297    1( b2u)   1( b2u)   +    +-        +-   +-   +-   +  
 w   3  2   29088  0.011011    1( b2u)   2( b2u)   +    +-        +-   +-   +-   +  
 w   3  1   29240 -0.011548    1( b1u)   1( b1u)   +     -   +-   +-   +-   +    +  
 w   3  1   29255 -0.010690    1( b2u)   1( b2u)   +     -   +-   +-   +-   +    +  
 w   3  1   29270 -0.018277    1( b3u)   1( b3u)   +     -   +-   +-   +-   +    +  
 w   3  1   29271 -0.017724    1( b3u)   2( b3u)   +     -   +-   +-   +-   +    +  
 w   3  1   29272 -0.011763    2( b3u)   2( b3u)   +     -   +-   +-   +-   +    +  
 w   1  1   29438 -0.014373    1( b3u)   1( b3u)   +     -   +-   +-   +    +-    - 
 w   1  1   29439 -0.013999    1( b3u)   2( b3u)   +     -   +-   +-   +    +-    - 
 w   3  1   31166 -0.022423    1( b1u)   1( b1u)        +-   +-   +-   +-   +    +  
 w   3  1   31167 -0.022880    1( b1u)   2( b1u)        +-   +-   +-   +-   +    +  
 w   3  1   31168 -0.016256    2( b1u)   2( b1u)        +-   +-   +-   +-   +    +  
 w   3  1   31181 -0.011967    1( b2u)   1( b2u)        +-   +-   +-   +-   +    +  
 w   3  1   31196 -0.015289    1( b3u)   1( b3u)        +-   +-   +-   +-   +    +  
 w   3  1   31197 -0.012089    1( b3u)   2( b3u)        +-   +-   +-   +-   +    +  
 w   1  1   31334 -0.017369    1( b1u)   1( b1u)        +-   +-   +-   +    +-    - 
 w   1  1   31335 -0.017770    1( b1u)   2( b1u)        +-   +-   +-   +    +-    - 
 w   1  1   31336 -0.012633    2( b1u)   2( b1u)        +-   +-   +-   +    +-    - 
 w   1  1   31364 -0.012453    1( b3u)   1( b3u)        +-   +-   +-   +    +-    - 
 w   1  1   31365 -0.010056    1( b3u)   2( b3u)        +-   +-   +-   +    +-    - 
 w   3  3   31686  0.011754    1( b1u)   1( b1u)        +-   +-   +    +-   +-   +  
 w   3  3   31687  0.012011    1( b1u)   2( b1u)        +-   +-   +    +-   +-   +  
 w   3  2   31854  0.011572    1( b1u)   1( b1u)        +-   +    +-   +-   +-   +  
 w   3  2   31855  0.010883    1( b1u)   2( b1u)        +-   +    +-   +-   +-   +  

 ci coefficient statistics:
           rq > 0.1                5
      0.1> rq > 0.01             113
     0.01> rq > 0.001           3290
    0.001> rq > 0.0001          8176
   0.0001> rq > 0.00001        10907
  0.00001> rq > 0.000001        8201
 0.000001> rq                   1466
           all                 32158
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:         126 2x:           0 4x:           0
All internal counts: zz :          15 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :          12 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:         193
  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.679658930620   -133.324815195370
     2     2      0.520746006220   -102.152214104899

 number of reference csfs (nref) is     2.  root number (iroot) is  3.
 c0**2 =   0.73311266  c**2 (all zwalks) =   0.93974503

 pople ci energy extrapolation is computed with 12 correlated electrons.

 eref      =   -196.164608408722   "relaxed" cnot**2         =   0.733112664966
 eci       =   -196.472582798811   deltae = eci - eref       =  -0.307974390089
 eci+dv1   =   -196.554777263041   dv1 = (1-cnot**2)*deltae  =  -0.082194464230
 eci+dv2   =   -196.584699896438   dv2 = dv1 / cnot**2       =  -0.112117097627
 eci+dv3   =   -196.648880522150   dv3 = dv1 / (2*cnot**2-1) =  -0.176297723339
 eci+pople =   -196.592231899512   ( 12e- scaled deltae )    =  -0.427623490790


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 4) =      -196.4725827985

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     2    3    4   11   14   17

                                         symmetry   ag   ag   ag   b1g  b2g  b3g

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  3  1       1  0.095353                        +-   +-   +-   +-   +-   +    +  
 z*  1  1       2 -0.012658                        +-   +-   +-   +-   +    +-    - 
 z   3  3       3  0.657887                        +-   +-   +-   +    +-   +-   +  
 z   3  2       4 -0.494669                        +-   +-   +    +-   +-   +-   +  
 z   3  2       5  0.395226                        +-   +    +-   +-   +-   +-   +  
 z   3  2       6  0.311089                        +    +-   +-   +-   +-   +-   +  
 y   3  3     113  0.013407              1( b1g)   +-   +-   +-        +-   +-   +  
 y   3  2     135  0.012954              1( b2g)   +-   +-    -   +-   +    +-   +  
 y   3  3     145 -0.014781              1( ag )   +-   +-    -   +    +-   +-   +  
 y   3  2     191  0.013452              1( ag )   +-   +-        +-   +-   +-   +  
 y   3  2     207  0.010457              1( b3g)   +-    -   +-   +-   +-   +    +  
 y   3  2     289  0.014317              3( ag )   +-   +     -   +-   +-   +-   +  
 y   3  3     337 -0.013777              1( ag )    -   +-   +-   +    +-   +-   +  
 y   3  2     347  0.011339              1( ag )    -   +-   +    +-   +-   +-   +  
 x   3  3    1707 -0.011459    1( b2g)   1( b3g)   +-   +-   +-   +     -    -   +  
 x   3  3    6165 -0.010712    3( ag )   1( b3g)   +-    -   +-   +    +-    -   +  
 x   3  3    6347  0.013402    3( ag )   1( b2g)   +-    -   +-   +     -   +-   +  
 x   3  3    6355  0.011270    5( ag )   2( b2g)   +-    -   +-   +     -   +-   +  
 x   3  3    6384  0.011894    4( b1u)   4( b3u)   +-    -   +-   +     -   +-   +  
 x   3  3   11000 -0.011143    2( ag )   1( b3g)    -   +-   +-   +    +-    -   +  
 x   3  3   11923 -0.010862    1( ag )   2( ag )    -   +-    -   +    +-   +-   +  
 w   3  3   17772  0.013151    1( b3g)   1( b3g)   +-   +-   +-   +    +-        +  
 w   3  3   17773 -0.011392    1( b3g)   2( b3g)   +-   +-   +-   +    +-        +  
 w   3  3   17774  0.011196    2( b3g)   2( b3g)   +-   +-   +-   +    +-        +  
 w   3  3   18215 -0.010273    1( b2g)   1( b3g)   +-   +-   +-   +    +     -   +  
 w   3  3   18411  0.013147    1( b2g)   1( b2g)   +-   +-   +-   +         +-   +  
 w   3  3   18412 -0.011391    1( b2g)   2( b2g)   +-   +-   +-   +         +-   +  
 w   3  3   18413  0.011196    2( b2g)   2( b2g)   +-   +-   +-   +         +-   +  
 w   3  2   19649 -0.010025    1( b2g)   1( b2g)   +-   +-   +    +-        +-   +  
 w   3  3   21414 -0.016256    1( b1u)   1( b1u)   +-   +-        +    +-   +-   +  
 w   3  3   21415 -0.011663    1( b1u)   2( b1u)   +-   +-        +    +-   +-   +  
 w   3  3   21429 -0.026110    1( b2u)   1( b2u)   +-   +-        +    +-   +-   +  
 w   3  3   21430 -0.026263    1( b2u)   2( b2u)   +-   +-        +    +-   +-   +  
 w   3  3   21431 -0.018413    2( b2u)   2( b2u)   +-   +-        +    +-   +-   +  
 w   3  3   21444 -0.016173    1( b3u)   1( b3u)   +-   +-        +    +-   +-   +  
 w   3  3   21445 -0.011175    1( b3u)   2( b3u)   +-   +-        +    +-   +-   +  
 w   3  3   23127 -0.010103    3( ag )   1( b2g)   +-   +    +-   +     -   +-   +  
 w   3  3   23997 -0.015745    1( b2u)   1( b2u)   +-   +     -   +    +-   +-   +  
 w   3  3   23998 -0.012915    1( b2u)   2( b2u)   +-   +     -   +    +-   +-   +  
 w   3  3   24012 -0.018237    1( b3u)   1( b3u)   +-   +     -   +    +-   +-   +  
 w   3  3   24013 -0.016243    1( b3u)   2( b3u)   +-   +     -   +    +-   +-   +  
 w   3  2   24792  0.014635    1( b1u)   1( b1u)   +-   +         +-   +-   +-   +  
 w   3  2   24807  0.023706    1( b2u)   1( b2u)   +-   +         +-   +-   +-   +  
 w   3  2   24808  0.022196    1( b2u)   2( b2u)   +-   +         +-   +-   +-   +  
 w   3  2   24809  0.014593    2( b2u)   2( b2u)   +-   +         +-   +-   +-   +  
 w   3  2   24822  0.019949    1( b3u)   1( b3u)   +-   +         +-   +-   +-   +  
 w   3  2   24823  0.016014    1( b3u)   2( b3u)   +-   +         +-   +-   +-   +  
 w   3  3   25454 -0.012478    3( ag )   3( ag )   +-        +-   +    +-   +-   +  
 w   3  3   25510 -0.010761    1( b3u)   1( b3u)   +-        +-   +    +-   +-   +  
 w   3  3   25511 -0.013200    1( b3u)   2( b3u)   +-        +-   +    +-   +-   +  
 w   3  3   25512 -0.010851    2( b3u)   2( b3u)   +-        +-   +    +-   +-   +  
 w   3  2   25663  0.010400    1( b2u)   1( b2u)   +-        +    +-   +-   +-   +  
 w   3  2   25678  0.015686    1( b3u)   1( b3u)   +-        +    +-   +-   +-   +  
 w   3  2   25679  0.016566    1( b3u)   2( b3u)   +-        +    +-   +-   +-   +  
 w   3  2   25680  0.011983    2( b3u)   2( b3u)   +-        +    +-   +-   +-   +  
 w   3  3   28262 -0.026296    1( b1u)   1( b1u)   +    +-    -   +    +-   +-   +  
 w   3  3   28263 -0.022182    1( b1u)   2( b1u)   +    +-    -   +    +-   +-   +  
 w   3  3   28264 -0.013102    2( b1u)   2( b1u)   +    +-    -   +    +-   +-   +  
 w   3  3   28277 -0.023988    1( b2u)   1( b2u)   +    +-    -   +    +-   +-   +  
 w   3  3   28278 -0.017954    1( b2u)   2( b2u)   +    +-    -   +    +-   +-   +  
 w   3  3   28292 -0.022378    1( b3u)   1( b3u)   +    +-    -   +    +-   +-   +  
 w   3  3   28293 -0.016343    1( b3u)   2( b3u)   +    +-    -   +    +-   +-   +  
 w   3  2   29072  0.022216    1( b1u)   1( b1u)   +    +-        +-   +-   +-   +  
 w   3  2   29073  0.017961    1( b1u)   2( b1u)   +    +-        +-   +-   +-   +  
 w   3  2   29074  0.010335    2( b1u)   2( b1u)   +    +-        +-   +-   +-   +  
 w   3  2   29087  0.025238    1( b2u)   1( b2u)   +    +-        +-   +-   +-   +  
 w   3  2   29088  0.021988    1( b2u)   2( b2u)   +    +-        +-   +-   +-   +  
 w   3  2   29089  0.013311    2( b2u)   2( b2u)   +    +-        +-   +-   +-   +  
 w   3  2   29102  0.019003    1( b3u)   1( b3u)   +    +-        +-   +-   +-   +  
 w   3  2   29103  0.013463    1( b3u)   2( b3u)   +    +-        +-   +-   +-   +  
 w   3  3   29760 -0.010645    1( b1u)   1( b1u)   +     -   +-   +    +-   +-   +  
 w   3  3   29775 -0.010537    1( b2u)   1( b2u)   +     -   +-   +    +-   +-   +  
 w   3  3   29790 -0.017989    1( b3u)   1( b3u)   +     -   +-   +    +-   +-   +  
 w   3  3   29791 -0.017409    1( b3u)   2( b3u)   +     -   +-   +    +-   +-   +  
 w   3  3   29792 -0.011453    2( b3u)   2( b3u)   +     -   +-   +    +-   +-   +  
 w   3  2   29928  0.018058    1( b1u)   1( b1u)   +     -   +    +-   +-   +-   +  
 w   3  2   29929  0.012334    1( b1u)   2( b1u)   +     -   +    +-   +-   +-   +  
 w   3  2   29943  0.019023    1( b2u)   1( b2u)   +     -   +    +-   +-   +-   +  
 w   3  2   29944  0.013717    1( b2u)   2( b2u)   +     -   +    +-   +-   +-   +  
 w   3  2   29958  0.024417    1( b3u)   1( b3u)   +     -   +    +-   +-   +-   +  
 w   3  2   29959  0.021643    1( b3u)   2( b3u)   +     -   +    +-   +-   +-   +  
 w   3  2   29960  0.013252    2( b3u)   2( b3u)   +     -   +    +-   +-   +-   +  
 w   3  3   31686 -0.021364    1( b1u)   1( b1u)        +-   +-   +    +-   +-   +  
 w   3  3   31687 -0.021835    1( b1u)   2( b1u)        +-   +-   +    +-   +-   +  
 w   3  3   31688 -0.015620    2( b1u)   2( b1u)        +-   +-   +    +-   +-   +  
 w   3  3   31701 -0.011410    1( b2u)   1( b2u)        +-   +-   +    +-   +-   +  
 w   3  3   31716 -0.015332    1( b3u)   1( b3u)        +-   +-   +    +-   +-   +  
 w   3  3   31717 -0.012286    1( b3u)   2( b3u)        +-   +-   +    +-   +-   +  
 w   3  2   31854  0.024757    1( b1u)   1( b1u)        +-   +    +-   +-   +-   +  
 w   3  2   31855  0.023678    1( b1u)   2( b1u)        +-   +    +-   +-   +-   +  
 w   3  2   31856  0.015981    2( b1u)   2( b1u)        +-   +    +-   +-   +-   +  
 w   3  2   31869  0.017427    1( b2u)   1( b2u)        +-   +    +-   +-   +-   +  
 w   3  2   31870  0.012193    1( b2u)   2( b2u)        +-   +    +-   +-   +-   +  
 w   3  2   31884  0.018226    1( b3u)   1( b3u)        +-   +    +-   +-   +-   +  
 w   3  2   31885  0.013829    1( b3u)   2( b3u)        +-   +    +-   +-   +-   +  
 w   3  2   32069 -0.011178    1( b1u)   2( b1u)        +    +-   +-   +-   +-   +  

 ci coefficient statistics:
           rq > 0.1                4
      0.1> rq > 0.01              92
     0.01> rq > 0.001           3369
    0.001> rq > 0.0001          6431
   0.0001> rq > 0.00001         7889
  0.00001> rq > 0.000001        9276
 0.000001> rq                   5097
           all                 32158
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:         126 2x:           0 4x:           0
All internal counts: zz :          15 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :          12 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:         193
  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.095353135229    -18.704745976635
     2     2     -0.012658453457      2.482971636170

 number of reference csfs (nref) is     2.  root number (iroot) is  4.
 c0**2 =   0.00925246  c**2 (all zwalks) =   0.93974628

 warning: cnot**2 < 0.5, dv3 is not computed.

 warning: pople ci energy extrapolation is not computed because of small fac.
 fac =      -0.981495086316

 eref      =   -196.162682457341   "relaxed" cnot**2         =   0.009252456842
 eci       =   -196.472582798533   deltae = eci - eref       =  -0.309900341192
 eci+dv1   =   -196.779615800193   dv1 = (1-cnot**2)*deltae  =  -0.307033001660
 eci+dv2   =   -229.656525929289   dv2 = dv1 / cnot**2       = -33.183943130756
 eci+dv3   =   -196.472582798533   dv3 = dv1 / (2*cnot**2-1) =   0.000000000000
 eci+pople =   -196.162682457341   ( 12e- scaled deltae )    =   0.000000000000


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 5) =      -196.4644811596

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     2    3    4   11   14   17

                                         symmetry   ag   ag   ag   b1g  b2g  b3g

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  3  1       1  0.531196                        +-   +-   +-   +-   +-   +    +  
 z*  1  1       2 -0.530754                        +-   +-   +-   +-   +    +-    - 
 z   3  2       4  0.169552                        +-   +-   +    +-   +-   +-   +  
 z   3  2       5  0.403948                        +-   +    +-   +-   +-   +-   +  
 z   3  2       6 -0.428941                        +    +-   +-   +-   +-   +-   +  
 y   3  1       9  0.011038              1( b3g)   +-   +-   +-   +-   +-        +  
 y   1  1      43  0.011028              1( b2g)   +-   +-   +-   +-        +-    - 
 y   3  1     117 -0.011931              1( ag )   +-   +-    -   +-   +-   +    +  
 y   1  1     127  0.012348              1( ag )   +-   +-    -   +-   +    +-    - 
 y   3  2     287 -0.011279              1( ag )   +-   +     -   +-   +-   +-   +  
 y   3  1     309 -0.010906              1( ag )    -   +-   +-   +-   +-   +    +  
 y   1  1     319  0.010672              1( ag )    -   +-   +-   +-   +    +-    - 
 y   3  2     335 -0.011453              1( b1g)    -   +-   +-   +    +-   +-   +  
 y   3  2     431  0.010639              1( ag )        +-   +-   +-   +-   +-   +  
 x   3  1    1043 -0.011700    1( b1g)   1( b2g)   +-   +-   +-    -    -   +    +  
 x   3  1    5137  0.012967    3( ag )   1( b2g)   +-    -   +-   +-    -   +    +  
 x   3  1    5145  0.010434    5( ag )   2( b2g)   +-    -   +-   +-    -   +    +  
 x   3  1    5174  0.011050    4( b1u)   4( b3u)   +-    -   +-   +-    -   +    +  
 x   3  1    5697 -0.011712    3( ag )   1( b1g)   +-    -   +-    -   +-   +    +  
 x   1  1    5835  0.011332    3( ag )   1( b1g)   +-    -   +-    -   +    +-    - 
 w   1  1   17206 -0.010614    1( b3g)   1( b3g)   +-   +-   +-   +-   +          - 
 w   3  1   17463 -0.010624    1( b2g)   1( b2g)   +-   +-   +-   +-        +    +  
 w   3  1   18530 -0.010601    1( b1g)   1( b1g)   +-   +-   +-        +-   +    +  
 w   1  1   18698  0.010591    1( b1g)   1( b1g)   +-   +-   +-        +    +-    - 
 w   3  1   20894 -0.014071    1( b1u)   1( b1u)   +-   +-        +-   +-   +    +  
 w   3  1   20895 -0.010402    1( b1u)   2( b1u)   +-   +-        +-   +-   +    +  
 w   3  1   20909 -0.021161    1( b2u)   1( b2u)   +-   +-        +-   +-   +    +  
 w   3  1   20910 -0.021303    1( b2u)   2( b2u)   +-   +-        +-   +-   +    +  
 w   3  1   20911 -0.014939    2( b2u)   2( b2u)   +-   +-        +-   +-   +    +  
 w   3  1   20924 -0.012083    1( b3u)   1( b3u)   +-   +-        +-   +-   +    +  
 w   1  1   21062  0.013765    1( b1u)   1( b1u)   +-   +-        +-   +    +-    - 
 w   1  1   21063  0.010113    1( b1u)   2( b1u)   +-   +-        +-   +    +-    - 
 w   1  1   21077  0.020639    1( b2u)   1( b2u)   +-   +-        +-   +    +-    - 
 w   1  1   21078  0.020770    1( b2u)   2( b2u)   +-   +-        +-   +    +-    - 
 w   1  1   21079  0.014664    2( b2u)   2( b2u)   +-   +-        +-   +    +-    - 
 w   1  1   21092  0.012808    1( b3u)   1( b3u)   +-   +-        +-   +    +-    - 
 w   3  1   23477 -0.012384    1( b2u)   1( b2u)   +-   +     -   +-   +-   +    +  
 w   3  1   23478 -0.010011    1( b2u)   2( b2u)   +-   +     -   +-   +-   +    +  
 w   3  1   23492 -0.014174    1( b3u)   1( b3u)   +-   +     -   +-   +-   +    +  
 w   3  1   23493 -0.012487    1( b3u)   2( b3u)   +-   +     -   +-   +-   +    +  
 w   1  1   23645  0.012013    1( b2u)   1( b2u)   +-   +     -   +-   +    +-    - 
 w   1  1   23660  0.014499    1( b3u)   1( b3u)   +-   +     -   +-   +    +-    - 
 w   1  1   23661  0.012840    1( b3u)   2( b3u)   +-   +     -   +-   +    +-    - 
 w   3  2   24807  0.012766    1( b2u)   1( b2u)   +-   +         +-   +-   +-   +  
 w   3  2   24808  0.013418    1( b2u)   2( b2u)   +-   +         +-   +-   +-   +  
 w   3  1   24934 -0.010471    3( ag )   3( ag )   +-        +-   +-   +-   +    +  
 w   3  1   24991 -0.010605    1( b3u)   2( b3u)   +-        +-   +-   +-   +    +  
 w   1  1   25159  0.010543    1( b3u)   2( b3u)   +-        +-   +-   +    +-    - 
 w   3  1   27742 -0.022133    1( b1u)   1( b1u)   +    +-    -   +-   +-   +    +  
 w   3  1   27743 -0.018882    1( b1u)   2( b1u)   +    +-    -   +-   +-   +    +  
 w   3  1   27744 -0.011132    2( b1u)   2( b1u)   +    +-    -   +-   +-   +    +  
 w   3  1   27757 -0.019565    1( b2u)   1( b2u)   +    +-    -   +-   +-   +    +  
 w   3  1   27758 -0.014719    1( b2u)   2( b2u)   +    +-    -   +-   +-   +    +  
 w   3  1   27772 -0.017092    1( b3u)   1( b3u)   +    +-    -   +-   +-   +    +  
 w   3  1   27773 -0.012185    1( b3u)   2( b3u)   +    +-    -   +-   +-   +    +  
 w   1  1   27910  0.021973    1( b1u)   1( b1u)   +    +-    -   +-   +    +-    - 
 w   1  1   27911  0.018661    1( b1u)   2( b1u)   +    +-    -   +-   +    +-    - 
 w   1  1   27912  0.010947    2( b1u)   2( b1u)   +    +-    -   +-   +    +-    - 
 w   1  1   27925  0.018515    1( b2u)   1( b2u)   +    +-    -   +-   +    +-    - 
 w   1  1   27926  0.013587    1( b2u)   2( b2u)   +    +-    -   +-   +    +-    - 
 w   1  1   27940  0.018103    1( b3u)   1( b3u)   +    +-    -   +-   +    +-    - 
 w   1  1   27941  0.013203    1( b3u)   2( b3u)   +    +-    -   +-   +    +-    - 
 w   3  2   29072 -0.016757    1( b1u)   1( b1u)   +    +-        +-   +-   +-   +  
 w   3  2   29073 -0.013153    1( b1u)   2( b1u)   +    +-        +-   +-   +-   +  
 w   3  2   29087 -0.021369    1( b2u)   1( b2u)   +    +-        +-   +-   +-   +  
 w   3  2   29088 -0.020338    1( b2u)   2( b2u)   +    +-        +-   +-   +-   +  
 w   3  2   29089 -0.013565    2( b2u)   2( b2u)   +    +-        +-   +-   +-   +  
 w   3  2   29102 -0.013378    1( b3u)   1( b3u)   +    +-        +-   +-   +-   +  
 w   3  1   29270 -0.014272    1( b3u)   1( b3u)   +     -   +-   +-   +-   +    +  
 w   3  1   29271 -0.013843    1( b3u)   2( b3u)   +     -   +-   +-   +-   +    +  
 w   1  1   29438  0.014654    1( b3u)   1( b3u)   +     -   +-   +-   +    +-    - 
 w   1  1   29439  0.014273    1( b3u)   2( b3u)   +     -   +-   +-   +    +-    - 
 w   3  2   30784 -0.020813    1( b1u)   1( b1u)   +    +     -   +-   +-   +-   +  
 w   3  2   30785 -0.016010    1( b1u)   2( b1u)   +    +     -   +-   +-   +-   +  
 w   3  2   30799 -0.020446    1( b2u)   1( b2u)   +    +     -   +-   +-   +-   +  
 w   3  2   30800 -0.015482    1( b2u)   2( b2u)   +    +     -   +-   +-   +-   +  
 w   3  2   30814 -0.021799    1( b3u)   1( b3u)   +    +     -   +-   +-   +-   +  
 w   3  2   30815 -0.017414    1( b3u)   2( b3u)   +    +     -   +-   +-   +-   +  
 w   3  2   31028 -0.014572    1( b3u)   1( b3u)   +         +-   +-   +-   +-   +  
 w   3  2   31029 -0.015863    1( b3u)   2( b3u)   +         +-   +-   +-   +-   +  
 w   3  2   31030 -0.011847    2( b3u)   2( b3u)   +         +-   +-   +-   +-   +  
 w   3  1   31166 -0.017546    1( b1u)   1( b1u)        +-   +-   +-   +-   +    +  
 w   3  1   31167 -0.017910    1( b1u)   2( b1u)        +-   +-   +-   +-   +    +  
 w   3  1   31168 -0.012728    2( b1u)   2( b1u)        +-   +-   +-   +-   +    +  
 w   3  1   31196 -0.011940    1( b3u)   1( b3u)        +-   +-   +-   +-   +    +  
 w   1  1   31334  0.017712    1( b1u)   1( b1u)        +-   +-   +-   +    +-    - 
 w   1  1   31335  0.018123    1( b1u)   2( b1u)        +-   +-   +-   +    +-    - 
 w   1  1   31336  0.012885    2( b1u)   2( b1u)        +-   +-   +-   +    +-    - 
 w   1  1   31364  0.012690    1( b3u)   1( b3u)        +-   +-   +-   +    +-    - 
 w   1  1   31365  0.010244    1( b3u)   2( b3u)        +-   +-   +-   +    +-    - 
 w   3  2   31854 -0.018111    1( b1u)   1( b1u)        +-   +    +-   +-   +-   +  
 w   3  2   31855 -0.016247    1( b1u)   2( b1u)        +-   +    +-   +-   +-   +  
 w   3  2   31856 -0.010158    2( b1u)   2( b1u)        +-   +    +-   +-   +-   +  
 w   3  2   31869 -0.014442    1( b2u)   1( b2u)        +-   +    +-   +-   +-   +  
 w   3  2   31870 -0.010627    1( b2u)   2( b2u)        +-   +    +-   +-   +-   +  
 w   3  2   31884 -0.013406    1( b3u)   1( b3u)        +-   +    +-   +-   +-   +  
 w   3  2   32068 -0.018660    1( b1u)   1( b1u)        +    +-   +-   +-   +-   +  
 w   3  2   32069 -0.016863    1( b1u)   2( b1u)        +    +-   +-   +-   +-   +  
 w   3  2   32070 -0.010639    2( b1u)   2( b1u)        +    +-   +-   +-   +-   +  
 w   3  2   32083 -0.011136    1( b2u)   1( b2u)        +    +-   +-   +-   +-   +  
 w   3  2   32098 -0.018002    1( b3u)   1( b3u)        +    +-   +-   +-   +-   +  
 w   3  2   32099 -0.015975    1( b3u)   2( b3u)        +    +-   +-   +-   +-   +  

 ci coefficient statistics:
           rq > 0.1                5
      0.1> rq > 0.01              97
     0.01> rq > 0.001           3505
    0.001> rq > 0.0001          6430
   0.0001> rq > 0.00001         8312
  0.00001> rq > 0.000001        9041
 0.000001> rq                   4768
           all                 32158
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:         126 2x:           0 4x:           0
All internal counts: zz :          15 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :          12 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:         193
  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.531196264883   -104.200268408725
     2     2     -0.530754299940    104.113570504491

 number of reference csfs (nref) is     2.  root number (iroot) is  5.
 c0**2 =   0.56386960  c**2 (all zwalks) =   0.93978187

 pople ci energy extrapolation is computed with 12 correlated electrons.

 eref      =   -196.161521839387   "relaxed" cnot**2         =   0.563869598730
 eci       =   -196.464481159639   deltae = eci - eref       =  -0.302959320252
 eci+dv1   =   -196.596610929549   dv1 = (1-cnot**2)*deltae  =  -0.132129769910
 eci+dv2   =   -196.698807979542   dv2 = dv1 / cnot**2       =  -0.234326819903
 eci+dv3   =   -197.498852539274   dv3 = dv1 / (2*cnot**2-1) =  -1.034371379635
 eci+pople =   -196.780116738127   ( 12e- scaled deltae )    =  -0.618594898741


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 6) =      -196.4644811586

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     2    3    4   11   14   17

                                         symmetry   ag   ag   ag   b1g  b2g  b3g

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  3  1       1  0.306176                        +-   +-   +-   +-   +-   +    +  
 z*  1  1       2  0.306942                        +-   +-   +-   +-   +    +-    - 
 z   3  3       3  0.613117                        +-   +-   +-   +    +-   +-   +  
 z   3  2       4  0.410981                        +-   +-   +    +-   +-   +-   +  
 z   3  2       5 -0.400916                        +-   +    +-   +-   +-   +-   +  
 z   3  2       6 -0.215106                        +    +-   +-   +-   +-   +-   +  
 y   3  3     113  0.012739              1( b1g)   +-   +-   +-        +-   +-   +  
 y   3  2     135 -0.010537              1( b2g)   +-   +-    -   +-   +    +-   +  
 y   3  3     145 -0.013835              1( ag )   +-   +-    -   +    +-   +-   +  
 y   3  2     191 -0.011284              1( ag )   +-   +-        +-   +-   +-   +  
 y   3  2     289 -0.012580              3( ag )   +-   +     -   +-   +-   +-   +  
 y   3  3     337 -0.012913              1( ag )    -   +-   +-   +    +-   +-   +  
 x   3  3    1707 -0.010666    1( b2g)   1( b3g)   +-   +-   +-   +     -    -   +  
 x   3  3    6347  0.012498    3( ag )   1( b2g)   +-    -   +-   +     -   +-   +  
 x   3  3    6355  0.010500    5( ag )   2( b2g)   +-    -   +-   +     -   +-   +  
 x   3  3    6384  0.011086    4( b1u)   4( b3u)   +-    -   +-   +     -   +-   +  
 x   3  3   11000 -0.010382    2( ag )   1( b3g)    -   +-   +-   +    +-    -   +  
 x   3  3   11923 -0.010147    1( ag )   2( ag )    -   +-    -   +    +-   +-   +  
 w   3  3   17772  0.012248    1( b3g)   1( b3g)   +-   +-   +-   +    +-        +  
 w   3  3   17773 -0.010616    1( b3g)   2( b3g)   +-   +-   +-   +    +-        +  
 w   3  3   17774  0.010436    2( b3g)   2( b3g)   +-   +-   +-   +    +-        +  
 w   3  3   18411  0.012248    1( b2g)   1( b2g)   +-   +-   +-   +         +-   +  
 w   3  3   18412 -0.010616    1( b2g)   2( b2g)   +-   +-   +-   +         +-   +  
 w   3  3   18413  0.010436    2( b2g)   2( b2g)   +-   +-   +-   +         +-   +  
 w   3  1   20909 -0.012205    1( b2u)   1( b2u)   +-   +-        +-   +-   +    +  
 w   3  1   20910 -0.012295    1( b2u)   2( b2u)   +-   +-        +-   +-   +    +  
 w   1  1   21077 -0.011938    1( b2u)   1( b2u)   +-   +-        +-   +    +-    - 
 w   1  1   21078 -0.012013    1( b2u)   2( b2u)   +-   +-        +-   +    +-    - 
 w   3  3   21414 -0.015161    1( b1u)   1( b1u)   +-   +-        +    +-   +-   +  
 w   3  3   21415 -0.010877    1( b1u)   2( b1u)   +-   +-        +    +-   +-   +  
 w   3  3   21429 -0.024368    1( b2u)   1( b2u)   +-   +-        +    +-   +-   +  
 w   3  3   21430 -0.024522    1( b2u)   2( b2u)   +-   +-        +    +-   +-   +  
 w   3  3   21431 -0.017197    2( b2u)   2( b2u)   +-   +-        +    +-   +-   +  
 w   3  3   21444 -0.015046    1( b3u)   1( b3u)   +-   +-        +    +-   +-   +  
 w   3  3   21445 -0.010382    1( b3u)   2( b3u)   +-   +-        +    +-   +-   +  
 w   3  3   23997 -0.014666    1( b2u)   1( b2u)   +-   +     -   +    +-   +-   +  
 w   3  3   23998 -0.012019    1( b2u)   2( b2u)   +-   +     -   +    +-   +-   +  
 w   3  3   24012 -0.016992    1( b3u)   1( b3u)   +-   +     -   +    +-   +-   +  
 w   3  3   24013 -0.015131    1( b3u)   2( b3u)   +-   +     -   +    +-   +-   +  
 w   3  2   24792 -0.014011    1( b1u)   1( b1u)   +-   +         +-   +-   +-   +  
 w   3  2   24807 -0.022508    1( b2u)   1( b2u)   +-   +         +-   +-   +-   +  
 w   3  2   24808 -0.021268    1( b2u)   2( b2u)   +-   +         +-   +-   +-   +  
 w   3  2   24809 -0.014118    2( b2u)   2( b2u)   +-   +         +-   +-   +-   +  
 w   3  2   24822 -0.018389    1( b3u)   1( b3u)   +-   +         +-   +-   +-   +  
 w   3  2   24823 -0.014569    1( b3u)   2( b3u)   +-   +         +-   +-   +-   +  
 w   3  3   25454 -0.011649    3( ag )   3( ag )   +-        +-   +    +-   +-   +  
 w   3  3   25510 -0.010042    1( b3u)   1( b3u)   +-        +-   +    +-   +-   +  
 w   3  3   25511 -0.012324    1( b3u)   2( b3u)   +-        +-   +    +-   +-   +  
 w   3  3   25512 -0.010133    2( b3u)   2( b3u)   +-        +-   +    +-   +-   +  
 w   3  2   25678 -0.014409    1( b3u)   1( b3u)   +-        +    +-   +-   +-   +  
 w   3  2   25679 -0.014954    1( b3u)   2( b3u)   +-        +    +-   +-   +-   +  
 w   3  2   25680 -0.010636    2( b3u)   2( b3u)   +-        +    +-   +-   +-   +  
 w   3  1   27742 -0.012751    1( b1u)   1( b1u)   +    +-    -   +-   +-   +    +  
 w   3  1   27743 -0.010872    1( b1u)   2( b1u)   +    +-    -   +-   +-   +    +  
 w   3  1   27757 -0.011276    1( b2u)   1( b2u)   +    +-    -   +-   +-   +    +  
 w   1  1   27910 -0.012718    1( b1u)   1( b1u)   +    +-    -   +-   +    +-    - 
 w   1  1   27911 -0.010801    1( b1u)   2( b1u)   +    +-    -   +-   +    +-    - 
 w   1  1   27925 -0.010712    1( b2u)   1( b2u)   +    +-    -   +-   +    +-    - 
 w   1  1   27940 -0.010465    1( b3u)   1( b3u)   +    +-    -   +-   +    +-    - 
 w   3  3   28262 -0.024518    1( b1u)   1( b1u)   +    +-    -   +    +-   +-   +  
 w   3  3   28263 -0.020678    1( b1u)   2( b1u)   +    +-    -   +    +-   +-   +  
 w   3  3   28264 -0.012210    2( b1u)   2( b1u)   +    +-    -   +    +-   +-   +  
 w   3  3   28277 -0.022364    1( b2u)   1( b2u)   +    +-    -   +    +-   +-   +  
 w   3  3   28278 -0.016735    1( b2u)   2( b2u)   +    +-    -   +    +-   +-   +  
 w   3  3   28292 -0.020845    1( b3u)   1( b3u)   +    +-    -   +    +-   +-   +  
 w   3  3   28293 -0.015221    1( b3u)   2( b3u)   +    +-    -   +    +-   +-   +  
 w   3  2   29072 -0.017234    1( b1u)   1( b1u)   +    +-        +-   +-   +-   +  
 w   3  2   29073 -0.013986    1( b1u)   2( b1u)   +    +-        +-   +-   +-   +  
 w   3  2   29087 -0.019268    1( b2u)   1( b2u)   +    +-        +-   +-   +-   +  
 w   3  2   29088 -0.016548    1( b2u)   2( b2u)   +    +-        +-   +-   +-   +  
 w   3  2   29102 -0.014870    1( b3u)   1( b3u)   +    +-        +-   +-   +-   +  
 w   3  2   29103 -0.010602    1( b3u)   2( b3u)   +    +-        +-   +-   +-   +  
 w   3  3   29790 -0.016784    1( b3u)   1( b3u)   +     -   +-   +    +-   +-   +  
 w   3  3   29791 -0.016255    1( b3u)   2( b3u)   +     -   +-   +    +-   +-   +  
 w   3  3   29792 -0.010701    2( b3u)   2( b3u)   +     -   +-   +    +-   +-   +  
 w   3  2   29928 -0.016099    1( b1u)   1( b1u)   +     -   +    +-   +-   +-   +  
 w   3  2   29929 -0.011261    1( b1u)   2( b1u)   +     -   +    +-   +-   +-   +  
 w   3  2   29943 -0.016616    1( b2u)   1( b2u)   +     -   +    +-   +-   +-   +  
 w   3  2   29944 -0.011973    1( b2u)   2( b2u)   +     -   +    +-   +-   +-   +  
 w   3  2   29958 -0.020999    1( b3u)   1( b3u)   +     -   +    +-   +-   +-   +  
 w   3  2   29959 -0.018425    1( b3u)   2( b3u)   +     -   +    +-   +-   +-   +  
 w   3  2   29960 -0.011199    2( b3u)   2( b3u)   +     -   +    +-   +-   +-   +  
 w   3  2   30784  0.011063    1( b1u)   1( b1u)   +    +     -   +-   +-   +-   +  
 w   3  2   30785  0.010200    1( b1u)   2( b1u)   +    +     -   +-   +-   +-   +  
 w   3  1   31166 -0.010112    1( b1u)   1( b1u)        +-   +-   +-   +-   +    +  
 w   3  1   31167 -0.010320    1( b1u)   2( b1u)        +-   +-   +-   +-   +    +  
 w   1  1   31334 -0.010228    1( b1u)   1( b1u)        +-   +-   +-   +    +-    - 
 w   1  1   31335 -0.010455    1( b1u)   2( b1u)        +-   +-   +-   +    +-    - 
 w   3  3   31686 -0.019915    1( b1u)   1( b1u)        +-   +-   +    +-   +-   +  
 w   3  3   31687 -0.020349    1( b1u)   2( b1u)        +-   +-   +    +-   +-   +  
 w   3  3   31688 -0.014555    2( b1u)   2( b1u)        +-   +-   +    +-   +-   +  
 w   3  3   31701 -0.010625    1( b2u)   1( b2u)        +-   +-   +    +-   +-   +  
 w   3  3   31716 -0.014301    1( b3u)   1( b3u)        +-   +-   +    +-   +-   +  
 w   3  3   31717 -0.011470    1( b3u)   2( b3u)        +-   +-   +    +-   +-   +  
 w   3  2   31854 -0.019271    1( b1u)   1( b1u)        +-   +    +-   +-   +-   +  
 w   3  2   31855 -0.018572    1( b1u)   2( b1u)        +-   +    +-   +-   +-   +  
 w   3  2   31856 -0.012643    2( b1u)   2( b1u)        +-   +    +-   +-   +-   +  
 w   3  2   31869 -0.013354    1( b2u)   1( b2u)        +-   +    +-   +-   +-   +  
 w   3  2   31884 -0.014182    1( b3u)   1( b3u)        +-   +    +-   +-   +-   +  
 w   3  2   31885 -0.010812    1( b3u)   2( b3u)        +-   +    +-   +-   +-   +  
 w   3  2   32068  0.010723    1( b1u)   1( b1u)        +    +-   +-   +-   +-   +  
 w   3  2   32069  0.012072    1( b1u)   2( b1u)        +    +-   +-   +-   +-   +  

 ci coefficient statistics:
           rq > 0.1                6
      0.1> rq > 0.01              96
     0.01> rq > 0.001           4064
    0.001> rq > 0.0001          7983
   0.0001> rq > 0.00001         9486
  0.00001> rq > 0.000001        8802
 0.000001> rq                   1721
           all                 32158
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:         126 2x:           0 4x:           0
All internal counts: zz :          15 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :          12 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         279    task #     2:         279    task #     3:        2869    task #     4:        1629
task #     5:         535    task #     6:        4079    task #     7:        3244    task #     8:          29
task #     9:        1451    task #    10:        5389    task #    11:        5389    task #    12:        2937
task #    13:        2937    task #    14:        1679    task #    15:        1681    task #    16:        1681
task #    17:         105    task #    18:         769    task #    19:        1689    task #    20:         193
  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.306175806033    -60.060874859175
     2     2      0.306941579203    -60.211090089359

 number of reference csfs (nref) is     2.  root number (iroot) is  6.
 c0**2 =   0.18795676  c**2 (all zwalks) =   0.93977927

 warning: cnot**2 < 0.5, dv3 is not computed.

 warning: pople ci energy extrapolation is not computed because of small fac.
 fac =      -0.624086485513

 eref      =   -196.164662496479   "relaxed" cnot**2         =   0.187956757244
 eci       =   -196.464481158605   deltae = eci - eref       =  -0.299818662126
 eci+dv1   =   -196.707946877237   dv1 = (1-cnot**2)*deltae  =  -0.243465718632
 eci+dv2   =   -197.759809521375   dv2 = dv1 / cnot**2       =  -1.295328362770
 eci+dv3   =   -196.464481158605   dv3 = dv1 / (2*cnot**2-1) =   0.000000000000
 eci+pople =   -196.164662496479   ( 12e- scaled deltae )    =   0.000000000000
 passed aftci ... 
