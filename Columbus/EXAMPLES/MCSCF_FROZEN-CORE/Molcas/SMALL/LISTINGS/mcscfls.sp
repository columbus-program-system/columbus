

     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 This program allows the csf mixing coefficient and orbital expansion coefficient
 optimization using the graphical unitary group approach and the exponential
 operator mcscf method.
 references:  r. shepard and j. simons, ' int. j. quantum chem. symp. 14, 211 (1980).
              r. shepard, i. shavitt, and j. simons, j. chem. phys. 76, 543 (1982).
              r. shepard in "ab initio methods in quantum chemistry ii" advances in chemical
                  physics 69, edited by k. p. lawley (wiley, new york, 1987) pp. 63-200.
 Original autor: Ron Shepard, ANL
 Later revisions: Michal Dallos, University Vienna

 This Version of Program MCSCF is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.4.0.2     **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 Workspace allocation information:
       498073600 of real*8 words ( 3800.00 MB) of work space has been allocated.

 user input information:

 ======== echo of the mcscf input ========
 ------------------------------------------------------------------------
  &input
   niter=100,
   nmiter=50,
   nciitr=300,
   tol(3)=1.e-4,
   tol(2)=1.e-4,
   tol(1)=1.e-8,
   NSTATE=0,
   npath=1,3,9,10,17,19,21,-11,12, 2,
   ncoupl=5,
   tol(9)=1.e-3,
   FCIORB=  5,2,40,5,3,40,6,1,40,6,2,40,7,1,40,7,2,40,8,1,40,8,2,40
   NAVST(1) = 1,
   WAVST(1,1)=1 ,
  &end
 ------------------------------------------------------------------------


 ***  Integral file informations  ***


 input integral file : /scratch/phasan/76124/WORK/aoints                        
    

 Integral file header information:
 SEWARD INTEGRALS                                                                
 user-specified FREEZE(*) used in program tran.                                  
 SIFS file created by program tran.      adler.itc.univie. 15:55:12.313 19-Feb-12

 Core type energy values:
 energy( 1)=  4.557548380050E+02, ietype=   -1,    core energy of type: Nuc.Rep.
 total ao core energy =  455.754838005


   ******  Basis set information:  ******

 Number of irreps:                  8
 Total number of basis functions:  20

 irrep no.              1    2    3    4    5    6    7    8
 irrep label          ag   b3u  b2u  b1g  b1u  b2g  b3g  au  
 no. of bas.fcions.     0    0    0    0    6    4    6    4


 ***  MCSCF optimization procedure parmeters:  ***


 maximum number of mcscf iterations:        niter=   100

 maximum number of psci micro-iterations:   nmiter=   50
 maximum r,s subspace dimension allowed:    nvrsmx=   30

 tol(1)=  1.0000E-08. . . . delta-emc convergence criterion.
 tol(2)=  1.0000E-04. . . . wnorm convergence criterion.
 tol(3)=  1.0000E-04. . . . knorm convergence criterion.
 tol(4)=  1.0000E-08. . . . apxde convergence criterion.
 tol(5)=  1.0000E-04. . . . small diagonal matrix element tolerance.
 tol(6)=  1.0000E-06. . . . minimum ci-psci residual norm.
 tol(7)=  1.0000E-05. . . . maximum ci-psci residual norm.
 tol(8)=  1.0000E+00. . . . maximum abs(k(xy)) allowed.
 tol(9)=  1.0000E-03. . . . wnorm coupling tolerance.
 tol(10)= 0.0000E+00. . . . maximum psci emergency shift parameter.
 tol(11)= 0.0000E+00. . . . minimum psci emergency shift parameter.
 tol(12)= 0.0000E+00. . . . increment of psci emergency shift parameter.


 *** State averaging informations: ***


 MCSCF calculation performed for  1 DRT.

 DRT  first state   no.of aver.states   weights
  1   ground state          1             1.000

 The number of hmc(*) eigenvalues and eigenvectors calculated each iteration per DRT:
 DRT.   no.of eigenv.(=ncol)
    1        2

 orbital coefficients are optimized for the ground state (nstate=0).

 Orbitals included in invariant subspaces:
   symmetry   orbital   mask
       5       2(  2)    40
       5       3(  3)    40
       6       1(  7)    40
       6       2(  8)    40
       7       1( 11)    40
       7       2( 12)    40
       8       1( 17)    40
       8       2( 18)    40

 npath(*) options:
  2:  orbital-state coupling terms will be included beginning on iteration ncoupl=  5
  3:  print intermediate timing information.
  9:  suppress the drt listing.
 10:  suppress the hmc(*) eigenvector listing.
 12:  diagonalize the hmc(*) matrix iteratively.
        nunitv= 1 nciitr=** mxvadd=20 nvcimx=20
       rtolci(*),wnorm=     1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 0.0000E+00
   noldv =   0
 17:  print the final natural orbitals and occupations.
 19:  transform the virtual orbitals to diagonalize qvv(*).
 21:  write out the one- and two- electron density for further use (files:mcd1fl, mcd2fl).


   ******  DRT info section  ******


 Informations for the DRT no.  1

 DRT file header:
  title                                                                          
 Molecular symmetry group:    ag 
 Total number of electrons:   10
 Spin multiplicity:            1
 Number of active orbitals:    8
 Number of active electrons:   8
 Total number of CSFs:       468
 

 faar:   0 active-active rotations allowed out of:   4 possible.


 Number of active-double rotations:         2
 Number of active-active rotations:         0
 Number of double-virtual rotations:        3
 Number of active-virtual rotations:       22

 Size of orbital-Hessian matrix B:                      403
 Size of the orbital-state Hessian matrix C:          12636
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:          13039



   ****** Integral transformation section ******


 Source of the initial MO coeficients:

  Unit matrix used as initial orbitals.
 

               starting mcscf iteration...   1

 orbital-state coupling will not be calculated this iteration.

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     8, naopsy(1) =     0, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 16383, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 32768

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore= 498070096

 inoutp: segmentation information:
 in-core transformation space,   avcinc = 497968960
 address segment size,           sizesg = 497963504
 number of in-core blocks,       nincbk =        19
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:       3446 transformed 1/r12    array elements were written in       1 records.


 Size of orbital-Hessian matrix B:                      403
 Total size of the state Hessian matrix M:                0
 Total size of HESSIAN-matrix for linear con            403


 mosort: allocated sort2 space, avc2is=   497997482 available sort2 space, avcisx=   497997734

 trial vectors are generated internally.

 trial vector  1 is unit matrix column    89
 ciiter=  13 noldhv=  5 noldv=  5

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -383.2916830236      -13.6748920628        0.0000005937        0.0000010000
    2      -383.0313897143      -13.4145987535        0.0049762408        0.0100000000
 
  tol(10)=  0.000000000000000E+000  eshsci=  9.449106453568556E-003
 Total number of micro iterations:    6

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0=-0.92672841 pnorm= 0.0000E+00 rznorm= 9.5066E-06 rpnorm= 0.0000E+00 noldr=  6 nnewr=  6 nolds=  0 nnews=  0
 

 fdd(*) eigenvalues. symmetry block  5
    -1.056895
 i,qaaresolved                     1 -0.623652190910607     
 i,qaaresolved                     2  0.362837071038751     

 qvv(*) eigenvalues. symmetry block  5
     1.478262    1.709501    1.881156
 i,qaaresolved                     1 -0.877552211368601     
 i,qaaresolved                     2  0.163667698903787     

 qvv(*) eigenvalues. symmetry block  6
     1.557086    1.876058
 i,qaaresolved                     1 -0.754155291036515     
 i,qaaresolved                     2  0.242765081137744     

 qvv(*) eigenvalues. symmetry block  7
     0.811252    1.621784    1.836100    2.153291
 i,qaaresolved                     1 -0.547609614431407     
 i,qaaresolved                     2  0.549466833026259     

 qvv(*) eigenvalues. symmetry block  8
     1.739925    2.006745

 restrt: restart information saved on the restart file (unit= 13).

 not all mcscf convergence criteria are satisfied.
 iter=    1 emc=   -383.2916830236 demc= 3.8329E+02 wnorm= 7.5593E-02 knorm= 3.7573E-01 apxde= 1.1450E-02    *not conv.*     

               starting mcscf iteration...   2

 orbital-state coupling will not be calculated this iteration.

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     8, naopsy(1) =     0, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 16383, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 32768

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore= 498070096

 inoutp: segmentation information:
 in-core transformation space,   avcinc = 497968960
 address segment size,           sizesg = 497963504
 number of in-core blocks,       nincbk =        19
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:       3446 transformed 1/r12    array elements were written in       1 records.


 Size of orbital-Hessian matrix B:                      403
 Total size of the state Hessian matrix M:                0
 Total size of HESSIAN-matrix for linear con            403


 mosort: allocated sort2 space, avc2is=   497997482 available sort2 space, avcisx=   497997734

   2 trial vectors read from nvfile (unit= 29).
 ciiter=  10 noldhv= 17 noldv= 17

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -383.3068280014      -13.6900370406        0.0000037097        0.0000040816
    2      -383.0534459202      -13.4366549594        0.0049910514        0.0100000000
 
  tol(10)=  0.000000000000000E+000  eshsci=  3.234169962043862E-003
 Total number of micro iterations:    7

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.96143225 pnorm= 0.0000E+00 rznorm= 2.5015E-06 rpnorm= 0.0000E+00 noldr=  7 nnewr=  7 nolds=  0 nnews=  0
 

 fdd(*) eigenvalues. symmetry block  5
    -1.041422
 i,qaaresolved                     1 -0.621602326283955     
 i,qaaresolved                     2  0.414694858044627     

 qvv(*) eigenvalues. symmetry block  5
     1.482277    1.681614    1.873370
 i,qaaresolved                     1 -0.865492760280448     
 i,qaaresolved                     2  0.196114051981138     

 qvv(*) eigenvalues. symmetry block  6
     1.562828    1.851856
 i,qaaresolved                     1 -0.742987191647463     
 i,qaaresolved                     2  0.286544830947746     

 qvv(*) eigenvalues. symmetry block  7
     0.812454    1.627199    1.808145    2.159115
 i,qaaresolved                     1 -0.536461077471601     
 i,qaaresolved                     2  0.602222595966028     

 qvv(*) eigenvalues. symmetry block  8
     1.736982    1.970150

 restrt: restart information saved on the restart file (unit= 13).

 not all mcscf convergence criteria are satisfied.
 iter=    2 emc=   -383.3068280014 demc= 1.5145E-02 wnorm= 2.5873E-02 knorm= 2.7504E-01 apxde= 2.7232E-03    *not conv.*     

               starting mcscf iteration...   3

 orbital-state coupling will not be calculated this iteration.

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     8, naopsy(1) =     0, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 16383, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 32768

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore= 498070096

 inoutp: segmentation information:
 in-core transformation space,   avcinc = 497968960
 address segment size,           sizesg = 497963504
 number of in-core blocks,       nincbk =        19
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:       3446 transformed 1/r12    array elements were written in       1 records.


 Size of orbital-Hessian matrix B:                      403
 Total size of the state Hessian matrix M:                0
 Total size of HESSIAN-matrix for linear con            403


 mosort: allocated sort2 space, avc2is=   497997482 available sort2 space, avcisx=   497997734

   2 trial vectors read from nvfile (unit= 29).
 ciiter=  13 noldhv=  2 noldv=  2

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -383.3108243356      -13.6940333748        0.0000004079        0.0000010000
    2      -383.0632646343      -13.4464736735        0.0079328418        0.0100000000
 
  tol(10)=  0.000000000000000E+000  eshsci=  1.625774093773789E-003
 Total number of micro iterations:    6

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.99244805 pnorm= 0.0000E+00 rznorm= 5.3651E-06 rpnorm= 0.0000E+00 noldr=  6 nnewr=  6 nolds=  0 nnews=  0
 

 fdd(*) eigenvalues. symmetry block  5
    -1.004490
 i,qaaresolved                     1 -0.654359728665961     
 i,qaaresolved                     2  0.449579663861492     

 qvv(*) eigenvalues. symmetry block  5
     1.481107    1.654353    1.873351
 i,qaaresolved                     1 -0.860140592885458     
 i,qaaresolved                     2  0.210914330444296     

 qvv(*) eigenvalues. symmetry block  6
     1.564785    1.841550
 i,qaaresolved                     1 -0.738683238639868     
 i,qaaresolved                     2  0.350385967291560     

 qvv(*) eigenvalues. symmetry block  7
     0.785033    1.628344    1.781411    2.160243
 i,qaaresolved                     1 -0.533958250155032     
 i,qaaresolved                     2  0.643579826632193     

 qvv(*) eigenvalues. symmetry block  8
     1.726804    1.943192

 restrt: restart information saved on the restart file (unit= 13).

 not all mcscf convergence criteria are satisfied.
 iter=    3 emc=   -383.3108243356 demc= 3.9963E-03 wnorm= 1.3006E-02 knorm= 1.2267E-01 apxde= 5.8301E-04    *not conv.*     

               starting mcscf iteration...   4

 orbital-state coupling will not be calculated this iteration.

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     8, naopsy(1) =     0, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 16383, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 32768

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore= 498070096

 inoutp: segmentation information:
 in-core transformation space,   avcinc = 497968960
 address segment size,           sizesg = 497963504
 number of in-core blocks,       nincbk =        19
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:       3446 transformed 1/r12    array elements were written in       1 records.


 Size of orbital-Hessian matrix B:                      403
 Total size of the state Hessian matrix M:                0
 Total size of HESSIAN-matrix for linear con            403


 mosort: allocated sort2 space, avc2is=   497997482 available sort2 space, avcisx=   497997734

   2 trial vectors read from nvfile (unit= 29).
 ciiter=  13 noldhv=  2 noldv=  2

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -383.3116585104      -13.6948675496        0.0000006683        0.0000010000
    2      -383.0643420340      -13.4475510732        0.0080549838        0.0100000000
 
  tol(10)=  0.000000000000000E+000  eshsci=  8.725443275326526E-004
 Total number of micro iterations:    6

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.99876058 pnorm= 0.0000E+00 rznorm= 2.1572E-06 rpnorm= 0.0000E+00 noldr=  6 nnewr=  6 nolds=  0 nnews=  0
 

 fdd(*) eigenvalues. symmetry block  5
    -0.978917
 i,qaaresolved                     1 -0.679353855546443     
 i,qaaresolved                     2  0.458492320458096     

 qvv(*) eigenvalues. symmetry block  5
     1.480362    1.645545    1.874897
 i,qaaresolved                     1 -0.858698401961333     
 i,qaaresolved                     2  0.215220846611691     

 qvv(*) eigenvalues. symmetry block  6
     1.565295    1.838940
 i,qaaresolved                     1 -0.736622098886333     
 i,qaaresolved                     2  0.389458926124826     

 qvv(*) eigenvalues. symmetry block  7
     0.761908    1.626965    1.771233    2.159893
 i,qaaresolved                     1 -0.533760147237799     
 i,qaaresolved                     2  0.646549486488722     

 qvv(*) eigenvalues. symmetry block  8
     1.726408    1.941920

 restrt: restart information saved on the restart file (unit= 13).

 not all mcscf convergence criteria are satisfied.
 iter=    4 emc=   -383.3116585104 demc= 8.3417E-04 wnorm= 6.9804E-03 knorm= 4.9773E-02 apxde= 1.0492E-04    *not conv.*     

               starting mcscf iteration...   5

 orbital-state coupling will not be calculated this iteration.

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     8, naopsy(1) =     0, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 16383, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 32768

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore= 498070096

 inoutp: segmentation information:
 in-core transformation space,   avcinc = 497968960
 address segment size,           sizesg = 497963504
 number of in-core blocks,       nincbk =        19
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:       3446 transformed 1/r12    array elements were written in       1 records.


 Size of orbital-Hessian matrix B:                      403
 Total size of the state Hessian matrix M:                0
 Total size of HESSIAN-matrix for linear con            403


 mosort: allocated sort2 space, avc2is=   497997482 available sort2 space, avcisx=   497997734

   2 trial vectors read from nvfile (unit= 29).
 ciiter=  13 noldhv=  2 noldv=  2

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -383.3118078148      -13.6950168540        0.0000007168        0.0000010000
    2      -383.0638641259      -13.4470731651        0.0079314142        0.0100000000
 
  tol(10)=  0.000000000000000E+000  eshsci=  4.225081600844884E-004
 Total number of micro iterations:    6

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.99978623 pnorm= 0.0000E+00 rznorm= 8.1979E-07 rpnorm= 0.0000E+00 noldr=  6 nnewr=  6 nolds=  0 nnews=  0
 

 fdd(*) eigenvalues. symmetry block  5
    -0.967346
 i,qaaresolved                     1 -0.690898976103639     
 i,qaaresolved                     2  0.460053805159595     

 qvv(*) eigenvalues. symmetry block  5
     1.480266    1.643414    1.875551
 i,qaaresolved                     1 -0.858315608050942     
 i,qaaresolved                     2  0.216944127634930     

 qvv(*) eigenvalues. symmetry block  6
     1.565474    1.837898
 i,qaaresolved                     1 -0.735663149678897     
 i,qaaresolved                     2  0.407487658416758     

 qvv(*) eigenvalues. symmetry block  7
     0.750659    1.625590    1.767532    2.159605
 i,qaaresolved                     1 -0.533863446082783     
 i,qaaresolved                     2  0.646453436363234     

 qvv(*) eigenvalues. symmetry block  8
     1.727199    1.941689

 restrt: restart information saved on the restart file (unit= 13).

 not all mcscf convergence criteria are satisfied.
 iter=    5 emc=   -383.3118078148 demc= 1.4930E-04 wnorm= 3.3801E-03 knorm= 2.0676E-02 apxde= 1.8858E-05    *not conv.*     

               starting mcscf iteration...   6

 orbital-state coupling will not be calculated this iteration.

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     8, naopsy(1) =     0, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 16383, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 32768

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore= 498070096

 inoutp: segmentation information:
 in-core transformation space,   avcinc = 497968960
 address segment size,           sizesg = 497963504
 number of in-core blocks,       nincbk =        19
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:       3446 transformed 1/r12    array elements were written in       1 records.


 Size of orbital-Hessian matrix B:                      403
 Total size of the state Hessian matrix M:                0
 Total size of HESSIAN-matrix for linear con            403


 mosort: allocated sort2 space, avc2is=   497997482 available sort2 space, avcisx=   497997734

   2 trial vectors read from nvfile (unit= 29).
 ciiter=  13 noldhv=  2 noldv=  2

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -383.3118346400      -13.6950436792        0.0000007868        0.0000010000
    2      -383.0634782180      -13.4466872572        0.0079217476        0.0100000000
 
  tol(10)=  0.000000000000000E+000  eshsci=  1.901695078431730E-004
 Total number of micro iterations:    6

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0=-0.99996242 pnorm= 0.0000E+00 rznorm= 3.2402E-07 rpnorm= 0.0000E+00 noldr=  6 nnewr=  6 nolds=  0 nnews=  0
 

 fdd(*) eigenvalues. symmetry block  5
    -0.962376
 i,qaaresolved                     1 -0.695884034136056     
 i,qaaresolved                     2  0.460370710455596     

 qvv(*) eigenvalues. symmetry block  5
     1.480251    1.642784    1.875810
 i,qaaresolved                     1 -0.858189115515664     
 i,qaaresolved                     2  0.217699160034678     

 qvv(*) eigenvalues. symmetry block  6
     1.565548    1.837437
 i,qaaresolved                     1 -0.735230930615929     
 i,qaaresolved                     2  0.415369181663447     

 qvv(*) eigenvalues. symmetry block  7
     0.745686    1.624839    1.766084    2.159455
 i,qaaresolved                     1 -0.533939132547526     
 i,qaaresolved                     2  0.646529843152674     

 qvv(*) eigenvalues. symmetry block  8
     1.727603    1.941405

 restrt: restart information saved on the restart file (unit= 13).

 not all mcscf convergence criteria are satisfied.
 iter=    6 emc=   -383.3118346400 demc= 2.6825E-05 wnorm= 1.5214E-03 knorm= 8.6690E-03 apxde= 3.3812E-06    *not conv.*     

               starting mcscf iteration...   7

 orbital-state coupling will not be calculated this iteration.

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     8, naopsy(1) =     0, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 16383, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 32768

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore= 498070096

 inoutp: segmentation information:
 in-core transformation space,   avcinc = 497968960
 address segment size,           sizesg = 497963504
 number of in-core blocks,       nincbk =        19
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:       3446 transformed 1/r12    array elements were written in       1 records.


 Size of orbital-Hessian matrix B:                      403
 Total size of the state Hessian matrix M:                0
 Total size of HESSIAN-matrix for linear con            403


 mosort: allocated sort2 space, avc2is=   497997482 available sort2 space, avcisx=   497997734

   2 trial vectors read from nvfile (unit= 29).
 ciiter=  13 noldhv=  2 noldv=  2

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -383.3118394491      -13.6950484883        0.0000008468        0.0000010000
    2      -383.0632826740      -13.4464917132        0.0078761691        0.0100000000
 
  tol(10)=  0.000000000000000E+000  eshsci=  8.262183847619000E-005
 Total number of micro iterations:    6

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0=-0.99999335 pnorm= 0.0000E+00 rznorm= 1.3258E-07 rpnorm= 0.0000E+00 noldr=  6 nnewr=  6 nolds=  0 nnews=  0
 

 fdd(*) eigenvalues. symmetry block  5
    -0.960264
 i,qaaresolved                     1 -0.698005543602395     
 i,qaaresolved                     2  0.460454160166475     

 qvv(*) eigenvalues. symmetry block  5
     1.480248    1.642559    1.875918
 i,qaaresolved                     1 -0.858141341218920     
 i,qaaresolved                     2  0.218026285395575     

 qvv(*) eigenvalues. symmetry block  6
     1.565580    1.837237
 i,qaaresolved                     1 -0.735042284449968     
 i,qaaresolved                     2  0.418738933988537     

 qvv(*) eigenvalues. symmetry block  7
     0.743554    1.624489    1.765495    2.159388
 i,qaaresolved                     1 -0.533976892042317     
 i,qaaresolved                     2  0.646594626135700     

 qvv(*) eigenvalues. symmetry block  8
     1.727781    1.941245

 restrt: restart information saved on the restart file (unit= 13).

 not all mcscf convergence criteria are satisfied.
 iter=    7 emc=   -383.3118394491 demc= 4.8091E-06 wnorm= 6.6097E-04 knorm= 3.6481E-03 apxde= 6.0430E-07    *not conv.*     

               starting mcscf iteration...   8

 orbital-state coupling will be calculated this iteration.

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     8, naopsy(1) =     0, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 16383, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 32768

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore= 498070096

 inoutp: segmentation information:
 in-core transformation space,   avcinc = 497968960
 address segment size,           sizesg = 497963504
 number of in-core blocks,       nincbk =        19
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:       3446 transformed 1/r12    array elements were written in       1 records.


 Size of orbital-Hessian matrix B:                      403
 Size of the orbital-state Hessian matrix C:          12636
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:          13039


 mosort: allocated sort2 space, avc2is=   497997482 available sort2 space, avcisx=   497997734

   2 trial vectors read from nvfile (unit= 29).
 ciiter=  13 noldhv=  2 noldv=  2

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -383.3118403085      -13.6950493477        0.0000008731        0.0000010000
    2      -383.0631943567      -13.4464033959        0.0078519794        0.0100000000
 
  tol(10)=  0.000000000000000E+000  eshsci=  3.532176984312676E-005
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 Total number of micro iterations:   11

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0=-0.99999647 pnorm= 3.9557E-03 rznorm= 5.2839E-07 rpnorm= 4.6785E-07 noldr=  8 nnewr=  8 nolds= 10 nnews= 10
 

 fdd(*) eigenvalues. symmetry block  5
    -0.959370
 i,qaaresolved                     1 -0.698903876515745     
 i,qaaresolved                     2  0.460481175996116     

 qvv(*) eigenvalues. symmetry block  5
     1.480246    1.642470    1.875963
 i,qaaresolved                     1 -0.858122110155372     
 i,qaaresolved                     2  0.218166174172871     

 qvv(*) eigenvalues. symmetry block  6
     1.565593    1.837151
 i,qaaresolved                     1 -0.734961425563142     
 i,qaaresolved                     2  0.420167613948676     

 qvv(*) eigenvalues. symmetry block  7
     0.742649    1.624336    1.765251    2.159359
 i,qaaresolved                     1 -0.533993886718492     
 i,qaaresolved                     2  0.646627807053054     

 qvv(*) eigenvalues. symmetry block  8
     1.727858    1.941170

 restrt: restart information saved on the restart file (unit= 13).

 not all mcscf convergence criteria are satisfied.
 iter=    8 emc=   -383.3118403085 demc= 8.5945E-07 wnorm= 2.8257E-04 knorm= 2.6575E-03 apxde= 1.8638E-07    *not conv.*     

               starting mcscf iteration...   9

 orbital-state coupling will be calculated this iteration.

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     8, naopsy(1) =     0, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 16383, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 32768

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore= 498070096

 inoutp: segmentation information:
 in-core transformation space,   avcinc = 497968960
 address segment size,           sizesg = 497963504
 number of in-core blocks,       nincbk =        19
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:       3446 transformed 1/r12    array elements were written in       1 records.


 Size of orbital-Hessian matrix B:                      403
 Size of the orbital-state Hessian matrix C:          12636
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:          13039


 mosort: allocated sort2 space, avc2is=   497997482 available sort2 space, avcisx=   497997734

   3 trial vectors read from nvfile (unit= 29).
 ciiter=  13 noldhv=  3 noldv=  3

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -383.3118404951      -13.6950495343        0.0000007900        0.0000010000
    2      -383.0631207381      -13.4463297773        0.0080954151        0.0100000000
 
  tol(10)=  0.000000000000000E+000  eshsci=  1.136415595508792E-007
 performing all-state projection
 Total number of micro iterations:    1

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 1.00000000 pnorm= 0.0000E+00 rznorm= 4.3750E-07 rpnorm= 1.4720E-07 noldr=  1 nnewr=  1 nolds=  0 nnews=  0
 

 fdd(*) eigenvalues. symmetry block  5
    -0.958717
 i,qaaresolved                     1 -0.699560403712406     
 i,qaaresolved                     2  0.460497801260059     

 qvv(*) eigenvalues. symmetry block  5
     1.480246    1.642407    1.875996
 i,qaaresolved                     1 -0.858108410914894     
 i,qaaresolved                     2  0.218269089929386     

 qvv(*) eigenvalues. symmetry block  6
     1.565603    1.837088
 i,qaaresolved                     1 -0.734902008466143     
 i,qaaresolved                     2  0.421211757467248     

 qvv(*) eigenvalues. symmetry block  7
     0.741987    1.624222    1.765075    2.159337
 i,qaaresolved                     1 -0.534006704788857     
 i,qaaresolved                     2  0.646653893135517     

 qvv(*) eigenvalues. symmetry block  8
     1.727914    1.941114

 restrt: restart information saved on the restart file (unit= 13).

 not all mcscf convergence criteria are satisfied.
 iter=    9 emc=   -383.3118404951 demc= 1.8658E-07 wnorm= 9.0913E-07 knorm= 2.8747E-07 apxde= 1.3087E-13    *not conv.*     

               starting mcscf iteration...  10

 orbital-state coupling will be calculated this iteration.

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     8, naopsy(1) =     0, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 16383, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 32768

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore= 498070096

 inoutp: segmentation information:
 in-core transformation space,   avcinc = 497968960
 address segment size,           sizesg = 497963504
 number of in-core blocks,       nincbk =        19
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:       3446 transformed 1/r12    array elements were written in       1 records.


 Size of orbital-Hessian matrix B:                      403
 Size of the orbital-state Hessian matrix C:          12636
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:          13039


 mosort: allocated sort2 space, avc2is=   497997482 available sort2 space, avcisx=   497997734

   2 trial vectors read from nvfile (unit= 29).
 ciiter=  13 noldhv=  2 noldv=  2

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -383.3118404951      -13.6950495343        0.0000008919        0.0000010000
    2      -383.0631277611      -13.4463368003        0.0078334169        0.0100000000
 
  tol(10)=  0.000000000000000E+000  eshsci=  5.813649229086420E-008
 performing all-state projection
 Total number of micro iterations:    1

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 1.00000000 pnorm= 0.0000E+00 rznorm= 4.0724E-07 rpnorm= 7.0342E-08 noldr=  1 nnewr=  1 nolds=  0 nnews=  0
 

 fdd(*) eigenvalues. symmetry block  5
    -0.958717
 i,qaaresolved                     1 -0.699560403202524     
 i,qaaresolved                     2  0.460497765250525     

 qvv(*) eigenvalues. symmetry block  5
     1.480246    1.642407    1.875996
 i,qaaresolved                     1 -0.858108410429422     
 i,qaaresolved                     2  0.218269070962802     

 qvv(*) eigenvalues. symmetry block  6
     1.565603    1.837088
 i,qaaresolved                     1 -0.734901955265786     
 i,qaaresolved                     2  0.421211788007710     

 qvv(*) eigenvalues. symmetry block  7
     0.741987    1.624222    1.765075    2.159337
 i,qaaresolved                     1 -0.534006703022378     
 i,qaaresolved                     2  0.646653891729417     

 qvv(*) eigenvalues. symmetry block  8
     1.727914    1.941114

 restrt: restart information saved on the restart file (unit= 13).

 all mcscf convergence criteria are satisfied.

 final mcscf convergence values:
 iter=   10 emc=   -383.3118404951 demc=-4.5475E-13 wnorm= 4.6509E-07 knorm= 2.5118E-07 apxde= 5.8396E-14    *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 wt 1.000 total energy=     -383.311840495, rel. (eV)=   0.000000
   ------------------------------------------------------------


 MO-coefficient print-out skipped (no flag 32)
 They may be found in the MOCOEF directory.

          natural orbitals of the final iteration, block  5

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6
  occ(*)=     2.00000000     1.92406689     0.06548537     0.00000000     0.00000000     0.00000000

          natural orbitals of the final iteration, block  6

               MO    1        MO    2        MO    3        MO    4
  occ(*)=     1.94315687     0.12898924     0.00000000     0.00000000

          natural orbitals of the final iteration, block  7

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6
  occ(*)=     1.93338590     0.07574724     0.00000000     0.00000000     0.00000000     0.00000000

          natural orbitals of the final iteration, block  8

               MO    1        MO    2        MO    3        MO    4
  occ(*)=     1.87431973     0.05484876     0.00000000     0.00000000
 d1(*), fmc(*), and qmc(*) written to the 1-particle density matrix file.
        211 d2(*) elements written to the 2-particle density matrix file: mcd2fl                                                      


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        1u  partial gross atomic populations
   ao class       11u        21u        31u        41u        51u        61u 
    to_ s       2.000000   1.924067   0.065485   0.000000   0.000000   0.000000

                        2g  partial gross atomic populations
   ao class       12g        22g        32g        42g 
    to_ s       1.943157   0.128989   0.000000   0.000000

                        3g  partial gross atomic populations
   ao class       13g        23g        33g        43g        53g        63g 
    to_ s       1.933386   0.075747   0.000000   0.000000   0.000000   0.000000

                        u   partial gross atomic populations
   ao class       1u         2u         3u         4u  
    to_ s       1.874320   0.054849   0.000000   0.000000


                        gross atomic populations
     ao           to_
      s        10.000000
    total      10.000000
 

 Total number of electrons:   10.00000000

