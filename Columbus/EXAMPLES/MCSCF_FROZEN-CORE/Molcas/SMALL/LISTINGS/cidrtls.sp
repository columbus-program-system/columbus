 
 program cidrt 5.9  

 distinct row table construction, reference csf selection, and internal
 walk selection for multireference single- and double-excitation
configuration interaction.

 references:  r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).
              h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. symp. 15, 91 (1981).

 based on the initial version by  Ron Shepard

 extended for spin-orbit CI calculations ( Russ Pitzer, OSU)

 and large active spaces (Thomas Müller, FZ Juelich)

 version date: 16-jul-04


 This Version of Program CIDRT is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIDRT       **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 workspace allocation parameters: lencor= 498073600 mem1=         0 ifirst=         1
 expanded "keystrokes" are being written to file:
 /scratch/phasan/76124/WORK/cidrtky                                              
 Spin-Orbit CI Calculation?(y,[n]) Spin-Free Calculation
 
 input the spin multiplicity [  0]: spin multiplicity, smult            :   1    singlet 
 input the total number of electrons [  0]: total number of electrons, nelt     :    10
 input the number of irreps (1:8) [  0]: point group dimension, nsym         :     8
 enter symmetry labels:(y,[n]) enter 8 labels (a4):
 enter symmetry label, default=   1
 enter symmetry label, default=   2
 enter symmetry label, default=   3
 enter symmetry label, default=   4
 enter symmetry label, default=   5
 enter symmetry label, default=   6
 enter symmetry label, default=   7
 enter symmetry label, default=   8
 symmetry labels: (symmetry, slabel)
 ( 1,  ag ) ( 2,  b3u) ( 3,  b2u) ( 4,  b1g) ( 5,  b1u) ( 6,  b2g) ( 7,  b3g) ( 8,  au ) 
 input nmpsy(*):
 nmpsy(*)=         0   0   0   0   6   4   6   4
 
   symmetry block summary
 block(*)=         1   2   3   4   5   6   7   8
 slabel(*)=      ag  b3u b2u b1g b1u b2g b3g au 
 nmpsy(*)=         0   0   0   0   6   4   6   4
 
 total molecular orbitals            :    20
 input the molecular spatial symmetry (irrep 1:nsym) [  0]: state spatial symmetry label        :  ag 
 
 input the frozen core orbitals (sym(i),rmo(i),i=1,nfct):
 total frozen core orbitals, nfct    :     0
 no frozen core orbitals entered
 
 number of frozen core orbitals      :     0
 number of frozen core electrons     :     0
 number of internal electrons        :    10
 
 input the frozen virtual orbitals (sym(i),rmo(i),i=1,nfvt):
 total frozen virtual orbitals, nfvt :     0

 no frozen virtual orbitals entered
 
 input the internal orbitals (sym(i),rmo(i),i=1,niot):
 niot                                :     9
 
 modrt(*)=         1   2   3   7   8  11  12  17  18
 slabel(*)=      b1u b1u b1u b2g b2g b3g b3g au  au 
 
 total number of orbitals            :    20
 number of frozen core orbitals      :     0
 number of frozen virtual orbitals   :     0
 number of internal orbitals         :     9
 number of external orbitals         :    11
 
 orbital-to-level mapping vector
 map(*)=          12  13  14   1   2   3  15  16   4   5  17  18   6   7   8
                   9  19  20  10  11
 
 input the number of ref-csf doubly-occupied orbitals [  0]: (ref) doubly-occupied orbitals      :     1
 
 no. of internal orbitals            :     9
 no. of doubly-occ. (ref) orbitals   :     1
 no. active (ref) orbitals           :     8
 no. of active electrons             :     8
 
 input the active-orbital, active-electron occmnr(*):
   2  3  7  8 11 12 17 18
 input the active-orbital, active-electron occmxr(*):
   2  3  7  8 11 12 17 18
 
 actmo(*) =        2   3   7   8  11  12  17  18
 occmnr(*)=        0   0   0   0   0   0   0   8
 occmxr(*)=        8   8   8   8   8   8   8   8
 reference csf cumulative electron occupations:
 modrt(*)=         1   2   3   7   8  11  12  17  18
 occmnr(*)=        2   2   2   2   2   2   2   2  10
 occmxr(*)=        2  10  10  10  10  10  10  10  10
 
 input the active-orbital bminr(*):
   2  3  7  8 11 12 17 18
 input the active-orbital bmaxr(*):
   2  3  7  8 11 12 17 18
 reference csf b-value constraints:
 modrt(*)=         1   2   3   7   8  11  12  17  18
 bminr(*)=         0   0   0   0   0   0   0   0   0
 bmaxr(*)=         0   8   8   8   8   8   8   8   8
 input the active orbital smaskr(*):
   2  3  7  8 11 12 17 18
 modrt:smaskr=
   1:1000   2:1111   3:1111   7:1111   8:1111  11:1111  12:1111  17:1111
  18:1111
 
 input the maximum excitation level from the reference csfs [  2]: maximum excitation from ref. csfs:  :     2
 number of internal electrons:       :    10
 
 input the internal-orbital mrsdci occmin(*):
   1  2  3  7  8 11 12 17 18
 input the internal-orbital mrsdci occmax(*):
   1  2  3  7  8 11 12 17 18
 mrsdci csf cumulative electron occupations:
 modrt(*)=         1   2   3   7   8  11  12  17  18
 occmin(*)=        0   0   0   0   0   0   0   0   8
 occmax(*)=       10  10  10  10  10  10  10  10  10
 
 input the internal-orbital mrsdci bmin(*):
   1  2  3  7  8 11 12 17 18
 input the internal-orbital mrsdci bmax(*):
   1  2  3  7  8 11 12 17 18
 mrsdci b-value constraints:
 modrt(*)=         1   2   3   7   8  11  12  17  18
 bmin(*)=          0   0   0   0   0   0   0   0   0
 bmax(*)=         10  10  10  10  10  10  10  10  10
 
 input the internal-orbital smask(*):
   1  2  3  7  8 11 12 17 18
 modrt:smask=
   1:1111   2:1111   3:1111   7:1111   8:1111  11:1111  12:1111  17:1111
  18:1111
 
 internal orbital summary:
 block(*)=         5   5   5   6   6   7   7   8   8
 slabel(*)=      b1u b1u b1u b2g b2g b3g b3g au  au 
 rmo(*)=           1   2   3   1   2   1   2   1   2
 modrt(*)=         1   2   3   7   8  11  12  17  18
 
 reference csf info:
 occmnr(*)=        2   2   2   2   2   2   2   2  10
 occmxr(*)=        2  10  10  10  10  10  10  10  10
 
 bminr(*)=         0   0   0   0   0   0   0   0   0
 bmaxr(*)=         0   8   8   8   8   8   8   8   8
 
 
 mrsdci csf info:
 occmin(*)=        0   0   0   0   0   0   0   0   8
 occmax(*)=       10  10  10  10  10  10  10  10  10
 
 bmin(*)=          0   0   0   0   0   0   0   0   0
 bmax(*)=         10  10  10  10  10  10  10  10  10
 

 a priori removal of distinct rows:

 input the level, a, and b values for the vertices 
 to be removed (-1/ to end).

 input level, a, and b (-1/ to end):
 no vertices marked for removal
 
 impose generalized interacting space restrictions?(y,[n]) generalized interacting space restrictions will be imposed.
 multp                     0                     0                     0
                     0                     0                     0
                     0                     0                     0
 spnir
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  hmult                     0
 lxyzir                     0                     0                     0
 spnir
  1  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  1  0  0  0  1  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  1  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  1  0  0  0  1  0  0  0  1  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  1  0  0  0  1  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0

 number of rows in the drt :  97
     1 arcs removed due to generalized interacting space restrictions.

 manual arc removal step:


 input the level, a, b, and step values 
 for the arcs to be removed (-1/ to end).

 input the level, a, b, and step (-1/ to end):
 remarc:   0 arcs removed out of   0 specified.

 xbarz=        5292
 xbary=        8820
 xbarx=        3864
 xbarw=        5292
        --------
 nwalk=       23268
 input the range of drt levels to print (l1,l2):
 levprt(*)        -1   0

 reference-csf selection step 1:
 total number of z-walks in the drt, nzwalk=    5292

 input the list of allowed reference symmetries:
 allowed reference symmetries:             1
 allowed reference symmetry labels:      ag 
 keep all of the z-walks as references?(y,[n]) all z-walks are initially deleted.
 
 generate walks while applying reference drt restrictions?([y],n) reference drt restrictions will be imposed on the z-walks.
 
 impose additional orbital-group occupation restrictions?(y,[n]) 
 apply primary reference occupation restrictions?(y,[n]) 
 manually select individual walks?(y,[n])
 step 1 reference csf selection complete.
      468 csfs initially selected from    5292 total walks.

 beginning step-vector based selection.
 enter [internal_orbital_step_vector/disposition] pairs:

 enter internal orbital step vector, (-1/ to end):
   1  2  3  7  8 11 12 17 18

 step 2 reference csf selection complete.
      468 csfs currently selected from    5292 total walks.

 beginning numerical walk based selection.
 enter positive walk numbers to add walks,
 negative walk numbers to delete walks, and zero to end:

 input reference walk number (0 to end) [  0]:
 numerical walk-number based selection complete.
      468 reference csfs selected from    5292 total z-walks.
 
 input the reference occupations, mu(*):
 reference occupations:
 mu(*)=            2   0   0   0   0   0   0   0   0
 
 interacting space determination:
 checking diagonal loops...
 checking 2-internal loops...
 checking 3-internal loops...
 checking 4-internal loops...
 
 this is an obsolete prompt.(y,[n])
 final mrsdci walk selection step:

 nvalw(*)=    1368    8660    3704    3836 nvalwt=   17568

 enter positive walk numbers to add walks,
 negative walk numbers to delete walks, and zero to end.

 input mrsdci walk number (0 to end) [  0]:
 end of manual mrsdci walk selection.
 number added=   0 number removed=   0

 nvalw(*)=    1368    8660    3704    3836 nvalwt=   17568


 lprune: l(*,*,*) pruned with nwalk=   23268 nvalwt=   17568
 lprune:  z-drt, nprune=   121
 lprune:  y-drt, nprune=    46
 lprune: wx-drt, nprune=    63

 xbarz=        2672
 xbary=        8820
 xbarx=        3864
 xbarw=        5288
        --------
 nwalk=       20644
 levprt(*)        -1   0

 beginning the reference csf index recomputation...

     iref   iwalk  step-vector
   ------  ------  ------------
        1       1  333330000
        2       2  333303000
        3       3  333301200
        4       6  333300300
        5       9  333300030
        6      10  333300012
        7      11  333300003
        8      12  333123000
        9      13  333121200
       10      16  333120300
       11      19  333120030
       12      20  333120012
       13      21  333120003
       14      22  333112200
       15      27  333110022
       16      28  333033000
       17      29  333031200
       18      32  333030300
       19      35  333030030
       20      36  333030012
       21      37  333030003
       22      38  333003300
       23      41  333003030
       24      42  333003012
       25      43  333003003
       26      46  333001230
       27      47  333001212
       28      48  333001203
       29      49  333001122
       30      52  333000330
       31      53  333000312
       32      54  333000303
       33      57  333000033
       34      60  331321020
       35      61  331321002
       36      63  331320120
       37      64  331320102
       38      69  331312020
       39      70  331312002
       40      71  331310220
       41      72  331310202
       42      76  331231020
       43      77  331231002
       44      79  331230120
       45      80  331230102
       46      85  331203120
       47      86  331203102
       48      90  331201320
       49      91  331201302
       50      96  331201032
       51      97  331201023
       52     101  331200132
       53     102  331200123
       54     105  331132020
       55     106  331132002
       56     107  331130220
       57     108  331130202
       58     110  331103220
       59     111  331103202
       60     113  331102320
       61     114  331102302
       62     119  331102032
       63     120  331102023
       64     123  331100232
       65     124  331100223
       66     126  331023120
       67     127  331023102
       68     131  331021320
       69     132  331021302
       70     137  331021032
       71     138  331021023
       72     142  331020132
       73     143  331020123
       74     145  331013220
       75     146  331013202
       76     148  331012320
       77     149  331012302
       78     154  331012032
       79     155  331012023
       80     158  331010232
       81     159  331010223
       82     160  330333000
       83     161  330331200
       84     164  330330300
       85     167  330330030
       86     168  330330012
       87     169  330330003
       88     170  330303300
       89     173  330303030
       90     174  330303012
       91     175  330303003
       92     178  330301230
       93     179  330301212
       94     180  330301203
       95     181  330301122
       96     184  330300330
       97     185  330300312
       98     186  330300303
       99     189  330300033
      100     190  330123300
      101     193  330123030
      102     194  330123012
      103     195  330123003
      104     198  330121230
      105     199  330121212
      106     200  330121203
      107     201  330121122
      108     204  330120330
      109     205  330120312
      110     206  330120303
      111     209  330120033
      112     212  330113022
      113     215  330112230
      114     216  330112212
      115     217  330112203
      116     218  330112122
      117     221  330111222
      118     222  330110322
      119     225  330033300
      120     228  330033030
      121     229  330033012
      122     230  330033003
      123     233  330031230
      124     234  330031212
      125     235  330031203
      126     236  330031122
      127     239  330030330
      128     240  330030312
      129     241  330030303
      130     244  330030033
      131     245  330003330
      132     246  330003312
      133     247  330003303
      134     250  330003033
      135     253  330001233
      136     254  330000333
      137     257  313321020
      138     258  313321002
      139     260  313320120
      140     261  313320102
      141     266  313312020
      142     267  313312002
      143     268  313310220
      144     269  313310202
      145     273  313231020
      146     274  313231002
      147     276  313230120
      148     277  313230102
      149     282  313203120
      150     283  313203102
      151     287  313201320
      152     288  313201302
      153     293  313201032
      154     294  313201023
      155     298  313200132
      156     299  313200123
      157     302  313132020
      158     303  313132002
      159     304  313130220
      160     305  313130202
      161     307  313103220
      162     308  313103202
      163     310  313102320
      164     311  313102302
      165     316  313102032
      166     317  313102023
      167     320  313100232
      168     321  313100223
      169     323  313023120
      170     324  313023102
      171     328  313021320
      172     329  313021302
      173     334  313021032
      174     335  313021023
      175     339  313020132
      176     340  313020123
      177     342  313013220
      178     343  313013202
      179     345  313012320
      180     346  313012302
      181     351  313012032
      182     352  313012023
      183     355  313010232
      184     356  313010223
      185     357  312333000
      186     358  312331200
      187     361  312330300
      188     364  312330030
      189     365  312330012
      190     366  312330003
      191     367  312303300
      192     370  312303030
      193     371  312303012
      194     372  312303003
      195     375  312301230
      196     376  312301212
      197     377  312301203
      198     378  312301122
      199     381  312300330
      200     382  312300312
      201     383  312300303
      202     386  312300033
      203     387  312123300
      204     390  312123030
      205     391  312123012
      206     392  312123003
      207     395  312121230
      208     396  312121212
      209     397  312121203
      210     398  312121122
      211     401  312120330
      212     402  312120312
      213     403  312120303
      214     406  312120033
      215     409  312113022
      216     412  312112230
      217     413  312112212
      218     414  312112203
      219     415  312112122
      220     418  312111222
      221     419  312110322
      222     422  312033300
      223     425  312033030
      224     426  312033012
      225     427  312033003
      226     430  312031230
      227     431  312031212
      228     432  312031203
      229     433  312031122
      230     436  312030330
      231     437  312030312
      232     438  312030303
      233     441  312030033
      234     442  312003330
      235     443  312003312
      236     444  312003303
      237     447  312003033
      238     450  312001233
      239     451  312000333
      240     452  311332200
      241     457  311330022
      242     460  311303022
      243     463  311302230
      244     464  311302212
      245     465  311302203
      246     466  311302122
      247     469  311301222
      248     470  311300322
      249     473  311223300
      250     476  311223030
      251     477  311223012
      252     478  311223003
      253     481  311221230
      254     482  311221212
      255     483  311221203
      256     484  311221122
      257     487  311220330
      258     488  311220312
      259     489  311220303
      260     492  311220033
      261     495  311213022
      262     498  311212230
      263     499  311212212
      264     500  311212203
      265     501  311212122
      266     504  311211222
      267     505  311210322
      268     510  311123022
      269     513  311122230
      270     514  311122212
      271     515  311122203
      272     516  311122122
      273     519  311121222
      274     520  311120322
      275     523  311112222
      276     526  311033022
      277     529  311032230
      278     530  311032212
      279     531  311032203
      280     532  311032122
      281     535  311031222
      282     536  311030322
      283     539  311003322
      284     544  311002233
      285     546  310323120
      286     547  310323102
      287     551  310321320
      288     552  310321302
      289     557  310321032
      290     558  310321023
      291     562  310320132
      292     563  310320123
      293     565  310313220
      294     566  310313202
      295     568  310312320
      296     569  310312302
      297     574  310312032
      298     575  310312023
      299     578  310310232
      300     579  310310223
      301     581  310233120
      302     582  310233102
      303     586  310231320
      304     587  310231302
      305     592  310231032
      306     593  310231023
      307     597  310230132
      308     598  310230123
      309     603  310203132
      310     604  310203123
      311     606  310201332
      312     607  310201323
      313     610  310133220
      314     611  310133202
      315     613  310132320
      316     614  310132302
      317     619  310132032
      318     620  310132023
      319     623  310130232
      320     624  310130223
      321     626  310103232
      322     627  310103223
      323     628  310102332
      324     629  310102323
      325     634  310023132
      326     635  310023123
      327     637  310021332
      328     638  310021323
      329     642  310013232
      330     643  310013223
      331     644  310012332
      332     645  310012323
      333     647  303333000
      334     648  303331200
      335     651  303330300
      336     654  303330030
      337     655  303330012
      338     656  303330003
      339     657  303303300
      340     660  303303030
      341     661  303303012
      342     662  303303003
      343     665  303301230
      344     666  303301212
      345     667  303301203
      346     668  303301122
      347     671  303300330
      348     672  303300312
      349     673  303300303
      350     676  303300033
      351     677  303123300
      352     680  303123030
      353     681  303123012
      354     682  303123003
      355     685  303121230
      356     686  303121212
      357     687  303121203
      358     688  303121122
      359     691  303120330
      360     692  303120312
      361     693  303120303
      362     696  303120033
      363     699  303113022
      364     702  303112230
      365     703  303112212
      366     704  303112203
      367     705  303112122
      368     708  303111222
      369     709  303110322
      370     712  303033300
      371     715  303033030
      372     716  303033012
      373     717  303033003
      374     720  303031230
      375     721  303031212
      376     722  303031203
      377     723  303031122
      378     726  303030330
      379     727  303030312
      380     728  303030303
      381     731  303030033
      382     732  303003330
      383     733  303003312
      384     734  303003303
      385     737  303003033
      386     740  303001233
      387     741  303000333
      388     743  301323120
      389     744  301323102
      390     748  301321320
      391     749  301321302
      392     754  301321032
      393     755  301321023
      394     759  301320132
      395     760  301320123
      396     762  301313220
      397     763  301313202
      398     765  301312320
      399     766  301312302
      400     771  301312032
      401     772  301312023
      402     775  301310232
      403     776  301310223
      404     778  301233120
      405     779  301233102
      406     783  301231320
      407     784  301231302
      408     789  301231032
      409     790  301231023
      410     794  301230132
      411     795  301230123
      412     800  301203132
      413     801  301203123
      414     803  301201332
      415     804  301201323
      416     807  301133220
      417     808  301133202
      418     810  301132320
      419     811  301132302
      420     816  301132032
      421     817  301132023
      422     820  301130232
      423     821  301130223
      424     823  301103232
      425     824  301103223
      426     825  301102332
      427     826  301102323
      428     831  301023132
      429     832  301023123
      430     834  301021332
      431     835  301021323
      432     839  301013232
      433     840  301013223
      434     841  301012332
      435     842  301012323
      436     844  300333300
      437     847  300333030
      438     848  300333012
      439     849  300333003
      440     852  300331230
      441     853  300331212
      442     854  300331203
      443     855  300331122
      444     858  300330330
      445     859  300330312
      446     860  300330303
      447     863  300330033
      448     864  300303330
      449     865  300303312
      450     866  300303303
      451     869  300303033
      452     872  300301233
      453     873  300300333
      454     874  300123330
      455     875  300123312
      456     876  300123303
      457     879  300123033
      458     882  300121233
      459     883  300120333
      460     884  300113322
      461     889  300112233
      462     890  300033330
      463     891  300033312
      464     892  300033303
      465     895  300033033
      466     898  300031233
      467     899  300030333
      468     900  300003333
 indx01:   468 elements set in vec01(*)

 beginning the valid upper walk index recomputation...
 indx01: 17568 elements set in vec01(*)

 beginning the final csym(*) computation...

  number of valid internal walks of each symmetry:

       ag      b3u     b2u     b1g     b1u     b2g     b3g     au 
      ----    ----    ----    ----    ----    ----    ----    ----
 z    1368       0       0       0       0       0       0       0
 y       0       0       0       0    2138    2162    2176    2184
 x     866     934     948     956       0       0       0       0
 w    1286     838     852     860       0       0       0       0

 csfs grouped by internal walk symmetry:

       ag      b3u     b2u     b1g     b1u     b2g     b3g     au 
      ----    ----    ----    ----    ----    ----    ----    ----
 z    1368       0       0       0       0       0       0       0
 y       0       0       0       0    6414    4324    8704    4368
 x    9526   13076   15168   13384       0       0       0       0
 w   28292   11732   13632   12040       0       0       0       0

 total csf counts:
 z-vertex:     1368
 y-vertex:    23810
 x-vertex:    51154
 w-vertex:    65696
           --------
 total:      142028
 
 input a title card, default=cidrt_title
 title card:
  cidrt_title                                                                   
  
 
 input a drt file name, default=cidrtfl
 drt and indexing arrays will be written to file:
 /scratch/phasan/76124/WORK/cidrtfl                                              
 
 write the drt file?([y],n) drt file is being written...
 wrtstr:  ag  b3u b2u b1g b1u b2g b3g au 
nwalk=   20644 cpos=    4033 maxval=    9 cmprfactor=   80.46 %.
nwalk=   20644 cpos=    2242 maxval=   99 cmprfactor=   78.28 %.
 compressed with: nwalk=   20644 cpos=    4044 maxval=    9 cmprfactor=   80.41 %.
initial index vector length:     20644
compressed index vector length:      4044reduction:  80.41%
nwalk=    2672 cpos=     609 maxval=    9 cmprfactor=   77.21 %.
nwalk=    2672 cpos=     406 maxval=   99 cmprfactor=   69.61 %.
 compressed with: nwalk=    2672 cpos=     609 maxval=    9 cmprfactor=   77.21 %.
initial ref vector length:      2672
compressed ref vector length:       609reduction:  77.21%
