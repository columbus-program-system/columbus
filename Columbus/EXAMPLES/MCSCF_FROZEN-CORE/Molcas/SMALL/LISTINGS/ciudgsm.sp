1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.

  This is a  mraqcc energy evaluation      

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  1  1   -383.3118404951  1.2257E-13  3.9848E-02  2.4511E-01  1.0000E-03
 mraqcc  #  2  1   -383.3576282457  4.5788E-02  4.9645E-03  8.8187E-02  1.0000E-03
 mraqcc  #  3  1   -383.3641141980  6.4860E-03  1.2270E-03  4.4023E-02  1.0000E-03
 mraqcc  #  4  1   -383.3656967460  1.5825E-03  3.4022E-04  2.4037E-02  1.0000E-03
 mraqcc  #  5  1   -383.3661087419  4.1200E-04  9.5862E-05  1.2592E-02  1.0000E-03
 mraqcc  #  6  1   -383.3662196532  1.1091E-04  2.7017E-05  6.8780E-03  1.0000E-03
 mraqcc  #  7  1   -383.3662474477  2.7794E-05  9.6980E-06  4.1152E-03  1.0000E-03
 mraqcc  #  8  1   -383.3662584988  1.1051E-05  2.8044E-06  2.2379E-03  1.0000E-03
 mraqcc  #  9  1   -383.3662616054  3.1065E-06  8.6979E-07  1.2472E-03  1.0000E-03
 mraqcc  # 10  1   -383.3662624596  8.5428E-07  4.2763E-07  8.7152E-04  1.0000E-03

 mraqcc   convergence criteria satisfied after 10 iterations.

 final mraqcc   convergence information:
 mraqcc  # 10  1   -383.3662624596  8.5428E-07  4.2763E-07  8.7152E-04  1.0000E-03

 number of reference csfs (nref) is   468.  root number (iroot) is  1.
 c0**2 =   0.93768173c0**2(scaled) =   0.99611643  c**2 (all zwalks) =   0.95019353
