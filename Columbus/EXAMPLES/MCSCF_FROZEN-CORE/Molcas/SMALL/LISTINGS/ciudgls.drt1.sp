1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      2009-03.    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


================ Computing sorting integral file structure ================

                    -----z----- -----y----- -----x----- -----w----- ---total---

                CSFs      1368       23810       51154       65696      142028
      internal walks      2672        8820        3864        5288       20644
valid internal walks      1368        8660        3704        3836       17568
 lcore1,lcore2=             498036007             498011921
 lencor,maxblo             498073600                 60000

 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg= 497957310
 minimum size of srtscr:     32767 WP (     1 records)
 maximum size of srtscr:    163835 WP (     3 records)
========================================
 current settings:
 minbl3          63
 minbl4          63
 locmaxbl3      252
 locmaxbuf      126
 maxbl3       60000
 maxbl3       60000
 maxbl4       60000
 maxbuf       30006
========================================

 sorted 4-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)

 sorted 3-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)
 Orig.  diagonal integrals:  1electron:        20
                             0ext.    :        90
                             2ext.    :       198
                             4ext.    :       132


 Orig. off-diag. integrals:  4ext.    :       753
                             3ext.    :      1863
                             2ext.    :      2310
                             1ext.    :      1365
                             0ext.    :       360
                             2ext. SO :         0
                             1ext. SO :         0
                             0ext. SO :         0
                             1electron:        22


 Sorted integrals            3ext.  w :      1613 x :      1363
                             4ext.  w :       533 x :       357


 compressed index vector length=                  4044
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 3
  GSET = 3
   NCOREL = 10
  NROOT = 1
  IVMODE = 3
  NBKITR = 1
  NVBKMN = 1
  RTOLBK = 1e-3,
  NITER = 20
  NVCIMN = 3
  RTOLCI = 1e-3,
  NVCIMX = 6
  NVRFMX = 6
  NVBKMX = 6
  IDEN  = 1
  CSFPRN = 10,
 /&end
 ------------------------------------------------------------------------
lodens (list->root)=  1
invlodens (root->list)=  1
 bummer (warning):2:changed keyword: nbkitr=                      0
 USING SEGMENTS OF EQUAL SIZE

****************  list of control variables  ****************
 lvlprt =    0      nroot  =    1      noldv  =   0      noldhv =   0
 nunitv =    1      nbkitr =    0      niter  =  20      davcor =  10
 csfprn =   10      ivmode =    3      istrt  =   0      vout   =   1
 iortls =    0      nvbkmx =    6      ibktv  =  -1      ibkthv =  -1
 nvcimx =    6      icitv  =   -1      icithv =  -1      frcsub =   0
 nvbkmn =    1      nvcimn =    3      maxseg =   4      nrfitr =  30
 ncorel =   10      nvrfmx =    6      nvrfmn =   3      iden   =   1
 itran  =    0      froot  =    0      rtmode =   0      ncouple=   1
 skipso =    F      dalton2=    0      molcas =   0      finalv =   0
 finalw =    0      cosmocalc=   0    with_tsklst=   0
 nsegwx =    1     1     1     1
 nseg0x =    1     1     1     1
 nseg1x =    1     1     1     1
 nseg2x =    1     1     1     1
 nseg3x =    1     1     1     1
 nseg4x =    1     1     1     1
 no0ex  =      0    no1ex  =      0    no2ex  =     0    no3ex  =     0
 no4ex  =      0    nodiag =      0
 cdg4ex =    1      c3ex1ex=    1      c2ex0ex=   1
 fileloc=    0     0     0     0     0     0     0     1     1     1
 directhd=   1      noaqccshift_zyxw=      0
 critical_crit=-1.00000    critical_delta= 0.05000

 ctol   = 0.010000    lrtshift=1.000000    smalld =0.001000


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-03    1.000E-03
 Computing density:                    .drt1.state1
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core          498073599 DP per process

********** Integral sort section *************


 workspace allocation information: lencor= 498073599

 echo of the input for program cisrt:
 ------------------------------------------------------------------------
  &input
  maxbl3=60000
  maxbl4=60000
  &end
 ------------------------------------------------------------------------
 
 ( 6) listing file:                    ciudgls             
 ( 5) input file:                      cisrtin   
 (17) cidrt file:                      cidrtfl             
 (11) transformed integrals file:      moints    
 (12) diagonal integral file:          diagint             
 (13) off-diagonal integral file:      ofdgint             
 (31) 4-external w integrals file:     fil4w               
 (32) 4-external x integrals file:     fil4x               
 (33) 3-external w integrals file:     fil3w               
 (34) 3-external x integrals file:     fil3x               
 (21) scratch da sorting file:         srtscr              
 (12) 2-e integral file [fsplit=2]:    moints2   

 input integral file header information:
 SEWARD INTEGRALS                                                                
 user-specified FREEZE(*) used in program tran.                                  
 SIFS file created by program tran.      adler.itc.univie. 15:55:12.313 19-Feb-12
  cidrt_title                                                                    
 MO-coefficients from mcscf.x                                                    
  total ao core energy = -369.616790961                                          
 SIFS file created by program tran.      owl7.itc.univie.a 15:03:23.595 19-Feb-12

 input energy(*) values:
 energy( 1)=  4.557548380050E+02, ietype=   -1,    core energy of type: Nuc.Rep.

 total core energy =   4.557548380050E+02

 nsym = 8 nmot=  20

 symmetry  =    1    2    3    4    5    6    7    8
 slabel(*) = ag   b3u  b2u  b1g  b1u  b2g  b3g  au  
 nmpsy(*)  =    0    0    0    0    6    4    6    4

 info(*) =          1      8192      6552      8192      5460         0

 orbital labels, i:molab(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012  13:tout:013  14:tout:014  15:tout:015  16:tout:016  17:tout:017  18:tout:018  19:tout:019  20:tout:020

 input parameters:
 prnopt=  0
 ldamin=    4095 ldamax=   32767 ldainc=      64
 maxbuf=   30006 maxbl3=   60000 maxbl4=   60000 intmxo=     766
  Using 32 bit compression 

 drt information:
  cidrt_title                                                                    
 nmotd =  20 nfctd =   0 nfvtc =   0 nmot  =  20
 nlevel =  20 niot  =   9 lowinl=  12
 orbital-to-level map(*)
   12  13  14   1   2   3  15  16   4   5  17  18   6   7   8   9  19  20  10  11
 compressed map(*)
   12  13  14   1   2   3  15  16   4   5  17  18   6   7   8   9  19  20  10  11
 levsym(*)
    5   5   5   6   6   7   7   7   7   8   8   5   5   5   6   6   7   7   8   8
 repartitioning mu(*)=
   2.  0.  0.  0.  0.  0.  0.  0.  0.

 new core energy added to the energy(*) list.
 from the integral file: h1_core= -8.253716289658E+02

 indxdg: diagonal integral statistics.
 total number of integrals contributing to diagonal matrix elements:       420
 number with all external indices:       132
 number with half external - half internal indices:       198
 number with all internal indices:        90

 indxof: off-diagonal integral statistics.
    4-external integrals: num=        753 strt=          1
    3-external integrals: num=       1863 strt=        754
    2-external integrals: num=       2310 strt=       2617
    1-external integrals: num=       1365 strt=       4927
    0-external integrals: num=        360 strt=       6292

 total number of off-diagonal integrals:        6651


 indxof(2nd)  ittp=   3 numx(ittp)=        2310
 indxof(2nd)  ittp=   4 numx(ittp)=        1365
 indxof(2nd)  ittp=   5 numx(ittp)=         360

 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg= 497898765
 pro2e        1     211     421     631     841     886     931    1141   44829   88517
   121284  129476  134936  156775

 pro2e:      5683 integrals read in     2 records.
 pro1e        1     211     421     631     841     886     931    1141   44829   88517
   121284  129476  134936  156775
 pro1e: eref =   -4.867636718613994E+00
 total size of srtscr:                     3  records of                  32767 
 WP =                786408 Bytes

 new core energy added to the energy(*) list.
 from the hamiltonian repartitioning, eref= -4.867636718614E+00
 putdg        1     211     421     631    1397   34164   56009    1141   44829   88517
   121284  129476  134936  156775

 putf:       4 buffers of length     766 written to file 12
 diagonal integral file completed.

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep4e:     7 blocks of linear combinations of 4-external integrals processed.
 number of sorted 4-external integrals        890
 number of original 4-external integrals      753


 putf34: external integral file complete. nfilw=    31 nfilx=    32 nrecw=     1 nrecx=     1 lbufp= 30006

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep3e:    26 blocks of linear combinations of 3-external integrals processed.
 number of sorted 3-external integrals       2976
 number of original 3-external integrals     1863


 putf34: external integral file complete. nfilw=    33 nfilx=    34 nrecw=     1 nrecx=     1 lbufp= 30006

 putf:       8 buffers of length     766 written to file 13
 off-diagonal files sort completed.
 executing brd_struct for cisrtinfo
cisrtinfo:
bufszi   766
 diagfile 4ext:     132 2ext:     198 0ext:      90
 fil4w,fil4x  :     753 fil3w,fil3x :    1863
 ofdgint  2ext:    2310 1ext:    1365 0ext:     360so0ext:       0so1ext:       0so2ext:       0
buffer minbl4      63 minbl3      63 maxbl2      66nbas:   0   0   0   0   3   2   4   2 maxbuf 30006
 CIUDG version 5.9.7 ( 5-Oct-2004)

 workspace allocation information: lcore= 498073599

 core energy values from the integral file:
 energy( 1)=  4.557548380050E+02, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -8.253716289658E+02, ietype=    6,   fcore energy of type: H1(*)   
 energy( 3)= -4.867636718614E+00, ietype=    5,   fcore energy of type: Vref(*) 

 total core repulsion energy = -3.744844276794E+02
 nmot  =    20 niot  =     9 nfct  =     0 nfvt  =     0
 nrow  =    97 nsym  =     8 ssym  =     1 lenbuf=  1600
 nwalk,xbar:      20644     2672     8820     3864     5288
 nvalwt,nvalw:    17568     1368     8660     3704     3836
 ncsft:          142028
 total number of valid internal walks:   17568
 nvalz,nvaly,nvalx,nvalw =     1368    8660    3704    3836

 cisrt info file parameters:
 file number  12 blocksize    766
 mxbld    766
 nd4ext,nd2ext,nd0ext   132   198    90
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int      753     1863     2310     1365      360        0        0        0
 minbl4,minbl3,maxbl2    63    63    66
 maxbuf 30006
 number of external orbitals per symmetry block:   0   0   0   0   3   2   4   2
 nmsym   8 number of internal orbitals   9
 executing brd_struct for drt
 executing brd_struct for orbinf
 executing brd_struct for momap
 calcthrxt: niot,maxw1=                     9                 11894
 block size     0
 pthz,pthy,pthx,pthw:  2672  8820  3864  5288 total internal walks:   20644
 maxlp3,n2lp,n1lp,n0lp 11894     0     0     0
 orbsym(*)= 5 5 5 6 6 7 7 8 8

 setref:      468 references kept,
                0 references were marked as invalid, out of
              468 total.
 nmb.of records onel     1
 nmb.of records 2-ext     4
 nmb.of records 1-ext     2
 nmb.of records 0-ext     1
 nmb.of records 2-int     0
 nmb.of records 1-int     0
 nmb.of records 0-int     0
 ---------memory usage in DP -----------------
 < n-ex core usage >
     routines:
    fourex            60816
    threx            131377
    twoex              1088
    onex                815
    allin               766
    diagon             1308
               =======
   maximum           131377
 
  __ static summary __ 
   reflst              1368
   hrfspc              1368
               -------
   static->            2736
 
  __ core required  __ 
   totstc              2736
   max n-ex          131377
               -------
   totnec->          134113
 
  __ core available __ 
   totspc         498073599
   totnec -          134113
               -------
   totvec->       497939486

 number of external paths / symmetry
 vertex x      11      14      16      14       0       0       0       0
 vertex w      22      14      16      14       0       0       0       0
segment: free space=   497939486
 reducing frespc by                 55315 to              497884171 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1        2672|      1368|         0|      1368|         0|         1|
 -------------------------------------------------------------------------------
  Y 2        8820|     23810|      1368|      8660|      1368|         2|
 -------------------------------------------------------------------------------
  X 3        3864|     51154|     25178|      3704|     10028|         3|
 -------------------------------------------------------------------------------
  W 4        5288|     65696|     76332|      3836|     13732|         4|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=        2292DP  conft+indsym=       34640DP  drtbuffer=       18383 DP

dimension of the ci-matrix ->>>    142028

 executing brd_struct for civct
 gentasklist: ntask=                    20
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS   
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1    3864    2672      51154       1368    3704    1368
     2  4   1    25      two-ext wz   2X  4 1    5288    2672      65696       1368    3836    1368
     3  4   3    26      two-ext wx*  WX  4 3    5288    3864      65696      51154    3836    3704
     4  4   3    27      two-ext wx+  WX  4 3    5288    3864      65696      51154    3836    3704
     5  2   1    11      one-ext yz   1X  2 1    8820    2672      23810       1368    8660    1368
     6  3   2    15      1ex3ex yx    3X  3 2    3864    8820      51154      23810    3704    8660
     7  4   2    16      1ex3ex yw    3X  4 2    5288    8820      65696      23810    3836    8660
     8  1   1     1      allint zz    OX  1 1    2672    2672       1368       1368    1368    1368
     9  2   2     5      0ex2ex yy    OX  2 2    8820    8820      23810      23810    8660    8660
    10  3   3     6      0ex2ex xx*   OX  3 3    3864    3864      51154      51154    3704    3704
    11  3   3    18      0ex2ex xx+   OX  3 3    3864    3864      51154      51154    3704    3704
    12  4   4     7      0ex2ex ww*   OX  4 4    5288    5288      65696      65696    3836    3836
    13  4   4    19      0ex2ex ww+   OX  4 4    5288    5288      65696      65696    3836    3836
    14  2   2    42      four-ext y   4X  2 2    8820    8820      23810      23810    8660    8660
    15  3   3    43      four-ext x   4X  3 3    3864    3864      51154      51154    3704    3704
    16  4   4    44      four-ext w   4X  4 4    5288    5288      65696      65696    3836    3836
    17  1   1    75      dg-024ext z  OX  1 1    2672    2672       1368       1368    1368    1368
    18  2   2    76      dg-024ext y  OX  2 2    8820    8820      23810      23810    8660    8660
    19  3   3    77      dg-024ext x  OX  3 3    3864    3864      51154      51154    3704    3704
    20  4   4    78      dg-024ext w  OX  4 4    5288    5288      65696      65696    3836    3836
----------------------------------------------------------------------------------------------------
REDTASK #   1 TIME=  19.000 N=  1 (task/type/sgbra)=(   1/24/0) (
REDTASK #   2 TIME=  18.000 N=  1 (task/type/sgbra)=(   2/25/0) (
REDTASK #   3 TIME=  17.000 N=  1 (task/type/sgbra)=(   3/26/1) (
REDTASK #   4 TIME=  16.000 N=  1 (task/type/sgbra)=(   4/27/2) (
REDTASK #   5 TIME=  15.000 N=  1 (task/type/sgbra)=(   5/11/0) (
REDTASK #   6 TIME=  14.000 N=  1 (task/type/sgbra)=(   6/15/0) (
REDTASK #   7 TIME=  13.000 N=  1 (task/type/sgbra)=(   7/16/0) (
REDTASK #   8 TIME=  12.000 N=  1 (task/type/sgbra)=(   8/ 1/0) (
REDTASK #   9 TIME=  11.000 N=  1 (task/type/sgbra)=(   9/ 5/0) (
REDTASK #  10 TIME=  10.000 N=  1 (task/type/sgbra)=(  10/ 6/1) (
REDTASK #  11 TIME=   9.000 N=  1 (task/type/sgbra)=(  11/18/2) (
REDTASK #  12 TIME=   8.000 N=  1 (task/type/sgbra)=(  12/ 7/1) (
REDTASK #  13 TIME=   7.000 N=  1 (task/type/sgbra)=(  13/19/2) (
REDTASK #  14 TIME=   6.000 N=  1 (task/type/sgbra)=(  14/42/1) (
REDTASK #  15 TIME=   5.000 N=  1 (task/type/sgbra)=(  15/43/1) (
REDTASK #  16 TIME=   4.000 N=  1 (task/type/sgbra)=(  16/44/1) (
REDTASK #  17 TIME=   3.000 N=  1 (task/type/sgbra)=(  17/75/1) (
REDTASK #  18 TIME=   2.000 N=  1 (task/type/sgbra)=(  18/76/1) (
REDTASK #  19 TIME=   1.000 N=  1 (task/type/sgbra)=(  19/77/1) (
REDTASK #  20 TIME=   0.000 N=  1 (task/type/sgbra)=(  20/78/1) (
 initializing v-file: 1:                142028

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       37290 2x:           0 4x:           0
All internal counts: zz :      141469 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:           0    task #     2:           0    task #     3:           0    task #     4:           0
task #     5:           0    task #     6:           0    task #     7:           0    task #     8:      112690
task #     9:           0    task #    10:           0    task #    11:           0    task #    12:           0
task #    13:           0    task #    14:           0    task #    15:           0    task #    16:           0
task #    17:       20226    task #    18:           0    task #    19:           0    task #    20:           0
 reference space has dimension     468
 dsyevx: computed roots 1 to    2(converged:   2)

    root           eigenvalues
    ----           ------------
       1        -383.3118404951
       2        -383.0632185342

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=                  1368

         vector  1 from unit 11 written to unit 49 filename cirefv              
 ncorel,neli=                    10                    10
  This is a  mraqcc energy evaluation      

    ------------------------------------------------------------

 setopt2: mraqcc energy evaluation      gvalue=  0.377777777777778      ncorel=
                    10

    ------------------------------------------------------------


    reference energies:

  reference state used is:   1with energy     -383.3118404951

  ### active excitation selection ###

 Inactive orbitals:                     1
    there are      468 all-active excitations of which    468 are references.

    the    468 reference all-active excitation csfs

    ------------------------------------------------------------------------------------------
         1     2     3     4     5     6     7     8     9    10    11    12    13    14    15
    ------------------------------------------------------------------------------------------
  2:     2     2     2     2     2     2     2     2     2     2     2     2     2     2     2
  3:     2     2     2     2     2     2     2     2     2     2     2     2     2     2     2
  4:     2     2     2     2     2     2     2     1     1     1     1     1     1     1     1
  5:     2     0     0     0     0     0     0    -1    -1    -1    -1    -1    -1     1     1
  6:     0     2     1     0     0     0     0     2     1     0     0     0     0    -1     0
  7:     0     0    -1     2     0     0     0     0    -1     2     0     0     0    -1     0
  8:     0     0     0     0     2     1     0     0     0     0     2     1     0     0    -1
  9:     0     0     0     0     0    -1     2     0     0     0     0    -1     2     0    -1
    ------------------------------------------------------------------------------------------
        16    17    18    19    20    21    22    23    24    25    26    27    28    29    30
    ------------------------------------------------------------------------------------------
  2:     2     2     2     2     2     2     2     2     2     2     2     2     2     2     2
  3:     2     2     2     2     2     2     2     2     2     2     2     2     2     2     2
  4:     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0
  5:     2     2     2     2     2     2     0     0     0     0     0     0     0     0     0
  6:     2     1     0     0     0     0     2     2     2     2     1     1     1     1     0
  7:     0    -1     2     0     0     0     2     0     0     0    -1    -1    -1     1     2
  8:     0     0     0     2     1     0     0     2     1     0     2     1     0    -1     2
  9:     0     0     0     0    -1     2     0     0    -1     2     0    -1     2    -1     0
    ------------------------------------------------------------------------------------------
        31    32    33    34    35    36    37    38    39    40    41    42    43    44    45
    ------------------------------------------------------------------------------------------
  2:     2     2     2     2     2     2     2     2     2     2     2     2     2     2     2
  3:     2     2     2     1     1     1     1     1     1     1     1     1     1     1     1
  4:     0     0     0     2     2     2     2     2     2     2     2    -1    -1    -1    -1
  5:     0     0     0    -1    -1    -1    -1     1     1     1     1     2     2     2     2
  6:     0     0     0     1     1     0     0    -1    -1     0     0     1     1     0     0
  7:     2     2     0     0     0     1     1     0     0    -1    -1     0     0     1     1
  8:     1     0     2    -1     0    -1     0    -1     0    -1     0    -1     0    -1     0
  9:    -1     2     2     0    -1     0    -1     0    -1     0    -1     0    -1     0    -1
    ------------------------------------------------------------------------------------------
        46    47    48    49    50    51    52    53    54    55    56    57    58    59    60
    ------------------------------------------------------------------------------------------
  2:     2     2     2     2     2     2     2     2     2     2     2     2     2     2     2
  3:     1     1     1     1     1     1     1     1     1     1     1     1     1     1     1
  4:    -1    -1    -1    -1    -1    -1    -1    -1     1     1     1     1     1     1     1
  5:     0     0     0     0     0     0     0     0     2     2     2     2     0     0     0
  6:     2     2     1     1     1     1     0     0    -1    -1     0     0     2     2    -1
  7:     1     1     2     2     0     0     1     1     0     0    -1    -1    -1    -1     2
  8:    -1     0    -1     0     2    -1     2    -1    -1     0    -1     0    -1     0    -1
  9:     0    -1     0    -1    -1     2    -1     2     0    -1     0    -1     0    -1     0
    ------------------------------------------------------------------------------------------
        61    62    63    64    65    66    67    68    69    70    71    72    73    74    75
    ------------------------------------------------------------------------------------------
  2:     2     2     2     2     2     2     2     2     2     2     2     2     2     2     2
  3:     1     1     1     1     1     1     1     1     1     1     1     1     1     1     1
  4:     1     1     1     1     1     0     0     0     0     0     0     0     0     0     0
  5:     0     0     0     0     0    -1    -1    -1    -1    -1    -1    -1    -1     1     1
  6:    -1    -1    -1     0     0     2     2     1     1     1     1     0     0     2     2
  7:     2     0     0    -1    -1     1     1     2     2     0     0     1     1    -1    -1
  8:     0     2    -1     2    -1    -1     0    -1     0     2    -1     2    -1    -1     0
  9:    -1    -1     2    -1     2     0    -1     0    -1    -1     2    -1     2     0    -1
    ------------------------------------------------------------------------------------------
        76    77    78    79    80    81    82    83    84    85    86    87    88    89    90
    ------------------------------------------------------------------------------------------
  2:     2     2     2     2     2     2     2     2     2     2     2     2     2     2     2
  3:     1     1     1     1     1     1     0     0     0     0     0     0     0     0     0
  4:     0     0     0     0     0     0     2     2     2     2     2     2     2     2     2
  5:     1     1     1     1     1     1     2     2     2     2     2     2     0     0     0
  6:    -1    -1    -1    -1     0     0     2     1     0     0     0     0     2     2     2
  7:     2     2     0     0    -1    -1     0    -1     2     0     0     0     2     0     0
  8:    -1     0     2    -1     2    -1     0     0     0     2     1     0     0     2     1
  9:     0    -1    -1     2    -1     2     0     0     0     0    -1     2     0     0    -1
    ------------------------------------------------------------------------------------------
        91    92    93    94    95    96    97    98    99   100   101   102   103   104   105
    ------------------------------------------------------------------------------------------
  2:     2     2     2     2     2     2     2     2     2     2     2     2     2     2     2
  3:     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0
  4:     2     2     2     2     2     2     2     2     2     1     1     1     1     1     1
  5:     0     0     0     0     0     0     0     0     0    -1    -1    -1    -1    -1    -1
  6:     2     1     1     1     1     0     0     0     0     2     2     2     2     1     1
  7:     0    -1    -1    -1     1     2     2     2     0     2     0     0     0    -1    -1
  8:     0     2     1     0    -1     2     1     0     2     0     2     1     0     2     1
  9:     2     0    -1     2    -1     0    -1     2     2     0     0    -1     2     0    -1
    ------------------------------------------------------------------------------------------
       106   107   108   109   110   111   112   113   114   115   116   117   118   119   120
    ------------------------------------------------------------------------------------------
  2:     2     2     2     2     2     2     2     2     2     2     2     2     2     2     2
  3:     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0
  4:     1     1     1     1     1     1     1     1     1     1     1     1     1     0     0
  5:    -1    -1    -1    -1    -1    -1     1     1     1     1     1     1     1     2     2
  6:     1     1     0     0     0     0     2    -1    -1    -1    -1     1     0     2     2
  7:    -1     1     2     2     2     0     0    -1    -1    -1     1    -1     2     2     0
  8:     0    -1     2     1     0     2    -1     2     1     0    -1    -1    -1     0     2
  9:     2    -1     0    -1     2     2    -1     0    -1     2    -1    -1    -1     0     0
    ------------------------------------------------------------------------------------------
       121   122   123   124   125   126   127   128   129   130   131   132   133   134   135
    ------------------------------------------------------------------------------------------
  2:     2     2     2     2     2     2     2     2     2     2     2     2     2     2     2
  3:     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0
  4:     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0
  5:     2     2     2     2     2     2     2     2     2     2     0     0     0     0     0
  6:     2     2     1     1     1     1     0     0     0     0     2     2     2     2     1
  7:     0     0    -1    -1    -1     1     2     2     2     0     2     2     2     0    -1
  8:     1     0     2     1     0    -1     2     1     0     2     2     1     0     2     2
  9:    -1     2     0    -1     2    -1     0    -1     2     2     0    -1     2     2     2
    ------------------------------------------------------------------------------------------
       136   137   138   139   140   141   142   143   144   145   146   147   148   149   150
    ------------------------------------------------------------------------------------------
  2:     2     1     1     1     1     1     1     1     1     1     1     1     1     1     1
  3:     0     2     2     2     2     2     2     2     2     2     2     2     2     2     2
  4:     0     2     2     2     2     2     2     2     2    -1    -1    -1    -1    -1    -1
  5:     0    -1    -1    -1    -1     1     1     1     1     2     2     2     2     0     0
  6:     0     1     1     0     0    -1    -1     0     0     1     1     0     0     2     2
  7:     2     0     0     1     1     0     0    -1    -1     0     0     1     1     1     1
  8:     2    -1     0    -1     0    -1     0    -1     0    -1     0    -1     0    -1     0
  9:     2     0    -1     0    -1     0    -1     0    -1     0    -1     0    -1     0    -1
    ------------------------------------------------------------------------------------------
       151   152   153   154   155   156   157   158   159   160   161   162   163   164   165
    ------------------------------------------------------------------------------------------
  2:     1     1     1     1     1     1     1     1     1     1     1     1     1     1     1
  3:     2     2     2     2     2     2     2     2     2     2     2     2     2     2     2
  4:    -1    -1    -1    -1    -1    -1     1     1     1     1     1     1     1     1     1
  5:     0     0     0     0     0     0     2     2     2     2     0     0     0     0     0
  6:     1     1     1     1     0     0    -1    -1     0     0     2     2    -1    -1    -1
  7:     2     2     0     0     1     1     0     0    -1    -1    -1    -1     2     2     0
  8:    -1     0     2    -1     2    -1    -1     0    -1     0    -1     0    -1     0     2
  9:     0    -1    -1     2    -1     2     0    -1     0    -1     0    -1     0    -1    -1
    ------------------------------------------------------------------------------------------
       166   167   168   169   170   171   172   173   174   175   176   177   178   179   180
    ------------------------------------------------------------------------------------------
  2:     1     1     1     1     1     1     1     1     1     1     1     1     1     1     1
  3:     2     2     2     2     2     2     2     2     2     2     2     2     2     2     2
  4:     1     1     1     0     0     0     0     0     0     0     0     0     0     0     0
  5:     0     0     0    -1    -1    -1    -1    -1    -1    -1    -1     1     1     1     1
  6:    -1     0     0     2     2     1     1     1     1     0     0     2     2    -1    -1
  7:     0    -1    -1     1     1     2     2     0     0     1     1    -1    -1     2     2
  8:    -1     2    -1    -1     0    -1     0     2    -1     2    -1    -1     0    -1     0
  9:     2    -1     2     0    -1     0    -1    -1     2    -1     2     0    -1     0    -1
    ------------------------------------------------------------------------------------------
       181   182   183   184   185   186   187   188   189   190   191   192   193   194   195
    ------------------------------------------------------------------------------------------
  2:     1     1     1     1     1     1     1     1     1     1     1     1     1     1     1
  3:     2     2     2     2    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1
  4:     0     0     0     0     2     2     2     2     2     2     2     2     2     2     2
  5:     1     1     1     1     2     2     2     2     2     2     0     0     0     0     0
  6:    -1    -1     0     0     2     1     0     0     0     0     2     2     2     2     1
  7:     0     0    -1    -1     0    -1     2     0     0     0     2     0     0     0    -1
  8:     2    -1     2    -1     0     0     0     2     1     0     0     2     1     0     2
  9:    -1     2    -1     2     0     0     0     0    -1     2     0     0    -1     2     0
    ------------------------------------------------------------------------------------------
       196   197   198   199   200   201   202   203   204   205   206   207   208   209   210
    ------------------------------------------------------------------------------------------
  2:     1     1     1     1     1     1     1     1     1     1     1     1     1     1     1
  3:    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1
  4:     2     2     2     2     2     2     2     1     1     1     1     1     1     1     1
  5:     0     0     0     0     0     0     0    -1    -1    -1    -1    -1    -1    -1    -1
  6:     1     1     1     0     0     0     0     2     2     2     2     1     1     1     1
  7:    -1    -1     1     2     2     2     0     2     0     0     0    -1    -1    -1     1
  8:     1     0    -1     2     1     0     2     0     2     1     0     2     1     0    -1
  9:    -1     2    -1     0    -1     2     2     0     0    -1     2     0    -1     2    -1
    ------------------------------------------------------------------------------------------
       211   212   213   214   215   216   217   218   219   220   221   222   223   224   225
    ------------------------------------------------------------------------------------------
  2:     1     1     1     1     1     1     1     1     1     1     1     1     1     1     1
  3:    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1
  4:     1     1     1     1     1     1     1     1     1     1     1     0     0     0     0
  5:    -1    -1    -1    -1     1     1     1     1     1     1     1     2     2     2     2
  6:     0     0     0     0     2    -1    -1    -1    -1     1     0     2     2     2     2
  7:     2     2     2     0     0    -1    -1    -1     1    -1     2     2     0     0     0
  8:     2     1     0     2    -1     2     1     0    -1    -1    -1     0     2     1     0
  9:     0    -1     2     2    -1     0    -1     2    -1    -1    -1     0     0    -1     2
    ------------------------------------------------------------------------------------------
       226   227   228   229   230   231   232   233   234   235   236   237   238   239   240
    ------------------------------------------------------------------------------------------
  2:     1     1     1     1     1     1     1     1     1     1     1     1     1     1     1
  3:    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1     1
  4:     0     0     0     0     0     0     0     0     0     0     0     0     0     0     2
  5:     2     2     2     2     2     2     2     2     0     0     0     0     0     0     2
  6:     1     1     1     1     0     0     0     0     2     2     2     2     1     0    -1
  7:    -1    -1    -1     1     2     2     2     0     2     2     2     0    -1     2    -1
  8:     2     1     0    -1     2     1     0     2     2     1     0     2     2     2     0
  9:     0    -1     2    -1     0    -1     2     2     0    -1     2     2     2     2     0
    ------------------------------------------------------------------------------------------
       241   242   243   244   245   246   247   248   249   250   251   252   253   254   255
    ------------------------------------------------------------------------------------------
  2:     1     1     1     1     1     1     1     1     1     1     1     1     1     1     1
  3:     1     1     1     1     1     1     1     1     1     1     1     1     1     1     1
  4:     2     2     2     2     2     2     2     2    -1    -1    -1    -1    -1    -1    -1
  5:     2     0     0     0     0     0     0     0    -1    -1    -1    -1    -1    -1    -1
  6:     0     2    -1    -1    -1    -1     1     0     2     2     2     2     1     1     1
  7:     0     0    -1    -1    -1     1    -1     2     2     0     0     0    -1    -1    -1
  8:    -1    -1     2     1     0    -1    -1    -1     0     2     1     0     2     1     0
  9:    -1    -1     0    -1     2    -1    -1    -1     0     0    -1     2     0    -1     2
    ------------------------------------------------------------------------------------------
       256   257   258   259   260   261   262   263   264   265   266   267   268   269   270
    ------------------------------------------------------------------------------------------
  2:     1     1     1     1     1     1     1     1     1     1     1     1     1     1     1
  3:     1     1     1     1     1     1     1     1     1     1     1     1     1     1     1
  4:    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1     1     1     1
  5:    -1    -1    -1    -1    -1     1     1     1     1     1     1     1    -1    -1    -1
  6:     1     0     0     0     0     2    -1    -1    -1    -1     1     0     2    -1    -1
  7:     1     2     2     2     0     0    -1    -1    -1     1    -1     2     0    -1    -1
  8:    -1     2     1     0     2    -1     2     1     0    -1    -1    -1    -1     2     1
  9:    -1     0    -1     2     2    -1     0    -1     2    -1    -1    -1    -1     0    -1
    ------------------------------------------------------------------------------------------
       271   272   273   274   275   276   277   278   279   280   281   282   283   284   285
    ------------------------------------------------------------------------------------------
  2:     1     1     1     1     1     1     1     1     1     1     1     1     1     1     1
  3:     1     1     1     1     1     1     1     1     1     1     1     1     1     1     0
  4:     1     1     1     1     1     0     0     0     0     0     0     0     0     0     2
  5:    -1    -1    -1    -1     1     2     2     2     2     2     2     2     0     0    -1
  6:    -1    -1     1     0    -1     2    -1    -1    -1    -1     1     0     2    -1     2
  7:    -1     1    -1     2    -1     0    -1    -1    -1     1    -1     2     2    -1     1
  8:     0    -1    -1    -1    -1    -1     2     1     0    -1    -1    -1    -1     2    -1
  9:     2    -1    -1    -1    -1    -1     0    -1     2    -1    -1    -1    -1     2     0
    ------------------------------------------------------------------------------------------
       286   287   288   289   290   291   292   293   294   295   296   297   298   299   300
    ------------------------------------------------------------------------------------------
  2:     1     1     1     1     1     1     1     1     1     1     1     1     1     1     1
  3:     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0
  4:     2     2     2     2     2     2     2     2     2     2     2     2     2     2     2
  5:    -1    -1    -1    -1    -1    -1    -1     1     1     1     1     1     1     1     1
  6:     2     1     1     1     1     0     0     2     2    -1    -1    -1    -1     0     0
  7:     1     2     2     0     0     1     1    -1    -1     2     2     0     0    -1    -1
  8:     0    -1     0     2    -1     2    -1    -1     0    -1     0     2    -1     2    -1
  9:    -1     0    -1    -1     2    -1     2     0    -1     0    -1    -1     2    -1     2
    ------------------------------------------------------------------------------------------
       301   302   303   304   305   306   307   308   309   310   311   312   313   314   315
    ------------------------------------------------------------------------------------------
  2:     1     1     1     1     1     1     1     1     1     1     1     1     1     1     1
  3:     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0
  4:    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1     1     1     1
  5:     2     2     2     2     2     2     2     2     0     0     0     0     2     2     2
  6:     2     2     1     1     1     1     0     0     2     2     1     1     2     2    -1
  7:     1     1     2     2     0     0     1     1     1     1     2     2    -1    -1     2
  8:    -1     0    -1     0     2    -1     2    -1     2    -1     2    -1    -1     0    -1
  9:     0    -1     0    -1    -1     2    -1     2    -1     2    -1     2     0    -1     0
    ------------------------------------------------------------------------------------------
       316   317   318   319   320   321   322   323   324   325   326   327   328   329   330
    ------------------------------------------------------------------------------------------
  2:     1     1     1     1     1     1     1     1     1     1     1     1     1     1     1
  3:     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0
  4:     1     1     1     1     1     1     1     1     1     0     0     0     0     0     0
  5:     2     2     2     2     2     0     0     0     0    -1    -1    -1    -1     1     1
  6:    -1    -1    -1     0     0     2     2    -1    -1     2     2     1     1     2     2
  7:     2     0     0    -1    -1    -1    -1     2     2     1     1     2     2    -1    -1
  8:     0     2    -1     2    -1     2    -1     2    -1     2    -1     2    -1     2    -1
  9:    -1    -1     2    -1     2    -1     2    -1     2    -1     2    -1     2    -1     2
    ------------------------------------------------------------------------------------------
       331   332   333   334   335   336   337   338   339   340   341   342   343   344   345
    ------------------------------------------------------------------------------------------
  2:     1     1     0     0     0     0     0     0     0     0     0     0     0     0     0
  3:     0     0     2     2     2     2     2     2     2     2     2     2     2     2     2
  4:     0     0     2     2     2     2     2     2     2     2     2     2     2     2     2
  5:     1     1     2     2     2     2     2     2     0     0     0     0     0     0     0
  6:    -1    -1     2     1     0     0     0     0     2     2     2     2     1     1     1
  7:     2     2     0    -1     2     0     0     0     2     0     0     0    -1    -1    -1
  8:     2    -1     0     0     0     2     1     0     0     2     1     0     2     1     0
  9:    -1     2     0     0     0     0    -1     2     0     0    -1     2     0    -1     2
    ------------------------------------------------------------------------------------------
       346   347   348   349   350   351   352   353   354   355   356   357   358   359   360
    ------------------------------------------------------------------------------------------
  2:     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0
  3:     2     2     2     2     2     2     2     2     2     2     2     2     2     2     2
  4:     2     2     2     2     2     1     1     1     1     1     1     1     1     1     1
  5:     0     0     0     0     0    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1
  6:     1     0     0     0     0     2     2     2     2     1     1     1     1     0     0
  7:     1     2     2     2     0     2     0     0     0    -1    -1    -1     1     2     2
  8:    -1     2     1     0     2     0     2     1     0     2     1     0    -1     2     1
  9:    -1     0    -1     2     2     0     0    -1     2     0    -1     2    -1     0    -1
    ------------------------------------------------------------------------------------------
       361   362   363   364   365   366   367   368   369   370   371   372   373   374   375
    ------------------------------------------------------------------------------------------
  2:     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0
  3:     2     2     2     2     2     2     2     2     2     2     2     2     2     2     2
  4:     1     1     1     1     1     1     1     1     1     0     0     0     0     0     0
  5:    -1    -1     1     1     1     1     1     1     1     2     2     2     2     2     2
  6:     0     0     2    -1    -1    -1    -1     1     0     2     2     2     2     1     1
  7:     2     0     0    -1    -1    -1     1    -1     2     2     0     0     0    -1    -1
  8:     0     2    -1     2     1     0    -1    -1    -1     0     2     1     0     2     1
  9:     2     2    -1     0    -1     2    -1    -1    -1     0     0    -1     2     0    -1
    ------------------------------------------------------------------------------------------
       376   377   378   379   380   381   382   383   384   385   386   387   388   389   390
    ------------------------------------------------------------------------------------------
  2:     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0
  3:     2     2     2     2     2     2     2     2     2     2     2     2     1     1     1
  4:     0     0     0     0     0     0     0     0     0     0     0     0     2     2     2
  5:     2     2     2     2     2     2     0     0     0     0     0     0    -1    -1    -1
  6:     1     1     0     0     0     0     2     2     2     2     1     0     2     2     1
  7:    -1     1     2     2     2     0     2     2     2     0    -1     2     1     1     2
  8:     0    -1     2     1     0     2     2     1     0     2     2     2    -1     0    -1
  9:     2    -1     0    -1     2     2     0    -1     2     2     2     2     0    -1     0
    ------------------------------------------------------------------------------------------
       391   392   393   394   395   396   397   398   399   400   401   402   403   404   405
    ------------------------------------------------------------------------------------------
  2:     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0
  3:     1     1     1     1     1     1     1     1     1     1     1     1     1     1     1
  4:     2     2     2     2     2     2     2     2     2     2     2     2     2    -1    -1
  5:    -1    -1    -1    -1    -1     1     1     1     1     1     1     1     1     2     2
  6:     1     1     1     0     0     2     2    -1    -1    -1    -1     0     0     2     2
  7:     2     0     0     1     1    -1    -1     2     2     0     0    -1    -1     1     1
  8:     0     2    -1     2    -1    -1     0    -1     0     2    -1     2    -1    -1     0
  9:    -1    -1     2    -1     2     0    -1     0    -1    -1     2    -1     2     0    -1
    ------------------------------------------------------------------------------------------
       406   407   408   409   410   411   412   413   414   415   416   417   418   419   420
    ------------------------------------------------------------------------------------------
  2:     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0
  3:     1     1     1     1     1     1     1     1     1     1     1     1     1     1     1
  4:    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1     1     1     1     1     1
  5:     2     2     2     2     2     2     0     0     0     0     2     2     2     2     2
  6:     1     1     1     1     0     0     2     2     1     1     2     2    -1    -1    -1
  7:     2     2     0     0     1     1     1     1     2     2    -1    -1     2     2     0
  8:    -1     0     2    -1     2    -1     2    -1     2    -1    -1     0    -1     0     2
  9:     0    -1    -1     2    -1     2    -1     2    -1     2     0    -1     0    -1    -1
    ------------------------------------------------------------------------------------------
       421   422   423   424   425   426   427   428   429   430   431   432   433   434   435
    ------------------------------------------------------------------------------------------
  2:     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0
  3:     1     1     1     1     1     1     1     1     1     1     1     1     1     1     1
  4:     1     1     1     1     1     1     1     0     0     0     0     0     0     0     0
  5:     2     2     2     0     0     0     0    -1    -1    -1    -1     1     1     1     1
  6:    -1     0     0     2     2    -1    -1     2     2     1     1     2     2    -1    -1
  7:     0    -1    -1    -1    -1     2     2     1     1     2     2    -1    -1     2     2
  8:    -1     2    -1     2    -1     2    -1     2    -1     2    -1     2    -1     2    -1
  9:     2    -1     2    -1     2    -1     2    -1     2    -1     2    -1     2    -1     2
    ------------------------------------------------------------------------------------------
       436   437   438   439   440   441   442   443   444   445   446   447   448   449   450
    ------------------------------------------------------------------------------------------
  2:     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0
  3:     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0
  4:     2     2     2     2     2     2     2     2     2     2     2     2     2     2     2
  5:     2     2     2     2     2     2     2     2     2     2     2     2     0     0     0
  6:     2     2     2     2     1     1     1     1     0     0     0     0     2     2     2
  7:     2     0     0     0    -1    -1    -1     1     2     2     2     0     2     2     2
  8:     0     2     1     0     2     1     0    -1     2     1     0     2     2     1     0
  9:     0     0    -1     2     0    -1     2    -1     0    -1     2     2     0    -1     2
    ------------------------------------------------------------------------------------------
       451   452   453   454   455   456   457   458   459   460   461   462   463   464   465
    ------------------------------------------------------------------------------------------
  2:     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0
  3:     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0
  4:     2     2     2     1     1     1     1     1     1     1     1     0     0     0     0
  5:     0     0     0    -1    -1    -1    -1    -1    -1     1     1     2     2     2     2
  6:     2     1     0     2     2     2     2     1     0     2    -1     2     2     2     2
  7:     0    -1     2     2     2     2     0    -1     2     2    -1     2     2     2     0
  8:     2     2     2     2     1     0     2     2     2    -1     2     2     1     0     2
  9:     2     2     2     0    -1     2     2     2     2    -1     2     0    -1     2     2
    ------------------
       466   467   468
    ------------------
  2:     0     0     0
  3:     0     0     0
  4:     0     0     0
  5:     2     2     0
  6:     1     0     2
  7:    -1     2     2
  8:     2     2     2
  9:     2     2     2

  ### end active excitation selection ###


 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:            142028
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                   20
 residual norm convergence criteria:               0.001000

          starting ci iteration   1

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    4.158917
ci vector #   2dasum_wr=    0.000000
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      412118 2x:      100824 4x:       16200
All internal counts: zz :      141469 yy:     1199860 xx:      733974 ww:      718502
One-external counts: yz :      664107 yx:     1998598 yw:     2016328
Two-external counts: yy :      580878 ww:      224232 xx:      351894 xz:       37085 wz:       33077 wx:      512216
Three-ext.   counts: yx :      139509 yw:      144149

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:       52777    task #     2:       40747    task #     3:      332519    task #     4:      166259
task #     5:      510932    task #     6:     1507769    task #     7:     1513804    task #     8:      112690
task #     9:     1292631    task #    10:      225883    task #    11:      225883    task #    12:      126547
task #    13:      126547    task #    14:       63273    task #    15:       63275    task #    16:       63275
task #    17:       20226    task #    18:      147443    task #    19:       51613    task #    20:       54916
 Overlap matrix (sovlci)

              sovl   1
 sovl   1     1.00000000
 Overlap matrix (active-active) sovlaa

              sovl   1
 sovl   1     1.00000000
 Overlap matrix (critical) sovlcrit

              sovl   1
 Final Overlap matrix sovl

              sovl   1
 sovl   1     0.00000000
 Subspace hamiltonian (htci) 

              htci   1
 htci   1    -8.82741282
 Subspace hamiltonian (active-active) 

              htaa   1
 htaa   1    -8.82741282
 Subspace hamiltonian (critical) 

              htcr   1
 scale factor: (eold-eref)*(1-gvalue):  0.000000000000000E+000 *
  0.622222222222222      =  0.000000000000000E+000
 Final subspace hamiltonian 

                ht   1
   ht   1    -8.82741282
Spectrum of overlapmatrix:    1.000000
calca4: root=   1 anorm**2=  0.00000000 scale:  1.00000000

          calcsovref: tciref block   1

              civs   1
 refs   1    1.00000    

          calcsovref: scrb block   1

                ci   1
 civs   1    1.00000    

          calcsovref: sovref block   1

              v      1
 ref    1    1.00000    

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000
 NCSF=                  1368                 23810                 51154
                 65696
 fact= -7.626492030491743E-014
 factd= -7.626492030491743E-014

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  1  1   -383.3118404951  1.2257E-13  3.9848E-02  2.4511E-01  1.0000E-03
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.02     0.01     0.00     0.02         0.    0.0528
    2   25    0     0.01     0.01     0.00     0.01         0.    0.0407
    3   26    0     0.28     0.07     0.00     0.28         0.    0.3325
    4   27    0     0.29     0.05     0.00     0.29         0.    0.1663
    5   11    0     0.36     0.28     0.00     0.36         0.    0.5109
    6   15    0     1.14     0.69     0.00     1.14         0.    1.5078
    7   16    0     1.17     0.67     0.00     1.17         0.    1.5138
    8    1    0     0.08     0.07     0.00     0.08         0.    0.1127
    9    5    0     0.87     0.58     0.00     0.87         0.    1.2926
   10    6    0     0.38     0.20     0.00     0.38         0.    0.2259
   11   18    0     0.36     0.19     0.00     0.36         0.    0.2259
   12    7    0     0.30     0.19     0.00     0.30         0.    0.1265
   13   19    0     0.29     0.16     0.00     0.29         0.    0.1265
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0633
   15   43    0     0.01     0.00     0.00     0.01         0.    0.0633
   16   44    0     0.01     0.00     0.00     0.01         0.    0.0633
   17   75    0     0.01     0.01     0.00     0.01         0.    0.0202
   18   76    0     0.06     0.06     0.00     0.06         0.    0.1474
   19   77    0     0.02     0.02     0.00     0.02         0.    0.0516
   20   78    0     0.03     0.12     0.00     0.03         0.    0.0075
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.120000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     5.6900s 
time spent in multnx:                   5.6900s 
integral transfer time:                 0.0000s 
time spent for loop construction:       3.3800s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            5.8200s 

          starting ci iteration   2

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.545334
ci vector #   2dasum_wr=    3.045226
ci vector #   3dasum_wr=    0.423783
ci vector #   4dasum_wr=    2.653283


====================================================================================================
Diagonal     counts:  0x:      412118 2x:      100824 4x:       16200
All internal counts: zz :      141469 yy:     1199860 xx:      733974 ww:      718502
One-external counts: yz :      664107 yx:     1998598 yw:     2016328
Two-external counts: yy :      580878 ww:      224232 xx:      351894 xz:       37085 wz:       33077 wx:      512216
Three-ext.   counts: yx :      139509 yw:      144149

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:       52777    task #     2:       40747    task #     3:      332519    task #     4:      166259
task #     5:      510932    task #     6:     1507769    task #     7:     1513804    task #     8:      112690
task #     9:     1292631    task #    10:      225883    task #    11:      225883    task #    12:      126547
task #    13:      126547    task #    14:       63273    task #    15:       63275    task #    16:       63275
task #    17:       20226    task #    18:      147443    task #    19:       51613    task #    20:       54916
 Overlap matrix (sovlci)

              sovl   1       sovl   2
 sovl   1     1.00000000
 sovl   2     0.00000000     0.02954205
 Overlap matrix (active-active) sovlaa

              sovl   1       sovl   2
 sovl   1     1.00000000
 sovl   2     0.00000000     0.00000000
 Overlap matrix (critical) sovlcrit

              sovl   1       sovl   2
 Final Overlap matrix sovl

              sovl   1       sovl   2
 sovl   1     0.00000000
 sovl   2     0.00000000     0.02954205
 Subspace hamiltonian (htci) 

              htci   1       htci   2
 htci   1    -8.82741282
 htci   2    -0.03984773    -0.22745428
 Subspace hamiltonian (active-active) 

              htaa   1       htaa   2
 htaa   1    -8.82741282
 htaa   2     0.00000000     0.00000000
 Subspace hamiltonian (critical) 

              htcr   1       htcr   2
 scale factor: (eold-eref)*(1-gvalue): -1.225686219186173E-013 *
  0.622222222222222      = -7.626492030491743E-014
 Final subspace hamiltonian 

                ht   1         ht   2
   ht   1    -8.82741282
   ht   2    -0.03984773    -0.22745428
Spectrum of overlapmatrix:    0.029542    1.000000
calca4: root=   1 anorm**2=  0.03754172 scale:  1.01859792
calca4: root=   2 anorm**2=  0.96245828 scale:  1.40087768

          calcsovref: tciref block   1

              civs   1       civs   2
 refs   1    1.00000       2.065449E-13

          calcsovref: scrb block   1

                ci   1         ci   2
 civs   1  -0.981050       0.193757    
 civs   2   -1.12729       -5.70782    

          calcsovref: sovref block   1

              v      1       v      2
 ref    1  -0.981050       0.193757    

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1    -0.98104958     0.19375685
 NCSF=                  1368                 23810                 51154
                 65696
 fact= -2.849015596268790E-002
 factd= -2.849015596268790E-002

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  2  1   -383.3576282457  4.5788E-02  4.9645E-03  8.8187E-02  1.0000E-03
 mraqcc  #  2  2   -382.1379783782  7.6536E+00  0.0000E+00  5.7557E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.01     0.01     0.00     0.01         0.    0.0528
    2   25    0     0.02     0.02     0.00     0.02         0.    0.0407
    3   26    0     0.28     0.06     0.00     0.28         0.    0.3325
    4   27    0     0.28     0.07     0.00     0.28         0.    0.1663
    5   11    0     0.37     0.32     0.00     0.37         0.    0.5109
    6   15    0     1.17     0.68     0.00     1.17         0.    1.5078
    7   16    0     1.20     0.75     0.00     1.20         0.    1.5138
    8    1    0     0.08     0.07     0.00     0.08         0.    0.1127
    9    5    0     0.87     0.58     0.00     0.87         0.    1.2926
   10    6    0     0.38     0.19     0.00     0.38         0.    0.2259
   11   18    0     0.36     0.19     0.00     0.36         0.    0.2259
   12    7    0     0.30     0.17     0.00     0.30         0.    0.1265
   13   19    0     0.29     0.19     0.00     0.29         0.    0.1265
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0633
   15   43    0     0.01     0.00     0.00     0.01         0.    0.0633
   16   44    0     0.01     0.00     0.00     0.01         0.    0.0633
   17   75    0     0.01     0.01     0.00     0.01         0.    0.0202
   18   76    0     0.06     0.06     0.00     0.06         0.    0.1474
   19   77    0     0.02     0.02     0.00     0.02         0.    0.0516
   20   78    0     0.03     0.12     0.01     0.03         0.    0.0075
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.130000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     5.7500s 
time spent in multnx:                   5.7500s 
integral transfer time:                 0.0100s 
time spent for loop construction:       3.5100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            5.8800s 

          starting ci iteration   3

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.494557
ci vector #   2dasum_wr=    1.634813
ci vector #   3dasum_wr=    0.510987
ci vector #   4dasum_wr=    1.344406


====================================================================================================
Diagonal     counts:  0x:      412118 2x:      100824 4x:       16200
All internal counts: zz :      141469 yy:     1199860 xx:      733974 ww:      718502
One-external counts: yz :      664107 yx:     1998598 yw:     2016328
Two-external counts: yy :      580878 ww:      224232 xx:      351894 xz:       37085 wz:       33077 wx:      512216
Three-ext.   counts: yx :      139509 yw:      144149

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:       52777    task #     2:       40747    task #     3:      332519    task #     4:      166259
task #     5:      510932    task #     6:     1507769    task #     7:     1513804    task #     8:      112690
task #     9:     1292631    task #    10:      225883    task #    11:      225883    task #    12:      126547
task #    13:      126547    task #    14:       63273    task #    15:       63275    task #    16:       63275
task #    17:       20226    task #    18:      147443    task #    19:       51613    task #    20:       54916
 Overlap matrix (sovlci)

              sovl   1       sovl   2       sovl   3
 sovl   1     1.00000000
 sovl   2     0.00000000     0.02954205
 sovl   3    -0.01688510    -0.00040916     0.00403830
 Overlap matrix (active-active) sovlaa

              sovl   1       sovl   2       sovl   3
 sovl   1     1.00000000
 sovl   2     0.00000000     0.00000000
 sovl   3    -0.01688510     0.00000000     0.00094921
 Overlap matrix (critical) sovlcrit

              sovl   1       sovl   2       sovl   3
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3
 sovl   1     0.00000000
 sovl   2     0.00000000     0.02954205
 sovl   3     0.00000000    -0.00040916     0.00308909
 Subspace hamiltonian (htci) 

              htci   1       htci   2       htci   3
 htci   1    -8.82741282
 htci   2    -0.03984773    -0.22745428
 htci   3     0.14999347     0.00787611    -0.03070984
 Subspace hamiltonian (active-active) 

              htaa   1       htaa   2       htaa   3
 htaa   1    -8.82741282
 htaa   2     0.00000000     0.00000000
 htaa   3    -0.00770300    -0.00770300    -0.00770300
 Subspace hamiltonian (critical) 

              htcr   1       htcr   2       htcr   3
 scale factor: (eold-eref)*(1-gvalue): -4.578775065431984E-002 *
  0.622222222222222      = -2.849015596268790E-002
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3
   ht   1    -8.82741282
   ht   2    -0.03984773    -0.22829594
   ht   3     0.14999347     0.00788776    -0.03079785
Spectrum of overlapmatrix:    0.003746    0.029549    1.000286
calca4: root=   1 anorm**2=  0.05092037 scale:  1.02514407
calca4: root=   2 anorm**2=  0.88886103 scale:  1.37435841
calca4: root=   3 anorm**2=  0.88300814 scale:  1.37222744

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3
 refs   1    1.00000       2.065449E-13  -1.688510E-02

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3
 civs   1  -0.955856      -0.380258       0.133494    
 civs   2   -1.25235        4.29294        3.72874    
 civs   3    1.06395       -10.0058        12.8683    

          calcsovref: sovref block   1

              v      1       v      2       v      3
 ref    1  -0.973821      -0.211309      -8.378905E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1    -0.97382128    -0.21130906    -0.08378905
 NCSF=                  1368                 23810                 51154
                 65696
 fact= -3.252585961391251E-002
 factd= -3.252585961391251E-002

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  3  1   -383.3641141980  6.4860E-03  1.2270E-03  4.4023E-02  1.0000E-03
 mraqcc  #  3  2   -382.4577045755  3.1973E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  3  3   -381.6831853662  7.1988E+00  0.0000E+00  3.4593E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.01     0.01     0.00     0.01         0.    0.0528
    2   25    0     0.02     0.01     0.00     0.02         0.    0.0407
    3   26    0     0.28     0.08     0.00     0.28         0.    0.3325
    4   27    0     0.28     0.06     0.00     0.28         0.    0.1663
    5   11    0     0.37     0.31     0.00     0.37         0.    0.5109
    6   15    0     1.17     0.66     0.00     1.17         0.    1.5078
    7   16    0     1.20     0.65     0.00     1.20         0.    1.5138
    8    1    0     0.08     0.08     0.00     0.08         0.    0.1127
    9    5    0     0.86     0.59     0.00     0.86         0.    1.2926
   10    6    0     0.38     0.21     0.00     0.38         0.    0.2259
   11   18    0     0.36     0.20     0.00     0.36         0.    0.2259
   12    7    0     0.31     0.17     0.00     0.31         0.    0.1265
   13   19    0     0.28     0.16     0.00     0.28         0.    0.1265
   14   42    0     0.01     0.00     0.01     0.00         0.    0.0633
   15   43    0     0.01     0.00     0.00     0.01         0.    0.0633
   16   44    0     0.01     0.00     0.00     0.01         0.    0.0633
   17   75    0     0.01     0.01     0.00     0.01         0.    0.0202
   18   76    0     0.05     0.05     0.00     0.05         0.    0.1474
   19   77    0     0.03     0.02     0.00     0.03         0.    0.0516
   20   78    0     0.02     0.10     0.00     0.02         0.    0.0075
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.119999
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     5.7400s 
time spent in multnx:                   5.7300s 
integral transfer time:                 0.0100s 
time spent for loop construction:       3.3700s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            5.8600s 

          starting ci iteration   4

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.318095
ci vector #   2dasum_wr=    0.894784
ci vector #   3dasum_wr=    0.281710
ci vector #   4dasum_wr=    0.761372


====================================================================================================
Diagonal     counts:  0x:      412118 2x:      100824 4x:       16200
All internal counts: zz :      141469 yy:     1199860 xx:      733974 ww:      718502
One-external counts: yz :      664107 yx:     1998598 yw:     2016328
Two-external counts: yy :      580878 ww:      224232 xx:      351894 xz:       37085 wz:       33077 wx:      512216
Three-ext.   counts: yx :      139509 yw:      144149

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:       52777    task #     2:       40747    task #     3:      332519    task #     4:      166259
task #     5:      510932    task #     6:     1507769    task #     7:     1513804    task #     8:      112690
task #     9:     1292631    task #    10:      225883    task #    11:      225883    task #    12:      126547
task #    13:      126547    task #    14:       63273    task #    15:       63275    task #    16:       63275
task #    17:       20226    task #    18:      147443    task #    19:       51613    task #    20:       54916
 Overlap matrix (sovlci)

              sovl   1       sovl   2       sovl   3       sovl   4
 sovl   1     1.00000000
 sovl   2     0.00000000     0.02954205
 sovl   3    -0.01688510    -0.00040916     0.00403830
 sovl   4     0.00983492    -0.00069461    -0.00035225     0.00104040
 Overlap matrix (active-active) sovlaa

              sovl   1       sovl   2       sovl   3       sovl   4
 sovl   1     1.00000000
 sovl   2     0.00000000     0.00000000
 sovl   3    -0.01688510     0.00000000     0.00094921
 sovl   4     0.00983492     0.00000000    -0.00042699     0.00039033
 Overlap matrix (critical) sovlcrit

              sovl   1       sovl   2       sovl   3       sovl   4
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4
 sovl   1     0.00000000
 sovl   2     0.00000000     0.02954205
 sovl   3     0.00000000    -0.00040916     0.00308909
 sovl   4     0.00000000    -0.00069461     0.00007474     0.00065007
 Subspace hamiltonian (htci) 

              htci   1       htci   2       htci   3       htci   4
 htci   1    -8.82741282
 htci   2    -0.03984773    -0.22745428
 htci   3     0.14999347     0.00787611    -0.03070984
 htci   4    -0.08667956     0.00564805     0.00197706    -0.00783455
 Subspace hamiltonian (active-active) 

              htaa   1       htaa   2       htaa   3       htaa   4
 htaa   1    -8.82741282
 htaa   2     0.00000000     0.00000000
 htaa   3    -0.00770300    -0.00770300    -0.00770300
 htaa   4    -0.00316077    -0.00316077    -0.00316077    -0.00316077
 Subspace hamiltonian (critical) 

              htcr   1       htcr   2       htcr   3       htcr   4
 scale factor: (eold-eref)*(1-gvalue): -5.227370295093081E-002 *
  0.622222222222222      = -3.252585961391251E-002
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4
   ht   1    -8.82741282
   ht   2    -0.03984773    -0.22841516
   ht   3     0.14999347     0.00788942    -0.03081031
   ht   4    -0.08667956     0.00567064     0.00197463    -0.00785569
Spectrum of overlapmatrix:    0.000913    0.003759    0.029565    1.000383
calca4: root=   1 anorm**2=  0.05799268 scale:  1.02858771
calca4: root=   2 anorm**2=  0.89458492 scale:  1.37643922
calca4: root=   3 anorm**2=  0.89573684 scale:  1.37685760
calca4: root=   4 anorm**2=  0.68208606 scale:  1.29695261

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4
 refs   1    1.00000       2.065449E-13  -1.688510E-02   9.834922E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4
 civs   1  -0.958602      -0.224343       0.250225      -0.362784    
 civs   2   -1.27585        2.83716       -4.83101       -1.23522    
 civs   3    1.33476       -10.8681       -4.91906       -11.2127    
 civs   4    1.11509       -16.4208       -20.5136        19.9672    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4
 ref    1  -0.970173      -0.202331       0.131534       2.291947E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1    -0.97017309    -0.20233071     0.13153389     0.02291947
 NCSF=                  1368                 23810                 51154
                 65696
 fact= -3.351055614311538E-002
 factd= -3.351055614311538E-002

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  4  1   -383.3656967460  1.5825E-03  3.4022E-04  2.4037E-02  1.0000E-03
 mraqcc  #  4  2   -382.6913500281  2.3365E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  4  3   -381.9049402655  2.2175E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  4  4   -381.5053422022  7.0209E+00  0.0000E+00  6.7155E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.01     0.01     0.00     0.01         0.    0.0528
    2   25    0     0.02     0.01     0.00     0.02         0.    0.0407
    3   26    0     0.28     0.06     0.00     0.28         0.    0.3325
    4   27    0     0.28     0.08     0.00     0.28         0.    0.1663
    5   11    0     0.37     0.32     0.00     0.37         0.    0.5109
    6   15    0     1.17     0.65     0.00     1.17         0.    1.5078
    7   16    0     1.21     0.67     0.00     1.21         0.    1.5138
    8    1    0     0.08     0.08     0.00     0.08         0.    0.1127
    9    5    0     0.86     0.62     0.00     0.86         0.    1.2926
   10    6    0     0.38     0.21     0.00     0.37         0.    0.2259
   11   18    0     0.37     0.20     0.00     0.37         0.    0.2259
   12    7    0     0.30     0.17     0.00     0.30         0.    0.1265
   13   19    0     0.29     0.19     0.00     0.29         0.    0.1265
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0633
   15   43    0     0.01     0.00     0.00     0.01         0.    0.0633
   16   44    0     0.01     0.00     0.00     0.01         0.    0.0633
   17   75    0     0.01     0.01     0.00     0.01         0.    0.0202
   18   76    0     0.06     0.06     0.00     0.06         0.    0.1474
   19   77    0     0.02     0.02     0.00     0.02         0.    0.0516
   20   78    0     0.03     0.10     0.00     0.03         0.    0.0075
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.110001
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     5.7600s 
time spent in multnx:                   5.7500s 
integral transfer time:                 0.0000s 
time spent for loop construction:       3.4600s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            5.8800s 

          starting ci iteration   5

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.184247
ci vector #   2dasum_wr=    0.556340
ci vector #   3dasum_wr=    0.199503
ci vector #   4dasum_wr=    0.490678


====================================================================================================
Diagonal     counts:  0x:      412118 2x:      100824 4x:       16200
All internal counts: zz :      141469 yy:     1199860 xx:      733974 ww:      718502
One-external counts: yz :      664107 yx:     1998598 yw:     2016328
Two-external counts: yy :      580878 ww:      224232 xx:      351894 xz:       37085 wz:       33077 wx:      512216
Three-ext.   counts: yx :      139509 yw:      144149

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:       52777    task #     2:       40747    task #     3:      332519    task #     4:      166259
task #     5:      510932    task #     6:     1507769    task #     7:     1513804    task #     8:      112690
task #     9:     1292631    task #    10:      225883    task #    11:      225883    task #    12:      126547
task #    13:      126547    task #    14:       63273    task #    15:       63275    task #    16:       63275
task #    17:       20226    task #    18:      147443    task #    19:       51613    task #    20:       54916
 Overlap matrix (sovlci)

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5
 sovl   1     1.00000000
 sovl   2     0.00000000     0.02954205
 sovl   3    -0.01688510    -0.00040916     0.00403830
 sovl   4     0.00983492    -0.00069461    -0.00035225     0.00104040
 sovl   5     0.00518529    -0.00015504    -0.00000329     0.00005871     0.00027156
 Overlap matrix (active-active) sovlaa

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5
 sovl   1     1.00000000
 sovl   2     0.00000000     0.00000000
 sovl   3    -0.01688510     0.00000000     0.00094921
 sovl   4     0.00983492     0.00000000    -0.00042699     0.00039033
 sovl   5     0.00518529     0.00000000    -0.00014664     0.00006897     0.00010514
 Overlap matrix (critical) sovlcrit

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5
 sovl   1     0.00000000
 sovl   2     0.00000000     0.02954205
 sovl   3     0.00000000    -0.00040916     0.00308909
 sovl   4     0.00000000    -0.00069461     0.00007474     0.00065007
 sovl   5     0.00000000    -0.00015504     0.00014335    -0.00001027     0.00016642
 Subspace hamiltonian (htci) 

              htci   1       htci   2       htci   3       htci   4       htci   5
 htci   1    -8.82741282
 htci   2    -0.03984773    -0.22745428
 htci   3     0.14999347     0.00787611    -0.03070984
 htci   4    -0.08667956     0.00564805     0.00197706    -0.00783455
 htci   5    -0.04573740     0.00113880     0.00003342    -0.00082234    -0.00199339
 Subspace hamiltonian (active-active) 

              htaa   1       htaa   2       htaa   3       htaa   4       htaa   5
 htaa   1    -8.82741282
 htaa   2     0.00000000     0.00000000
 htaa   3    -0.00770300    -0.00770300    -0.00770300
 htaa   4    -0.00316077    -0.00316077    -0.00316077    -0.00316077
 htaa   5    -0.00083670    -0.00083670    -0.00083670    -0.00083670    -0.00083670
 Subspace hamiltonian (critical) 

              htcr   1       htcr   2       htcr   3       htcr   4       htcr   5
 scale factor: (eold-eref)*(1-gvalue): -5.385625094429258E-002 *
  0.622222222222222      = -3.351055614311538E-002
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1    -8.82741282
   ht   2    -0.03984773    -0.22844425
   ht   3     0.14999347     0.00788982    -0.03081335
   ht   4    -0.08667956     0.00567132     0.00197455    -0.00785633
   ht   5    -0.04573740     0.00114400     0.00002861    -0.00082200    -0.00199897
Spectrum of overlapmatrix:    0.000242    0.000913    0.003761    0.029566    1.000410
calca4: root=   1 anorm**2=  0.06101998 scale:  1.03005824
calca4: root=   2 anorm**2=  0.85842826 scale:  1.36324182
calca4: root=   3 anorm**2=  0.77648879 scale:  1.33284988
calca4: root=   4 anorm**2=  0.78527561 scale:  1.33614206
calca4: root=   5 anorm**2=  0.71520248 scale:  1.30965739

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1    1.00000       2.065449E-13  -1.688510E-02   9.834922E-03   5.185286E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 civs   1  -0.964078      -2.437802E-02   0.516876      -0.300035      -8.408874E-03
 civs   2   -1.28018        1.84373       -4.55672       -2.95767       0.290212    
 civs   3    1.40342       -9.20410        3.26004       -13.1030       -1.69983    
 civs   4    1.40285       -19.6326       -14.6771        3.03861        21.8785    
 civs   5    1.04003       -23.1871       -31.0413        21.7619       -46.4405    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1  -0.968585      -0.182283       0.156524       6.393696E-02  -5.341194E-03

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1    -0.96858505    -0.18228252     0.15652352     0.06393696    -0.00534119
 NCSF=                  1368                 23810                 51154
                 65696
 fact= -3.376690910440961E-002
 factd= -3.376690910440961E-002

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  5  1   -383.3661087419  4.1200E-04  9.5862E-05  1.2592E-02  1.0000E-03
 mraqcc  #  5  2   -382.8419652285  1.5062E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  5  3   -382.1520496538  2.4711E-01  0.0000E+00  2.1940E-01  1.0000E-04
 mraqcc  #  5  4   -381.6429804893  1.3764E-01  0.0000E+00  3.9204E-01  1.0000E-04
 mraqcc  #  5  5   -381.1030499745  6.6186E+00  0.0000E+00  7.3715E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.02     0.01     0.00     0.02         0.    0.0528
    2   25    0     0.02     0.01     0.00     0.02         0.    0.0407
    3   26    0     0.28     0.06     0.00     0.28         0.    0.3325
    4   27    0     0.28     0.07     0.00     0.28         0.    0.1663
    5   11    0     0.38     0.29     0.00     0.38         0.    0.5109
    6   15    0     1.17     0.64     0.00     1.17         0.    1.5078
    7   16    0     1.20     0.68     0.00     1.20         0.    1.5138
    8    1    0     0.08     0.07     0.00     0.08         0.    0.1127
    9    5    0     0.86     0.57     0.00     0.86         0.    1.2926
   10    6    0     0.38     0.19     0.00     0.38         0.    0.2259
   11   18    0     0.36     0.20     0.00     0.36         0.    0.2259
   12    7    0     0.31     0.18     0.00     0.31         0.    0.1265
   13   19    0     0.28     0.17     0.00     0.28         0.    0.1265
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0633
   15   43    0     0.02     0.00     0.00     0.02         0.    0.0633
   16   44    0     0.01     0.00     0.00     0.01         0.    0.0633
   17   75    0     0.01     0.01     0.00     0.01         0.    0.0202
   18   76    0     0.05     0.04     0.00     0.05         0.    0.1474
   19   77    0     0.02     0.01     0.00     0.02         0.    0.0516
   20   78    0     0.03     0.12     0.00     0.03         0.    0.0075
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.129999
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     5.7600s 
time spent in multnx:                   5.7600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       3.3200s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            5.8900s 

          starting ci iteration   6

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.111448
ci vector #   2dasum_wr=    0.298081
ci vector #   3dasum_wr=    0.126628
ci vector #   4dasum_wr=    0.267276


====================================================================================================
Diagonal     counts:  0x:      412118 2x:      100824 4x:       16200
All internal counts: zz :      141469 yy:     1199860 xx:      733974 ww:      718502
One-external counts: yz :      664107 yx:     1998598 yw:     2016328
Two-external counts: yy :      580878 ww:      224232 xx:      351894 xz:       37085 wz:       33077 wx:      512216
Three-ext.   counts: yx :      139509 yw:      144149

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:       52777    task #     2:       40747    task #     3:      332519    task #     4:      166259
task #     5:      510932    task #     6:     1507769    task #     7:     1513804    task #     8:      112690
task #     9:     1292631    task #    10:      225883    task #    11:      225883    task #    12:      126547
task #    13:      126547    task #    14:       63273    task #    15:       63275    task #    16:       63275
task #    17:       20226    task #    18:      147443    task #    19:       51613    task #    20:       54916
 Overlap matrix (sovlci)

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6
 sovl   1     1.00000000
 sovl   2     0.00000000     0.02954205
 sovl   3    -0.01688510    -0.00040916     0.00403830
 sovl   4     0.00983492    -0.00069461    -0.00035225     0.00104040
 sovl   5     0.00518529    -0.00015504    -0.00000329     0.00005871     0.00027156
 sovl   6    -0.00243637     0.00003690     0.00009618    -0.00000944    -0.00002369     0.00007942
 Overlap matrix (active-active) sovlaa

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6
 sovl   1     1.00000000
 sovl   2     0.00000000     0.00000000
 sovl   3    -0.01688510     0.00000000     0.00094921
 sovl   4     0.00983492     0.00000000    -0.00042699     0.00039033
 sovl   5     0.00518529     0.00000000    -0.00014664     0.00006897     0.00010514
 sovl   6    -0.00243637     0.00000000     0.00004630    -0.00001104    -0.00002908     0.00004264
 Overlap matrix (critical) sovlcrit

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6
 sovl   1     0.00000000
 sovl   2     0.00000000     0.02954205
 sovl   3     0.00000000    -0.00040916     0.00308909
 sovl   4     0.00000000    -0.00069461     0.00007474     0.00065007
 sovl   5     0.00000000    -0.00015504     0.00014335    -0.00001027     0.00016642
 sovl   6     0.00000000     0.00003690     0.00004987     0.00000160     0.00000539     0.00003678
 Subspace hamiltonian (htci) 

              htci   1       htci   2       htci   3       htci   4       htci   5       htci   6
 htci   1    -8.82741282
 htci   2    -0.03984773    -0.22745428
 htci   3     0.14999347     0.00787611    -0.03070984
 htci   4    -0.08667956     0.00564805     0.00197706    -0.00783455
 htci   5    -0.04573740     0.00113880     0.00003342    -0.00082234    -0.00199339
 htci   6     0.02151772    -0.00023807    -0.00085348     0.00008263     0.00011771    -0.00058140
 Subspace hamiltonian (active-active) 

              htaa   1       htaa   2       htaa   3       htaa   4       htaa   5       htaa   6
 htaa   1    -8.82741282
 htaa   2     0.00000000     0.00000000
 htaa   3    -0.00770300    -0.00770300    -0.00770300
 htaa   4    -0.00316077    -0.00316077    -0.00316077    -0.00316077
 htaa   5    -0.00083670    -0.00083670    -0.00083670    -0.00083670    -0.00083670
 htaa   6    -0.00033092    -0.00033092    -0.00033092    -0.00033092    -0.00033092    -0.00033092
 Subspace hamiltonian (critical) 

              htcr   1       htcr   2       htcr   3       htcr   4       htcr   5       htcr   6
 scale factor: (eold-eref)*(1-gvalue): -5.426824677494402E-002 *
  0.622222222222222      = -3.376690910440961E-002
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1    -8.82741282
   ht   2    -0.03984773    -0.22845182
   ht   3     0.14999347     0.00788992    -0.03081415
   ht   4    -0.08667956     0.00567150     0.00197453    -0.00785650
   ht   5    -0.04573740     0.00114404     0.00002858    -0.00082199    -0.00199901
   ht   6     0.02151772    -0.00023931    -0.00085517     0.00008257     0.00011753    -0.00058265
Spectrum of overlapmatrix:    0.000071    0.000243    0.000914    0.003762    0.029566    1.000416
calca4: root=   1 anorm**2=  0.06198899 scale:  1.03052850
calca4: root=   2 anorm**2=  0.82108969 scale:  1.34947756
calca4: root=   3 anorm**2=  0.78362893 scale:  1.33552571
calca4: root=   4 anorm**2=  0.82564617 scale:  1.35116475
calca4: root=   5 anorm**2=  0.61436072 scale:  1.27057495
calca4: root=   6 anorm**2=  0.60483006 scale:  1.26681887

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1    1.00000       2.065449E-13  -1.688510E-02   9.834922E-03   5.185286E-03  -2.436369E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1   0.963009       3.700653E-02  -0.344831       0.111119       0.512433       0.159813    
 civs   2    1.28092       -1.32220        4.20446       -3.61480       0.712863      -0.181137    
 civs   3   -1.42160        7.89152       -5.81407       -9.68221        8.85277       -2.38903    
 civs   4   -1.47980        18.7785        4.79767       -13.3749       -17.1450        15.6800    
 civs   5   -1.31956        29.4527        29.5365        18.0853       -11.0939       -44.4585    
 civs   6  -0.992620        31.7388        51.1864        63.7789        61.5445        49.7632    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.968036       0.163835      -0.171029       8.145148E-02  -1.313715E-02   2.591652E-03

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.96803584     0.16383525    -0.17102920     0.08145148    -0.01313715     0.00259165

 trial vector basis is being transformed.  new dimension:   3
 NCSF=                  1368                 23810                 51154
                 65696
 fact= -3.383592059595660E-002
 factd= -3.383592059595660E-002

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  6  1   -383.3662196532  1.1091E-04  2.7017E-05  6.8780E-03  1.0000E-03
 mraqcc  #  6  2   -382.9207064164  7.8741E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  6  3   -382.3134400919  1.6139E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  6  4   -381.6615469132  1.8566E-02  0.0000E+00  2.2260E-01  1.0000E-04
 mraqcc  #  6  5   -381.6261635183  5.2311E-01  0.0000E+00  7.8502E-01  1.0000E-04
 mraqcc  #  6  6   -380.9596646965  6.4752E+00  0.0000E+00  8.6699E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.02     0.02     0.00     0.02         0.    0.0528
    2   25    0     0.02     0.01     0.00     0.02         0.    0.0407
    3   26    0     0.28     0.07     0.00     0.28         0.    0.3325
    4   27    0     0.28     0.07     0.00     0.28         0.    0.1663
    5   11    0     0.36     0.31     0.00     0.36         0.    0.5109
    6   15    0     1.18     0.65     0.00     1.18         0.    1.5078
    7   16    0     1.19     0.66     0.00     1.19         0.    1.5138
    8    1    0     0.09     0.08     0.00     0.09         0.    0.1127
    9    5    0     0.86     0.57     0.00     0.86         0.    1.2926
   10    6    0     0.38     0.20     0.00     0.38         0.    0.2259
   11   18    0     0.36     0.19     0.00     0.36         0.    0.2259
   12    7    0     0.31     0.18     0.00     0.31         0.    0.1265
   13   19    0     0.28     0.18     0.00     0.28         0.    0.1265
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0633
   15   43    0     0.01     0.00     0.00     0.01         0.    0.0633
   16   44    0     0.01     0.00     0.00     0.01         0.    0.0633
   17   75    0     0.01     0.01     0.00     0.01         0.    0.0202
   18   76    0     0.06     0.06     0.00     0.06         0.    0.1474
   19   77    0     0.02     0.02     0.00     0.02         0.    0.0516
   20   78    0     0.03     0.12     0.00     0.03         0.    0.0075
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.139999
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     5.7500s 
time spent in multnx:                   5.7500s 
integral transfer time:                 0.0000s 
time spent for loop construction:       3.4000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            5.8900s 

          starting ci iteration   7

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.056416
ci vector #   2dasum_wr=    0.163178
ci vector #   3dasum_wr=    0.073378
ci vector #   4dasum_wr=    0.150455


====================================================================================================
Diagonal     counts:  0x:      412118 2x:      100824 4x:       16200
All internal counts: zz :      141469 yy:     1199860 xx:      733974 ww:      718502
One-external counts: yz :      664107 yx:     1998598 yw:     2016328
Two-external counts: yy :      580878 ww:      224232 xx:      351894 xz:       37085 wz:       33077 wx:      512216
Three-ext.   counts: yx :      139509 yw:      144149

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:       52777    task #     2:       40747    task #     3:      332519    task #     4:      166259
task #     5:      510932    task #     6:     1507769    task #     7:     1513804    task #     8:      112690
task #     9:     1292631    task #    10:      225883    task #    11:      225883    task #    12:      126547
task #    13:      126547    task #    14:       63273    task #    15:       63275    task #    16:       63275
task #    17:       20226    task #    18:      147443    task #    19:       51613    task #    20:       54916
 Overlap matrix (sovlci)

              sovl   1       sovl   2       sovl   3       sovl   4
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4    -0.00144211    -0.00085576    -0.00022784     0.00002185
 Overlap matrix (active-active) sovlaa

              sovl   1       sovl   2       sovl   3       sovl   4
 sovl   1     0.93801101
 sovl   2     0.15071320     0.17891031
 sovl   3    -0.16620287     0.09843230     0.21637107
 sovl   4    -0.00141435    -0.00080944    -0.00008705     0.00001171
 Overlap matrix (critical) sovlcrit

              sovl   1       sovl   2       sovl   3       sovl   4
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4
 sovl   1     0.06198899
 sovl   2    -0.15071320     0.82108969
 sovl   3     0.16620287    -0.09843230     0.78362893
 sovl   4    -0.00002776    -0.00004632    -0.00014079     0.00001014
 Subspace hamiltonian (htci) 

              htci   1       htci   2       htci   3       htci   4
 htci   1    -8.87969880
 htci   2    -0.00508912    -8.40855308
 htci   3     0.00561216    -0.00332375    -7.80255169
 htci   4     0.01278060     0.00834925     0.00369225    -0.00015865
 Subspace hamiltonian (active-active) 

              htaa   1       htaa   2       htaa   3       htaa   4
 htaa   1    -8.15661541
 htaa   2    -0.18277668    -5.69653398
 htaa   3     3.12422839    -3.32686167    -3.81222220
 htaa   4    -0.00009118    -0.00009118    -0.00009118    -0.00009118
 Subspace hamiltonian (critical) 

              htcr   1       htcr   2       htcr   3       htcr   4
 scale factor: (eold-eref)*(1-gvalue): -5.437915810064453E-002 *
  0.622222222222222      = -3.383592059595660E-002
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4
   ht   1    -8.88179625
   ht   2     0.00001040    -8.43633540
   ht   3    -0.00001147     0.00000679    -7.82906649
   ht   4     0.01278154     0.00835081     0.00369702    -0.00015899
Spectrum of overlapmatrix:    0.000019    1.000000    1.000000    1.000003
calca4: root=   1 anorm**2=  0.06209849 scale:  1.03058163
calca4: root=   2 anorm**2=  0.74533909 scale:  1.32111282
calca4: root=   3 anorm**2=  0.77960574 scale:  1.33401864
calca4: root=   4 anorm**2=  0.62658611 scale:  1.27537685

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4
 refs   1   0.968036       0.163835      -0.171029      -1.487015E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4
 civs   1   -1.00124      -6.187061E-02   0.107361       0.302834    
 civs   2   1.488571E-03   0.933215       0.256252       0.319406    
 civs   3   1.372319E-03   0.120413      -0.906643       0.407716    
 civs   4  -0.870229       -45.0662        76.0279        211.814    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4
 ref    1  -0.967937       0.139420       0.187920      -3.921795E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1    -0.96793691     0.13942046     0.18791995    -0.03921795
 NCSF=                  1368                 23810                 51154
                 65696
 fact= -3.385321493611507E-002
 factd= -3.385321493611507E-002

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  7  1   -383.3662474477  2.7794E-05  9.6980E-06  4.1152E-03  1.0000E-03
 mraqcc  #  7  2   -382.9732266884  5.2520E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  7  3   -382.4709230408  1.5748E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  7  4   -381.1861047798 -4.7544E-01  0.0000E+00  1.0069E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.02     0.01     0.00     0.02         0.    0.0528
    2   25    0     0.02     0.01     0.00     0.02         0.    0.0407
    3   26    0     0.28     0.07     0.00     0.28         0.    0.3325
    4   27    0     0.28     0.07     0.00     0.28         0.    0.1663
    5   11    0     0.36     0.30     0.00     0.36         0.    0.5109
    6   15    0     1.18     0.67     0.00     1.18         0.    1.5078
    7   16    0     1.20     0.69     0.00     1.20         0.    1.5138
    8    1    0     0.08     0.08     0.00     0.08         0.    0.1127
    9    5    0     0.86     0.57     0.00     0.86         0.    1.2926
   10    6    0     0.38     0.19     0.00     0.38         0.    0.2259
   11   18    0     0.36     0.20     0.00     0.36         0.    0.2259
   12    7    0     0.31     0.18     0.00     0.31         0.    0.1265
   13   19    0     0.28     0.18     0.00     0.28         0.    0.1265
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0633
   15   43    0     0.02     0.00     0.00     0.02         0.    0.0633
   16   44    0     0.01     0.00     0.00     0.01         0.    0.0633
   17   75    0     0.01     0.01     0.00     0.01         0.    0.0202
   18   76    0     0.05     0.03     0.00     0.05         0.    0.1474
   19   77    0     0.02     0.01     0.00     0.02         0.    0.0516
   20   78    0     0.03     0.13     0.00     0.02         0.    0.0075
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.129997
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     5.7500s 
time spent in multnx:                   5.7400s 
integral transfer time:                 0.0000s 
time spent for loop construction:       3.4000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            5.8800s 

          starting ci iteration   8

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.035575
ci vector #   2dasum_wr=    0.092119
ci vector #   3dasum_wr=    0.036735
ci vector #   4dasum_wr=    0.085352


====================================================================================================
Diagonal     counts:  0x:      412118 2x:      100824 4x:       16200
All internal counts: zz :      141469 yy:     1199860 xx:      733974 ww:      718502
One-external counts: yz :      664107 yx:     1998598 yw:     2016328
Two-external counts: yy :      580878 ww:      224232 xx:      351894 xz:       37085 wz:       33077 wx:      512216
Three-ext.   counts: yx :      139509 yw:      144149

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:       52777    task #     2:       40747    task #     3:      332519    task #     4:      166259
task #     5:      510932    task #     6:     1507769    task #     7:     1513804    task #     8:      112690
task #     9:     1292631    task #    10:      225883    task #    11:      225883    task #    12:      126547
task #    13:      126547    task #    14:       63273    task #    15:       63275    task #    16:       63275
task #    17:       20226    task #    18:      147443    task #    19:       51613    task #    20:       54916
 Overlap matrix (sovlci)

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4    -0.00144211    -0.00085576    -0.00022784     0.00002185
 sovl   5    -0.00019225     0.00025975     0.00031235     0.00000119     0.00000670
 Overlap matrix (active-active) sovlaa

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5
 sovl   1     0.93801101
 sovl   2     0.15071320     0.17891031
 sovl   3    -0.16620287     0.09843230     0.21637107
 sovl   4    -0.00141435    -0.00080944    -0.00008705     0.00001171
 sovl   5    -0.00014619     0.00023820     0.00035913     0.00000210     0.00000309
 Overlap matrix (critical) sovlcrit

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5
 sovl   1     0.06198899
 sovl   2    -0.15071320     0.82108969
 sovl   3     0.16620287    -0.09843230     0.78362893
 sovl   4    -0.00002776    -0.00004632    -0.00014079     0.00001014
 sovl   5    -0.00004606     0.00002156    -0.00004678    -0.00000091     0.00000361
 Subspace hamiltonian (htci) 

              htci   1       htci   2       htci   3       htci   4       htci   5
 htci   1    -8.87969880
 htci   2    -0.00508912    -8.40855308
 htci   3     0.00561216    -0.00332375    -7.80255169
 htci   4     0.01278060     0.00834925     0.00369225    -0.00015865
 htci   5     0.00170597    -0.00228524    -0.00268463     0.00000072    -0.00004592
 Subspace hamiltonian (active-active) 

              htaa   1       htaa   2       htaa   3       htaa   4       htaa   5
 htaa   1    -8.15661541
 htaa   2    -0.18277668    -5.69653398
 htaa   3     3.12422839    -3.32686167    -3.81222220
 htaa   4    -0.00009118    -0.00009118    -0.00009118    -0.00009118
 htaa   5    -0.00002197    -0.00002197    -0.00002197    -0.00002197    -0.00002197
 Subspace hamiltonian (critical) 

              htcr   1       htcr   2       htcr   3       htcr   4       htcr   5
 scale factor: (eold-eref)*(1-gvalue): -5.440695257589923E-002 *
  0.622222222222222      = -3.385321493611507E-002
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1    -8.88179732
   ht   2     0.00001301    -8.43634960
   ht   3    -0.00001434     0.00000850    -7.82908004
   ht   4     0.01278154     0.00835082     0.00369702    -0.00015899
   ht   5     0.00170753    -0.00228597    -0.00268304     0.00000075    -0.00004605
Spectrum of overlapmatrix:    0.000006    0.000019    1.000000    1.000000    1.000003
calca4: root=   1 anorm**2=  0.06223950 scale:  1.03065004
calca4: root=   2 anorm**2=  0.66975283 scale:  1.29218916
calca4: root=   3 anorm**2=  0.81013494 scale:  1.34541255
calca4: root=   4 anorm**2=  0.86733393 scale:  1.36650427
calca4: root=   5 anorm**2=  0.41305575 scale:  1.18872022

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1   0.968036       0.163835      -0.171029      -1.487015E-03  -1.371102E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 civs   1   -1.00156      -8.380645E-02   0.114425      -0.121905       0.270222    
 civs   2   2.067299E-03   0.827211       0.502643      -0.315921       0.131212    
 civs   3   1.867310E-03   0.181523      -0.706185      -0.691850       9.523872E-02
 civs   4   -1.23944       -72.2904        98.7943       -120.615        154.179    
 civs   5    1.02875        76.9770       -127.906        259.738        257.007    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1  -0.967829       0.120295       0.184525       9.230292E-02   2.287417E-03

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1    -0.96782883     0.12029542     0.18452477     0.09230292     0.00228742
 NCSF=                  1368                 23810                 51154
                 65696
 fact= -3.386009121736748E-002
 factd= -3.386009121736748E-002

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  8  1   -383.3662584988  1.1051E-05  2.8044E-06  2.2379E-03  1.0000E-03
 mraqcc  #  8  2   -383.0187484597  4.5522E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  8  3   -382.5989582348  1.2804E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  8  4   -381.8120291194  6.2592E-01  0.0000E+00  4.9123E-01  1.0000E-04
 mraqcc  #  8  5   -380.6473413493 -9.7882E-01  0.0000E+00  1.2298E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.02     0.01     0.00     0.02         0.    0.0528
    2   25    0     0.01     0.01     0.00     0.01         0.    0.0407
    3   26    0     0.29     0.06     0.00     0.29         0.    0.3325
    4   27    0     0.28     0.06     0.00     0.28         0.    0.1663
    5   11    0     0.36     0.27     0.00     0.36         0.    0.5109
    6   15    0     1.17     0.72     0.00     1.17         0.    1.5078
    7   16    0     1.20     0.65     0.00     1.20         0.    1.5138
    8    1    0     0.09     0.08     0.00     0.09         0.    0.1127
    9    5    0     0.86     0.58     0.00     0.86         0.    1.2926
   10    6    0     0.38     0.19     0.00     0.38         0.    0.2259
   11   18    0     0.36     0.18     0.00     0.36         0.    0.2259
   12    7    0     0.30     0.17     0.00     0.30         0.    0.1265
   13   19    0     0.29     0.18     0.00     0.29         0.    0.1265
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0633
   15   43    0     0.01     0.00     0.00     0.01         0.    0.0633
   16   44    0     0.01     0.00     0.00     0.01         0.    0.0633
   17   75    0     0.01     0.01     0.00     0.01         0.    0.0202
   18   76    0     0.06     0.06     0.00     0.06         0.    0.1474
   19   77    0     0.02     0.02     0.00     0.02         0.    0.0516
   20   78    0     0.03     0.13     0.00     0.03         0.    0.0075
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.130001
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     5.7500s 
time spent in multnx:                   5.7500s 
integral transfer time:                 0.0000s 
time spent for loop construction:       3.3800s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            5.8800s 

          starting ci iteration   9

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.018548
ci vector #   2dasum_wr=    0.055494
ci vector #   3dasum_wr=    0.027040
ci vector #   4dasum_wr=    0.049597


====================================================================================================
Diagonal     counts:  0x:      412118 2x:      100824 4x:       16200
All internal counts: zz :      141469 yy:     1199860 xx:      733974 ww:      718502
One-external counts: yz :      664107 yx:     1998598 yw:     2016328
Two-external counts: yy :      580878 ww:      224232 xx:      351894 xz:       37085 wz:       33077 wx:      512216
Three-ext.   counts: yx :      139509 yw:      144149

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:       52777    task #     2:       40747    task #     3:      332519    task #     4:      166259
task #     5:      510932    task #     6:     1507769    task #     7:     1513804    task #     8:      112690
task #     9:     1292631    task #    10:      225883    task #    11:      225883    task #    12:      126547
task #    13:      126547    task #    14:       63273    task #    15:       63275    task #    16:       63275
task #    17:       20226    task #    18:      147443    task #    19:       51613    task #    20:       54916
 Overlap matrix (sovlci)

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4    -0.00144211    -0.00085576    -0.00022784     0.00002185
 sovl   5    -0.00019225     0.00025975     0.00031235     0.00000119     0.00000670
 sovl   6    -0.00025517     0.00002805     0.00002910     0.00000007     0.00000035     0.00000192
 Overlap matrix (active-active) sovlaa

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6
 sovl   1     0.93801101
 sovl   2     0.15071320     0.17891031
 sovl   3    -0.16620287     0.09843230     0.21637107
 sovl   4    -0.00141435    -0.00080944    -0.00008705     0.00001171
 sovl   5    -0.00014619     0.00023820     0.00035913     0.00000210     0.00000309
 sovl   6    -0.00021776     0.00000510     0.00006653    -0.00000004     0.00000017     0.00000073
 Overlap matrix (critical) sovlcrit

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6
 sovl   1     0.06198899
 sovl   2    -0.15071320     0.82108969
 sovl   3     0.16620287    -0.09843230     0.78362893
 sovl   4    -0.00002776    -0.00004632    -0.00014079     0.00001014
 sovl   5    -0.00004606     0.00002156    -0.00004678    -0.00000091     0.00000361
 sovl   6    -0.00003741     0.00002295    -0.00003743     0.00000011     0.00000018     0.00000119
 Subspace hamiltonian (htci) 

              htci   1       htci   2       htci   3       htci   4       htci   5       htci   6
 htci   1    -8.87969880
 htci   2    -0.00508912    -8.40855308
 htci   3     0.00561216    -0.00332375    -7.80255169
 htci   4     0.01278060     0.00834925     0.00369225    -0.00015865
 htci   5     0.00170597    -0.00228524    -0.00268463     0.00000072    -0.00004592
 htci   6     0.00226513    -0.00026004    -0.00025583    -0.00000064    -0.00000585    -0.00001331
 Subspace hamiltonian (active-active) 

              htaa   1       htaa   2       htaa   3       htaa   4       htaa   5       htaa   6
 htaa   1    -8.15661541
 htaa   2    -0.18277668    -5.69653398
 htaa   3     3.12422839    -3.32686167    -3.81222220
 htaa   4    -0.00009118    -0.00009118    -0.00009118    -0.00009118
 htaa   5    -0.00002197    -0.00002197    -0.00002197    -0.00002197    -0.00002197
 htaa   6    -0.00000536    -0.00000536    -0.00000536    -0.00000536    -0.00000536    -0.00000536
 Subspace hamiltonian (critical) 

              htcr   1       htcr   2       htcr   3       htcr   4       htcr   5       htcr   6
 scale factor: (eold-eref)*(1-gvalue): -5.441800374219774E-002 *
  0.622222222222222      = -3.386009121736748E-002
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1    -8.88179775
   ht   2     0.00001404    -8.43635525
   ht   3    -0.00001549     0.00000917    -7.82908543
   ht   4     0.01278154     0.00835082     0.00369702    -0.00015899
   ht   5     0.00170753    -0.00228597    -0.00268304     0.00000075    -0.00004605
   ht   6     0.00226640    -0.00026082    -0.00025456    -0.00000064    -0.00000586    -0.00001335
Spectrum of overlapmatrix:    0.000002    0.000006    0.000019    1.000000    1.000000    1.000003
calca4: root=   1 anorm**2=  0.06233070 scale:  1.03069428
calca4: root=   2 anorm**2=  0.62721392 scale:  1.27562295
calca4: root=   3 anorm**2=  0.82710197 scale:  1.35170336
calca4: root=   4 anorm**2=  0.77935918 scale:  1.33392623
calca4: root=   5 anorm**2=  0.66721532 scale:  1.29120692
calca4: root=   6 anorm**2=  0.49690356 scale:  1.22348010

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1   0.968036       0.163835      -0.171029      -1.487015E-03  -1.371102E-04  -2.186754E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1   -1.00140      -6.222254E-02   3.971541E-02   9.777273E-02  -0.362397      -4.283944E-02
 civs   2   2.248427E-03   0.745357       0.639507      -0.138409      -0.264135      -2.232854E-02
 civs   3   1.996383E-03   0.197245      -0.497790      -0.786044      -0.338134       2.307594E-02
 civs   4   -1.33858       -81.7556        88.3890       -19.0888       -183.095       -71.2608    
 civs   5    1.30493        110.953       -168.747        182.904        18.1747       -287.149    
 civs   6   0.955096        107.526       -203.093        351.491       -407.993        448.831    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.967764       0.110993       0.164469       0.132853       2.273388E-02  -1.886252E-03

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.96776435     0.11099323     0.16446909     0.13285268     0.02273388    -0.00188625

 trial vector basis is being transformed.  new dimension:   3
 NCSF=                  1368                 23810                 51154
                 65696
 fact= -3.386202417108863E-002
 factd= -3.386202417108863E-002

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  9  1   -383.3662616054  3.1065E-06  8.6979E-07  1.2472E-03  1.0000E-03
 mraqcc  #  9  2   -383.0454110232  2.6663E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  9  3   -382.6858097111  8.6851E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  9  4   -382.1883709781  3.7634E-01  0.0000E+00  3.8679E-01  1.0000E-04
 mraqcc  #  9  5   -381.1777508414  5.3041E-01  0.0000E+00  8.1163E-01  1.0000E-04
 mraqcc  #  9  6   -380.1666254402 -7.9304E-01  0.0000E+00  1.1386E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.02     0.01     0.00     0.02         0.    0.0528
    2   25    0     0.01     0.01     0.00     0.01         0.    0.0407
    3   26    0     0.29     0.07     0.00     0.29         0.    0.3325
    4   27    0     0.28     0.07     0.00     0.28         0.    0.1663
    5   11    0     0.36     0.28     0.00     0.36         0.    0.5109
    6   15    0     1.17     0.74     0.00     1.17         0.    1.5078
    7   16    0     1.20     0.67     0.00     1.20         0.    1.5138
    8    1    0     0.09     0.08     0.00     0.09         0.    0.1127
    9    5    0     0.86     0.57     0.00     0.86         0.    1.2926
   10    6    0     0.38     0.20     0.00     0.38         0.    0.2259
   11   18    0     0.36     0.21     0.00     0.36         0.    0.2259
   12    7    0     0.31     0.18     0.00     0.31         0.    0.1265
   13   19    0     0.28     0.16     0.00     0.28         0.    0.1265
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0633
   15   43    0     0.01     0.00     0.00     0.01         0.    0.0633
   16   44    0     0.02     0.00     0.00     0.02         0.    0.0633
   17   75    0     0.01     0.00     0.00     0.01         0.    0.0202
   18   76    0     0.05     0.05     0.00     0.05         0.    0.1474
   19   77    0     0.02     0.02     0.00     0.02         0.    0.0516
   20   78    0     0.03     0.11     0.00     0.03         0.    0.0075
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.130001
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     5.7500s 
time spent in multnx:                   5.7500s 
integral transfer time:                 0.0000s 
time spent for loop construction:       3.4300s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            5.8800s 

          starting ci iteration  10

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.011003
ci vector #   2dasum_wr=    0.029192
ci vector #   3dasum_wr=    0.015644
ci vector #   4dasum_wr=    0.028836


====================================================================================================
Diagonal     counts:  0x:      412118 2x:      100824 4x:       16200
All internal counts: zz :      141469 yy:     1199860 xx:      733974 ww:      718502
One-external counts: yz :      664107 yx:     1998598 yw:     2016328
Two-external counts: yy :      580878 ww:      224232 xx:      351894 xz:       37085 wz:       33077 wx:      512216
Three-ext.   counts: yx :      139509 yw:      144149

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:       52777    task #     2:       40747    task #     3:      332519    task #     4:      166259
task #     5:      510932    task #     6:     1507769    task #     7:     1513804    task #     8:      112690
task #     9:     1292631    task #    10:      225883    task #    11:      225883    task #    12:      126547
task #    13:      126547    task #    14:       63273    task #    15:       63275    task #    16:       63275
task #    17:       20226    task #    18:      147443    task #    19:       51613    task #    20:       54916
 Overlap matrix (sovlci)

              sovl   1       sovl   2       sovl   3       sovl   4
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4    -0.00007699     0.00010265    -0.00003451     0.00000062
 Overlap matrix (active-active) sovlaa

              sovl   1       sovl   2       sovl   3       sovl   4
 sovl   1     0.93766930
 sovl   2    -0.09402901     0.37278608
 sovl   3    -0.16178427    -0.14843294     0.17289803
 sovl   4    -0.00008292     0.00010094    -0.00001314     0.00000029
 Overlap matrix (critical) sovlcrit

              sovl   1       sovl   2       sovl   3       sovl   4
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4
 sovl   1     0.06233070
 sovl   2     0.09402901     0.62721392
 sovl   3     0.16178427     0.14843294     0.82710197
 sovl   4     0.00000594     0.00000171    -0.00002137     0.00000033
 Subspace hamiltonian (htci) 

              htci   1       htci   2       htci   3       htci   4
 htci   1    -8.87972340
 htci   2     0.00318383    -8.53974582
 htci   3     0.00547803     0.00502595    -8.17337628
 htci   4     0.00068310    -0.00100866     0.00050275    -0.00000438
 Subspace hamiltonian (active-active) 

              htaa   1       htaa   2       htaa   3       htaa   4
 htaa   1    -8.19156636
 htaa   2    -1.01933382    -4.95176497
 htaa   3     2.00695982    -0.63817885    -2.38189274
 htaa   4    -0.00000214    -0.00000214    -0.00000214    -0.00000214
 Subspace hamiltonian (critical) 

              htcr   1       htcr   2       htcr   3       htcr   4
 scale factor: (eold-eref)*(1-gvalue): -5.442111027496388E-002 *
  0.622222222222222      = -3.386202417108863E-002
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4
   ht   1    -8.88183405
   ht   2    -0.00000018    -8.56098456
   ht   3    -0.00000031    -0.00000029    -8.20138363
   ht   4     0.00068290    -0.00100872     0.00050348    -0.00000439
Spectrum of overlapmatrix:    0.000001    1.000000    1.000000    1.000000
calca4: root=   1 anorm**2=  0.06231827 scale:  1.03068825
calca4: root=   2 anorm**2=  0.58663300 scale:  1.25961621
calca4: root=   3 anorm**2=  0.84688190 scale:  1.35900033
calca4: root=   4 anorm**2=  0.58271859 scale:  1.25806144

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4
 refs   1  -0.967764       0.110993       0.164469       9.138525E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4
 civs   1   -1.00006       1.125905E-02   1.941427E-02   9.590596E-02
 civs   2  -2.555313E-04   0.973485      -0.137042      -0.225891    
 civs   3   2.437744E-04  -8.302119E-02  -0.964781       0.253537    
 civs   4  -0.843675        151.962        256.838        1252.87    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4
 ref    1   0.967762       9.738674E-02  -0.169205       3.830596E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.96776154     0.09738674    -0.16920462     0.03830596
 NCSF=                  1368                 23810                 51154
                 65696
 fact= -3.386255572166259E-002
 factd= -3.386255572166259E-002

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  # 10  1   -383.3662624596  8.5428E-07  4.2763E-07  8.7152E-04  1.0000E-03
 mraqcc  # 10  2   -383.0653698893  1.9959E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 10  3   -382.7439674956  5.8158E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 10  4   -381.3724077591 -8.1596E-01  0.0000E+00  1.5560E+00  1.0000E-04
 
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.01     0.01     0.00     0.01         0.    0.0528
    2   25    0     0.02     0.02     0.00     0.02         0.    0.0407
    3   26    0     0.28     0.06     0.00     0.28         0.    0.3325
    4   27    0     0.28     0.07     0.00     0.28         0.    0.1663
    5   11    0     0.37     0.30     0.00     0.37         0.    0.5109
    6   15    0     1.17     0.68     0.00     1.17         0.    1.5078
    7   16    0     1.20     0.77     0.00     1.20         0.    1.5138
    8    1    0     0.08     0.07     0.00     0.08         0.    0.1127
    9    5    0     0.87     0.60     0.00     0.87         0.    1.2926
   10    6    0     0.38     0.19     0.00     0.38         0.    0.2259
   11   18    0     0.36     0.19     0.00     0.36         0.    0.2259
   12    7    0     0.30     0.17     0.00     0.30         0.    0.1265
   13   19    0     0.29     0.18     0.00     0.29         0.    0.1265
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0633
   15   43    0     0.01     0.00     0.00     0.01         0.    0.0633
   16   44    0     0.01     0.00     0.00     0.01         0.    0.0633
   17   75    0     0.01     0.01     0.00     0.01         0.    0.0202
   18   76    0     0.06     0.06     0.00     0.06         0.    0.1474
   19   77    0     0.02     0.02     0.00     0.02         0.    0.0516
   20   78    0     0.03     0.12     0.00     0.03         0.    0.0075
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.130001
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     5.7500s 
time spent in multnx:                   5.7500s 
integral transfer time:                 0.0000s 
time spent for loop construction:       3.5200s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            5.8800s 

 mraqcc   convergence criteria satisfied after 10 iterations.

 final mraqcc   convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  # 10  1   -383.3662624596  8.5428E-07  4.2763E-07  8.7152E-04  1.0000E-03
 mraqcc  # 10  2   -383.0653698893  1.9959E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 10  3   -382.7439674956  5.8158E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 10  4   -381.3724077591 -8.1596E-01  0.0000E+00  1.5560E+00  1.0000E-04

####################CIUDGINFO####################

   aqcc vector at position   1 energy= -383.366262459648

################END OF CIUDGINFO################

 
 a4den factor =  1.040340032 for root   1
diagon:itrnv=   0
    1 of the   5 expansion vectors are transformed.
    1 of the   4 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
maximum overlap with reference    1(overlap= 0.96776)
reference energy =                        -383.3118404951
total aqcc energy =                       -383.3662624596
diagonal element shift (lrtshift) =         -0.0544219646

 information on vector: 1 from unit 11 written to unit 48 filename civout              


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =      -383.3662624596

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7    8    9

                                          orbital     1    2    3    7    8   11   12   17   18

                                         symmetry   b1u  b1u  b1u  b2g  b2g  b3g  b3g  au   au 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1  0.023714                        +-   +-   +-   +-   +-                     
 z*  1  1       2 -0.056602                        +-   +-   +-   +-        +-                
 z*  1  1       5 -0.057329                        +-   +-   +-   +-                  +-      
 z*  1  1      23 -0.018024                        +-   +-   +-             +-        +-      
 z*  1  1      34 -0.143413                        +-   +-   +    +-    -   +          -      
 z*  1  1      43 -0.011960                        +-   +-   +     -   +-   +               - 
 z*  1  1      46 -0.047584                        +-   +-   +     -        +-   +     -      
 z*  1  1      50 -0.044003                        +-   +-   +     -        +         +-    - 
 z*  1  1      58  0.023649                        +-   +-   +    +         +-    -    -      
 z*  1  1      62 -0.035255                        +-   +-   +    +          -        +-    - 
 z*  1  1      82 -0.131234                        +-   +-        +-   +-   +-                
 z*  1  1      85 -0.058600                        +-   +-        +-   +-             +-      
 z*  1  1      88 -0.030157                        +-   +-        +-        +-   +-           
 z*  1  1      89  0.890789                        +-   +-        +-        +-        +-      
 z*  1  1      91 -0.031666                        +-   +-        +-        +-             +- 
 z*  1  1      93  0.031886                        +-   +-        +-        +     -   +     - 
 z*  1  1      95 -0.027738                        +-   +-        +-        +    +     -    - 
 z*  1  1      96 -0.017662                        +-   +-        +-             +-   +-      
 z*  1  1      99 -0.017828                        +-   +-        +-                  +-   +- 
 z*  1  1     102  0.081211                        +-   +-        +     -   +-        +     - 
 z*  1  1     104 -0.056520                        +-   +-        +     -   +     -   +-      
 z*  1  1     112 -0.066656                        +-   +-        +    +    +-         -    - 
 z*  1  1     113  0.024413                        +-   +-        +    +     -    -   +-      
 z*  1  1     120 -0.035719                        +-   +-             +-   +-        +-      
 z*  1  1     122  0.012830                        +-   +-             +-   +-             +- 
 z*  1  1     131 -0.042856                        +-   +-                  +-   +-   +-      
 z*  1  1     134 -0.040771                        +-   +-                  +-        +-   +- 
 z*  1  1     139  0.014692                        +-   +    +-   +-    -        +     -      
 z*  1  1     186  0.020950                        +-   +     -   +-   +-   +     -           
 z*  1  1     193  0.052919                        +-   +     -   +-        +-        +     - 
 z*  1  1     195 -0.080331                        +-   +     -   +-        +     -   +-      
 z*  1  1     204 -0.031260                        +-   +     -   +     -   +-        +-      
 z*  1  1     208 -0.018311                        +-   +     -   +     -   +     -   +     - 
 z*  1  1     242 -0.022490                        +-   +    +    +-        +-         -    - 
 z*  1  1     243  0.018873                        +-   +    +    +-         -    -   +-      
 z*  1  1     250  0.032013                        +-   +    +     -    -   +-        +-      
 z*  1  1     256 -0.020130                        +-   +    +     -    -   +    +     -    - 
 z*  1  1     285  0.137102                        +-   +         +-    -   +-   +     -      
 z*  1  1     289  0.049401                        +-   +         +-    -   +         +-    - 
 z*  1  1     293  0.044400                        +-   +         +-   +    +-    -    -      
 z*  1  1     297 -0.028220                        +-   +         +-   +     -        +-    - 
 z*  1  1     302  0.021917                        +-   +          -   +-   +-   +          - 
 z*  1  1     309  0.100884                        +-   +          -        +-   +    +-    - 
 z*  1  1     321  0.013734                        +-   +         +         +-    -   +-    - 
 z*  1  1     326  0.012020                        +-   +               -   +-   +     -   +- 
 z*  1  1     340 -0.016378                        +-        +-   +-        +-        +-      
 z*  1  1     390 -0.018184                        +-        +    +-    -   +    +-    -      
 z*  1  1     436  0.021988                        +-             +-   +-   +-   +-           
 z*  1  1     437 -0.034289                        +-             +-   +-   +-        +-      
 z*  1  1     448 -0.089810                        +-             +-        +-   +-   +-      
 z*  1  1     451 -0.040585                        +-             +-        +-        +-   +- 
 z*  1  1     455 -0.015494                        +-             +     -   +-   +-   +     - 
 z*  1  1     460  0.010235                        +-             +    +    +-   +-    -    - 
 z*  1  1     468  0.011530                        +-                       +-   +-   +-   +- 
 z   1  1     524 -0.023127                        +    +-    -   +-        +-        +-      
 z   1  1     527 -0.039464                        +    +-    -   +-        +     -   +-      
 z   1  1     574  0.032902                        +    +-   +    +-        +-         -    - 
 z   1  1     582 -0.025827                        +    +-   +     -    -   +-        +-      
 z   1  1     617  0.051308                        +    +-        +-    -   +-   +     -      
 z   1  1     621 -0.011803                        +    +-        +-    -   +         +-    - 
 z   1  1     625 -0.022757                        +    +-        +-   +    +-    -    -      
 z   1  1     629  0.027851                        +    +-        +-   +     -        +-    - 
 z   1  1     653  0.014642                        +    +-        +         +-    -   +-    - 
 z   1  1     780 -0.028903                        +     -        +-        +-   +-   +-      
 z   1  1    1064 -0.031187                             +-   +-   +-        +-        +-      
 z   1  1    1161 -0.012266                             +-        +-   +-   +-        +-      
 z   1  1    1172 -0.018062                             +-        +-        +-   +-   +-      
 y   1  1    1582  0.013317              1( b2g)   +-   +-    -   +-        +          -      
 y   1  1    1656  0.025662              1( b3g)   +-   +-    -   +         +-         -      
 y   1  1    1657 -0.012653              2( b3g)   +-   +-    -   +         +-         -      
 y   1  1    2151  0.010310              1( b2g)   +-   +-        +-    -   +-                
 y   1  1    2207 -0.016670              1( b3g)   +-   +-        +-         -        +-      
 y   1  1    2223  0.020556              1( b3g)   +-   +-        +-        +          -    - 
 y   1  1    2224 -0.010634              2( b3g)   +-   +-        +-        +          -    - 
 y   1  1    2231  0.010137              1( b3g)   +-   +-        +-              -   +-      
 y   1  1    2291  0.024046              1( b3g)   +-   +-         -   +     -        +-      
 y   1  1    2415 -0.015573              1( b3g)   +-   +-        +     -    -        +-      
 y   1  1    2416  0.010206              2( b3g)   +-   +-        +     -    -        +-      
 y   1  1    3074  0.020343              1( b3g)   +-    -   +    +-         -        +-      
 y   1  1    3610  0.037097              1( b3g)   +-    -        +-   +    +-         -      
 y   1  1    4422  0.024708              1( b3g)   +-   +     -   +-         -        +-      
 y   1  1    5178  0.020138              1( b3g)   +-   +         +-    -   +-         -      
 y   1  1    5248  0.013907              1( b2g)   +-   +         +-        +-    -    -      
 y   1  1    7329  0.013554              1( b2g)   +-             +-    -   +-        +-      
 y   1  1    7411  0.020687              1( b3g)   +-             +-        +-    -   +-      
 y   1  1    8732  0.024507              1( b3g)    -   +-        +-   +    +-         -      
 y   1  1    8797 -0.010116              1( b1u)    -   +-        +-        +-        +-      
 y   1  1   12701  0.011169              1( b3g)   +    +-   +-   +-    -              -      
 y   1  1   12984  0.017419              1( b3g)   +    +-    -   +-   +-    -                
 y   1  1   13052  0.076929              1( b3g)   +    +-    -   +-         -        +-      
 y   1  1   13053 -0.011154              2( b3g)   +    +-    -   +-         -        +-      
 y   1  1   13055  0.016478              4( b3g)   +    +-    -   +-         -        +-      
 y   1  1   13808  0.069619              1( b3g)   +    +-        +-    -   +-         -      
 y   1  1   13809 -0.022921              2( b3g)   +    +-        +-    -   +-         -      
 y   1  1   13811  0.011807              4( b3g)   +    +-        +-    -   +-         -      
 y   1  1   13826  0.010166              1( au )   +    +-        +-    -    -        +-      
 y   1  1   14080 -0.024557              1( b3g)   +    +-         -        +-        +-    - 
 y   1  1   15891  0.013649              1( b3g)   +     -        +-   +-   +-    -           
 y   1  1   16041  0.055698              1( b3g)   +     -        +-        +-    -   +-      
 y   1  1   16044  0.011606              4( b3g)   +     -        +-        +-    -   +-      
 y   1  1   16725 -0.011587              1( b3g)   +    +     -   +-    -    -   +     -      
 y   1  1   20775  0.046632              1( b3g)        +-        +-        +-    -   +-      
 y   1  1   20778  0.011165              4( b3g)        +-        +-        +-    -   +-      
 w   1  1   77984  0.010884    2( b3g)   1( au )   +-   +-        +-        +          -      
 w   1  1   78064 -0.024998    1( b3g)   1( b3g)   +-   +-        +-                  +-      
 w   1  1   78495 -0.010818    1( b2g)   1( au )   +-   +-        +         +-         -      
 w   1  1   85431 -0.010083    1( b1u)   1( b2g)   +-   +          -        +-        +-      
 w   1  1   85432  0.011617    2( b1u)   1( b2g)   +-   +          -        +-        +-      
 w   1  1   93446 -0.010915    1( b3g)   1( b3g)   +-             +-        +-        +-      
 w   1  1  100083 -0.013966    1( b1u)   1( b3g)   +    +-        +-         -        +-      
 w   1  1  100085  0.016002    3( b1u)   1( b3g)   +    +-        +-         -        +-      
 w   1  1  100094  0.010221    3( b1u)   4( b3g)   +    +-        +-         -        +-      
 w   1  1  108724 -0.035813    1( b3g)   1( b3g)   +     -        +-        +-        +-      
 w   1  1  108730 -0.013816    1( b3g)   4( b3g)   +     -        +-        +-        +-      
 w   1  1  132468  0.012561    1( b3g)   1( b3g)        +-   +    +-    -   +          -      
 w   1  1  133524  0.010694    1( b3g)   1( b3g)        +-        +-   +-   +-                
 w   1  1  133672  0.010267    1( b1u)   3( b1u)        +-        +-        +-        +-      
 w   1  1  133678 -0.061759    1( b3g)   1( b3g)        +-        +-        +-        +-      
 w   1  1  133684 -0.023621    1( b3g)   4( b3g)        +-        +-        +-        +-      

 ci coefficient statistics:
           rq > 0.1                5
      0.1> rq > 0.01             114
     0.01> rq > 0.001           1857
    0.001> rq > 0.0001         16210
   0.0001> rq > 0.00001        55367
  0.00001> rq > 0.000001       54645
 0.000001> rq                  13830
           all                142028
  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.023714475521
     2     2     -0.056602224092
     3     3      0.007409599824
     4     4      0.003103414332
     5     5     -0.057328797871
     6     6     -0.000376514411
     7     7      0.003122732435
     8     8      0.001587789783
     9     9      0.007534469752
    10    10     -0.001155835002
    11    11     -0.000204084839
    12    12     -0.008879948297
    13    13      0.000507348932
    14    14     -0.004034384461
    15    15      0.007059733073
    16    16      0.004826099088
    17    17     -0.000997145938
    18    18     -0.001979565204
    19    19      0.003468412292
    20    20      0.000434534571
    21    21     -0.002277408747
    22    22      0.005196841685
    23    23     -0.018023511312
    24    24     -0.001430325203
    25    25      0.002912457148
    26    26      0.002563292565
    27    27      0.000246511852
    28    28     -0.000637545397
    29    29      0.002924934666
    30    30      0.002881864280
    31    31     -0.000093850293
    32    32     -0.000455362850
    33    33      0.004921038119
    34    34     -0.143412609177
    35    35      0.000606289785
    36    36     -0.000255035684
    37    37     -0.006307169061
    38    38     -0.006013712418
    39    39      0.003595263835
    40    40     -0.001844594461
    41    41      0.000559843473
    42    42      0.001331161984
    43    43     -0.011959767607
    44    44      0.008873460785
    45    45     -0.001075944866
    46    46     -0.047583855347
    47    47      0.003792547244
    48    48      0.003999586269
    49    49      0.001888675065
    50    50     -0.044003065149
    51    51      0.001514111624
    52    52     -0.000473924537
    53    53      0.001465807499
    54    54      0.003307996966
    55    55     -0.009240896012
    56    56     -0.003582660602
    57    57      0.000153048039
    58    58      0.023648895973
    59    59      0.000647345475
    60    60     -0.001452397425
    61    61      0.001597167638
    62    62     -0.035254792439
    63    63     -0.001518600273
    64    64      0.001240447301
    65    65     -0.001689607860
    66    66      0.003693271659
    67    67     -0.002938044249
    68    68      0.008819773894
    69    69     -0.000527197515
    70    70      0.001204408266
    71    71     -0.009441644858
    72    72      0.001602565921
    73    73     -0.000245179374
    74    74     -0.000868834147
    75    75      0.003404816961
    76    76     -0.000725474487
    77    77     -0.000341260373
    78    78     -0.001053919888
    79    79     -0.001997577066
    80    80      0.002511615653
    81    81     -0.000270733511
    82    82     -0.131233877551
    83    83     -0.008305871048
    84    84      0.003862651963
    85    85     -0.058599807895
    86    86      0.001116981566
    87    87      0.004676417230
    88    88     -0.030157487825
    89    89      0.890788912321
    90    90      0.004199597053
    91    91     -0.031665994529
    92    92     -0.001025542571
    93    93      0.031886030937
    94    94     -0.001141980121
    95    95     -0.027738245923
    96    96     -0.017661560229
    97    97      0.001204364350
    98    98      0.002084283209
    99    99     -0.017827917661
   100   100      0.006009424438
   101   101     -0.000273532841
   102   102      0.081211247383
   103   103     -0.003184926887
   104   104     -0.056519688861
   105   105      0.005156627684
   106   106      0.004186029107
   107   107     -0.001912328654
   108   108     -0.000216711219
   109   109     -0.003245945421
   110   110      0.000360853693
   111   111     -0.001857380348
   112   112     -0.066656449713
   113   113      0.024412948908
   114   114     -0.003284255991
   115   115     -0.001412787429
   116   116     -0.002579812314
   117   117     -0.001291688132
   118   118      0.001758309323
   119   119      0.007698038619
   120   120     -0.035718583312
   121   121     -0.003084604603
   122   122      0.012829583866
   123   123      0.000202039210
   124   124     -0.006636493022
   125   125      0.001271552146
   126   126      0.001156932266
   127   127      0.005802355333
   128   128     -0.000673338506
   129   129     -0.000718193201
   130   130      0.003057082864
   131   131     -0.042856062023
   132   132      0.003105837568
   133   133      0.002645487381
   134   134     -0.040770603334
   135   135     -0.000650758120
   136   136      0.001496629274
   137   137      0.001186713315
   138   138     -0.008591926281
   139   139      0.014692243753
   140   140     -0.000064522842
   141   141      0.001489696308
   142   142      0.003690227028
   143   143      0.004728050466
   144   144     -0.001179903907
   145   145      0.006771777991
   146   146      0.001043810858
   147   147     -0.000218357574
   148   148      0.004668161412
   149   149     -0.000222151426
   150   150     -0.007715838933
   151   151      0.002106537035
   152   152      0.000855625064
   153   153      0.000302250467
   154   154     -0.001466827954
   155   155      0.008439455505
   156   156      0.000296855529
   157   157     -0.000518533859
   158   158     -0.000062955637
   159   159     -0.001067544267
   160   160      0.000687643393
   161   161      0.001329802172
   162   162      0.000602544011
   163   163     -0.002562147303
   164   164      0.000237119584
   165   165     -0.000839874490
   166   166     -0.002384830648
   167   167      0.002513595037
   168   168      0.000350568458
   169   169     -0.003670967737
   170   170      0.000171691272
   171   171      0.000296803249
   172   172      0.001561504320
   173   173     -0.001749393274
   174   174      0.000299952052
   175   175      0.000245667738
   176   176      0.001914860304
   177   177     -0.001526934878
   178   178     -0.000130272401
   179   179      0.000110251531
   180   180     -0.000836497595
   181   181      0.001805041251
   182   182      0.000247850614
   183   183      0.000253341210
   184   184      0.001016089164
   185   185      0.008374739002
   186   186      0.020950035521
   187   187      0.001374505862
   188   188     -0.006505570192
   189   189     -0.007600442709
   190   190      0.000933896834
   191   191     -0.006167802421
   192   192     -0.004433294315
   193   193      0.052919454832
   194   194      0.000179674388
   195   195     -0.080331119497
   196   196     -0.003209227857
   197   197      0.004774877360
   198   198     -0.001570618222
   199   199      0.000010416987
   200   200     -0.004096486688
   201   201      0.000321542917
   202   202     -0.002494371271
   203   203     -0.002178605359
   204   204     -0.031259825386
   205   205     -0.005503888772
   206   206      0.006157775895
   207   207     -0.001693327737
   208   208     -0.018311041271
   209   209      0.000540153743
   210   210      0.004056721065
   211   211      0.005763443400
   212   212     -0.000324785568
   213   213     -0.000931659522
   214   214     -0.000163143982
   215   215      0.002913226840
   216   216      0.001436757859
   217   217      0.003824560965
   218   218     -0.000520039680
   219   219      0.005600635095
   220   220      0.006745632799
   221   221      0.000256843987
   222   222      0.000246524079
   223   223      0.001476223930
   224   224     -0.004200699352
   225   225     -0.001213987211
   226   226      0.005237158322
   227   227      0.000702202204
   228   228     -0.003209751120
   229   229      0.000722805367
   230   230      0.000524343323
   231   231      0.001888296484
   232   232     -0.000380410026
   233   233      0.000618435630
   234   234     -0.002214434553
   235   235     -0.005260082019
   236   236      0.000814839824
   237   237      0.000770314528
   238   238      0.006454816081
   239   239      0.000320456969
   240   240     -0.005136356190
   241   241      0.004293144372
   242   242     -0.022489862667
   243   243      0.018872676120
   244   244     -0.001592584429
   245   245     -0.001086792975
   246   246     -0.001697473779
   247   247      0.003546882479
   248   248      0.000663620275
   249   249     -0.006961892185
   250   250      0.032013460372
   251   251      0.001341644744
   252   252     -0.001146081096
   253   253     -0.001443987090
   254   254      0.004185093451
   255   255      0.000798244967
   256   256     -0.020130063634
   257   257     -0.000018119389
   258   258      0.000041935376
   259   259      0.000977331310
   260   260     -0.003143718350
   261   261      0.000415998162
   262   262      0.001566364256
   263   263      0.001428555405
   264   264     -0.000680181672
   265   265     -0.000356095071
   266   266      0.002295296694
   267   267     -0.000247671328
   268   268      0.002964220544
   269   269     -0.003476765444
   270   270      0.004052205674
   271   271      0.000287922660
   272   272      0.004186949801
   273   273      0.001121692977
   274   274     -0.000434916140
   275   275     -0.003053203573
   276   276      0.001621692844
   277   277     -0.001157021933
   278   278     -0.000163951683
   279   279      0.001220363752
   280   280      0.000224562971
   281   281     -0.000772283645
   282   282     -0.000766615176
   283   283      0.002478693876
   284   284     -0.002818465810
   285   285      0.137102425553
   286   286     -0.004666857632
   287   287      0.003851726161
   288   288     -0.002848106648
   289   289      0.049401108421
   290   290     -0.004052561871
   291   291      0.000499041553
   292   292     -0.003522569318
   293   293      0.044400309117
   294   294     -0.007161258473
   295   295     -0.000592624050
   296   296      0.001624020991
   297   297     -0.028220181747
   298   298      0.001051958406
   299   299      0.001556278374
   300   300     -0.001551062624
   301   301     -0.005658379254
   302   302      0.021916734547
   303   303     -0.003254560271
   304   304      0.001534968517
   305   305     -0.003918026200
   306   306      0.003256685469
   307   307     -0.008170255763
   308   308      0.000118181783
   309   309      0.100883532811
   310   310      0.003566103730
   311   311      0.000740466179
   312   312      0.004665622049
   313   313     -0.006560969676
   314   314      0.006239586830
   315   315     -0.004068673973
   316   316      0.000882319011
   317   317     -0.000713335122
   318   318     -0.003869515652
   319   319      0.000776198985
   320   320     -0.000231422468
   321   321      0.013734244331
   322   322      0.004241038424
   323   323      0.001690109921
   324   324      0.000155523899
   325   325      0.002872093254
   326   326      0.012020062473
   327   327     -0.005626545218
   328   328      0.001099480859
   329   329      0.003763855115
   330   330      0.006033054154
   331   331      0.002859938357
   332   332     -0.000038850071
   333   333      0.003989295796
   334   334     -0.001530533690
   335   335     -0.003786085586
   336   336      0.003404038771
   337   337     -0.001064828340
   338   338     -0.001951222128
   339   339      0.005574660650
   340   340     -0.016377526372
   341   341     -0.000332890148
   342   342      0.005179802504
   343   343      0.000837291503
   344   344     -0.005410085941
   345   345     -0.000652313161
   346   346     -0.000087442524
   347   347      0.008942474707
   348   348      0.000492166972
   349   349     -0.000768392354
   350   350      0.002772062015
   351   351      0.000335750291
   352   352      0.001343108181
   353   353     -0.003051096437
   354   354     -0.000655115651
   355   355      0.003981316964
   356   356      0.000507734102
   357   357     -0.001761842832
   358   358     -0.000134155725
   359   359      0.000282501129
   360   360      0.002637703076
   361   361      0.000068891367
   362   362     -0.000145865137
   363   363      0.001681849921
   364   364     -0.000785643533
   365   365      0.000414205871
   366   366      0.000747625965
   367   367     -0.000139603400
   368   368     -0.000295088204
   369   369     -0.001522667729
   370   370     -0.001158769168
   371   371      0.002269574128
   372   372      0.000322550607
   373   373     -0.000709400045
   374   374     -0.000421177491
   375   375      0.000932479768
   376   376      0.000398250521
   377   377     -0.001065666441
   378   378     -0.000849064574
   379   379      0.000017198884
   380   380      0.000970995948
   381   381     -0.000516516256
   382   382      0.002787826330
   383   383      0.000137674163
   384   384     -0.001172877979
   385   385      0.001442900370
   386   386     -0.000331430746
   387   387     -0.001346323664
   388   388     -0.004011842673
   389   389      0.003070570618
   390   390     -0.018183723088
   391   391     -0.000810630692
   392   392      0.003937913152
   393   393      0.008530375143
   394   394     -0.002233747863
   395   395     -0.000310256254
   396   396     -0.000250364343
   397   397      0.003848188243
   398   398     -0.003091247579
   399   399      0.000920641282
   400   400     -0.001219530644
   401   401     -0.000759972551
   402   402      0.002784950000
   403   403     -0.000065693405
   404   404      0.003689698950
   405   405     -0.001641767589
   406   406     -0.000555147089
   407   407     -0.003351025411
   408   408      0.002285933902
   409   409     -0.000666471209
   410   410     -0.000750446867
   411   411     -0.001604613643
   412   412     -0.001462120173
   413   413      0.005213108182
   414   414     -0.008549856507
   415   415     -0.000443273944
   416   416     -0.001629125107
   417   417     -0.000758338762
   418   418      0.000959290435
   419   419     -0.002008548756
   420   420      0.001891357297
   421   421     -0.000284142689
   422   422     -0.000295468392
   423   423      0.000877515906
   424   424     -0.001417160790
   425   425     -0.002543817146
   426   426     -0.005126821184
   427   427     -0.000438610305
   428   428     -0.005042049895
   429   429     -0.001112920690
   430   430     -0.000357867213
   431   431     -0.003314264316
   432   432     -0.000154863436
   433   433     -0.000058525076
   434   434     -0.000341719764
   435   435     -0.000460612157
   436   436      0.021987565716
   437   437     -0.034289436174
   438   438      0.005869969273
   439   439      0.007518995090
   440   440      0.006568036918
   441   441      0.002049075735
   442   442     -0.000235204671
   443   443      0.007277483254
   444   444      0.005879589316
   445   445      0.000549215163
   446   446     -0.001135725755
   447   447      0.005444658134
   448   448     -0.089810374322
   449   449     -0.006582969366
   450   450      0.004906179165
   451   451     -0.040584647457
   452   452      0.002459408556
   453   453      0.002873898773
   454   454     -0.005039207346
   455   455     -0.015494182152
   456   456     -0.000344490863
   457   457      0.003731727892
   458   458      0.005259077500
   459   459      0.000160075998
   460   460      0.010234503738
   461   461     -0.002824701906
   462   462      0.005698393471
   463   463     -0.000081577357
   464   464     -0.003720785185
   465   465      0.002897516277
   466   466     -0.000915177629
   467   467     -0.001262411018
   468   468      0.011529788169

 number of reference csfs (nref) is   468.  root number (iroot) is  1.
 c0**2 =   0.93768173c0**2(scaled) =   0.99611643  c**2 (all zwalks) =   0.95019353
 passed aftci ... 
 readint2: molcas,dalton2=                     0                     0
 files%faoints=aoints              
lodens (list->root)=  1
                       Size (real*8) of d2temp for two-external contributions       2046
 
                       Size (real*8) of d2temp for all-internal contributions        360
                       Size (real*8) of d2temp for one-external contributions       1365
                       Size (real*8) of d2temp for two-external contributions       2046
size_thrext:  lsym   l1    ksym   k1strt   k1       cnt3 
                5    1    5    1    4       21
                5    1    6    1    3       27
                5    1    7    1    5       90
                5    1    8    1    3       75
                5    2    5    1    4       21
                5    2    6    1    3       27
                5    2    7    1    5       90
                5    2    8    1    3       75
                5    3    5    1    4       21
                5    3    6    1    3       27
                5    3    7    1    5       90
                5    3    8    1    3       75
                6    4    6    1    3       42
                6    4    7    1    5       60
                6    4    8    1    3       90
                6    5    6    1    3       42
                6    5    7    1    5       60
                6    5    8    1    3       90
                7    6    7    1    5      156
                7    6    8    1    3       72
                7    7    7    1    5      156
                7    7    8    1    3       72
                8    8    7    1    5       72
                8    8    8    1    3      120
                8    9    7    1    5       72
                8    9    8    1    3      120
                       Size (real*8) of d2temp for three-external contributions       1863
                       Size (real*8) of d2temp for four-external contributions        819
 enough memory for temporary d2 elements on vdisk ... 
location of d2temp files... fileloc(dd012)=       1
location of d2temp files... fileloc(d3)=       1
location of d2temp files... fileloc(d4)=       1
 files%dd012ext =  unit=  22  vdsk=   1  filestart=       1
 files%d3ext =     unit=  23  vdsk=   1  filestart=    5363
 files%d4ext =     unit=  24  vdsk=   1  filestart=   65363
            0xdiag    0ext      1ext      2ext      3ext      4ext
d2off                   767      1533      3065         1         1
d2rec                     1         2         3         1         1
recsize                 766       766       766     60000     60000
d2bufferlen=          60000
maxbl3=               60000
maxbl4=               60000
  allocated                 125363  DP for d2temp 
sifcfg setup: record length 4096 DP
# d1 elements per record  3272
# d2 elements per record  2730
  The AQCC density will be calculated.
 item #                     1 suffix=:.drt1.state1:
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method:  3 last record  1max overlap with ref# 97% root-following 0
 MR-AQCC energy:  -383.36626246    -8.88183478
 residuum:     0.00087152
 deltae:     0.00000085
 apxde:     0.00000043
 a4den:     1.04034003
 reference energy:  -383.31184050    -8.82741282

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.96776154     0.09738674    -0.16920462     0.03830596     0.02273388    -0.00188625     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.96776154     0.09738674    -0.16920462     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 rdcirefv: extracting reference vector # 1(blocksize=  1368 #zcsf=    1368)
 =========== Executing IN-CORE method ==========
================================================================================
   DYZ=       0  DYX=       0  DYW=       0
   D0Z=    3798  D0Y=       0  D0X=       0  D0W=       0
  DDZI=    9312 DDYI=       0 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
adding (  1) 2.00to den1(    1)=   0.0000000
adding (  2) 0.00to den1(    3)=   1.9240273
adding (  3) 0.00to den1(    6)=   0.0655251
adding (  4) 0.00to den1(   22)=   1.9431536
adding (  5) 0.00to den1(   24)=   0.1289925
adding (  6) 0.00to den1(   32)=   1.9332890
adding (  7) 0.00to den1(   34)=   0.0758440
adding (  8) 0.00to den1(   53)=   1.8743192
adding (  9) 0.00to den1(   55)=   0.0548493
 root #                      1 : Scaling(2) with    1.04034003160117      
 corresponding to ref #                      1
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=   20847  DYX=   64375  DYW=   66147
   D0Z=    3798  D0Y=   29412  D0X=    9870  D0W=   10500
  DDZI=    9312 DDYI=   57044 DDXI=   21280 DDWI=   22500
  DDZE=       0 DDYE=    8660 DDXE=    3704 DDWE=    3836
================================================================================
adding (  1) 2.00to den1(    1)=  -0.0575182
adding (  2) 0.00to den1(    3)=   1.9011085
adding (  3) 0.00to den1(    6)=   0.0822413
adding (  4) 0.00to den1(   22)=   1.9393672
adding (  5) 0.00to den1(   24)=   0.1443598
adding (  6) 0.00to den1(   32)=   1.9140616
adding (  7) 0.00to den1(   34)=   0.0995347
adding (  8) 0.00to den1(   53)=   1.8561730
adding (  9) 0.00to den1(   55)=   0.0571607
Trace of MO density:    10.000000
   10  correlated and     0  frozen core electrons

Natural orbital populations,block 5
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO
  occ(*)=     1.95962914     1.88502414     0.08185334     0.00315256     0.00209737     0.00054551

Natural orbital populations,block 6
              MO     1       MO     2       MO     3       MO     4       MO
  occ(*)=     1.93937058     0.14438241     0.00268127     0.00064984

Natural orbital populations,block 7
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO
  occ(*)=     1.91454760     0.11314376     0.03411875     0.00227222     0.00073700     0.00017285

Natural orbital populations,block 8
              MO     1       MO     2       MO     3       MO     4       MO
  occ(*)=     1.85620120     0.05719021     0.00193676     0.00029349


 total number of electrons =   10.0000000000

 test slabel:                     8
  ag  b3u b2u b1g b1u b2g b3g au 


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        b1u partial gross atomic populations
   ao class       1b1u       2b1u       3b1u       4b1u       5b1u       6b1u
    to_ s       1.959629   1.885024   0.081853   0.003153   0.002097   0.000546

                        b2g partial gross atomic populations
   ao class       1b2g       2b2g       3b2g       4b2g
    to_ s       1.939371   0.144382   0.002681   0.000650

                        b3g partial gross atomic populations
   ao class       1b3g       2b3g       3b3g       4b3g       5b3g       6b3g
    to_ s       1.914548   0.113144   0.034119   0.002272   0.000737   0.000173

                        au  partial gross atomic populations
   ao class       1au        2au        3au        4au 
    to_ s       1.856201   0.057190   0.001937   0.000023


                        gross atomic populations
     ao           to_
      s         9.999729
    total       9.999729
 

 Total number of electrons:    9.99972932

========================GLOBAL TIMING PER NODE========================
   process    vectrn  integral   segment    diagon     dmain    davidc   finalvw   driver  
--------------------------------------------------------------------------------
   #   1         0.0       0.0       0.0      58.7      58.9       0.0       0.0      59.0
--------------------------------------------------------------------------------
       1         0.0       0.0       0.0      58.7      58.9       0.0       0.0      59.0
 DA ...
