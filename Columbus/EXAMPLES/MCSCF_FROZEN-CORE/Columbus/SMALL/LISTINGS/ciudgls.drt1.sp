1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      2009-03.    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


================ Computing sorting integral file structure ================

                    -----z----- -----y----- -----x----- -----w----- ---total---

                CSFs      1368       23810       51154       65696      142028
      internal walks      2672        8820        3864        5288       20644
valid internal walks      1368        8660        3704        3836       17568
 lcore1,lcore2=             498036007             498011921
 lencor,maxblo             498073600                 60000

 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg= 497957310
 minimum size of srtscr:     32767 WP (     1 records)
 maximum size of srtscr:    163835 WP (     3 records)
========================================
 current settings:
 minbl3          63
 minbl4          63
 locmaxbl3      252
 locmaxbuf      126
 maxbl3       60000
 maxbl3       60000
 maxbl4       60000
 maxbuf       30006
========================================

 sorted 4-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)

 sorted 3-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)
 Orig.  diagonal integrals:  1electron:        20
                             0ext.    :        90
                             2ext.    :       198
                             4ext.    :       132


 Orig. off-diag. integrals:  4ext.    :       753
                             3ext.    :      1863
                             2ext.    :      2310
                             1ext.    :      1365
                             0ext.    :       360
                             2ext. SO :         0
                             1ext. SO :         0
                             0ext. SO :         0
                             1electron:        22


 Sorted integrals            3ext.  w :      1613 x :      1363
                             4ext.  w :       533 x :       357


 compressed index vector length=                  4044
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 3
  GSET = 3
   NCOREL = 10
  NROOT = 1
  IVMODE = 3
  NBKITR = 1
  NVBKMN = 1
  RTOLBK = 1e-3,
  NITER = 20
  NVCIMN = 3
  RTOLCI = 1e-3,
  NVCIMX = 6
  NVRFMX = 6
  NVBKMX = 6
  IDEN  = 1
  CSFPRN = 10,
 /&end
 ------------------------------------------------------------------------
lodens (list->root)=  1
invlodens (root->list)=  1
 bummer (warning):2:changed keyword: nbkitr=                      0
 USING SEGMENTS OF EQUAL SIZE

****************  list of control variables  ****************
 lvlprt =    0      nroot  =    1      noldv  =   0      noldhv =   0
 nunitv =    1      nbkitr =    0      niter  =  20      davcor =  10
 csfprn =   10      ivmode =    3      istrt  =   0      vout   =   1
 iortls =    0      nvbkmx =    6      ibktv  =  -1      ibkthv =  -1
 nvcimx =    6      icitv  =   -1      icithv =  -1      frcsub =   0
 nvbkmn =    1      nvcimn =    3      maxseg =   4      nrfitr =  30
 ncorel =   10      nvrfmx =    6      nvrfmn =   3      iden   =   1
 itran  =    0      froot  =    0      rtmode =   0      ncouple=   1
 skipso =    F      dalton2=    0      molcas =   0      finalv =   0
 finalw =    0      cosmocalc=   0    with_tsklst=   0
 nsegwx =    1     1     1     1
 nseg0x =    1     1     1     1
 nseg1x =    1     1     1     1
 nseg2x =    1     1     1     1
 nseg3x =    1     1     1     1
 nseg4x =    1     1     1     1
 no0ex  =      0    no1ex  =      0    no2ex  =     0    no3ex  =     0
 no4ex  =      0    nodiag =      0
 cdg4ex =    1      c3ex1ex=    1      c2ex0ex=   1
 fileloc=    0     0     0     0     0     0     0     1     1     1
 directhd=   1      noaqccshift_zyxw=      0
 critical_crit=-1.00000    critical_delta= 0.05000

 ctol   = 0.010000    lrtshift=1.000000    smalld =0.001000


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-03    1.000E-03
 Computing density:                    .drt1.state1
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core          498073599 DP per process

********** Integral sort section *************


 workspace allocation information: lencor= 498073599

 echo of the input for program cisrt:
 ------------------------------------------------------------------------
  &input
  maxbl3=60000
  maxbl4=60000
  &end
 ------------------------------------------------------------------------
 
 ( 6) listing file:                    ciudgls             
 ( 5) input file:                      cisrtin   
 (17) cidrt file:                      cidrtfl             
 (11) transformed integrals file:      moints    
 (12) diagonal integral file:          diagint             
 (13) off-diagonal integral file:      ofdgint             
 (31) 4-external w integrals file:     fil4w               
 (32) 4-external x integrals file:     fil4x               
 (33) 3-external w integrals file:     fil3w               
 (34) 3-external x integrals file:     fil3x               
 (21) scratch da sorting file:         srtscr              
 (12) 2-e integral file [fsplit=2]:    moints2   

 input integral file header information:
 Hermit Integral Program : SIFS version  owl7.itc.univie.a 13:41:51.495 19-Feb-12
  cidrt_title                                                                    
 mo coefficients generated by scfpq                                              
 SIFS file created by program tran.      owl7.itc.univie.a 13:42:11.277 19-Feb-12
 MO-coefficients from mcscf.x                                                    
  total ao core energy = -369.616790184                                          
 SIFS file created by program tran.      owl7.itc.univie.a 13:42:35.495 19-Feb-12

 input energy(*) values:
 energy( 1)=  4.557548380050E+02, ietype=   -1,    core energy of type: Nuc.Rep.

 total core energy =   4.557548380050E+02

 nsym = 8 nmot=  20

 symmetry  =    1    2    3    4    5    6    7    8
 slabel(*) =  Ag   B3u  B2u  B1g  B1u  B2g  B3g  Au 
 nmpsy(*)  =    0    0    0    0    6    4    6    4

 info(*) =          1      8192      6552      8192      5460         0

 orbital labels, i:molab(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012  13:tout:013  14:tout:014  15:tout:015  16:tout:016  17:tout:017  18:tout:018  19:tout:019  20:tout:020

 input parameters:
 prnopt=  0
 ldamin=    4095 ldamax=   32767 ldainc=      64
 maxbuf=   30006 maxbl3=   60000 maxbl4=   60000 intmxo=     766
  Using 32 bit compression 

 drt information:
  cidrt_title                                                                    
 nmotd =  20 nfctd =   0 nfvtc =   0 nmot  =  20
 nlevel =  20 niot  =   9 lowinl=  12
 orbital-to-level map(*)
   12  13  14   1   2   3  15  16   4   5  17  18   6   7   8   9  19  20  10  11
 compressed map(*)
   12  13  14   1   2   3  15  16   4   5  17  18   6   7   8   9  19  20  10  11
 levsym(*)
    5   5   5   6   6   7   7   7   7   8   8   5   5   5   6   6   7   7   8   8
 repartitioning mu(*)=
   2.  0.  0.  0.  0.  0.  0.  0.  0.

 new core energy added to the energy(*) list.
 from the integral file: h1_core= -8.253716281894E+02

 indxdg: diagonal integral statistics.
 total number of integrals contributing to diagonal matrix elements:       420
 number with all external indices:       132
 number with half external - half internal indices:       198
 number with all internal indices:        90

 indxof: off-diagonal integral statistics.
    4-external integrals: num=        753 strt=          1
    3-external integrals: num=       1863 strt=        754
    2-external integrals: num=       2310 strt=       2617
    1-external integrals: num=       1365 strt=       4927
    0-external integrals: num=        360 strt=       6292

 total number of off-diagonal integrals:        6651


 indxof(2nd)  ittp=   3 numx(ittp)=        2310
 indxof(2nd)  ittp=   4 numx(ittp)=        1365
 indxof(2nd)  ittp=   5 numx(ittp)=         360

 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg= 497898765
 pro2e        1     211     421     631     841     886     931    1141   44829   88517
   121284  129476  134936  156775

 pro2e:      5683 integrals read in     2 records.
 pro1e        1     211     421     631     841     886     931    1141   44829   88517
   121284  129476  134936  156775
 pro1e: eref =   -4.867637328741319E+00
 total size of srtscr:                     3  records of                  32767 
 WP =                786408 Bytes

 new core energy added to the energy(*) list.
 from the hamiltonian repartitioning, eref= -4.867637328741E+00
 putdg        1     211     421     631    1397   34164   56009    1141   44829   88517
   121284  129476  134936  156775

 putf:       4 buffers of length     766 written to file 12
 diagonal integral file completed.

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep4e:     7 blocks of linear combinations of 4-external integrals processed.
 number of sorted 4-external integrals        890
 number of original 4-external integrals      753


 putf34: external integral file complete. nfilw=    31 nfilx=    32 nrecw=     1 nrecx=     1 lbufp= 30006

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep3e:    26 blocks of linear combinations of 3-external integrals processed.
 number of sorted 3-external integrals       2976
 number of original 3-external integrals     1863


 putf34: external integral file complete. nfilw=    33 nfilx=    34 nrecw=     1 nrecx=     1 lbufp= 30006

 putf:       8 buffers of length     766 written to file 13
 off-diagonal files sort completed.
 executing brd_struct for cisrtinfo
cisrtinfo:
bufszi   766
 diagfile 4ext:     132 2ext:     198 0ext:      90
 fil4w,fil4x  :     753 fil3w,fil3x :    1863
 ofdgint  2ext:    2310 1ext:    1365 0ext:     360so0ext:       0so1ext:       0so2ext:       0
buffer minbl4      63 minbl3      63 maxbl2      66nbas:   0   0   0   0   3   2   4   2 maxbuf 30006
 CIUDG version 5.9.7 ( 5-Oct-2004)

 workspace allocation information: lcore= 498073599

 core energy values from the integral file:
 energy( 1)=  4.557548380050E+02, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -8.253716281894E+02, ietype=    6,   fcore energy of type: H1(*)   
 energy( 3)= -4.867637328741E+00, ietype=    5,   fcore energy of type: Vref(*) 

 total core repulsion energy = -3.744844275131E+02
 nmot  =    20 niot  =     9 nfct  =     0 nfvt  =     0
 nrow  =    97 nsym  =     8 ssym  =     1 lenbuf=  1600
 nwalk,xbar:      20644     2672     8820     3864     5288
 nvalwt,nvalw:    17568     1368     8660     3704     3836
 ncsft:          142028
 total number of valid internal walks:   17568
 nvalz,nvaly,nvalx,nvalw =     1368    8660    3704    3836

 cisrt info file parameters:
 file number  12 blocksize    766
 mxbld    766
 nd4ext,nd2ext,nd0ext   132   198    90
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int      753     1863     2310     1365      360        0        0        0
 minbl4,minbl3,maxbl2    63    63    66
 maxbuf 30006
 number of external orbitals per symmetry block:   0   0   0   0   3   2   4   2
 nmsym   8 number of internal orbitals   9
 executing brd_struct for drt
 executing brd_struct for orbinf
 executing brd_struct for momap
 calcthrxt: niot,maxw1=                     9                 11894
 block size     0
 pthz,pthy,pthx,pthw:  2672  8820  3864  5288 total internal walks:   20644
 maxlp3,n2lp,n1lp,n0lp 11894     0     0     0
 orbsym(*)= 5 5 5 6 6 7 7 8 8

 setref:      468 references kept,
                0 references were marked as invalid, out of
              468 total.
 nmb.of records onel     1
 nmb.of records 2-ext     4
 nmb.of records 1-ext     2
 nmb.of records 0-ext     1
 nmb.of records 2-int     0
 nmb.of records 1-int     0
 nmb.of records 0-int     0
 ---------memory usage in DP -----------------
 < n-ex core usage >
     routines:
    fourex            60816
    threx            131377
    twoex              1088
    onex                815
    allin               766
    diagon             1308
               =======
   maximum           131377
 
  __ static summary __ 
   reflst              1368
   hrfspc              1368
               -------
   static->            2736
 
  __ core required  __ 
   totstc              2736
   max n-ex          131377
               -------
   totnec->          134113
 
  __ core available __ 
   totspc         498073599
   totnec -          134113
               -------
   totvec->       497939486

 number of external paths / symmetry
 vertex x      11      14      16      14       0       0       0       0
 vertex w      22      14      16      14       0       0       0       0
segment: free space=   497939486
 reducing frespc by                 55315 to              497884171 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1        2672|      1368|         0|      1368|         0|         1|
 -------------------------------------------------------------------------------
  Y 2        8820|     23810|      1368|      8660|      1368|         2|
 -------------------------------------------------------------------------------
  X 3        3864|     51154|     25178|      3704|     10028|         3|
 -------------------------------------------------------------------------------
  W 4        5288|     65696|     76332|      3836|     13732|         4|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=        2292DP  conft+indsym=       34640DP  drtbuffer=       18383 DP

dimension of the ci-matrix ->>>    142028

 executing brd_struct for civct
 gentasklist: ntask=                    20
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS   
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1    3864    2672      51154       1368    3704    1368
     2  4   1    25      two-ext wz   2X  4 1    5288    2672      65696       1368    3836    1368
     3  4   3    26      two-ext wx*  WX  4 3    5288    3864      65696      51154    3836    3704
     4  4   3    27      two-ext wx+  WX  4 3    5288    3864      65696      51154    3836    3704
     5  2   1    11      one-ext yz   1X  2 1    8820    2672      23810       1368    8660    1368
     6  3   2    15      1ex3ex yx    3X  3 2    3864    8820      51154      23810    3704    8660
     7  4   2    16      1ex3ex yw    3X  4 2    5288    8820      65696      23810    3836    8660
     8  1   1     1      allint zz    OX  1 1    2672    2672       1368       1368    1368    1368
     9  2   2     5      0ex2ex yy    OX  2 2    8820    8820      23810      23810    8660    8660
    10  3   3     6      0ex2ex xx*   OX  3 3    3864    3864      51154      51154    3704    3704
    11  3   3    18      0ex2ex xx+   OX  3 3    3864    3864      51154      51154    3704    3704
    12  4   4     7      0ex2ex ww*   OX  4 4    5288    5288      65696      65696    3836    3836
    13  4   4    19      0ex2ex ww+   OX  4 4    5288    5288      65696      65696    3836    3836
    14  2   2    42      four-ext y   4X  2 2    8820    8820      23810      23810    8660    8660
    15  3   3    43      four-ext x   4X  3 3    3864    3864      51154      51154    3704    3704
    16  4   4    44      four-ext w   4X  4 4    5288    5288      65696      65696    3836    3836
    17  1   1    75      dg-024ext z  OX  1 1    2672    2672       1368       1368    1368    1368
    18  2   2    76      dg-024ext y  OX  2 2    8820    8820      23810      23810    8660    8660
    19  3   3    77      dg-024ext x  OX  3 3    3864    3864      51154      51154    3704    3704
    20  4   4    78      dg-024ext w  OX  4 4    5288    5288      65696      65696    3836    3836
----------------------------------------------------------------------------------------------------
REDTASK #   1 TIME=  19.000 N=  1 (task/type/sgbra)=(   1/24/0) (
REDTASK #   2 TIME=  18.000 N=  1 (task/type/sgbra)=(   2/25/0) (
REDTASK #   3 TIME=  17.000 N=  1 (task/type/sgbra)=(   3/26/1) (
REDTASK #   4 TIME=  16.000 N=  1 (task/type/sgbra)=(   4/27/2) (
REDTASK #   5 TIME=  15.000 N=  1 (task/type/sgbra)=(   5/11/0) (
REDTASK #   6 TIME=  14.000 N=  1 (task/type/sgbra)=(   6/15/0) (
REDTASK #   7 TIME=  13.000 N=  1 (task/type/sgbra)=(   7/16/0) (
REDTASK #   8 TIME=  12.000 N=  1 (task/type/sgbra)=(   8/ 1/0) (
REDTASK #   9 TIME=  11.000 N=  1 (task/type/sgbra)=(   9/ 5/0) (
REDTASK #  10 TIME=  10.000 N=  1 (task/type/sgbra)=(  10/ 6/1) (
REDTASK #  11 TIME=   9.000 N=  1 (task/type/sgbra)=(  11/18/2) (
REDTASK #  12 TIME=   8.000 N=  1 (task/type/sgbra)=(  12/ 7/1) (
REDTASK #  13 TIME=   7.000 N=  1 (task/type/sgbra)=(  13/19/2) (
REDTASK #  14 TIME=   6.000 N=  1 (task/type/sgbra)=(  14/42/1) (
REDTASK #  15 TIME=   5.000 N=  1 (task/type/sgbra)=(  15/43/1) (
REDTASK #  16 TIME=   4.000 N=  1 (task/type/sgbra)=(  16/44/1) (
REDTASK #  17 TIME=   3.000 N=  1 (task/type/sgbra)=(  17/75/1) (
REDTASK #  18 TIME=   2.000 N=  1 (task/type/sgbra)=(  18/76/1) (
REDTASK #  19 TIME=   1.000 N=  1 (task/type/sgbra)=(  19/77/1) (
REDTASK #  20 TIME=   0.000 N=  1 (task/type/sgbra)=(  20/78/1) (
 initializing v-file: 1:                142028

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       37290 2x:           0 4x:           0
All internal counts: zz :      141469 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:           0    task #     2:           0    task #     3:           0    task #     4:           0
task #     5:           0    task #     6:           0    task #     7:           0    task #     8:      112690
task #     9:           0    task #    10:           0    task #    11:           0    task #    12:           0
task #    13:           0    task #    14:           0    task #    15:           0    task #    16:           0
task #    17:       20226    task #    18:           0    task #    19:           0    task #    20:           0
 reference space has dimension     468
 dsyevx: computed roots 1 to    2(converged:   2)

    root           eigenvalues
    ----           ------------
       1        -383.3118404911
       2        -383.0632185372

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=                  1368

         vector  1 from unit 11 written to unit 49 filename cirefv              
 ncorel,neli=                    10                    10
  This is a  mraqcc energy evaluation      

    ------------------------------------------------------------

 setopt2: mraqcc energy evaluation      gvalue=  0.377777777777778      ncorel=
                    10

    ------------------------------------------------------------


    reference energies:

  reference state used is:   1with energy     -383.3118404911

  ### active excitation selection ###

 Inactive orbitals:                     1
    there are      468 all-active excitations of which    468 are references.

    the    468 reference all-active excitation csfs

    ------------------------------------------------------------------------------------------
         1     2     3     4     5     6     7     8     9    10    11    12    13    14    15
    ------------------------------------------------------------------------------------------
  2:     2     2     2     2     2     2     2     2     2     2     2     2     2     2     2
  3:     2     2     2     2     2     2     2     2     2     2     2     2     2     2     2
  4:     2     2     2     2     2     2     2     1     1     1     1     1     1     1     1
  5:     2     0     0     0     0     0     0    -1    -1    -1    -1    -1    -1     1     1
  6:     0     2     1     0     0     0     0     2     1     0     0     0     0    -1     0
  7:     0     0    -1     2     0     0     0     0    -1     2     0     0     0    -1     0
  8:     0     0     0     0     2     1     0     0     0     0     2     1     0     0    -1
  9:     0     0     0     0     0    -1     2     0     0     0     0    -1     2     0    -1
    ------------------------------------------------------------------------------------------
        16    17    18    19    20    21    22    23    24    25    26    27    28    29    30
    ------------------------------------------------------------------------------------------
  2:     2     2     2     2     2     2     2     2     2     2     2     2     2     2     2
  3:     2     2     2     2     2     2     2     2     2     2     2     2     2     2     2
  4:     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0
  5:     2     2     2     2     2     2     0     0     0     0     0     0     0     0     0
  6:     2     1     0     0     0     0     2     2     2     2     1     1     1     1     0
  7:     0    -1     2     0     0     0     2     0     0     0    -1    -1    -1     1     2
  8:     0     0     0     2     1     0     0     2     1     0     2     1     0    -1     2
  9:     0     0     0     0    -1     2     0     0    -1     2     0    -1     2    -1     0
    ------------------------------------------------------------------------------------------
        31    32    33    34    35    36    37    38    39    40    41    42    43    44    45
    ------------------------------------------------------------------------------------------
  2:     2     2     2     2     2     2     2     2     2     2     2     2     2     2     2
  3:     2     2     2     1     1     1     1     1     1     1     1     1     1     1     1
  4:     0     0     0     2     2     2     2     2     2     2     2    -1    -1    -1    -1
  5:     0     0     0    -1    -1    -1    -1     1     1     1     1     2     2     2     2
  6:     0     0     0     1     1     0     0    -1    -1     0     0     1     1     0     0
  7:     2     2     0     0     0     1     1     0     0    -1    -1     0     0     1     1
  8:     1     0     2    -1     0    -1     0    -1     0    -1     0    -1     0    -1     0
  9:    -1     2     2     0    -1     0    -1     0    -1     0    -1     0    -1     0    -1
    ------------------------------------------------------------------------------------------
        46    47    48    49    50    51    52    53    54    55    56    57    58    59    60
    ------------------------------------------------------------------------------------------
  2:     2     2     2     2     2     2     2     2     2     2     2     2     2     2     2
  3:     1     1     1     1     1     1     1     1     1     1     1     1     1     1     1
  4:    -1    -1    -1    -1    -1    -1    -1    -1     1     1     1     1     1     1     1
  5:     0     0     0     0     0     0     0     0     2     2     2     2     0     0     0
  6:     2     2     1     1     1     1     0     0    -1    -1     0     0     2     2    -1
  7:     1     1     2     2     0     0     1     1     0     0    -1    -1    -1    -1     2
  8:    -1     0    -1     0     2    -1     2    -1    -1     0    -1     0    -1     0    -1
  9:     0    -1     0    -1    -1     2    -1     2     0    -1     0    -1     0    -1     0
    ------------------------------------------------------------------------------------------
        61    62    63    64    65    66    67    68    69    70    71    72    73    74    75
    ------------------------------------------------------------------------------------------
  2:     2     2     2     2     2     2     2     2     2     2     2     2     2     2     2
  3:     1     1     1     1     1     1     1     1     1     1     1     1     1     1     1
  4:     1     1     1     1     1     0     0     0     0     0     0     0     0     0     0
  5:     0     0     0     0     0    -1    -1    -1    -1    -1    -1    -1    -1     1     1
  6:    -1    -1    -1     0     0     2     2     1     1     1     1     0     0     2     2
  7:     2     0     0    -1    -1     1     1     2     2     0     0     1     1    -1    -1
  8:     0     2    -1     2    -1    -1     0    -1     0     2    -1     2    -1    -1     0
  9:    -1    -1     2    -1     2     0    -1     0    -1    -1     2    -1     2     0    -1
    ------------------------------------------------------------------------------------------
        76    77    78    79    80    81    82    83    84    85    86    87    88    89    90
    ------------------------------------------------------------------------------------------
  2:     2     2     2     2     2     2     2     2     2     2     2     2     2     2     2
  3:     1     1     1     1     1     1     0     0     0     0     0     0     0     0     0
  4:     0     0     0     0     0     0     2     2     2     2     2     2     2     2     2
  5:     1     1     1     1     1     1     2     2     2     2     2     2     0     0     0
  6:    -1    -1    -1    -1     0     0     2     1     0     0     0     0     2     2     2
  7:     2     2     0     0    -1    -1     0    -1     2     0     0     0     2     0     0
  8:    -1     0     2    -1     2    -1     0     0     0     2     1     0     0     2     1
  9:     0    -1    -1     2    -1     2     0     0     0     0    -1     2     0     0    -1
    ------------------------------------------------------------------------------------------
        91    92    93    94    95    96    97    98    99   100   101   102   103   104   105
    ------------------------------------------------------------------------------------------
  2:     2     2     2     2     2     2     2     2     2     2     2     2     2     2     2
  3:     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0
  4:     2     2     2     2     2     2     2     2     2     1     1     1     1     1     1
  5:     0     0     0     0     0     0     0     0     0    -1    -1    -1    -1    -1    -1
  6:     2     1     1     1     1     0     0     0     0     2     2     2     2     1     1
  7:     0    -1    -1    -1     1     2     2     2     0     2     0     0     0    -1    -1
  8:     0     2     1     0    -1     2     1     0     2     0     2     1     0     2     1
  9:     2     0    -1     2    -1     0    -1     2     2     0     0    -1     2     0    -1
    ------------------------------------------------------------------------------------------
       106   107   108   109   110   111   112   113   114   115   116   117   118   119   120
    ------------------------------------------------------------------------------------------
  2:     2     2     2     2     2     2     2     2     2     2     2     2     2     2     2
  3:     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0
  4:     1     1     1     1     1     1     1     1     1     1     1     1     1     0     0
  5:    -1    -1    -1    -1    -1    -1     1     1     1     1     1     1     1     2     2
  6:     1     1     0     0     0     0     2    -1    -1    -1    -1     1     0     2     2
  7:    -1     1     2     2     2     0     0    -1    -1    -1     1    -1     2     2     0
  8:     0    -1     2     1     0     2    -1     2     1     0    -1    -1    -1     0     2
  9:     2    -1     0    -1     2     2    -1     0    -1     2    -1    -1    -1     0     0
    ------------------------------------------------------------------------------------------
       121   122   123   124   125   126   127   128   129   130   131   132   133   134   135
    ------------------------------------------------------------------------------------------
  2:     2     2     2     2     2     2     2     2     2     2     2     2     2     2     2
  3:     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0
  4:     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0
  5:     2     2     2     2     2     2     2     2     2     2     0     0     0     0     0
  6:     2     2     1     1     1     1     0     0     0     0     2     2     2     2     1
  7:     0     0    -1    -1    -1     1     2     2     2     0     2     2     2     0    -1
  8:     1     0     2     1     0    -1     2     1     0     2     2     1     0     2     2
  9:    -1     2     0    -1     2    -1     0    -1     2     2     0    -1     2     2     2
    ------------------------------------------------------------------------------------------
       136   137   138   139   140   141   142   143   144   145   146   147   148   149   150
    ------------------------------------------------------------------------------------------
  2:     2     1     1     1     1     1     1     1     1     1     1     1     1     1     1
  3:     0     2     2     2     2     2     2     2     2     2     2     2     2     2     2
  4:     0     2     2     2     2     2     2     2     2    -1    -1    -1    -1    -1    -1
  5:     0    -1    -1    -1    -1     1     1     1     1     2     2     2     2     0     0
  6:     0     1     1     0     0    -1    -1     0     0     1     1     0     0     2     2
  7:     2     0     0     1     1     0     0    -1    -1     0     0     1     1     1     1
  8:     2    -1     0    -1     0    -1     0    -1     0    -1     0    -1     0    -1     0
  9:     2     0    -1     0    -1     0    -1     0    -1     0    -1     0    -1     0    -1
    ------------------------------------------------------------------------------------------
       151   152   153   154   155   156   157   158   159   160   161   162   163   164   165
    ------------------------------------------------------------------------------------------
  2:     1     1     1     1     1     1     1     1     1     1     1     1     1     1     1
  3:     2     2     2     2     2     2     2     2     2     2     2     2     2     2     2
  4:    -1    -1    -1    -1    -1    -1     1     1     1     1     1     1     1     1     1
  5:     0     0     0     0     0     0     2     2     2     2     0     0     0     0     0
  6:     1     1     1     1     0     0    -1    -1     0     0     2     2    -1    -1    -1
  7:     2     2     0     0     1     1     0     0    -1    -1    -1    -1     2     2     0
  8:    -1     0     2    -1     2    -1    -1     0    -1     0    -1     0    -1     0     2
  9:     0    -1    -1     2    -1     2     0    -1     0    -1     0    -1     0    -1    -1
    ------------------------------------------------------------------------------------------
       166   167   168   169   170   171   172   173   174   175   176   177   178   179   180
    ------------------------------------------------------------------------------------------
  2:     1     1     1     1     1     1     1     1     1     1     1     1     1     1     1
  3:     2     2     2     2     2     2     2     2     2     2     2     2     2     2     2
  4:     1     1     1     0     0     0     0     0     0     0     0     0     0     0     0
  5:     0     0     0    -1    -1    -1    -1    -1    -1    -1    -1     1     1     1     1
  6:    -1     0     0     2     2     1     1     1     1     0     0     2     2    -1    -1
  7:     0    -1    -1     1     1     2     2     0     0     1     1    -1    -1     2     2
  8:    -1     2    -1    -1     0    -1     0     2    -1     2    -1    -1     0    -1     0
  9:     2    -1     2     0    -1     0    -1    -1     2    -1     2     0    -1     0    -1
    ------------------------------------------------------------------------------------------
       181   182   183   184   185   186   187   188   189   190   191   192   193   194   195
    ------------------------------------------------------------------------------------------
  2:     1     1     1     1     1     1     1     1     1     1     1     1     1     1     1
  3:     2     2     2     2    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1
  4:     0     0     0     0     2     2     2     2     2     2     2     2     2     2     2
  5:     1     1     1     1     2     2     2     2     2     2     0     0     0     0     0
  6:    -1    -1     0     0     2     1     0     0     0     0     2     2     2     2     1
  7:     0     0    -1    -1     0    -1     2     0     0     0     2     0     0     0    -1
  8:     2    -1     2    -1     0     0     0     2     1     0     0     2     1     0     2
  9:    -1     2    -1     2     0     0     0     0    -1     2     0     0    -1     2     0
    ------------------------------------------------------------------------------------------
       196   197   198   199   200   201   202   203   204   205   206   207   208   209   210
    ------------------------------------------------------------------------------------------
  2:     1     1     1     1     1     1     1     1     1     1     1     1     1     1     1
  3:    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1
  4:     2     2     2     2     2     2     2     1     1     1     1     1     1     1     1
  5:     0     0     0     0     0     0     0    -1    -1    -1    -1    -1    -1    -1    -1
  6:     1     1     1     0     0     0     0     2     2     2     2     1     1     1     1
  7:    -1    -1     1     2     2     2     0     2     0     0     0    -1    -1    -1     1
  8:     1     0    -1     2     1     0     2     0     2     1     0     2     1     0    -1
  9:    -1     2    -1     0    -1     2     2     0     0    -1     2     0    -1     2    -1
    ------------------------------------------------------------------------------------------
       211   212   213   214   215   216   217   218   219   220   221   222   223   224   225
    ------------------------------------------------------------------------------------------
  2:     1     1     1     1     1     1     1     1     1     1     1     1     1     1     1
  3:    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1
  4:     1     1     1     1     1     1     1     1     1     1     1     0     0     0     0
  5:    -1    -1    -1    -1     1     1     1     1     1     1     1     2     2     2     2
  6:     0     0     0     0     2    -1    -1    -1    -1     1     0     2     2     2     2
  7:     2     2     2     0     0    -1    -1    -1     1    -1     2     2     0     0     0
  8:     2     1     0     2    -1     2     1     0    -1    -1    -1     0     2     1     0
  9:     0    -1     2     2    -1     0    -1     2    -1    -1    -1     0     0    -1     2
    ------------------------------------------------------------------------------------------
       226   227   228   229   230   231   232   233   234   235   236   237   238   239   240
    ------------------------------------------------------------------------------------------
  2:     1     1     1     1     1     1     1     1     1     1     1     1     1     1     1
  3:    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1     1
  4:     0     0     0     0     0     0     0     0     0     0     0     0     0     0     2
  5:     2     2     2     2     2     2     2     2     0     0     0     0     0     0     2
  6:     1     1     1     1     0     0     0     0     2     2     2     2     1     0    -1
  7:    -1    -1    -1     1     2     2     2     0     2     2     2     0    -1     2    -1
  8:     2     1     0    -1     2     1     0     2     2     1     0     2     2     2     0
  9:     0    -1     2    -1     0    -1     2     2     0    -1     2     2     2     2     0
    ------------------------------------------------------------------------------------------
       241   242   243   244   245   246   247   248   249   250   251   252   253   254   255
    ------------------------------------------------------------------------------------------
  2:     1     1     1     1     1     1     1     1     1     1     1     1     1     1     1
  3:     1     1     1     1     1     1     1     1     1     1     1     1     1     1     1
  4:     2     2     2     2     2     2     2     2    -1    -1    -1    -1    -1    -1    -1
  5:     2     0     0     0     0     0     0     0    -1    -1    -1    -1    -1    -1    -1
  6:     0     2    -1    -1    -1    -1     1     0     2     2     2     2     1     1     1
  7:     0     0    -1    -1    -1     1    -1     2     2     0     0     0    -1    -1    -1
  8:    -1    -1     2     1     0    -1    -1    -1     0     2     1     0     2     1     0
  9:    -1    -1     0    -1     2    -1    -1    -1     0     0    -1     2     0    -1     2
    ------------------------------------------------------------------------------------------
       256   257   258   259   260   261   262   263   264   265   266   267   268   269   270
    ------------------------------------------------------------------------------------------
  2:     1     1     1     1     1     1     1     1     1     1     1     1     1     1     1
  3:     1     1     1     1     1     1     1     1     1     1     1     1     1     1     1
  4:    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1     1     1     1
  5:    -1    -1    -1    -1    -1     1     1     1     1     1     1     1    -1    -1    -1
  6:     1     0     0     0     0     2    -1    -1    -1    -1     1     0     2    -1    -1
  7:     1     2     2     2     0     0    -1    -1    -1     1    -1     2     0    -1    -1
  8:    -1     2     1     0     2    -1     2     1     0    -1    -1    -1    -1     2     1
  9:    -1     0    -1     2     2    -1     0    -1     2    -1    -1    -1    -1     0    -1
    ------------------------------------------------------------------------------------------
       271   272   273   274   275   276   277   278   279   280   281   282   283   284   285
    ------------------------------------------------------------------------------------------
  2:     1     1     1     1     1     1     1     1     1     1     1     1     1     1     1
  3:     1     1     1     1     1     1     1     1     1     1     1     1     1     1     0
  4:     1     1     1     1     1     0     0     0     0     0     0     0     0     0     2
  5:    -1    -1    -1    -1     1     2     2     2     2     2     2     2     0     0    -1
  6:    -1    -1     1     0    -1     2    -1    -1    -1    -1     1     0     2    -1     2
  7:    -1     1    -1     2    -1     0    -1    -1    -1     1    -1     2     2    -1     1
  8:     0    -1    -1    -1    -1    -1     2     1     0    -1    -1    -1    -1     2    -1
  9:     2    -1    -1    -1    -1    -1     0    -1     2    -1    -1    -1    -1     2     0
    ------------------------------------------------------------------------------------------
       286   287   288   289   290   291   292   293   294   295   296   297   298   299   300
    ------------------------------------------------------------------------------------------
  2:     1     1     1     1     1     1     1     1     1     1     1     1     1     1     1
  3:     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0
  4:     2     2     2     2     2     2     2     2     2     2     2     2     2     2     2
  5:    -1    -1    -1    -1    -1    -1    -1     1     1     1     1     1     1     1     1
  6:     2     1     1     1     1     0     0     2     2    -1    -1    -1    -1     0     0
  7:     1     2     2     0     0     1     1    -1    -1     2     2     0     0    -1    -1
  8:     0    -1     0     2    -1     2    -1    -1     0    -1     0     2    -1     2    -1
  9:    -1     0    -1    -1     2    -1     2     0    -1     0    -1    -1     2    -1     2
    ------------------------------------------------------------------------------------------
       301   302   303   304   305   306   307   308   309   310   311   312   313   314   315
    ------------------------------------------------------------------------------------------
  2:     1     1     1     1     1     1     1     1     1     1     1     1     1     1     1
  3:     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0
  4:    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1     1     1     1
  5:     2     2     2     2     2     2     2     2     0     0     0     0     2     2     2
  6:     2     2     1     1     1     1     0     0     2     2     1     1     2     2    -1
  7:     1     1     2     2     0     0     1     1     1     1     2     2    -1    -1     2
  8:    -1     0    -1     0     2    -1     2    -1     2    -1     2    -1    -1     0    -1
  9:     0    -1     0    -1    -1     2    -1     2    -1     2    -1     2     0    -1     0
    ------------------------------------------------------------------------------------------
       316   317   318   319   320   321   322   323   324   325   326   327   328   329   330
    ------------------------------------------------------------------------------------------
  2:     1     1     1     1     1     1     1     1     1     1     1     1     1     1     1
  3:     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0
  4:     1     1     1     1     1     1     1     1     1     0     0     0     0     0     0
  5:     2     2     2     2     2     0     0     0     0    -1    -1    -1    -1     1     1
  6:    -1    -1    -1     0     0     2     2    -1    -1     2     2     1     1     2     2
  7:     2     0     0    -1    -1    -1    -1     2     2     1     1     2     2    -1    -1
  8:     0     2    -1     2    -1     2    -1     2    -1     2    -1     2    -1     2    -1
  9:    -1    -1     2    -1     2    -1     2    -1     2    -1     2    -1     2    -1     2
    ------------------------------------------------------------------------------------------
       331   332   333   334   335   336   337   338   339   340   341   342   343   344   345
    ------------------------------------------------------------------------------------------
  2:     1     1     0     0     0     0     0     0     0     0     0     0     0     0     0
  3:     0     0     2     2     2     2     2     2     2     2     2     2     2     2     2
  4:     0     0     2     2     2     2     2     2     2     2     2     2     2     2     2
  5:     1     1     2     2     2     2     2     2     0     0     0     0     0     0     0
  6:    -1    -1     2     1     0     0     0     0     2     2     2     2     1     1     1
  7:     2     2     0    -1     2     0     0     0     2     0     0     0    -1    -1    -1
  8:     2    -1     0     0     0     2     1     0     0     2     1     0     2     1     0
  9:    -1     2     0     0     0     0    -1     2     0     0    -1     2     0    -1     2
    ------------------------------------------------------------------------------------------
       346   347   348   349   350   351   352   353   354   355   356   357   358   359   360
    ------------------------------------------------------------------------------------------
  2:     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0
  3:     2     2     2     2     2     2     2     2     2     2     2     2     2     2     2
  4:     2     2     2     2     2     1     1     1     1     1     1     1     1     1     1
  5:     0     0     0     0     0    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1
  6:     1     0     0     0     0     2     2     2     2     1     1     1     1     0     0
  7:     1     2     2     2     0     2     0     0     0    -1    -1    -1     1     2     2
  8:    -1     2     1     0     2     0     2     1     0     2     1     0    -1     2     1
  9:    -1     0    -1     2     2     0     0    -1     2     0    -1     2    -1     0    -1
    ------------------------------------------------------------------------------------------
       361   362   363   364   365   366   367   368   369   370   371   372   373   374   375
    ------------------------------------------------------------------------------------------
  2:     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0
  3:     2     2     2     2     2     2     2     2     2     2     2     2     2     2     2
  4:     1     1     1     1     1     1     1     1     1     0     0     0     0     0     0
  5:    -1    -1     1     1     1     1     1     1     1     2     2     2     2     2     2
  6:     0     0     2    -1    -1    -1    -1     1     0     2     2     2     2     1     1
  7:     2     0     0    -1    -1    -1     1    -1     2     2     0     0     0    -1    -1
  8:     0     2    -1     2     1     0    -1    -1    -1     0     2     1     0     2     1
  9:     2     2    -1     0    -1     2    -1    -1    -1     0     0    -1     2     0    -1
    ------------------------------------------------------------------------------------------
       376   377   378   379   380   381   382   383   384   385   386   387   388   389   390
    ------------------------------------------------------------------------------------------
  2:     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0
  3:     2     2     2     2     2     2     2     2     2     2     2     2     1     1     1
  4:     0     0     0     0     0     0     0     0     0     0     0     0     2     2     2
  5:     2     2     2     2     2     2     0     0     0     0     0     0    -1    -1    -1
  6:     1     1     0     0     0     0     2     2     2     2     1     0     2     2     1
  7:    -1     1     2     2     2     0     2     2     2     0    -1     2     1     1     2
  8:     0    -1     2     1     0     2     2     1     0     2     2     2    -1     0    -1
  9:     2    -1     0    -1     2     2     0    -1     2     2     2     2     0    -1     0
    ------------------------------------------------------------------------------------------
       391   392   393   394   395   396   397   398   399   400   401   402   403   404   405
    ------------------------------------------------------------------------------------------
  2:     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0
  3:     1     1     1     1     1     1     1     1     1     1     1     1     1     1     1
  4:     2     2     2     2     2     2     2     2     2     2     2     2     2    -1    -1
  5:    -1    -1    -1    -1    -1     1     1     1     1     1     1     1     1     2     2
  6:     1     1     1     0     0     2     2    -1    -1    -1    -1     0     0     2     2
  7:     2     0     0     1     1    -1    -1     2     2     0     0    -1    -1     1     1
  8:     0     2    -1     2    -1    -1     0    -1     0     2    -1     2    -1    -1     0
  9:    -1    -1     2    -1     2     0    -1     0    -1    -1     2    -1     2     0    -1
    ------------------------------------------------------------------------------------------
       406   407   408   409   410   411   412   413   414   415   416   417   418   419   420
    ------------------------------------------------------------------------------------------
  2:     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0
  3:     1     1     1     1     1     1     1     1     1     1     1     1     1     1     1
  4:    -1    -1    -1    -1    -1    -1    -1    -1    -1    -1     1     1     1     1     1
  5:     2     2     2     2     2     2     0     0     0     0     2     2     2     2     2
  6:     1     1     1     1     0     0     2     2     1     1     2     2    -1    -1    -1
  7:     2     2     0     0     1     1     1     1     2     2    -1    -1     2     2     0
  8:    -1     0     2    -1     2    -1     2    -1     2    -1    -1     0    -1     0     2
  9:     0    -1    -1     2    -1     2    -1     2    -1     2     0    -1     0    -1    -1
    ------------------------------------------------------------------------------------------
       421   422   423   424   425   426   427   428   429   430   431   432   433   434   435
    ------------------------------------------------------------------------------------------
  2:     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0
  3:     1     1     1     1     1     1     1     1     1     1     1     1     1     1     1
  4:     1     1     1     1     1     1     1     0     0     0     0     0     0     0     0
  5:     2     2     2     0     0     0     0    -1    -1    -1    -1     1     1     1     1
  6:    -1     0     0     2     2    -1    -1     2     2     1     1     2     2    -1    -1
  7:     0    -1    -1    -1    -1     2     2     1     1     2     2    -1    -1     2     2
  8:    -1     2    -1     2    -1     2    -1     2    -1     2    -1     2    -1     2    -1
  9:     2    -1     2    -1     2    -1     2    -1     2    -1     2    -1     2    -1     2
    ------------------------------------------------------------------------------------------
       436   437   438   439   440   441   442   443   444   445   446   447   448   449   450
    ------------------------------------------------------------------------------------------
  2:     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0
  3:     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0
  4:     2     2     2     2     2     2     2     2     2     2     2     2     2     2     2
  5:     2     2     2     2     2     2     2     2     2     2     2     2     0     0     0
  6:     2     2     2     2     1     1     1     1     0     0     0     0     2     2     2
  7:     2     0     0     0    -1    -1    -1     1     2     2     2     0     2     2     2
  8:     0     2     1     0     2     1     0    -1     2     1     0     2     2     1     0
  9:     0     0    -1     2     0    -1     2    -1     0    -1     2     2     0    -1     2
    ------------------------------------------------------------------------------------------
       451   452   453   454   455   456   457   458   459   460   461   462   463   464   465
    ------------------------------------------------------------------------------------------
  2:     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0
  3:     0     0     0     0     0     0     0     0     0     0     0     0     0     0     0
  4:     2     2     2     1     1     1     1     1     1     1     1     0     0     0     0
  5:     0     0     0    -1    -1    -1    -1    -1    -1     1     1     2     2     2     2
  6:     2     1     0     2     2     2     2     1     0     2    -1     2     2     2     2
  7:     0    -1     2     2     2     2     0    -1     2     2    -1     2     2     2     0
  8:     2     2     2     2     1     0     2     2     2    -1     2     2     1     0     2
  9:     2     2     2     0    -1     2     2     2     2    -1     2     0    -1     2     2
    ------------------
       466   467   468
    ------------------
  2:     0     0     0
  3:     0     0     0
  4:     0     0     0
  5:     2     2     0
  6:     1     0     2
  7:    -1     2     2
  8:     2     2     2
  9:     2     2     2

  ### end active excitation selection ###


 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:            142028
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                   20
 residual norm convergence criteria:               0.001000

          starting ci iteration   1

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    4.158917
ci vector #   2dasum_wr=    0.000000
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:      412118 2x:      100824 4x:       16200
All internal counts: zz :      141469 yy:     1199860 xx:      733974 ww:      718502
One-external counts: yz :      664107 yx:     1998598 yw:     2016328
Two-external counts: yy :      580878 ww:      224232 xx:      351894 xz:       37085 wz:       33077 wx:      512216
Three-ext.   counts: yx :      139509 yw:      144149

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:       52777    task #     2:       40747    task #     3:      332519    task #     4:      166259
task #     5:      510932    task #     6:     1507769    task #     7:     1513804    task #     8:      112690
task #     9:     1292631    task #    10:      225883    task #    11:      225883    task #    12:      126547
task #    13:      126547    task #    14:       63273    task #    15:       63275    task #    16:       63275
task #    17:       20226    task #    18:      147443    task #    19:       51613    task #    20:       54916
 Overlap matrix (sovlci)

              sovl   1
 sovl   1     1.00000000
 Overlap matrix (active-active) sovlaa

              sovl   1
 sovl   1     1.00000000
 Overlap matrix (critical) sovlcrit

              sovl   1
 Final Overlap matrix sovl

              sovl   1
 sovl   1     0.00000000
 Subspace hamiltonian (htci) 

              htci   1
 htci   1    -8.82741298
 Subspace hamiltonian (active-active) 

              htaa   1
 htaa   1    -8.82741298
 Subspace hamiltonian (critical) 

              htcr   1
 scale factor: (eold-eref)*(1-gvalue):  0.000000000000000E+000 *
  0.622222222222222      =  0.000000000000000E+000
 Final subspace hamiltonian 

                ht   1
   ht   1    -8.82741298
Spectrum of overlapmatrix:    1.000000
calca4: root=   1 anorm**2=  0.00000000 scale:  1.00000000

          calcsovref: tciref block   1

              civs   1
 refs   1    1.00000    

          calcsovref: scrb block   1

                ci   1
 civs   1    1.00000    

          calcsovref: sovref block   1

              v      1
 ref    1    1.00000    

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000
 NCSF=                  1368                 23810                 51154
                 65696
 fact= -2.984279490192421E-014
 factd= -2.984279490192421E-014

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  1  1   -383.3118404911  4.7962E-14  3.9848E-02  2.4511E-01  1.0000E-03
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.02     0.01     0.00     0.02         0.    0.0528
    2   25    0     0.01     0.01     0.00     0.01         0.    0.0407
    3   26    0     0.29     0.07     0.00     0.29         0.    0.3325
    4   27    0     0.28     0.08     0.00     0.28         0.    0.1663
    5   11    0     0.36     0.28     0.00     0.36         0.    0.5109
    6   15    0     1.15     0.62     0.00     1.15         0.    1.5078
    7   16    0     1.17     0.73     0.00     1.17         0.    1.5138
    8    1    0     0.08     0.07     0.00     0.08         0.    0.1127
    9    5    0     0.87     0.64     0.00     0.87         0.    1.2926
   10    6    0     0.38     0.19     0.00     0.38         0.    0.2259
   11   18    0     0.36     0.20     0.00     0.36         0.    0.2259
   12    7    0     0.31     0.17     0.00     0.31         0.    0.1265
   13   19    0     0.29     0.19     0.00     0.29         0.    0.1265
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0633
   15   43    0     0.01     0.00     0.00     0.01         0.    0.0633
   16   44    0     0.01     0.00     0.00     0.01         0.    0.0633
   17   75    0     0.01     0.01     0.00     0.01         0.    0.0202
   18   76    0     0.06     0.06     0.00     0.06         0.    0.1474
   19   77    0     0.02     0.02     0.00     0.02         0.    0.0516
   20   78    0     0.03     0.11     0.00     0.03         0.    0.0075
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.110000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     5.7100s 
time spent in multnx:                   5.7100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       3.4600s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            5.8200s 

          starting ci iteration   2

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.545333
ci vector #   2dasum_wr=    3.045226
ci vector #   3dasum_wr=    0.423783
ci vector #   4dasum_wr=    2.653283


====================================================================================================
Diagonal     counts:  0x:      412118 2x:      100824 4x:       16200
All internal counts: zz :      141469 yy:     1199860 xx:      733974 ww:      718502
One-external counts: yz :      664107 yx:     1998598 yw:     2016328
Two-external counts: yy :      580878 ww:      224232 xx:      351894 xz:       37085 wz:       33077 wx:      512216
Three-ext.   counts: yx :      139509 yw:      144149

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:       52777    task #     2:       40747    task #     3:      332519    task #     4:      166259
task #     5:      510932    task #     6:     1507769    task #     7:     1513804    task #     8:      112690
task #     9:     1292631    task #    10:      225883    task #    11:      225883    task #    12:      126547
task #    13:      126547    task #    14:       63273    task #    15:       63275    task #    16:       63275
task #    17:       20226    task #    18:      147443    task #    19:       51613    task #    20:       54916
 Overlap matrix (sovlci)

              sovl   1       sovl   2
 sovl   1     1.00000000
 sovl   2     0.00000000     0.02954206
 Overlap matrix (active-active) sovlaa

              sovl   1       sovl   2
 sovl   1     1.00000000
 sovl   2     0.00000000     0.00000000
 Overlap matrix (critical) sovlcrit

              sovl   1       sovl   2
 Final Overlap matrix sovl

              sovl   1       sovl   2
 sovl   1     0.00000000
 sovl   2     0.00000000     0.02954206
 Subspace hamiltonian (htci) 

              htci   1       htci   2
 htci   1    -8.82741298
 htci   2    -0.03984773    -0.22745430
 Subspace hamiltonian (active-active) 

              htaa   1       htaa   2
 htaa   1    -8.82741298
 htaa   2     0.00000000     0.00000000
 Subspace hamiltonian (critical) 

              htcr   1       htcr   2
 scale factor: (eold-eref)*(1-gvalue): -4.796163466380676E-014 *
  0.622222222222222      = -2.984279490192421E-014
 Final subspace hamiltonian 

                ht   1         ht   2
   ht   1    -8.82741298
   ht   2    -0.03984773    -0.22745430
Spectrum of overlapmatrix:    0.029542    1.000000
calca4: root=   1 anorm**2=  0.03754172 scale:  1.01859792
calca4: root=   2 anorm**2=  0.96245828 scale:  1.40087768

          calcsovref: tciref block   1

              civs   1       civs   2
 refs   1    1.00000      -2.287966E-14

          calcsovref: scrb block   1

                ci   1         ci   2
 civs   1  -0.981050       0.193757    
 civs   2   -1.12729       -5.70782    

          calcsovref: sovref block   1

              v      1       v      2
 ref    1  -0.981050       0.193757    

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1    -0.98104958     0.19375686
 NCSF=                  1368                 23810                 51154
                 65696
 fact= -2.849015914006906E-002
 factd= -2.849015914006906E-002

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  2  1   -383.3576282468  4.5788E-02  4.9645E-03  8.8187E-02  1.0000E-03
 mraqcc  #  2  2   -382.1379783599  7.6536E+00  0.0000E+00  5.7557E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.02     0.02     0.00     0.02         0.    0.0528
    2   25    0     0.02     0.01     0.00     0.02         0.    0.0407
    3   26    0     0.28     0.08     0.00     0.28         0.    0.3325
    4   27    0     0.28     0.06     0.00     0.28         0.    0.1663
    5   11    0     0.37     0.32     0.00     0.37         0.    0.5109
    6   15    0     1.18     0.65     0.01     1.17         0.    1.5078
    7   16    0     1.20     0.67     0.00     1.20         0.    1.5138
    8    1    0     0.08     0.07     0.00     0.08         0.    0.1127
    9    5    0     0.87     0.62     0.00     0.87         0.    1.2926
   10    6    0     0.38     0.19     0.00     0.38         0.    0.2259
   11   18    0     0.37     0.20     0.00     0.37         0.    0.2259
   12    7    0     0.30     0.17     0.00     0.30         0.    0.1265
   13   19    0     0.29     0.19     0.00     0.29         0.    0.1265
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0633
   15   43    0     0.01     0.00     0.00     0.01         0.    0.0633
   16   44    0     0.01     0.00     0.00     0.01         0.    0.0633
   17   75    0     0.01     0.01     0.00     0.01         0.    0.0202
   18   76    0     0.05     0.05     0.00     0.05         0.    0.1474
   19   77    0     0.03     0.02     0.00     0.03         0.    0.0516
   20   78    0     0.02     0.11     0.00     0.02         0.    0.0075
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.110000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     5.7700s 
time spent in multnx:                   5.7600s 
integral transfer time:                 0.0100s 
time spent for loop construction:       3.4400s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            5.8900s 

          starting ci iteration   3

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.494557
ci vector #   2dasum_wr=    1.634813
ci vector #   3dasum_wr=    0.510987
ci vector #   4dasum_wr=    1.344407


====================================================================================================
Diagonal     counts:  0x:      412118 2x:      100824 4x:       16200
All internal counts: zz :      141469 yy:     1199860 xx:      733974 ww:      718502
One-external counts: yz :      664107 yx:     1998598 yw:     2016328
Two-external counts: yy :      580878 ww:      224232 xx:      351894 xz:       37085 wz:       33077 wx:      512216
Three-ext.   counts: yx :      139509 yw:      144149

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:       52777    task #     2:       40747    task #     3:      332519    task #     4:      166259
task #     5:      510932    task #     6:     1507769    task #     7:     1513804    task #     8:      112690
task #     9:     1292631    task #    10:      225883    task #    11:      225883    task #    12:      126547
task #    13:      126547    task #    14:       63273    task #    15:       63275    task #    16:       63275
task #    17:       20226    task #    18:      147443    task #    19:       51613    task #    20:       54916
 Overlap matrix (sovlci)

              sovl   1       sovl   2       sovl   3
 sovl   1     1.00000000
 sovl   2     0.00000000     0.02954206
 sovl   3    -0.01688510    -0.00040916     0.00403830
 Overlap matrix (active-active) sovlaa

              sovl   1       sovl   2       sovl   3
 sovl   1     1.00000000
 sovl   2     0.00000000     0.00000000
 sovl   3    -0.01688510     0.00000000     0.00094921
 Overlap matrix (critical) sovlcrit

              sovl   1       sovl   2       sovl   3
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3
 sovl   1     0.00000000
 sovl   2     0.00000000     0.02954206
 sovl   3     0.00000000    -0.00040916     0.00308909
 Subspace hamiltonian (htci) 

              htci   1       htci   2       htci   3
 htci   1    -8.82741298
 htci   2    -0.03984773    -0.22745430
 htci   3     0.14999346     0.00787611    -0.03070984
 Subspace hamiltonian (active-active) 

              htaa   1       htaa   2       htaa   3
 htaa   1    -8.82741298
 htaa   2     0.00000000     0.00000000
 htaa   3    -0.00770300    -0.00770300    -0.00770300
 Subspace hamiltonian (critical) 

              htcr   1       htcr   2       htcr   3
 scale factor: (eold-eref)*(1-gvalue): -4.578775576082528E-002 *
  0.622222222222222      = -2.849015914006906E-002
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3
   ht   1    -8.82741298
   ht   2    -0.03984773    -0.22829596
   ht   3     0.14999346     0.00788777    -0.03079785
Spectrum of overlapmatrix:    0.003746    0.029549    1.000286
calca4: root=   1 anorm**2=  0.05092038 scale:  1.02514408
calca4: root=   2 anorm**2=  0.88886102 scale:  1.37435840
calca4: root=   3 anorm**2=  0.88300815 scale:  1.37222744

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3
 refs   1    1.00000      -2.287966E-14  -1.688510E-02

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3
 civs   1  -0.955856      -0.380258       0.133494    
 civs   2   -1.25235        4.29294        3.72874    
 civs   3    1.06395       -10.0058        12.8683    

          calcsovref: sovref block   1

              v      1       v      2       v      3
 ref    1  -0.973821      -0.211309      -8.378906E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1    -0.97382127    -0.21130907    -0.08378906
 NCSF=                  1368                 23810                 51154
                 65696
 fact= -3.252586323716160E-002
 factd= -3.252586323716160E-002

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  3  1   -383.3641141998  6.4860E-03  1.2270E-03  4.4023E-02  1.0000E-03
 mraqcc  #  3  2   -382.4577045751  3.1973E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  3  3   -381.6831853960  7.1988E+00  0.0000E+00  3.4593E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.02     0.02     0.00     0.02         0.    0.0528
    2   25    0     0.02     0.01     0.00     0.02         0.    0.0407
    3   26    0     0.28     0.06     0.00     0.28         0.    0.3325
    4   27    0     0.28     0.07     0.00     0.28         0.    0.1663
    5   11    0     0.37     0.32     0.00     0.37         0.    0.5109
    6   15    0     1.17     0.71     0.00     1.17         0.    1.5078
    7   16    0     1.20     0.75     0.00     1.20         0.    1.5138
    8    1    0     0.09     0.07     0.00     0.09         0.    0.1127
    9    5    0     0.86     0.58     0.00     0.86         0.    1.2926
   10    6    0     0.38     0.22     0.00     0.38         0.    0.2259
   11   18    0     0.37     0.21     0.00     0.37         0.    0.2259
   12    7    0     0.30     0.17     0.00     0.30         0.    0.1265
   13   19    0     0.29     0.18     0.00     0.29         0.    0.1265
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0633
   15   43    0     0.01     0.00     0.00     0.01         0.    0.0633
   16   44    0     0.02     0.00     0.00     0.02         0.    0.0633
   17   75    0     0.01     0.01     0.00     0.01         0.    0.0202
   18   76    0     0.05     0.04     0.00     0.05         0.    0.1474
   19   77    0     0.02     0.02     0.00     0.02         0.    0.0516
   20   78    0     0.03     0.13     0.00     0.03         0.    0.0075
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.120001
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     5.7700s 
time spent in multnx:                   5.7700s 
integral transfer time:                 0.0000s 
time spent for loop construction:       3.5700s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            5.9000s 

          starting ci iteration   4

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.318095
ci vector #   2dasum_wr=    0.894784
ci vector #   3dasum_wr=    0.281710
ci vector #   4dasum_wr=    0.761372


====================================================================================================
Diagonal     counts:  0x:      412118 2x:      100824 4x:       16200
All internal counts: zz :      141469 yy:     1199860 xx:      733974 ww:      718502
One-external counts: yz :      664107 yx:     1998598 yw:     2016328
Two-external counts: yy :      580878 ww:      224232 xx:      351894 xz:       37085 wz:       33077 wx:      512216
Three-ext.   counts: yx :      139509 yw:      144149

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:       52777    task #     2:       40747    task #     3:      332519    task #     4:      166259
task #     5:      510932    task #     6:     1507769    task #     7:     1513804    task #     8:      112690
task #     9:     1292631    task #    10:      225883    task #    11:      225883    task #    12:      126547
task #    13:      126547    task #    14:       63273    task #    15:       63275    task #    16:       63275
task #    17:       20226    task #    18:      147443    task #    19:       51613    task #    20:       54916
 Overlap matrix (sovlci)

              sovl   1       sovl   2       sovl   3       sovl   4
 sovl   1     1.00000000
 sovl   2     0.00000000     0.02954206
 sovl   3    -0.01688510    -0.00040916     0.00403830
 sovl   4     0.00983492    -0.00069461    -0.00035225     0.00104040
 Overlap matrix (active-active) sovlaa

              sovl   1       sovl   2       sovl   3       sovl   4
 sovl   1     1.00000000
 sovl   2     0.00000000     0.00000000
 sovl   3    -0.01688510     0.00000000     0.00094921
 sovl   4     0.00983492     0.00000000    -0.00042699     0.00039033
 Overlap matrix (critical) sovlcrit

              sovl   1       sovl   2       sovl   3       sovl   4
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4
 sovl   1     0.00000000
 sovl   2     0.00000000     0.02954206
 sovl   3     0.00000000    -0.00040916     0.00308909
 sovl   4     0.00000000    -0.00069461     0.00007474     0.00065007
 Subspace hamiltonian (htci) 

              htci   1       htci   2       htci   3       htci   4
 htci   1    -8.82741298
 htci   2    -0.03984773    -0.22745430
 htci   3     0.14999346     0.00787611    -0.03070984
 htci   4    -0.08667957     0.00564805     0.00197706    -0.00783455
 Subspace hamiltonian (active-active) 

              htaa   1       htaa   2       htaa   3       htaa   4
 htaa   1    -8.82741298
 htaa   2     0.00000000     0.00000000
 htaa   3    -0.00770300    -0.00770300    -0.00770300
 htaa   4    -0.00316077    -0.00316077    -0.00316077    -0.00316077
 Subspace hamiltonian (critical) 

              htcr   1       htcr   2       htcr   3       htcr   4
 scale factor: (eold-eref)*(1-gvalue): -5.227370877400972E-002 *
  0.622222222222222      = -3.252586323716160E-002
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4
   ht   1    -8.82741298
   ht   2    -0.03984773    -0.22841519
   ht   3     0.14999346     0.00788942    -0.03081031
   ht   4    -0.08667957     0.00567064     0.00197463    -0.00785569
Spectrum of overlapmatrix:    0.000913    0.003759    0.029565    1.000383
calca4: root=   1 anorm**2=  0.05799268 scale:  1.02858771
calca4: root=   2 anorm**2=  0.89458492 scale:  1.37643922
calca4: root=   3 anorm**2=  0.89573683 scale:  1.37685759
calca4: root=   4 anorm**2=  0.68208602 scale:  1.29695259

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4
 refs   1    1.00000      -2.287966E-14  -1.688510E-02   9.834922E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4
 civs   1  -0.958602      -0.224343       0.250225      -0.362784    
 civs   2   -1.27585        2.83715       -4.83101       -1.23522    
 civs   3    1.33476       -10.8681       -4.91906       -11.2127    
 civs   4    1.11509       -16.4208       -20.5136        19.9672    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4
 ref    1  -0.970173      -0.202331       0.131534       2.291947E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1    -0.97017308    -0.20233071     0.13153391     0.02291947
 NCSF=                  1368                 23810                 51154
                 65696
 fact= -3.351055992693530E-002
 factd= -3.351055992693530E-002

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  4  1   -383.3656967481  1.5825E-03  3.4022E-04  2.4037E-02  1.0000E-03
 mraqcc  #  4  2   -382.6913500372  2.3365E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  4  3   -381.9049403034  2.2175E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  4  4   -381.5053422019  7.0209E+00  0.0000E+00  6.7155E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.02     0.01     0.00     0.02         0.    0.0528
    2   25    0     0.01     0.01     0.00     0.01         0.    0.0407
    3   26    0     0.29     0.07     0.00     0.29         0.    0.3325
    4   27    0     0.28     0.07     0.00     0.28         0.    0.1663
    5   11    0     0.37     0.31     0.00     0.37         0.    0.5109
    6   15    0     1.17     0.64     0.00     1.17         0.    1.5078
    7   16    0     1.20     0.70     0.00     1.20         0.    1.5138
    8    1    0     0.08     0.07     0.00     0.08         0.    0.1127
    9    5    0     0.87     0.57     0.00     0.87         0.    1.2926
   10    6    0     0.38     0.20     0.00     0.38         0.    0.2259
   11   18    0     0.36     0.19     0.00     0.36         0.    0.2259
   12    7    0     0.31     0.18     0.00     0.31         0.    0.1265
   13   19    0     0.29     0.16     0.00     0.29         0.    0.1265
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0633
   15   43    0     0.01     0.00     0.00     0.01         0.    0.0633
   16   44    0     0.01     0.00     0.00     0.01         0.    0.0633
   17   75    0     0.01     0.01     0.00     0.01         0.    0.0202
   18   76    0     0.05     0.03     0.00     0.05         0.    0.1474
   19   77    0     0.03     0.03     0.00     0.03         0.    0.0516
   20   78    0     0.02     0.10     0.00     0.02         0.    0.0075
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.009998
time for cinew                         0.120001
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     5.7600s 
time spent in multnx:                   5.7600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       3.3500s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            5.8900s 

          starting ci iteration   5

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.184247
ci vector #   2dasum_wr=    0.556340
ci vector #   3dasum_wr=    0.199503
ci vector #   4dasum_wr=    0.490678


====================================================================================================
Diagonal     counts:  0x:      412118 2x:      100824 4x:       16200
All internal counts: zz :      141469 yy:     1199860 xx:      733974 ww:      718502
One-external counts: yz :      664107 yx:     1998598 yw:     2016328
Two-external counts: yy :      580878 ww:      224232 xx:      351894 xz:       37085 wz:       33077 wx:      512216
Three-ext.   counts: yx :      139509 yw:      144149

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:       52777    task #     2:       40747    task #     3:      332519    task #     4:      166259
task #     5:      510932    task #     6:     1507769    task #     7:     1513804    task #     8:      112690
task #     9:     1292631    task #    10:      225883    task #    11:      225883    task #    12:      126547
task #    13:      126547    task #    14:       63273    task #    15:       63275    task #    16:       63275
task #    17:       20226    task #    18:      147443    task #    19:       51613    task #    20:       54916
 Overlap matrix (sovlci)

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5
 sovl   1     1.00000000
 sovl   2     0.00000000     0.02954206
 sovl   3    -0.01688510    -0.00040916     0.00403830
 sovl   4     0.00983492    -0.00069461    -0.00035225     0.00104040
 sovl   5     0.00518529    -0.00015504    -0.00000329     0.00005871     0.00027156
 Overlap matrix (active-active) sovlaa

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5
 sovl   1     1.00000000
 sovl   2     0.00000000     0.00000000
 sovl   3    -0.01688510     0.00000000     0.00094921
 sovl   4     0.00983492     0.00000000    -0.00042699     0.00039033
 sovl   5     0.00518529     0.00000000    -0.00014664     0.00006897     0.00010514
 Overlap matrix (critical) sovlcrit

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5
 sovl   1     0.00000000
 sovl   2     0.00000000     0.02954206
 sovl   3     0.00000000    -0.00040916     0.00308909
 sovl   4     0.00000000    -0.00069461     0.00007474     0.00065007
 sovl   5     0.00000000    -0.00015504     0.00014335    -0.00001027     0.00016642
 Subspace hamiltonian (htci) 

              htci   1       htci   2       htci   3       htci   4       htci   5
 htci   1    -8.82741298
 htci   2    -0.03984773    -0.22745430
 htci   3     0.14999346     0.00787611    -0.03070984
 htci   4    -0.08667957     0.00564805     0.00197706    -0.00783455
 htci   5    -0.04573742     0.00113880     0.00003342    -0.00082234    -0.00199339
 Subspace hamiltonian (active-active) 

              htaa   1       htaa   2       htaa   3       htaa   4       htaa   5
 htaa   1    -8.82741298
 htaa   2     0.00000000     0.00000000
 htaa   3    -0.00770300    -0.00770300    -0.00770300
 htaa   4    -0.00316077    -0.00316077    -0.00316077    -0.00316077
 htaa   5    -0.00083670    -0.00083670    -0.00083670    -0.00083670    -0.00083670
 Subspace hamiltonian (critical) 

              htcr   1       htcr   2       htcr   3       htcr   4       htcr   5
 scale factor: (eold-eref)*(1-gvalue): -5.385625702543173E-002 *
  0.622222222222222      = -3.351055992693530E-002
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1    -8.82741298
   ht   2    -0.03984773    -0.22844428
   ht   3     0.14999346     0.00788982    -0.03081336
   ht   4    -0.08667957     0.00567132     0.00197455    -0.00785633
   ht   5    -0.04573742     0.00114400     0.00002862    -0.00082200    -0.00199897
Spectrum of overlapmatrix:    0.000242    0.000913    0.003761    0.029566    1.000410
calca4: root=   1 anorm**2=  0.06101999 scale:  1.03005825
calca4: root=   2 anorm**2=  0.85842825 scale:  1.36324182
calca4: root=   3 anorm**2=  0.77648874 scale:  1.33284986
calca4: root=   4 anorm**2=  0.78527561 scale:  1.33614206
calca4: root=   5 anorm**2=  0.71520242 scale:  1.30965737

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1    1.00000      -2.287966E-14  -1.688510E-02   9.834922E-03   5.185289E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 civs   1  -0.964078      -2.437798E-02   0.516876      -0.300035      -8.408821E-03
 civs   2   -1.28018        1.84373       -4.55672       -2.95767       0.290212    
 civs   3    1.40342       -9.20410        3.26004       -13.1030       -1.69983    
 civs   4    1.40285       -19.6326       -14.6771        3.03860        21.8785    
 civs   5    1.04003       -23.1871       -31.0413        21.7619       -46.4405    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1  -0.968585      -0.182283       0.156524       6.393698E-02  -5.341188E-03

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1    -0.96858505    -0.18228253     0.15652352     0.06393698    -0.00534119
 NCSF=                  1368                 23810                 51154
                 65696
 fact= -3.376691294235662E-002
 factd= -3.376691294235662E-002

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  5  1   -383.3661087440  4.1200E-04  9.5862E-05  1.2592E-02  1.0000E-03
 mraqcc  #  5  2   -382.8419652399  1.5062E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  5  3   -382.1520496832  2.4711E-01  0.0000E+00  2.1940E-01  1.0000E-04
 mraqcc  #  5  4   -381.6429805557  1.3764E-01  0.0000E+00  3.9204E-01  1.0000E-04
 mraqcc  #  5  5   -381.1030499364  6.6186E+00  0.0000E+00  7.3715E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.02     0.02     0.00     0.02         0.    0.0528
    2   25    0     0.02     0.01     0.00     0.02         0.    0.0407
    3   26    0     0.28     0.07     0.00     0.28         0.    0.3325
    4   27    0     0.28     0.07     0.00     0.28         0.    0.1663
    5   11    0     0.37     0.32     0.00     0.37         0.    0.5109
    6   15    0     1.17     0.70     0.00     1.17         0.    1.5078
    7   16    0     1.20     0.66     0.00     1.20         0.    1.5138
    8    1    0     0.08     0.06     0.00     0.08         0.    0.1127
    9    5    0     0.87     0.58     0.00     0.87         0.    1.2926
   10    6    0     0.38     0.21     0.00     0.38         0.    0.2259
   11   18    0     0.37     0.20     0.00     0.37         0.    0.2259
   12    7    0     0.30     0.18     0.00     0.30         0.    0.1265
   13   19    0     0.29     0.20     0.00     0.29         0.    0.1265
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0633
   15   43    0     0.01     0.00     0.00     0.01         0.    0.0633
   16   44    0     0.01     0.00     0.00     0.01         0.    0.0633
   17   75    0     0.01     0.01     0.00     0.01         0.    0.0202
   18   76    0     0.06     0.04     0.00     0.06         0.    0.1474
   19   77    0     0.02     0.02     0.00     0.02         0.    0.0516
   20   78    0     0.03     0.12     0.00     0.03         0.    0.0075
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.129999
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     5.7700s 
time spent in multnx:                   5.7700s 
integral transfer time:                 0.0000s 
time spent for loop construction:       3.4700s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            5.9000s 

          starting ci iteration   6

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.111448
ci vector #   2dasum_wr=    0.298081
ci vector #   3dasum_wr=    0.126628
ci vector #   4dasum_wr=    0.267276


====================================================================================================
Diagonal     counts:  0x:      412118 2x:      100824 4x:       16200
All internal counts: zz :      141469 yy:     1199860 xx:      733974 ww:      718502
One-external counts: yz :      664107 yx:     1998598 yw:     2016328
Two-external counts: yy :      580878 ww:      224232 xx:      351894 xz:       37085 wz:       33077 wx:      512216
Three-ext.   counts: yx :      139509 yw:      144149

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:       52777    task #     2:       40747    task #     3:      332519    task #     4:      166259
task #     5:      510932    task #     6:     1507769    task #     7:     1513804    task #     8:      112690
task #     9:     1292631    task #    10:      225883    task #    11:      225883    task #    12:      126547
task #    13:      126547    task #    14:       63273    task #    15:       63275    task #    16:       63275
task #    17:       20226    task #    18:      147443    task #    19:       51613    task #    20:       54916
 Overlap matrix (sovlci)

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6
 sovl   1     1.00000000
 sovl   2     0.00000000     0.02954206
 sovl   3    -0.01688510    -0.00040916     0.00403830
 sovl   4     0.00983492    -0.00069461    -0.00035225     0.00104040
 sovl   5     0.00518529    -0.00015504    -0.00000329     0.00005871     0.00027156
 sovl   6    -0.00243637     0.00003690     0.00009618    -0.00000944    -0.00002369     0.00007942
 Overlap matrix (active-active) sovlaa

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6
 sovl   1     1.00000000
 sovl   2     0.00000000     0.00000000
 sovl   3    -0.01688510     0.00000000     0.00094921
 sovl   4     0.00983492     0.00000000    -0.00042699     0.00039033
 sovl   5     0.00518529     0.00000000    -0.00014664     0.00006897     0.00010514
 sovl   6    -0.00243637     0.00000000     0.00004630    -0.00001104    -0.00002908     0.00004264
 Overlap matrix (critical) sovlcrit

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6
 sovl   1     0.00000000
 sovl   2     0.00000000     0.02954206
 sovl   3     0.00000000    -0.00040916     0.00308909
 sovl   4     0.00000000    -0.00069461     0.00007474     0.00065007
 sovl   5     0.00000000    -0.00015504     0.00014335    -0.00001027     0.00016642
 sovl   6     0.00000000     0.00003690     0.00004987     0.00000160     0.00000539     0.00003678
 Subspace hamiltonian (htci) 

              htci   1       htci   2       htci   3       htci   4       htci   5       htci   6
 htci   1    -8.82741298
 htci   2    -0.03984773    -0.22745430
 htci   3     0.14999346     0.00787611    -0.03070984
 htci   4    -0.08667957     0.00564805     0.00197706    -0.00783455
 htci   5    -0.04573742     0.00113880     0.00003342    -0.00082234    -0.00199339
 htci   6     0.02151773    -0.00023807    -0.00085348     0.00008263     0.00011771    -0.00058140
 Subspace hamiltonian (active-active) 

              htaa   1       htaa   2       htaa   3       htaa   4       htaa   5       htaa   6
 htaa   1    -8.82741298
 htaa   2     0.00000000     0.00000000
 htaa   3    -0.00770300    -0.00770300    -0.00770300
 htaa   4    -0.00316077    -0.00316077    -0.00316077    -0.00316077
 htaa   5    -0.00083670    -0.00083670    -0.00083670    -0.00083670    -0.00083670
 htaa   6    -0.00033092    -0.00033092    -0.00033092    -0.00033092    -0.00033092    -0.00033092
 Subspace hamiltonian (critical) 

              htcr   1       htcr   2       htcr   3       htcr   4       htcr   5       htcr   6
 scale factor: (eold-eref)*(1-gvalue): -5.426825294307314E-002 *
  0.622222222222222      = -3.376691294235662E-002
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1    -8.82741298
   ht   2    -0.03984773    -0.22845185
   ht   3     0.14999346     0.00788992    -0.03081415
   ht   4    -0.08667957     0.00567150     0.00197453    -0.00785650
   ht   5    -0.04573742     0.00114404     0.00002858    -0.00082200    -0.00199901
   ht   6     0.02151773    -0.00023931    -0.00085517     0.00008257     0.00011753    -0.00058265
Spectrum of overlapmatrix:    0.000071    0.000243    0.000914    0.003762    0.029566    1.000416
calca4: root=   1 anorm**2=  0.06198900 scale:  1.03052850
calca4: root=   2 anorm**2=  0.82108969 scale:  1.34947756
calca4: root=   3 anorm**2=  0.78362887 scale:  1.33552569
calca4: root=   4 anorm**2=  0.82564619 scale:  1.35116475
calca4: root=   5 anorm**2=  0.61436061 scale:  1.27057491
calca4: root=   6 anorm**2=  0.60483000 scale:  1.26681885

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1    1.00000      -2.287966E-14  -1.688510E-02   9.834922E-03   5.185289E-03  -2.436369E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1   0.963009       3.700648E-02  -0.344831       0.111119       0.512433       0.159813    
 civs   2    1.28092       -1.32220        4.20446       -3.61480       0.712862      -0.181137    
 civs   3   -1.42160        7.89152       -5.81407       -9.68221        8.85277       -2.38903    
 civs   4   -1.47980        18.7785        4.79767       -13.3749       -17.1450        15.6800    
 civs   5   -1.31956        29.4527        29.5365        18.0853       -11.0939       -44.4585    
 civs   6  -0.992620        31.7388        51.1864        63.7789        61.5445        49.7632    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.968036       0.163835      -0.171029       8.145150E-02  -1.313713E-02   2.591653E-03

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.96803583     0.16383526    -0.17102920     0.08145150    -0.01313713     0.00259165

 trial vector basis is being transformed.  new dimension:   3
 NCSF=                  1368                 23810                 51154
                 65696
 fact= -3.383592444722565E-002
 factd= -3.383592444722565E-002

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  6  1   -383.3662196554  1.1091E-04  2.7017E-05  6.8780E-03  1.0000E-03
 mraqcc  #  6  2   -382.9207064184  7.8741E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  6  3   -382.3134400836  1.6139E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  6  4   -381.6615469987  1.8566E-02  0.0000E+00  2.2260E-01  1.0000E-04
 mraqcc  #  6  5   -381.6261635370  5.2311E-01  0.0000E+00  7.8502E-01  1.0000E-04
 mraqcc  #  6  6   -380.9596646988  6.4752E+00  0.0000E+00  8.6699E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.02     0.01     0.00     0.02         0.    0.0528
    2   25    0     0.01     0.01     0.00     0.01         0.    0.0407
    3   26    0     0.29     0.06     0.00     0.29         0.    0.3325
    4   27    0     0.28     0.07     0.00     0.28         0.    0.1663
    5   11    0     0.36     0.30     0.00     0.36         0.    0.5109
    6   15    0     1.18     0.63     0.00     1.18         0.    1.5078
    7   16    0     1.20     0.67     0.00     1.20         0.    1.5138
    8    1    0     0.08     0.08     0.00     0.08         0.    0.1127
    9    5    0     0.87     0.60     0.00     0.87         0.    1.2926
   10    6    0     0.38     0.20     0.00     0.38         0.    0.2259
   11   18    0     0.36     0.20     0.00     0.36         0.    0.2259
   12    7    0     0.31     0.18     0.00     0.31         0.    0.1265
   13   19    0     0.29     0.17     0.00     0.29         0.    0.1265
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0633
   15   43    0     0.01     0.00     0.00     0.01         0.    0.0633
   16   44    0     0.01     0.00     0.00     0.01         0.    0.0633
   17   75    0     0.01     0.01     0.00     0.01         0.    0.0202
   18   76    0     0.05     0.05     0.00     0.05         0.    0.1474
   19   77    0     0.03     0.02     0.00     0.03         0.    0.0516
   20   78    0     0.02     0.13     0.00     0.02         0.    0.0075
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.019997
time for cinew                         0.120003
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     5.7600s 
time spent in multnx:                   5.7600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       3.3900s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            5.9000s 

          starting ci iteration   7

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.056416
ci vector #   2dasum_wr=    0.163178
ci vector #   3dasum_wr=    0.073378
ci vector #   4dasum_wr=    0.150455


====================================================================================================
Diagonal     counts:  0x:      412118 2x:      100824 4x:       16200
All internal counts: zz :      141469 yy:     1199860 xx:      733974 ww:      718502
One-external counts: yz :      664107 yx:     1998598 yw:     2016328
Two-external counts: yy :      580878 ww:      224232 xx:      351894 xz:       37085 wz:       33077 wx:      512216
Three-ext.   counts: yx :      139509 yw:      144149

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:       52777    task #     2:       40747    task #     3:      332519    task #     4:      166259
task #     5:      510932    task #     6:     1507769    task #     7:     1513804    task #     8:      112690
task #     9:     1292631    task #    10:      225883    task #    11:      225883    task #    12:      126547
task #    13:      126547    task #    14:       63273    task #    15:       63275    task #    16:       63275
task #    17:       20226    task #    18:      147443    task #    19:       51613    task #    20:       54916
 Overlap matrix (sovlci)

              sovl   1       sovl   2       sovl   3       sovl   4
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4    -0.00144211    -0.00085576    -0.00022784     0.00002185
 Overlap matrix (active-active) sovlaa

              sovl   1       sovl   2       sovl   3       sovl   4
 sovl   1     0.93801100
 sovl   2     0.15071321     0.17891031
 sovl   3    -0.16620287     0.09843232     0.21637113
 sovl   4    -0.00141435    -0.00080944    -0.00008705     0.00001171
 Overlap matrix (critical) sovlcrit

              sovl   1       sovl   2       sovl   3       sovl   4
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4
 sovl   1     0.06198900
 sovl   2    -0.15071321     0.82108969
 sovl   3     0.16620287    -0.09843232     0.78362887
 sovl   4    -0.00002776    -0.00004632    -0.00014079     0.00001014
 Subspace hamiltonian (htci) 

              htci   1       htci   2       htci   3       htci   4
 htci   1    -8.87969897
 htci   2    -0.00508912    -8.40855324
 htci   3     0.00561216    -0.00332376    -7.80255184
 htci   4     0.01278060     0.00834925     0.00369225    -0.00015865
 Subspace hamiltonian (active-active) 

              htaa   1       htaa   2       htaa   3       htaa   4
 htaa   1    -8.15661557
 htaa   2    -0.18277621    -5.69653546
 htaa   3     3.12422912    -3.32686276    -3.81222340
 htaa   4    -0.00009118    -0.00009118    -0.00009118    -0.00009118
 Subspace hamiltonian (critical) 

              htcr   1       htcr   2       htcr   3       htcr   4
 scale factor: (eold-eref)*(1-gvalue): -5.437916429018408E-002 *
  0.622222222222222      = -3.383592444722565E-002
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4
   ht   1    -8.88179642
   ht   2     0.00001040    -8.43633557
   ht   3    -0.00001147     0.00000679    -7.82906665
   ht   4     0.01278154     0.00835082     0.00369702    -0.00015899
Spectrum of overlapmatrix:    0.000019    1.000000    1.000000    1.000003
calca4: root=   1 anorm**2=  0.06209850 scale:  1.03058163
calca4: root=   2 anorm**2=  0.74533907 scale:  1.32111281
calca4: root=   3 anorm**2=  0.77960567 scale:  1.33401862
calca4: root=   4 anorm**2=  0.62658613 scale:  1.27537686

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4
 refs   1   0.968036       0.163835      -0.171029      -1.487015E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4
 civs   1   -1.00124      -6.187060E-02   0.107361       0.302834    
 civs   2   1.488571E-03   0.933215       0.256252       0.319406    
 civs   3   1.372319E-03   0.120413      -0.906643       0.407716    
 civs   4  -0.870229       -45.0662        76.0279        211.814    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4
 ref    1  -0.967937       0.139420       0.187920      -3.921796E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1    -0.96793690     0.13942047     0.18791996    -0.03921796
 NCSF=                  1368                 23810                 51154
                 65696
 fact= -3.385321878940128E-002
 factd= -3.385321878940128E-002

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  7  1   -383.3662474498  2.7794E-05  9.6980E-06  4.1152E-03  1.0000E-03
 mraqcc  #  7  2   -382.9732266851  5.2520E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  7  3   -382.4709230249  1.5748E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  7  4   -381.1861047480 -4.7544E-01  0.0000E+00  1.0069E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.02     0.01     0.00     0.02         0.    0.0528
    2   25    0     0.02     0.01     0.00     0.02         0.    0.0407
    3   26    0     0.28     0.06     0.00     0.28         0.    0.3325
    4   27    0     0.28     0.07     0.00     0.28         0.    0.1663
    5   11    0     0.37     0.29     0.00     0.37         0.    0.5109
    6   15    0     1.18     0.65     0.00     1.18         0.    1.5078
    7   16    0     1.20     0.70     0.00     1.20         0.    1.5138
    8    1    0     0.08     0.08     0.00     0.08         0.    0.1127
    9    5    0     0.87     0.58     0.00     0.87         0.    1.2926
   10    6    0     0.38     0.18     0.00     0.38         0.    0.2259
   11   18    0     0.36     0.20     0.00     0.36         0.    0.2259
   12    7    0     0.31     0.17     0.00     0.31         0.    0.1265
   13   19    0     0.28     0.17     0.00     0.28         0.    0.1265
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0633
   15   43    0     0.02     0.00     0.00     0.02         0.    0.0633
   16   44    0     0.01     0.00     0.00     0.01         0.    0.0633
   17   75    0     0.01     0.01     0.00     0.01         0.    0.0202
   18   76    0     0.05     0.03     0.00     0.05         0.    0.1474
   19   77    0     0.03     0.02     0.00     0.03         0.    0.0516
   20   78    0     0.02     0.12     0.00     0.02         0.    0.0075
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.009998
time for cinew                         0.119999
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     5.7700s 
time spent in multnx:                   5.7700s 
integral transfer time:                 0.0000s 
time spent for loop construction:       3.3500s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            5.9000s 

          starting ci iteration   8

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.035575
ci vector #   2dasum_wr=    0.092119
ci vector #   3dasum_wr=    0.036735
ci vector #   4dasum_wr=    0.085352


====================================================================================================
Diagonal     counts:  0x:      412118 2x:      100824 4x:       16200
All internal counts: zz :      141469 yy:     1199860 xx:      733974 ww:      718502
One-external counts: yz :      664107 yx:     1998598 yw:     2016328
Two-external counts: yy :      580878 ww:      224232 xx:      351894 xz:       37085 wz:       33077 wx:      512216
Three-ext.   counts: yx :      139509 yw:      144149

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:       52777    task #     2:       40747    task #     3:      332519    task #     4:      166259
task #     5:      510932    task #     6:     1507769    task #     7:     1513804    task #     8:      112690
task #     9:     1292631    task #    10:      225883    task #    11:      225883    task #    12:      126547
task #    13:      126547    task #    14:       63273    task #    15:       63275    task #    16:       63275
task #    17:       20226    task #    18:      147443    task #    19:       51613    task #    20:       54916
 Overlap matrix (sovlci)

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4    -0.00144211    -0.00085576    -0.00022784     0.00002185
 sovl   5    -0.00019225     0.00025975     0.00031235     0.00000119     0.00000670
 Overlap matrix (active-active) sovlaa

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5
 sovl   1     0.93801100
 sovl   2     0.15071321     0.17891031
 sovl   3    -0.16620287     0.09843232     0.21637113
 sovl   4    -0.00141435    -0.00080944    -0.00008705     0.00001171
 sovl   5    -0.00014619     0.00023820     0.00035913     0.00000210     0.00000309
 Overlap matrix (critical) sovlcrit

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5
 sovl   1     0.06198900
 sovl   2    -0.15071321     0.82108969
 sovl   3     0.16620287    -0.09843232     0.78362887
 sovl   4    -0.00002776    -0.00004632    -0.00014079     0.00001014
 sovl   5    -0.00004606     0.00002156    -0.00004678    -0.00000091     0.00000361
 Subspace hamiltonian (htci) 

              htci   1       htci   2       htci   3       htci   4       htci   5
 htci   1    -8.87969897
 htci   2    -0.00508912    -8.40855324
 htci   3     0.00561216    -0.00332376    -7.80255184
 htci   4     0.01278060     0.00834925     0.00369225    -0.00015865
 htci   5     0.00170597    -0.00228524    -0.00268463     0.00000072    -0.00004592
 Subspace hamiltonian (active-active) 

              htaa   1       htaa   2       htaa   3       htaa   4       htaa   5
 htaa   1    -8.15661557
 htaa   2    -0.18277621    -5.69653546
 htaa   3     3.12422912    -3.32686276    -3.81222340
 htaa   4    -0.00009118    -0.00009118    -0.00009118    -0.00009118
 htaa   5    -0.00002197    -0.00002197    -0.00002197    -0.00002197    -0.00002197
 Subspace hamiltonian (critical) 

              htcr   1       htcr   2       htcr   3       htcr   4       htcr   5
 scale factor: (eold-eref)*(1-gvalue): -5.440695876868062E-002 *
  0.622222222222222      = -3.385321878940128E-002
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1    -8.88179749
   ht   2     0.00001301    -8.43634977
   ht   3    -0.00001434     0.00000850    -7.82908020
   ht   4     0.01278154     0.00835082     0.00369702    -0.00015899
   ht   5     0.00170753    -0.00228597    -0.00268304     0.00000075    -0.00004605
Spectrum of overlapmatrix:    0.000006    0.000019    1.000000    1.000000    1.000003
calca4: root=   1 anorm**2=  0.06223950 scale:  1.03065004
calca4: root=   2 anorm**2=  0.66975280 scale:  1.29218915
calca4: root=   3 anorm**2=  0.81013490 scale:  1.34541254
calca4: root=   4 anorm**2=  0.86733396 scale:  1.36650429
calca4: root=   5 anorm**2=  0.41305575 scale:  1.18872022

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1   0.968036       0.163835      -0.171029      -1.487015E-03  -1.371102E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 civs   1   -1.00156      -8.380645E-02   0.114425      -0.121905       0.270222    
 civs   2   2.067299E-03   0.827211       0.502643      -0.315921       0.131212    
 civs   3   1.867310E-03   0.181523      -0.706185      -0.691850       9.523872E-02
 civs   4   -1.23944       -72.2904        98.7943       -120.615        154.179    
 civs   5    1.02875        76.9770       -127.906        259.738        257.007    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1  -0.967829       0.120295       0.184525       9.230291E-02   2.287414E-03

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1    -0.96782882     0.12029543     0.18452478     0.09230291     0.00228741
 NCSF=                  1368                 23810                 51154
                 65696
 fact= -3.386009507100296E-002
 factd= -3.386009507100296E-002

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  8  1   -383.3662585010  1.1051E-05  2.8044E-06  2.2379E-03  1.0000E-03
 mraqcc  #  8  2   -383.0187484506  4.5522E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  8  3   -382.5989582072  1.2804E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  8  4   -381.8120290470  6.2592E-01  0.0000E+00  4.9123E-01  1.0000E-04
 mraqcc  #  8  5   -380.6473413185 -9.7882E-01  0.0000E+00  1.2298E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.02     0.02     0.00     0.02         0.    0.0528
    2   25    0     0.01     0.01     0.00     0.01         0.    0.0407
    3   26    0     0.29     0.07     0.00     0.28         0.    0.3325
    4   27    0     0.28     0.07     0.00     0.28         0.    0.1663
    5   11    0     0.37     0.31     0.00     0.37         0.    0.5109
    6   15    0     1.17     0.67     0.00     1.17         0.    1.5078
    7   16    0     1.20     0.77     0.00     1.20         0.    1.5138
    8    1    0     0.08     0.07     0.00     0.08         0.    0.1127
    9    5    0     0.87     0.58     0.00     0.87         0.    1.2926
   10    6    0     0.38     0.20     0.01     0.38         0.    0.2259
   11   18    0     0.37     0.22     0.00     0.37         0.    0.2259
   12    7    0     0.30     0.18     0.00     0.30         0.    0.1265
   13   19    0     0.29     0.20     0.00     0.29         0.    0.1265
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0633
   15   43    0     0.01     0.00     0.00     0.01         0.    0.0633
   16   44    0     0.01     0.00     0.00     0.01         0.    0.0633
   17   75    0     0.01     0.01     0.00     0.01         0.    0.0202
   18   76    0     0.06     0.06     0.00     0.06         0.    0.1474
   19   77    0     0.02     0.02     0.00     0.02         0.    0.0516
   20   78    0     0.03     0.12     0.00     0.03         0.    0.0075
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.119999
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     5.7700s 
time spent in multnx:                   5.7600s 
integral transfer time:                 0.0100s 
time spent for loop construction:       3.5800s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            5.9000s 

          starting ci iteration   9

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.018548
ci vector #   2dasum_wr=    0.055494
ci vector #   3dasum_wr=    0.027040
ci vector #   4dasum_wr=    0.049597


====================================================================================================
Diagonal     counts:  0x:      412118 2x:      100824 4x:       16200
All internal counts: zz :      141469 yy:     1199860 xx:      733974 ww:      718502
One-external counts: yz :      664107 yx:     1998598 yw:     2016328
Two-external counts: yy :      580878 ww:      224232 xx:      351894 xz:       37085 wz:       33077 wx:      512216
Three-ext.   counts: yx :      139509 yw:      144149

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:       52777    task #     2:       40747    task #     3:      332519    task #     4:      166259
task #     5:      510932    task #     6:     1507769    task #     7:     1513804    task #     8:      112690
task #     9:     1292631    task #    10:      225883    task #    11:      225883    task #    12:      126547
task #    13:      126547    task #    14:       63273    task #    15:       63275    task #    16:       63275
task #    17:       20226    task #    18:      147443    task #    19:       51613    task #    20:       54916
 Overlap matrix (sovlci)

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4    -0.00144211    -0.00085576    -0.00022784     0.00002185
 sovl   5    -0.00019225     0.00025975     0.00031235     0.00000119     0.00000670
 sovl   6    -0.00025517     0.00002805     0.00002910     0.00000007     0.00000035     0.00000192
 Overlap matrix (active-active) sovlaa

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6
 sovl   1     0.93801100
 sovl   2     0.15071321     0.17891031
 sovl   3    -0.16620287     0.09843232     0.21637113
 sovl   4    -0.00141435    -0.00080944    -0.00008705     0.00001171
 sovl   5    -0.00014619     0.00023820     0.00035913     0.00000210     0.00000309
 sovl   6    -0.00021776     0.00000510     0.00006653    -0.00000004     0.00000017     0.00000073
 Overlap matrix (critical) sovlcrit

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6
 sovl   1     0.06198900
 sovl   2    -0.15071321     0.82108969
 sovl   3     0.16620287    -0.09843232     0.78362887
 sovl   4    -0.00002776    -0.00004632    -0.00014079     0.00001014
 sovl   5    -0.00004606     0.00002156    -0.00004678    -0.00000091     0.00000361
 sovl   6    -0.00003741     0.00002295    -0.00003743     0.00000011     0.00000018     0.00000119
 Subspace hamiltonian (htci) 

              htci   1       htci   2       htci   3       htci   4       htci   5       htci   6
 htci   1    -8.87969897
 htci   2    -0.00508912    -8.40855324
 htci   3     0.00561216    -0.00332376    -7.80255184
 htci   4     0.01278060     0.00834925     0.00369225    -0.00015865
 htci   5     0.00170597    -0.00228524    -0.00268463     0.00000072    -0.00004592
 htci   6     0.00226513    -0.00026004    -0.00025583    -0.00000064    -0.00000585    -0.00001331
 Subspace hamiltonian (active-active) 

              htaa   1       htaa   2       htaa   3       htaa   4       htaa   5       htaa   6
 htaa   1    -8.15661557
 htaa   2    -0.18277621    -5.69653546
 htaa   3     3.12422912    -3.32686276    -3.81222340
 htaa   4    -0.00009118    -0.00009118    -0.00009118    -0.00009118
 htaa   5    -0.00002197    -0.00002197    -0.00002197    -0.00002197    -0.00002197
 htaa   6    -0.00000536    -0.00000536    -0.00000536    -0.00000536    -0.00000536    -0.00000536
 Subspace hamiltonian (critical) 

              htcr   1       htcr   2       htcr   3       htcr   4       htcr   5       htcr   6
 scale factor: (eold-eref)*(1-gvalue): -5.441800993554047E-002 *
  0.622222222222222      = -3.386009507100296E-002
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1    -8.88179792
   ht   2     0.00001404    -8.43635542
   ht   3    -0.00001549     0.00000917    -7.82908559
   ht   4     0.01278154     0.00835082     0.00369702    -0.00015899
   ht   5     0.00170753    -0.00228597    -0.00268304     0.00000075    -0.00004605
   ht   6     0.00226640    -0.00026082    -0.00025456    -0.00000064    -0.00000586    -0.00001335
Spectrum of overlapmatrix:    0.000002    0.000006    0.000019    1.000000    1.000000    1.000003
calca4: root=   1 anorm**2=  0.06233071 scale:  1.03069428
calca4: root=   2 anorm**2=  0.62721390 scale:  1.27562294
calca4: root=   3 anorm**2=  0.82710199 scale:  1.35170337
calca4: root=   4 anorm**2=  0.77935917 scale:  1.33392622
calca4: root=   5 anorm**2=  0.66721534 scale:  1.29120693
calca4: root=   6 anorm**2=  0.49690354 scale:  1.22348009

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1   0.968036       0.163835      -0.171029      -1.487015E-03  -1.371102E-04  -2.186754E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1   -1.00140      -6.222254E-02   3.971544E-02   9.777270E-02  -0.362397      -4.283945E-02
 civs   2   2.248427E-03   0.745357       0.639507      -0.138409      -0.264135      -2.232855E-02
 civs   3   1.996383E-03   0.197245      -0.497790      -0.786044      -0.338134       2.307594E-02
 civs   4   -1.33858       -81.7556        88.3890       -19.0888       -183.095       -71.2608    
 civs   5    1.30493        110.953       -168.747        182.904        18.1748       -287.149    
 civs   6   0.955096        107.526       -203.093        351.491       -407.993        448.831    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.967764       0.110993       0.164469       0.132853       2.273388E-02  -1.886248E-03

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.96776435     0.11099324     0.16446910     0.13285267     0.02273388    -0.00188625

 trial vector basis is being transformed.  new dimension:   3
 NCSF=                  1368                 23810                 51154
                 65696
 fact= -3.386202802481143E-002
 factd= -3.386202802481143E-002

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  9  1   -383.3662616075  3.1065E-06  8.6979E-07  1.2472E-03  1.0000E-03
 mraqcc  #  9  2   -383.0454110097  2.6663E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  9  3   -382.6858096750  8.6851E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  9  4   -382.1883709540  3.7634E-01  0.0000E+00  3.8679E-01  1.0000E-04
 mraqcc  #  9  5   -381.1777508531  5.3041E-01  0.0000E+00  8.1163E-01  1.0000E-04
 mraqcc  #  9  6   -380.1666254729 -7.9304E-01  0.0000E+00  1.1386E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.02     0.01     0.00     0.02         0.    0.0528
    2   25    0     0.01     0.01     0.00     0.01         0.    0.0407
    3   26    0     0.29     0.07     0.00     0.29         0.    0.3325
    4   27    0     0.28     0.07     0.00     0.28         0.    0.1663
    5   11    0     0.37     0.31     0.00     0.37         0.    0.5109
    6   15    0     1.17     0.63     0.00     1.17         0.    1.5078
    7   16    0     1.20     0.67     0.00     1.20         0.    1.5138
    8    1    0     0.08     0.07     0.00     0.08         0.    0.1127
    9    5    0     0.87     0.60     0.00     0.87         0.    1.2926
   10    6    0     0.38     0.18     0.00     0.38         0.    0.2259
   11   18    0     0.37     0.20     0.00     0.37         0.    0.2259
   12    7    0     0.30     0.18     0.00     0.30         0.    0.1265
   13   19    0     0.29     0.19     0.00     0.29         0.    0.1265
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0633
   15   43    0     0.01     0.00     0.00     0.01         0.    0.0633
   16   44    0     0.01     0.00     0.00     0.01         0.    0.0633
   17   75    0     0.01     0.01     0.00     0.01         0.    0.0202
   18   76    0     0.06     0.06     0.00     0.06         0.    0.1474
   19   77    0     0.02     0.02     0.00     0.02         0.    0.0516
   20   78    0     0.02     0.12     0.00     0.02         0.    0.0075
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.020000
time for cinew                         0.119999
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     5.7600s 
time spent in multnx:                   5.7600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       3.4000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            5.9000s 

          starting ci iteration  10

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.011003
ci vector #   2dasum_wr=    0.029192
ci vector #   3dasum_wr=    0.015644
ci vector #   4dasum_wr=    0.028836


====================================================================================================
Diagonal     counts:  0x:      412118 2x:      100824 4x:       16200
All internal counts: zz :      141469 yy:     1199860 xx:      733974 ww:      718502
One-external counts: yz :      664107 yx:     1998598 yw:     2016328
Two-external counts: yy :      580878 ww:      224232 xx:      351894 xz:       37085 wz:       33077 wx:      512216
Three-ext.   counts: yx :      139509 yw:      144149

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:       52777    task #     2:       40747    task #     3:      332519    task #     4:      166259
task #     5:      510932    task #     6:     1507769    task #     7:     1513804    task #     8:      112690
task #     9:     1292631    task #    10:      225883    task #    11:      225883    task #    12:      126547
task #    13:      126547    task #    14:       63273    task #    15:       63275    task #    16:       63275
task #    17:       20226    task #    18:      147443    task #    19:       51613    task #    20:       54916
 Overlap matrix (sovlci)

              sovl   1       sovl   2       sovl   3       sovl   4
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4    -0.00007699     0.00010265    -0.00003451     0.00000062
 Overlap matrix (active-active) sovlaa

              sovl   1       sovl   2       sovl   3       sovl   4
 sovl   1     0.93766929
 sovl   2    -0.09402903     0.37278610
 sovl   3    -0.16178428    -0.14843296     0.17289801
 sovl   4    -0.00008292     0.00010094    -0.00001314     0.00000029
 Overlap matrix (critical) sovlcrit

              sovl   1       sovl   2       sovl   3       sovl   4
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4
 sovl   1     0.06233071
 sovl   2     0.09402903     0.62721390
 sovl   3     0.16178428     0.14843296     0.82710199
 sovl   4     0.00000594     0.00000171    -0.00002137     0.00000033
 Subspace hamiltonian (htci) 

              htci   1       htci   2       htci   3       htci   4
 htci   1    -8.87972357
 htci   2     0.00318383    -8.53974597
 htci   3     0.00547803     0.00502595    -8.17337641
 htci   4     0.00068310    -0.00100866     0.00050275    -0.00000438
 Subspace hamiltonian (active-active) 

              htaa   1       htaa   2       htaa   3       htaa   4
 htaa   1    -8.19156652
 htaa   2    -1.01933431    -4.95176638
 htaa   3     2.00696027    -0.63817878    -2.38189292
 htaa   4    -0.00000214    -0.00000214    -0.00000214    -0.00000214
 Subspace hamiltonian (critical) 

              htcr   1       htcr   2       htcr   3       htcr   4
 scale factor: (eold-eref)*(1-gvalue): -5.442111646844694E-002 *
  0.622222222222222      = -3.386202802481143E-002
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4
   ht   1    -8.88183421
   ht   2    -0.00000018    -8.56098471
   ht   3    -0.00000031    -0.00000029    -8.20138376
   ht   4     0.00068290    -0.00100872     0.00050348    -0.00000439
Spectrum of overlapmatrix:    0.000001    1.000000    1.000000    1.000000
calca4: root=   1 anorm**2=  0.06231828 scale:  1.03068826
calca4: root=   2 anorm**2=  0.58663299 scale:  1.25961621
calca4: root=   3 anorm**2=  0.84688194 scale:  1.35900035
calca4: root=   4 anorm**2=  0.58271851 scale:  1.25806141

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4
 refs   1  -0.967764       0.110993       0.164469       9.138517E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4
 civs   1   -1.00006       1.125904E-02   1.941425E-02   9.590586E-02
 civs   2  -2.555313E-04   0.973485      -0.137042      -0.225891    
 civs   3   2.437744E-04  -8.302119E-02  -0.964781       0.253537    
 civs   4  -0.843675        151.962        256.838        1252.87    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4
 ref    1   0.967762       9.738676E-02  -0.169205       3.830596E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.96776154     0.09738676    -0.16920463     0.03830596
 NCSF=                  1368                 23810                 51154
                 65696
 fact= -3.386255957545169E-002
 factd= -3.386255957545169E-002

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  # 10  1   -383.3662624618  8.5428E-07  4.2763E-07  8.7152E-04  1.0000E-03
 mraqcc  # 10  2   -383.0653698749  1.9959E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 10  3   -382.7439674651  5.8158E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 10  4   -381.3724079421 -8.1596E-01  0.0000E+00  1.5560E+00  1.0000E-04
 
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.02     0.01     0.00     0.02         0.    0.0528
    2   25    0     0.02     0.01     0.00     0.02         0.    0.0407
    3   26    0     0.28     0.07     0.00     0.28         0.    0.3325
    4   27    0     0.28     0.07     0.00     0.28         0.    0.1663
    5   11    0     0.37     0.32     0.00     0.37         0.    0.5109
    6   15    0     1.17     0.71     0.00     1.17         0.    1.5078
    7   16    0     1.21     0.64     0.01     1.21         0.    1.5138
    8    1    0     0.09     0.08     0.00     0.09         0.    0.1127
    9    5    0     0.86     0.59     0.00     0.86         0.    1.2926
   10    6    0     0.38     0.22     0.00     0.38         0.    0.2259
   11   18    0     0.37     0.21     0.00     0.37         0.    0.2259
   12    7    0     0.30     0.17     0.00     0.30         0.    0.1265
   13   19    0     0.29     0.17     0.00     0.29         0.    0.1265
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0633
   15   43    0     0.01     0.00     0.00     0.01         0.    0.0633
   16   44    0     0.02     0.00     0.00     0.02         0.    0.0633
   17   75    0     0.01     0.01     0.00     0.01         0.    0.0202
   18   76    0     0.05     0.04     0.00     0.05         0.    0.1474
   19   77    0     0.02     0.01     0.00     0.02         0.    0.0516
   20   78    0     0.03     0.14     0.00     0.03         0.    0.0075
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.119999
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     5.7800s 
time spent in multnx:                   5.7800s 
integral transfer time:                 0.0100s 
time spent for loop construction:       3.4700s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            5.9000s 

 mraqcc   convergence criteria satisfied after 10 iterations.

 final mraqcc   convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  # 10  1   -383.3662624618  8.5428E-07  4.2763E-07  8.7152E-04  1.0000E-03
 mraqcc  # 10  2   -383.0653698749  1.9959E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 10  3   -382.7439674651  5.8158E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 10  4   -381.3724079421 -8.1596E-01  0.0000E+00  1.5560E+00  1.0000E-04

####################CIUDGINFO####################

   aqcc vector at position   1 energy= -383.366262461808

################END OF CIUDGINFO################

 
 a4den factor =  1.040340036 for root   1
diagon:itrnv=   0
    1 of the   5 expansion vectors are transformed.
    1 of the   4 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
maximum overlap with reference    1(overlap= 0.96776)
reference energy =                        -383.3118404911
total aqcc energy =                       -383.3662624618
diagonal element shift (lrtshift) =         -0.0544219707

 information on vector: 1 from unit 11 written to unit 48 filename civout              


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =      -383.3662624618

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7    8    9

                                          orbital     1    2    3    7    8   11   12   17   18

                                         symmetry   b1u  b1u  b1u  b2g  b2g  b3g  b3g  au   au 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1  0.023714                        +-   +-   +-   +-   +-                     
 z*  1  1       2 -0.056602                        +-   +-   +-   +-        +-                
 z*  1  1       5 -0.057329                        +-   +-   +-   +-                  +-      
 z*  1  1      23 -0.018024                        +-   +-   +-             +-        +-      
 z*  1  1      34  0.143413                        +-   +-   +    +-    -   +          -      
 z*  1  1      43 -0.011960                        +-   +-   +     -   +-   +               - 
 z*  1  1      46  0.047584                        +-   +-   +     -        +-   +     -      
 z*  1  1      50 -0.044003                        +-   +-   +     -        +         +-    - 
 z*  1  1      58 -0.023649                        +-   +-   +    +         +-    -    -      
 z*  1  1      62 -0.035255                        +-   +-   +    +          -        +-    - 
 z*  1  1      82 -0.131234                        +-   +-        +-   +-   +-                
 z*  1  1      85 -0.058600                        +-   +-        +-   +-             +-      
 z*  1  1      88 -0.030157                        +-   +-        +-        +-   +-           
 z*  1  1      89  0.890789                        +-   +-        +-        +-        +-      
 z*  1  1      91 -0.031666                        +-   +-        +-        +-             +- 
 z*  1  1      93 -0.031886                        +-   +-        +-        +     -   +     - 
 z*  1  1      95  0.027738                        +-   +-        +-        +    +     -    - 
 z*  1  1      96 -0.017662                        +-   +-        +-             +-   +-      
 z*  1  1      99 -0.017828                        +-   +-        +-                  +-   +- 
 z*  1  1     102 -0.081211                        +-   +-        +     -   +-        +     - 
 z*  1  1     104 -0.056520                        +-   +-        +     -   +     -   +-      
 z*  1  1     112  0.066656                        +-   +-        +    +    +-         -    - 
 z*  1  1     113  0.024413                        +-   +-        +    +     -    -   +-      
 z*  1  1     120 -0.035719                        +-   +-             +-   +-        +-      
 z*  1  1     122  0.012830                        +-   +-             +-   +-             +- 
 z*  1  1     131 -0.042856                        +-   +-                  +-   +-   +-      
 z*  1  1     134 -0.040771                        +-   +-                  +-        +-   +- 
 z*  1  1     139 -0.014692                        +-   +    +-   +-    -        +     -      
 z*  1  1     186  0.020950                        +-   +     -   +-   +-   +     -           
 z*  1  1     193 -0.052919                        +-   +     -   +-        +-        +     - 
 z*  1  1     195 -0.080331                        +-   +     -   +-        +     -   +-      
 z*  1  1     204 -0.031260                        +-   +     -   +     -   +-        +-      
 z*  1  1     208  0.018311                        +-   +     -   +     -   +     -   +     - 
 z*  1  1     242  0.022490                        +-   +    +    +-        +-         -    - 
 z*  1  1     243  0.018873                        +-   +    +    +-         -    -   +-      
 z*  1  1     250  0.032013                        +-   +    +     -    -   +-        +-      
 z*  1  1     256  0.020130                        +-   +    +     -    -   +    +     -    - 
 z*  1  1     285 -0.137102                        +-   +         +-    -   +-   +     -      
 z*  1  1     289  0.049401                        +-   +         +-    -   +         +-    - 
 z*  1  1     293 -0.044400                        +-   +         +-   +    +-    -    -      
 z*  1  1     297 -0.028220                        +-   +         +-   +     -        +-    - 
 z*  1  1     302  0.021917                        +-   +          -   +-   +-   +          - 
 z*  1  1     309  0.100884                        +-   +          -        +-   +    +-    - 
 z*  1  1     321  0.013734                        +-   +         +         +-    -   +-    - 
 z*  1  1     326 -0.012020                        +-   +               -   +-   +     -   +- 
 z*  1  1     340 -0.016378                        +-        +-   +-        +-        +-      
 z*  1  1     390  0.018184                        +-        +    +-    -   +    +-    -      
 z*  1  1     436  0.021988                        +-             +-   +-   +-   +-           
 z*  1  1     437 -0.034289                        +-             +-   +-   +-        +-      
 z*  1  1     448 -0.089810                        +-             +-        +-   +-   +-      
 z*  1  1     451 -0.040585                        +-             +-        +-        +-   +- 
 z*  1  1     455  0.015494                        +-             +     -   +-   +-   +     - 
 z*  1  1     460 -0.010235                        +-             +    +    +-   +-    -    - 
 z*  1  1     468  0.011530                        +-                       +-   +-   +-   +- 
 z   1  1     524  0.023127                        +    +-    -   +-        +-        +-      
 z   1  1     527  0.039464                        +    +-    -   +-        +     -   +-      
 z   1  1     574  0.032902                        +    +-   +    +-        +-         -    - 
 z   1  1     582  0.025827                        +    +-   +     -    -   +-        +-      
 z   1  1     617  0.051308                        +    +-        +-    -   +-   +     -      
 z   1  1     621  0.011803                        +    +-        +-    -   +         +-    - 
 z   1  1     625 -0.022757                        +    +-        +-   +    +-    -    -      
 z   1  1     629 -0.027851                        +    +-        +-   +     -        +-    - 
 z   1  1     653 -0.014642                        +    +-        +         +-    -   +-    - 
 z   1  1     780  0.028903                        +     -        +-        +-   +-   +-      
 z   1  1    1064 -0.031186                             +-   +-   +-        +-        +-      
 z   1  1    1161 -0.012265                             +-        +-   +-   +-        +-      
 z   1  1    1172 -0.018062                             +-        +-        +-   +-   +-      
 y   1  1    1582  0.013317              1( b2g)   +-   +-    -   +-        +          -      
 y   1  1    1656  0.025662              1( b3g)   +-   +-    -   +         +-         -      
 y   1  1    1657 -0.012653              2( b3g)   +-   +-    -   +         +-         -      
 y   1  1    2151 -0.010310              1( b2g)   +-   +-        +-    -   +-                
 y   1  1    2207  0.016670              1( b3g)   +-   +-        +-         -        +-      
 y   1  1    2223  0.020556              1( b3g)   +-   +-        +-        +          -    - 
 y   1  1    2224 -0.010634              2( b3g)   +-   +-        +-        +          -    - 
 y   1  1    2231 -0.010137              1( b3g)   +-   +-        +-              -   +-      
 y   1  1    2291 -0.024046              1( b3g)   +-   +-         -   +     -        +-      
 y   1  1    2415  0.015573              1( b3g)   +-   +-        +     -    -        +-      
 y   1  1    2416 -0.010206              2( b3g)   +-   +-        +     -    -        +-      
 y   1  1    3074 -0.020343              1( b3g)   +-    -   +    +-         -        +-      
 y   1  1    3610  0.037097              1( b3g)   +-    -        +-   +    +-         -      
 y   1  1    4422 -0.024708              1( b3g)   +-   +     -   +-         -        +-      
 y   1  1    5178  0.020138              1( b3g)   +-   +         +-    -   +-         -      
 y   1  1    5248  0.013907              1( b2g)   +-   +         +-        +-    -    -      
 y   1  1    7329 -0.013554              1( b2g)   +-             +-    -   +-        +-      
 y   1  1    7411 -0.020687              1( b3g)   +-             +-        +-    -   +-      
 y   1  1    8732 -0.024507              1( b3g)    -   +-        +-   +    +-         -      
 y   1  1    8797 -0.010116              1( b1u)    -   +-        +-        +-        +-      
 y   1  1   12701 -0.011169              1( b3g)   +    +-   +-   +-    -              -      
 y   1  1   12984  0.017419              1( b3g)   +    +-    -   +-   +-    -                
 y   1  1   13052  0.076929              1( b3g)   +    +-    -   +-         -        +-      
 y   1  1   13053 -0.011154              2( b3g)   +    +-    -   +-         -        +-      
 y   1  1   13055  0.016478              4( b3g)   +    +-    -   +-         -        +-      
 y   1  1   13808 -0.069619              1( b3g)   +    +-        +-    -   +-         -      
 y   1  1   13809  0.022921              2( b3g)   +    +-        +-    -   +-         -      
 y   1  1   13811 -0.011807              4( b3g)   +    +-        +-    -   +-         -      
 y   1  1   13826 -0.010166              1( au )   +    +-        +-    -    -        +-      
 y   1  1   14080 -0.024557              1( b3g)   +    +-         -        +-        +-    - 
 y   1  1   15891  0.013649              1( b3g)   +     -        +-   +-   +-    -           
 y   1  1   16041  0.055698              1( b3g)   +     -        +-        +-    -   +-      
 y   1  1   16044  0.011606              4( b3g)   +     -        +-        +-    -   +-      
 y   1  1   16725  0.011587              1( b3g)   +    +     -   +-    -    -   +     -      
 y   1  1   20775 -0.046632              1( b3g)        +-        +-        +-    -   +-      
 y   1  1   20778 -0.011165              4( b3g)        +-        +-        +-    -   +-      
 w   1  1   77984  0.010884    2( b3g)   1( au )   +-   +-        +-        +          -      
 w   1  1   78064 -0.024998    1( b3g)   1( b3g)   +-   +-        +-                  +-      
 w   1  1   78495 -0.010818    1( b2g)   1( au )   +-   +-        +         +-         -      
 w   1  1   85431 -0.010083    1( b1u)   1( b2g)   +-   +          -        +-        +-      
 w   1  1   85432 -0.011617    2( b1u)   1( b2g)   +-   +          -        +-        +-      
 w   1  1   93446 -0.010915    1( b3g)   1( b3g)   +-             +-        +-        +-      
 w   1  1  100083  0.013966    1( b1u)   1( b3g)   +    +-        +-         -        +-      
 w   1  1  100085 -0.016002    3( b1u)   1( b3g)   +    +-        +-         -        +-      
 w   1  1  100094 -0.010221    3( b1u)   4( b3g)   +    +-        +-         -        +-      
 w   1  1  108724  0.035813    1( b3g)   1( b3g)   +     -        +-        +-        +-      
 w   1  1  108730  0.013816    1( b3g)   4( b3g)   +     -        +-        +-        +-      
 w   1  1  132468 -0.012561    1( b3g)   1( b3g)        +-   +    +-    -   +          -      
 w   1  1  133524  0.010694    1( b3g)   1( b3g)        +-        +-   +-   +-                
 w   1  1  133672  0.010267    1( b1u)   3( b1u)        +-        +-        +-        +-      
 w   1  1  133678 -0.061759    1( b3g)   1( b3g)        +-        +-        +-        +-      
 w   1  1  133684 -0.023621    1( b3g)   4( b3g)        +-        +-        +-        +-      

 ci coefficient statistics:
           rq > 0.1                5
      0.1> rq > 0.01             114
     0.01> rq > 0.001           1857
    0.001> rq > 0.0001         16210
   0.0001> rq > 0.00001        55367
  0.00001> rq > 0.000001       54645
 0.000001> rq                  13830
           all                142028
  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.023714477620
     2     2     -0.056602248388
     3     3      0.007409600835
     4     4      0.003103411944
     5     5     -0.057328803345
     6     6      0.000376515821
     7     7      0.003122731306
     8     8      0.001587792155
     9     9      0.007534469740
    10    10     -0.001155835130
    11    11     -0.000204082799
    12    12      0.008879948852
    13    13      0.000507348787
    14    14     -0.004034383161
    15    15     -0.007059733960
    16    16      0.004826104586
    17    17     -0.000997146105
    18    18     -0.001979564940
    19    19      0.003468414351
    20    20     -0.000434535018
    21    21     -0.002277408829
    22    22      0.005196842300
    23    23     -0.018023529143
    24    24      0.001430326435
    25    25      0.002912458286
    26    26      0.002563293646
    27    27     -0.000246511617
    28    28     -0.000637545241
    29    29     -0.002924934484
    30    30      0.002881864161
    31    31      0.000093850343
    32    32     -0.000455362466
    33    33      0.004921038289
    34    34      0.143412614035
    35    35      0.000606291330
    36    36      0.000255032425
    37    37     -0.006307162050
    38    38      0.006013713517
    39    39      0.003595258423
    40    40      0.001844595428
    41    41      0.000559843093
    42    42     -0.001331161292
    43    43     -0.011959767900
    44    44     -0.008873458260
    45    45     -0.001075944240
    46    46      0.047583852762
    47    47      0.003792545499
    48    48     -0.003999586961
    49    49      0.001888672668
    50    50     -0.044003065687
    51    51     -0.001514110291
    52    52     -0.000473923241
    53    53     -0.001465805685
    54    54     -0.003308002917
    55    55     -0.009240896725
    56    56      0.003582658198
    57    57      0.000153048135
    58    58     -0.023648886122
    59    59      0.000647345264
    60    60      0.001452396247
    61    61      0.001597165956
    62    62     -0.035254795177
    63    63      0.001518598666
    64    64      0.001240447592
    65    65      0.001689606244
    66    66     -0.003693272817
    67    67     -0.002938044180
    68    68     -0.008819772043
    69    69     -0.000527197536
    70    70      0.001204410597
    71    71      0.009441644471
    72    72      0.001602565426
    73    73      0.000245179118
    74    74      0.000868835167
    75    75      0.003404816007
    76    76      0.000725473852
    77    77     -0.000341260030
    78    78     -0.001053921058
    79    79      0.001997577080
    80    80      0.002511614639
    81    81      0.000270733515
    82    82     -0.131233878401
    83    83     -0.008305864069
    84    84      0.003862647126
    85    85     -0.058599784910
    86    86     -0.001116979629
    87    87      0.004676412564
    88    88     -0.030157458407
    89    89      0.890788909141
    90    90     -0.004199625425
    91    91     -0.031665978658
    92    92     -0.001025572547
    93    93     -0.031886006402
    94    94     -0.001141979776
    95    95      0.027738208285
    96    96     -0.017661543410
    97    97     -0.001204361685
    98    98      0.002084279134
    99    99     -0.017827902356
   100   100      0.006009424122
   101   101     -0.000273569140
   102   102     -0.081211245222
   103   103     -0.003184924099
   104   104     -0.056519672093
   105   105     -0.005156621733
   106   106      0.004186025018
   107   107      0.001912324700
   108   108     -0.000216708091
   109   109      0.003245941739
   110   110      0.000360852969
   111   111     -0.001857379445
   112   112      0.066656451121
   113   113      0.024412928660
   114   114      0.003284255154
   115   115     -0.001412784460
   116   116      0.002579812246
   117   117      0.001291688971
   118   118     -0.001758306621
   119   119      0.007698037107
   120   120     -0.035718603860
   121   121      0.003084608462
   122   122      0.012829583320
   123   123      0.000202039227
   124   124      0.006636491534
   125   125      0.001271551633
   126   126     -0.001156930794
   127   127      0.005802351785
   128   128      0.000673337913
   129   129     -0.000718192141
   130   130      0.003057081360
   131   131     -0.042856052167
   132   132     -0.003105836042
   133   133      0.002645484727
   134   134     -0.040770599358
   135   135     -0.000650756709
   136   136      0.001496627501
   137   137     -0.001186716125
   138   138     -0.008591928737
   139   139     -0.014692245040
   140   140     -0.000064522986
   141   141     -0.001489696336
   142   142      0.003690229300
   143   143     -0.004728050706
   144   144     -0.001179903671
   145   145     -0.006771785937
   146   146      0.001043811590
   147   147      0.000218357720
   148   148      0.004668161429
   149   149      0.000222153536
   150   150     -0.007715841121
   151   151     -0.002106537260
   152   152      0.000855625239
   153   153      0.000302251673
   154   154      0.001466828233
   155   155      0.008439455574
   156   156     -0.000296855683
   157   157      0.000518534342
   158   158     -0.000062955636
   159   159      0.001067544910
   160   160      0.000687643551
   161   161     -0.001329803342
   162   162      0.000602543886
   163   163      0.002562147162
   164   164      0.000237119575
   165   165     -0.000839874510
   166   166      0.002384831743
   167   167      0.002513595365
   168   168     -0.000350568466
   169   169      0.003670971675
   170   170      0.000171691218
   171   171     -0.000296803437
   172   172      0.001561504297
   173   173     -0.001749395159
   174   174     -0.000299952305
   175   175      0.000245667478
   176   176     -0.001914860279
   177   177      0.001526936254
   178   178     -0.000130272252
   179   179     -0.000110251719
   180   180     -0.000836497617
   181   181      0.001805043233
   182   182     -0.000247850719
   183   183      0.000253341227
   184   184     -0.001016089160
   185   185      0.008374745564
   186   186      0.020950036580
   187   187      0.001374504776
   188   188     -0.006505570345
   189   189      0.007600442405
   190   190      0.000933896830
   191   191     -0.006167803002
   192   192     -0.004433320462
   193   193     -0.052919465580
   194   194      0.000179674469
   195   195     -0.080331123538
   196   196      0.003209231525
   197   197      0.004774875105
   198   198      0.001570617630
   199   199      0.000010421196
   200   200      0.004096483829
   201   201      0.000321542607
   202   202     -0.002494370793
   203   203     -0.002178604921
   204   204     -0.031259852592
   205   205      0.005503894507
   206   206      0.006157776937
   207   207     -0.001693323013
   208   208      0.018311040931
   209   209      0.000540153593
   210   210     -0.004056720439
   211   211      0.005763442611
   212   212      0.000324784670
   213   213     -0.000931658678
   214   214     -0.000163143981
   215   215     -0.002913226522
   216   216      0.001436757487
   217   217     -0.003824560152
   218   218     -0.000520040040
   219   219     -0.005600635539
   220   220     -0.006745633182
   221   221     -0.000256843949
   222   222      0.000246524160
   223   223      0.001476225188
   224   224      0.004200703468
   225   225     -0.001213987622
   226   226      0.005237161452
   227   227     -0.000702202634
   228   228     -0.003209751039
   229   229     -0.000722805824
   230   230      0.000524343152
   231   231     -0.001888296015
   232   232     -0.000380409878
   233   233      0.000618435842
   234   234     -0.002214434432
   235   235      0.005260081613
   236   236      0.000814839782
   237   237      0.000770315583
   238   238      0.006454815814
   239   239      0.000320456567
   240   240     -0.005136356303
   241   241     -0.004293145545
   242   242      0.022489877385
   243   243      0.018872676169
   244   244      0.001592582530
   245   245     -0.001086792266
   246   246      0.001697472153
   247   247     -0.003546883114
   248   248     -0.000663619652
   249   249     -0.006961892661
   250   250      0.032013498733
   251   251     -0.001341648480
   252   252     -0.001146082085
   253   253     -0.001443988466
   254   254     -0.004185093404
   255   255      0.000798244530
   256   256      0.020130062389
   257   257     -0.000018119672
   258   258     -0.000041934986
   259   259      0.000977330181
   260   260     -0.003143718199
   261   261     -0.000415998688
   262   262      0.001566363797
   263   263     -0.001428555683
   264   264     -0.000680181014
   265   265      0.000356094827
   266   266     -0.002295296718
   267   267      0.000247671634
   268   268     -0.002964222076
   269   269     -0.003476768416
   270   270     -0.004052205486
   271   271      0.000287922653
   272   272     -0.004186950669
   273   273     -0.001121692649
   274   274      0.000434916005
   275   275      0.003053203449
   276   276     -0.001621695698
   277   277     -0.001157022607
   278   278      0.000163952074
   279   279      0.001220363692
   280   280     -0.000224563465
   281   281      0.000772283828
   282   282      0.000766614963
   283   283     -0.002478693662
   284   284     -0.002818465703
   285   285     -0.137102425309
   286   286     -0.004666855525
   287   287     -0.003851722494
   288   288     -0.002848102157
   289   289      0.049401103037
   290   290      0.004052560078
   291   291      0.000499042789
   292   292      0.003522565523
   293   293     -0.044400306057
   294   294     -0.007161256478
   295   295      0.000592627164
   296   296      0.001624020110
   297   297     -0.028220187369
   298   298     -0.001051957251
   299   299      0.001556277683
   300   300      0.001551061156
   301   301      0.005658382916
   302   302      0.021916733028
   303   303      0.003254559917
   304   304      0.001534967504
   305   305     -0.003918027795
   306   306     -0.003256684780
   307   307     -0.008170251971
   308   308     -0.000118181779
   309   309      0.100883522867
   310   310     -0.003566104049
   311   311      0.000740462787
   312   312     -0.004665616727
   313   313      0.006560973148
   314   314      0.006239587212
   315   315      0.004068671890
   316   316      0.000882318505
   317   317     -0.000713334898
   318   318      0.003869516028
   319   319      0.000776198626
   320   320      0.000231422449
   321   321      0.013734246150
   322   322     -0.004241038600
   323   323      0.001690108759
   324   324     -0.000155523656
   325   325      0.002872091798
   326   326     -0.012020061361
   327   327     -0.005626543446
   328   328     -0.001099480000
   329   329      0.003763854798
   330   330     -0.006033053463
   331   331      0.002859936972
   332   332      0.000038850264
   333   333      0.003989301044
   334   334     -0.001530534904
   335   335     -0.003786085850
   336   336      0.003404041914
   337   337      0.001064828379
   338   338     -0.001951222305
   339   339      0.005574663028
   340   340     -0.016377542991
   341   341      0.000332892694
   342   342      0.005179804829
   343   343      0.000837295736
   344   344      0.005410086813
   345   345     -0.000652313283
   346   346      0.000087443064
   347   347      0.008942475340
   348   348     -0.000492167482
   349   349     -0.000768391962
   350   350      0.002772061965
   351   351      0.000335750077
   352   352      0.001343111149
   353   353      0.003051100011
   354   354     -0.000655116191
   355   355      0.003981320217
   356   356     -0.000507735066
   357   357     -0.001761843060
   358   358      0.000134156041
   359   359      0.000282500490
   360   360     -0.002637703108
   361   361      0.000068891382
   362   362     -0.000145865092
   363   363     -0.001681852533
   364   364     -0.000785644169
   365   365     -0.000414205974
   366   366      0.000747626038
   367   367      0.000139603238
   368   368      0.000295088501
   369   369      0.001522667827
   370   370     -0.001158770434
   371   371      0.002269578768
   372   372     -0.000322551356
   373   373     -0.000709401026
   374   374     -0.000421178015
   375   375     -0.000932480639
   376   376      0.000398250671
   377   377      0.001065667498
   378   378     -0.000849065100
   379   379     -0.000017198785
   380   380      0.000970995896
   381   381     -0.000516516664
   382   382      0.002787828638
   383   383     -0.000137674253
   384   384     -0.001172878219
   385   385      0.001442901840
   386   386     -0.000331431149
   387   387     -0.001346323609
   388   388      0.004011845386
   389   389      0.003070570304
   390   390      0.018183723457
   391   391     -0.000810630553
   392   392      0.003937912887
   393   393     -0.008530375231
   394   394     -0.002233747449
   395   395      0.000310256200
   396   396      0.000250366234
   397   397      0.003848189810
   398   398      0.003091247492
   399   399      0.000920640454
   400   400     -0.001219532015
   401   401      0.000759972849
   402   402      0.002784950169
   403   403      0.000065693273
   404   404     -0.003689703444
   405   405     -0.001641768356
   406   406      0.000555147130
   407   407     -0.003351025253
   408   408      0.002285936353
   409   409      0.000666471236
   410   410     -0.000750446648
   411   411      0.001604613327
   412   412     -0.001462123273
   413   413     -0.005213108151
   414   414     -0.008549856288
   415   415      0.000443274170
   416   416      0.001629125720
   417   417     -0.000758339260
   418   418     -0.000959291371
   419   419     -0.002008548796
   420   420      0.001891359340
   421   421      0.000284143076
   422   422     -0.000295468514
   423   423     -0.000877515766
   424   424     -0.001417162054
   425   425      0.002543817580
   426   426     -0.005126821363
   427   427      0.000438610222
   428   428     -0.005042054497
   429   429      0.001112921301
   430   430     -0.000357866690
   431   431      0.003314263909
   432   432     -0.000154863660
   433   433      0.000058525180
   434   434     -0.000341719750
   435   435      0.000460612156
   436   436      0.021987565530
   437   437     -0.034289469414
   438   438     -0.005869969538
   439   439      0.007518995151
   440   440      0.006568036491
   441   441     -0.002049075582
   442   442     -0.000235204644
   443   443     -0.007277481630
   444   444      0.005879586981
   445   445     -0.000549215193
   446   446     -0.001135724519
   447   447      0.005444656606
   448   448     -0.089810373287
   449   449      0.006582972775
   450   450      0.004906176065
   451   451     -0.040584645369
   452   452      0.002459408874
   453   453      0.002873896448
   454   454     -0.005039202410
   455   455      0.015494180989
   456   456     -0.000344491142
   457   457      0.003731729499
   458   458      0.005259076104
   459   459      0.000160075879
   460   460     -0.010234503623
   461   461     -0.002824701137
   462   462      0.005698396857
   463   463      0.000081576646
   464   464     -0.003720784749
   465   465      0.002897518438
   466   466     -0.000915177556
   467   467     -0.001262410379
   468   468      0.011529786242

 number of reference csfs (nref) is   468.  root number (iroot) is  1.
 c0**2 =   0.93768172c0**2(scaled) =   0.99611643  c**2 (all zwalks) =   0.95019351
 passed aftci ... 
 readint2: molcas,dalton2=                     0                     0
 files%faoints=aoints              
lodens (list->root)=  1
                       Size (real*8) of d2temp for two-external contributions       2046
 
                       Size (real*8) of d2temp for all-internal contributions        360
                       Size (real*8) of d2temp for one-external contributions       1365
                       Size (real*8) of d2temp for two-external contributions       2046
size_thrext:  lsym   l1    ksym   k1strt   k1       cnt3 
                5    1    5    1    4       21
                5    1    6    1    3       27
                5    1    7    1    5       90
                5    1    8    1    3       75
                5    2    5    1    4       21
                5    2    6    1    3       27
                5    2    7    1    5       90
                5    2    8    1    3       75
                5    3    5    1    4       21
                5    3    6    1    3       27
                5    3    7    1    5       90
                5    3    8    1    3       75
                6    4    6    1    3       42
                6    4    7    1    5       60
                6    4    8    1    3       90
                6    5    6    1    3       42
                6    5    7    1    5       60
                6    5    8    1    3       90
                7    6    7    1    5      156
                7    6    8    1    3       72
                7    7    7    1    5      156
                7    7    8    1    3       72
                8    8    7    1    5       72
                8    8    8    1    3      120
                8    9    7    1    5       72
                8    9    8    1    3      120
                       Size (real*8) of d2temp for three-external contributions       1863
                       Size (real*8) of d2temp for four-external contributions        819
 enough memory for temporary d2 elements on vdisk ... 
location of d2temp files... fileloc(dd012)=       1
location of d2temp files... fileloc(d3)=       1
location of d2temp files... fileloc(d4)=       1
 files%dd012ext =  unit=  22  vdsk=   1  filestart=       1
 files%d3ext =     unit=  23  vdsk=   1  filestart=    5363
 files%d4ext =     unit=  24  vdsk=   1  filestart=   65363
            0xdiag    0ext      1ext      2ext      3ext      4ext
d2off                   767      1533      3065         1         1
d2rec                     1         2         3         1         1
recsize                 766       766       766     60000     60000
d2bufferlen=          60000
maxbl3=               60000
maxbl4=               60000
  allocated                 125363  DP for d2temp 
sifcfg setup: record length 4096 DP
# d1 elements per record  3272
# d2 elements per record  2730
  The AQCC density will be calculated.
 item #                     1 suffix=:.drt1.state1:
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method:  3 last record  1max overlap with ref# 97% root-following 0
 MR-AQCC energy:  -383.36626246    -8.88183495
 residuum:     0.00087152
 deltae:     0.00000085
 apxde:     0.00000043
 a4den:     1.04034004
 reference energy:  -383.31184049    -8.82741298

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.96776154     0.09738676    -0.16920463     0.03830596     0.02273388    -0.00188625     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.96776154     0.09738676    -0.16920463     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 rdcirefv: extracting reference vector # 1(blocksize=  1368 #zcsf=    1368)
 =========== Executing IN-CORE method ==========
================================================================================
   DYZ=       0  DYX=       0  DYW=       0
   D0Z=    3798  D0Y=       0  D0X=       0  D0W=       0
  DDZI=    9312 DDYI=       0 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
adding (  1) 2.00to den1(    1)=   0.0000000
adding (  2) 0.00to den1(    3)=   1.9240273
adding (  3) 0.00to den1(    6)=   0.0655251
adding (  4) 0.00to den1(   22)=   1.9431536
adding (  5) 0.00to den1(   24)=   0.1289925
adding (  6) 0.00to den1(   32)=   1.9332890
adding (  7) 0.00to den1(   34)=   0.0758440
adding (  8) 0.00to den1(   53)=   1.8743192
adding (  9) 0.00to den1(   55)=   0.0548493
 root #                      1 : Scaling(2) with    1.04034003594393      
 corresponding to ref #                      1
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=   20847  DYX=   64375  DYW=   66147
   D0Z=    3798  D0Y=   29412  D0X=    9870  D0W=   10500
  DDZI=    9312 DDYI=   57044 DDXI=   21280 DDWI=   22500
  DDZE=       0 DDYE=    8660 DDXE=    3704 DDWE=    3836
================================================================================
adding (  1) 2.00to den1(    1)=  -0.0575182
adding (  2) 0.00to den1(    3)=   1.9011085
adding (  3) 0.00to den1(    6)=   0.0822413
adding (  4) 0.00to den1(   22)=   1.9393672
adding (  5) 0.00to den1(   24)=   0.1443598
adding (  6) 0.00to den1(   32)=   1.9140616
adding (  7) 0.00to den1(   34)=   0.0995346
adding (  8) 0.00to den1(   53)=   1.8561730
adding (  9) 0.00to den1(   55)=   0.0571607
Trace of MO density:    10.000000
   10  correlated and     0  frozen core electrons

Natural orbital populations,block 5
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO
  occ(*)=     1.95962914     1.88502413     0.08185336     0.00315256     0.00209737     0.00054551

Natural orbital populations,block 6
              MO     1       MO     2       MO     3       MO     4       MO
  occ(*)=     1.93937058     0.14438241     0.00268127     0.00064984

Natural orbital populations,block 7
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO
  occ(*)=     1.91454761     0.11314375     0.03411876     0.00227222     0.00073700     0.00017285

Natural orbital populations,block 8
              MO     1       MO     2       MO     3       MO     4       MO
  occ(*)=     1.85620120     0.05719020     0.00193676     0.00029349


 total number of electrons =   10.0000000000

 test slabel:                     8
  ag  b3u b2u b1g b1u b2g b3g au 


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        b1u partial gross atomic populations
   ao class       1b1u       2b1u       3b1u       4b1u       5b1u       6b1u
    to_ s       1.959629   1.885024   0.081853   0.003153   0.002097   0.000546

                        b2g partial gross atomic populations
   ao class       1b2g       2b2g       3b2g       4b2g
    to_ s       1.939371   0.144382   0.002681   0.000650

                        b3g partial gross atomic populations
   ao class       1b3g       2b3g       3b3g       4b3g       5b3g       6b3g
    to_ s       1.914548   0.113144   0.034119   0.002272   0.000737   0.000173

                        au  partial gross atomic populations
   ao class       1au        2au        3au        4au 
    to_ s       1.856201   0.057190   0.001937   0.000023


                        gross atomic populations
     ao           to_
      s         9.999729
    total       9.999729
 

 Total number of electrons:    9.99972932

========================GLOBAL TIMING PER NODE========================
   process    vectrn  integral   segment    diagon     dmain    davidc   finalvw   driver  
--------------------------------------------------------------------------------
   #   1         0.0       0.0       0.0      58.9      59.0       0.0       0.0      59.2
--------------------------------------------------------------------------------
       1         0.0       0.0       0.0      58.9      59.0       0.0       0.0      59.2
 DA ...
