#!/usr/bin/env colperl
#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/

#**************************************************************************
#  performs calculations according to the displfl file
#  (setup in colinp)  
#
#  Program reads the file DISPLACEMENT/displfl, which has the following
#  structure:
#
#  N /number of internal coordinates
#  yes/no /calculate dip.mom.derivatives
#  yes/no /calculate reference point
#  coordinate1 displacement1+
#  coordinate1 displacement1-
#        :
#        :
#  coordinateN displacementN+
#  coordinateN displacementN-
#
#  at the end of the lines defining the coordinates and displacements
#  the following additional key words may be added:
#  skip - used for all '+' displacement in case numerical differentiation
#         is done relatively to the reference point only
#  fixc - used for all displacements of one coordinate, in order to skip the
#         calculation for this coordinate. If set, forceconst.pl program assumes
#         1 at the diagonal for the force constant matrix. This option
#         may be useful if user wants to calculate the some frequency
#         components in higher symmetry, and skip contributions from
#         displacements which are not present in the given point group.
#**************************************************************************
#
  use lib join('/',$ENV{"COLUMBUS"},"CPAN") ;
  use colib_perl;
  use Shell qw( date pwd );
  use File::Copy 'cp';  # use perl cp
#
#  GLOBAL:ERROR CODES
#
   $ERRCODE[2]  ='general error';
   $ERRCODE[10] ='input file missing';
   $ERRCODE[11] ='abnormal program termination';
   $ERRCODE[101]='calculation not konverged';
   $ERRCODE[201]='cigrd error: inconsistent energies';
   $ERRCODE[301]='rgf error';
#
#  $JDIR   absolute path to job directory
  $JDIR = pwd();             # job directory
  $JDIR =~ s/\n//;
  $MODIR = "MOCOEFS";         # MO coefficient files
  $modir ="$JDIR/$MODIR";
  if (!-e "$modir") { mkdir("$modir",0755);}
#
$scriptversion="Version 0.8, 17_October_2007 (md, revised tm)";
$cbus = $ENV{"COLUMBUS"};
#
# default values for corememory size and vdisk size
   print "\n\n";
   print " Script name: calc.pl\n\n";
   print " version: $scriptversion\n";
   print " comments: Preforms calculations according the list in file: DISPLACEMENT/displfl\n"; 
   print "           This script is called in the form: nohup \$COLUMBUS/calc.pl >& runls & \n";
   print "           Current COLUMBUS setting is: $cbus \n";
  
  $coremem = 300 ;
  $vdisk = 0 ;   
  $morestart=0;
  $cirestart_input= 0;
  $i=0;
  $clean="ints";
  while ( $i <= $#ARGV )
  { $_=$ARGV[$i];
    if (/^-m$/) { $i++; $coremem = $ARGV[$i]; $i++;}
    elsif (/^-morestart$/) { $morestart= 1 ; $i++;}
    elsif (/^-cirestart$/) { $cirestart_input= 1 ; $i++;}
    elsif (/^-clean$/) { $i++; $clean=$ARGV[$i]; $i++;}
    elsif (/^-nproc$/) { $i++; $nproc=$ARGV[$i]; $i++;}
    elsif (/^-help$/) { 
   print " usage calc.pl [-m corememory]  [-nproc ncpu] [-morestart]   [-cirestart] [-clean (work|ints)] \n \n";
   print "  -morestart  activate mcscf restart from previous mo coefficients \n";
   print "  -cirestart activate ci restart from previous ci vector file  \n";
   print "  -clean work remove WORK directory \n";
   print "  -clean ints remove the files fil* aoints* moints* fil* cihvfl from WORK directory\n";
   print "  -nproc number of processes \n";
   die();
    }
    else { print ("invalid argument $_ \n"), $i++;}
  }
  print "Input parameter:\n";
  print "corememory: $coremem  nproc:$nproc ¸\n";
  print "morestart=$morestart cirestart=$cirestart_input clean=$clean\n";

# 
#
  open(DISPLFL,"$JDIR/DISPLACEMENT/displfl") or
   die " \n\n $JDIR/DISPLACEMENT/displfl file not found, make input first!\n\n";
#  skip the first two lines  
   @dispflcontent=<DISPLFL>;
   close DISPLFL;
   shift @dispflcontent;
   shift @dispflcontent;
   $refcalc=shift @dispflcontent;
   chomp ($refcalc);
   $refcalc=~s/\W.*//;  
   open CURVELOG,">$JDIR/curve.log.$$";
   print CURVELOG "---------------dispfl content-------------------\n",@dispflcontent,"---------------------------------------------------\n";
  
   
#
#
##  Input consistency check
#
     if (($refcalc eq "yes") && ($morestart==1))
      { die " Reference point calculation not allowed in combination with MOCOEF restart mode\n";}
#
     if ($morestart==1)
      {
       print "\n  ####   NOTE:  MOCOEF restart mode active:  ###\n";
       print "    MOCOEF restart mode is active:\n";
       print "   For the first point, the mocoef.start will be used.\n";
       print "   For the following points, the mocoef files form the prewious calculations will be used.\n\n";
    
         if ( -s "mocoef.start" ) {cp ("mocoef.start", "mocoef_prev") }
         if ( -s "molcas.RasOrb.start" ) {cp ("molcas.RasOrb.start", "molcas.RasOrb_prev")}
         if (! (-s "mocoef.start" || -s "molcas.RasOrb.start" )) 
          {
           print "   No starting mocoef found at: $JDIR\n";
           print "   Please copy the mocoef for the first point to the file: mocoef.start\n";
           die "   !!! Input error !!! \n";
          }
      }
       open (NIN,"control.run") or die" File: control.run not found!\n";
        $cirestart=0;$ciudgav=0;
        while(<NIN>)
         { if (/ciudg/ && $cirestart_input){$cirestart=1;}
           if (/ciudgav/ && $cirestart_input) {$ciudgav=1;}
         }
       close NIN;
#
#  perform reference point calculation
#
     if ($refcalc eq "yes")
      {
#
#   check if this is a ci type calculation
#
      if ($ciudgav==1)
       {
        $/="/";
        open(CIDRTIN,"cidrtmsin");
        $_=<CIDRTIN>;
        s/\D//g;
        $ndrtci=$_;
        close CIDRTIN;
        print "ndtci:$ndrtci\n";
       }
#
       $dir="$JDIR/DISPLACEMENT/REFPOINT";
          if ( !-e $dir )
           {
            print "\n\n Directory: $dir is missing\n";
            print "     make input first\n";
            die "input error!!!\n";
           } # ( !-e $dir ) 
       chdir $dir;
       print "   -----------------------------------\n";
       print "    Performing REFERENCE POINT calculation at: $dir\n";
       $ERR=system(" $cbus/runc -m $coremem -nproc $nproc ");
       $ERR=$ERR/256;
       if (!$ERR ==0 ) {$!=$ERR;die" Error occured in runc: $ERRCODE[$ERR]\n";}
       if (-e "WORK/gdiisfl") {cp ("WORK/gdiisfl", "LISTINGS/.");}
#
       if($cirestart == 1)
        {
         print "\n  Restart modus for CI-type calculation will be activated for all displacement calculations\n";
         if ($ciudgav==1) 
          {
            for ($i=1;$i<=$ndrtci;$i++)
             {
              rename ("WORK/civfl.drt$i","../civfl.drt$i") or die" Cannot move file: civfl.drt$i\n";
              rename ("WORK/civout.drt$i","../civout.drt$i")or die" Cannot move file: civout.drt$i\n";
             }
          }
         else
          {
           rename ("WORK/civfl","../civfl") or die" Cannot move file: WORK/civfl \n";
           rename ("WORK/civout","../civout") or die " Cannot move file: WORK/civout \n";
           rename ("WORK/cirefv","../cirefv") or die " Cannot move file: WORK/cirefv \n";
          }
         cp ("MOCOEFS/mocoef_mc.sp", "../mocoef_restart") or die" Cannot copy file MOCOEFS/mocoef_mc.sp\n";
        }
#
      if ("$clean" eq "work") { system ("rm -rf WORK  ");}
              else   { chdir("WORK"); system("rm -f aoints* moints* fil* cihvfl "); chdir("../");}
      } # end of: if ($refcalc eq "yes")
#
#-------------------------------------------------------------------------------
#  preforme displacement calculations
#
   $/="\n";
   chdir $JDIR;
   while( $#dispflcontent >-1 )
    {
     $_=shift @dispflcontent;
     chomp;s/^ *//;
      print CURVELOG "Operating on task $_\n";
     ($coor,$displ,$flag)=split(/\s+/,$_,3);
        if (($flag ne "skip")&&($flag ne "fixc"))
	 {
          $dir="$JDIR/DISPLACEMENT/CALC.c$coor.d$displ";
           if ( -d "${dir}au") { $dir=$dir."au";}
            elsif ( -d "${dir}Ang" ) {$dir=$dir."Ang";}
             elsif ( -d "${dir}Deg" ) {$dir=$dir."Deg";}
              elsif (! -d "${dir}" ) {die("cannot find directory $dir\n");}
             if ( !-e $dir )
	      {
               print "\n\n Directory: $dir is missing\n";
               print "     make input first\n";
               die "input error!!!\n";
              } # ( !-e $dir )
          chdir $dir;
          print "   -----------------------------------\n";
          print "    Performing DISPLACEMENT CALCULATION\n";
	  print "    Int.coordinate: $coor, Displacement: $displ\n";
	  print "    Calculation in directory: $dir\n";
#
             if ($morestart == 1)
              {
               print "\n  ####   NOTE:  MOCOEF restart mode active:  ###\n";
               print "   Using mocoef_prev for the current calculation!!!\n\n";
               if ( -f "$JDIR/molcas.RasOrb_prev") { cp("$JDIR/molcas.RasOrb_prev","molcas.RasOrb"); }
               if ( -f "$JDIR/mocoef_prev") { cp("$JDIR/mocoef_prev","mocoef"); }
              }
             else
              {print "\n  ####   NOTE:  MOCOEF restart mode NOT active:  ###\n";}
#
#    change the restart mode if required
#
             if($cirestart == 1) 
              {
               if (! -e "WORK") {system ("mkdir WORK");}
               if ($ciudgav==1)
                {
                  for ($i=1;$i<=$ndrtci;$i++)
                   { # ivmode=1 restarts always directly from the old civfl/civout file  
                      if ( -f "../civfl.drt$i" ) {
                       &changekeyword("ciudgin.drt$i","ciudgin.drt$i"," IVMODE",4);
                       &changekeyword("ciudgin.drt$i","ciudgin.drt$i"," NBKITR",0);
		       $nroot=keyinfile("ciudgin.drt$i","nroot");
                       &changekeyword("ciudgin.drt$i","ciudgin.drt$i"," NOLDV",$nroot);
                       cp ("../civfl.drt$i","WORK/civfl.drt$i") or die" Cannot copy file: civfl.drt$i\n";
                       cp ("../cirefv.drt$i","WORK/cirefv.drt$i") or die" Cannot copy file: cirefv.drt$i\n";
                       cp ("../civout.drt$i","WORK/civout.drt$i") or die" Cannot copy file: civfl.drt$i\n"; }
                   }
                 }
               else
                 { if ( -f "../civfl" ) {
                      &changekeyword("ciudgin","ciudgin"," IVMODE",4);
                      &changekeyword("ciudgin","ciudgin"," NBKITR",0);
		      $nroot=keyinfile("ciudgin","nroot");
                      &changekeyword("ciudgin","ciudgin"," NOLDV",$nroot);
                      print "path= ", pwd(), "\n"; 
                      cp ("../civfl","WORK/civfl") or die" Cannot copy file:  civfl\n";
                      cp ("../cirefv","WORK/cirefv") or die" Cannot copy file: cirefv\n";
                      cp ("../civout","WORK/civout") or die" Cannot copy file: civout\n"; }
                 }
#
# possibly important for frequency calculations
# here it should not be necessary due to preceeding mcscf
#
#                cp ("../mocoef_restart", "mocoef") or die " Cannot copy file: ../mocoef_restart, $!\n";
              } # end of: if($cirestart == 1)
#
          print 'current directory=',pwd();
          if ($nproc > 1 ) { $ERR=system (" $cbus/runc -m $coremem  -nproc $nproc"); }
                 else      { $ERR=system (" $cbus/runc -m $coremem"); }
###########
#          chdir("WORK"); 
#          changekeyword("ciudgin","ciudgin"," GSET",3);
#          changekeyword("ciudgin","ciudgin"," NTYPE",3);
#          system("/software/mpich.pgf90/bin/mpirun -np 2 $cbus/pciudg.x -m $coremem"); 
#          system("mv ciudgls ../LISTINGS/ciudgls.aqcc.sp"); 
#          system("mv ciudgsm ../LISTINGS/ciudgsm.aqcc.sp"); 
#          system("mv nocoef_ci.1 ../MOCOEFS/nocoef_aqcc.drt1.state1.sp"); 
           chdir("$dir"); 
            print 'current directory2=',pwd();
##########
	  $ERR=$ERR/256;
	  if (!$ERR ==0 ) {$!=$ERR;die" Error occured in runc: $ERRCODE[$ERR]\n";}
            if ($morestart == 1)
             {
              print "   Creating mocoef_prev file in the main dir: $JDIR!!!\n\n";
              if ( -f  "MOCOEFS/mocoef_mc.sp" ) {cp ("MOCOEFS/mocoef_mc.sp", "$JDIR/mocoef_prev");} 
              if ( -f "WORK/molcas.RasOrb" ) { cp ("WORK/molcas.RasOrb", "$JDIR/molcas.RasOrb_prev"); }
             }
              if ( -f "MOCOEFS/mocoef_mc.sp" ) { cp ("MOCOEFS/mocoef_mc.sp", "$modir/mocoef.c$coor.d$displ"); }
              if ( -f "WORK/molcas.RasOrb" ) { cp ("WORK/molcas.RasOrb", "$modir/molcas.RasOrb.c$coor.d$displ"); }
            if ($cirestart_input == 1) { system("mv WORK/civfl* ../"); print " reusing civfl \n"; 
                                         system("mv WORK/civout* ../"); print " civout \n"; 
                                         system("mv WORK/cirefv* ../"); print " cirefv\n";}

  	  if (-e "WORK/gdiisfl"){cp ("WORK/gdiisfl", "LISTINGS/.");}
#fp hessian also deleted (for RGF)
       if ("$clean" eq "work") { system ("rm -rf WORK  ");}
               else   { chdir("WORK"); system("rm -f aoints* moints* fil* cihvfl hessian*"); chdir("../");}
          chdir $JDIR;
         } # end of: if (!$displ==0)
    } # end of: 

   close CURVELOG;
