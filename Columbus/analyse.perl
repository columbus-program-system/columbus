#!/usr/bin/perl

 if ($ARGV[0] != -type ) {die("invalid usage: ./analyse.perl -type [bk|ci|sv] files \n");}
 shift @ARGV; 
 $typeofcalc=$ARGV[0];
 shift @ARGV;
 print " timings of $typeofcalc computations \n";
 if ($typeofcalc ne "bk" && $typeofcalc ne "ci" && $typeofcalc ne "sv" )
  {die("invalid usage: ./analyse.perl -type [bk|ci|sv] files \n");}

 foreach $listingfile (@ARGV) 

  { print "analysing file $listingfile \n";
    open LISTF,"<$listingfile" ;
    $time_mult=0;
    $time_multnx=0;
    $time_loop=0;
    $time_sync=0;
    $time_total=0;

    @time_mult=();
    @time_multnx=();
    @time_loop=();
    @time_sync=();
    @time_total=();
    @maxtask=();
    @maxtaskno=();
    @maxwait=(); 
    @minwait=();
    @pdegree=();
    @galocaltime=();
    @ganonlocaltime=();
    @galocalvolume=();
    @ganonlocalvolume=();

#  determine number of nodes
    while (<LISTF>) 
   { if (/^node/) {last;}}
    $nodes=0;
    while (<LISTF>)
     { if (/^======/) { last;} $nodes++;}
    close LISTF;

    open LISTF,"<$listingfile" ;
    $cnt=1;
    { while (<LISTF>) { if (/^ *starting $typeofcalc /) {last;}}}


    while (<LISTF>)
    { chop; 

      if (/TIMING STATISTICS PER TASK/) 
           {$x=<LISTF>; $maxtime=0;
            while (<LISTF>) { if (/======/) {last;}
                              s/^ *//;
                              @x=split(/\s+/,$_);
                              if ($x[3] > $maxtime) {$maxtime=$x[3];$maxtaskno=$x[0];}
                            }
            push @maxtask,$maxtime; 
            push @maxtaskno,$maxtaskno;} 

      if (/TIMING STATISTICS PER NODE/)
           {$x=<LISTF>; $maxwait=0;
            while (<LISTF>) { if (/======/) {last;}
                              s/^ *//;
                              @x=split(/\s+/,$_);
                              if ($x[1] > $maxwait) {$maxwait=$x[1];}
                            }
            push @maxwait,$maxwait;}




      if (/time spent in mult:/) { s/^.*://; s/ *//g; s/s/ /;
                                  $time_mult+=$_; push @time_mult, $_/$nodes; }
      if (/time spent in multnx:/) { s/^.*://; s/ *//g; s/s/ /;
                                  $time_multnx+=$_; push @time_multnx,$_/$nodes;}
      if (/time spent for loop construction:/) { s/^.*://; s/ *//g; s/s/ /;
                                  $time_loop+=$_; push @time_loop,$_/$nodes;}
      if (/syncronization time in mult:/) { s/^.*://; s/ *//g; s/s/ /;
                                  $time_sync+=$_; push @time_sync,$_/$nodes; }
      if (/total time per CI iteration:/) { s/^.*://; s/ *//g; s/s/ /;
                                  $time_total+=$_; push @time_total,$_/$nodes; }
      if (/parallelization degree for mult/) { $cnt++;s/^.*://; push @pdegree, $_;}

      if (/== LOCAL GA TRANSFER ==/)
           {$x=<LISTF>;
            while (<LISTF>) { if (/total:/) {last;}}
             s/^ *//;
             @x=split(/\s+/,$_);
            push @galocaltime,$x[2] ;
            push @galocalvolume,$x[1] ;
   }

      if (/== NON-LOCAL GA TRANSFER ==/)
           {$x=<LISTF>; 
            while (<LISTF>) { if (/total:/) {last;}}
             s/^ *//;
             @x=split(/\s+/,$_);
            push @ganonlocaltime,$x[2] ;
            push @ganonlocalvolume,$x[1];
   }


      if (/^ *starting .* iteration/) { s/^ *starting//; s/iteration.*$//;
                                        s/ //g; 
                                   if ($typeofcalc ne $_ ){last;}}
    }
    close LISTF;
    $cnt--;
    print " summary timings for $cnt  $typeofcalc iterations on $nodes nodes \n";
    print " timings refer to the average times per node\n";

    printf "%-20s %12.5f sec (av. per iter. %9.5f ) \n","time mult:",
         $time_mult/$nodes,$time_mult/($cnt*$nodes);
    printf "%-20s %12.5f sec (av. per iter. %9.5f ) \n","time multnx:", 
         $time_multnx/$nodes,$time_multnx/($nodes*$cnt);
    printf "%-20s %12.5f sec (av. per iter. %9.5f ) \n","time loops:", 
         $time_loop/$nodes, $time_loop/($cnt*$nodes);
    printf "%-20s %12.5f sec (av. per iter. %9.5f ) \n","sync time in mult:",
         $time_sync/$nodes, $time_sync/($cnt*$nodes);
    printf "%-20s %12.5f sec (av. per iter. %9.5f ) \n","total time :", 
         $time_total/$nodes, $time_total/($cnt*$nodes);

    print "individual timings for $cnt  $typeofcalc iterations per node \n";
    print "iter       mult        multnx      loop       sync      total      pdegree \n";
    for ($iter=0;$iter<$cnt; $iter++)
    {
    printf "%6d %10.4f %10.4f %10.4f %10.4f %10.4f %10.4f  \n",
         $iter+1,$time_mult[$iter],$time_multnx[$iter],
         $time_loop[$iter], $time_sync[$iter], $time_total[$iter],
         $pdegree[$iter];
     }
    
    print "individual global array access info\n";
    print "iter       local  GA  transfer         non-local GA transfer\n";
    print "           total time   total volume    total time total volume \n";
    for ($iter=0;$iter<$cnt; $iter++)
    {
    printf "%6d %10.4f s %10.4f MB %10.4f s %10.4f MB \n",
         $iter+1,$galocaltime[$iter],$galocalvolume[$iter],
         $ganonlocaltime[$iter], $ganonlocalvolume[$iter];
    }


    print "performance info\n";
    print "iter     maxtask   maxtaskno   maxwait \n"; 
    for ($iter=0;$iter<$cnt; $iter++)
    {
    printf "%6d %10.4f %10d %12.4f\n",
         $iter+1,$maxtask[$iter],$maxtaskno[$iter], $maxwait[$iter];
    }
 

  }

    
