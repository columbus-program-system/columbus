#!/usr/bin/env colperl
#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/

use lib join('/',$ENV{"COLUMBUS"},"CPAN") ;
use colib_perl;
#
    $scriptversion="Version 0.7, 22_May_2002 (md)";
#
#=======================================================================
#
#     PROGRAM: curve.pl, 
#     author: Michal Dallos, University Vienna, Austria
#     currently rewritten
#
#     This script reads out the calculated energies and c**2 values for
#      MCSCF, CI, AQCC, AQCC-LPR calculations performed in the 
#      according to the informations in the displfl 
#
#=======================================================================
   if ($#ARGV == -1 )
   { print "usage: curve.pl [displacement directory] \n";
     print "default displacement directory DISPLACEMENT \n";
     print "input file curvin:\n";
     print " method:drt:nstate:energy:coeffs:geomopt:csf:comment\n";
     print " method= (scf|mcscf|ci|aqcc|acpf|ccsd|ccsdt|caspt2) \n";
     print " drt [1] = from which drt \n";
     print " nstate [1] = which state  \n";
     print " energy = ON|OFF  \n";
     print " coeffs = ON|OFF  \n";
     print " geomopt = ON|OFF  \n";
     print " csf =  number of CSFs (optional)  \n";
     print " comment =   (optional)  \n";
     $displacement="DISPLACEMENT";}
  else
    { $displacement = shift @ARGV;}

  %bigtable={};

#
#
# check the existence of displfl
   if (! -s "$displacement/displfl") {die "$displacement/displfl does not exist!!!\n\n"; }
#
#  read the input file
   $/="\n";
   open (INP,"<curvein") or die "curvein does not exist!!!\n\n";
#  $options{method} = method
#  $options{energy} = 0,1
#  $options{coeffs} = 0,1
#  $options{drt} = 1
#  $options{nstate} = 1
#  $options{geomopt} = 0, 1 
#  $options{csf} = " "; 
#  $options{comment} = " "; 
#
   @inputdefs=();
   
   $i=1;
   $geomopt=0;
     print "\n\n Input echo:\n";
     while(<INP>)
      { 
       chop;
       if (/^\#/ || /^ *$/ || /^$/ ) {next;}
       s/^ *//;  
       ($method,$drt,$nstate,$energy,$coeffs,$geomopt,$csf,$comment)=split(/:/,$_);
        if ( ! grep(/^(scf|mcscf|ci|aqcc|acpf|ccsd|ccsdt|caspt2)/,$method)) 
           { die ("invalid method specification: $method\n");}
        $inputdefs[$i]{method}=$method;
        if ($drt <1 ) {  $drt=1;}
        $inputdefs[$i]{drt}=$drt;
        if ($nstate <1 ) {  $nstate=1;}
        $inputdefs[$i]{nstate}=$nstate;
        if ( $energy ne OFF  ) {  $energy=1;} else {$energy=0;}
        $inputdefs[$i]{energy}=$energy;
        if ( $coeffs ne OFF  ) {  $coeffs=1;} else {$coeffs=0;}
        $inputdefs[$i]{coeffs}=$coeffs;
        if ( $geomopt ne ON  ) { $geomopt=0;} else {$geomopt=1;}
        $inputdefs[$i]{geomopt}=$geomopt;
        if (!defined($csf)) {$inputdefs[$i]{csf}=" ";}else{$inputdefs[$i]{csf}=$csf; }
        if (!defined($comment)) {$inputdefs[$i]{comment}=" ";}else{$inputdefs[$i]{comment}=$comment; }
        print "LINE $i:$inputdefs[$i]{method}:$inputdefs[$i]{drt}:$inputdefs[$i]{nstate}:$inputdefs[$i]{energy}:$inputdefs[$i]{coeffs}:$inputdefs[$i]{geomopt}\n"; 
       $i++;
      } # end of: while(<INP>)
   close INP;
   $nanal=$i-1;
#
##  read the file
#  
   print "\n----------------------------------------------------------\n";
   open(DISPLFL,"$displacement/displfl") or  die "$displacement/displfl does not exist!!!\n\n";
   $line=<DISPLFL>; $line=<DISPLFL>; $line=<DISPLFL>; #  skip the first 3 lines
     $i=1;
     while(<DISPLFL>){
     ($coord[$i],$displ[$i])=split(/\s+/,$_,3);
     $i++;
     } # end of: while(<DISPLFL>)
     close(DISPLFL); 
     $ndisp=$i-1;
#
#----------------------------------------------------------
#



   for ($i=1; $i<=$nanal; $i++)
    {
     $csf= " " . $inputdefs[$i]{csf};
     $method= $inputdefs[$i]{method};

#   compute and print data  to office file and output
    print "processing line $i of curvein ($method),", join(':',%{$inputdefs[$i]})," \n";
    $bigtableentries = &{$method}(\%{$inputdefs[$i]},$i)

    } # end of: for ($i=1; $i<=$nanal; $i++)


     exit;


#
#   generate combined outputs - additional input options necessary 
#

   if (grep(/caspt2/,@method))
    {
   for ($i=1; $i<= $ndisp; $i++)
   { printf "r: %8.4f caspt2 (variational): %16.8f   caspt2 (ref): %16.8f    caspt2(c0**2): %16.8f \n", $displ[$i], ${$bigtable{"caspt2_evar"}}[$i][1],
       ${$bigtable{"caspt2_ref"}}[$i][1],${$bigtable{"caspt2_c0"}}[$i][1];
    }

   open FOFF,">data.office.caspt2";
   print FOFF "Spalten:  R    caspt2 (Evar) caspt2(Eref)  c0**2 \n";
   for ($i=1; $i<= $ndisp; $i++)
   { printf FOFF " %8.4f  %16.8f    %16.8f    %16.8f \n", $displ[$i], ${$bigtable{"caspt2_evar"}}[$i][1],
       ${$bigtable{"caspt2_ref"}}[$i][1],${$bigtable{"caspt2_c0"}}[$i][1];
   }

  close FOFF;
  }


   if (grep(/ccsdt/,@method))
    {
   for ($i=1; $i<= $ndisp; $i++)
   { printf "r: %8.4f ccsd:   %16.8f   ccsdt: %16.8f    norm(t1aa+t1bb): %16.8f \n", $displ[$i], ${$bigtable{"ccsd"}}[$i][1],
       ${$bigtable{"ccsdt"}}[$i][1],${$bigtable{"t1norm"}}[$i][1];
    }

   open FOFF,">data.office.ccsdt";
   print FOFF "Spalten:  R    ccsd    ccsdt   t1norm            \n";
   for ($i=1; $i<= $ndisp; $i++)
   { printf FOFF " %8.4f  %16.8f    %16.8f    %16.8f \n", $displ[$i], ${$bigtable{"ccsd"}}[$i][1],
       ${$bigtable{"ccsdt"}}[$i][1],${$bigtable{"t1norm"}}[$i][1];
   }

  close FOFF;
  }
 
exit;
#
################ MCSCF ####################################
#
   sub mcscf{
   my ($options,$cnt)=@_;
   my ($i,$j,$energy,$suffix);
   my (@mcscfenergy,@mcscfconv,$retval,$idisp,$energy,$iter);
#
     $suffix='sp';
     if ($$options{geomopt}) {$suffix="all";}

     for ( $idisp=1; $idisp<=$ndisp;$idisp++) 
     { $basename="$displacement/CALC.c$coord[$idisp].d$displ[$idisp]";
       if ( -d "${basename}au") {$basename=$basename . "au"; $unit="au"; }
       elsif ( -d "${basename}Ang") {$basename=$basename . "Ang"; $unit="A";} 
        elsif ( -d "${basename}Deg") {$basename=$basename . "Deg"; $unit="deg";} 
       $fname="$basename/LISTINGS/mcscfsm.$suffix";
       open FMCSCF,"<$fname" or warn("Could not open $fname!"); 
       $iter=0;
       while (<FMCSCF>)
       {
            if (/final mcscf/)
            {
                $_ = <FMCSCF>;
                @line = split(/\s+/,$_);
                $iter = $line[1];
                if (/\*converged\*/) {$mcscfconv[$idisp]=abs($iter); }
                else    {$mcscfconv[$idisp]=-abs($iter); }
                           
            }
          if (/Individual total energies for all states/i) {last;}
          }
          
      
      while (<FMCSCF>)
       { if (!/^ *DRT/) {last;}
         s/^ *//; chop; 
         s/weight.*total/total/; chop; 
         s/(\#|DRT|state|wt|total energy=)//g; 
         s/, rel.*//;
         s/ +/ /g; 
         s/^ *//g;
         #print "$_\n";
         ($drt,$state,$wt,$energy)=split(/\s+/,$_);
          if ( ($drt == $$options{drt}) && ( $state == $$options{nstate}) ) 
           { $mcscfenergy[$idisp]=$energy; last;}
         # else {print "mcscfinfo: skipping drt $drt state $state  $energy ...\n";}
       }
       close FMCSCF;
      $bigtable{"mcscf"}=\@mcscfenergy;
      $bigtable{"mcscfconv"}=\@mcscfconv;

      if ($$options{coeffs}) 
        { $fname="$basename/LISTINGS/mcpcls.drt$drt.state$state.$suffix";
          $/="";
          print "Coord $coord[$idisp]  Displacement $dipsl[$idisp]  fname=mcpcls.drt$drt.state$state.$suffix \n";
          open FMCPC,"<$fname";
          while (<FMCPC>) { if (/^   csf       coeff       coeff\*\*2 /) { print $_; last;}} 
          close FMCPC;
          $/="\n";
        }
       
    } # end for idisp    
#  print data
#  don't print  $displ for r 
    {
   for ($i=1; $i<= $ndisp; $i++)
   { if ($unit eq "A") 
      { printf "r: %8.4f A ( %9.5f au) mcscf: %16.8f  converged: %3d \n", $displ[$i], $displ[$i]/0.529177, ${$bigtable{"mcscf"}}[$i], ${$bigtable{"mcscfconv"}}[$i];}
     elsif ($unit eq "deg") 
      { printf "angle: %8.4f deg mcscf: %16.8f  converged: %3d \n", $displ[$i],  ${$bigtable{"mcscf"}}[$i], ${$bigtable{"mcscfconv"}}[$i];}
     elsif ($unit eq "au" )
      { printf "r: %8.4f A ( %9.5f au) mcscf: %16.8f  converged: %3d \n", $displ[$i]*0.529177, $displ[$i], ${$bigtable{"mcscf"}}[$i], ${$bigtable{"mcscfconv"}}[$i];}
     }
   open FOFF,">data.office.mcscf_line$cnt";
   print FOFF "Spalten:  R   mcscfenergie  \n";
   for ($i=1; $i<= $ndisp; $i++)
   { printf FOFF "%8.4f  %16.8f  \n", $displ[$i], ${$bigtable{"mcscf"}}[$i];}
   close FOFF;
  }
#
   $retval="mcscf:mcscfconv";
   return $retval;
   } # end of: sub mcscf

################ CIUDG ####################################

   sub genciudg{
   my ($options,$cnt)=@_;
   my ($i,$j,$energy,$suffix,$finalci);
   my (@energy,@energy1,@energy2,@energy3,@ciconv,@residual,$retval,$idisp,$energy,$iter);
#
     if ($$options{geomopt}) {$suffix="all";}
     else {$suffix="sp";}

     for ( $idisp=1; $idisp<=$ndisp;$idisp++)
     { $basename="$displacement/CALC.c$coord[$idisp].d$displ[$idisp]";
       if ( -d "${basename}au") {$basename=$basename . "au"; $unit="au"; }
       elsif ( -d "${basename}Ang") {$basename=$basename . "Ang"; $unit="A";} 
        elsif ( -d "${basename}Deg") {$basename=$basename . "Deg"; $unit="deg";} 
       
       $fnamemeth="$basename/LISTINGS/ciudgsm.$$options{method}.$suffix";
       $fnameonly="$basename/LISTINGS/ciudgsm.$suffix";
       
       if ( -f "$fnamemeth")
       {
            $fname=$fnamemeth;
       }
       else
       {
         # print "File $fnamemeth not found, using $fnameonly\n";
           $fname = $fnameonly
       }

       $/="\n";
       open FCIUDGSM,"<$fname" or die("could not open $fname\n");
       $finalci=0;
       while (<FCIUDGSM>)
          { if (/beginning the ci iterative diagonalization procedure/) {$finalci=1; next;} 
            if (/convergence not reached after/ && $finalci ) { s/^.*after//; s/iter.*$//; $ciconv[$idisp]=-$_;
                                                                $_=<FCIUDGSM>;$_=<FCIUDGSM>;
                                                             # pick the correct state 
                                                               for ($ii=1; $ii<=$$options{nstate}; $ii++) {$_=<FCIUDGSM>;} 
                                                               chop ; s/^.*\#//; s/^ *//; 
                                                               ($x1,$x2,$energy[$idisp],$deltae,$apxde,$residual[$idisp],$rtol)=split(/\s+/,$_);last;} 
            if (/convergence criteria satisfied after/ && $finalci ) {s/^.*after//; s/iter.*$//; $ciconv[$idisp]=$_; 
                                                                $_=<FCIUDGSM>; chop ;
                                                                $_=<FCIUDGSM>; chop ;
                                                               # pick the correct state 
                                                               for ($ii=1; $ii<=$$options{nstate}; $ii++) {$_=<FCIUDGSM>;} 
                                                                 chop ;
                                                                 s/^.*\#//; s/^ *//; 
                                                               ($x1,$x2,$energy[$idisp],$deltae,$apxde,$residual[$idisp],$rtol)=split(/\s+/,$_);last;} 
          }
       if ($$options{method} eq "ci") 
        { $cntt=0;  
           while (<FCIUDGSM>) 
           { if (/eci\+dv1/ && $cntt == $$options{nstate}) {s/^ *//; s/=/ /g; ($str,$energy1[$idisp],$str2)=split(/\s+/,$_,3); next;}
             if (/eci\+dv2/ && $cntt == $$options{nstate}) {s/^ *//; s/=/ /g; ($str,$energy2[$idisp],$str2)=split(/\s+/,$_,3); next;}
             if (/eci\+dv3/ && $cntt == $$options{nstate}) {s/^ *//; s/=/ /g; ($str,$energy3[$idisp],$str2)=split(/\s+/,$_,3); next;}
             if (/eci     / && $cntt == $$options{nstate}) {s/^ *//; s/=/ /g; ($str,$energy[$idisp],$str2)=split(/\s+/,$_,3); next;}
             if (/^ eref /) {s/^ *//; s/=/ /g; s/\"relaxed\" cnot\*\*2 //; ($str,$eref[$idisp],$cnot[$idisp])=split(/\s+/,$_,3); $cntt++;next;}
           }
        }
       close FCIUDGSM;
     }

   if ($$options{method} eq "ci") { $retval="ci:ci+dv1:ci+dv2:ci+dv3:cieref:cic02:ciconv";
                                   $bigtable{ci}=\@energy; $bigtable{"ci+dv1"}=\@energy1; $bigtable{"ci+dv2"}=\@energy2; $bigtable{"ci+dv3"}=\@energy3;
                                   $bigtable{cieref}=\@eref; $bigtable{cnot}=\@cnot;$bigtable{ciconv}=\@ciconv;
                                   for ($i=1; $i<= $ndisp; $i++)
                                    { printf "r: %8.4f eci: %16.8f eci+dv1: %16.8f eci+dv2: %16.8f eci+dv3: %16.8f converged: %3d \n", 
                                      $displ[$i], 
                                      ${$bigtable{"ci"}}[$i],  ${$bigtable{"ci+dv1"}}[$i],  ${$bigtable{"ci+dv2"}}[$i],  ${$bigtable{"ci+dv3"}}[$i], 
                                      ${$bigtable{"ciconv"}}[$i]; }
                                   open FOFF,">data.office.ci_line$cnt";
                                   print FOFF "Spalten:  r    ci   ci+dv1    ci+dv2   ci+dv3   converged \n";
                                   for ($i=1; $i<= $ndisp; $i++)
                                      { printf FOFF "%8.4f %16.8f  %16.8f  %16.8f  %16.8f %3d \n", 
                                        $displ[$i], 
                                        ${$bigtable{"ci"}}[$i],  ${$bigtable{"ci+dv1"}}[$i],  ${$bigtable{"ci+dv2"}}[$i],  ${$bigtable{"ci+dv3"}}[$i], 
                                        ${$bigtable{"ciconv"}}[$i]; }
                                   close FOFF; }

   if ($$options{method} eq "acpf") {$retval="acpf:ciconv";$bigtable{acpf}=\@energy;$bigtable{ciconv}=\@ciconv;
                                   for ($i=1; $i<= $ndisp; $i++)
                                    { printf      "r: %8.4f acpf: %16.8f converged: %3d \n",
                                      $displ[$i], ${$bigtable{"acpf"}}[$i], ${$bigtable{"ciconv"}}[$i]; }
                                   open FOFF,">data.office.acpf_line$cnt";
                                   print FOFF "Spalten:  r    acpf   converged \n";
                                   for ($i=1; $i<= $ndisp; $i++)
                                      { printf FOFF "%8.4f %16.8f %3d \n",                                       
                                      $displ[$i], ${$bigtable{"acpf"}}[$i], ${$bigtable{"ciconv"}}[$i]; }
                                   close FOFF; }

   if ($$options{method} eq "aqcc") {$retval="aqcc:ciconv";$bigtable{aqcc}=\@energy;$bigtable{ciconv}=\@ciconv;
                                   for ($i=1; $i<= $ndisp; $i++)
                                    { printf "r: %8.4f A (%9.5f au )aqcc: %16.8f converged: %3d \n",
                                      $displ[$i],  $displ[$i]/0.529177, ${$bigtable{"aqcc"}}[$i], ${$bigtable{"ciconv"}}[$i]; }
                                   open FOFF,">data.office.aqcc_line$cnt";
                                   print FOFF "Spalten:  r    aqcc   converged \n";
                                   for ($i=1; $i<= $ndisp; $i++)
                                      { printf FOFF "%8.4f %16.8f %3d \n",                        
                                      $displ[$i], ${$bigtable{"aqcc"}}[$i], ${$bigtable{"ciconv"}}[$i]; }
                                   close FOFF; }
   return $retval;
   } # end of: sub genciudg 


sub  aqcc { my ($retval); $retval=genciudg(@_); }
sub  acpf { my ($retval); $retval=genciudg(@_); }
sub  ci   { my ($retval); $retval=genciudg(@_); }

################ MOLCAS CASPT2 ####################################

sub caspt2 
{
        ($options) =@_;
	my $i;
	local (@myref,@myvar,@myc0,@myalphacrit);
#
#       weight (normalized Psi0+Psi1) = (1+alpha)**(-N/2)     
#        (Roos in: Quantum mechanical electronic structure calculations with chemical accuracy, p. 368) 
#       alpha = weight**(-2/N)-1   should  not exceed 0.02 , typically 0.01 to 0.02 
#
        if ($$options{nstate}!=1) {die("caspt2: only nstate=1 supported currently\n");}
        if ($$options{drt}!=1) {die("caspt2: only drt=1 supported currently\n");}
#
	for ($j=1;$j<=$ndisp;$j++)
        { $basename="$displacement/CALC.c$coord[$idisp].d$displ[$idisp]";
                 if ( -d "${basename}au" ) {$basename=$basename . "au";}
                 elsif ( -d "${basename}Ang" ) {$basename=$basename . "Ang";}
                      $dir="$basename";
			$myref[$j]=ngetenergy("$dir/LISTINGS/caspt2ls.sp ","Reference energy: ",1,1,1.0,'14.9f');
			$myvar[$j]=ngetenergy("$dir/LISTINGS/caspt2ls.sp ","Total energy:",1,1,1.0,'14.9f');
			$myc0[$j]=ngetenergy("$dir/LISTINGS/caspt2ls.sp ","Reference weight:",1,1,1.0,'14.9f');
			$neleccl=ngetenergy("$dir/LISTINGS/caspt2ls.sp ","Number of closed shell electrons",1,1,1.0,'14.9f');
			$nelecac=ngetenergy("$dir/LISTINGS/caspt2ls.sp ","Number of electrons in active shells",1,1,1.0,'14.9f');
                        $myalphacrit[$j]=(($myc0[$j])**(-2/($neleccl+$nelecac)))-1;
		} # end of: for ($j=1;$j<=$ndisp;$j++)
#
        $bigtable{caspt2}=\@myvar;
        $bigtable{caspt2ref}=\@myref;
        $bigtable{caspt2weight}=\@myc0;
        $bigtable{caspt2alpha}=\@myalphacrit;
        $retval="caspt2:caspt2ref:caspt2weight:caspt2alpha";

        for ($i=1; $i<= $ndisp; $i++)
         { printf "r: %8.4f caspt2: %16.8f c0**2 %8.4f alpha %8.4f  \% \n",
                  $displ[$i], ${$bigtable{"caspt2"}}[$i], ${$bigtable{"caspt2weight"}}[$i],${$bigtable{"caspt2alpha"}}[$i]; }
        open FOFF,">data.office.caspt2";
        print FOFF "Spalten:  r    caspt2    refweight  alpha \n";
        for ($i=1; $i<= $ndisp; $i++)
          { printf FOFF "%8.4f %16.8f %8.4f %8.4f  \n", $displ[$i], ${$bigtable{"caspt2"}}[$i], ${$bigtable{"caspt2weight"}}[$i],
             ${$bigtable{"caspt2alpha"}}[$i]; } close FOFF; 
	return $retval; 
}

################ MOLCAS CCSD/CCSDT ####################################
sub ccsdt 
{
        ($options) =@_;
	my $i;
	local (@myccsd,@myccsdt,@myt1norm,$neleccl,$nelecac,@myt1crit);
        if ($$options{nstate}!=1) {die("ccsdt: only nstate=1 supported currently\n");}
        if ($$options{drt}!=1) {die("ccsdt: only drt=1 supported currently\n");}
#
		for ($j=1;$j<=$ndisp;$j++)
		{
                        $dir="$displacement/CALC.c$coord[$idisp].d$displ[$idisp]";
                        if ( -d "${dir}au") {$dir=$dir . "au";}
                        elsif ( -d "${dir}Ang") {$dir=$dir . "Ang";}
			$myccsd[$j]=ngetenergy("$dir/LISTINGS/ccsdtls.sp ","CCSD     =",1,1,1.0,'14.9f');
			$myccsdt[$j]=ngetenergy("$dir/LISTINGS/ccsdtls.sp "," T3=",1,1,1.0,'14.9f');
			$tmp=ngetenergy("$dir/LISTINGS/ccsdtls.sp ","Euclidian norm is : ",1,1,1.0,'14.9f');
			$myt1norm[$j]=$tmp*$tmp;
			$tmp=ngetenergy("$dir/LISTINGS/ccsdtls.sp ","Euclidian norm is : ",2,1,1.0,'14.9f');
			$myt1norm[$j]+=$tmp*$tmp;
			$myt1norm[$j]=sqrt($myt1norm[$j][1]);
			$neleccl=ngetenergy("$dir/LISTINGS/ccsdtls.sp ","Number of closed shell electrons",1,1,1.0,'14.9f');
			$nelecac=ngetenergy("$dir/LISTINGS/ccsdtls.sp ","Number of electrons in active shells",1,1,1.0,'14.9f');
                        $myt1crit[$j]=$myt1norm[$j]/sqrt($nelecac+$neleccl);

		} # end of: for ($j=1;$j<=$ndisp;$j++)
#
        $bigtable{ccsd}=\@myvar;
        $bigtable{ccsdt}=\@myref;
        $bigtable{t1norm}=\@myc0;
        $retval="ccsd:ccsdt:t1norm";

        for ($i=1; $i<= $ndisp; $i++)
         { printf "r: %8.4f ccsd: %16.8f ccsdt %16.8f  t1norm %8.4f T1crit %8.4f   \n",
                  $displ[$i], ${$bigtable{"ccsd"}}[$i], ${$bigtable{"ccsdt"}}[$i],${$bigtable{"t1norm"}}[$i], $myt1crit[$i]; }
        open FOFF,">data.office.ccsdt";
        print FOFF "Spalten:  r    ccsd    ccsdt   t1norm   T1crit\n";
        for ($i=1; $i<= $ndisp; $i++)
          { printf FOFF "%8.4f %16.8f %16.8f  %8.4f \n", $displ[$i], ${$bigtable{"ccsd"}}[$i], ${$bigtable{"ccsdt"}}[$i],${$bigtable{"t1norm"}}[$i],
            $myt1crit[$i]; }
        close FOFF; 
	return $retval; 
}
################ MOLCAS CASSCF ####################################
#
#-----------------------------------------------------------------
#
#
#    MRPT
sub mrpt{
#
   ($nstate)=@_;
   my ($i,$j);
   print "\n ***  E2(MRPT)�*** \n\n";
     for ($i=1; $i<=$nstate; $i++){
     print "----------------------------------------------------------\n";
     print "E2(MRPT) State Nr $i\n";
       for ($j=1; $j<=$ndisp; $j++){ 
       $dir="$displacement/CALC.c$coord[$j].d$displ[$j]";
       if ( -d "${dir}au") {$dir=$dir . "au";}
       elsif ( -d "${dir}Ang") {$dir=$dir . "Ang";}
        &getenergy("$dir/LISTINGS/ciudgls.sp ","mrpt2",$i,3,1.0,'14.9f');
       } # end of: for ($j=1; $j<=$nstate; $j++)
     } # end of: for ($i=1; $i<=$nstate; $i++)
#
   print "\n *** E3(MRPT)�*** \n\n";
     for ($i=1; $i<=$nstate; $i++){
     print "----------------------------------------------------------\n";
     print "E3(MRPT) State Nr $i\n";
       for ($j=1; $j<=$ndisp; $j++){
       $dir="$displacement/CALC.c$coord[$j].d$displ[$j]";
        &getenergy("$dir/LISTINGS/ciudgls.sp ","mrpt3",$i,3,1.0,'14.9f');
       } # end of: for ($j=1; $j<=$nstate; $j++){
     } # end of: for ($i=1; $i<=$nstate; $i++)
#
}
####################################################################################
#
   sub getenergy {
#

     local ($energy,$found);
     ($filename, $string,$istate,$position,$factor,$fmt)=@_;
#     print "$filename,$string,$istate,$position\n";

      open (ANYFILE, $filename) or die "File: $filename not found!\n";
      $/="\n";
       $icount=0;
       while ( <ANYFILE> ){
         if ( /$string/  ){
          $icount++;
	  $found=$_;
#	  print "FOUND: $found \n";
	    if ($icount == $istate){
            chomp; s/^ *//;
            (@field)=split(/\s+/,$_,$position+1); 
#           print " field=",join(':',@field)," choosing position $position+1 \n";
            $energy=$field[$position-1];
            $energy=$energy*$factor;
#           printf "%$fmt\n",$energy;
	    } # end of: if ($icount == $istate)
	  }
        } # end of: while ( <ANYFILE> ) 
# 	printf "%$ "$icount,$istate\n";
	if ( $icount < $istate){ print"0.0\n";}

       close ANYFILE;
       return ($energy);
      }    


   sub ngetenergy {
#

     local ($energy,$found);
     ($filename, $string,$istate,$position,$factor,$fmt)=@_;
#     print "$filename,$string,$istate,$position\n";

      open (ANYFILE, $filename) or die "File: $filename not found!\n";
      $/="\n";
       $icount=0; 
       while ( <ANYFILE> ){
         if ( /$string/  ){
          $icount++;
          $found=$_;
          print "FOUND: $found \n";
            if ($icount == $istate){
            s/^.*$string//;
            chomp; s/^ *//;
            (@field)=split(/\s+/,$_,$position+1);
            print " field=",join(':',@field)," choosing position $position+1 \n";
            $energy=$field[$position-1];
            $energy=$energy*$factor;
            printf "%$fmt\n",$energy;
            return $energy;
            } # end of: if ($icount == $istate)
          }
        } # end of: while ( <ANYFILE> ) 
#       printf "%$ "$icount,$istate\n";
        if ( $icount < $istate){ print"0.0\n";}

       close ANYFILE;
       return $energy;
      }    



####################################################################################
#
   sub getc2 {
#

     local ($energy,$found);
     ($filename, $string,$istate,$position,$string2)=@_;
#     print "$filename,$string,$istate,$position,$string2\n";

      open (ANYFILE, $filename) or die "File: $filename not found!\n";
      $/="\n";
       $ifound=0;
       $icount=0;
       while ( <ANYFILE> ){
         if ( /$string2/  ){$icount++;}
         if ( /$string/  ){
            if ($icount == $istate){
	    $ifound=1;
            chomp; s/^ *//;
            (@field)=split(/\s+/,$_,$position+1);
            $energy=$field[$position-1];
            printf "%4.2f\n",$energy;
            last;
            } # end of: if ($icount == $istate)
          }
        } # end of: while ( <ANYFILE> )
#       print "$icount,$istate\n";
        if ( $ifound == 0){ print"0.0\n";}

       return ($energy);
      }       
#
#----------------------------------------------------------------------
#
