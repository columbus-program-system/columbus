#!/usr/bin/env colperl
#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/


open (FIND, "ls $ARGV[0] | ");

 while ($filename = <FIND>)
   { chop $filename; 
     if ( -T $filename) 
     { open (TEXTFILE, $filename);
         $notopen = 1; 
       while (<TEXTFILE>)
       { $zeile= $_ ; 
         chop ;
         tr /A-Z/a-z/;
          if (/^ /) 
          {
          s/^\s*//g ;
 # Fall 1 File �ffnen 
         if ((/^blockdata/ || /^subroutine/ || /function/ || /^program/ ) && ($notopen) )
          {  $name = $_ ;  
             if (/function/ ) 
             { $name =~ s/.*function(.*)\(.*/\1/; } 
             else
             { $name =~ s/^subroutine|^program|^blockdata// ;
             $name =~ s/(.*)\(.*/\1/;
             }
             print $name, " in file ", $filename, "\n" ; 
             if ($notopen) 
               { open (NEWFILE,"> $name.f");
                 $notopen=0;
                 print NEWFILE $zeile;
               };
           }
          elsif ((/^end / && /subroutine|function/ ) || (/^end *$/) || (/^end$/) )
             { $notopen=1;
               print NEWFILE $zeile;
               close NEWFILE; }
          else {print NEWFILE $zeile; } 
         }
         else 
         {print NEWFILE $zeile;} 
       }
      }
 }

