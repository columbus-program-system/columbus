#!/usr/bin/env colperl
#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/


# Calculate the distances in the LIIC path

$WDIR="DISPLACEMENT";
$disp="displfl";
$DIR="/quant/LIC";

if (-s "dyn.mld"){
 system("mv dyn.mld dyn.nld.bk");
}

open(DP,"$WDIR/$disp") or die ":( $WDIR/$disp";
$_=<DP>;
$_=<DP>;
$_=<DP>;
$ind=0;
while(<DP>){
  chomp;
  $_ =~ s/^\s*//;         # remove leading blanks
  $_ =~ s/\s*$//;         # remove trailing blanks
  ($c,$i)=split(/\s+/,$_);
  find_geom();
  $ind++;
}
close(DP);

system("$DIR/distance.pl");
system("molden dyn.mld&");

# .........................

sub find_geom{
  $file="$WDIR/CALC.c$c.d$i/geom";
  geom2xyz();  
}

# .........................
sub geom2xyz{
   $au2ang=0.529177;
   $xyztemp="dyn.mld";
   open (FILE, $file) or die "Failed to open file: $file\n";
   $i=0;
   while (<FILE>)
    {
      $i++;
      chomp;
      $_ =~ s/^\s*//;         # remove leading blanks
      $_ =~ s/\s*$//;         # remove trailing blanks
      ($symbol[$i],$charge[$i],$x[$i],$y[$i],$z[$i])=split(/\s+/,$_);
      $charge[$i]=$charge[$i];
      $natom=$i;
    }
    open (OUT,">>$xyztemp")or die" Failed to open file: $xyztemp\n";
    print {OUT}" $natom\n\n";
    for ($i=1; $i<=$natom; $i++)
    {
      $x[$i]=$x[$i]*$au2ang;
      $y[$i]=$y[$i]*$au2ang;
      $z[$i]=$z[$i]*$au2ang;
      printf {OUT}("%7s  %15.6f  %15.6f   %15.6f\n", $symbol[$i],$x[$i],$y[$i],$z[$i]);
    }
   close OUT;
   close FILE;
   if ($ind == 0){
    open (OUT,">geom.molden")or die" Failed to open file: geom.molden\n";
    print {OUT}" $natom\n\n";
    for ($i=1; $i<=$natom; $i++)
    {
      printf {OUT}("%7s  %15.6f  %15.6f   %15.6f\n", $symbol[$i],$x[$i],$y[$i],$z[$i]);
    }
    close OUT;
   }
}
