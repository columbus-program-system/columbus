package  runc_jobstep;
use Exporter();
@ISA       =qw(Exporter);
@EXPORT    =qw( motran prep_inputs potmat aointegrals transmo scfpart caspt2 ccsdt mcdrtpart mcscfpart drtpart preci 
                call_cipc preprop cipart_if cigrd_if cinad_if 
                exptvlscf exptvlci exptvlmcscf info4molden info4molden2 mcscfgrad cigrad displacement intcx rgf orthogonalize moldenformat
                slapaf gdiisstuff link_grads_mxs mcscfmom ciudgmom polyhes cartgrd_alaska cartgrd_dalton );
use File::Copy 'cp';  # use perl cp
use Cwd;
use Shell qw(pwd cd date mv cat);
use runc_globaldata;
use runc_utilities;
use runc_control;
use colib_perl;

    sub cinad_if{
     my $thisdrt;
     ($thisdrt) = @_;
     print "\n\n +++++++++++ STARTING NAD COUPLINGS EVALUATION OF DRT# $thisdrt +++++++++++ \n\n";

#
#    move or symlink civector info for cigrd
#
     print "nnadcoupl=$nad{nnadcoupl} \n"; 
     for ($inad=1;$inad<=$nad{nnadcoupl};$inad++) {
       $naddrt1=${$nad{nad_drt1}}[$inad];
       $naddrt2=${$nad{nad_drt2}}[$inad];
       $nadstate1=${$nad{nad_state1}}[$inad];
       $nadstate2=${$nad{nad_state2}}[$inad];
       if ($naddrt1 != $thisdrt) { print " skipping entry from nadcoupl list $naddrt1 $thisdrt  ...\n"; next;}
       print "\n -------- NAD. COUPLING for DRT:$naddrt1 STATE:$nadstate1 ";
       print "AND DRT:$naddrt2 STATE:$nadstate2 --------\n";
       print "nadcoupl_ci:nadcoupl_mc: $kwords{nadcoupl_ci},$kwords{nadcoupl_mc} \n"; 

      if ($kwords{nadcoupl_ci}) 
      { # pick energy1,energy2 and deltae from %energy 
       if ($kwords{"gdiis"}){
         print "before putenergydata\n"; 
      print "NAD(deltae)3= $nad{deltae}  $nad{energy1} $nad{energy2} \n";
         &put_energydata("$naddrt1:$nadstate1:$naddrt2:$nadstate2","TRANS","energy.gdiis");
      print "NAD(deltae)4= $nad{deltae}  $nad{energy1} $nad{energy2} \n";
         print "after putenergydata\n";}
       if ($kwords{"polyhes"}){  #polyhes reads both energy and transmomin  - this is going to give rise to trouble
         open (EFILE,">energy.polyhes") or die "Cannot open file: energy";
         print EFILE "$nad{energy1}   $nad{energy2}   $nad{deltae}\n";
  #$nroot=&keyinfile("$direct{JDIR}/ciudgin","nroot");
         print EFILE "$efile{nroots}\n";
          for ($i=1; $i<=$$efile{nroots}; $i++){
  #      $energy=&getcienergy("ciudgls.drt$naddrt1.sp",$i); print EFILE "$energy   ";
  #     $energy=&getcienergy("ciudgls",$i); 
            my @tmp; 
            @tmp=split(/:/,${$efile{roots}}[$i]);
             print EFILE "$tmp[2] "; }
           print EFILE "\n";
           close EFILE; }
        else { 
         &put_energydata("$naddrt1:$nadstate1:$naddrt2:$nadstate2","TRANS","energy.gdiis");
      print "NAD(deltae)4= $nad{deltae}  $nad{energy1} $nad{energy2} \n";
         print "after putenergydata\n";}
       }
      elsif ($kwords{nadcoupl_mc}) 
       { my( $energy1,$energy2,$deltae);
         open(MCSCFSM,"mcscfsm");
         $/="\n";
         $energy1=0;
         $energy2=0;
         while (<MCSCFSM>)
         {chop;
      if (/DRT \#$naddrt1 state \# $nadstate1/) { s/^.*energy=//; s/, rel.*//; $energy1=$_; }
      if (/DRT \#$naddrt2 state \# $nadstate2/) { s/^.*energy=//; s/, rel.*//; $energy2=$_; }}
      if (! ($energy1 && $energy2) ) {$!=101;die "mcscf calculation did not converge. Exiting ...\n";}
       close(MCSCFSM);
       $nad{energy1 }=$energy1;
       $nad{energy2 }=$energy2;
       $nad{deltae} = $energy1-$energy2; 
       $deltae=$energy1-$energy2;
# does this also work with polyhes???
       open(EFILE,">energy");
       print EFILE "$energy1\t$energy2\t$deltae\n";
       close EFILE ;
      }
     &appendtofile("energy.gdiis","$direct{list}/energy.all",$control{iter});
     cp("energy","$direct{list}/energy");

#   Finally calculate the requested nonadiabatic coupling

    &changekeyword("cigrdin","cigrdin","nadcalc",$nad{nadcalc});
# .... fp: this is also needed here
    &changekeyword("cigrdin","cigrdin","drt1",$naddrt1);
    &changekeyword("cigrdin","cigrdin","root1",$nadstate1);
    &changekeyword("cigrdin","cigrdin","drt2",$naddrt2);
    &changekeyword("cigrdin","cigrdin","root2",$nadstate2);
# nadcalc=0  no NAD calculation
# nadcalc=1  DCI contribution only - used for conical intersection optimization
# nadcalc=2  DCSF contribution only - auxiliary term
# nadcalc=3  DCI+DCSF contribution - complete non-adiabatic coupling
    unlink("cid1fl.tr","cid2fl.tr","cid1trfl");
   if ($nad{nadcalc} == 2){
      if ( $kwords{"slapaf"} ) { die("cannot use slapaf with nadcalc==2\n");}
      callprog("dzero.x > dzerols");
      symlink ("dzero","cid1fl.tr");
      symlink ("dzero2","cid2fl.tr");
   }
   if ($kwords{"nadcoupl_mc"}) {
    $fstate1=sprintf("%02d",$nadstate1);
    $fstate2=sprintf("%02d",$nadstate2);
    if ( $nad{nadcalc} != 2) { 
     symlink ("mcsd1fl.drt$naddrt1.st$fstate1-st$fstate2","cid1fl.tr");
     symlink ("mcsd2fl.drt$naddrt1.st$fstate1-st$fstate2","cid2fl.tr");
    }
    symlink ("mcad1fl.drt$naddrt1.st$fstate1-st$fstate2","cid1trfl");
   }
   elsif ($kwords{"nadcoupl_ci"}) {
    if ( $nad{nadcalc} != 2) {
     symlink ("cid1fl.trd$nadstate1"."to$nadstate2","cid1fl.tr");
     symlink ("cid2fl.trd$nadstate1"."to$nadstate2","cid2fl.tr");
    }
    symlink ("cid1trfl.FROMdrt$naddrt1.state$nadstate1"."TOdrt$naddrt2.state$nadstate2",cid1trfl);
   }

    &call_cigrd(".nad.drt$naddrt1.state$nadstate1.drt$naddrt2.state$nadstate2",$kwords{pcigrd},$control{iter},".tr");

# molcas change start
    &effden_tran($kwords{"alaska"},"nad",3);
    if ( $kwords{"alaska"} ) { &cartgrd_alaska("nad.drt$naddrt1.state$nadstate1.drt$naddrt2.state$nadstate2",1); }
                          else { &cartgrd_dalton("drt$naddrt1.state$nadstate1.drt$naddrt2.state$nadstate2",1); }
    &process_nads();

# ..............................................................................
# This block calls the orthogonalization routine "slope"
# slope is only executed to produce some information for the user
# The MXS search using orthogonalized g and h has not been implemented in runc
# FP 2010-07
# ..............................................................................
#  orthogonalize g and h (MXS search case)
#      $au2ev = 27.211396;
#      $dethres= keyinfile("$direct{work}/control.run","dethres");
#      $thres_de = $dethres/$au2ev; # Orthogonalize if DE < $thres_de (hartree)

#      if ($kwords{"gdiis"}||$kwords{"polyhes"}) {
        if ( $kwords{"slope"} ) {
          print "\n Calling the slope program: \n";
          if ($nad{nadcalc} == 1) {
             $coupf = "cartgrd.nad.drt$naddrt1.state$nadstate1.drt$naddrt2.state$nadstate2";
          }
          else {
             $coupf= cartgrd_total_times_DE;
          }
#
          orthogonalize();
#
#fp: the results of the localization are found in slopels. the coupling is no longer printed out
#
        }

    }
   unlink("cid1trfl");
 }


  sub process_nads
   {
    if ($nad{nadcalc}==1){   # without CSF correction
       cp("cartgrd","$direct{grad}/cartgrd.nad.drt$naddrt1.state$nadstate1.drt$naddrt2.state$nadstate2.sp");
       cp("cartgrd","cartgrd.nad.drt$naddrt1.state$nadstate1.drt$naddrt2.state$nadstate2");
       &file_output("Interstate coupling for DRT:$naddrt1 STATE:$nadstate1 and DRT:$naddrt2 STATE:$nadstate2","cartgrd");

        if ($kwords{"gdiis"}||$kwords{"polyhes"}){
           &appendtofile("cartgrd","$direct{grad}/cartgrd.nad.drt$naddrt1.state$nadstate1.drt$naddrt2.state$nadstate2.all",$control{iter});
        }
       &transfgrad(".nad.drt$naddrt1.state$nadstate1.drt$naddrt2.state$nadstate2","grd",0);
       #cp("intgrd","$direct{grad}/intgrd.nad.drt$naddrt1.state$nadstate1.drt$naddrt2.state$nadstate2.sp");
    }


   if  ($nad{nadcalc}>1){  # with CSF correction
   # calculation of the last term in eqn 49
#     cartgrd contains the leading term in eqn 49
#      if ($debug{debug_deriv}) {cp("cartgrd","$direct{grad}/cartgrd.nad$nadcalc.drt$naddrt1.state$nadstate1.drt$naddrt2.state$nadstate2.first-term");}
      open (FILE,"cartgrd") or die"Failed to open file:cartgrd\n";
      $i=1;
      $/="\n";
      while (<FILE>){
        print "cartgrd:$_"; 
        s/D/E/g;chomp;s/^ *//;($nadx[$i],$nady[$i],$nadz[$i])=split(/\s+/,$_,3);$i++;
      }
      close FILE;

      &dxcalc;  # compute CSF correction via  abacus

#     nadxfl contains the contribution of the last term in eq 49, JCP 120(2004) 7322
#

      open (FILE,"nadxfl") or die"Failed to open file:nadxfl\n";
      $i=1;
      $/="\n";
      while (<FILE>){
        print "nadxfl:$_"; 
        s/D/E/g;chomp;s/^ *//;($dx[$i],$dy[$i],$dz[$i])=split(/\s+/,$_,3);$i++;
      }
      close FILE;
      $ndim=$i-1;


# is all this really necessary

      open  (FILE,">tmp")  or die"Failed to open file:tmp\n";

#   We need some more temporary files. See also printf's below. MB sep 2005
      open  (FILE1,">tm1") or die"Failed to open file:tm1\n";
      open  (FILE2,">tm2") or die"Failed to open file:tm2\n";
      open  (FILE3,">tm3") or die"Failed to open file:tm3\n";
      print "NAD(deltae)1= $nad{deltae}  ndim=$ndim\n";
      $norm_nad = 0;
      for ($i=1;$i<=$ndim;$i++){
        $x=$dx[$i]+$nadx[$i]/$nad{deltae};   # total value of eqn 49
        $y=$dy[$i]+$nady[$i]/$nad{deltae};
        $z=$dz[$i]+$nadz[$i]/$nad{deltae};
        $nadxtmp[$i]=$dx[$i]*$nad{deltae}+$nadx[$i];  # nad vector scaled by deltae
        $nadytmp[$i]=$dy[$i]*$nad{deltae}+$nady[$i];
        $nadztmp[$i]=$dz[$i]*$nad{deltae}+$nadz[$i];
        $norm_nad = $norm_nad + $x*$x+$y*$y+$z*$z; # norm of nad vector according to eqn 49
        printf{FILE}( "%15.6E%15.6E%15.6E\n",$x,$y,$z);
        printf{FILE1}( "%15.6E%15.6E%15.6E\n",$dx[$i],$dy[$i],$dz[$i]);
        printf{FILE2}( "%15.6E%15.6E%15.6E\n",$nadx[$i]/$nad{deltae},$nady[$i]/$nad{deltae},$nadz[$i]/$nad{deltae});
        printf{FILE3}( "%15.6E%15.6E%15.6E\n",$nadxtmp[$i],$nadytmp[$i],$nadztmp[$i]);
      }
      $norm_nad = sqrt($norm_nad);
      close FILE;
      close FILE1;
      close FILE2;
      close FILE3;
      cp("tmp","$direct{work}/cartgrd_total");
      symlink ("cartgrd_total","cartgrd.nad.drt$naddrt1.state$nadstate1.drt$naddrt2.state$nadstate2");
      &file_output("Non-adiabatic coupling for DRT $naddrt1 STATE $nadstate1 and DRT $naddrt2 STATE $nadstate2","cartgrd_total");

      cp("tmp","$direct{grad}/cartgrd.nad.drt$naddrt1.state$nadstate1.drt$naddrt2.state$nadstate2.sp");
      if ($kwords{"gdiis"}||$kwords{"polyhes"}){
           &appendtofile("tmp","$direct{grad}/cartgrd.nad.drt$naddrt1.state$nadstate1.drt$naddrt2.state$nadstate2.all",$control{iter});
      }
# Total nonadiabatic coupling vector times DE. MB Sep 2005
      cp("tm3","$direct{work}/cartgrd_total_times_DE");
# ..............................................................................
# This block calls the orthogonalization routine for single point case.
# It appears once more at the MXS search (below).
# MB Sep 2005
# ..............................................................................
#  orthogonalize g and h (Single point case)
#      $au2ev = 27.211396;
#      $dethres= keyinfile("$direct{work}/control.run","dethres");
#      $thres_de = $dethres/$au2ev; # Orthogonalize if DE < $thres_de (hartree)

      print "\n Norm of the nonadiabatic coupling vector: \n";
      print " |h(drt$naddrt1.state$nadstate1,drt$naddrt2.state$nadstate2)|= $norm_nad \n";

      &transfgrad(".nad.drt$naddrt1.state$nadstate1.drt$naddrt2.state$nadstate2","grd",0);
      #cp("$direct{grad}/intgrd_total.sp","$direct{grad}/intgrd.nad.drt$naddrt1.state$nadstate1.drt$naddrt2.state$nadstate2.sp");

#   calculate <L> value using the total non-adiabatic coupling elements
#
#   get the <L> values calculated in transci.x program
     if ($kwords{"nadcoupl_ci"}) {
      open (FILE,"$direct{work}/trncils") or die "Failed to open file:$direct{work}/trncils\n";
      while (<FILE>){
        if (/<L> electro/){
          chomp;s/^ *//;($dum1,$dum2,$lx,$ly,$lz,$dum3)=split(/\s+/,$_,6);
        }
      }
      close FILE;

# do the calculation for <L> value:
        $file="$direct{work}/geom";
        open (FILE,"$file") or die "Failed to open file:$file\n";
        $i=1;
        while (<FILE>){
          chomp;s/^ *//;($dum1,$dum2,$x[$i],$y[$i],$z[$i],$dum3)=split(/\s+/,$_,6);$i++
        }
        close FILE;
        $natom=$i-1;

        $nadlx=0,$nadly=0;$nadlz=0;
        for ($i=1;$i<=$natom;$i++){
          $nadlx=$nadlx + $y[$i]*$nadztmp[$i]- $z[$i]*$nadytmp[$i];
          $nadly=$nadly + $z[$i]*$nadxtmp[$i]- $x[$i]*$nadztmp[$i];
          $nadlz=$nadlz + $x[$i]*$nadytmp[$i]- $y[$i]*$nadxtmp[$i];
        }

      print "\n Cross-check of the non-adiabatic coupling term:\n";
      print "  DeltaE= $nad{deltae}\n\n";
      printf ("  <L>*dE form CIGRD   :  %14.10f   %14.10f   %14.10f\n",$lx*$nad{deltae},$ly*$nad{deltae},$lz*$nad{deltae});
      printf ("  <L>*dE form NADCOUPL:  %14.10f   %14.10f   %14.10f\n",$nadlx,$nadly,$nadlz);
      print " -------------------------------------------------------------------------\n";
      printf ("  Difference          :  %14.10f   %14.10f   %14.10f\n",$lx*$nad{deltae}-$nadlx,$ly*$nad{deltae}-$nadly,$lz*$nad{deltae}-$nadlz);
      print "\n NOTE: This test is meaningless at a point of conical intersection!\n\n";
     } #$kwords{"nadcoupl_ci"}
    } # nadcalc>1
}

   sub cigrd_if {
     my $thisdrt;
     ($thisdrt) = @_;
     print "\n\n +++++++++++ STARTING GRADIENTS EVALUATION DRT# $thisdrt +++++++++++ \n\n";
     chdir($direct{work});
     cp("$direct{JDIR}/cigrdin","cigrdin"); 
     if (! $control{USENEWCIDENSITY}) {die("OLDCIDENSITY mode disabled!\n");}
     print "GEOMOPT=$kwords{GEOMOPT}\n";
     if ($kwords{GEOMOPT} eq "GEOM") 
        { my ($s,$d); $s=$grad{state1}; $d=$grad{drt1};
           print "entering\n";
           if ( $s eq "FROOT") { open  FTEMP,"<energy"; while (<FTEMP>) { if (! /\*/) {last;}}
                                   chop; my @x1; s/^ *//; @x1 = split(/\s+/,$_); $s=$x1[0]; 
                                   print "1a: s=$s\n"; } 
                                   # determine actual root
           symlink ("cid1fl.drt$d.state$s","cid1fl");
           symlink ("cid2fl.drt$d.state$s","cid2fl");
           &call_cigrd(".drt$d.state$s",$kwords{pcigrd},$control{iter},"");
           &effden_tran($kwords{"alaska"},"drt$d.state$s",1);
          if ( $kwords{"alaska"} ) { &cartgrd_alaska("drt$d.state$s",0); }
                             else  { &cartgrd_dalton("drt$d.state$s",0);}
           &appendtofile("cartgrd","$direct{grad}/cartgrd.all",$control{iter});
           cp("cartgrd","cartgrd.drt$d.state$s");
           cp ("cartgrd","$direct{grad}/cartgrd.drt$d.state$s.sp");
           &file_output("Gradient of DRT $d, state $s","cartgrd");
           &transfgrad(".drt$d.state$s","grd",0);
           cp("intgeom","$direct{geom}/intgeom.sp");
           &appendtofile("intgeom","$direct{geom}/intgeom.all",$control{iter}); 
           system("tail -1 energy > energy.gdiis");
           print "leaving\n";
        }

     if ($kwords{GEOMOPT} eq "NAD") 
       { if ($kwords{nadcoupl_ci}) {&changekeyword("$direct{JDIR}/cigrdin","$direct{JDIR}/cigrdin","samcflag",0);
                                    print " Using CI densities ...  (DRT#) $thisdrt \n";}
         elsif ($kwords{nadcoupl_mc})  {&changekeyword("$direct{JDIR}/cigrdin","$direct{JDIR}/cigrdin","samcflag",1);
                                          print " Using MCSCF densities ... (DRT#) $thisdrt \n"; } 

          $nad{nadcalc}=keyinfile("cigrdin","nadcalc");     # save current nadcalc value  DCI,DCSF,DCI+DCSF
          &changekeyword("cigrdin","cigrdin","nadcalc",0);  # force gradients

          for ($igrad=1;$igrad<=$nad{ngrad};$igrad++)      # %nad structure must be changed, temporarily fix the multi-drt problem 
                {$graddrt=${$nad{grad_drt}}[$igrad];            # by copying the cidrtfl file and skipping unwanted entries from this list
                 $gradstate=${$nad{grad_state}}[$igrad];
                 if ($graddrt != $thisdrt ) { print "skipping entry $igrad from nad.ngrad list ...\n"; next;}
                 $actual_state="$graddrt"."$gradstate";
                 ${$nad{dens_calc}}{$actual_state}=1;
                 print" --- Computing gradient for DRT:$graddrt, State; $gradstate\n\n";
                if ($kwords{"nadcoupl_ci"}) 
                  { cp("cidrtfl.$thisdrt","cidrtfl");    # pick the correct DRT
                    symlink ("cid1fl.drt$graddrt.state$gradstate","cid1fl");
                    symlink ("cid2fl.drt$graddrt.state$gradstate","cid2fl"); }
                elsif ($kwords{"nadcoupl_mc"}) 
                     { $fstate=sprintf("%02d",$gradstate);
                       symlink ("mcsd1fl.drt$graddrt.st$fstate","cid1fl");
                       symlink ("mcsd2fl.drt$graddrt.st$fstate","cid2fl"); }
        

                 &call_cigrd(".drt$graddrt.state$gradstate",$kwords{pcigrd},$control{iter},"");
                 &effden_tran($kwords{"alaska"},"drt$graddrt.state$gradstate",1);
                  if ( $kwords{"alaska"} ) { &cartgrd_alaska("drt$graddrt.state$gradstate",0); }
                                     else  { &cartgrd_dalton("drt$graddrt.state$gradstate",0);}
     
                 &appendtofile("cartgrd","$direct{grad}/cartgrd.drt$graddrt.state$gradstate.all",$control{iter});
                 cp("cartgrd","cartgrd.drt$graddrt.state$gradstate");
                 cp ("cartgrd","$direct{grad}/cartgrd.drt$graddrt.state$gradstate.sp");
                 &file_output("Gradient of DRT $graddrt, state $gradstate","cartgrd");
                 &transfgrad(".drt$graddrt.state$gradstate","grd",0);
                 cp("intgeom","$direct{geom}/intgeom.sp");
                if ($kwords{"gdiis"}||$kwords{"polyhes"}){ &appendtofile("intgeom","$direct{geom}/intgeom.all",$control{iter}); }
                }
       }
  }



 sub motran {
   my ($isfrozencore,@tmp,$tmp);

   if ($kwords{nadcoupl_mc}) {  print "motran: Copying the MCSCF MOs ... \n" ;
                                cp("$direct{modir}/mocoef_mc.sp","mocoef"); }


   if ($control{USENEWCIGRD}) {

#------------------------------- CHANGE FROZEN CORE ---------------------------
#  find out whether there are frozen core orbitals
#  if there are negative entries in the map vectors we have frozen
#  core orbitals
#  we need to include support for multiple DRTs (just copy operation)

    open CIDRTFL,"<cidrtfl.1" || die ("could not open cidrtfl.1  (sub cid2tr)\n");
    while (<CIDRTFL>) { if (/  map/){last;}}
    @tmp=();
    while (<CIDRTFL>) { if (/  mu/) {last; }chop ; push @tmp,$_;}
    close CIDRTFL;
    $tmp = join(':',@tmp);
    $tmp = ':' . $tmp . ':';
    $tmp =~s/  */ /g;
    $tmp =~s/ /:/g;
    $tmp =~s/::/:/g;
    $isfrozencore=grep(/:-1:/,$tmp);

    if ($isfrozencore) { print "INFO: this is a frozen core gradient calculation\n";
                         if ($kwords{ciudgav} || $kwords{pciudgav}) 
                               { system("cp $direct{JDIR}/cidrtmsin.cigrd .");  # cidrtin.grad is the step vector form
                                 system("mv cidrtfl.1 cidrtfl.ci");    # cidrtfl.ci is generated with fc orbitals
                                 callprog("cidrtms.x -m $control{coremem} < cidrtmsin.cigrd > cidrtmsls.cigrd"); # now cidrtfl is in step vector from
                                 cp("cidrtfl.1","cidrtfl");
                               } 
                         else  
                               { system("cp $direct{JDIR}/cidrtin.cigrd .");  # cidrtin.grad is the step vector form
                                 system("mv cidrtfl.1 cidrtfl.ci");    # cidrtfl.ci is generated with fc orbitals
                                 callprog("cidrt.x -m $control{coremem} < cidrtin.cigrd > cidrtls.cigrd"); # now cidrtfl is in step vector from
                                 cp("cidrtfl","cidrtfl.1");
                               }
                         unlink "moints";
                         if ($kwords{seward}) { changekeyword("$direct{JDIR}/tranin","tranin","SEWARD",1);}
                         callprog("tran.x -m $control{coremem} "); # redo full AO-MO transformation
                         changekeyword("$direct{JDIR}/cigrdin","cigrdin","assume_fc",1);
                       }
    else  {changekeyword("$direct{JDIR}/cigrdin","cigrdin","assume_fc",0); }
    }
    else { changekeyword("$direct{JDIR}/cigrdin","cigrdin","assume_fc",0); }

#
#   now cigrd.x understands the keyword assume_fc =0,1
#   if assume_fc=0  it runs with the old modus including step vector based frozen core
#   if assume_fc=1  it extracts the proper mapping vector and fc info from cidrtfl.ci
#                   and constructs the 2e density from those of cid2fl,cid1fl supplemented
#                   by the frozen core orbital contributions
#                   consistent with the ordering in cidrtfl
#

  }

    sub cipart_if
{
 my ($tempstr, $cnt); ( $cnt ) = @_;
 chdir "$direct{work}";
  $control{thisdrt}=$cnt;
 cp("cidrtfl.$cnt","cidrtfl");
 cp("$direct{JDIR}/ciudgin.drt$cnt","ciudgin"); 
 if ( $kwords{"GEOMOPT"} && $control{itercount} > 1 && $kwords{"cirestart"})
   { print " Activating ciudg restart: setting noldv to  $fr \n ";
   unless (rename("cirefv.drt$cnt","cirefv")) {die("rename $!\n");};
   unless (rename("civfl.drt$cnt", "civfl")) {die("rename $!\n"); }
   unless (rename("civout.drt$cnt", "civout")) {die("rename $!\n"); } }
   if ($kwords{GEOMOPT}) {  changekeyword("ciudgin","ciudgin","iden",2); }
   if ($kwords{GEOMOPT} eq "NAD") 
      {$tempstr=sprintf("transition\n%3d%3d%3d%3d \n",${$nad{nad_drt1}}[1],${$nad{nad_state1}}[1],${$nad{nad_drt2}}[1],${$nad{nad_state2}}[1]); }
      else { $tempstr= "INVALID";}

   if ($kwords{cosmoci}) { if ($cnt) {rename("out.cosmo_ci.drt$cnt","$direct{JDIR}/$direct{COSMO}/out.cosmo");
                                      rename("phi_ground_outer_ci.drt$cnt","$direct{JDIR}/$direct{COSMO}/phi_ground_outer"); }
                              else   {rename("out.cosmo_ci","$direct{JDIR}/$direct{COSMO}/out.cosmo");
                                      rename("phi_ground_outer_ci","$direct{JDIR}/$direct{COSMO}/phi_ground_outer");}}

    if ($kwords{pciudgav} || $kwords{pciudg} ) { cp("$direct{JDIR}/TASKLIST.drt$cnt","TASKLIST"); }

     &cipart($tempstr,$cnt);  # calls ciudg.x/pciudg.x tmodel.pl ; optionally call_cipc (cipc), cutci.x ,exptvl.x, 
         

     unless (rename("cirefv","cirefv.drt$cnt")) {die("rename $!\n");};
     unless (rename("civfl", "civfl.drt$cnt")) {die("rename $!\n"); }
     unless (rename("civout", "civout.drt$cnt")) {die("rename $!\n"); }
     cp("ciudgls","$direct{list}/ciudgls.drt$cnt.sp");
     cp("ciudgsm","$direct{list}/ciudgsm.drt$cnt.sp");
     rename("ciudgls","ciudgls.drt$cnt.sp");  
     rename("ciudgsm","ciudgsm.drt$cnt.sp");  

   if ($kwords{"GEOMOPT"}) 
       { &appendtofile("ciudgls.drt$cnt.sp","$direct{list}/ciudgls.drt$cnt.all",$control{iter});
         &appendtofile("ciudgsm.drt$cnt.sp","$direct{list}/ciudgsm.drt$cnt.all",$control{iter});}

   foreach $i ( <nocoef_ci.*> )
     { my $newfilename=$i;
       $newfilename=~s/drt./drt$cnt/;
      print "moving $i to $direct{modir}/$newfilename.sp \n";
      rename("$i","$direct{modir}/$newfilename.sp"); }

     if ( -s "cipcls" ) { &appendtofile("cipcls","$direct{list}/cipcls.drt$cnt.all",$control{iter}) ;
                            rename ("cipcls","$direct{list}/cipcls.drt$cnt.sp");}
                                   
     if ( -s "ciudg.timings") { rename("ciudg.timings","ciudg.timings.drt$cnt");}
     if ( -s "ciudg.perf"   ) { rename("ciudg.perf","ciudg.perf.drt$cnt");}
 
     if ($kwords{cosmoci})  {rename("out.cosmo","$direct{JDIR}/$direct{COSMO}/out.cosmo_ci.drt$cnt");
                              rename("phi_ground_outer","$direct{JDIR}/$direct{COSMO}/phi_ground_outer.drt$cnt"); }

  return;
}


  sub prep_inputs {

   printf "\n ++++++++++++++++++ input file generation +++++++++++++ \n";
   chdir $direct{JDIR};
   cp("geom","$direct{work}/geom");
   if ($debug{debug_geo})
   {
    print "COLUMBUS geometry\n";
    open(GEOM,"<geom");
    while (<GEOM>)
    {
      print $_;
    }
    close(GEOM);
   }
   cp("infofl","$direct{work}/infofl");
   cp("control.run","$direct{work}/control.run");
   cp("geom","$direct{work}/geom");
   cp("infofl","$direct{work}/infofl");
   cp("control.run","$direct{work}/control.run");
#(tm)   cosmo is currently most likely not working properly
   if ($kwords{"cosmomcscf"} || $kwords{"cosmoci"}) {cp("cosmo.inp","$direct{work}/cosmo.inp")};
   printf " runc start: %s \n ", date();
   chdir $direct{work};

   printf " runc start: %s \n ", date();
   chdir $direct{work};
   if (  $kwords{turbocol} )
        { $/="\n";
          local ($atomsymbol,$charge,$x,$y,$z,$mass);
          open(GEOM,"geom") ||  die "could not open file: geom";
          open(TGEOM,">control.basis") || die "geom";
          printf TGEOM "\$coord \n";
          while( <GEOM> )
           { s/^ *//g; ($atomsymbol,$charge,$x,$y,$z,$mass) = split /\s+/ ;
             printf TGEOM "%10.6f %10.6f %10.6f %8s \n", $x,$y,$z,$atomsymbol; }
          close(GEOM);
          open(CBASIS,"$direct{JDIR}/control.basis");
           while ( <CBASIS>) { printf TGEOM ;}
          close(CBASIS);
          close(TGEOM);
         }
     else
        { # get symmetry unique atoms and cartesian coordinates
          open(ROTMAX,">rotmax") || die "rotmax";
           printf ROTMAX " 1  0  0 \n 0  1  0 \n 0  0  1\n";
          close(ROTMAX);
          unlink("bummer");
          callprog ( "unik.gets.x > unikls" );
        }
   if ( $kwords{argos} )
        { cp("$direct{JDIR}/argosin","argosin") ;
          callprog ( "argnew.x ");
          cp("argosin.new","argosin") ;
        }
   if ( $kwords{hermit} || $kwords{hermitmolden}  )
        { cp("$direct{JDIR}/daltaoin","daltaoin");
          cp("$direct{JDIR}/daltcomm","daltcomm");
          callprog ("hernew.x");
          cp("daltaoin.new","daltaoin");
          }
   if ( $kwords{seward})
         {
           # extract seward input from molcas.input
           $sewardinput=extractmolcas("$direct{JDIR}/molcas.input","SEWARD");
#          output lines separated by :
#         analyse $seward input and replace old coordinates by the new ones from geom.unik
          @sewardlines=split(/:/,$sewardinput);
           $/="\n";
           open GEOMUNIK,"<geom.unik";
           @geomunik=<GEOMUNIK>;
           close GEOMUNIK;
            $iatom=1;
            $i=0;
            $multipole_seward=0;
           while ($i<$#sewardlines)
            {  if (grep(/Basis set/i,$sewardlines[$i]))
                     { $i++; $i++;
                       while ( 1 )
                       {
                       if (grep (/charge/i,$sewardlines[$i])) { $i++;$i++;}
                       if (grep (/spherical/i,$sewardlines[$i])) { $i++;}
                       if ( grep (/end of basis/i,$sewardlines[$i])) { last;}
                          $symbol=$sewardlines[$i];
                          $symbol=~m/^(\S+) /;
                          $symbol=$1;
                          $iatom++;
                         #$direct{geom}unik[$iatom]=~s/^ *//;
                          ($scr,$oz,$x,$y,$z)=split(/\s+/,$geomunik[$iatom]);
                          $sewardlines[$i]=sprintf("%-6s  %12.8f   %12.8f   %12.8f",$symbol,$x,$y,$z);
                          #print "iatom=$iatom, sewardline=$sewardlines[$i] geomline=$direct{geom}unik[$iatom]";
                          $i++;
                       }
                       $i++;
                       next;
                     }
               if (grep(/MULTIPOLES/i,$sewardlines[$i])) { $i++; if ($sewardlines[$i] <4 ) { $sewardlines[$i]="  4";}
                                                           $multipole_seward=1; $i++; next;}
               if ( ($i == $#sewardlines-1) && ( ! $multipole_seward) )
                   { $sewardlines[$i+3]=$sewardlines[$i+1]; $sewardlines[$i+1]="MULTIPOLES "; $sewardlines[$i+2]="  4"; last;}
              $i++;
            }

           open FOUT,">molcas.input.seward";
           $sewardinput=join(':',@sewardlines); $sewardinput.=':'; $sewardinput=~s/:/\n/g;
            print FOUT $sewardinput;
           close FOUT;
           # should automatically update molcas.input from geom
         }



      if ($kwords{dalton2})
         { # enter new geometry from geom file to dalton2.input !
           $/="\n";
           open GEOMUNIK,"<geom.unik";
           @geomunik=<GEOMUNIK>;
           close GEOMUNIK;
           cp("$direct{JDIR}/dalton2.input","dalton2.input");
           open DALT2,"<dalton2.input";
           open DALTN,">dalton2.newinput";
             $iatom=1;
           while (<DALT2>)
            {
              if (/Charge=/) { print DALTN; chop; s/^.*Atoms=//; s/^ *//; s/ *.*$//; $natoms=$_;
                               for ($i=1;$i<=$natoms;$i++) { ($scr,$oz,$x,$y,$z)=split(/\s+/,$geomunik[$iatom]);
                                                             $nline=<DALT2>; chop $nline; $nline=~s/^ *//; ($symbol,$oldx,$oldy,$oldz)=split(/\s+/,$nline);
                                                             printf DALTN "%-6s %9.5f   %9.5f   %9.5f",$symbol,$x,$y,$z; $iatom++;}
                              next;}
               print DALTN;
            }
            close DALT2;
            close DALTN;
           system("mv dalton2.input dalton2.oldinput");
           system("mv dalton2.newinput dalton2.input");
         }
 }

##########################################################################################################################
sub potmat {
  #matruc_28jun07
  chdir "$direct{work}";
  printf "+++++++++++++ adding potential matrix to aoints ++++++++++++++++\n";

  &changekeyword("$direct{JDIR}/elpotin","elpotin","property","\'hmat\'");

  #fp: if specified in pot_reduce.in, the potential is reduced before the calculation
  $potred=keyinfile("$direct{JDIR}/pot_reduce.in","reduce");
  if ( $potred )
  {
    if ($control{iter} eq 1)
    {
     print "  reducing potential.xyz first\n";
     cp("$direct{JDIR}/potential.xyz","pot_old.xyz");
     cp("$direct{JDIR}/pot_reduce.in","pot_reduce.in");
     callprog("pot_reduce.py reduce > pot_reducels");
     cp("pot_reducels","$direct{list}/pot_reducels.sp");
     cp("potential.xyz","$direct{rest}/potential.xyz.reduced");
    }
    else
    {
     print "second iteration: not reducing potential.xyz as it should already be reduced.\n";
    }
  }
  else
  {
    cp("$direct{JDIR}/potential.xyz","potential.xyz");
  }
  cp("aoints","aoints.core");
  callprog("potential.x -m $control{coremem}");
  cp("elpotls","$direct{list}/elpotls.sp");
  cp("aoints.ptchrg","aoints");
  printf ">>>>> Added charge contributions to  1-e AO integrals<<<<<<\n";
}

###########################################################################################################################


#======================================================================
# Execute argos/hermit
#======================================================================

  sub aointegrals {

   chdir $direct{work};

   printf "++++++++++++++++++++ ao integral calculation ++++++++++++++++\n";

   if ( $kwords{hermit} ) { callprog("dalton.x -m $control{coremem} > hermitls");
                   cp("hermitls","$direct{list}/hermitls.sp");
                   cp("soinfo.dat","$direct{rest}/soinfo.dat.sp");}

   if ( $kwords{argos} ) { callprog("argos.x -m $control{coremem} ");
                   cp("argosls","$direct{list}/argosls.sp")}

   if ( $kwords{seward} ) {  callprog("molcas molcas.input.seward > molcas.output.seward ");
                    #system("molcas molcas.input.seward > molcas.output.seward 2>&1 ") == 0 || die("call to seward failed\n");
                    #print " Seward terminated successfully\n";
                    #system("rm -f ORDINT; ln -s molcas.OrdInt ORDINT");
                    system("rm -f ONEINT; ln -s molcas.OneInt ONEINT");
                    system("rm -f RUNFILE; ln -s molcas.RunFile RUNFILE");
                     cp("molcas.output.seward","$direct{list}/sewardls.sp");}

    if ($kwords{dalton2} ) {  unlink "DALTON.INP";
                      system("ln -s dalton2.input DALTON.INP");
                      system("$ENV{DALTON2}/bin/dalton.x");
                      cp("DALTON.OUT","$direct{list}/dalton2ls.sp");}


   if ( $kwords{ffield} ) { cp("$direct{JDIR}/fieldin","fieldin");
                    callprog("ffield.x -m $control{coremem} ");
                    mv("aoints.field","aoints");
              print ">>>>>>>>> added finite field integrals <<<<<<<<<\n";}

   }

###########################################################################################################################


   sub transmo {
   #felix plasser: after the soinfo.dat is available orbitals from a higher symmetry
   #   may be transformed to the current symmetry

   print "\n++++ orbital transformation to lower symmetry ++++\n";
   chdir "$direct{work}";

   if ( -f "$direct{JDIR}/transmoin" )
   {
      print " found transmoin\n";
      cp("$direct{JDIR}/transmoin","transmoin");
   }
   elsif ( -f "$direct{JDIR}/mcdrtin.high" )
   {
      print " found no transmoin but mcdrtin.high\n";
      print " ... trying to create an appropriate transmoin file\n";
      # take the order in mcdrtin as the order in transmoin
      $moorder="";
      open(MCH,"$direct{JDIR}/mcdrtin.high");
      $/="";
      $a =<MCH> ;
      @mchlines = split(/\//,$a); # separate at the "/"
      close(EFILE);

      @docc = split(/\n/,$mchlines[5]);
      for ($i = 1; $i<=$#docc; $i++)
      {
          $moorder.=$docc[$i];
      }
      @act = split(/\n/,$mchlines[6]);
      for ($i = 1; $i<=$#act; $i++)
      {
          $moorder.=$act[$i];
      }
      print "moorder=$moorder\n";
      open(TMOIN,">transmoin");
      print TMOIN "&input\n motype=2\n";
      print TMOIN "moorder=$moorder\n";
      print TMOIN "printlevel=2\n ixyz=123\n &end\n";
   }
   else
   {
      print " you have to provide either transmoin or mcdrtin.high!\n";
      die ("insufficient information for motran\n");
   }
   cp("$direct{JDIR}/mocoef.high","mocoef.high");
   cp("$direct{JDIR}/soinfo.high","soinfo.high");
   symlink("soinfo.dat","soinfo.low");

   callprog("transmo.x > transmols");

   cp("transmols","$direct{list}/transmols.start.sp");
   cp("mocoef.new","$direct{modir}/mocoef_trans.sp");
   symlink("mocoef.new","mocoef");
   }

###########################################################################################################################
#=====================================================================
# Execute  scfpq/scf_d
#======================================================================

  sub scfpart {

    local ($scf,$turbocol,$seward,$dalton2);

     chdir "$direct{work}";

    if ($kwords{dft}) {
          if ( ! $kwords{seward} ) {die("DFT calculations need seward integrals\n");}
         my $scfinput=extractmolcas("$direct{JDIR}/molcas.input","SCF");
         open FOUT,">molcas.input.scf";
         $scfinput=~s/:/\n/g;
         print FOUT $scfinput;
         close FOUT;
           callprog("molcas molcas.input.scf > molcas.output.scf ");
           print " ----------------------------------------------------------\n";
           cp("molcas.output.scf","$direct{list}/dftls.sp");
           cp("molcas.ScfOrb","$direct{modir}/mos.dft.sp");
           cp("molcas.ScfOrb","mocoef_lumorb");
            return;
              }

    if ($kwords{scf} ) {

    printf "++++++++++++++++++++ SCF calculation +++++++++++++++\n";

    cp("$direct{JDIR}/scfin","scfin");

    if ( $kwords{turbocol} ) { cp("$direct{JDIR}/infofl.argos","infofl");
                       callprog("m_scfturbo.x");
                       callprog("scf_d.x -m $control{coremem} > scf_dls");
                       cp("mocoef","$direct{modir}/mocoef_scf.sp");
                       cp("mos","$direct{modir}/mos.scf_d.sp");
                       cp("scf_dls","$direct{list}/scf_dls.sp");
                     }
    elsif ( $kwords{seward}) { # modify second line of default scfin input
                       # to ensure reading integrals in MOLCAS format
                       my ( @sline, @scr );
                       $/="\n";
                       open SCFIN,"<scfin";
                       @sline=<SCFIN>;
                       close SCFIN;
                       chop $sline[1];
                       @scr = unpack("a4" x 21,$sline[1]);
                       $scr[20]="   2";
                       $sline[1]=pack("a4" x 21,@scr);
                       $sline[1].="\n";
                       open SCFIN,">scfin";
                       print SCFIN @sline;
                       close SCFIN;
                       callprog("scfpq.x -m $control{coremem}");
                       cp("mocoef","$direct{modir}/mocoef_scf.sp");
                       cp("mocoef_lumorb","molcas.RasOrb");
                       cp("scfls","$direct{list}/scfls.sp"); }
     elsif ( $kwords{dalton2} ) {  # modify second line of default scfin input
                           # to ensure reading integrals in dalton2 format
                       my ( @sline, @scr );
                       $/="\n";
                       open SCFIN,"<scfin";
                       @sline=<SCFIN>;
                       close SCFIN;
                       chop $sline[1];
                       @scr = unpack("a4" x 21,$sline[1]);
                       $scr[20]="   3";
                       $sline[1]=pack("a4" x 21,@scr);
                       $sline[1].="\n";
                       open SCFIN,">scfin";
                       print SCFIN @sline;
                       close SCFIN;
                       callprog("scfpq.x -m $control{coremem}");
                       cp("mocoef","$direct{modir}/mocoef_scf.sp");
                       cp("scfls","$direct{list}/scfls.sp"); }

             else
                    { callprog("scfpq.x -m $control{coremem}");
                       cp("mocoef","$direct{modir}/mocoef_scf.sp");
                       cp("scfls","$direct{list}/scfls.sp");
                     }
   }}

###########################################################################################################################

  sub caspt2 {
         print " --------------------------Caspt2 -------------------------\n";
         $caspt2input=extractmolcas("$direct{JDIR}/molcas.input","CASPT2");
         open FOUT,">molcas.input.caspt2";
         $caspt2input=~s/:/\n/g;
         print FOUT $caspt2input;
         close FOUT;
         system("rm -f JOBIPH; ln -s  molcas.JobIph JOBIPH");
         #system("molcas molcas.input.caspt2 > molcas.output.caspt2 2>&1 ") == 0 || die("call to caspt2 failed\n");
         callprog("molcas molcas.input.caspt2 > molcas.output.caspt2 ");
         print " ----------------------------------------------------------\n";
         cp("molcas.output.caspt2","$direct{list}/caspt2ls.sp");
   return;
   }

###########################################################################################################################

  sub ccsdt {
   local ($ccsd,$ccsdt);
         print " --------------------------CCSDT  -------------------------\n";
         $motrainput=extractmolcas("$direct{JDIR}/molcas.input","MOTRA");
         $motrainput=~s/:/\n/g;
         open FOUT,">molcas.input.motra";
         print FOUT $motrainput;
         close FOUT;
         system("cp molcas.RasOrb INPORB");
#        system("molcas molcas.input.motra > molcas.output.motra 2>&1 ") == 0 || die("call to motra failed\n");
         callprog("molcas molcas.input.motra > molcas.output.motra ");
         if ($kwords{ccsd})
               { $ccsdtinput=extractmolcas("$direct{JDIR}/molcas.input","CCSD");
                 open FOUT,">molcas.input.ccsd";
                 $ccsdtinput=~s/CCSD/CCSDT/;
                 $ccsdtinput=~s/:/\n/g;
                 $triples=grep(/tripl/i,$ccsdtinput);
                 if ( $triples) {die("inconsistent input CCSD contains triples\n");}
                 print FOUT "$ccsdtinput\n";
                 close FOUT;
                 system("rm -f JOBIPH; ln -s JOBIPH molcas.JobIph");
                # system("molcas molcas.input.ccsd > molcas.output.ccsd 2>&1 ") == 0 || die("call to ccsd failed\n");
                 callprog("molcas molcas.input.ccsd > molcas.output.ccsd ");
                 system ("grep -A 3 '  Total energy (diff) :  ' molcas.output.ccsd");
                 print " ----------------------------------------------------------\n";
                 system("rm JOBIPH");
                 cp("molcas.output.ccsd","$direct{list}/ccsdls.sp");
               }
         if ($kwords{ccsdt})
               { $ccsdtinput=extractmolcas("$direct{JDIR}/molcas.input","CCSDT");
                 open FOUT,">molcas.input.ccsdt";
                 $ccsdtinput=~s/:/\n/g;
                 $triples=grep(/tripl/i,$ccsdtinput);
                 if (! $triples) {die("inconsistent input CCSDT contains no triples\n");}
                 print FOUT "$ccsdtinput\n";
                 close FOUT;
                 system("rm -f JOBIPH; ln -s JOBIPH molcas.JobIph");
                 #system("molcas molcas.input.ccsdt > molcas.output.ccsdt 2>&1 ") == 0 || die("call to ccsdt failed\n");
                 callprog("molcas molcas.input.ccsdt > molcas.output.ccsdt ");
                 system ("grep -A 3 '  Total energy (diff) :  ' molcas.output.ccsdt");
                 print " ----------------------------------------------------------\n";
                 system("rm JOBIPH");
                 cp("molcas.output.ccsdt","$direct{list}/ccsdtls.sp");
               }
   return;
   }

###########################################################################################################################


#=====================================================================
# Execute  mcdrt/mcuft
# Consider MCSCF state averaging
#======================================================================

  sub mcdrtpart {


     chdir "$direct{work}";


   if ($debug{debug_mcscf}) {printf "debug: mcdrtpart ndrt=$kwords{mcscfndrt}\n";}
    for ($i=1; $i <= $kwords {mcscfndrt}; $i++)
   {printf " >>>>>>>>>>>>> info: preparing drt no $i out of $ndrt <<<<<<\n" ;

   cp("$direct{JDIR}/mcdrtin.$i","mcdrtin");
   callprog("mcdrt.x -m $control{coremem} < mcdrtin > mcdrtls");
   cp("mcdrtls","$direct{list}/mcdrtls.drt$i.sp");
   callprog("mcuft.x");
   if ( $kwords{mcscfndrt} > 1)
       { for $j ("mcdrtls","mcdrtfl","mcuftls","mcdftfl","mcoftfl")
             { unless (rename("$j","$j.$i")) {die("rename $!\n");}}
   }}
  }

###########################################################################################################################

#=====================================================================
# Execute  mcscf/mcscf_d
#======================================================================
  sub mcscfpart {

     local ($cosmo,$mcscf,$scf,$turbocol,$seward,$rasscf,$ccsdt,$av,$dalton2,$irasscf);
     $ccsdt = $kwords{ccsdt} || $kwords{ccsd};

     chdir "$direct{work}";


     if (($kwords{rasscf} && (! $kwords{mcscf}) ) || $kwords{irasscf} )
       {
       # print "---------------------------- RASSCF ----------------------------------------\n";
         $rasscfinput=extractmolcas("$direct{JDIR}/molcas.input","RASSCF");
         if ( grep(/jobiph/i,$rasscfinput)) {print "JOBIPH keyword in runc not supported - replacing by lumorb \n"; $rasscfinput=~s/jobiph/LUMORB/i;}
         if ( grep(/core/i,$rasscfinput)) {print "CORE keyword in runc not supported, use scf instead - replacing by lumorb\n"; $rasscfinput=~s/core/LUMORB/i;}
         if ( ! grep(/lumorb/i,$rasscfinput)) {die("activate LUMORB for rasscf\n $rasscfinput\n");}
         if (  (! grep(/OUTORB/,$rasscfinput)) )
           { print "adding OUTORBITALS CANONICAL ...\n";
             $rasscfinput=~s/(:End of Input)/:OUTORBITALS: CANONICAL\1/i; }
         open FOUT,">molcas.input.rasscf";
         $rasscfinput=~s/:/\n/g;
         print FOUT $rasscfinput;
         close FOUT;
         if ( -f "molcas.RasOrb" ) {system("cp  molcas.RasOrb  INPORB");}
         elsif ( -f "$direct{JDIR}/molcas.RasOrb") {system("cp  $direct{JDIR}/molcas.RasOrb  INPORB");}
         else {die("cannot run a rasscf calculation - no input orbitals (molcas.RasOrb)\n");}
         if ($kwords{pmolcas}) { distribute("INPORB");}
         callprog("molcas molcas.input.rasscf > molcas.output.rasscf");
         cp("molcas.output.rasscf","$direct{list}/rasscfls.sp");
         cp("molcas.RasOrb","$direct{modir}/molcas.RasOrb");
         if ($kwords{mcscf}) {cp("molcas.RasOrb","mocoef_lumorb");}
         if (! $kwords{irasscf}) { return } ;
       }


     if ( $kwords{mcscf} ) {

    printf "++++++++++++++++++++ MCSCF calculation +++++++++++++++\n";
       getmcscfinfo();

       cp("$direct{JDIR}/mcscfin","mcscfin");
      if (-s "$direct{JDIR}/mcdenin")
      {
         print "found mcdenin file. This file will be used without any alterations!\n";
         &add_mcscf_flag("30");
         cp("$direct{JDIR}/mcdenin","mcdenin");
      }
      else
      {
       if ( $kwords{"nadcoupl_mc"} || $kwords{"mcscfprop"}){
#fp: create mcdenin which contains the input for the density matrices computed by mcscf.x
#   to do this collect all densities that are need for computation of either
#      properties, gradients, or couplings
#    to maximize the efficiency one could rewrite this part to produce the minimum number of unique bra states (cf. mcscf.txt)

#      add flag 30 to NPATH for density computation
             &add_mcscf_flag("30");

             print "Writing mcdenin file for mcscf.x\n";
             open TMIN, ">mcdenin";
             print TMIN "MCSCF\n";
             if ( $kwords{"mcscfprop"})
             # if properties are computed all diagonal densities are needed
             {
                open(MCSCFIN,"mcscfin");
                $/="\n";
                while (<MCSCFIN>)
                { if ( $debug{debug_mcscf} ) { print ;}
                    if (/NAVST/i) {s/[A-Za-z(),=]/ /g; s/^ *//g;
                    @x = split(/\s+/,$_);
                    $navst[$x[0]] =$x[1] ;
                    $ndrt=$ndrt+1;
                    if ($debug{debug_mcscf}) {print "exptvlmc debug navst @x \n ";} }
                }
                close(MCSCFIN);
                $idrt = 1; # for now only DRT #1 works
                for ($istate=1;$istate<=$navst[$idrt];$istate++)
                {
                    $temp=join( "  ", ($idrt, $istate, $idrt, $istate));
                    print TMIN "$temp\n";
                }

             }
             else
             {
                for ($igrad=1;$igrad<=$ngrad;$igrad++){
                    $temp=join( "  ", ($grad_drt[$igrad], $grad_state[$igrad], $grad_drt[$igrad], $grad_state[$igrad]));
                    print TMIN "$temp\n";
                 }
             }
             if ( $kwords{"nadcoupl_mc"})
             {
               for ($inad=1;$inad<=$nnadcoupl;$inad++){
                  $temp=join( "  ", ($nad_drt1[$inad], $nad_state1[$inad], $nad_drt2[$inad], $nad_state2[$inad]));
                  print TMIN "$temp\n";
               }
             }
             close (TMIN);
          }
          elsif ( $kwords{"samcgrad"})
          # the state for which the gradient is computed is written in transmomin
          {
          #      add flag 30 to NPATH for density computation
              &add_mcscf_flag("30");
              cp("$direct{JDIR}/transmomin","mcdenin");
          }
         } # creation of mcdenin file
# copy start MOs
#
#  the following line is wrong
#
       if ( $kwords{scf} ) { cp("$direct{modir}/mocoef_scf.sp","mocoef");}
        else { print "info: taking mocoef file from $direct{JDIR}/mocoef\n";
                cp("$direct{JDIR}/mocoef","mocoef");}

# special treatment of direct mcscf

       if ( $kwords{turbocol} )
         { printf " preparing control file ... \n";
           cp("$direct{JDIR}/infofl.argos","infofl");
           callprog("m_mcscfturbo.x");
           callprog("mcscf_d.x -m $control{coremem} 1> mcscf_stderr");}
       elsif ( $kwords{seward} || -f "molcas.input.seward" )
# add flag 26 to NPATH (SEWARD integrals)
# add flag 29 to NPATH (read LUMORB format)
         { $/="\n";
           my @inp;
            if (! -f "mcscfin") {die ("could not find mcscfin\n");}
           open MCSCFIN,"<mcscfin";
           @inp=<MCSCFIN>;
           close MCSCFIN;
           for ($i=1;$i<$#inp; $i++) { if (grep(/NPATH/i,$inp[$i]))
                                      {chop $inp[$i]; $inp[$i]=~s/, *$//; $inp[$i]=~s/ *$//; $inp[$i].=",26";
                                       if ($irasscf) {$inp[$i]=~s/,13/,-13/; $inp[$i].=",29\n";}last;}}
           open MCSCFIN,">mcscfin";
           print MCSCFIN @inp;
           close MCSCFIN;
           callprog("mcscf.x -m $control{coremem}");
         }
       elsif ( $kwords{dalton2} )
#      add flag 27 to NPATH
           { $/="\n"; my @inp;  if (! -f "mcscfin") {die ("could not find mcscfin\n");}
                        open MCSCFIN,"<mcscfin";
           @inp=<MCSCFIN>;
           close MCSCFIN;
           for ($i=1;$i<$#inp; $i++) { if (grep(/NPATH/i,$inp[$i]))
                                      {chop $inp[$i]; $inp[$i]=~s/, *$//; $inp[$i]=~s/ *$//; $inp[$i].=",27\n";last;}}
           open MCSCFIN,">mcscfin";
           print MCSCFIN @inp;
           close MCSCFIN;
           callprog("mcscf.x -m $control{coremem}");
         }
       else
# normal mcscf
       {   callprog("mcscf.x -m $control{coremem}");}

       cp("mcscfls","$direct{list}/mcscfls.sp");
       cp("mcscfsm","$direct{list}/mcscfsm.sp");
       cp("restart","$direct{rest}/restart.sp");
       if ($kwords{cosmomcscf}) {
                     cp("out.cosmo","$direct{JDIR}/$direct{COSMO}/out.cosmo_mcscf");
                     cp("phi_ground_outer","$direct{JDIR}/$direct{COSMO}/phi_ground_outer_mcscf"); }
#felix plasser: mocoef_mc and nocoef_mc are printed directly by mcscf.x to save this call
#   and allow for a transfer of the symmetry labels
#        callprog("mofmt.x < mofmtin > mofmtls ");

       cp("mocoef_mc","$direct{modir}/mocoef_mc.sp");
       cp("nocoef_mc","$direct{modir}/nocoef_mc.sp");
       my @nocoefs = <nocoef_mc*.d*.*>;
       foreach $nocoef ( @nocoefs )
       {
          if ($debug{debug_runc}) {print "copying $nocoef\n";}
          cp("$nocoef","$direct{modir}/$nocoef.sp");
       }




       if ( $av == 0 )
        {  createmcpcin(1,1);
           callprog("mcpc.x -m $control{coremem} < mcpcin > mcpcls");
           cp("mcpcls","$direct{list}/mcpcls.drt1.state1.sp"); }
       else
        {
     open(MCSCFIN,"<mcscfin");
     local ( $state,$drt, $indrt,$ndrt,@navst );
     $/="\n";
     $ndrt=0;
     $indrt=0;
         while (<MCSCFIN>)
          { if (/NAVST/i) {s/[A-Za-z(),=]/ /g; s/^ *//g;
                          @x = split(/\s+/,$_);
                          if ( $debug{debug_mcscf}) {print "x(0..1)= $x[0],$x[1]\n";}
# state-averaging ok 01 versus 12 ??
                          $navst[$x[0]] =$x[1] ;
                          $ndrt=$ndrt+1;
                          $indrt=0;
                          if ($debug{debug_mcscf}) {print "ndrt=$ndrt debug navst @x \n ";} }
          }
#
#    @navst[drt#] = x
#
         close(MCSCFIN);
         for ($drt=1; $drt <= $ndrt ; $drt++)
          { if ( $debug{debug_mcscf} ) {print "drt=$drt navst=$navst[$drt]\n"; }
            for ( $state=1; $state <= $navst[$drt] ; $state++)
             { createmcpcin( $state,$drt );
              if ( $debug{debug_mcscf} ) {print "drt=$drt state=$state \n";}
               callprog("mcpc.x -m $control{coremem} < mcpcin > mcpcls");
               cp("mcpcls","$direct{list}/mcpcls.drt$drt.state$state.sp");
           }
           }
       }
    }

     if ($kwords{rasscf} )
       {
         print "---------------------------- RASSCF ----------------------------------------\n";
         $rasscfinput=extractmolcas("$direct{JDIR}/molcas.input","RASSCF");
         if ( grep(/jobiph/i,$rasscfinput)) {print "JOBIPH keyword in runc not supported - replacing by lumorb \n"; $rasscfinput=~s/jobiph/LUMORB/i;}
         if ( grep(/core/i,$rasscfinput)) {print "CORE keyword in runc not supported, use scf instead - replacing by lumorb\n"; $rasscfinput=~s/core/LUMORB/i;}
         if ( ! grep(/lumorb/i,$rasscfinput)) {die("activate LUMORB for rasscf\n $rasscfinput\n");}
         print "keywords mcscf and rasscf present - disabling orbital optimization in rasscf!\n"; $rasscfinput=~s/(:End of Input)/:CIONLY\1/i;
         if (  (! grep(/OUTORB/,$rasscfinput)) )
           { print "adding OUTORBITALS CANONICAL ...\n";
             $rasscfinput=~s/(:End of Input)/:OUTORBITALS: CANONICAL\1/i; }
         open FOUT,">molcas.input.rasscf";
         $rasscfinput=~s/:/\n/g;
         print FOUT $rasscfinput;
         close FOUT;
     #   system("rm -f INPORB; ln -s INPORB mocoef_mc.lumorb ");
         if ( ! -f "mocoef_mc.lumorb" ) {die("cannot run a rasscf calculation - no input orbitals\n");}
         system("cp  mocoef_mc.lumorb  INPORB");
         #system("molcas molcas.input.rasscf > molcas.output.rasscf 2>&1 ") == 0 || die("call to rasscf failed\n");
         callprog("molcas molcas.input.rasscf > molcas.output.rasscf ");
         system ("grep 'RASSCF energy             ' molcas.output.rasscf");
         system ("grep 'Maximum BLB matrix element' molcas.output.rasscf");
         print "----------------------------------------------------------------------------\n";
         cp("molcas.output.rasscf","$direct{list}/rasscfls.sp");

       }

 }

######################################################################################################################################


#=====================================================================
# Execute  cidrt and tran.x/tran_d.x
#======================================================================

  sub drtpart {

     local ($ndrt,$mcscf,$scf,$seward,$motra,$dalton2,$turbocol,$ciudgmom,$nocidrt,$rasscf);

     chdir "$direct{work}";

     cp("$direct{JDIR}/tranin","tranin");

     if ( -s "$direct{JDIR}/cidrtmsin")
          { callprog( "cidrtms.x -m $control{coremem} < $direct{JDIR}/cidrtmsin > cidrtmsls ");
            cp("cidrtmsls","$direct{list}/cidrtmsls.sp"); }
     else
       { for ($i=1; $i<=$kwords{ciudgndrt}; $i++) 
       { callprog("cidrt.x -m $control{coremem} < $direct{JDIR}/cidrtin.$i > cidrtls.drt$i ");
       cp("cidrtls.drt$i","$direct{list}/cidrtls.drt$i.sp");
        mv("cidrtfl","cidrtfl.$i");}} 

     symlink("cidrtfl.1","cidrtfl"); # for tran 
     if ( $kwords{mcscf} ) { cp("$direct{modir}/mocoef_mc.sp","mocoef");
                     printf " using the MCSCF MOs ... \n" ;
                     &changekeyword("$direct{JDIR}/tranin","tranin"," LUMORB",0)}
     elsif ( $kwords{rasscf} ) { &changekeyword("$direct{JDIR}/tranin","tranin"," LUMORB",1);
                       cp("tranin","$direct{JDIR}/tranin");
                       cp("$direct{modir}/molcas.RasOrb","mocoef_lumorb");
                        printf " using the RASSCF MOs ... \n";}
     elsif ( $kwords{scf} ) {cp("$direct{modir}/mocoef_scf.sp","mocoef");
                     printf " using the SCF MOs ... \n" ; }
     elsif ( $kwords{dft} ) { #cp("$direct{modir}/mocoef_scf.sp","mocoef");
                    &changekeyword("$direct{JDIR}/tranin","tranin"," LUMORB",1);
                       cp("tranin","$direct{JDIR}/tranin");
                     printf " using the KS MOs ... \n" ; }
     else { if ( ! -s "$direct{JDIR}/mocoef" )
           { die "missing $direct{JDIR}/mocoef file ... \n";}
            cp("$direct{JDIR}/mocoef","mocoef");
            printf "using $direct{JDIR}/mocoef ... \n";}

     if ( $kwords{turbocol} ) { open(CBASIS,"control.basis");
                        $/="\n";
                        open(CONTROL,">control");
                        while (<CBASIS>) {print CONTROL ;  }
                        close(CBASIS);
                        printf CONTROL "\$lock off \n\$scftol 1.d-11
\$amatrix \n\$mointunit \n type=intermed unit=61  size=10 file=halfint
\$end\n" ;
                        close CONTROL;
                        callprog("tran_d.x -m $control{coremem}");
                        }
       elsif ( $kwords{seward}  || $kwords{intprogram} eq "seward" )
        { if ($kwords{motra})
          {
         $motrainput=extractmolcas("$direct{JDIR}/molcas.input","MOTRA");
         $motrainput=~s/:/\n/g;
         open FOUT,">molcas.input.motra";
         print FOUT $motrainput;
         close FOUT;
#        system("cp mocoef_mc.lumorb molcas.RasOrb");
         system("cp mocoef_mc.lumorb INPORB");
#        system("$MOLCAS molcas.input.motra > molcas.output.motra 2>&1 ") == 0 || die("call to motra failed\n");
         system("molcas molcas.input.motra > molcas.output.motra 2>&1 ") == 0 || die("call to motra failed\n");
         }
          else
          {&changekeyword("$direct{JDIR}/tranin","tranin"," SEWARD",1);
          callprog("tran.x -m $control{coremem} ");
        }}
       else
        { if ($kwords{dalton2}) { &changekeyword("$direct{JDIR}/tranin","tranin"," DALTON2",1);}
          callprog("tran.x -m $control{coremem} "); }
     unlink (cidrtfl); 
#
#  at this point there are only the files  cidrtfl.1 ... cidrtfl.ndrtci 
#

    }

############################################################################################################################################

#=====================================================================
# Execute  cisrt/pcisrt/cisrt2x and ciuft if necessary
#======================================================================

  sub preci {

     local ($mcscf,$scf,$turbocol,$ciudg,$motra);

     chdir "$direct{work}";
      &getciinfo();
     cp("$direct{JDIR}/cisrtin","cisrtin");

     if ($kwords{turbocol})
       { callprog("cisrt2x.x -m $control{coremem}");}
     if ($kwords{motra})
         {changekeyword("$direct{JDIR}/cisrtin","cisrtin"," MOLCAS",1);
         system("ln -s molcas.RunFile RUNFILE");
         system("ln -s molcas.TraOne TRAONE");
         system("ln -s molcas.TraInt TRAINT");
         system("ln -s molcas.TraInt1 TRAINT1");
         system("touch moints");
         callprog("cisrt_molcas.x -m $control{coremem}");}
     }
############################################################################################################################################

#=====================================================================
# Execute  ciudg/ciudg_d/pciudg
#======================================================================

 sub cipart {
     my ($transitionstring,$drt) ; ( $transitionstring,$drt ) = @_;

     local ($mcscf,$scf,$seward,$dalton2,$motra,$turbocol,$ciudg,$pciudg,$ivmode);
     local ($froot,@x,$i,$ci,$teaqcc,$aqcc,$acpf,$name);
     
     if ($kwords{debug_runc}) {print "cipart: $transitionstring\n"; }

     if ($kwords{cosmoci})   { if ($kwords{seward}) {die("cosmo and seward not simultaneously supported for ci\n");}
                             $cosmocalc=keyinfile("$direct{JDIR}/ciudgin","cosmocalc");
                             print "runc: found cosmocalc= $cosmocalc\n";
                             if ($cosmocalc == 2)
                                { if ( ! -f "$direct{JDIR}/out.cosmo" ) {die("could not find $direct{JDIR}/out.cosmo\n");}
                                  if ( ! -f "$direct{JDIR}/phi_ground_outer" ) {die("could not find $direct{JDIR}/phi_ground_outer\n");}}
                             }
#
#   activate the restart for ciudg.x in case of geometry optimization
     $fr=keyinfile("ciudgin","froot");
     if (! $fr) { $fr=keyinfile("ciudgin","nroot"); }
     if (! $fr) { print " no nroot/froot entry in ciudgin, using default nroot=1 \n"; $fr=1;}
     if ($kwords{"GEOMOPT"} || $kwords{"nadcoupl"})  
                { &changekeyword("ciudgin","ciudgin","iden","2");                    
                  if ($kwords{cirestart} && $control{itercount} > 1) 
                  { &changekeyword("ciudgin","ciudgin"," NOLDV",$fr);  
                    &changekeyword("ciudgin","ciudgin"," IVMODE",1);    
                    &changekeyword("ciudgin","ciudgin"," NBKITR",0);} 
                  if (! grep (/INVALID/,$transitionstring)) { &appendtonl("ciudgin","ciudgin",$transitionstring);}} 
              else { &changekeyword("ciudgin","ciudgin","iden","1");
                      print "setting iden=1 in ciudgin\n"; }

      if ($kwords{ciudg} || $kwords{ciudgav})
       { callprog("ciudg.x -m $control{coremem}"); }
      elsif  ($kwords{pciudg} || $kwords{pciudgav})
       {  print "info: starting pciudg section ... \n";
      pcallprog("pciudg.x -m $control{pcoremem} ");
      system("$ENV{COLUMBUS}/perlscripts/tmodel.pl ciudg.timings > ciudg.perf");
       }
#
#  now read out the file energy to extract E1,E2,DE for all computed transitions 
#
       &get_energydata($drt);
       &call_cipc("");
  }
####################################################################################################################################


  sub call_cipc {
  # Felix Plasser, 2011-05-31
  # subroutine for calling cipc.x and optionally civecconsolidate
 #     finalv=0 only converged ci vectors on file 
#     finalv=1 full transformed subspace on file
#     finalv=-1 no ci vector on file 
#     default for seriel operation  finalv=1
      my $post; ( $post ) = @_; 
      $docipc=&keyinfile("$direct{JDIR}/ciudgin","finalv");
      if ($docipc != -1)
      {
        open CIPCIN,">cipcin";
        if ( $kwords{"detprt"} ) {print CIPCIN " 3\n";}
        else {print CIPCIN " 1\n";}
        close CIPCIN;
        callprog("cipc.x -m $control{coremem} < cipcin > cipcls");
        cp("cipcls","$direct{list}/cipcls$post");

        if ( $kwords{"detprt"} )
        {
            print "Reducing to a unique determinant expansion...\n";
            open CID,"<cidrtin";
            $/="\n";
            while(<CID>)
            {
                if (/total number of electrons/)
                {
                    @line=split(/\s+/,$_);
                    $nelec=$line[1];
                }
            }
            close CID;

            open CIC,">civecconsolidate.in";
            print CIC "$nelec 0\n";
            close CIC;
            system("cat eivectorshead eivectors >eivectors.combined");

            # create a fake bummer file since, this program does not do that
            open BM,">bummer";
            print BM "normal termination\n";
            close BM;

            callprog("civecconsolidate eivectors.combined slaterfile eivectors.red slaterfile.red consolidatefile  <civecconsolidate.in>civecconsolidate.out");
        }
      }
      else {
         print "finalv = $docipc, cipc.x skipped\n";
      }
  }

#############################################################################################################################

#=====================================================================
# For separate property calculations we need to recalculate
# either the one-electron integrals or in the case of mcscf
# state-averaging the two-electron integrals as well
#======================================================================

  sub preprop {

     local ($x,$argos,$dalton,$turbocol,$av);

     chdir "$direct{work}";

     if ($kwords{turbocol} )
      { cp("$direct{JDIR}/cidrtin","cidrtin");
        callprog("cidrt.x -m $control{coremem} < cidrtin > cidrtls ");
        cp("cidrtls","$direct{list}/cidrtls.sp");
        open(CBASIS,"control.basis");
        $/="\n";
        open(CONTROL,">control");
        while (<CBASIS>) {print CONTROL ;  }
        close(CBASIS);
        printf CONTROL "\$amatrix \n\$end \n " ;
        cp("$direct{JDIR}/tranin.property","tranin");
        callprog("tran_d.x -m $control{coremem}");
       }
     if ($kwords{dalton})
       {# 
        {open(HERMITIN,"daltcomm");
         $/="";
         $x=<HERMITIN>;
        close(HERMITIN); }
         $x=~s/NOSUP/NOSUP\n.NOTWO/;
         open(DCOMM,">daltcomm");
          print DCOMM "$x" ;
        close(DCOMM);
        callprog("dalton.x -m $control{coremem} > hermitls.property");
       }
     if ($kwords{argos} )
       { #if ($av)
        {open(ARGOSIN,"argosin");
         $/="\n";
         open(ARGNEW,">argosin.scr");
         $x=<ARGOSIN>;
         substr($x,26,1)="1";
         print ARGNEW $x ;
        while (<ARGOSIN>) { print ARGNEW  ;  }
        close(ARGOSIN);
        close(ARGNEW);
        unless (rename("argosin.scr","argosin")) {die("rename $!\n"); }}
        callprog("argos.x -m $control{coremem} ");
       }
  }

#############################################################################################################################

#=====================================================================
# Now we contract the AO-property integrals with the density calculated
# from the NO coefficient file through exptvl.x
#=====================================================================

  sub exptvlscf {


       if ($kwords{seward}) { die ("currently no support for expectation values with seward \n");}

     chdir "$direct{work}";

       cp("$direct{JDIR}/propin","propin");
       printf " ------ scf properties ----------\n";
       cp("$direct{modir}/mocoef_scf.sp","mocoef_prop");
       cp("mocoef_prop","mocoef");
       callprog("exptvl.x -m $control{coremem} ");
       cp("propls","$direct{list}/propls.scf.sp");

 }
#############################################################################################################################

  sub exptvlci {


     chdir "$direct{work}";

       cp("$direct{JDIR}/propin","propin");

       while (<$direct{modir}/nocoef_ci.*.sp>)
        {   my ($num,$drt,$state);
           #print "MOCOEFS: $_ \n";
            $num=$_;
            $num=~s/^.*nocoef_ci\.//g;
            $num=~s/\.sp$//g;
            $drt=$num; $drt=~s/drt//g; $drt=~s/\..*$//g;
            $state=$num; $state=~s/.*state//g;
            print " --- ci properties for state $state of  DRT $drt ----\n";
            cp("$direct{modir}/nocoef_ci.drt$drt.state$state.sp","mocoef_prop");
            callprog("exptvl.x -m $control{coremem} ");
            cp("propls","$direct{list}/propls.ci.drt$drt.state$state.sp");
            unlink("mocoef_prop");
         }
    }
#################################################################################################################################

  sub exptvlmcscf {

     local ($scfprop,$ciprop,$mcscfprop);
     local ($ndrt,$navst,@x,$i,$j);

     chdir "$direct{work}";

     if ($kwords{seward}) {die(" no support for properties with seward at mcscf level\n");}

     cp("$direct{JDIR}/propin","propin");

     open(MCSCFIN,"mcscfin");
     $/="\n";
#       $ndrt=0;
#       while (<MCSCFIN>)
#       { if ( $debug ) { print ;}
#       if (/NAVST/i) {
 #           s/[A-Za-z(),=]/ /g; s/^ *//g;
#           @x = split(/\s+/,$_);
#           $navst[$x[0]] =$x[1] ;
 #           $ndrt=$ndrt+1;
#           if ($debug) {print "exptvlmc debug navst @x \n ";} }
#       }


     $navst=0;
     while (<MCSCFIN>)
     { if ( $debug{debug_mcscf} ) { print ;}
       if (/NAVST/i) {$navst=1;}
     }
     close(MCSCFIN);

     if ( ! $navst  )
     {
        printf "\n=========== mcscf properties (single state) =========\n";
               cp("$direct{modir}/nocoef_mc.sp","mocoef_prop");
               cp("$direct{modir}/mocoef_mc.sp","mocoef");
               callprog("exptvl.x -m $control{coremem} ");
               cp("propls","$direct{list}/propls.mcscf.drt1.state1.sp");
               unlink("mocoef_prop") ;
     }
     else
     {
        printf "\n========= state properties for state-averaged MCSCF =========\n";
        chdir ($direct{modir});
        @x = <nocoef_mc*.d*.st??.sp> ;
        chdir ($direct{work});
# just look for all possible NO files and compute the state properties
#        for ($drt1=1; $drt1 <= $ndrt; $drt1++) {
#           for ($state1=1; $state1 <= $navst[$drt1]; $state1++) {
#             $fstate1=sprintf("%02d",$grd_state1);
#               
#           }
#        }
        foreach $i ( @x )
        {
           $_=$i;
           cp("$direct{modir}/$i","mocoef_prop");
#           if (!/-/) 
#           { 
#             print "property\n";
#             @parts = split(/\./,$i);
#             print @parts;
#           }
#           else
#           {
#             print "transition\n";
#           }
           $i=~s/nocoef_mc/mcscf/;

           print "$i\n";
           callprog("exptvl.x -m $control{coremem}  ");
           cp("propls","$direct{list}/propls.$i");
           unlink("mocoef_prop") ;
        }
     }

#
# restore restart mocoef_mc.sp mcscfsm hdiagf mchess
#
#              unlink("mocoef");
#              unlink("restart");
#              cp("$direct{modir}/mocoef_mc.sp","mocoef");
#              cp("$rest/restart.sp","restart");
#              unlink("mcscfsm");
#         unless (rename("mcscfin.old","mcscfin")) {die("rename $!\n"); }
#         unless (rename("mcscfsm.old","mcscfsm")) {die("rename $!\n"); }
#         unless (rename("restart.old","restart")) {die("rename $!\n"); }
#         unless (rename("mcd1fl.old","mcd1fl")) {die("rename $!\n"); }
#         unless (rename("mcd2fl.old","mcd2fl")) {die("rename $!\n"); }
#        unless (rename("hdiagf.old","hdiagf")) {die("rename $!\n"); }
#         if (-s "hdiagf.old") { rename ("hdiagf.old", "hdiagf")}
#         if (-s "mchess.old") { rename ("mchess.old", "mchess")}
  }
######################################################################################################################


   sub info4molden {

    local($control{iter},$line);
    local($imd, $au2angs,$icount);
    local($geomfile,$geoconvfl,$geomout,$convout,$allout);
    ($control{iter})=@_;
    $/="\n";
#
     $au2angs=0.529177;
#
#  sources of informations
     $geomfile ="$direct{JDIR}/geom";
     $geoconvfl= "$direct{work}/geoconvfl";
     $energyfl= "$direct{list}/energy.all";
#
#  output file of informations
     $geomout = "$direct{molden}/molden.geom";
     $convout = "$direct{molden}/molden.conv";
     $allout  = "$direct{molden}/molden.all";
#
#  append the current geometry to the molden.geom file
#   NOTE: geometry is writte in Angstrom
#
       if ( -s $geomout)
        { open (ALL,">>$geomout");}
       else
        {
        open (ALL,">$geomout");
        printf ALL " [Molden Format]\n";
        printf ALL " [GEOMETRIES] XYZ\n";
        }
#
     open(SP,$geomfile);
     $imd=0;
     while(<SP>){ $imd++; };
     printf ALL "$imd /coord \n";
     printf ALL "$control{iter} /current iter \n";
     close(SP);
#
     open(SP,$geomfile);
       while(<SP>)
        {
         s/^ *//;
         ($type,$charge,$x,$y,$z,$mass)=split(/\s+/,$_,6);
         $x=$x*$au2angs;
         $y=$y*$au2angs;
         $z=$z*$au2angs;
         printf {ALL} ("%3s %10.6f %10.6f %10.6f\n",$type,$x,$y,$z);
        }
       close(SP);
       close(ALL);
#
#  write out the converegence informations to the file molden.conv
#
if ( $kwords{"gdiis"}|| $kwords{"rgf"})
 {
      $icount=0;
      open(SP,$geoconvfl) or die "Failed to open file:$geoconvfl\n";
      while(<SP>)
      {
       s/^ *//; chomp;
       ($field1,$field2,$dum)=split(/\s+/,$_,3);
      if ( $field1 eq 'iter') {$icount++;}
      if ( $field1 eq 'eci=') {$energy[$icount]=$field2;}
      if ( $field1 eq 'emc=') { $energy[$icount]=$field2;}
      if ( $field1 eq 'max-step=') { $mx_step[$icount]=$field2;}
      if ( $field1 eq 'rms-step=') { $rms_step[$icount]=$field2;}
      if ( $field1 eq 'max-force=') { $mx_force[$icount]=$field2;}
      if ( $field1 eq 'rms-force=') { $rms_force[$icount]=$field2;}
      }
      close(SP);
#     $icount=$icount-1;
 }
#
if ( $kwords{"polyhes"})
 {
  $file="$direct{geom}/displace.all";
  $/="\n";
  open (FILE,"$file") or die "Failed to open file:$file\n";
  $i=0;
  while(<FILE>)
   { $i++,chomp; s/^ *//;$intdisp[$i]=$_;}
  close FILE;
#
  $npass=$i/($nintcoor+3);
  ($nintcoor,$intcoor)=&intc(); #  determine the number of internal coordinates
    for ($j=1;$j<=$npass;$j++)
     {
      $disp_mean=0;$mx_step[$j]=0;
       for ($i=1;$i<=$nintcoor;$i++)
        {
         $ind=($j-1)*($nintcoor+3)+3+$i;
         $disp_mean=$disp_mean+$intdisp[$ind];
         if ($mx_step[$j]< abs($intdisp[$ind])){$mx_step[$j]=abs($intdisp[$ind])}
        }
       $disp_mean=$disp_mean/$nintcoor;
       $rms_step[$j]=0;
       for ($i=1;$i<=$nintcoor;$i++)
        {
         $ind=($j-1)*$nintcoor+3+$i;
         $rms_step[$j]=$rms_step[$j]+($intdisp[$ind]-$disp_mean)**2;
        }
#
       $rms_step[$j]= sqrt( 1/($nintcoor-1)*$rms_step[$j]);
      }
 }
#
if ( $kwords{"nadcoupl"})
 {
  open (ALL,"$energyfl") or die "Failed to open file: $energyfl\n";
  $i=1;
   while (<ALL>)
    {
     $line=<ALL>;$line=<ALL>;
     $_=<ALL>;chomp; s/^ *//; ($e1[$i],$e2[$i],$de[$i])=split(/\s+/,$_,3);
     $energy[$i]=abs($de[$i]);
     $mx_force[$i]=$e1[$i]; $rms_force[$i]=$e2[$i];
     while (<ALL>) {if (/[^A-Za-z]/) {last;}}
     $i++;
    }
   close ALL;
   $icount =$i-1;
#
 }
#
      open (ALL,">$convout");
#
## write out the energy
      printf ALL " [GEOCONV]\n";
      printf ALL " energy\n";
      for ($i=1; $i<=$icount; $i++){printf ALL"$energy[$i]\n";}
#
## write out max-step=
      printf ALL " max-step\n";
      for ($i=1; $i<=$icount; $i++){printf ALL"$mx_step[$i]\n"; }
#
## write out rms-step=
      printf ALL " rms-step\n";
      for ($i=1; $i<=$icount; $i++){printf ALL"$rms_step[$i]\n";}
#
## write out max-force=
      printf ALL " max-force\n";
      for ($i=1; $i<=$icount; $i++){printf ALL"$mx_force[$i]\n";}
#
## write out rms-force
      printf ALL " rms-force\n";
      for ($i=1; $i<=$icount; $i++){printf ALL"$rms_force[$i]\n"; }
#
      close(ALL);
#
#  merge the files: molden.geom and  molden.conv to: molden.all
    unlink $allout;
    &append($allout,$geomout);
    &append($allout,$convout);
#
    }

####################################################################################################################

#
#-----------------------------------------------------------
#
   sub info4molden2
{
   $/="\n";
#  create frequency file for molden to visualize motion along g and h vectors
   open(OUT,">$direct{JDIR}/MOLDEN/molden.freq") or die "cannot open MOLDEN/molden.freq !!!\n";
    print {OUT} " \[Molden Format\]\n";
    print {OUT} " [FREQ]\n"; print {OUT} "  1000.0\n";print {OUT} "  1000.0\n";
    print {OUT} " [FR-COORD]\n";
     open(GEOM, "$direct{JDIR}/geom") or die "\n\n  File $direct{JDIR}/geom not found!!!\n\n";
       while (<GEOM>) {
       s/^ *//;chomp;
       ($label,$charge,$x,$y,$z,$mass)=split(/\s+/,$_,6);
       printf{OUT}("%3s%10.5f%10.5f%10.5f%5.2f\n",$label,$x,$y,$z,$charge);
       }
     close(GEOM);
   print {OUT} " [FR-NORM-COORD]\n";
#
#  append g vector direction
   print {OUT} " vibration   1\n";
   $file="$direct{work}/cartgrd.drt$naddrt1.state$nadstate1";
   open (VEC,"$file") or die "Failed to open: $file\n";
   $i=1;
   while (<VEC>){s/D/E/g;chomp;s/^ *//;($grad1x[$i],$grad1y[$i],$grad1z[$i])=split(/\s+/,$_,3);$i++;}
   close VEC;
   $file="$direct{work}/cartgrd.drt$naddrt2.state$nadstate2";
   open (VEC,"$file") or die "Failed to open: $file\n";
   $i=1;
   while (<VEC>){s/D/E/g;chomp;s/^ *//;($grad2x[$i],$grad2y[$i],$grad2z[$i])=split(/\s+/,$_,3);$i++;}
   close VEC;
#
   $natom=$i-1;
   for ($i=1;$i<=$natom;$i++)
    {
     $gradx=$grad2x[$i]-$grad1x[$i];$grady=$grad2y[$i]-$grad1y[$i];$gradz=$grad2z[$i]-$grad1z[$i];
     printf {OUT}("%10.5f%10.5f%10.5f\n",$gradx,$grady,$gradz);
    }
#
#  append h vector direction
   print {OUT} " vibration   2\n";
   $file="cartgrd.nad.drt$naddrt1.state$nadstate1.drt$naddrt2.state$nadstate2";
   open (VEC,"$file") or die "Failed to open: $file\n";
   while (<VEC>)
   {
    s/D/E/g;chomp;s/^ *//;
    ($gradx,$grady,$gradz)=split(/\s+/,$_,3);
    printf {OUT}("%10.5f%10.5f%10.5f\n",$gradx,$grady,$gradz);
   }
   close VEC;
#
   close OUT;
}
####################################################################################################################

 sub mcscfgrad {
# gradient for MCSCF and SA-MCSCF
   chdir $direct{work};
   cp("$direct{modir}/mocoef_mc.sp","mocoef");
#
#fp
   if ($kwords{"samcgrad"}) {
      local ($i);
      open (INPFL,"$direct{JDIR}/transmomin") or die "Could not open file: $direct{JDIR}/transmomin!\n";
      $/="\n";
      <INPFL>;
      $i=0;
      while (<INPFL>)
      {
        chomp; s/^ *//;$i++;
        ($grd_drt1,$grd_state1,$grd_drt2,$grd_state2) = split(/\s+/,$_,4);
      }
      if($i>1) {die "please specify only 1 gradient in transmomin";}
      if($grd_drt1!=$grd_drt2||$grd_state1!=$grd_state2) {die "a gradient has to be entered with the same bra and ket in transmomin!";}
      print "Computing SA-MCSCF gradient for DRT #$grd_drt1, state #$grd_state1\n";
      $fstate=sprintf("%02d",$grd_state1);
      printf " using the MCSCF MOs ... \n" ;
      &changekeyword("$direct{JDIR}/tranin","tranin"," LUMORB",0);


# solve the coupled perturbed MCSCF equations
    $indens1="mcsd1fl.drt$grd_drt1.st$fstate";
    $indens2="mcsd2fl.drt$grd_drt1.st$fstate";
    symlink ("$indens1","cid1fl");
    symlink ("$indens2","cid2fl");
#   cp("$indens1","cid1fl");
#   cp("$indens2","cid2fl");
    cp("$direct{JDIR}/cigrdin","cigrdin");
    &changekeyword("cigrdin","cigrdin","samcflag",1);

    call_cigrd("",$kwords{pcigrd},$control{iter},"");

   } # samcgrad

   else { # regular mcscf gradient
    $grd_drt1 = 1;
    $grd_state1 = 1;

    unless (rename("mcd1fl","modens")) {die("rename $!\n"); }
    unless (rename("mcd2fl","modens2")) {die("rename $!\n"); }
   }

   open(MCSCFSM,"mcscfsm");
       $/="\n";
       $energy1=0;
       while (<MCSCFSM>)
       {chop;
        if (/DRT \#$grd_drt1 state \# $grd_state1/) { s/^.*energy=//;
                            s/, rel.*//;
                            $energy1=$_; }
       }

       if (! ($energy1) ) {$!=101;die "mcscf calculation did not converge. Exiting ...\n";}
       close(MCSCFSM);
       open(FILE,">$direct{work}/energy.gdiis");
       print FILE "  $grd_state1 $energy1\n";
       close FILE ;
       cp ("$direct{work}/energy.gdiis","$direct{list}/energy");
#
#
   if ( $kwords{"alaska"} ) {
       &changekeyword("$direct{JDIR}/tranmcdenin","tranin","seward",1); }
     else { cp("$direct{JDIR}/tranmcdenin","tranin");}
# no longer necessary bugfix abacol
#          &changekeyword("$direct{JDIR}/tranmcdenin","tmp","lrc1mx",2048);
#          &changekeyword("tmp","tranin","lrc2mx",2048);
#          unlink("tmp"); }
#  cp("$direct{modir}/mocoef_mc.sp","mocoef");
    callprog("tran.x -m $control{coremem}");
    unlink <fort*>;
#fp_tmp    unlink ("tranin","modens","modens2");
    &appendtofile("tranls","$direct{list}/tranls.den.all",$control{iter});
     if ( $kwords{"alaska"} ) {
         $alaskainput=extractmolcas("$direct{JDIR}/molcas.input","ALASKA");
         $alaskainput=~s/:/\n/g;
         open FOUT,">molcas.input.alaska";
         print FOUT $alaskainput;
         close FOUT;
         system("cp mocoef_mc.lumorb INPORB");
         #system("molcas  molcas.input.alaska > molcas.output.alaska 2>&1 ") == 0 || die("call to alaska failed\n");
         #die ("before alaska");
         callprog("molcas molcas.input.alaska > molcas.output.alaska ");
         print "cartesian gradient by alaska (MOLCAS)\n";
         cp("molcas.output.alaska","$direct{list}/alaskals.sp");
         #system("cat cartgrd");
                              }
     else { cp("$direct{JDIR}/abacusin","daltcomm");
            callprog("dalton.x -m $control{coremem} > abacusls");
            unlink <fort*>;
            unlink ("aoints*","moints");
            print "cartesian gradient by abacus (DALTON1.x)\n";
            #system("cat cartgrd");
            &appendtofile("abacusls","$direct{list}/abacusls.all",$control{iter}); }


#
# matruc 21dec07 adding PCM gradient
    if (defined $kwords{"potmat"}){
      printf "++++++++++++ adding potential matrix to cartgrad +++++++++++++++\n";
      &changekeyword("$direct{JDIR}/elpotin","elpotin","property","\'gradient\'");
      callprog("potential.x -m $control{coremem}");
      &appendtofile("elpotls","$direct{list}/elpotls.grad",$control{iter});
      &appendtofile("cartgrd.pointcharges","$direct{grad}/pcgrd.all",$control{iter});
      printf ">>>>>>> Added charge contributions to atomic gradients <<<<<<<<\n";
    }
   unlink <fort*>;
   &appendtofile("abacusls","$direct{list}/abacusls.all",$control{iter});
   &appendtofile("cartgrd","$direct{grad}/cartgrd.all",$control{iter});

   &transfgrad("","grd",0);
#
  }

########################################################################################################################################

  sub cigrad {
   local ($froot,$control{iter},@x,$pciden,$pcigrd,$pabacus);

if (! $control{chains})
   {chdir "$direct{work}";
   cp("$direct{JDIR}/cidenin","cidenin");
   $froot=keyinfile("ciudgin","froot");
   if ( $froot ) { my ($newfroot,$energy);
                   ($newfroot,$energy) = &frootci("ciudgls");
                   print "info: followed root at position $newfroot in ci vector file.\n";
                   &changekeyword("$direct{JDIR}/cidenin","cidenin","lroot",$froot);
                   $lroot=$froot;
                   $control{cienergy_last}=$energy;

                   }
   else {
#fp: the state of the CI gradient is specified in the following order of priority:
#  transmomin - cidenin - nroot - 1
#  this is a little bit confusing. but it is the only way to be compatible with the gradients in nadcoupl input
#      and with lroot in Columbus 5.9
      if (-e "$direct{JDIR}/transmomin")
      {
        local ($i);
        open (INPFL,"$direct{JDIR}/transmomin") or die "Could not open file: $direct{JDIR}/transmomin!\n";
        $/="\n";
        <INPFL>;
        $i=0;
        while (<INPFL>)
        {
            chomp; s/^ *//;$i++;
            ($grd_drt1,$grd_state1,$grd_drt2,$grd_state2) = split(/\s+/,$_,4);
        }
        if($i>1) {die "please specify only 1 gradient in transmomin";}
        if($grd_drt1!=$grd_drt2||$grd_state1!=$grd_state2) {die "a gradient has to be entered with the same bra and ket in transmomin!";}
        print "Gradient specification taken from transmomin\n";
        $lroot = $grd_state1;
        $ldrt  = $grd_drt1;
      }
      print "Computing CI gradient for state #$lroot\n";
      for ($i=1; $i<$efile{nroots}; $i++)
        { my (@tmp,$tmp1);
          @tmp=split(/:/,${$efile{roots}}[$i]); 
          $tmp1=join(':',@tmp[0,1]);
          if ( $tmp1 eq "$ldrt:$lroot") { $control{cienergy_last}=$tmp[2]; $grad[drt1]=$ldrt; $grad[state1]=$lroot; $grad[energy]=$tmp[2]; last;}
        } 
    }
#modified

    if ( $pciden) {
#                   print "skipping pciden.x call ! "
                   if (! $control{USENEWCIDENSITY} ) {  pcallprog ( "pciden.x -m $control{pcoremem} ");
                  &appendtofile("cidenls","$direct{list}/cidenls.cigrd.all",$control{iter});}
                   else {   # there should be only a single set of files cid1fl* cid2fl*
                          print "skipping pciden call \n";
                         while (<cid*fl.*>) { my $n=$_; s/fl\..*$/fl/; system("ln -s $n $_");}}}
             else {
                   if ( ! $control{USENEWCIDENSITY} ) { callprog("ciden.x -m $control{coremem}");
                                               &appendtofile("cidenls","$direct{list}/cidenls.cigrd.all",$control{iter});}
                                else {print "skipping ciden.x call ! \n";
#                                      while (<cid*fl.*>) { my $n=$_; s/fl\..*$/fl/; system("ln -s $n $_");}}
#fp: use the gradient specified by "lroot" in cidenin
                                       while (<cid*fl.*state$lroot>) { my $n=$_;
                                       s/fl\..*$/fl/;
                                       print "linking $n to $_\n";
                                       system("ln -s $n $_");}}
                                     }
    unlink <flrd*>;
    unlink <flwd*>;
    unlink <fil*>;
    unlink ("flacpfd","ciflind");

    if ($control{chainp}) {die("chainp acive - exiting after pciden... \n");}
} #chains
 else {
   chdir($direct{work});
   while (<cid[12]fl.*>) { my $n=$_; s/fl\..*$/fl/; system("ln -s $n $_");}
    open(CIUDGLS,"ciudgls");
    @x=();
    while (<CIUDGLS>)
    {if (/total energy/) { s/[a-z=\(\)]//g;s/---//g; @x=split(/\s+/,$_);
                            printf "x: @x\n"; }}
    if (! $#x) {$!=101;die "ci calculation did not converge. Exiting ...\n";}
    close(CIUDGLS);
    $control{cienergy_last}=$x[3];
       }


   if ($control{USENEWCIGRD}) {

#------------------------------- CHANGE FROZEN CORE ---------------------------
#  find out whether there are frozen core orbitals
#  if there are negative entries in the map vectors we have frozen
#  core orbitals

    open CIDRTFL,"<cidrtfl";
    while (<CIDRTFL>) { if (/  map/){last;}}
    @tmp=();
    while (<CIDRTFL>) { if (/  mu/) {last; }chop ; push @tmp,$_;}
    close CIDRTFL;
    $tmp = join(':',@tmp);
    $tmp = ':' . $tmp . ':';
    $tmp =~s/  */ /g;
    $tmp =~s/ /:/g;
    $tmp =~s/::/:/g;
    $isfrozencore=grep(/:-1:/,$tmp);
#   $isfrozencore=0;

    if ($isfrozencore) { print "INFO: this is a frozen core gradient calculation\n";
                         system("cp $direct{JDIR}/cidrtin.cigrd .");  # cidrtin.grad is the step vector form
                         system("mv cidrtfl cidrtfl.ci");    # cidrtfl.ci is generated with fc orbitals
                         callprog("cidrt.x -m $control{coremem} < cidrtin.cigrd > cidrtls.cigrd"); # now cidrtfl is in step vector from
                         unlink "moints";
                         if ($kwords{seward}) { changekeyword("$direct{JDIR}/tranin","tranin","SEWARD",1);}
                         callprog("tran.x -m $control{coremem} "); # redo full AO-MO transformation
                         changekeyword("$direct{JDIR}/cigrdin","cigrdin","assume_fc",1);
                       }
    else  {changekeyword("$direct{JDIR}/cigrdin","cigrdin","assume_fc",0); }
#   system("cp $direct{JDIR}/cigrdin cigrdin");

    }

   else { changekeyword("$direct{JDIR}/cigrdin","cigrdin","assume_fc",0); }

#
#   now cigrd.x understands the keyword assume_fc =0,1
#   if assume_fc=0  it runs with the old modus including step vector based frozen core
#   if assume_fc=1  it extracts the proper mapping vector and fc info from cidrtfl.ci
#                   and constructs the 2e density from those of cid2fl,cid1fl supplemented
#                   by the frozen core orbital contributions
#                   consistent with the ordering in cidrtfl
#
    call_cigrd("",$kwords{pcigrd},$control{iter},"");

# test now moved into cigrd (due to total energy in header of density file for nadcalc=0)
# this avoids complications in case of output format changes
     if ( $kwords{"alaska"} ) {
           &changekeyword("$direct{JDIR}/trancidenin","tranin","seward",1);
                              }
     else {cp("$direct{JDIR}/trancidenin","tranin");}
#no longer necessary bugfix abacol
#          &changekeyword("$direct{JDIR}/trancidenin","tmp","lrc1mx",2048);
#          &changekeyword("tmp","tranin","lrc2mx",2048);
#          unlink("tmp");
#                             }
    my $lcoretran=$control{coremem};
    if ($kwords{"alaska"} ) { $lcoretran=$lcoretran*2;
                              print "raising corememory for tran.x (density) to $lcoretran \n"; }
    callprog("tran.x -m $lcoretran");
    unlink <fort*>;
    unlink ("tranin","modens","modens2");
    &appendtofile("tranls","$direct{list}/tranls.den.all",$control{iter});
     if ( $kwords{"alaska"} ) {
         $alaskainput=extractmolcas("$direct{JDIR}/molcas.input","ALASKA");
         $alaskainput=~s/:/\n/g;
         open FOUT,">molcas.input.alaska";
         print FOUT $alaskainput;
         close FOUT;
         system("cp mocoef_mc.lumorb INPORB");
         #system("molcas  molcas.input.alaska > molcas.output.alaska 2>&1 ") == 0 || die("call to alaska failed\n");
         #die ("before alaska");
         callprog("molcas molcas.input.alaska > molcas.output.alaska ");
         cp("molcas.output.alaska","$direct{list}/alaskals.sp");
         if ($debug{debug_deriv})
         {
           print "cartesian gradient by alaska (MOLCAS)\n";
           system("cat cartgrd");
         }
                              }
     else { cp("$direct{JDIR}/abacusin","daltcomm");
            if ($kwords{pabacus})
             { pcallprog("pdalton.x -m $control{pcoremem}  > abacusls");}
            else
             {callprog("dalton.x -m $control{coremem}   > abacusls"); }
            unlink <fort*>;
            unlink ("aoints*","moints");
            if ($debug{debug_deriv})
            {
              print "cartesian gradient by abacus (DALTON1.x)\n";
              system("cat cartgrd");
            }
            &appendtofile("abacusls","$direct{list}/abacusls.all",$control{iter}); }


# matruc 21dec07 adding PCM gradient
    if (defined $kwords{"potmat"}) {
      printf "++++++++++++ adding potential matrix to cartgrad +++++++++++++++\n";
      &changekeyword("$direct{JDIR}/elpotin","elpotin","property","\'gradient\'");
      callprog("potential.x -m $control{coremem}");
      &appendtofile("elpotls","$direct{list}/elpotls.grad",$control{iter});
      &appendtofile("cartgrd.pointcharges","$direct{grad}/pcgrd.all",$control{iter});
      printf ">>>>>>> Added charge contributions to atomic gradients <<<<<<<<\n";
    }
    unlink("aodens*");

    &appendtofile("cartgrd","$direct{grad}/cartgrd.all",$control{iter});
    &file_output("Final cartesian gradient","cartgrd");
#
  &transfgrad("","grd",0)
    }

######################################################################################################################################
  sub displacement{
  my ($coor,$displ,$dum);
  ($geomfile)=@_;
  $/="\n";
#
   print "  Reference geometry take from file:\n";
   print "  $direct{geom}file\n";
#
#  read the internal coordinate definition
#
   ($dum,$intcoor)=&intc();
#
   if (! -s "$direct{JDIR}/DISPLACEMENT/displfl")
    { $!=10;  die " \n\n $direct{JDIR}/DISPLACEMENT/displfl file not found, make input first!\n\n";}
   open(DISPLFL,"$direct{JDIR}/DISPLACEMENT/displfl");
#
#  skip the first 3 lines
   $line=<DISPLFL>; $line=<DISPLFL>;
   $refcalc=<DISPLFL>;
   chomp ($refcalc);
   $refcalc=~s/\W.*//;
#
#   copy refenrence point geometry
#
     if ($refcalc eq "yes")
      {
       $dir="$direct{JDIR}/DISPLACEMENT/REFPOINT";
       if ( -e "$dir/GRADIENTS"){system("rm -rf $dir/GRADIENTS")}
       chdir $dir or die "directory $dir do not exists";
        cp ("$direct{geom}file","geom");
      }
#
#  generate displacement geometries
#
     while(<DISPLFL>){
     chomp;s/^ *//;
     ($coor,$displ,$flag)=split(/\s+/, $_,3);
#    @field = split(" ",$_);
       if (!$displ==0 && $flag ne "fixc"){
        $dir="$direct{JDIR}/DISPLACEMENT/CALC.c$coor.d$displ";
        if (! -e "$dir"){mkdir("$dir",0755);}
        if ( -e "$dir/GRADIENTS"){system("rm -rf $dir/GRADIENTS")}
        chdir "$dir" or die "directory $dir do not exists";
        print "Preparing geometry in $dir\n";
        system ("rm -rf LISTINGS RESTART MOCOEFS GEOMS  ");
        print "    $_\n";
         cp ("$direct{geom}file","geom");
          open(BMATIN,">bmatin");
          print{BMATIN} "card              1.\n";
          print{BMATIN} "disp              1.\n";
          printf{BMATIN} ("%10.6f%10.6f\n",$coor,$displ);
          print{BMATIN} $intcoor;
          print{BMATIN} "stop\n";
          close(BMATIN);
          print "echo bmatin\n";
          system("cat bmatin");
          callprog("bmat.x < bmatin > bmatls");
          cp ("geom","geom.start");
       } # end of: if (!$displ==0)
     } # end of: while(<DISPLFL>)
 close DISPLFL;
 chdir $direct{JDIR};
#
  return;
  }
#####################################################################################################################################

#--------------------------------------------------------------------
  sub intcx {
      my (@coortype,$i,@field,$string);
      @coortype=qw(stre bend out tors lin1 lin2 tor2);
      @hessdiag=();
      $/="\n";
      $intcoor="";
      open(INTC,"intcfl") or die(" File: intcfl missing; performe gradient input first!");
      $title=<INTC>;
      $i=1;
      $nintcoor=0;
      $icount=0;
      while (<INTC>) {
      $line=$_;
      $line=~tr/A-Z/a-z/;
      @field=split('');
      $string=join'',@field[20..23];
      $kstring=$field[0];
      $string=~tr/A-Z/a-z/;
      $string=~s/\W.*//;
      $kstring=~s/\W.*//;
      $string=~tr/A-Z/a-z/;
      $kstring=~tr/A-Z/a-z/;
        for ($i=0; $i<=7;$i++)
         {
          if ($i==7)
           {
# read the force constat diagonals
            $line=~s/^ *//;
            (@tmp)=split(/\s+/,$line,8);
            @hessdiag= (@hessdiag,@tmp);
           }
          if ($string eq $coortype[$i] && $kstring eq "k"){$intcoor=$intcoor.$line;$nintcoor++;last}
          if ($string eq $coortype[$i]){$intcoor=$intcoor.$line;last}
         } # end of for ($i=0; $i<=6;$i++)
      } # end of: while (<INTC>)
      close(INTC);
     return $nintcoor,$intcoor,\@hessdiag ;
  } # end of: sub intc

##########################################################################################################################################

  sub rgf {
   local ($flag,$control{iter},$cbus,$text,$text2,$value,$value,$hessian,$istat);
#  global couter_rgf in neccessary for calc. mode 2
#
#  RGF calculation  modes (based on hessian matrix values):
# 1.'exact'
#     Hessian matrix will be calculated every iteration exactly. Input
#     files at the directory DISPLACEMENT have to be present.
#
# 2.'exact corrector_u'
#     Hessian matrix will be calculated after every corrector step, on
#     predictor step update will be used.
#
# 3.'exact corrector_c'
#     Hessian matrix will be calculated after every corrector step, otherwise
#     it will be hold constant
#
# 4.'exact predictor_u'
#     Hessian matrix will be calculated after every predictor step, on
#     corrector step update will be used.
#
# 5.'exact predictor_c'
#     Hessian matrix will be calculated after every predictor step, otherwise
#     it will be hold constant
#
# 6.'update'
#     Hessian matrix will be updated by the rgf.value.
#
# 7.'exact_n_u N'
#     Hessian matrix will be calculated every N-th step exactly otherwise
#     updated by the rgf.value.
#
# 8.'exact_n_c N'
#     Hessian matrix will be calculated every N-th step exactly otherwise
#     hold constant.
#
#  NOTE:
#   * In case of 'update' at the beginning of the calculation the first
#      hessian matrix has to be present aj the $direct{JDIR} directory.
#   * In other cases (see above) the input files for the force constant
#      calculation has to be present in the directory 'DISPLACEMENT'.
#
#  NOTE for Hessian matrix update:
#   If the flag Const_Sign_Update is active in the rgf.x program,
#   after the sign of the hessian matrix changes (rgf.x istat value=5/7,
#   read in from the file rgfendmark) the hessian matrix is
#   recalculated exactly and the calculation continues with the
#   hessian matrix update. At the beginning the first hessian
#   matrix has to be present aj the $direct{JDIR} directory. Input files at
#   the directory DISPLACEMENT have to be present.
#   In case flag Const_Sign_Update is not active, only the update
#   of the hessian matrix by the rgf.x program is preformed.
#   Directory DISPLACEMENT is not neccessery in this case.
#   Input files at the directory DISPLACEMENT have to be present.

    $cbus = $ENV{"COLUMBUS"};
    $/="\n";
#
#  transform  -> cartforce and cartgeom
#
   cp("$direct{JDIR}/cart2intin.1","$direct{work}/cart2intin");
   cp("$direct{JDIR}/intcfl","intcfl")or die"Failed to copy file:$direct{JDIR}/intcfl\n";
   chdir $direct{work};
   callprog("$control{CART2INT}");
   &appendtofile("$direct{work}/cart2intls","$direct{list}/cart2int1ls.all",$control{iter});
   &appendtofile("$direct{work}/intgeom","$direct{list}/intgeom.all",$control{iter});
   &appendtofile("$direct{work}/intgrad","$direct{list}/intgrad.all",$control{iter});

   $file="$direct{work}/energy";
   open (FILE,"$file") or die" Failed to open file:$file\n";
   $energy=<FILE>;close FILE;
   cp ("$direct{work}/energy","$direct{list}/energy");
    open(GCONVFL ,">>geoconvfl");
    print GCONVFL "iter $control{iter} ====\n";
    print GCONVFL "eci= $energy";
    close GCONVFL ;
#
#  check for the istat value of rgf.x from the last iteration
#
#  rgf_istat= 1  error in rgf.x
#           = 4  performed predictor step;   hessian will be updated
#           = 5  performed predictor step; new hessian required
#           = 6  performed corrector step;   hessian will be updated
#           = 7  performed corrector step; new hessian required
#           =10  saddle point found; stop the calculation
#
#   check how to get new Hessian type (for explanation see above)
#
   $predictor=0;
   $corrector=0;
   $ncount=0;
   $istat=0;
   &appendtofile("$direct{work}/rgfendmark","$direct{list}/rgfendmark.all",$control{iter});
#
#------------------------------------------------------------------------------
#  read in the edn mark of the RGF calculation performed in the last iteration
#
   if (-s "$direct{work}/rgfendmark")
     {
      open(RGF,"$direct{work}/rgfendmark") or
       die "could not open file: $direct{work}/rgfendmark\n\n" ;
      while (<RGF>)
       {
        chop;  s/ *$//g;
        ($text,$text2,$istat)=split(/\s+/, $_,3);
        if ($istat==4 || $istat==5)
         {
          $predictor=1;
          print "\n  RGF performed a prediction step in the previous iteration\n";
         } # performed predictor step
        if ($istat==6 || $istat==7)
         {
          $corrector=1;
          print "\n  RGF performed a corretior step in the previous iteration\n";
         } # preformed corrector step
       }
      close RGF ;
     }
    else
     {
      if (! $control{iter} == 1) {die "no $direct{work}/rgfendmark file found !!!\n";}
     }
#
#--------------------------------------------------------------------------------
#  read in the RGF calculation mode
#   (see comment at the begining of the subroutine)
#
   chdir $direct{JDIR};
   if (!-s "rgfin") {$!=10;die "rgfin file is missing !!!\n";}
   open(RGFIN,"rgfin") or die "could not open file: rgfin !!!\n";
     while (<RGFIN>)
      {
      chop($_); s/ *$//g;
      ($flag,$value1,$value2)=split(/\s+/, $_,3);
      } # end of: while (<RGFIN>)
   close RGFIN ;
#
#------------------------------------------------------------------------------
#
#-- read out the force constant diagonal values from the intcfl file
     if ($value1 eq 'update_1')
      {
       ($nintcoor,$intcoor,$hessdiag)=&intc;
       print "$nintcoor,$intcoor\n";
       for ($i=0;$i<$nintcoor;$i++)
        {
         print "$i:$$hessdiag[$i]\n";
        }
       open(HESS,">$direct{work}/hessian");
       local $ncol=8; # defined the number of columns for the force constat matrix
       local $zero=0.0;
       for ( $i=1; $i<= $nintcoor; $i++){
          $icount =0;
          for ( $j=1; $j<= $nintcoor; $j=$j+$ncol)
           {
            $jstart = $j;
            $jstop = $j+$ncol-1;
            if ($jstop > $nintcoor) {$jstop = $nintcoor}
              for ($j1=$jstart; $j1<=$jstop; $j1++)
               {
                if ($i == $j1)
                {printf{HESS}( "%13.6f", $$hessdiag[$i-1]); }
                else {printf{HESS}( "%13.6f", $zero);}
               } # end of: for ($j1=$j; $j1<=$j+$nact-1; $j1++)
           $icount++;
           print{HESS} "\n";
           } # end of: for ( $j=1; $j<= $nintcoor; $j=$j+$ncol)
       } # end of: for ( $i=1; $i<= $nintcoor; $i++)
#
   close HESS;
      }
#-- always update the hessian matrix
     if ($value1 eq 'update' || $value1 eq 'update_1') { $hessian='update'; }
#
#-- always calculate the hessian matrix
     if ($value1 eq 'exact'){$hessian='exact';}
#
#     Hessian matrix will be calculated after every corrector step, on
#     predictor step update will be used.
     if ($value1 eq 'corrector_u')
       {
         if ($corrector==1)
           { $hessian='exact'}
         else
           { $hessian='update'}
       }
#
#     Hessian matrix will be calculated after every corrector step,
#     otherwise it will be hold constant
     if ($value1 eq 'corrector_c')
       {
         if ($corrector==1)
           { $hessian='exact';}
         else
           { $hessian='constant';}
       }
#
#     Hessian matrix will be calculated after every predictor step, on
#     corrector step update will be used.
     if ($value1 eq 'predictor_u')
       {
         if ($predictor==1)
           { $hessian='exact';}
         else
           { $hessian='update';}
       }
#
#     Hessian matrix will be calculated after every predictor step,
#     otherwise it will be hold constant
     if ($value1 eq 'predictor_c')
       {
         if ($predictor==1)
           { $hessian='exact';}
         else
           { $hessian='constant';}
       }
#
#     Hessian matrix will be calculated every N-th step exactly otherwise
#     updated by the rgf.value.
        if (grep(/exact_n_c/,$value1))
          {
           $control{counter_rgf}++;
           $ncount=$value2;
           if ($control{counter_rgf} == $ncount)
            {
             $control{counter_rgf}=0;
             $hessian='exact';
            }
           else
            {
             $hessian='constant';
            }
          } # if ($value1 eq 'exact_n_c')
#
#     Hessian matrix will be calculated every N-th step exactly otherwise
#     hold constant.
        if (grep(/exact_n_u/,$value1))
          {
           $control{counter_rgf}++;
           $ncount=$value2;
           if ($control{counter_rgf} == $ncount)
            {
             $control{counter_rgf}=0;
             $hessian='exact';
            }
           else
            {
             $hessian='update';
            }
          } # if ($value1 eq 'exact_n_u')
#
#  in case update only do not change nothing,
#  otherwise overwrite the hessian matrix calculation mode for the
#  first iteration
    if ( ( $value1 ne "update" && $value1 ne "update_1") && ($control{iter} == 1))
     {
      print "\n  First iteration => initial calc. of the Hessian matrix follows\n";
      $hessian='exact';
     } # end of: if ( ( $value1 ne "update") && ($control{iter} == 1))
#
#
#

  print "hessian= $hessian \n";
  if (-e "$direct{WORK}/store.rgf")
   {
    print "  setting: \'o_no_up=T\' in WORK/store.rgf\n";
  open (FILE,"$direct{WORK}/store.rgf");
   open (FILE_NEW,">$direct{WORK}/store.new");
     while(<FILE>)
      {
       if ( grep (/o_req_ex/,$_) || grep (/O_REQ_EX/,$_) )
        {
         if ($hessian eq 'exact')
          {
           print FILE_NEW "o_req_ex=T\n";
           print "\n   new HESSIAN matrix will be calculated: EXACTLY\n\n";
          }
         if ($hessian eq 'update')
          {
           print FILE_NEW "o_req_ex=F\n";
           print "\n   new HESSIAN matrix will be calculated by: UPDATE\n\n";
          }
         if ($hessian eq 'constant')
          {
           print FILE_NEW "o_req_ex=T\n";
           print "\n   new HESSIAN matrix will be kept: CONSTANT\n\n";
          }
        }
       else
        {
         print FILE_NEW ;
        }
       }# end of: while(<FILE>)
   close FILE_NEW;
   close FILE;
   cp ("$direct{WORK}/store.new","$direct{WORK}/store.rgf");
    } # end of: if (-e "$direct{WORK}/store.rgf")
#
# if (-s "$direct{WORK}/store.rgf") { system("cat $direct{WORK}/store.rgf")};
#
#--------------------------------------------------------------------------
#  if required, do the hessian matrix calculation
#
#
     if ($hessian eq 'exact')
      {
       print "\n\n  Starting EXACT HESSIAN matrix calculation\n\n";
       &displacement("$direct{JDIR}/geom");
       print "\n\n  Performing HESSIAN MATRIX calculation,
             see calcls for details\n\n";
#
       $ERR=system ("$cbus/calc.pl > calcls");
       $ERR=$ERR/256;
       if (!$ERR ==0 )
        {$!=$ERR;die" Error occured in calc.pl/runc.perl: $ERRCODE[$ERR]\n";}
#
       $ERR=system ("$cbus/forceconst.pl");
       $ERR=$ERR/256;
       if (!$ERR ==0 )
        {$!=$ERR;die" Error occured in forceconst.pl: $ERRCODE[$ERR]\n";}
       &appendtofile("$direct{list}/forceconstls","$direct{list}/forceconstls.all",$control{iter});
#
       print " Calculated Hessian matrix:\n";
       cp("$direct{JDIR}/hessian","$direct{work}/hessian");
       open(HESS,"$direct{JDIR}/hessian"); while(<HESS>) {print $_;}close HESS;
       print "\n";
      } # end of: if ($hessian eq 'exact')
#
#
#-------------------------------------------------------------------------
#  now do the RGF calculation
#
   chdir $direct{work};
   if ( ! -e "$direct{work}/hessian" ) {
     cp("$direct{JDIR}/hessian","$direct{work}/hessian")
        or die "\n hessian file missing\n";
     }
   &appendtofile("$direct{work}/hessian","$direct{list}/hessian.all",$control{iter});
#
   cp("$direct{JDIR}/param.rgf","$direct{work}/param.rgf");
   callprog("rgf.x ");
   cp("$direct{work}/rgfls","$direct{list}/rgfls");
   cp("$direct{work}/message.rgf","$direct{list}/message.rgf");
#
   if (-s "$direct{work}/rgfendmark")
     {
      open(RGF,"$direct{work}/rgfendmark") or
       die "could not open file: $direct{work}/rgfendmark\n\n" ;
      while (<RGF>)
       {
        chop; s/ *$//g;
        ($text,$text2,$istat)=split(/\s+/,$_,3);
        if ($istat==4 || $istat==5)
         {
          $predictor=1;
          print "\n RGF performed a prediction step\n\n";
         } # performed predictor step
        if ($istat==6 || $istat==7)
         {
          $corrector=1;
          print "\n RGF performed a corretior step\n\n";
         } # preformed corrector step
        if ($istat==10)
         {
          print "\n Stopping criterion satisfied in rgf.x, \
           saddle point found!\n\n";
          }
       }
      close RGF ;
     }
    else
     {
      if (! $control{iter} == 1) {die "no $direct{work}/rgfendmark file found !!!\n";}
     }
#
   cp("intgeom","intgeomch");
   cp("$direct{JDIR}/cart2intin.2","$direct{work}/cart2intin");
   callprog("$control{CART2INT}");
   &appendtofile("$direct{work}/cart2intls","$direct{list}/cart2int2ls.all",$control{iter});
#
#  write iout the information for MOLDEN
   print "\n generating MOLDEN informations\n";
   &info4molden($control{iter});
#
   cp("$direct{work}/geom.new","$direct{work}/geom");
   cp("$direct{work}/geom.new","$direct{JDIR}/geom");
#
#------------------------------------------------------------------
#  generate the rgfprotol file
#

   chdir $direct{JDIR};
   $ERR=system ("$cbus/rgfprotocol.pl >> $direct{list}/rgfprotocol");
   $ERR=$ERR/256;
      if (!$ERR ==0 )
       {$!=$ERR;die" Error occured in rgfprotocol.pl run by runc: $ERRCODE[$ERR]\n";}
#
#  exit for the end of the calculation
#
   if ($istat==10){
   unlink "$direct{work}/rgfendmark";
   exit 0;}
#

    }

############################################################################################################################################

################################################################################
  sub slapaf {

# NOTES:   * slapaf writes the new geometry to the RUNFILE , key 'GeoNew'
#          * in presence of 'GeoNew' seward takes the geometry from there
#          * only, concerning property calculation, a geom file must be
#            created - or better - the RUNFILE is search for 'GeoNew'
#            and only if failing geom is read
#          * there is no need to call intc.x or makintc.x in colinp
#            (failures are not checked for!)
#            intc.x supports currently only until element 18
#
#     USE DEFAULT
#      &SLAPAF &END
#      ITERATIONS
#       1
#      End of Input
#


     chdir $direct{work};
     $slapafinput=extractmolcas("$direct{JDIR}/molcas.input","SLAPAF");
     if ( $slapafinput == -1 ) {die ("could not find slapaf module ... terminating\n");}
#    print '#####\n';
#    print "SLAPAFINPUT=",$slapafinput;
#    print '#####\n';
     open FOUT,">molcas.input.slapaf";
     $slapafinput=~s/:/\n/g;
     print FOUT $slapafinput;
     close FOUT;
     print "molcas.input.slapaf:\n";
     system("cat molcas.input.slapaf");
     callprog("molcas molcas.input.slapaf > molcas.output.slapaf ");
     system ("cat molcas.structure*");
     cp("molcas.output.slapaf","$direct{list}/slapafls.sp");
     open SLAPAF,"<molcas.output.slapaf" || die ("could not open molcas.output.slapaf");
     $reached=0;$notreached=0;
     while (<SLAPAF>) {if (/Convergence not reached yet|No convergence after max iterations/i) {$notreached=1;}
                       if (/Geometry is converged in /i) {$reached=1;}}
     close SLAPAF;

#
#   update geom
#
     { my ($natoms,$scr,@label,@x,@y,@z,@cz,@cy,@cx,@mass,@charge,@clabel);
     open OPT,"<molcas.Opt.xyz" || die ("could not open molcas.Opt.xyz\n");
     $natoms=<OPT>; chop $natoms;
     $scr=<OPT>;
     for ($i=1; $i<=$natoms; $i++)
       { $_=<OPT>;  s/^ *//;  chop; ($label[$i], $x[$i], $y[$i],$z[$i])=split(/\s+/,$_); }
     close OPT;
     print "found $natoms atoms on molcas.opt.xyz\n";

     open GEOM,"<geom" || die ("could not open geom\n");
     for ($i=1; $i<=$natoms; $i++)
       { $_=<GEOM>; s/^ *//;  chop; ($clabel[$i], $charge[$i],$cx[$i], $cy[$i],$cz[$i],$mass[$i])=split(/\s+/,$_); }
     close GEOM;
     open GEOM,">geom";
     for ($i=1; $i<=$natoms; $i++)
      {printf GEOM "%-3s%7.1f%14.8f%14.8f%14.8f%14.8f\n", $clabel[$i],$charge[$i],$x[$i]/0.529177,$y[$i]/0.529177,$z[$i]/0.529177,$mass[$i];}
     close GEOM;
    }

     if ($notreached && $reached) {die("slapaf: internal runc error reached && notreached...\n");}
     if (! ($reached || $notreached)) {die("slapaf: internal runc error neither found ...\n");}
     if ($reached)
       {cp("molcas.structure","$direct{list}/molcas.structure");
        cp("molcas.Opt.xyz","$direct{list}/molcas.Opt.xyz");
     print " -------- Final Molcas Structure -------------\n ";
     if ( ! -f "molcas.Opt.xyz" ) { die("molcas.Opt.xyz file missing\n");}
     system("cat molcas.Opt.xyz");
     exit(0);}
   return;
   }


################################################################################################################################

  sub gdiisstuff {
   my ($energy1,$energy2,$energy,$deltae,$order,$phase);
    $/="\n";

   chdir "$direct{work}";
#
    if (!$kwords{"nadcoupl"})
     {
      $file="$direct{work}/energy.gdiis";
      open (FILE,"$file") or die" Failed to open file:$file\n";
      $energy=<FILE>;close FILE;
      $energy=~s/^ *//;$energy=~s/^\d *//;
      cp ("$direct{work}/energy","$direct{list}/energy");
     }
    else
     {$energy="0\n";}
    open(GCONVFL ,">>geoconvfl");
    print GCONVFL "iter $control{iter} ====\n";
    print GCONVFL "eci= $energy";
    close GCONVFL ;
#
   cp("$direct{JDIR}/gdiisin","gdiisin");
   cp("$direct{JDIR}/intcfl","intcfl");
#
#  copy if exists
   if (-s "$direct{JDIR}/hessian") {cp("$direct{JDIR}/hessian","hessian");}
   if (-s "$direct{JDIR}/hessian_st1") {cp("$direct{JDIR}/hessian_st1","hessian_st1");}
   if (-s "$direct{JDIR}/hessian_st2") {cp("$direct{JDIR}/hessian_st2","hessian_st2");}
   if (-s "$direct{JDIR}/hessian_nad") {cp("$direct{JDIR}/hessian_nad","hessian_nad");}
#
   cp("$direct{JDIR}/curr_iter","curr_iter");
#
#   ciudg now ensures consistent ordering of the densities
#   cid*fl.rt1, cid*fl.rt2 so that there is no need to exchange
#   the cartesian gradient files
#   after some more testing most of the reordering and copying stuff
#   can disappear.
#
#
#   for MXS optimization the cartforce links are created at the end of cid2tr
    if ( !$kwords{"nadcoupl"}) {
        unlink("cartforce");
        symlink("cartgrd","cartforce") or die "symlink failed";
    }

   if ( ! -s 'gdiisin' ) {die("could not find gdiisin file\n");}
   callprog($control{GDIIS});
   &appendtofile("gdiisls","$direct{list}/gdiisls.all",$control{iter});
   cp("gdiisfl","$direct{list}/gdiisfl");

   open(GDMG,"gdiismg");
   $_=<GDMG>;
   if (/convergence reached/) { $donewithit=1;}
        else {$donewithit=0;}

   open(GEOM,">geom");
   while (<GDMG>) {print GEOM ; }
   close(GEOM);
   close(GDMG);
#
    &info4molden($control{iter});
    #if ($kwords{"nadcoupl"} && ($kwords{"gdiis"}||$kwords{"polyhes"})) { &info4molden2();}
    if ($kwords{"nadcoupl"}) { &info4molden2();}
#
   cp("geom","$direct{JDIR}/geom");
    if ($donewithit) {printf "gradient calculation converged.\n";
                      cp("gdiisls","$direct{list}/gdiisls.min");
                      cp("$direct{geom}/geom.$control{iter}","$direct{geom}/geom.min");
                      cp("$direct{geom}/geom.$control{iter}","$direct{JDIR}/geom");
# Copy converged-MXS molden file with a new name. MB Sep 2005
                      cp("$direct{molden}/molden.nad.drt$naddrt1.state$nadstate1.drt$naddrt2.state$nadstate2.$control{iter}","$direct{molden}/molden.nad.min");
                      if ( -f "$direct{modir}/mocoef_mc.$control{iter}" )
                       { cp("$direct{modir}/mocoef_mc.$control{iter}","$direct{modir}/mocoef_mc.min");}
                      if ( -f "$direct{modir}/mocoef_scf.$control{iter}" )
                       { cp("$direct{modir}/mocoef_scf.$control{iter}","$direct{modir}/mocoef_scf.min");}
                      printf "runc.perl finished\n";
                      &printsummary();
                      exit 0;
                      }
      else {printf "gradient calculation not yet converged.\n";}

    }

###########################################################################################################################

sub link_grads_mxs{
    # perform a phase correction and create links for conical intersection optimization
    $/="\n";

      unlink(<cartforce*>);
      $file1="cartgrd.drt$naddrt1.state$nadstate1";
      $file2="cartgrd.drt$naddrt2.state$nadstate2";
      print "file1: $file1\n";
      symlink("$file1","cartforce1") or die "symlink failed";
      symlink("$file2","cartforce2") or die "symlink failed";

      $file="cartgrd.nad.drt$naddrt1.state$nadstate1.drt$naddrt2.state$nadstate2";
# FP: check the phase of the non-adiabatic coupling vector
      if (!$kwords{"nophasecorr"}) {
        print "Phase correction of h vector\n";
        if (-e "$file.old") {
          print "$file.old found\n";
#          system("cat $file.old");
          open (OLD,"$file.old");
          $i=1;
          while (<OLD>){
              s/D/E/g;chomp;s/^ *//;($nadxo[$i],$nadyo[$i],$nadzo[$i])=split(/\s+/,$_,3);$i++;
          }
          close OLD;
          $nat=$i-1;
          open (NEW,"$file");
          $i=1;
          while (<NEW>){
              s/D/E/g;chomp;s/^ *//;($nadx[$i],$nady[$i],$nadz[$i])=split(/\s+/,$_,3);$i++;
          }
          close NEW;
#
          $overl=0;
          for ($i=1,$i<$nat,$i++){
              $overl+=$nadxo[$i]*$nadx[$i]+$nadyo[$i]*$nady[$i]+$nadzo[$i]+$nadz[$i];
          }
          print "Overlap to previous coupling vector: $overl\n";
          if ($overl > 0) {
              print "Phase of the coupling vector remained the same\n";
              $filecorr=$file;
          }
          else {
              print "Phase of the coupling vector changed\n";
              open (SIGN,">$file.sign");
              for ($i=1;$i<=$nat;$i++){
                  printf{SIGN}( "%15.6E%15.6E%15.6E\n",-$nadx[$i],-$nady[$i],-$nadz[$i]);
              }
              close SIGN;
              $filecorr="$file.sign";
          }
         }
           else { # coupling vector from last iteration not found
            print "No $file.old, cannot perform phase correction\n";
            $filecorr = $file;
           }
#         print "New corrected h:\n";
#         system("cat $filecorr");
         symlink("$filecorr","cartforce3");
         cp("$filecorr","$file.old");
         &appendtofile("$filecorr","$direct{grad}/$file.phase-corr",$control{iter});
       }
       else { # no phase correction
         print "No phase correction for non-adiabatic coupling vector is performed\n";
         symlink("$file","cartforce3") or die "symlink failed $file\n";
       }
}

##########################################################################################################################################

#
# --------------------------------------------------------------------------
# Routine to perform Yarkony orthogonalization and topographic analysis of the conical intersection. MB Sep 2005
#
  sub orthogonalize{
# 1. write slope.inp
    open(SLP,">slope.inp") || warn "Cannot open slope.inp to write.";
    print SLP "&slope \n";
    print SLP "   crit     = 0, \n";
    print SLP "   ort_type = Y, \n";
    print SLP "   geomf    = \"geom\",   \n";
    print SLP "   grad1f   = \"cartgrd.drt$naddrt2.state$nadstate2\",   \n";
    print SLP "   grad2f   = \"cartgrd.drt$naddrt1.state$nadstate1\",   \n";
    #print SLP "   coupf    = \"cartgrd_total\", \n";
    print SLP "   coupf    = \"$coupf\", \n";
    print SLP "   de       = $deltae, \n";
    print SLP "  /end slope  \n";
# isn't it somewhat strange to compute the orthogonalization for a 2 vector problem 
# this way - there is a trivial analytical formulae for this
# 2. execute slope
#     $cgn = "cartgrd.nad.drt$naddrt1.state$nadstate1.drt$naddrt2.state$nadstate2";
#    cp("$cgn","$cgn.org");
    callprog("slope.x");
#    cp("nadvecort","$cgn.ortho");
#    cp("$direct{grad}/$cgn.sp","$direct{grad}/$cgn.sp.org");
#    cp("nadvecort","$direct{grad}/$cgn.ortho");
#
    if ($kwords{"nadcoupl"} && ($kwords{"gdiis"}||$kwords{"polyhes"})) {
    cp("moldenort.freq","$direct{molden}/molden.nad.drt$naddrt1.state$nadstate1.drt$naddrt2.state$nadstate2.$control{iter}");}
    else {
    cp("moldenort.freq","$direct{molden}/molden.nad.drt$naddrt1.state$nadstate1.drt$naddrt2.state$nadstate2");}
    &appendtofile("slopels","$direct{list}/slopels.drt$naddrt1.state$nadstate1.drt$naddrt2.state$nadstate2",$control{iter});
  }


###########################################################################################################################################

  sub mcscfmom {

     local ($scfprop,$ciprop,$mcscfprop,$turbocol,$av);
     my ($cnt);
     local ($kdrt,$kstate,$bdrt,$bstate);
     local ($ndrt,@x);

     if ($kwords{seward}) {die(" no transition moment support at mcscf level for seward integrals\n");}

     if ( $kwords{mcscfav} == 0 )
        { print "skipping mcscfmom because av=0 \n";}
     else
     {
     chdir "$direct{work}";

     cp("$direct{rest}/restart.sp","restart");
     cp("$direct{JDIR}/mcscfin","mcscfin");

#   repeating this analysis helps checking consistent
#   input to transft/transmom

     open(MCSCFIN,"mcscfin");
     $/="\n";
     $ndrt=0;
         while (<MCSCFIN>)
          { $_ = uc;
           if (/NAVST/i) {s/[a-zA-Z(),=]/ /g;
                          s/^ *//;
                          @x = split(/\s+/,$_);
                          $navst[$x[0]] =$x[1] ;
                          $ndrt=$ndrt+1;
                          if ($debug{debug_mcscf}) {print " debug navst @x \n ";} }
          }

         close(MCSCFIN);

     cp("$direct{JDIR}/transmomin","transmomin");

# transmomin contains the sequence of pairs of states
# for which transition moments are to be calculated
# 4 numbers per line describing bra ( DRT number, state number)
# and ket ( DRT number,state number)
# there are at most two sections labeled CI and MCSCF,respectively
# where the MCSCF and CI transition moments are listed

       open (TRANSMOMIN,"transmomin");
       $/="\n";
       local ( $ciblock,$mcscfblock);
LINE:   while (<TRANSMOMIN>)
        {
        if ( /^#/ ) {next LINE;}
        if ( /^ *$/ ) {next LINE;}
        if ( /MCSCF/ ) { $mcscfblock=1; $ciblock=0;next LINE;}
        if ( /CI/ ) { $mcscfblock=0; $ciblock=1; next LINE;}
        if ( $mcscfblock ) {s/^ *//g;
          ($kdrt,$kstate,$bdrt,$bstate) = split(/\s+/,$_);
          if ( $kdrt > $ndrt || $bdrt > $ndrt )
             { die " invalid DRT numbers \n";}
          if ( $kstate > $navst[$kdrt] ||
               $bstate > $navst[$bdrt] )
             { print "kdrt=$kdrt kstate=$kstate bdrt=$bdrt bstate=$bstate \n";
               print "but I found: \n";
               print "    number of averaged states kdrt=$navst[$kdrt]\n";
               print "    number of averaged states bdrt=$navst[$bdrt]\n";
               die "invalid state numbers \n";}

          printf " Calculating transition moment for (%d,%d) => (%d,%d)\n ",
                   $kdrt,$kstate,$bdrt,$bstate ;
          open (TRFTIN,">transftin");
          printf TRFTIN "y\n%d\n%d\n%d\n%d\n",$kdrt,$kstate,$bdrt,$bstate;
          close TRFTIN;
          callprog ("transft.x < transftin > transftls");
          callprog ("transmom.x");
          cp("transftls","$direct{list}/transftls.FROMdrt$kdrt.state${kstate}TOdrt$bdrt.state$bstate");
          cp("transls","$direct{list}/transls.FROMdrt$kdrt.state${kstate}TOdrt$bdrt.state$bstate");
         }
         }
       close TRANSMOMIN
  }

         return 0;
         }

##########################################################################################################################



  sub ciudgmom {

# solely transmomin determines which transition densities are computed
# hence, one could run a check that solely transitions with the same
# spin multiplicity are included (e.g. in genrouting) 

     my ($i,@x,$cnt,$mcscf,$scf,$ndrt,$lvalue);
     local ($kdrt,$kstate,$bdrt,$bstate);

     chdir $direct{work};
    unlink "matel"; 

if ( $kwords{ciudgndrt} > 1 )
 {   # normal case with cidrtms.x
  cp("$direct{JDIR}/transmomin","transmomin");
  open (TRANSMOMIN,"transmomin");
  $/="\n";
  local ( $ciblock,$mcscfblock);
  $icount=1;
LINE:while (<TRANSMOMIN>)
      {
       if ( /^#/ ) {next LINE;}
       if ( /MCSCF/ ) { $mcscfblock=1; $ciblock=0;next LINE;}
       if ( /CI1e/ ) { $mcscfblock=0; $ciblock=1; next LINE;}
       if ( /CI$/ || /CI *$/ ) { $mcscfblock=0; $ciblock=0; next LINE;}
       if ( $ciblock )
        {
         s/^ *//g;
         ($kdrt,$kstate,$bdrt,$bstate) = split(/\s+/,$_);
#
           if ( $kdrt > $kwords{ciudgndrt} || $bdrt > $kwords{ciudgndrt} )
            { die " invalid DRT numbers \n";}
         open (TRCIIN,">trnciin");
         print TRCIIN " &input \n  lvlprt=1, \n ";
           if (  &keyinfile("$direct{JDIR}/ciudgin.drt$kdrt","ntype") == 3  )
            {
             local $gset;$gset = &keyinfile ("$direct{JDIR}/ciudgin.drt$kdrt","gset");
             print TRCIIN "   method=$gset, \n";
               if ( &keyinfile("$direct{JDIR}/ciudgin.drt$bdrt","ntype") != 3)
                { die ("inconsistent ntype settings in ciudgin* files\n");}
               if ( &keyinfile("$direct{JDIR}/ciudgin.drt$kdrt","gset") != $gset )
                { die ("inconsistent gset settings in ciudgin* files\n")}
            } # end of: if (  &keyinfile("$direct{JDIR}/ciudgin.drt$kdrt","ntype") == 3  )
#
           if ( &keyinfile("$direct{JDIR}/ciudgin.drt$kdrt","froot") ||
               &keyinfile("$direct{JDIR}/ciudgin.drt$bdrt","froot"))
            {
             print "info: froot is set .. overriding entries in transmomin\n";
             my $en;
             ($kstate,$en)= &frootci("$direct{list}/ciudgls.drt$kdrt.sp");
             ($bstate,$en)= &frootci("$direct{list}/ciudgls.drt$bdrt.sp");
            }

         print TRCIIN "  nroot1=$kstate, \n  nroot2=$bstate \n ";
         print TRCIIN "  drt1=$kdrt, \n drt2=$bdrt \n /&end \n ";
         close TRCIIN ;
         printf " Calculating transition moment for (%d,%d) => (%d,%d)\n ",$kdrt,$kstate,$bdrt,$bstate ;
#
#         system("cat trnciin");
          callprog ("transci.x -m $control{coremem}");
#
          cp("trncils","$direct{list}/trncils.FROMdrt$kdrt.state${kstate}TOdrt$bdrt.state$bstate");
          cp("cid1trfl","cid1trfl.FROMdrt$kdrt.state${kstate}TOdrt$bdrt.state$bstate");
          }
          }
 }
else
 {  # specific case one drt with several roots
        symlink ("civfl","civfl.drt1");
        symlink ("cidrtfl","cidrtfl.1");
        symlink ("civout","civout.drt1");
        if ( -s "cirefv" ) { symlink ("cirefv","cirefv.drt1");}
       cp("$direct{JDIR}/transmomin","transmomin");
       open (TRANSMOMIN,"transmomin");
       $/="\n";
       local ( $ciblock,$mcscfblock);
LINET:  while (<TRANSMOMIN>)
        {
        if ( /^#/ ) {next LINET;}
        if ( /MCSCF/ ) { $mcscfblock=1; $ciblock=0;next LINET;}
        if ( /CI/ ) { $mcscfblock=0; $ciblock=1; next LINET;}
        if ( $ciblock ) {s/^ *//g;
          ($kdrt,$kstate,$bdrt,$bstate) = split(/\s+/,$_);
          if ( $kdrt > $kwords{ciudgndrt} || $bdrt > $kwords{ciudgndrt} ) { die " invalid DRT numbers \n";}
          open (TRCIIN,">trnciin");
          print TRCIIN " &input \n  lvlprt=3, \n ";
          print TRCIIN "  nroot1=$kstate, \n  nroot2=$bstate \n ";
          print TRCIIN "  drt1=$kdrt, \n drt2=$bdrt \n\/&end \n ";
          close TRCIIN ;
          printf " Calculating transition moment for (%d,%d) => (%d,%d)\n ",
 $kdrt,$kstate,$bdrt,$bstate ;
#
          callprog ("transci.x -m $control{coremem} ");
#
cp("trncils","$direct{list}/trncils.FROMdrt$kdrt.state${kstate}TOdrt$bdrt.state$bstate");
cp("cid1trfl","cid1trfl.FROMdrt$kdrt.state${kstate}TOdrt$bdrt.state$bstate");
          }
          }
 } # end of: if ( $ndrt )
#
# matel contains ALL lvalue components 
# format: 
##  transition   2 A1 ->   1 B1 (root1,drt1,root2,drt2,lx,ly,lz)
# 1 2 2 1 0.00000000E+00-0.10452828E+01 0.00000000E+00
  cp("matel","$direct{list}/lvalue_matel");

if ($kwords{lvalue})  # better route call and analysis of outptu
 { callprog ("lvalue.x");
  cp("lvaluels","$direct{list}/lvaluels.sp"); }
return 0 ;
}

############################################################################################################################

  sub moldenformat
   { my ($hermit,$hermitmolden,@x,$i);
     chdir ($direct{work});
     if ( ! $kwords{hermit} )
      { # must run hermit first to obtain sofinfo.dat
        # make sure that the integral program input is for hermit, indeed
        if ( ! grep(/hermit/,$kwords{intprogram}))
        { $!=2;
          die "molden interface in conjunction with hermit, only";}
        close INTPRG;
        if ( ! -s "daltcomm" ) { die "cannot find daltcomm\n"; }
        cp("daltcomm","daltcomm.old");
        open HERMITIN, ">daltcomm";
        open HERMITOLD, "<daltcomm.old";
        $/="";
        $inp=<HERMITOLD>;
        $inp=~s/\*\*INTEGRALS/**INTEGRALS\n.NOTWO/;
        print HERMITIN $inp;
        close HERMITIN;
        close HERMITOLD;
        callprog("dalton.x -m $control{coremem} > hermitls.molden");
        cp("daltdomm.old","daltcomm");
        unlink ("daltcomm.old");
       $/="\n";
      }



     if ( ! -s "soinfo.dat" )
      {$!=10;
       die "cannot find $direct{work}/soinfo.dat\n"; }
#
#    save current mocoef file in WORK directory
#
     cp("mocoef","mocoef.savemolden");
     unlink("mocoef");

#fp: For MOs use motype=2
     chdir ($direct{modir});
     @x = <mocoef*.sp> ;
     chdir ($direct{work});
     open TRANSMOIN, ">transmoin";
     print TRANSMOIN " &input\n motype=2,\n molden=1 \n\/&end\n";
     close TRANSMOIN;

     foreach $i ( @x )
       { cp("$direct{modir}/$i","mocoef");
         callprog("transmo.x > transmols");
         $i=~s/mocoef_/molden_mo_/;
#         $i=~s/nocoef_/molden_no_/;
         print "converting $direct{MODIR}/$i \n ... to molden prop. format ($direct{MOLDEN}/$i)\n";
         cp("molden","$direct{molden}/$i");
       }

#fp: For NOs use motype=1
     chdir ($direct{modir});
     @x = <nocoef*.sp> ;
     chdir ($direct{work});
     open TRANSMOIN, ">transmoin";
     print TRANSMOIN " &input\n motype=1,\n molden=1 \n\/&end\n";
     close TRANSMOIN;

     foreach $i ( @x )
       { cp("$direct{modir}/$i","mocoef");
         callprog("transmo.x > transmols");
#         $i=~s/mocoef_/molden_mo_/;
          $i=~s/nocoef_/molden_no_/;
         print "converting $direct{MODIR}/$i \n ... to molden prop. format ($direct{MOLDEN}/$i)\n";
         cp("molden","$direct{molden}/$i");
       }
#
#     re-establish original state
#
     cp("mocoef.savemolden","mocoef");
     unlink("mocoef.savemolden");
    return;
    }
#########################################################################################################################

  sub polyhes
{
   chdir $direct{work};$/="\n";
#
#  calculate constrains
#
   $maxit=$kwords{'niter'};
   &changekeyword("$direct{JDIR}/polyhesin","polyhesin"," maxit",$maxit);
   cp ("$direct{JDIR}/polyhesin","polyhesin");
#
   callprog("constraint.x > constraintls");
   &appendtofile("constraintls","$direct{list}/constraintls.all",$control{iter});
#
#  transform cartesian gradients and coupling elements into internal coord.
#
   cp ("$direct{JDIR}/intcfl","intcfl") or die "Cannot copy file: $direct{JDIR}/intcfl\n";
#
#  gradien for state 1
   &transfgrad(".drt$naddrt1.state$nadstate1","grd",1);
#
#  gradien for state 2
   &transfgrad(".drt$naddrt2.state$nadstate2","grd",1);
#
#  cartesian nonadiabatic coupling
   &transfgrad(".nad.drt$naddrt1.state$nadstate1.drt$naddrt2.state$nadstate2","grd",1);
#
#  cartesian constrains
   if (-s "cartcgrd")
    {
     &transfgrad("","cgrd",1);
     &appendtofile("cartcgrd","$direct{grad}/cartcgrd.all",$control{iter});
    }
#
    $/="\n";
    open (NIN,"cartgrd.drt$naddrt1.state$nadstate1") or die "Cannt open file: cartgrd.drt$naddrt1.state$nadstate1";
    $icount=0;
    while(<NIN>)
     {
      $icount++;
      $line=$_; $line=~s/D/E/g; chomp $line; $line=~s/^ *//;
      ($gr1[$icount][1],$gr1[$icount][2],$gr1[$icount][3])=split(/\s+/,$line,3);
     }
    close NIN;
#
    open (NIN,"cartgrd.drt$naddrt2.state$nadstate2") or die "Cannt open file: cartgrd.drt$naddrt2.state$nadstate2";
    $icount=0;
    while(<NIN>)
     {
      $icount++;
      $line=$_; $line=~s/D/E/g; chomp $line; $line=~s/^ *//;
      ($gr2[$icount][1],$gr2[$icount][2],$gr2[$icount][3])=split(/\s+/,$line,3);
     }
    close NIN;
#
     $norm=0;
     for ($i=1;$i<=$icount;$i++)
      {
       for ($j=1;$j<=3;$j++)
        {$norm=$norm + ($gr1[$i][$j]-$gr2[$i][$j])*($gr1[$i][$j]-$gr2[$i][$j]) }
      }
     $norm=sqrt($norm);
#    print "norm: $norm\n";
#
#     $energy1=&getcienergy("ciudgls.drt$naddrt1.sp",$nadstate1);
#     $energy2=&getcienergy("ciudgls.drt$naddrt1.sp",$nadstate2);
#    $temp=($energy2-$energy1)/$norm;

#
#      if ($kwords{"nadhess"} && ($temp < 0.01))
       if ($kwords{"nadhess"})
        {
#---------
         chdir $direct{JDIR};
         print "\n\n  Starting EXACT HESSIAN matrix calculation\n\n";
         &displacement("$direct{JDIR}/geom");
         print "\n\n  Performing HESSIAN MATRIX calculation, see calcls for details\n\n";
         $cbus = $ENV{"COLUMBUS"};
         $ERR=system ("$cbus/calc.pl > calcls");
         $ERR=$ERR/256;
         if (!$ERR ==0 )
          {$!=$ERR;die" Error occured in calc.pl/runc.perl: $ERRCODE[$ERR]\n";}
#  hessian matrix should be calculated only ones
         delete $kwords{"nadhess"};
         chdir $direct{work};
#--------
         &changekeyword("polyhesin","polyhesin","newton",0);
         &changekeyword("polyhesin","polyhesin","iheseq1",0);
         &changekeyword("polyhesin","polyhesin","ihess",1);
#
# get the internal gradient for the REFERENCE point
#
         $dir="$direct{JDIR}/DISPLACEMENT/REFPOINT/GRADIENTS";
#
         $file="intgrd.drt$naddrt1.state$nadstate1";
         cp ("$dir/$file.sp","$file") or die "Failed to copy file:$dir/$file.sp\n";
         $file="intgrd.drt$naddrt2.state$nadstate2";
         cp ("$dir/$file.sp","$file") or die "Failed to copy file:$dir/$file.sp\n";
         $file="intgrd.nad.drt$naddrt1.state$nadstate1.drt$naddrt2.state$nadstate2";
         cp ("$dir/$file.sp","$file") or die "Failed to copy file:$dir/$file.sp\n";
#
         unlink ("progress");
         print "calling polyhes for reference point\n";
         callprog("polyhes.x > polyhesls");
         cp("polyhesls","polyhes.refpoint");
#
# get the internal gradient for the DISPLACEMENT points
#
         open (NIN,"$direct{JDIR}/DISPLACEMENT/displfl") or die "Failed to open file:$direct{JDIR}/DISPLACEMENT/displfl\n";
         <NIN>;<NIN>;<NIN>;
           while (<NIN>)
            {
             chomp; s/^ *//;
             ($coor,$displ,$flag)=split(/\s+/,$_,3);
               if (($flag ne "skip")&&($flag ne "fixc"))
                {
                 $dir="$direct{JDIR}/DISPLACEMENT/CALC.c$coor.d$displ/GRADIENTS";
#
                 $file="intgrd.drt$naddrt1.state$nadstate1";
                 cp ("$dir/$file.sp","$file") or die "Failed to copy file:$dir/$file.sp\n";
                 $file="intgrd.drt$naddrt2.state$nadstate2";
                 cp ("$dir/$file.sp","$file") or die "Failed to copy file:$dir/$file.sp\n";
                 $file="intgrd.nad.drt$naddrt1.state$nadstate1.drt$naddrt2.state$nadstate2";
                 cp ("$dir/$file.sp","$file") or die "Failed to copy file:$dir/$file.sp\n";
                }
             print "calling polyhes for point:$coor,$displ\n";
             callprog("polyhes.x > polyhesls");
             cp("polyhesls","polyhes.c$coor.d$displ");
            }
         close NIN;
        }
       else
        {
         callprog("polyhes.x > polyhesls");
         &appendtofile("polyhesls","$direct{list}/polyhesls.all",$control{iter});
        }
#
  &appendtofile("displace","$direct{geom}/displace.all",$control{iter});
  cp ("displace","$direct{geom}/displace.sp");
  &protocol4nad($control{iter});
#  generate new cartesian geometry
  open (NIN,">cart2intin") or die "Cannot open file: cart2intin";
  print NIN " &input\n calctype='int2cart',\n linallow=1,\n";
  print NIN " linonly=0,\n maxiter=200,\n geomch=1,\n";
  print NIN " intgeomchfl=\'displace\',\n";
  print NIN "\/&end\n";
  close NIN;
  callprog("$control{CART2INT}");
  &appendtofile("$direct{work}/cart2intls","$direct{list}/cart2int1ls.disp.all",$control{iter});
#
# update the geometry
  cp("geom.new","$direct{JDIR}/geom");
#
  &info4molden($control{iter});
  if ($kwords{"nadcoupl"} && ($kwords{"gdiis"}||$kwords{"polyhes"})) { &info4molden2();}
#
  &appendtofile("message","$direct{list}/message.all",$control{iter});
  print "message:\n";
  system("cat message");
  open (FILE3,"$direct{work}/message") or die "failed to open file: message\n";
  $_=<FILE3>; close FILE3;
  print "    \n POLYHES status:$_";
   if (/CONTINUES/)
    {print " convergence not reached, calculation continues...\n\n";}
   if (/HALTED/)
    {
     print " maximum iterations of polyhes reached, calculation stops\n";
     printf "runc.perl finished\n";
     exit 0;
    }
   if (/FAILED/)
    {
     print " calculations diverged , calculation stops\n";
     printf "runc.perl finished\n";
     exit 0;
    }
   if (/CONVERGED/)
    {
     print " convergence reached, calculation stops\n";
     printf "runc.perl finished\n";
     exit 0;
    }

}


#############################################################################################################################


  sub cartgrd_alaska {
   my ($extension,$nonuc)=@_;
         $alaskainput=extractmolcas("$direct{JDIR}/molcas.input","ALASKA");
         if ($nonuc) { print " adding keyword NONUC to alaska input\n";
                       $alaskainput=~s/:End of Input/:NONUC:End of Input/i; }
         $alaskainput=~s/:/\n/g;
         open FOUT,">molcas.input.alaska";
         print FOUT $alaskainput;
         close FOUT;
         system("cp mocoef_mc.lumorb INPORB");
         #system("molcas  molcas.input.alaska > molcas.output.alaska 2>&1 ") == 0 || die("call to alaska failed\n");
         #die ("before alaska");
         callprog("molcas molcas.input.alaska > molcas.output.alaska ");
         print "cartesian gradient by alaska (MOLCAS)\n";
         cp("molcas.output.alaska","$direct{list}/alaskals.sp");
         &appendtofile("molcas.output.alaska","$direct{list}/alaskals.$extension.all",$control{iter});
#        system("cat cartgrd");
         return;}


  sub cartgrd_dalton {
   my ($extension,$nonuc)=@_;
         cp("$direct{JDIR}/abacusin","daltcomm") or die"File: abacusin not found\n";
         if ($nonuc) { open (NIN,"daltcomm"); open (NIN2,">daltcomm.new");
                       while (<NIN>){
                             if (/COLBUS/) { print NIN2 "$_";print NIN2 ".NONUCG\n"; }
                                   else { print NIN2 "$_"; }}
                      close NIN; close NIN2;
                       mv ("daltcomm.new","daltcomm"); }


         if ($kwords{"pabacus"} )
          {pcallprog("pdalton.x -m $control{pcoremem} > abacusls"); }
         else
          {callprog("dalton.x -m $control{coremem} > abacusls"); }
         if ($debug{debug_deriv})
         {
         # in the cases of potmat and/or nadcalc=3 this is only an intermediate result
           print "cartesian gradient by abacus (DALTON 1)\n";
           system("cat cartgrd");
         }
          if (defined $kwords{"potmat"}){
               printf "++++++++++++ adding potential matrix to cartgrad +++++++++++++++\n";
               if ($nonuc) {&changekeyword("$direct{JDIR}/elpotin","elpotin","property","\'nad\'");}
                    else   { &changekeyword("$direct{JDIR}/elpotin","elpotin","property","\'gradient\'"); }
               callprog("potential.x -m $control{coremem}");
               if ($nonuc) {  &appendtofile("elpotls","$direct{list}/elpotls.nad.$extension.all",$control{iter});
#fp: it is confusing to have so many gradient files
                            if ($nadcalc > 1){
  if ($debug{debug_deriv}){ &appendtofile("cartgrd.pointcharges","$direct{grad}/pcgrad.grd.$extension.all",$control{iter});}
                                               #cp("cartgrd.pointcharges","$direct{grad}/pcgrad.grd.$extension.sp");
                                               open(FILE,"cartgrd.pointcharges") or die "Failed to open file: cartgrd.pointcharges\n$!\n";
                                                $i=1;
                                                while(<FILE>){ s/D/E/g;chomp;s/^\s+//;($pcdx[$i],$pcdy[$i],$pcdz[$i])=split(/\s+/,$_,3);$i++; }
                                                close FILE;
                                                $ndim=$i-1;
                                                open(FILE,">cartgrd.pointcharges") or die "Cannot write to file: cartgrd.pointcharges\n$!\n";
                                                for($i=1;$i<=$ndim;$i++){ $x=$pcdx[$i]/$deltae; $y=$pcdy[$i]/$deltae; $z=$pcdz[$i]/$deltae;
                                                                           printf{FILE}("%15.6E%15.6E%15.6E\n",$x,$y,$z); }
                                                close FILE; }

                          }
                   else    { &appendtofile("elpotls","$direct{list}/elpotls.grd.$extension.all",$control{iter});}
               printf ">>>>>>> Added charge contributions to atomic gradients <<<<<<<<\n";

       $pcgrad=keyinfile("$direct{JDIR}/pot_reduce.in","pcgrad");
       if ($pcgrad eq "") {$pcgrad = 0;}
       # the default (pcgrad=0) is to take the pcgrad files as they are here
       # pcgrad = -1 supresses this listing
       # pcgrad =  1 recovers the full gradient from a coarse grained potential.xyz
       if ( $pcgrad eq 0 )
       {
           print "pcgrad files saved in $grad directory\n";
           if ($nonuc)
           {
               &appendtofile("cartgrd.pointcharges","$direct{grad}/pcgrad.nad.$extension.all",$control{iter});
               cp("cartgrd.pointcharges","$direct{grad}/pcgrad.nad.$extension.sp");
           }
           else
           {
               &appendtofile("cartgrd.pointcharges","$direct{grad}/pcgrad.$extension.all",$control{iter});
               cp ("cartgrd.pointcharges","$direct{grad}/pcgrad.$extension.sp");
           }
       }
       elsif ( $pcgrad eq 1 )
       {
          print "\nRecovering full point charge gradient from reduced potential.xyz\n";
          callprog("pot_reduce.py expand > pot_reducels");
          &appendtofile("pot_reducels","$direct{list}/pot_reducels.expand.$extension.all",$control{iter});
           if ($nonuc)
           {
               &appendtofile("cartgrd.pointcharges.expanded","$direct{grad}/pcgrad.nad.$extension.all",$control{iter});
               cp("cartgrd.pointcharges.expanded","$direct{grad}/pcgrad.nad.$extension.sp");
           }
           else
           {
               &appendtofile("cartgrd.pointcharges.expanded","$direct{grad}/pcgrad.$extension.all",$control{iter});
               cp ("cartgrd.pointcharges.expanded","$direct{grad}/pcgrad.$extension.sp");
           }

       }

              }
          unlink (<fort*>,"aodens");
          &appendtofile("abacusls","$direct{list}/abacusls.$extension.all",$control{iter});
          return;
        }


