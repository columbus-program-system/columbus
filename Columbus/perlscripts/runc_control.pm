package  runc_control;
use Exporter();
@ISA       =qw(Exporter);
@EXPORT    =qw(genrouting gradinit control_section2 printsummary );
  use Cwd;
  use Shell qw(pwd cd date mv cat);
  use File::Copy 'cp';  # use perl cp
                        # use perl unlink instead of rm
                        # use perl rename instead of mv

use colib_perl;
use runc_globaldata;
use runc_utilities;
 

 sub genrouting {

 local ($schluessel,$wert, $validkeys,@vk,@routing);
#local (%kwords, $schluessel,$wert, $validkeys,@vk,@routing);

#
# @vk = routing card may contain the following subroutine names
#
#  prep_inputs                    exptvlscf
#  aointegrals                    mcdrtpart
#  potmat                         mcscfpart
#  scfpart                        mcscfmom
#  preprop                        exptvlmcscf
#  drtpart                        preci
#  ciudgpart_0:8                  cutci
#  ciudgmom                       exptvlci 
#  exptvlci                       moldenformat
#  polyhes                        caspt2
#  ccsdt                          control_section2 
#
 if ($debug{debug_runc}) { print " executing sub genrouting \n";} 

  $kref=$_[0];

#
# produce an ordered listing of subroutine calls
#
   $validkeys="";
   while ( ($schluessel,$wert) = each %$kref )
    { if ($wert) { $validkeys = $validkeys . ":" . $schluessel ; }}

   @vk=split(/:/,$validkeys);

### list of impossible key word combinations in control.run
$nimpossible=5;
$impossible[1]="ciudg:pciudg";
$impossible[2]="ciudg:ciudgav";
$impossible[3]="ciudgav:pciudgav";
$impossible[4]="pciudg:pciudgav";
$impossible[5]="pcigrd:cigrd";

    for ($i=1; $i<=$nimpossible; $i++)
   { my ($x1,$x2);
      ($x1,$x2)=split(':',$impossible[$i]);
     if ( grep(/^$x1$|^$x2$/, @vk) > 1 )
      { die ("invalid keyword combination in control.run $x1 and  $x2 \n");}} 

     if ( -s "cidrtin" ) {cp ("$direct{JDIR}/ciudgin","$direct{JDIR}/ciudgin.drt1"); cp("$direct{JDIR}/TASKLIST","$direct{JDIR}/TASKLIST.drt1"); }
    $$kref{"listofdrts"} = join(':',(1 .. $$kref{ciudgndrt}));
    #print "detected $$kref{ciudgndrt} DRTs in CI step \n";
    #print "listofdrts=",$$kref{"listofdrts"},"\n";

    if (-s "mcscfin") { $/="\n";
                        my $navst=0;
                        my $ndrt =0;
                        open(MCSCFIN,"mcscfin");
                        while(<MCSCFIN>) {$ndrt+=/NAVST/i; $navst+=/WAVST/i;}
                        close(MCSCFIN);
                        if (! $navst ) { $navst=1;}
                        if (! $ndrt ) { $ndrt=1;}
                        $$kref{"mcscfav"}=$navst;
                        if ($$kref{"mcscfndrt"} > $ndrt) { print "you are referencing less DRTs in mcscfin than there are mcdrtin* files\n";
                                                           print "check your input for correctness\n";}
                        elsif ($$kref{"mcscfndrt"}< $ndrt) {die("inconsistent input for mcdrtin*/mcscfin $ndrt <-> $$kref{mcscfndrt} \n");} 
                        print "MCSCF energy averaged across $ndrt DRTs and $navst states\n";}


#### in case of multiple drts and $ndrtci > 1  make sure that ciudgav is present

    if ($$kref{mcscfgrad}) { $$kref{LEVEL}="GRAD-MCSCF";}
    if ($$kref{samcgrad}) { $$kref{LEVEL}="GRAD-SAMCSCF";}
    if ($$kref{cigrad}) { $$kref{LEVEL}="GRAD-MRCI";}

  # print "STATUS optimize=$$kref{optimize} \n"; 

### the following applies to CI level optimization 
#--- need to include the MCSCF and SAMCSCF case 

     if ($$kref{ciudgndrt}>1 && ! grep(/ciudgav$/,@vk) ) 
     { die("cannot specify multiple DRTs without specification of ciudgav/pciudgav\n");}

###                $ndrtci=1   single-DRT case with cidrtmsin or cidrtin -> cidrtfl.1
###                $ndrtci>1   multi-DRT case with cidrtmsin -> cidrtfl.1, cidrtfl.2 ....


    ### a geometry optimization is characterized by niter > 1 (top line of control.run)
    ### AND the presence of the some geometry optimizer (gdiis,slapaf,polyhes)
    ### supported are: single state geometry optimization                                $kwords{optimize}=GEOMOPT:$state1:$root1
    ###                conical intersection optimization (two states, one drt)           $kwords{optimize}=MXS:$state1:$root1:$state2:$root2
    ###                state intersection optimization   (two states, two drts, no NACs) $kwords{optimize}=MXS-no-h:$state1:$root1:$state2:$root2
    
    if ( $$kref{optimize} ) { if ( ! grep/^gdiis$|^slapaf$|^polyhes$|^rgf$/, @vk )
                              { die ("cannot specify geometry optimization (niter=" . $$kref{niter} .
                                        ") without optimizer (gdiis|slapaf|polyhes|rgf)\n");}
                              $$kref{SINGLE}=0; 
                             if ( grep /MXS:/,$$kref{optimize} ) {$$kref{GEOMOPT}="NAD"; 
                                                                 print "**** This is a conical intersection optimization ****\n";}
                             elsif ( grep /MXS-no-h/,$$kref{optimize} ) {  $$kref{GEOMOPT}="CROSS";
                                                                          print "*** This is a intersection optimization without NAC terms ****\n";}
                             elsif ( grep /GEOMOPT:/,$$kref{optimize} ) 
                               { $$kref{GEOMOPT}="GEOM";
                                 print "*** This is a single state geometry optimization ****\n";}
                              else
                               {die("this is an invalid keyword combination for geometry optimizations\n");}

                              if ( ! grep (/GRAD/,$$kref{LEVEL}))
                                   {die("gradient is not specified for geometry optimization (mcscfgrad|samcgrad|cigrad)\n");}

                              if ( ! grep (/abacus$|alaska/,@vk))
                                   {die("ao derivative integral package not specified (abacus|alaska) \n");}
                              $$kref{geomopt}=1;

                            # POSTPONE ALL INTEGRITY TESTS/ ON-THE-FLY MODIFICATIONS  AFTER CALL TO getciinfo/getmcscfinfo  
                            # also set grad{state1},grad{drt1} later - if necessary at all.

                             }
    ### supported are: SINGLENAD:   evaluation of NADC vectors for multiple states within one irrep (one DRT) 
    ###                             in case of multiple DRTs, NADC contributions to transitions across DRTs are ommitted.
    ###                SINGLEGRAD:  evaluation of gradients for multiple states and multiple DRTs 
                else         {$$kref{"GEOMOPT"}=0;      ## if gradients are specified, this may serve as input to 
                                                        ## some dynamics package (newton-x) 
                                                        ## or to numerical frequency calculation
                                                        ## an arbitrary number of gradients can be evaluated 
                                if (  $$kref{LEVEL}) { $$kref{SINGLE}="GRAD"; } else { $$kref{SINGLE}="POINT";}
                                if ( $kkref{"SINGLE"} eq "GRAD" && ! grep (/abacus$|alaska/,@vk))
                                      {die("ao derivative integral package not specified (abacus|alaska) SINGLEGRAD case \n");} 
                                if (  grep(/nadcoupl/,@vk) && $$kref{"SINGLE"} eq "GRAD"   ) { $$kref{"SINGLE"}="NAD"}
                                if ($$kref{"SINGLE"} eq "GRAD" ) {print "*** This is a single point gradient evaluation ***\n";}
                                if ($$kref{"SINGLE"} eq "NAD") {print "*** This is a single point NAC evaluation ***\n";}
                                if ($$kref{"SINGLE"} eq "POINT" ) {print "*** This is a single point energy evaluation ***\n";}

                             } 

   if ($$kref{GEOMOPT} eq "NAD"  && $$kref{LEVEL} eq "GRAD-MRCI") {$$kref{nadcoupl_ci}=1; $$kref{ciudgmom}=1;}
   if ($$kref{GEOMOPT} eq "NAD"  && $$kref{LEVEL} eq "GRAD-SAMCSCF") {$$kref{nadcoupl_mc}=1;}
   if ($$kref{SINGLE} eq "NAD"  && $$kref{LEVEL} eq "GRAD-SAMCSCF") {$$kref{nadcoupl_ci}=1;$$kref{ciudgmom}=1;}
   if ($$kref{SINGLE} eq "NAD"  && $$kref{LEVEL} eq "GRAD-SAMCSCF") {$$kref{nadcoupl_mc}=1;}


   if ($$kref{optimize} || $kkref{dynamic}) {&gradinit();}


 if ($debug{debug_runc}) {
 print "STATUS: \n"; 
 print "kword{GEOMOPT}= $$kref{GEOMOPT} \n";
 print "kword{SINGLE} = $$kref{SINGLE} \n";
 print "kword{LEVEL}  = $$kref{LEVEL} \n";
 print "kword{nadcoupl}  = $$kref{nadcoupl} \n";
 print "kword{nadcoupl_ci}  = $$kref{nadcoupl_ci} \n";
 print "kword{nadcoupl_mc}  = $$kref{nadcoupl_mc} \n";
 print "kword{geomopt}  = $$kref{geomopt} \n";
 print "vk:",join(':',@vk),"\n";} 


##############################################################################
###################### ROUTING RULES #########################################
##############################################################################

   @routing=("prep_inputs" );

### INTEGRAL STEP 

#--- Falls argos oder hermit berechne aointegrale
   if (( grep /argos|hermit|seward|dalton2/, @vk ) && (! grep /nointegrals/,@vk )) { @routing = (@routing, "aointegrals");}
#--- if transmo - run transmo.x to transform the orbitals from a higher symmetry
   if ( $$kref{transmo} )  { @routing = (@routing, "transmo");}
#--- if potmat - run potential
#---  this changes the 1e-aoints matruc_25jun07
   if (( $$kref{potmat})  && ( ! grep /nointegrals/,@vk) ) { @routing = (@routing, "potmat"); }

### MO generation step   -- need to add pmcscf options  

#---- Falls scf               berechen scf
   if ( grep /^scf$|^dft$/, @vk )         { @routing = (@routing, "scfpart");}
#---- Falls scfprop           berechne scfprop gleich danach
   if ( $$kref{scfprop}) {
        if ( ! $$kref{scf} )
              { @routing = (@routing, "preprop", "exptvlscf");}
        else  { @routing = (@routing, "exptvlscf");}
      }
#----  Falls mcscf             berechne mcscf
   if ( ($$kref{mcscf}) )
         {@routing = (@routing, "mcdrtpart","mcscfpart");}
#----  use molcas rasscf code 
   if ( (grep /(^rasscf$|^irasscf$)/, @vk) && ( ! grep /^mcscf/, @vk) )       
         {@routing = (@routing, "mcscfpart");}

#----  Falls mcscfmom          zuerst transmom
   if (( $$kref{mcscfmom})  && ($$kref{mcscf}))
            { @routing = (@routing, "mcscfmom");}
   if (( $$kref{mcscfmom} ) && !( $$kref{mcscfmom} ) && !((grep /(^rasscf$|^irasscf$)/, @vk)))
        { @routing = (@routing, "mcdrtpart","mcscfmom");}
#----  Falls mcscfprop         gleich danach mcscfprop

   if ( ($$kref{mcscfprop} )  && !((grep /(^rasscf$|^irasscf$)/, @vk)))      
      { if ( ! $$kref{mcscf} )
              { @routing = (@routing, "preprop", "exptvlmcscf");}
        else  { @routing = (@routing, "exptvlmcscf");}}


#---- if molden  save MOs in molden format for visualization

   if ( $$kref{molden} )
      { @routing = (@routing, "moldenformat");
        $$kref{"hermitmolden"}=1;
       }

####  MOLCAS correlation methods
#--- caspt2 if caspt2
   if ($$kref{caspt2} ) {@routing=(@routing, "caspt2"); }
#--- ccsd/ccsd(t)
   if (( $$kref{ccsdt}) ) {@routing=(@routing, "ccsdt"); }

#### (multiple) CI energy step(s)

   if ( grep /^(ciudg|ciudgav|pciudg|pciudgav)$/, @vk )
     { @routing = (@routing,"drtpart","preci");
       for ($i=1; $i<=$$kref{ciudgndrt}; $i++) { @routing = ( @routing, "cipart_if($i)")}}

#--- remark:  cutci should be called directly within cipart_x (seriel only) 
#---          ciprop can be eliminated because the ci code 
#---                 should directly evaluate 1e properties  
#---                 


#### postprocessing of multiple CI vectors across multiple DRTs

#--- 1e-transition properties - this takes infos out of transmomin !!!
   if (($$kref{ciudgmom} ))
     { @routing = (@routing,"ciudgmom");}
#--- 1e properties for all states optionally for multiple DRTs  !!! modify preprop/extpvlci 
   if ( $$kref{ciprop} )       {
        if  (! grep /ciudg$|ciudgav$/, @vk )
              { @routing = (@routing, "preprop", "exptvlci");}
        else  { @routing = (@routing, "exptvlci");}
      }


####  GRADIENT part 

#--- control_section2 placed directly before the gradient portion
     if (! $$kref{GEOMOPT}) { @routing =(@routing, "printsummary");}
     @routing=(@routing, "control_section2");

#--- nad coupling at ci level
   if ( $$kref{nadcoupl}  && $$kref{cigrad} )
      { @routing = ( @routing, "motran");
       for ($i=1; $i<=$$kref{ciudgndrt}; $i++) { @routing = ( @routing, "cigrd_if($i)")}
       for ($i=1; $i<=$$kref{ciudgndrt}; $i++) { @routing = ( @routing, "cinad_if($i)")}
       if ( ($nad{nnadcoupl}==1) && ($kwords{"slope"} || $kwords{"gdiis"} || $kwords{"polyhes"} ))
        {@routing = (@routing, "link_grads_mxs");}
      }
#--- nad coupling at mcscf level  # not yet fully fixed
   elsif ( $$kref{nadcoupl}  && $$kref{samcgrad} )
      {  @routing = ( @routing, "motran" );
         if ($kwords{mcscfndrt}) 
         { #for ($i=1; $i<=$kwords{mcscfndrt}; $i++) { @routing = ( @routing, "cigrd_$i");}
           for ($i=1; $i<=$kwords{mcscfndrt}; $i++) { @routing = ( @routing, "cinad_$i");}}
         else {@routing = ( @routing, "cigrd_0");
               @routing = ( @routing, "cinad_0"); }
      }
#--- ci gradient
   elsif ($$kref{cigrad}) 
        {@routing = (@routing,  "motran");
          for ($i=1; $i<=$$kref{ciudgndrt}; $i++) { @routing = ( @routing, "cigrd_if($i)")}}
#--- mcscf gradient
    elsif ($$kref{mcscfgrad} || $$kref{samcgrad}) { @routing=(@routing, "mcscfgrad"); }

#####  geometry update
#----  polyhes

   if ( grep /^polyhes/, @vk )
      { @routing = (@routing, "polyhes");
        $$kref{"polyhes"}=1;
        $natom=&geom;
      }
#---- gdiis
    if ($kwords{gdiis}) { @routing =(@routing, "gdiisstuff"); }
#---- rgf
    if ($kwords{rgf}) { @routing =(@routing, "rgf");}
#---- slapaf 
    if ($kwords{slapaf}) { @routing =(@routing, "slapaf"); }

   @routing = (@routing, "printsummary");


#############################################################################
######################## INPUT FILE CHECK ###################################
#############################################################################


  local ($nerr,$av,$ndrt,$ndrtci);
# printf "Checking for complete set of input files ... ";
  if ($debug{debug_runc}) {
     print "Keys present:\n";
     foreach $i  ( keys(%kwords) ) { print "key $i = $kwords{$i}\n";}
  }
  &mocoef_check();
  ($nerr,$av,$ndrt,$ndrtci) = &check_inputfiles(%$kref);
  if ($debug{debug_runc}) {
    print "Input files checked:\n";
    print "nerr: $nerr, av: $av, ndrt: $ndrt, ndrtci: $ndrtci\n";
  }
  if ( $nerr )
   { $!=10;
    die " $nerr missing input files \n";
   }
  if ($debug{debug_runc}) { print "ROUTING=",@routing; }
  return (@routing);
 }

#################################################################################################################

  sub gradinit 
{
#  later we should make this more direct !!
  my ($i,@tmp,@tmpo); 
  $nad{nnadcoupl}=0;
  $nad{ngrad}=0;
### supported are: single state geometry optimization                                $kwords{optimize}=GEOMOPT:$state1:$root1
###                conical intersection optimization (two states, one drt)           $kwords{optimize}=MXS:$state1:$root1:$state2:$root2
###                state intersection optimization   (two states, two drts, no NACs) $kwords{optimize}=MXS-no-h:$state1:$root1:$state2:$root2

# input for a gradient
 if ($debug{debug_runc}) { print " executing sub gradinit \n";} 
 @tmpo=split(/:/,$kwords{optimize});
 @tmpod=split(/:/,$kwords{dynamic});
  if ($debug{debug_runc}){ print "gradinit: tmpo = ",join(':',@tmpo)," \n"; 
                           print "translation: ",keys %{$translation{cidrt}},"\n";
                           print "translation: ",values %{$translation{cidrt}},"\n";
                           print "cigrad = $cigrad{n} \n";
                           print "gradinint: tmpod =",join(':',@tmpod),"\n"; }

# Now, let's put in as default all possible and necessary gradients and couplings
# this has to be done here because, the list of states from control.run 
# has not been read at the time of read_transmomin

  if ($kwords{optimize})
   { if ($kwords{LEVEL} eq "GRAD-MRCI" && $cigrad{n} == 0 ) 
     { if ($debug{debug_runc}) {print "selecting default settings for MRCI gradients (optimize)\n";} 
       $cigrad{n}++; ${$cigrad{drt}}[$cigrad{n}]=${$translation{cidrt}}{$tmpo[1]};
                   ${$cigrad{root}}[$cigrad{n}]=$tmpo[2];
     if ($tmpo[0] ne "GEOMOPT") 
      { $cigrad{n}++; ${$cigrad{drt}}[$cigrad{n}]=${$translation{cidrt}}{$tmpo[2]};
                   ${$cigrad{root}}[$cigrad{n}]=$tmpo[4]; }
    }


     if ($kwords{LEVEL} eq "GRAD-MCSCF" && $mcscfgrad{n} == 0 )
     { if ($debug{debug_runc}) {print "selecting default settings for MCSCF gradients (optimize)\n";}
       $mcscfgrad{n}++; ${$mcscfgrad{drt}}[$mcscfgrad{n}]=${$translation{mcdrt}}{$tmpo[1]};
                   ${$mcscfgrad{root}}[$mcscfgrad{n}]=$tmpo[2];
     if ($tmpo[0] ne "GEOMOPT")
      { $mcscfgrad{n}++; ${$mcscfgrad{drt}}[$mcscfgrad{n}]=${$translation{mcdrt}}{$tmpo[2]};
                   ${$mcscfgrad{root}}[$mcscfgrad{n}]=$tmpo[4]; }
    }

     if ($kwords{LEVEL} eq "GRAD-MRCI" && $citrans{n} == 0) 
     { if ($debug{debug_runc}) {print "selecting default settings for MRCI nacs (optimize) \n";} 
       if ($tmpo[0] eq "MXS" ) 
       { $citrans{n}++;
         ${$citrans{entries}}[$citrans{n}]= join(':',(${$translation{cidrt}}{$tmpo[1]},$tmpo[2],${$translation{cidrt}}{$tmpo[3]},$tmpo[4])); } }

    }

   if ($kwords{dynamic})
   { if ($kwords{LEVEL} eq "GRAD-MRCI" && $cigrad{n} == 0 ) 
     { if ($debug{debug_runc}) {print "selecting default settings for MRCI gradients (dynamics)\n";} 
        my ($nstates,$i,$j); $nstates=$#tmpod%2;
          $j=1;
         for ($i=1; $i<=$nstates; $i++) 
           { $j++;$cigrad{n}++; ${$cigrad{drt}}[$cigrad{n}]=${$translation{cidrt}}{$tmpod[$j]};
             $j++;${$cigrad{root}}[$cigrad{n}]=$tmpod[$j];}   
     }
     if ($kwords{LEVEL} eq "GRAD-MRCI" && $citrans{n} == 0 )
     { if ($debug{debug_runc}) {print "selecting default settings for MRCI nacs      (dynamics)\n";}
         for ($i=1; $i<=$cigrad{n}; $i++)
           { for($j=1; $j<$i; $j++) 
            { if (${$cigrad{drt}}[$i] == ${$cigrad{drt}}[$j]) {$citrans{n}++; 
                ${$citrans{entries}}[$citrans{n}]=join(':',(${$cigrad{drt}}[$i],${$cigrad{root}}[$i],${$cigrad{drt}}[$j],${$cigrad{root}}[$j]));} 
           }}
    }}


  for ($i=1; $i<=$cigrad{n}; $i++)    # this has been set by read_transmomin
     { if ($kwords{optimize}) {   # select only relevant gradients - assume a complete set  (this can be included via read_transmomin)
                                if ($tmpo[0] eq "GEOMOPT") {
                                if (${$translation{cidrt}}{$tmpo[1]} != ${$cigrad{drt}}[$i]) {next;} 
                                if ($tmpo[2] != ${$cigrad{root}}[$i]) {next;}} 
                                else {
                                if (${$translation{cidrt}}{$tmpo[1]} != ${$cigrad{drt}}[$i]) {next;} 
                                if ($tmpo[2] != ${$cigrad{root}}[$i]) {next;} 
                                if (${$translation{cidrt}}{$tmpo[3]} != ${$cigrad{drt}}[$i]) {next;} 
                                if ($tmpo[4] != ${$cigrad{root}}[$i]) {next;}} 
                              }
      $nad{ngrad}++;
      ${$nad{grad_drt}}[$nad{ngrad}]=${$cigrad{drt}}[$i]; 
      ${$nad{grad_state}}[$nad{ngrad}]=${$cigrad{root}}[$i];
     }

   if ($tmpo[0] eq "GEOMOPT" && $nad{ngrad} != 1  && $kwords{LEVEL} eq "GRAD-MRCI") { die("gradinit inconsistent settings GEOMOPT and $nad{ngrad} \n");}
   if ($tmpo[0] eq "GEOMOPT" ) { $grad{drt1}=${$nad{grad_drt}}[$nad{ngrad}]; $grad{state1}=${$nad{grad_state}}[$nad{ngrad}]; }


# non-adiabatic coupling
  for ($i=1; $i<=$citrans{n}; $i++) 
     { @tmp=split(/:/,${$citrans{entries}}[$i]); 
       if ($kwords{optimize}) {   # select only relevant transition - this is just a check that it is included in transmomin 
                                if ($tmpo[0] eq "GEOMOPT" || $tmpo eq "MXS-no-h") {  # no need for transition densities 
                                                              # set nadcoupl to 0 
                                                             if ($kwords{nadcoupl}) {print "deleting nadcoupl ...\n"; $kwords{nadcoupl}=0;} last;}
                                if ($tmpo[0] eq "MXS")  { 
                                if (${$translation{cidrt}}{$tmpo[1]} != $tmp[0]) {next;} 
                                if ($tmpo[2] != $tmp[1]) {next;} 
                                if (${$translation{cidrt}}{$tmpo[3]} != $tmp[2]) {next;} 
                                if ($tmpo[4] != $$tmp[3]) {next;}}} 
      $nad{nnadcoupl}++; 
      ${$nad{nad_drt1}}[$nad{nnadcoupl}]= $tmp[0]; 
      ${$nad{nad_state1}}[$nad{nnadcoupl}]=$tmp[1];    
      ${$nad{nad_drt2}}[$nad{nnadcoupl}]= $tmp[2]; 
      ${$nad{nad_state2}}[$nad{nnadcoupl}]=$tmp[3];    
      print "New Coupling ($nad{nnadcoupl}), ${$nad{nad_drt1}}[$nad{nnadcoupl}],",
            " ${$nad{nad_state1}}[$nad{nnadcoupl}], ${$nad{nad_drt2}}[$nad{nnadcoupl}], ${$nad{nad_state2}}[$nad{nnadcoupl}]\n"
     }
     
    if ($tmpo[0] eq "MXS" && (! $nad{nnadcoupl} )) 
       {die("inconsistent transmomin entries (CI section) and optimize keyword in control.run (MXS)\n");}

    for ($idrt=1; $idrt<=$kwords{ciudgndrt}; $idrt++)
     { $filen="$direct{JDIR}/ciudgin.drt$idrt"; 
       if (-e $filen) {
          &changekeyword("$filen","$filen","iden",2);
          my $tempstr ="transition\n";
          my $cnt = 0;
          for ($i=1;$i<=$nad{nnadcoupl};$i++)   # transition densities
          { if (${$nad{nad_drt1}}[$i] != $idrt )  {next;}
              $tempstr.=sprintf("%3d%3d%3d%3d \n",${$nad{nad_drt1}}[$i],${$nad{nad_state1}}[$i],${$nad{nad_drt2}}[$i],${$nad{nad_state2}}[$i]);
              $cnt++; }
          for ($i=1;$i<=$nad{ngrad};$i++)   # gradients
          { if (${$nad{grad_drt}}[$i] != $idrt )  {next;}
              $tempstr.=sprintf("%3d%3d%3d%3d \n",${$nad{grad_drt}}[$i],${$nad{grad_state}}[$i],${$nad{grad_drt}}[$i],${$nad{grad_state}}[$i]);
              $cnt++; }
          if ($cnt) { print "appendtonl: $tempstr\n";
                      &appendtonl("$filen","$filen",$tempstr); }}
     }
#
  $nad{nadcalc}=keyinfile("$direct{JDIR}/cigrdin","nadcalc");
# for details see
#  J. Chem. Phys. 120 (2004) 7322
#  J. Chem. Phys. 120 (2004) 7330
 if ($nad{nnadcoupl}) {
   my ($nadtype);
  $nadtype[1]="DCI";$nadtype[2]="DCSF";$nadtype[3]="DCI+DCSF";
    if ($nad{nadcalc}==0)
     {
      print "\n\n   ERROR:\n";
      print " According to control.run file you want to perform:\n";
      print " Nonadiabatic coupling term calculation!!!\n";
      print " input error in file: cigrdin, flag nadcalc not set!\n";
      print " ... Please set this flag or run \"Set up job control\" in colinp ...\n\n\n";
      die   "nadtype: input error!\n";
     }
#
  print "\n Nonadiabatic coupling terms of type: $nadtype[$nadcalc]\n";
  print "   $nad{nnadcoupl} transitions involved: \n";
  for ($i=1;$i<=$nad{nnadcoupl};$i++)
   { print "   DRT${$nad{nad_drt1}}[$i],state${$nad{nad_state1}}[$i];  DRT${$nad{nad_drt2}}[$i],state${$nad{nad_state2}}[$i]\n";}
  print "\n";
  }
  print "$nad{ngrad} gradients will be computed:\n";
  for ($i=1;$i<=$nad{ngrad};$i++) { print " i=$i   DRT#",${$nad{grad_drt}}[$i],"  state#",${$nad{grad_state}}[$i],"\n";}
  print "\n";
#
}



##############################################################################################################################

sub control_section2
 {
 if ($debug{debug_runc}) { print " executing sub control_section2 \n";} 
  if ( ! (grep (/GRAD/,$kwords{LEVEL}))) {
    print "\n\n============== timings ===================\n\n";
    print $clock{wallreport};
    print "\n==========================================\n\n";
    exit 0 ;
  }
  if ( $control{chaini} ) {print "exiting from chaini\n";exit 0;}
  if ( $control{chains} >= 2 ) {die("exiting chains $control{chains}\n");}
  chdir $direct{list};
  $MARKER="<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n";
  while ( <*\.sp>)
  { s/\.sp//g;
    local($xx);
 #    if (/ciudgls|ciudgsm|cipcls/ && ($kwords{do_ci}+$kwords{do_aqcc}+$kwords{do_acpf}))
 #     { # replace ciudgls.ci.drt1.sp bei ciudgls.drt1.sp
    $xx=$_;
    &appendtofile("$xx.sp","$xx.all",$control{iter});
    unlink("$xx.sp");
    if ($debug{debug_runc}) {printf "info: $xx.sp appended to $xx.all\n"; }}

  chdir $direct{modir};
  while (<*\.sp>)
  { s/\.sp//g;
    cp("$_.sp","$_.$control{iter}");
    if ($debug{debug_runc}) {printf "info: $_.sp renamed to $_.$control{iter}\n"; }}

  chdir $direct{molden};
  while (<*\.sp>)
  { s/\.sp//g;
    cp("$_.sp","$_.$control{iter}");
    if ($debug{debug_runc}) {printf "info: $_.sp renamed to $_.$control{iter}\n"; }}

  chdir $direct{rest};
  while (<*\.sp>)
  { s/\.sp//g;
    cp("$_.sp","$_.$control{iter}");
    if ($debug{debug_runc}) {printf "info: $_.sp renamed to $_.$control{iter}\n"; }}


    if ($control{iter} == $control{start_iter})
    { delete $kwords{"scf"};
      $wherami=999;
      for ($w=0; $w<=@route+0;$w++)
        {   if ($route[$w] eq "scfpart") {$wherami=$w;}}
    if ($wherami != 999 ) {splice(@route,$wherami,1);}
   # printf "after delete scf route=@route\n";
    }
}
     
