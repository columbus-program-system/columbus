#!/usr/bin/env colperl
#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/


 $nbar=shift @ARGV;
$nresult=0;
@results=();
 while (@ARGV)
 { $filename=shift @ARGV;
   print "processing $filename \n";
   $ncpu=$filename;
   $ncpu=~s/TASKLIST\.//;
   $ncpu=~s/cpu.*$//;
   $nresult++;
   $results[$nresult][0]=$ncpu;
   print "processing $filename ncpu=$ncpu \n";
 open FTSK,"<$filename" || die("could not open file $filename \n");

 $x=<FTSK>;
 $x=<FTSK>;
 chop $x;
 $ntotstatic=$x;
 $ntotstatic--;
 $tottask=$ntotstatic;
 $ maxtime=0;
 @acheck=();
 while (<FTSK>)
  { s/^ *//; chop;
    ($no,$bra,$ket,$type,$sl,$id,$time)=split(/\s+/,$_);
      if ($id > 0) { $acheck[$id]=1; $times[$id]=$time; if ($maxtime < $time) {$maxtime=$time;}}
        else       { $tottask++; $times[$tottask]=$time;$acheck[$tottask]=1; if ($maxtime < $time) {$maxtime=$time; }}
  }
close FTSK;
print "total number of tasks=$tottask\n";

$range=int((int($maxtime)+$nbar+1)/($nbar));

printf " MAXTIME= %8.3f sec nbar=%5d  range= %8.3f \n", $maxtime, $nbar, $range;

@histogram=();

for ($i=1; $i<=$tottask; $i++) 
  { $n=$times[$i]/$range; $n++; $histogram[$n]++;}

$checksum=0;
for ($i=0; $i< int($maxtime/$range)+1; $i++)
  { $checksum=$checksum+$histogram[$i+1]; 
    printf "range  %8.3f sec - %8.3f : %6d \n", $i*$range, ($i+1)*$range, $histogram[$i+1];  $results[$nresult][$i+1]=$histogram[$i+1];
#   print "adding", $histogram[$i+1] ," to $nresult,",$i+1,"=",$results[$nresult][$i+1], "\n"; 
    }
if ($checksum != $tottask) {print(" WARNING invalid checksum $checksum != $tottask\n");}
 for ($i=1; $i<=$tottask; $i++) { if ( $acheck[$i] == 0 ) { printf "ID=$i missing\n";}}
 }



 printf "  ncpu      .... ranges ....\n";
 for ($i=1; $i<=$nresult; $i++) 
  { printf " %5d",$results[$i][0]; 
    for ($j=1; $j<=$nbar; $j++) {  printf " %6d ", $results[$i][$j]; }
    print "\n";
  }


 


