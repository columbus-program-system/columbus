#!/usr/bin/env colperl
#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/


$ffile=shift @ARGV;

   $/="\n";
   open FIN,"<$ffile";
   open FNEW,">$ffile.new";
 $foundsub=0;
 $headerline=0;
   while (<FIN>) 
   { 
     if ($headerline &&  (! ((/^     \S/) || (/^[A-Za-z1-9]/)) ))   {$headerline=0;
                                                                     if (! /implicit/i) { $save=$_; }
                                                                     print FNEW "       implicit none \n"; 
                                                                     $putinfile=uc($foundsub).".putin"; 
                                                                      print "adding include file $putinfile \n";
                                                                       open FPUTIN,"<$putinfile"; 
                                                                      while (<FPUTIN>) { print FNEW;  }  close FPUTIN;  print FNEW $save; next; } 
     print FNEW; 
     if ( /^[A-Za-z1-9]/ || (/^     \S/) ) {  next;} 
     if ( /(subroutine |function )/i ) { $foundsub=$_; chop $foundsub; $foundsub=~s/^.*(subroutine |function )//i; $foundsub=~s/^ *//; $foundsub=~s/[ (].*$//; 
                                         print "found subroutine $foundsub\n"; $headerline=1; }
   } 
  close FIN;
 exit;
  
@tmparray; }

   { my @tmparray=();
    for ($i=0; $i<=$#content2; $i++)
     { $_=$content2[$i];
       if (!(/^ *$/ || /^$/)) { push @tmparray,$_;}}
     @content2=@tmparray; }

    if ($DEBUG) {print "CONTENT1=$#content1  CONTENT2:$#content2\n"; }
 

     $endfile=1;
     while (<FCONFIG>)
        { if (/^#/) {print ; next; }
          if (/^ +$/ || /^$/ ) {next; }
          if (/^%/) {chop; $filestring=$_;$endfile=0;last;}
          chop; $patternstring=$_; ($pattern,$substitution,$tolerance,$blockstart,$blockend)=split(/!/,$patternstring);
          $pattern=~s/ *$//; $pattern=~s/^ *//; 
          $blockstart=~s/^ *//; $blockstart=~s/ *$//;
          $blockend=~s/^ *//; $blockend=~s/ *$//;
          $substitution=~s/^ *//; $substitution=~s/ *$//;
          $tolerance=~s/[dD]/e/;
          if ($blockstart) { print "pattern $pattern  item# $substitution  tolerance= $tolerance blockstart=$blockstart blockend=$blockend \n"; }
                   else        {  print "pattern $pattern  item# $substitution  tolerance= $tolerance \n";}
          if (grep(/|/,$tolerance)) { $magnitude=1; $tolerance=~s/\|//g; } else {$magnitude=0;}

          $pattern=~s/^'//; $pattern=~s/'$//;
          $blockstart=~s/^'//; $blockstart=~s/'$//;
          $blockend=~s/^'//; $blockend=~s/'$//;


          $substitution=~s/\D/ /g;
          @itemnos=split(/\s+/,$substitution);

#
#      blockstart:  SOF   = start of file
#      blockend  :  EOF   = end of file 
#
#      first select block range (including delimitter) 
#     second apply pattern to block range
#
       if ($DEBUG) { print "blockstart:$blockstart blockend:$blockend\n";
                     print "CONTENT1=$#content1  CONTENT2:$#content2\n"; }
       if (($blockstart eq 'SOF' && $blockend eq 'EOF' ) || ($blockstart eq '' && $blockend eq '')) 
              { if ($DEBUG) { print "BLOCK SOF/EOF\n"; }
                @lines1=grep (/$pattern/,@content1); 
                @lines2=grep (/$pattern/,@content2);} 
       elsif ($blockstart eq 'SOF' )
              { if ($DEBUG) { print "BLOCK SOF\n";}
                 my @tmplines1=(); my  @tmplines2=();
                for ($i=0; $i<=$#content1;$i++ )
                  { push @tmplines1,$content1[$i];  
                    if ( grep (/$blockend/,$content1[$i])) { last;}}
                for ($i=0; $i<=$#content2; $i++ )
                  { push @tmplines2,$content2[$i];   
                    if ( grep (/$blockend/,$content2[$i])) {last;}}
                @lines1=grep (/$pattern/,@tmplines1); 
                @lines2=grep (/$pattern/,@tmplines2); 
               }
       elsif ($blockend eq 'EOF' ) 
            {  if ($DEBUG){ print "BLOCK EOF\n"; }
               my @tmplines1=(); my @tmplines2=();
              for ($i=$#content1; $i>0; $i--  ) 
              { push @tmplines1,$content1[$i];   
                if ( grep (/$blockstart/,$content1[$i])) {last;}}
              for ($i=$#content2; $i>0; $i--  )
              { push @tmplines2,$content2[$i];                 
                if ( grep (/$blockstart/,$content2[$i])) {last;}}
                @lines1=grep (/$pattern/,@tmplines1); 
                @lines2=grep (/$pattern/,@tmplines2); 
             }

       else  { if ($DEBUG) { print "BLOCK $blockstart:$blockend\n"; }
                 my @tmplines1=(); my  @tmplines2=();
                 $addlines=0;
                  for ($i=0; $i<=$#content1; $i++ )
                  { if ( grep (/$blockstart/,$content1[$i])) { $addlines=1; }
                    if ($addlines) {push @tmplines1,$content1[$i];}
                    if ( grep (/$blockend/,$content1[$i])) { push @tmplines1,$content1[$i]; last;}}

                 $addlines=0;
                  for ($i=0; $i<=$#content2; $i++ )
                  { if ( grep (/$blockstart/,$content2[$i])) { $addlines=1; }
                    if ($addlines) {push @tmplines2,$content2[$i];}
                    if ( grep (/$blockend/,$content2[$i])) { push @tmplines2,$content2[$i]; last;}}

                   @lines1=grep (/$pattern/,@tmplines1);
                   @lines2=grep (/$pattern/,@tmplines2);
               }


#
#
#
          if ($DEBUG) { print " CONTENT1:", $#lines1,"\n",@lines1;
                        print " CONTENT2:",$#lines2,"\n", @lines2; }
           if (($#lines1 != $#lines2)  || $#lines1 < 0  ) {  print " CONTENT1:", $#lines1,"\n",@lines1;
                                                              print " CONTENT2:",$#lines2,"\n", @lines2; 
#fp                                                            print " ... ignoring this check "; next; }         
                                                    print " ... files could not be compared!\n"; $FAILED=1; next; }

           for ($imatch=0; $imatch<=$#lines1 ; $imatch++)
            { 
#              print "extracting item # $substitution\n";
               chop $lines1[$imatch]; chop $lines2[$imatch];
               $lines1[$imatch]=~s/^ +//g; 
               $lines2[$imatch]=~s/^ +//g; 
               $lines1[$imatch]=~s/[Dd]/e/g; 
               $lines2[$imatch]=~s/[Dd]/e/g; 
#              print "$lines1[$imatch] \n $lines2[$imatch] \n";
               @item1=split(/\s+/,$lines1[$imatch]);
               @item2=split(/\s+/,$lines2[$imatch]);
               for ($isubst=0; $isubst<=$#itemnos; $isubst++) 
                { $substitution=$itemnos[$isubst]; if ($substitution >$#item1) {next;}
           #print $item1[$substitution]," - ", $item2[$substitution]," =", $item1[$substitution] - $item2[$substitution],"\n";
              if ($magnitude) {$item1[$substitution]=abs($item1[$substitution]); 
                               $item2[$substitution]=abs($item2[$substitution]);}
           if (abs($item1[$substitution] - $item2[$substitution]) > $tolerance) 
                       {$FAILED=1; 
                         print " test failed($imatch): $item1[$substitution] versus $item2[$substitution] "; }
            else { if ($isubst eq 0) {print " test passed ($imatch): $item1[$substitution]"; $numtests=$numtests+1;} else {print " $item1[$substitution] " ;}
                }
            }
              print "\n";
            }
          }
     if ($endfile) {last;}
  }

   if ($FAILED) { print "*********** SOME TESTS FAILED ************\n";}
        else    { print "*********** ALL TESTS PASSED ($numtests) *************\n";}

              


