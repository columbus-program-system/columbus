package  runc_utilities;
use Exporter();
@ISA       =qw(Exporter);
@EXPORT    =qw(process_commandline process_env  process_controlrun getmcscfinfo getciinfo printsummary
               mocoef_check read_control_run setdir check_inputfiles callprog analyse_output appendtofile append outputheader get_energydata put_energydata 
               createmcpcin tail cutci geom getcienergy transfgrad dxcalc protocol4nad gendaltonunique setup_ordints distribute file_output effden_tran call_cigrd);
use runc_globaldata;
use colib_perl;
use File::Copy 'cp';  # use perl cp
use Cwd;
use Shell qw(pwd cd date mv cat);


#======================================================================
# check whether mo coefficient files are available
#======================================================================

sub get_energydata{        # structure of energy file must be changed !
                           #**** ROOTS ****
                           #     1        -25.75839368
                           #     2        -25.71635717
                           #**** TRANSITION ****
                           #     1     1     1     2
                           #      -25.75839368        -25.71635717         -0.04203650
  my (@eflines,$roots,$transition,$ntrans,$i,$r,$e,$drt1,$drt2,$state1,$state2,$e1,$e2,$de);
  my ($temp,$cnt);
  ($cnt) =@_;
   if ($debug{debug_runc}) { print " executing sub get_energydata \n";}
  open (EFILE,"energy");
  @eflines=<EFILE>;
  close EFILE;
  $roots=0;
  $nroots=$efile{nroots};
  $ntrans=$efile{ntransitions};
    while ($#eflines>-1) 
     { $_=shift @eflines;
     if (/ROOTS/ ) { $roots=1; next;}
     if (/TRANSITION/ ) { $roots=0; next;}
     if ($roots) 
      { chop; s/^\s+//; s/ *$//; ($r,$e) = split(/\s+/,$_); 
        $nroots++;
        ${$efile{roots}}[$nroots]=join(':',($cnt,$r,$e));  next;}
     else
      { chop; s/^\s+//; s/ *$//; ($drt1,$state1,$drt2,$state2) = split(/\s+/,$_);
       $_=shift @eflines;
       chop; s/^\s+//; s/ *$//; ($e1,$e2,$de) = split(/\s+/,$_);
        $ntrans++;
        $temp=join(':',($drt1,$state1,$drt2,$state2,$e1,$e2,$de));
        ${$efile{transition}}[$ntrans]=$temp;
        next;}
    }
  $efile{nroots}=$nroots;
  $efile{ntransitions}=$ntrans;

#debug

# for ($i=1; $i<=$efile{nroots}; $i++) 
#  {print "DRT:ROOT:ENERGY=",${$efile{roots}}[$i],"\n";}
# for ($i=1; $i<=$efile{ntransitions}; $i++) 
#  {print "TRANSITION:DRT1:STATE1:DRT2:STATE2:ENERGY1:ENERGY2:DE=",${$efile{transition}}[$i],"\n";}
  
}

sub put_energydata{
  my ($spec,$info,$fname) =@_;
  my ($tmp1,@tmp);
   if ($debug{debug_runc}) { print " executing sub put_energydata \n";}
   open EGDIIS,">$fname";
  if ($info eq "TRANS") { # $spec=drt1:state1:drt2:state2 
                          my ($i);
                          for ($i=1; $i<=$efile{ntransitions}; $i++)
                              {@tmp=split(/:/,${$efile{transition}}[$i]);
                               $tmp1=join(':',@tmp[0,1,2,3]);
                               print "comparing $tmp1|$spec\n"; 
                              if ($tmp1 eq $spec) { print EGDIIS " $tmp[4]  $tmp[5]  $tmp[6] \n";
                                                    $nad{energy1}=$tmp[4]; $nad{energy2}=$tmp[5]; $nad{deltae}=$tmp[6]; last;}}
                         }      
  if ($info eq "ROOTS") { # $spec=drt1:state1
                          my ($i);
                          for ($i=1; $i<=$efile{nroots}; $i++)
                              {@tmp=split(/:/,${$efile{roots}}[$i]);
                               $tmp1=join(':',@tmp[0,1]);
                              if ($tmp1 eq $spec) { print EGDIIS " $tmp[2] \n"; $grad{energy}=$tmp[2];last;}}
                         }      
   close EGDIIS;

#  print "put_energydata: $fname \n"; 
# system("cat $fname");

}

sub process_commandline{

#----------------------------------------------------------------------------
# ------------------- evaluate command line arguments -----------------------
#----------------------------------------------------------------------------
   if ($debug{debug_runc}) { print " executing sub process_commandline \n";}
  $i=0;
  while ( $i <= $#ARGV )
  { $_=$ARGV[$i];
    if (/-m$/) { $i++; $control{pcoremem} = $ARGV[$i]; $i++;}
    elsif (/-msmp/) { $i++; $control{coremem} = $ARGV[$i]; $i++;}
    elsif (/-olddensity/) { $control{USENEWCIDENSITY}=0; $i++;}
    elsif (/-oldcigrd/) { $control{USENEWCIGRD}=0; $i++;}
    elsif (/-chainp/) { $control{chainp}=1; $i++;}
    elsif (/-chains/) { $control{chains}=1; $i++;}
    elsif (/-chaini/) { $control{chaini}=1; $i++;}
    elsif (/-nproc/) { $i++; $control{nproc_nl}=$ARGV[$i]; $i++;}
    elsif (/-pmolcas/) { $i++; $control{pmolcas}=1; }
    elsif (/-machinefile/) { $i++; $control{machinefile_nl}=$ARGV[$i]; $i++;}
    elsif (/-debug/) { $debug=1;$i++;
      print "Debug mode called\n";

      $fl=substr($ARGV[$i],0,1);
      #print "$i $#ARGV $ARGV[$i] $fl\n";
      # check if a bitvector was given
      if ($i > $#ARGV || $fl eq '-')
      {
        print "producing debug for all program steps (no debug bitvector given)\n";
        @bitvec = split(//,"111111111");
      }
      else
      {
        print "using custom debug bitvector $ARGV[$i]\n";
        @bitvec = split(//,$ARGV[$i]);
        $i++;
      }
      if ($bitvec[0]=="1") {$debug{debug_runc}=1; print "Debug: runc\n";}
      if ($bitvec[1]=="1") {$debug{debug_geo}=1; print "Debug: geometry\n";}
      if ($bitvec[2]=="1") {$debug{debug_integ}=1; print "Debug: integrals\n";}
      if ($bitvec[3]=="1") {$debug{debug_scf}=1; print "Debug: SCF\n";}
      if ($bitvec[4]=="1") {$debug{debug_mcscf}=1; print "Debug: MCSCF\n";}
      if ($bitvec[5]=="1") {$debug{debug_ci}=1; print "Debug: MR-CI\n";}
      if ($bitvec[6]=="1") {$debug{debug_dens}=1; print "Debug: Density matrices\n";}
      if ($bitvec[7]=="1") {$debug{debug_deriv}=1; print "Debug: Gradients/Couplings\n";}
      if ($bitvec[8]=="1") {$debug{debug_par}=1; print "Debug: Parallel runs\n";}
    }
    elsif (/-help/) { print "usage: runc [-m memory_in_MB]               memory in MB (per process)\n",
                            "            [-msmp mem_in_MB]               memory per SMP node for mixed sequ./par. calculations\n",
                            "            [-nproc ncpu ]                  number of processors \n",
                            "            [-machinefile machinefile]      hostlist (optionally) \n",
                            "            [-olddensity ]                  old density code     \n",
                            "            [-oldcigrd]                     old cigrd (frozen core through step vectors) \n",
                            "            [-chaini]                       terminate initial startup just before ci code \n",
                            "            [-chainp]                       continue previous calculation with -chaini or -chains option\n",
                            "                                            and terminate just after parallel section \n",
                            "            [-chains]                       continue  with serial portion up to next call to the CI code \n",
                            "            [-pmolcas]                      use parallel molcas where applicable \n",
                            "            [-debug [bitvec]]               raise the print level and give additional debug information \n",
                            "      bitvector: (runc, geometry, integrals, SCF, MCSCF, MR-CI, Dens. Mat., Grad./Coupl., Parallel)\n",
                            "      e.g. -debug 101001011\n";
                            die("terminating\n");}
    else { printf "argument $ARGV[$i] ignored!\n";$i++;}
  }

  if ($control{coremem} == 0) {$control{coremem} = $control{pcoremem};}
  $_=$control{pcoremem}; if ( grep(/\D/,$control{pcoremem})) { die "invalid -m option $control{pcoremem} \n";}
  if ($control{pcoremem}>100000) {die("changed memory specification to MB unit!\n");}
  printf "coremem. (seq.): %d MB,  coremem. (par.): %d MB\nVdisk size: %d DP\n", $control{coremem}, $control{pcoremem}, $vdisk;
  if ($debug{debug_par}) {printf "CHAINP: %1d CHAINS: %1d CHAINI %1d \n", $control{chainp}, $control{chains}, $control{chaini};}
 }


  sub process_env {

   if ($debug{debug_runc}) { print " executing sub process_env \n";}
  if ($ENV{MOLDYN_PAR})
   { print " found MOLDYN_PAR $ENV{MOLDYN_PAR} \n";
     ($arg1,$arg2,$arg3,$arg4) = split(/\s+/,$ENV{MOLDYN_PAR});
     if ( $arg1 eq "-nproc" ) { $nproc_nl=$arg2;}
     if ( $arg3 eq "-machinefile" ) { $machinefile_nl=$arg4;}
     print "found nproc=$nproc_nl   machinefile=$machinefile_nl \n";
   }
# DALTON work memory
  $ENV{"WRKMEM"}=$control{coremem} ;
# ARMCI
   $ENV{"RT_GRQ"}="ON";

#
# For Intel/MKL based applications it is sensible to suppress multi-threading
#   ... but as usual Intel tries to be overly clever in selling its products 
#       and mkl does not necessarily responds to the setting
#
   $ENV{"OMP_NUM_THREADS"}="1";
   $ENV{"MKL_DYNAMIC"}="FALSE";
   $ENV{"MKL_NUM_THREADS"}="1";
#  make sure that  LD_LIBRARY_PATH is set appropriately, if necessary

 $installconfig=colib_perl::analyse_installconfig($ENV{"COLUMBUS"}."/install.config");

# printf "Reading job control file control.run ... ";
# &read_transmomin;
#%kwords = &read_control_run();

 if ($$installconfig{"MOLCAS"})
  {  # update environment

#
# It is sufficient to have Columbus linked against the serial libmolcas.a
#  (actually it is more convenient, since there are no links to GAs here)
# If MOLCAS is set to a parallel MOLCAS installation, then automatically
#  all molcas calls are parallel
#  here we support explicitly only the molcas modules
#           seward, alaska, rasscf, caspt2, ccsd, ccsdt
#  by looking for PARALLEL='yes' in $MOLCAS/Symbols we can sort out
#  whether it is a parallel or serial molcas installation
#  if parallel,

     $ENV{"SMOLCAS"}=$$installconfig{"MOLCAS"};
     print "SMOLCAS=$ENV{SMOLCAS}\n";
     $ENV{"PMOLCAS"}=$$installconfig{"PMOLCAS"};
     if ($debug{debug_par}) {print "PMOLCAS=$ENV{PMOLCAS}\n";}
# MOLCAS work memory
#fp if ($control{coremem} > 200000000 ) { $ENV{"MOLCASMEM"}=int(200000000/131072);}
#   else { $ENV{"MOLCASMEM"}=int($control{coremem}/131072);}
   $ENV{"MOLCASMEM"}=$control{coremem}; #int($control{coremem}/131072);
   if ($debug{debug_runc}) {print "MOLCASMEM=$control{coremem}\n";}
   if ($control{pmolcas}) { $kwords{"pmolcas"}=0;
                   open FSYMBOL,"<$ENV{PMOLCAS}/Symbols";
                   while (<FSYMBOL>) {  if ((/PARALLEL=/) && (/yes/)) { $kwords{"pmolcas"}=1; last;}}
                   close FSYMBOL;
                   if ($kwords{"pmolcas"}==1 ) {print "Found parallel Molcas Installation at ",$ENV{"PMOLCAS"},"\n"; $ENV{"MOLCAS"}=$ENV{"PMOLCAS"};}
                   else { die " specified -pmolcas on command line but no parallel MOLCAS available \n";}
                 }
   else     {print "Found serial Molcas Installation at ",$ENV{"SMOLCAS"},"\n";$ENV{"MOLCAS"}=$ENV{"SMOLCAS"};}
   $ENV{"Project"}="molcas";
   $ENV{"WorkDir"}=$work;
   $ENV{"ThisDir"}=$direct{JDIR};
  }
  else {$control{pmolcas}=0; undef $kwords{"pmolcas"};}
}

sub process_controlrun{

 if ($debug{debug_runc}) { print " executing sub process_controlrun \n";}
 &getmolinfo ; 
 &getdrttranslation;
 &read_transmomin;
 &read_control_run();


# for parallel run check that nproc /machinefile/hosts is set

  if ($kwords{"pciudg"} || $kwords{"pciden"} || $kwords{"pmolcas"} )
         { print "parallel CI calculation specified ... \n";
#fp: I think something like this makes more sense
           if ( $control{nproc_nl} )
              {print "info: commandline option -nproc overrides nproc statement in control.run \n";
              $kwords{"nproc"}=$control{nproc_nl};}
           if ( $control{machinefile_nl} )
              {print  "info commandline option -machinefile overrides machinefile statement in control.run \n";
              $kwords{"machinefile"}=$control{machinefile_nl}}
           if ( ! ($kwords{"nproc"})  )
           {
              if ($control{chaini}){
                print "Performing preparation step (chaini)\n";
              }
              else
              {die("either use control.run options to specify number of processors" .
                   " and hosts or machinefile \n ".
                   " or use the commandline options -nproc xx -machinefile yy \n");}
           }
         }

# for MOLCAS
# note due to integral access and performance issues, restrict MOLCAS to at most 8 processes
  if ($kwords{"nproc"}>$MAXCPU_MOLCAS) { $ENV{CPUS}=$MAXCPU_MOLCAS; print " .... info: limiting MOLCAS parallelism to at most $MAXCPU_MOLCAS processes \n";}
                else  { $ENV{CPUS}=$kwords{"nproc"};}
#


  if ($kwords{"cosmomcscf"}) { printf "This is a COSMO mcscf run !\n";}
  if ($kwords{"cosmoci"}) { printf "This is a COSMO ci run !\n";}
  if ($kwords{"pcigrad"}) { $kwords{"cigrad"}=1; $kwords{"pcigrd"}=1; }

   open INTPRG, "<$direct{JDIR}/intprogram";
   $intprogram=<INTPRG>;
   chop $intprogram;
   $kwords{"intprogram"}=$intprogram;
   close INTPRG;


}



sub mocoef_check {
  my ($sci_x,$ci_x,$scf_x);

  if ($debug{debug_runc}) { print " executing sub mocoef_check \n";}
  if ($debug{debug_runc}) {
     print "Keys present in mocoef_check:\n";
     foreach $i  ( keys(%kwords) ) { print "key $i = $kwords{$i}\n";}
  }

  $sci_x= exists($kwords{"pciudg"} );
  $ci_x = exists($kwords{"ciudg"}) +  $sci_x ;
  $scf_x= exists($kwords{"scf"}) + exists($kwords{"mcscf"}) + exists($kwords{"dft"});

  #   $direct{modir}="$direct{JDIR}/$MODIR";

  if ( (! $ci_x ) && exists($kwords{"ciprop"}) ){
    local (@tmp);
    @tmp=<${modir}/nocoef_ci.*.sp> ;
    if (! $#tmp) {
      $rosebud="keyword combination ciprop without ciudg";
      $rosebud="$rosebud but cannot find $direct{modir}/nocoef_ci.?.sp.\n";
      die $rosebud;#matruc_25jun07
    }
  }


  if ((!exists($kwords{"mcscf"}))&&exists($kwords{"mcscfprop"})&&(!-s "$direct{modir}/nocoef_mc.sp")){
    $rosebud="keywords combination mcscfprop without mcscf";
    $rosebud="$rosebud but cannot find $direct{modir}/nocoef_mc.sp.\n";
    die $rosebud;#matruc_25jun07
  }

  if ((!exists($kwords{"scf"}))&&exists($kwords{"scfprop"})&&(!-s "$direct{modir}/mocoef_scf.sp")){
    $rosebud="keyword combination scfprop without scf";
    $rosebud="$rosebud but cannot find $direct{modir}/mocoef_scf.sp.\n";
    die $rosebud;#matruc_25jun07
  }

  if ((! $scf_x) && $ci_x  && (! -s "$direct{JDIR}/mocoef" )) {
    $rosebud= " keyword combination ci without scf or mcscf";
    $rosebud="$rosebud but cannot find MO coefficient file in $direct{JDIR}";
    $rosebud="$rosebud please the correct MO coefficient file from the";
    $rosebud="$rosebud MOCOEFS subdirectory to $direct{JDIR}!";
    $rosebud="$rosebud exiting ... \n ";
    print $rosebud;
    die $rosebud;#matruc_25jun07
  }

  if ( exists($kwords{"mcscf"}) && ! exists($kwords{"scf"})) {
    printf "\nmcscf calculation without preceeding scf calculation \n";
    if ( $kwords{"transmo"} )
    {
      print "will convert mocoef.high to starting MOs of the current symmetry\n";
    }
    elsif ( -s "$direct{WORK}/mocoef" ){
      printf "will use \n $direct{JDIR}/$direct{WORK}/mocoef as starting MOs \n";
    }
    elsif ( -s "$direct{JDIR}/mocoef" ){
      printf "will use \n $direct{JDIR}/mocoef as starting MOs \n";
    }
    else{
#fp: this check should only be performed with the appropriate flags in mcscfin
      $rosebud = "cannot find starting MOs \n please copy them into the main directory\n";
      $!=10;
      print $rosebud;
      die $rosebud;
    }
  }

  if (exists($kwords{"turbocol"})){
    if(exists($kwords{"scfprop"})||exists($kwords{"mcscfprop"})){
      if(!-s "$direct{JDIR}/cidrtin"){
        $!=10;
        $rosebud="need cidrtin file for property calculation via turbocol";
        $rosebud="$rosebud prepare some cidrtin file with colinp \n";
      }
    }
  }#matruc_25jun07 changed one overlong line to nested if
}

############################################################################################

 sub read_control_run {
#
# read the keywords from control.run, analyse them including niter
# and return them in the associative array
# %kwords(keyword)=occurency
#


  local ($a,$foundniter,$niter,$w,@keywords);
  if ($debug{debug_runc}) { print " executing sub read_control_run \n";}

  open(CONTROLRUN,"control.run") || die " Cannot open control.run, errno=$!
  run the interactive program colinp first.\n";

  $/="";
  $a =<CONTROLRUN> ;
  @keywords = split(/\n/,$a); # aufteilen in einzelne Zeilen
  close(CONTROLRUN);

# optimize=1^1A1                 ==> GEOMOPT
# optimize=1^1A1 -> 2^1A1        ==> MXS
# optimize=1^1A1 -> 1^3A1        ==> MXS-no-h

# dynamic=1^1A1,2^1A1 1^1B1     ===> gradients & energy for each state are computed
#                                    NADs for transitions between states of alike symmetry and multiplicity
#                                    are computed

  foreach ( @keywords ){
    $w=$_;
# special keywords
    if (/^nproc/i) { $w=~ s/^.*=//g; $w=~ s/ //g; $kwords{"nproc"}=$w; next; }
    if (/^hosts/i) { $w=~ s/^.*=//g;$w=~ s/ //g; $kwords{"hosts"}=$w; next; }
    if (/^machinefile/i) { $w=~ s/^.*=//g;$w=~s/ //g; $kwords{"machinefile"}=$w; next; }
    if (/^niter/i) { s/\D//g; s/ //g; $kwords{niter}=$_; next;}
    if (/^optimize/i) {s/^.*=//; my (@root,@state,$i) ; @root=split(/->/,$_);    # optimize=1^2A1 or optimize=1^1A1->2^1A1 
                       for ($i=0; $i<=$#root; $i++)  
                        { $state[$i]=$root[$i]; 
                          $state[$i]=~s/^\d+//; $root[$i]=~s/\^.*$//; } 
                       if ($state[1]){ if ($state[0] eq $state[1]) 
                                             {print "found MXS ($state[0]) from state $root[0] to $root[1]\n";
                                              $kwords{optimize}=join(':',("MXS",$state[0],$root[0],$root[1]));}
                                             else 
                                             {print "found MXS-no-h from state $root[0]:$state[0] to $root[1]:$state[1]\n";
                                              $kwords{optimize}=join(':',("MXS-no-h",$state[0],$root[0],$state[1],$state[0]));
                                              my ($mult1,$mult2); $mult1=$state[0]; $mult1=~s/\w.*$//; 
                                               $mult2=$state[1]; $mult2=~s/\w.*$//; 
                                              # same multiplicity -> cidrtmsin possible , ommitting NAC invalid 
                                              # unlike multiplicity -> cidrtin.1,cidrtin.2 , ommitting NAC in absence of SOCI valid. 
                                              if ($mult1 eq $mult2) {print "same multiplicity different symmetry:\n";
                                                                     print "ommitting NACs is an approximation !\n";}}}
                       else {print "found geometry optimization for $root[0] : $state[0]\n";
                              $kwords{optimize}=join(':',("GEOMOPT",$state[0],$root[0]));} # GEOMOPT
                       next;}
     if (/^dynamic/i) { s/^.*=//; my (@states,@root,@multsym,@drt,$i,$n) ; @states=split(/,/,$_);  
                        ${$kwords{dynamic}}{nstate}=$#states+1;
                        for ($i=0; $i<=$#states; $i++) {$root[$i]=$states[$i]; $multsym[$i]=$states[i]; 
                                                  $multsym[$i]=~s/^\d+//; $root[$i]=~s/\^.*$//;
                                                  $drt[$i]=${${translation}{cidrt}}{$multsym[$i]};
                                                  ${$kwords{dynamic}{states}}[$i+1]=join(':',($drt[$i],$root[$i],$multsym[$i])); 
                                                  print "found dynamic state #",$i+1," = ",${$kwords{dynamic}{states}}[$i+1],"\n";
                                                 } 
			       # now modifiy the %cigrad, %citrans entries 
                        $cigrad{n}=0;
                        $citrans{n}=0;
                        for ($i=0; $i<=$#states;$i++) 
                           { $cigrad{n}++; ${$cigrad{drt}}[$cigrad{n}]=$drt[$i]; ${$cigrad{root}}[$cigrad{n}]=$root[$i]; 
                            for ($j=0; $j<$i;$j++)
                            {  if ($drt[$i] == $drt[$j] ) { $citrans{n}++; 
                                                           ${$citrans{eintrag}}[$citrans{n}]=join(':',($drt[$i],$root[$i],$drt[$j],$root[$j]));}
                            }} 
                       next;}
# generic keywords
    $w =~ s/ //g; $w =~ tr/A-Z/a-z/;
    $kwords{$w}++;
  }
  if ( ! $kwords{niter} ) {$kwords{"niter"}=1;}
  if ( $kwords{optimize} && $kwords{niter} == 1 ) { print "add \"niter = <geom_cycles>\" to control.run: current value of geom_cycles=1\n";} 
  if ( ! $kwords{optimize} && $kwords{niter} > 1) { print "no optimization specified, resetting niter to 1\n"; $kwords{niter}=1;}

  # remove redundancy

  delete $kwords{"turbocol"};

  if ($kwords{"mcscfgrad"} + $kwords{"samcgrad"} + $kwords{"cigrad"} > 1 )
    { die "please select only one out of mcscfgrad, samcgrad, cigrad ... exiting\n";}

  if ( ($kwords{optimize} || $kwords{dynamic}) && $kwords{"argos"})
  { die "cannot use argos for energy gradients ... exiting\n";}

  if ($kwords{"nadcoupl"} && $kwords{"seward"})
  { print ("INFO: keywords nadcoupl and seward are supported for DCI mode only!\n");}

  # translate machinefile if environment variable
  if (exists($kwords{"machinefile"})){
    $_=$kwords{"machinefile"};
    if ( /^\$/ ) {
      print "found machinefile environment variable $_\n";
      s/^\$//;
      $kwords{"machinefile"}=$ENV{$_};
      print " setting machinefile to", $kwords{"machinefile"},"\n";
    }
  }
  return ;
}
##############################################################################################

#======================================================================
# create subdirectories in the JDIR directory
#======================================================================

sub setdir {
    local ( @subdirs,$w,$geomopt );
  $geomopt = $_[0];

  if ($debug{debug_runc}) {printf "setdir: geomopt=$geomopt\n";}
  @subdirs = ( "$direct{WORK}","$direct{MODIR}","$direct{LIST}","$direct{REST}","$direct{MOLDEN}","$direct{GRADDIR}");

  if ($geomopt){
    push @subdirs,"$direct{GEOMDIR}";
  } #matruc_25jun07
  else{
    push @subdirs, "$direct{COSMO}";
  } #matruc_25jun07

  if ($kwords{"rgf"} && ! -s "$direct{HESSIAN}") {push @subdirs, "$direct{HESSIAN}";}

  chdir $direct{JDIR};
  foreach $w ( @subdirs){
    if ( ! -e $w ) {
      # die ersetzen
      mkdir("$w",0755) or die "can not create subdirectory $w \n Errno= $! \n";
    }
    if ( ! -d $w ) {die " $w is no directory \n"; }
  }
  return @subdirs ;
}

######################################################################################


#======================================================================
# check for the availablity of all input files
#======================================================================


 sub check_inputfiles {

   local (%inpfiles,%kwords,$x,$w,@v,$nerr,$navst,$ndrt,$ndrtci,$loc);
   %kwords = @_ ;
   chdir $direct{"JDIR"};
  if ($debug{debug_runc}) { print " executing sub check_inputfiles \n";}

#
# just temporary

   if ( ! -f "daltcomm" && -f "hermitin") { system("mv hermitin daltcomm");}

   %inpfiles = (    argos   => "argosin:geom" ,
                    seward  => "molcas.input" ,
                    alaska  => "molcas.input" ,
                    hermit  => "daltcomm:daltaoin:geom",
                    transmo => "soinfo.high:mocoef.high",
                    ffield  => "fieldin",
                    scf     => "scfin" ,
                    mcscf   => "mcscfin:mcdrtin",
                    ciudg   => "cidrtin:tranin:cisrtin:ciudgin",
                    pciudg  => "cidrtin:tranin:cisrtin:ciudgin",
                    scfprop => "propin" ,
                    mcscfprop =>  "propin" ,
                    mcscfgrad => "tranmcdenin:abacusin",
                    samcgrad => "tranmcdenin:abacusin:cigrdin",
                    nadcoupl_mc => "transmomin",
                    nadcoupl_ci => "transmomin",
                    cigrad    => "trancidenin:abacusin:cigrdin",
                    gdiis => "intcfl:gdiisin",
                    ciprop  =>    "propin" ,
                    mcscfmom => "transmomin" ,
                    ciudgav => "cidrtmsin:tranin:cisrtin:ciudgin.x",
                    ciudgmom => "cidrtmsin:transmomin",
                    turbocol => "control.basis:geom" ,
                    elpot     => "elpotin",
                    potmat    => "elpotin:potential.xyz"  ); #matruc_25jun07


   $nerr=0;
   $ndrt=0;
   $ndrtci=0;
   foreach $w ( keys(%kwords))
   { if ( $w ne "niter" )
      {
       if ( grep (/ciudgav/,$w) ) { $w="ciudgav";}
       if ( exists($inpfiles{$w}) ) {
        @v = split (/:/, $inpfiles{$w});
       foreach $x ( @v )
       {
        if ( $x eq "mcdrtin" )
           { $loc=0;
# temporary fix
             if ( $ndrt == 1  && -s "mcdrtin" && ! -s "mcdrtin.1" )
               {cp("mcdrtin","mcdrtin.1");}
#
             while ( $loc < $ndrt )
              { $loc++;
                if (! -s "mcdrtin.$loc" )
               { ++$nerr; printf "\n mcdrtin.$loc missing\n";}
              }
           }
        elsif ( $x eq "mcscfin")
          { $/="\n";

            $navst=0;
            $ndrt =0;
            open(MCSCFIN,"mcscfin");
            while(<MCSCFIN>) {$ndrt+=/NAVST/i;
                              $navst+=/WAVST/i;}
            close(MCSCFIN);}
        elsif ( $x eq cidrtin ) 
           { if ( -s "cidrtin") {cp("cidrtin","cidrtin.1");} 
             my ($err); $err=0;
              for ($ndrtci=1; $ndrtci<=$kwords{ciudgndrt}; $ndrtci++)
              { if ( ! -s "cidrtin.$ndrtci") { print " cidrtin.$ndrtci is missing\n"; $err++;}} 
             if ($err) {die("terminating \n");}}
        elsif ( $x eq "ciudgin.x" )
           { $loc=0;
             while ( $loc < $ndrtci )
              { $loc++;
                if (! -s "ciudgin.drt$loc" )
               { ++$nerr; printf "\n ciudgin.drt$loc missing\n";}
              }
           }
        elsif ( $x eq "abacusin" )
           { if ( $kwords{hermit} && (! -s "abacusin") ) { printf "\n 1: $x missing \n"; ++$nerr;}}
        elsif ( $x eq "gdiisin" )
           { if ( ( ! $kwords{slapaf}) && (! -s $x ) ) { printf "\n 1: $x missing \n"; ++$nerr;}}
        elsif ( $x eq "intcfl" )
           { if ( (! $kwords{slapaf} )  && (! -s $x ) ) { printf "\n 1: $x missing \n"; ++$nerr;}}
        else
        {if (! -s $x ) { ++$nerr; printf "\n $x missing \n"; }}
       }
      }
     }
   }
   if ($ndrt == 0) {$ndrt++;}
   return ($nerr,$navst,$ndrt,$ndrtci);
   }
########################################################################################################

#======================================================================
# call a fortran program , check for successful execution
# and give some short summary of the output file
#======================================================================

sub callprog {
  local ($cbus,$errno,$x,$prog);
  local (@timing,$startt,$endt,$i,$wallt,$wallh,$wallm,$walls);

  # get user time for process and all child processes
#   $i=0;
#   undef(@timing);
#   @timing=times();
#   until(not defined($timing[$i]))
#   {
#     $startt += $timing[$i];
#     $i+=2;
#   }
  $clock{startt}=time();
   $errno=0;
   $cbus = $ENV{"COLUMBUS"};
   $prog = shift @_;  ;
   $x = $prog;
   $started="starting $x at " . date();
   $x =~ s/[\<\>-].*$//;
   $molcasrun = grep (/molcas/i,$x);
   print "$started\n";
#  print "callprog: kwords{pmolcas}=",$kwords{"pmolcas"},"\n";

   if ( $molcasrun ) { @x=split(/\s+/,$prog);
                       $x[1]=~s/molcas\.input\.//;
                       $started="starting $x[1] at %s " . date();
                       if ($x[1] eq "seward" && $kwords{pmolcas} )
                          {  $cnt=$kwords{nproc};
                             $prog="molcas molcas.input.seward > molcas.output.seward ";
                             while (<ORDINT*>) { unlink $_;}
                            print "$ENV{MOLCAS}/sbin/$prog  \n";
                            $errno= system("$prog  2>> $direct{JDIR}/runc.error");
                            if ($errno) {die("Execution of parallel seward failed\n"); }
                            # setting links
                           # system("find ./ -name molcas.OrdInt -print > ordintls");
                            &setup_ordints(1)
                          }
                       elsif ($x[1] eq "alaska" && $kwords{pmolcas} )
                          { system("find ./ -type d -print > dirs");
                            open ADIRS,"<dirs";
                             while (<ADIRS>) { chop; if (! /\w/ ) {next; }
############################################  besser  linken #################################
                              cp("molcas.RunFile","$_/molcas.RunFile");
                              chdir($_);
                              if ( -l "GAMMA" ) { unlink "GAMMA";}
                              if ( -l "GTOC" ) { unlink "GTOC";}
                              system("ln -s ../GAMMA GAMMA");
                              system("ln -s ../GTOC GTOC");
                               chdir("$work"); }
##############################################################################################
                            $cnt=$kwords{nproc};
                            $prog="molcas molcas.input.alaska > molcas.output.alaska ";
                            $errno= system("$prog  2>> $direct{JDIR}/runc.error");

                          }
                       else { if ($kwords{"pmolcas"}) { &setup_ordints(0);}
                              $errno= system("$prog 2>> $direct{JDIR}/runc.error");
                              if ($errno) {
                               if ( grep(/slapaf/,$prog) ) { open SLAPAF,"<molcas.output.slapaf" || die ("could not open molcas.output.slapaf");
                                                     $ok=0;
                                                     while (<SLAPAF>) {if (/convergence problem/i) {$ok=1;last;}}
                                                     close SLAPAF;
                                                     if ($ok) {print "slapaf error (convergence problem) silently ignored ...\n";}
                                                     else { die("Execution of $prog failed with errno $errno\n");}
                                                   }
                                           else    {die("Execution of $prog failed with errno $errno\n");}
                                    }
                             &setup_ordints(1);
                             }
                       $finished=" finished $x[1] at %s " . date();
                       analyse_output($x[1],$started,$finished,$molcasrun);
                       return;
                     }

    $pscript=$$installconfig{"PSCRIPT"};

    if (grep /t3e/i , $pscript)
    {
      print "mpprun -a  $cbus/$prog 2>> $direct{JDIR}/runc.error\n";
      system("mpprun -a  $cbus/$prog 2>> $direct{JDIR}/runc.error");
    }
    else
    {
       if ($debug{debug_runc}) {print "$cbus/$prog 2>> $direct{JDIR}/runc.error\n";}
      if ($prog eq "cidrt.x" && $kwords{localmrci})
        {system("$cbus/$prog -l 2>> $direct{JDIR}/runc.error");}
      else
       {system("$cbus/$prog 2>> $direct{JDIR}/runc.error");}
    }

    if (!-s "bummer") {$errno = $errno + 256 ;}

    open (BUMMER, "bummer") or die "cannot open file: bummer in $x \n";
    while ( <BUMMER> )
    { if ( ! /normal|warning/) {$errno = $errno + 256 ;}}
    close (BUMMER);

    if ($errno)
    {
      print "\n *** Error occured! ***\n";
      file_output("runc.error","$direct{JDIR}/runc.error");
      $!=11; die "Error occurred in $x errno=$errno \n";
    }
    unlink ("bummer");
    $finished=" finished $x at %s " . date();
#     $i=0;
#     undef(@timing);
#     @timing=times();
#     until(not defined($timing[$i]))
#     {
#       $endt += $timing[$i];
#       $i+=2;
#     }
    $clock{endt}=time();
    $wallt = $clock{endt}-$clock{startt};
#   if ($wallt < 0)
#   {
#      $clock{wallreport} .=sprintf "   %-15s   less than 2 secs. \n", $x;
#   }
#   else
    {
        $walls = $wallt%60;
        $wallt = ($wallt-$walls)/60;
        $wallm = $wallt%60;
        $wallt = ($wallt-$wallm)/60;
        $wallh = $wallt;
        $clock{wallreport} .= sprintf " * %-15s  walltime %4d:%02d:%02d\n", $x, $wallh, $wallm, $walls;
    }
    analyse_output($x,$molcasrun,$prog);
}

##################################################################################################################

#======================================================================
# give some short summary of the output file of the program $prog
#======================================================================

   sub analyse_output {

     local ( $prog,$molcasrun,$cline);
     $prog = shift @_;
     $molcasrun = shift @_;
     $prog =~ s/ //g ;
     $cline=shift @_;
     $cline=~s/^.*>//; 

     printf " -------------------- $prog ------------------------\n";
     if ( $prog eq "scf" && $molcasrun)
       { open (F,"<molcas.output.scf");
         while (<F>) { if (/ Convergence information/) { $_=<F>; s/iterations:.*$//; print "Functional:", $_;last;}}
         while (<F>) { if (/ energy/) {print $_;} if (/ Max non-diagonal/) {last;}}
         close F;}

##################### SEWARD   #########################################
     if ( $prog eq "seward")
       { open (F,"<molcas.output.seward");
         $cntr=0;
         while (<F>)
         {if (/Basis set label/i || $cntr || /Nuclear Potential energy/i  ) {print;}
          if (/Center  Label/i) { $cntr=1; print "  Nuclear Coordinates in                   atomic units                                   Angstrom \n"; print;}
          if ($cntr && /^ *$/ ) {$cntr=0;}

         }
        close F;
       }
##################### CASPT2   #########################################
     if ( $prog eq "caspt2")
       { open (F,"<molcas.output.caspt2");
         $cntr=0;
         while (<F>)
         {if (/Reference energy:/i) {print ;}
          if (/E2 \(Non-variational\):/i) {print ;}
          if (/E2 \(Variational\):/i) {print ;}
          if (/Total energy:/i) {print ;}
          if (/Reference weight:/i) {print ; last;}
         }
        close F;
       }

##################### CCSD     #########################################
     if ( $prog eq "ccsd")
       { open (F,"<molcas.output.ccsd");
         $cntr=0;
         while (<F>)
         {if (/Reference energy:/i) {print ;}
          if (/E2 \(Non-variational\):/i) {print ;}
          if (/E2 \(Variational\):/i) {print ;}
          if (/Total energy:/i) {print ;}
          if (/Reference weight:/i) {print ; last;}
         }
        close F;
       }
##################### CCSDT   #########################################
     if ( $prog eq "ccsdt")
       { open (F,"<molcas.output.ccsdt");
         $cntr=0;
         while (<F>)
         {if (/Reference energy:/i) {print ;}
          if (/E2 \(Non-variational\):/i) {print ;}
          if (/E2 \(Variational\):/i) {print ;}
          if (/Total energy:/i) {print ;}
          if (/Reference weight:/i) {print ; last;}
         }
        close F;
       }

######################RASSCF################################################
      if ( $prog eq "rasscf")
       { open(F,"<molcas.output.rasscf");
         $cntr=0;
         $converged=0;
         while (<F>)
         {
           if ( /Final state energy/i) { while (<F>) { if (/root/i) {print "$_";}
                                                        if (/molecular/i) {last;}
                                                      } last; }
           if (/iter CI/i ) {print "$_"; $_=<F>; print"$_";}
           if (/convergence after/i) { s/^ *//; my @tmp=split(/\s+/,$_);
                                       while( <F>) { s/^ *//;  my @tmp2=split(/\s+/,$_);
                                                    if ($tmp2[0] == $tmp[2]+1) {print"$_"; $converged=1; last;}
                                                    if (/\*\*\*/) {last;}}
                                     }
         }
         if (! $converged ) {die("rasscf calculation did not converge!\n");}
         close F;
       }
##################### TRANSMOM #########################################

     if ( $prog eq "transmom.x" )
          { &tail(30,"transls") ; }

##################### TRANSCI #########################################

     if ( $prog eq "transci.x" )
       { open (F,"<trncils");
         while (<F>) {if (/FINAL RESULTS/i) { print; last ;} }
         while (<F>) { if (/Transition moment components/) { last;} print; }
         while (<F>) {if (/Tr\.dipole/i) { print; last ;} }
         while (<F>) { if (/Einstein B coef/) { print; last;} print; }
         while (<F>) {if (/FINAL RESULTS/i) {last ;} }
         while (<F>) {if (/<L> value components:/i) {print; last ;} }
         while (<F>) { print; if (/length:/) { last;}}
        close F;
       }


##################### scfpq #########################################

     if ( $prog eq "scfpq.x" )
         {  local ( @x, $rest,$etot,$ekin,$epot,$vt,$enuc);
            open(SCFLS,"scfls");
            $/="";
            while ( <SCFLS>)
            { if(/total energy/) {s/[a-zA-Z=\n]/ /g;
                  ($rest,$etot,$ekin,$epot,$vt)=split(/\s+/,$_);}
              if(/nuclear repulsion/)
                  {($rest,$enuc)=split(/^.*=\s+/,$_);
                   $enuc =~ s/\n.*//g ; }}
            close(SCFLS);
  printf "  nuclear repulsion energy    %18.10f\
  kinetic energy              %18.10f\
  potential energy            %18.10f\
  total energy                %18.10f\
  virial theorem              %18.10f\n",$enuc,$ekin,$epot,$etot,$vt;
         }


##################### DALTON ###########################################

     if ( $prog eq "dalton.x" )
       { open (INTLS,"hermitls");
         local (@irreps,@orbs,@x,$x,$rest,$enuc);
         $/="";
         while ( <INTLS>)
         { if ( /^  label/  && $debug{debug_geo}){ printf $_ ;}
           if ( /Bond distance/i && $debug{debug_geo})
            { $x = <INTLS> ;
              chop $x;
              printf "$x (Bond distances in Angstroem)\n\n";}
           if ( /Bond angle/i && $debug{debug_geo})    { $x = <INTLS> ; printf $x ;}
           if ( /Nuclear rep/i)
            { ($rest,$enuc) = split(/^.*:\s+/,$_);
             printf "  Nuclear repulsion energy:  %18.10f\n\n ",$enuc ;}
           if (/Number of orbitals in each sym/i)
              {@x = split(/.*:/,$_);
               @orbs = split(/\s+/,$x[1]);}
           if (/Direct product table/i)
              { $x = <INTLS>; $x=~ s/\n//g;
                $x=~ s/-.*//g; $x=~ s/^\W+//g; @irreps=split(/\b/,$x);}
         }
         printf ( "Irred. repr. : @irreps \n");
         printf ( " # orbitals  : @orbs \n");
         close (INTLS);
       }


############################ MCDRT.X ###################################

   if ( $prog eq "mcdrt.x" )
    {
      local ($x,@x,$mult,$spsym,$csf );
      open(MCDRTLS,"mcdrtls");
      $/="\n";
      while ( <MCDRTLS> )
      { if (/spin multiplicity/) { chop ; @x=split(/\b/,$_); $mult=$x[-2];}
        if (/spatial symmetry/) {chop; @x=split(/\b/,$_); $spsym=$x[-1];}
        if (/final csf sel/) {$x = <MCDRTLS>; @x=split(/\b/,$x); $csf=$x[1];}
      }
      close(MCDRTLS);
      printf "  Spin multiplicity: %s \n  Spatial symmetry (#): %s
  Number of Configurations: %10d \n ", $mult,$spsym, $csf ;
    }

########################## MCSCF.X ####################################

   if ( $prog eq "mcscf.x" || $prog eq "mcscf_d.x" )
     {
     local($skip,$x,@x,$etot,$edrop,$wnorm,$knorm,$nconv);
      open(MCSCFSM,"mcscfsm");
      $skip=0; $/="\n";
      while (<MCSCFSM>)
      { if (/final mcscf/)
          { $_ = <MCSCFSM> ; chop ;
            $nconv = /not conv/;
            s/[a-z=\*]/ /g;
            @x = split(/\s+/,$_);
            ($etot,$edrop,$wnorm,$knorm) = @x[2..5];}
        if (/Individual/i) { $skip=1;}
        if (/DRT #/i) {$x[$skip]=$_ ; $skip++; }
      }
      if ($skip > 0 )
           { printf " Individual total energies for all states: \n";
             $skip--; printf " @x[1..$skip]";
             my ($ii,$iroot,$drt,$w,$e,$ex);
             for ($ii=1; $ii<=$skip; $ii++)
              { $x[$ii]=~s/[a-zA-Z=#(),]//g;
                $x[$ii]=~s/^ *//;
                $x[$ii]=~s/ \. /   /; # DRT root  weight energy excitation energy
                ($drt,$iroot,$w,$e,$ex)=split(/\s+/,$x[$ii]);
                ${${$MCSCF_wf[$drt]}{roots}}[$iroot]=join(':',($w,$e,$ex)); } 
              }
       else
           { printf
  "%-30s%15.8f\n%-30s%15.8f\n%-30s%15.8f\n%-30s%15.8f\n",
                    "Total Energy:", $etot ,
                    "knorm:", $knorm,
                    "wnorm: ",$wnorm,
                    "last energy change:", $edrop  ; 
                    ${${$MCSCF_wf[1]}{roots}}[1]=join(':',(1.0,$etot,0.0));}
      if ( $nconv ) { 
          $nmiter=&keyinfile("mcscfin","NMITER");
          if ($nmiter != 0) {
#fp: print this message only if this is not the artificial property mcscf run
             printf " Warning: calculation not converged ... \n"; 
          }
      }

    }

################### cidrt ###########################################

   if ( $prog eq "cidrt.x" )
     {
     local($sym,$nrefs,$ncsf);
      open(CIDRTLS,"<$cline") || die ("could not open $cline (analyse_output cidrt.x)\n");
      $skip=0; $/="\n"; ;
      while (<CIDRTLS>)
      {
      if (/total number of orbitals  /) { print $_;
                                        for ($i=1; $i<5; $i++)
                                        { $x = <CIDRTLS>; print $x ;}}
      if (/state spatial sym/) { s/^.*://g; $sym=$_; }
      if (/reference csfs selected from/) { s/\D/ /g ;
                                            @x=split(/\s+/,$_);
                                            $nrefs=$x[1];}
      if (/total:/) { s/\D//g ; $ncsf=$_;}
      }
      printf " spatial symmetry: $sym \n";
      printf " total number of configurations %10d
 number of reference configurations %5d\n",
        $ncsf,$nrefs;
      close(CIDRTLS);
     }

################### ciudg  ###########################################

    if ( $prog eq "ciudg.x" || $prog eq "ciudg_d.x" || $prog eq "pciudg.x" )
     { local($dim,$skip,@x,$type);
        system("grep 'dimension of the ci-matrix' ciudgls > tmp");
  # better reorganize the "energy" file and read from this file
        open(CIUDGSM,"<tmp");
         $dim=<CIUDGSM>;
        close CIUDGSM;
        chop $dim; $dim=~s/\D//g;
        open(CIUDGSM,"ciudgsm") || die ;
      $skip=0; $/="\n";
        while(<CIUDGSM>)
        { if (/beginning the ci iterative/) { $skip=1;}
         #if (/dimension/) {@x=split(/\s+/,$_); $dim=$x[7]; }
          if (/convergence information/) { if ($skip) {@x=split(/\s+/,$_);
                                                      $type = $x[2];
                                                      $skip = 2;}}
          if (/ #[ 1-9]/) { if ($skip==2) {
          printf "%-30s %s\n"," type of calculation:", $type;
          printf "%-30s %d\n"," number of configurations:",$dim;
          s/[a-z#]/ /g; s/ - //g; @x=split(/\s+/,$_);
          printf "%-30s %18.10f \n ", " total $type energy",$x[3];
          printf "%-30s %18.10f \n ", "last $type energy change",$x[4];
          printf "%-30s %18.10f \n ", "residual",$x[6]; }}
          if ( $type eq "mr-sdci" )
            { if ( /eci\+dv1/ ) { chop; s/  dv1.*$//; s/^.*=//;
                                  print "mr-sdci+(1-c0**2)*(eci-eref)             = $_\n ";}
              if ( /eci\+dv2/ ) { chop; s/  dv2.*$//; s/^.*=//;
                                  print "mr-sdci+(1-c0**2)/(c0**2)*(eci-eref)     = $_\n ";}
              if ( /eci\+dv3/ ) { chop;s/  dv3.*$//; s/^.*=//;
                                  print "mr-sdci+(1-c0**2)/(2*c0**2-1)*(eci-eref) = $_\n";}
            }
          if ( /c0\*\*2/ && /all zwalks/ ) {print "$_ ";}
        }
       close(CIUDGSM);
       my ($drt,$roots,$trans,$e,$iroot);
       $drt=$control{thisdrt};
       open ENRGY,"<energy";
        while (<ENRGY>) 
        { if ( /ROOTS/) {$roots=1; next;}
          if ( /TRANSITION/ ) {$trans=1; last; }
          chop; s/^ *//; ($iroot,$e)=split(/\s+/,$_);
          ${${$MRCI_wf[$drt]}{roots}}[$iroot]=$e;  } 
        close(ENRGY);
      }


################### exptvl  ###########################################

     if ( $prog eq "exptvl.x")
      { $/=""; local ( $x);
        open(PROPLS,"propls");
        while (<PROPLS>)
         { if (/Dipole moments/i) {print "  === dipole moment === \n"  ;
                                  $x=<PROPLS>; print $x;}
           if (/Quadrupole moment:/i) {print "  === quadrupole moment === \n"  ;
                                  $x=<PROPLS>; print $x;}
           if (/Second moments:/i) {print "  === second moment === \n"  ;
                                  $x=<PROPLS>; print $x;}
         }
        close(PROPLS);
      }

################### cigrd   ###########################################

     if ( $prog eq "cigrd.x")
     { $/="\n"; local( $eci,$ecieff);
     open(CIGRDLS,"cigrdls");
     while (<CIGRDLS>)
      { if (/eci =/) { s/[a-z\)\(=+\*]//g; s/ \d //g; $eci=$_;}
        if (/eci\(eff\) =/) { s/[a-z\)\(=+\*]//g; s/ \d //g; $ecieff=$_;}
     }
     close(CIGRDLS);
     printf " Total energy (ciudg) : %18.12f \n ", $eci;
     printf " Effective energy     : %18.12f \n ", $ecieff;
     printf " Delta E              : %18.12f \n ", $eci-$ecieff;
      }
################### gdiis   ###########################################

     if ( $prog eq "gdiis.x" && !$kwords{"nadcoupl"}==1)
      {$/="\n";
##      local ($flag1,$flag2,$etot);
#       open(GDIISLS,"gdiisls");
#       $flag1=0;
#       $flag2=0;
#       while (<GDIISLS>)
#        { if ( /      energy:   / ) { s/[a-z:]//g;
#              printf " total energy: %18.12f \n", $_;  }
#          if ( /   internal coordinates and forces/ )
#               {$flag1=1;
#                printf " \n %20s  %20s %20s \n\n", " # int. coord.",
#                       " coord. [au|rad]", " force [au] ";
#                $_="";}
#          if ( / maximum force on coord. / ) { $flag1=0; }
#          if ( /    GEOMETRY CHANGE AND NEW INTERNAL COORDINATES/ )
#                {$flag2=1;
#                printf " \n %10s  %20s %20s \n", " # int. coord.",
#                       " change [au|rad]", "value [au|rad] "; $_="";}
#          if ( /^ ROUND / ) { $flag2=0;}
#          if ( $flag1 || $flag2 ) {s/^ *//g; @x = split/\s+/;
#            if ( $x[0] > 0 ) {
#            printf "%10d                %16.8f       %16.8f    \n",
#                @x } }
#         }
#
#       close(GDIISLS);
#
        open(GEOCONV,"geoconvfl");
#
          while(<GEOCONV>)
           {
            chop;
            s/^ *//g; @x = split/\s+/;
            if ($x[0] eq 'max-step='){$line1=$_; $geomconv{maxstep}=$x[1];}
            if ($x[0] eq 'rms-step='){$line2=$_; $geomconv{rmsstep}=$x[1];}
            if ($x[0] eq 'max-force='){$line3=$_; $geomconv{maxforce}=$x[1];}
            if ($x[0] eq 'rms-force='){$line4=$_; $geomconv{rmsforce}=$x[1];}
           }
          close(GEOCONV);
          printf "\n          value                converged\n";
          printf "        -----------------------------------\n";
          printf "         $line1 \n";
          printf "         $line2 \n";
          printf "         $line3 \n";
          printf "         $line4 \n\n";
#
        }
##############################################################################

    printf  $_[2];
    printf " --------------------------------------------------- \n ";
   }

##############################################################################

   sub appendtofile {

    local($sp,$all,$control{iter});
    ($sp,$all,$control{iter})=@_;
   $MARKER="<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n";
#    printf "appendtofile: sp,all,iter=$sp,$all,$control{iter}\n";
       if ( -s "$all") { open (ALL,">>$all");}
               else      { open (ALL,">$all");}
       open(SP,"$sp") or return;
       printf ALL "\n\&\& iteration number $control{iter} $MARKER\n ";
       while(<SP>) { print ALL;}
       close(SP);
       close(ALL);
    }

  sub append{
  local ($fileall,$file);
    ($fileall,$file)=@_;

#    printf "appending file: $file to file: $fileall\n";
       if ( -e $fileall) { open (ALL,">>$fileall");}
               else      { open (ALL,">$fileall");}
       open(SP,"$file");
       while(<SP>) { print ALL;}
       close(SP);
       close(ALL);

  }

  sub outputheader{

  local ($version,$colversion,$versiondate);
  $version="7.0 beta ";
  $versiondate="Jan 2011 ";
  $colversion="COL_7.0_beta";

  print "===========================================================\n";
  print "===                   runc                              ===\n";
  print "===           version ",$version,", ",$versiondate,
  "              ===\n";
  print "===          requires COLUMBUS version ",$colversion,
  "     ===\n";
  print "===                                                     ===\n";
  print "===   written by: Thomas Mueller                        ===\n";
  print "===   gradient ext. by: Michal Dallos                   ===\n";
  print "===               Institute for Theoretical Chemistry   ===\n";
  print "===               University of Vienna                  ===\n";
  print "===               Waehringerstr. 17                     ===\n";
  print "===               A-1090 Vienna, Austria                ===\n";
  print "===   in revision by: Thomas Mueller, JSC, FZ Juelich   ===\n";
  print "===               Felix Plasser, University of Vienna   ===\n";
  print "===========================================================\n";
  print "\n\n";
  }


      sub createmcpcin {
       local ( $state,$drt );
       ($state,$drt)=@_;
       if ($debug{debug_runc}) {print "executing sub createmcpcin \n";}
       open MCPCIN, ">mcpcin";
       print MCPCIN "  $drt / \n  $state / \n ";
       print MCPCIN "  2 / \n  0.001 / \n  0 / \n  0 / \n";
       close MCPCIN ;
       return 0;
       }




  sub tail {
     local ($lines,$name,$cnt1,$cnt2,@inlines);
    ($lines,$name) = @_ ;
       if ($debug{debug_runc}) {print "executing sub tail \n";}

        open(LSFILE,$name);
        $cnt1=1;
         $/="\n";
         while (<LSFILE>)
         { $inlines[$cnt1]=$_;
          $cnt1++;
          if ( $cnt1 > $lines ) { $cnt1 = 1;}
          }

        $cnt2=$cnt1+1;
          while ( $cnt2 <= $lines )
           { print $inlines[$cnt2]; $cnt2++;}
          $cnt2=1 ;
          while ( $cnt2 <= $cnt1 )
           { print $inlines[$cnt2]; $cnt2++;}

         close LSFILE;

      }


  sub cutci
  {my (@civfl,$f,$postfix);
       if ($debug{debug_runc}) {print "executing sub cutci \n";}
   chdir($work);
   @civfl = <civfl.*> ;
   foreach $f ( @civfl )
        { $postfix=$f; $postfix=~s/civfl\.//g; $postfix=~s/ *$//g;
          if ( ! -s "civout.$postfix" )
                { &popup_ask("cutci: cannot find civout.$postfix\n"); exit;}
          open CUTCIIN,">cutciin";
          print CUTCIIN "  \&input\n vectorin=\'$f\'\n vectorout=\'civfl.new\'\n";
          print CUTCIIN "  infoin=\'civout.$postfix\',\n infoout=\'civout.new\'\n";
          print CUTCIIN "  lvlprt=0, delsrc=1\n\/&end\n";
          close CUTCIIN;
          print "info:  reducing size of $f \n";
          &callprog("cutci.x");
          mv("civout.new","civout.$postfix");
          mv("civfl.new",$f);
        }
   return;
   }

  sub geom
{
  if ($debug{debug_runc}) {print "executing sub geom \n";}
  open (GEOM,"$direct{JDIR}/geom") or die"Failed to open file: $direct{JDIR}/geom\n";
  $natom=0;
  while(<GEOM>){$natom++;}
  close GEOM;
  return ($natom);
}


  sub  getcienergy
{
  local ($file,$state,$icount,$dum);
  ($file,$state)=@_;
  if ($debug{debug_runc}) {print "executing sub getcinergy \n";}
#
  open (FILE,$file) or die "cannot open file: $file\n";
  $icount=1;
  while(<FILE>)
   {
    if (/eci       =/i)
     {
       if ($icount == $state )
        {
         chomp; s/^ *//;
         ($dum,$dum,$energy,$dum)=split(/\s+/,$_,4);
         last;
        }
      $icount++;
     }
   }
  close FILE;
  return ($energy);
}

#########################################################################################################################

  sub transfgrad
{
  my ($name,$type,$forceinau)=@_;
  if ($debug{debug_runc}) {print "executing sub transfgrad \n";}

  #fp: cart2int.x was causing problems..
  #  if "intcfl" is not present the call can be avoided
  if ( -f "$direct{JDIR}/intcfl" )
  {
    print " Found intcfl file, performing internal coordinate analysis.\n";
    cp ("$direct{JDIR}/intcfl","intcfl") or die "Failed to copy file: $direct{JDIR}/intcfl\n";
    open (NIN,">cart2intin") or die "Cannot open file: cart2intin\n";
    print NIN " &input\n calctype='cart2int',\n intgradout=1,\n";
    print NIN " gradfl=\'cart$type$name\',\n forceinau=$forceinau,\n";
    print NIN " intgradfl=\'int$type$name\',\n\/&end\n";
    close NIN;
    callprog($control{CART2INT});
    &appendtofile("$direct{work}/cart2intls","$direct{list}/cart2int1ls$name.all",$control{iter});
    &appendtofile("$direct{work}/int$type$name","$direct{grad}/int$type$name.all",$control{iter});
    cp ("$direct{work}/int$type$name","$direct{grad}/int$type$name.sp");
    cp ("$direct{work}/intgeom","$direct{geom}/intgeom.sp");
    &appendtofile("$direct{work}/intgeom","$direct{geom}/intgeom.all",$control{iter});
  }
#
}


###############################################################################################################################

  sub dxcalc
{
#
#   Calculate the Dx(r,s) term of D[CSF] nonadiabatic coupling term
#   JCP 120 (2004) 7322, last term eqn. 49
#   Output: nadaxfl
#
#  transforms the antisymmetric 1-e transition density matrix MO -> AO
#
  if ($debug{debug_runc}) {print "executing sub dxcalc \n";}
    print "\n Calculating the Dx(r,s) term of D[CSF] nonadiabatic coupling term.\n";

#   callprog("dzero.x > dzerols");
#   cp ("dzero2","modens2");
#cdd unless (rename("cid1trfl","modens")) {die ("rename error $!\n");}
    unless (cp("cid1trfl","modens")) {die ("rename error $!\n");}
    if ($kwords{"nadcoupl_mc"}) {
       $infile="tranmcdenin"
    }
    else {
       $infile="trancidenin"
    }
     &changekeyword("$direct{JDIR}/$infile","tranin","tr1e",1);
# no longer necessary bugfix abacol
#    &changekeyword("tmp","tmp","tr1e",1);
#    &changekeyword("tmp","tranin","lrc2mx",2048);
#    unlink("tmp");
    callprog("tran.x -m $control{coremem}");
    &appendtofile("tranls","$direct{list}/tranls.dx.all",$control{iter});
#
#   contract the 1-e transition density matrix(AO) with the antisymmetrized 1-el
#   half-differenciated overlap matrix x(r,s):
#   x(r,s) = 1/2 {<Phi(r)|Phi(s)x>-<Phi(r)x|Phi(r)>
#
   cp("$direct{JDIR}/abacusin.nad","daltcomm") or die "Cannot find file: $direct{JDIR}/abacusin.nad\n";
   callprog("dalton.x -m $control{coremem} > abacusls");
   unlink <fort*>;
   &appendtofile("abacusls","$direct{list}/abacusls.dx.all",$control{iter});
   cp ("nadxfl","$direct{grad}/nadxfl.sp");
   if ($kwords{"gdiis"}||$kwords{"polyhes"}){
       &appendtofile("nadxfl","$direct{grad}/nadxfl.all",$control{iter});
   }
}

########################################################################################################################################


  sub protocol4nad
{
  local ($control{iter})=@_;
  local ($e1,$e2,$de);
  if ($debug{debug_runc}) {print "executing sub protocol4nad \n";}
#
  open (ALL,"$direct{list}/energy") or die "Failed to open file: $direct{list}/energy\n";
    $_=<ALL>;chomp; s/^ *//;
    ($e1,$e2,$de)=split(/\s+/,$_,3); $de=abs($de);
  close ALL;
#
  cp ("$direct{JDIR}/intcfl","$direct{work}/intcfl") or die "Failed to copy: $direct{JDIR}/intcfl\n";
  ($nintcoor,$intcoor)=&intc(); #  determine the number of internal coordinates
#
#  get internal geometry
  open (FILE1,"$direct{geom}/intgeom.sp");
  for ($i=1;$i<=$nintcoor;$i++)
   {$_=<FILE1>;chomp; s/^ *//;$intgeom[$i]=$_;}
  close FILE1;
#
#  get internal displacement
  open (FILE1,"$direct{geom}/displace.sp");
  for ($i=1;$i<=$nintcoor;$i++)
   {$_=<FILE1>;chomp; s/^ *//;$intdisp[$i]=$_;}
  close FILE1;
#
#  get internal gradient for state1
  $file="$direct{grad}/cartgrd.drt$naddrt1.state$nadstate1.sp";
  open (FILE1,"$file") or die "Failed to open file:$file\n";
  for ($i=1;$i<=$nintcoor;$i++)
   {$_=<FILE1>;chomp; s/^ *//;$gradst1[$i]=$_;}
  close FILE1;
#
#  get internal gradient for state2
  $file="$direct{grad}/cartgrd.drt$naddrt2.state$nadstate2.sp";
  open (FILE1,"$file") or die "Failed to open file:$file\n";
  for ($i=1;$i<=$nintcoor;$i++)
   {$_=<FILE1>;chomp; s/^ *//;$gradst2[$i]=$_;}
  close FILE1;
#
#  get internal nonadiabatic gradient
  $file="$direct{grad}/cartgrd.nad.drt$naddrt1.state$nadstate1.drt$naddrt2.state$nadstate2.sp";
  open (FILE1,"$file") or die "Failed to open file:$file\n";
  for ($i=1;$i<=$nintcoor;$i++)
   {$_=<FILE1>;chomp; s/^ *//;$gradnad[$i]=$_;}
  close FILE1;
#
  $file = "$direct{list}/polyhessm";
  if ( -e $file) {open (NIN,">>$file")}
  else           {open (NIN,">$file")}
#
  print NIN "\n **************************************************\n";
  print NIN "         Iteration number: $control{iter}\n\n";
  print NIN "    System with: $nintcoor internal coordinates detected\n";
  print NIN "  Energy[DRT:$naddrt1,State:$nadstate1] = $e1\n";
  print NIN "  Energy[DRT:$naddrt2,State:$nadstate2] = $e2\n";
  print NIN "  DeltaE = $de\n\n";
  print NIN "       Curr. int.geom      Displacement     New int.geom\n";
  print NIN "  ---------------------------------------------------\n";
  for ($i=1;$i<=$nintcoor;$i++)
   {printf {NIN} ("   %s %10.6f           %10.6f       %10.6f\n",$i,$intgeom[$i],$intdisp[$i],$intgeom[$i]+$intdisp[$i]);}
#
  print NIN "\n       CIgrad DRT:$naddrt1\ST:$nadstate1     CIgrad DRT:$naddrt2\ST:$nadstate2    NADgrad DRT:$naddrt1\ST:$nadstate1-DRT:$naddrt2\ST:$nadstate2\n";
  print NIN "  ---------------------------------------------------\n";
  for ($i=1;$i<=$nintcoor;$i++)
   {printf {NIN} ("   %s %10.6f           %10.6f       %10.6f\n",$i,$gradst1[$i],$gradst2[$i],$gradnad[$i]);}
#
  close NIN;
}

##################################################################################################################################

    sub  gendaltonunique {
    if ($debug{debug_runc}) {print "executing sub gendaltonunique \n";}
      my $daltonout;
      ($daltonout)=@_;
       open DALT2,"<$daltonout";
       open DUNIQUE,">DALTON.UNIQUE";
        while (<DALT2>) { if (/^ *Number of orbitals in each symmetry:/) { last;}}
        while (<DALT2>)
         { if (/Symmetry/) { $x=<DALT2>;
                             while (<DALT2>) { if (/^ *$/ || /^$/ ) {last;}
                                               s/^ *//; chop;
                                               ($bfnno,$atomlabel,$basislabel,$remainder)=split(/\s+/,$_);
                                               $uniquelabel=sprintf "%-2s %-4s",$atomlabel,$basislabel;
                                               $uniquelabel=~s/ /_/g;
                                               printf DUNIQUE "%5d     %-8s\n",$bfnno,$uniquelabel;
                                              }
                           }
           if (/Symmetries/i) {last;}
         }
        close DALT2;
         close DUNIQUE;
       return;
    }

######################################################################################################################################
  sub setup_ordints
   { my ($fordint,$x,$cnt,$gen);
     ($gen)=@_;
      if ($debug{debug_runc}) {print "executing sub setup_ordints \n";}
      if ($gen) { system("find ./ -name molcas.OrdInt -print > ordintls");
                  $fordint="ORDINT";
                  $cnt=0;
                  open ORDINT,"<ordintls";
                   while (<ORDINT>) { $x=$_; chop $x; system("rm -f $fordint");system("ln -s $x $fordint");
                                     print "Generating link ln -s $x $fordint \n";
                                     $cnt++; $fordint = "ORDINT" . $cnt; }
                  close ORDINT;
                 unlink "ordintls"; }
      else       { print "deleting ordints\n ";
                   system ("find ./ -name \'ORDINT*\' -print > ordintls ");
                  open ORDINT,"<ordintls";
                  while (<ORDINT>) { chop; unlink $_;} close ORDINT; unlink "ordintls"; }
      return;
 }

######################################################################################################################################
  sub distribute
      { my ($filename,$x,$cnt,$gen) ;
         ($filename)=@_;
      if ($debug{debug_runc}) {print "executing sub distribute \n";}
         system("find ./ -type d -name 'tmp_*'  -print > ordintls ");
         open ORDINT,"<ordintls";
           while (<ORDINT>) { chop; print "cp $filename $_/$filename\n"; system("cp $filename $_/$filename");}
         close ORDINT;
        unlink "ordintls";
        return;
       }


######################################################################################################################################

        sub file_output{
        my ($title,$file)=@_;
        if ($debug{debug_runc}) {print "executing sub file_output \n";}
        print "\n === $title === \n";
        open(FILE,"<$file");
        while (<FILE>)
        {
          print $_;
        }
        close(FILE);
        print "===================================\n";
        }

######################################################################################################################################
        sub effden_tran{
        my ($alaska,$extension,$ngrad)=@_;
        if ($debug{debug_runc}) {print "executing sub effden_tran \n";}

        if ($kwords{"nadcoupl_mc"}) {
           $infile="tranmcdenin"
        }
        else {
           $infile="trancidenin"
        }
        if ($alaska) { &changekeyword("$direct{JDIR}/$infile","tranin","seward",1);
                       &changekeyword("tranin","tranin","ngrad",$ngrad);
                       unlink("GAMMA","GTOC"); }
                else { cp("$direct{JDIR}/$infile","tranin");}
                        # no longer necessary bugfix abacol
                        #&changekeyword("$direct{JDIR}/$infile","tmp","lrc1mx",2048);
                        #&changekeyword("tmp","tranin","lrc2mx",2048);
                        #unlink("tmp"); }

        callprog("tran.x -m $control{coremem}");
        unlink (<fort*>,"tranin","modens","modens2");
        &appendtofile("tranls","$direct{list}/tranls.$extension.all",$control{iter});
        return;}

######################################################################################################################################
        sub call_cigrd{
        #fp: central sub for calling cigrd.x/pcigrd.x; including possibility for debug
        my ($name,$pcigrd,$iter,$tr) = @_;
        if ($debug{debug_runc}) {print "executing sub call_cigrd \n";}

        if ($debug{debug_dens})
        {
            open IIN,">istatin";
            print IIN "cid1fl$tr\n cid2fl$tr\n";
            close IIN;
            print "\n +++ Statistics of the input density: +++\n";
            callprog("istat.x < istatin > istatls");
            &appendtofile("istatls","$direct{list}/istatls.inp$name.all",$control{iter});
        }

        if ($pcigrd)
        #{ pcallprog("pcigrd.x -m 100000000"); }
        { pcallprog("pcigrd.x -m 1000"); }
        else
        { callprog("cigrd.x -m $control{coremem}"); }
        &appendtofile("cigrdls","$direct{list}/cigrdls$name.all",$control{iter});

        unless (rename("effd1fl","modens")) {die ("rename error $!\n");}
        unless (rename("effd2fl","modens2")){ die ("rename error $!\n");}

        if ($debug{debug_dens})
        {
            open IIN,">istatin";
            print IIN "modens\n modens2\n";
            close IIN;
            print "\n +++ Statistics of the output effective density: +++\n";
            callprog("istat.x < istatin > istatls");
            &appendtofile("istatls","$direct{list}/istatls.eff$name.all",$control{iter});
        }
            unlink("cid1fl","cid2fl");
        }

       sub read_transmomin{
       # read transmomin and place data into appropriate global data structures
       # sections MCSCF   (processed by mcscf.x)
       #          MCSCF1e (processed by transmom.x)
       #          CI1e    (CI 1e transition densities or 1e densities processed directly by transci.x)
       #          CI      (CI 2e transition densities or 2e densities processed by ciudg 
       #                   after adding to "transition section of ciudgin" 
       #                   separating between 2e and 1e densities allows ciudg to read directly
       #                   the transmomin file, if requested 
       #  drt1 root1 drt2 root2 
        if ($debug{debug_runc}) {print "executing sub read_transmomin \n";}

        $mcscfgrad{n}=0;
        $mcscftrans{n}=0;
        $cigrad{n}=0;
        $citrans{n}=0;
        $trans1e{n}=0;
        $transmom{n}=0;
         if ( ! -s "transmomin") {return;}
         open FIN,"<transmomin" ;
         while (<FIN>)
          { if (/^#/) {next;}
            s/ *$//; chop; 
            if (/^MCSCF$/) {$transmomsection=0;$mcscfsection=1; $ci1esection=0; $cisection=0; next;}
            if (/^MCSCF1e$/) {$transmomsection=1;$mcscfsection=1; $ci1esection=0; $cisection=0; next;}
            if (/^CI$/)    {$transmomsection=0;$mcscfsection=0; $ci1esection=0; $cisection=1; next;}
            if (/^CI1e$/)  {$transmomsection=0;$mcscfsection=0; $ci1esection=1; $cisection=0; next;}
            s/^ *//; @tmp=split(/\s+/,$_);
            if ($mcscfsection) { if ($tmp[1]==$tmp[3] && $tmp[0] == $tmp[2]) { $mcscfgrad{n}++; 
                                                                               ${$mcscfgrad{drt}}[$mcscfgrad{n}]=$tmp[0];
                                                                               ${$mcscfgrad{root}}[$mcscfgrad{n}]=$tmp[1]; }
                                 else {$mcscftrans{n}++; ${$mcscftrans{eintrag}}[$mcscftrans{n}]=join(':',@tmp); }}
 
            if ($cisection)    { if ($tmp[1]==$tmp[3] && $tmp[0] == $tmp[2]) { $cigrad{n}++; 
                                                                               ${$cigrad{drt}}[$cigrad{n}]=$tmp[0];
                                                                               ${$cigrad{root}}[$cigrad{n}]=$tmp[1]; }
                                 else {$citrans{n}++; ${$citrans{eintrag}}[$citrans{n}]=join(':',@tmp); }}
            if ($ci1esection)  { $trans1e{n}++; ${$trans1e{eintrag}}[$trans1e{n}]=join(':',@tmp); }
            if ($tranmomsection)  { $transmom{n}++; ${$transmom{eintrag}}[$transmom{n}]=join(':',@tmp); }
          }
        close (FIN);
       }
 sub getmolinfo {
# extract molecular information
# from infofl
        if ($debug{debug_runc}) {print "executing sub getmolinfo \n";}
   $/="\n";
   open FINFOFL,"<infofl";
   my (@tmp,$tmp);
   my @infofl = <FINFOFL>;
   close FINFOFL;
   chomp @infofl;
   $tmp=shift @infofl;
   $tmp=~s/ *$//;
   $molecule{pointgroup}=$tmp;
   $tmp=shift @infofl;
   $tmp=~s/^ *//;
   @tmp=split(/\s+/,$tmp);
   @{${$molecule{irrep}}{nbfao}}[1..8]=@tmp[0..7];

   $tmp=shift @infofl;
   $tmp=~s/^ *//;
   ${$molecule{irrep}}{nsym}=$tmp;
   $tmp=shift @infofl;
   $tmp=~s/^ *//;
   @tmp=split(/\s+/,$tmp);
   @{${$molecule{irrep}}{symbols}}[1..8]=@tmp[0..7];
 # from geom (redundant coordinates)
   open FGEOM,"<geom" || die ("cannot open file geom \n");
   @infofl = <FGEOM>;
   close FGEOM;
   my ($natoms,%formula,$k, $formula,$mass);
   chomp(@infofl);
   $natoms=0;
   while ($#infofl > -1)
   { $tmp=shift @infofl;
     $tmp=~s/^ *//;
     @tmp=split(/\s+/,$tmp);
     $natoms++;
     $mass=$mass+$tmp[5];
#    print "$tmp ==> $tmp[0], $tmp[5] \n";;
     $formula{$tmp[0]}++;}
   $formula="";
   foreach $k ( keys %formula )
   { $formula = $formula . $k . $formula{$k} ;}
#  print "formula=$formula \n";
   $molecule{formula}=$formula;
   $molecule{mass}=$mass;

# wavefunction info can be explored best late 
# hence insert some subroutine in the routing card
# and finally print this info at the end of the
# iteration in some tabulated form on some file
# say COLUMBUS.ARCHIV
 }

 sub  getdrttranslation{

   # generate  $drttranslate{^multSymm} = #DRT
        if ($debug{debug_runc}) {print "executing sub getdrttranslation \n";}

   $/="\n";
   $drt=0;
   $maxdrt=0;
   foreach $x (<mcdrtin*> )
   { open FIN,"<$x";
      $drt=$x; $drt=~s/mcdrtin\.//;
      if (grep(/mcdrtin$/,$x)) {$drt=1;}
      if ($drt > $maxdrt) {$maxdrt=$drt;}

      while (<FIN>)
     { if (/spin multiplicity/) {chop;s/^ *//; s/ *\/.*$//; $mult=$_;next;}
       if (/molecular spatial symmetry/) {chop;s/^ *//; s/ *\/.*$//; print "isym=$isym\n";
                                          @isym=split(//,${${$molecule{irrep}}{symbols}}[$_]); $isym[0]=uc $isym[0]; $isym=join('',@isym);last;}}
    ${$translation{mcscfdrt}}{"^$mult$isym"}=$drt;
    ${$translation{mcscfdrtrev}}[$drt]="^$mult$isym";
    close FIN; }
    if ($maxdrt) { print "MCSCFDRT: $maxdrt\n";
                   foreach $x (keys %{$translation{mcscfdrt}})  { print " mcscfdrt{$x}=${$translation{mcscfdrt}}{$x} \n"; }
                   $kwords{mcscfndrt}=$maxdrt;}

  # same for cidrtin or cidrtmsin

   if ( -s "cidrtmsin" )      # current version: common multiplicity, different symmetry
                             # for MXS between different multiplicities we would need multiple cidrtin
                             # input files 
    { open FIN,"<cidrtmsin";
      my ($idrt,$ndrtci,$multci,@symmetry);
      $idrt=0;
       while (<FIN>)
       { if (/number of DRTs/) {chop; s/\/.*$//; s/ //g; $kwords{ciudgndrt}=$_; next;    } # 
         if (/spin multiplicity/) {chop; s/\/.*$//; s/ //g; $multci=$_; next; }
         if (/molecular spatial symmetry/) {chop; s/\/.*$//; s/ //g; $idrt++;
                                            @isym=split(//,${${$molecule{irrep}}{symbols}}[$_]); $isym[0]=uc $isym[0]; $isym=join('',@isym);
                                            ${$translation{cidrt}}{"^$multci$isym"}=$idrt;
                                            ${$translation{cidrtrev}}[$idrt]="^$multci$isym"; next; }
       }
      close FIN;
    }
    $maxdrt=0;
   foreach $x (<cidrtin*> )
   { open FIN,"<$x";
      $idrt=$x; $idrt=~s/cidrtin\.//;
      if (grep(/cidrtin$/,$x)) {$idrt=1;}
       if ($idrt>$maxdrt) {$maxdrt=$idrt;}
      while (<FIN>)
       { if (/spin multiplicity/) {chop; s/\/.*$//; s/ //g; $multci=$_;  next; }
         if (/molecular spatial symmetry/) {chop; s/\/.*$//; s/ //g; $isym=$_;
                                            @isym=split(//,${${$molecule{irrep}}{symbols}}[$_]); $isym[0]=uc $isym[0]; $isym=join('',@isym);
                                            ${$translation{cidrt}}{"^$multci$isym"}=$idrt;
                                            ${$translation{cidrtrev}}[$idrt]="^$multci$isym"; last; }
       }
      close FIN;
    }
    if ($maxdrt) { #print "CIDRT: $maxdrt\n";
                   foreach $x (keys %{$translation{cidrt}})  { print " translation state ->#drt:  cidrt{$x} => ${$translation{cidrt}}{$x} \n"; }
                   $kwords{ciudgndrt}=$maxdrt;}
   }



  sub getmcscfinfo{
   my ($file,$maxdrt,@tmp,$tmp,$drt,@docc,@active,$item);
        if ($debug{debug_runc}) {print "executing sub getmcscfinfo \n";}
    $maxdrt=0;
    foreach $file (<mcdrtfl*>)
    { if ( ! grep(/\./,$file)) { $drt=1;}
      else { $tmp=$file; $tmp=~s/^.*\.//; $drt=$tmp;}
      open FIN,"<$file";
      while (<FIN>) { if (/info/) {last;}}
      $tmp=<FIN>; chop;
      $tmp=~s/^ *//;
      @tmp=split(/\s+/,$tmp);
     # order: ndot,nact,nsymd,nrow,ssym,smult,nelt,nela,nwalk,ncscf
       print join(':',@tmp),"\n";
     ${$MCSCF_wf[$drt]}{symmetry}=$tmp[4];
     ${$MCSCF_wf[$drt]}{multiplicity}=$tmp[5];
     ${$MCSCF_wf[$drt]}{ncsf}=$tmp[9];
#    print "$tmp[4],$tmp[5],$tmp[9] ==> ",${$MCSCF_wf[$drt]}{symmetry},${$MCSCF_wf[$drt]}{multiplicity},${$MCSCF_wf[$drt]}{ncsf},"\n";
     while (<FIN>)  {if (/symd/) {last;}}
      $tmp=<FIN>;
      $tmp=~s/^ *//;
      @tmp=split(/\s+/,$tmp);
      @docc=();
      foreach $item ( @tmp ) { $docc[$item]++;}
     while (<FIN>)  {if (/syml/) {last;}}
      $tmp=<FIN>;
      $tmp=~s/^ *//; chop;
      @tmp=split(//,$tmp);
      @active=();
      foreach $item ( @tmp ) { $active[$item]++;}
     @{${$MCSCF_wf[$drt]}{DOCC}}[1..8]=@docc[1..8];
     @{${$MCSCF_wf[$drt]}{ACT}}[1..8]=@active[1..8];
     close FIN;
     if ($drt>$maxdrt) {$maxdrt=$drt;}
   }
    $MCSCF_wf{ndrt}=$maxdrt;
  }


 sub getciinfo{

   my ($file,$maxdrt,@tmp,$tmp,$drt,$item,@nmpsy,@nexo,@internal);
        if ($debug{debug_runc}) {print "executing sub getciinfo \n";}
    $maxdrt=0;
    foreach $file (<cidrtfl*>)
    { if ( ! grep(/\./,$file)) { $drt=0;}
      else { $tmp=$file; $tmp=~s/^.*\.//; $drt=$tmp;}
      $/="\n";
      open FIN,"<$file";
      while (<FIN>) { if (/info/) {last;}}
      $tmp=<FIN>; chop $tmp;
      $tmp= $tmp . <FIN>; chop $tmp;
      $tmp=~s/^ *//;
      @tmp=split(/\s+/,$tmp);
#     print " (file: $file ) info DRT# $drt: ",join('|',@tmp),"\n";
     # order: nmot, niot,nfct, smult ,nrow,nsym,ssym,pthz,pthy,pthx,pthw,nvalz,nvaly,nvalx,nvalw,ncsft 
     #         0     1    2    3    4    5    6     7   8    9    10    11    12    13    14
#             48     9    0   0    62    8    1    13 128   56    71    13   128     56   71 14558

     ${$MRCI_wf[$drt]}{symmetry}=$tmp[6];
     ${$MRCI_wf[$drt]}{nmot}=$tmp[0];
     ${$MRCI_wf[$drt]}{niot}=$tmp[1];
     ${$MRCI_wf[$drt]}{nfct}=$tmp[2];
     ${$MRCI_wf[$drt]}{nrow}=$tmp[4];
  #  ${$MRCI_wf[$drt]}{multiplicity}=$tmp[3];
     ${$MRCI_wf[$drt]}{ncsf}=$tmp[15];
     ${$MRCI_wf[$drt]}{nvalzyxw}=$tmp[11]+$tmp[12]+$tmp[13]+$tmp[14];
     ${$MRCI_wf[$drt]}{pthzyxw}=$tmp[7]+$tmp[8]+$tmp[9]+$tmp[10];
     while (<FIN>)  {if (/nmpsy/) {last;}}
      $tmp=<FIN>;
      $tmp=~s/^ *//;
      @nmpsy=split(/\s+/,$tmp);
      unshift @nmpsy, 0;
      $tmp=<FIN>;
      $tmp=<FIN>;
      $tmp=~s/^ *//;
      @nexo=split(/\s+/,$tmp);
      unshift @nexo, 0;
     while (<FIN>)  {if (/syml/) {last;}}
      $tmp=<FIN>;
      $tmp=~s/^ *//; chop;
      @tmp=split(//,$tmp);
      @internal=();
      foreach $item ( @tmp ) { $internal[$item]++;}
      for ($item=1; $item<=8; $item++) {   ${${$MRCI_wf[$drt]}{frozeni}}[$item]=${${$molecule{irrep}}{symbols}}[$item] -${${$MRCI_wf[$drt]}{nmo}}[$item]; }
     @{${$MRCI_wf[$drt]}{nmo}}[1..8]=@nmpsy[1..8];
     @{${$MRCI_wf[$drt]}{intern}}[1..8]=@internal[1..8];
     @{${$MRCI_wf[$drt]}{extern}}[1..8]=@nexo[1..8];
      $tmp="";
      while (<FIN>) { if (/     b$/) {last;}}
      while (<FIN>) { if (/[()]/) {last;} chop; $tmp= $tmp . $_; }
      $tmp=~s/^ *//;
      @bvalues=split(/\s+/,$tmp);
     ${$MRCI_wf[$drt]}{multiplicity}=2*$bvalues[${$MRCI_wf[$drt]}{nrow}-1]+1;
     close FIN;
     if ($drt>$maxdrt) {$maxdrt=$drt;}
   }
    $MRCI_wf{ndrt}=$maxdrt;
    my ($type); $type=keyinfile("ciudgin.drt1","NTYPE");
    my ($gset); $gset=keyinfile("ciudgin.drt1","GSET");
    if ($type == 0) { $MRCI_wf{type}="MRCISD";}
    if ($type == 3 && $gset == 3  ) { $MRCI_wf{type}="MRAQCC";}

  }


 sub printsummary{
 if ($debug{debug_runc}) {print "executing sub printsummary \n";}
 open ARCHIV,">>$direct{JDIR}/COLUMBUS.ARCHIV";
  printf ARCHIV " ######################### iteration %3d ####################### \n",$control{iter}; 
 if ($control{header}) 
 { print  ARCHIV " =================== MOLECULAR INFORMATION =====================\n";
   printf ARCHIV " = sum formula      %-12s    molecular mass  %-8.1f   =\n",$molecule{formula}, $molecule{mass};
   printf ARCHIV " = point group      %-5s                                      =\n", $molecule{pointgroup};                
   printf ARCHIV " = irreps           %-4s %-4s %-4s %-4s %-4s %-4s %-4s %-4s    =\n", @{${$molecule{irrep}}{symbols}}[1..8];
   printf ARCHIV " = basis functions  %-4s %-4s %-4s %-4s %-4s %-4s %-4s %-4s    =\n", @{${$molecule{irrep}}{nbfao}}[1..8]; }
   my @mult=(" ","Singlett","Dublett","Triplett","Quartett","Quintett","Sextett","Heptett","Octett","Nonett");
   my @statetype=();
   if ($kwords{mcscf}) {print ARCHIV " ============== MCSCF WAVEFUNCTION SPECIFICATION ===============\n";
                       if ($MCSCF_wf{ndrt} && $control{header}) 
                        { for ($i=1; $i<=$MCSCF_wf{ndrt}; $i++) {$thisdrt=$i; 
                          printf ARCHIV " = DRT %-4d states   %-6s          ncsf=%-6d               =\n"
                                  ,$thisdrt, ${$translation{mcscfdrtrev}}[$thisdrt] ,${$MCSCF_wf[$thisdrt]}{ncsf}; 
                          printf ARCHIV " = irreps           %-4s %-4s %-4s %-4s %-4s %-4s %-4s %-4s    =\n",  @{${$molecule{irrep}}{symbols}}[1..8]; 
                          printf ARCHIV " = doubly occ. orb. %-4s %-4s %-4s %-4s %-4s %-4s %-4s %-4s    =\n",@{ ${$MCSCF_wf[$thisdrt]}{DOCC}}[1..8];
                          printf ARCHIV " = active orb.      %-4s %-4s %-4s %-4s %-4s %-4s %-4s %-4s    =\n",@{ ${$MCSCF_wf[$thisdrt]}{ACT}}[1..8];
                          print ARCHIV " =-------------------------------------------------------------=\n";
                         }}
                          print ARCHIV "===================== MCSCF ENERGIES ===========================\n"; 
                       if ($MCSCF_wf{ndrt}) 
                        { for ($i=1; $i<=$MCSCF_wf{ndrt}; $i++) {$thisdrt=$i; 
                           for ($iroot=1; $iroot<100;$iroot++) 
                          { if (${${$MCSCF_wf[$thisdrt]}{roots}}[$iroot] ) {
                          printf ARCHIV " = %2d %-4s(%2d.%-2d)( %4.2f ) E %12.6f au/Erel %6.3f eV     =\n",
                                            $iroot, ${$translation{mcscfdrtrev}}[$i], $thisdrt, $iroot, split(/:/,${${$MCSCF_wf[$thisdrt]}{roots}}[$iroot]); }
                            else {last;}
                          }
                        }
                        }
                       else { $thisdrt=0; die("should not occur!!!\n"); }
    #                  print ARCHIV " =-------------------------------------------------------------=\n";
                      }

  if ($kwords{ciudg}||$kwords{ciudgav} || $kwords{pciudg} || $kwords{pciudgav}) 
                       {print ARCHIV " ============= $MRCI_wf{type}  WAVEFUNCTION SPECIFICATION ==============\n";
                       if ($MRCI_wf{ndrt} ) 
                        { for ($i=1; $i<=$MRCI_wf{ndrt}; $i++) {$thisdrt=$i;
                          
                          if ($control{header}) {
                          printf ARCHIV " = DRT %-2d states=%-6s  niot=%-3d nmot=%-3d nfct=%-3d            =\n"
                                  ,$thisdrt, ${$translation{cidrtrev}}[$thisdrt] , ${$MRCI_wf[$thisdrt]}{niot},${$MRCI_wf[$thisdrt]}{nmot},${$MRCI_wf[$thisdrt]}{nfct};
                          printf ARCHIV " = ncsf= %-12d                                          =\n",${$MRCI_wf[$thisdrt]}{ncsf} ; 
                          printf ARCHIV " = irreps           %-4s %-4s %-4s %-4s %-4s %-4s %-4s %-4s    =\n",  @{${$molecule{irrep}}{symbols}}[1..8];
                          printf ARCHIV " = internal  orb.   %-4s %-4s %-4s %-4s %-4s %-4s %-4s %-4s    =\n",@{ ${$MRCI_wf[$thisdrt]}{intern}}[1..8];
                          printf ARCHIV " = external  orb.   %-4s %-4s %-4s %-4s %-4s %-4s %-4s %-4s    =\n",@{ ${$MRCI_wf[$thisdrt]}{extern}}[1..8];
                          printf ARCHIV " = frozen    orb.   %-4s %-4s %-4s %-4s %-4s %-4s %-4s %-4s    =\n",@{ ${$MRCI_wf[$thisdrt]}{frozen}}[1..8];
                          printf ARCHIV " = non-frz   orb.   %-4s %-4s %-4s %-4s %-4s %-4s %-4s %-4s    =\n",@{ ${$MRCI_wf[$thisdrt]}{nmo}}[1..8];
                          print ARCHIV " =                                                             =\n"; }
                          for ($iroot=1; $iroot<100; $iroot++) 
                           { if (${${$MRCI_wf[$thisdrt]}{roots}}[$iroot] ) 
                             { printf ARCHIV " = %2d %-4s(%2d.%-2d) etot= %12.6f au                        =\n", 
                                       $iroot,${$translation{cidrtrev}}[$thisdrt],$thisdrt,$iroot,${${MRCI_wf[$thisdrt]}{roots}}[$iroot];  }
                             #else {last;} energy file actually only lists those roots and transitions for which a density is computed 
                           }
                         }}
                         else { $thisdrt=0; die("should not occur!!!\n");}
                       }
      if ($kwords{gdiis}){ print ARCHIV " =----------------------geometry convergence ------------------=\n";
                          printf ARCHIV " = max-step   %10.6f au  (%10.6f A)                    =\n", $geomconv{maxstep},$geomconv{maxstep}*0.52917;
                          printf ARCHIV " = rms-step   %10.6f au  (%10.6f A)                    =\n", $geomconv{rmsstep},$geomconv{rmsstep}*0.52917;
                          printf ARCHIV " = max-force  %10.6f au                                    =\n", $geomconv{maxforce};
                          printf ARCHIV " = rms-force  %10.6f au                                    =\n", $geomconv{rmsforce}; }
     print ARCHIV " =-------------------------------------------------------------=\n";

     close(ARCHIV);
 }



