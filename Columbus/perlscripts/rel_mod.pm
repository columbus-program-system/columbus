  package rel_mod;
#  require Exporter;
#  @ISA       =qw(Exporter);
#  @EXPORT    =qw();
  use Cwd;
  use CPAN::Shell qw(pwd cd date mv cat);
  use lib join('/',$ENV{"COLUMBUS"},"CPAN") ;
  use colib_perl;
  use File::Copy 'cp';  # use perl cp
                        # use perl unlink instead of rm
                        # use perl rename instead of mv

  sub prep_drts
  { my ($nsym,$hmult,$root,$symlabel,$coremem,$socihisym)=@_;
    my ($listofdrts,$listofsodrts); 
     print "Preparing SO cidrtin files for $nsym symmetries\n";
     for ($irrepx=1; $irrepx<=$nsym; $irrepx++)
    { print "cat ../cidrtin | sed -e 's/^.*input the molecular spatial symmetry/$irrepx \\\/ molsym/' > cidrtin.so_${$symlabel}[$irrepx] \n";
     system("cat cidrtin | sed -e 's/^.*input the molecular spatial symmetry/$irrepx \\\/ molsym/' > cidrtin.so_${$symlabel}[$irrepx] ");
     callprog2("cidrt.x -m $coremem < cidrtin.so_${$symlabel}[$irrepx] > cidrtls.soci_${$symlabel}[$irrepx]");
#    callprog2("cidrt.x -m $coremem -s 1 < cidrtin.so_${$symlabel}[$irrepx] > cidrtls.soci_${$symlabel}[$irrepx]");
     system ("mv cidrtfl cidrtfl.so__${$symlabel}[$irrepx]"); 
    }

    #my $reindexroot=join(',',(1..$nsym));
     my $tmp=$socihisym; $tmp=~s/:/,/g; ($rtirrep,$soirrep)=split('%',$tmp); 

      print "Checking the DRTs for consistency ...\n";
      open FOUT,">reindexin";
      print FOUT " &input \n cidrtflso=\'cidrtfl.so' \n cidrtfl=\'cidrtfl\' \n civflso=\'civfl.so\' \n civfl=\'civfl\'\n";
     #print FOUT " nroot=$reindexroot\n  rtirrep=$rtirrep \n soirrep=$soirrep\n &end\n "; 
      print FOUT " rtirrep=$rtirrep \n soirrep=$soirrep\n &end\n "; 
      close FOUT;
      
      callprog2("reindex.x -i 1 -m $coremem ");

      ($listofdrts,$listofsodrts)=rel_mod::getreindexinfo("reindexls");
      return ($listofdrts,$listofsodrts);
  }
                        
  sub printmatrix
  { my ($ht,$n,$unit)=@_; 
    if ($unit eq "au") { print "-------------- HT matrix in AU ----------------  \n"; 
                         for ($i=1; $i<=$n; $i++ ) 
                         { for ($j=1; $j<=$i; $j++) { printf "%9.6f    ", " $$ht[$i][$j]  "; }
                           print "\n";}}
    if ($unit eq "eV") { print "-------------- HT matrix in eV ---------------- \n"; 
                         for ($i=1; $i<=$n; $i++ ) 
                         { for ($j=1; $j<=$i; $j++) { $tmp1=$$ht[$i][$j] * 27.2113845; printf "%9.3f   ", " $tmp1 "; }
                           print "\n";}}
    if ($unit eq "cm") { print "-------------- HT matrix in cm-1 ------------------- \n";
                         $eref = $$ht[1][1];
                         print "Eref = $eref\n"; 
                         for ($i=1; $i<=$n; $i++ ) 
                         {
                            for ($j=1; $j<$i; $j++) { $tmp1=$$ht[$i][$j] * 219475; printf "%9.2f   "," $tmp1 ";}
                            $tmp1=($$ht[$i][$i] - $eref)*219475; printf "%9.2f   "," $tmp1 ";
                           print "\n";
                         }
                        }
     return;}

  
  sub printmatrix_labelled
  { my ($ht,$n,$unit,$labels,$title)=@_;
    if ($unit eq "au") { print "-------------- $title in AU   ----------------  \n";
                         for ($i=1; $i<=$n; $i++ )
                         {printf "%-10s ", $$labels[$i-1]; for ($j=1; $j<=$i; $j++) { printf "%9.6f    ", " $$ht[$i][$j]  "; }
                           print "\n";}}
    if ($unit eq "eV") { print "-------------- $title in eV ---------------- \n";
                         for ($i=1; $i<=$n; $i++ )
                         {printf "%-10s ", $$labels[$i-1];
                          for ($j=1; $j<=$i; $j++) { $tmp1=$$ht[$i][$j] * 27.2113845; printf "%9.3f   ", " $tmp1 "; }
                           print "\n";}}
    if ($unit eq "cm") { print "-------------- $title in cm-1 ------------------- \n";
                         $eref = $$ht[1][1];
                         print "Eref = $eref\n";
                         for ($i=1; $i<=$n; $i++ )
                         { printf "%-10s ", $$labels[$i-1];
                            for ($j=1; $j<$i; $j++) { $tmp1=$$ht[$i][$j] * 219475; printf "%9.2f   "," $tmp1 ";}
                            $tmp1=($$ht[$i][$i] - $eref)*219475; printf "%9.2f   "," $tmp1 ";
                           print "\n";
                         }
                        }
     return;}

                        
  sub getreindexinfo 
  { my ($fname,@tmp,@listofdrts,$ndrts,@listofsodrts,$nsodrts); 
    $fname=shift @_;
    @listofdrts=();
    @listofsodrts=();
    open FIN,"<$fname";
    while (<FIN>) {if (/INTERFACE TO RUNC/) {last;}}
    while (<FIN>) {if (/LIST OF DRTS/) { $ndrts=<FIN>; chop $ndrts; $ndrts=~s/ //g; last;}
                   chop; s/^ *//; @tmp=split(/\s+/,$_); 
                   push @listofsodrts,$tmp[0];}
    while (<FIN>) {if (/END OF INTERFACE TO RUNC/) {last;}
                    chop; s/^ *//; @tmp=split(/\s+/,$_);
                    push @listofdrts, join(':',@tmp); } 
    close FIN; 
    if ($ndrts-1 != $#listofdrts) {die("inconsistent reindex -i  ($fname) $ndrts versus $#listofdrts");}
    return (\@listofdrts,\@listofsodrts)
   }
  
    sub makecidrt
    { my ($imult,$irrep,$symlabel,$nroot,$fcidrtinso,$fciudgin,$aqcc,$rtol,$drtind,$lrtshift,$nitero,$nsym); 
      ($imult,$irrep,$symlabel,$nroot,$fcidrtinso,$fciudgin,$aqcc,$rtol,$drtind,$lrtshift,$nsym) = @_;
      open FIN,"<$fcidrtinso"; 
      $sirrep=${$symlabel}[$irrep];
#     print "makecidrt  cidrtin.${imult}_${sirrep}  fcidrtinso=$fcidrtinso \n";
      if ($drtind == 0)
      { # naming in relativistic.pl
        $cidin = "cidrtin.${imult}_${sirrep}";
        $ciuin = "ciudgin.${imult}_$sirrep";
      }
      else
      { # names for ciudgav in runc
        $cidin = "cidrtin.$drtind";
        $ciuin = "ciudgin.drt$drtind";
      }
      
      open FOUT,">$cidin"; 
      while (<FIN>)
      { if (/Spin-orbit CI calculation/i) {<FIN>; <FIN>; print FOUT " n /Spin-Orbit CI Calculation? \n $imult / input the spin multiplicity \n";  next;}
        if (/input the molecular spatial symmetry/) { print FOUT " $irrep /input the molecular spatial symmetry \n"; next; } 
        if (/allowed reference symmetries/) { for ($i=1; $i<=$nsym; $i++) {print FOUT "$i "; } print FOUT " /allowed reference symmetries \n"; next; }
        print FOUT; }
      if ($nroot) {
                    $rtolins=$rtol x $nroot; $rtolins=~s/,$//;
                    if ($aqcc == 1 ) { changekeyword($fciudgin,"$ciuin","nroot",1); 
                                       changekeyword("$ciuin","$ciuin","gset",3);
                                       changekeyword("$ciuin","$ciuin","ntype",3);}
                    elsif ($aqcc == 2 ) { changekeyword($fciudgin,"$ciuin","nroot",$nroot); 
                                       changekeyword("$ciuin","$ciuin","gset",3);
                                       changekeyword("$ciuin","$ciuin","ntype",3);
                                       changekeyword("$ciuin","$ciuin","lrtshift",$lrtshift);}
                    else      {changekeyword($fciudgin,"$ciuin","nroot",$nroot);}
                    changekeyword("$ciuin","$ciuin","rtolci",$rtolins);
                    #changekeyword("$ciuin","$ciuin","ivmode",3);
                    changekeyword("$ciuin","$ciuin","update_mode",1); 

                    $nvcimxo = keyinfile("$ciuin","nvcimn");
                    if ($nvcimxo < $nroot+1) {changekeyword("$ciuin","$ciuin","nvcimn",$nroot+1);}
                    $nvcimxo = keyinfile("$ciuin","nvrfmn");
                    if ($nvcimxo < $nroot+1) {changekeyword("$ciuin","$ciuin","nvrfmn",$nroot+1);}
                    $nvcimxo = keyinfile("$ciuin","nvbkmn");
                    if ($nvcimxo < $nroot+1) {changekeyword("$ciuin","$ciuin","nvbkmn",$nroot+1);}
                    $nvcimxo = keyinfile("$ciuin","nvcimx");
                    if ($nvcimxo < $nroot+4) {changekeyword("$ciuin","$ciuin","nvcimx",$nroot+4);}
                    $nvcimxo = keyinfile("$ciuin","nvrfmx");
                    if ($nvcimxo < $nroot+4) {changekeyword("$ciuin","$ciuin","nvrfmx",$nroot+4);}
                    $nvcimxo = keyinfile("$ciuin","nvbkmx");
                    if ($nvcimxo < $nroot+4) {changekeyword("$ciuin","$ciuin","nvbkmx",$nroot+4);}
                    $nitero = keyinfile("$ciuin","niter");
                    if ($nitero < $nroot*30) {changekeyword("$ciuin","$ciuin","niter",$nroot*30);}
       }
      return;
     } 
     
  sub makeciudgin_so
  { my ($molcas,$aqccrefstate,$lrtshift,$noldv,$maxroot,$onlypert,$rtolins)=@_; 
  
   if ($molcas) { changekeyword("ciudgin","ciudgin","molcas",1);}   

   if ($aqccrefstate) 
   { changekeyword("ciudgin","ciudgin","gset",3);
     changekeyword("ciudgin","ciudgin","ntype",3);
     changekeyword("ciudgin","ciudgin","lrtshift",$lrtshift);
   }
   changekeyword("ciudgin","ciudgin","ivmode",1);
   changekeyword("ciudgin","ciudgin","iden",1);
   changekeyword("ciudgin","ciudgin","noldv",$noldv);
   if ($noldv > $maxroot ) { changekeyword("ciudgin","ciudgin","nroot",$maxroot);}
                 else      { changekeyword("ciudgin","ciudgin","nroot",$noldv);}
   if ($onlypert) 
    {
   changekeyword("ciudgin","ciudgin","nvcimx",$noldv+2);
   changekeyword("ciudgin","ciudgin","nvbkmx",$noldv+2);
   changekeyword("ciudgin","ciudgin","nvrfmx",$noldv+2); 
   changekeyword("ciudgin","ciudgin","iden",0);
   changekeyword("ciudgin","ciudgin","finalv",-1);
   changekeyword("ciudgin","ciudgin","finalw",-1); }
   else {
   changekeyword("ciudgin","ciudgin","nvcimx",$noldv*3);
   changekeyword("ciudgin","ciudgin","nvbkmx",$noldv*3);
   changekeyword("ciudgin","ciudgin","nvrfmx",$noldv*3); }
   if ($onlypert) { changekeyword("ciudgin","ciudgin","niter",1); }
            else  { changekeyword("ciudgin","ciudgin","niter",$noldv*20); }
   changekeyword("ciudgin","ciudgin","nbkitr",0);
   changekeyword("ciudgin","ciudgin","rtolci",$rtolins);
   changekeyword("ciudgin","ciudgin","update_mode",1);
  }
  
  sub getcidrtflinfo 
  { my (@mult, $fname);
    $fname=shift @_;
    $/="\n";
    open FIN,"<$fname" || die ("could not find $fname\n");
     my($spnodd,$info,@info,$slabel,@tmp);
     my($validzwalks,$spnodd,$nsym,$hmult,@irrep);
    <FIN>;
    $_=<FIN>; chop; s/^ *//; @tmp=split(/\s+/,$_);  # spnorb, spnodd, lxyzir,hmult 
    $hmult=$tmp[5];
    $spnodd=($tmp[1] eq "T");
    <FIN>; <FIN> ;  #cidrt
    <FIN>;
    $info=" ";#  nmot,niot, nfct, nfvt, nrow, nsym, ssym ,xbar(1,1),xbar(2,2),xbar(3,3),xbar(4,4),nvalw(1:4),ncsft
    while(<FIN>) { if (/slabel/) {last;} chop; $info=$info . $_; }
    $info=~s/^ *//; @info=split(/\s+/,$info);
    $slabel=<FIN>; chop $slabel; $slabel=uc($slabel);  @irrep=split(/\s+/,$slabel);
    close(FIN);
    $nsym=$info[5];
    $validzwalks=$info[11];
#   print "getcidrtflinfo: $validzwalks, INFO=",join(' : ',@info),"\n"; 
    return ($validzwalks,$spnodd,$nsym,$hmult,\@irrep);}

  
   sub  extractenergies{
    ($nr,$fname,$type)=@_;
     open FN,"<$fname" || die("could not find $fname\n");
     my $iroot=0;
     my @roots =();
     if ($type eq "ci" )
     { while (<FN>)
       { if (/^ eci     /) {$iroot++; chop;  s/delta.*$//; s/^.*=//; s/ //g; push @roots, $_;}} 
       if ($iroot != $nr ) {die("inconsistent number of ci roots on $fname: $nr versus $iroot \n"); }
     }
     elsif ($type eq "aqcc" )
     { while (<FN>)
       { if (/^ final mraqcc/) {last;}}
       while(<FN>)
       { if (/mraqcc/) {$iroot++; chop; s/^ *//;  @tmp=split(/\s+/,$_);  push @roots, $tmp[4];}} 
       if ($iroot != $nr ) {die("inconsistent number of aqcc roots on $fname: $nr versus $iroot \n"); }
     }

     return \@roots;} 

   sub analyse
   { my ($nsym,$hmult,$root,$symlabel)=@_;


   # 1. collect all data from ciudgls.so__*
   #    into @combined_htm

   my @combined_htm=();
   my @combined_eig=();
   for ($irrep=1; $irrep<=$nsym; $irrep++)
   { $sirrep = ${$symlabel}[$irrep] ;
     if ( ! -f "ciudgls.so__$sirrep" ) {next;} 
    print "************* Results for Spin-Orbit CI gamma=$sirrep **************** \n"; 
     open FIN,"<ciudgls.so__$sirrep";
#    while (<FIN>) {if (/total core repulsion energy/) {s/^.*=//; s/E/e/g; chop; $totcore=$_;}}
     #@htmatrix=();
     while (<FIN>) { if (/starting ci iteration   1/) {last;}}
     while (<FIN>) { if (/ Final subspace hamiltonian/) { <FIN>;last;}}
     $htblock = 0;
     $read = 0;
     my @eigenfunctions=();
     my @htm=();
     while (<FIN>) {
        if (/ht /) {
            if ($read) {
                @tmp=split(/\s+/,$_);
                $i = $tmp[2];
                #print "i: $i\n";
                for ($j=1; $j<=$i; $j++) {
                    $htm[$i][$j + 8*$htblock]=$tmp[$j+2];
                    if ($j eq 8) {last;}
                } 
            }
            else {$read = 1;}
        }
        elsif (/Spin-Orbit CI: perturb/) {s/^ *//; push @eigenfunctions,$_; <FIN>; last;} # go to the next block if more than eight roots are computed
        else { $htblock += 1;$read=0;}
     }
#    while (<FIN>) { if (/Spin-Orbit CI: perturb/) { s/^ *//; push @eigenfunctions,$_; <FIN>; last; }}
     while (<FIN>) { if (/iter/) {last;}
                       push @eigenfunctions, $_; }
     $cnt=0; $cntd=0;
     while (<FIN>) { if (/^ eci      /) {$cnt++; s/ *delta.*$//; s/^ eci *=//; chop; $eci[$cnt]=$_; }
                     if (/^ eci\+dv3 /) {$cntd++; s/ *dv3 =.*$//; s/^.*=//; chop; $dv3[$cnt]=$_; }} 
     close FIN;
#    print "subspace matrix dimension = ",$#htmatrix+1,"\n"; 
     print "Final spin-orbit CI state energies: \n";
     for ($i=1; $i<=$cnt; $i++) { print " MRCISD ($i) = $eci[$i]\n"; }
     for ($i=1; $i<=$cnt; $i++) { print " MRCISD+DV3 ($i) = $dv3[$i]\n"; }
     print "\n\n"; 
     
     rel_mod::printmatrix(\@htm,$#htm,"au"); 
     rel_mod::printmatrix(\@htm,$#htm,"eV"); 
     rel_mod::printmatrix(\@htm,$#htm,"cm"); 

     push @combined_htm, \@htm;     

     print "\n\n"; 
     push @combined_eig, \@eigenfunctions;
     print @eigenfunctions; 
   }

   # 2. extract INTERFACE2 from reindexls, to get model space characterization

     print " model space basis from reindexls .. \n"; 
     open FIN,"<reindexls" || die("could not open reindexls !\n");
     while (<FIN>) {if (/INTERFACE2 TO RUNC/) {last;}}
     $_=<FIN>; print; $_=<FIN>;  print;
     @characteristics=();
     while (<FIN>) { if (/INTERFACE2 TO RUNC/) {last;}; 
                     print;chop; s/^\s+//; push @characteristics, $_;}
     #  --SO-CI-------         -----NR-CI-------             ---model space----
     # Gamma  root             S   irrep   root                S     ms*
     #  AG      1             1      1      1                 1   0+

     print "\n"; 
     close FIN; 

     # collect labels (model space basis)  |S,ms*;S,irrep,root>
       while ($#characteristics > -1) 
        {@x = split(/\s+/,shift @characteristics );
       # print "|".$x[5].",".$x[6].";".$x[2].",".$x[3].",".$x[4]. ">\n"; 
         if (! $labels{$x[0]}) {my @tmp=();$labels{$x[0]}=\@tmp; }
         push @{$labels{$x[0]}}, "|".$x[5].",".$x[6].";".$x[2].",".$x[3].",".$x[4].">";}

   print "************************** SUMMARY *********************************\n";

   # 3. print joined matrices with appropriate model space labels
    $icnt=0;
    for ($irrep=1; $irrep<=$nsym; $irrep++)
        { $sirrep = ${$symlabel}[$irrep] ;
          if ($labels{$sirrep}) 
            {$thishtm=$combined_htm[$icnt];$icnt++;  
             print "\n";
             printmatrix_labelled($thishtm,$#$thishtm,"eV",$labels{$sirrep}," SO matrix for irrep $sirrep");}
        } 
    $icnt=0;
    for ($irrep=1; $irrep<=$nsym; $irrep++)
        { $sirrep = ${$symlabel}[$irrep] ;
          if ($labels{$sirrep})
            {$thishtm=$combined_htm[$icnt];$icnt++;
             print "\n";
             printmatrix_labelled($thishtm,$#$thishtm,"cm",$labels{$sirrep}," SO matrix for irrep $sirrep");}
        }

    #4.  print eigenvectors with proper labels
     print "\n\n ** perturbational estimate of Spin-Orbit CI energies and wavefunctions in model space basis** \n\n";  
    
    $icnt=0;
    $energies="";
    for ($irrep=1; $irrep<=$nsym; $irrep++)
        { $sirrep = ${$symlabel}[$irrep] ; 
          if ($labels{$sirrep})
            {$thiseig=$combined_eig[$icnt];$icnt++;
             $icnt2=0;
             while ($#$thiseig >-1) 
              {$_=shift @$thiseig;
               if (/Spin-Orbit CI:/) {next;}
               if (/SO/) { $icnt2=0; s/SO /$sirrep/g; s/^ /       /;print; next;}
               if (/^ *energy /) { s/^ /          /; print;  chop; s/^ *energy\s+/ /; s/ *$//; 
                           $energies=$energies . $_;  next;}
               if (/NR/) {# my @tmp=split(/\s+/,$_); if ($tmp[1]=="NR") {$icnt2=$tmp[2];} 
                           $s=${$labels{$sirrep}}[$icnt2]; $icnt2++; s/NR \s+\d+/$s/;  print;  next;} 
                s/^ /          /;
               print;
              }}
        }

     @energies= sort numeric split(/\s+/,$energies);
     print "Sorted Energies:\n";
     for ($i=0; $i<$#energies; $i++) {print $i+1,"  $energies[$i] \n"; }
   }
  
   sub numeric { $a <=> $b;}
1;
