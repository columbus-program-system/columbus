#!/usr/bin/env colperl
#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/

#
#  converts the contents of the geom file into molden format
#  usage:   col2molden.pl > molden.input
#

  open GEOM,"<geom" || die ("could not open geom\n");

  $natom=0;
  while (<GEOM>)
  { $natom++;
    s/^ *//g; chop;
    if (/^$/ || /^ *$/) {next;}
   ($label,$charge,$x,$y,$z,$mass)=split(/\s+/,$_);
   push @label,$label;
   push @x, $x;
   push @y, $y;
   push @z, $z;
  }
    

  close GEOM;

 $bohrtoau=0.529177;

  print  "$natom\n";
  print  "# comment \n";
  while (@label)
   { $label=shift @label;
     $x =shift @x;
     $y =shift @y;
     $z =shift @z;
     printf " %2s  %10.6f   %10.6f  %10.6f \n", $label,$x*$bohrtoau,$y*$bohrtoau,$z*$bohrtoau;
  }
  close MOLDEN;

