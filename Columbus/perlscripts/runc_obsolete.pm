package  runc_obsolete;
use Exporter();
@ISA       =qw(Exporter);
@EXPORT    =qw(cid2trold);
use runc_globaldata;
use File::Copy 'cp';  # use perl cp
use Cwd;
use Shell qw(pwd cd date mv cat);


#
#---------------------------------------------------------------------------
#
sub cid2trold {
  local ($i,@x,$cnt,$mcscf,$scf,$pciden);
  local ($kdrt,$kstate,$bdrt,$bstate,$energy,$energy1,$energy2);
  ($pciden)=@{$_[0]};

# Now phaseci and cirestart run at the end of this subroutine. MB Sep 2005
#
# if the symmetric two-particle transition density is required, this
# implicitly imposes the restriction $naddrt1==$naddrt2 !
#

# loop for all nonadiabatic coupling listed in the file transmomin
  print "\n\n +++++++++++ STARTING GRADIENTS AND NAD. COUPLINGS CALCULATION +++++++++++ \n\n";

  @calculated_grad=0;
  for ($inad=1;$inad<=$nnadcoupl;$inad++) {
    $naddrt1=$nad_drt1[$inad];
    $naddrt2=$nad_drt2[$inad];
    $nadstate1=$nad_state1[$inad];
    $nadstate2=$nad_state2[$inad];
    print "\n -------- GRAD. AND NAD. COUPLINGS for DRT:$naddrt1 STATE:$nadstate1 ";
    print "AND DRT:$naddrt2 STATE:$nadstate2 --------\n";

# extract the energies for the 2 states of interest:
    open (EFILE,">energy") or die "Cannot open file: energy";
    $energy1=&getcienergy("ciudgls.drt$naddrt1.sp",$nadstate1);
    $energy2=&getcienergy("ciudgls.drt$naddrt2.sp",$nadstate2);
    $deltae=$energy1-$energy2;
    print EFILE "$energy1   $energy2   $deltae\n";
    $nroot=&keyinfile("$direct{JDIR}/ciudgin.drt$naddrt1","nroot");
    print EFILE "$nroot\n";
    for ($i=1; $i<=$nroot; $i++){
      $energy=&getcienergy("ciudgls.drt$naddrt1.sp",$i); print EFILE "$energy   ";
    }
    print EFILE "\n";
    close EFILE;
    &appendtofile("energy","$direct{list}/energy.all",$control{iter});
    cp("energy","$direct{list}/energy");
    cp ("civfl.drt$naddrt1","civfl") or die"Cound not copy file:civfl.drt$naddrt1\n";
    cp ("civout.drt$naddrt1","civout")or die"Cound not copy file:civout.drt$naddrt1\n";
    cp ("cidrtfl.$naddrt1","cidrtfl")or die"Cound not copy file:cidrtfl.$naddrt1\n";

# state 1
  print"\n *** STATE1 ***\n";
#  $actual_state="$naddrt1"."$nadstate1";
#  if ($calculated_grad{$actual_state}==1){
#    print "\n Gradient for DRT:$naddrt1 STATE:$nadstate1; Energy=$energy1 already done.\n\n";
#  }
#  else
#??  {
    print"\n Calculating density for: DRT:$naddrt1 STATE:$nadstate1; Energy=$energy1\n";
    if ($kwords{"gdiis"}||$kwords{"polyhes"}) {
      $calculated_grad{$actual_state}=0;
    }
    else{
      $calculated_grad{$actual_state}=1;
    }

      &changekeyword("$direct{JDIR}/cidenin","cidenin","lroot",$nadstate1);
      if ( $pciden){
        pcallprog ( "pciden.x -m $pcoremem ");
        &appendtofile("pcidenls","$direct{list}/cidenls.drt$naddrt1.state$nadstate1.all",$control{iter});
      }
      else{
        callprog("ciden.x -m $control{coremem}");
        &appendtofile("cidenls","$direct{list}/cidenls.drt$naddrt1.state$nadstate1.all",$control{iter});
      }
      unlink (<flrd*>, <flwd*>,<fil*>,"flacpfd","ciflind");

      print" Calculating CI gradient for DRT:$naddrt1, State; $nadstate1\n";

      &changekeyword("$direct{JDIR}/cigrdin","cigrdin","nadcalc",0);
      callprog("cigrd.x -m $control{coremem}");
      &appendtofile("cigrdls","$direct{list}/cigrdls.drt$naddrt1.state$nadstate1.all",$control{iter});
      mv ("cid1fl","cid1fl.drt$naddrt1.state$nadstate1");
      mv ("cid2fl","cid2fl.drt$naddrt1.state$nadstate1");
      unless (rename("effd1fl","modens")) {die ("rename error $!\n");}
      unless (rename("effd2fl","modens2")){ die ("rename error $!\n");}

# molcas change start

      &effden_tran($kwords{"alaska"},"drt$naddrt1.state$nadstate1",1);
      if ( $kwords{"alaska"} ) { &cartgrd_alaska("drt$naddrt1.state$nadstate1",0); }
                         else  { &cartgrd_dalton("drt$naddrt1.state$nadstate1",0);}
# molcas change end
      &appendtofile("cartgrd","$direct{grad}/cartgrd.drt$naddrt1.state$nadstate1.all",$control{iter});
      cp("cartgrd","cartgrd.drt$naddrt1.state$nadstate1");
      cp ("cartgrd","$direct{grad}/cartgrd.drt$naddrt1.state$nadstate1.sp");
      &transfgrad(".drt$naddrt1.state$nadstate1","grd",0);
      cp("intgeom","$direct{geom}/intgeom.sp");
      cp("intgrd","$direct{grad}/intgrd.drt$naddrt1.state$nadstate1.sp");
      &appendtofile("intgeom","$direct{geom}/intgeom.all",$control{iter});
#??    }

# state 2
    cp ("civfl.drt$naddrt2","civfl") or die"Cound not copy file:civfl.drt$naddrt2\n";
    cp ("civout.drt$naddrt2","civout")or die"Cound not copy file:civout.drt$naddrt2\n";
    cp ("cidrtfl.$naddrt2","cidrtfl")or die"Cound not copy file:cidrtfl.$naddrt2\n";

    print"\n *** STATE2 ***\n";
#   $actual_state="$naddrt2"."$nadstate2";
#   if ($calculated_grad{$actual_state}==1){
#     print "\n Gradient for DRT:$naddrt2 STATE:$nadstate2; Energy=$energy2 already done. \n\n";
#   }
#   else 
#??     {
      print"\n Calculating density for: DRT:$naddrt2 STATE:$nadstate2; energy=$energy2\n";
      if ($kwords{"gdiis"}||$kwords{"polyhes"}) {
        $calculated_grad{$actual_state}=0;
      }
      else{
        $calculated_grad{$actual_state}=1;
      }

      &changekeyword("$direct{JDIR}/cidenin","$direct{work}/cidenin","lroot",$nadstate2);
      if ( $pciden) {
        pcallprog ( "pciden.x -m $pcoremem ");
        &appendtofile("pcidenls","$direct{list}/cidenls.drt$naddrt2.state$nadstate2.all",$control{iter});
      }
      else{
        callprog("ciden.x -m $control{coremem}");
        &appendtofile("cidenls","$direct{list}/cidenls.drt$naddrt2.state$nadstate2.all",$control{iter});
      }
      unlink (<flrd*>,<flwd*>,<fil*>,"flacpfd","ciflind");
      print" calculate CI gradient for: $nadstate1\n";

      &changekeyword("$direct{JDIR}/cigrdin","$direct{JDIR}/WORK/cigrdin","nadcalc",0);
      callprog("cigrd.x -m $control{coremem}");
      &appendtofile("cigrdls","$direct{list}/cigrdls.drt$naddrt2.state$nadstate2.all",$control{iter});
      mv ("cid1fl","cid1fl.drt$naddrt2.state$nadstate2");
      mv ("cid2fl","cid2fl.drt$naddrt2.state$nadstate2");

      unless (rename("effd1fl","modens")) {die ("rename error $!\n");}
      unless (rename("effd2fl","modens2")){ die ("rename error $!\n");}


# molcas change start
      &effden_tran($kwords{"alaska"},"drt$naddrt2.state$nadstate2",2);
     if ( $kwords{"alaska"} ) { &cartgrd_alaska("drt$naddrt2.state$nadstate2",0); }
                        else  { &cartgrd_dalton("drt$naddrt2.state$nadstate2",0);}

      &appendtofile("cartgrd","$direct{grad}/cartgrd.drt$naddrt2.state$nadstate2.all",$control{iter});
      cp ("cartgrd","$direct{grad}/cartgrd.drt$naddrt2.state$nadstate2.sp");
      cp("cartgrd","cartgrd.drt$naddrt2.state$nadstate2");
      &transfgrad(".drt$naddrt2.state$nadstate2","grd",0);
      cp("intgrd","$direct{grad}/intgrd.drt$naddrt2.state$nadstate2.sp");
#??    }

##  1+2 state
#   sum the two states

#
#  unnecessary copy operations result in many GB data transfer! (tm)
#
    print"\n Calculating the sum CI vectors:\n";
    print" DRT:$naddrt1,state:$nadstate1 + DRT:$naddrt2,state:$nadstate2\n";

    open SUMIN,">sumciin";
#(tm)   print SUMIN " \&input\n vectorin=\"civfl\"\n vectorout=\"civfl.new\"\n";
    print SUMIN " \&input\n vectorin=\"civfl.drt$naddrt1\"\n vectorout=\"civfl\"\n";
    print SUMIN " infoin=\"civout.drt$naddrt1\"\n infoout=\"civout\"\n";
    print SUMIN " vector1=$nadstate1\n vector2=$nadstate2\n lvlprt=2\n \&end \n";
    close SUMIN;

#(tm)   cp ("civfl.drt$naddrt1","civfl") or die"Cound not copy file:civfl.drt$naddrt1\n";
#(tm)   cp ("civout.drt$naddrt1","civout")or die"Cound not copy file:civout.drt$naddrt1\n";
    callprog ("sumcivec.x -m $control{coremem}");
    &appendtofile ("sumcils","$direct{list}/sumcils.all",$control{iter});
#(tm)   cp ("civfl.new","civfl");
#(tm)   cp ("civout.new","civout");

    print"\n Calculating density for:  DRT:$naddrt1,state:$nadstate1 + DRT:$naddrt2,state:$nadstate2\n";
    &changekeyword("$direct{JDIR}/cidenin","cidenin","lroot",1);
    open CIDENIN, ">cidenin.new";
    open CIDENOLD, "<cidenin";
    while ( <CIDENOLD> ){
      if(/lroot/){
        print CIDENIN $_; print CIDENIN "  wrtind=1\n";
      }
      else{
        print CIDENIN $_;
      }
    }
    close CIDENIN;
    close CIDENOLD;
    mv ("cidenin.new","cidenin");

    if ( $pciden){
      pcallprog ( "pciden.x -m $pcoremem ");
      &appendtofile("pcidenls","$direct{list}/cidenls.sum.all",$control{iter});
    }
    else{
      callprog("ciden.x -m $control{coremem}");
      &appendtofile("cidenls","$direct{list}/cidenls.sum.all",$control{iter});
    }
    unlink (<flrd*>,<flwd*>,<fil*>,"flacpfd","ciflind");
    mv ("cid1fl","cid1fl.drt$naddrt1.state$nadstate1+drt$naddrt2.state$nadstate2");
    mv ("cid2fl","cid2fl.drt$naddrt1.state$nadstate1+drt$naddrt2.state$nadstate2");

## calculate symmetric transition density matrix
    open TRNIN, ">trci2denin";
    print TRNIN " \&input\n";
    print TRNIN " prtlvl=10\n";
    print TRNIN " d1fl1=\'cid1fl.drt$naddrt1.state$nadstate1'\n";
    print TRNIN " d1fl2=\'cid1fl.drt$naddrt2.state$nadstate2'\n";
    print TRNIN " d1flsum=\'cid1fl.drt$naddrt1.state$nadstate1+drt$naddrt2.state$nadstate2'\n";
    print TRNIN " d2fl1=\'cid2fl.drt$naddrt1.state$nadstate1'\n";
    print TRNIN " d2fl2=\'cid2fl.drt$naddrt2.state$nadstate2'\n";
    print TRNIN " d2flsum=\'cid2fl.drt$naddrt1.state$nadstate1+drt$naddrt2.state$nadstate2'\n";
    print TRNIN " \&end \n";
    close TRNIN;
    callprog ("trci2den.x -m $control{coremem}");
    &appendtofile("trci2denls","$direct{list}/trci2denls.all",$control{iter});

## calculate antisymmetric transition density matrix (Oct. 2006 MB)
    open (TRCIIN,">trnciin");
    print TRCIIN " &input \n  lvlprt=3, \n ";
    print TRCIIN "  nroot1=$nadstate1, \n  nroot2=$nadstate2 \n ";
    print TRCIIN "  drt1=$naddrt1, \n drt2=$naddrt2 \n &end \n ";
    close TRCIIN ;

    callprog ("transci.x -m $control{coremem} ");

#   Finally calculate the requested nonadiabatic coupling

    cp ("$direct{JDIR}/cigrdin","cigrdin");
# .... Included on Oct 2006 MB
    &changekeyword("cigrdin","cigrdin","drt1",$naddrt1);
    &changekeyword("cigrdin","cigrdin","root1",$nadstate1);
    &changekeyword("cigrdin","cigrdin","drt2",$naddrt2);
    &changekeyword("cigrdin","cigrdin","root2",$nadstate2);
#    ....
#
#   MOLCAS/COLUMBUS case:  nadcalc=0,1 only
#

    $nadcalc=&keyinfile("cigrdin","nadcalc");
# nadcalc=0  no NAD calculation
# nadcalc=1  DCI contribution only
# nadcalc=2  DCSF contribution only
# nadcalc=3  DCI+DCSF contribution
#
    if ($nadcalc == 2){
      callprog("dzero.x > dzerols");
      cp ("dzero","cid1fl.tr");
      cp ("dzero2","cid2fl.tr");
     if ( $kwords{"slapaf"} ) { die("cannot MXS search with slapaf and nadcalc==2\n");}
    }
    callprog("cigrd.x -m $control{coremem}");
    &appendtofile("cigrdls","$direct{list}/cigrdls.nad.drt$naddrt1.state$nadstate1.drt$naddrt2.state$nadstate2.all",$control{iter});
    unless (rename("effd1fl","modens")) {die ("rename error $!\n");}
    unless (rename("effd2fl","modens2")){ die ("rename error $!\n");}

# molcas change start
      &effden_tran($kwords{"alaska"},"nad",3);
     if ( $kwords{"alaska"} ) { &cartgrd_alaska("nad.drt$naddrt1.state$nadstate1.drt$naddrt2.state$nadstate2",1); }
                          else { &cartgrd_dalton("drt$naddrt1.state$nadstate1.drt$naddrt2.state$nadstate2",1); }
# molcas change end
    &appendtofile("cartgrd","$direct{grad}/cartgrd.nad.drt$naddrt1.state$nadstate1.drt$naddrt2.state$nadstate2.all",$control{iter});
    cp("cartgrd","$direct{grad}/cartgrd.nad.drt$naddrt1.state$nadstate1.drt$naddrt2.state$nadstate2.sp");
    cp("cartgrd","cartgrd.nad.drt$naddrt1.state$nadstate1.drt$naddrt2.state$nadstate2");
    &transfgrad(".nad.drt$naddrt1.state$nadstate1.drt$naddrt2.state$nadstate2","grd",0);
    cp("intgrd","$direct{grad}/intgrd.nad.drt$naddrt1.state$nadstate1.drt$naddrt2.state$nadstate2.sp");

    if ($nadcalc>1){
#     calculation of the last term in eqn 49
      &dxcalc;
      $/="\n";
#     cartgrd contains the leading term in eqn 49
      cp("cartgrd","$direct{grad}/cartgrd.grd.drt$naddrt1.state$nadstate1.drt$naddrt2.state$nadstate2");
      open (FILE,"cartgrd") or die"Failed to open file:cartgrd\n";
      $i=1;
      while (<FILE>){
        s/D/E/g;chomp;s/^ *//;($nadx[$i],$nady[$i],$nadz[$i])=split(/\s+/,$_,3);$i++;
      }
      close FILE;

#     nadxfl contains the contribution of the last term in eq 49, JCP 120(2004) 7322

      open (FILE,"nadxfl") or die"Failed to open file:nadxfl\n";
      $i=1;
      while (<FILE>){
        s/D/E/g;chomp;s/^ *//;($dx[$i],$dy[$i],$dz[$i])=split(/\s+/,$_,3);$i++;
      }
      close FILE;

      $ndim=$i-1;
      open  (FILE,">tmp")  or die"Failed to open file:tmp\n";

#   We need some more temporary files. See also printf's below. MB sep 2005

      open  (FILE1,">tm1") or die"Failed to open file:tm1\n";
      open  (FILE2,">tm2") or die"Failed to open file:tm2\n";
      open  (FILE3,">tm3") or die"Failed to open file:tm3\n";
      $norm_nad = 0;
      for ($i=1;$i<=$ndim;$i++){
        $x=$dx[$i]+$nadx[$i]/$deltae;   # total value of eqn 49
        $y=$dy[$i]+$nady[$i]/$deltae;
        $z=$dz[$i]+$nadz[$i]/$deltae;
        $nadxtmp[$i]=$dx[$i]*$deltae+$nadx[$i];  # nad vector scaled by deltae
        $nadytmp[$i]=$dy[$i]*$deltae+$nady[$i];
        $nadztmp[$i]=$dz[$i]*$deltae+$nadz[$i];
        $norm_nad = $norm_nad + $x*$x+$y*$y+$z*$z; # norm of nad vector according to eqn 49
        printf{FILE}( "%15.6E%15.6E%15.6E\n",$x,$y,$z);
        printf{FILE1}( "%15.6E%15.6E%15.6E\n",$dx[$i],$dy[$i],$dz[$i]);
        printf{FILE2}( "%15.6E%15.6E%15.6E\n",$nadx[$i]/$deltae,$nady[$i]/$deltae,$nadz[$i]/$deltae);
        printf{FILE3}( "%15.6E%15.6E%15.6E\n",$nadxtmp[$i],$nadytmp[$i],$nadztmp[$i]);
      }
      $norm_nad = sqrt($norm_nad);
      close FILE;
      close FILE1;
      close FILE2;
      close FILE3;
      cp("tmp","$direct{work}/cartgrd_total");
      cp("tmp","$direct{grad}/cartgrd.nad.drt$naddrt1.state$nadstate1.drt$naddrt2.state$nadstate2.sp");

# Total nonadiabatic coupling vector times DE. MB Sep 2005
      cp("tm3","$direct{work}/cartgrd_total_times_DE");

# ..............................................................................
# This block calls the orthogonalization routine for single point case.
# Variable $thres_de should be defined in some input file. Now it is 1.0 eV.
# It appears once more at the MXS search (below).
# MB Sep 2005
# ..............................................................................
#  orthogonalize g and h (Single point case)
#    called only if nadcalc > 1
      $au2ev = 27.211396;
      $dethres= keyinfile("$direct{work}/control.run","dethres");
      $thres_de = $dethres/$au2ev; # Orthogonalize if DE < $thres_de (hartree)

      print "\n Norm of the nonadiabatic coupling vector: \n";
      print " |h(drt$naddrt1.state$nadstate1,drt$naddrt2.state$nadstate2)|= $norm_nad \n";
      print " DE = $deltae hartree        DE_thres = $thres_de hartree \n\n";
      if ($kwords{"slope"} && (abs($deltae)<abs($thres_de))) {
        print "\n Orthogonalizing g and h vectors: \n";
        $coupf="cartgrd_total_times_DE";      # With this file, h*DE is orthogonalized
        # $coupf="cartgrd_total";                # With this file, h is orthogonalized
        orthogonalize();
        cp("nadvecort","cartgrd");

#   after orthogonalize, divides by DE
        open (FILE,"nadvecort") or die"Failed to open file:nadvecort\n";
        $i=1;
        while (<FILE>){
          s/D/E/g;chomp;s/^ *//;($ntdx[$i],$ntdy[$i],$ntdz[$i])=split(/\s+/,$_,3);$i++;
        }
        close FILE;
        $ndim=$i-1;
        open  (FILE,">tm4") or die"Failed to open file:tm4\n";
        for ($i=1;$i<=$ndim;$i++){
          $ntx=$ntdx[$i]/$deltae;
          $nty=$ntdy[$i]/$deltae;
          $ntz=$ntdz[$i]/$deltae;
          printf{FILE}( "%15.6E%15.6E%15.6E\n",$ntx,$nty,$ntz);
        }
        close FILE;
        cp("tm4","$direct{grad}/cartgrd.nad.drt$naddrt1.state$nadstate1.drt$naddrt2.state$nadstate2.sp");
        cp("cartgrd.nad.drt$naddrt1.state$nadstate1.drt$naddrt2.state$nadstate2","cartgrd.nad.drt$naddrt1.state$nadstate1.drt$naddrt2.state$nadstate2.org");
        cp("tm4","cartgrd.nad.drt$naddrt1.state$nadstate1.drt$naddrt2.state$nadstate2");
        if ($kwords{"nadcoupl"}) {
          &info4molden2();
        }
      }
      else {   # else if ($kwords{"slope"}) {
        if ($kwords{"slope"}) {
          print "\n Too big DeltaE: g and h vectors will not be orthogonalized.. \n \n";
        }         # if (($kwords{"slope"}) {
      }          # end  if ($kwords{"slope"}) {
# ..............................................................................

      &transfgrad("_total","grd",0);
      cp("$direct{grad}/intgrd_total.sp","$direct{grad}/intgrd.nad.drt$naddrt1.state$nadstate1.drt$naddrt2.state$nadstate2.sp");

#   calculate <L> value using the total non-adiabatic coupling elements
#
#   get the <L> values calculated in transci.x program
      open (FILE,"$direct{work}/trncils") or die "Failed to open file:$direct{work}/trncils\n";
      while (<FILE>){
        if (/<L> electro/){
          chomp;s/^ *//;($dum1,$dum2,$lx,$ly,$lz,$dum3)=split(/\s+/,$_,6);
        }
      }
      close FILE;

# do the calculation for <L> value:
        $file="$direct{work}/geom";
        open (FILE,"$file") or die "Failed to open file:$file\n";
        $i=1;
        while (<FILE>){
          chomp;s/^ *//;($dum1,$dum2,$x[$i],$y[$i],$z[$i],$dum3)=split(/\s+/,$_,6);$i++
        }
        close FILE;
        $natom=$i-1;

#     $file="$direct{work}/cartgrd_total";
#     open (FILE,"$file") or die "Failed to open file:$file\n";
#     $i=1;
#      while (<FILE>)
#       {chomp;s/^ *//;($nadx[$i],$nady[$i],$nadz[$i])=split(/\s+/,$_,6);$i++}
#     close FILE;
        $nadlx=0,$nadly=0;$nadlz=0;
        for ($i=1;$i<=$natom;$i++){
          $nadlx=$nadlx + $y[$i]*$nadztmp[$i]- $z[$i]*$nadytmp[$i];
          $nadly=$nadly + $z[$i]*$nadxtmp[$i]- $x[$i]*$nadztmp[$i];
          $nadlz=$nadlz + $x[$i]*$nadytmp[$i]- $y[$i]*$nadxtmp[$i];
        }

      print "\n Cross-check of the non-adiabatic coupling term:\n";
      printf ("  <L>(state1-state2)  :  %14.10f   %14.10f   %14.10f  \n", $lx,$ly,$lz);
      printf ("  dE                  :  %14.10f                      \n", $deltae);
      printf ("  <L>*dE form CIGRD   :  %14.10f   %14.10f   %14.10f\n",$lx*$deltae,$ly*$deltae,$lz*$deltae);
      printf ("  <L>*dE form NADCOUPL:  %14.10f   %14.10f   %14.10f\n",$nadlx,$nadly,$nadlz);
      print " -------------------------------------------------------------------------\n";
      printf ("  Difference          :  %14.10f   %14.10f   %14.10f\n",$lx*$deltae-$nadlx,$ly*$deltae-$nadly,$lz*$deltae-$nadlz);
      print "\n NOTE: This test is meaningless at a point of conical intersection!\n\n";

    } # nadcalc>1

   if ( $kwords{"slapaf"} ) { print "leaving cid2tr prior to orthogonalization\n"; return;}


# ..............................................................................
# This block calls the orthog. and phaseci subroutines for a MXS search.
# Variable $thres_de should be defined in some input file. Now it is 1.0 eV.
# It appears once more at Single Point calculation (above).
# MB Sep 2005
# ..............................................................................
#  orthogonalize g and h (MXS search case)
      $au2ev = 27.211396;
      $dethres= keyinfile("$direct{work}/control.run","dethres");
      $thres_de = $dethres/$au2ev; # Orthogonalize if DE < $thres_de (hartree)

      if ($kwords{"gdiis"}||$kwords{"polyhes"}) {
        if ( $kwords{"slope"} && (abs($deltae)<abs($thres_de)) ) {
          print "\n Orthogonalizing g and h vectors: \n";
          $coupf= "cartgrd.nad.drt$naddrt1.state$nadstate1.drt$naddrt2.state$nadstate2";
          orthogonalize();
        }                   # end   if ($kwords{"slope"} && (abs($deltae)<abs($thres_de)) ) {
        if ( $kwords{"slope"} && (abs($deltae)>=abs($thres_de))) {
          print "\n Too big DeltaE: g and h vectors will not be orthogonalized. \n";
        }                   # end   if ( $kwords{"slope"} && (abs($deltae)>=abs($thres_de))) {
      }                     # end if ($kwords{"gdiis"}||$kwords{"polyhes"}) {

#  calculate the phase factor
      $ivmode= keyinfile("$direct{work}/ciudgin","ivmode");
      if ($ivmode == 4) {
        print " \n Computing the phase: \n";
        callprog("phaseci.x -m $control{coremem}");
        cp("ciphase","$direct{grad}/ciphase.sp");
        &appendtofile("phasecils","$direct{list}/phasecils.all",$control{iter});
      }
# ..............................................................................

  } # end of; for ($inad=1;$inad<=$nnadcoupl;$inad++)

}

############################################################################################################################
