#!/usr/bin/env colperl
#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/

  use POSIX;
########### for segmentation/ressource/timing estimates define node range #############
 $minnodes=20;
 $maxnodes=300;
 $nodesinc=2;
 $maxtaskportion=0.5;
 $typeofcalc="ci";
 $verbose=0;
 
 $callline="tmodel.pl " . join(' ',@ARGV);

  while (1)
  {  $_ = shift @ARGV;
    if ( /-help|-h/ ) 
    { print 
"usage: tmodel.pl options  ciudg.timings_filename \n\n" .
"       options:  -type [bk|ci|rf]    default ci \n" .
"                 -coremem  xxxx      physical core memory in DP per CPU \n" .
"                 -bw a,b             bandwidth in MB/s = a + CPU*b \n" .
"                 -nvmax   xxxx       maximum subspace dimension for performance model \n" .
"                 -fsizes  name       name of the file containing the integral file sizes \n" .
"                 -range   from,to,inc performance model for the processor range from,to inc \n" .
"                 -v                  increase verbosity level                              \n" .
"                 -help, -h           this info                                             \n" .
"                 -maxtasksize  xxx   fraction of the maximum task size of the time of a CI iteration \n" ; 
     die (); }
    if ( /-type/ ) { $typeofcalc= shift @ARGV; next;}
    if ( /-coremem/ ) { $coremem = shift @ARGV; next;}
    if ( /-bw/ ) { $bw=shift @ARGV; ($bandwidtha,$bandwidthb) = split(/,/,$bw); next;}
    if ( /-nvmax/ ) { $nvcimx = shift @ARGV; next;}
    if ( /-fsizes/ ) {$fsizes = shift @ARGV; next;}
    if ( /-range/  ) {$range=shift @ARGV; ($minnodes,$maxnodes,$nodesinc) = split(/,/,$range); next;}
    if ( /-maxtask/ ) {$maxtaskportion=shift @ARGV; next;}
    if ( /-vvv/)        {$verbose=3;next;}
    if ( /-vv/)        {$verbose=2;next;}
    if ( /-v/)        {$verbose=1;next;}
    $timingfile=$_; last;
 }
 open OUTF,">analyse.log";
 print OUTF  " -type $typeofcalc \n";
 print OUTF  " -coremem $coremem \n";
 print OUTF  " -bw $bandwidtha,$bandwidthb\n";
 print OUTF  " -nvmax $nvcimx \n";
 print OUTF  " -fsizes $fsizes \n";
 print OUTF  " ciudg.timing file: $timingfile\n";
 print OUTF  " range $minnodes $maxnodes $nodesinc \n";
 print OUTF  " maxtaskportion $maxtaskportion \n";
 # check for correct usage
  if ( ! grep (/(bk|ci|rf)/,$typeofcalc) ) 
    {die ("typeofcalc: invalid usage: ./tmodel.pl  -type [bk|ci|rf]  -coremem xxx  -bw a,b [-nvmax xx] [-fsizes xx ] <ciudgls>\n");}
 
  if (  grep (/\D/,$coremem) ) 
    {die ("coremem: invalid usage: ./tmodel.pl  -type [bk|ci|rf]  -coremem xxx  -bw a,b [-nvmax xx] [-fsizes xx ] <ciudgls>\n");}

  if ( $bandwidtha && $bandwidthb && ( ! grep (/\d|\./,$bandwidtha,$bandwidthb)) ) 
    {die ("bandwidtha:$bandwidtha;$bandwidthb,invalid usage: ./tmodel.pl  -type [bk|ci|rf]  -coremem xxx  -bw a,b [-nvmax xx] [-fsizes xx ] <ciudgls>\n");}
  
 

 
 prtheader();

 print "**** calling arguments :\n";
 print " $callline \n\n\n"; 

 printf " core memory : %10d DP \n bandwidth : %8.3f -  ncpu *  %8.3f  MB/s \n", $coremem, $bandwidtha,$bandwidthb; 
 print " timings of $typeofcalc computations \n";

 rdcidrtfl();

 open BUM,"<bummer" || die("could not find status file bummer\n"); 
 $_=<BUM>; if (!/normally terminated/) 
           { if (/fatal error encountered/) { die("ciudg.x/pciudg.x have not terminated properly, check ciudgls and error file\n");}
             print STDERR " ciudg.x/pciudg.x have terminated with a warning, still trying to proceed analysing the file ciudg.timings\n";   
             print " ciudg.x/pciudg.x have terminated with a warning, still trying to proceed analysing the file ciudg.timings\n"; } 
 close BUM; 

 print "====================================FILE SIZES ====================================================\n";
 if ( $fsizes )
  { # read filesize info from $fsizes 
    open TMPIN,"<$fsizes" || die ("could not find $fsizes \n");
    $fil4w=-1;
    $fil4x=-1;
    $fil3x=-1;
    $fil3w=-1;
    $ofdgint=-1;
    $diagint=-1;
    while (<TMPIN>) 
    { 
      if (/fil4x/i) { chop;s/fil4x//i; s/\D//g; $fil4x=$_;}
      if (/fil4w/i) { chop;s/fil4w//i;s/\D//g; $fil4w=$_;}
      if (/fil3x/i) { chop;s/fil3x//i;s/\D//g; $fil3x=$_;}
      if (/fil3w/i) { chop;s/fil3w//i;s/\D//g; $fil3w=$_;}
      if (/ofdgint/i) { chop;s/ofdgint//i;s/\D//g; $ofdgint=$_;}
      if (/diagint/i) { chop;s/diagint//i;s/\D//g;$diagint=$_;}
    }
     close TMPIN;

}
 else
 { open TMPIN,"<$timingfile" || die ("could not find ciudg.timings\n");
   while (<TMPIN>) { if (/ CSFs  /) {last;}}
   chop; s/^.*CSFs *//; s/ *$//; my @tmp=split(/\s+/,$_); $cidim=$tmp[4];  
   while (<TMPIN>) { if (/ file sizes /){last;}}
   while (<TMPIN>) {
      if (/fil4x/i) { chop;s/fil4x//i; s/\D//g; $fil4x=$_;}
      if (/fil4w/i) { chop;s/fil4w//i;s/\D//g; $fil4w=$_;}
      if (/fil3x/i) { chop;s/fil3x//i;s/\D//g; $fil3x=$_;}
      if (/fil3w/i) { chop;s/fil3w//i;s/\D//g; $fil3w=$_;}
      if (/ofdgint/i) { chop;s/ofdgint//i;s/\D//g; $ofdgint=$_;}
      if (/diagint/i) { chop;s/diagint//i;s/\D//g;$diagint=$_;}
      if (/nvcimx/i) { chop;s/nvcimx//i;s/\D//g;$nvcimx=$_;}
      if (/nodes/i) { chop;s/nodes//i;s/\D//g;$nodes=$_;}
      if (/---/) {next;}
      if (/nseg0x=/i) { chop;s/nseg0x=//i;s/^ *//; s/^ *$//; ($nseg0xz,$nseg0xy,$nseg0xx,$nseg0xw)=split(/\s+/,$_);} 
      if (/nseg1x=/i) { chop;s/nseg1x=//i;s/^ *//; s/^ *$//; ($nseg1xz,$nseg1xy,$nseg1xx,$nseg1xw)=split(/\s+/,$_);} 
      if (/nseg2x=/i) { chop;s/nseg2x=//i;s/^ *//; s/^ *$//; ($nseg2xz,$nseg2xy,$nseg2xx,$nseg2xw)=split(/\s+/,$_);} 
      if (/nseg3x=/i) { chop;s/nseg3x=//i;s/^ *//; s/^ *$//; ($nseg3xz,$nseg3xy,$nseg3xx,$nseg3xw)=split(/\s+/,$_);} 
      if (/nseg4x=/i) { chop;s/nseg4x=//i;s/^ *//; s/^ *$//; ($nseg4xz,$nseg4xy,$nseg4xx,$nseg4xw)=split(/\s+/,$_);} 
      if (/nsegwx=/i) { chop;s/nsegwx=//i;s/^ *//; s/^ *$//; ($nseg2wxz,$nseg2wxy,$nseg2wxx,$nseg2wxw)=split(/\s+/,$_);} 
      if (/TASKLIST/) {last;}
  }  
  $nsegdgz=$nseg4xz;
  $nsegdgy=$nseg4xy;
  $nsegdgx=$nseg4xx;
  $nsegdgw=$nseg4xw;
  
 print "Filesizes:\n";
 printf "      fil4x: %10d Bytes ( %8.2f MB) \n", $fil4x, $fil4x/(1024*1024);
 printf "      fil3x: %10d Bytes ( %8.2f MB) \n", $fil3x, $fil3x/(1024*1024);
 printf "      fil4w: %10d Bytes ( %8.2f MB) \n", $fil4w, $fil4w/(1024*1024);
 printf "      fil3w: %10d Bytes ( %8.2f MB) \n", $fil3w, $fil3w/(1024*1024);
 printf "    ofdgint: %10d Bytes ( %8.2f MB) \n", $ofdgint, $ofdgint/(1024*1024);
 printf "    diagint: %10d Bytes ( %8.2f MB) \n", $diagint, $diagint/(1024*1024);
 printf " maximum subspace dimension  %3d     \n", $nvcimx;
 printf " number of nodes             %4d    \n", $nodes;
 print "===================================================================================================\n";
 print "=============================== CIUDG.TIMINGS INFO ================================================\n";
 printf " maximum subspace dimension : %4d \n", $nvcimx;
 printf " number of nodes            : %4d \n", $nodes;
 printf " actually used segm (0ext)  : %4d %4d %4d %4d \n", $nseg0xz,$nseg0xy,$nseg0xx,$nseg0xw;
 printf " actually used segm (1ext)  : %4d %4d %4d %4d \n", $nseg1xz,$nseg1xy,$nseg1xx,$nseg1xw;
 printf " actually used segm (2ext)  : %4d %4d %4d %4d \n", $nseg2xz,$nseg2xy,$nseg2xx,$nseg2xw;
 printf " actually used segm (2extwx): %4d %4d %4d %4d \n", 0,0 ,$nseg2wxx,$nseg2wxw;
 printf " actually used segm (3ext)  : %4d %4d %4d %4d \n", $nseg3xz,$nseg3xy,$nseg3xx,$nseg3xw;
 printf " actually used segm (4ext)  : %4d %4d %4d %4d \n", $nseg4xz,$nseg4xy,$nseg4xx,$nseg4xw;
 print "===================================================================================================\n";
    if ($verbose)
    { print "> \n";
      print "> * there are at most 6 concurrent segementation schemes\n";
      print ">   for the contributions of 0-, 1-, 2-, 3- and 4-external integrals \n";
      print ">   plus one segmentation for the WX contribution by the 2-external integrals \n";
      print "> * the input parameters nseg0x,nseg1x,nseg2x,nseg3x,nseg4x and nsegwx determine \n";
      print ">   the MINIMUM segmentation in terms of the number z, y, x, w segments (in that order) \n";
      print ">   provided there is sufficient memory, otherwise the number of segments is increased \n";
      print ">   automatically up to at most MAXSEG = sum of z, y,x and w segments\n";
      print "> \n";
    } 
 
 $noperfm=0;

 if ($fil4x<0 || $fil4w < 0 || $fil3x < 0 || $fil3w < 0 || $ofdgint < 0 || $diagint < 0 ) 
  { print "some filesizes could not be determined => no performance model\n"; $noperfm=1;}

    $time_mult=0;
    $time_multnx=0;
    $time_loop=0;
    $time_sync=0;
    $time_total=0;

    @time_mult=();
    @time_multnx=();
    @time_loop=();
    @time_sync=();
    @time_total=();
    @maxtask=();
    @maxtaskno=();
    @maxwait=(); 
    @minwait=();
    @pdegree=();
    @galocaltime=();
    @ganonlocaltime=();
    @galocalvolume=();
    @ganonlocalvolume=();
    @ga3xtime=();
    @ga4xtime=();
    @ga3xvolume=();
    @ga4xvolume=();
    @tasktypes=();
    @tasktypetimes=();
    @tasktypemult=();
    @tasktypecnt=();
    @tasktypemflop=();
    @tasktypemloop=();
    @tasktypeltime=();
    $gfliploop0xzz=0;
    $gfliploop0xyy=0;
    $gfliploop0xxx=0;
    $gfliploop0xww=0;
    $gfliploop1xyz=0;
    $gfliploop1xyw=0;
    $gfliploop1xyx=0;
    $gfliploop2xxz=0;
    $gfliploop2xwz=0;
    $gfliploop2xyy=0;
    $gfliploop2xxx=0;
    $gfliploop2xww=0;
    $gfliploop2xxw=0;
    $gfliploop3xyw=0;
    $gfliploop3xyx=0;

#  determine number of nodes and csfs

    while (<TMPIN>) 
    {if (/TASK\# BRA\# KET\#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS/)
        {$x=<TMPIN>;
         while (<TMPIN>)
           {if (/------------------/) {last;}
          @x=unpack("A6A3A4A6A17A4A3A2A8A8A11A11A8A8",$_);
#           $tmp= sprintf " %3d  %-8s  %3s ",$x[3] ,$x[4] , $x[5];
            $tmp= sprintf "(%3d,%3d) %3d  %-13s  ",$x[1],$x[2],$x[3] ,$x[4] ; 
            push @tasktypes, $tmp;
            $mnemonics[$x[3]]=$x[4]; $mnemonics[$x[3]]=~s/^ *//;
           }
         last;}
     } 
     print "found ",$#tasktypes," Tasks\n"; 

    if ($cidim != $csftot ) {die("inconsistent ci dimension in ciudgls and cidrtfl: $cidim,$csftot \n");}

    @unsortedtasks=();
    $class[1..110][1..4]=0 x 440;

    while (<TMPIN>) { if (/REDTASK/) {last;}}
    while (<TMPIN>) { if (!/REDTASK/) {last;} }
    $cnt=0;
    $ignore=0;
 TIMINGDAT:   while (<TMPIN>)
    { chop; 
      if (/^ *starting .* iteration/) { s/^ *starting//; s/iteration.*$//; s/ //g;
                                   if ($typeofcalc eq $_ ){$cnt++; $ignore=0;}
                                                   else  { $ignore=1;} }

       if ($ignore) {goto TIMINGDAT;}

#
# some old version has awful format strings
#     1   21    41208.41   5.84   0.061204.80
#

      if (/TIMING STATISTICS PER TASK/) 
           {$x=<TMPIN>; $maxtime=0;
    #       @sorttasks=();
            while (<TMPIN>) { if (/======/) {last;}
#                             s/^ *//;
#                             @x=split(/\s+/,$_);
#old
# 1471   47    1    107.15      0.06      0.00     89.58
#                             @x=unpack("A5A5A5A10A10A10A10",$_);
#                             if ($x[3] > $maxtime) {$maxtime=$x[3];$maxtaskno=$x[0];}
#                             $tasktypetimes[$x[1]]=$tasktypetimes[$x[1]]+$x[6];
#                             $tasktypecnt[$x[1]]++;
#                             $tasktypemflop[$x[1]]+=1;
#                             $tasktypemloop[$x[1]]+=1;    
#                             $tasktypeltime[$x[1]]+=$x[4];

#new
#                             @x=unpack("A5A5A5A7A7A7A7A12A9",$_);
#newer
                              @x=unpack("A5A5A5A9A9A9A9A11A10",$_);
                              if ($x[3] > $maxtime) {$maxtime=$x[3];$maxtaskno=$x[0];}
                              $tasktypetimes[$x[1]]=$tasktypetimes[$x[1]]+$x[6];
                              $tasktypecnt[$x[1]]++;
                              $tasktypemflop[$x[1]]+=$x[7]*$x[6];
                              $tasktypemloop[$x[1]]+=$x[8];
                              $tasktypeltime[$x[1]]+=$x[4];
                              $tasktypemult[$x[1]]+=$x[3];
                              push @{$unsortedtasks[$cnt]},$x[3];
                              if ($cnt == 1 ) {next;}
                              $class[$x[1]][3]++;
                              $class[$x[1]][4]+=$x[6];
                              if ($class[$x[1]][1] == 0 ) { $class[$x[1]][1]=$x[6]; $class[$x[1]][2]=$x[6];}
                                elsif ($class[$x[1]][1] > $x[6] ) { $class[$x[1]][1]=$x[6];}
                                 elsif ($class[$x[1]][2] < $x[6] ) { $class[$x[1]][2]=$x[6];}
    #                         push @sorttasks,$x[6];
                            }
            push @maxtask,$maxtime; 
            push @maxtaskno,$maxtaskno;

    #       @sitems=mysort(\@sorttasks);
            

} 

#  1   6.43   4.64   9.41   0.02   0.00  94.673049.91

      if (/TIMING STATISTICS PER NODE/)
           {$x=<TMPIN>; $maxwait=0;
            while (<TMPIN>) { if (/======/) {last;}
                  #           s/^ *//;
                  #           @x=split(/\s+/,$_);
                              @x=unpack("A3A7A7A7A7A7A7A7",$_);
                              if ($x[1] > $maxwait) {$maxwait=$x[1];}
                            }
            push @maxwait,$maxwait;}




      if (/time spent in mult:/) { s/^.*://; s/ *//g; s/s/ /;
                                  $time_mult+=$_; push @time_mult, $_/$nodes; }
      if (/time spent in multnx:/) { s/^.*://; s/ *//g; s/s/ /;
                                  $time_multnx+=$_; push @time_multnx,$_/$nodes;}
      if (/time spent for loop construction:/) { s/^.*://; s/ *//g; s/s/ /;
                                  $time_loop+=$_; push @time_loop,$_/$nodes;}
      if (/syncronization time in mult:/) { s/^.*://; s/ *//g; s/s/ /;
                                  $time_sync+=$_; push @time_sync,$_/$nodes; }
      if (/total time per CI iteration:/) { s/^.*://; s/ *//g; s/s/ /;
                                  $time_total+=$_; push @time_total,$_/$nodes; }
      if (/parallelization degree for mult/) { s/^.*://; push @pdegree, $_;}

      if (/== LOCAL GA TRANSFER ==/)
           {$x=<TMPIN>;
            while (<TMPIN>) { if (/total:/) {last;}}
#    v  (read)       13090.37       64.94        201.569
#            s/^ *//;
#            @x=split(/\s+/,$_);
             @x=unpack("A16A13A11A15",$_);
            push @galocaltime,$x[2] ;
            push @galocalvolume,$x[1] ;
   }
         if (/loop MFLIP data:/) {
            $_=<TMPIN>;
            @x=unpack("A8A8A2A8A2A8A2A8",$_);
            $gfliploop0xzz+=$x[1];
            $gfliploop0xyy+=$x[3];
            $gfliploop0xxx+=$x[5];
            $gfliploop0xww+=$x[7];
            $_=<TMPIN>;
            @x=unpack("A9A8A3A8A3A8",$_);
            $gfliploop1xyz+=$x[1];
            $gfliploop1xyx+=$x[3];
            $gfliploop1xyw+=$x[5];
            $_=<TMPIN>;
            @x=unpack("A9A8A3A8A3A8A3A8A3A8A3A8A3A8",$_);
            $gfliploop2xxz+=$x[1];
            $gfliploop2xwz+=$x[3];
            $gfliploop2xyy+=$x[5];
            $gfliploop2xxx+=$x[7];
            $gfliploop2xww+=$x[9];
            $gfliploop2xxw+=$x[11];
#           print "inline= $_";
#           print "x: ",join(':',@x),"\n";
#           print "-> $gfliploop2xxz $gfliploop2xwz $gfliploop2xyy $gfliploop2xxx $gfliploop2xww $gfliploop2xxw \n";
            $_=<TMPIN>;
            @x=unpack("A9A8A3A8A3",$_);
            $gfliploop3xyx+=$x[1];
            $gfliploop3xyw+=$x[3];
           }



      if (/== NON-LOCAL GA TRANSFER ==/)
           {$x=<TMPIN>; 
            while (<TMPIN>) { if (/3x / && /read/) {last;}}
             @x=unpack("A16A13A11A15",$_);
            push @ga3xtime,$x[2] ;
            push @ga3xvolume,$x[1];
            while (<TMPIN>) { if (/4x / && /read/) {last;}}
             @x=unpack("A16A13A11A15",$_);
            push @ga4xtime,$x[2] ;
            push @ga4xvolume,$x[1];
            while (<TMPIN>) { if (/total:/) {last;}}
             @x=unpack("A16A13A11A15",$_);
            push @ganonlocaltime,$x[2] ;
            push @ganonlocalvolume,$x[1];
   }

    }
    close TMPIN; 
   
 #  check if timing file is reasonably complete 
    if ($#maxtask < 2) {die("ciudg.timings file is not sufficiently complete to allow proper analysis maxtask=$#maxtask \n");}
    if ($#maxwait < 2) {print "This is a seriel ciudg.x run; this cannot be used as a basis for a performance model  \n"; 
                        print STDERR "This is a seriel ciudg.x run; this cannot be used as a basis for a performance model  \n";}

    if ($time_total[$cnt-1] == 0.0)
    {
      print "\nZero time for the last iteration. Did the calculation not finish?\n";
      print " -> Not considering the last iteration!\n\n";
      $cnt = $cnt - 1;
    }

    print " -----------  Summary of the performance of $typeofcalc iterations ---------------------------- \n";
    print "number of $typeofcalc iterations:  $cnt \n";

    printf "%-20s %12.5f sec %-14s  %9.5f ) \n","wallclock time mult:  ",
         $time_mult/$nodes," ( av. time per iter.", $time_mult/($cnt*$nodes);
    printf "%-20s %12.5f sec %-14s  %9.5f ) \n","wallclock time multnx:",
         $time_multnx/$nodes," ( av. time per iter.", $time_multnx/($cnt*$nodes);
    printf "%-20s %12.5f sec %-14s  %9.5f ) \n","wallclock time loops: ",
         $time_loop/$nodes," ( av. time per iter.", $time_loop/($cnt*$nodes);
    printf "%-20s %12.5f sec %-14s  %9.5f ) \n","sync. time  in mult:  ",
         $time_sync/$nodes," ( av. time per iter.", $time_sync/($cnt*$nodes);
    printf "%-20s %12.5f sec %-14s  %9.5f ) \n","total wallclock time: ",
         $time_total/$nodes," ( av. time per iter.", $time_total/($cnt*$nodes);
    print "\n";

 if ($verbose) 
   {  print "> \n"; 
      print "> total wall clock time for calculation and average wall clock time per iteration \n";
      print "> wallclock time loops : computational effort for on the fly computation of internal loops \n";
      print "> wallclock time multnx: time for computing the matrix vector product \n" ;
      print ">        including integral file transfer and loop calculation \n";
      print "> wallclock time mult  : time for computing the matrix vector product \n";
      print ">        including integral file transfer, loop calculation and v and w vector transfer \n";
      print "> sync. time in mult   :  idle time in mult waiting for processes to complete  \n";
      print "> total wallclock time : time for one complete Davidson iteration \n";
      print "> \n";
    }


    print " -----------  wall clock times for the individual iterations ---------------------------------- \n";
    print "iter       mult        multnx      loop       sync      total      pdegree \n";
    print "           [s]          [s]         [s]        [s]       [s]               \n";
    for ($iter=0;$iter<$cnt; $iter++)
    {
    printf "%6d %10.4f %10.4f %10.4f %10.4f %10.4f %10.4f  \n",
         $iter+1,$time_mult[$iter],$time_multnx[$iter],
         $time_loop[$iter], $time_sync[$iter], $time_total[$iter],
         $pdegree[$iter];
     }

 if ($verbose) 
   {  print "> \n"; 
      print "> performance info per CI iteration summed over all CPUs \n";
      print "> iter   : sequence number of CI iteration \n";                                     
      print "> mult   : time in seconds for one matrix vector product, excluding syncronization \n";
      print "> multnx : time in seconds for one matrix vector product, excluding syncr. and v and w vector access\n";
      print "> loop   : time in seconds for internal loop calculation \n";
      print "> sync   : accumulated syncronization time in mult while waiting for processes to complete \n";
      print "> total  : total time for complete Davidson iteration in seconds  \n";
      print "> pdegree: parallelization degree = (t_mult)/(t_mult+t_sync) \n"; 
      print "> NOTE, that according to this definition the timings in these table ought to be independent \n";
      print ">       of the number of processors used!\n";
      print "> \n";
    }

    print "\n";
    print " -----------  total global array communication time & volume (summed over all nodes) ------------ \n";
    print "    iter   local  GA  transfer         non-local GA transfer\n";
#   print "           total time   total volume    total time total volume                     required MB/(s*nodes)  achieved \n";
    printf " %15s  %13s %11s %13s %26s\n", "time", "volume", "time","volume","(average transfer rate)";
    for ($iter=0;$iter<$cnt; $iter++)
    {
#   printf "%6d %10.4f s %10.4f GB  %10.4fs %10.4f GB (%6.1f MB/s)   ***  %6.1f MB/(nodes*s)  %6.1f MB/(nodes*s) \n",
#        $iter+1,$galocaltime[$iter],$galocalvolume[$iter]/1024, 
#        $ganonlocaltime[$iter], $ganonlocalvolume[$iter]/1024,  $ganonlocalvolume[$iter]/($ganonlocaltime[$iter]),
#         ($ganonlocalvolume[$iter]+ $galocalvolume[$iter])/($time_multnx[$iter]*$nodes),
#         ($ganonlocalvolume[$iter]+ $galocalvolume[$iter])/($time_mult[$iter]*$nodes) ;
     if (abs($ganonlocaltime[$iter])>0.0001) {
    printf "%6d %10.4f s %10.4f GB  %10.4fs %10.4f GB (%6.1f MB/s)  \n",
         $iter+1,$galocaltime[$iter],$galocalvolume[$iter]/1024, 
         $ganonlocaltime[$iter], $ganonlocalvolume[$iter]/1024,  $ganonlocalvolume[$iter]/($ganonlocaltime[$iter]);
      }
     else
     {
    printf "%6d %10.4f s %10.4f GB  %10.4fs %10.4f GB (%6.1f MB/s)  \n",
         $iter+1,$galocaltime[$iter],$galocalvolume[$iter]/1024, 
         $ganonlocaltime[$iter], $ganonlocalvolume[$iter]/1024, 0;
      }
    }

    print "\n";
    print " -----------  integral related GA communication time & volume (summed over all nodes) ------------ \n";
    print "    iter       3-external read                  4-external read     \n";
    printf " %15s  %13s %12s %11s %13s %12s  %26s\n", "time", "volume", "pertask", "time","volume","pertask","(average transfer rate) ";

    $n3xtasks=($tasktypecnt[31]+$tasktypecnt[32])/$cnt;
    for ($iter=0;$iter<$cnt; $iter++)
    {
     if ($n3xtasks <=0 ) {next;}
     if ($nseg4xw <=0 || $nseg4xx <=0 ) {next; }
     if ( $ga3xtime[$iter]+$ga4xtime[$iter] <=0 )
       {
    printf "%6d %10.4f s %10.4f GB %8.2f s  %10.4f s %10.4f GB  %8.2f s(%6.1f MB/s)  \n",
         $iter+1,$ga3xtime[$iter],$ga3xvolume[$iter]/1024, $ga3xtime[$iter]/($n3xtasks),
         $ga4xtime[$iter], $ga4xvolume[$iter]/1024, $ga4xtime[$iter]/($nseg4xw+$nseg4xx);
       }
     else { 
    printf "%6d %10.4f s %10.4f GB %8.2f s  %10.4f s %10.4f GB  %8.2f s(%6.1f MB/s)  \n",
         $iter+1,$ga3xtime[$iter],$ga3xvolume[$iter]/1024, $ga3xtime[$iter]/($n3xtasks),
         $ga4xtime[$iter], $ga4xvolume[$iter]/1024, $ga4xtime[$iter]/($nseg4xw+$nseg4xx),
         ($ga4xvolume[$iter]+$ga3xvolume[$iter])/($ga3xtime[$iter]+$ga4xtime[$iter]);
        }
    }


if ($verbose) 
   { print "> \n";
     print "> Global Array based communication times and communication volume \n";
     print "> local GA transfer is defined as the whole data patch that is to be transferred is located \n";
     print "> on the node that requests the data \n";
     print "> On shared-memory systems, the data local to given process is the data patch associated with \n";
     print "> this process by the Global Arrays \n";
     print "> Also, explicit local GA data access through a pointer mechanism is not included (subspace matrix manipulation,only) \n";
     print "> Note, that with increasing number of processes, the data locality is vanishing \n";
     print "> \n"; } 


    print "\n ------------------------ performance tuning --------------------------------------------\n";
    print "\n most time consuming task in each iteration\n";
    print "    iter   time    task no.      tasktype       mnemonic      max. sync. time      tmult     tcomm \n"; 
    print "           [s]                                                     [s]              [s]       [s] \n"; 
    for ($iter=0;$iter<$cnt; $iter++)
    {
    printf "%6d %10.4f %6d %18s %12.4f %18.4f %8.4f \n",
         $iter+1,$maxtask[$iter],$maxtaskno[$iter],$tasktypes[$maxtaskno[$iter]-1], $maxwait[$iter],
         $time_mult[$iter],$ganonlocaltime[$iter]/$nodes;
       # $iter+1,$maxtask[$iter],$maxtaskno[$iter],$tasktypes[$maxtaskno[$iter]+1], $maxwait[$iter],
    }
     $totsync=0; $maxrat=0; @changetask=(); $tcommproblem=0;

    for ($iter=1;$iter<$cnt; $iter++)
     { if ($maxtask[$iter] < 0.1) {die("tmodel.pl: timings too short for an analysis - skipping analysis \n")} 
       if ($maxwait[$iter]>0.2*$ganonlocaltime[$iter]/$nodes) { $tcommproblem=1;};
       if ( $time_multnx[$iter]/$maxtask[$iter] < 1.5 ) { $trslation= translate_mnemonic($tasktypes[$maxtaskno[$iter]-1]);
                                                         push @changetask,  $maxtaskno[$iter].$tasktypes[$maxtaskno[$iter]-1] . $trslation;}
     }

     print "\n";
     if ($tcommproblem && $#changetask == -1 ) 
          { print "syncronization times are large relative to communication times";
            print "possibly oversegmented ... \n";}
     elsif ($#changetask != -1 ) 
            { print "  ++++  increase segmentation for task types  +++++\n   task no.      tasktype       mnemonic   \n   ",join("\n   ",@changetask),"\n";
              print " ----------------------------------------------------------------\n"; 
              print " info: nseg*x refers to namelist input variables in ciudgin \n ";
              print "       which take four integers separated by \',\'  \n ";
              print "       e.g.  nseg3x=*,+,*,+  suggests raising the second and forth entry of nseg3x \n";
              print "  +++++++++++++++++++++++++++++++++++++++++++++++++\n ";}
     else { print "         => performance looks reasonably balanced ... \n";}
      

    if ($cnt > 2 ) {
     print "\n maximum deviations of timings for iterations > 1 on per task basis including communication \n ";
     print " analyzing iterations 2 to $cnt \n";
     print " #task  mnemonic         min time [s]         max time[s]        max -min [s]  (max - min)/average [%] \n";
    print "--------------------------------------------------------------------------------------------\n";
     my $ntask =$#{$unsortedtasks[$cnt]};
     $ttmax=0; 
     for ($i=0; $i<=$ntask; $i++)
     { $tmin=99999; $tmax=0; $tsum=0; 
      for ($j=2; $j<=$cnt; $j++)
      { if ($tmin > ${$unsortedtasks[$j]}[$i] ) {$tmin=${$unsortedtasks[$j]}[$i];}
        if ($tmax < ${$unsortedtasks[$j]}[$i] ) {$tmax=${$unsortedtasks[$j]}[$i];}
        $tsum+= ${$unsortedtasks[$j]}[$i] ;
      }
       $tsum=0.01*$tsum/($cnt-1);
       if ($ttmax < $tmax-$tmin) { $ttmax=$tmax-$tmin;$ttmaxtsk=$i+1;}
       if (abs($tsum)>1.e-5) { printf "%6d    %18s    %10.4f          %10.4f     %10.4f        %10.2f  \n", $i+1,$tasktypes[$i], 
                                 $tmin, $tmax, $tmax-$tmin, ($tmax-$tmin)/$tsum; }
       else                  { printf "%6d    %18s    %10.4f          %10.4f     %10.4f        %10.2f  \n", $i+1,$tasktypes[$i],
                               $tmin, $tmax, $tmax-$tmin,0;}
     }
    print "--------------------------------maximum deviation ------------------------------------------\n";
      printf "%6d                                            %10.4f          \n", $ttmaxtsk, $ttmax ;
    print "--------------------------------------------------------------------------------------------\n";
 }


            $gflip[1]=$gfliploop0xzz/$cnt/1024;
            $gflip[2]=$gfliploop0xyy/$cnt/1024;
            $gflip[3]=$gfliploop0xxx/$cnt/1024;
            $gflip[4]=$gfliploop0xww/$cnt/1024;
            $gflip[11]=$gfliploop1xyz/$cnt/1024;
            $gflip[13]=$gfliploop1xyx/$cnt/1024;
            $gflip[14]=$gfliploop1xyw/$cnt/1024;
            $gflip[31]=$gfliploop3xyx/$cnt/1024;
            $gflip[32]=$gfliploop3xyw/$cnt/1024;
            $gflip[24]=$gfliploop2xxz/$cnt/1024;
            $gflip[25]=$gfliploop2xwz/$cnt/1024;
            $gflip[21]=$gfliploop2xyy/$cnt/1024;
            $gflip[22]=$gfliploop2xxx/$cnt/1024;
            $gflip[23]=$gfliploop2xww/$cnt/1024;
            $gflip[26]=$gfliploop2xxw/$cnt/1024;




    print "\n----------------------------------- total times per task type ------------------------------------------------------------------\n";
    print "task type                        tmult    tcomm(vw)   MFLOPS       GFLOP     loops           loop time    loop          ntasks \n";
    print "  code         mnemonic           [s]        [s]      exloop       total    [million]            [s]      GFLOP              \n";
    print "--------------------------------------------------------------------------------------------------------------------------------\n";
    for ( $i=1;$i<200; $i++) { #print "i $i  times mflop = $tasktypetimes[$i] $tasktypemflop[$i] cnt= $cnt\n"; 
                               $tasktypemult[$i]=$tasktypemult[$i]/$cnt;
                               $tasktypetimes[$i]=$tasktypetimes[$i]/$cnt;
                               $tasktypemloop[$i]=$tasktypemloop[$i]/$cnt;
                               $tasktypeltime[$i]=$tasktypeltime[$i]/$cnt; 
                               $tasktypemflop[$i]=$tasktypemflop[$i]/$cnt; 
                               $tasktypecnt[$i]=$tasktypecnt[$i]/$cnt; 
#                              if ($tasktypetimes[$i]) 
#                                   {$tasktypemflop[$i]=($tasktypemflop[$i]-$gflip[$i]*1024)/($tasktypetimes[$i]-$tasktypeltime[$i])}
#                               print "--> $tasktypemflop[$i]\n";
                              }


    $tottime=0;
    @checksum=();
    for ( $i=1;$i<200; $i++)
    {  #if ($tasktypetimes[$i]) { 
        if ($mnemonics[$i]) {
           $underflow=($tasktypetimes[$i]-$tasktypeltime[$i]);
           if (abs($underflow)<0.0000001) {$underflow=0.0000001;}
           $underflow2=$tasktypetimes[$i]; if (abs($underflow2)<0.0000001) {$underflow2= 0.0000001;}
           printf "%6d %18s  %10.2f  %7.1f  %11.3f   %8.3f   %8.3f      %8.2f ( %4.1f \%)  %8.3f  %5d  \n", 
           $i, $mnemonics[$i],$tasktypemult[$i],-$tasktypetimes[$i]+$tasktypemult[$i],
           ($tasktypemflop[$i]-$gflip[$i]*1024)/($underflow), $tasktypemflop[$i]/1024.0,$tasktypemloop[$i], 
           $tasktypeltime[$i],  100*$tasktypeltime[$i]/$underflow2, $gflip[$i] ,$tasktypecnt[$i];
           $tottime+=$tasktypetimes[$i];
           $checksum[1]+=$tasktypetimes[$i];
           $checksum[2]+=$tasktypemloop[$i];
           $checksum[3]+=$tasktypeltime[$i];
           $checksum[4]+=$tasktypemflop[$i]/1024.0;
           $checksum[5]+=$gflip[$i];
           $checksum[6]+=$tasktypecnt[$i];
           $checksum[7]+=$tasktypemult[$i];
           }}

    print "------------------------------------------------------------------------------------------------------------------------------------\n";
    printf "%-24s   %10.2f  %7.1f  %11.3f   %8.3f   %8.3f      %8.2f ( %4.1f \%)  %8.3f  %5d \n",
           " sum", $checksum[1], $checksum[7]-$checksum[1],
            ($checksum[4]-$checksum[5])*1024/($checksum[1]-$checksum[3]),
              $checksum[4],$checksum[2],$checksum[3],100*$checksum[3]/$checksum[1],$checksum[5], $checksum[6];
    print "------------------------------------------------------------------------------------------------------------------------------------\n";

    print "\n---------- variation of task sizes per task type --------------------\n";
    print "task type                      tmultnx [s]             \n";
    print "                           min        max        average                         \n";
    print "------------------------------------------------------------------------\n";
    for ( $i=1;$i<200; $i++)
    {  if ($class[$i][2]) 
      { printf "%18s  %10.2f   %10.2f  %10.2f        \n", $mnemonics[$i], $class[$i][1],$class[$i][2] ,
           $class[$i][4]/$class[$i][3] ; }  
    }

    print "------------------------------------------------------------------------\n";
   
    
    exit;
    die ("performance prediction by tmodel.pl is unreliable! .... skipped \n");


    $corememsave=$coremem;
   @pnodes=();
   @pvwdiag=();
   @p34ex=();
   @pvdisk=();
   @pbw=();
   @pcore=();
   @ptwork=();
   @ptcomm=();
   @pnseg0x=();
   @pnseg1x=();
   @pnseg2x=();
   @pnseg3x=();
   @pnseg4x=();
   @pdrt=();

   if ($noperfm) {exit;}

    for ($newnodes=$minnodes; $newnodes <=$maxnodes ; $newnodes+=$nodesinc)

   {
  print OUTF "************************ $newnodes **************************\n";
    $bandwidth=$bandwidtha+$newnodes*$bandwidthb;
    $coremem=$corememsave;
             $coremem-=($nvcimx*2+1)*($csf[1]+$csf[2]+$csf[3]+$csf[4])/$newnodes;
             $coremem-=ceil(($fil4w)/(8*$newnodes))+ceil(($fil4x)/(8*$newnodes))
                       +ceil(($fil3w)/(8*$newnodes))+ceil(($fil3x)/(8*$newnodes));
             $coremem-=ceil($ofdgint/8);
             $coremem-=ceil($diagint/8);
             $coremem-=ceil($drtsize*3.5/$newnodes);


    push @pnodes,$newnodes;
    push @pvwdiag,($nvcimx*2+1)*($csf[1]+$csf[2]+$csf[3]+$csf[4])/$newnodes;
    push @p34ex, ceil(($fil4w)/(8*$newnodes))+ceil(($fil4x)/(8*$newnodes))+ceil(($fil3w)/(8*$newnodes))
                  +ceil(($fil3x)/(8*$newnodes));
    push @pvdisk, ceil($diagint/8)+ceil($ofdgint/8);
    push @pbw, $bandwidth;
    push @pdrt, ceil($drtsize*3.5/$newnodes);
    if ($coremem < 1000000) { push @pcore,-1;
                              push @ptcomm,0; push @ptwork,0; push @pttotal,0;
                              push @pnseg0x,"-"; 
                              push @pnseg1x,"-"; 
                              push @pnseg2x,"-"; 
                              push @pnseg3x,"-"; 
                              push @pnseg4x,"-"; 
                              next;}
    else { push @pcore, $coremem; }

    corememreq();
    loadbal();
    estimate();


     print OUTF "constructed $ntasks tasks\n";
     @bucket=();
     $totvol=0; $totwork=0;
     for ($i=1; $i<=$ntasks; $i++)
    {
#       printf OUTF "tasktype %3d work: %8.2f sec v+w: %8.2f MB ( %8.2f secs) \n",
#        $ntasktype[$i],$ntaskwork[$i],$ntaskvw[$i]*8/(1024*1024),
#         $ntaskvw[$i]*8/(1024*1024*$bandwidth); 
        $totwork+=$ntaskwork[$i];
        $totvol+=$ntaskvw[$i];
        $x=($avtime*2/($ntaskwork[$i]+$ntaskvw[$i]*8/(1024*1024*$bandwidth)));
        if ($x < 1.33) {$bucket[1]++;}
         else { $x=ceil($x); $bucket[$x]++;}
}
     printf OUTF " > 75 prozent estimated wallclock: %2d tasks ( %4.1f\%) \n", $bucket[1], 100*($bucket[1])/$ntasks;
     printf OUTF " > 50 prozent estimated wallclock: %2d tasks ( %4.1f\%) \n", $bucket[1]+$bucket[2], 100*($bucket[1]+$bucket[2])/$ntasks;
      printf OUTF " > 25 prozent estimated wallclock: %2d tasks ( %4.1f\%) \n", $bucket[3]+$bucket[4], 100*($bucket[3]+$bucket[4])/$ntasks;
     $h=0;
     for ($i=5; $i<=10; $i++) {$h+=$bucket[$i];}
     printf OUTF " > 10 prozent estimated wallclock: %2d tasks ( %4.1f\%) \n", $h, 100*($h)/$ntasks;
     $h=0;
     for ($i=11; $i<=100; $i++) {$h+=$bucket[$i];}
     printf OUTF " > 1 prozent estimated wallclock: %2d tasks ( %4.1f\%) \n", $h, 100*($h)/$ntasks;

  print OUTF "assuming perfect loadbalancing on $newnodes nodes: \n";
  printf OUTF "total work %8.2f  total comm %8.2f  = %8.2f secs/Iteration on %3d nodes * %10.3f GB \n", 
   $totwork/$newnodes, $totvol*8/(1024*1024*$bandwidth*$newnodes),
   $totwork/$newnodes+$totvol*8/(1024*1024*$bandwidth*$newnodes),$newnodes,
   $totvol*8/(1024*1024*1024);

   push @ptwork,$totwork/$newnodes;
   push @ptcomm,$totvol*8/(1024*1024*$bandwidth*$newnodes);
   push @pttotal, $totwork/$newnodes+$totvol*8/(1024*1024*$bandwidth*$newnodes);
   }


  #printtable
  print " -----------------  estimated timings, ressource requirements, segmentation ----------------\n";
  printf "\n physical memory per node: %12d DP ( %6.3f MB) \n", $corememsave, $corememsave/(1024*128);
  printf "maximum subspace dimension = $nvcimx \n";
  printf "maximum estimated task size = %4.1f percent of total iteration time\n",$maxtaskportion*100;
  print "  ncpu      v+w     fil34        vdisk     drt    core      bwidth    twork   tcomm    total \n ";
  print "            [MB]     [MB]        [MB]     [MB]    [MB]       [MB/s]    [s]     [s]      [s]  \n";
  print "       ------------------------- per  node -----------------  ----- wall clock -------\n";
  $cnt=$#pnodes;
  for ($i=0; $i <= $cnt; $i++)
  { printf " %4d %11.3f  %10.3f   %8.3f %8.3f %10.3f %7.1f   %7.1f  %5.1f  %8.1f    \n",
    $pnodes[$i], (shift @pvwdiag)/125000, (shift @p34ex)/125000, (shift @pvdisk)/125000, (shift @pdrt)/125000,
     (shift @pcore)/125000 , shift @pbw ,
    shift @ptwork, shift @ptcomm, shift @pttotal ; }

  print "\n";
  print "  ncpu      0-ext.       1-ext         2-ext         3-ext        4-ext      diagonal  \n";    
  print "           nseg0x       nseg1x        nseg2x         nseg3x       nseg4x      nsegd    \n"; 
  for ($i=0; $i <= $cnt; $i++)
  { printf " %4d %12s  %12s  %12s %12s %12s %12s \n",                                     
    $pnodes[$i],shift @pnseg0x, shift @pnseg1x, shift @pnseg2x, shift @pnseg3x, $pnseg4x[$i],$pnseg4x[$i];}
# print "\nadditional settings: c2ex0ex=0 \n";
# print "                     c3ex1ex=0 \n";
# print "                     cdg4ex =1 \n";
# print "                     fileloc=2,2,2,2,2,2,2 \n";


  }

    
   sub corememreq
 {
    printf OUTF "ZCSF: %12d  YCSF: %12d  XCSF: %12d  WCSF: %12d \n",
           $csf[1], $csf[2], $csf[3], $csf[4];
    print OUTF "coremem = $coremem\n";
    $hz=2*$csf[1]/$coremem;
    $hy=2*$csf[2]/$coremem;
    $hx=2*$csf[3]/$coremem;
    $hw=2*$csf[4]/$coremem;
    print OUTF "hz=$hz  hy=$hy hx=$hx hw=$hw \n";

    $nsg0[1]=ceil(2*$hz);
    $nsg0[2]=ceil(2*$hy);
    $nsg0[3]=ceil(2*$hx);
    $nsg0[4]=ceil(2*$hw);

    $nsg4[1]=ceil($hz);
    $nsg4[2]=ceil($hy);
    $nsg4[3]=ceil($hx);
    $nsg4[4]=ceil($hw);

    while (1)
    { $rem=$coremem-2*$csf[2]/ceil($hy);
      if ($rem/$coremem > 0.5) {last;}
      $hy=$hy+1;}
 



    $nsg3[1]=1;
    $nsg3[2]=ceil($hy);
    $nsg3[3]=ceil(2*$csf[3]/$rem);
    $nsg3[4]=ceil(2*$csf[4]/$rem);
# yz
    $rem=$coremem-$csf[1]*2/ceil($hz);
    $nsg1[1]=ceil($hz);
    $nsg1[2]=ceil(2*$csf[2]/$rem);
# yw
    while(1)
    { $rem=$coremem-$csf[2]*2/ceil($nsg1[2]);
      if ($rem/$coremem > 0.5) {last;}
      $nsg1[2]++;}
    $nsg1[3]=ceil(2*$csf[3]/$rem);
# yx
    $nsg1[4]=ceil(2*$csf[4]/$rem);


    $nsg2[1]=1;
#yy
    $nsg2[2]=ceil(2*$hy);
#xx
    $nsg2[3]=ceil(2*$hx);
#ww
    $nsg2[4]=ceil(2*$hw);
#xz
    $rem=$coremem-$csf[1]*2/ceil($nsg2[1]);
    if (ceil(2*$csf[3]/$rem) > $nsg2[3] ) {$nsg2[3]=ceil(2*$csf[3]/$rem)};
    if (ceil(2*$csf[4]/$rem) > $nsg2[4] ) {$nsg2[4]=ceil(2*$csf[4]/$rem)};
#wx


    print OUTF "*** minimum segmentation due to core memory requirements ****\n";
    print OUTF "allinternal: nseg0ex= $nsg0[1], $nsg0[2], $nsg0[3], $nsg0[4] \n";
    print OUTF "fourexternal:nseg4ex= $nsg4[1], $nsg4[2], $nsg4[3], $nsg4[4]  \n";
    print OUTF "diagonal:    nsegdg = $nsg4[1], $nsg4[2], $nsg4[3], $nsg4[4]  \n";
    print OUTF "threeext    :nseg3ex= $nsg3[1], $nsg3[2], $nsg3[3], $nsg3[4]  \n";
    print OUTF "one-ext.    :nseg1ex= $nsg1[1], $nsg1[2], $nsg1[3], $nsg1[4]  \n";
    print OUTF "two-ext.    :nseg2ex= $nsg2[1], $nsg2[2], $nsg2[3], $nsg2[4]  \n";


#summary
    print OUTF "core memory requirements for this segmentation\n";
    $cmem=4*$csf[1]/$nsg0[1];
    if ($cmem < 4*$csf[2]/$nsg0[2]) {$cmem=4*$csf[2]/$nsg0[2];}
    if ($cmem < 4*$csf[3]/$nsg0[3]) {$cmem=4*$csf[3]/$nsg0[3];}
    if ($cmem < 4*$csf[4]/$nsg0[4]) {$cmem=4*$csf[4]/$nsg0[4];}
    print OUTF "allint:  $cmem DP\n";

    $cmem=2*$csf[1]/$nsg4[1];
    if ($cmem < 2*$csf[2]/$nsg4[2]) {$cmem=2*$csf[2]/$nsg4[2];}
    if ($cmem < 2*$csf[3]/$nsg4[3]) {$cmem=2*$csf[3]/$nsg4[3];}
    if ($cmem < 2*$csf[4]/$nsg4[4]) {$cmem=2*$csf[4]/$nsg4[4];}
    print OUTF "diag+fourext:  $cmem DP\n";

    $cmem=2*$csf[2]/$nsg3[2]+2*$csf[3]/$nsg3[3];
    if ($cmem < 2*$csf[2]/$nsg3[2]+2*$csf[4]/$nsg3[4]) { $cmem=2*$csf[2]/$nsg3[2]+2*$csf[4]/$nsg3[4];}
    print OUTF "three-ext:  $cmem DP\n";

    $cmem=2*$csf[2]/$nsg1[2]+2*$csf[1]/$nsg1[1];
    if ($cmem < 2*$csf[2]/$nsg1[2]+2*$csf[3]/$nsg1[3]) { $cmem=2*$csf[2]/$nsg1[2]+2*$csf[3]/$nsg1[3];}
    if ($cmem < 2*$csf[2]/$nsg1[2]+2*$csf[4]/$nsg1[4]) { $cmem=2*$csf[2]/$nsg1[2]+2*$csf[4]/$nsg1[4];}
    print OUTF "one-ext:  $cmem DP\n";

    $cmem=4*$csf[2]/$nsg2[2];
    if ($cmem < 4*$csf[3]/$nsg2[3]) {$cmem=4*$csf[3]/$nsg2[3];}
    if ($cmem < 4*$csf[4]/$nsg2[4]) {$cmem=4*$csf[4]/$nsg2[4];}
    if ($cmem < 2*$csf[1]/$nsg2[1]+2*$csf[3]/$nsg2[3]) { $cmem=2*$csf[1]/$nsg2[1]+2*$csf[3]/$nsg2[3];}
    if ($cmem < 2*$csf[1]/$nsg2[1]+2*$csf[4]/$nsg2[4]) { $cmem=2*$csf[1]/$nsg2[1]+2*$csf[4]/$nsg2[4];}
    if ($cmem < 2*$csf[3]/$nsg2[3]+2*$csf[4]/$nsg2[4]) { $cmem=2*$csf[3]/$nsg2[3]+2*$csf[4]/$nsg2[4];}
    print OUTF "two-ext:  $cmem DP\n";
    return;
  }

   sub loadbal
  {
    printf OUTF 
      "applying additional segmentation to make sure that no task exceeds %3d percent of the total cpu time\n",
    ceil( $maxtaskportion*100);
    printf OUTF "estimated time per CI iteration: %10.2f sec\n", $tottime/$newnodes;
    $avtime=$maxtaskportion*$tottime/$newnodes;
#  task 1 to 4:  tasks = nseg*(nseg+1)/2  
#               avtime= 2*$tasktypetimes/(nseg*(nseg+1))
#               nseg*(nseg+1)=2*tasktypetimes/avtime
#               nseg = ceil(sqrt(2*tasktypetimes/avtime+0.25)-0.5)
    $h=ceil(sqrt(2*$tasktypetimes[1]/$avtime+0.25)-0.5);
    if ($nsg0[1] < $h) {$nsg0[1]= $h;}
    $h=ceil(sqrt(2*$tasktypetimes[2]/$avtime+0.25)-0.5);
    if ($nsg0[2] < $h) {$nsg0[2]= $h;}
    $h=ceil(sqrt(2*$tasktypetimes[3]/$avtime+0.25)-0.5);
    if ($nsg0[3] < $h) {$nsg0[3]= $h;}
    $h=ceil(sqrt(2*$tasktypetimes[4]/$avtime+0.25)-0.5);
    if ($nsg0[4] < $h) {$nsg0[4]= $h;}
    print OUTF "allinternal: nseg0ex= $nsg0[1], $nsg0[2], $nsg0[3], $nsg0[4] \n";

#   task 75
    $h=ceil($tasktypetimes[75]/$avtime);
    if ($nsg4[1] < $h) {$nsg4[1]= $h;}
#   task 45
    $h=ceil($tasktypetimes[45]/$avtime);
    if ($nsg4[2] < $h) {$nsg4[2]= $h;}
#   task 46
    $h=ceil($tasktypetimes[46]/$avtime);
    if ($nsg4[3] < $h) {$nsg4[3]= $h;}
#   task 47
    $h=ceil($tasktypetimes[47]/$avtime);
    if ($nsg4[4] < $h) {$nsg4[4]= $h;}
    print OUTF "fourexternal:nseg4ex= $nsg4[1], $nsg4[2], $nsg4[3], $nsg4[4]  \n";

    
# task 31 yx
#   $avtime=tasktypetimes/(nseg1*nseg2)
#   nseg1*nseg2=tasktypetimes/avtime
#   keep y fixed and increase x only
#   nsegx=tasktypetimes/(avtime*nsegy)
    $h=ceil($tasktypetimes[31]/($avtime*$nsg3[2]));
    if ($nsg3[3] < $h) {$nsg3[3]= $h;}
    $h=ceil($tasktypetimes[32]/($avtime*$nsg3[2]));
    if ($nsg3[4] < $h) {$nsg3[4]= $h;}

    print OUTF "three-external:nseg3ex= $nsg3[1], $nsg3[2], $nsg3[3], $nsg3[4]  \n";

# task 11,13,14 yx
#   $avtime=tasktypetimes/(nseg1*nseg2)
#   nseg1*nseg2=tasktypetimes/avtime
#   keep y fixed and increase x only
#   nsegx=tasktypetimes/(avtime*nsegy)
    $h=ceil(sqrt($tasktypetimes[11]/$avtime));
    if ( $nsg1[1] < $h-1) { $nsg1[1]=$h-1;}
    $h=ceil($tasktypetimes[11]/($avtime*$nsg1[1]));
    if ($nsg1[2] < $h) {$nsg1[2]= $h;}
    $h=ceil($tasktypetimes[13]/($avtime*$nsg1[2]));
    if ($nsg1[3] < $h) {$nsg1[3]= $h;}
    $h=ceil($tasktypetimes[14]/($avtime*$nsg1[2]));
    if ($nsg1[4] < $h) {$nsg1[4]= $h;}

    print OUTF "one-external:nseg1ex= $nsg1[1], $nsg1[2], $nsg1[3], $nsg1[4]  \n";

    
#  two-external
#  yy,xx,ww
    $h=ceil(sqrt(2*$tasktypetimes[21]/$avtime+0.25)-0.5);
    if ($nsg2[2] < $h) {$nsg2[2]= $h;} 
    $h=ceil(sqrt(2*$tasktypetimes[22]/$avtime+0.25)-0.5);
    if ($nsg2[3] < $h) {$nsg2[3]= $h;}
    $h=ceil(sqrt(2*$tasktypetimes[23]/$avtime+0.25)-0.5);
    if ($nsg2[4] < $h) {$nsg2[4]= $h;}
#  xz,wz
#   $avtime=tasktypetimes/(nseg1*nseg2)
#   nseg1*nseg2=tasktypetimes/avtime
#   keep y fixed and increase x only
#   nsegx=tasktypetimes/(avtime*nsegy)
    $h=ceil($tasktypetimes[24]/($avtime*$nsg2[1]));
    if ($nsg2[3] < $h) {$nsg2[3]= $h;}
    $h=ceil($tasktypetimes[25]/($avtime*$nsg2[1]));
    if ($nsg2[4] < $h) {$nsg2[4]= $h;}
#
# wx
     while ( $avtime < $tasktypetimes[26]/($nsg2[3]*$nsg2[4]))
      { if ($nsg2[3] < $nsg2[4]) {$nsg2[3]++;}
            else {$nsg2[4]++;}}
#   { printf "warning twoext wx estimated to %10.2f secs \n",$tasktypetimes[26]/($nsg2[3]*$nsg2[4]);}
    print OUTF "two-external: nseg2ex= $nsg2[1], $nsg2[2], $nsg2[3], $nsg2[4] \n";

   return;}


   sub estimate
  {
@ntasktype=();
@ntaskwork=();
@ntaskvw=();
$ntasks=0;
#
# now compute estimates for computation time per task along with communication volume
#
##  all-internal
push @pnseg0x,join(',', $nsg0[1],$nsg0[2],$nsg0[3],$nsg0[4]);
push @pnseg1x,join(',', $nsg1[1],$nsg1[2],$nsg1[3],$nsg1[4]);
push @pnseg2x,join(',', $nsg2[1],$nsg2[2],$nsg2[3],$nsg2[4]);
push @pnseg3x,join(',', $nsg3[1],$nsg3[2],$nsg3[3],$nsg3[4]);
push @pnseg4x,join(',', $nsg4[1],$nsg4[2],$nsg4[3],$nsg4[4]);

    $savetask=$ntasks;

    for ($ntype=1; $ntype<=4; $ntype++)
    { $work=$tasktypetimes[$ntype]/($nsg0[$ntype]*($nsg0[$ntype]+1)*0.5);
      for ($k=1; $k<=$nsg0[$ntype]; $k++ )
        { for ($j=1; $j<$k; $j++)
           {$comm=4*$csf[$ntype]/$nsg0[$ntype];
            $ntasks++;
            $ntasktype[$ntasks]=$ntype;
            $ntaskwork[$ntasks]=$work;
            $ntaskvw[$ntasks]=$comm;
     #      printf "tasktype %3d work: %8.2f sec v+w: %8.2f MB\n", $ntype,$work, $comm*8/(1024*1024);
}
           {$comm=2*$csf[$ntype]/$nsg0[$ntype];
            $ntasks++;
            $ntasktype[$ntasks]=$ntype;
            $ntaskwork[$ntasks]=$work;
            $ntaskvw[$ntasks]=$comm;
     #      printf "tasktype %3d work: %8.2f sec v+w: %8.2f MB\n", $ntype,$work, $comm*8/(1024*1024);
}
        }

     }
#   print "**************** $newnodes *************\n";
#   printf "generated %10d all internal tasks \n", $ntasks-$savetask;
#   $savetask=$ntasks;
##  fourex+diag
#  75
    $ntype=75;
    $work=$tasktypetimes[$ntype]/($nsg4[1]);
    for ($k=1; $k<=$nsg4[1]; $k++ )
      {$comm=2*$csf[1]/$nsg4[1];
            $ntasks++;
            $ntasktype[$ntasks]=$ntype;
            $ntaskwork[$ntasks]=$work;
            $ntaskvw[$ntasks]=$comm;
#      printf "tasktype %3d work: %8.2f sec v+w: %8.2f MB\n", $ntype,$work, $comm*8/(1024*1024);
}

#  45,46,47
    for ($ntype=45; $ntype<=47; $ntype++)
    {$work=$tasktypetimes[$ntype]/($nsg4[$ntype-43]);
    for ($k=1; $k<=$nsg4[$ntype-43]; $k++ )
      {$comm=2*$csf[$ntype-43]/$nsg4[$ntype-43];
            $ntasks++;
            if ($ntype == 46) {$comm+=$fil4x/8}
            if ($ntype == 47) {$comm+=$fil4w/8}
            $ntasktype[$ntasks]=$ntype;
            $ntaskwork[$ntasks]=$work;
            $ntaskvw[$ntasks]=$comm;
#      printf "tasktype %3d work: %8.2f sec v+w: %8.2f MB\n", $ntype,$work, $comm*8/(1024*1024);
}
    }
#   printf "generated %10d fourex+diag  tasks \n", $ntasks-$savetask;
#   $savetask=$ntasks;

#  31 32
    for ($ntype=31; $ntype<=32; $ntype++)
    { $work=$tasktypetimes[$ntype]/($nsg3[$ntype-28]*($nsg3[2]));
      for ($k=1; $k<=$nsg3[$ntype-28]; $k++ )
        { for ($j=1; $j<=$nsg3[2]; $j++)
           {$comm=$csf[$ntype-28]/$nsg3[$ntype-28]+$csf[2]/$nsg3[2];
            $comm=$comm*2;
            if ($ntype == 31) {$comm+=$fil3x/8}
            if ($ntype == 32) {$comm+=$fil3w/8}
            $ntasks++;
            $ntasktype[$ntasks]=$ntype;
            $ntaskwork[$ntasks]=$work;
            $ntaskvw[$ntasks]=$comm;
#           printf "tasktype %3d work: %8.2f sec v+w: %8.2f MB\n", $ntype,$work, $comm*8/(1024*1024);
}
        }
     }
#   printf "generated %10d threx +diag  tasks \n", $ntasks-$savetask;
#   $savetask=$ntasks;

#11
     $work=$tasktypetimes[11]/($nsg1[1]*($nsg1[2]));
      for ($k=1; $k<=$nsg1[1]; $k++ )
        { for ($j=1; $j<=$nsg1[2]; $j++)
           {$comm=$csf[1]/$nsg1[1]+$csf[2]/$nsg1[2];
            $comm=$comm*2;
            $ntasks++;
            $ntasktype[$ntasks]=$ntype;
            $ntaskwork[$ntasks]=$work;
            $ntaskvw[$ntasks]=$comm;
#           printf "tasktype %3d work: %8.2f sec v+w: %8.2f MB\n", 11,$work, $comm*8/(1024*1024);
}
        }



#  13 14
    for ($ntype=13; $ntype<=14; $ntype++)
    { $work=$tasktypetimes[$ntype]/($nsg1[$ntype-10]*($nsg1[2]));
      for ($k=1; $k<=$nsg1[$ntype-10]; $k++ )
        { for ($j=1; $j<=$nsg1[2]; $j++)
           {$comm=$csf[$ntype-10]/$nsg1[$ntype-10]+$csf[2]/$nsg1[2];
            $comm=$comm*2;
            $ntasks++;
            $ntasktype[$ntasks]=$ntype;
            $ntaskwork[$ntasks]=$work;
            $ntaskvw[$ntasks]=$comm;
#           printf "tasktype %3d work: %8.2f sec v+w: %8.2f MB\n", $ntype,$work, $comm*8/(1024*1024);
}
        }
     }
#   printf "generated %10d oneex  tasks \n", $ntasks-$savetask;
#   $savetask=$ntasks;

##  two-internal yy,xx,ww 21,22,23 
    for ($ntype=21; $ntype<=23; $ntype++)
    { $work=$tasktypetimes[$ntype]/($nsg2[$ntype-19]*($nsg2[$ntype-19]+1)*0.5);
      for ($k=1; $k<=$nsg2[$ntype-19]; $k++ )
        { for ($j=1; $j<$k; $j++)
           {$comm=4*$csf[$ntype-19]/$nsg2[$ntype-19];
            $ntasks++;
            $ntasktype[$ntasks]=$ntype;
            $ntaskwork[$ntasks]=$work;
            $ntaskvw[$ntasks]=$comm;
#           printf "tasktype %3d work: %8.2f sec v+w: %8.2f MB\n", $ntype,$work, $comm*8/(1024*1024);
}
           {$comm=2*$csf[$ntype-19]/$nsg2[$ntype-19];
            $ntasks++;
            $ntasktype[$ntasks]=$ntype;
            $ntaskwork[$ntasks]=$work;
            $ntaskvw[$ntasks]=$comm;
#           printf "tasktype %3d work: %8.2f sec v+w: %8.2f MB\n", $ntype,$work, $comm*8/(1024*1024);
}
        }
     }

#  24 25 xz wz
    for ($ntype=24; $ntype<=25; $ntype++)
    { $work=$tasktypetimes[$ntype]/($nsg2[$ntype-21]*($nsg2[1]));
      for ($k=1; $k<=$nsg2[$ntype-21]; $k++ )
        { for ($j=1; $j<=$nsg2[1]; $j++)
           {$comm=$csf[$ntype-21]/$nsg2[$ntype-21]+$csf[1]/$nsg2[1];
            $comm=$comm*2;
            $ntasks++;
            $ntasktype[$ntasks]=$ntype;
            $ntaskwork[$ntasks]=$work;
            $ntaskvw[$ntasks]=$comm;
#           printf "tasktype %3d work: %8.2f sec v+w: %8.2f MB\n", $ntype,$work, $comm*8/(1024*1024);
}
        }
     }
#26
     $work=$tasktypetimes[26]/($nsg2[3]*($nsg2[4]));
      for ($k=1; $k<=$nsg2[3]; $k++ )
        { for ($j=1; $j<=$nsg2[4]; $j++)
           {$comm=$csf[3]/$nsg2[3]+$csf[4]/$nsg2[4];
            $comm=$comm*2;
            $ntasks++;
            $ntasktype[$ntasks]=$ntype;
            $ntaskwork[$ntasks]=$work;
            $ntaskvw[$ntasks]=$comm;
#           printf "tasktype %3d work: %8.2f sec v+w: %8.2f MB\n", 26,$work, $comm*8/(1024*1024);
}
        }
#   printf "generated %10d twoex   tasks \n", $ntasks-$savetask;


  return;
}


  sub rdcidrtfl
{ my (@nx,$nline,@nline);

    
    $nline = getcidrt("info");
   @nline=split(/\s+/,$nline);
    $nline2 = getcidrt("nexo");
   @nx=split(/\s+/,$nline2);
   print "====================================CIDRT INFO ARRAY===============================================\n";
   printf " total number of MOs           : %4d \n", $nline[0]; 
   printf " total number of internal MOs  : %4d \n", $nline[1]; 
   printf " frozen core orbitals          : %4d \n", $nline[2]; 
   printf " frozen virtual orbitals       : %4d \n", $nline[3]; 
   printf " number of rows in DRT         : %4d \n", $nline[4] ; 
   $nsym=$nline[5];
   printf " number of irreducible repr.   : %4d \n", $nsym;               
   print " number of external orbitals   : " ;
   for ($i=1; $i<=$nsym; $i++) { printf " %4d ", $nx[$i-1];} print "\n";
   printf " symmetry of state             : %4d \n", $nline[6]; 
   printf " number of walks (zyxw)        : %10d  %10d  %10d  %10d  \n",$nline[7],$nline[8],$nline[9],$nline[10]; 
   printf " number of valid walks (zyxw)  : %10d  %10d  %10d  %10d  \n",$nline[11],$nline[12],$nline[13],$nline[14] ; 
   $csftot=$nline[15];

   $drtsize=$nline[7]+$nline[8]+$nline[9]+$nline[10]+
            ($nline[11]+$nline[12]+$nline[13]+$nline[14])*2 + 9*$nline[4]*200;
   $drtsize=$drtsize*7;


   $nline = getcidrt(" ncsfsm");
   @nline=split(/\s+/,$nline);
#  shift @nline;
   $zcsf=0; $ycsf=0; $xcsf=0; $wcsf=0;
   for ($i=1; $i<=$nsym; $i++) { $zcsf+= shift @nline;
                                 $ycsf+= shift @nline;
                                 $xcsf+= shift @nline;
                                 $wcsf+= shift @nline;}
   printf " number of csfs   zyxw         : %10d  %10d  %10d  %10d   total= %12d \n", $zcsf, $ycsf, $xcsf, $wcsf, $csftot;
   print "===================================================================================================\n";
    if ( $zcsf+$ycsf+$xcsf+$wcsf != $csftot ) { printf " %10d <> %10d \n", $zcsf+$ycsf+$xcsf+$wcsf, $csftot; 
  die ("inconsistent cidrtfile \n");}
   $csf[1]=$zcsf;
   $csf[2]=$ycsf;
   $csf[3]=$xcsf;
   $csf[4]=$wcsf;

   
   return;
} 


   sub getfsize
   { my ($fname)=@_;
     if ( ! -e $fname ) {print "missing file $fname \n"; return -1;}
     system ("ls -l $fname > xxx ");
     open TMP,"<xxx";
     $xxx = <TMP> ; @xxx = split(/\s+/,$xxx); return $xxx[4];
    }     
    

    sub getcidrt
     { my ($label,$nline);
      ($label)=@_;
      open CIDRTFL,"<cidrtfl" || die("cannot find cidrtfl file , exiting\n");
      while (<CIDRTFL>)
      { if ( grep (/$label/,$_)) {last;}}
      $nline=' ';
      while (<CIDRTFL>) 
      { if (/\(/) { last;}
        $nline= $nline . ' ' . $_; chop $nline;}
      $nline=~s/^ *//;
      return $nline;
     }


   sub prtheader
   { 
     print "************************************************\n";
     print "*       Timing Model & Performance Analysis    * \n";
     print "*        for the parallel COLUMBUS CI code     *\n";
     print "*                                              *\n";
     print "*       Version 0.4 / november 10th 2011       * \n";
     print "*            Thomas Mueller                    *\n";
     print "*        Juelich Supercomputing Centere        *\n";
     print "*        Research Centre Juelich               *\n";
     print "*        D-52425 Juelich, Germany              *\n";
     print "*         th.mueller@fz-juelich.de             *\n";
     print "************************************************\n";
     print "\n\n\n";
   return;
   }
   
  sub translate_mnemonic {
    my ($mtasktype ) = @_;
    my $translation ; 
    $_=$mtasktype;
 
    if (/allint/) { $translation=" modify  nseg0x=";}
    if (/one-ext/) { if (/ yz/) {$translation=" modify  nseg1x=+,+,*,*";}
                     if (/ yx/) {$translation=" modify  nseg1x=*,+,+,*";}
                     if (/ yw/) {$translation=" modify  nseg1x=*,+,*,+";}
                   }
    if (/two-ext/) { if (/ yy/) {$translation=" modify  nseg2x=*,+,*,*";}
                     if (/ xz/) {$translation=" modify  nseg2x=+,*,+,*";}
                     if (/ wz/) {$translation=" modify  nseg2x=+,*,*,+";}
                     if (/ xx/) {$translation=" modify  nsegwx=*,*,+,*";}
                     if (/ ww/) {$translation=" modify  nsegwx=*,*,*,+";}
                     if (/ wx/) {$translation=" modify  nsegwx=*,*,+,+";}
                   }
    if (/thr-ext/) { if (/ yw/) {$translation=" modify  nseg3x=*,+,*,+";}
                     if (/ yx/) {$translation=" modify  nseg3x=*,+,+,*";}
                   }
    if (/four-ext/) { if (/ y/) {$translation=" modify nseg4x=*,+,*,*"; }
                      if (/ x/) {$translation=" modify nseg4x=*,*,+,*"; }
                      if (/ w/) {$translation=" modify nseg4x=*,*,*,+"; }
                    }
    if (/dg-024ext/) { $translation=" ";}


   return $translation;
  }

     
    
       
