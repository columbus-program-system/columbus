#!/usr/bin/env colperl
#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/


 open FIN,"<coord" || die ("cannot open coord file\n");
 open FOUT,">xmol" || die ("cannot open xmol\n");
 @ausgabe=();
 $cnt=0;

  $fac=0.529217;
 while (<FIN>) 
 { if (/coord/) {last;}}

 while (<FIN>) 
 { if (/bonds/ || /end/ ) {last;}
   chop; s/^ *//g; s/ *$//g;
   ($x,$y,$z,$symbol) = split(/\s+/,$_);
   $eintrag=sprintf "%-5s %10.6f %10.6f %10.6f\n", $symbol, $x*$fac, $y*$fac, $z*$fac;
   push @ausgabe, $eintrag;
   $cnt++;
 }

 print  "$cnt\n\n";
 print FOUT "$cnt\n\n";
 foreach $i (@ausgabe)
 { print  $i;
   print FOUT $i;}
 print FOUT "\n\n";
 close FOUT;
 exit;

 
   
