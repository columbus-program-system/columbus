#!/usr/bin/env colperl
#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/

#use lib "/home7/jzam07/jzam0703/Columbus/Columbus/perlscripts";
 use lib "$ENV{COLUMBUS}/perlscripts";
#use lib "/scr/cipaper/jump/perl.uptodate";
use model;
$DEBUG=0;

#print @ARGV,"=$#ARGV \n";

#defaults

 $prtlevel=0;
 $skipsize=0;
 $skipgeom=0;
 $skipx=0; $skipy=0;
 $tasks=10;
 $timeitr=500;
 @wantseg=(1,1,1,1);
 @red=(1,1,1,1);
 $skipthresh=0;
 $bw=1e12;
 $maxmem=10000000000;
 $ncpu=12;
 $nvmax=4;
 $suffix="def";
 $preseg="on";
 $minz0=-1;
 $scilab=0;
 $minstatic=0.3;
 $maxstatic=0.5;
 $hdiag=0;
 $ncnode=1;
 $partitionsize=0;
#$fsizefile=0;
# redx=1 means no reduction

#
#./optimize.pl  -ncpu 64 -maxtask 0.7 -maxmem 16000000 -tmodel tmodells -print 1
#


 while ($_=shift @ARGV)
  {  if (/-help/) { usageinfo(); die ();}
     if (/-print/) { $prtlevel=shift @ARGV;next;}
     if (/-skipsize/) { $skipsize=shift @ARGV; next;}
     if (/-skipgeom/) { $skipgeom=shift @ARGV; next;}
     if (/-time/) { $timeitr=shift @ARGV; next;}
     if (/-maxmem/) {$maxmem=shift @ARGV; next;}
     if (/-seg/) { @wantseg=split(',',shift @ARGV); next;}
     if (/-red/) { @red=split(',',shift @ARGV); next;}
     if (/-skipthresh/) {$skipthresh=shift @ARGV; next;}
     if (/-class/) {$class=shift @ARGV; next;}
     if (/-fsizes/) {$fsizefile=shift @ARGV; next;}
     if (/-tmodel/) {$tmodel=shift @ARGV; next;}
     if (/-bw/) {$bw=shift @ARGV; next;}
     if (/-nvmax/) {$nvmax=shift @ARGV; next;}
     if (/-ncnode/) {$ncnode=shift @ARGV; next;}
     if (/-partitionsize/) {$partitionsize=shift @ARGV; next;}
     if (/-ncpu/) {$ncpu=shift @ARGV; next;}
     if (/-suffix/) {$suffix=shift @ARGV; next;}
     if (/-minz0/) {$minz0=shift @ARGV; next;}
     if (/-scilab/) {$scilab= shift @ARGV; next;}
     if (/-static/) {$minstatic= shift @ARGV; $maxstatic= shift @ARGV; next;}  
     if (/-hdiag/) {$hdiag=1;  next;}
     if (/-preseg/) {$preseg=shift @ARGV; if ($preseg != "on" && $preseg != "off") {die("invalid -preseg option");} next;}
   { usageinfo();  print $_,"... wrong \n"; die ();}
  }

 if ($skipgeom) { ($skipx,$skipy)=split(/,/,$skipgeom);}
 if ($preseg eq "on") { $preseg=0;} else {$preseg=1;}
 if (! $partitionsize) { $partitionsize=$ncpu/$ncnode;}


print "#################################### INPUT parameters #####################################\n";
print " printlevel (-print)                                  $prtlevel \n";
print " pre-optimization step \n"; 
print "     minimum block size (-skipsize)                   $skipsize \n";
print "     minimum block geometry (-skipgeom)               $skipsize[0], $skipsize[1] \n";
print "     thresholding           (-skipthresh)             $skipthresh \n";
print " maximum time per task   (-time)                      $timeitr  seconds \n";
print " minimum number of z,y,x,w segments (-seg)            ",join(',',@wantseg), "\n";
print " relative reduction size z,y,x,w (-red)               ",join(',',@red)," \n";
print " threshold   (-skipthresh)                            $skipthresh \n";
print " task class (-class)                                  $class  [ 1x|3x|2x_wx|2x|0x|4x|all]\n";
print " maximum memory usage per core (-maxmem)              $maxmem  MB\n";
print " effective bandwidth (-bw)                            $bw   MB/s \n";
print " maximum subspace dimension (-nvmax)                  $nvmax     \n";
print " number of cores            (-ncpu)                   $ncpu      \n";
print " take filesizes from file   (-fsizes)                 $fsizefile \n";
print " take runtime info from file (-tmodel)                $tmodel \n";
print " skip presegmentation       (-preseg)                 $preseg \n";
print " compute diagonal H elements once only (-hdiag)       $hdiag \n";
print " minimum zsegments for 0ext  (-minz0)                 $minz0  \n";
print " static supertask generation (-static)  ( YX_1X, YW_1X only) \n";
print "        collect all tasks with timings below         ", $minstatic*100,"% of wall clock time \n";
print "        maximum size of supertask                    ", $maxstatic*100,"% of wall clock time \n";
print " number of cores per node (-ncnode)                  ", $ncnode  , "                          \n";
print " number of nodes per partition(-partitionsize)       ", $partitionsize,"                       \n";
print "###########################################################################################\n";
 if ( $fsizefile )
  { # read filesize info from $fsizes
    open TMPIN,"<$fsizefile" || die ("could not find $fsizefile \n");
    $fil4w=-1;
    $fil4x=-1;
    $fil3x=-1;
    $fil3w=-1;
    $ofdgint=-1;
    $diagint=-1;
    while (<TMPIN>)
    {
      if (/fil4x/i) { chop;s/fil4x//i; s/\D//g; $fil4x=$_;}
      if (/fil4w/i) { chop;s/fil4w//i;s/\D//g; $fil4w=$_;}
      if (/fil3x/i) { chop;s/fil3x//i;s/\D//g; $fil3x=$_;}
      if (/fil3w/i) { chop;s/fil3w//i;s/\D//g; $fil3w=$_;}
      if (/ofdgint/i) { chop;s/ofdgint//i;s/\D//g; $ofdgint=$_;}
      if (/diagint/i) { chop;s/diagint//i;s/\D//g;$diagint=$_;}
    }
     close TMPIN;

}
 else
 { open FMOD,"<$tmodel";
    while(<FMOD>) { if (/=====FILE SIZES ===/) {last;}}
    $fil4x=<FMOD>;
    $fil4x=<FMOD>; $fil4x=~s/^.*://; $fil4x=~s/Bytes.*$//; $fil4x=~s/ //g; 
    $fil3x=<FMOD>; $fil3x=~s/^.*://; $fil3x=~s/Bytes.*$//; $fil3x=~s/ //g; 
    $fil4w=<FMOD>; $fil4w=~s/^.*://; $fil4w=~s/Bytes.*$//; $fil4w=~s/ //g; 
    $fil3w=<FMOD>; $fil3w=~s/^.*://; $fil3w=~s/Bytes.*$//; $fil3w=~s/ //g; 
    $ofdgint=<FMOD>; $ofdgint=~s/^.*://; $ofdgint=~s/Bytes.*$//; $ofdgint=~s/ //g; 
    $diagint=<FMOD>; $diagint=~s/^.*://; $diagint=~s/Bytes.*$//; $diagint=~s/ //g; 
    close FMOD;
 #$fil4x=getfsize("fil4x");
 #$fil4w=getfsize("fil4w");
 #$fil3x=getfsize("fil3x");
 #$fil3w=getfsize("fil3w");
 #$ofdgint=getfsize("ofdgint");
 #$diagint=getfsize("diagint");
 }
 print "#################################  Integral file sizes:  #################################\n";
 printf "      fil4x: %10d Bytes ( %8.2f MB) \n", $fil4x, $fil4x/(1024*1024);
 printf "      fil3x: %10d Bytes ( %8.2f MB) \n", $fil3x, $fil3x/(1024*1024);
 printf "      fil4w: %10d Bytes ( %8.2f MB) \n", $fil4w, $fil4w/(1024*1024);
 printf "      fil3w: %10d Bytes ( %8.2f MB) \n", $fil3w, $fil3w/(1024*1024);
 printf "    ofdgint: %10d Bytes ( %8.2f MB) \n", $ofdgint, $ofdgint/(1024*1024);
 printf "    diagint: %10d Bytes ( %8.2f MB) \n", $diagint, $diagint/(1024*1024);
 print "###########################################################################################\n";

 if ( $tmodel ) 
 { open FMOD,"<$tmodel";

    while(<FMOD>) { if (/integral related GA communication/) {last;}}
    $x=<FMOD>;
    $x=<FMOD>;
     $i3xtime=0; $i4xtime=0; $i3xtimeptask=0; $i4xtimeptask=0; $icnt=0;
    while (<FMOD>)
    { if (/^$/ || /^ *$/) {last;}
      @tmp= split(/\s+/,$_); 
      $i3xtime+=$tmp[2];
      $i3xtimeptask+=$tmp[6];
      $i4xtime+=$tmp[8];
      $i4xtimeptask+=$tmp[12];
      $icnt++;
    }
    if ($icnt >0) { $i3xtime/=$icnt;
                    $i4xtime/=$icnt;
                    $i4xtimeptask/=$icnt;
                    $i3xtimeptask/=$icnt;
                  }
          else    { $i3xtime=0; $i4xtime=0; $i4xtimeptask=0; $i3xtimeptask=0;}

   close FMOD;
   open FMOD,"<$tmodel";

   while (<FMOD>) {if (/-- total times per task type --/) {last;}}



   $cost2x_wx=0;
   $cost0x_ww=0;
   $cost0x_xx=0;
   $cost2x_xx=0;
   $cost2x_ww=0;
   $cost2x_yy=0;
# oldformat
# extracted loop numbers (in millions) instead of loop times 
#  $lidx=7;
   $lidx=8;
#tmodells structure
#task type                        tmult    tcomm(vw)   MFLOPS       GFLOP     loops           loop time        loop      ntasks 
#  code         mnemonic           [s]        [s]      exloop       total    [million]            [s]          GFLOP               
#------------------------------------------------------------------------------------------------------------------------
#     1         allint zz       100.00      0.00        0.00        0.00       20.00       10.00  ( 00.0 %)      0.000      1
#     2         allint yy       100.00      0.00        0.00        0.00       20.00       10.00  ( 00.0 %)      0.000      1
#
  

#
#  wegen Unausgewogenheit von xx+,xx* (warum??) wird jetzt anstelle von 
#       Zeit=zeit(xx*)+zeit(xx+)   genommen   Zeit=2*max(xx*,xx+)
#

   while (<FMOD>) 
    { if ( / thr-ext yx / ) { @tmp = split(/\s+/, $_); $cost3x_yx=$tmp[4]; $loop3x_yx=$tmp[$lidx]; $ntask_3x_yx=$tmp[14];} 
      if ( / thr-ext yw / ) { @tmp = split(/\s+/, $_); $cost3x_yw=$tmp[4]; $loop3x_yw=$tmp[$lidx];$ntask_3x_yw=$tmp[14];} 
#old scheme
       if ( / two-ext wx\+ / ){@tmp = split(/\s+/, $_); $cost2x_wx+=$tmp[4]; $loop2x_wx+=$tmp[$lidx]; $ntask_2x_wx+=$tmp[14];} 
       if ( / two-ext wx\* / ){@tmp = split(/\s+/, $_); $cost2x_wx+=$tmp[4]; $loop2x_wx+=$tmp[$lidx]; $ntask_2x_wx+=$tmp[14];} 
       if ( / two-ext wx / ){@tmp = split(/\s+/, $_); $cost2x_wx+=$tmp[4];   $loop2x_wx+=$tmp[$lidx]; $ntask_2x_wx+=$tmp[14];} 
      if ( / two-ext xx\* / ){@tmp = split(/\s+/, $_); $cost2x_xx+=$tmp[4]; $loop2x_xx+=$tmp[$lidx];  $ntask_2x_xx+=$tmp[14];} 
      if ( / two-ext xx\+ / ){@tmp = split(/\s+/, $_); $cost2x_xx+=$tmp[4]; $loop2x_xx+=$tmp[$lidx];  $ntask_2x_xx+=$tmp[14];} 
      if ( / two-ext xx / ){@tmp = split(/\s+/, $_); $cost2x_xx+=$tmp[4];   $loop2x_xx+=$tmp[$lidx];  $ntask_2x_xx+=$tmp[14];} 
      if ( / two-ext ww / ){@tmp = split(/\s+/, $_); $cost2x_ww+=$tmp[4];   $loop2x_ww+=$tmp[$lidx];  $ntask_2x_ww+=$tmp[14];}  
      if ( / two-ext ww\* / ){@tmp = split(/\s+/, $_); $cost2x_ww+=$tmp[4]; $loop2x_ww+=$tmp[$lidx];  $ntask_2x_ww+=$tmp[14];} 
      if ( / two-ext ww\+ / ){@tmp = split(/\s+/, $_); $cost2x_ww+=$tmp[4]; $loop2x_ww+=$tmp[$lidx]; $ntask_2x_ww+=$tmp[14];} 
#test scheme
#     if ( / two-ext wx\* / ){@tmp = split(/\s+/, $_); if ($cost2x_wx < 0.01) { $cost2x_wx+=$tmp[4];}
#                                                          else           { if ($tmp[4]>$cost2x_wx) { $cost2x_wx=$tmp[4]; }}
#                                                                           $loop2x_wx+=$tmp[$lidx]; $ntask_2x_wx+=$tmp[14];} 
#     if ( / two-ext wx\+ / ){@tmp = split(/\s+/, $_); if ($cost2x_wx < 0.01) { $cost2x_wx+=$tmp[4];}
#                                                          else           { if ($tmp[4]>$cost2x_wx) { $cost2x_wx=$tmp[4]; }}
#                                                                           $loop2x_wx+=$tmp[$lidx]; $ntask_2x_wx+=$tmp[14];} 
#     if ( / two-ext wx / ){@tmp = split(/\s+/, $_); $cost2x_wx+=$tmp[4];   $loop2x_wx+=$tmp[$lidx]; $ntask_2x_wx+=$tmp[14];} 
      if ( / one-ext yz / ){@tmp = split(/\s+/, $_); $cost1x_yz=$tmp[4];    $loop1x_yz=$tmp[$lidx];  $ntask_1x_yz=$tmp[14];} 
      if ( / one-ext yx / ){@tmp = split(/\s+/, $_); $cost1x_yx=$tmp[4];    $loop1x_yx=$tmp[$lidx];  $ntask_1x_yx=$tmp[14];} 
      if ( / one-ext yw / ){@tmp = split(/\s+/, $_); $cost1x_yw=$tmp[4];    $loop1x_yw=$tmp[$lidx];  $ntask_1x_yw=$tmp[14];} 
      if ( / allint zz / ){@tmp = split(/\s+/, $_); $cost0x_zz=$tmp[4];     $loop0x_zz=$tmp[$lidx];  $ntask_0x_zz=$tmp[14];} 
      if ( / allint yy / ){@tmp = split(/\s+/, $_); $cost0x_yy=$tmp[4];     $loop0x_yy=$tmp[$lidx];  $ntask_0x_yy=$tmp[14];} 
      if ( / allint xx/ ){@tmp = split(/\s+/, $_); $cost0x_xx+=$tmp[4];     $loop0x_xx+=$tmp[$lidx];  $ntask_0x_xx+=$tmp[14];} 
      if ( / allint ww/ ){@tmp = split(/\s+/, $_); $cost0x_ww+=$tmp[4];     $loop0x_ww+=$tmp[$lidx];  $ntask_0x_ww+=$tmp[14];} 
#     if ( / two-ext xx\+ / ){@tmp = split(/\s+/, $_); if ($cost2x_xx < 0.01) { $cost2x_wx+=$tmp[4];}
#                                                          else           { if ($tmp[4]>$cost2x_xx) { $cost2x_xx=$tmp[4]; }}
#                                                                           $loop2x_xx+=$tmp[$lidx]; $ntask_2x_xx+=$tmp[14];} 
#     if ( / two-ext xx\* / ){@tmp = split(/\s+/, $_); if ($cost2x_xx < 0.01) { $cost2x_xx+=$tmp[4];}
#                                                          else           { if ($tmp[4]>$cost2x_xx) { $cost2x_xx=$tmp[4]; }}
#                                                                           $loop2x_xx+=$tmp[$lidx]; $ntask_2x_xx+=$tmp[14];} 

#     if ( / two-ext xx / ){@tmp = split(/\s+/, $_); $cost2x_xx+=$tmp[4];   $loop2x_xx+=$tmp[$lidx];  $ntask_2x_xx+=$tmp[14];} 
#     if ( / two-ext ww / ){@tmp = split(/\s+/, $_); $cost2x_ww+=$tmp[4];   $loop2x_ww+=$tmp[$lidx];  $ntask_2x_ww+=$tmp[14];}  
#     if ( / two-ext ww\+ / ){@tmp = split(/\s+/, $_); if ($cost2x_ww < 0.01) { $cost2x_ww+=$tmp[4];}
#                                                          else           { if ($tmp[4]>$cost2x_ww) { $cost2x_ww=$tmp[4]; }}
#                                                                           $loop2x_ww+=$tmp[$lidx]; $ntask_2x_ww+=$tmp[14];} 
#     if ( / two-ext ww\* / ){@tmp = split(/\s+/, $_); if ($cost2x_ww < 0.01) { $cost2x_ww+=$tmp[4];}
#                                                          else           { if ($tmp[4]>$cost2x_ww) { $cost2x_ww=$tmp[4]; }}
#                                                                           $loop2x_ww+=$tmp[$lidx]; $ntask_2x_ww+=$tmp[14];} 
      if ( / two-ext yy / ){@tmp = split(/\s+/, $_); $cost2x_yy=$tmp[4];    $loop2x_yy=$tmp[$lidx];  $ntask_2x_yy=$tmp[14];} 
      if ( / two-ext xz / ){@tmp = split(/\s+/, $_); $cost2x_xz=$tmp[4];    $loop2x_xz=$tmp[$lidx]; $ntask_2x_xz=$tmp[14];} 
      if ( / two-ext wz / ){@tmp = split(/\s+/, $_); $cost2x_wz=$tmp[4];    $loop2x_wz=$tmp[$lidx]; $ntask_2x_wz=$tmp[14];} 
      if ( / four-ext y / || / 4exdg024 y / ){@tmp = split(/\s+/, $_); $cost4x_y=$tmp[4];    $ntask_4x_y=$tmp[14]; } 
      if ( / four-ext x / || / 4exdg024 x / ){@tmp = split(/\s+/, $_); $cost4x_x=$tmp[4];   $ntask_4x_x=$tmp[14];} 
      if ( / four-ext w / || / 4exdg024 w / ){@tmp = split(/\s+/, $_); $cost4x_w=$tmp[4];   $ntask_4x_w=$tmp[14];} 
#
#  muss auf 0-ext addiert werden
#
      if ( / dg-024ext z / ){@tmp = split(/\s+/, $_); $cost0x_zz+=$tmp[4];  $ntask_4x_z=$tmp[14];} 
      if ( / dg-024ext y / ){@tmp = split(/\s+/, $_); $cost0x_yy+=$tmp[4];  $ntask_4x_y+=$tmp[14];} 
      if ( / dg-024ext x / ){@tmp = split(/\s+/, $_); $cost0x_xx+=$tmp[4];  $ntask_4x_x+=$tmp[14];} 
      if ( / dg-024ext w / ){@tmp = split(/\s+/, $_); $cost0x_ww+=$tmp[4];  $ntask_4x_w+=$tmp[14];} 
      if ( / variation of/ ) {last;}
    }
#
#
   $fac2x=1.0;  # statt 2.0
   $cost2x_ww=$cost2x_ww*$fac2x;
   $cost2x_wx=$cost2x_wx*$fac2x;
   $cost2x_xx=$cost2x_xx*$fac2x;
   $cost0x_xx=$cost0x_xx*$fac2x;
   $cost0x_ww=$cost0x_ww*$fac2x;
   close FMOD;
 }
 else {die("tmodel output missing ... \n");}

print " Computational cost taken from $tmodel (multnx excluding v/w communication):\n";
printf " 0-external:   zz=%10.1f s  yy=%10.1f s  xx=%10.1f s  ww=%10.1f s \n",$cost0x_zz,$cost0x_yy,$cost0x_xx,$cost0x_ww; 
printf " 1-external:   yz=%10.1f s  yx=%10.1f s  yw=%10.1f s   \n",$cost1x_yz,$cost1x_yx,$cost1x_yw; 
printf " 2-external:   yy=%10.1f s  xx=%10.1f s  ww=%10.1f s  wx=%10.1f s   xz=%10.1f s  wz=%10.1f s\n",
                        $cost2x_yy,$cost2x_xx,$cost2x_ww,$cost2x_wx,$cost2x_xz,$cost2x_wz;
printf " 3-external:   yx=%10.1f s  yw=%10.1f s   \n", $cost3x_yx,$cost3x_yw;
printf " 4-external:    z=%10.1f s   y=%10.1f s   x=%10.1f s   w=%10.1f s \n",$cost4x_z,$cost4x_y,$cost4x_x,$cost4x_w; 
print "###########################################################################################\n";
print " Computational loop cost taken from $tmodel :\n";
printf " 0-external:   zz=%10.1f s  yy=%10.1f s  xx=%10.1f s  ww=%10.1f s \n",$loop0x_zz,$loop0x_yy,$loop0x_xx,$loop0x_ww; 
printf " 1-external:   yz=%10.1f s  yx=%10.1f s  yw=%10.1f s   \n",$loop1x_yz,$loop1x_yx,$loop1x_yw; 
printf " 2-external:   yy=%10.1f s  xx=%10.1f s  ww=%10.1f s  wx=%10.1f s   xz=%10.1f s  wz=%10.1f s\n",
                        $loop2x_yy,$loop2x_xx,$loop2x_ww,$loop2x_wx,$loop2x_xz,$loop2x_wz;
printf " 3-external:   yx=%10.1f s  yw=%10.1f s   \n", $loop3x_yx,$loop3x_yw;
printf " 4-external:    z=%10.1f s   y=%10.1f s   x=%10.1f s   w=%10.1f s \n",$loop4x_z,$loop4x_y,$loop4x_x,$loop4x_w; 
print "###########################################################################################\n";
print " integral read overhead (3externals and 4externals) \n";
printf " 3-external    yx=%10.1f s (%5d tasks)  yw=%10.1f s (%5d tasks) \n", $i3xtimeptask*$ntask_3x_yx,$ntask_3x_yx,$i3xtimeptask*$ntask_3x_yw,$ntask_3x_yw;
printf " 4-external     x=%10.1f s (%5d tasks)   w=%10.1f s (%5d tasks) \n", $i4xtimeptask*$ntask_4x_x,$ntask_4x_x,$i4xtimeptask*$ntask_4x_w,$ntask_4x_w;
print "###########################################################################################\n";
print " approximate Linear Algebra related effort:\n";
printf " 0-external:   zz=%10.1f s  yy=%10.1f s  xx=%10.1f s  ww=%10.1f s \n",$cost0x_zz-$loop0x_zz,$cost0x_yy-$loop0x_yy,$cost0x_xx-$loop0x_xx,$cost0x_ww-$loop0x_ww;
printf " 1-external:   yz=%10.1f s  yx=%10.1f s  yw=%10.1f s   \n",$cost1x_yz-$loop1x_yz,$cost1x_yx-$loop1x_yx,$cost1x_yw-$loop1x_yw;
printf " 2-external:   yy=%10.1f s  xx=%10.1f s  ww=%10.1f s  wx=%10.1f s   xz=%10.1f s  wz=%10.1f s\n",
                        $cost2x_yy-$loop2x_yy,$cost2x_xx-$loop2x_xx,$cost2x_ww-$loop2x_ww,$cost2x_wx-$loop2x_wx,$cost2x_xz-$loop2x_xz,$cost2x_wz-$loop2x_wz;
printf " 3-external:   yx=%10.1f s  yw=%10.1f s   \n", $cost3x_yx-$loop3x_yx-$i3xtimeptask*$ntask_3x_yx,
                                                       $cost3x_yw-$loop3x_yw-$i3xtimeptask*$ntask_3x_yw;
printf " 4-external:    z=%10.1f s   y=%10.1f s   x=%10.1f s   w=%10.1f s \n",$cost4x_z,$cost4x_y,
                                                       $cost4x_x-$i4xtimeptask*$ntask_4x_x,$cost4x_w-$i4xtimeptask*$ntask_4x_w;

#scalefactor 4ex x,w: N**3 (warum nicht N**2) 
#scalefactor 4ex y: N
#scalefactor 3ex: N**3
#scalefactor 2ex wx,xx,ww: N**3
#scalefactor 2ex wz,xz   : N**2
#scalefactor 2ex yy      : N**2
#scalefactor 0ex       zz: 1    
#scalefactor 0ex, yy     : N
#scalefactor 0ex, xx,ww  : N**2
#scalefactor 1ex yx,yw  : N**2
#scalefactor 1ex yz     : N
$ratio=1.00;
$ratio2=$ratio*$ratio;
$ratio3=$ratio2*$ratio;
$ratio4=$ratio3*$ratio;
print "###########################################################################################################\n";
print "scaled effort: ratio=$ratio\n";
printf " 0-external:   zz=%10.1f s  yy=%10.1f s  xx=%10.1f s  ww=%10.1f s \n",
    ($cost0x_zz),
    ($cost0x_yy-$loop0x_yy)*$ratio+$loop0x_yy,
    ($cost0x_xx-$loop0x_xx)*$ratio2+$loop0x_xx,
    ($cost0x_ww-$loop0x_ww)*$ratio2+$loop0x_ww;
printf " 1-external:   yz=%10.1f s  yx=%10.1f s  yw=%10.1f s   \n",
    ($cost1x_yz-$loop1x_yz)*$ratio+$loop1x_yz,
    ($cost1x_yx-$loop1x_yx)*$ratio2+$loop1x_yx,
    ($cost1x_yw-$loop1x_yw)*$ratio2+$loop1x_yw;
printf " 2-external:   yy=%10.1f s  xx=%10.1f s  ww=%10.1f s  wx=%10.1f s   xz=%10.1f s  wz=%10.1f s\n",
                        ($cost2x_yy-$loop2x_yy)*$ratio2+$loop2x_yy,
                        ($cost2x_xx-$loop2x_xx)*$ratio3+$loop2x_xx,
                        ($cost2x_ww-$loop2x_ww)*$ratio3+$loop2x_ww,
                        ($cost2x_wx-$loop2x_wx)*$ratio3+$loop2x_wx,
                        ($cost2x_xz-$loop2x_xz)*$ratio2+$loop2x_xz,
                        ($cost2x_wz-$loop2x_wz)*$ratio2+$loop2x_wz;
printf " 3-external:   yx=%10.1f s  yw=%10.1f s   \n", ($cost3x_yx-$loop3x_yx-$i3xtimeptask*$ntask_3x_yx)*$ratio3+$loop3x_yx,
                                                       ($cost3x_yw-$loop3x_yw-$i3xtimeptask*$ntask_3x_yw)*$ratio3+$loop3x_yw;
printf " 4-external:    z=%10.1f s   y=%10.1f s   x=%10.1f s   w=%10.1f s \n",$cost4x_z,$cost4x_y*$ratio,
                                                       ($cost4x_x-$i4xtimeptask*$ntask_4x_x)*$ratio3+$loop4x_x,
                                                       ($cost4x_w-$i4xtimeptask*$ntask_4x_w)*$ratio3+$loop2x_w;








$LARGEST_TASK=0;
$zero_task=0;
@zero_task=();


#
#  read SEGANALYSE
#
@csfbatch_z=();
@csfbatch_y=();
rd_seganalyse_hdr($nsym,\@nval_zyxw,\@nextern,\@nsize_zyxw,
                  \@csfbatch_z,\@csfbatch_y,\@csfbatch_x,\@csfbatch_w,
                  \@statseg,\@totcsf);

$csftot=$totcsf[0]+$totcsf[1]+$totcsf[2]+$totcsf[3];
$filega=$fil4x+$fil4w+($fil3x+$fil3w)*($ncpu/($partitionsize*$ncnode));
################## check for vanashing y,x,w CSFs  ###################

 $csftype=0;
 @csftype=();
 $tmp=1;
 for ($i=0; $i<4; $i++) { if ($totcsf[$i] > 0) {$csftype=$csftype+$tmp; $tmp=$tmp*10; $csftype[$i+1]=1;}}
 print "CSFTYPE = $csftype\n";
 if ($csftype == 11) { print " This is a MR-CIS calculation ... \n"; }
 elsif ($csftype == 1111 ) { print " This is a general MR-CISD calculation ...\n"; }
 elsif ($csftype == 1 ) { print " This is a special MR-CI calculation within the internal orbital space, only ...\n";}
 else  { print " This is some spezialized MR-CI calculation ...\n"};  

#############
#$filevdsk=$ofdgint+$diagint;
$filevdsk=0;
$filega = $filega + ($ofdgint+ $diagint)*($ncpu/$ncnode);
# no hdiag file any longer
print "csftot=$csftot filega=$filega  filevdsk=$filevdsk\n";
#no hdiag
$memga=$csftot*(2*$nvmax)+int($filega/8);
# hdiag file switch 
if ($hdiag) {$memga=$memga+$csftot;}
   
print " 2*$nvmax=",2*$nvmax," *$csftot=",$csftot*(2*$nvmax), "filega:8 ",$filega/8," ncpu=$ncpu \n";
print "Reducing total memory $maxmem MB by ",$filevdsk/(1024*1024), ' MB (vdsk)', $memga/($ncpu*131072)," MB (GA) \n";
# maxmem in MB=1024*1024 Bytes
$maxmem=$maxmem-$filevdsk/(1024*1024)-$memga/($ncpu*131072);
print "New maxmem value= $maxmem\n";
if ($maxmem < 10 ) {die("not enough memory: $maxmem\n");}

if ($prtlevel > 1 ) {
print "CSFBATCH_Z:",join(' ',@csfbatch_z),"\n";
print "CSFBATCH_Y:",join(' ',@csfbatch_y),"\n";
print "CSFBATCH_X:",join(' ',@csfbatch_x),"\n";
print "CSFBATCH_W:",join(' ',@csfbatch_w),"\n";
 }

$maxmem=$maxmem/(1.024*1.024);

$csfbatches[0]=\@csfbatch_z;
$csfbatches[1]=\@csfbatch_y;
$csfbatches[2]=\@csfbatch_x;
$csfbatches[3]=\@csfbatch_w;
@zero_tasks=();
@$TASKLIST{'index'}=();
@$TASKLIST{'range'}=();
@$TASKLIST{'time'}=();
@$TASKLIST{'volume'}=();
@$TASKLIST{'memory'}=();
@$TASKLIST{'tasks'}=();

if ( $class eq "3ext" || $class eq "all" ) 
  
    {
        if ($csftype[2] && $csftype[4]) {  
        ($ybd,$wbd) = partition_data("3x","yw",1,3,\@nsize_zyxw,\@statseg,"YW_3x",
                      $skipsize,$skipx,$skipy,\@csfbatches,$timeitr,
                      $wantseg[1],$wantseg[3],$red[1],$red[3],0,0,\@zero_tasks,$skipthresh, 
                      $fil3w, $cost3x_yw, $prtlevel, $maxmem,\%TASKLIST,$bw,$preseg,$scilab); }
         else { @ybd=(); @wbd=(); $ybd=\@ybd; $wbd=\@wbd;}

        $factor=.00000000745058059692;  #  8/1024/1024/1024  = csf to GB
        $ifactor=.00000000093132257461; # 1/(1024)**3 = byte to GB
        # volume for equal sized segments
        # sum_yseg  sum_wseg (yseglen_i+wseglen_j)*2 = 2 sum_yseg yseglen_i+totcsf_w
        #                                            = 2 sum_wseg wseglen_j+totcsf_y
        #  
        #
        #  2\sum_i{\sum_j{y_i+w_j}} = 2\sum_i{\sum_j{y_i}}+2\sum_i{\sum_j{w_j}} 
        #  = 2\sum_j{csf_y}+2\sum_i{csf_w}


        if ($csftype[2] && $csftype[3]) {  
        ($y2bd,$xbd) = partition_data("3x","yx",1,2,\@nsize_zyxw,\@statseg,"YX_3x",
                       $skipsize,$skipx,$skipy,\@csfbatches,$timeitr,
                       $wantseg[1],$wantseg[2],1,$red[2],$ybd,1,\@zero_tasks,$skipthresh,
                       $fil3x,$cost3x_yx,$prtlevel,$maxmem,\%TASKLIST,$bw,$preseg,$scilab); }
         else { @y2bd=(); @xbd=(); $y2bd=\@y2bd; $xbd=\@xbd;}


       @zbd=(); 
       ($nseg3x,$nnseg3x)=createciudgin (\@zbd, $ybd, $xbd,$wbd,\@nsize_zyxw,\@nval_zyxw);
#      print "nseg3x=" . join(',',@$nseg3x) . "\n";
#      print "nnseg3=" . join(',',@$nnseg3x) . "\n";
   }

if ( $class eq "2x_wx" || $class eq "all" ) 
   {

    @zbd=();@ybd=();
    if ($csftype[3] && $csftype[4]) {  
    ($wbd,$xbd) = partition_data("2x","wx",3,2,\@nsize_zyxw,\@statseg,"WX_2x",
               $skipsize,$skipx,$skipy,\@csfbatches,$timeitr,
               $wantseg[3],$wantseg[2],$red[3],$red[2],0,0,\@zero_tasks,$skipthresh,
               $ofdgint,$cost2x_wx,$prtlevel,$maxmem,\%TASKLIST,$bw,$preseg,$scilab); }
         else { @wbd=(); @xbd=(); $wbd=\@wbd; $xbd=\@xbd;}



 
    ($nsegwx,$nnsegwx)=createciudgin (\@zbd, \@ybd, $xbd,$wbd,\@nsize_zyxw,\@nval_zyxw);
#    print "nsegwx=" . join(',',@$nsegwx) . "\n";
#    print "nnsegwx=" . join(',',@$nnsegwx) . "\n";
   }

if ( $class eq "2x" || $class eq "all" ) 
    {

     $reserve_forz=100;
#    optimierung kann knapp sein, so dass bei ww/xx segmentierung (memfac=1) nicht genug Platz fuer wz, xz ist (memfac=2)

    @zbd=();@ybd=();
    $tmpmem=$maxmem-$reserve_forz;
    if ($csftype[4]) {  
    ($wbd,$scrbd) = partition_data("2x","ww",3,3,\@nsize_zyxw,\@statseg,"WW_2x",
               $skipsize,$skipx,$skipy,\@csfbatches,$timeitr,
               $wantseg[3],$wantseg[3],$red[3],$red[3],0,0,\@zero_tasks,$skipthresh,
               $ofdgint,$cost2x_ww,$prtlevel,$tmpmem,\%TASKLIST,$bw,$preseg,$scilab); }
         else { @wbd=(); @scrbd=(); $wbd=\@wbd; $scrbd=\@scrbd;}

    if ($csftype[3]) {  
   ($xbd,$scrbd) = partition_data("2x","xx",2,2,\@nsize_zyxw,\@statseg,"XX_2x",
               $skipsize,$skipx,$skipy,\@csfbatches,$timeitr,
               $wantseg[2],$wantyseg[2],$red[2],$red[2],0,0,\@zero_tasks,$skipthresh,
               $ofdgint,$cost2x_xx,$prtlevel,$tmpmem,\%TASKLIST,$bw,$preseg,$scilab); }
         else { @xbd=(); @scrbd=(); $xbd=\@xbd; $scrbd=\@scrbd;}

    if ($csftype[2]) {  
   ($ybd,$scrbd) = partition_data("2x","yy",1,1,\@nsize_zyxw,\@statseg,"YY_2x",
               $skipsize,$skipx,$skipy,\@csfbatches,$timeitr,
                $wantseg[1],$wantseg[1],$red[1],$red[1],0,0,\@zero_tasks,$skipthresh,
                $ofdgint,$cost2x_yy,$prtlevel,$maxmem,\%TASKLIST,$bw,$preseg,$scilab); }
         else { @ybd=(); @scrbd=(); $ybd=\@ybd; $scrbd=\@scrbd;}

    if ($csftype[1] && $csftype[3]) {  
   ($scrbd,$zbd) = partition_data("2x","xz",2,0,\@nsize_zyxw,\@statseg,"XZ_2x",
               $skipsize,$skipx,$skipy,\@csfbatches,$timeitr,
               $wantseg[2],$wantseg[0],1,$red[0],$xbd,1,\@zero_tasks,$skipthresh,
               $ofdgint,$cost2x_xz,$prtlevel,$maxmem,\%TASKLIST,$bw,$preseg,$scilab); }
         else { @zbd=(); @scrbd=(); $zbd=\@zbd; $scrbd=\@scrbd;}

#
#
#   this assumes that the now fixed segmentation does not yield to load-imbalancing problems for
#   the wz case !
#
   @wzbd=();
   $wzbd[1]=$wbd;
   $wzbd[2]=$zbd;
    if ($csftype[1] && $csftype[4]) {  
   ($scrbd,$zzbd) = partition_data("2x","wz",3,0,\@nsize_zyxw,\@statseg,"WZ_2x",
               $skipsize,$skipx,$skipy,\@csfbatches,$timeitr,
               $wantseg[3],$wantseg[0],1,$red[0],\@wzbd,3,\@zero_tasks,$skipthresh,
               $ofdgint,$cost2x_wz,$prtlevel,$maxmem,\%TASKLIST,$bw,$preseg,$scilab); }
         else { @zzbd=(); @scrbd=(); $zzbd=\@zzbd; $scrbd=\@scrbd;}

    ($nseg2x,$nnseg2x)=createciudgin ($zbd, $ybd, $xbd,$wbd,\@nsize_zyxw,\@nval_zyxw);
#    print "nseg2x=" . join(',',@$nseg2x) . "\n";
#    print "nnseg2=" . join(',',@$nnseg2x) . "\n";
   }

if ( $class eq "1x" || $class eq "all" )
 {

    if ($csftype[2] && $csftype[3]) {  
           ($ybd,$xbd) = partition_data("1x","yx",1,2,\@nsize_zyxw,\@statseg,"YX_1x",
               $skipsize,$skipx,$skipy,\@csfbatches,$timeitr,
               $wantseg[1],$wantseg[2],$red[1],$red[2],0,0,\@zero_tasks,$skipthresh,
               $ofdgint,$cost1x_yx,$prtlevel,$maxmem,\%TASKLIST,$bw,$preseg,$scilab); }
         else { @ybd=(); @zbd=(); $zbd=\@zbd; $ybd=\@ybd;}

    if ($csftype[2] && $csftype[4]) {  
        ($y2bd,$wbd) = partition_data("1x","yw",1,3,\@nsize_zyxw,\@statseg,"YW_1x",
               $skipsize,$skipx,$skipy,\@csfbatches,$timeitr,
               $wantseg[1],$wantseg[3],1,$red[3],$ybd,1,\@zero_tasks,$skipthresh,
               $ofdgint,$cost1x_yw,$prtlevel,$maxmem,\%TASKLIST,$bw,$preseg,$scilab); }
         else { @y2bd=(); @wbd=(); $wbd=\@wbd; $y2bd=\@y2bd;}


    if ($csftype[2] && $csftype[1]) {  
             ($y2bd,$zbd) = partition_data("1x","yz",1,0,\@nsize_zyxw,\@statseg,"YZ_1x",
               $skipsize,$skipx,$skipy,\@csfbatches,$timeitr,
               $wantseg[1],$wantseg[0],1,$red[0],$ybd,1,\@zero_tasks,$skipthresh,
               $ofdgint,$cost1x_yz,$prtlevel,$maxmem,\%TASKLIST,$bw,$preseg,$scilab); }
         else { @y2bd=(); @zbd=(); $zbd=\@zbd; $y2bd=\@y2bd;}

 ($nseg1x,$nnseg1x)=createciudgin ($zbd, $ybd, $xbd,$wbd,\@nsize_zyxw,\@nval_zyxw);

#    print "nseg1x=" . join(',',@$nseg1x) . "\n";
#    print "nnseg1=" . join(',',@$nnseg1x) . "\n";
}

if ( $class eq "0x" || $class eq "all" )
 {
 @zbf=();
 @ybd=();
 @scrbd=();

    if ($csftype[1]) {  
          ($ybd,$scrbd) = partition_data("0x","yy",1,1,\@nsize_zyxw,\@statseg,"YY_0x",
               $skipsize,$skipx,$skipy,\@csfbatches,$timeitr,
               $wantseg[1],$wantseg[1],$red[1],$red[1],0,0,\@zero_tasks,$skipthresh,
               $ofdgint,$cost0x_yy,$prtlevel,$maxmem,\%TASKLIST,$bw,$preseg,$scilab); }
         else { @ybd=(); @scrbd=(); $ybd=\@ybd; $scrbd=\@scrbd;}

 if ($minz0>1) {
    if ($csftype[1]) {  
         ($zbd,$scrbd) = partition_data("0x","zz",0,0,\@nsize_zyxw,\@statseg,"ZZ_0x",
               $skipsize,$skipx,$skipy,\@csfbatches,$timeitr,
               $minz0,$minz0,$red[0],$red[0],0,0,\@zero_tasks,$skipthresh,
               $ofdgint,$cost0x_zz,$prtlevel,$maxmem,\%TASKLIST,$bw,$preseg,$scilab); }
         else { @zbd=(); @scrbd=(); $zbd=\@zbd; $scrbd=\@scrbd;}
               }
 else
               {
    if ($csftype[1]) {  
               ($zbd,$scrbd) = partition_data("0x","zz",0,0,\@nsize_zyxw,\@statseg,"ZZ_0x",
               $skipsize,$skipx,$skipy,\@csfbatches,$timeitr,
               $wantseg[0],$wantseg[0],$red[0],$red[0],0,0,\@zero_tasks,$skipthresh,
               $ofdgint,$cost0x_zz,$prtlevel,$maxmem,\%TASKLIST,$bw,$preseg,$scilab); }
         else { @zbd=(); @scrbd=(); $zbd=\@zbd; $scrbd=\@scrbd;}
               }

    if ($csftype[3]) {  
          ($xbd,$scrbd) = partition_data("0x","xx",2,2,\@nsize_zyxw,\@statseg,"XX_0x",
               $skipsize,$skipx,$skipy,\@csfbatches,$timeitr,
               $wantseg[2],$wantseg[2],$red[2],$red[2],0,0,\@zero_tasks,$skipthresh,
               $ofdgint,$cost0x_xx,$prtlevel,$maxmem,\%TASKLIST,$bw,$preseg,$scilab); }
         else { @xbd=(); @scrbd=(); $xbd=\@xbd; $scrbd=\@scrbd;}


    if ($csftype[4]) {  
              ($wbd,$scrbd) = partition_data("0x","ww",3,3,\@nsize_zyxw,\@statseg,"WW_0x",
               $skipsize,$skipx,$skipy,\@csfbatches,$timeitr,
               $wantseg[3],$wantseg[3],$red[3],$red[3],0,0,\@zero_tasks,$skipthresh,
               $ofdgint,$cost0x_ww,$prtlevel,$maxmem,\%TASKLIST,$bw,$preseg,$scilab); }
         else { @wbd=(); @scrbd=(); $wbd=\@wbd; $scrbd=\@scrbd;}


 ($nseg0x,$nnseg0x)=createciudgin ($zbd, $ybd, $xbd,$wbd,\@nsize_zyxw,\@nval_zyxw);

#    print "nseg0x=" . join(',',@$nseg0x) . "\n";
#    print "nnseg0=" . join(',',@$nnseg0x) . "\n";
}


 if ( $class eq "4x" || $class eq "all" ) 
  { 
#    print "4X wantseg=",join(':',@wantseg), "\n";
     if ($csftype[1] ) { $zbd=partition_4x("Z_4XDG",$cost4x_z,$timeitr,1,$totcsf[0],\@csfbatch_z,\%TASKLIST,$diagint,$maxmem,$bw); }
                    else { @zbd=(); $zbd=\@zbd; }
     if ($csftype[2] ) { $ybd=partition_4x("Y_4XDG",$cost4x_y,$timeitr,1,$totcsf[1],\@csfbatch_y,\%TASKLIST,$diagint,$maxmem,$bw); }
                    else { @ybd=(); $ybd=\@ybd; }
     if ($csftype[3] ) { $xbd=partition_4x("X_4XDG",$cost4x_x,$timeitr,$wantseg[2],$totcsf[2],\@csfbatch_x,\%TASKLIST,$diagint+$fil4x,$maxmem,$bw); }
                    else { @xbd=(); $xbd=\@xbd; }
     if ($csftype[4] ) {$wbd=partition_4x("W_4XDG",$cost4x_w,$timeitr,$wantseg[3],$totcsf[3],\@csfbatch_w,\%TASKLIST,$diagint+$fil4w,$maxmem,$bw); }
                    else { @wbd=(); $wbd=\@wbd; }

 ($nseg4x,$nnseg4x)=createciudgin ($zbd, $ybd, $xbd,$wbd,\@nsize_zyxw,\@nval_zyxw);
#    print "nseg4x=" . join(',',@$nseg4x) . "\n";
#    print "nnseg4=" . join(',',@$nnseg4x) . "\n";

}

$loclist=sorttasklist(\%TASKLIST,$bw);
$csftot=$totcsf[0]+$totcsf[1]+$totcsf[2]+$totcsf[3];
$filega=$fil4x+$fil4w+$fil3x+$fil3w;
#$filevdsk=$ofdgint+$diagint;

printtasklist($loclist,$bw,$nseg0x,$nseg1x,$nseg2x,$nseg3x,$nseg4x,$nsegwx,$csftot,$filega,$filevdsk,$nvmax,$ncpu,$suffix);







if ( $class eq "all" )
  { print "#####################  add to ciudgin ##########################\n";
    print " nseg4x=" . join(',',@$nseg4x) . "\n";
    printstring("nnseg4",$nnseg4x);
    $maxseg=$$nseg4x[0]+$$nseg4x[1]+$$nseg4x[2]+$$nseg4x[3];

    print " nseg3x=" . join(',',@$nseg3x) . "\n";
    printstring("nnseg3",$nnseg3x);
    $tmp=$$nseg3x[0]+$$nseg3x[1]+$$nseg3x[2]+$$nseg3x[3];
    if ($tmp>$maxseg) {$maxseg=$tmp;}

    print " nseg2x=" . join(',',@$nseg2x) . "\n";
    printstring("nnseg2",$nnseg2x);
    $tmp=$$nseg2x[0]+$$nseg2x[1]+$$nseg2x[2]+$$nseg2x[3];
    if ($tmp>$maxseg) {$maxseg=$tmp;}

    print " nsegwx=" . join(',',@$nsegwx) . "\n";
    printstring("nnsegwx",$nnsegwx);
    $tmp=$$nsegwx[0]+$$nsegwx[1]+$$nsegwx[2]+$$nsegwx[3];
    if ($tmp>$maxseg) {$maxseg=$tmp;}

    print " nseg1x=" . join(',',@$nseg1x) . "\n";
    printstring("nnseg1",$nnseg1x);
    $tmp=$$nseg1x[0]+$$nseg1x[1]+$$nseg1x[2]+$$nseg1x[3];
    if ($tmp>$maxseg) {$maxseg=$tmp;}

    print " nseg0x=" . join(',',@$nseg0x) . "\n";
    printstring("nnseg0",$nnseg0x);
    $tmp=$$nseg0x[0]+$$nseg0x[1]+$$nseg0x[2]+$$nseg0x[3];
    if ($tmp>$maxseg) {$maxseg=$tmp;}

   print " maxseg=",$maxseg,"\n";
#  print " maxseg=",$maxseg+1,"\n";
#  print " fileloc=2,2,2,2,2,2,2,2,2,2 \n";
 }

exit;




 sub printtasklist
 { my ($tasklist,$bandwidth,$nseg0x,$nseg1x,$nseg2x,$nseg3x,$nseg4x,$nsegwx,$csftot,$filega,$filevdsk,$nvmax,$ncpu,$suffix)=@_;
   my %loclist=();
   @{$loclist{'index'}}=@{$$tasklist{'index'}};
   @{$loclist{'range'}}=@{$$tasklist{'range'}};
   @{$loclist{'time'}}=@{$$tasklist{'time'}};
   @{$loclist{'volume'}}=@{$$tasklist{'volume'}};
   @{$loclist{'memory'}}=@{$$tasklist{'memory'}};
   @{$loclist{'tasks'}}= @{$$tasklist{'tasks'}};
   @{$loclist{'static'}} =();
   open TASKFILE,">TASKLIST.$suffix";
#  print TASKFILE "nseg0x= @$nseg0x\n";
#  print TASKFILE "nseg1x= @$nseg1x\n";
#  print TASKFILE "nseg2x= @$nseg2x\n";
#  print TASKFILE "nseg3x= @$nseg3x\n";
#  print TASKFILE "nseg4x= @$nseg4x\n";
#  print TASKFILE "nsegwx= @$nsegwx\n";
   $currentprocess=1;
   @savedvolume=();
   @supertask=();
    $currentprocess=makestatic(\@supertask,\%loclist,$nseg0x,\@savedvolume,"0x",$currentprocess);
    $currentprocess=makestatic(\@supertask,\%loclist,$nseg1x,\@savedvolume,"1x",$currentprocess);
    $currentprocess=makestatic(\@supertask,\%loclist,$nseg3x,\@savedvolume,"3x",$currentprocess);
    $currentprocess=makestatic(\@supertask,\%loclist,$nseg2x,\@savedvolume,"2xwz",$currentprocess);
    $currentprocess=makestatic(\@supertask,\%loclist,$nseg2x,\@savedvolume,"2xww",$currentprocess);
    $currentprocess=makestatic(\@supertask,\%loclist,$nsegwx,\@savedvolume,"2xxw",$currentprocess);
#   makenewlist(\@supertask,\%loclist,\%newlist);

   $convert{'Z'}=0;
   $convert{'Y'}=1;
   $convert{'X'}=2;
   $convert{'W'}=3;
   $tasktype{'ZZ_0x'}=1;
   $tasktype{'YY_0x'}=2;
   $tasktype{'XX_0x'}=3;
   $tasktype{'WW_0x'}=4;
# offset mapping requires the name to be consistent with
# the actual ordering!!!
   $tasktype{'YZ_1x'}=11;
   $tasktype{'YX_1x'}=13;
   $tasktype{'YW_1x'}=14;
   $tasktype{'YX_3x'}=31;
   $tasktype{'YW_3x'}=32;
   $tasktype{'YY_2x'}=21;
   $tasktype{'XX_2x'}=22;
   $tasktype{'WW_2x'}=23;
   $tasktype{'XZ_2x'}=24;
   $tasktype{'WZ_2x'}=25;
   $tasktype{'WX_2x'}=26;
   $tasktype{'Z_4XDG'}=71;
   $tasktype{'Y_4XDG'}=42;
   $tasktype{'X_4XDG'}=43;
   $tasktype{'W_4XDG'}=44;


   $icnt=0;
   $totvol=0;
   $totseconds=0;
   print " ********************************************************* SORTED TASKLIST ****************************************************** \n";
   print "   TASKTYPE             BLOCK RANGE                  TIME                                 VOLUME          LOCAL MEMORY          TASKS  \n";
   print TASKFILE "    no         braseg         ketseg      tasktype       slice     static( -1=dynamic, >=0 static)    time \n";
   print TASKFILE " $currentprocess  \n";

#
#   also print a statistics for data transfer per task class
#
    @volume=0;

   $icnttsk=0;
   $maxmem=0;
   $maxtasktime=0;

    

#
   $dyntask=0;
   $stattask=-1;
   while (@{$loclist{'index'}} )
   { $iicnt= shift @{$loclist{'tasks'}};
     $volume=shift @{$loclist{'volume'}};
     $totvol+=($volume*$iicnt);
     $time=shift @{$loclist{'time'}};
     $index=shift @{$loclist{'index'}};
     $memory=shift @{$loclist{'memory'}};
     $static=shift @{$loclist{'static'}};
      if ($memory>$maxmem) {$maxmem=$memory;}
     if ($time>$maxtasktime) {$maxtasktime=$time;}
     $_=$index;
     s/[|:_]/ /g;
     ($type,$ext,$ibra,$iket)=split(/\s+/,$_);
     ($bra,$ket)=split(//,$type);
      $bra=$convert{$bra};
      $ket=$convert{$ket};
     if ("$ext" eq "3x") { $slice=5; $seg=$nseg3x;}
     elsif ("$ext" eq "2x" && "$type" eq "WX") { $slice=1; $seg=$nsegwx;}  
     elsif ("$ext" eq "2x" ) { $slice=4; $seg=$nseg2x;}  
     elsif ("$ext" eq "1x") { $slice=3; $seg=$nseg1x;}  
     elsif ("$ext" eq "0x") { $slice=2; $seg=$nseg0x;}  
     elsif ("$ext" eq "4XDG") { $slice=6; $seg=$nseg4x;$ket=$bra;}  
     else {die "invalid ext=$ext\n";}
#    print "index:$index,  $type,$ext,$ibra,$iket, braket: $bra,$ket\n";
     for ($i=0; $i<$bra; $i++) {$ibra+=$$seg[$i];}
     for ($i=0; $i<$ket; $i++) {$iket+=$$seg[$i];}
     if ($ibra <$iket) {$tmp=$ibra; $ibra=$iket; $iket=$tmp;} 
     $ty="$type"."_"."$ext"; 
     $tt=$tasktype{$ty};
     if ($tt==71) {next;}
#DEBUG
    if ($DEBUG) {
     print  TASKFILE "converted $index  -> ";
     print  TASKFILE "                 $ibra :  $iket : $tt ($ty=$type _ $ext) :  $slice  static=$static \n"; } 
     $icnttsk++;
     $save_icnttsk=$icnttsk;
     if ($static>-1) { if ($stattask<$static) { $stattask=$static; if ($iicnt==2) {$stattask++;}}
                        $time=$supertask[$static]; $staticp1=$static+1; $superiicnt[$static]=$iicnt;}
             else    {$dyntask++;$staticp1=-1; $totseconds=$totseconds+$time*$iicnt;}
     printf  TASKFILE " %10d  %10d  %10d   %10d   %10d  %10d  %10.4f \n",$icnttsk ,$ibra , $iket, $tt , $slice,  $static, $time ;
     if ($tt==2  && $ibra==$iket) 
           { $icnttsk++; printf  TASKFILE " %10d  %10d  %10d   %10d   %10d  %10d  %10.4f \n",$icnttsk ,$ibra , $iket, 76 , $slice, $static, 0.1 ; $dyntask++;}
# would not work with 71 !
     if ($tt==1  && $ibra==$iket) 
           { $icnttsk++; printf  TASKFILE " %10d  %10d  %10d   %10d   %10d  %10d  %10.4f \n",$icnttsk ,$ibra , $iket, 75 , $slice, $static, 0.1 ; $dyntask++;}
     if (($tt==3 || $tt==4)&& $ibra==$iket) 
           { $icnttsk++; printf  TASKFILE " %10d  %10d  %10d   %10d   %10d  %10d  %10.4f \n",$icnttsk ,$ibra , $iket, 74+$tt , $slice, $static, 1.0 ; $dyntask++;}
     if ($tt==22) {$icnttsk++;printf  TASKFILE " %10d  %10d  %10d   %10d   %10d  %10d  %10.4f \n",$icnttsk ,$ibra , $iket, 28 , $slice, $staticp1, $time ; $dyntask++;}
     elsif ($tt==23) {$icnttsk++;printf  TASKFILE " %10d  %10d  %10d   %10d   %10d  %10d  %10.4f\n",$icnttsk ,$ibra , $iket, 29 , $slice, $staticp1, $time ;;$dyntask++;}
     elsif ($tt==26) {$icnttsk++;printf  TASKFILE " %10d  %10d  %10d   %10d   %10d  %10d  %10.4f \n",$icnttsk ,$ibra , $iket, 27 , $slice, $staticp1, $time; ;$dyntask++;}
     elsif ($tt==3) {$icnttsk++;printf  TASKFILE " %10d  %10d  %10d   %10d   %10d  %10d  %10.4f \n",$icnttsk ,$ibra , $iket, 8 , $slice, $staticp1,$time;$dyntask++;}
     elsif ($tt==4) {$icnttsk++;printf  TASKFILE " %10d  %10d  %10d   %10d   %10d  %10d  %10.4f \n",$icnttsk ,$ibra , $iket, 9 , $slice, $staticp1,$time ;$dyntask++;}
     $volume[$slice]=$volume[$slice]+$volume*$iicnt; # in MB
     printf "%4d  %-20s %-20s %8.2f seconds (comm: %8.2f secs) %9.3f GB  %14.3f  MB  %10d tasks %4d procid \n", $save_icnttsk,$index , shift @{$loclist{'range'}},
       $time,$volume*1024/$bandwidth,$volume,$memory, ($icnttsk-$save_icnttsk+1), $static;
#  falsche gesamtzeit
     $icnt+=$iicnt;
   }
# add static tasktimes
   for ($i=0; $i<=$#supertask; $i++) { $totseconds+=$supertask[$i]*$superiicnt[$i];}

  print " ******************************************************************************************************************************** \n";
  printf "  0-ex %8.3f 1-ex %8.3f 2-ex %8.3f 3-ex %8.3f 4-exdg %8.3f  total %8.3f GB \n",
            $volume[2],$volume[3],($volume[4]+$volume[1]),$volume[5],$volume[6],
            ($volume[1]+$volume[2]+$volume[3]+$volume[4]+$volume[5]+$volume[6]);
 $totvolsaved=$savedvolume[1]+$savedvolume[2]+$savedvolume[3]+$savedvolume[4]+$savedvolume[5]+$savedvolume[6];
  printf "S 0-ex %8.3f 1-ex %8.3f 2-ex %8.3f 3-ex %8.3f 4-exdg %8.3f  total %8.3f GB \n",
            $savedvolume[2],$savedvolume[3],($savedvolume[4]+$savedvolume[1]),$savedvolume[5],$savedvolume[6],
            $totvolsaved; 

  print " ************************************************** SUMMARY ********************************************************************* \n";
  printf " total number of tasks: %8d dynamic + %8d static instead of %8d   \n", $dyntask,$stattask ,$icnttsk;
  printf " maximum memory demand: %8.2f \n", $maxmem;
  printf " largest task (incl. communciation): %8.2f  \n", $maxtasktime;
  printf " total communication data volume:  %12.3f  GB  (corresponding communicationtime: %12.3f seconds)\n", $totvol,$totvol*1024/$bandwidth;
  printf "                     reduced to :  %12.3f  GB (communication time %12.3f seconds) \n", ($totvol-$totvolsaved),($totvol-$totvolsaved)*1024/$bandwidth;
  $totseconds=$totseconds-$totvolsaved*1024/$bandwidth;
  print "  effective bandwidth: $bandwidth MB/s\n";
  print "  size of CSF space: $csftot  maximum subspace dimension: $nvmax \n";
  printf"  size of integrals stored on global arrays: %8.2f MB\n",$filega/(1024*1024);
  printf"  size of integrals stored on virtual disk : %8.2f MB\n",$filevdsk/(1024*1024);
# no hdiag 
# with hdiag 2*nvmax+1
  $memga=$csftot*(2*$nvmax)+int($filega/8);
  if ($hdiag) {$memga=$memga+$csftot;}
  printf " memory required for GA (excluding DRT info) = %12d MB per node = %12.3f GB en total\n",int($memga/($ncpu*131072))+1,$memga*7.4505006e-9;
  printf " minimum core memory (excluding index vectors)  = %12d MB per node \n", $maxmem; 
  printf " core memory setting for pciudg must exceed  -m %8.2f \n", ($maxmem+($filevdsk/(1024*1024))+($memga/($ncpu*131072))) ;
  printf " depending upon the complexity of the DRT add about 4 MB (or more) to this value \n";
  printf " the actual value is processor count dependent and obtained by  \'grep reducing ciudgls\' in the work directory \n"; 
  printf " execute       pciudg.x -m %8.2f \n", ($maxmem+($filevdsk/(1024*1024))+($memga/($ncpu*131072))); 
  printf " total cost %12.1f seconds => with perfect load balancing %12.1f seconds wall clock time \n", $totseconds, $totseconds/$ncpu;
  print " ******************************************************************************************************************************** \n";
  return;
}

 sub sorttasklist
 { my ($tasklist,$bandwidth)=@_;
   my %loclist=();
   @loc=();
   @{$loclist{'index'}}=@{$$tasklist{'index'}};
   @{$loclist{'range'}}=@{$$tasklist{'range'}};
   @{$loclist{'time'}}=@{$$tasklist{'time'}};
   @{$loclist{'volume'}}=@{$$tasklist{'volume'}};
   @{$loclist{'memory'}}=@{$$tasklist{'memory'}};
   @{$loclist{'tasks'}}= @{$$tasklist{'tasks'}};
   @{$loclist{'static'}}= @{$$tasklist{'static'}};
   while (@{$loclist{'index'}} )
   { 
     $t=shift @{$loclist{'time'}};
     $v=shift @{$loclist{'volume'}};
     $r=pack "A20A20A20A20A20A20A20", shift @{$loclist{'index'}}, shift @{$loclist{'range'}},
       $t+$v*1024/$bandwidth,$v,shift @{$loclist{'memory'}}, shift @{$loclist{'tasks'}},shift @{$loclist{'static'}};

 #    $i=shift @{$loclist{'index'}};
 #    $r=shift @{$loclist{'range'}};
 #    $m=shift @{$loclist{'memory'}};
 #    $tsks= shift @{$loclist{'tasks'}};
 #    $r=pack "A20A20A20A20A20A20", $i,$r,$t,$v,$m,$tsks,$st;
     push @loc, $r;
   }

   @loc2=sort tsort @loc;
 
  
   @$loclist{'index'}=();
   @$loclist{'range'}=();
   @$loclist{'time'}=();
   @$loclist{'volume'}=();
   @$loclist{'memory'}=();
   @$loclist{'tasks'}=();
   @$loclist{'static'}=();
   while (@loc2) 
   { ($i,$r,$t,$v,$m,$tsk,$st)=unpack "A20A20A20A20A20A20A20", shift @loc2;
      push @{$loclist{'index'}},$i;
      push @{$loclist{'range'}},$r;
      push @{$loclist{'time'}},$t;
      push @{$loclist{'volume'}},$v;
      push @{$loclist{'memory'}},$m;
      push @{$loclist{'tasks'}},$tsk;
      push @{$loclist{'static'}},$st ;
   } 

  return \%loclist;
   sub tsort
   { ($t,$r,$i)=unpack "A20A20A20",$a;
     ($t,$r,$j)=unpack "A20A20A20",$b;
#    print "i,j $i $j \n";
     return  -($i<=>$j);} 

}



sub usageinfo 
{
print "#################################### INPUT parameters #####################################\n";
print " printlevel (-print)                                  <number> \n";
print " pre-optimization step \n";
print "     minimum block size (-skipsize)                   <number> \n";
print "     minimum block geometry (-skipgeom)               <dim1,dim2> \n";
print "     thresholding           (-skipthresh)             <number> \n";
print " maximum time per task   (-time)                      <time in seconds> \n";
print " minimum number of z,y,x,w segments (-seg)            <zseg,yseg,xseg,wseg> \n";
print " relative reduction size z,y,x,w (-red)               <zred,yred,xred,wred> \n";
print " threshold   (-skipthresh)                            <number>    \n";
print " task class (-class)                                  <1x|3x|2x_wx|2x|0x|4x|all>\n";
print " maximum local memory usage (-maxmem)                 <MB>\n";
print " effective bandwidth (-bw)                            <MB/s> \n";
print " maximum subspace dimension (-nvmax)                  <number>   \n";
print " number of CPUs             (-ncpu)                   <number>      \n";
print " take filesizes from file   (-fsizes)                 <filename> \n";
print " take runtime info from file (-tmodel)                <filename> \n";
print " presegmentation             (-preseg)                <on|off>      \n";
print " compute diagonal H elements once, only (-hdiag)      <on|off>      \n";
print "###########################################################################################\n";
return;
}



 sub makestatic 
{ my ($supertask,$savedvolume,$list,$totvolsaved,@yx_stat,@used,$icnt,$iicnt,$time,$index,$volume,$type,$ext,$ibra,$iket,$i,$seg,$slice,$what,@tasktype,@convert,$currentprocess);
  my (@convert,@tasktype,$add,@locvolume);
  ($supertask,$list,$seg,$savedvolume,$what,$currentprocess)=@_;
  my $debug=0;

 # supertask[isupertask]=tasktime
   $convert{'Z'}=0;
   $convert{'Y'}=1;
   $convert{'X'}=2;
   $convert{'W'}=3;
   $tasktype{'ZZ_0x'}=0;
   $tasktype{'YY_0x'}=0;
   $tasktype{'XX_0x'}=0;
   $tasktype{'WW_0x'}=0;
# offset mapping requires the name to be consistent with
# the actual ordering!!!
   $tasktype{'YZ_1x'}=0 ;
   $tasktype{'YX_1x'}=0 ;
   $tasktype{'YW_1x'}=0 ;
   $tasktype{'YX_3x'}=0 ; $tasktype{'YW_3x'}=0 ;
   $tasktype{'YY_2x'}=0 ;
   $tasktype{'XX_2x'}=0 ;
   $tasktype{'WW_2x'}=0 ;
   $tasktype{'XZ_2x'}=0 ;
   $tasktype{'WZ_2x'}=0 ;
   $tasktype{'WX_2x'}=0 ;
   $tasktype{'Z_4XDG'}=0 ;
   $tasktype{'Y_4XDG'}=0 ;
   $tasktype{'X_4XDG'}=0 ;
   $tasktype{'W_4XDG'}=0 ;
   $add=1;
   if ( $currentprocess==1 ) { for ($icnt=$#{$$list{'index'}}; $icnt>=0; $icnt-- ) {${$$list{'static'}}[$icnt]=-1;} } 
   if ( "$what" eq "0x" ) { $what2=$what;$slice=2; $add=2; $tasktype{'XX_0x'}=1; $tasktype{'WW_0x'}=1;  } 
   elsif ( "$what" eq "1x" ) { $what2=$what;$add=1; $slice=3; $tasktype{'YX_1x'}=1; $tasktype{'YW_1x'}=1;  } 
   elsif ( "$what" eq "3x" ) { $what2=$what;$add=1; $slice=5;$tasktype{'YX_3x'}=1; $tasktype{'YW_3x'}=1;  } 
   elsif ( "$what" eq "2xwz" ) {$what2="2x"; $add=1; $slice=4;$tasktype{'WZ_2x'}=1; $tasktype{'XZ_2x'}=1;  } 
   elsif ( "$what" eq "2xww" ) {$what2="2x"; $add=2; $slice=4;$tasktype{'WW_2x'}=1; $tasktype{'XX_2x'}=1;  } 
   elsif ( "$what" eq "2xxw" ) {$what2="2x"; $add=2; $slice=1;$tasktype{'WX_2x'}=1; } 
   else   {die ("makestatic: invalid what=$what\n");}

   $totvolsaved=0;
   @locsaved=();
   @yx_stat=();
   @used=();
   $maxibra=0;
   if ($debug) {print "makestatic: starting with currentprocess=$currentprocess\n";}
    for ($icnt=$#{$$list{'index'}}; $icnt>=0; $icnt-- )
   { $iicnt= ${$$list{'tasks'}}[$icnt];
     $time= ${$$list{'time'}}[$icnt];
     $index=${$$list{'index'}}[$icnt];
     $volume=${$$list{'volume'}}[$icnt];
     $_=$index;
     s/[|:_]/ /g;
     ($type,$ext,$ibra,$iket)=split(/\s+/,$_);
     ($bra,$ket)=split(//,$type);
      $bra=$convert{$bra};
      $ket=$convert{$ket};
     if ("$ext" ne "$what2") {next;}
     for ($i=0; $i<$bra; $i++) {$ibra+=$$seg[$i];}
     for ($i=0; $i<$ket; $i++) {$iket+=$$seg[$i];}
     if ($ibra <$iket) {$tmp=$ibra; $ibra=$iket; $iket=$tmp;}
     $ty="$type"."_"."$ext";
     $tt=$tasktype{$ty};
      if ($ibra>$maxibra) {$maxibra=$ibra;}
     if ( ! $tt ) { next;}
     if ($time < $minstatic*$timeitr) 
                                     {  if ($yx_stat[$ibra]+$time > $maxstatic*$timeitr) {
                                                                   if ($debug) {printf "1generated static supertask for process %4d (%4d individ. tasks) total time %f9.2 seconds saved vol1=%9.2f GB \n",
                                                                             int($used[$ibra]/100), $used[$ibra]%100, $yx_stat[$ibra],$locsaved[$ibra];
                                                                    $$supertask[int($used[$ibra]/100)]=$yx_stat[$ibra]; }
                                                                      printf " supertask[ %5d ] = %f8.2 sec ibra=%5d \n", $used[$ibra]/100,$yx_stat[$ibra],$ibra;
                                                                    $totvolsaved=$totvolsaved+$locsaved[$ibra];
                                                                    if ($add == 2) { $$supertask[int($used[$ibra]/100)+1]=$yx_stat[$ibra];
                                                                                     printf " supertask[ %5d ] = %f8.2 sec ibra=%5d \n", $used[$ibra]/100+1,$yx_stat[$ibra],$ibra;
                                                                                    }
                                                                    $currentprocess+=$add; 
                                                                    # currentprocess is the highest used process number
                                                                    $used[$ibra]=$currentprocess*100+1;  
                                                                    ${$$list{'static'}}[$icnt]=int($used[$ibra]/100); 
                                                                    $currentprocess+=$add; 
                                                                    # ${$$list{'static'}}[$icnt]=$currentprocess;
                                                                   $yx_stat[$ibra]=$time;  $locsaved[$ibra]=0;}     # bucket full
                                         else   { if ( ! $used[$ibra] ) { $used[$ibra]=$currentprocess*100+1; $currentprocess+=$add;  
                                                                         if ($debug) { printf "3- ibra=$ibra,add=$add,currentproces=$currentprocess\n";}
                                                                         $locsaved[$ibra]=0;}
                                                                  else       {$locsaved[$ibra]+=$volume; $used[$ibra]++; }
                                                                    ${$$list{'static'}}[$icnt]=int($used[$ibra]/100); 
                                                                   $yx_stat[$ibra]+=$time; }   # add to current bucket
                               }
#      printf  " %10d  %10d  %10d   %10d   %10d  %8.3f sec  \n",$icnttsk ,$ibra , $iket, $tt , ${$$list{'static'}}[$icnt],$time  ;
   }
#    for ($ibra=0; $ibra <=$maxibra; $ibra++) {
     for ($ibra=0; $ibra <=$#yx_stat; $ibra++) {
        if ($used[$ibra]) {  if ($debug) {printf "2generated static supertask for process %4d (%4d individ. tasks) total time %f9.2 seconds saved vol2=%9.2f GB \n",
                                                 int($used[$ibra]/100), $used[$ibra]%100, $yx_stat[$ibra],$locsaved[$ibra]; }
                             $$supertask[int($used[$ibra]/100)]=$yx_stat[$ibra];
                             $totvolsaved+=$locsaved[$ibra];
                               printf " supertask[ %5d ] = %f8.2 sec ibra=%5d \n", $used[$ibra]/100,$yx_stat[$ibra],$ibra;
                             if ($add == 2) { $$supertask[int($used[$ibra]/100)+1]=$yx_stat[$ibra];
                                              printf " supertask[ %5d ] = %f8.2 sec ibra= %5d \n", $used[$ibra]/100+1,$yx_stat[$ibra],$ibra;
                                              }
                              }}

     print " finished supertask generation for $what:  currentprocess=$currentprocess  total saved volume = $totvolsaved\n";
     $$savedvolume[$slice]+=$totvolsaved;
 return $currentprocess;
}

    sub makenewlist { 
       my ($supertask,$loclist)=@_;
   my %newlist={};
   my ($iicnt,$icnt,$iicnt,$time,$index,$volume,$memory,$static);
    $newstatic=1;
    $newdynamic=$#$supertask+1;
    for ($icnt=$#{$$list{'index'}}; $icnt>=0; $icnt-- )
   { $iicnt= ${$$list{'tasks'}}[$icnt];
     $time= ${$$list{'time'}}[$icnt];
     $index=${$$list{'index'}}[$icnt];
     $volume=${$$list{'volume'}}[$icnt];
     $memory=${$$list{'memory'}}[$icnt];
     $static=${$$list{'static'}}[$icnt];
     if ($static == -1) { $newdynamic++;
                          $curr=$newdynamic;}
          else          { $curr=$newstatic+$static;}
   }  



    return;
   }
