#!/bin/sh
#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/
#
# Add iargc.o, fdate.o, hostnm.o and !vp_alloc.o! to the COLUMBUS library.

cp $COLUMBUS/colib.a .
make -f $COLUMBUS/makefile iargc.o
cc -c fdate.c
cc -c hostnm.c
#cc -c vp_alloc.c  !old-style memory allocation

#ar rv colib.a iargc.o fdate.o hostnm.o vp_alloc.o  !old
ar rv colib.a iargc.o fdate.o hostnm.o

$COLUMBUS/xupdate colib.a
