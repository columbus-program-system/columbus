!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
c iargc.f - equiv of iargc() in Sun Fortran
c roger edberg  14-nov-91

      function iargc()

      integer*4     j, iargc
      character*256 argj

      j = 1
1000  continue
         call getarg( j, argj )
         if ( argj .eq. ' ' ) goto 1100
         j = j + 1
      goto 1000
1100  continue

      iargc = j - 1

      return
      end
