/*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
/* hostnm.c - uxp equivalent of hostnm in Sun Fortran
 *
 * roger edberg  14-Nov-91
 *
 */

#include <stdio.h>
#include <sys/utsname.h>
#include <sys/systeminfo.h>

int hostnm_( char *host_name )
{
	char name[SYS_NMLN];
	char *p;
	long count = 257;
	long ierr;

	ierr = sysinfo( SI_HOSTNAME, name, count );
	if ( ierr == -1 ) {
		printf( "hostnm_(): sysinfo() call failed \n" );
		printf( "           ...returned %l \n", ierr );
		return (int) ierr;
	}

	p = &name[0];
	strncpy( host_name, p, ierr );

#ifdef DEBUG
	printf( "hostnm_: returning host_name = %s \n", host_name );
#endif

	return 0;

}

