/*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
/************************************************************************

    NAME

	fdma.c - Fortran dynamic memory allocation support functions

    SYNOPSIS

	char *calloc_( long *nelem, long *elsize )
	char *malloc_( long *size )
	char *realloc_( char *ptr, long *size )
	void  free_( char *ptr )

    DESCRIPTION

	The functions calloc_, malloc_, realloc_, and free_ are stubs
	which allow a Fortran program to dynamically allocate memory
	using the C library functions calloc(3), malloc(3), realloc(3),
	and free(3).  Parameters and return values are identical to
	those for the C library (3) functions.

	calloc_, malloc_, realloc_, and free_ require an extension to
	Fortran 77 which permits call by value.  (Normally Fortran 77
	implements all subprogram parameters by call by reference.)
	On the FPS Model 500 EA running FPX 4.3.0 this is accomplished
	by the %VAL construct.

    NOTES

	The FPX 4.3.0 implementation of these functions is limited to
	allocating 32M per request.

	These functions are portable across all UNIX systems in which
	the C compiler prepends an underscore and the Fortran compiler
	both prepends and appends an underscore.

	The UNIX C library function alloca(3) is intentionally omitted
	because it is machine dependent and its use is therefore to be
	discouraged.

    AUTHOR

	Nick Nystrom

    REVISION LOG

	901026 (NAN) -	Initial revision.

 ************************************************************************/

/* This file may be redistributed only in its original, unaltered form. */

#if __STDC__ == 1
#include <stdlib.h>
#else
char *calloc(), *malloc(), *realloc();
#endif

char *calloc_(nelem,elsize)
long *nelem, *elsize;
{
    return( calloc( (unsigned) *nelem, (unsigned) *elsize ) );
}

char *malloc_(size)
long *size;
{
    return( malloc( (unsigned) *size ) );
}

char *realloc_(ptr,size)
char *ptr;
long *size;
{
    return( realloc( ptr, (unsigned) *size ) );
}

void free_(ptr)
char *ptr;
{
    free(ptr);
}

