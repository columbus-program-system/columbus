#!/bin/sh
#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/
#
# The file fdma.c was written by Nick Nystrom, Chemistry Department,
# University of Pittsburgh, for the FPS 500EA.  This routine must be
# compiled and loaded into colib.a in order to perform runtime memory
# allocation on the FPS.
#
# Ron Shepard
# shepard@tcg.anl.gov

     cp $COLUMBUS/colib.a .
     make -f $COLUMBUS/makefile fdma.o
     ar rv colib.a fdma.o
     ranlib colib.a
     $COLUMBUS/xupdate colib.a

