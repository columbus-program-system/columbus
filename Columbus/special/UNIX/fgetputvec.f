!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
          program testgetputvec

          implicit none

          double precision vector(10),v(10)
          integer  i,j,k
          integer*8 i8,j8,k8

          open(unit=6,file='output')
          
          do i=1,10
            vector(i)=100.d0-i
          enddo
          write(6,*) 'initializing civfl'
          write(6,*) 'IN'
          write(6,50) (i,vector(i),i=1,10)
          i=3
          j=10
          i8=2
c         call getputvec(i, vector,j,i8,'new')
c         call getputvec(i, vector,j,i8,'close')
c         call getputvec(i, vector,j,i8,'openwt')
          call getputvec("civfl", vector,j,i8,'new')
          call getputvec("civfl", vector,j,i8,'close')
          call getputvec("civfl", vector,j,i8,'openwt')

          i=2
          j=10
          i8=0
c         call getputvec(i, vector,j,i8,'new')
c         call getputvec(i, vector,j,i8,'close')
c         call getputvec(i, vector,j,i8,'openwt')
          call getputvec("cihvfl", vector,j,i8,'new')
          call getputvec("cihvfl", vector,j,i8,'close')
          call getputvec("cihvfl", vector,j,i8,'openwt')


          i=3 
          j= 7
          i8=4
c          call getputvec(i, v,j,i8,'openrd')
          call getputvec("civfl", v,j,i8,'openrd')
          write(6,*) 'OUT'
          write(6,50) (i,v(i),i=1,j)
 50       format(10(i3,f12.4))
          call getputvec("civfl", v,j,i8,'delete')
          call getputvec("cihvfl", v,j,i8,'delete')
          
          close(6)
          stop
          end



