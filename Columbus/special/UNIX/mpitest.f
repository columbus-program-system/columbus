!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      program mpi_test

      implicit none
      include 'mpif.h'
      integer*4  ierror,myrank,nproc
      integer   ierrhost,ierrwork,iproc
      integer   hostnm,workdir
      character*64 name,work

      call mpi_init(ierror)

      call mpi_comm_rank(mpi_comm_world,myrank,ierror)
      call mpi_comm_size(mpi_comm_world,nproc,ierror)
      ierrhost = hostnm(name(1:64) )
      ierrwork=workdir(work(1:64))

      do iproc=0,nproc-1
      if (myrank.eq.iproc) then
        write(0,'(a,i4)') 'Process #',myrank
        if (ierrhost.eq.0) 
     .     write(0,'(a,a)') '    hostname:',(name)
        if (ierrwork.eq.0) 
     .      write(0,'(a,a)') '    workdir:',(work)
      endif
      call mpi_barrier(mpi_comm_world,ierror)
      enddo  
      call mpi_finalize(ierror)
      stop
      end
 
