#!/usr/bin/env colperl
#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/

use Config;
use lib join('/',$ENV{"COLUMBUS"},"CPAN") ;

BEGIN { $Curses::OldCurses = 1; }
use Curses;                     # PerlMenu needs "Curses"
use File::Copy 'cp';
use colib_perl;
use colib_curses_perl;
use perlmenu;                   # Main menu package (Perl5 only)
require "menuutil.pl";        # For "pause" and "print_nl" routines.

$| = 1;             # Flush after every write to stdout
$debug=1; 
if ( $debug ) { open (DEB,">DEBUG");}

 $VERSION="Version 0.104 - 09/04/01";
#-----------------------------------------------------------
# global variables taken from infofl
#
# $ndrt  : number of DRTs 
# @nmpsy : number of orbitals per irrep
# @labels: irrep labels
# $nsym  : number of irreps
#
# @strd  : passing of ref doubly occupied orbitals
#                     excitation level, state symmetry
#                     allowed reference symmetries  per irrep
#
# @docc  : frozen core per irrep (counted from the bottom)
# @cas   : frozen virtual per irrep (counted from the top)
# @ras   : orbitals doubly occupied in all references (counted from the bottom)
# @aux   : CI active orbitals (variably occupied in all references)
# @nel   : number of electrons
# @mult  : multiplicity
# @sym   : spatial symmetry
# @exc[idrt][0] aux excitation
# @exc[idrt][1] ras excitation
# 
#-----------------------------------------------------------
#  @allroots: number of roots per drt
#-----------------------------------------------------------
# # $groups[$nogsets]{$el}   group definition set nogsets group el
# #                  $elmin minimum occupation
# #                  $elmax maximum occupation
# # $nogsets          number of group sets
#
#  contained in  @groupdefs[$drt][0] = $nogsets
#                @groupdefs[$drt][1] = \@groups
#----------------------------------------------------------------- 
  @groupdefs=();

 open ( INFOFL, "infofl") || die "infofl missing \n";
 $/="\n";
 $ptgrp=<INFOFL>;
 $_=<INFOFL>;
 s/^ *//g;
 @nmpsy = split /\s+/,$_ ;
 $nsym=<INFOFL>;
 chop $nsym;
 $_=<INFOFL>;
 s/^ *//g;
 @labels = split /\s+/,$_ ;
 close INFOFL;

 if ( $debug ) { print DEB " nsym=$nsym\n"; }

 #felix plasser: add functionality to remember user input
 open (IN, "<makmcky"); # open key stroke file for reading the data of the previous run
 our @lines=<IN>;
 chomp @lines;
 close (IN);
 $aline=0;
 close(IN);
 open (OUT, ">makmcky.new"); # open for writing the data of this run

 $window=initscr();
  &menu_curses_application($window);
 &menu_init(1,"",1," ");
 $sel = &menu_display("",$a,$b,$c);

 &mcdrt();
 &prodmcscfin($skipdrt);
 if ($transmom) {&transin;}
 &endwin();
 system("mv makmcky.new makmcky");
 system("stty sane");
 &printbummer();
 close DEB; 
 exit;

 sub printbummer {
  open BUMMER,">bummer";
  print BUMMER "normal termination of makmc.pl\n";
  close BUMMER;
  return;
  }


 sub mcdrt {


 &obpart;

 $row=20; $col=0;


# each DRT is characterized by mulitplicity
#                              number of electrons
#                              spatial symmetry
#                              configuration space
#
# but ALL DRTs share the same orbital space structure
#  

  clear();
  &addstr("MCSCF WAVE FUNCTION DEFINITION \n"
             . "============================== \n"
             . "(for an explanation see the COLUMBUS documentation and tutorial)\n");
 $row=1;
# clear();

   $yn = &getyesno(8,"Freeze orbitals prior to MCSCF (no gradients available) [y|n]",1);
   if ( grep (/[yY]/,$yn) )
   { $mcfreeze=1; $gradient = 0;}
   else
   {
    $mcfreeze=0;
 $gradient = &getnumber(8,
       "prepare input for no(0), CI(1), MCSCF(2), SA-MCSCF(3) analytical gradient",1,0,3);
	}
 { my $drtinp ;
    if ( -s "mcscfin" && ( -s "mcdrtin" || -s "mcdrtin.1")  ) 
    { $drtinp = &getyesno(9,"Skip DRT input (y|n)",1);
      if ( grep(/[yY]/, $drtinp)  )
     { $skipdrt=1;
       cp("mcscfin","mcscfin.old");
       open MCSCFIN, "<mcscfin.old";
       $ndrt=0;
       while (<MCSCFIN>)
       { if (/NAVST/i) {$ndrt++;}
         print DEB "$ndrt\n"; }
       close MCSCFIN;
       if (! $ndrt) {$ndrt=1;}
       return; }
     else { $skipdrt=0;}
    }
    else {$skipdrt=0;}
 }


if ($gradient == 2 ) { $ndrt=1;}
else        
 {$ndrt = &getnumber(8,"Enter number of DRTS [1-8]  ",1,1,8);}

 &clearline(8);
 $qrow = 6+$ndrt+10;
 my $rasexc,$auxexc;
 $rasexc=0;
 $auxexc=0;

 my ($num_at, $charge, $mass, %sumformula) = &get_geom_info("geom");
 for ($idrt = 1; $idrt <= $ndrt ; $idrt++)
 {
  while ( 1 )
  {
                  #############
  $nel[$idrt] = &getnumber($qrow,"number of electrons for DRT #$idrt (nucl. charge: $charge)",3,1,999);
  $str=pack("A3A1A13A3","DRT",$idrt,": #electrons:",$nel[$idrt]);
  &mvstr(5+$idrt,0,$str);
                  #############
  $mult[$idrt] = &getnumber($qrow,"multiplicity for DRT #$idrt",2,1,$nel[$idrt]+1);
   if (($nel[$idrt] %  2) ==  ($mult[$idrt] % 2)) 
     { &mvstr($LINES-3,0,"inconsistent number of electrons and multiplicity");
       }
   else
     { if ( $nel[$idrt] < ($mult[$idrt]-1))
     { &mvstr($LINES-3,0,"inconsistent number of electrons and multiplicity");
       }
     else
      {$str1=pack("A5A3","mult:",$mult[$idrt]);
       &mvstr(5+$idrt,0,$str . $str1);
       &clearline($LINES-3);
       last; }
    }
   }
                  #############
  $sym[$idrt] = &getnumber($qrow,"spatial symmetry for DRT #$idrt",1,1,$nsym);
  $str2=pack("A4A3","sym:",$labels[$sym[$idrt]-1]);
  &mvstr(5+$idrt,0,$str . $str1 . $str2);
                  #############
  $exc[$idrt][0] = 
     &getnumber($qrow,"excitation level (cas,ras)->aux", 1,0,$nel[$idrt]);
  $exc[$idrt][1] = 
     &getnumber($qrow,"excitation level ras->(cas,aux)", 1,0,$nel[$idrt]);
  $str3=pack("A4A2A2","exc:",$exc[$idrt][0],$exc[$idrt][1]);
  &mvstr(5+$idrt,0,$str . $str1 . $str2 . $str3 );
  if ($exc[$idrt][0]) {$auxexc=1;}
  if ($exc[$idrt][1]) {$rasexc=1;}

  &clearline($qrow);
                  #############
#
# also get title string for each drt
#

 }
 
   $stat1=&gendata($nel,$auxexc,$rasexc);

   $nelfrz = 0;
   if ($mcfreeze) {
	foreach ( @frzc ) {
    $nelfrz += 2*$_;
	}
	&frzprep();
   }
   
#
# apply group restrictions, one drt after the other
#
 $x1=13+$ndrt+2;
 $x2=$x1+3;
 my $groups;

 for ($idrt = 1; $idrt <= $ndrt ; $idrt++)
 { 
   $nel[$idrt] -= $nelfrz;
   $groups =
   &getyesno($x2,"Apply add. group restrictions for DRT $idrt [y|n]",1);
   &clearline($x2);
   if ( grep (/[yY]/,$groups) )
      { $restrictdrt=1;
        if ( $idrt > 1) 
      {
       $groups =
       &getyesno($x2,"Take over group restrictions from DRT 1 [y|n]",1);
       &clearline($x2);
       if ( grep (/[yY]/,$groups))
       { $groupdefs[$idrt][0]=$groupdefs[1][0];
         $groupdefs[$idrt][1]=$groupdefs[1][1];}
       else
      { ( $groupdefs[$idrt][0], $groupdefs[$idrt][1]) =
                               &grouprestrictions($x1,$x2,$idrt);}
      }
      else
      { ( $groupdefs[$idrt][0], $groupdefs[$idrt][1]) =
                               &grouprestrictions($x1,$x2,$idrt);}
      }
 }

#
# print mcdrtfiles one after the other
#
 for ($idrt = 1; $idrt <= $ndrt ; $idrt++)
 { &mcdrtout($idrt);}

#&pause;
  return; 
  }
  $drtinp = &getyesno(9,"check (y|n)",1);
 

 


 sub obpart {
 
&mvstr(1,0,"=============================================");
&mvstr(2,0,"====   MCSCF-DRT input facility            ==");
&mvstr(3,0,"====      $VERSION         ==");
&mvstr(4,0,"=============================================");
 return 0;
 } 



 
 sub gendata {


#       irrep1  irrep2   irrep3  irrep4  
# nmo     x       x        x        x  
# DOCC    x       x        x        x
# CAS     x       x        x        x
# RAS     x       x        x        x
# AUX     x       x        x        x 
  
  my ( $nelec,$auxexc,$rasexc ) = @_ ; 
  local ($x,$str1,$nmo,$ierr,$falsch);
  local ( $docc,$cas,$ras,$aux,$ext);

 $qrow = 6 + $ndrt +10;
 $str1=pack("A15A6A6A6A6A6A6A6A6","irreps",@labels);
 $nmo =pack("A15A6A6A6A6A6A6A6A6","# basis fcts ",@nmpsy);
 &mvstr(6+$ndrt+2,0,$str1);
 &mvstr(6+$ndrt+3,0,$nmo);

   &mvstr(6+$ndrt+1,0,"count order (bottom to top): DOCC - RAS  - CAS - AUX "); 

               ############
  if ($mcfreeze) {
  $frzc = &getvector($qrow,"number of frozen core orbitals per irrep",$nsym,"");
  @frzc = split(':',$frzc);
  $str1=pack("A15A6A6A6A6A6A6A6A6","frzc",@frzc);
  &mvstr(6+$ndrt+4,0,$str1);
  }
               ############
  $docc = &getvector($qrow,"number of doubly occupied orbitals per irrep",$nsym,"");
  @docc = split(':',$docc);
  $str1=pack("A15A6A6A6A6A6A6A6A6","docc",@docc);
  &mvstr(6+$ndrt+5,0,$str1);
               ############
  if ( $rasexc )
    {$ras = &getvector($qrow,"number of RAS orbitals per irrep",$nsym,"");
     @ras = split(':',$ras);
     $str1=pack("A15A6A6A6A6A6A6A6A6","ras",@ras);
     &mvstr(6+$ndrt+6,0,$str1);}
               ############
     $cas = &getvector($qrow,"number of CAS orbitals per irrep",$nsym,"");
     @cas = split(':',$cas);
     $str1=pack("A15A6A6A6A6A6A6A6A6","cas",@cas);
     &mvstr(6+$ndrt+7,0,$str1);
               ############
  if ( $auxexc ) 
  {$aux = &getvector($qrow,"number of AUX orbitals per irrep",$nsym,"");
  @aux = split(':',$aux);
  $str1=pack("A15A6A6A6A6A6A6A6A6","aux",@aux);
  &mvstr(6+$ndrt+8,0,$str1); }
               ############
  if ($mcfreeze) {
  $frzv = &getvector($qrow,"number of frozen virtual orbitals per irrep",$nsym,"");
  @frzv = split(':',$frzv);
  $str1=pack("A15A6A6A6A6A6A6A6A6","frzv",@frzv);
  &mvstr(6+$ndrt+9,0,$str1);
    @intext = ();		
    for ($isym = 0; $isym < $nsym ; $isym++)	
    {  
	  $intext[$isym] = $nmpsy[$isym] - $frzc[$isym] - $frzv[$isym];
    }
  }

  &clearline($qrow);
               ############
			  
	
	
# carry out some checks

 $x=0;
 $falsch="";
 $ierr=0;
 for ($x = 0; $x < $nsym ; $x++)
 { $ext[$x]=$nmpsy[$x]-$docc[$x]-$ras[$x]-$cas[$x]-$aux[$x];
   if ( $ext[$x] < 0 ) { $falsch = join(" ",$falsch,$labels[$x]);$ierr++;} }
 if ($ierr) 
   { $str1=pack("A15A6A6A6A6A6A6A6A6","virtual",@ext);
     &mvstr($qrow,0,$str1); 
     &mvstr($qrow+1,0,"input errors for irrep(s) $falsch");
     &mvstr($qrow+2,0,"repeat input");
     &pause("press return to exit "); exit; }
  return $ierr;

}

  sub frzprep {
	open (TRIN,">tranin.mcfreeze");
	print TRIN "&input\n LRC1MX=0\n LRC2MX=0";
	
        open(INT,"<intprogram");
        $/="\n";
        my @intprog= <INT>;
        close INT;
        if ( grep(/seward/, @intprog) ) {print TRIN "\n SEWARD=1";}
        
	$iorb = 1;
	for ($isym = 0; $isym < $nsym ; $isym++)	
    {  
		if ($frzc[$isym] gt 0) {
		  print TRIN "\n FREEZE($iorb)=";
		  print TRIN "-1 " x $frzc[$isym];
		}
		$iorb += $frzc[$isym] + $intext[$isym];
		if ($frzv[$isym] gt 0) {
		  print TRIN "\n FREEZE($iorb)=";
		  print TRIN "0 " x $frzv[$isym];
		}
		$iorb += $frzv[$isym];
	}
	
	print TRIN "\n&end\n";
	close TRIN;
	
	open (MULT,">mo_multin");
	print MULT "&input\n mo_left=\'mocoef.full\'\n";
    print MULT " mo_right=\'insert\'\n mo_out=\'mocoef.mult\'\n";
	print MULT " lvprt=1\n";
    print MULT " nfrzcr(1)=@frzc\n";
    print MULT " nfrzvr(1)=@frzv\n &end\n";
	close MULT;
	
	open (INF, ">infofl.mcfreeze");
	print INF "$ptgrp";
	print INF "@intext\n";
	print INF "$nsym\n";
	print INF "@labels\n";
	close INF;
  }


  sub mcdrtout { 

  local ($idrt);
  ( $idrt) = @_;
  my (@scr,$scr1,$scr2);

  open (MCDRTKY,">mcdrtin.$idrt");
  print MCDRTKY " $mult[$idrt] / input the spin multiplicity \n";
  print MCDRTKY " $nel[$idrt]  / input the total number of electrons \n";
  print MCDRTKY " $nsym / input the number of irreps \n";
  print MCDRTKY " Y / input symmetry labels ";
  foreach $str1 ( @labels )
     {print MCDRTKY "\n $str1 ";}
  print MCDRTKY "\n $sym[$idrt] / input the molecular symmetry \n";
#
# doubly occupied orbitals
#
  @scr=();
  $i=1;
  $ndocc=0;
  while ( $i <= $nsym )
   {  $j=1;
     while ( $j <= $docc[$i-1] )
      { push @scr,$i,$j; $j++; $ndocc++; }
     $i++;
    }
   &printarray(2,\@scr," / doubly occupied orbitals ",' '); 
    #
    # ras  cas aux orbitals
    #
      @scr=();
      $i=1;
      $nras=0;
      while ( $i <= $nsym )
       { $j=1;
         while ( $j <= $ras[$i-1] )
          {  $x = $docc[$i-1]+$j; 
             push @scr, $i,$x; $j++; $nras++; }
         $i++;
       }
       if ( $nras ) {&printarray(2,\@scr," ",' ');} 

      @scr=();
      $i=1;
      $ncas=0;
      while ( $i <= $nsym )
        { $j=1;
          while ( $j <= $cas[$i-1] )
           {  $x = $docc[$i-1]+$ras[$i-1]+$j; 
              push @scr,$i,$x; $j++; $ncas++; }
          $i++;
        }
      if ( $ncas ) { &printarray(2,\@scr," ",' ');} 

  @scr=();
  $i=1;
  $naux=0;
  while ( $i <= $nsym )
   { $j=1;
     while ( $j <= $aux[$i-1] )
      {  $x = $docc[$i-1]+$ras[$i-1]+$cas[$i-1]+$j; 
        push @scr,$i; push @scr,$x; $j++; $naux++; }
     $i++;
    }
   &printarray(2,\@scr,"/ active orbitals ",' '); 
   
#
# active electrons = $nel[$idrt]- $ndocc*2
# occmin ras       = $nras*2-$exc[$idrt][0]
# occmax ras       = $nras*2
# occmin aux       = 0 
# occmax aux       = $exc[$idrt][1]
# occmin cas       = 1. cas : occmin ras  last cas: nactive - $exc[$idrt]
# occmax cas       = active electrons
#
 $nactel = $nel[$idrt]- $ndocc*2;

 if ( $nactel ) 
  {
    #RAS CAS AUX
    $occminras = $nras*2-$exc[$idrt][1]; 
    if ($occminras < 0 ) { $occminras=0;}
    $occmaxras = $nras*2; 
    $occminaux = $nactel-$exc[$idrt][0];
    $occmaxaux = $nactel;
    $occmincas = $occminras;
    $occmaxcas = $nactel;
    my $nrasm,$ncasm,$nauxm;
    if($nras) {$nrasm = $nras-1;}
    if($ncas) {$ncasm = $ncas-1;}
    if($naux) {$nauxm = $naux-1;}

    @scr=();
    if ( $nrasm  ) { @scr= (  (0)  x ($nrasm) );}
    if ( $nras  )  { push @scr,$occminras;}
    if ( $ncasm  ) { push @scr, ( $occmincas)  x ($ncasm )  ; }
    if ( $ncas  )  { push @scr, $occminaux ;}
    if ( $nauxm  ) { push @scr ,( $occminaux) x ($nauxm)  ;}
    if ( $naux  )  { push @scr,$nactel;}
    &printarray(1,\@scr," / occmin ",' ' );

    @scr=();
    if ( $nras ) { @scr=($occmaxras)  x ($nras ) ;}
    if ( $ncas )  {push @scr, ($occmaxcas)  x ($ncas) ;}
    if ( $naux)  { push @scr, ($occmaxaux)  x ($naux);}
    &printarray(1,\@scr," / occmax ",' ');

    print MCDRTKY "  / bmin \n  /bmax \n / step masks \n";
  }
    print MCDRTKY " 0/input the number of vertices to be deleted \n";
    print MCDRTKY " n/are any arcs to be manually removed? \n";
    print MCDRTKY " /drt levels to be printed \n";

    if ( ! $nactel ) 
          { print MCDRTKY " y/keep all of these walks?\n";}
    else
    {if ( ! $groupdefs[$idrt][0] ) 
          { print MCDRTKY " n/keep all of these walks?\n";
             print MCDRTKY 
           " N / impose additional orbital-group occupation restrictions?\n";}

    else 
          {  print MCDRTKY " n/ keep all of these walks?\n"; 
             print MCDRTKY 
           " Y / impose additional orbital-group occupation restrictions?\n";
            print MCDRTKY "$groupdefs[$idrt][0] / number of orbital group sets \n";
            for ( $j=1; $j <= $groupdefs[$idrt][0]; $j++ )
               { my @group = @{$groupdefs[$idrt][1]};
                 my @orbgroups= keys %{$group[$j]};
                 printf MCDRTKY " %3d  / number of orbital groups in set $j \n",
                                           int($#orbgroups+1)/3;
                 for  $el (@orbgroups)
                  { if (grep(/min/,$el) || grep (/max/,$el)) {next;}
                    $elmin =join('',$el,"min");
                    $elmax =join('',$el,"max");
                    print XX "elmin,elmax=$elmin,$elmax, el=$el\n";
                    printf MCDRTKY " %3d %3d %3d \n",
                    $#{$group[$j]{$el}}+1, $group[$j]{$elmin}, $group[$j]{$elmax} ;
                  }
                  print MCDRTKY " / ngroup occmin occmax \n";
                 for  $el (@orbgroups)
                  { if (grep(/min/,$el) || grep (/max/,$el)) {next;}
                   $glevel = join (' ',@{$group[$j]{$el}});
                   print MCDRTKY " $glevel \n";
                  }
                  print MCDRTKY " / glevel \n";

                }
          }

        print MCDRTKY " N /apply primary reference occupation restrictions? \n" ;
        print MCDRTKY " N /keep all these walks \n "; 
        print MCDRTKY "  -1 /\n ";}
        print MCDRTKY " 0 / input walk number (0 to end)"; 
        print MCDRTKY " \n title\nmcdrtfl\n";
        print MCDRTKY " y/ write the drt file \n";
        print MCDRTKY " y/ include step vectors\n";
  close MCDRTKY;
  return 0; 
  }


  sub prodmcscfin {
  
  local ($skipdrt);
   ($skipdrt ) = @_;

@input_data = ();	# Place to put data entered on screen
@defaults = ();		# Default data
@protect = ();		# Protected markers
@required = ();		# Required field markers
$bell = "\007";		# Ascii bell character
$row = $col = 0;	# Storage for row/col used by menuutil.pl

  print DEB 'entering prodmcscfin\n';
$defaults[0] = 100  ; # number of mcscf iterations
$defaults[1] = 50   ; # number of micro iterations
$defaults[2] = 300  ; # number of ci iterations
if ($gradient == 1)
{
$defaults[3] = '1.e-5'  ; # knorm
$defaults[4] = '1.e-5'  ; # wnorm
}
else
{
$defaults[3] = '1.e-4'  ; # knorm
$defaults[4] = '1.e-4'  ; # wnorm
}

$defaults[5] = '1.e-8'  ; # delta e
$defaults[6] = 'n'    ; # build HMC explicitly
$defaults[7] = 'y'    ; # diagonalize HMC iteratively
$defaults[8] = 'y'    ; # quadratic convergence
$defaults[9] = 5      ; # from iteration 5 onwards
$defaults[10] = '1.e-3'; # wnorm tolerance
#
if  (-s "mcscfin.old")
#fp: if mcscfin is present, read some of the values
{  
   $defaults[0] = &keyinfile("mcscfin.old","niter");
   $defaults[1] = &keyinfile("mcscfin.old","nmiter");
   $defaults[2] = &keyinfile("mcscfin.old","nciitr");
#   $defaults[3] = &keyinfile("mcscfin.old","tol(3)");
   $defaults[9] = &keyinfile("mcscfin.old","ncoupl");
}
if ( ! $skipdrt ) 
  {
   if ( $gradient )
    {
     $defaults[11] = 'NO' ; # RAS resolution
     $defaults[12] = 'NO' ; # CAS resolution
     $defaults[13] = 'NO' ; # AUX resolution
     @protect[11..13] = (1,1,1);
     }
   else
    {
     $defaults[11] = 'QAA' ; # RAS resolution
     $defaults[12] = 'QAA' ; # CAS resolution
     $defaults[13] = 'QAA' ; # AUX resolution
    }
   }
 else
 {$defaults[11] = 'file' ; # RAS resolution
  $defaults[12] = 'file' ; # CAS resolution
  $defaults[13] = 'file' ; # AUX resolution
  @protect[11..13] = (1,1,1);
 }

 $i=0;
 do  { $i++; $defaults[13+$i]=1; $i++; $defaults[13+$i]=1.0; }
 until $i == $ndrt*2; 

if  ((-s "mcscfin.old") && ($ndrt==1))
{
#fp: in this case it is easy to read in the navst value
   $defaults[14] = &keyinfile("mcscfin.old","navst");
   $defaults[15] = '1';
   for ($i=1;$i<$defaults[14];$i++)
   {
      $defaults[15] .= ",1";
   }
}

$defaults[14+$ndrt*2] = 'N'; # no transition moments

$templstring = 
 "
   Convergence
      1. Iterations              #iter [___]    #miter [___]   #ciiter [___]
      2. Thresholds              knorm [______]  wnorm [______]  DE [______]
                               
      3. HMC-matrix              build explicitly [_]
                                 diagonalize iteratively [_]
      4. Miscellaneous           quadratic cnvg. [_] from #iter [___]
                                       ... only with wnorm < [______]
 ";
 if ( $skipdrt)
   { $templstring .= "
   Resolution (file)             RAS [____] CAS [____] AUX [____]   \n";}
 else
 {
 if ( $gradient ) 
   { $templstring .= "
   Resolution (NO)               RAS [____] CAS [____] AUX [____]   \n";}
   else 
   { $templstring .= "
   Resolution (none|NO|QAA|FAA)  RAS [____] CAS [____] AUX [____]  \n";} 
 }

 if ( not $gradient == 2 )
    { $templstring .= "  State-averaging";     
    for ( $idrt=1; $idrt <=$ndrt ; $idrt++)
     { $templstring .= "     
                          DRT $idrt: #states [__] weights[____________________]";
     }}
 else
    { $templstring .= "\n             State to be optimized [__]  [__] ";  }

 $templstring .= "
                  transition moments / non-adiabatic couplings [_] ";
 $templstring .= "

                             FINISHED [_]

   Editing: left/right, 'delete'; Switching fields: 'Tab', up/down
   Help is available through selecting a field and pressing 'return'.
   Indicate completion by selecting 'Finished' and pressing 'return'. ";



@required=(0) x 28;

#
# Activate left and right markers, both in standout rendition.
#
  &menu_template_prefs("*"," ",1,"*"," ",1);

  &menu_load_template_array(split("\n", $templstring));

  &menu_display_template(*input_data,*defaults,*protect,"mcscf_help",
			 *required);
  &refresh();

#  
#  print mcscfin file
#

    unlink("mcscfin");
   my $flags="1,3,9,10,13,17,19,21,";

   open MCSCFIN,">mcscfin";
   print MCSCFIN " &input \n";
   print MCSCFIN "  niter=$input_data[0], \n";
   print MCSCFIN "  nmiter=$input_data[1], \n";
   print MCSCFIN "  nciitr=$input_data[2], \n";
   print MCSCFIN "  tol(3)=$input_data[3], \n";
   print MCSCFIN "  tol(2)=$input_data[4], \n";
   print MCSCFIN "  tol(1)=$input_data[5], \n";
   if ( $gradient == 2 ) { print MCSCFIN "  NSTATE=",$input_data[14]-1," \n";}
   else                { print MCSCFIN "  NSTATE=0, \n";}
#
#gradienten: 11,-12 ???
#normal     -11,12
#ao         -11,12,24
#
#  one out of four combinations for the input fields 6 and 7
#  is invalid, hence restore to some default

   if (grep(/[nN]/,$input_data[6]) &&
       grep(/[nN]/,$input_data[7]) ) {$flags.="-11,12"}
   else {
      if (grep(/[yY]/,$input_data[6])) {$flags.="11,";}
                                 else  {$flags.="-11,";}

      if (grep(/[yY]/,$input_data[7])) {$flags.="12,";}
                                 else  {$flags.="-12,";}
        }

   if (grep(/[yY]/,$input_data[8])) {$flags.=" 2,";}
                             else  {$flags.="-2,";}
#fp: compute density matrices in mcscf.x, flag 30 is added in runc
#   if ( $gradient == 3 ) {$flags.="30,";}
   print MCSCFIN "  npath=$flags\n";
   print MCSCFIN "  ncoupl=$input_data[9],\n";
   print MCSCFIN "  tol(9)=$input_data[10],\n";

  if ( $skipdrt )
   { local ( $fciorb);
     print DEB "vor extracfciorb\n"; 
     $fciorb = &extractfciorb("mcscfin.old");
     print MCSCFIN " $fciorb";}
  else
   {
#
# Resolution Isym, iorb,imask
# imask = 1 (no resol), 2 (no), 3 (faa) 4 (qaa)
#
  if (grep(/NO/,$input_data[11])) {$imaskras=2;}
  if (grep(/none/,$input_data[11])) {$imaskras=1;}
  if (grep(/FAA/,$input_data[11])) {$imaskras=3;}
  if (grep(/QAA/,$input_data[11])) {$imaskras=4;}

  if (grep(/NO/,$input_data[12])) {$imaskcas=20;}
  if (grep(/none/,$input_data[12])) {$imaskcas=10;}
  if (grep(/FAA/,$input_data[12])) {$imaskcas=30;}
  if (grep(/QAA/,$input_data[12])) {$imaskcas=40;}

  if (grep(/NO/,$input_data[13])) {$imaskaux=200;}
  if (grep(/none/,$input_data[13])) {$imaskaux=100;}
  if (grep(/FAA/,$input_data[13])) {$imaskaux=300;}
  if (grep(/QAA/,$input_data[13])) {$imaskaux=400;}
#   

   if ( $restrictdrt !=1) {
     my @scr;
     @scr=();
     $i=1;
      while ( $i <= $nsym )
       { $j=1;
         while ( $j <= $ras[$i-1] )
          {  $x = $docc[$i-1]+$j;
             push @scr,$i,$x,$imaskras; 
             $j++;}
         $i++;
       }

     $i=1;
      while ( $i <= $nsym )
       { $j=1;
         while ( $j <= $cas[$i-1] )
          {  $x = $docc[$i-1]+$ras[$i-1]+$j;
             push @scr,$i,$x,$imaskcas; 
             $j++;}
         $i++;
       }

     $i=1;
      while ( $i <= $nsym )
       { $j=1;
         while ( $j <= $aux[$i-1] )
          {  $x = $docc[$i-1]+$ras[$i-1]+$cas[$i-1]+$j;
             push @scr,$i,$x,$imaskaux; 
             $j++;}
         $i++;
       }
      if ( $#scr > 1 ) 
       { print MCSCFIN "  FCIORB= "; 
         &printarray(3,\@scr," ", ",",1); }
      } # if ( $restrictdrt !=1) 
  }  # skipdrt

 if ( $gradient != 2 )
  {for ($idrt=1;$idrt<=$ndrt;$idrt++)
   { my ( $aa );
     $aa=$input_data[12+$idrt*2]; 
     print MCSCFIN "  NAVST($idrt) = $aa,\n"; 
     $input_data[13+$idrt*2] =~ s/,/ /g;
     $input_data[13+$idrt*2] =~ s/  / /g;
     $input_data[13+$idrt*2] =~ s/ *$//g;
     $input_data[13+$idrt*2] =~ s/^ *//g;
     @w = split(/\s+/,$input_data[13+$idrt*2]);
     if ( $#w+1  != $input_data[12+$idrt*2] ) 
          { 
#  print MCSCFIN "w=",join(':',@w),"...\n";
   die ("incompatible weights and averaging states $#w $input_data[13+$idrt*2] \n");}
     for ($i=0; $i<=$#w; $i++) 
       { my ($aa);
         $aa=$i+1;
         print MCSCFIN  "  WAVST($idrt,$aa)=$w[$i] ,\n";} 
  }}
   
  
  print MCSCFIN " &end \n";
  close MCSCFIN;
  if ( grep(/[yY]/,$input_data[13+$ndrt*2+1])) {$transmom=1;}
                                     else     {$transmom=0;}
#  open MOFMTIN,">mofmtin";
#  print MOFMTIN "\n\nmorbl\n\n";
#  close MOFMTIN;

#
#  file tranmcdenin is the input file for tran for the back
#  transformation of the mcscf density matrices
#
 if ($gradient) 
  { open TRAN,">tranmcdenin";                                  
    print TRAN " &input\n  denopt=1,\n  freeze = 1,\n";
    printf TRAN "  nsymao=%2d\n  naopsy= ", $nsym;
    $maxnmpsy = 0;
    for ( $i=0; $i < $nsym; $i++)
    { 
      printf TRAN " %3d,",$nmpsy[$i];
      if ($nmpsy[$i] > $maxnmpsy) {$maxnmpsy = $nmpsy[$i];}
    }
    if ($maxnmpsy >= 128)
    {
        $ldamax = int($maxnmpsy*$maxnmpsy/2)+1024
    }
    else
    {
        $ldamax = 8191
    }
    print TRAN "\n ldamax=$ldamax";
    print TRAN " \n &end \n";
    close TRAN;
#
    if ($gradient == 3) {
        if ( -s "cigrdin")
        {
            $wrcgr = &query("cigrdin exists. Do you want to overwrite it?","yn");
        }
        else {$wrcgr = "y";}
        
        if ($wrcgr eq "y") {&write_cigrdin();}
    }
  }
}

#======================================================================
sub mcscf_help {
  local($direction,$last_index,$next_index,$still_required) = @_;

# Return now if they are skipping between fields
# include all error-checking of input
  if ($direction) { # correct weights 
                     { my $in,$i,$j;
                      $in=0;
                      for ( $i=1; $i <= $ndrt; $i++ )
                     { if ( $last_index == (12+$i*2) || $last_index == (13+$i*2) )
                         { $input_data[13+$i*2]= ' ';
                           $in=1;
                           for ( $j =1; $j < $input_data[12+$i*2];$j++)
                           { $input_data[13+$i*2] = $input_data[13+$i*2] . "1,";}
                           $input_data[13+$i*2] = $input_data[13+$i*2] . "1";
                         }
                     }
                       display_template_internal(1);
                       if ($in)
                       { &menu_overlay_template($LINES-3,3,"                                       ",0,0);
                         return($next_index);}
                    }

                    # iterations >= 1
                    if ( $last_index == 0  && ($input_data[0] < 1) )
                        {&menu_overlay_template($LINES-3,3,"#iter > 0",0,0);
                         return($last_index);}
                    if ( $last_index == 1  && ($input_data[1] < 1) )
                        {&menu_overlay_template($LINES-3,3,"#miter > 0",0,0);
                         return($last_index);}
                    if ( $last_index == 2  && ($input_data[2] < 1) )
                        {&menu_overlay_template($LINES-3,3,"#ciiter > 0",0,0);
                         return($last_index);}
                    # tolerances > 0
                    if ( $last_index == 3  && ($input_data[3] < 0) )
                        {&menu_overlay_template($LINES-3,3,"knorm > 0",0,0);
                         return($last_index);}
                    if ( $last_index == 4  && ($input_data[4] < 0) )
                        {&menu_overlay_template($LINES-3,3,"wnorm > 0",0,0);
                         return($last_index);}
                    if ( $last_index == 5  && ($input_data[5] < 0) )
                        {&menu_overlay_template($LINES-3,3,"DE> 0",0,0);
                         return($last_index);}
                    if ( $last_index == 6  && (! grep(/[yYNn]/,$input_data[6])))
                        {&menu_overlay_template($LINES-3,3,"enter y/n",0,0);
                         return($last_index);}
                    if ( $last_index == 7  && (! grep(/[yYNn]/,$input_data[7])))
                        {&menu_overlay_template($LINES-3,3,"enter y/n",0,0);
                         return($last_index);}
                    if ( $last_index == 8  && (! grep(/[yYNn]/,$input_data[8])))
                        {&menu_overlay_template($LINES-3,3,"enter y/n",0,0);
                         return($last_index);}
                    if ( $last_index == 9   && ($input_data[9] < 0) )
                        {&menu_overlay_template($LINES-3,3,"iter > 0",0,0);
                         return($last_index);}
                    if ( $last_index == 10   && ($input_data[10] < 0) )
                        {&menu_overlay_template($LINES-3,3,"quad. conv. wnorm > 0",0,0);
                         return($last_index);}
                    if ( $last_index == 11 || $last_index == 12 || $last_index == 13) 
                       { if ( $gradient != 2 ) { 
                          if (( ! grep(/NO/,$input_data[$last_index])) &&
                             ( ! grep(/none/,$input_data[$last_index])) &&
                             ( ! grep(/FAA/,$input_data[$last_index])) &&
                             ( ! grep(/QAA/,$input_data[$last_index])))
                        {&menu_overlay_template($LINES-3,3,"enter NO,none,FAA or QAA",0,0);
                         return($last_index);}}
                         else
                          {if ( ! grep(/NO/,$input_data[$last_index])) 
                        {&menu_overlay_template($LINES-3,3,"enter NO",0,0);
                         return($last_index);}}}

                    return($next_index);
                  }

# User says they are done (they pressed "Return").
  &menu_overlay_clear(); # Clear any old overlays
  if ( $last_index < 15+$ndrt*2 ) 
         { local ( @helpstr, $cnt ) ;
          @helpstr= helpmcscf($last_index);
          $cnt=3;
          foreach $x ( @helpstr ) 
          {&menu_overlay_template($LINES-$cnt,3, $x,0,0);
           $cnt--;}
              return($last_index);}
       else
       { my $i,$j,@j;
         for ( $i=1; $i <= $ndrt; $i++ )
           { $j= $input_data[13+$i*2];
             $j=~s/,/ /g;
             $j=~s/^ *//g; $j=~s/ *$//g; $j=~s/  / /g;
             @j=split(/\s+/,$j);
             if ($#j+1 != $input_data[12+$i*2] ) 
              {&menu_overlay_template($LINES-3,3,
               "number of weights and states are not compatible",0,0);
               return(13+$i*2);}
           }  

        return(-1);}
 

# Put out message if there are still required fields.
  if ($still_required) {
    &menu_overlay_template($LINES-5,10,
	"Fields marked with a \"*\" are STILL required.",1);
    return(-1);		# Still need required field(s) - auto-position
  }

# Let them be done.
  return(-1);
}




#========================================================================


 sub transin {

 while (1) {  # always true
 &menu_init($numbered_flag, "COLUMBUS INPUT FACILITY ",0,
                ,"\n-transition moment / coupling selections
                  \n for non-adiabatic coupling calculations:
                  \n select couplings as bra!=ket and additional gradients as bra==ket",         
                $VERSION,"");
 for ( $idrt=1; $idrt <= $ndrt ; $idrt++ )
   {  for ($bra=1; $bra <= $input_data[12+$idrt*2]; $bra++)  
       { for ( $jdrt=$idrt; $jdrt <= $ndrt ; $jdrt++ )
         {  if ( $idrt == $jdrt ) 
              { for ($ket = $bra; $ket <= $input_data[12+$jdrt*2]; $ket++)  
                 {$temp = join ( "  ", ( $jdrt, $ket, $idrt, $bra ));
                  &menu_item("bra: DRT# $idrt state# $bra ket: DRT# $jdrt state# $ket", $temp); }}
            else 
              { for ($ket = 1; $ket <= $input_data[12+$jdrt*2]; $ket++)  
                 {$temp = join ( "  ", ( $jdrt, $ket, $idrt, $bra ));
                  &menu_item("bra: DRT# $idrt state# $bra ket: DRT# $jdrt state# $ket", $temp); }}
          
         }
       }
    }
     $sel = &menu_display_mult("");
     local($found1);
     if ( $sel eq "%NONE%" ) {return 0;}
      else
      { if ( -T "transmomin" ) { open TRN, "<transmomin";
                                 $/="";
                                 $found1=<TRN>;
                                 $found1=~s/\n\n/\n/g;
                                 $found1 =~ s/MCSCF.*CI/CI/sg;
                                 if ( grep /MCSCF/,$found1 ) 
                                         { $found1 =~ s/MCSCF.*$//sg ;}
                                 close TRN; }

        @selec=split(/[,]/,$sel);
        open TRN, ">transmomin";
        print TRN  "$found1"; 
        print TRN  "MCSCF\n";
        $temp = join ( "\n",@selec);
        print TRN "$temp\n";
        close TRN;
        last; 
      }
    } # end while
# modify hermitin as needed
#
      open(INFOFL,"<intprogram");
      $/="\n";
      my (@infofl);
      @infofl= <INFOFL>;
      close INFOFL;


      if ( grep(/hermit/, @infofl) )
                     {  open (HERMITIN,">daltcomm.new");
                        open (HERMITOLD,"<daltcomm");
                          my (@stuff,$newstuff);
                          @stuff = <HERMITOLD> ;
                          close HERMITOLD;
                          if ( grep /\.CARMOM/, @stuff )  {
                            for ( $i = 0; $i <= $#stuff ; $i++)
                              { if ( grep /\.CARMOM/, $stuff[$i] )
                                 { if ( $stuff[$i+1] < 1 ) {
                                   $stuff[$i+1]="    1\n" ; last;}}}}
                            else {
                            for ( $i = 0 ; $i <= $#stuff ; $i++)
                              { if ( grep /\*\*INTEGRALS/,$stuff[$i])
                                { $newstuff = join (':', @stuff[0..$i],
                                  ".CARMOM\n","    1\n",
                                  @stuff[$i+1..$#stuff]);
                                  @stuff = split (/:/,$newstuff);last;}}}
                          print HERMITIN join '',@stuff;
                          close HERMITIN;
                          `cp -f daltcomm.new daltcomm`;
                          } # hermit

    return 0;
   }


  sub helpmcscf {

   local ( $keyel, @z  );
    ( $keyel ) = @_ ; 
      SWITCH: {
           if ( $keyel == 0 ) { $z[0]="Select a MR-SDCI calculation   ";
                                $z[1]="corresponds to NTYPE=0";
                               last SWITCH;}
              } 

   return @z ;  
  } 



 sub grouprestrictions
 { my ($mrow,$qrow,$idrt) =@_;
   my  @group = ( );
   my  $nogsets;
#
# assumes the following arrays to contain valid data:
#  $multi is set if multiple DRTs are used (ordering of fc/refdocc/act)
#
# replace fc by docc
# take the order ras/cas/aux
#================================= group occupations =========================
 &move($mrow,0);
#my $groups = 
#&getzeichen($qrow,"Apply add. group restrictions for DRT $idrt [y|n]",1);
#&clearline($qrow);
 $mrow++;
 $nogsets=0;
#if ( grep (/[yY]/,$groups) ) 
#    { 
       my ($el,$cnt,$i,$j);
       $nogsets=1;
     while ( )
      {
       my @leveldocc = ();
       my @levelcas = ();
       my @levelras= ();
       my @levelaux= ();
       &move($mrow+1,0);
       &addstr("All active orbitals must be distributed among groups.");
       &move($mrow+2,0);
       &addstr("Each group is designated by one uppercase letter A B ...");
       &move($mrow+3,0);
       &addstr("State the group labels below each orbital given:");

      my $str="    ";
      my $str2=' ';
      my ($nfc,$nrefdoc,$von,$bis,$offset,$actel);
#
# docc
#
      $offset=0;
      for ( $i=0; $i<$nsym; $i++) { $offset= $offset+$docc[$i];}
      $ndocc=$offset;

     #
     #ras 
     #

      $offset=0;
      for ( $i=0; $i<$nsym; $i++)
        { $von=$docc[$i]+1;
          $bis=$von+$ras[$i];
        for ( $j= $von; $j< $bis ; $j++)
           { $str=$str . "${j}$labels[$i] ";
             $levelras[$offset]=$offset+1;
             $offset++; }
        }
       $nras=$offset;

      #
      # cas 
      #

      $offset=0;
      for ( $i=0; $i<$nsym; $i++)
        { $von=$docc[$i]+$ras[$i]+1;
          $bis=$von+$cas[$i];
        for ( $j= $von; $j< $bis ; $j++)
           { $str=$str . "${j}$labels[$i] ";
             $levelcas[$offset]=$offset+$nras+1;
             $offset++; }
        }
       $ncas=$offset;
#
#aux
#
      $offset=0;
      for ( $i=0; $i<$nsym; $i++)
        { $von=$docc[$i]+$ras[$i]+$cas[$i]+1;
          $bis=$von+$aux[$i];
        for ( $j= $von; $j< $bis ; $j++)
           { $str=$str . "${j}$labels[$i] ";
             $levelaux[$offset]=$offset+$nras+$ncas+1;
             $offset++; }
        }
       $naux=$offset;

     @levelact = split(/:/, join(':',@levelras, @levelcas, @levelaux ));

#
#  in accordance with the later orbital -> level ordering
#  we have the order  ras cas aux 
#  this list contains already the glevel!
#


      &move($mrow+4,0); 
      print XX "str=$str\n"; 
      print XX "nsym=$nsym \n";
      print XX "docc=@docc \n" ;
      print XX "cas=@cas \n" ;
      print XX "ras=@ras \n" ;
      print XX "aux=@aux \n" ;
      print XX "levelcas=@levelcas \n";
      print XX "levelras=@levelras \n";
      print XX "levelaux=@levelaux \n";
      &addstr($str);
      my $grpdes=&getvector($mrow+5,"->",1,"",1);
      $grpdes = uc $grpdes;
      my @grpdes=split (':',$grpdes);
#
      if ( $#levelact != $#grpdes ) 
        {$str=sprintf "not enough group designations: need %d found %d \n",$#levelact+1,$#grpdes+1;
         &popup_ask($str); next;}

      my $exgroups=" ";
      for $el ( A .. Z )
       { if (grep(/$el/,$grpdes)) { $exgroups = $exgroups . "$el:"; }}
      $exgroups=~s/^ *//;
      for $el ( split(/:/,$exgroups) )
      { $cnt=-1; 
       for ( $i=0 ; $i <= $#levelact ; $i++)
        {if ( $grpdes[$i] eq $el ) 
                    { $cnt++; 
                     $group[$nogsets]{$el}[$cnt]=$levelact[$i];} 
        } 
      }  
      $actel=$nel[$idrt]-2*$ndocc;
      
      while (1)
      {
      my ( $elingroup);
      $elingroup=0;
      foreach $el ( keys %{$group[$nogsets]} )
       { 
      my $minmax=&getnumber($mrow+7,"Enter the number of electrons in group $el ( 0 .. $actel) ",3,0,$actel);
      $elingroup= $elingroup+$minmax;
      my $elx = join('',$el,"min");
      $group[$nogsets]{$elx}=$minmax;
      $elx = join('',$el,"max");
      $group[$nogsets]{$elx}=$minmax;
      $actel=$actel-$minmax;
      }
        last;
      }


#
#  at this point %$group[$nogsets] contains the group information
#  for group set definition $nogsets
#         @group{$el}[] = each entry bfn#:irrep#
#         $group{$el . min } = minimum occupation 
#         $group{$el . max } = maximum occupation 
#                       

     for ( $i=1; $i <=7 ; $i++)
        { &clearline($mrow+$i);}
     print XX "nogsets=$nogsets\n";
 my $anotherone = &getyesno($qrow,"Another group set definition [y|n]",1);
     if ( grep (/[nN]/,$anotherone ) ) {last;}
     $nogsets++;
     } 
#    }
 return ( $nogsets, \@group );
}

 sub getnumber {

  local ($xrow,$text,$xlen,$von,$bis,$number,$gettext);
  ($xrow,$text,$xlen,$von,$bis) = @_;
  my ( $valid );

  $valid=1;
#fp
  if (( $lines[$aline] eq "y") || ($lines[$aline] eq "n" ))  {$aline += 1} #special treatment for "Skip DRT input"
  if ($lines[$aline] eq "") {
    $gettext = "$text   ";}
  else {
    $gettext = "$text \[ $lines[$aline]\]   ";}
  do { $number = &menu_getstr($xrow,0,$gettext,0,"",$xlen,0,1);
  #do { $number = &menu_getstr($xrow,0,$text . "  ",0,"",$xlen,0,1);
         if (( $number eq "" ))
            { $number = $lines[$aline]; }
         if ( ( $number < $von ) || ($number > $bis))
                               { &move($xrow+1,0);
                                &addstr("invalid number"); $valid=0} 
       else {&move($xrow+1,0);
             &clearline($xrow+1);$valid=1}
      }
  until ($valid);
  #fp
  $aline += 1;
  print OUT "$number\n";

  return $number;
 }

 sub getyesno {

  local ($xrow,$text,$xlen,$zeichen,$gettext);
  ($xrow,$text,$xlen) = @_;
  if ($lines[$aline] eq "") {
    $gettext = "$text   ";}
  else {
    $gettext = "$text \[ $lines[$aline]\]   ";}
 do {   $zeichen = &menu_getstr($xrow,0,$gettext,1,"",$xlen,0,0);
         $zeichen =~ s/[A-Z]/[a-z]/g;
   #fp
        if (( $zeichen eq "" ))
            { $zeichen = $lines[$aline]; }
         if ( ! ( $zeichen eq "y" || $zeichen eq "n" ))  
                   { &move($xrow+1,0);
                     &addstr("invalid entry"); $zeichen=0;} 
         else { &move($xrow+1,0);
                &addstr("                  ");}
      }
  
  until ($zeichen);
  #fp
  if ( ( $lines[$aline] eq "y") || ($lines[$aline] eq "n" ))  {$aline += 1}
  print OUT "$zeichen\n";
  return $zeichen;
 }

 sub getvector {

  local ($x,$xrow,$text,$vector,@vector,$minel,$default,$clear,$gettext);
# returns a vector separated by :
  ($xrow,$text,$minel,$default,$clear)=@_;
  if ($lines[$aline] eq "") {
    $gettext = "$text   ";}
  else {
    $gettext = "$text \[ $lines[$aline]\]   ";}
  if ( $clear) {$clear=0;}
 do { $x = &menu_getstr($xrow,0,$gettext, $clear,$default,0,0,0);
      if (( length($x) == 0 ))
            { $x = $lines[$aline]; }
      if ( $debug ) { print DEB "getvector : $x\n";} 
      if ( ( ! $x ) && ( $x ne "0")  ) { &move(15,0);
                       &addstr("invalid number");}
      else {
      $x=~s/^ *//;
      @vector= split /[\s\/]+/,$x;
      $x = @vector;
      if ( $debug ) { print DEB "getvector2 : $x, @vector, $nsym,$minel \n";} 
      if ( $x == 0 ) { @vector = (0) x $nsym ; $x=$nsym;}
      if ( $x <  $minel) { &move($xrow+3,0);
        &addstr("you entered values for $x instead of $minel irreps"); $x=0; }}
    }
  until ($x) ;
  print XX "getvector: $clear\n"; 
  &clearline($xrow+3); 
  $vector = join ':',@vector;
 #fp
  $aline += 1;
  print OUT "@vector\n";
  return $vector;
  }

  sub clearline {
   local ($xrow,$str);
   ($xrow) = @_;
   &move($xrow,0);
   $str = ' ' x 80;
   &addstr($str);
   return;
   }


  sub countrefel {
    local ( $i, $el, @feld );
    ($i)=@_; 
    @feld = split(/:/,$i); 
    $el=0;
    foreach $i ( @feld  ) { $el+=$i}
    $el=2*$el;
    return $el;
    }
   
  sub extractfciorb {
#  FCIORB=
#   RAS orbs max. nsym lines
#   CAS orbs max. nsym lines
#   AUX orbs max. nsym lines
#  NAVST
#  WAVST
#  &end
    my  ( $filename, $fciorb );
    ($filename)=@_;
    $/="";
    open LOCALFILE,"<$filename";
    $fciorb=<LOCALFILE>;
    if (!( grep(/FCIORB/i,$fciorb))) 
        { $fciorb=" "; return $fciorb;}
    $fciorb=~s/^.*FCIORB/FCIORB/sg;
    $fciorb=~s/NAVST.*\n//g;
    $fciorb=~s/WAVST.*\n//g;
    $fciorb=~s/\&end.*\n//g;
    $fciorb=~s/[\n\t ] *$/\n/g;
    close LOCALFILE;
    return $fciorb;
    }
     


 sub printarray
  {
# print an array @$locstr  and add the label $description
# print with at most 72 characters per line and separate
# @$locstr by groups of $nel elements
#
    my ($nel,$locstr,$description,$sep,$tomcscf) = (@_);
    my (@loclocstr,$i,$j,$tmpold,$tmp);
    print DEB "groups of $nel elements, $description,  $#{$locstr} \n";
    print DEB "locstr:", join(':',@$locstr),"\n"; 
    $i=$nel;
    $tmpold='';
     while ($i < $#{$locstr} )
     {@loclocstr=();
      for ( $j =$i-$nel; $j< $i ; $j++)
        { push @loclocstr,$$locstr[$j]; }
      $tmp= join($sep,@loclocstr);
      if ((  length($tmpold) < 2 ) && length($tmp) > 72 )
         {print DEB "printarray: group too large:\n";
          print DEB "$tmpold:$tmp\n";
          die "printarray: group too large \n"; }

      if (( length($tmp)+length($tmpold)+length($sep)) > 72 )
          { if ( $tomcscf ) { print  MCSCFIN  " $tmpold$sep\n";}
                      else  { print  MCDRTKY  " $tmpold$sep\n";}
            $tmpold="$tmp";}
      else {if (length($tmpold)) { $tmpold="$tmpold$sep$tmp";}
                       else      { $tmpold=$tmp;}
           }
      $i+=$nel;}

      @loclocstr=();
      for ( $j = $i-$nel; $j <= $#$locstr; $j++)
        { push @loclocstr,$$locstr[$j]; }
      $tmp= join($sep,@loclocstr);
      if (( length($tmp)+length($tmpold)+length($sep)) > 72 )
          { if ( $tomcscf ) { print  MCSCFIN  " $tmpold$sep\n";}
                      else  { print  MCDRTKY  " $tmpold$sep\n";}
            $tmpold="$tmp"; }
      else {if (length($tmpold)) { $tmpold="$tmpold$sep$tmp";}
                       else      { $tmpold=$tmp;}
           }
           if ( $tomcscf ) { print  MCSCFIN  " $tmpold\n";}
                     else  { print  MCDRTKY  " $tmpold $description\n";}
  return;
  }




  sub mvstr
  { my ( $r,$c,$str) = (@_);
    &move ( $r, $c);
    &addstr( $str);
    return;
  }


  sub displaytext 
  { my ($txt,@txt,@xline) = (@_);
    my ($currline,$txt1,$txt2,$i);
    # width of window: $COLS
    # text is given as a single line with \n 
    # and need to be broken into separate lines at word boundaries
    # if necessary
    # strip off leading blanks

    @txt=split(/\n/,$txt);
    $currline=2;
    foreach $txt1 ( @txt ) 
     { if (length($txt1) < ($COLS-2) ) { $txt1=~s/^ *//;
                                         &mvstr($currline,1,$txt1);
                                         $currline++;}
        else { @xline = split(/\s+/,$txt1);
               $txt2='';
               for ( $i=0;$i <= $#xline;$i++)
                 { if (length($txt2)+length($xline[$i]) > ($COLS-3) )
                      { $txt2=~s/^ *//;
                        mvstr($currline,1,$txt2);
                        $currline++;
                        $txt2=$xline[$i];}
                    else { $txt2 = join(' ',$txt2,$xline[$i]);}
                 } 
                if ( length($txt2) > 1 ) {$txt2=~s/^ *//;
                                          mvstr($currline,1,$txt2);
                                          $currline++;}
              }
     }
     $row=$currline++;
     &pause(" press return to continue ");
     return;
     }
    
