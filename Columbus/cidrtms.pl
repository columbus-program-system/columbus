#!/usr/bin/env colperl
#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/

#
use lib join('/',$ENV{"COLUMBUS"},"CPAN") ;

# Perl5+Curses ONLY!
# Comment these lines for use with Perl4/curseperl
BEGIN { $Curses::OldCurses = 1; }
use Config;
use Curses;                     # PerlMenu needs "Curses"
use perlmenu;                   # Main menu package (Perl5 only)
use colib_perl;
use colib_curses_perl;

use Shell qw( date  pwd mv );
require "menuutil.pl";        # For "pause" and "pprint_nl" routines.

$VERSION="Version 0.992  - mar 2016";


$| = 1;             # Flush after every write to stdout
$debug=1; 
$exitvalue=0;
if ( $debug ) { open (DEB,">DEBUG");}

############################################
## Define which ordering to use       ######
## $ORDERING = REFTOP     (right)     ######
## $ORDERING = REFBOTTOM  (left)      ######
############################################

$ORDERING = "REFBOTTOM";

if ( ( ! grep(/REFTOP/,$ORDERING) ) && (! grep(/REFBOTTOM/,$ORDERING)))
 {&popup_ask("invalid setting of ORDERING see topline of cidrtms.perl\n");
  die();}

############################################
# global variables taken from infofl
# exitvalue ueberfluessig
#
# $ndrt  : number of DRTs 
# @nmpsy : number of orbitals per irrep
# @labels: irrep labels
# $nsym  : number of irreps
#
# @strd  : passing of ref doubly occupied orbitals
#                     excitation level, state symmetry
#                     allowed reference symmetries  per irrep
# @strd sollte besser verwaltet werden ( hash/array)
#
# @fc    : frozen core per irrep (counted from the bottom)
# @fv    : frozen virtual per irrep (counted from the top)
# @refdoc: orbitals doubly occupied in all references (counted from the bottom)
# @ciact : CI active orbitals (variably occupied in all references)
# @ext   : CI virtual (or external) orbitals
# @ciaux : CI auxiliary internal orbitals(limited occupancy in all irreps,
#                                        positioned above ciact )
# @int   : CI internal orbitals = @ciact + @refdoc + @ciaux
# $ncorel: number of correlated electrons
# $auxel : number of auxiliary electrons
#
# counting scheme per irrep for one DRT (taking over from mcscf)
#  | fc | refdoc | ciact | ciaux  | ext    | fv |
#
# counting scheme per irrep for multiple DRTs
# form one group and restrictions are applied
# through step vectors
#  | fc |  refdoc + ciact | ciaux | ext    | fv |
#        .........int ............
#-----------------------------------------------------------
#  @allroots: number of roots per drt
#-----------------------------------------------------------
# # $groups[$nogsets]{$el}   group definition set nogsets group el
# #                  $elmin minimum occupation
# #                  $elmax maximum occupation
# # $nogsets          number of group sets
#
#  contained in  @groupdefs[$drt][0] = $nogsets
#                @groupdefs[$drt][1] = \@groups
#----------------------------------------------------------------- 

  @groupdefs=();

# 
# read symmetry information from infofl
#
  &readinfofl();

#
#  build main input menu
#
 #fp
 open (IN, "<cidrtplky"); # open key stroke file for reading the data of the previous run
 our @lines=<IN>;
 chomp @lines;
 close (IN);
 $aline=0;
 close(IN);
 
 open (OUT, ">cidrtplky.new"); # open for writing the data of this run

$window=initscr();
&menu_curses_application($window);
 
  clear();
  &displaytext("              CI WAVE FUNCTION DEFINITION             \n"
             . "             =============================            \n"
             ."In the Distinct Row Table(s) (DRT) the wave functions "
             ."for the electronic states to be calculated are defined. "
             ."The CI wave function is defined in terms of single- and "
             ."double excitations with respect to a set of reference "
             ."configurations. Use one DRT for each class of states "
             ."(e.g. one DRT for each symmetry). In the multiple-DRT case "
             ."one unified DRT is constructed for the computation of "
             ."transition moments. For more information on DRTs and the "
             ."Graphical Unitary Group Approach (GUGA) see the COLUMBUS "
             ."Program Documentation and references therein.\n"
             ."Orbital classification scheme: \n"
             ."fc: frozen core (not correlated) \n"
             ."docc: reference doubly-occupied \n"
             ."active: variable occupation in the reference configurations\n"
             ."auxiliary: limited reference occupation\n"
             ."external: reference-unoccupied\n"
             ."fv: frozen virtual\n"
             ."internal = docc + active + auxiliary\n"
             ."Note: additional group restrictions available "
             ."(see COLUMBUS documentation)\n\n"); 
  clear();
  $row=1;

  SKIPDRT: while (1) {

   &menu_init(1,"CIDRT INPUT FACILITY",1,$VERSION);
 if ( -T "mcdrtin.1" && -s "mcdrtin.1" && (! -s "mcdrtin.2") ) 
  {&menu_item("Def. of CI wave function - one-DRT case (data from MCSCF)","cionem");}
   &menu_item("Def. of CI wave function - one-DRT case ","cione");
   &menu_item("Def. of CI wave function - multiple-DRT case (transition moments)","cimult");
   &menu_item("Def. of CI wave function - independent multiple-DRTs (intersystem crossings)","ciisc");
   &menu_item("Skip DRT input (old input files in the current directory)","skip");

    my ($a,$b,$c);
    $sel = &menu_display("",$a,$b,$c);
    if ( $sel eq "Exit") { last;}
    if ( $sel eq "%EMPTY%" ) { die "not enough screen lines \n";}
    if ( $sel ne "%UP%" ) { $callprog = $sel; last;}
   }

#
#  consistency checks for skip drt modus
#

  if ( $sel eq "skip" ) {  
                           $ncorel = &keyinfile("ciudgin","NCOREL");
                           if (  (! -s "cidrtin" ) && (! -s "cidrtmsin" )) 
                               { &popup_ask("perform DRT input first\n");
                                 goto SKIPDRT;}
                           if ( ( -s "cidrtin" ) && ( -s "cidrtmsin" )) 
                               { &popup_ask("found two conflicting DRT inputs\n"
                                 . "redo DRT input \n");
                                 goto SKIPDRT;}
                           if ( ( -s "cidrtin" ) && (! -s "mcdrtin" )) { $callprog = "cidrtin";}
                           if ( ( -s "cidrtin" ) && ( -s "mcdrtin" )) { $callprog = "cionem";}
                           if ( ( -s "cidrtmsin" ) ) { $callprog = "cimult";
                                                       open (CIDRTKY,"<cidrtmsin");
                                                       $in = <CIDRTKY>;
                                                       $in =~ s/\/.*$//; 
                                                       $ndrt=$in;
                                                       close CIDRTKY; } 
                           if ( ( -s "cigrdin" ) ) { $cigradient=1;} 
                                              else {$cigradient=0;}
  
  #
  # organize proper setting of ncorel by extracting it from ciudgin/ciudgin.drt*
  #
   if ( $callprog eq "cimult" || $callprog eq "cimultgrad" ) 
      { my ( $idrt,$ncorel1) ;
        $ncorel=0;
        for ( $idrt =1; $idrt <= $ndrt ; $idrt++)
         { if ( ! -s "ciudgin.drt$idrt" ) 
             { &popup_ask("cannot find ciudgin.drt$idrt "); exit};
           $ncorel1 = &keyinfile("ciudgin.drt$idrt","NCOREL");
           if ( $ncorel ) { if ( $ncorel != $ncorel1 ) 
                            {&popup_ask("inconsitent ncorel on ciudgin.drt$idrt\n");
                             exit;}}
               else       { $ncorel=$ncorel1;}
         }
      }
   else { $ncorel = &keyinfile("ciudgin","NCOREL");}
   }  # if skip 

#
#  initialize DRT input
#
  else
    {
     $ciprogram="ciudg";   
      system("stty sane");
      `rm -f cidrtin*`; 
#
#  we temprarily disable REFTOP ordering!
#  better gis 
#
#    if (&query("Ref.DOCC orbitals at the bottom (default)[0] or top [1]?",
#          "01") eq "0" ) { $ORDERING = "REFBOTTOM";}
#        else { $ORDERING = "REFTOP";}
#  $yn = &query("Do you want to compute gradients or non-adiabatic couplings?","yn");
  $yn = &getzeichen(8,"Do you want to compute gradients or non-adiabatic couplings? [y|n]",1);
  if ($yn eq "y") {$cigradient = 1;}
  else {$cigradient = 0;}

     $ORDERING="REFBOTTOM";
     if ( $sel eq "cionem" ) { $callprog = "cionem"; }
     if ( $sel eq "cione" ) { $callprog="cidrtin"; }
     if ( $sel eq "cimult" ) { $callprog="cimult"; }
     if ( $sel eq "ciisc" ) { $callprog="ciisc";}
     print DEB "$sel,$callprog,cigradient=$cigradient,ndrt=$ndrt\n"; 
     if ( $callprog ) { &$callprog($cigradient); } 
   }

  clear();
  my $temp=getnumber(12,"Choose CI program: sequential ciudg [1]; parallel ciudg[2]",1,1,2);
  if ( $temp == 1 ) {$ciprogram="ciudg";}
              else  {$ciprogram="pciudg";} 
#
# ciudgin input section for multi DRT mode
# multi-drt mode cannot run with cigradient
 if ( $callprog eq "cimult" || $callprog eq "ciisc")  
    { for ( $idrt=1; $idrt <= $ndrt ; $idrt++) 
         { print DEB "ndrt,idrt=$ndrt , $idrt\n";  
          &prodciudgin( $idrt,$cigradient,$ciprogram,\$maxmem,\$ncpu,\$nvmax,\$bandwidth,\$ncnode);}
       # allroots is evaluated in transin
       &transin();
    }

  if ( $callprog eq "cionem" || $callprog eq "cidrtin")
 {
  $cigradient = &prodciudgin(1,$cigradient,$ciprogram,\$maxmem,\$ncpu,\$nvmax,\$bandwidth,\$ncnode);
  # $cigradient contains the root to be followed
  print DEB "cionem: ncnode =",$ncnode,"maxmem,ncpu,nvmax,bandwidth=",$maxmem,$ncpu,$nvmax,$bandwidth,"\n"; 
  `mv ciudgin.drt1 ciudgin ` ;
#     if ( ! $cigradient ) 
        if (grep(/:/,$allroots[1])) { $ndrt=1;&transin();} 
  } #endif cionem cidrtin

  open TRANIN, ">tranin";
   print TRANIN " \&input\n \&end\n";
  close TRANIN;

# the size of maxbuf, maxbl3,maxbl4 must be potentially adjusted!
# adapt makpciudg.x for that purpose 
  open CISRTIN, ">cisrtin";
   print CISRTIN " \&input\n maxbl3=60000\n maxbl4=60000\n \&end\n";
  close  CISRTIN;

# the input procedure is modified as to 
# allow for additional input checks (cisrtin) 
# and to display the configuration space summary information
# it is required for pciudg and a sensible info anyway.
#      check for the required input file
   $notfound=0;
   if (-s "ciudgin*") { &pprint_nl("\n  ciudgin input file(s) missing ...",1); $notfound++;}
   if (-s "cidrtin*" && -s "cidrtmsin" ) { &pprint_nl("\n  cidrtin input file(s) missing ...",2); $notfound++;}
   if (-s "cisrtin") { &pprint_nl("\n  cisrtin input file missing ...",3); $notfound++;}
   if ($notfound >1) { &pause(" Make input first ...    press return to continue"); return; }
MEM:
   if ($ciprogram eq "pciudg")
   {
    clear();
    $outstr = "Parallel CI input parameter generation\n";
    $outstr.= "\nDo you want to:\n";
    $outstr.= " [0] Keep default ciudgin file\n";
    $outstr.= " [1] Perform customization using cidrt.x, makpciudg.x, cimkseg.x\n";
    $outstr.= " [2] Run programs in batchmode (and return to [3] afterwards)\n";
    $outstr.= "     - only available for single DRT mode\n";
    $outstr.= " [3] Call only pscript (use this if [1] or [2] was already called)\n";
    $outstr.= "   Choice: ";
    $preppar = &getnumber(8,$outstr,1,0,3);
   }
   else
   {
    $preppar = 0;
   }
   clear();
   if ($preppar == 1)
   {
   clear();
   inpcoremem();
   clear();
   if ($callprog eq "cimult") 
     { &pprint_nl(" Executing cidrtms.x, this may take several minutes, please wait...",1);
       $errno=callcidrt("cidrtms.x -m $coremem < cidrtmsin > cidrtmsls",\$total,$ndrt);
#fp: bummer is unlinked in callcidrt, checkbummer does not work here!
       #$errno= checkbummer();
       if ( $errno != 0 ) { clear();
                            &pprint_nl("cisrtms.x run executed with an error!! Try to increase memory or check the cidrtmsin inputfile\n");
                            &pprint_nl("Error code: $errno\n");
                            &pause("          Press return to continue");
                            goto MEM; }
     }
    else
     { &pprint_nl(" Executing cidrt.x, this may take several minutes, please wait...",1);
       $errno=callcidrt("cidrt.x -m $coremem < cidrtin > cidrtls",\$total,1);
#       &pause("errno: $errno");
       #$errno= checkbummer();
#fp: errno should be unequal zero if there is an error?!
#        if ( $errno == 0 ) { clear();
       if ( $errno != 0 ) { clear();
                            &pprint_nl("cidrt.x run executed with an error!! Try to increase memory or check the cidrtin inputfile\n");
                            &pprint_nl("Error code: $errno\n");
                            &pause("          Press return to continue");
                            goto MEM; }
     }
    clear();
    pprint_nl("\n ",1);
    pprint_nl(" CIDRT calculation performed successfully \n",1);
    pprint_nl("total number of configurations: $total\n ",1); 
    } # endif $drtcalc
#
# generate remaining CI related input files
#

 
if ( $cigradient )

 {open CIDENIN, ">cidenin";
  print CIDENIN " \&input\n  iden2 = 1 \n  iwrino = 1 \n "; 
  print CIDENIN " lroot = $cigradient \n \&end \n";
  close CIDENIN ; 

  if ( -s "cigrdin")
  {
    $wrcgr = &query("cigrdin exists. Do you want to overwrite it?","yn");
  }
  else {$wrcgr = "y";}
  
  if ($wrcgr eq "y") {&write_cigrdin();}
  
  open  TRANCIDENIN, ">trancidenin";
   print TRANCIDENIN " \&input\n denopt=1\n \&end\n";
  close  TRANCIDENIN;
  } 

#   now run makpciudg.x
#   this should also check for insufficient size of maxbl3,maxbl4,maxbuf
#   makpciudg.x should also produce a fake tmodells file

    if ($preppar == 1) {&prepare_ciinput($callprog,$ndrt,$ciprogram,$preppar);}
    if ($preppar == 2) {
        inpcoremem();
        open PREP, ">run_ciprep";
        print PREP "\$COLUMBUS/cidrt.x -m $coremem < cidrtin > cidrtls\n";
        print PREP "\$COLUMBUS/makpciudg.x -m $coremem\n";
        print PREP "\$COLUMBUS/cimkseg.x -m $coremem\n";
        close PREP;
        system("chmod +x run_ciprep");
        &pause("File run_ciprep created.\n Please run this file and return to [3].");
    }
    if ($preppar == 3) {&pscript($nvmax,$ncpu,$maxmem,$bandwidth,0,$ncnode);}

  close DEB;
  system("mv cidrtplky.new cidrtplky"); 
 &printbummer();
 &endwin();
 system("stty sane");
 exit($exitvalue);



 sub printbummer {     #ok
  open BUMMER,">bummer";
  print BUMMER "normal termination of cidrtms.perl\n";
  close BUMMER;
  return;
  }


 sub cionem {   # ok

 local ($cigradient);
 ( $cigradient ) = @_;
 local ($cbus,$kommando);
 $cbus = $ENV{"COLUMBUS"};
 system("stty sane");
 $kommando="$cbus/mcdrt.x -m 1700 < mcdrtin.1 > mcdrtls ";
 &pprint_nl("Executing mcdrt.x to create mcdrtfl ... ",1);
 $errno = system ("$kommando");
 print DEB "errno,kommando: $errno,$kommando\n"; 
 &clear($window);;
 &rdmcdrtin($cigradient);
 `rm -f mcdrtls mcdrtfl mcdrtky `;
 return;
 }
#
#------------------------------------------------------------------
#
 sub cimult { #ok

 local ($drt,$mult,$nel,$gensp,$cigradient);
 ( $cigradient ) = @_;

 &obpart;

# windows related variables

 $row=20; $col=0;

 $drt=0;
 $mult=0;
 $nel=0;
 $gensp=" ";

## ..................................................................
#### I did small modifications in this block to fix the MXS search input
#### MB Sep 2005
#
# $lx =-1; $ly=-1; $lz=-1;
#
# &status($drt,$mult,$nel,$gensp,$lx,$ly,$lz);
#
# $drt = &getnumber(8,"Enter number of DRTS [1-8]  ",1,1,8);
#
# &status($drt,$mult,$nel,$gensp,$lx,$ly,$lz);
#
# $spinorbit = &getzeichen(8,"Spin-Orbit CI [y|n] ", 1);
# if ( $spinorbit) 
#   { $mult = &getnumber(8,"Enter highest multiplicity (all DRTs)  ",2,1,100);
#     $lxyzir = &getvector(8,"irreducible representation of lx, ly, lz ",3,"");
#     ($lx,$ly,$lz) = split(/:/,$lxyzir);
#   }
#  else
#  { $mult = &getnumber(8,"Enter the multiplicity (all DRTs)  ",2,1,100);}
#
#
# &status($drt,$mult,$nel,$gensp,$lx,$ly,$lz);
## ..................................................................

 &status($drt,$mult,$nel,$gensp,$lx,$ly,$lz);

 $drt = &getnumber(8,"Enter number of DRTS [1-8]  ",1,1,8);

 &status($drt,$mult,$nel,$gensp,$lx,$ly,$lz);

 $spinorbit = &getzeichen(8,"Spin-Orbit CI [y|n] ", 1);

 if ( grep (/[yY]/,$spinorbit )) 
   { $mult = &getnumber(8,"Enter highest multiplicity (all DRTs)  ",2,1,100);
     $lxyzir = &getvector(8,"irreducible representation of lx, ly, lz ",3,"");
     ($lx,$ly,$lz) = split(/:/,$lxyzir);
   }
  else
  { $mult = &getnumber(8,"Enter the multiplicity (all DRTs)  ",2,1,100);}


 &status($drt,$mult,$nel,$gensp,$lx,$ly,$lz);
 
# .................................................................

 $nel = &getnumber(8,"Enter the number of electrons (all DRTs)  ",3,1,1000);

 &status($drt,$mult,$nel,$gensp,$lx,$ly,$lz);

 $gensp = &getzeichen(8,"Generalized interacting space restrictions [y|n] ",
                      1);
 &status($drt,$mult,$nel,$gensp,$lx,$ly,$lz);
  
 $ndrt = $drt; 


# obtain the input data applying to all DRTs 

 {my $stat1=1;
 while ($stat1) { &clear();
                  &obpart();
                  &status($drt,$mult,$nel,$gensp,$lx,$ly,$lz);
                  $stat1=&gendata($nel);
                  }
 }



 &specdata($drt,$nel);


   if ($cigradient)
       { &output1($drt,$mult,$nel,$nsym,$lx,$ly,$lz,$cigradient); 
         system("mv cidrtmsin cidrtmsin.cigrd"); }
   &output1($drt,$mult,$nel,$nsym,$lx,$ly,$lz,0); 
       
  return; 
  }


 
 sub status {  #ok
  local ($drt,$mult,$nel,$genspace,$lx,$ly,$lz); 
  ($drt,$mult,$nel,$genspace,$lx,$ly,$lz) = @_ ;

  &mvstr(18,0,"Input shared by ALL DRTs:");
  if ($lx) 
   { &mvstr(19,0,"#DRTs:$drt mult:$mult #el:$nel genspace:$genspace SOCI lxyz:$lx,$ly,$lz");}
  else 
   {&mvstr(19,0,"#DRTs:$drt mult:$mult #el:$nel genspace:$genspace NO SOCI");} 
  return 0;
  }



 sub obpart { #ok
 
 &mvstr(1,0,"=============================================");
 &mvstr(2,0,"====   CIDRTMS input facility              ==");
 &mvstr(3,0,"====      $VERSION         ==");
 &mvstr(4,0,"=============================================");
 return 0;
 } 



 
 sub gendata {  #ok


#       irrep1  irrep2   irrep3  irrep4  
# nmo     x       x        x        x  
# fcore   x       x        x        x
# fvirt   x       x        x        x
# intern  x       x        x        x
# ciactivex       x        x        x
# ciaux   x       x        x        x
# extern  x       x        x        x 
#
#
# @int=@ciact+@ciaux
  
  my ( $nelec ) = @_ ; 
  local ($ierr);
  my ( $ciaux, $x,$falsch,$int,$fv,$fc);


 &mvstr(7,0,"count order (bottom to top): fcore-docc-active-aux-extern-fvirt");
 &mvstr(8,0,pack("A15A6A6A6A6A6A6A6A6","irreps",@labels));
 &mvstr(9,0,pack("A15A6A6A6A6A6A6A6A6","# basis fcts ",@nmpsy));

               ############
  $fc = &getvector(14,"number of frozen core orbitals per irrep ",$nsym,"");
  @fc = split(/:/,$fc);
  &mvstr(10,0,pack("A15A6A6A6A6A6A6A6A6","frozen core",@fc));
  $ncorel=$nelec - &countrefel(join(':',@fc));
               ############
  $fv = &getvector(14,"number of frozen virtual orbitals per irrep",$nsym,"");
  @fv = split(/:/,$fv);
  &mvstr(11,0,pack("A15A6A6A6A6A6A6A6A6","frozen virt",@fv));
               ############

  do
  { $int = &getvector(14,"number of internal (=docc+active+aux)"
                        ." orbitals per irrep",$nsym,"");
    &clearline(16);
    @int = split(/:/,$int);
    if (&countorbs(join(':',@int)) < 2 )
     { &mvstr(16,0,"need at least two internal orbitals");
       $int=-1;
     }
  }
  until ( $int != -1 );

  &mvstr(12,0,pack("A15A6A6A6A6A6A6A6A6","internal",@int));
               ############
  $ciaux = &getvector(14,"number of aux. orbitals per irrep",$nsym,"");
  @ciaux=split(/:/,$ciaux);
  if ( &countorbs(join(':',@ciaux)))
  {$auxel = &getnumber(14,"Max. number of electrons in ci  aux. (ref.CSF only)",1,1,$nelec);}
 else { $auxel=0;}
  &mvstr(14,0,pack("A15A6A6A6A6A6A6A6A6","ci auxiliary ",@ciaux));

  for ( $x=0; $x < $nsym; $x++)
   { $ciact[$x]=$int[$x]-$ciaux[$x];}

  &mvstr(13,0,pack("A15A6A6A6A6A6A6A6A6","ci active ",@ciact));

# carry out some checks

 $x=0;
 $falsch="";
 $ierr=0;
 for ($x = 0; $x < $nsym ; $x++)
 { $ext[$x]=$nmpsy[$x]-$fc[$x]-$fv[$x]-$int[$x];
   if ( $ext[$x] < 0 ) { $falsch = join(" ",$falsch,$labels[$x]);$ierr++;} }
 &mvstr(15,0,pack("A15A6A6A6A6A6A6A6A6","external",@ext));

 if ( $ierr ) { &mvstr(16,0,"input errors for irrep(s) $falsch");
                &mvstr(17,0,"repeat input"); }

  &pause("press return to continue ",18);

   print DEB  "gendata: ",join(':',@fc),"\n";

  return $ierr;



}

  sub specdata { #ok

   local ($ndrt,$nelec);
   ($ndrt,$nelec)=@_;
   my ($idrt, $rfdoc, @rfdoc ,$exc ,$sym ,$x, @refsym ,$str3, $i); 

   &clear();

 &mvstr(3,0,pack("A15A6A6A6A6A6A6A6A6","irreps",@labels).
            pack("A6A6A6","exc","sym","refsym") );
 &mvstr(4,0,pack("A15A6A6A6A6A6A6A6A6","# basis fcts ",@nmpsy));
 
 &mvstr(1,0,"DRT specific input");
 &mvstr(5,0,pack("A15A6A6A6A6A6A6A6A6","ci active",@ciact));
   

 $idrt=0;
 for ($idrt = 1; $idrt <= $ndrt ; $idrt++)
 { 
                  #############

 do {
  $rfdoc = &getvector(14,"ref doubly occ orbitals per irrep for DRT #$idrt",$nsym,"");
  @rfdoc=split(/:/,$rfdoc);
  if ( ($nelec - &countrefel(join(':',@fc))-&countrefel(join(':',@rfdoc))) < 0 ) 
   { &mvstr(15,0,
      "too few electrons for so many doubly occ orbitals"); $x=-1;}
   else {$x=1;}
   }
  until ($x > -1 );

  $strd[$idrt]=pack("A13A2A6A6A6A6A6A6A6A6","ref docc DRT#",$idrt,@rfdoc);
  &mvstr(5+$idrt,0,$strd[$idrt]);

                  #############

 $exc = &getnumber(14,"excitation level (0,1,2)",1,0,2);
 &mvstr(5+$idrt,0,$strd[$idrt] . pack("A6",$exc) );
                  #############
 $sym = &getnumber(14,"state symmetry (1 .. nsym) ",1,0,$nsym);
 &mvstr(5+$idrt,0,$strd[$idrt] . pack("A6A6",$exc,$labels[$sym-1]) );
                  #############
 do {

  $x = &getvector(14,"allowed reference symmetries",1,"");
  @refsym=split(/:/,$x);
  foreach $x (@refsym) 
  { if ( $x < 1 || $x > $nsym   ) { 
    &mvstr(15,0,"you entered an invalid irrep"); $x=-1;}}
  }
 until ($x > -1) ;


 $str3=pack("A6A6A2A2A2A2A2A2A2A2",$exc,$labels[$sym-1],@refsym);
 &mvstr(5+$idrt,0,$strd[$idrt] . $str3 );
 $str3=pack("A6A6A2A2A2A2A2A2A2A2",$exc,$sym,@refsym);
 $strd[$idrt] = $strd[$idrt] . $str3 ; 
 $str3=""; 

#
# extract rfdoc for the current DRT
# grouprestr expects rfdoc for current DRT in @refdoc
#

  @refdoc = unpack("A13A2A6A6A6A6A6A6A6A6A6A6A2A2A2A2A2A2",$strd[$idrt]);
# $refel =0;
  my $groups;
  for ( $i = 0; $i < $nsym ; $i++)
   {  $refdoc[$i]=$refdoc[$i+2];
#     $refel=$refdoc[$i]*2+$refel;
      $ciact[$i] = $int[$i]-$refdoc[$i]-$ciaux[$i];}
  if ($cigradient)
   {
    for ($i=0; $i < $nsym; $i++) {$refdoc[$i]=$refdoc[$i]+$fc[$i]; }
    @refdoc_save=@refdoc; @fc_save=@fc; @fc=();
   }
     $groups = &getzeichen(17,"Apply additional group restrictions for DRT $idrt [y|n]",1);
     &clearline(17);
     if ( grep (/[yY]/,$groups) ) 
       { if ( $idrt > 1 ) 
         { $groups = &getzeichen(17,"take over group restrictions from DRT 1 [y|n]",1);
           &clearline(17);
           if ( grep (/[yY]/,$groups) ) 
           { $groupdefs[$idrt][0]=$groupdefs[1][0];
             $groupdefs[$idrt][1]=$groupdefs[1][1];}
           else
           { ( $groupdefs[$idrt][0], $groupdefs[$idrt][1]) =
                              &grouprestrictions(14,17,0,1,$nelec);}
         }
        else
          { ( $groupdefs[$idrt][0], $groupdefs[$idrt][1]) =
                              &grouprestrictions(14,17,0,1,$nelec);}
       }
      if ($cigradient){@fc=@fc_save; @refdoc=@refdoc_save;}
 }
#fix  if ($cigradient){@fc=@fc_save; @refdoc=@refdoc_save;}
 return;
 }
#
#---------------------------------------------------------------------
#
  sub output1 {  #ok

 local ($ndrt, $mult, $nel, $nsym,$lx,$ly,$lz,$cigradient);
  ( $ndrt, $mult, $nel, $nsym,$lx,$ly,$lz ,$cigradient) = @_;
  my ($str1, $i, $nf, $j , $nv, $x ,$naux, $nactdoub, $idrt, $scr1,$scr2);
  my (@scr, @dat);
   
#
#  if spinorbit CI  $lx>0, $ly>0 $lz>0 
#

  open (CIDRTKY,">cidrtmsin");
  print CIDRTKY " $ndrt / number of DRTs to be constructed \n";

  if ($lx) 
   { print CIDRTKY "Y /Spin-Orbit CI calculation? \n";
     print CIDRTKY " $mult / maximal spin multiplicity \n";
     print CIDRTKY " $lx  $ly  $lz \n";}
  else
   { print CIDRTKY " N /Spin-Orbit CI Calculation? \n";
    print CIDRTKY " $mult / input the spin multiplicity \n";}
    
  print CIDRTKY " $nel  / input the total number of electrons \n";
  print CIDRTKY " $nsym / input the number of irreps \n";
  print CIDRTKY " Y / input symmetry labels ";


  foreach $str1 ( @labels )
  {print CIDRTKY "\n $str1 ";}
  print CIDRTKY " / symmetry labels ";
  print CIDRTKY "\n ",join("  ",@nmpsy)," / orbitals per irrep \n "; 

#### now we have the following problem:
#### in the multi-drt case the number of reference doubly occupied
#### orbitals may differ between different DRTs; in the previous
#### implementation, therefore the internal orbitals were ordered 
#### by symmetry and the refdocc orbitals simply marked by 1000 in
#### the reference step vector; However, this approach generally 
#### produces different orbital orderings from the one-drt case 
#### and - since the orbital ordering affects the efficiency of gis -
#### is a bit unfortunate 
#### The following fix is now applied:

#### step 1: search for minimum joint number of refdocc orbitals among
####         all drts
###          only REFBOTTOM (default) for now 
   @jdocc=();
#  @dat : 2 .. nsym+1  refdocc(*)
   @dat = unpack("A13A2A6A6A6A6A6A6A6A6A6A6A2A2A2A2A2A2",$strd[1]);
   for ($i=2; $i<=$nsym+1; $i++) { $jdocc[$i-2]=$dat[$i];}
   for ($idrt=2; $idrt<=$ndrt; $idrt++) 
    { @dat = unpack("A13A2A6A6A6A6A6A6A6A6A6A6A2A2A2A2A2A2",$strd[$idrt]);
      for ($i=2; $i<=$nsym+1; $i++) { 
          if ($dat[$i]<$jdocc[$i-2]) {$jdocc[$i-2]=$dat[$i];}}
    }
    print DEB 'minimum joint docc vector=',join(',',@jdocc), "\n";
    print DEB " isym     fc      int        jdocc     ciaux \n";
    print DEB "---------------------------------------------\n";
    for ($i=1; $i<=$nsym; $i++) { printf  DEB " %7d  %7d  %7d    %7d    %7d \n", $i,$fc[$i-1],$int[$i-1],$jdocc[$i-1],$ciaux[$i-1]; }
    print DEB " ORDERING= $ORDERING\n";
#


######################################## REF TOP ordering #####################
if ( grep(/REFTOP/,$ORDERING))
 {

#
#  ORDER: ciaux  ciact/refdocc
 
  @scr=();
  $i=1;
   $nf=0; 
  while ( $i <= $nsym )
  {  $j=1;
     $nf+=$fc[$i-1]; 
     while ( $j <= $fc[$i-1] )
      { if ( !$cigradient ) {push @scr,$i,$j ; $nel-=2;}$j++;}
     $i++;
   }
 
   &printarray(2,\@scr," / frozen core orbitals ");
 
  @scr=(); 
  $i=1;
    $nv=0;
  while ( $i <= $nsym )
  {  $j=1;
     $nv += $fv[$i-1]; 
     while ( $j <= $fv[$i-1] )
      { $x=$nmpsy[$i-1]+1-$j;
        push @scr,$i,$x; $j++;}
     $i++;
   }

  &printarray(2,\@scr," / frozen virtual orbitals ");

  @scr=();
  $i=1;
  $naux=0;    
  while ( $i <= $nsym )
  { $j=$ciaux[$i-1];
     while ( $j > 0 )
      { $x=$fc[$i-1]+$int[$i-1]-$ciaux[$i-1]+$j;
        push @scr,$i,$x; $j--; $naux++;
        }
     $i++;
   }

   if ( $naux ) {&printarray(2,\@scr," ");}

  @scr=();
  $i=1;
  $nactdoub=0;
  while ( $i <= $nsym )
  { $j=$int[$i-1]-$ciaux[$i-1];
     while ( $j > 0 )
      {# $x=$fc[$i-1]+$ciaux[$i-1]+$j;
        $x =$fc[$i-1]+$j;
        push @scr,$i,$x; $j--; $nactdoub++;
        }
     $i++;
   }
   &printarray(2,\@scr," ");
#
# frozen core
#
  @scr=();
  if ($cigradient)
    { $i=1;
      while ( $i < $nsym )
       { $j=$fc[$i-1];
         while ( $j > 0 )
          { push @scr,$i,$j; $j--; $nintern++; }
         $i++;
        }
     }
  &printarray(2,\@scr," /internal orbitals \n  ");
}
############################## REF BOTTOM ORDERING #############################
if ( grep(/REFBOTTOM/,$ORDERING))
 {
  @scr=();
  $i=1;
   $nf=0;
  while ( $i <= $nsym )
  { $j=1;
     $nf+=$fc[$i-1];
     while ( $j <= $fc[$i-1] )
      { if ( ! $cigradient ) {push @scr,$i,$j; $nel-=2;}$j++;}
     $i++;
   }
  print DEB "fc= ",join(':',@fc),"nf=$nf\n";
  &printarray(2,\@scr," / frozen core orbitals \n  ");
#
  @scr=();
  $i=1;
    $nv=0;
  while ( $i <= $nsym )
  {  $j=1;
     $nv +=$fv[$i-1];
     while ( $j <= $fv[$i-1] )
      { $x=$nmpsy[$i-1]+1-$j;
        push @scr,$i,$x;  $j++;}
     $i++;
   }
  &printarray(2,\@scr," / frozen virtual orbitals \n  ");
#
# place the frozen core orbitals
#
  $nintern=0;
  @scr=();
  if ($cigradient)
    { $i=1;
      while ( $i <= $nsym )
       { $j=1;
         while ( $j <= $fc[$i-1] )
          { push @scr,$i,$j; $j++; $nintern++; }
         $i++;
        }
     }
  if ( $nintern ) { &printarray(2,\@scr," ");}
#
#### step 2 all doccs in jdocc
  @scr=();
  $i=1;
  $njdocc=0;
  while ( $i <= $nsym )
  { $j=1;
     while ( $j <= $jdocc[$i-1])
      { $x=$fc[$i-1]+$j;
        push @scr,$i,$x;
        $j++; $njdocc++;
        }
     $i++;
   }
  if ( $njdocc ) {&printarray(2,\@scr," ");}


  @scr=();
  $i=1;
  $nactdoub=0;
  while ( $i <= $nsym )
  { $j=1;
     while ( $j <= $int[$i-1]-$ciaux[$i-1] -$jdocc[$i-1])
      { $x=$fc[$i-1]+$jdocc[$i-1]+$j;
        push @scr,$i,$x; 
        $j++; $nactdoub++;
        }
     $i++;
   }
  if ( $nactdoub ) {&printarray(2,\@scr," ");}

  @scr=();
  $i=1;
  $naux=0;
  while ( $i <= $nsym )
  { $j=1;
     while ( $j <= $ciaux[$i-1])
      { # DOCCFIX
        # $x=$fc[$i-1]+$j+$int[$i-1]+$jdocc[$i-1]-$ciaux[$i-1];
        $x=$fc[$i-1]+$j+$int[$i-1]-$ciaux[$i-1];
        push @scr,$i,$x; 
        $j++; $naux++;
        }
     $i++;
   }
#
  &printarray(2,\@scr," / internal orbitals ");

}
##############################################################################
 
#
#   within the drt loops, data pertaining to all drts is 
#   occasionally modified causing potential errors in all DRTs
#   except for the first one 
#
     $nactdoub_save=$nactdoub;


   $idrt=1;
   while ( $idrt <= $ndrt ) 
    {  @dat = unpack("A13A2A6A6A6A6A6A6A6A6A6A6A2A2A2A2A2A2",$strd[$idrt]);
#  @dat : 2 .. nsym+1  refdocc(*)
#    $help=join(":",@dat); print CIDRTKY "dat:$help\n"; 
      for ($i=1; $i<=$nsym; $i++) { $dat[$i+1]=$dat[$i+1]-$jdocc[$i-1];}

     print CIDRTKY "$dat[11] / input the molecular symmetry \n";
# DOCCFIX
#    print CIDRTKY " 0 /input the number of ref-csf doubly occupied orbitals\n";
     if ($cigradient) 
      { print CIDRTKY " ", $njdocc+$nf, " /input the number of ref-csf doubly occupied orbitals\n"; }
     else
      { print CIDRTKY " $njdocc /input the number of ref-csf doubly occupied orbitals\n"; }

     $nactdoub=$nactdoub_save;
###########################REFBOTTOM ORDERING ################################
  if ( grep(/REFBOTTOM/,$ORDERING))
   {  
# DOCCFIX
#    if ($cigradient) {$nactdoub=$nactdoub+$nf+$njdocc;}
#       else {$nactdoub=$nactdoub+$njdocc}
     if ($cigradient) {$nactdoub=$nactdoub;}
        else {$nactdoub=$nactdoub}
#    $scr2 = $nel-$auxel;
     if ($cigradient) { $scr2=$nel-$auxel-$njdocc*2-$nf*2}
      else  {$scr2 = $nel-$auxel-$njdocc*2; }
     $scr2 = " " . $scr2 . " ";
     @scr = ( (0) x ($nactdoub-1) , ($scr2) x ($naux+1));
     &printarray(1,\@scr," / occminr ");
     if ($cigradient) { @scr = (( $nel-($njdocc+$nf)*2 ) x ($nactdoub+$naux)); }
                else  { @scr = (( $nel-($njdocc)*2 ) x ($nactdoub+$naux)); }
     &printarray(1,\@scr," / occmaxr ");
   }
########################### REFTOP ordering ##########################
  if ( grep(/REFTOP/,$ORDERING))
   {  
     if ($cigradient) { $nactdoub=$nactdoub+$nf;}
     @scr = (( 0) x ($naux+1),  ($auxel) x ($nactdoub-1));
     &printarray(1,\@scr," / occminr ");

     @scr = ( ($auxel) x $naux , ( $nel) x $nactdoub);
     &printarray(1,\@scr," / occmaxr ");
   }

#######################################################################
     $nintern=$nactdoub+$naux;
     @scr =  ( (0) x $nintern); 
     &printarray(1,\@scr," / bminr ");
     @scr = ( ($nel-($njdocc)*2) x ($nintern)); 
     &printarray(1,\@scr," / bmaxr ");

# DOCCFIX  reference step vector  
#    if ($cigradient) 
#        { @locstr = (("1000") x ($njdocc+$nf), ( "1111" ) x ($nactdoub+$naux-$nf-$njdocc)); }
#    else {@locstr = (("1000") x ($njdocc), ( "1111" ) x ($nactdoub+$naux-$njdocc)); }
     if ($cigradient) 
         { @locstr = (( "1111" ) x ($nactdoub+$naux)); }
     else {@locstr = (( "1111" ) x ($nactdoub+$naux)); }
     @{$mustr[$idrt]} =  ( "0" ) x $nintern;

     for ( $i=0; $i < $nsym; $i++ ) { $ciact[$i]=$int[$i]-$ciaux[$i];}
########################### REFTOP ordering ##########################

if ( grep(/REFTOP/,$ORDERING))
{          
  $i=1;
  my $ioff=$ciact[0]+$jdocc[0];
  while ( $i <= $nsym )
  { $str1="";
     $j=1;
     while ( $j <= $dat[$i+1] )
      { $locstr[$ioff+$naux-$j]="1000";
# Note: reference vector occupation cannot be used with cigradient (md)
        if (!$cigradient){$mustr[$idrt][$ioff+$naux-$j]= "2";} $j++;}
     $ioff=$ciact[$i]+$ioff+$jdocc[$i];
     $i++;
   }
    if ($cigradient)
     {
      $i=1;
       while ( $i <= $nsym )
        {
          for ($j=$nactdoub-$nf+1; $j<=$nactdoub; $j++)
          {$locstr[$j-1]="1000"; }
         $i++;
        }
     }
}
########################### REFBOTTOM ordering ##########################
if ( grep(/REFBOTTOM/,$ORDERING))
 {
  $i=1;
  my $ioff=0;
  if ($cigradient){$ioff=$nf;}
  $ioff=$ioff;
  while ( $i <= $nsym )
  { $str1="";
     $j=1;
     while ( $j <= $dat[$i+1] )
      { $locstr[$ioff+$jdocc[$i-1]+$j-1]="1000"; 
# Note: reference vector occupation cannot be used with cigradient (md)
        if (!$cigradient) {$mustr[$idrt][$ioff+$j-1] = "2";} $j++;}
     $ioff=$ciact[$i-1]+$ioff;
     $i++;
   }
#   if ($cigradient) 
#    {
#     $i=1;
#      while ( $i <= $nsym )
#       {
#         for ($j=1; $j<=$nf; $j++)
#         {$locstr[$j-1]="1000"; } 
#        $i++;
#       }
#    }
}
#######################################################################

     &printarray(1,\@locstr," /step masksr");

     print CIDRTKY " $dat[10] / input the maximum excitation level \n";

########################### REFTOP ordering ##########################

if ( grep(/REFTOP/,$ORDERING))
   {
     if ( $auxel > $dat[10] ) 
      { $scr2= $auxel-$dat[10] ;}
     else
      { $scr2= 0;}
      if ($naux) { @scr= ( (0)  x ($naux - 1), ($scr2) x ($nactdoub+1));}
      else { @scr= ( ($scr2) x ($nactdoub));}
     &printarray(1,\@scr,"/ occmin ");

     $scr2 =  $auxel+$dat[10]  ;
     @scr= (($scr2) x $naux, ($nel) x $nactdoub);
     &printarray(1,\@scr,"/ occmax ");

     my (@scr, @dat);
     @scr =( (0) x ($nintern)); 
     &printarray(1,\@scr,"/ bmin ");
     @scr =( ( $nel) x $nintern); 
     &printarray(1,\@scr," / bmax ");
     @locstr = ( "1111" ) x $nintern;
   }


########################### REFBOTTOM ordering ##########################
if ( grep(/REFBOTTOM/,$ORDERING))
   { 
     $scr2 = $nel-$auxel-$dat[10] ; 
# DOCCFIX 
     if ($cigradient) { @scr = ((0) x ($nactdoub-1+$njdocc+$nf),( $scr2) x ($naux+1)); }
               else   { @scr = ((0) x ($nactdoub-1+$njdocc),( $scr2) x ($naux+1)); }
     &printarray(1,\@scr,"/ occmin "); 
     if ($cigradient) { @scr= (($nel) x ($nactdoub+$naux+$njdocc+$nf));
                        @{$mustr[$idrt]} =  ( "0" ) x ($nactdoub+$naux+$njdocc+$nf);   
                        @{$svdisp[$idrt]} =  ( "4" ) x ($nactdoub+$naux+$njdocc+$nf);  }
               else   { @scr= (($nel) x ($nactdoub+$naux+$njdocc)); 
                        @{$mustr[$idrt]} =  ( "0" ) x ($nactdoub+$naux+$njdocc);   
                        @{$svdisp[$idrt]} =  ( "4" ) x ($nactdoub+$naux+$njdocc);  }
     &printarray(1,\@scr,"/ occmax ");
     if ($cigradient) { @scr= ((0) x ($nactdoub+$naux+$njdocc+$nf));}
               else   { @scr= ((0) x ($nactdoub+$naux+$njdocc));} 
     &printarray(1,\@scr,"/ bmin "); 
# THIS IS WRONG!
#    if ($cigradient) { @scr = ((3) x ($nactdoub-1+$njdocc+$nf),( $nel ) x ($naux+1)); }
#              else   { @scr = ((3) x ($nactdoub-1+$njdocc),( $nel ) x ($naux+1)); }
     if ($cigradient) { @scr = (($nel) x ($nactdoub-1+$njdocc+$nf),( $nel ) x ($naux+1)); }
               else   { @scr = (($nel) x ($nactdoub-1+$njdocc),( $nel ) x ($naux+1)); }
     &printarray(1,\@scr,"/ bmax ");
     if ($cigradient) {@locstr = ( "1111" ) x ($nactdoub+$naux+$njdocc+$nf); }
              else    {@locstr = ( "1111" ) x ($nactdoub+$naux+$njdocc); }
   }
##########################################################################

     if ($cigradient)
      {
       $i=1;
        while ( $i <= $nsym )
         {
           if ( grep(/REFTOP/,$ORDERING))
            {
             for ($j=$nactdoub-$nf+1; $j<=$nactdoub; $j++)
             {$locstr[$j-1]="1000"; }
            }
           if ( grep(/REFBOTTOM/,$ORDERING))
            {
             for ($j=1; $j<=$nf; $j++)
             {$locstr[$j-1]="1000"; }
            }
          $i++;
         }
       }
 
     &printarray(1,\@locstr, "/ step masks");

     $idrt++;}
     if ($dat[10] == 2 )
     {print CIDRTKY " $gensp / impose generalized space restrictions \n"; }
     else
     {print CIDRTKY " N / impose generalized space restrictions \n"; }
     print CIDRTKY " -1 0 0 /vertices to be removed  \n";
     print CIDRTKY "  -1 0 0 0 /arcs to be removed  \n";
     print CIDRTKY "  -1  0 / DRT levels to be printed out \n "; 


    $idrt=1;
    while ( $idrt <= $ndrt ) 
    {  @dat = unpack("A13A2A6A6A6A6A6A6A6A6A6A6A2A2A2A2A2A2",$strd[$idrt]);

     $i=12;
     $j=@dat;
     while ( $i <= $j ) { if ( $dat[$i]) {print CIDRTKY "  $dat[$i]";} $i++;}
     print CIDRTKY " / allowed reference symmetries \n"; 
     print CIDRTKY " N /keep all of the z-walks as references? \n";
     print CIDRTKY " Y /generate walks while applying reference drt restrictions? \n" ;

     if ( $groupdefs[$idrt][0] )
      { print CIDRTKY " Y / impose additional orbital-group occupation restrictions?\n";
        print CIDRTKY "$groupdefs[$idrt][0] / number of orbital group sets \n";
        for ( $j=1; $j <= $groupdefs[$idrt][0]; $j++ )
         { my @group = @{$groupdefs[$idrt][1]};
           my @orbgroups= keys %{$group[$j]};
           printf CIDRTKY " %3d  / number of orbital groups in set $j \n",
                                           int($#orbgroups+1)/3;
           for  $el (@orbgroups)
            { if (grep(/min/,$el) || grep (/max/,$el)) {next;}
              $elmin =join('',$el,"min");
              $elmax =join('',$el,"max");
               print DEB "elmin,elmax=$elmin,$elmax, el=$el\n";
              printf CIDRTKY " %3d %3d %3d / #orb, minocc,maxocc \n",
    $#{$group[$j]{$el}}+1, $group[$j]{$elmin}, $group[$j]{$elmax} ;
              $glevel = join (' ',@{$group[$j]{$el}});
              print CIDRTKY " $glevel / glevel \n";
            }
         }
      }
     else
      {print CIDRTKY " N /impose additional orbital-group occupation restrictions? \n ";}

     print CIDRTKY " N /apply primary reference occupation restrictions? \n" ;
     print CIDRTKY " N /manually select individual walks? \n "; 
     print CIDRTKY " -1 "; 
#    @scr = ( (4) x ($nintern-1));
     &printarray ( 1, \@{$svdisp[$idrt]}, " /step vector disposition pairs");
     print CIDRTKY "   0 /input reference walk number (0 to end)\n";
     &printarray ( 1, \@{$mustr[$idrt]}, " /reference occupations vector");
    
#    print CIDRTKY  "  ",join(' ',@{$mustr[$idrt]}),
#                   " / reference occupations vector \n";  
     $idrt++;     
   }

    $idrt=1;
    while ( $idrt <= $ndrt ) 
    { print CIDRTKY "  0 /input mrsdci walk number (0 to end) \n";
     print CIDRTKY " cidrt_title DRT#$idrt\n";
     $idrt++;     
   }
  close CIDRTKY;
  return 0; 
  }
#
########################################################################
#
  sub prodciudgin { #ok
  
   my ($idrt,$cigradient,$ciprogram,$maxmem,$ncpu,$nvmax,$bandwidth,$ncnode);
   ($idrt,$cigradient,$ciprogram,$maxmem,$ncpu,$nvmax,$bandwidth,$ncnode) = @_;
   local($nrefiter,$nciiter,$citol,$reftol);
   local ($subspaceoffset);
    my ($i, $j);

@input_data = ();	# Place to put data entered on screen
@defaults = ();		# Default data
@protect = ();		# Protected markers
@required = ();		# Required field markers
$bell = "\007";		# Ascii bell character
$row = $col = 0;	# Storage for row/col used by menuutil.pl

#
# Activate left and right markers, both in standout rendition.
#
&menu_template_prefs("*"," ",1,"*"," ",1);

 if ($ciprogram eq 'ciudg' ) 
 {
&menu_load_template_array(split("\n", <<'END_OF_TEMPLATE'));



   Type of calculation:               CI  [_]  AQCC [_]  AQCC-LRT [_]
   LRT shift:                         LRTSHIFT [_________________]              
   State(s) to be optimized           NROOT [__] ROOT TO FOLLOW [_] 
   Reference space diagonalization    INCORE[_] NITER [___]
                          RTOL  [_____________________________________________]
   Bk-procedure:                      NITER [___] MINSUB [__] MAXSUB [__]
                          RTOL  [_____________________________________________]
   CI/AQCC procedure:                 NITER [___] MINSUB [__] MAXSUB [__]
                          RTOL  [_____________________________________________]
   
                             FINISHED [_]

   Editing: left/right, "delete"; Switching fields: "Tab", up/down
   Help is available through selecting a field and pressing "return".
   Indicate completion by selecting "Finished" and pressing "Return".

END_OF_TEMPLATE
}
 else 
{ 
&menu_load_template_array(split("\n", <<'END_OF_PTEMPLATE'));

    
    
   Type of calculation:               CI  [_]  AQCC [_]  AQCC-LRT [_]
   LRT shift:                         LRTSHIFT [_________________]
   State(s) to be optimized           NROOT [__] ROOT TO FOLLOW [_]
   Reference space diagonalization    INCORE[_] NITER [___]
                          RTOL  [_____________________________________________]
   Bk-procedure:                      NITER [___] MINSUB [__] MAXSUB [__]
                          RTOL  [_____________________________________________]
   CI/AQCC procedure:                 NITER [___] MINSUB [__] MAXSUB [__]
                          RTOL  [_____________________________________________]
   
   parallel options     NCORES                                            [___]     
                        MAX. MEMORY PER CORE    (MB)                   [______]
                        EFF. AVGE BANDWIDTH PER CORE (MB/(s*ncore))    [______]
                        NUMBER OF PROCESSORS PER NODE                      [__]
                                
   
                             FINISHED [_]

   Editing: left/right, "delete"; Switching fields: "Tab", up/down
   Help is available through selecting a field and pressing "return".
   Indicate completion by selecting "Finished" and pressing "Return".
    
END_OF_PTEMPLATE
}

if ( $idrt ) {
     &menu_overlay_template(0,28,"CIUDGIN INPUT MENU DRT# $idrt",1,1); }

#
# Set defaults for all records the same in this example.
# For record updating you would set the defaults to the existing values
# from an old record.


$subspaceoffset= 5;
#
$defaults[0] = "Y";			# CI              
$defaults[1] = "N";			# AQCC            
$defaults[2] = "N";  		# lrt   
$defaults[3] = "0";             # lrt shift
$defaults[4 ] = "1";        #  nroot 
$defaults[5] = "0";     	# froot 
$defaults[6] = "Y";     	# incore
$defaults[7] = " ";         # rf iter
$defaults[8] = " ";         # rf rtol
$defaults[9] = "1";     	# bk iter
$defaults[10] = "1";     	# bk minsub
$defaults[11] = $subspaceoffset+$defaults[4];  # bk maxsub
$defaults[13] = "30";     	# ci iter
$defaults[14] = "1";     	# ci minsub
$defaults[15] = $subspaceoffset+$defaults[4];     	# ci maxsub
if ($ciprogram eq 'pciudg') { $defaults[17]=8;  # ncpu
                              $defaults[18]=3000; # 3GB memory per cpu
                              $defaults[19]=50; # MB/s band width 
                              $defaults[20]="4"; # assume four-processor SMP nodes
                              $defaults[21]="X"; # pciudg  
                            }
if ( $cigradient )
{ $citol= "1e-4"; 
  $reftol= "1e-4"; 
  $defaults[12] = "1e-4";     # bk rtol
  $defaults[16] = $reftol;     # ci rtol
  $nrefiter=30;
  $nciiter=30;
  $defaults[13]=$nciiter;
}
 else
{ $citol= "1e-3"; 
  $reftol= "1e-3"; 
  $defaults[12] = "1e-3";     # bk rtol
  $defaults[16] = $reftol;     # ci rtol 
  $nrefiter=20;
  $nciiter=20;
  $defaults[13]=$nciiter;
}  
#
# Set protected fields for all records in this example.
# This lets us supply a record number as a default in the first field but
# not allow the user to change it.
#
if ($ciprogram eq 'ciudg') { @protect=(0) x 18; }
                   else    { @protect=(0) x 21; }
# cigradient works with froot/fmode
#if ( $cigradient ) {$protect[2]=1; $protect[3]=1;}
#
# Set required fields for records in this example.
# Note that the offset value is "2" to prevent overlaying the "["
# on the template.
#
if ($ciprogram eq 'ciudg') { @required=(0) x 18; }
                   else    { @required=(0) x 21; }

# IMPORTANT: Note the use of pointers to arrays here
  &menu_display_template(*input_data,*defaults,*protect,"ciudg_help",
			 *required);

  $allroots[$idrt]="-1";
  for ( $i = 0 ; $i < $#input_data ; $i++ )
  { $input_data[$i] =~ s/ //g;}

  $$maxmem=$input_data[18];
  $$ncpu=$input_data[17];
  $$bandwidth=$input_data[19];
  $$ncnode = $input_data[20];

  print DEB "prodciudgin: input_data(*)=",join(':',@input_data),"\n";
  print DEB "prodciudgin: ncnode=",$$ncnode,"\n"; 

  if ( $idrt) {open (CIUDGIN,">ciudgin.drt$idrt");}
      else    {open (CIUDGIN,">ciudgin");}

  print CIUDGIN " \&input \n";

# nvbkmn,nvcimn >= nroot
# nvbkmx,nvicmx >= nroot+4
  

# CI
  if ( grep /[Yy]/,$input_data[0] ) 
                { print CIUDGIN " NTYPE = 0, \n GSET = 0,\n ";
		  print CIUDGIN " DAVCOR =10,\n NCOREL = $ncorel \n"}
# AQCC

  if ( grep /[Yy]/,$input_data[1] ) 
                { print CIUDGIN " NTYPE = 3 \n GSET = 3\n "; 
                  print CIUDGIN " NCOREL = $ncorel \n";
                }
# LRT
  if ( grep /[Yy]/,$input_data[2] ) 
                { print CIUDGIN " NTYPE = 3 \n GSET = 3\n "; 
                  print CIUDGIN " LRTSHIFT = $input_data[3]\n";
                  print CIUDGIN " NCOREL = $ncorel \n";
                }
# FROOT

  if ( $input_data[5]) 
                { print CIUDGIN " FROOT = $input_data[5]\n";  
                  $allroots[$idrt]="$input_data[5]"; }
          else  { print CIUDGIN " NROOT = $input_data[4]\n";} 

# REFSPACE

  if  ( grep /[Yy]/,$input_data[6] ) {print CIUDGIN " IVMODE = 3 \n";}
                          else       {print CIUDGIN " IVMODE = 8 \n";
                                  print CIUDGIN " NRFITR = $input_data[7]\n";}

  print CIUDGIN " NBKITR = $input_data[9]\n";
  print CIUDGIN " NVBKMN = $input_data[10]\n";
  print CIUDGIN " RTOLBK = $input_data[12]\n";
 
  print CIUDGIN " NITER = $input_data[13]\n";
  print CIUDGIN " NVCIMN = $input_data[14]\n";
  print CIUDGIN " RTOLCI = $input_data[16]\n";
if ($ciprogram eq 'ciudg') { print CIUDGIN " NVCIMX = $input_data[15]\n";
                             print CIUDGIN " NVRFMX = $input_data[15]\n";
                             print CIUDGIN " NVBKMX = $input_data[11]\n"; }
      else                 {print CIUDGIN " NVCIMX = $input_data[15]\n";
                            print CIUDGIN " NVRFMX = $input_data[15]\n";
                            print CIUDGIN " NVBKMX = $input_data[15]\n"; }
  $$nvmax=$input_data[15];
# DENSITY
# if ( grep /[Yy]/,$input_data[17] ) 
      { print CIUDGIN " IDEN  = 1 \n";}

  if ( $allroots[$idrt] == -1 ) { $allroots[$idrt] = "1"; 
                                  for ($j =2; $j <= $input_data[4] ; $j++)
                                  { $allroots[$idrt] = $allroots[$idrt] . ":$j";}
                                }
  print CIUDGIN " CSFPRN = 10, \n";
  if ($ciprogram eq "pciudg") {
# parallel default keywords
  print CIUDGIN " FINALV = 0\n";  # write converged CI vectors to civfl
  print CIUDGIN " FINALW =-1\n";  # don't dump sigma vectors to file    
  $ondisk=0;
#obsolete
# print CIUDGIN " FILELOC = 2,2,2,2,2,2,2,2,2,2 \n";
  print CIUDGIN " CDG4EX =1, C2EX0EX=0,C3EX1EX=0 \n"; # almost obsolete options
# for MR-CIS the reasonable defaults are different.
#   note that this part does not work if the user chooses "Skip DRT input"
  if ($exc eq 1)
  {
    print CIUDGIN " NSEG0X = 4,4,1,1, \n";    # segments for 0ex case
    print CIUDGIN " NSEG1X = 3,3,1,1, \n";    # segments for 1ex case
    print CIUDGIN " NSEG2X = 3,3,1,1, \n";    # segments for 2ex case
    print CIUDGIN " MAXSEG = 15 \n"; 
  }
  else
  {
    print CIUDGIN " NSEG0X = 2,2,1,1, \n";    # segments for 0ex case
    print CIUDGIN " NSEG1X = 2,2,1,1, \n";    # segments for 1ex case
    print CIUDGIN " NSEG2X = 1,1,2,2, \n";    # segments for 2ex case
    print CIUDGIN " NSEGWX = 1,1,2,2, \n";    # segments for 2ex-WX case
    print CIUDGIN " NSEG3X = 1,1,2,2, \n";    # segments for 3ex case
    print CIUDGIN " NSEG4X = 1,1,2,2, \n";    # segments for 4ex case 
    print CIUDGIN " MAXSEG = 8 \n"; 
  }
 }

  print CIUDGIN "\/&end\n";
  close CIUDGIN;

&clear_screen();
&refresh();
#
  print DEB "prodciudgin: ncnode (end)=",$$ncnode,"\n"; 
#  
   if ( $cigradient && grep(/[Yy]/, $input_data[2]) ) { return $input_data[5]; }
   else
    { if ( $cigradient ) { return $input_data[4];}
     else { return 0 ; }
   }
}

#======================================================================
sub ciudg_help { #ok
  local($direction,$last_index,$next_index,$still_required) = @_;

# Return now if they are skipping between fields
# include all error-checking of input
  if ($direction) { #  lrtshift <= 0
                    if ( $last_index == 3  && $input_data[3] > 0 )
                        {&menu_overlay_template($LINES-3,3,"lrtshift<= 0",0,0);
                         return($last_index);}
                    if ( $last_index == 3  && $input_data[3] < 0 && (!$input_data[2]))
                        {&menu_overlay_template($LINES-3,3,"lrtshift for LRT calculations only",0,0);
                         return($last_index);}
                    if ( $last_index == 0 && ! grep /[YyNn]/, $input_data[0])
                        { &menu_overlay_template($LINES-3,3,"CI= y|n",0,0);
                         return($last_index);}
                    if ( $last_index == 1 && ! grep /[YyNn]/, $input_data[1])
                        { &menu_overlay_template($LINES-3,3,"AQCC= y|n",0,0);
                         return($last_index);}
                    if ( $last_index == 2 && ! grep /[YyNn]/, $input_data[2])
                        { &menu_overlay_template($LINES-3,3,"LRT= y|n",0,0);
                         return($last_index);}

                    if ( $last_index == 0 && grep(/[Yy]/, $input_data[0]) && (
                             grep(/[Yy]/, $input_data[1]) || grep(/[Yy]/,$input_data[2])))
                        { &menu_overlay_template($LINES-3,3,"choose either AQCC = y or CI = y or LRT = y ",0,0);
                         $input_data[0]="N";
                         $input_data[1]="N";
                         $input_data[2]="N";
                         display_template_internal(1); 
                         return(0);}

                    if ( $last_index == 1 && grep(/[Yy]/, $input_data[1]) && (
                             grep(/[Yy]/, $input_data[0]) || grep(/[Yy]/,$input_data[2])))
                        { &menu_overlay_template($LINES-3,3,"choose either AQCC = y or CI = y or LRT = y $input_data[0],$input_data[1],$input_data[2]",0,0);
                         $input_data[0]="N";
                         $input_data[1]="N";
                         $input_data[2]="N";
                         display_template_internal(1); 
                         return(1);}

                    if ( $last_index == 2 && grep(/[Yy]/, $input_data[2]) && (
                             grep(/[Yy]/, $input_data[0]) || grep(/[Yy]/,$input_data[1])))
                        { &menu_overlay_template($LINES-3,3,"choose either AQCC = y or CI = y or LRT = y ",0,0);
                         $input_data[0]="N";
                         $input_data[1]="N";
                         $input_data[2]="N";
                         display_template_internal(1); 
                         return(2);}

                    if ( $last_index == 2 && (! 
                      (grep(/[Yy]/, $input_data[0]) || grep(/[Yy]/, $input_data[1]) || grep(/[Yy]/, $input_data[2])))) 
                        { &menu_overlay_template($LINES-3,3,"choose either AQCC = y or CI = y or LRT = y ",0,0);
                       return (0);}

                    if ( $last_index == 3 &&  $input_data[3] > 0 )
                        {&menu_overlay_template($LINES-3,3,"LRTSHIFT <= 0",0,0);
                         return($last_index);}

                    if ( $last_index == 3 &&  $input_data[3] < 0 && grep(/[Nn]/, $input_data[2])   )
                        {&menu_overlay_template($LINES-3,3,"LRTSHIFT only for LRT calculation",0,0);
                         $input_data[3] = 0;
                         return($last_index);}
#
                    if ( $last_index == 4 && $input_data[4] )
                        {$input_data[13]=$nrefiter*$input_data[4]; 
			 $input_data[10]=$input_data[4]; 
			 $input_data[11]=$input_data[4]+$subspaceoffset; 
			 $input_data[14]=$input_data[4]+2; 
			 $input_data[15]=$input_data[4]+$subspaceoffset; 
                         $input_data[8]="";
                         $input_data[12]="";
                         $input_data[16]="";
                           for ( $i=1; $i <= $input_data[4]; $i++) 
                           {
                            $input_data[8]=$input_data[8] ."$citol".","; 
                            $input_data[12]=$input_data[12] ."$citol".","; 
                            $input_data[16]=$input_data[16] . "$reftol".","; 
                           } 
                         display_template_internal(1); 
                        }
#
                    if ( $last_index == 4 &&  $input_data[5]>0) 
                        {
                         $input_data[4]=$input_data[5];
                         $input_data[13]=$nrefiter*$input_data[4];
                         $input_data[8]="";
                         $input_data[12]="";
                         $input_data[16]="";
                           for ( $i=1; $i <= $input_data[4]; $i++)
                           {
                            $input_data[8]=$input_data[8] ."$citol".",";
                            $input_data[12]=$input_data[12] ."$citol".",";
                            $input_data[16]=$input_data[16] . "$reftol".",";
                           }                                 
			 display_template_internal(1); 
                        } 
#
                    if ( $last_index == 4 &&  grep(/[Nn]/,$input_data[6]) )
                        {$input_data[7]=$nrefiter*$input_data[4];
                         $input_data[13]=$nciiter*$input_data[4];
			 display_template_internal(1); 
                         return($next_index);
                        }

                    if ( $last_index == 5 && $input_data[5] )
                        {$input_data[4]=$input_data[5];
                         $input_data[13]=$nciiter*$input_data[4];
			 $input_data[11]=$input_data[4]+$subspaceoffset; 
			 $input_data[14]=$input_data[4]+2; 
			 $input_data[15]=$input_data[4]+$subspaceoffset; 
                         $input_data[8]="";
                         $input_data[12]="";
                         $input_data[16]="";
                         for ( $i=1; $i <= $input_data[5]; $i++)
                         {$input_data[8]=$input_data[8] . $reftol.",";   
                          $input_data[12]=$input_data[12] . $citol.","; 
                          $input_data[16]=$input_data[16] . $citol.","; }
                         display_template_internal(1);
                         return($next_index);}


                    if ( $last_index == 6 &&  ! grep(/[YyNn]/,$input_data[6]) )
                        {&menu_overlay_template($LINES-3,3,"incore= y|n",0,0);
                         return($last_index);}

                    if ( $last_index == 6 &&  grep(/[Nn]/,$input_data[6]) )
                        {$input_data[7]=$nrefiter*$input_data[4];
                         $input_data[13]=$nrefiter*$input_data[4];
			 display_template_internal(1); 
                         return($next_index);
                        }

                    if ( $last_index == 7 &&  grep(/[Nn]/,$input_data[6]) && $input_data[7] < 1 )
                        {&menu_overlay_template($LINES-3,3,"rfitr > 0",0,0);
                         return($last_index);}



                    if ( $last_index == 10 &&  $input_data[10] < 1 )
                        {&menu_overlay_template($LINES-3,3,"minsub>= 1",0,0);
                         return($last_index);}

                    if ( $last_index == 11 &&  $input_data[11] < $input_data[10] )
                        {&menu_overlay_template($LINES-3,3,"maxsub> minsub",0,0);
                         return($last_index);}


                    if ( $last_index == 14 &&  $input_data[14] < 1 )
                        {&menu_overlay_template($LINES-3,3,"minsub>= 1",0,0);
                         return($last_index);}
                         
                    if ( $last_index == 15 &&  $input_data[15] < $input_data[14] )
                        {&menu_overlay_template($LINES-3,3,"maxsub> minsub",0,0);
                         return($last_index-1);}

                    if ( $last_index == 4 &&  $input_data[4] < 0 && $input_data[3] == 0 )
                        {&menu_overlay_template($LINES-3,3,"nroot >= 0",0,0);
                         return($last_index-1);}

                    if ( "$input_data[21]" eq "pciudg") { 
                         
                    if ( $last_index == 17  && $input_data[17]<2  )
                        {&menu_overlay_template($LINES-3,3,"ncores > 1",0,0);
                         return($last_index);}
                         
                    if ( $last_index == 18  && $input_data[18]<20  )
                        {&menu_overlay_template($LINES-3,3,"maxmem per core > 20 ",0,0);
                         return($last_index);}

                    if ( $last_index == 19  && $input_data[19]<1  )
                        {&menu_overlay_template($LINES-3,3,"effective band width per core > 1 ",0,0);
                         return($last_index);}

                    if ( $last_index == 20  && $input_data[20]<1  )
                        {&menu_overlay_template($LINES-3,3,"number of cores per node > 1 ",0,0);
                         return($last_index);}
                                              }

                     &menu_overlay_template($LINES-3,3,"                                                         ",0,0);
                       return($next_index); 
                  }

# User says they are done (they pressed "Return").
  &menu_overlay_clear(); # Clear any old overlays
  if ( $last_index < 17 ) 
         { local ( @helpstr, $cnt,$x ) ;
          @helpstr= helpciudg($last_index);
          $cnt=5;
          foreach $x ( @helpstr ) 
          {&menu_overlay_template($LINES-$cnt,3, $x,0,0);
           $cnt--;}
              return($last_index);}
       else
       {return(-1);}
 

# Put out message if there are still required fields.
  if ($still_required) {
    &menu_overlay_template($LINES-5,10,
	"Fields marked with a \"*\" are STILL required.",1);
    return(-1);		# Still need required field(s) - auto-position
  }

# Let them be done.
  return(-1);
}




#========================================================================


 sub transin {  #ok

  my ( $idrt, @braroot,$bra,$jdrt,@ketroot,$ket,$temp, $sel,$found1);
  my ( $drt1,$state1,$drt2,$state2,$i);

 while (1) {  # always true
 &menu_init(1, "COLUMBUS INPUT FACILITY ",0,
       ,"\n-symmetric and antisymmetric transition moment selections \n
         (select one or more transitions)",
                $VERSION,"");
 for ( $idrt=1; $idrt <= $ndrt ; $idrt++ )
   { @braroot = split /:/, $allroots[$idrt];
      for ($bra=1; $bra <= $#braroot+1; $bra++)  
       { for ( $jdrt=$idrt; $jdrt <= $ndrt ; $jdrt++ )
         { @ketroot = split /:/, $allroots[$jdrt];
            if ( $idrt == $jdrt ) 
              { for ($ket = $bra; $ket <= $#ketroot+1; $ket++)  
                 {$temp = join ( "  ", ( $idrt, $bra, $jdrt, $ket ));
                  &menu_item("bra: DRT# $idrt state# $bra ket: DRT# $jdrt state# $ket", $temp); }}
            else 
              { for ($ket = 1; $ket <= $#ketroot+1; $ket++)  
                 {$temp = join ( "  ", ( $idrt, $bra, $jdrt, $ket ));
                  &menu_item("bra: DRT# $idrt state# $bra ket: DRT# $jdrt state# $ket", $temp); }}
          
         }
       }
    }
     $sel = &menu_display_mult("");
     if ( $sel eq "%NONE%" ) { return 0;}
      else
      { if ( -T "transmomin" ) { open TRN, "<transmomin";
                                 $/="";
                                 $found1=<TRN>;
                                 $found1=~s/\n\n/\n/g;
                                 $found1 =~ s/CI.*MCSCF/MCSCF/sg;
                                 if ( grep /CI/,$found1 ) { $found1 =~ s/CI.*$//sg ;}
                                 close TRN; }

        @selec=split(/[,]/,$sel);
        open TRN, ">transmomin";
        print TRN  "$found1"; 
        print TRN  "CI\n";
        $temp = join ( "\n",@selec);
        print TRN "$temp\n";
        close TRN;
        last; 
      }

    } # end while
#
# modify hermitin as needed
#
      open(INFOFL,"<intprogram");
      $/="\n";
      my (@infofl);
      @infofl= <INFOFL>;
      close INFOFL;
      

      if ( grep(/hermit/, @infofl) ) 
                     {  open (HERMITIN,">daltcomm.new");
                        open (HERMITOLD,"<daltcomm");
                          my ($i,@stuff,$newstuff);
                          @stuff = <HERMITOLD> ;
                          close HERMITOLD;
                          if ( grep /\.CARMOM/, @stuff )  {
                            for ( $i = 0; $i <= $#stuff ; $i++)
                              { if ( grep /\.CARMOM/, $stuff[$i] )
                                 { if ( $stuff[$i+1] < 1 ) {
                                   $stuff[$i+1]="    1\n" ; last;}}}}
                            else {
                            for ( $i = 0 ; $i <= $#stuff ; $i++)
                              { if ( grep /\*\*INTEGRALS/,$stuff[$i])
                                { $newstuff = join (':', @stuff[0..$i],
                                  ".CARMOM\n","    1\n",
                                  @stuff[$i+1..$#stuff]);
                                  @stuff = split (/:/,$newstuff);last;}}}
                          print HERMITIN join '',@stuff;
                          close HERMITIN;
                          `cp -f daltcomm.new daltcomm`;
                          } # hermit

    # $exitvalue=1;
    return 0;
   }


  sub helpciudg { #ok

   local ( $keyel, @z  );
    ( $keyel ) = @_ ; 
      SWITCH: {
           if ( $keyel == 0 ) { $z[0]="Select a MR-SDCI calculation   ";
                                $z[1]="corresponds to NTYPE=0";
                               last SWITCH;}
           if ( $keyel == 1 ) { $z[0]="Select a MR-AQCC calculation   ";
                                $z[1]="corresponds to NTYPE=3, GSET=3 ";
                               last SWITCH;}
           if ( $keyel == 2 ) { $z[0]="Select a LRT calculation   ";
                                $z[1]="corresponds to NTYPE=3, GSET=3, LRTSHIFT must be set. ";
                               last SWITCH;}
           if ( $keyel == 3 ) { $z[0]="Number of states to be calculated" ;
                                $z[1]="In case of geometry optimization NROOT is the state to be optimized " ;
                               last SWITCH;}
           if ( $keyel == 4 ) { $z[0]="Reference vector to be followed " .
                                  " (FROOT) ";
                               last SWITCH;}

           if ( $keyel == 5 ) { $z[0]="In-core reference space diagonalization ";
                                $z[1]="recommended for reference spaces below  300 CSFs   ";
                                $z[2]="corresponds to IVMODE=3/8 ";
                               last SWITCH;}

           if ( $keyel == 6 ) { $z[0]="maximum number of iterations ";
                                $z[1]="(out-of-core only) corresponds to NRFITR";
                               last SWITCH;}
           if ( $keyel == 7 ) { $z[0]="convergence criterion rtol for out-of-core ref.space diag.";
                                $z[1]="the energy is converged to approx. rtol**2";
                                $z[2]="enter rtol values for each root separated by colons";
                                $z[3]="corresponds to RTOLRF";
                                last SWITCH;}

           if ( $keyel == 8 ) { $z[0]="No. of Bk iterations ";
                                $z[1]="corresponds to NBKITR "; 
                                last SWITCH;}
           if ( $keyel == 9 ) { $z[0]="minimum subspace dimension for Bk procedure";
                                $z[1]="corresponds to NVBKMN "; 
                                last SWITCH;}
           if ( $keyel == 10) { $z[0]="maximum subspace dimension for Bk procedure";
                                $z[1]="corresponds to NVBKMX "; 
                                last SWITCH;}
           if ( $keyel == 11) { $z[0]="convergence criterion rtol for Bk procedure";
                                $z[1]="the energy is converged to approx. rtol**2";
                                $z[2]="enter rtol values for each root separated by colons";
                                $z[3]="corresponds to RTOLBK";
                                last SWITCH;}
           if ( $keyel == 12) { $z[0]="No. of CI iterations ";
                                $z[1]="corresponds to NCIITR "; 
                                last SWITCH;}
           if ( $keyel == 13) { $z[0]="minimum subspace dimension for CI procedure";
                                $z[1]="corresponds to NVCIMN "; 
                                last SWITCH;}
           if ( $keyel == 14) { $z[0]="maximum subspace dimension for CI procedure";
                                $z[1]="corresponds to NVCIMX "; 
                                last SWITCH;}
           if ( $keyel == 15) { $z[0]="convergence criterion rtol for CI procedure";
                                $z[1]="the energy is converged to approx. rtol**2";
                                $z[2]="enter rtol values for each root separated by colons";
                                $z[3]="corresponds to RTOLCI";
                                last SWITCH;}

           if ( $keyel == 16) { $z[0]="Calculate one-electron density and NOs";
                                $z[1]="corresponds to IDEN "; 
                                last SWITCH;}

              } 

   return @z ;  
  } 



  sub rdmcdrtin {  #ok
   my ( $cigradient);
   ( $cigradient ) = @_;

   my ( $i, $ndot,$nact,$nsymd,$nrow,$smult,$nelt,$nela,$nwalk,$ncsf);
   local ( $spinmult,$noelec,$molsym,$tmp,$auxel);
   local ( $doub,@doub,$symd,@symd,$modrt,@modrt,$syml,@syml);


   open MCDRTFL,"<mcdrtfl";
#line 5:  ndot, nact, nsymd, nrow,ssym,smult,nelt,nela,nwalk,ncsf
   for ($i=1; $i<5; $i++) {$tmp=<MCDRTFL>; }
   $_=<MCDRTFL>;
   chop ;
   s/^ *//g;
   ($ndot,$nact,$nsymd,$nrow,$ssym,$smult,$nelt,$nela,$nwalk,$ncsf) = 
                                                             split(/\s+/,$_);
    print DEB "ndot,nact,nsymd,nrow,ssym,smult,nelt,nela,nwalk=
              $ndot,$nact,$nsymd,$nrow,$ssym,$smult,$nelt,$nela,$nwalk\n";

   if ( $nsymd != $nsym ) 
             { die "inconsistent infofl and mcdrtfl file (nsym) \n";}

   $spinmult=$smult;
   $noelec   = $nelt;
   $molsym  = $ssym;
   for ($i=1; $i<3; $i++) {$tmp=<MCDRTFL>;}
#  
#  if there are no doubly occupied mcscf orbitals
#  the corresponding lines are missing in mcdrtfl
#
   $/="\n";
   if ( $ndot ) 
        { $tmp=<MCDRTFL>;
          if (! grep (/doub/,$tmp) ) {die ("cannot find double entry");}
          $tmp=<MCDRTFL>;
          @doub=();
          while (! grep (/[a-z]/,$tmp))
           {$doub=$tmp; chop $doub; $doub =~s/^ *//g;
            @doub = (@doub ,split(/\s+/,$doub));
            print DEB "doub=@doub\n";
            $tmp=<MCDRTFL>; }
          @symd=();
          $tmp=<MCDRTFL>;
          while (! grep (/[a-z]/,$tmp))
           {$symd=$tmp; chop $symd; $symd =~s/^ *//g;
            @symd = (@symd, split(/\s+/,$symd));
            print DEB "symd=@symd\n";
            $tmp=<MCDRTFL>; }
        }
   else { @doub = () ;
          while(<MCDRTFL>) { if ( grep (/modrt/,$_) ) {last;} }
          @symd = () ; }

    if ($nact) {
   @modrt=();
    { $tmp=<MCDRTFL>;
      while ( ! grep(/syml/,$tmp ))
     { $modrt=$tmp; chop $modrt; $modrt =~s/^ *//g;
       @modrt = (@modrt, split(/\s+/,$modrt));
       print DEB "modrt=@modrt\n";
      $tmp=<MCDRTFL>; }
    }

    $tmp=<MCDRTFL>;
   @syml=();
     while ( ! grep(/nj/,$tmp))
     { $syml=$tmp; chop $syml; $syml =~s/^ *//g;
       @syml = split(//,$syml);
       print DEB "syml=@syml\n";
       $tmp=<MCDRTFL>; }
      }
     else { @syml=(); @modrt=();}
 
   close MCDRTFL;
     
#  $doub=join ':',@doub;
#  $symd=join ':',@symd;
#  $modrt=join ':',@modrt;
#  $syml=join ':',@syml;

   
   $auxel=mcdrtbasedinp($ssym,$smult,$noelec,\@doub,\@symd,\@modrt,\@syml);

#  fix for gradient calculations 

   if ($cigradient) 
       { &output2($smult,$noelec,$nsym,$cigradient,$auxel,0);
         system("mv cidrtin cidrtin.cigrd"); }
  
   &output2($smult,$noelec,$nsym,0,$auxel,1,0);


   return;
   }


 sub mcdrtbasedinp { #ok
  local ($ssym,$mult,$nel,$doub,$symd,$modrt,$syml);
  ($ssym,$mult,$nel,$doub,$symd,$modrt,$syml) = @_ ;
  my ( $mrow );
  my  (@mcscfdocc,$ierr,$i,@mcscfact,@rd );
  my ( $i,$x,$qrow);

  $mrow=2;

  &mvstr($mrow++ ,0,"MCDRT input data:");
  &mvstr($mrow++ ,0,"spatial symmetry: $ssym  mult:$mult #el:$nel");

# analyse doub


# @doub = split (/:/,$doub);
# @symd = split (/:/,$symd);
  @mcscfdocc = (0) x $nsym;
  for ( $i=0; $i<=$#doub; $i++)
   { $mcscfdocc[$$symd[$i]-1]++; } 
  $ierr=0; 
  for ( $i=0; $i<=$#$doub; $i++)
   { if ($$doub[$i] > $mcscfdocc[$$symd[$i]-1]) {$ierr++; }} 
  if ($ierr) {die "inconsistent doubly occ orbs ierr=$ierr\n";}


# @modrt= split (/:/,$modrt);
# @syml = split (/:/,$syml);
  @mcscfact = (0) x $nsym;
  print DEB "mcscfact:@mcscfact\n"; 
  print DEB "syml:@$syml\n";
  for ( $i=0; $i<=$#modrt; $i++)
   { $mcscfact[$$syml[$i]-1]++;  
     print DEB "$i mcscfact:@mcscfact\n" }; 
  $ierr=0; 
  for ( $i=0; $i<=$#$modrt; $i++)
   { if ($$modrt[$i] > ($mcscfact[$$syml[$i]-1]+$mcscfdocc[$$syml[$i]-1])) 
         {$ierr++;
          print "syml[$i]=$$syml[$i]  symd[$i]=$$symd[$i]\n";  }} 
  if ($ierr) {print "modrt: @$modrt\n mcscfact: @mcscfact \n mcscfdocc: @mcscfdocc\n"; 
              die "inconsistent active orbs ierr=$ierr\n";}


#       irrep1  irrep2   irrep3  irrep4
# nmo     x       x        x        x
# fcore   x       x        x        x
# fvirt   x       x        x        x
# intern  x       x        x        x
# extern  x       x        x        x


 &mvstr($mrow++,0,
    "count order (bottom to top): fc-docc-active-aux-extern-fv");
 &mvstr($mrow++,0,pack("A15A6A6A6A6A6A6A6A6","irreps",@labels));
 &mvstr($mrow++,0,pack("A15A6A6A6A6A6A6A6A6","MCSCF docc ",@mcscfdocc));
 &mvstr($mrow++,0,pack("A15A6A6A6A6A6A6A6A6","MCSCF active ",@mcscfact));
 &mvstr($mrow++,0,pack("A15A6A6A6A6A6A6A6A6","# basis fcts",@nmpsy));
 $mrow++;

#============================ frozen core ================================

 $qrow=18;
 do { 
  $x = &getvector($qrow,"MCSCF docc => frozen core",$nsym,"");
  if ( $debug ) { print DEB "MCSCF docc : $x \n"; }
  @fc=split(/:/,$x);
  $ierr=0;
  $ncorel =0;
  for ( $i=0; $i<$nsym ; $i++  )
    {if ( $fc[$i] > $mcscfdocc[$i] ) { $ierr++; }}
  if ( $ierr ) { 
    &mvstr(16,0,  
    "the number of frozen core orbs exceeds the number of docc mcscf orbs");
     $x=-1; }
    else { &clearline(16);} 
    }
 until ($x > -1 ) ;
 $ncorel= $nel-&countrefel(join(':',@fc));

 &mvstr($mrow++,0,pack("A15A6A6A6A6A6A6A6A6","frozen core  ",@fc));

#============================ frozen virtuals ============================

 do { $x = &getvector($qrow,"enter the frozen virt. orbitals per irrep",$nsym,"");
      @fv=split(/:/,$x);
      $ierr=0;
      for ( $i=0; $i<$nsym ; $i++  )
       {if ( $fv[$i] > ($nmpsy[$i]-$mcscfdocc[$i]) ) { $ierr++; }}

      if ( $ierr ) { &mvstr(16,0,  
                     "number of frozen core orbs greater than the available number of orbs"); $x=-1; }
    }
 until ($x > -1) ;

 &mvstr($mrow++,0,pack("A15A6A6A6A6A6A6A6A6","frozen virt  ",@fv));

 for ( $i=0; $i<$nsym; $i++ )
   { $ext[$i]=$nmpsy[$i]-$fc[$i]-$fv[$i]-$refdoc[$i]-$mcscfact[$i];}

 &mvstr($mrow++,0,pack("A15A6A6A6A6A6A6A6A6","external     ",@ext));

#============================ ref. docc orb.  ============================
 for ( $i=0; $i<$nsym; $i++) 
 {$refdoc[$i]=$mcscfdocc[$i]-$fc[$i];}

 &mvstr($mrow,0,pack("A15A6A6A6A6A6A6A6A6","ref. docc.  ",@refdoc));
 &mvstr($mrow+1,0,pack("A15A6A6A6A6A6A6A6A6","ci internal ",@mcscfact));


 do { $x = &getvector($qrow,"MCSCF active ==>  ref docc ",$nsym,"");
      @rd=split(/:/,$x);
      $ierr=0;
      for ( $i=0; $i<$nsym ; $i++  )
       {if ( $rd[$i] >  $mcscfact[$i] ) { $ierr++; }}

      if ( $ierr ) { &mvstr(16,0,  
                     "there are not so many mcscf active orbs"); $x=-1; }
    }
 until ($x > -1 ) ;

 for ( $i=0; $i< $nsym; $i++) 
 {$refdoc[$i]=$refdoc[$i]+$rd[$i]; 
  $ciact[$i]=$mcscfact[$i]-$rd[$i];}

 &mvstr($mrow++,0,pack("A15A6A6A6A6A6A6A6A6","ref docc  ",@refdoc));
 &mvstr($mrow  ,0,pack("A15A6A6A6A6A6A6A6A6","ci active   ",@ciact));

#============================ ci auxil. orb.  ============================

 do { $x = &getvector($qrow,"MCSCF active ==>  ci auxiliary ",$nsym,"");
      @ciaux=split(/:/,$x);
      $ierr=0;
      for ( $i=0; $i<$nsym ; $i++  )
       {if ( $ciaux[$i] >  ($mcscfact[$i]-$rd[$i]) ) { $ierr++; }}

      if ( $ierr ) { &mvstr(16,0,
                     "there are not so many mcscf active orbs"); $x=-1; }
    }
 until ($x > -1 ) ;

 if ( &countorbs(join(':',@ciaux)) ) { 
  $auxel = &getnumber($qrow,"Max. number of electrons in ci  aux. (ref.CSF only)",1,1,$ncorel);}
 else { $auxel=0;}


 for ( $i=0; $i< $nsym; $i++) 
 { $ciact[$i]=$mcscfact[$i]-$rd[$i]-$ciaux[$i];}

 &mvstr($mrow++,0,pack("A15A6A6A6A6A6A6A6A6","ci active   ",@ciact));
 &mvstr($mrow,0,pack("A15A6A6A6A6A6A6A6A6","ci auxiliary ",@ciaux));

 if ( ($nel - &countrefel(join(':',@fc)) - &countrefel(join(':',@refdoc))) < 0 )
   { &mvstr(15,0,"too few electrons for so many doubly occ orbitals"); $x=-1; 
    &pause("repeat complete input .. press return to exit"); die;}

#============================        virtuals ============================

 do { $x = &getvector($qrow,"MCSCF active orbs => virtual space ",$nsym,"");
      @rd=split(/:/,$x);
      $ierr=0;
      for ( $i=0; $i<$nsym ; $i++  )
       {if ( $rd[$i] > ($ciact[$i]) ) { $ierr++; }}

      if ( $ierr ) { &mvstr(16,0, 
                     "number of virtual orbs greater than the available active of orbs"); $x=-1; }
    }
 until ($x > -1) ;

    if (&countorbs(join(':',@ciact)) < 2)
     { &popup_ask("need at least two internal orbitals");
       die();
     }


 for ( $i=0; $i<$nsym; $i++) 
 {$ciact[$i]=$ciact[$i]-$rd[$i];
  $ext[$i]=$ext[$i]+$rd[$i]}
 $nciact =pack("A15A6A6A6A6A6A6A6A6","ci active  ",@ciact);
 &mvstr($mrow-1,0,$nciact);
 &mvstr($mrow-3,0,pack("A15A6A6A6A6A6A6A6A6","external     ",@ext));

#=========================== exc level, refsym ,gensp ===================

 $mrow++;
 $mrow++;

 $exc = &getnumber($qrow,"Enter the excitation level (0,1,2)",1,0,2);
 &mvstr($mrow,0,pack("A10A2","exc.level:",$exc));

 if ( $exc == 2 ) 
 {$gensp = &getzeichen($qrow,"Generalized interacting space restrictions [y|n]",
           1);  }
  else { $gensp="n";}
 &mvstr($mrow,0,pack("A10A2A10A2A10A2","exc.level:",$exc,"gen.space:",$gensp,"#aux.elec:",$auxel));

 do { $refsym = &getvector($qrow,"Enter the allowed reference symmetries",1,$ssym,""); 
      @refsym=split(/:/,$refsym);
      $ierr=0;
      for ( $i=0; $i<=$#refsym ; $i++  )
       {if ( $refsym[$i] > $nsym || $refsym[$i] < 1 ) { $ierr++; }}
      if ( $ierr ) { &mvstr(19,0,"invalid reference symmetry"); $refsym=-1; }
    }
 until ($refsym > -1) ;
 &clearline(19);

 $refsym =~ s/:/ /g;
 &mvstr($mrow,0,pack("A10A2A10A2A18A2A2A2A2A2A2A2A2",
 "exc.level:",$exc,"gen.space:",$gensp,
 "allowed ref. syms:",$refsym));

  my $groups = &getzeichen($qrow,"Apply additional group restrictions for DRT $idrt [y|n]",1);
  &clearline($qrow);
  if ( grep (/[yY]/,$groups)) 
   { ($groupdefs[$idrt][0],$groupdefs[$idrt][1]) = 
       &grouprestrictions($mrow,$qrow,$cigradient,0,$nel);}

 $row=$qrow+1; 
 print DEB "nogsets=$groupdefs[$idrt][0]\n";
  return  $auxel;
}

 sub grouprestrictions {  # noch nicht durchgesehen
   my ($mrow,$qrow,$stepfc,$multi,$noelec) =@_;
   my  @group = ( );
   my  $nogsets;
#
# assumes the following arrays to contain valid data:
#  @fc @refdocc @ciact
#  $stepfc is set if fcore orbitals are enforced through step vectors
#  $multi is set if multiple DRTs are used (ordering of fc/refdocc/act)
#
#================================= group occupations =========================
 &move($mrow,0);
#my $groups = &getzeichen($qrow,"Apply additional group restrictions [y|n]",1);
#&clearline($qrow);
 $mrow++;
 $nogsets=0;
#if ( grep (/[yY]/,$groups) ) 
#    { 
       my ($el,$cnt,$i,$j);
       $nogsets=1;
     while ( )
      {
      my @levelfc = ();
      my @levelrd = ();
      my @levelact= ();
      &mvstr($mrow+1,0,"All active orbitals must be distributed among groups.");
      &mvstr($mrow+2,0,"Each group is designated by one letter A(a) B(b) ...");
      &mvstr($mrow+3,0,"State the group labels below each orbital given:");
      my $str="    ";
      my $str2=' ';
      my ($nfc,$nrefdoc,$von,$bis,$offset,$actel);
      my ($offset2,$i,$j );

################################ REFTOP ordering #########################

 if ( grep(/REFTOP/,$ORDERING)) 
 {
# no multi
# also reorder to  aux - active - refdocc - fc 

     {
      $nfc=0;
      $offset2=0;
      for ( $i=0; $i<$nsym; $i++)
       { $nfc += $fc[$i];
         $offset2 = $offset2 + $ciaux[$i]+$ciact[$i] + $refdoc[$i]}

      if ( $cigradient) 
       { for ( $i=0; $i<$nfc; $i++) {$levelfc[$i]=$i+1+$offset2;}}

      $offset2=0;
      for ( $i=0; $i<$nsym; $i++)
       {$offset2 = $offset2 + $ciaux[$i] + $ciact[$i]}

      $nrefdoc=0;
      for ( $i=0; $i<$nsym; $i++) 
        { for ( $j= 0; $j< $refdoc[$i] ; $j++)
            {$levelrd[$nrefdoc]=$nrefdoc+$offset2+1; 
             $nrefdoc++; }
        } 

      $offset=0;
      for ( $i=0; $i<$nsym; $i++)
        {$von=$fc[$i]+$refdoc[$i]+$ciact[$i]+1;
         $bis=$von+$ciaux[$i];
         for ( $j= $von; $j< $bis ; $j++)
           { $str=$str . "${j}$labels[$i] "; 
             $levelact[$offset]=$offset+1;
             $offset++; }
        }
      for ( $i=0; $i<$nsym; $i++)
        {$von=$fc[$i]+$refdoc[$i]+1;
         $bis=$von+$ciact[$i];
         for ( $j= $von; $j< $bis ; $j++)
           { $str=$str . "${j}$labels[$i] ";
             $levelact[$offset]=$offset+1;
             $offset++; }
        }


       }
}
################################ REFBOTTOM ordering ########################
if (grep(/REFBOTTOM/,$ORDERING))
 {# no multi

     {$offset=0;
      for ( $i=0; $i<$nsym; $i++)
       { $offset= $offset+$fc[$i];}
      if ( $cigradient)
       { for ( $i=0; $i<$offset; $i++) {$levelfc[$i]=$i+1;}}
      $nfc=$offset;
      $offset=0;

      for ( $i=0; $i<$nsym; $i++)
        { $von=$fc[$i]+1;
          $bis=$von+$refdoc[$i];
          for ( $j= $von; $j< $bis ; $j++)
           { if ($stepfc) {push @levelrd,$offset+$nfc+1; }
                  else    {push @levelrd,$offset+1; }
             $offset++; }
        }
      $nrefdoc=$offset;
      $offset=0;

      for ( $i=0; $i<$nsym; $i++)
        {
      $von=$fc[$i]+$refdoc[$i]+1;
      $bis=$von+$ciact[$i];
        for ( $j= $von; $j< $bis ; $j++)
           { $str=$str . "${j}$labels[$i] ";
             if ( $stepfc) { push @levelact,$offset+$nfc+$nrefdoc+1;}
                   else    { push @levelact, $offset+$nrefdoc+1;}
             $offset++; }
        }

      for ( $i=0; $i<$nsym; $i++)
        {
      $von=$fc[$i]+$refdoc[$i]+$ciact[$i]+1;
      $bis=$von+$ciaux[$i];
        for ( $j= $von; $j< $bis ; $j++)
           { $str=$str . "${j}$labels[$i] ";
             if ( $stepfc) { push @levelact,$offset+$nfc+$nrefdoc+1;}
                   else    { push @levelact, $offset+$nrefdoc+1;}
             $offset++; }
        }
       }
  }
################################################################################


#
#  in accordance with the later orbital -> level ordering
#  we have the order  all fc - all refdoc - all active
#  this list contains already the glevel!
#


      print DEB "str=$str\n"; 
      print DEB "nsym=$nsym \n";
      print DEB "fc=@fc \n" ;
      print DEB "ciact=@ciact \n";
      print DEB "refdoc=@refdoc \n";
      print DEB "levelfc=@levelfc \n";
      print DEB "levelrd=@levelrd \n";
      print DEB "levelact=@levelact\n";
      print DEB "multi = $multi =====================\n"; 
      &mvstr($mrow+4,0,$str);
      my $grpdes=&getvector($mrow+5,"->",1,"",1);
      $grpdes = uc $grpdes;
      my @grpdes=split (/:/,$grpdes);
#
      if ( $#levelact != $#grpdes ) 
        {$str=sprintf "not enough group designations: need %d found %d \n",$#levelact+1,$#grpdes+1;
         &popup_ask($str); next;}

      my $exgroups=" ";
      for $el ( A .. Z )
       { if (grep(/$el/,$grpdes)) { $exgroups = $exgroups . "$el:"; }}
      $exgroups=~s/^ *//;
      for $el ( split(/:/,$exgroups) )
      { $cnt=-1; 
       for ( $i=0 ; $i <= $#levelact ; $i++)
        {if ( $grpdes[$i] eq $el ) 
                    { $cnt++; 
                     $group[$nogsets]{$el}[$cnt]=$levelact[$i];} 
        } 
      }  
      $actel=$noelec-2*($nfc+$nrefdoc);
      
      while (1)
      {
      my ( $elingroup);
      $elingroup=0;
      foreach $el ( keys %{$group[$nogsets]} )
       { 
      my $minmax=&getnumber($mrow+7,"Enter the number of electrons in group $el ( 0 .. $actel) ",3,0,$actel);
      $elingroup= $elingroup+$minmax;
      my $elx = join('',$el,"min");
      $group[$nogsets]{$elx}=$minmax;
      $elx = join('',$el,"max");
      $group[$nogsets]{$elx}=$minmax;
       $actel=$actel-$minmax;
      }
       last;
      }

#
# add a separate group for the refdocc and fcore part
# using the REFDOC and FCORE label
#
         if ( ! $stepfc ) 
             { for ( $i=0 ; $i < $nrefdoc ; $i++ )
                {$group[$nogsets]{"REFDOC"}[$i]=$levelrd[$i];}
               $group[$nogsets]{"REFDOCmin"}=$nrefdoc*2;
               $group[$nogsets]{"REFDOCmax"}=$nrefdoc*2;
             }
        else              
             { for ( $i=0 ; $i < $nrefdoc ; $i++ )
                {$group[$nogsets]{"REFDOC"}[$i]=$levelrd[$i];}
               $group[$nogsets]{"REFDOCmin"}=$nrefdoc*2;
               $group[$nogsets]{"REFDOCmax"}=$nrefdoc*2;

               for ( $i=0 ; $i < $nfc ; $i++ )
                {$group[$nogsets]{"FCORE"}[$i]=$levelfc[$i];}
               $group[$nogsets]{"FCOREmin"}=$nfc*2;
               $group[$nogsets]{"FCOREmax"}=$nfc*2;
             }

#
#  at this point %$group[$nogsets] contains the group information
#  for group set definition $nogsets
#         @group{$el}[] = each entry bfn#:irrep#
#         $group{$el . min } = minimum occupation 
#         $group{$el . max } = maximum occupation 
#                       

     for ( $i=1; $i <=7 ; $i++)
        { &clearline($mrow+$i);}
     print DEB "nogsets=$nogsets\n";
 my $anotherone = &getzeichen($qrow,"Another group set definition [y|n]",1);
     if ( grep (/[nN]/,$anotherone ) ) {last;}
     $nogsets++;
     } 
#    }
 return ( $nogsets, \@group );
}




# additional checks: (fc+refdocc)*2 < neltot
# ciactive > 0 

  sub output2 {

  my ($str1,$idrt, $mult, $nel, $nsym, $cigradient,$auxel,$dosoci);
  ($mult, $nel, $nsym,$cigradient,$auxel,$dosoci,$idrt ) = @_;
   local ($nactel,$nreforb,$nactorb,$nauxorb);
  my ($nintern,@scr);
  

  open (CIDRTKY,">cidrtin");
    if (($lx) && ($dosoci)) 
   { print CIDRTKY "Y /Spin-Orbit CI calculation? \n";
     print CIDRTKY " $mult / maximal spin multiplicity \n";
     print CIDRTKY " $lx  $ly  $lz / lx ly lz\n";}
  else
   { print CIDRTKY " N /Spin-Orbit CI Calculation? \n";
    print CIDRTKY " $mult / input the spin multiplicity \n";}
    
  #print CIDRTKY " $mult / input the spin multiplicity \n";
  print CIDRTKY " $nel  / input the total number of electrons \n";
  print CIDRTKY " $nsym / input the number of irreps \n";
  print CIDRTKY " Y / input symmetry labels ";
  foreach $str1 ( @labels )
  {print CIDRTKY "\n $str1 ";}
  print CIDRTKY " / symmetry labels ";
  print CIDRTKY "\n ", join("  ",@nmpsy)," / orbitals per irrep \n "; 
  print CIDRTKY " $ssym /input the molecular spatial symmetry \n ";

  
  my ( $i,$nf,$str1,$j ); 
  @scr=();
  $i=1;
  $nf=0; 
  while ( $i <= $nsym )
  { $j=1;
     $nf+=$fc[$i-1]; 
     while ( $j <= $fc[$i-1] )
      { if ( ! $cigradient ) {push @scr,$i,$j;} $j++; 
        if ( ! $cigradient ) { $nel -=2;} }
     $i++;
   }
   &printarray(2,\@scr," / frozen core orbitals ");
 
   @scr=(); 
  $i=1;
   my ( $nv ) ; 
    $nv=0;
  while ( $i <= $nsym )
  {  $j=1;
     $nv +=$fv[$i-1]; 
     while ( $j <= $fv[$i-1] )
      { $x=$nmpsy[$i-1]+1-$j;
        push @scr,$i,$x; $j++;}
     $i++;
   }
   &printarray(2,\@scr," / frozen virtual orbitals ");

################################# REFTOP ordering ##########################

 if (grep(/REFTOP/,$ORDERING))
 {
   my ($nn);
# need to have proper ordering:
#  1. ciactive 2. refdocc 3. fcore (if cigrad) 

  $nintern=0;
#
#  ci aux      
#
   @scr=();
   $i=1;
  while ( $i <= $nsym )
  { $j=1;
     while ( $j <= $ciaux[$i-1] )
      { $x=$fc[$i-1]+$refdoc[$i-1]+$ciact[$i-1]+$j;
        push @scr,$i,$x; $j++; $nintern++;
        }
     $i++;
   }

  if ( $nintern ) {  &printarray(2,\@scr," ");}
#
#  ci active
#
   $nn=$nintern;
   @scr=();
   $i=1;
  while ( $i <= $nsym )
  { $j=1;
     while ( $j <= $ciact[$i-1] )
      { $x=$fc[$i-1]+$refdoc[$i-1]+$j;
        push @scr,$i,$x;$j++; $nintern++;
        }
     $i++;
   }
   if ( $nintern != $nn ) {&printarray(2,\@scr," ");}

#
# ref docc
#
  $nn=$nintern;
  @scr=();
  $i=1;
  while ( $i <= $nsym )
  { $j=1;
     while ( $j <= $refdoc[$i-1] )
      { $x=$fc[$i-1]+$j;
        push @scr,$i,$x;$j++; $nintern++;
        }
     $i++;
   }
   if ( $nintern != $nn ) {&printarray(2,\@scr," ");}
#
# frozen core
#
  @scr=();
  if ($cigradient) 
    { $i=1;
      while ( $i < $nsym )
       { $j=1; 
         while ( $j <= $fc[$i-1] )
          { push @scr,$i,$j; $j++; $nintern++; }
         $i++;
        }
     }
   &printarray(2,\@scr," /internal orbitals ");

}
################################### REF BOTTOM ordering ###################

  if ( grep(/REFBOTTOM/,$ORDERING))
 {
    my ($nn);
# need to have proper ordering:
#  1. fcore (if cigrad) 2. ref docc 3. ciactive 4. ciaux
  $nintern=0;
  @scr=();
  if ($cigradient)
    { $i=1;
      while ( $i <= $nsym )
       { $j=1;
         while ( $j <= $fc[$i-1] )
          { push @scr,$i,$j; $j++; $nintern++; 
           }
         $i++;
        }
     }
  if ( $nintern ) { &printarray(2,\@scr," ");}
  $nn=$nintern;
  @scr=();
  $i=1;
  while ( $i <= $nsym )
  { $j=1;
     while ( $j <= $refdoc[$i-1] )
      { $x=$fc[$i-1]+$j;
        push @scr,$i,$x; $j++; $nintern++;
        }
     $i++;
   }
  if ( $nintern != $nn ) { &printarray(2,\@scr," ");}

   $nn=$nintern;
   @scr=();
   $i=1;
  while ( $i <= $nsym )
  { $j=1;
     while ( $j <= $ciact[$i-1] )
      { $x=$fc[$i-1]+$refdoc[$i-1]+$j;
        push @scr,$i,$x; $j++; $nintern++;
        }
     $i++;
   }
  if ( $nintern != $nn ) { &printarray(2,\@scr," ");}
 
  # auxiliary active orbitals  - no more than $auxel electrons
    @scr=();
    $i=1;
    while ( $i <= $nsym )
    { $j=1;
       while ( $j <= $ciaux[$i-1] )
        { $x=$fc[$i-1]+$refdoc[$i-1]+$ciact[$i-1]+$j;
          push @scr,$i,$x;$j++; $nintern++;
          }
       $i++;
     }
 
   &printarray(2,\@scr," /internal orbitals ");

 }
##########################################################################

   $nreforb=0; $nactorb=0;
   $nauxorb=0; 
   for ( $i=0; $i<$nsym; $i++)
   { $nreforb+=$refdoc[$i];
     $nauxorb+=$ciaux[$i];
     $nactorb+=$ciact[$i] ; } 
   if ( $cigradient ) { $nreforb+=$nf;}
   $nactel = $nel-$nreforb*2;
###################### REFTOP ordering ################################

 if (grep(/REFTOP/,$ORDERING))
 {
   print CIDRTKY "  /input the number of ref-csf doubly occupied orbitals\n";

     my ( $scr1,$scr2);
     $scr1 = $nel-$nreforb*2;
     @scr = ((0) x ($nauxorb+1) , ( $auxel)  x ($nactorb-1), 
            ( $scr1) x $nreforb );
     &printarray ( 1, \@scr, "/occminr"); 

     @scr = ( ( $auxel) x $nauxorb,( $nel) x ($nreforb+$nactorb));
     &printarray ( 1, \@scr, "/occmaxr");;

     @scr =  ( ( 0) x ($nintern)); 
     &printarray ( 1, \@scr, "/bminr");;
     @scr =  ( ($nel) x ($nintern)); 
     &printarray ( 1, \@scr, "/bmaxr");;

     @locstr = (( "1111" ) x ($nactorb+$nauxorb), ( "1000" ) x $nreforb);
     &printarray(1,\@locstr,"/step masks r");

     print CIDRTKY " $exc / input the maximum excitation level \n";

     if ( $auxel > $exc ) { $scr2 = $auxel-$exc;}
                     else {$scr2 = 0;}
     @scr = (( 0 )  x $nauxorb , 
            ( $scr2) x ($nreforb+$nactorb));
     print DEB "occmin: scr2=$scr2, nreforb=$nreforb, actorb=$nactorb\n";
     &printarray ( 1, \@scr, "/occmin"); 

     $scr2 = $exc + $auxel ; 
     @scr =( ( $scr2) x $nauxorb,( $nel) x ($nreforb+$nactorb));
     &printarray ( 1, \@scr, "/occmax"); 



     @scr =   ((0) x $nintern);
     &printarray ( 1, \@scr, "/bmin"); 
     @scr = (($nel) x $nintern); 
     &printarray ( 1, \@scr, "/bmax"); 

 if ( $cigradient) 
  { # all frozen core orbitals have a step vector 1000
    my (@str1,@str2);
    @locstr = (("1111") x ($nauxorb+$nactorb+$nreforb-$nf) , ("1000") x $nf ); 
  }
  else { @locstr = ( "1111" ) x $nintern; }

 }

######################################## REFBOTTOM ordering ##################
if ( grep(/REFBOTTOM/,$ORDERING)) 
 {
   print CIDRTKY " $nreforb /input the number of ref-csf doubly occupied orbitals\n";
   # if there are no internal orbitals left skip the next few lines
     my ( $scr1,$scr2,@scr);
     if ( ($nactorb+$nauxorb) != 0 )
     { $scr1 = 1+$nauxorb;
       $scr2 = $nactel-$auxel; 
       @scr = ( ( 0 ) x ($nactorb-1), ($scr2) x ($scr1));
       &printarray ( 1, \@scr, "/occminr");

       $scr2 = $nauxorb+$nactorb; 
       @scr = ( ($nactel) x $scr2 ) ;
       &printarray ( 1, \@scr, "/occmaxr");
       @scr = ( ( 0 )  x $scr2 );
       &printarray ( 1, \@scr, "/bminr");
       @scr = ( ( $nactel )  x $scr2 );
       &printarray ( 1, \@scr, "/bmaxr");

# no more than 80 char per line

#    $i=$nactorb+$nauxorb; 
       @locstr = ( "1111" ) x ($nactorb+$nauxorb);
       &printarray(1,\@locstr,"/step masks r");
     }

     print CIDRTKY " $exc / input the maximum excitation level \n";

     $scr1 = 1+$nauxorb;
     if ( $cigradient) { $scr2 = $ncorel-$auxel-$exc+2*$nf;}
                else       { $scr2 = $ncorel-$auxel-$exc; }
     if ( $scr2 < 0 ) { $scr2=0;}
#fp: adjust occmin in the case of CI singles (or MCSCF) to make sure
#      that cidrt.x does not run out of memory
     if ($exc == 2)
     {
        @scr = (  (0) x ($nintern-$nauxorb-1), ($scr2) x $scr1 ) ;
     }
     else
     {
        undef(@scr);
        $currel=0;
        if ( $cigradient)
        {
            for ($iorb=1; $iorb<=$nf; $iorb++)
            {
                $currel+=2;
                push(@scr,$currel);
            }
            $ndocc=$nintern-$nauxorb-$nactorb-$nf
        }
        else
        {
            $ndocc=$nintern-$nauxorb-$nactorb
        }
        $currel-=$exc;
        for ($iorb=1; $iorb<=$ndocc; $iorb++)
        {
            $currel+=2;
            push(@scr,$currel);
        }
        for ($iorb=1; $iorb<=$nactorb-1; $iorb++)
        {
            push(@scr,$currel);
        }
        for ($iorb=1; $iorb<=$scr1; $iorb++)
        {
            push(@scr,$scr2);
        }
        
        
     }
     &printarray ( 1, \@scr, "/occmin");
     @scr = ( ( $nel) x $nintern ) ;
     &printarray ( 1, \@scr, "/occmax");
     @scr = ( ( 0) x $nintern ) ;
     &printarray ( 1, \@scr, "/bmin");
     @scr = (  ($nel) x $nintern ) ;
     &printarray ( 1, \@scr, "/bmax");

 if ( $cigradient)
  { # all frozen core orbitals have a step vector 1000
    my (@str1,@str2);
    @locstr = (("1000") x $nf, ("1111") x ($nintern-$nf));
  }
  else { @locstr = ( "1111" ) x $nintern; }

 }

#####################################################################################

     &printarray(1,\@locstr,"/step masks");

     print CIDRTKY " -1 0 0 /vertices to be removed  \n";
# no generalized interacting space restrictions with more than
# one reference symmetry allowed.
     if ($refsym[1] || ( $exc != 2 )) { $gensp="N";}
     print CIDRTKY " $gensp / impose generalized space restrictions \n"; 
     print CIDRTKY "  -1 0 0 0 /arcs to be removed  \n";
     print CIDRTKY "  -1  0 / DRT levels to be printed out \n "; 

     print CIDRTKY " @refsym / allowed reference symmetries \n"; 
     print CIDRTKY " N /keep all of the z-walks as references? \n";
     print CIDRTKY " Y /generate walks while applying reference drt restrictions? \n" ;

     if ( $groupdefs[$idrt][0] ) 
      { print CIDRTKY " Y / impose additional orbital-group occupation restrictions?\n";
        print CIDRTKY "$groupdefs[$idrt][0] / number of orbital group sets \n";
        for ( $j=1; $j <= $groupdefs[$idrt][0]; $j++ )
         { my @group = @{$groupdefs[$idrt][1]}; 
           my @orbgroups= keys %{$group[$j]};
           printf CIDRTKY " %3d  / number of orbital groups in set $j \n",
                                           int($#orbgroups+1)/3;
           for  $el (@orbgroups)
            { if (grep(/min/,$el) || grep (/max/,$el)) {next;} 
              $elmin =join('',$el,"min");
              $elmax =join('',$el,"max");
               print DEB "elmin,elmax=$elmin,$elmax, el=$el\n"; 
              printf CIDRTKY " %3d %3d %3d / #orb, minocc,maxocc \n",
    $#{$group[$j]{$el}}+1, $group[$j]{$elmin}, $group[$j]{$elmax} ;
              $glevel = join (' ',@{$group[$j]{$el}});
              print CIDRTKY " $glevel / glevel \n";
            }
         } 
      }
     else
      {print CIDRTKY " N /impose additional orbital-group occupation restrictions? \n ";} 
     print CIDRTKY " N /apply primary reference occupation restrictions? \n" ;
     print CIDRTKY " N /manually select individual walks? \n "; 
     print CIDRTKY " -1 "; 
     @scr = (  (4)  x ($nintern-1) ) ;
     &printarray ( 1, \@scr, "/step vector disp pairs");
     print CIDRTKY "   0 /input reference walk number (0 to end)\n";
 if (grep(/REFBOTTOM/,$ORDERING))
    { @scr = ( (2) x $nreforb, ( 0 ) x ($nintern-$nreforb));}  
 if (grep(/REFTOP/,$ORDERING))
    { @scr = ( (0)  x ($nintern-$nreforb),  (2)  x $nreforb,);}  
     print DEB "nintern=$nintern,nreforb=$nreforb\n"; 
     &printarray ( 1, \@scr, "/reference occupations vector ");
     print CIDRTKY " n /\n  0 /input mrsdci walk number (0 to end) \n";
     print CIDRTKY " cidrt_title \n \n y /print drt file \n \n";
  close CIDRTKY;
  return 0; 
  }


 sub getnumber {

  local ($xrow,$text,$xlen,$von,$bis,$number,$gettext);
  ($xrow,$text,$xlen,$von,$bis) = @_;
  my ( $valid );

  $valid=1;
  if (( $lines[$aline] eq "y") || ($lines[$aline] eq "n" ))  {$aline += 1} #special treatment for "Skip DRT input"
  if ($lines[$aline] eq "") {
    $gettext = "$text   ";}
  else {
    $gettext = "$text \[ $lines[$aline]\]   ";}
  do { $number = &menu_getstr($xrow,0,$gettext,0,"",$xlen,0,1);
  #do { $number = &menu_getstr($xrow,0,$text . "  ",0,"",$xlen,0,1);
         if (( $number eq "" ))
            { $number = $lines[$aline]; }
         if ( ( $number < $von ) || ($number > $bis))
                               { &move($xrow+1,0);
                                &addstr("invalid number"); $valid=0} 
       else {&move($xrow+1,0);
             &clearline($xrow+1);$valid=1}
      }
  until ($valid);
  #fp
  $aline += 1;
  print OUT "$number\n";

  return $number;
 }

 sub getzeichen {

  local ($xrow,$text,$xlen,$number,$gettext);
  ($xrow,$text,$xlen) = @_;
  
  if ($lines[$aline] eq "") {
    $gettext = "$text   ";}
  else {
    $gettext = "$text \[ $lines[$aline]\]   ";}
  do {   $number = &menu_getstr($xrow,0,$gettext . "  ",1,"",$xlen,0,0);
         $number =~ s/[A-Z]/[a-z]/g;
         if (( $number eq "" ))
            { $number = $lines[$aline]; }
         if ( ! ( $number eq "y" || $number eq "n" ))  
                   { &mvstr($xrow+1,0,"invalid entry"); $number=0;} 
         else { &mvstr($xrow+1,0,"                  ");}
      }
  
  until ($number);
  
  $aline += 1;
  print OUT "$number\n";
  
  return $number;
 }

 sub getvector {

  local ($x,$xrow,$text,$vector,@vector,$minel,$default,$clear,$gettext);
# returns a vector separated by :
  ($xrow,$text,$minel,$default,$clear)=@_;
  if ($lines[$aline] eq "") {
    $gettext = "$text   ";}
  else {
    $gettext = "$text \[ $lines[$aline]\]   ";}
  if ( $clear) {$clear=0;}
 do { $x = &menu_getstr($xrow,0,$gettext, $clear,$default,0,0,0);
      if (( length($x) == 0 ))
            { $x = $lines[$aline]; }
      if ( $debug ) { print DEB "getvector : $x\n";} 
      if ( ( ! $x ) && ( $x ne "0")  ) { &move(15,0);
                       &addstr("invalid number");}
      else {
      $x=~s/^ *//;
      @vector= split /[\s\/]+/,$x;
      $x = @vector;
      if ( $debug ) { print DEB "getvector2 : $x, @vector, $nsym,$minel \n";} 
      if ( $x == 0 ) { @vector = (0) x $nsym ; $x=$nsym;}
      if ( $x <  $minel) { &move($xrow+3,0);
        &addstr("you entered values for $x instead of $minel irreps"); $x=0; }}
    }
  until ($x) ;
  print XX "getvector: $clear\n"; 
  &clearline($xrow+3); 
  $vector = join ':',@vector;
 #fp
  $aline += 1;
  print OUT "@vector\n";
  return $vector;
  }

  sub clearline {
   local ($xrow,$str);
   ($xrow) = @_;
   $str = ' ' x 80;
   &mvstr($xrow,0,$str);
   return;
   }


  sub countrefel {
    my ( $i, $el, @feld );
    ($i)=@_; 
    @feld = split(/:/,$i); 
    $el=0;
    foreach $i ( @feld  ) { $el+=$i}
    $el=2*$el;
    return $el;
    }

   sub countorbs {
    my  ( $i, $el, @feld );
    ($i)=@_;
    @feld = split(/:/,$i);
    $el=0;
    foreach $i ( @feld  ) { $el+=$i}
    return $el;
    }
  
  sub ciisc {
   my ( $cigradient ) ;
   ( $cigradient ) = @_;  
   
   $ndrt = &getnumber(8,"Enter number of DRTS [1-8]  ",1,1,8);
   open (CMS,">cidrtmsin");
   print CMS "$ndrt\n";
   close(CMS);
   
   for ($idrt=1; $idrt<=$ndrt; $idrt++)
   {
    &clear();
    &pause("Input for DRT #$idrt\n");
    &cidrtin($cigradient,$idrt);
    
    rename("cidrtin","cidrtin.$idrt");
    rename("ciudgin","ciudgin.drt$idrt");
   }
  }

  sub cidrtin {

   my ( $cigradient,$idrt ) ;
   ( $cigradient,$idrt ) = @_;

   local ( $spinmult,$noelec,$molsym,$tmp);
   local ( $doub,@doub,$symd,@symd,$modrt,@modrt,$syml,@syml);
   local ( @refsym,$exc,$gensp);


   $smult=0;
   $noelec=0;
   $ssym=0;

   ($smult,$noelec,$ssym,$auxel)= &cidrtinp($idrt);

      if ($cigradient)
             { &output2($smult,$noelec,$nsym,$cigradient,$auxel,0,$idrt);
               system("mv cidrtin cidrtin.cigrd"); }
      &output2($smult,$noelec,$nsym,0,$auxel,1,$idrt);

   return;
   }


 sub cidrtinp {
    my ($idrt) = @_; 
 local ($ssym,$mult,$nel);
 local ( $mrow );

 $qrow=18;
 $mrow=5;
 &mvstr($mrow++,0,"count order (bottom to top): fc-docc-active-aux-extern-fv");
 &mvstr($mrow++,0,pack("A15A6A6A6A6A6A6A6A6","irreps",@labels));
 &mvstr($mrow++,0,pack("A15A6A6A6A6A6A6A6A6","# basis fcts",@nmpsy));
 $mrow++;

 &mvstr(2,0,
 pack("A13A2A11A3A12A2","Multiplicity:",$mult,"#electrons:",$nel,
             "Molec. symm.",$ssym));
             
$spinorbit = &getzeichen(8,"Spin-Orbit CI [y|n] ", 1);

 if ( grep (/[yY]/,$spinorbit )) 
   { $mult = &getnumber(8,"Enter highest multiplicity  ",2,1,100);
     $lxyzir = &getvector(8,"irreducible representation of lx, ly, lz ",3,"");
     ($lx,$ly,$lz) = split(/:/,$lxyzir);
   }
  else
  { $mult = &getnumber(8,"Enter the multiplicity",2,1,100);}
             
 #$mult = &getnumber($qrow,"Enter the multiplicity",2,1,100);

 &mvstr(2,0,
 pack("A13A2A11A3A12A2","Multiplicity:",$mult,"#electrons:",$nel,
             "Molec. symm.",$ssym));
 $nel = &getnumber($qrow,"Enter the number of electrons  ",3,1,1000);

 &mvstr(2,0,
 pack("A13A2A11A3A12A2","Multiplicity:",$mult,"#electrons:",$nel,
             "Molec. symm.",$ssym));
 $ssym = &getnumber($qrow,"Enter the molec. spatial symmetry ",1,1,$nsym);
 &mvstr(2,0,
  pack("A13A2A11A3A13A3","Multiplicity:",$mult,"#electrons:",$nel,
             "Molec. symm.:",$labels[$ssym-1]));
 &clearline($qrow);


#       irrep1  irrep2   irrep3  irrep4
# nmo     x       x        x        x
# fcore   x       x        x        x
# fvirt   x       x        x        x
# refdocc x       x        x        x
# ciact   x       x        x        x
# ciaux   x       x        x        x
# extern  x       x        x        x


#============================ frozen core ================================

 my ( $i,$x);
 $x = &getvector($qrow,"number of frozen core orbitals per irrep",$nsym,"");
 @fc=split(/:/,$x);
 $ncorel = $nel-&countrefel($x);

 &mvstr($mrow++,0,pack("A15A6A6A6A6A6A6A6A6","frozen core  ",@fc));

#============================ frozen virtuals ============================

 do { $x = &getvector($qrow,"number of frozen virt. orbitals per irrep",$nsym,"");
      @fv=split(/:/,$x);
      $ierr=0;
      for ( $i=0; $i<$nsym ; $i++  )
       {if ( $fv[$i] > ($nmpsy[$i]-$fc[$i]) ) { $ierr++; }}

      if ( $ierr ) { &mvstr(16,0,
                     "number of frozen core orbs greater than the available number of orbs"); $x=-1; }
    }
 until ($x > -1) ;

 &mvstr($mrow++,0,pack("A15A6A6A6A6A6A6A6A6","frozen virt  ",@fv));


#============================ internal orbitals ============================
  do 
  { $x = &getvector($qrow,"number of internal(=docc+active+aux)"
                         ." orbitals per irrep",$nsym,"");
    &clearline(16);
    @int=split(/:/,$x);
    if (&countorbs(join(':',@int)) < 2 ) 
     { &mvstr(16,0,"need at least two internal orbitals");
       $x=-1;
     }
  }
  until ( $x != -1 );
  

  &mvstr($mrow++,0,pack("A15A6A6A6A6A6A6A6A6","internal",@int));

#============================ ref. docc orb.  ============================
 do { $x = &getvector($qrow,"ref doubly occ orbitals per irrep",$nsym,"");
      @refdoc=split(/:/,$x);
      $ierr=0;
      for ( $i=0; $i<$nsym ; $i++  )
       {if ( $refdoc[$i] >  $int[$i] ) { $ierr++; }}
      if ( $ierr ) { &mvstr(16,0,
                     "there are not so many internal orbs"); $x=-1; }
    }
 until ($x > -1) ;

 &mvstr($mrow++,0,pack("A15A6A6A6A6A6A6A6A6","ref. docc.  ",@refdoc));
#============================ auxiliary internal orb. =====================
 do { $x = &getvector($qrow,"auxiliary internal orbitals per irrep",$nsym,"");
      @ciaux=split(/:/,$x);
      $ierr=0;
      for ( $i=0; $i<$nsym ; $i++  )
       {if ( $refdoc[$i]+$ciaux[$i] >  $int[$i] ) { $ierr++; }}
      if ( $ierr ) { &mvstr(16,0,
                     "there are not so many internal orbs"); $x=-1; }
    }
 until ($x > -1) ;

  if (&countorbs(join(':',@ciaux)))
  { $auxel = &getnumber($qrow,"Max. number of electrons in ci aux. (ref.CSF,only)",1,1,$ncorel);
 &mvstr(2,0,
  pack("A13A2A11A3A13A3A10A3","Multiplicity:",$mult,"#electrons:",$nel,
             "Molec. symm.:",$labels[$ssym-1]),"Aux. el.: ",$auxel);
 &clearline($qrow);
     }
  else { $auxel=0;}
#========================================================================

 if ( $auxel ) { for ( $i=0; $i<$nsym; $i++)
                 {$ciact[$i]=$int[$i]-$refdoc[$i]-$ciaux[$i];}
               }
    else       { for ( $i=0; $i<$nsym; $i++)
                 {$ciact[$i]=$int[$i]-$refdoc[$i];}
               }

 for ( $i=0; $i<$nsym; $i++)
  {$ext[$i]=$nmpsy[$i]-$int[$i]-$fc[$i]-$fv[$i];}


 &mvstr($mrow++,0,pack("A15A6A6A6A6A6A6A6A6","ci active   ",@ciact));
 &mvstr($mrow++,0,pack("A15A6A6A6A6A6A6A6A6","ci auxiliary ",@ciaux));
 &mvstr($mrow  ,0,pack("A15A6A6A6A6A6A6A6A6","external    ",@ext  ));

 for ( $i=0; $i<$nsym; $i++)
  { if ( $ciact[$i] < 0 )
     { &mvstr(15,0,"#int < #ciact + #refdoc + #ciaux"); $x=0;
       &pause("repeat complete input .. press return to exit"); die;}
    if ( $ext[$i] < 0 )
     { &mvstr($mrow+1,0,"#ext < 0 - not enough basis functions" ); 
       &mvstr($mrow+2,0,"repeat complete input, enter return to cont." ); 
       &pause(" "); die;}
  }


 if ( ($nel -$auxel - &countrefel(join(':',@refdoc)) - &countrefel(join(':',@fc))) < 0 )
   { &mvstr(15,0,
       "too few electrons for so many doubly occ orbitals"); $x=0;
    &pause("repeat complete input .. press return to exit"); die;}


#=========================== exc level, refsym ,gensp ===================

 $mrow++;
 $mrow++;

 $exc = &getnumber($qrow,"Enter the excitation level (0,1,2)",1,0,2);
 &mvstr($mrow,0,pack("A10A2","exc.level:",$exc));

 if ( $exc == 2 ) 
 {$gensp = &getzeichen($qrow,"Generalized interacting space restrictions [y|n]",
           1); }
  else { $gensp="n";}
 &mvstr($mrow,0,pack("A10A2A10A2","exc.level:",$exc,"gen.space:",$gensp));

 do { $refsym = &getvector($qrow,"Enter the allowed reference symmetries",1,$ssym,"");
      @refsym=split(/:/,$refsym);
      $ierr=0;
      for ( $i=0; $i<=$#refsym ; $i++  )
       {if ( $refsym[$i] > $nsym || $refsym[$i] < 1 ) { $ierr++; }}
      if ( $ierr ) { &mvstr(19,0, "invalid reference symmetry"); $refsym= -1; }
    }
 until ($refsym > -1) ;

 &clearline(19);
 $refsym =~ s/:/ /g;
 &mvstr($mrow,0,pack("A10A2A10A2A18A16",
 "exc.level:",$exc,"gen.space:",$gensp,
 "allowed ref. syms:",$refsym));

  my $groups = &getzeichen($qrow,"Apply additional group restrictions for DRT $idrt [y|n]",1);
  &clearline($qrow);
  if ( grep (/[yY]/,$groups))
   { ($groupdefs[$idrt][0],$groupdefs[$idrt][1]) =
       &grouprestrictions($mrow,$qrow,$cigradient,0,$nel);}

 $row=$qrow+1;
  return ($mult,$nel,$ssym,$auxel);
}




 sub readinfofl
 { local ($ptgrp);
   open ( INFOFL, "infofl.mcfreeze") || open ( INFOFL, "infofl") || die "infofl missing \n";
   $/="\n";
   $ptgrp=<INFOFL>;
   $_=<INFOFL>;
   s/^ *//g;
   @nmpsy = split /\s+/,$_ ;
   $nsym=<INFOFL>;
   chop $nsym;
   $_=<INFOFL>;
   s/^ *//g;
   @labels = split /\s+/,$_ ;
   close INFOFL;
  
   if ( $debug ) { print DEB " nsym=$nsym\n"; }
 }

 sub printarray 
  {
#
# print an array @$locstr  and add the label $description
# print with at most 72 characters per line and separate
# @$locstr by groups of $nel elements 
#
    my ($nel,$locstr,$description) = (@_);
    my (@loclocstr,$i,$j,$tmpold,$tmp,$sep);
    print DEB "groups of $nel elements, $description,  $#{$locstr} \n"; 
    $i=$nel;
    $sep=' ';
    $tmpold='';
     while ($i < $#{$locstr} )
     {@loclocstr=();
      for ( $j =$i-$nel; $j< $i ; $j++)
        { push @loclocstr,$$locstr[$j]; }
      $tmp= join($sep,@loclocstr);
      if ((  length($tmpold) < 2 ) && length($tmp) > 72 ) 
         {print DEB "printarray: group too large:\n";
          print DEB "$tmpold:$tmp\n"; 
          die "printarray: group too large \n"; } 

      if (( length($tmp)+length($tmpold)+length($sep)) > 72 )
          { print  CIDRTKY  " $tmpold$sep\n";
            $tmpold="$tmp";}
      else { if ( length($tmpold)) {$tmpold="$tmpold$sep$tmp";}
                   else            {$tmpold=$tmp;}
           }
      $i+=$nel;}

      @loclocstr=();
      for ( $j = $i-$nel; $j <= $#$locstr; $j++)
        { push @loclocstr,$$locstr[$j]; }
      $tmp= join($sep,@loclocstr);
      if (( length($tmp)+length($tmpold)+length($sep)) > 72 )
          { print  CIDRTKY  " $tmpold$sep\n";
            $tmpold="$tmp"; }
      else {if ( length($tmpold)) {$tmpold="$tmpold$sep$tmp";}  
                       else       {$tmpold=$tmp;}
           }
     print CIDRTKY  " $tmpold  $description \n";
  return;
  } 



#
################################################################################
#
  sub inpcoremem{
#
   @input_data = ();       # Place to put data entered on screen
   @defaults = ();         # Default data
   @protect = ();          # Protected markers
   @required = ();         # Required field markers
   $bell = "\007";         # Ascii bell character
   $row = $col = 0;        # Storage for row/col used by menuutil.pl

#
  $window=initscr();
# Activate left and right markers, both in standout rendition.
#
   &menu_template_prefs("*"," ",1,"*"," ",1);

&menu_load_template_array(split("\n", <<'END_OF_TEMPLATE'));


   Input the available core memory (MB) for the CIDRT calculation:




       core memory [__________]




   Indicate completion by pressing "Return".

END_OF_TEMPLATE

#
   $defaults[0]=1700;
   &menu_display_template(*input_data,*defaults);
   &endwin();
#
   $input_data[0] =~ s/ //g;
   $coremem=$input_data[0];
#
   return ;
  } # end of: sub inpcoremem
#
################################################################################
#
#======================================================================
# call a fortran program , check for successful execution
# and give some short summary of the output file
#======================================================================
#
  sub callcidrt {
   local ($cbus,$errno,$x,$prog,$ndrt);
   $cbus = $ENV{"COLUMBUS"};
   ($prog,$totcsf,$ndrt) = @_;
    
   $x = $prog;
   $errno=0;
   $x =~ s/[\<\>-].*$//;
   system ("$cbus/$prog");
#
   if (grep (/cidrtms/,$prog)) 
       { open FIN,"<cidrtmsls" || die ("callcidrt: could not find cidrtmsls");
         @tmp=();
         while (<FIN>) { if (/ total: /) {chop;push @tmp,$_}; ;}
         close FIN;
         my ($j,$h,$poppy); 
         if ($#tmp>2*$ndrt) {$poppy=1; } else {$poppy=0;}
         $$totcsf=" ";  
         print DEB "tmp=@tmp \n";
         for ($j=1; $j<=$ndrt;$j++) {$h=pop @tmp; if ($poppy) { pop @tmp;}  print DEB "h=$h\n"; $h=~s/^ *total: *//; $$totcsf=$$totcsf . "DRT$j: $h ";}
        }
   else
       { open FIN,"<cidrtls" || die ("callcidrt: could not find cidrtls");
         while (<FIN>) { if (/ total: /) {print ; chop;$$totcsf=$_; last;}}
         close FIN;
         $$totcsf=~s/^ *total: *//;
        }
   if (!-s "bummer"){$errno = $errno + 256 ;}
    open (BUMMER, "bummer");
     while ( <BUMMER> )
      {
       if ( ! /normal/) {$errno = $errno + 256 ;}
      }
    close (BUMMER);
   unlink ("bummer");
   return $errno;
   }
#
#========================================================================
#
   sub addciudg
{ 
   local ($dum);
   open (ADD,"ciudgin_add");
   @add=<ADD>;
   foreach (@add) {s/^ *//;chomp;}
   close ADD;
   $add[$#add+1]="\/&end\n";
#
   for ($i=0; $i<=$#add; $i++ )
    { ($key[$i],$dum)=split(/=/,$add[$i],2); }
#  
   open (NOLD,"ciudgin");
   open (NNEW,">ciudgin_new");
#
   while (<NOLD>)
    {
      $inlist=0;
      for ($i=0; $i<=$#add; $i++ )
       { if (/$key[$i]/i)  {$inlist=1;} }  
      if ($inlist == 0) {print NNEW "$_";}
    }
  for ($i=0; $i<=$#add; $i++ )
    { print NNEW" $add[$i]\n"; }
 close NOLD; 
 close NNEW;
 mv ("ciudgin_new","ciudgin");
}

  sub pscript 
   { my($nvmax,$ncpu,$maxmem,$bandwidth,$ondisk,$ncnode)=@_;
#################################### INPUT parameters for partition2 ####################
# printlevel (-print)                                  <number>
# pre-optimization step
#     minimum block size (-skipsize)                   <number>
#     minimum block geometry (-skipgeom)               <dim1,dim2>
#     thresholding           (-skipthresh)             <number>
# maximum time per task   (-time)                      <time in seconds>
# minimum number of z,y,x,w segments (-seg)            <zseg,yseg,xseg,wseg>
# relative reduction size z,y,x,w (-red)               <zred,yred,xred,wred>
# threshold   (-skipthresh)                            <number>
# task class (-class)                                  <1x|3x|2x_wx|2x|0x|4x|all>
# maximum local memory usage (-maxmem)                 <MB>
# effective bandwidth (-bw)                            <MB/s>
# maximum subspace dimension (-nvmax)                  <number>
# number of CPUs             (-ncpu)                   <number>
# number of cores per node   (-ncnode)                 <number>
# take filesizes from file   (-fsizes)                 <filename>
# take runtime info from file (-tmodel)                <filename>
# presegmentation             (-preseg)                <on|off>
###########################################################################################
    my ($P,$reftime,$waltime,$minz0,$minx,$icnt,$x);
    $P=$ENV{"COLUMBUS"}."/perlscripts/partition2.pl";
     if ($ncnode < 1) {print "cidrtms.pl: sub pscript got incorrect argument ncnode=$ncnode \n";
                       print "cidrtms.pl:  resetting ncnode to 1\n"; $ncnode=1; }
     $reftime=1000;
     $walltime=0;
     if ($ncpu < 4 ) { $minz0=int(sqrt($ncpu));
                       $minx=1; }
     else { $minz0=int(sqrt($ncpu));
            $minx=int(0.5*sqrt($ncpu)+1); }
     $icnt=0;
     $ondisk=0;
     print DEB "pscript got: nvmax=$nvmax, ncpu=$ncpu, maxmem=$maxmem bandwidth=$bandwidth  ondisk=$ondisk ncnode=$ncnode \n";
     if ($ondisk) { open FAKE,"<tmodells.fake";
                    my @tmpfake=();
                    while (<FAKE>) { push @tmpfake, $_; if (/Filesizes:/) {last;}}
                    $tmp=<FAKE>; $tmp = "fil4x:       10 Bytes (  0.00 MB) \n"; push @tmpfake, $tmp; 
                    $tmp=<FAKE>; $tmp = "fil3x:       10 Bytes (  0.00 MB) \n"; push @tmpfake, $tmp; 
                    $tmp=<FAKE>; $tmp = "fil4w:       10 Bytes (  0.00 MB) \n"; push @tmpfake, $tmp; 
                    $tmp=<FAKE>; $tmp = "fil3w:       10 Bytes (  0.00 MB) \n"; push @tmpfake, $tmp; 
                    while (<FAKE>) { push @tmpfake, $_;}
                    close FAKE;
                    open FAKE,">tmodells.fake";
                    print FAKE @tmpfake;
                    close FAKE; }
     pprint_nl("  setting up prelimniary segmentation .... \n",2);
     pprint_nl("  ncpu    task     coremem        totvol       tcomm     bandwidth   \n",1);
         
     while (1)
      {  $outf="partitioned_${ncpu}cpu_${bandwidth}bw_${maxmem}mb.dat";
         $xreftime=$reftime/$ncpu;
         pprint_nl();
         $pcommand="$P -time $xreftime -ncpu $ncpu -bw $bandwidth -preseg off -tmodel tmodells.fake -nvmax $nvmax -maxmem $maxmem -class all -red 2,2,1.8,2.0 -seg 1,1,$minx,$minx  -minz0 $minz0 -ncnode $ncnode > $outf";
         print DEB "pscript command:\n$pcommand\n";
            
         $errno=system($pcommand); 
         if ($errno)
         {
            pprint_nl();
            print DEB "... failed\n";
            pause("\n\n partitioning failed see DEBUG\n\n");
            die("pscript failed \n");
         }
         my $saveit=$/;
         undef $/;
         open FIN,"<$outf";
         $x=<FIN>;
         $x=~s/.*SORTED TASK//s;
         $x=~m/^ total number of tasks: *([0-9].*)/m;
         $tottask=$1;
         $x=~m/^ total communication data volume: *([0-9].*\.[0-9]*)[ a-zA-Z():]*([0-9]*\.[0-9]*)/m;
         $tcomm=$2; $totvol=$1;
         $x=~m/^ core memory setting .*-m *([0-9]*)/m;
         $coremem=$1; 
         $x=~m/^ total cost *([0-9]*\.[0-9]) [^0-9]*([0-9]*\.[0-9])/m;
         $totcost=$1; $walltime=$2;
         $x=~m/^ maxseg=([0-9]*)/m;
         $maxseg=$1;
         $x=~m/^ largest task.*: *([0-9.]*)/m;
         $maxtasktime=$1;
         close FIN;
         $/=$saveit;
         $tmp=sprintf "%4d    %6d  %10d %10.2f GB    %8.2f s   %4.1f MB/s \n",
          $ncpu, $tottask,$coremem,$totvol,$tcomm,$bandwidth;
         if ($debug2) { pprint_nl($tmp,1); }
          $oldref=$reftime;
         $reftime=$walltime*$ncpu*0.88;
          if (!($oldref/$ncpu > 0.92*$walltime || $oldref/$ncpu < 0.7*$walltime )) {last;}
         $icnt++; if ($icnt > 5) {last;}
      }
          pprint_nl("$tmp",2); 
          pprint_nl(" pciudg (runc) must be called with at least -m $coremem \n",1);

        pause("\n\n partitioning finished, press return to continue \n\n");

#  additional checks : oversegmentation, memory contention etc. 
#  partition2.pl must also handle integral file location on disk
#  add to ciudgin 

      open FIN,"<ciudgin";
      open FOUT,">ciudgin._${ncpu}cpu_${bandwidth}bw_${maxmem}mb";
         while (<FIN>) { if (/end *$/) {last;} if ( ! grep(/nseg|maxseg/i,$_) ) {print FOUT;}} 
       close FIN;
      open FIN,"<$outf";
         while (<FIN>) { if (/add to ciudgin /) {last;}}
         while (<FIN>) { if ( grep(/ nseg|maxseg/i,$_) ) {print FOUT;}}
      close FIN;
      print FOUT  " &end\n";
      close FOUT;
      system("mv -f ciudgin._${ncpu}cpu_${bandwidth}bw_${maxmem}mb ciudgin");
      system("mv TASKLIST.def TASKLIST");
     return;
   }

  
     sub prepare_ciinput {
      my ($callprog,$ndrt,$ciprogram,$preppar);
      ($callprog,$ndrt,$ciprogram,$preppar)=@_;
      $cbus = $ENV{"COLUMBUS"};
      print DEB " prepare_ciinput: $callprog,$ndrt,$ciprogram\n";

       if ($callprog eq "cimult")  { for ($idrt=1; $idrt<=$ndrt; $idrt++) 
                                     { system("cp ciudgin.drt$idrt ciudgin");
                                       system("cp cidrtfl.$idrt cidrtfl");
                                       print DEB "cimult: drt $idrt\n";
                                       prepare_ciinput2(" ",$ciprogram);
                                       system("mv -f ciudgin ciudgin.drt$idrt");
                                       if ( -f "TASKLIST") { system("mv -f TASKLIST TASKLIST.drt$idrt"); }
                                     }
                                    }
       else
       {
           prepare_ciinput2(" ",$ciprogram); 
       }
            # now we have ciudgin and TASKLIST 
       pause("press return to continue");

       return;
      }

      sub prepare_ciinput2{
      my ($idrt,$ciprogram)=@_;
      $cbus = $ENV{"COLUMBUS"};
      $errno=system ("$cbus/makpciudg.x -m $coremem");
      open FIN,"<bummer" || die("makpciudg.x failed\n");
       my $message=<FIN>;
      if ( ! grep(/normal/,$message)) {&pause("\n makpciudg.x failed: $message \n"); die("makpciudg.x failed: $message \n");}
      close FIN;
      open FIN,"<makpciudgls";
      while (<FIN>) {if (/Integral filesizes in bytes/) {last;}}
      pprint_nl("makpciudg.x: DRT $idrt  $_",1);
      $_=<FIN>;
      pprint_nl("$_",1);
      $_=<FIN>;
      pprint_nl("$_",1);
      if ($ciprogram eq "pciudg") { 
                                   &pprint_nl(" Executing cimkseg.x, this may take several minutes, please wait...",1);
                                   &pprint_nl("  progress can be monitored in file cimkseg.info ...",1);
                                   $errno=system ("$cbus/cimkseg.x -m $coremem 2> cimkseg.info ");
                                  if ($errno) {&pause("\n cimkseg.x failed \n"); die("cimkseg.x failed \n"); }
                                   &pscript($nvmax,$ncpu,$maxmem,$bandwidth,$ondisk,$ncnode); 
                                   &pause ("press return to continue");
                                  }
      return;
      } 
