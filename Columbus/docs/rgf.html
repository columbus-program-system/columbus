<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<HTML>
<HEAD>
  <META name="generator" content="HTML Tidy for Linux (vers 11 February 2007), see www.w3.org">

  <TITLE>Stationary Point Search</TITLE>
  <LINK rel="STYLESHEET" type="text/css" href="style.css">
</HEAD>

<BODY bgcolor="#CCFFCC">
  <H2>Stationary Point Search<BR>
  Reduced Gradient Following (RGF)</H2>

  <P><B>Program name</B>: <TT>rgf.x</TT><BR>
  <B>Input file name</B>: <TT>param.rgf, rgfin</TT><BR>
  <B>Output file name</B>: <TT>message.rgf, molden.all</TT><BR></P>

  <P>To see the full <TT>rgf</TT> input documentation click <A href="fulldoc/rgf.txt">here</A>.</P>

  <P>The RGF (reduced gradient following) technique of Quapp et al. (1998) has been implemented into the COLUMBUS program. For more information see the <A href="methods.html">Methods</A> section and the original reference.</P>

  <H3>Input generation</H3>

  <P>The RGF calculation is much more complex than the geometry optimizations. It is important to notice, that the RGF procedure requires a proper Hessian matrix in each optimization step and that the signature of the Hessian can change along the RGF curve. Several possibilities - a) recalculation of the Hessian in each optimization step using a quantum chemical method, b) Bofill update (Bofill (1994)) - and various combinations have been tested for the eveluation of the Hessian matrix (Dallos et al. (2002)). In the following input description we will discuss the general case of a combined method.</P>

  <P>As starting point, one has to set up a complete input for a force constant calculation in the main directory. It can be set up in C<SUB>1</SUB> symmetry or in the higher symmetry in which the RGF search is to be performed. As explained in the section for <A href="freq.html">force constants</A> in more detail, the following steps have to be executed:</P>

  <UL>
    <LI>
      <P>gradient input with <TT>colinp</TT></P>
    </LI>

    <LI>
      <P>force constant input with <TT>colinp</TT></P>
    </LI>

    <LI>
      <P>copy the input files using the copy feature of <TT>dispcalc.pl</TT></P>
    </LI>
  </UL>

  <P>Since the input files for the force constant calculation have been copied to appropriate subdirectories (<TT>DISPLACEMENT/...</TT>), the main directory is free for another input to be used for the gradient calculation in the RGF search. In this way the two computational steps can be decoupled. After these inputs have been made, go to the following submenu in <TT>colinp</TT>:</P>
  <PRE>
<CODE>
                  main menu options

     1) Integral program input (for argos/dalton/turbocol)
     2) SCF input
     3) MCSCF input
     4) CI input
-&gt;   5) Set up job control
     6) Exit the input facility 

-------------------------------------------------

        submenu 1: type of calculation 

-&gt;   1) Job control for single point or gradient calculation
     2) Generate int. coordinates for potential energy curve
     3) Potential energy curve for one int. coordinate
     4) Vibrational frequencies and force constants
     5) Exit   

-------------------------------------------------

     submenu 1.1: job control setup

     1) single-point calculation
     2) geometry optimization
     3) saddle point calculation (local search - GDIIS)
-&gt;   4) stationary point calculation (global search - RGF)
     5) Exit  


-------------------------------------------------
</CODE>
</PRE>

  <P>After entering a suitable number of iterations (should be much larger than in case of geometry optimization), the gradient method to be used (MCSCF or CI/AQCC) has to be specidfied. Note, that at this point the selection need not agree with the previously defined gradient method used to calculate the hessian matrix (for example: you can calculate the Hessian matrix at the MCSCF level only and perform the RGF search on CI level). Afterwards, a new menu point will appear:</P>
  <PRE>
<CODE>
       RGF parameter adjustment:


        System with N internal coordinates detected!

     Stopping Criterion      [0.08 ]
     Predictor Corrector Criterion [0.001]
     Length of Predictor steps [0.1 ]
     Minimal Number of Steps [5 ]
     Maximal Number of Steps [150]

      Direction of search [1/-1]
         Int.coord. 1:   direction [0 ]
         Int.coord. 2:   direction [0 ]
            :
            :
            :
         Int.coord. N:   direction [0 ]
</CODE>
</PRE>

  <P>Here you have to specify the RGF search direction and the search parameter. The search direction is defined in terms of the internal coordinates, which have been generated automatically and written into the <TT>intcfl</TT> file. A linear combination of internal coordinates may be specified as search direction. The meaning of the other parameters can be found in the <TT><A href="fulldoc/rgf.txt">rgf.txt</A></TT> file. Next, the desired Hessian matrix recalculation/update procedure has to be specified:</P>
  <PRE>
<CODE>         Hessian matrix calculations mode:

   [n] start form diagonal force constat matrix and UPDATE by rgf
   [n] calculated after every PREDICTOR step otherwise UPDATE
   [n] calculated after every PREDICTOR step otherwise CONSTANT   
   [n] UPDATE by the rgf program
   [n] calculated EXACTLY at every iteration

   [n] calculated at EVERY N iterations otherwise UPDATE   
   [n] calculated at EVERY N iterations otherwise CONSTANTT
                 N=  [0]

                            FINISHED [ ]      
</CODE>
</PRE>

  <P>Now you are ready for the calculation!</P>

  <P><B>Note:</B> To fix coordinates in RGF searches the following two steps have to be made</P>

  <UL>
    <LI>Include <TT>fixc</TT> in the <A href="freq.html#fixc">force constant calculation</A>.</LI>

    <LI>Insert <TT>fixc = list of coordinates</TT> into the file <TT>cart2intin</TT></LI>
  </UL>

  <H3>Running the calculation</H3>

  <P>The RGF stationary point calculation is by the <TT>runc</TT> script.</P>

  <H3>Output analysis</H3>

  <P>Output analysis is performed mainly using the <TT>MOLDEN</TT> graphical program interface. The RGF procedure generates a MOLDEN input file, <TT>molden.all</TT> where all the necessary informations of the search can be displayed. A typical MOLDEN output is displayed on the following picture:<BR>
  <BR></P>

  <TABLE border="0">
    <TR>
      <TD><IMG src="./rgf.jpg" width="718" height="463" border="0"></TD>
    </TR>
  </TABLE>

  <P><BR></P>

  <P>On the left upper picture part we see the plot of the energy during the RGF search. We can clearly recognize two types of energy changes, larger ones, corresponding to the predictor steps, and smaller ones, corresponding to the corrector steps. This interpretation may be verified on the right picture part, where the actual step size (in absolute value) is displayed. Note that due to the inflexibility of MOLDEN "aver. step" is used to label the step size in the search direction and <B>not</B> the average stepsize! The lowest part of the figure displays the norms of the gradient in the search direction (label "aver. step") and of the maximal force. We see a typical shape, where the gradient values first increase, passes a maximum and decreases towards the stationary point. On this picture we see clearly, that the RGF procedure connects two stationary point. The RGF procedure locates the stationary point only approximatively, so a further optimization using the <A href="geometry_opt.html"><TT>gdiis</TT></A> procedure is necessary.<BR></P>
</BODY>
</HTML>
