<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<HTML>
<HEAD>
  <META name="generator" content="HTML Tidy for Linux (vers 11 February 2007), see www.w3.org">

  <TITLE>Hessians</TITLE>
  <LINK rel="STYLESHEET" type="text/css" href="style.css">
</HEAD>

<BODY link="#0000FF" vlink="#800080" bgcolor="#CCFFCC">
  <H2>Hessians, Dipole Moment Derivatives<BR>
  and Harmonic Vibrational Frequencies</H2>

  <P>Harmonic force constants are computed via finite differences from analytic gradients in terms of internal coordinates. The procedure is semi-automatic since inputs for displaced geometries have to be created.<BR>
  The dipole moment derivatives are calculated as finite differences also. In both cases a complete input set for gradient calculation has to be present in the main directory. In case dipole moment derivatives are computed, input files for dipole moment calculation have to be present.</P>

  <H3>Input generation</H3>

  <P>Before starting with the specific menu point in colinp for Harmonic force constants and dipole moment derivatives, a complete gradient input set has to be specified using the previous <TT>colinp</TT> menu points. You may choose MCSCF or CI/AQCC gradient.</P>

  <P>Symmetry considerations: displaced molecular geometries created from non-totally symmetric coordinates will have lower symmetry than the original reference point of the optimized structure. Thus, new inputs matching the actual symmetry would have to be created for each case individually for optimal performance. The most convenient way is to ignore symmetry at all and create inputs in C<SUB>1</SUB> symmetry. For complete symmetry treatment <A href="#full%20symmetry">see below</A>.</P>

  <P><A name="symmetry" id="symmetry">Experience</A> showed that calculations without using symmetry can give problems in the MCSCF for the construction of starting orbitals (e.g. from a SCF calculation). Moreover, one has to take care that the energy and the wavefunction show a continuous behavior on antisymmetric geometrical distortions (<A href="colinp_input.html#symmetry">see reference symmetries</A>). Therefore, the following scheme for the combination of geometry optimization and force constant calculation is recommended:</P>

  <H4>Hessian of the energy</H4>

  <OL>
    <LI>
      <P>optimize the geometry using a) symmetry, and in case of CI calculations b) interacting space restriction and c) one reference symmetry. This is the fastest alternative.</P>
    </LI>

    <LI>
      <P>use symmetry, but no interacting space and all possible reference symmetries in the next step. The <TT>mocoef</TT> file from step 1) can be used. This calculation will give identical results to a calculation without symmetry and interacting space restriction. Optimize the molecule again starting from the geometry of step 1)</P>
    </LI>

    <LI>
      <P>calculation without symmetry. Use the program <TT><A href="utilities.html#transmo">transmo</A></TT> to transform the mocoef file of step 2) to a <TT>mocoef</TT> file without symmetry. Perform one more geometry optimization step to check that results are identical to step 2).</P>
    </LI>

    <LI>
      <P>copy the input files from the main directory of step 3 into the main directory of the force constant calculation</P>
    </LI>
  </OL>

  <H4>Hessian of the nonadiabatic coupling</H4>

  <P>Prepare the input for a calculation of the nonadiabtic coupling terms in C<SUB>1</SUB> symmetry at the desired geometry.</P>

  <P>Then you are ready to proceed. The following <TT>colinp</TT> menu point has to be entered:</P>
  <PRE>
<CODE>
     1) Job control for single point or gradient calculation
     2) Generate int. coordinates for potential energy curve
     3) Potential energy curve for one int. coordinate
--&gt;  4) Vibrational frequencies and force constants
     5) Exit

------------------------------------------------------------

     submenu 4.1: freq.calculation options

     1) frequency calculation
     2) frequency and dipole moment derivative calculation

------------------------------------------------------------

     submenu 4.1.1/4.1.2: Individual int.coord. displacement adjustment:

   System with N internal coordinates detected!

    Int.coord. :   -disp [-0.001 ]  +disp [0.001  ]
    Int.coord. :   -disp [-0.001 ]  +disp [0.001  ]
      :                                  :
      :                                  :
      :                                  :
    Int.coord. :   -disp [-0.001 ]  +disp [0.001  ]


     calculate negative displacements only [n]

</CODE>
</PRE>

  <P>The last menu point generates a new directory structure:</P>
  <PRE>
<CODE>

     DISPLACEMENT/REFPOINT
     DISPLACEMENT/CALC.c<I>a</I>.d<I>-A</I>
     DISPLACEMENT/CALC.c<I>a</I>.d<I>A</I>
      :          
      :           
      :            
     DISPLACEMENT/CALC.c<I>z</I>.d<I>-Z</I>
     DISPLACEMENT/CALC.c<I>z</I>.d<I>Z</I>

</CODE>
</PRE>

  <P>In the <TT>DISPLACEMENT/REFPOINT</TT> directory is a copy of the unchanged input files for the reference geometry. The notation for the <TT>DISPLACEMENT/CALC.c<I>a</I>.d<I>-A</I></TT> directories is: displacement <TT><I>-A</I></TT> in internal coordinate <TT><I>a</I></TT>. In every directory there will be now a new geometry file corresponding to the desired displacement. Moreover, the control file <TT>DISPLACEMENT/displfl</TT> is generated. Here the informations about points to be calculated are listed. It is also possible to calculate negative displacements only (reduces the computational effort by cca. 50%). In this case all positive displacements are omitted, and the numerical differentiation is performed relative to the reference point.</P>

<!-- fp: disp.pl is no longer needed
  <P><A name="fixc" id="fixc"></A> If only a subset of the forceconstant matrix is to be calculated, respective displacements have to be deactivated. This is done by adding the key word <TT>fixc</TT> at the end of the corresponding line in the file <TT>DISPLACEMENT/displfl</TT>.</P>

  <P>Next steps are performed using the script: <A name="full symmetry"><TT>disp.pl</TT>. After calling the script <TT>disp.pl</TT> you will see on the screen:</A></P>
  <PRE>
<CODE>
        menu 1: COLUMBUS calculations based on the displfl file

     1) copy input files for force constant calculation
     2) copy input files for potential curve calculation
     3) perform force constant calculations (no CI restart)
     4) perform force constant calculations (using CI restart)
     5) perform potential curve calculations
     6) quit                                       
</CODE>
</PRE>

  <P>The first point will copy all input files from the main directory into the subdirectories <TT>DISPLACEMENT/CALC.c<I>a</I>.d<I>A</I></TT> (as listed in the file <TT>DISPLACEMENT/displfl</TT>). Thus, in these subdirectories complete inputs for individual gradient calculations for displaced geometries are located, which will be executed, as explained below. Instead of copying these same input files (usually for C<SUB>1</SUB> symmetry) into the subdirectories, specific input files for the calculation can be generated individually, in order to take into account symmetry and to achieve maximum performance. An input for geometry optimization has to be created with only one geometry optimization iteration. In the CI case the interacting space option should be disabled and all reference symmetries specified .</P>
-->

  <H3>Executing the calculation</H3>

  <P>There are three options for executing the calculation
  <ul>
  <li>
  An <b>interactive</b> calculation is started using the point 2 in the <TT>disp.pl</TT> script. The calculation is started in background with the <TT>nohup</TT> option and the points are calculated one by one. After one point is finished, the directories <TT>WORK</TT> and <TT>RESTART</TT> are removed. In this way the whole calculation requires only as much disk space as a single calculation.</li>
  <li>
  A <b>batch</b> calculation can be done by using the <TT>calc.pl</TT> script.
  </li>
  <li>
  For a trivial <b>parallel</b> run, you may simply execute the <TT>runc</TT> script in every subdirectory. When using a queueing system you may do this for example by submitting a queue script in every directory or (preferably) by submitting an array job.
  </li>
  </ul></P>

  <H3>Output analysis</H3>

  <P>After finishing the calculation using the script <TT>disp.pl</TT> or <TT>calc.pl</TT> we get in all displacement directories the corresponding gradients (nonadiabatic coupling terms) in internal coordinates (if required also dipole moment values). These data are collected automatically by the script <TT>forceconst.pl</TT>, which performs also the numerical differentiation. This program generates the file <TT>LISTINGS/forceconstls</TT>, which contains the basic informations about the performed calculations (calculation type, energy, convergence, number of performed iterations) and the calculated Hessian(s). The script generated also a file <TT>hessian</TT> in case of the Hessian of the energy and the files <TT>hessian_st1</TT>, <TT>hessian_st2</TT> and <TT>hessian_nad</TT> in case of the Hessian of nonadiabatic couplings. These files are located in the main directory and may be used for geometry optimizations.</P>

  <P>It is recommended to check the following informations listed in the <TT>LISTINGS/forceconstls</TT> file:</P>

  <OL>
    <LI><B>convergence</B> of all the performed calculations</LI>

    <LI><B>number of MCSCF iterations</B> at every point (if the number of interations is much higher for some points than for the rest, then the character of the wave-function might have changed in this points and the ansatz for the wavefunction should be reconsidered.</LI>

    <LI><B>number of CI/AQCC iterations</B>. Especially when root following is used a sudden increase of the number of iterations may be the result of a calculation, where e.g. the root following did not work correctly. In this case the calculation has to be reconsidered (check the reference space, NVCIMN and NVCIMX values, ...).</LI>

    <LI><B>energy differences</B> relative to the reference point for all calculations. In case of shallow potential surfaces the size of the displacement has to be reconsidered (increased).</LI>

    <LI><B>non-symmetricity in the force constant matrix</B>. The harmonic force constant matrix should be symmetric. If large non-symmetric terms occurs, reconsider the displacement values or in the worst case the whole calculation.</LI>
  </OL>

  <P>Afterwards, the <TT>forceconst.pl</TT> script calculates the harmonic force constant matrix and calls the program <TT>suscal.x</TT> (Pulay et al. (1983) and Pongor et al. (1992)) for calculation of harmonic vibrational frequencies (and IR intensities, if required) automatically. A <TT>MOLDEN</TT> input file is generated also. The <TT>suscal.x</TT> results are found in file <TT>LISTINGS/suscalls</TT>. The MOLDEN input is found in the file: <TT>MOLDEN/molden.freq</TT>. This file allows the visualization of vibrational modes.<BR></P>
</BODY>
</HTML>
