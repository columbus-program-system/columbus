\documentclass[DIV=12,headings=normal]{scrartcl}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[UKenglish]{babel}

\usepackage[intlimits]{amsmath}
\usepackage{amssymb}
\usepackage{icomma}
\usepackage{braket}
\usepackage{color}
\usepackage{listings}

\usepackage{graphicx}
%\usepackage{subfig}
\usepackage{booktabs}
\usepackage{pdflscape}
\usepackage{subfigure}

\usepackage{tikz}
% \usetikzlibrary{decorations}
% \usetikzlibrary{decorations.pathmorphing}
% \usetikzlibrary{plotmarks}
% \usetikzlibrary{patterns}
\usepackage{pgfplots}
\pgfplotsset{compat=1.3}
\usepgfplotslibrary{groupplots}

\usepackage{siunitx}
\sisetup{decimalsymbol=comma}
\usepackage{multirow}
\usepackage{url}
\usepackage{fancyhdr}

\usepackage[version=3,arrows=pgf]{mhchem}

\usepackage{lmodern}
\usepackage{fancyvrb}

\setkomafont{subject}{\usekomafont{title}}
\setkomafont{caption}{\small}
% \captionsetup[subfloat]{font={default,small}}

\newsavebox\MBox
\newcommand\Cline[2][red]{{\sbox\MBox{#2}%
  \rlap{\usebox\MBox}\color{#1}\rule[-1.2\dp\MBox]{\wd\MBox}{0.5pt}}}

\newcommand{\comment}[1]{\textcolor{blue}{#1}}
\newcommand{\red}[1]{\textcolor{red}{#1}}
\newcommand{\redl}[1]{\Cline{\textcolor{red}{#1}}}

\let\origttfamily=\ttfamily % alte Definition von \ttfamily sichern
\renewcommand{\ttfamily}{\origttfamily \hyphenchar\font=`\-}

\newcommand{\greybox}[1]{
  \vspace{3mm}
  \fcolorbox{black}{black!15}{
    \begin{minipage}{0.9\textwidth}\textit{#1}\end{minipage}}
  \vspace{3mm}
}

\newcommand{\todo}[1]{\textcolor{red}{#1}}
\newcommand{\col}{\textsc{Columbus}}
\newcommand{\molcas}{\textsc{Molcas}}
\newcommand{\seward}{\texttt{Seward}}

\newcounter{number}
\newcommand{\numbering}[1]{\thenumber. #1\addtocounter{number}{1}}
\newcommand{\renumber}{\setcounter{number}{1}}

\pgfplotsset{every tick label/.append style={/pgf/number format/use comma,/pgf/number format/1000 sep={}}}
\pgfplotsset{scaled ticks=false}
\pgfplotsset{small,width=0.6\textwidth,max space between ticks=35}
\pgfplotsset{tick style={thin,black}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\fancyhf{}				%Leeren aller Header, Footer
\fancyhead{}		%Links
\fancyfoot[L]{\textsc{Columbus} SO-CI tutorial}
\fancyfoot[R]{\thepage}			%Seite x von y
\renewcommand{\headrulewidth}{0pt}	%Linie unter Header

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subject{\Large A Tutorial for Columbus}
\title{\LARGE SO-CI including gradients, dipole moments and wavefunction overlaps}
\author{\large Sebastian~Mai, Felix~Plasser}
\date{\large Vienna, 2013\\[1em] Loughborough, 2023}
%\publishers{\small \textit{Institute for Theoretical Chemistry -- University of Vienna}\\[6em]www.univie.ac.at/columbus/tutorial\_soci}

\begin{document}

\pagestyle{fancy}
\selectlanguage{UKenglish}

\maketitle
\cleardoublepage

\tableofcontents
\clearpage

\section{Before Starting}

\subsection{Introduction}

This tutorial will give you a short introduction to the calculation of spin-orbit couplings (SOC) with \textsc{Columbus}. The focus will be lead on perturbative SOC with respect to non-relativistic wavefunctions, computed in connection with \textsc{Molcas}. This methodology will be combined with the simultaneous calculation of gradients for the non-relativistic wavefunctions, transition dipole moments and wavefunction overlaps via \texttt{cioverlap}.

\subsection{Notation}

The same notation as in the standard and parallel tutorials will be used.

\scriptsize
\begin{Verbatim}[commandchars=\\\{\}]
This kind of font indiates what is seen on the screen
\redl{and the command lines that you should write <ENTER>}  \comment{! Comments come here}
\end{Verbatim}
\normalsize

\greybox{Important information related to Columbus but not necessarily connected to the current job comes in boxes like this.}

\section{Manual Input of Basis Sets for Use with Columbus and Seward Integrals}\label{sec:BSinput}

In order to calculate relativistic effects, one needs the appropriate integrals. By default, relativistic integrals in \col\ are delivered by \textsc{Argos}. Unfortunately, this program package can not also provide integral derivatives for gradient calculations. Another possibility to obtain the necessary integrals is the usage of \molcas' integral program \seward.

Relativistic calculations need basis sets with appropriate contractions to generate meaningful results. The basis sets used for non-relativistic calculations usually do not suffice for this task. The easiest way to access non-standard basis sets in \col\ is by adding them to the \molcas\ library. 

\setcounter{number}{1}
\numbering{Create the file with the orbital exponents and the contraction scheme as given in the example below. Basis sets can be obtained e.g. from \url{bse.pnl.gov/bse/portal}. Because the rest of our tutorial uses \ce{SO2} as example system, we show the content of our basis set file for elements O and S:}

\scriptsize
\begin{Verbatim}[commandchars=\\\{\}]
* My Basis Set Datafile
cc-pVDZ-DK
/O.cc-pVDZ-DK.Dunning.9s4p1d.3s2p1d.. 
T.H. Dunning, Jr. J. Chem. Phys. 90, 1007 (1989).
OXYGEN       (9s,4p,1d) -> [3s,2p,1d]
Options
OrbitalEnergies
EndOptions
          8.   2
* S-type functions
    9    3
   11720.00000000
    1759.00000000
     400.80000000
     113.70000000
      37.03000000
      13.27000000
       5.02500000
       1.01300000
       0.30230000
      0.0009775             -0.0002206              0.0000000        
      0.0058106             -0.0013435              0.0000000        
      0.0283649             -0.0064010              0.0000000        
      0.1054311             -0.0259275              0.0000000        
      0.2834470             -0.0711286              0.0000000        
      0.4482844             -0.1656153              0.0000000        
      0.2702944             -0.1160807              0.0000000        
      0.0154031              0.5585241              0.0000000        
     -0.0025739              0.5713981              1.0000000 
 2
 -20.66866  -1.24433
* P-type functions
    4    2
      17.70000000
       3.85400000
       1.04600000
       0.27530000
      0.0431553              0.0000000        
      0.2289863              0.0000000        
      0.5086083              0.0000000        
      0.4605682              1.0000000 
 1
 -0.63192
* D-type functions
    1    1
       1.18500000
  1.00000000
 0
/S.cc-pVDZ-DK.Dunning.12s8p1d.4s3p1d..
D.E. Woon and T.H. Dunning, Jr.  J. Chem. Phys. 98, 1358 (1993).
SULFUR       (12s,8p,1d) -> [4s,3p,1d]
Options
OrbitalEnergies
EndOptions
         16.   2
* S-type functions
   12    4
  110800.00000000
   16610.00000000
    3781.00000000
    1071.00000000
     349.80000000
     126.30000000
      49.26000000
      20.16000000
       5.72000000
       2.18200000
       0.43270000
       0.15700000
      0.0007215             -0.0002010              0.0000583              0.0000000        
      0.0028720             -0.0007969              0.0002317              0.0000000        
      0.0115194             -0.0032599              0.0009442              0.0000000        
      0.0424617             -0.0119806              0.0035003              0.0000000        
      0.1311104             -0.0400080              0.0116247              0.0000000        
      0.3046040             -0.1005679              0.0299867              0.0000000        
      0.4193121             -0.2001709              0.0600636              0.0000000        
      0.2282718             -0.1188477              0.0399813              0.0000000        
      0.0176436              0.5185418             -0.2109019              0.0000000        
     -0.0029299              0.6009686             -0.3885008              0.0000000        
      0.0008367              0.0386850              0.6389962              0.0000000        
     -0.0003625             -0.0092249              0.5505990              1.0000000   
 3
 -92.00447  -9.00431  -0.87955
* P-type functions
    8    3
     399.70000000
      94.19000000
      29.75000000
      10.77000000
       4.11900000
       1.62500000
       0.47260000
       0.14070000
      0.0047937             -0.0012442              0.0000000        
      0.0347928             -0.0088187              0.0000000        
      0.1451669             -0.0393306              0.0000000        
      0.3542203             -0.0934934              0.0000000        
      0.4584235             -0.1477061              0.0000000        
      0.2058117              0.0308319              0.0000000        
      0.0102234              0.5613096              0.0000000        
     -0.0000639              0.5347270              1.0000000  
 2
 -6.68253  -0.43737
* D-type functions
    1    1
       0.47900000
  1.00000000
 0
\end{Verbatim}
\normalsize

\numbering{Name this file \texttt{CC-PVDZ-DK} and copy it to the \molcas\ basis set library:}

\scriptsize
\begin{Verbatim}[commandchars=\\\{\}]
\redl{cp CC-PVDZ-DK $MOLCAS/basis_library/}
\end{Verbatim}
\normalsize

\numbering{This will allow \molcas\ to find the cc-pVDZ-DK basis set, which is appropriate for calculations with Douglas-Kroll integrals.}

Alternatively, the relativistic ANO-RCC basis sets are also included in \seward.

\section{Input for perturbational SO-CI including gradients, transition dipole moments and overlaps}

In this section, we will perform perturbational SO-CI calculations on non-relativistic wavefunctions. This functionality is integrated into \texttt{runc} via the \texttt{socinr} keyword in \texttt{control.run}. We will not use symmetry in the tutorial.

We will use the \ce{SO2} system as an example in this tutorial. The task will be to calculate SOC, gradients, transition dipole moments and overlaps for the four lowest singlet and three lowest triplet states of this molecule.

Prepare a new directory and copy the molecular geometry to the file \texttt{geom}:
\scriptsize
\begin{Verbatim}[commandchars=\\\{\}]
 S     16.    0.00000000    0.00000000    0.00000000   31.97207180
 O      8.    0.00000000   -2.38400000    1.36200000   15.99491464
 O      8.    0.00000000    2.38400000    1.36200000   15.99491464
\end{Verbatim}
\normalsize

Then call the \col\ input facility:
\scriptsize
\begin{Verbatim}[commandchars=\\\{\}]
~> \redl{$COLUMBUS/colinp}
\end{Verbatim}
\normalsize

\subsection{Integral input}

We will calculate the integrals using \seward. 

\renumber
\numbering{Start with the integral input:}

\scriptsize
\begin{Verbatim}[commandchars=\\\{\}]
\red{->    1)} Integral program input (for argos/dalton/turbocol/molcas)
      2) SCF input
      3) MCSCF input
      4) CI input
      5) Set up job control
      6) Utilities
      7) Exit the input facility
\end{Verbatim}
\normalsize

\numbering{Use the \texttt{prepinp} facility:}

\scriptsize
\begin{Verbatim}[commandchars=\\\{\}]
Run the preparation program (prepinp)? (y|n) \redl{y} \comment{! Press <ENTER> after the input}
\end{Verbatim}
\normalsize

\numbering{Enter information about program, symmetry and geometry file. Also enter basis set and choose relativistic integrals.}

\scriptsize
\begin{Verbatim}[commandchars=\\\{\}]
Input for DALTON (1) or MOLCAS (2): \redl{2}
Enter the symmetry generators: 
(e.g. empty for c1, "Z" for Cs, "XY" for C2)
\redl{<ENTER>}   \comment{! no symmetry in this example}


Name of the file containing the cartesian coordinates
of the unique atoms (COLUMBUS format): \redl{geom}

Number of atoms = 3      \comment{! verify that the file was read in correctly}
Sum formula:      S1 O2

Enter basis set: \redl{ano-rcc-vdzp}  \comment{! you can also use the cc-vdzp-dk basis}
                               \comment{! if you included it in the library (see section \ref{sec:BSinput})}
Include scalar relativistic effects? (y/n)\redl{y}
Include spin-orbit coupling effects (AMFI)? (y/n)\redl{y}
\end{Verbatim}
\normalsize

\subsection{SCF input}

SCF input is straight-forward.

\renumber\numbering{Go to the corresponding menu.}

\scriptsize
\begin{Verbatim}[commandchars=\\\{\}]
      1) Integral program input (for argos/dalton/turbocol/molcas)
\red{->    2)} SCF input
      3) MCSCF input
      4) CI input
      5) Set up job control
      6) Utilities
      7) Exit the input facility
\end{Verbatim}
\normalsize

\numbering{Input the necessary information.}

\scriptsize
\begin{Verbatim}[commandchars=\\\{\}]
     ******************************************
     **    PROGRAM:              MAKSCF      **
     **    PROGRAM VERSION:      5.5.1       **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 Symmetry orbital summary:

 Molecular symmetry group:   c1 
 Symm.blocks:         1
 Symm.labels:          a 
 Number of basis
  functions:          46
 
 Do you want a closed shell calculation ? <YES> \redl{<ENTER>}
 
 Input the no. of doubly occupied orbitals for each irrep, DOCC:
\redl{16}


 The orbital occupation is:

        a
 DOCC   16
 OPSH    0
 
 Is this correct? <YES> \redl{<ENTER>}
 
 Would you like to change the default program parameters? <NO> \redl{<ENTER>}
  Input a title: <Default SCF Title>
 --> \redl{<ENTER>}

\end{Verbatim}
\normalsize

\section{MCSCF input}\label{ssec:mcscf}

We set up a CASSCF(12,9) calculation, with state-averaging over 4 singlets and 3 triplets. A normal MCSCF input (preparing for CI gradients) is required.

\renumber\numbering{Go to the corresponding menu.}

\scriptsize
\begin{Verbatim}[commandchars=\\\{\}]
      1) Integral program input (for argos/dalton/turbocol/molcas)
      2) SCF input
\red{->    3)} MCSCF input
      4) CI input
      5) Set up job control
      6) Utilities
      7) Exit the input facility
\end{Verbatim}
\normalsize

\numbering{We want to calculate MRCI level gradients.}

\scriptsize
\begin{Verbatim}[commandchars=\\\{\}]
Freeze orbitals prior to MCSCF (no gradients available) [y|n] \redl{n}
prepare input for no(0), CI(1), MCSCF(2), SA-MCSCF(3) analytical gradient  \redl{1}
\end{Verbatim}
\normalsize

\numbering{We want to include neutral singlet and triplet states here. The active space is 12 electrons in 9 orbitals. There are 10 doubly occupied orbitals. }

\scriptsize
\begin{Verbatim}[commandchars=\\\{\}]
Enter number of DRTS [1-8]  \redl{2}

number of electrons for DRT #1 (nucl. charge: 32)  \redl{32}
multiplicity for DRT #1  \redl{1}
spatial symmetry for DRT #1  \redl{1}
excitation level (cas,ras)->aux  \redl{0}
excitation level ras->(cas,aux)  \redl{0}

number of electrons for DRT #2 (nucl. charge: 32)  \redl{32}
multiplicity for DRT #2  \redl{3}
spatial symmetry for DRT #2  \redl{1}
excitation level (cas,ras)->aux  \redl{0}
excitation level ras->(cas,aux)  \redl{0}

number of doubly occupied orbitals per irrep   \redl{10}
number of CAS orbitals per irrep   \redl{9}

Apply add. group restrictions for DRT 1 [y|n]   \redl{n}
Apply add. group restrictions for DRT 2 [y|n]   \redl{n}
\end{Verbatim}
\normalsize

\numbering{We want to calculate 4 singlet and 3 triplet states. For MR-CI dynamics it is recommended to use very tight convergence criteria for MCSCF. These will only require negligible additional time as compared to the MR-CI step but may be important for stable gradient computations.}

\scriptsize
\begin{Verbatim}[commandchars=\\\{\}]
   Convergence
      1. Iterations              #iter [100]    #miter [50 ]   #ciiter [300]
      2. Thresholds              knorm [1.e-\redl{6} ]  wnorm [1.e-\redl{6} ]  DE [1.e-\redl{10}]   \comment{! increased convergence}

      3. HMC-matrix              build explicitly [\redl{y}]    \comment{! if you change this, adjust the cigrdin file later!}
                                 diagonalize iteratively [y]
      4. Miscellaneous           quadratic cnvg. [y] from #iter [5  ]
                                       ... only with wnorm < [1.e-3 ]

   Resolution (NO)               RAS [NO  ] CAS [NO  ] AUX [NO  ]
  State-averaging
                          DRT 1: #states [\redl{4} ] weights[\redl{1,1,1,1}             ]
                          DRT 2: #states [\redl{3} ] weights[\redl{1,1,1}               ]
                  transition moments / non-adiabatic couplings [N]

                             FINISHED [\redl{X}]

   Editing: left/right, 'delete'; Switching fields: 'Tab', up/down
   Help is available through selecting a field and pressing 'return'.
   Indicate completion by selecting 'Finished' and pressing 'return'. 
\end{Verbatim}
\normalsize

\greybox{An important comment on the two options in 3. (HMC matrix): These options strongly influence the performance of the MCSCF calculation. For small CASSCF expansions, choose \texttt{y n}. For medium expansions, \texttt{y y} is recommended (as in the example above) and for large active spaces \texttt{n y} (the default) is advisable. }

\greybox{Another note regarding the Orbital resolution: \col\ uses \texttt{NO} (natural orbitals) if gradients are requested and \texttt{QAA} (canonical orbitals) if no gradients are required. This may affect energetics of a subsequent MR-CI calculations (if the reference space is changed) and may therefore lead to inconsistencies. 
%\textbf{This may lead to inconsistencies between PES scans (or single point energies) and optimizations/dynamics!} 
Please check this option always if you perform independent calculations on the same system.}


\section{CI input}

Now we set up an MR-CIS calculation, including frozen core orbitals (these are compatible with gradients and overlaps). We use a CAS(6,6) reference space. This is a normal CI input for SO-CI.

\renumber\numbering{Go to the corresponding menu.}

\scriptsize
\begin{Verbatim}[commandchars=\\\{\}]
      1) Integral program input (for argos/dalton/turbocol/molcas)
      2) SCF input
      3) MCSCF input
\red{->    4)} CI input
      5) Set up job control
      6) Utilities
      7) Exit the input facility
\end{Verbatim}
\normalsize

\numbering{Press \texttt{ENTER} in the initial screen explaining CI wavefunction definitions.}

\numbering{Choose the first option, since we prepare only the SO-CI input with \texttt{colinp}, the non-relativistic input is generated automatically.}

\scriptsize
\begin{Verbatim}[commandchars=\\\{\}]
\red{->   1)} Def. of CI wave function - one-DRT case
     2) Def. of CI wave function - multiple-DRT case (transition moments)
     3) Def. of CI wave function - independent multiple-DRTs (intersystem crossings)
     4) Skip DRT input (old input files in the current directory)
\end{Verbatim}
\normalsize

\numbering{We do want to calculate gradients.}

\scriptsize
\begin{Verbatim}[commandchars=\\\{\}]
Do you want to compute gradients or non-adiabatic couplings? [y|n]   \redl{y}
\end{Verbatim}
\normalsize

\numbering{Now enter all information for the DRT construction. Remember, we generate the SO-CI input here. }

\scriptsize
\begin{Verbatim}[commandchars=\\\{\}]
Multiplicity:  #electrons:   Molec. symm.


count order (bottom to top): fc-docc-active-aux-extern-fv
irreps         a
# basis fcts   46
Spin-Orbit CI [y|n] \redl{y}
Enter highest multiplicity   \redl{3}
irreducible representation of lx, ly, lz  \redl{1 1 1}

Enter the number of electrons  \redl{32}
Enter the molec. spatial symmetry   \redl{1}
number of frozen core orbitals per irrep  \redl{7}
number of frozen virt. orbitals per irrep  \redl{0}
number of internal(=docc+active+aux) orbitals per irrep  \redl{12}
ref doubly occ orbitals per irrep   \redl{6}
auxiliary internal orbitals per irrep   \redl{0}
Enter the excitation level (0,1,2)   \redl{1}
Enter the allowed reference symmetries   1\redl{<ENTER>}
Apply additional group restrictions for DRT  [y|n]   \redl{n}

Choose CI program: sequential ciudg [1]; parallel ciudg[2]  \redl{1}
\end{Verbatim}
\normalsize

\numbering{In the next screen, the \texttt{NROOT} keyword can be ignored, since the number of states is given later when we specify the \texttt{socinr} method. However, setting \texttt{NROOT} to values larger than 1 allows to request transition dipole moments (this will automatically generate the \texttt{transmomin} file).}


\scriptsize
\begin{Verbatim}[commandchars=\\\{\}]
                            CIUDGIN INPUT MENU DRT# 1


   Type of calculation:               CI  [Y]  AQCC [N]  AQCC-LRT [N]
   LRT shift:                         LRTSHIFT [0                ]
   State(s) to be optimized           NROOT [\redl{4} ] ROOT TO FOLLOW [0]
   Reference space diagonalization    INCORE[Y] NITER [   ]
                          RTOL  [1e-4,1e-4,1e-4,1e-4,                         ]
   Bk-procedure:                      NITER [1  ] MINSUB [4 ] MAXSUB [9 ]
                          RTOL  [1e-4,1e-4,1e-4,1e-4,                         ]
   CI/AQCC procedure:                 NITER [120] MINSUB [6 ] MAXSUB [9 ]
                          RTOL  [1e-4,1e-4,1e-4,1e-4,                         ]

                             FINISHED [\redl{X}]

   Editing: left/right, "delete"; Switching fields: "Tab", up/down
   Help is available through selecting a field and pressing "return".
   Indicate completion by selecting "Finished" and pressing "Return".

->   1) (Done with selections)
     2) [\redl{X}] bra: DRT# 1 state# 1 ket: DRT# 1 state# 1
     3) [\redl{X}] bra: DRT# 1 state# 1 ket: DRT# 1 state# 2
     4) [\redl{X}] bra: DRT# 1 state# 1 ket: DRT# 1 state# 3
     5) [\redl{X}] bra: DRT# 1 state# 1 ket: DRT# 1 state# 4
     6) [\redl{X}] bra: DRT# 1 state# 2 ket: DRT# 1 state# 2
     7) [\redl{X}] bra: DRT# 1 state# 2 ket: DRT# 1 state# 3
     8) [\redl{X}] bra: DRT# 1 state# 2 ket: DRT# 1 state# 4
     9) [\redl{X}] bra: DRT# 1 state# 3 ket: DRT# 1 state# 3
    10) [\redl{X}] bra: DRT# 1 state# 3 ket: DRT# 1 state# 4
    11) [\redl{X}] bra: DRT# 1 state# 4 ket: DRT# 1 state# 4
\end{Verbatim}
\normalsize

\section{Setup job control}

This will complete the \texttt{colinp} prodecure.

\renumber\numbering{Go to the corresponding menu.}

\scriptsize
\begin{Verbatim}[commandchars=\\\{\}]
     1) Integral program input (for argos/dalton/turbocol/molcas)
     2) SCF input
     3) MCSCF input
     4) CI input
\red{->   5)} Set up job control
     6) Utilities
     7) Exit the input facility

\red{->   1)} Job control for single point or gradient calculation
     2) Potential energy curve for one int. coordinate
     3) Vibrational frequencies and force constants
     4) Exit

\red{->   1)} single point calculation
     2) geometry optimization with GDIIS
     3) geometry optimization with SLAPAF
     4) saddle point calculation (local search - GDIIS)
     5) stationary point calculation (global search - RGF)
     6) optimization on the crossing seam or ISC (GDIIS)
     7) optimization on the crossing seam (POLYHES)
     8) Exit
\end{Verbatim}
\normalsize

\numbering{Choose all the options indicated below. Choose SCF only if you do not provide starting orbitals.}

\scriptsize
\begin{Verbatim}[commandchars=\\\{\}]
->   1) (Done with selections)
     2) [\redl{X}] SCF
     3) [\redl{X}] MCSCF
     4) [ ] transition moments for MCSCF
     5) [ ] MR-CISD (serial operation)
     6) [ ] MR-CISD (parallel operation)
     7) [\redl{X}] SO-CI coupled to non-rel. CI
     8) [\redl{X}] one-electron properties for all methods
     9) [\redl{X}] transition moments for MR-CISD
    10) [ ] single point gradient
    11) [\redl{X}] nonadiabatic couplings (and/or gradients)
    12) [ ] <L> value calculation for MR-CISD
    13) [ ] convert MOs into molden format
    14) [ ] get starting MOs from a higher symmetry
    15) [ ] finite field calculation for all methods
    16) [ ] include point charges
    17) [ ] extended MOLCAS interface
\end{Verbatim}
\normalsize

\numbering{Now choose the number of states.}

\scriptsize
\begin{Verbatim}[commandchars=\\\{\}]
Number of states per multiplicity (S/D/T/...)  \redl{4 0 3}
\end{Verbatim}
\normalsize

\numbering{Choose first moment calculations for transition dipole moments.}

\scriptsize
\begin{Verbatim}[commandchars=\\\{\}]
->   1) (Done with selections)
     2) [\redl{X}] first moment
     3) [ ] second moment
     4) [ ] third moment
     5) [ ] fourth moment
\end{Verbatim}
\normalsize

\numbering{The following questions are related to the calculation of non-adiabatic couplings. This option has no effect here, as we plan to use CI overlaps rather than non-adiabatic couplings. However, in general only interstate couplings and not the full non-adiabatic couplings can be computed with the \seward\ module. }

\scriptsize
\begin{Verbatim}[commandchars=\\\{\}]
 Do you want to perform an analysis in internal coordinates? (y|n) \redl{n}

\red{->   1)} DCI*(E2-E1) term (interstate coupling)
     2) DCSF term
     3) DCI+DCSF term (non-adiabatic coupling)

 Perform intersection analysis (slope)? (y|n)  \redl{n}
\end{Verbatim}
\normalsize

\numbering{Exit \texttt{colinp}.}

\section{Adjusting the input files}

A number of input files have to be adjusted afterwards.

\subsection{Input for cigrd}

If you requested the explicit HMC matrix construction in the MCSCF input (section~\ref{ssec:mcscf}), you have to adjust the \texttt{cigrdin} file. The option \texttt{mdir} has to be changed to zero.

\scriptsize
\begin{Verbatim}[commandchars=\\\{\}]
 &input 
 nmiter= 100, print=0, fresdd=1, 
 fresaa=1, fresvv=1, 
 mdir=\redl{0}, 
 cdir=1, 
 rtol=1e-6, dtol=1e-6,
 wndtol=1e-7,wnatol=1e-7,wnvtol=1e-7 
 nadcalc=1
 &end
\end{Verbatim}
\normalsize

\greybox{If you do not know anymore, whether you requested the explicit HMC matrix construction, you can check in file \texttt{mcscfin}. If the line starting with \texttt{npath} contains the number \textbf{11} (not -11), the HMC matrix is constructed explicitly.}

\subsection{Transition dipole moment and gradient specifications}

The file \texttt{transmomin} contains the specifications, for which states gradients and transition dipole moments have to be calculated.

For our example (4 singlet and 3 triplet states), the file should look like this:
\scriptsize
\begin{Verbatim}[commandchars=\\\{\}]
CI
1 1 1 1 
1 1 1 2 T
1 1 1 3 T
1 1 1 4 T
1 2 1 2 
1 2 1 3 T
1 2 1 4 T
1 3 1 3 
1 3 1 4 T
1 4 1 4 
2 1 2 1 
2 1 2 2 T
2 1 2 3 T
2 2 2 2 
2 2 2 3 T
2 3 2 3 
\end{Verbatim}
\normalsize

The first line is just a keyword and need not be changed.

The following lines specify pairs of states (as in matrix elements like $\langle \psi_1|\hat{O}|\psi_2\rangle$). The first column is the number of the DRT of the bra state. In our case, each multiplicity corresponds to one DRT (in the case of symmetry, each DRT contains one irrep), meaning that singlet=1 and triplet=2. The second column is the index of the bra state. Columns 3 and 4 specify the ket state in the same manner. The file should contain all possible pairs of states with the same multiplicity. 

Adding a \texttt{T} after the line will calculate only the transition moment between the specified states. Omitting the \texttt{T} also leads to the calculation of a gradient (if bra and ket are equal) or a non-adiabatic coupling vector (which we do not need here). 

\subsection{Control file}

Finally, inspect the file \texttt{control.run}. It contains keywords for all subtasks to be executed. 

Add the keyword \texttt{detprt} if you intend to calculate wavefunction overlaps. This will generate binary files containing the CI vectors.

Optionally, delete the keywords \texttt{scfprop} and \texttt{mcscfprop}.

\section{Execution and Results}

\subsection{Execution}

In order to execute the calculation, run
\scriptsize
\begin{Verbatim}[commandchars=\\\{\}]
~> \$COLUMBUS/runc -m 2000 > runls&     \comment{! memory is in MB}  
\end{Verbatim}
\normalsize

The calculation can be followed with the \texttt{runls} file, which states all steps executed and also contains the main results. The convergence of the MCSCF and MRCI calculations can be followed in \texttt{WORK/mcscfsm} and \texttt{WORK/ciudgsm}.

\subsection{Extraction of main results}

All important quantities calculated are listed in the file \texttt{runls}. 

Those quantities include:
\begin{itemize}
  \item CI energies
  \item SO matrix
  \item Transition dipole moments
  \item State dipole moments
  \item Gradients
\end{itemize}

\section{Calculation of wavefunction overlaps}

Considering that the full non-adiabatic coupling vectors were initially not available when using the seward program, we compute the non-adiabatic interactions via wavefunction overlaps here.
Note, however, that in Columbus 7.2 also nonadiabatic coupling vectors are available using seward/alaska from OpenMolcas.

The full procedure for computing wavefunction overlaps has been implemented into the \col\ interface for the \textsc{Sharc} dynamics program. Therefore we will only list the main steps here:
\begin{itemize}
  \item Manually generate an input for seward including both geometries (as a kind of ``double molecule'')
  \item Call \molcas\ with this input to calculate the AO overlap integrals
  \item Link the \molcas\ runfile and integral file, the old and new MO coefficients and the \texttt{cidrtfl}
  \item Generate the \texttt{mkciovinpin} file, which is the input for the input generator for \texttt{cioverlaps}
  \item Run \texttt{mkciovinp}, this will generate the input for \texttt{cioverlaps}
  \item Link the old and new \texttt{eivectors}, the \texttt{slaterfile} and possibly the \texttt{excitlistfile}
  \item Run \texttt{cioverlaps}
\end{itemize}

\section{Literature}
\begin{enumerate}
\item
S. Mai, T. Müller, F. Plasser, P. Marquetand, H. Lischka, L. González, ``Perturbational treatment of spin-orbit coupling for generally applicable high-level multi-reference methods'' \textit{J. Chem. Phys.} \textbf{2014}, 141, 074105.
\end{enumerate}


























\end{document}