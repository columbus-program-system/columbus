<LI><A NAME="tex2html1" HREF="node5.html#566">Open Intrashell Energy Coefficient Formulas</A>
<LI><A NAME="tex2html2" HREF="node5.html#633">Open Intrashell Energy Coefficients for Atoms</A>
<LI><A NAME="tex2html4" HREF="node5.html#714">Open Intrashell Energy Coefficients for Linear Molecules</A>
<LI><A NAME="tex2html6" HREF="node5.html#753">Open Intershell Energy Coefficients for Atoms</A>
<LI><A NAME="tex2html7" HREF="node5.html#784">Open Intershell Energy Coefficients for Linear Molecules</A>
<LI><A NAME="tex2html8" HREF="node5.html#967">Open Intershell Energy Coefficients for  <IMG WIDTH=37 HEIGHT=28 ALIGN=MIDDLE ALT="tex2html_wrap_inline1118" SRC="img1.gif"  >  Molecules</A>
<LI><A NAME="tex2html9" HREF="node5.html#820">Open Intrashell Energy Coefficients for Octahedral and Tetrahedral 
Molecules</A>
<LI><A NAME="tex2html10" HREF="node5.html#881">Open Intershell Energy Coefficients for Octahedral and Tetrahedral 
Weak-Field States</A>
