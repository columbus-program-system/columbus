      program sumcivec

   This program allows to sum two valid ci vectors (as defined in the
   ci info file civout) from a given ci (or sigma) vector file.

   input file:   sumciin
   listing file: sumcils

   Namelist type input

   vectorin : name of the input vector file
   vectorout: name of the output vector file
   infoin   : name of the input vector info file
   infoout  : name of the output vector info file
   lvlprt   : printlevel (>=0)
   vector1  : defines the first vector to be summed
   vector2  : defines the second vector to be summed
   delsrc   : if set to 1 the input vector and input vector info
              files are deleted after the copying operation

  09/20/00 (tm)




