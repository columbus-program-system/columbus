			 "CIPC" Documentation
			Written by Ron Shepard
		    Revision Date: 26-Nov-91 (rls)

modified to work with civfl and new civout (tm) 19-Mar-98 
 
    Program CIPC prints CSF information for MRSDCI wave functions computed
using program CIUDG.  The CIPC program is set up to be run in an interactive
mode but it also can be used to analyze the vector and CSFs following CI
calculations in a batch mode by preparing the input in advance.

    Program CIPC requires two input data files:

    (1) CIDRTFL is read to determine the frozen-core, internal, and external
orbital partitioning.  Some DRT chaining arrays are also read from this file.

    (2) CIVOUT contains the header information on all nroot CI vectors
computed. This includes titles, energies, and convergence information
are also read from this file.

    (3) CIVFL  contains the nroot CI vectors

    Program CIPC is similar to the MCSCF analysis program MCPC, but it is
designed to work with very long wave function expansions.  In order to achieve
this goal, only preselected CSF coefficients are available for examination.
These preselected coefficients are determined by the selection parameters
ICSFMN, ICSFMX, CSFMN, and CSFMX.  Only CSF coefficients that satisfy the
criteria

        ICSFMN <= ICSF <= ICSFMN

and

        CSFMN <= |CSFVEC(ICSF)| <= CSFMX

are available for examination.  Different preselection criteria may be imposed
on the vector for detailed examination of different vector element subsets, as
described below under main menu item 7.  These four quantities may be though of
as defining an active "rectangle" of CSF coefficients.  The total number of
selected coefficients within the current rectangle is limited by the amount of
workspace available to program CIPC.

    On unix machines, the amount of workspace may be set on the command line in
the usual manner.  For example, "$COLUMBUS/cipc.x -m 500000" would give CIPC
1/2 MW of workspace.

    The CSF coefficient listings generated from program CIPC may be used as
initial formatted trial vectors to program CIUDG.  (See the descriptions of
IVMODE and CIFVFL in cidrt.doc.)

    Program CIPC is a menu driven program.  The input to the main menu
is the option numbers (1-8).  Each of these options may require the
selection of additional submenu items.  When all options needed have
been specified then a 0 terminates the program.  The menu options are
listed below.
 
    1 - Print CSFs info by sorted CSF coefficient.  The CSF coefficients are
sorted in decreasing order of absolute values.  The submenu options require a
card with ISTART,IEND indicating sorted index number range.  After all segments
have been examined, end with 0,0 to return to the main menu.
 
    2 - Print CSF info by contributing thresholds.  The submenu requires the
input of a threshold range.  Available CSFs are printed in sorted order.  Enter
a -1/ to return to the main menu.
 
    3 - Print CSF info by CSF numbers.  The submenu options require a card
with ISTART,IEND indicating the CSF index number range.  After all ranges
have been examined, end with 0,0 to return to the main menu.
 
    4 - Additional print options.  The submenu has the following options.
        1 - Print the Slater determinant representations of the CSFs
            along with the step vector representations.  This option
            requires the Ms value of the determinants to to specified.
            The Ms value must be consistent with the spin multiplicity 
            of the wave function.
        2 - Suppress determinant printing.
        3 - Reset the character*1 cstep(0:3) array.  The default values
            are '0', '1', '2', and '3', but these may be changed to suit
            individual tastes or local conventions.
        4 - Reset the character*1 cdet(0:3) array.  The default values
            are '.', '+', '-', and '#'.
        5 - Reset the SLABEL*4(1:nsym) array.
        0 - End and return to the main menu.
 
    5 - Print the sorted CSF vector with no orbital occupation info.  The
entire selected vector in sorted order is printed.  No additional data is
required.  The return to the main menu is automatic.

    6 - Print the CSF vector with no orbital occupation info.  The entire
selected vector in CSF index order is printed.  No additional data is required.
The return to the main menu is automatic.
 
    7 - Reset the preselection criteria (see above) and read (or reread as
appropriate) the CSF coefficients.  A histogram of the selected coefficients is
computed also during this step.  Note that the other main menu options depend
on this step.  Consequently, this option must be selected first or the other
main menu options have no meaning.  The selection criteria and histogram
parameters are set according to the following menu:

        1 - Reset CSFMN and CSFMX.
        2 - Reset FHIST.  Histogram breakpoints are set as 
                  CSFMX*FHIST**(i-1).
            for i=1 to NHIST. 0.0<FHIST<1.0 should be satisfied.
        3 - Reset NHIST, the number of histogram breakpoints.  NHIST is reset
            to be consistent with CSFMN, CSFMX, and FHIST.  Consequently, NHIST
            should usually be changed after (and not before) these other
            quantities.
        4 - Reset ICSFMN and ICSFMX.
        5 - Select the CI vector to be analysed ( 1 through nroot)
            default is 1
        0 - Read the CI vector (if necessary) and return to the
            main menu.
 
    8 - Print the current CSF coefficient histogram.  This gives a quick
overview of the structure of the CSF vector.  The return to the main menu is
automatic.
 
    0 - Terminates the program.
 
The following is a sample input deck for a batch mode job using selected
options.  Any combination of the following can be used depending on
their usefulness to the problem being studied (spaces have been added to
clarify distinctions between different options and need not be present
in the input deck):
 
$COLUMBUS/cipc.x <<EOF

7    / Read the CI vector.  This should always be done first.

1    / reset CSFMN and CSFMX.
0.0 1.0 / these are the default values (entire range for normalized vectors).

2    / reset FHIST.
0.75 / each histogram range will be 3/4 of the previous one.

3    / reset NHIST.
100  / Set to 100 breakpoints (99 bins).

5    / select CI vector
1    /  number 1

0    / read the CI vector with the above selection parameters.

4    / set print options.

1    / print determinants.
/      Ms value.  Default is the maximal value for the given multiplicity.
0    / return to the main menu.
 
1    / option to print CSFs by contributing factors.
1,5  / print 1st largest thru 5th largest CSF coefficient.
0,0  / terminate this option and return to the main menu.

4    / set print options.
4    / Reset the way determinants are printed.
' ' '^' 'v' 'X' / new values for empty, alpha, beta, and double.
0    / return to the main menu.
 
2    / option to print CSFs by threshold range.
0.01 1.0  / threshold range.
/ terminate this option and return to the main menu.
 
4    / set print options.

2    / suppress determinant printing.

5    / reset SLABEL(*).
'   1', '   2', '   3', '   4' /use symmetry block numbers.

0    / return to the main menu.
 
3     / option to print CSFs according to their CSF number.
10,20 / print CSF #10 thru CSF #20.  Selected CSFs only are available.
0,0   / terminate option and return to the main menu.
 
8     / Print the current histogram.

7     / Reread the CSF coefficients with new selection criteria.

1     / reset CSFMN and CSFMX.
0.004 1.0 /

0     / Read the CSFs and return to the main menu.
 
5     / Print the entire sorted CSF vector (only selected coefficients).  

6     / Print the CSF vector in CSF index order (only selected coefficients).
 
0     / terminate program.
 
EOF
