
                    "MAKCI" Program Documentation


   MAKCI is a simplifed version of CIDRT and also serves as an interactive
program for ciudgin and cidenin.  tranin, cisrtin, and cigrdin are
automatically generated.  Defaults are taken for some parameters.  The file
cikey is then used as input for CIDRT.

Files:

(in)
infofl          point group and basis set information file

(out)
cikey           the output file
imakci.key      part of keystroke file
ciudgin         input file for prorgam ciudg
tranin          input file for program tran
cisrtin         input file for program cisrt
cidenin         input file for program ciden
cigrdin         input file for program cigrd
