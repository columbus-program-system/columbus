
                    "MAKMC" Program Documentation


   MAKMC is a simplified version of MCDRT and also serves as interactive program
for mcscfin.  nofmtin is automatically generated.  Defaults are 
taken for some parameters.  The file makmc.key is then used as input for
MCDRT.

Files:

(in)
infofl          point group and basis set information file

(out)
makmc.key       the output file
imakmc.key      keystroke file of defining reference space
mofmtin         default input file for program mofmt
nofmtin         input for program nofmt
mcscfin         input file for program mcscf
