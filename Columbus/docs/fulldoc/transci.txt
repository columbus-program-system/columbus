 
                         PROGRAM TRANSCI
     
          Computation of MR-CI transition moments and related properties

                         May 2000
          Thomas Mueller, Institute for Theoretical Chemistry, University of Vienna



The program computes the MR-CI transition density for pairs of states of
the same (one DRT) as well as different symmetry (two DRTs). 
Certain restrictions are imposed on the structure of the DRTs in the 
two-DRT case which is taken care of by the cidrtms program. 

The one-electron transition density is calculated with the formula tape
being calculated on the fly.

NAMELIST input

         NROOT1 : sequence number of the (bra) state on the CI vector file
         NROOT2 : sequence number of the (ket) state on the CI vector file
         LVLPRT : print level
         METHOD : 0 = MR-CISD
                  1 = CEPA-0
                  2 = MR-ACPF
                  3 = MR-AQCC
         DRT1   : number of the DRT of the bra state 
         DRT2   : number of the DRT of the ket state 
          


Note, that cidrtms generates one cidrtfl file per DRT named cidrtfl.drtXX where XX
runs from 1 to the total number of DRTs. The same applies to the CI vector file
(civfl.drtXX), the CI info file (civout.drtXX) and the CI reference vector file
(cirefvfl.drtXX).

Hence, in order to calculated the MR-CISD transition moment between the first and the
third root on the CI vector file corresponding to DRT2 the input is

 &input
  nroot1=1,
  nroot2=3,
  method=0
  DRT1=2,
  DRT2=2
 &end


File name          unit no                  description

 trncils              6                  standard listing file        (out).
 trnciin              5                  user input file.             (in)
 trncism              7                  summary listing file.        (out) 
 index1               8                  bra index file.              (scr)
 index2               9                  ket index file.              (scr)
 civfl.drtDRT1        10                 bra CI vector                (in)           
 civfl.drtDRT2        11                 ket CI vectro                (in)
 cidrtfl.drtDRT1      17                 DRT file for DRT1            (in)
 cidrtfl.drtDRT2      18                 DRT file for DRT2            (in)
 civout.drtDRT1       20                 bra CI info file             (in)
 civout.drtDRT2       21                 ket CI info file             (in)
 flacpfd              25                                              (scr)
 cirefv.drtDRT1       26                 bra reference vector, seq    (scr)
 cirefv.drtDRT2       27                 ket reference vector, seq    (scr)
 cirfl1               35                 bra reference vector, da     (scr)
 cirfl2               36                 ket reference vector, da     (scr)
 cisrtif              28                 cisrt info file              (in)
 trndens              29                 transition density           (out)
 geom                 30                 molecular geometry           (in)
 moints               33                 MO integral file             (in)
 cid1trfl             34                 symmetric and antisymmetric 
                                         1-e desity file              (out)
