
                    "GETMAKCI" Program Documentation

   Given point group and keystroke file of MAKCI at minimum geometry
calculation, the irrep of displaced internal coordinate, and descended
point group, GETMAKCI generates input for MAKCID at descended point group.
Present version of GETMAKCI is limited to cases where reference space is
not selected manually, and it is better that in reference space the
orbitals are ordered according to orbital energy, ie. low -> high, to avoid
any complication.

Files:

(in)
imakci.key.min  keystroke file of program makci at minimum geometry
infofl.old      infofl at minimum gemmetry
infofl          infofl at displaced geometry
curr.label      the irrep of displaced internal coordinate

(out)
makci.input     input file for program makcid
