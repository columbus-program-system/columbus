#!/bin/bash 
# set -x 
#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/
#
# For Columbus version 5.9   March    3rd, 2002 (tm) 
#
# shell script for the automatic installation of the entire COLUMBUS Program
# System - to be executed by install.automatic.
#
# Options:
# -forcestatic  force static linking of the binaries 
#
# -c : force recompilation of the codes and removal of  *.o files.
#
# -p machine_id :   supported machines for completely automatic installation
#                   see $COLUMBUS/machine.cfg
#
# -v port_version : grad - installs the CI density and gradient programs
#                   parallel - installs only the parallel pciudg program
#                   direct - installs direct programs
#                            (scf_d, mcscf_d, colop, ciudg_d ) 
#                   standard - base program version  
#
# -ponly    : skip generating binaries
#
# if the -v option is omitted the base program version is installed
# 
# the following machin_id options 
#
# 28-jul-97 automatic installation of parallel and direct package -tm 
#  8-oct-96 automatic installation -tm and -hl 
# 27-apr-92 colib.a always updated. -rls
# 28-sep-91 mcscf codes added. -rls
# 31-May-91 colib.a is updated instead of recreated on -c. -rls
# 17-Dec-90 written by ron shepard
#
#=================function definitions=====================================
#
# extract keyword from file
# $1 keyword  $2 filename
# return value  1 keyword not found 0 keyword found
# if keyword is found the variable keyword is set to its value
# file structure: one keyword per line, starting at position 1
#                 separated from its value by one or more blanks
#

  getkeyword () {
     INP=`grep "^$1" $2 | head -1 | sed -e 's/ *$//' `
     grep "^$1 " $2 > /dev/null
     if [ $? -eq 0 ] ; then
       VALUE=${INP#"$1"}
       eval "$1"=\'"${VALUE# *}"\'
       echo "found keyword $1 in $2 with value $VALUE "
       return 0
     else
       echo "could not find keyword $1 in $2 "
       return 1
     fi
    }

  getkeywtmp () {
     INP=`grep $1 $2 | head -1 | sed -e 's/ *$//' `
     grep "^$1 " $2 > /dev/null
     if [ $? -eq 0 ] ; then
       VALUE=${INP#"$1"}
       eval TMP=\'"${VALUE# *}"\'
       echo "found keyword $1 in $2 with value $TMP "
       return 0
     else
       return 1
     fi
    }




    chkenv () {
     eval "TMPP=\$$1"
     echo $TMPP
     if [ -z "$TMPP" ] ; then
      echo "$1 environment variable not defined "
      echo "exiting ..."
     fi
     return 0
   }

   spatt () {
#
#   search pattern $2 in string $1
#   return 1 if nothing matched and 0 if a match occured
#
    echo "searching for $2 in $1 "
     echo $1 | grep $2  > /dev/null
     return $?
#    if [ $? -eq 0 ] ; then 
#     return 0
#   else
#    return 1
#    fi
    }

# variable set to zero requires all of the source codes
# variable set to one  copies executables  mcscf_d.x tran_d.x 
# scf_d.x ciudg_d.x copies to the columbus directory 
#
THIS_IS_FOR_DISTRIBUTION=1
#
#=============================================================================
# should already be set by install.automatic

echo " =====  colinstall.sh  ... version 03/03/2002 (tm)      ===== "
echo " =====  Columbus $COLUMBUSVERSION installation starting ===== " 
 chkenv COLUMBUS 
 if [[ ! "$COLUMBUS" -ef "$PWD" ]] ; then
# 16/3/2012 lf : use the builtin shell comparison operator for comparing paths instead of !=
 echo The '$COLUMBUS' variable has already been set to $COLUMBUS
 echo but this does not agree with the current path
 echo exiting colinstall.sh in $PWD
 exit 5
 fi

 chkenv COLUMBUSVERSION 
 chkenv TURBOCOL 
 chkenv RANLIB 
 chkenv CPPL 
 chkenv BLASLIBRARY 
 chkenv LAPACKLIBRARY 

 export COLUMBUSVERSION TURBOCOL RANLIB CPPL BLASLIBRARY LAPACKLIBRARY
 echo "BLASLIBRARY=$BLASLIBRARY:"
 echo "LAPACKLIBRARY=$LAPACKLIBRARY:"

 DALTON_DIR="$COLUMBUS/source/dalton"
 export DALTON_DIR 
 echo "DALTON_DIR set to $DALTON_DIR"


#############################################################################
# interprete command line parameters 
#---------------------------------------------------------------------------
INPUT_ERROR=0
SHOULD_PORT=0
SHOULD_CLEAN=0
PORT_VERS="standard"
MACHINE_ID=0
PONLY=0

ARG_LIST=$*

while [ $# -gt 0 ] ;do
   case $1 in
   "-c")			# ==> force compilation and clean up dirs.
      SHOULD_CLEAN=1
      shift
      ;;
   "-p")			# ==> port with $COLUMBUS/port.
      if [ $# -ge 2 ] ;then
         SHOULD_PORT=1
         MACHINE_ID=$2
         shift ; shift # shift 2 does not work on the convex.
      else
         INPUT_ERROR=1		# machine_id is required with the -p option.
         break
      fi  
      ;;
   "-v")			# ==> port with $COLUMBUS/port.
      if [ $# -ge 2 ] ;then
         PORT_VERS=$2
         shift ; shift # shift 2 does not work on the convex.
      else
         INPUT_ERROR=1		# port version is required with the -v option.
         break
      fi  
      ;;
   "-ponly")
      PONLY=1
      shift
      ;;
   "-forcestatic")
      FORCESTATIC=1
      shift
      ;; 
   *)				# everything else is an error.
      INPUT_ERROR=1
      break
      ;;
   esac
done

if [ $INPUT_ERROR != 0 ] ;then
   echo usage: $0 [-c] [-p machine_id] [-v port_vers] 1>&2
   exit 1
fi

if [ $PONLY -eq 0 ]; then
 getkeywtmp MACHINEID "$COLUMBUS/../install.config" 

if [ $? -eq 0 ] ;  then
  MACHINE_ID=$TMP
  echo "V: MACHINE_ID=$MACHINE_ID"
  spatt "$MACHINE_ID" "rs6000"
  if  [ $? -eq 0 ]   && [ "$MACHINE_ID" != "rs6000.5.x" ]  ; then
      MACHINE_ID=rs6000
  fi
  else
  echo "cannot find machineid  entry in $COLUMBUS/../install.config"
  if [  $SHOULD_PORT -eq 0 ]; then
    echo "and no -p option has been given on the command line "
    exit
  fi
fi
else
  echo "H: MACHINE_ID=$MACHINE_ID"
spatt "$MACHINE_ID" "rs6000"
  if [ $? -eq 0 ]  && [ "$MACHINE_ID" != "rs6000.5.x" ] ; then
      MACHINE_ID=rs6000
  fi
fi

##########################################################
case $MACHINE_ID in 
"rs6000.4.x"|"sp2"|"rs6000.3.x"|"rs6000"|"sp4"|"sp4int64"|"rs6000.5.x") 
      XLFRTEOPTS=$XLFRTEOPTS:namelist=old
      export XLFRTEOPTS ;;
*)  echo   
esac
echo $XLFRTEOPTS

###########################################################################
getkeyword GACOMMUNICATION "$COLUMBUS/../install.config"
##########################################################
echo "Building the COLUMBUS Program System" `date`
echo Command Line: $0 $ARG_LIST
echo "Root directory COLUMBUS =" $COLUMBUS
GMAKEC="$GMAKE -f $COLUMBUS/makefile MMID=$MACHINE_ID "
if [ ! -z $FORCESTATIC ]; then 
if [ $FORCESTATIC -eq 1 ]; then
GMAKEC="$GMAKEC FORCESTATIC=ON"
fi
fi 
echo "GMAKEC=$GMAKEC"
#$GMAKE -f $COLUMBUS/makefile V  #echo the makefile version date
###########################################################################


#################################################################
# set up lists of programs belonging to the various packages
#---------------------------------------------------------------

############ standard version ##################################
 
list=" dalton cosmolib   argos	tran 	\
	cidrt	    scfpq  	\
	mcdrt	mcuft	mcscf	mcpc lvalue  \
	cnvrt cipc  iargos sif  get 	mak  transft transmom makpciudg ciudg  \
        cimkseg exptvl transci cidrtms transmo ffield utils trci2den sumcivec potential"
getkeyword MOLCAS "$COLUMBUS/../install.config"
if [ $? -eq 0 ] ; then
list="molcas $list"
echo "added molcas to list:"
echo $list
else
list="molcaswrapper $list"
echo "added molcaswrapper to list"
fi
list="blaswrapper colib $list"


list="dalton2 $list"
echo "added dalton2 to list:"
GMAKEC="$GMAKEC DALTON2=$DALTON2 LIBDALTON2=$COLUMBUS/libdalton2.a "
echo "GMAKEC=$GMAKEC"
echo $list
export LIBDALTON2=$COLUMBUS/libdalton2.a
echo "LIBDALTON2=$LIBDALTON2"

#lvalue needs complex lapack

getkeyword MOLCAS "$COLUMBUS/../install.config"

if [ ! -z "$MOLCAS" ] ; then
#list="$list molcas"
export LIBMOLCAS="$MOLCAS/lib/libmolcas.a"
echo "info: generating molcas/columbus interface "
echo "info: setting LIBMOLCAS to $LIBMOLCAS"
getkeyword MCFLAGS "$COLUMBUS/../install.config"
export MCFLAGS
echo "info found: MCFLAGS =$MCFLAGS"
else
echo "MOLCAS interface will not be installed"
fi

############# gradient version #################################
listgrad=" cigrd gdiis intc suscal bmat cart2int rgf dzero polyhes constraint "
############# parallel version ##################################
listpar=" pciudg "
############## ao direct versions ################################
listdirect=" " 
############## incremental stuff ################################
listxjoda=" "
######################################################################

# set up variables determining porting mode 
# and possibly copy executables

#--------------------------------------------------------------------
MAINDIR="$COLUMBUS"
case $PORT_VERS in
grad)  list="$listgrad"
       MODE='normal';;
parallel) list="$listpar"
          getkeyword GACOMMUNICATION "$COLUMBUS/../install.config"
          case $GACOMMUNICATION in
          "TCGMSG") MODE='parallel' ;;
          "MPI"|"MPI-MYRINET"|"MPILAM"|"MPICH") 
             MODE='parallelmpi' 
             ;;          
          *) echo "invalid GACOMMUNICATION entry in $COLUMBUS/../install.config"
             echo "exiting ... "
             exit 
          esac  
           ;;
xjoda)  MAINDIR="$COLUMBUS/source/xjoda"
        list="$listxjoda"
        MODE='normal' ;; 
standard) list="$list"
          MODE='normal' ;;
direct)   list="$listdirect" ;;
*)      echo "invalid package name "
        exit;;
esac 

#############################################################################
###### porting section #########
############################################################################# 

 if [ $SHOULD_PORT = 1 ] ; then 
for i in	$list
do
   echo ===== $0: $i "(porting) ===== "
   ROOT=$i

   case $i in	# assign the target variable.
   "colib"|"libr"|"cosmolib"|"blaswrapper")	# libraries are special cases.
      TARGETC=$i.a 
      ;;
   *)
      TARGETC=$i.x ;;
   esac
   case $i in 
   "libr")
      echo "not prepared for porting" 
    ;;
   "dalton")
    rm -f $COLUMBUS/source/dalton/*.o
    rm -f $COLUMBUS/source/dalton/*.a
    touch $COLUMBUS/source/dalton/*.F
    ;; 
   "pciden")
    if [ ! -d $MAINDIR/source/$ROOT ] ; then
      mkdir $MAINDIR/source/$ROOT 
    fi 
    cd $MAINDIR/source/ciden
    for item in `ls *.f ` 
     do 
     rm -f ../pciden/p$item
     cp -f $item ../pciden/p$item
     done
    cd $MAINDIR/source/$ROOT
    $COLUMBUS/cmdc.pl $MACHINE_ID $MODE *.f || exit 55 
    case $MACHINE_ID in
     "aixxlf.sp6"|"aixxlf32.sp6"|"linux.bgp32")
        $COLUMBUS/gaunder 1 *.f *.inc *.h 
       ;;
    *) 
        $COLUMBUS/gaunder 2 *.f *.inc *.h 
       ;;
    esac
    ;; 
   "pciudg")
    if [ ! -d $MAINDIR/source/$ROOT ] ; then
      mkdir $MAINDIR/source/$ROOT 
    fi 
    cd $MAINDIR/source/ciudg
    for item in `ls *.F90 ` 
     do 
     rm ../pciudg/$item
     cp -f  $item ../pciudg/$item
     done
    cd $MAINDIR/source/$ROOT
    case $MACHINE_ID in
     "aixxlf.sp6"|"aixxlf32.sp6"|"linux.bgp32")
        $COLUMBUS/gaunder 1 *.F90
      ;; 
     *) 
        $COLUMBUS/gaunder 2 *.F90
       ;;
    esac 
     ;; 
   "pmcscf") 
    if [ ! -d $MAINDIR/source/$ROOT ]; then
       mkdir $MAINDIR/source/$ROOT 
    fi 
       cd $MAINDIR/source/mcscf
       for item in `ls *.f *.F90 *.inc *.h` 
        do
        rm ../pmcscf/$item
        cp -f $item ../pmcscf/$item
        done
        cd $MAINDIR/source/$ROOT
        cp $MPI_MAINDIR/include/mpif.h mpif.h 
        $COLUMBUS/cmdc.pl $MACHINE_ID $MODE *.f || exit 56 
    case $MACHINE_ID in
     "aixxlf.sp6"|"aixxlf32.sp6"|"linux.bgp32")
        $COLUMBUS/gaunder 1 *.F90 *.f *.inc
      ;;
    *)
        $COLUMBUS/gaunder 2 *.F90 *.f *.inc
       ;;
    esac
    PMAKE=yes
    ;;
   "pcigrd")
    if [ ! -d $MAINDIR/source/$ROOT ]; then
       mkdir $MAINDIR/source/$ROOT
    fi
       cd $MAINDIR/source/cigrd
       for item in `ls *.f *.F90 *.inc `
        do
        rm ../pcigrd/$item
        cp -f $item ../pcigrd/$item
        done
        cd $MAINDIR/source/$ROOT
        cp $MPI_MAINDIR/include/mpif.h mpif.h
        $COLUMBUS/cmdc.pl $MACHINE_ID $MODE *.f || exit 56
    case $MACHINE_ID in
     "aixxlf.sp6"|"aixxlf32.sp6"|"linux.bgp32")
        $COLUMBUS/gaunder 1 *.F90 *.f *.inc
      ;;
    *) 
        $COLUMBUS/gaunder 2 *.F90 *.f *.inc
       ;;
    esac
    PMAKE=yes
    ;; 
   "pdalton")
    if [ ! -d $MAINDIR/source/$ROOT ]; then
       mkdir $MAINDIR/source/$ROOT
       mkdir $MAINDIR/source/$ROOT/include
    fi
       cd $MAINDIR/source/dalton
       for item in `ls *.F *.F90`
        do
        rm ../pdalton/$item
        cp -f $item ../pdalton/$item
        done
        rm ../pdalton/include/*.inc
        cp include/*.inc ../pdalton/include/
        cd $MAINDIR/source/$ROOT
        cp $MPI_MAINDIR/include/mpif.h mpif.h
        $COLUMBUS/cmdc.pl $MACHINE_ID $MODE *.f || exit 56
    case $MACHINE_ID in
     "aixxlf.sp6"|"aixxlf32.sp6"|"linux.bgp32")
        $COLUMBUS/gaunder 1 *.F90 *.F *.inc
      ;;
    *)
        $COLUMBUS/gaunder 2 *.F90 *.F *.inc
       ;;
    esac
    PMAKE=yes
    ;;
   "ciudg")
    cd $MAINDIR/source/$ROOT  
    if [ "$MACHINE_ID" = "aixxlf.sp6" ]; then
        $COLUMBUS/gaunder 1 *.F90 
    else
        $COLUMBUS/gaunder 2 *.F90
    fi
    $COLUMBUS/cmdc.pl $MACHINE_ID $MODE *.f *.inc *.h || exit 57
    ;;
    "molcas"|"molcaswrapper")
# keep molcaswrapper in there 
#    mkdir $MAINDIR/source/molcas
     cd $MAINDIR/source/molcas
#    cp ../cisrt/*.f .
     if [ -z $MOLCAS ]; then
         LMODE="normal"
     else
         LMODE="molcas"
     fi 
     $COLUMBUS/cmdc.pl $MACHINE_ID $LMODE molcaswrapper.f molcasstub.f
     $COLUMBUS/cmdc.pl $MACHINE_ID $LMODE cisrt*.f
     ;;
   "direct")
    echo "$MACHINE_ID" > $TURBOCOL/machine_id 
    ;;
   "cosmolib")
     cd $MAINDIR/source/$ROOT		# cd to the right place.
     $COLUMBUS/cmdc.pl $MACHINE_ID $MODE *.f || exit 5
     touch *.F
    ;;
   *) 
   cd $MAINDIR/source/$ROOT		# cd to the right place.
   $COLUMBUS/cmdc.pl $MACHINE_ID $MODE *.f || exit 5
   ;;
   esac
done
echo $0 $ARG_LIST finished
fi 
if [ $SHOULD_CLEAN = 1 ] ; then 
########################################################################
#  cleaning section #
########################################################################
for i in $list
do 
  case $i in
  "direct")
      rm -f $TURBOCOL/source/$i/$i*.o 
      rm -f $TURBOCOL/source/$i/$i*.a
      rm -f $TURBOCOL/source/$i/$i*.x
  ;;
  *)
  echo " ==== $0: $i  (removing *.x, *.a *.o files) =====" 
  rm -f $COLUMBUS/$i.a $COLUMBUS/$i.x
  rm -f $MAINDIR/source/$i/$i*.o $MAINDIR/source/$i/$i.a
  esac 
done
fi 

if [ $PONLY = 1 ]; then
  echo "skipping make section "
  exit
fi

#########################################################################
#   make section #
######################################################################### 
echo "processing $list "
for i in	$list
do
   echo ===== $0: $i =====
   ROOT=$i

   case $i in	# assign the target variable.
   "colib"|"libr"|"cosmolib"|"blaswrapper")	# libraries are special cases.
   cd $MAINDIR/source/$ROOT		# cd to the right place.
      TARGETC=$i.a 
    $GMAKEC $TARGETC || exit 7	# make the target.
      ;;
   "iargos")   # iargos has to installed in a different manner
   #fp: the ptgrp links and basis links in the HOME directory are no longer necessary
   cd $MAINDIR/source/$ROOT		# cd to the right place.
         TARGETC=$i.x 
    $GMAKEC $TARGETC || exit 7	# make the target.
      ;;
#       if [ -f "$COLUMBUS/../TOOLS/g/tcgmsg/ipcv4.0/parallel" ] ; then
#         rm -f $COLUMBUS/parallel
#         cp -f $COLUMBUS/../TOOLS/g/tcgmsg/ipcv4.0/parallel $COLUMBUS/ || exit 
   "pdalton"|"pciudg"|"pciden"|"pmcscf"|"pcigrd")
       cd $COLUMBUS/source/$i
       $GMAKEC PMAKE=yes $i.x
    ;;
   "molcaswrapper")
        cd $COLUMBUS/source/molcas
        $GMAKEC  molcaswrapper.a || exit 8
    ;;
   "dalton2")
        cd $COLUMBUS/source/dalton2
        $GMAKEC libdalton2.a
    ;;
   "molcas")
        cd $COLUMBUS/source/molcas
        $GMAKEC molcas.a || exit 8
        cd $COLUMBUS/source/tran
        $GMAKEC tdmrunfile.x || exit 8        
        $COLUMBUS/xupdate "tdmrunfile.x"
    ;;
   "direct")
      echo running installation script $TURBOCOL/install.turbocol
    if [ $SHOULD_PORT = 1 ]
     then
     $TURBOCOL/install.turbocol -p $MACHINE_ID
     if [ $? != 0 ] 
       then 
       echo "install.turbocol failed "
       exit 100   
     fi    
     else
     $TURBOCOL/install.turbocol
     if [ $? != 0 ] 
       then 
       echo "install.turbocol failed "
       exit 101   
     fi    
    fi
    ;;
   "cidrt") # cidrt contains cidrt.x and reindex.x now, this has to be taken care of.
   TARGETC="cidrt.x reindex.x" 
   cd $MAINDIR/source/$ROOT		# cd to the right place.
    $GMAKEC  $TARGETC || exit 7	# make the target.
    ;;
   *) 
   TARGETC=$i.x 
   cd $MAINDIR/source/$ROOT		# cd to the right place.
    $GMAKEC  $TARGETC || exit 7	# make the target.
   ;;
   esac
#
#  TARGET update
#

   case $i in	
   "libr"|"blaswrapper")
    ;; 
   "colib")	

   echo "update colib"
   $COLUMBUS/xupdate "colib.a" || exit 8	# update target in $COLUMBUS/
      rm -f colib.a
      cp -f $COLUMBUS/colib.a colib.a
      ;;
   "cosmolib")
    $COLUMBUS/xupdate "cosmolib.a" 
    ;;
   "get")
   $COLUMBUS/xupdate "*new.x uni*.x" 
   ;;
   "mak")
   $COLUMBUS/xupdate "mak*.x"
   ;;
   "utils")
   $COLUMBUS/xupdate "cutci.x reordermo.x densav.x makeryd.x phaseci.x slope.x xyz2col.x tm2col.x col2tm.x liic.x liicm.x disp_normcoord.x mo_mult.x mo_convert.x mkciovinp.x"
   ;;
   "in")
   $COLUMBUS/xupdate "in*.x" 
   ;;
   "trci2den")
   $COLUMBUS/xupdate "trci2den.x " 
   ;;
   "sif")
   $COLUMBUS/xupdate "testcode.x testmm.x tpack.x iwfmt.x ifake.x icheck.x istatmo.x istat.x mofmt.x "
   ;; 
   "molcas")
    $COLUMBUS/xupdate "cisrt_molcas.x libmolcas_col.a"  
   ;;
   "dalton2")
    cp $COLUMBUS/source/dalton2/libdalton2.a $COLUMBUS/ 
   ;;
   "cidrt")
    $COLUMBUS/xupdate "cidrt.x reindex.x"
   ;;
direct|ga|molcaswrapper)
   ;; 
   *)
   $COLUMBUS/xupdate "$i.x" || exit 9	# update target in $COLUMBUS/

   esac

   if [ $SHOULD_CLEAN = 1 ] ;then
      $GMAKEC clean	# clean up afterwards too.
   fi
done
echo $0 $ARG_LIST finished
exit 0
