#!/usr/bin/env colperl
#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/

#
use Shell qw(pwd cd date tail grep | );
  use lib join('/',$ENV{"COLUMBUS"},"CPAN") ;
  use colib_perl;
#
#==========================================================
#
#   perl program: rgfprotocol.pl
#   aim: program generates the protocol file for an RGF calculation
#   author: Michal Dallos,
#           Institute for Theoretical Chemistry and Molecular Biology
#           University of Vienna
#           Waehringerstrasse 17, A-1090 Vienna, Austria
#
#==========================================================
#
#   GLOBAL VARIABLES
#
#
  $JDIR = pwd();             # job directory
  $JDIR =~ s/\n//;
  $work ="$JDIR/WORK";	     # calculations are carried out in WORK
#
  print "\n *************************************************\n";
#
  open (FILE,"$JDIR/curr_iter") or die " ERROR: file $JDIR/curr_iter not found!\n";
  $iter=<FILE>;
  close FILE;
  ($nintcoor,$dum)=&intc();
#
  open (FILE,"$work/cart2intls") or die " ERROR: file $work/cart2intls not found!\n";
    while (<FILE>)
     {
        if (/Geometries/)
         {
          $line =<FILE>;
	  $line =<FILE>;
            for ($i=1;$i<=$nintcoor;$i++)
             {
	      $_=<FILE>;
              chop($_);
	      s/^ *//;
              ($coor_old[$i],$displ[$i],$coor_new[$i])=split(/\s+/,$_,4);
             } # end of: for ($i=1;$i<=$nintcoor;$i++)
      
         } # end of: if (/Geometries/)
# 
        if (/internal coordinates and forces/)
         {
          $_=<FILE>;
            for ($i=1;$i<=$nintcoor;$i++)
             {
	      $_=<FILE>;
              chop($_);
	      s/^ *//;
              ($dum,$dum,$force[$i])=split(/\s+/,$_,4);
             } # end of: for ($i=1;$i<=$nintcoor;$i++)
         } # end of: if (/internal coordinates and forces/)
     } # end of: while (<FILE>)
  close FILE;
#
 open (FILE,"$work/hessian") or die " ERROR: file $work/hessian not found!\n";
    $i=1;
    while (<FILE>)
     {
      chop($_);
      s/^ *//; 
      @help=split(/\s+/,$_,$nintcoor);
         for ($j=1;$j<=$nintcoor;$j++)
          {
           $hessian[$i][$j]=$help[$j-1];
          } # end of: for ($j=1;$j<=$nintcoor;$j++)
      $i++;
     } # end of: while (<FILE>)
 close FILE;
#
#
 open (FILE,"$work/message.rgf") or die " ERROR: file $work/message.rgf not found!\n";
    while (<FILE>)
     {
       if (/exact Hessian/) {chop($_);$hess_in_rgf="$_";}
       if (/updated Hessian/) {chop($_);$hess_in_rgf="$_";}
       if (/Hessian:/)
        {
         $i=1;
           for ($i2=1;$i2<=$nintcoor;$i2++)
            {
             $_=<FILE>;
             chop($_);
             s/^ *//;
             @help=split(/\s+/,$_,$nintcoor);
               for ($j=1;$j<=$nintcoor;$j++)
                {
                 $hessian_rgf[$i][$j]=$help[$j-1];
                } # end of: for ($j=1;$j<=$nintcoor;$j++)
             $i++;
            } # end of: for ($i2=1;$i2<=$nintcoor;$i2++)
        } # end of: if (/Hessian:/)
       if (/signature/) 
        {
	 chop($_);
	 $hessian_signature=$_;
	} # end of: if (/signature/) 
     } # end of: while (<FILE>) 
 close FILE; 
#
#   define end mark values
#
  $endvalues[1]="Error occurred (missing or wrong param.rgf)";
  $endvalues[4]="PREDICTOR step executed, next HESSIAN will be UPDATED.";
  $endvalues[5]="PREDICTOR step executed, request for EXACT HESSIAN next step.";
  $endvalues[6]="CORRECTOR step executed, next HESSIAN will be UPDATED.";
  $endvalues[7]="CORRECTOR step executed, request for EXACT HESSIAN next step.";
  $endvalues[10]="Stopping criterion satisfied";
 
 open (FILE,"$work/rgfendmark") or die " ERROR: file $work/rgfendmark not found!\n";
 $_=<FILE>;
 chop($_);
 s/^ *//;
 ($dum,$endcode)=split(/\s+/,$_,2);
 close FILE; 
#
#  get energy
#
 open (FILE,"$work/geoconvfl") or die " ERROR: file $work/geoconvfl not found!\n";
   while(<FILE>)
    {
     if (/emc/ || /eci/) 
      { ($dum,$energy)=split(/\s+/,$_,2); }
    }
 close FILE; 
  

##-----------------------------------------------------------------------
##   OUTPUT
##
  print "   RGF protocol for iteration: $iter\n";
  print "  ----------------------------------\n\n";
#
  print "  System with $nintcoor internal coordinated detected\n";
#
  print "\n  Total energy: $energy a.u.\n\n";
#
  print "\n\n  ***  RGF input file echo:  ***\n\n";
#
     print "  ***  Geometries:\n";
     print "   ---> source: WORK/cart2intls\n";
     print "  coord      current          change           next\n";
     print " ------------------------------------------------------\n";
       for ($i=1;$i<=$nintcoor;$i++)
        {
         print "   $i       $coor_old[$i]      $displ[$i]       $coor_new[$i]\n";  
        } # end of: for ($i=1;$i<=$nintcoor;$i++)
#
     print "\n\n  ***  Forces at current geom.:\n";
     print "   ---> source: WORK/cart2intls\n";
     print "  coord     force\n";
     print " -----------------------------\n";
       for ($i=1;$i<=$nintcoor;$i++)
        {
         print "   $i      $force[$i]\n";  
        } # end of: for ($i=1;$i<=$nintcoor;$i++)
#
#  

#
     print "\n\n  ***  Input Hessian matrix at current geom.:\n";
     print "  ----------------------------------------\n";
     print "   ---> source: WORK/hessian\n";
#
       $step=8;
       if ($step > $nintcoor) {$step=$nintcoor}
       $icount=0;
       for ($j1=1;$j1<=$nintcoor;$j1=$j1+$step)
        {
         $line="   ";
           for ($j=$icount+1;$j<=$icount+$step;$j++)
            {
             $line=$line."         ".$j;
            } # end of: for ($j=$icount+1;$j<=$icount+$step;$j++)
         print "$line\n";
           for ($i=1; $i<=$nintcoor;$i++)
            {
             $line="   $i ";
               for ($j=$icount+1;$j<=$icount+$step;$j++)
                {
                 $line=$line."  ".$hessian[$i][$j]
                } # end of: for ($j=$icount+1;$j<=$icount+$step;$j++)
             print "$line\n";
            } # end of: for ($i=1; $i<=$nintcoor;$i++)
          $icount=$icount+$step
        } # end of: for ($j1=1;$j1<=$nintcoor;$j1=$j1+$step)
  print "\n";
#
  print "\n\n  ***  Data from RGF program:  ***\n\n";
#
     print "  ***  Hessian matrix used in RGF (at current geom.):\n";
     print "   *$hessian_signature\n";
     print "   * RGF program using: $hess_in_rgf\n";
     print "  --------------------------------\n";
     print "   ---> source: WORK/message.rgf\n";
#
       $step=8;
       if ($step > $nintcoor) {$step=$nintcoor}
       $icount=0;
       for ($j1=1;$j1<=$nintcoor;$j1=$j1+$step)
        {
         $line="   ";
           for ($j=$icount+1;$j<=$icount+$step;$j++)
            {
             $line=$line."        ".$j;
            } # end of: for ($j=$icount+1;$j<=$icount+$step;$j++)
         print "$line\n";
           for ($i=1; $i<=$nintcoor;$i++)
            {
             $line="   $i ";
               for ($j=$icount+1;$j<=$icount+$step;$j++)
                {
                 $line=$line."  ".$hessian_rgf[$i][$j]
                } # end of: for ($j=$icount+1;$j<=$icount+$step;$j++)
             print "$line\n";
            } # end of: for ($i=1; $i<=$nintcoor;$i++)
          $icount=$icount+$step
        } # end of: for ($j1=1;$j1<=$nintcoor;$j1=$j1+$step) 
#
  print "\n >>>  Performed RGF calculations  <<<\n\n";
  print "  RGF end mark:\n     --> $endvalues[$endcode]\n\n";
#
# --------------------------------------------------------------------------
#
