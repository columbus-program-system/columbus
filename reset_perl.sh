#/bin/sh
# Reset the perl interpreters to "/usr/bin/env colperl"
# This is useful before commits/pulls to/from the repository
# Simply copy $COLUMBUS/colperl into your path and everything will work
#    alternatively call "install.automatic cpan"

echo "COLUMBUS: $COLUMBUS"
cd $COLUMBUS
export PERL="/usr/bin/env colperl"
$PERL $COLUMBUS/perlshebang xxx *.pl gaunder colinp runc prepinp DtoSblas StoDblas perlscripts/*.pl
