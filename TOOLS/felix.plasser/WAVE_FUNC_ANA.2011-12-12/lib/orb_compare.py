#!/usr/bin/python

"""
===================================================================================
version 1.0
author: Felix Plasser
usage: Subroutines for orbital analysis and manipulation.
====================================================================================
"""

# this is a somewhat lose set of different routines for orbital analysis
#   it is not very coherent and should possibly be cleaned up eventually
# this should preferably be a library and the script should be separated

import os,locale,sys
import numpy
import file_handler
from file_handler import line_to_words as ltw

def orb_compare(st_ind, en_ind, ref_file, mo_files, col_num=3,at_lists=[]):
    ref_MOs = MO_set(ref_file)
    ref_MOs.read_molden_file(at_lists=at_lists)
    ref_MOs.compute_inverse()

    for file in mo_files:
      #try: # -temp
        print '*** ' + file + ' ***'
        MOs = MO_set(file)
        MOs.read_molden_file(st=st_ind,en=en_ind)
        
        print "Occupations"
        for occ in MOs.occs:
            print "%6.3f"%occ,
        print
        
        comp_list = MOs.ret_composition(ref_MOs)
        tm = file_handler.table_maker([6,3] + col_num*[6,6,11])
        for ind,comp in enumerate(comp_list):
            tm.write_line(['-'+str(ind+st_ind)+'-','->'] + comp)
        print tm.return_table()

        if at_lists!=[]:
            print 'Distribution over fragments:'
            pops = MOs.ret_pop(ref_MOs.bf_lists)
            tm = file_handler.table_maker([6,3] + len(ref_MOs.bf_lists)*[12])
            for ind,pop in enumerate(pops):
                tm.write_line(['-'+str(ind+st_ind)+'-','->'] + pop)
            print tm.return_table()
      #except: # -temp
        #print 'Failed' # -temp

def put_sym(in_file, out_file):
    """
    Add the keyword Sym to every orbital in a molden file that does not contain it.
        This has to be done for example with molpro jobs.
    """
    MO = False
    ind=1
    w_file = open(out_file, 'w')
    for line in open(in_file,'r'):
      if '[' in line:
          MO = False

      if '[MO]' in line:
          MO = True
      elif MO:
        if ('Ene' in line) or ('ene' in line):
            w_file.write(' Sym='+str(ind)+'\n')
            ind += 1

      w_file.write(line)
    w_file.close()
    
def check_at_lists(at_lists,prt_lvl=0):
    """
    Check if an at_lists definition is useful.
    """
    num_lists = len(at_lists)
    
    lens = []
    sum_list = []
    for at_list in at_lists:
        sum_list+=at_list
        lens.append(len(at_list))
        
    if prt_lvl >= 1:
        print '*** Checking atoms lists'
        print '  %i lists with individual numbers of entries:'%(num_lists)
        print lens
        
    if prt_lvl >= 2:
        print 'at_lists=', at_lists
    
        
    numen = len(sum_list)
    maxen = max(sum_list)
    if prt_lvl >= 1:
        print '  %i total entries, with maximal value %i'%(numen,maxen)
        
    for i in xrange(1,maxen+1):
        ci = sum_list.count(i)
        if ci!=1:
            print 'Warning value %i present %i times in at_lists!'%(i,ci)
    

############
# Analysis of Mulliken Populations
def print_fragment_dist(out_file,**kwargs):
    """
    Print formatted information for "ret_fragment_dist".
    If an <out_file> is given, the information is appended there.
    """
    conf_info = ret_fragment_dist(**kwargs)
    
    print '  Exc./eV (osc.)| Exciton |   CT'
    print ' ----------------------------------'
    for state in conf_info:
        outstr = '  %.3f (%.3f) | % .4f | % .4f'%(state['exc_en'],state['osc_str'],state['exc_char'],state['CT_char'])
        print outstr 
        #print state['exc_en'],state['osc_str'],state['exc_char'],state['CT_char']
        
    if not out_file == None:
        outstr = ''
        for state in conf_info:
            outstr += '% .8f % .8f % .8f % .8f'%(state['exc_en'],state['osc_str'],state['exc_char'],state['CT_char'])
        outf=open(out_file,'a')
        outf.write(outstr+'\n')
        outf.close()
    
def print_fragment_dist_multi(out_file,**kwargs):
    """
    Print formatted information for "ret_fragment_dist_multi".
    If an <out_file> is given, the information is appended there.
    """
    conf_info = ret_fragment_dist_multi(**kwargs)

    print '  Exc./eV (osc.)|  CT     |  POS    |  DEL'
    print ' ----------------------------------'
    for state in conf_info:
        outstr = '  %.3f (%.3f) | % .4f | % .4f | % .4f'%(state['exc_en'],state['osc_str'],state['CT'],state['POS'],state['DEL'])
        print outstr
        #print state['exc_en'],state['osc_str'],state['exc_char'],state['CT_char']

    if not out_file == None:
        outstr = ''
        for state in conf_info:
            outstr += '% .8f % .8f % .8f % .8f % .8f'%(state['exc_en'],state['osc_str'],state['CT'],state['POS'],state['DEL'])
        outf=open(out_file,'a')
        outf.write(outstr+'\n')
        outf.close()


def ret_fragment_dist(at_lists,prog='tm',mull_args={},conf_args={}):
    """
    Compute the distribution of the excitation over fragments.
    Excited states are classified according to 4 possibilities:
    locally excited A
    locally excited B
    charge transfer A->B
    charge transfer B->A
    """
    if prog=='tm':
        mull_dict = {}
        tmp = ret_mull_tm(at_lists,**mull_args)
        for i,orb in enumerate(tmp[0]):
            mull_dict[orb] = tmp[1][i]
        #print mull_dict
        conf_info = ret_conf_tm(**conf_args)
    #print conf_info
    
    for state in conf_info:
        # the following two are temporary unnormalized quantities
        CT_char = 0.
        exc_char = 0.
        state['total_wt'] = 0.
        for conf in state['char']:
            #print conf
            # -> todo skip if orbital is not present
            try:
                dist_occ = mull_dict[conf['occ']]
                dist_virt = mull_dict[conf['virt']]
                
                # get the distribution over fragments for CT character and excitonic character per configuration
                conf['exc_char'] = .5*((dist_occ[1] + dist_virt[1]) - (dist_occ[0] + dist_virt[0]))
                conf['CT_char'] = .5*((dist_occ[0] + dist_virt[1]) - (dist_occ[1] + dist_virt[0]))
                #print 'weight, CT_char, exc_char:', conf['weight'], conf['CT_char'], conf['exc_char']
                
                # sum over configurations to get the character of the state
                #   in a more elaborate version one could consider phase as well but this may also require interference terms
                #print conf
                exc_char += conf['exc_char'] * conf['weight']
                CT_char += conf['CT_char'] * conf['weight']
                state['total_wt'] += conf['weight']
            except KeyError:
                print "Configuration skipped", conf
                
        state['CT_char'] = CT_char / state['total_wt']
        state['exc_char'] = exc_char / state['total_wt']
    return conf_info

def ret_fragment_dist_multi(at_lists,prog='tm',mull_args={},conf_args={}):
    """
    Compute the distribution of the excitation over multiple fragments.
    This is the new subroutine.
    This algorithm does not work properly when the orbitals are delocalized.
    """
    num_frag = len(at_lists)
    if prog=='tm':
        mull_dict = {}
        tmp = ret_mull_tm(at_lists,**mull_args)
        for i,orb in enumerate(tmp[0]):
            mull_dict[orb] = tmp[1][i]
    #   print mull_dict
        conf_info = ret_conf_tm(**conf_args)
    #print conf_info

    for state in conf_info:
        # Lambda_I and Lambda_F
         # I -> initial = "occ."
         # F -> final = "virt."
        state['lamI'] = numpy.zeros(num_frag,float)
        state['lamF'] = numpy.zeros(num_frag,float)
        state['total_wt'] = 0.
        for conf in state['char']:
            #print conf
            # -> todo skip if orbital is not present
            try:
                distI = mull_dict[conf['occ']]
                distF = mull_dict[conf['virt']]
            except KeyError:
                print "Warning: Configuration skipped", conf
            else:
                for ifrag in xrange(num_frag):
                   state['lamI'][ifrag]+=distI[ifrag] * conf['weight']
                   state['lamF'][ifrag]+=distF[ifrag] * conf['weight']
                state['total_wt'] += conf['weight']
         
        state['mu_I']=sum(ifrag*state['lamI'][ifrag] for ifrag in xrange(num_frag))/state['total_wt']
        state['mu_F']=sum(ifrag*state['lamF'][ifrag] for ifrag in xrange(num_frag))/state['total_wt']

        state['absd_I'] = sum(abs(ifrag-state['mu_I'])*state['lamI'][ifrag] for ifrag in xrange(num_frag))/state['total_wt']
        state['absd_F'] = sum(abs(ifrag-state['mu_F'])*state['lamF'][ifrag] for ifrag in xrange(num_frag))/state['total_wt']

        state['CT'] = state['mu_I'] - state['mu_F']
        state['POS']= 0.5 * (state['mu_I'] + state['mu_F']) + 1
        state['DEL']= state['absd_I'] + state['absd_F'] + 1
        
        #print state
    return conf_info


def print_mull_tm(at_lists,in_file='dscf.out'):
    """
    Print out Mulliken populations from <in_file>.
    <in_file> was produced with turbomole dscf and the line
    $pop mo 60-74 thrpl=0.0
    """
    print 'Mulliken populations for MOs summed over atoms lists'
    (labels,prt_list) = ret_mull_tm(at_lists=at_lists,in_file=in_file)
    for i,prt in enumerate(prt_list):
        out = '-'+labels[i]+'- -> '
        for val in prt:
            out+='  %.4f |'%val
        print out

def ret_mull_tm(at_lists,in_file='dscf.out'):
    """
    Get Mulliken populations, added up over <at_lists>, from <in_file>.
    <in_file> is the standard output of turbomole dscf with the line
    $pop mo 60-74 thrpl=0.0
    """
    ret_list = []
    labels = []
    num_out = len(at_lists)
    MULLIKEN = False
    for line in open(in_file, 'r'):
        if 'MULLIKEN BRUTTO' in line:
            MULLIKEN = True
            # extract the information in that section
        elif '=====' in line:
            MULLIKEN = False
        elif MULLIKEN:
            words = file_handler.line_to_words(line)
            if len(words)==0:
                pass
            elif 'energy' in line:
                labels.append(words[0])
                ret_list.append([0. for i in xrange(num_out)])
            else:
                at = int(words[0][:-1])
                for i,at_list in enumerate(at_lists):
                    if at in at_list:
                        # divide by 2 because Turbomole output is based on doubly occ. orbitals
                       ret_list[-1][i]+=.5*float(words[1])
    return (labels,ret_list)

def ret_conf_tm(in_file='grad.out', method='TDDFT'):
    """
    Return information about configurations in a Turbomole calculation.
    """
    if method.lower()=='tddft':
        rlines = open(in_file, 'r').readlines()[100:]
        ret_list = []
        occ_orb = False # section of the file
        for nr,line in enumerate(rlines):
            if 'excitation' in line and not 'vector' in line:
                ret_list.append({})
                words = line.split()
                ret_list[-1]['state_ind'] = int(words[0])
                #ret_list[-1]['irrep'] = file_handler.line_to_words(line)[2]
                ret_list[-1]['irrep'] = words[2]
                ret_list[-1]['tot_en'] = eval(rlines[nr+3][40:])
                ret_list[-1]['exc_en'] = eval(rlines[nr+7][40:])
                ret_list[-1]['osc_str'] = eval(rlines[nr+16][40:])
                ret_list[-1]['char'] = []
            elif 'occ. orbital' in line:
                occ_orb = True
                #print 'occ. orbital in line'
            elif occ_orb:
                words = file_handler.line_to_words(line)
                #print words
                if len(words) > 0:
                    #print 'words > 0'
                    ret_list[-1]['char'].append({})
                    ret_list[-1]['char'][-1]['occ'] = words[0]+words[1]
                    ret_list[-1]['char'][-1]['virt'] = words[3]+words[4]
                    ret_list[-1]['char'][-1]['weight'] = float(words[-1])/100.
                    # escf does not print the phase of the coefficient
                else:
                    occ_orb = False
    elif method.lower()=='ricc2':
        ret_list = []
        curr_osc = 0 # running index for oscillator strengths
        lines = open(in_file,'r') 
        while True: # loop over all lines
          try:
            line = lines.next()
          except StopIteration:
            break
          else:
            if 'Energy:   ' in line:
                ret_list.append({})
                words = line.split()
                ret_list[-1]['exc_en'] = float(words[3])
                for i in xrange(3): # skip two lines
                    line = lines.next()
                    
                words = file_handler.line_to_words(line,sep=[' ','|',':']) # type: RE0
                ret_list[-1]['state_ind'] = int(words[5])
                ret_list[-1]['irrep'] = words[3]
                
                for i in xrange(3): # skip three lines
                    line = lines.next()
                    
                ret_list[-1]['char'] = []
                while True: # loop over information about this excitation
                    line = lines.next()
                    words=file_handler.line_to_words(line,sep=[' ','|'])
                    if len(words) == 8:
                        ret_list[-1]['char'].append({})
                        ret_list[-1]['char'][-1]['occ'] = words[0]+words[1]
                        #ret_list[-1]['char'][-1]['i_occ'] = int(words[2]) # this is index is no langer correct in the molden file
                        ret_list[-1]['char'][-1]['virt'] = words[3]+words[4]
                        #ret_list[-1]['char'][-1]['i_virt'] = int(words[5])
                        ret_list[-1]['char'][-1]['coeff'] = float(words[6])
                        ret_list[-1]['char'][-1]['weight'] = float(words[7])/100.
                    else:
                        break
                    
            elif 'oscillator strength (length gauge)' in line:
                words = line.split()
                ret_list[curr_osc]['osc_str'] = float(words[5])
                curr_osc+=1

    else:
        print 'ret_conf_tm: method %s not implemented!'%method
        exit(1)
        
    return ret_list
    

########

class MO_set:
    """
    Main class that contains orbital information.
    """
    def __init__(self, file):
        self.file = file

    def read_molden_file(self, st=0, en='a', at_lists=[], nd=6, get_GTO=False, add_6d=False):
        """
        Read in MO coefficients from a molden File. The coefficients are read into the rows
          of self.mo_mat, therefore self.mo_mat = C^T according to the usual notation.
        As default all MOs are read in ('a' is larger than any number).
        Optionally assign the AO indices belonging to atoms in mutually exclusive <at_lists>.
           <nd> is the number of functions per d block.
        """
        MO = False
        GTO = False
        mo_vecs = []
        mo_ind = 0
        self.syms = [] # list with the orbital descriptions. they are entered after Sym in the molden file.
        self.occs = [] # occupations
        if at_lists != []:
            get_GTO=True
            curr_list=-1
            self.bf_lists=[[] for i in xrange(len(at_lists))] # contains information about which basis functions are on which fragment
            
        num_bas={'s':1,'p':3,'d':nd,'f':10,'g':15}
        orient={'s':['1'],'p':['x','y','z'],'d':['x2','y2','z2','xy','xz','yz'],'f':10*['?'],'g':15*['?']}
        if get_GTO:
            num_orb=-1
            curr_at=-1
            self.bas_types = [] # contains information about the type of each basis function

        for line in open(self.file, 'r'):
            # what section are we in
            if '[' in line:
                MO = False
                GTO = False
            
            if '[MO]' in line:
                MO = True
            # extract the information in that section
            elif MO:
                if not '=' in line:
                    if st <= mo_ind <= en: mo_vecs[-1].append(float(ltw(line)[1]))
                elif ('Ene' in line) or ('ene' in line):
                    mo_ind += 1
                    if st <= mo_ind <= en:
                        mo_vecs.append([])
                elif ('Sym' in line) or ('sym' in line):
                        self.syms.append(ltw(line,sep=' =')[-1])
                elif ('Occup' in line) or ('occup' in line):
                        if st <= mo_ind <= en: self.occs.append(float(ltw(line,sep=' =')[-1]))
            elif ('[D5]' in line) or ('[5D]' in line):
                num_bas['d']=5
                orient['d']=5*['?']
            elif ('[F7]' in line) or ('[7F]' in line):
                num_bas['f']=7
                orient['7']=7*['?']
            elif (get_GTO and '[GTO]' in line):
                GTO = True
                # extract the information in that section
            elif GTO:
                words=line.split()
                if len(words)==0: # empty line: atom is finished
                    curr_at = -1
                elif curr_at==-1:
                    curr_at = int(words[0])
                    if at_lists != []:
                        curr_list=-1
                        for ind,at_list in enumerate(at_lists):
                            if curr_at in at_list: curr_list = ind
                elif len(words)==3:
               # elif words[0] in num_bas: # words[0] is 's', 'p', or 'd'
                  try:
                    for i in xrange(num_bas[words[0]]):
                        self.bas_types.append({})
                        self.bas_types[-1]['at_ind'] = curr_at
                        self.bas_types[-1]['l'] = words[0]
                        self.bas_types[-1]['ml'] = orient[words[0]][i]
                        num_orb+=1
                        if at_lists != []:
                            if curr_list!=-1: self.bf_lists[curr_list].append(num_orb)
                  except KeyError:
                        print "\n Basis function type %s not supported!\n"%words[0]
                        raise

            
        self.num_extra_d = 0
        if add_6d: # add the 6th d-function to yield a square matrix
            # These do not form an orthonormal set with the remaining MOs, but that should not matter.
            # In fact it seems that their form does not affect the results at all.
            i = 0
            while(i<num_orb):
                if self.bas_types[i]['l'] == 'd':
                    print 'Adding d-function ...'
                    mo_vecs.append([])
                    mo_vecs[-1] = i*[0.] + [3.**(-.5)] + (num_orb-i)*[0.]
                    self.num_extra_d += 1
                    self.occs.append(0.)
                    i+=5
                i+=1

        if get_GTO:
            print 'Number of MOs read in: %i'%len(mo_vecs)
            print 'Dimension: %i,%i,...,%i'%(len(mo_vecs[0]),len(mo_vecs[1]),len(mo_vecs[-1]))
            print 'Number of basis functions parsed: ', num_orb+1
            
            if len(mo_vecs[0])!=num_orb+1:
                print '  Inconsistent number of basis functions!'
                sys.exit(55)

        try:
           self.mo_mat = numpy.array(mo_vecs)
        except ValueError:
           print "\n *** Unable to construct MO matrix! ***"
           print "Is there a mismatch between spherical/cartesian functions?\n ---"
           raise
        

    def compute_inverse(self,strict=False):
        """
        Compute the inverse of the MO matrix.
        If the matrix is not square, the transpose of the matrix with normalised lines is taken which leads to a projection (the result is the same if the lines are orthogonal).
        """
        try:
            #raise(numpy.linalg.LinAlgError)
            self.inv_mo_mat = numpy.linalg.inv(self.mo_mat)
        except numpy.linalg.LinAlgError:
            print 'Inverting failed, is the MO-coefficient matrix quadratic?'
            print '  This is either due to frozen orbitals or cartesian d(,f,...)-orbitals.'
            print 'Dimension of the MO-matrix: %i x %i\n'%(len(self.mo_mat),len(self.mo_mat[0]))
            if strict:
                raise
            else:
               #print '  Using the normalized transposed matrix instead.'
               #self.inv_mo_mat = numpy.array([line / numpy.linalg.norm(line)**2 for line in self.mo_mat]).transpose() 
               print '  Using the Moore-Penrose pseudo inverse instead.'
               self.inv_mo_mat = numpy.linalg.pinv(self.mo_mat) 


    def ret_composition(self, ref_MOs, add_descr=True, coeff_num=3, cutoff=0.01,digits=2):
        """
        Get the composition of the MOs according to a reference set of MOs.
        The <coeff_num> most important coefficients are printed.
        If <add_descr==True> the orbital descriptions are written into the output.
        """
        transform = numpy.dot(self.mo_mat, ref_MOs.inv_mo_mat)
       
        ret_list = [] 
        for line in transform:
            ret_list.append([])
            qu_line = line**2 
            sorted = qu_line.argsort() # sort according to squared coefficient
            for ind in xrange(-1, -coeff_num-1, -1):
                ret_list[-1] += ["-"+str(sorted[ind]+1)+"-"] # Index
                if add_descr: ret_list[-1] += [ref_MOs.ret_sym(sorted[ind]+1)] # description

                coeff = line[sorted[ind]] # coefficient
                if abs(coeff) < cutoff: coeff = 0.
                ret_list[-1] += ['(%+.*f) |'%(digits,coeff)]
        return ret_list

    def ret_sym(self, MO_ind):
        """
        Return the desription of an MO as found in the molden file.
        """
        try:
            return self.syms[MO_ind-1]
        except:
            return MO_ind

    def ret_pop(self, bf_lists, digits=3, format=True):
        """
        Return populations for the MOs summed up over a set of atoms.
        This is a simple analysis assuming orthogonal AOs.
        """
        #print '***',self.mo_mat
        ret_list = []
        for line in self.mo_mat:
            norm = numpy.linalg.norm(line)
            #print '**', line
            ret_list.append([])
            for bf_list in bf_lists:
                bfsum = 0.
                for bf in bf_list:
                    #print '*',line[bf], ret_list[-1][-1]
                    bfsum+=(line[bf]/norm)**2
                if format:
                    ret_list[-1].append(' %.*f |'%(digits,bfsum))
                else:
                    ret_list[-1].append(bfsum)
    
        return ret_list
