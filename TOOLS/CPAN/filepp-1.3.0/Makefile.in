########################################################################
#
# filepp is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; see the file COPYING.  If not, write to
# the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
#
########################################################################
########################################################################
#
#  Project      :  File Pre Processor
#  Filename     :  $RCSfile: Makefile.in,v $
#  Originator   :  $Author: hlischka $
#  Maintainer   :  Darren Miller: darren@cabaret.demon.co.uk
#  File version :  $Revision: 2.2 $
#  Last changed :  $Date: 2009/08/25 10:53:09 $
#  Description  :  Main Makefile
#  Licence      :  GNU copyleft
#
########################################################################

##############################################################################
# Paths
##############################################################################
prefix = @prefix@
exec_prefix = @exec_prefix@
bindir = @bindir@
mandir = @mandir@
srcdir = @srcdir@
VPATH  = @srcdir@
moduledir = @moduledir@

##############################################################################
# Programs and files
##############################################################################
FILEPP     = ./filepp
FILEPPMAN  = filepp.1
FILEPPHTML = filepp.html
FILEPPLSM  = filepp.lsm
DISTRIBDIR = filepp-@VERSION@
DISTRIB    = $(DISTRIBDIR).tar.gz
CONFIGURE  = $(srcdir)/configure
ECHO       = echo
RM         = rm -f
CP         = cp -pr
MKDIR      = $(srcdir)/mkinstalldirs
INSTALL    = @INSTALL@
TAR        = tar
GZIP       = gzip
MAN2HTML   = man2html

##############################################################################
# Dependencies
##############################################################################

IFLAGS = -I$(srcdir)
DFLAGS = -DHIDDEN

default: $(FILEPP) man

$(FILEPP): $(FILEPPIN)
	$(CONFIGURE)

# man page
$(FILEPPMAN): $(srcdir)/filepp.1.in

man: $(FILEPPMAN)

# lsm entry
$(FILEPPLSM): $(srcdir)/filepp.lsm.in

lsm:	$(FILEPPLSM)

# check, test, demo, whatever you wanna call it
test:	test.html

check:	test

example:test

demo:	test

test.html: $(FILEPP) $(srcdir)/test.html.in \
	$(srcdir)/header.html.in $(srcdir)/footer.html.in $(srcdir)/testsuite

# html version of man page
$(FILEPPHTML): $(FILEPPMAN)
	$(MAN2HTML) $(FILEPPMAN) > $(FILEPPHTML)

# installation
install: $(FILEPP) $(FILEPPMAN)
	@$(MKDIR) $(bindir)
	$(INSTALL) $(FILEPP) $(bindir)
	@$(MKDIR) $(mandir)/man1
	$(INSTALL) -m 644 $(FILEPPMAN) $(mandir)/man1
	$(MKDIR) $(moduledir)
	$(INSTALL) -m 644 $(srcdir)/modules/for/for.pm $(moduledir)
	$(INSTALL) -m 644 $(srcdir)/modules/case/tolower.pm $(moduledir)
	$(INSTALL) -m 644 $(srcdir)/modules/case/toupper.pm $(moduledir)
	$(INSTALL) -m 644 $(srcdir)/modules/comment/c-comment.pm $(moduledir)
	$(INSTALL) -m 644 $(srcdir)/modules/comment/hash-comment.pm $(moduledir)
	$(INSTALL) -m 644 $(srcdir)/modules/literal/literal.pm $(moduledir)

# distribution
distrib: $(FILEPPMAN) $(FILEPPLSM) $(FILEPPHTML)
	-$(MKDIR) $(DISTRIBDIR)
	$(CP) $(FILEPPMAN)       $(FILEPPLSM)         $(FILEPPHTML) \
	$(srcdir)/filepp.lsm.in  $(srcdir)/filepp.1.in \
	$(srcdir)/Makefile.in    $(srcdir)/configure.in \
	$(srcdir)/COPYING        $(srcdir)/INSTALL    $(srcdir)/README \
	$(srcdir)/install-sh     $(srcdir)/configure  $(srcdir)/mkinstalldirs \
	$(srcdir)/filepp.in      $(srcdir)/ChangeLog \
	$(srcdir)/footer.html.in $(srcdir)/header.html.in \
	$(srcdir)/test.html.in   $(srcdir)/testsuite  $(srcdir)/testinc \
	$(srcdir)/testmod.pm \
        $(DISTRIBDIR)
	-$(MKDIR) $(DISTRIBDIR)/modules
	$(CP) $(srcdir)/modules/for $(DISTRIBDIR)/modules
	$(CP) $(srcdir)/modules/case $(DISTRIBDIR)/modules
	$(CP) $(srcdir)/modules/comment $(DISTRIBDIR)/modules
	$(CP) $(srcdir)/modules/literal $(DISTRIBDIR)/modules
	$(TAR) czvf $(DISTRIB) $(DISTRIBDIR)
	$(RM) -r $(DISTRIBDIR)

# clean and distclean
clean:
	$(RM) test.html *~

distclean: clean
	$(RM) Makefile $(FILEPPLSM) $(FILEPP) $(FILEPPMAN) $(FILEPPHTML) \
	config.cache config.status config.log file $(DISTRIB)

##############################################################################
# Rules
##############################################################################

.SUFFIXES: .in .html.in .html .1.in .1 .lsm.in .lsm

COMPILE.filepp  = $(FILEPP) $(DFLAGS) $(IFLAGS)

# rule to convert .html.in to .html
.html.in.html:
	@$(ECHO) Processing $<
	$(COMPILE.filepp) $< -o $@

# rule to convert 1.in to .1 (man page)
.1.in.1:
	@$(ECHO) Processing $<
	$(COMPILE.filepp) $< -o $@

# rule to convert lsm.in to .lsm
.lsm.in.lsm:
	@$(ECHO) Processing $<
	$(COMPILE.filepp) -k $< -o $@
