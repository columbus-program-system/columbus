########################################################################
#
# literal is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; see the file COPYING.  If not, write to
# the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
#
########################################################################
#
#  Project      :  File Preprocessor - literal module
#  Filename     :  $RCSfile: literal.pm,v $
#  Author       :  $Author: hlischka $
#  Maintainer   :  Darren Miller: darren@cabaret.demon.co.uk
#  File version :  $Revision: 2.2 $
#  Last changed :  $Date: 2009/08/25 10:53:10 $
#  Description  :  This module allows literal strings ("string") to
#                  pass though filepp WITHOUT any macros in the string
#                  being replaces
#  Licence      :  GNU copyleft
#
########################################################################
# THIS IS A FILEPP MODULE, YOU NEED FILEPP TO USE IT!!!
# usage: filepp -m literal.pm <files>
########################################################################

package Literal;

use strict;

# version number of module
my $VERSION = '1.0.0';

# flag to say if in string or not
my $in_string = 0;

# if LITERAL_REVERSE is defined, only replace macros that are in strings.
if(Filepp::Ifdef("LITERAL_REVERSE")) { $in_string = 1; }

# char to start and end literal strings with
my $literal = '"';

# replace all macros that do NOT appear in literal strings
sub ReplaceDefines
{
    my $string = shift;
    # check for literal in string
    if($string =~ /$literal/) {
	my @Chars;
	my $char;
	my $substr = "";
	# split string into chars (can't use split coz it deletes \n at end)
	for($char=0; $char<length($string); $char++) {
	    push(@Chars, substr($string, $char, 1));
	}
	$string = "";

	# strip out strings and replace macros in everything else
	foreach $char (@Chars) {
	    if($char eq $literal)  {
		if(!$in_string) {
		    $string = $string.Filepp::ReplaceDefines($substr).$literal;
		    $in_string = 1;
		}
		else {
		    $string = $string.$substr.$literal;
		    $in_string = 0;
		}
		$substr = "";
	    }
	    else {
		$substr = $substr.$char;
	    }
	}
	# take care of leftovers
	if(!$in_string) {
	    $string = $string.Filepp::ReplaceDefines($substr);
	}
	else {
	    $string = $string.$substr;
	}
	    
    }
    elsif(!$in_string) {
	$string = Filepp::ReplaceDefines($string);
    }
    return $string;    
}

# remove filepp's ReplaceDefines routine and replace it with Literals.
Filepp::RemoveProcessor("ReplaceDefines");
Filepp::AddProcessor("Literal::ReplaceDefines");

return 1;

########################################################################
# End of file
########################################################################
