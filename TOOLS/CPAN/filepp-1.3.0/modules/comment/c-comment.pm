########################################################################
#
# c-comment is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; see the file COPYING.  If not, write to
# the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
#
########################################################################
#
#  Project      :  File Preprocessor - c-comment module
#  Filename     :  $RCSfile: c-comment.pm,v $
#  Author       :  $Author: hlischka $
#  Maintainer   :  Darren Miller: darren@cabaret.demon.co.uk
#  File version :  $Revision: 2.2 $
#  Last changed :  $Date: 2009/08/25 10:53:10 $
#  Description  :  This module removes all C and C++ style comments from
#                  a file.
#  Licence      :  GNU copyleft
#
########################################################################
# THIS IS A FILEPP MODULE, YOU NEED FILEPP TO USE IT!!!
# usage: filepp -m c-comment.pm <files>
########################################################################

package CComment;

use strict;

# version number of module
my $VERSION = '1.0.0';

# remove all comments from string
sub RemoveComments
{
    my $string = shift;

    # remove all C comments
    while($string =~ /\/\*/) {
	# remove everything up to comment
	my $newstr = substr($string, 0, index($string, "/*"), "");
	# check for end of comment in this line
	if($string =~ /\*\//) {
	    # get rest of line following end of comment
	    $string = $newstr.substr($string, index($string, "*/")+2);
	}
	# multi-line comment
	else {
	    # find line with end of comment
	    $string = Filepp::GetNextLine();
	    while($string && $string !~ /\*\//) {
		$string = Filepp::GetNextLine();
	    }
	    if($string) {
		# get rest of line following end of comment
		$string = $newstr.substr($string, index($string, "*/")+2);
	    }
	    else {
		$string = $newstr;
	    }
	}
    }
    
    # remove all C++ comments
    if($string =~ /\/\//) {
	# remove everything before comment
	$string = substr($string, 0, index($string, "//"), "");
	# put newline back if line not blank
	if($string =~ /\S/) { $string = $string."\n"; }
    }

    return $string;
}
Filepp::AddProcessor("CComment::RemoveComments");

return 1;

########################################################################
# End of file
########################################################################
