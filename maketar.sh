#!/bin/sh
#
#this shell script creates the $COLUMBDIR tar files for distribution.
#It has to be executed in the directory where the $COLUMBDIR dircetory
#is located.
#
echo "usage: maketar.sh  [-binary|-source] [-withmolcas]"
GTAR=tar
NAME=`grep MACHINEID install.config| sed -e 's/MACHINEID//' | sed -e 's/^ *//'`
VERS=`grep COLUMBUSVERSION install.config | sed -e 's/COLUMBUSVERSION//' | sed -e 's/ //g' |sed -e 's/\//+/g' `
DATUM=` date --rfc-3339=date`
NAME="Col${VERS}_${DATUM}_${NAME}"
if test "$2" = "-withmolcas"
  then
    echo "  -withmolcas set. Setting AUTOMOLCAS ..."
    cp install.config install.config.tmp
    grep -v MOLCAS install.config.tmp > install.config
    echo AUTOMOLCAS ON >> install.config
    NAME="${NAME}_molcas"
fi
echo "Columbus Version: $VERS"

if test "$1" = "-binary"
  then
######## Binary Distribution, statically linked ###############

find Columbus -type f -not \( -name '.cvs*' -or -name '.\#*'  -or -name 'cmdcin' -or -name 'sedscrip' \)  -print  | grep -v source | grep -v special | sed -e '/CVS/d' | grep -v '\.o'  >  Col.binary

find Columbus/source/iargos -type f -print -not -name '*.axo'| sed -e '/CVS/d' >> Col.binary
echo "install.config" >> Col.binary
echo "install.automatic" >> Col.binary
echo "INFO" >> Col.binary

find TOOLS/CPAN -type f  -not  -name '.[axo]' -print | sed -e '/CVS/d' > CPAN.Files
cat CPAN.Files >> Col.binary

echo "generating binary distribution for $NAME"
$GTAR -cf ${NAME}_bin.tar -T Col.binary
#$GTAR -rzf ${NAME}_bin.tar.gz -T CPAN.Files
echo "generated Columbus.binary ${NAME}_bin.tar "

MPIDIR=`grep MPI_MAINDIR install.config| sed -e 's/MPI_MAINDIR//' | sed -e 's/^ *//'`
#echo "generating binary distribution for (mpilibs)  $NAME from $MPIDIR"
#$GTAR  --transform='s/^.*\/mpich/mpich/' -czf mpich.binary.$NAME.tar.gz $MPIDIR
#echo "generated  mpich.binary.$NAME.tar.gz "

if test "$2" = "-withmolcas"
  then
   MCDIR=OpenMolcas/build
   echo "adding OpenMolcas files from $MCDIR to $NAME"
   $GTAR -rf ${NAME}_bin.tar $MCDIR/[a-j,n-z]* $MCDIR/molcas.rte $MCDIR/LICENSE $MCDIR/.molcas*
   echo "files appended to ${NAME}_bin.tar"
fi

gzip -v ${NAME}_bin.tar

fi

if test "$1" = "-source" ; then

####################################################################
find Columbus  -type f  -not \( -name '*.[axo]' -or  -name '*.[ax].*'  -or -name '.cvs*' -or -name '.\#*' -or -name 'pciudg*.f' -or -name 'pciden*.f' -or -name 'cmdcin' -or -name pcigrd*.f -or -name pmcscf*.f \
    -or -name 'sedscrip' \)    -print | sed -e '/CVS/d' | sed -e '/gcf/d'  >  Col.Files

find TOOLS/CPAN -type f  -not  -name '.[axo]' -print | sed -e '/CVS/d' > CPAN.Files

#find TOOLS/Global_Arrays \( -type d -a  -name 'ga.4.3.3*' \)  -o  \( -type f  -name 'makega*' \) -print | sed -e '/CVS/d' > GA.Files
echo "install.config" >> Col.Files
echo "install.automatic" >> Col.Files
echo "INFO" >> Col.Files
echo "install.cpan" >> Col.Files
echo "filepp" >> Col.Files
echo "maketar.sh" >> Col.Files

cat CPAN.Files >> Col.Files

echo " standard.tar.gz  ... "
$GTAR  -cf ${NAME}_src.tar  -T Col.Files
echo " ...  done  , file list in Col.Files  CPAN.Files"

if [ "$2" = "-withmolcas" ]; then
  MCDIR=OpenMolcas
  echo "adding OpenMolcas files from $MCDIR to $NAME"
  $GTAR -rf ${NAME}_src.tar $MCDIR/[A-Z]* $MCDIR/basis_library $MCDIR/[c-z]*
fi

gzip -v ${NAME}_src.tar

fi
