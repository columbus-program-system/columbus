COLUMBUS
========

The COLUMBUS Program System is free software: you can redistribute it and/or modify it
under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, version 3.0.

COLUMBUS is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with COLUMBUS. If not, see <http://www.gnu.org/licenses/>.

Copyright (C) 1980-2022, The COLUMBUS Authors

Contributors
============

The COLUMBUS open-source repository is managed by

* Hans Lischka
* Ron Shepard
* Scott R. Brozell
* Felix Plasser

The following is a list of people who have contributed at some point
to the code of COLUMBUS. They are hereby acknowledged and, along with the four
authors mentioned above, collectively identified as "The COLUMBUS Authors".

* Reinhard Ahlrichs
* Mario	Barbatti
* E.V. Beck
* Lachlan T. Belcher
* Jean-Philippe Blaudeau
* H.-J. Böhm
* Frank B. Brown
* A. H. H. Chang
* D.C. Comeau
* Holger Dachsel
* Michal Dallos
* Anita	Das
* C. Ehrhardt
* M. Ernzerhof
* G. Gawboy
* R. Gdanitz
* Yafu Guan
* Robert J.	Harrison
* S. Irle
* Gary Kedziora
* K. Kim
* T. Kovar
* Spiridoula Matsika
* M. Minkoff
* Thomas Müller
* Reed Nieman
* Jaroslaw Nieplocha
* M. Pepper
* Russel M.	Pitzer
* Matthias Ruckenbauer
* P. Scharf
* H. Schiffer
* M. Schindler
* M. Schüler
* Michael Schuurmann
* Bernhard Sellner
* M. Seth
* Isaiah Shavitt
* Rene F.K.	Spada
* Eric A. Stahlberg
* Peter G. Szalay
* Jaroslaw J. Szymczak
* David R. Yarkony
* Satoshi Yabushita
* Zhiyong Zhang
* J.-G. Zhao

External support
----------------

In addition, we are glad to acknowledge the support by external groups:

* T. Helgaker, H. J. Jensen, P. Jørgensen, J. Olsen, P. R. Taylor, K. Ruud and coworkers for the AO integral and gradient routines of DALTON
* P. Pulay and coworkers for the internal coordinate and GDIIS geometry optimization programs
* J. Simons for the input program IARGOS from MESSKIT.
* W. Quapp and coworkers for the saddle-point program using reduced gradient following (RGF)
* Support for the parallelization of COLUMBUS came from R. J. Harrison and J. Nieplocha (Pacific Northwest National Laboratories) through the global array tools.
* E. Carter and coworkers for the local MRCI module
* The OpenMolcas team for the AO integral and gradient routines of OpenMolcas and for support in creating the interface
